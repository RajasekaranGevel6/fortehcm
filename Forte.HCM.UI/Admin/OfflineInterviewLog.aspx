﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteAdminMaster.Master"
    CodeBehind="OfflineInterviewLog.aspx.cs" Inherits="Forte.HCM.UI.Admin.OfflineInterviewLog" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/SiteAdminMaster.Master" %>
<asp:Content ID="OfflineInterviewLog_bodyContent" ContentPlaceHolderID="SiteAdminMaster_body"
    runat="server">
    <script type="text/javascript">
        function ExpORCollapseRows(targetControl) {
            var ctrl = document.getElementById('<%=OfflineInterviewLog_stateExpandHiddenField.ClientID %>');
            targetValue = ctrl.value;
            if (targetValue == "0") {
                targetControl.innerHTML = "Collapse All";
                ExpandAllRows(targetControl);
                ctrl.value = 1;
            }
            else {
                targetControl.innerHTML = "Expand All";
                CollapseAllRows(targetControl);
                ctrl.value = 0;
            }

            return false;
        }
        function ExpandAllRows(targetControl) {
            var gridCtrl = document.getElementById("<%= OfflineInterviewLog_DetailsDIV.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf("OfflineInterviewLog_MessagedetailsDiv") != "-1") {
                        rowItems[indexRow].style.display = "block";
                    }
                }
            }
            else {
                targetControl.innerHTML = "Expand All";
                CollapseAllRows(targetControl);
                ctrl.value = 0;
            }
            return false;
        }
        //Hide all the question's Option panel
        function CollapseAllRows(targetControl) {
            var gridCtrl = document.getElementById("<%= OfflineInterviewLog_DetailsDIV.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf("OfflineInterviewLog_MessagedetailsDiv") != "-1") {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }

            return false;
        }

    </script>
    <asp:UpdatePanel ID="OfflineInterviewLog_updatePanel" runat="server">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 72%" class="header_text_bold">
                                    <asp:Literal ID="OfflineInterviewLog_headerLiteral" runat="server" Text="Offline Interview Log"></asp:Literal>
                                </td>
                                <td width="28%" align="right">
                                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                        <tr>
                                            <td style="width: 52%">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="width: 16%" align="right">
                                                <asp:LinkButton ID="OfflineInterviewLog_topResetLinkButton" runat="server" Text="Reset"
                                                    SkinID="sknActionLinkButton" OnClick="OfflineInterviewLog_topResetLinkButton_Click"></asp:LinkButton>
                                            </td>
                                            <td width="4%" align="center" class="link_button">
                                                |
                                            </td>
                                            <td width="18%" align="left">
                                                <asp:LinkButton ID="OfflineInterviewLog_topCancelLinkButton" runat="server" Text="Cancel"
                                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="msg_align">
                        <asp:UpdatePanel ID="OfflineInterviewLog_topMessageUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="OfflineInterviewLog_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                <asp:Label ID="OfflineInterviewLog_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td class="tab_body_bg">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td>
                                                <div id="OfflineInterviewLog_SummaryDIV" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <div>
                                                                    <asp:UpdatePanel ID="OfflineInterviewLog_selectRoleUpdatePanel" runat="server">
                                                                        <ContentTemplate>
                                                                            <table width="100%" cellpadding="0" cellspacing="3" border="0">
                                                                                <tr>
                                                                                    <td style="vertical-align: middle">
                                                                                        <div id="OfflineInterviewLog_searchImageDIV" style="display: block; float: left; width: 100%"
                                                                                            runat="server">
                                                                                            <asp:UpdatePanel ID="OfflineInterviewLog_searchImageDIV_updatePanel" runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <table width="100%" cellpadding="0" cellspacing="3" border="0" class="panel_inner_body_bg">
                                                                                                        <tr>
                                                                                                            <td style="width: 7%">
                                                                                                                <div>
                                                                                                                    <asp:Label ID="OfflineInterviewLog_systemNameLabel" runat="server" Text="System Name"
                                                                                                                        SkinID="sknLabelFieldHeaderText">
                                                                                                                    </asp:Label>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td style="width: 200px">
                                                                                                                <asp:TextBox ID="OfflineInterviewLog_systemNameTextBox" runat="server" Width="150px"
                                                                                                                    MaxLength="256"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td style="width: 5%">
                                                                                                                <asp:Label ID="OfflineInterviewLog_macAddressLabel" runat="server" Text="MAC Address"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 100px">
                                                                                                                <asp:TextBox ID="OfflineInterviewLog_macAddressTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td style="width: 5%" align="right">
                                                                                                                <asp:Label ID="OfflineInterviewLog_dateLabel" runat="server" Text="Date" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <table width="97.5%" cellpadding="0" cellspacing="0" border="0">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 37%">
                                                                                                                            <asp:TextBox ID="OfflineInterviewLog_datePopupTextBox" runat="server" MaxLength="10"
                                                                                                                                AutoCompleteType="None" Text=""></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td align="left">
                                                                                                                            <asp:ImageButton ID="OfflineInterviewLog_datePopupCalendarImageButton" SkinID="sknCalendarImageButton"
                                                                                                                                runat="server" ImageAlign="Middle" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <ajaxToolKit:MaskedEditExtender ID="OfflineInterviewLog_dateMaskedEditExtender" runat="server"
                                                                                                                    TargetControlID="OfflineInterviewLog_datePopupTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                                                                    OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                                                                                                    DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                                                                                <%--  <ajaxToolKit:MaskedEditValidator ID="OfflineInterviewLog_dateMaskedEditValidator" runat="server"
                                                                                                                    ControlExtender="OfflineInterviewLog_dateMaskedEditExtender" ControlToValidate="OfflineInterviewLog_datePopupTextBox"
                                                                                                                    EmptyValueMessage="Expiry Date is required" InvalidValueMessage="Expiry Date is invalid"
                                                                                                                    Display="None" TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                                                                                                    ValidationGroup="MKE" />--%>
                                                                                                                <ajaxToolKit:CalendarExtender ID="OfflineInterviewLog_dateCustomCalendarExtender" runat="server"
                                                                                                                    TargetControlID="OfflineInterviewLog_datePopupTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy"
                                                                                                                    PopupPosition="BottomLeft" PopupButtonID="OfflineInterviewLog_datePopupCalendarImageButton" />
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_height_5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right" colspan="2">
                                                                                        <asp:Button ID="OfflineInterviewLog_searchButton" runat="server" Text="Search" SkinID="sknButtonId"
                                                                                            Style="margin-left: 19px" OnClick="OfflineInterviewLog_searchButton_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_height_5">
                                </td>
                            </tr>
                            <caption>
                            </caption>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" style="width: 98%">
                                    <asp:Label ID="OfflineInterviewLog_assignRolesLabel" runat="server"></asp:Label>
                                </td>
                                <td align="right" style="width: 2%">
                                    <asp:HiddenField ID="OfflineInterviewLog_restoreHiddenField" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="OfflineInterviewLog_TR" runat="server">
                    <td align="center" class="header_bg">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" class="header_text_bold" style="width: 50%">
                                    <asp:Literal ID="OfflineInterviewLog_searchResutlsHeaderLiteral" runat="server" Text="Search Results"></asp:Literal>
                                    <asp:Label ID="OfflineInterviewLog_HeaderHelpLabel" runat="server" SkinID="sknLabelText"
                                        Text=" - Click column headers to sort"></asp:Label>
                                </td>
                                <td style="width: 48%" align="right">
                                    <table border="0" cellpadding="0" cellspacing="4" align="right">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="OfflineInterviewLog_resultsExpandLinkButton" runat="server" Text="Expand All"
                                                    SkinID="sknActionLinkButton" OnClientClick="javascript:return ExpORCollapseRows(this);"
                                                    Visible="false"></asp:LinkButton>
                                                <asp:HiddenField ID="OfflineInterviewLog_stateExpandHiddenField" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right" style="width: 50%">
                                    <span id="OfflineInterviewLog_UparrowSpan" runat="server" style="display: block;">
                                        <asp:Image ID="OfflineInterviewLog_UpArrow" runat="server" SkinID="sknMinimizeImage" />
                                    </span><span id="OfflineInterviewLog_DownarrowSpan" runat="server" style="display: none;">
                                        <asp:Image ID="OfflineInterviewLog_DownArrow" runat="server" SkinID="sknMaximizeImage" />
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="display: none">
                            <asp:Button ID="OfflineInterviewLog_deleteMessageHiddenButton" runat="server" />
                        </div>
                        <asp:Panel ID="OfflineInterviewLog_deleteMessagePopupPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_confirm_remove">
                            <uc2:ConfirmMsgControl ID="OfflineInterviewLog_deleteMessageConfirmMsgControl" runat="server"
                                OnOkClick="OfflineInterviewLog_deleteMessageConfirmMsgControl_okClick" OnCancelClick="OfflineInterviewLog_deleteMessageConfirmMsgControl_cancelClick" />
                            <asp:HiddenField ID="OfflineInterviewLog_deleteHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="OfflineInterviewLog_deleteFormModalPopupExtender"
                            runat="server" PopupControlID="OfflineInterviewLog_deleteMessagePopupPanel" TargetControlID="OfflineInterviewLog_deleteMessageHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="OfflineInterviewLog_DetailsDIV" runat="server" style="height: 600; overflow: auto;">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="grid_body_bg">
                                        <asp:UpdatePanel runat="server" ID="OfflineInterviewLog_GridViewUpdatePanel">
                                            <ContentTemplate>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left">
                                                            <div style="overflow: auto;" runat="server" id="OfflineInterviewLog_resultsDiv" visible="true">
                                                                <asp:GridView ID="OfflineInterviewLog_resultsGridView" runat="server" Visible="true" OnRowDataBound="OfflineInterviewLog_resultsGridView_RowDataBound"
                                                                    OnSorting="OfflineInterviewLog_resultsGridView_Sorting" OnRowCreated="OfflineInterviewLog_resultsGridView_RowCreated"
                                                                    OnRowCommand="OfflineInterviewLog_resultsGridView_RowCommand">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="OfflineInterviewLog_showDesktopImageButton" runat="server" SkinID="sknCyberProctorDesktopImage"
                                                                                    ToolTip="Show Desktop Images" CommandArgument='<%# Eval("LogID") %>' CommandName="DesktopImages" Visible='<%# IsImageAvailable(Eval("IsDesktopImageAvailable").ToString())%>'/>
                                                                                <asp:ImageButton ID="OfflineInterviewLog_showWebcamImageButton" runat="server" SkinID="sknCyberProctorWebCamImage"
                                                                                    ToolTip="Show Webcam Images" CommandName="WebcamImages" CommandArgument='<%# Eval("LogID") %>' Visible='<%# IsImageAvailable(Eval("IsWebcamImageAvailable").ToString())%>'/>
                                                                                <asp:ImageButton ID="OfflineInterviewLog_viewLogFileImageButton" runat="server" SkinID="sknCyberProctorViewLogFile"
                                                                                    ToolTip="View Log File" CommandName="ViewLogFile" CommandArgument='<%# Eval("LogID") %>' Visible='<%# IsLogFileAvailable(Eval("LogFileID").ToString())%>'/>
                                                                                <asp:ImageButton ID="OfflineInterviewLog_deleteImageButton" runat="server" ToolTip="Delete Log Message"
                                                                                    CommandName="DeleteMessage" CommandArgument='<%# Eval("LogID") %>' SkinID="sknDeleteCyberProctorMessageImageButton" />
                                                                                <asp:HiddenField ID="OfflineInterviewLog_LogIDHiddenField" runat="server" Value='<%# Eval("LogID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="System Name" SortExpression="SYSTEMNAME" HeaderStyle-Width="80px">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="OfflineInterviewLog_systemNameLinkButton" runat="server" Text='<%# Eval("SystemName") %>'></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="MAC Address" SortExpression="MACADDRESS" HeaderStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:Literal ID="OfflineInterviewLog_macAddressnGridLiteral" runat="server" Text='<%# Eval("MacAddress") %>'></asp:Literal>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField HeaderText="IP Address" DataField="IpAddress" HeaderStyle-Width="150px"
                                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" SortExpression="IPADDRESS">
                                                                        </asp:BoundField>
                                                                        <asp:BoundField HeaderText="Operating System Info" DataField="OSInfo" HeaderStyle-Width="245px"
                                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" SortExpression="OSINFO">
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="Date & Time" SortExpression="DATE">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="OfflineInterviewLog_dateDateLabel" runat="server" Text='<%# Eval("CreatedDate") %>'
                                                                                    Width="125px"></asp:Label>
                                                                                <tr>
                                                                                    <td class="grid_padding_right" colspan="7">
                                                                                        <div id="OfflineInterviewLog_MessagedetailsDiv" runat="server" style="display: none;
                                                                                            overflow: auto; height: 142px" class="table_outline_bg">
                                                                                            <div id="OfflineInterviewLog_Message_detailsDiv" runat="server" style="overflow: auto;
                                                                                                height: 133px">
                                                                                                <asp:GridView ID="OfflineInterviewLog_detailsGridView" runat="server">
                                                                                                    <Columns>
                                                                                                        <asp:BoundField HeaderText="Message" DataField="Message" HeaderStyle-Width="200px"
                                                                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" />
                                                                                                        <asp:BoundField HeaderText="Date" DataField="CreatedDate" HeaderStyle-Width="200px"
                                                                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" />
                                                                                                    </Columns>
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                        </div>
                                                                                        <%-- popup DIV --%>
                                                                                        <a href="#OfflineInterviewLog_focusmeLink" id="OfflineInterviewLog_focusDownLink" runat="server">
                                                                                        </a><a href="#" id="OfflineInterviewLog_focusmeLink" runat="server"></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="OfflineInterviewLog_searchButton" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                </tr>
                <caption>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" runat="server" id="OfflineInterviewLog_PageNumberHidden" value="1" />
                            <uc2:PageNavigator ID="OfflineInterviewLog_bottomPagingNavigator" runat="server" />
                        </td>
                    </tr>
                    </tr>
                </caption>
            </table>
            </div> </caption> </td> </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="OfflineInterviewLog_bottomMessageUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="OfflineInterviewLog_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            <asp:Label ID="OfflineInterviewLog_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="header_bg">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 72%" class="header_text_bold">
                            </td>
                            <td width="28%" align="right">
                                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                    <tr>
                                        <td style="width: 52%">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td style="width: 16%" align="right">
                                            <asp:LinkButton ID="OfflineInterviewLog_bottomResetLinkButton" runat="server" Text="Reset"
                                                SkinID="sknActionLinkButton" OnClick=" OfflineInterviewLog_topResetLinkButton_Click"></asp:LinkButton>
                                        </td>
                                        <td width="4%" align="center" class="link_button">
                                            |
                                        </td>
                                        <td width="18%" align="left">
                                            <asp:LinkButton ID="OfflineInterviewLog_bottomCancelLinkButton" runat="server" Text="Cancel"
                                                SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="OfflineInterviewLog_isMaximizedHiddenField" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="year_hd" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="msg_align">
                    <asp:UpdatePanel ID="OfflineInterviewLog_message_updatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="OfflineInterviewLog_bottomSuccessLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="OfflineInterviewLog_bottomErrorLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            </table>
            <asp:HiddenField ID="OfflineInterviewLog_LogIDHiddenFied" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
