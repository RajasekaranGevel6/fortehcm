﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    AutoEventWireup="true" CodeBehind="ClientManagement.aspx.cs" Inherits="Forte.HCM.UI.ClientManagement.ClientManagement" %>

<%@ Register Src="~/CommonControls/ClientControl.ascx" TagName="Client" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ClientDetailView.ascx" TagName="ClientDetailView"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/DepartmentControl.ascx" TagName="Department" TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ContactControl.ascx" TagName="Contact" TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMessage"
    TagPrefix="uc6" %>
<%@ Register Src="~/CommonControls/ClientDepartmentDetailView.ascx" TagName="ClientDepartmentDetailView"
    TagPrefix="uc7" %>
<%@ Register Src="~/CommonControls/ClientContactDetailView.ascx" TagName="ClientContactDetailsView"
    TagPrefix="uc8" %>
    <%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc9" %>
<%@ Register Src="~/CommonControls/ConfirmClientManagementMsgControl.ascx" TagName="ConfirmClientManagementMsgControl"
    TagPrefix="uc10" %>
<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="ClientManagementContent" ContentPlaceHolderID="PositionProfileMaster_body" EnableViewState="false" 
    runat="server">
    <script type="text/javascript" language="javascript">
        //Expand and Collapse All Question's 
        //Option Details panel show and Hide.
        function ExpORCollapseRows(targetControl, hdnField, searchResultDiv, detailDiv) {
            var ctrl = document.getElementById(hdnField);
            targetValue = ctrl.value;
            var linkID = document.getElementById(targetControl);
            if (targetValue == "0") {
                linkID.innerHTML = "Collapse All";
                ExpandAllRows(targetControl, hdnField, searchResultDiv, detailDiv);
                ctrl.value = 1;
            }
            else {
                linkID.innerHTML = "Expand All";
                CollapseAllRows(targetControl, hdnField, searchResultDiv, detailDiv);
                ctrl.value = 0;
            }

            return false;
        }
        //Expand all the question's Option panel
        function ExpandAllRows(targetControl, hdnField, searchResultDiv, detailDiv) {
            var gridCtrl = document.getElementById(searchResultDiv);
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf(detailDiv) != "-1") {
                        rowItems[indexRow].style.display = "block";
                    }
                }
            }
            return false;
        }



        //Hide all the question's Option panel
        //        function CollapseAllRows(targetControl, hdnField, searchResultDiv, detailDiv) {
        //            try {
        //                var gridCtrl = document.getElementById(searchResultDiv);
        //                if (gridCtrl != null) {
        //                    var rowItems = gridCtrl.getElementsByTagName("div");
        //                    for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
        //                        if (rowItems[indexRow].id.indexOf(detailDiv) != "-1") {
        //                            rowItems[indexRow].style.display = "none";
        //                        }
        //                    }
        //                }
        //                var linkcontrol = document.getElementById(targetControl);
        //                if (linkcontrol.innerHTML = "Collapse All") {
        //                    linkcontrol.innerHTML = "Expand All";
        //                    var ctrl = document.getElementById(hdnField);
        //                    ctrl.value = 0;

        //                }
        //                return false;
        //            } catch (Exp) {
        //            alert(Exp);
        //            }
        //    }


        function CollapseAllRows(targetControl, hdnField, searchResultDiv, detailDiv) {
            var gridCtrl = document.getElementById(searchResultDiv);
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf(detailDiv) != "-1") {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }
            var linkcontrol = document.getElementById
            (targetControl);
            if (linkcontrol.innerHTML = "Collapse All") {
                linkcontrol.innerHTML = "Expand All";
                var ctrl = document.getElementById(hdnField);
                ctrl.value = 0;

            }
            return false;
        }

        function GetMouseClickedPos(ctrlID) {
            if (document.getElementById(ctrlID) != null) {
                document.getElementById(ctrlID).focus();
            }
            return true;
        }


        function ShowCreatedByDiv(checkBox, div, textBoxID) {
            var checkBox = document.getElementById(checkBox);
            var divID = document.getElementById(div);
            if (checkBox.checked == true) {
                document.getElementById(textBoxID).value = "";
                divID.style["display"] = "none";
                //var textBox = document.getElementById(textBoxID);

                //                   if (textBox.style.visibility == "hidden" || textBox.style.display == "none" || textBox.disabled == true) {

                //                       //do something because it can't accept focus()
                //                   } else {
                //                       textBox.focus();
                //                   }

                //alert(test);
                //document.getElementById(textBoxID).focus();
            }
            else {
                divID.style["display"] = "block";
            }
        }

        function ClientHideDetails(linkButtonID, ctrlId, ctrlDiv, hdnField, searchResultDiv, detailDiv) {
            try {
                if (document.getElementById(ctrlId).style.display == "none") {
                    CollapseAllRows(linkButtonID, hdnField, searchResultDiv, detailDiv);
                    document.getElementById(ctrlId).style.display = "block";
                }
                else {
                    CollapseAllRows(linkButtonID, hdnField, searchResultDiv, detailDiv);
                    document.getElementById(ctrlId).style.display = "none";
                }
                if (document.getElementById(ctrlDiv) != null) {
                    document.getElementById(ctrlDiv).focus();
                }
                return false;
            }
            catch (ex)
            { alert(ex); }
        }

        function goBack() {
            window.history.back()
        }


        function LoadUserForClientManagement(ctrlId, ctrlHidID, labelCtrl, roleCodes) {
        
            var height = 530;
            var width = 750;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";

            var queryStringValue = "../popup/SearchUser.aspx?ctrlid=" + ctrlId + "&ctrlhidId=" + ctrlHidID + "&ctrlFirstNameid=" + labelCtrl + "&rolecodes=" + roleCodes;
            //window.open(queryStringValue, window.self, sModalFeature);
            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }

        function pageScroll() {
            window.scrollTo(0, 0); // horizontal and vertical scroll increments
          
             
        }
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ClientManagement_headerLiteral" runat="server" Text="Client Management"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="ClientManagement_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="ClientManagement_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ClientManagement_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ClientManagement_topMessagesUpdatePanel" runat="server">
                    <ContentTemplate>

                        <asp:HiddenField ID="ClientManagement_viewClientIDHiddenField" runat="server" />
                        <asp:HiddenField ID="ClientManagement_viewDepartmentIDHiddenField" runat="server" />
                        <asp:HiddenField ID="ClientManagement_viewContactIDHiddenField" runat="server" />
                        <asp:HiddenField ID="ClientManagement_contactDepartmentIDHiddenField" runat="server" />
                        <asp:HiddenField ID="ClientManagement_clientDepartmentIDHiddenField" runat="server" />
                        <asp:Label ID="ClientManagement_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ClientManagement_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <asp:UpdatePanel ID="ClientManagement_mainTabContainerUpdatePanel" runat="server"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="ClientManagement_viewCliendNameHiddenField" runat="server" Value="Client Name" />
                        <asp:HiddenField ID="ClientManagement_viewCliendIDHiddenField" runat="server" Value="0" />
                        <ajaxToolKit:TabContainer ID="ClientManagement_mainTabContainer" runat="server" Width="100%"
                            ActiveTabIndex="0">
                            <ajaxToolKit:TabPanel ID="ClientManagement_clinetTabPanel" runat="server" TabIndex="0">
                                <HeaderTemplate>
                                    <asp:Label ID="ClientManagement_clinetTabPanelHeaderLabel" runat="server" Text="Client"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel ID="ClientManagement_clientCreatePanel" runat="server" DefaultButton="ClientManagement_Client_searchButton">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="ClientManagement_clientTopLinkUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton ID="ClientManagement_Client_backLinktButton" runat="server" Text="<< Back"
                                                                            SkinID="sknActionLinkButton" OnClick="ClientManagement_backLinkButton_Click"
                                                                            Visible="false"></asp:LinkButton>
                                                                    </td>
                                                                    <td align="center" class="link_button" runat="server" id="ClientManagement_Client_backLinkSeperator"
                                                                        visible="false">
                                                                        |
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="ClientManagement_Client_createClientButton" runat="server" SkinID="sknAddClientImageButton"
                                                                            OnCommand="ClientManagement_createClientButton_Command" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <asp:UpdatePanel ID="ClientManagement_Client_searchDivUpdatePanel" runat="server"
                                                                    UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td width="100%">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_Client_clientNameHeadLabel" runat="server" Text="Client Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_Client_clientNameTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_Client_contactNameHeadLabel" runat="server" Text="Contact Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_Client_contactNameTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div>
                                                                                                    <asp:CheckBox ID="ClientManagement_Client_userEnableCheckBox" Text="My Records" SkinID="sknMyRecordCheckBox"
                                                                                                        runat="server" Checked="true" />
                                                                                                    <asp:ImageButton ID="ClientManagement_createdByOnlyHelpImageButton" SkinID="sknHelpImageButton"
                                                                                                        runat="server" ToolTip="Check this to show only the clients created by me, or consist of departments/contacts created by me"
                                                                                                        ImageAlign="Top" OnClientClick="javascript:return false;" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="td_height_5">
                                                                                            <td colspan="5">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_Client_locationHeadLabel" runat="server" Text="Location"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_Client_locationTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td colspan="2">
                                                                                                <div id="ClientManagement_createdByDiv" runat="server">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 37%">
                                                                                                                <asp:Label ID="ClientManagement_Client_createdByHeadLabel" runat="server" Text="Created By"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <div style="float: left; padding-right: 2px;">
                                                                                                                    <asp:TextBox ID="ClientManagement_Client_createdByTextBox" runat="server" MaxLength="50"
                                                                                                                        ReadOnly="true"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="ClientManagement_Client_createdByImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select the client creator" />
                                                                                                                    <asp:HiddenField ID="ClientManagement_Client_createdByummyAuthorIdHidenField" runat="server" />
                                                                                                                    <asp:HiddenField ID="ClientManagement_Client_createdByHiddenField" runat="server"
                                                                                                                        Value="0" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table align="right">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="ClientManagement_Client_searchButton" runat="server" SkinID="sknButtonId"
                                                                                        Text="Search" OnCommand="ClientManagement_searchButton_Command" CommandName="searchclient" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr align="right">
                                                <td align="right" class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="header_text_bold">
                                                                <asp:Literal ID="ClientManagement_searchHeaderLiteral" runat="server" Text="Search Results "></asp:Literal>
                                                                &nbsp;<asp:Label ID="ClientManagement_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                    Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="ClientManagement_expandAllUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="ClientManagement_client_ExpandLinkButton" runat="server" Text="Expand All"
                                                                                        SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                    <asp:HiddenField ID="ClientManagement_stateExpandHiddenField" runat="server" Value="0" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <asp:UpdatePanel runat="server" ID="ClientManagement_clientSerachResultsUpdatePanel"
                                                        UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div runat="server" id="ClientManagement_clientSerachResultsDiv" visible="true" style="width: 950px;
                                                                height: 500px; overflow: auto">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td align="left">                                               

                                                                            <asp:GridView ID="ClientManagement_clientSerachResultsGridView" runat="server" AllowSorting="True"
                                                                                Width="100%" SkinID="sknWrapHeaderGrid" OnRowDataBound="ClientManagement_clientSerachResultsGridView_RowDataBound"
                                                                                OnSorting="ClientManagement_clientSerachResultsGridView_Sorting" OnRowCreated="ClientManagement_clientSerachResultsGridView_RowCreated"
                                                                                OnRowCommand="ClientManagement_serachResultsGridView_RowCommand">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Client ID" SortExpression="CLIENT_ID" HeaderStyle-Width="100px">
                                                                                 <%--   <HeaderTemplate>
                                                                                        <asp:LinkButton runat="server" Text='<%# GetHeaderText("ClientID") %>'></asp:LinkButton>
                                                                                    </HeaderTemplate>--%>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_clientIdlabel" runat="server" Text='<%# Eval("ClientID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Client Name" SortExpression="CLIENT_NAME" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="ClientManagement_clientNameLinkButton" runat="server" Text='<%# Eval("ClientName") %>' ToolTip="Click here to show/hide detailed view"></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="City Name" SortExpression="CITY_NAME" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" Text='<%# ((Forte.HCM.DataObjects.ClientInformation)Container.DataItem).ContactInformation.City%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="State Name" SortExpression="STATE_NAME" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" Text='<%# ((Forte.HCM.DataObjects.ClientInformation)Container.DataItem).ContactInformation.State%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="No Of Departments" SortExpression="NO_OF_DEPARTMENTS DESC"
                                                                                        HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_noOfDepartmentsLabel" runat="server" Text='<%# Eval("NoOfDepartments") %>'
                                                                                                Visible="false"></asp:Label>
                                                                                            <asp:LinkButton ID="ClientManagement_noOfDepartmentsLinkButton" runat="server" Text='<%# Eval("NoOfDepartments") %>'
                                                                                                Visible="false" ToolTip="Click here to view the list of departments"></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="No Of Contacts" SortExpression="NO_OF_CONTACTS DESC" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_noOfContactLabel" runat="server" Text='<%# Eval("NoOfContacts") %>'
                                                                                                Visible="false"></asp:Label>
                                                                                            <asp:LinkButton ID="ClientManagement_noOfContactLinkButton" runat="server" Text='<%# Eval("NoOfContacts") %>'
                                                                                                Visible="false" ToolTip="Click here to view the list of contacts"></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="No Of Job descriptions" SortExpression="NO_OF_POSITION_PROFILES DESC"
                                                                                        HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_noOfPositionProfileLabel" runat="server" Text='<%# Eval("NoOfPositionProfile") %>'
                                                                                                Visible="false"></asp:Label>
                                                                                            <asp:LinkButton ID="ClientManagement_noOfPositionProfileLinkButton" runat="server"
                                                                                                Text='<%# Eval("NoOfPositionProfile") %>' Visible="false" ToolTip="Click here to view the list of Job descriptions"></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Created By" SortExpression="CREATED_BY">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_modifiedDateLabel" runat="server" Text='<%#  Eval("CreatedByName")%>'
                                                                                                Width="125px"></asp:Label>
                                                                                            <tr>
                                                                                                <td class="grid_padding_right" colspan="8">
                                                                                                    <div id="ClientManagement_detailsDiv" runat="server" style="display: none;" class="client_details_table_outline_bg">
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <uc2:ClientDetailView ID="ClientManagement_viewControl" runat="server"
                                                                                                                        ClientDetailViewDataSource='<%# ((Forte.HCM.DataObjects.ClientInformation)Container.DataItem)%>'
                                                                                                                         OnOnRepeaterCommandClick="ClientManagement_departmentListOnRepeaterCommandClick" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                    <%-- popup DIV --%>
                                                                                                    <a href="#ClientManagement_focusmeLink" id="ClientManagement_focusDownLink" runat="server">
                                                                                                    </a><a href="#" id="ClientManagement_focusmeLink" runat="server"></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <uc3:PageNavigator ID="ClientManagement_client_bottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ClientManagement_Client_createClientButton" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel ID="ClientManagement_client_department_TabPanel" runat="server"
                                TabIndex="1">
                                <HeaderTemplate>
                                    <asp:Label ID="ClientManagement_client_department_TabPanelHeaderLabel" runat="server"
                                        Text="Client Departments"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel ID="ClientManagement_department_departmentCreatePanel" runat="server"
                                        DefaultButton="ClientManagement_department_searchButton">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="ClientManagement_departmentTopLinkUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton ID="ClientManagement_departmentBackLinkButton" runat="server" Text="<< Back"
                                                                            SkinID="sknActionLinkButton" OnClick="ClientManagement_backLinkButton_Click"
                                                                            Visible="false"></asp:LinkButton>
                                                                    </td>
                                                                    <td align="center" class="link_button" runat="server" id="ClientManagement_departmentBackLinkSeperator"
                                                                        visible="false">
                                                                        |
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="ClientManagement_department_createClientButton" runat="server"
                                                                            SkinID="sknAddDepartmentImageButton" OnCommand="ClientManagement_createClientButton_Command" />
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <asp:UpdatePanel ID="ClientManagement_department_searchDivUpdatePanel" runat="server"
                                                                    UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td width="100%">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_department_clientNameHeadLabel" runat="server" Text="Client Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_department_clientNameTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_department_contactNameHeadLabel" runat="server" Text="Contact Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_department_contactNameTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div>
                                                                                                    <asp:CheckBox ID="ClientManagement_department_userEnableCheckBox" Text="My Records"
                                                                                                        SkinID="sknMyRecordCheckBox" runat="server" Checked="true" />
                                                                                                    <asp:ImageButton ID="ClientManagement_department_createdByOnlyHelpImageButton" SkinID="sknHelpImageButton"
                                                                                                        runat="server" ToolTip="Check this to show only the departments created by me, or consist of contacts created by me"
                                                                                                        ImageAlign="Top" OnClientClick="javascript:return false;" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="td_height_5">
                                                                                            <td colspan="5">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_department_departmentNameHeadLabel" runat="server" Text="Deparment Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_departmentNameTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td colspan="2">
                                                                                                <div id="ClientManagement_department_createdByDiv" runat="server">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 37%">
                                                                                                                <asp:Label ID="ClientManagement_department_createdByHeadLabel" runat="server" Text="Created By"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <div style="float: left; padding-right: 2px;">
                                                                                                                    <asp:TextBox ID="ClientManagement_department_createdByTextBox" runat="server" MaxLength="50"
                                                                                                                        ReadOnly="true"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="ClientManagement_department_createdByImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select the client creator" />
                                                                                                                    <asp:HiddenField ID="ClientManagement_department_createdByummyAuthorIdHidenField"
                                                                                                                        runat="server" />
                                                                                                                    <asp:HiddenField ID="ClientManagement_department_createdByHiddenField" runat="server"
                                                                                                                        Value="0" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table align="right">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="ClientManagement_department_searchButton" runat="server" SkinID="sknButtonId"
                                                                                        Text="Search" OnCommand="ClientManagement_searchButton_Command" CommandName="searchdepartment" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr align="right">
                                                <td align="right" class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="header_text_bold">
                                                                <asp:Literal ID="ClientManagement_department_searchHeaderLiteral" runat="server"
                                                                    Text="Search Results "></asp:Literal>
                                                                &nbsp;<asp:Label ID="ClientManagement_department_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                    Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="ClientManagement_department_expandAllUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="ClientManagement_department_ExpandLinkButton" runat="server"
                                                                                        Text="Expand All" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                    <asp:HiddenField ID="ClientManagement_department_stateExpandHiddenField" runat="server"
                                                                                        Value="0" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <asp:UpdatePanel runat="server" ID="ClientManagement_department_clientSerachResultsUpdatePanel"
                                                        UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div runat="server" id="ClientManagement_department_clientSerachResultsDiv" visible="true"
                                                                style="width: 950px; height: 500px; overflow: auto">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:GridView ID="ClientManagement_department_clientSerachResultsGridView" runat="server"
                                                                                SkinID="sknWrapHeaderGrid" OnRowCreated="ClientManagement_departmentSerachResultsGridView_RowCreated"
                                                                                OnSorting="ClientManagement_departmentSerachResultsGridView_Sorting" AllowSorting="True"
                                                                                OnRowDataBound="ClientManagement_departmentSerachResultsGridView_RowDataBound"
                                                                                Width="100%" OnRowCommand="ClientManagement_serachResultsGridView_RowCommand">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Client ID" SortExpression="CLIENT_ID" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_department_clientIdlabel" runat="server" Text='<%# Eval("ClientID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Client Name" DataField="ClientName" HeaderStyle-Width="12%"
                                                                                        ItemStyle-Width="10%" SortExpression="CLIENT_NAME" />
                                                                                        
                                                                                    <asp:TemplateField HeaderText="Department Name" SortExpression="DEPARTMENT_NAME"
                                                                                        HeaderStyle-Width="120px">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="ClientManagement_department_deparmentNameLinkButton" runat="server" ToolTip="Click here to show/hide detailed view"
                                                                                                Text='<%# Eval("DepartmentName") %>'></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="City Name" SortExpression="CITY_NAME" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" Text='<%# ((Forte.HCM.DataObjects.Department)Container.DataItem).ContactInformation.City%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="State Name" SortExpression="STATE_NAME" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" Text='<%# ((Forte.HCM.DataObjects.Department)Container.DataItem).ContactInformation.State%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="No Of Contacts" SortExpression="NO_OF_CONTACTS DESC" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                        <asp:HiddenField ID="ClientManagement_departmentIDHiddenField" runat="server" Value='<%# Eval("DepartmentID") %>'/>
                                                                                            <asp:Label ID="ClientManagement_department_noOfContactLabel" runat="server" Text='<%# Eval("NoOfContacts") %>'
                                                                                                Visible="false"></asp:Label>
                                                                                            <asp:LinkButton ID="ClientManagement_department_noOfContactLinkButton" runat="server"
                                                                                                Text='<%# Eval("NoOfContacts") %>' Visible="false" ToolTip="Click here to view the list of contacts"></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="No Of Job descriptions" SortExpression="NO_OF_POSITION_PROFILES DESC"
                                                                                        HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_department_noOfPositionProfileLabel" runat="server"
                                                                                                Text='<%# Eval("NoOfPositionProfile") %>' Visible="false"></asp:Label>
                                                                                            <asp:LinkButton ID="ClientManagement_department_noOfPositionProfileLinkButton" runat="server"
                                                                                                Text='<%# Eval("NoOfPositionProfile") %>' Visible="false" ToolTip="Click here to view the list of Job descriptions"></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Created By" SortExpression="CREATED_BY">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_department_modifiedDateLabel" runat="server" Text='<%#  Eval("CreatedByName")%>'
                                                                                                Width="125px"></asp:Label>
                                                                                            <tr>
                                                                                                <td class="grid_padding_right" colspan="8">
                                                                                                    <div id="ClientManagement_department_detailsDiv" runat="server" style="display: none;"
                                                                                                        class="client_details_department_table_outline_bg">
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <%--<uc2:ClientDetailView ID="ClientManagement_department_viewControl" runat="server"
                                                                                                                OnOkCommand="ClientManagement_department_viewControl_OkCommand" ClientDetailViewDataSource='<%# ((Forte.HCM.DataObjects.ClientInformation)Container.DataItem)%>' />--%>
                                                                                                                    <uc7:ClientDepartmentDetailView ID="ClientManagement_department_viewControl" runat="server"
                                                                                                                        ClientDepartmentDetailViewDataSource='<%# ((Forte.HCM.DataObjects.Department)Container.DataItem)%>'
                                                                                                                       />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                    <%-- popup DIV --%>
                                                                                                    <a href="#ClientManagement_department_focusmeLink" id="ClientManagement_department_focusDownLink"
                                                                                                        runat="server"></a><a href="#" id="ClientManagement_department_focusmeLink" runat="server">
                                                                                                        </a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <uc3:PageNavigator ID="ClientManagement_department_bottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ClientManagement_department_createClientButton" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolKit:TabPanel>
                            <ajaxToolKit:TabPanel ID="ClientManagement_client_contact_TabPanel" runat="server">
                                <HeaderTemplate>
                                    <asp:Label ID="ClientManagement_client_contact_TabPanelHeaderLabel" runat="server"
                                        Text="Client Contacts"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel ID="ClientManagement_contact_departmentCreatePanel" runat="server" DefaultButton="ClientManagement_contact_searchButton">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel ID="ClientManagement_contacttTopLinkUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton ID="ClientManagement_contactBackLinkButton" runat="server" Text="<< Back"
                                                                            SkinID="sknActionLinkButton" OnClick="ClientManagement_backLinkButton_Click"
                                                                            Visible="false"></asp:LinkButton>
                                                                    </td>
                                                                    <td align="center" class="link_button" id="ClientManagement_contactBackLinkSeperator"
                                                                        runat="server" visible="false">
                                                                        |
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="ClientManagement_contact_createClientButton" runat="server"
                                                                            SkinID="sknAddContactImageButton" OnCommand="ClientManagement_createClientButton_Command" />
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="panel_bg">
                                                                <asp:UpdatePanel ID="ClientManagement_contact_searchDivUpdatePanel" runat="server"
                                                                    UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td width="100%">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_contact_clientNameHeadLabel" runat="server" Text="Client Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_contact_clientNameTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_contact_contactNameHeadLabel" runat="server" Text="Contact Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_contact_contactNameTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <div>
                                                                                                    <asp:CheckBox ID="ClientManagement_contact_userEnableCheckBox" Text="My Records"
                                                                                                        runat="server" SkinID="sknMyRecordCheckBox" Checked="true"/>
                                                                                                    <asp:ImageButton ID="ClientManagement_contact_createdByOnlyHelpImageButton" SkinID="sknHelpImageButton"
                                                                                                        runat="server" ToolTip="Check this to show only the contacts created by me" ImageAlign="Top"
                                                                                                        OnClientClick="javascript:return false;" />
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="td_height_5">
                                                                                            <td colspan="5">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="ClientManagement_contact_departmentHeadLabel" runat="server" Text="Deparment Name"
                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="ClientManagement_contact_departmentTextBox" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td colspan="2">
                                                                                                <div id="ClientManagement_contact_createdByDiv" runat="server">
                                                                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td style="width: 37%">
                                                                                                                <asp:Label ID="ClientManagement_contact_createdByHeadLabel" runat="server" Text="Created By"
                                                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                            </td>
                                                                                                            <td align="left">
                                                                                                                <div style="float: left; padding-right: 2px;">
                                                                                                                    <asp:TextBox ID="ClientManagement_contact_createdByTextBox" runat="server" MaxLength="50"
                                                                                                                        ReadOnly="true"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <div style="float: left;">
                                                                                                                    <asp:ImageButton ID="ClientManagement_contact_createdByImageButton" SkinID="sknbtnSearchicon"
                                                                                                                        runat="server" ImageAlign="Middle" ToolTip="Click here to select the client creator" />
                                                                                                                    <asp:HiddenField ID="ClientManagement_contact_createdByummyAuthorIdHidenField" runat="server" />
                                                                                                                    <asp:HiddenField ID="ClientManagement_contact_createdByHiddenField" runat="server"
                                                                                                                        Value="0" />
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table align="right">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="ClientManagement_contact_searchButton" runat="server" SkinID="sknButtonId"
                                                                                        Text="Search" OnCommand="ClientManagement_searchButton_Command" CommandName="searchcontact" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr align="right">
                                                <td align="right" class="header_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="header_text_bold">
                                                                <asp:Literal ID="ClientManagement_contact_searchHeaderLiteral" runat="server" Text="Search Results "></asp:Literal>
                                                                &nbsp;<asp:Label ID="ClientManagement_contact_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                    Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <table border="0" cellpadding="0" cellspacing="4" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="ClientManagement_contact_expandAllUpdatePanel" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="ClientManagement_contact_ExpandLinkButton" runat="server" Text="Expand All"
                                                                                        SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                    <asp:HiddenField ID="ClientManagement_contact_stateExpandHiddenField" runat="server"
                                                                                        Value="0" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="grid_body_bg">
                                                    <asp:UpdatePanel runat="server" ID="ClientManagement_contact_clientSerachResultsUpdatePanel"
                                                        UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div runat="server" id="ClientManagement_contact_clientSerachResultsDiv" visible="true"
                                                                style="width: 950px; height: 500px; overflow: auto">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:GridView ID="ClientManagement_contact_SerachResultsGridView" runat="server"
                                                                                AllowSorting="True" Width="100%" SkinID="sknWrapHeaderGrid" OnRowDataBound="ClientManagement_contactSerachResultsGridView_RowDataBound"
                                                                                OnSorting="ClientManagement_contactSerachResultsGridView_Sorting" OnRowCreated="ClientManagement_contactSerachResultsGridView_RowCreated"
                                                                                OnRowCommand="ClientManagement_serachResultsGridView_RowCommand">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Client ID" SortExpression="CLIENT_ID" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_contact_clientIdlabel" runat="server" Text='<%# Eval("ClientID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="Client Name" DataField="ClientName" HeaderStyle-Width="10%"
                                                                                        ItemStyle-Width="10%" SortExpression="CLIENT_NAME" />
                                                                                    <asp:TemplateField HeaderText="Contact Name" SortExpression="CONTACT_NAME" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="ClientManagement_contact_contactNameLinkButton" runat="server" ToolTip="Click here to show/hide detailed view"
                                                                                                Text='<%# Eval("ContactName") %>'></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="City Name" SortExpression="CITY_NAME" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" Text='<%# ((Forte.HCM.DataObjects.ClientContactInformation)Container.DataItem).ContactInformation.City%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="State Name" SortExpression="STATE_NAME" HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" Text='<%# ((Forte.HCM.DataObjects.ClientContactInformation)Container.DataItem).ContactInformation.State%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="No Of Departments" SortExpression="NO_OF_DEPARTMENTS DESC"
                                                                                        HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                        <asp:HiddenField ID="ClientManagement_contactIDHiddenField" runat="server" Value='<%# Eval("ContactID") %>'/>
                                                                                            <asp:Label ID="ClientManagement_contact_noOfDepartmentsLabel" runat="server" Text='<%# Eval("NoOfDepartments") %>'
                                                                                                Visible="false"></asp:Label>
                                                                                            <asp:LinkButton ID="ClientManagement_contact_noOfDepartmentsLinkButton" runat="server"
                                                                                                Text='<%# Eval("NoOfDepartments") %>' Visible="false" ToolTip="Click here to view the list of departments"></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="No Of Job descriptions" SortExpression="NO_OF_POSITION_PROFILES DESC"
                                                                                        HeaderStyle-Width="100px">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_contact_noOfPositionProfileLabel" runat="server"
                                                                                                Text='<%# Eval("NoOfPositionProfile") %>' Visible="false"></asp:Label>
                                                                                            <asp:LinkButton ID="ClientManagement_contact_noOfPositionProfileLinkButton" runat="server"
                                                                                                Text='<%# Eval("NoOfPositionProfile") %>' Visible="false" ToolTip="Click here to view the list of Job descriptions"></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Created By" SortExpression="CREATED_BY">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="ClientManagement_contact_modifiedDateLabel" runat="server" Text='<%#  Eval("CreatedByName")%>'
                                                                                                Width="125px"></asp:Label>
                                                                                            <tr>
                                                                                                <td class="grid_padding_right" colspan="8">
                                                                                                    <div id="ClientManagement_contact_detailsDiv" runat="server" style="display: none;"
                                                                                                        class="client_details_table_outline_bg">
                                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <%--   <uc2:ClientDetailView ID="ClientManagement_contact_viewControl" runat="server" OnOkCommand="ClientManagement_contact_viewControl_OkCommand"
                                                                                                                        ClientDetailViewDataSource='<%# ((Forte.HCM.DataObjects.ClientInformation)Container.DataItem)%>' />--%>
                                                                                                                    <uc8:ClientContactDetailsView ID="ClientManagement_contact_viewControl" runat="server"
                                                                                                                        ClientContactDetailViewDataSource='<%# ((Forte.HCM.DataObjects.ClientContactInformation)Container.DataItem)%>'
                                                                                                                        OnOnRepeaterCommandClick="ClientManagement_departmentListOnRepeaterCommandClick"/>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                    <%-- popup DIV --%>
                                                                                                    <a href="#ClientManagement_contact_focusmeLink" id="ClientManagement_contact_focusDownLink"
                                                                                                        runat="server"></a><a href="#" id="ClientManagement_contact_focusmeLink" runat="server">
                                                                                                        </a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_5">

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <uc3:PageNavigator ID="ClientManagement_contact_bottomPagingNavigator" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ClientManagement_contact_createClientButton" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolKit:TabPanel>
                        </ajaxToolKit:TabContainer>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ClientManagement_bottomMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ClientManagement_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ClientManagement_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr align="right">
            <td align="right" class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="ClientManagement_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="ClientManagement_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ClientManagement_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="ClientManagement_clientModalPopUpUpdatePanel" runat="server"
        UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="ClientManagement_clientIDHiddenField" runat="server" Value="0" />
            <span style="display: none">
                <asp:Button ID="ClientManagement_clientDummybutton" runat="server" />
            </span>
            <asp:Panel ID="ClientManagement_clientPaenl" runat="server" CssClass="client_details">
                <uc1:Client ID="ClientManagement_clientControl" runat="server" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="ClientManagement_clientModalpPopupExtender" runat="server"
                TargetControlID="ClientManagement_clientDummybutton" PopupControlID="ClientManagement_clientPaenl"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="ClientManagement_departmentModalPopUpUpdatePanel" runat="server"
        UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="ClientManagement_departIDHiddenField" runat="server" />
            <span style="display: none">
                <asp:Button ID="ClientManagement_departmentDummybutton" runat="server" />
            </span>
            <asp:Panel ID="ClientManagement_departmentPaenl" runat="server" CssClass="department_details">
                <uc4:Department ID="ClientManagement_departmentControl" runat="server" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="ClientManagement_departmentModalpPopupExtender"
                runat="server" TargetControlID="ClientManagement_departmentDummybutton" PopupControlID="ClientManagement_departmentPaenl"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="ClientManagement_contactModalPopUpUpdatePanel" runat="server"
        UpdateMode="Conditional">
        <ContentTemplate>
            <span style="display: none">
                <asp:Button ID="ClientManagement_contactDummybutton" runat="server" />
            </span>
            <asp:Panel ID="ClientManagement_contactPaenl" runat="server" CssClass="contact_details">
                <uc5:Contact ID="ClientManagement_contactControl" runat="server" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="ClientManagement_contactModalpPopupExtender"
                runat="server" TargetControlID="ClientManagement_contactDummybutton" PopupControlID="ClientManagement_contactPaenl"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="ClientManagement_confirmMessageModalPopUpUpdatePanel" runat="server"
        UpdateMode="Conditional">
        <ContentTemplate>
            <span style="display: none">
                <asp:Button ID="ClientManagement_confirmMessageDummybutton" runat="server" />
            </span>
            <asp:Panel ID="ClientManagement_confirmMessagePaenl" runat="server" CssClass="client_confirm_message_box">
                <uc6:ConfirmMessage ID="ClientManagement_ConfirmMessageControl" runat="server" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="ClientManagement_confirmMessageModalpPopupExtender"
                runat="server" TargetControlID="ClientManagement_confirmMessageDummybutton" PopupControlID="ClientManagement_confirmMessagePaenl"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>

      <asp:UpdatePanel ID="ClientManagement_confirmDeleteMessageModalPopUpUpdatePanel"
        runat="server">
        <ContentTemplate>
            <span style="display: none">
                <asp:Button ID="ClientManagement_confirmDeleteMessageDummybutton" runat="server" />
            </span>
            <asp:Panel ID="ClientManagement_deleteBusinessPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_confirm_delete">
                <uc10:ConfirmClientManagementMsgControl ID="ClientManagement_deleteBusinessConfirmMsgControl"
                    runat="server" OnOkClick="ClientManagement_deleteConfirmMsgControl_okClick" OnCancelClick="ClientManagement_deleteConfirmMsgControl_cancelClick" />
                <asp:HiddenField ID="ClientManagement_deleteHiddenField" runat="server" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="ClientManagement_deleteFormModalPopupExtender"
                runat="server" PopupControlID="ClientManagement_deleteBusinessPopupPanel" TargetControlID="ClientManagement_confirmDeleteMessageDummybutton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="ClientManagement_confirmDeleteContactMessageModalPopUpUpdatePanel"
        runat="server">
        <ContentTemplate>
            <span style="display: none">
                <asp:Button ID="ClientManagement_confirmDeleteContactMessageDummybutton" runat="server" />
            </span>
            <asp:Panel ID="ClientManagement_deleteContactPopupPanel" runat="server" Style="display: none"
                CssClass="popupcontrol_confirm_remove">
                <uc9:ConfirmMsgControl ID="ClientManagement_deleteContactConfirmMsgControl"
                    runat="server" OnOkClick="ClientManagement_deleteContactConfirmMsgControl_okClick" OnCancelClick="ClientManagement_deleteContactConfirmMsgControl_cancelClick" />
                <asp:HiddenField ID="ClientManagement_deleteContactHiddenField" runat="server" />
            </asp:Panel>
            <ajaxToolKit:ModalPopupExtender ID="ClientManagement_deleteContactModalPopupExtender"
                runat="server" PopupControlID="ClientManagement_deleteContactPopupPanel" TargetControlID="ClientManagement_confirmDeleteContactMessageDummybutton"
                BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
