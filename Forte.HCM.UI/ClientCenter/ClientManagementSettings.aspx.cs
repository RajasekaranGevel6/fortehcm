﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientManagementSettings.aspx.cs
// File that represents the user interface layout and functionalities 
// for the client management settings page. This page helps in configuring
// the client management admin settings. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header                                                              

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

#endregion Directives                                                          

namespace Forte.HCM.UI.ClientCenter
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for
    /// the client management settings page. This page helps in configuring 
    /// the client management admin settings.This class inherits 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ClientManagementSettings : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set default button and focus
                Page.Form.DefaultButton = ClientManagementSettings_topSaveButton.UniqueID;

                // Set page title
                Master.SetPageCaption("Settings");

                if (!IsPostBack)
                {
                    // Load default values
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagementSettings_topErrorMessageLabel,
                    ClientManagementSettings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ClientManagementSettings_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear all labels
                ClearMessages();

                // Get the settings from the user interface.
                ClientManagementSettingsDetail settings = new ClientManagementSettingsDetail();
                settings.TenantID = base.tenantID;
                settings.UserID = base.userID;

                if (ClientManagementSettings_generalSettingsDeleteRadioButtonList.SelectedValue.Trim() == "0")
                    settings.AllowDeleteClientForAll = true;
                else
                    settings.AllowDeleteClientForAll = false;

                // Update settings to the database.
                new ClientBLManager().UpdateClientManagementSettings(settings);
                Session[Constants.SessionConstants.IS_CLIENT_MANAGEMENT_DELETE] = 
                    new ClientBLManager().GetClientManagementSettings(base.tenantID).AllowDeleteClientForAll;
                // Show a success messsage.
                base.ShowMessage(ClientManagementSettings_topSuccessMessageLabel,
                   ClientManagementSettings_bottomSuccessMessageLabel,
                   Resources.HCMResource.ClientManagementSettings_SavedSuccessfully);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagementSettings_topErrorMessageLabel,
                    ClientManagementSettings_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the reset button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirects to the same page by loading the previously
        /// saved values.
        /// </remarks>
        protected void ClientManagementSettings_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagementSettings_topErrorMessageLabel,
                    ClientManagementSettings_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        
        /// <summary>
        /// Method that clear all messages.
        /// </summary>
        private void ClearMessages()
        {
            ClientManagementSettings_topErrorMessageLabel.Text = string.Empty;
            ClientManagementSettings_bottomErrorMessageLabel.Text = string.Empty;
            ClientManagementSettings_topSuccessMessageLabel.Text = string.Empty;
            ClientManagementSettings_bottomSuccessMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Get the settings from the database.
            ClientManagementSettingsDetail settings = new ClientBLManager().
                GetClientManagementSettings(base.tenantID);

            // Check if settings is null. If null, the default settings is taken 
            // that is 'Allow only for customer administrators' is set to true.
            if (settings == null)
                return;

            // Configure the settings.
            if (settings.AllowDeleteClientForAll)
                ClientManagementSettings_generalSettingsDeleteRadioButtonList.SelectedValue = "0";
            else
                ClientManagementSettings_generalSettingsDeleteRadioButtonList.SelectedValue = "1";
        }

        #endregion Protected Overridden Methods                                
    }
}
