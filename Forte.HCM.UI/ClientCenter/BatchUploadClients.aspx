﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    CodeBehind="BatchUploadClients.aspx.cs" Inherits="Forte.HCM.UI.ClientCenter.BatchUploadClients" %>

<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/DisclaimerControl.ascx" TagName="DisclaimerControl"
    TagPrefix="uc3" %>
<asp:Content ContentPlaceHolderID="PositionProfileEntry_bodyContent" runat="server" ID="BatchUploadClients_body">
    <script type="text/javascript" language="javascript">


    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="BatchUploadClients_headerLiteral" runat="server" Text="Batch Clients Information"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="BatchUploadClients_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Reset" OnClick="BatchUploadClients_resetLinkButton_Click" CausesValidation="false" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="BatchUploadClients_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" OnClick="ParentPageRedirect" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <table width="100%">
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="BatchUploadClients_topSuccessMessageLabel" runat="server" Tag="12"
                                SkinID="sknSuccessMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="BatchUploadClients_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                ID="BatchUploadClients_stateHiddenField" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <div id="BatchUploadClients_uploadExcelDiv" runat="server" style="display: block">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="header_bg">
                                <asp:Literal ID="BatchUploadClients_fileSelectionLiteral" runat="server" Text="File Selection"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="grid_body_bg">
                                <table border="0" cellpadding="2" cellspacing="5" width="80%">
                                    <tr>
                                        <td style="width: 40%">
                                            <asp:Label ID="BatchUploadClients_fileNameLabel" runat="server" Text="Excel File (that contains the client information)"
                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            <span class="mandatory">*</span>
                                        </td>
                                        <td valign="middle">
                                            
                                                <asp:FileUpload ID="BatchUploadClients_fileUpload" runat="server" Width="90%" />
                                           
                                                <asp:ImageButton ID="BatchUploadClients_helpImageButton" SkinID="sknHelpImageButton"
                                                    runat="server" OnClientClick="javascript:return false;" ToolTip="Click here to select the excel file that contains the client information" />
                                            
                                        </td>
                                        <td >
                                            <asp:Button ID="BatchUploadClients_uploadButton" runat="server" Text="Upload" SkinID="sknButtonId"
                                                OnClick="BatchUploadClients_uploadButton_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:RegularExpressionValidator ID="BatchUploadClients_regularExpressionValidataor"
                                                runat="server" ControlToValidate="BatchUploadClients_fileUpload" ErrorMessage="Only excel files are allowed"
                                                ValidationExpression="^.+\.((xls|xlsx))$">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                              
                                    </table>
                            </td>
                        </tr>
                    </table>
                </div>
                 <div id="BatchUploadClients_resultDiv" runat="server" visible="false">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="msg_align_center">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="BatchUploadClients_recordLabel" runat="server" Tag="12" Text="" SkinID="sknRecordCount"></asp:Label>
                                            <span class="signin_text">|</span>
                                            <asp:LinkButton ID="BatchUploadClients_nextLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                Text="Next" OnClick="BatchUploadClients_nextLinkButton_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ajaxToolKit:TabContainer ID="BatchUploadClients_mainTabContainer" runat="server"
                                    ActiveTabIndex="0">
                                    <ajaxToolKit:TabPanel ID="BatchUploadClients_questionsTabPanel" HeaderText="Clients"
                                        runat="server">
                                        <HeaderTemplate>
                                            Clients</HeaderTemplate>
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="5%" width="100%">
                                                <tr>
                                                    <td class="msg_align">
                                                        <asp:Label ID="BatchUploadClients_clientsTabPanel_errorMessageLabel" runat="server"
                                                            SkinID="sknErrorMessage"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="BatchUploadClients_removeQuestionLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                        Text="Remove&nbsp;All" OnClick="BatchUploadClients_removeQuestionLinkButton_Click" />
                                                                </td>
                                                                <td style="width: 5%">
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="BatchUploadClients_saveButton" runat="server" SkinID="sknButtonId"
                                                                        Text="Save" OnClick="BatchUploadClients_saveButton_Click" /><asp:HiddenField ID="BatchUploadClients_isSavedHiddenField"
                                                                            runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:GridView ID="BatchUploadClients_questionGridView" runat="server" AutoGenerateColumns="False"
                                                OnRowDataBound="BatchUploadClients_ClientsDataList_RowDataBound" ShowHeader="False"
                                                SkinID="sknNewGridView" 
                                                OnRowCommand="BatchUploadClients_questionGridView_RowCommand" 
                                                EnableModelValidation="True">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                                <tr>
                                                                    <td valign="middle" style="width: 8%">
                                                                        <div style="float: left; width: 35%;">
                                                                            <asp:ImageButton ID="BatchUploadClients_questionsGridView_deleteImageButton" runat="server"
                                                                                SkinID="sknDeleteImageButton" ToolTip="Delete Question" CommandName="DeleteQuestion"
                                                                                CommandArgument='<%# Eval("QuestionID")%>' /></div>
                                                                        <div style="float: left;">
                                                                            <asp:Image ID="BatchUploadClients_questionImage" runat="server" SkinID="sknQuestionImage"
                                                                                ToolTip="Question" />
                                                                            <asp:Label ID="BatchUploadClients_questionNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                runat="server" ReadOnly="true" Text='<%# Eval("QuestionID") %>'>
                                                                            </asp:Label></div>
                                                                    </td>
                                                                    <td colspan="6" style="width: 92%">
                                                                        <asp:TextBox ID="BatchUploadClients_questionTextBox" runat="server" SkinID="sknMultiLineTextBox"
                                                                            TextMode="MultiLine" Width="91%" Height="40" Text='<%# Eval("Question") %>' MaxLength="500"
                                                                            onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td colspan="6">
                                                                        <asp:DataList ID="BatchUploadClients_subject_categoryDataList" runat="server" RepeatColumns="2"
                                                                            RepeatDirection="Vertical" Width="100%" DataSource='<%# Eval("AnswerChoices") %>'
                                                                            OnItemDataBound="BatchUploadClients_subject_categoryDataList_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 10px">
                                                                                            <asp:RadioButton ID="BatchUploadClients_selectRadioButton" runat="server" Checked='<%# Eval("IsCorrect") %>' />
                                                                                        </td>
                                                                                        <td style="width: 420px">
                                                                                            <asp:TextBox ID="BatchUploadClients_answerTextBox" SkinID="sknMultiLineTextBox" runat="server"
                                                                                                TextMode="MultiLine" Text='<%# Eval("Choice") %>' Width="80%" Height="30px" MaxLength="200"
                                                                                                onkeyup="CommentsCount(200,this)" onchange="CommentsCount(200,this)"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:DataList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="BatchUploadClients_categoryQuestionLabel" runat="server" Text="Category"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="BatchUploadClients_categoryQuestionReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                                                        <asp:HiddenField ID="BatchUploadClients_categoryIDHiddenField" runat="server" Value='<%# Eval("CategoryID") %>' />
                                                                    </td>
                                                                    <td style="width: 6%">
                                                                        <asp:Label ID="BatchUploadClients_subjectQuestionLabel" runat="server" Text="Subject"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="BatchUploadClients_subjectQuestionReadOnlyLabel" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                                        <asp:HiddenField ID="BatchUploadClients_subjectIDHiddenField" runat="server" Value='<%# Eval("SubjectID") %>' />
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="BatchUploadClients_testAreaQuestionHeadLabel" runat="server" Text="Test Area"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BatchUploadClients_testAreaQuestionLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("TestAreaName") %>' />
                                                                        <asp:HiddenField ID="BatchUploadClients_testAreaIDHiddenField" runat="server" Value='<%# Eval("TestAreaID") %>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BatchUploadClients_complexityQuestionHeadLabel" runat="server" Text="Complexity"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BatchUploadClients_complexityQuestionLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("ComplexityName") %>' />
                                                                        <asp:HiddenField ID="BatchUploadClients_complexityIDHiddenField" runat="server" Value='<%# Eval("Complexity") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BatchUploadClients_tagQuestionHeadLabel" runat="server" Text="Tag"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BatchUploadClients_tagQuestionReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" Text='<%# Eval("Tag") %>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BatchUploadClients_authorQuestionHeadLabel" runat="server" Text="Author"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BatchUploadClients_authorQuestionReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                            runat="server" ReadOnly="true" Text='<%# Eval("AuthorName") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="BatchUploadClients_imageLinkTR">
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="BatchUploadClients_imageNameHeadLabel" runat="server" Text="Image Name"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td colspan="5">
                                                                       
                                                                        <asp:LinkButton ID="BatchUploadClients_qstImageLinkButton" SkinID="sknActionLinkButton"
                                                                            runat="server" Text='<%# Eval("ImageName") %>' Visible='<%# Eval("HasImage") %>'> 
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </ajaxToolKit:TabPanel>
                                    <ajaxToolKit:TabPanel ID="BatchUploadClients_inCompleteClientsTabPanel" HeaderText="Incomplete Clients"
                                        runat="server">
                                        <HeaderTemplate>
                                            Incomplete Clients</HeaderTemplate>
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="5%" width="100%">
                                                <tr>
                                                    <td class="msg_align">
                                                        <asp:Label ID="BatchUploadClients_inCompleteClientsTabPanel_errorMessageLabel"
                                                            runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="BatchUploadClients_removeInvalidQuestionLinkButton" runat="server"
                                                                        SkinID="sknActionLinkButton" Text="Remove&nbsp;All" OnClick="BatchUploadClients_removeInvalidQuestionLinkButton_Click" />
                                                                </td>
                                                                <td style="width: 5%">
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="BatchUploadClients_invalidSaveButton" runat="server" SkinID="sknButtonId"
                                                                        Text="Save" OnClick="BatchUploadClients_invalidSaveButton_Click" /><asp:HiddenField
                                                                            ID="BatchUploadClients_isInvalidSavedHiddenField" runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:GridView ID="BatchUploadClients_inCompleteClientsGridView" runat="server"
                                                AutoGenerateColumns="false" 
                                                SkinID="sknNewGridView" ShowFooter="false" ShowHeader="false" >
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="2">
                                                                <tr>
                                                                    <td valign="middle" style="width: 8%">
                                                                        <div style="float: left; width: 35%;">
                                                                            <asp:ImageButton ID="BatchUploadClients_questionsGridView_deleteImageButton" runat="server"
                                                                                SkinID="sknDeleteImageButton" ToolTip="Delete Question" CommandName="DeleteQuestion"
                                                                                CommandArgument='<%# Eval("QuestionID")%>' /></div>
                                                                        <div style="float: left;">
                                                                            <asp:Image ID="BatchUploadClients_incompleteQuestionImage" runat="server" SkinID="sknQuestionImage"
                                                                                ToolTip="Question" />
                                                                            <asp:Label ID="BatchUploadClients_incompleteQuestionNoLabel" SkinID="sknLabelFieldHeaderTextRecordNumber"
                                                                                runat="server" ReadOnly="true" Text='<%# Eval("QuestionID") %>'>
                                                                            </asp:Label></div>
                                                                    </td>
                                                                    <td colspan="3" style="width: 92%">
                                                                        <asp:TextBox ID="BatchUploadClients_incompleteQuestionTextBox" runat="server" TextMode="MultiLine"
                                                                            Width="91%" Height="40" SkinID="sknMultiLineTextBox" Text='<%# Eval("Question") %>'
                                                                            MaxLength="500" onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:DataList ID="BatchUploadClients_subjectcategoryQuestionDataList" runat="server"
                                                                            RepeatColumns="2" RepeatDirection="Vertical" Width="100%" DataSource='<%# Eval("AnswerChoices") %>'
                                                                            OnItemDataBound="BatchUploadClients_subjectcategoryQuestionDataList_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td style="width: 10px">
                                                                                            <asp:RadioButton ID="BatchUploadClients_selectQuestionRadioButton" runat="server"
                                                                                                Checked='<%# Eval("IsCorrect") %>' onclick="CheckOtherIsCheckedByGVID(this)" />
                                                                                        </td>
                                                                                        <td style="width: 420px">
                                                                                            <asp:TextBox ID="BatchUploadClients_answerQuestionTextBox" runat="server" TextMode="MultiLine"
                                                                                                Text='<%# Eval("Choice") %>' SkinID="sknMultiLineTextBox" Width="80%" Height="30px"
                                                                                                MaxLength="200" onkeyup="CommentsCount(200,this)" onchange="CommentsCount(200,this)"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:DataList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td style="width: 55%" valign="middle">
                                                                        <div style="float: left; width: 92%;" class="grouping_border_bg">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="BatchUploadClients_categoryLabel" runat="server" Text="Category" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 43%">
                                                                                        <asp:TextBox ID="BatchUploadClients_categoryTextBox" runat="server" Text='<%# Eval("CategoryName") %>'
                                                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="BatchUploadClients_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 22%">
                                                                                        <asp:TextBox ID="BatchUploadClients_subjectTextBox" runat="server" Text='<%# Eval("SubjectName") %>'
                                                                                            MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="BatchUploadClients_subjectIdHiddenField" runat="server" Value='<%# Eval("SubjectID") %>' />
                                                                                        <asp:HiddenField ID="BatchUploadClients_categoryNameHiddenField" runat="server" Value='<%# Eval("CategoryName") %>' />
                                                                                        <asp:HiddenField ID="BatchUploadClients_subjectNameHiddenField" runat="server" Value='<%# Eval("SubjectName") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div style="float: left; padding-left: 5px; padding-top: 5px;">
                                                                            <asp:ImageButton ID="BatchUploadClients_categoryImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ToolTip="Click here to select category and subject" /></div>
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="BatchUploadClients_testAreaLabel" runat="server" Text="Test Area"
                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="BatchUploadClients_testAreaDropDownList" runat="server" Width="133px">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="BatchUploadClients_testAreaHiddenField" runat="server" Value='<%# Eval("TestAreaName") %>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td style="width: 55%">
                                                                        <div style="float: left; padding-right: 5px; width: 92%;" class="grouping_bg">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label ID="BatchUploadClients_complexityLabel" runat="server" Text="Complexity"
                                                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 43%">
                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                            <asp:DropDownList ID="BatchUploadClients_complexityDropDownList" runat="server" Width="133px">
                                                                                            </asp:DropDownList>
                                                                                            <asp:HiddenField ID="BatchUploadClients_complexityDropDownListHiddenField" runat="server"
                                                                                                Value='<%#Eval("ComplexityName") %>' />
                                                                                        </div>
                                                                                        <div style="float: left;">
                                                                                            <asp:ImageButton ID="BatchUploadClients_complexityImageButton" SkinID="sknHelpImageButton"
                                                                                                runat="server" ImageAlign="Middle" CommandArgument='<%# Eval("Complexity") %>'
                                                                                                OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, ComplexityHelp %>" /></div>
                                                                                    </td>
                                                                                    <td style="width: 16%">
                                                                                        <asp:Label ID="BatchUploadClients_tagHeadLabel" runat="server" Text="Tag" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 22%">
                                                                                        <asp:TextBox ID="BatchUploadClients_tagTextBox" runat="server" Text='<%# Eval("Tag") %>'
                                                                                            MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="BatchUploadClients_authorHeadLabel" runat="server" Text="Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <div style="float: left; padding-right: 5px;">
                                                                            <asp:TextBox ID="BatchUploadClients_authorTextBox" runat="server" Text='<%#Eval("AuthorName") %>'>
                                                                            </asp:TextBox>
                                                                            <asp:HiddenField ID="BatchUploadClients_authorHiddenField" runat="server" Value='<%#Eval("Author") %>' />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:ImageButton ID="BatchUploadClients_authorImageButton" SkinID="sknbtnSearchicon"
                                                                                runat="server" ImageAlign="Middle" ToolTip="Click here to select the question author" /></div>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="BatchUploadClients_addImageLinkTR">
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:LinkButton ID="BatchUploadClients_addQuestionImageLinkButton" runat="server"
                                                                            Text="Add Image" SkinID="sknActionLinkButton" ToolTip="Add Question Image" Visible='<%# Eval("HasImage") %>'>
                                                                        </asp:LinkButton>
                                                                        <asp:HiddenField ID="BatchUploadClients_qstImageHiddenField" runat="server" Value='<%# Eval("ImageName") %>'>
                                                                        </asp:HiddenField>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <asp:Label ID="BatchUploadClients_remarksHeadLabel" runat="server" Text="Remarks "
                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="BatchUploadClients_remarksReadOnlyLabel" SkinID="sknLabelFieldText"
                                                                                        runat="server" ReadOnly="true" Text='<%#Eval("InvalidQuestionRemarks") %>'>                                                                                        
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <uc1:PageNavigator ID="BatchUploadClients_pageNavigator" runat="server" />
                                        </ContentTemplate>
                                    </ajaxToolKit:TabPanel>
                                </ajaxToolKit:TabContainer>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="BatchUploadClients_removepopupPanel" runat="server" Style="display: none"
                                    CssClass="popupcontrol_confirm_remove">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="popup_td_padding_10">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 75%" class="popup_header_text" valign="middle" align="left">
                                                            <asp:Label ID="BatchUploadClients_confirmMsgControl_titleLabel" runat="server" Text="Remove Confirm"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%" valign="top" align="right">
                                                            <asp:ImageButton ID="BatchUploadClients_confirmMsgControl_questionCloseImageButton"
                                                                runat="server" SkinID="sknCloseImageButton" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="popup_td_padding_10">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                                                    <tr>
                                                        <td align="left" class="popup_td_padding_10">
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="BatchUploadClients_confirmMsgControl_messageValidLabel" runat="server"
                                                                            SkinID="sknLabelFieldText" Text="Are you sure want to remove all the questions?"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                                <tr valign="bottom">
                                                                    <td align="right" style="text-align: center">
                                                                        <asp:Button ID="BatchUploadClients_confirmMsgControl_yesValidButton" runat="server"
                                                                            SkinID="sknButtonId" Text="Yes" Width="64px"  />
                                                                        <asp:Button ID="BatchUploadClients_confirmMsgControl_noValidButton" runat="server"
                                                                            SkinID="sknButtonId" Text="No" Width="45px" OnClientClick="javascript:return false;" />
                                                                        <asp:HiddenField ID="BatchUploadClients_questionIDHiddenField" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_20">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <div style="display: none">
                                    <asp:Button ID="BatchUploadClients_hiddenButton" runat="server" />
                                </div>
                                <ajaxToolKit:ModalPopupExtender ID="BatchUploadClients_deletepopupExtender" runat="server"
                                    PopupControlID="BatchUploadClients_removepopupPanel" TargetControlID="BatchUploadClients_hiddenButton"
                                    BackgroundCssClass="modalBackground" CancelControlID="BatchUploadClients_confirmMsgControl_noValidButton"
                                    DynamicServicePath="" Enabled="True">
                                </ajaxToolKit:ModalPopupExtender>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
          <tr>
            <td class="msg_align">
                <table width="100%">
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="BatchUploadClients_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <asp:Label ID="BatchUploadClients_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text">
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="BatchUploadClients_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="BatchUploadClients_resetLinkButton_Click"
                                            CausesValidation="false" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="BatchUploadClients_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
