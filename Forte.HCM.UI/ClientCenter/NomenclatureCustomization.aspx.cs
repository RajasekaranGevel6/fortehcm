﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// NomenclatureCustomization.aspx.cs
// File that represents the user interface layout and functionalities 
// for Nomenclaturecustomization page. This page helps in setting up the label contents
// This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion Header                                                                    

#region Directives                                                             

using System;
using System.Xml;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.NomenclatureCustomization
{
    public partial class NomenclatureCustomization : PageBase
    {
        #region Event Handler                                                  
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Nomenclature Customization");

                NomenclatureCustomization_topSuccessMessageLabel.Text = string.Empty;
                NomenclatureCustomization_bottomSuccessMessageLabel.Text = string.Empty;
                NomenclatureCustomization_topErrorMessageLabel.Text = string.Empty;
                NomenclatureCustomization_bottomErrorMessageLabel.Text = string.Empty;

                if (!IsPostBack)
                {

                    LoadClientAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT);

                    LoadDepartmentAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT);

                    LoadContactAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS);

                    LoadRolesAttribute(tenantID);

                    ClientScript.RegisterStartupScript(this.GetType(), "VisiblityValidation", "PostBack_ValidateVisiblity();", true);                   

                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NomenclatureCustomization_topErrorMessageLabel,
                    NomenclatureCustomization_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the NomenClatureCustomizationSaveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void NomenclatureCustomization_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateTextFields())
                {
                    base.ShowMessage(NomenclatureCustomization_topErrorMessageLabel,
                        NomenclatureCustomization_bottomErrorMessageLabel, "Mandatory fields cannot be empty");

                    return;
                }
                NomenclatureCustomize claturecustomizationDetails = Datasource();

                NomenClatureBLManager nomenClatureBLManager = new NomenClatureBLManager();

                nomenClatureBLManager.InsertNomenClature(base.tenantID, claturecustomizationDetails.DataSource, base.userID);
                Session[Constants.SessionConstants.ALL_NOMENCLATURE] = new
                       ControlUtility().LoadAllAttributeDetails(tenantID);
                InsertRoleAttributes();

                
                base.ShowMessage(NomenclatureCustomization_topSuccessMessageLabel,
                     NomenclatureCustomization_bottomSuccessMessageLabel, "Nomenclature customization settings saved successfully");

                ClientScript.RegisterStartupScript(this.GetType(), "VisiblityValidation", "PostBack_ValidateVisiblity();", true);                   


            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NomenclatureCustomization_topErrorMessageLabel,
                    NomenclatureCustomization_bottomErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the NomenClatureCustomizationRestoreDefaultLinkbutton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void NomenclatureCustomization_restoreDefaultsLinkbutton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadClientAttributeDetails(1, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT);

                LoadDepartmentAttributeDetails(1, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT);

                LoadContactAttributeDetails(1, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS);

                LoadRolesAttribute(1);

                ClientScript.RegisterStartupScript(this.GetType(), "VisiblityValidation", "PostBack_ValidateVisiblity();", true);                   

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NomenclatureCustomization_topErrorMessageLabel,
                    NomenclatureCustomization_bottomErrorMessageLabel, exception.Message);
            }
        }

        protected void NomenclatureCustomization_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(NomenclatureCustomization_topErrorMessageLabel,
                    NomenclatureCustomization_bottomErrorMessageLabel, exception.Message);
            }
        }

        #endregion

        #region Private Methods                                                

        /// <summary>
        /// Loads the roles attribute.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        private void LoadRolesAttribute(int tenantID)
        {
            List<NomenclatureCustomize> roleAttributeList = new 
                NomenClatureBLManager().GetRoleAttributes(tenantID);

            if (roleAttributeList == null || roleAttributeList.Count == 0)
                return;

            for (int i = 0; i < roleAttributeList.Count; i++)
            {
                if (roleAttributeList[i].RoleID == Constants.RoleCodeConstants.DELIVERY_MANAGER.ToString())
                    NomenclatureCustomization_rolesSection_deliverymanagerTextBox.Text = roleAttributeList[i].RoleAliasName;
                if (roleAttributeList[i].RoleID == Constants.RoleCodeConstants.RECRUITER.ToString())
                    NomenclatureCustomization_rolesSection_recruiterTextBox.Text = roleAttributeList[i].RoleAliasName;
                if (roleAttributeList[i].RoleID == Constants.RoleCodeConstants.CONTENT_AUTHOR.ToString())
                    NomenclatureCustomization_rolesSection_contentAuthorTextBox.Text = roleAttributeList[i].RoleAliasName;
            }
        }

        /// <summary>
        /// Loads the client attribute details.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="element">The element.</param>
        public void LoadClientAttributeDetails(int tenantID, string element)
        {
            ControlUtility controlUtility = new ControlUtility();

            NomenclatureCustomize nc = controlUtility.LoadAttributeDetails
                (tenantID, element);

            if (nc == null)
                return;

            if (element == Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT)
            {
                AssignClientTextboxValues(nc);

                AssignClientCheckboxValues(nc);
            }
        }

        /// <summary>
        /// Loads the department attribute details.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="element">The element.</param>
        public void LoadDepartmentAttributeDetails(int tenantID, string element)
        {
            ControlUtility cu = new ControlUtility();

            NomenclatureCustomize nc = cu.LoadAttributeDetails(tenantID, element);

            if (nc == null)
                return;

            if (element == Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT)
            {
                AssignClientDepartmentTextboxValues(nc);

                AssignClientDepartmentCheckboxValues(nc);
            }


        }
        /// <summary>
        /// Loads the contact attribute details.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="element">The element.</param>
        public void LoadContactAttributeDetails(int tenantID, string element)
        {
            ControlUtility cu = new ControlUtility();

            NomenclatureCustomize nc = cu.LoadAttributeDetails(tenantID, element);

            if (nc == null)
                return;

            if (element == Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS)
            {
                AssignClientContactTextboxValues(nc);

                AssignClientContactCheckboxValues(nc);
            }

        }
        /// <summary>
        /// Assigns the client contact checkbox values.
        /// </summary>
        /// <param name="attributeDetails">The attribute details.</param>
        private void AssignClientContactCheckboxValues(NomenclatureCustomize attributeDetails)
        {
            //NomenclatureCustomization_clientContactsSection_firstNameMandatoryCheckbox.Checked = attributeDetails.FirstNameIsMandatory;
            //NomenclatureCustomization_clientContactsSection_lastNameMandatoryCheckbox.Checked = attributeDetails.LastNameIsMandatory;
            NomenclatureCustomization_clientContactsSection_middleNameMandatoryCheckBox.Checked = attributeDetails.MiddlenameIsMandatory;
            NomenclatureCustomization_clientContactsSection_streetAddressMandatoryCheckBox.Checked = attributeDetails.StreetAddressIsMandatory;
            NomenclatureCustomization_clientContactsSection_cityMandatoryCheckBox.Checked = attributeDetails.CityIsMandatory;
            NomenclatureCustomization_clientContactsSection_stateMandatoryCheckBox.Checked = attributeDetails.StateIsMandatory;
            NomenclatureCustomization_clientContactsSection_zipCodeMandatoryCheckBox.Checked = attributeDetails.ZipCodeIsMandatory;
            NomenclatureCustomization_clientContactsSection_countryMandatoryCheckBox.Checked = attributeDetails.CountryIsMandatory;
            NomenclatureCustomization_clientContactsSection_faxNumberMandatoryCheckBox.Checked = attributeDetails.FaxNoIsMandatory;
            NomenclatureCustomization_clientContactsSection_phoneNumberMandatoryCheckbox.Checked = attributeDetails.PhoneNumberFieldIsMandatory;
            //NomenclatureCustomization_clientContactsSection_emailIDMandatoryCheckbox.Checked = attributeDetails.EmailIDFieldIsMandatory;
            NomenclatureCustomization_clientContactsSection_departmentsMandatoryCheckbox.Checked = attributeDetails.DepartmentsIsMandatory;
            NomenclatureCustomization_clientContactsSection_additionalInfoMandatoryCheckbox.Checked = attributeDetails.AdditionalInfoIsMandatory;
            //NomenclatureCustomization_clientContactsSection_firstNameVisibleCheckbox.Checked = attributeDetails.FirstNameIsVisible;
            //NomenclatureCustomization_clientContactsSection_lastNameVisibleCheckbox.Checked = attributeDetails.LastNameIsVisible;
            NomenclatureCustomization_clientContactsSection_middleNameVisibleCheckBox.Checked = attributeDetails.MiddlenameIsVisible;
            NomenclatureCustomization_clientContactsSection_streetAddressVisibleCheckBox.Checked = attributeDetails.StreetAddressIsVisible;
            NomenclatureCustomization_clientContactsSection_cityVisibleCheckBox.Checked = attributeDetails.CityIsVisible;
            NomenclatureCustomization_clientContactsSection_stateVisibleCheckBox.Checked = attributeDetails.StateIsVisible;
            NomenclatureCustomization_clientContactsSection_zipCodeVisibleCheckBox.Checked = attributeDetails.ZipCodeIsVisible;
            NomenclatureCustomization_clientContactsSection_countryVisibleCheckBox.Checked = attributeDetails.CountryIsVisible;
            NomenclatureCustomization_clientContactsSection_faxNumberVisibleCheckBox.Checked = attributeDetails.FaxNoIsVisible;
            NomenclatureCustomization_clientContactsSection_phoneNumberVisibleCheckbox.Checked = attributeDetails.PhoneNumberIsVisible;
            //NomenclatureCustomization_clientContactsSection_emailIDVisibleCheckbox.Checked = attributeDetails.EmailIDIsVisible;
            NomenclatureCustomization_clientContactsSection_departmentsVisibleCheckbox.Checked = attributeDetails.DepartmentsIsVisible;
            NomenclatureCustomization_clientContactsSection_additionalInfoVisibleCheckbox.Checked = attributeDetails.AdditionalInfoIsVisible;
        }

        /// <summary>
        /// Assigns the textbox values.
        /// </summary>
        /// <param name="attributeDetails">The attribute details.</param>
        private void AssignClientTextboxValues(NomenclatureCustomize attributeDetails)
        {
            NomenclatureCustomization_clientTextbox.Text = attributeDetails.Client;
            NomenclatureCustomization_clientSection_clientNameTextbox.Text = attributeDetails.ClientName;
            NomenclatureCustomization_clientSection_streetAddressTextbox.Text = attributeDetails.StreetAddress;
            NomenclatureCustomization_clientSection_cityTextBox.Text = attributeDetails.City;
            NomenclatureCustomization_clientSection_stateTextBox.Text = attributeDetails.state;
            NomenclatureCustomization_clientSection_zipCodeTextBox.Text = attributeDetails.Zipcode;
            NomenclatureCustomization_clientSection_countryTextBox.Text = attributeDetails.Country;
            NomenclatureCustomization_clientSection_feinTextBox.Text = attributeDetails.FEINNO;
            NomenclatureCustomization_clientSection_faxNumberTextBox.Text = attributeDetails.FaxNo;
            NomenclatureCustomization_clientSection_phoneNumberTextbox.Text = attributeDetails.PhoneNumber;
            NomenclatureCustomization_clientSection_emailIDTextbox.Text = attributeDetails.EmailID;
            NomenclatureCustomization_clientSection_websiteURLTextbox.Text = attributeDetails.WebsiteURL;
            NomenclatureCustomization_clientSection_additionalInfoTextbox.Text = attributeDetails.AdditionalInfo;
            NomenclatureCustomization_clientSection_contactNameTextBox.Text = attributeDetails.ContactName;
            NomenclatureCustomization_clientSection_myRecordsTextBox.Text = attributeDetails.MyRecords;
            NomenclatureCustomization_clientSection_locationTextBox.Text = attributeDetails.Location;
            NomenclatureCustomization_clientSection_createdByTextBox.Text = attributeDetails.CreatedBy;
            NomenclatureCustomization_clientSection_clientIDTextBox.Text = attributeDetails.ClientID;
            NomenclatureCustomization_clientSection_noOfDepartmentsTextBox.Text = attributeDetails.NoOfDepartments;
            NomenclatureCustomization_clientSection_noOfContactsTextBox.Text = attributeDetails.NoOfContacts;
            NomenclatureCustomization_clientSection_noOfPositionProfilesTextBox.Text = attributeDetails.NoOfPositionProfiles;
            NomenclatureCustomization_clientSection_listOfDepartmentsTextBox.Text = attributeDetails.ListOfDepartments;
        }

        /// <summary>
        /// Assigns the client department textbox values.
        /// </summary>
        /// <param name="attributeDetails">The attribute details.</param>
        private void AssignClientDepartmentTextboxValues(NomenclatureCustomize attributeDetails)
        {
            NomenclatureCustomization_clientDepartmentTextbox.Text = attributeDetails.Department;
            NomenclatureCustomization_clientDepartmentsSection_departmentNameTextbox.Text = attributeDetails.DepartmentName;
            NomenclatureCustomization_clientDepartmentsSection_descriptionTextbox.Text = attributeDetails.Description;
            NomenclatureCustomization_clientDepartmentsSection_streetAddressTextbox.Text = attributeDetails.StreetAddress;
            NomenclatureCustomization_clientDepartmentsSection_cityTextBox.Text = attributeDetails.City;
            NomenclatureCustomization_clientDepartmentsSection_stateTextBox.Text = attributeDetails.state;
            NomenclatureCustomization_clientDepartmentsSection_zipCodeTextBox.Text = attributeDetails.Zipcode;
            NomenclatureCustomization_clientDepartmentsSection_countryTextBox.Text = attributeDetails.Country;
            NomenclatureCustomization_clientDepartmentsSection_faxNumberTextBox.Text = attributeDetails.FaxNo;
            NomenclatureCustomization_clientDepartmentsSection_phoneNumberTextbox.Text = attributeDetails.PhoneNumber;
            NomenclatureCustomization_clientDepartmentsSection_emailIDTextbox.Text = attributeDetails.EmailID;
            NomenclatureCustomization_clientDepartmentsSection_additionalInfoTextbox.Text = attributeDetails.AdditionalInfo;
            NomenclatureCustomization_clientDepartmentsSection_contactNameTextBox.Text = attributeDetails.ContactName;
            NomenclatureCustomization_clientDepartmentsSection_myRecordsTextBox.Text = attributeDetails.MyRecords;
            NomenclatureCustomization_clientDepartmentsSection_createdByTextBox.Text = attributeDetails.CreatedBy;
            NomenclatureCustomization_clientDepartmentsSection_clientIDTextBox.Text = attributeDetails.ClientID;
            NomenclatureCustomization_clientDepartmentsSection_noOfContactsTextBox.Text = attributeDetails.NoOfContacts;
            NomenclatureCustomization_clientDepartmentsSection_noOfPositionProfilesTextBox.Text = attributeDetails.NoOfPositionProfiles;
        }

        /// <summary>
        /// Assigns the client contact textbox values.
        /// </summary>
        /// <param name="attributeDetails">The attribute details.</param>
        private void AssignClientContactTextboxValues(NomenclatureCustomize attributeDetails)
        {
            NomenclatureCustomization_clientContactsTextbox.Text = attributeDetails.Contacts;
            NomenclatureCustomization_clientContactsSection_firstNameTextbox.Text = attributeDetails.FirstName;
            NomenclatureCustomization_clientContactsSection_lastNameTextbox.Text = attributeDetails.LastName;
            NomenclatureCustomization_clientContactsSection_middleNameTextbox.Text = attributeDetails.Middlename;
            NomenclatureCustomization_clientContactsSection_streetAddressTextbox.Text = attributeDetails.StreetAddress;
            NomenclatureCustomization_clientContactsSection_cityTextBox.Text = attributeDetails.City;
            NomenclatureCustomization_clientContactsSection_stateTextBox.Text = attributeDetails.state;
            NomenclatureCustomization_clientContactsSection_zipCodeTextBox.Text = attributeDetails.Zipcode;
            NomenclatureCustomization_clientContactsSection_countryTextBox.Text = attributeDetails.Country;
            NomenclatureCustomization_clientContactsSection_faxNumberTextBox.Text = attributeDetails.FaxNo;
            NomenclatureCustomization_clientContactsSection_phoneNumberTextbox.Text = attributeDetails.PhoneNumber;
            NomenclatureCustomization_clientContactsSection_emailIDTextbox.Text = attributeDetails.EmailID;
            NomenclatureCustomization_clientContactsSection_departmentsTextbox.Text = attributeDetails.Departments;
            NomenclatureCustomization_clientContactsSection_additionalInfoTextbox.Text = attributeDetails.AdditionalInfo;
            NomenclatureCustomization_clientContactsSection_myRecordsTextBox.Text = attributeDetails.MyRecords;
            NomenclatureCustomization_clientContactsSection_createdByTextBox.Text = attributeDetails.CreatedBy;
            NomenclatureCustomization_clientContactsSection_clientIDTextBox.Text = attributeDetails.ClientID;
            NomenclatureCustomization_clientContactsSection_noOfDepartmentsTextBox.Text = attributeDetails.NoOfDepartments;
            NomenclatureCustomization_clientContactsSection_noOfPositionProfilesTextBox.Text = attributeDetails.NoOfPositionProfiles;
            NomenclatureCustomization_clientContactsSection_listOfDepartmentsTextBox.Text = attributeDetails.ListOfDepartments;
        }

        /// <summary>
        /// Assigns the client department checkbox values.
        /// </summary>
        /// <param name="attributeDetails">The attribute details.</param>
        private void AssignClientDepartmentCheckboxValues(NomenclatureCustomize attributeDetails)
        {
            //NomenclatureCustomization_clientDepartmentsSection_departmentNameMandatoryCheckbox.Checked = attributeDetails.DepartmentNameIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_descriptionMandatoryCheckbox.Checked = attributeDetails.DescriptionIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_streetAddressMandatoryCheckbox.Checked = attributeDetails.StreetAddressIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_cityMandatoryCheckbox.Checked = attributeDetails.CityIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_stateMandatoryCheckbox.Checked = attributeDetails.StateIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_zipCodeMandatoryCheckbox.Checked = attributeDetails.ZipCodeIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_countryMandatoryCheckbox.Checked = attributeDetails.CountryIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_faxNumberMandatoryCheckbox.Checked = attributeDetails.FaxNoIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_phoneNumberMandatoryCheckbox.Checked = attributeDetails.PhoneNumberFieldIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_emailIDMandatoryCheckBox.Checked = attributeDetails.EmailIDFieldIsMandatory;
            NomenclatureCustomization_clientDepartmentsSection_additionalInfoMandatoryCheckBox.Checked = attributeDetails.AdditionalFieldInfoIsMandatory;
            //NomenclatureCustomization_clientDepartmentsSection_departmentNameVisibleCheckbox.Checked = attributeDetails.DepartmentNameIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_descriptionVisibleCheckbox.Checked = attributeDetails.DescriptionIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_streetAddressVisibleCheckbox.Checked = attributeDetails.StreetAddressIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_cityVisibleCheckbox.Checked = attributeDetails.CityIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_stateVisibleCheckbox.Checked = attributeDetails.StateIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_zipCodeVisibleCheckbox.Checked = attributeDetails.ZipCodeIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_countryVisibleCheckbox.Checked = attributeDetails.CountryIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_faxNumberVisibleCheckbox.Checked = attributeDetails.FaxNoIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_phoneNumberVisibleCheckbox.Checked = attributeDetails.PhoneNumberIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_emailIDVisibleCheckBox.Checked = attributeDetails.EmailIDIsVisible;
            NomenclatureCustomization_clientDepartmentsSection_additionalInfoVisibleCheckBox.Checked = attributeDetails.AdditionalInfoIsVisible;
        }

        /// <summary>
        /// Assigns the checkbox values.
        /// </summary>
        /// <param name="attributeDetails">The attribute details.</param>
        private void AssignClientCheckboxValues(NomenclatureCustomize attributeDetails)
        {
            //NomenclatureCustomization_clientSection_clientNameMandatoryCheckbox.Checked = attributeDetails.ClientNameFieldIsMandatory;
            NomenclatureCustomization_clientSection_streetAddressMandatoryCheckbox.Checked = attributeDetails.StreetAddressIsMandatory;
            NomenclatureCustomization_clientSection_cityMandatoryCheckBox.Checked = attributeDetails.CityIsMandatory;
            NomenclatureCustomization_clientSection_stateMandatoryCheckBox.Checked = attributeDetails.StateIsMandatory;
            NomenclatureCustomization_clientSection_zipCodeMandatoryCheckBox.Checked = attributeDetails.ZipCodeIsMandatory;
            NomenclatureCustomization_clientSection_countryMandatoryCheckBox.Checked = attributeDetails.CountryIsMandatory;
            NomenclatureCustomization_clientSection_feinMandatoryCheckBox.Checked = attributeDetails.FEINNOIsMandatory;
            NomenclatureCustomization_clientSection_faxNumberMandatoryCheckBox.Checked = attributeDetails.FaxNoIsMandatory;
            NomenclatureCustomization_clientSection_phoneNumberMandatoryCheckbox.Checked = attributeDetails.PhoneNumberFieldIsMandatory;
            NomenclatureCustomization_clientSection_emailIDMandatoryCheckbox.Checked = attributeDetails.EmailIDFieldIsMandatory;
            NomenclatureCustomization_clientSection_websiteURLMandatoryCheckbox.Checked = attributeDetails.WebsiteURLFieldIsMandatory;
            NomenclatureCustomization_clientSection_additionalInfoMandatoryCheckbox.Checked = attributeDetails.AdditionalFieldInfoIsMandatory;
            //NomenclatureCustomization_clientSection_clientNameVisibleCheckbox.Checked = attributeDetails.ClientNameIsVisible;
            NomenclatureCustomization_clientSection_streetAddressVisibleCheckbox.Checked = attributeDetails.StreetAddressIsVisible;
            NomenclatureCustomization_clientSection_cityVisibleCheckBox.Checked = attributeDetails.CityIsVisible;
            NomenclatureCustomization_clientSection_stateVisibleCheckbox.Checked = attributeDetails.StateIsVisible;
            NomenclatureCustomization_clientSection_zipCodeVisibleCheckBox.Checked = attributeDetails.ZipCodeIsVisible;
            NomenclatureCustomization_clientSection_countryVisibleCheckBox.Checked = attributeDetails.CountryIsVisible;
            NomenclatureCustomization_clientSection_feinVisibleCheckBox.Checked = attributeDetails.FEINNOIsVisible;
            NomenclatureCustomization_clientSection_faxNumberVisibleCheckbox.Checked = attributeDetails.FaxNoIsVisible;
            NomenclatureCustomization_clientSection_phoneNumberVisibleCheckbox.Checked = attributeDetails.PhoneNumberIsVisible;
            NomenclatureCustomization_clientSection_emailIDVisibleCheckbox.Checked = attributeDetails.EmailIDIsVisible;
            NomenclatureCustomization_clientSection_websiteURLVisibleCheckbox.Checked = attributeDetails.WebsiteURLIsVisible;
            NomenclatureCustomization_clientSection_additionalInfoVisibleCheckbox.Checked = attributeDetails.AdditionalInfoIsVisible;
        }

        /// <summary>
        /// Datasources this instance.
        /// </summary>
        /// <returns></returns>
        private NomenclatureCustomize Datasource()
        {
            NomenclatureCustomize nomenClatureDetail = new NomenclatureCustomize();
            XmlDocument xmlDocument = new XmlDocument();

            XmlNode xmlNode = xmlDocument.CreateNode(XmlNodeType.XmlDeclaration, "NameSpace", "");
            xmlDocument.AppendChild(xmlNode);

            XmlElement xmlElementRoot = xmlDocument.CreateElement("NomenclatureCustomization");

            //for client
            XmlElement xmlElementClient = xmlDocument.CreateElement("Client");
            SetClientAttribute(xmlElementClient);

            //for client dept
            XmlElement xmlElementClientDept = xmlDocument.CreateElement("ClientDepartment");
            SetClientDepartmentAttribute(xmlElementClientDept);

            //for client contacts
            XmlElement xmlElementClientContact = xmlDocument.CreateElement("ClientContacts");
            SetClientContactsAttribute(xmlElementClientContact);


            xmlElementRoot.AppendChild(xmlElementClient);
            xmlElementRoot.AppendChild(xmlElementClientDept);
            xmlElementRoot.AppendChild(xmlElementClientContact);

            xmlDocument.AppendChild(xmlElementRoot);

            nomenClatureDetail.DataSource = xmlDocument.InnerXml;

            return nomenClatureDetail;
        }
        /// <summary>
        /// Sets the client contacts attribute.
        /// </summary>
        /// <param name="xmlElementClient">The XML element client.</param>
        private void SetClientContactsAttribute(XmlElement xmlElementClient)
        {

            xmlElementClient.SetAttribute("Contact", NomenclatureCustomization_clientContactsTextbox.Text.Trim());
            xmlElementClient.SetAttribute("FirstName", NomenclatureCustomization_clientContactsSection_firstNameTextbox.Text.Trim());
            xmlElementClient.SetAttribute("LastName", NomenclatureCustomization_clientContactsSection_lastNameTextbox.Text.Trim());
            xmlElementClient.SetAttribute("MiddleName", NomenclatureCustomization_clientContactsSection_middleNameTextbox.Text.Trim());
            xmlElementClient.SetAttribute("StreetAddress", NomenclatureCustomization_clientContactsSection_streetAddressTextbox.Text.Trim());
            xmlElementClient.SetAttribute("City", NomenclatureCustomization_clientContactsSection_cityTextBox.Text.Trim());
            xmlElementClient.SetAttribute("State", NomenclatureCustomization_clientContactsSection_stateTextBox.Text.Trim());
            xmlElementClient.SetAttribute("ZipCode", NomenclatureCustomization_clientContactsSection_zipCodeTextBox.Text.Trim());
            xmlElementClient.SetAttribute("Country", NomenclatureCustomization_clientContactsSection_countryTextBox.Text.Trim());
            xmlElementClient.SetAttribute("FaxNO", NomenclatureCustomization_clientContactsSection_faxNumberTextBox.Text.Trim());
            xmlElementClient.SetAttribute("PhoneNo", NomenclatureCustomization_clientContactsSection_phoneNumberTextbox.Text.Trim());
            xmlElementClient.SetAttribute("EmailID", NomenclatureCustomization_clientContactsSection_emailIDTextbox.Text.Trim());
            xmlElementClient.SetAttribute("Departments", NomenclatureCustomization_clientContactsSection_departmentsTextbox.Text.Trim());
            xmlElementClient.SetAttribute("AdditionalInfo", NomenclatureCustomization_clientContactsSection_additionalInfoTextbox.Text.Trim());
            xmlElementClient.SetAttribute("MyRecords", NomenclatureCustomization_clientContactsSection_myRecordsTextBox.Text.Trim());
            xmlElementClient.SetAttribute("CreatedBy", NomenclatureCustomization_clientContactsSection_createdByTextBox.Text.Trim());
            xmlElementClient.SetAttribute("ClientID", NomenclatureCustomization_clientContactsSection_clientIDTextBox.Text.Trim());
            xmlElementClient.SetAttribute("NoOfDepartments", NomenclatureCustomization_clientContactsSection_noOfDepartmentsTextBox.Text.Trim());
            xmlElementClient.SetAttribute("NoOfPositionProfiles", NomenclatureCustomization_clientContactsSection_noOfPositionProfilesTextBox.Text.Trim());
            xmlElementClient.SetAttribute("ListOfDepartments", NomenclatureCustomization_clientContactsSection_listOfDepartmentsTextBox.Text.Trim());

            xmlElementClient.SetAttribute("FirstNameIsMandatory", NomenclatureCustomization_clientContactsSection_firstNameMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("LastNameIsMandatory", NomenclatureCustomization_clientContactsSection_lastNameMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("MiddleNameIsMandatory", NomenclatureCustomization_clientContactsSection_middleNameMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("StreetAddressIsMandatory", NomenclatureCustomization_clientContactsSection_streetAddressMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("CityIsMandatory", NomenclatureCustomization_clientContactsSection_cityMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("StateIsMandatory", NomenclatureCustomization_clientContactsSection_stateMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("ZipCodeIsMandatory", NomenclatureCustomization_clientContactsSection_zipCodeMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("CountryIsMandatory", NomenclatureCustomization_clientContactsSection_countryMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("FaxNOIsMandatory", NomenclatureCustomization_clientContactsSection_faxNumberMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("PhoneNoIsMandatory", NomenclatureCustomization_clientContactsSection_phoneNumberMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("EmailIDIsMandatory", NomenclatureCustomization_clientContactsSection_emailIDMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("DepartmentsIsMandatory", NomenclatureCustomization_clientContactsSection_departmentsMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("AdditionalInfoIsMandatory", NomenclatureCustomization_clientContactsSection_additionalInfoMandatoryCheckbox.Checked.ToString());

            xmlElementClient.SetAttribute("FirstNameIsVisible", NomenclatureCustomization_clientContactsSection_firstNameVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("LastNameIsVisible", NomenclatureCustomization_clientContactsSection_lastNameVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("MiddleNameIsVisible", NomenclatureCustomization_clientContactsSection_middleNameVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("StreetAddressIsVisible", NomenclatureCustomization_clientContactsSection_streetAddressVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("CityIsVisible", NomenclatureCustomization_clientContactsSection_cityVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("StateIsVisible", NomenclatureCustomization_clientContactsSection_stateVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("ZipCodeIsVisible", NomenclatureCustomization_clientContactsSection_zipCodeVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("CountryIsVisible", NomenclatureCustomization_clientContactsSection_countryVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("FaxNOIsVisible", NomenclatureCustomization_clientContactsSection_faxNumberVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("PhoneNoIsVisible", NomenclatureCustomization_clientContactsSection_phoneNumberVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("EmailIDIsVisible", NomenclatureCustomization_clientContactsSection_emailIDVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("DepartmentsIsVisible", NomenclatureCustomization_clientContactsSection_departmentsVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("AdditionalInfoIsVisible", NomenclatureCustomization_clientContactsSection_additionalInfoVisibleCheckbox.Checked.ToString());

            if (NomenclatureCustomization_clientContactsSection_streetAddressVisibleCheckBox.Checked || NomenclatureCustomization_clientContactsSection_cityVisibleCheckBox.Checked
                || NomenclatureCustomization_clientContactsSection_stateVisibleCheckBox.Checked || NomenclatureCustomization_clientContactsSection_zipCodeVisibleCheckBox.Checked
               || NomenclatureCustomization_clientContactsSection_countryVisibleCheckBox.Checked)
                xmlElementClient.SetAttribute("ContactsPrimaryAddress", "true");
            else
                xmlElementClient.SetAttribute("ContactsPrimaryAddress", "false");
        }

        /// <summary>
        /// Sets the client department attribute.
        /// </summary>
        /// <param name="xmlElementClient">The XML element client.</param>
        private void SetClientDepartmentAttribute(XmlElement xmlElementClient)
        {
            xmlElementClient.SetAttribute("Department", NomenclatureCustomization_clientDepartmentTextbox.Text.Trim());
            xmlElementClient.SetAttribute("DepartmentName", NomenclatureCustomization_clientDepartmentsSection_departmentNameTextbox.Text.Trim());
            xmlElementClient.SetAttribute("Description", NomenclatureCustomization_clientDepartmentsSection_descriptionTextbox.Text.Trim());
            xmlElementClient.SetAttribute("StreetAddress", NomenclatureCustomization_clientDepartmentsSection_streetAddressTextbox.Text.Trim());
            xmlElementClient.SetAttribute("City", NomenclatureCustomization_clientDepartmentsSection_cityTextBox.Text.Trim());
            xmlElementClient.SetAttribute("State", NomenclatureCustomization_clientDepartmentsSection_stateTextBox.Text.Trim());
            xmlElementClient.SetAttribute("ZipCode", NomenclatureCustomization_clientDepartmentsSection_zipCodeTextBox.Text.Trim());
            xmlElementClient.SetAttribute("Country", NomenclatureCustomization_clientDepartmentsSection_countryTextBox.Text.Trim());
            xmlElementClient.SetAttribute("FaxNO", NomenclatureCustomization_clientDepartmentsSection_faxNumberTextBox.Text.Trim());
            xmlElementClient.SetAttribute("PhoneNo", NomenclatureCustomization_clientDepartmentsSection_phoneNumberTextbox.Text.Trim());
            xmlElementClient.SetAttribute("EmailID", NomenclatureCustomization_clientDepartmentsSection_emailIDTextbox.Text.Trim());
            xmlElementClient.SetAttribute("AdditionalInfo", NomenclatureCustomization_clientDepartmentsSection_additionalInfoTextbox.Text.Trim());
            xmlElementClient.SetAttribute("ContactName", NomenclatureCustomization_clientDepartmentsSection_contactNameTextBox.Text.Trim());
            xmlElementClient.SetAttribute("MyRecords", NomenclatureCustomization_clientDepartmentsSection_myRecordsTextBox.Text.Trim());
            xmlElementClient.SetAttribute("CreatedBy", NomenclatureCustomization_clientDepartmentsSection_createdByTextBox.Text.Trim());
            xmlElementClient.SetAttribute("ClientID", NomenclatureCustomization_clientDepartmentsSection_clientIDTextBox.Text.Trim());
            xmlElementClient.SetAttribute("NoOfContacts", NomenclatureCustomization_clientDepartmentsSection_noOfContactsTextBox.Text.Trim());
            xmlElementClient.SetAttribute("NoOfPositionProfiles", NomenclatureCustomization_clientDepartmentsSection_noOfPositionProfilesTextBox.Text.Trim());

            xmlElementClient.SetAttribute("DepartmentNameIsmandatory", NomenclatureCustomization_clientDepartmentsSection_departmentNameMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("DescriptionIsmandatory", NomenclatureCustomization_clientDepartmentsSection_descriptionMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("StreetAddressIsmandatory", NomenclatureCustomization_clientDepartmentsSection_streetAddressMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("CityIsmandatory", NomenclatureCustomization_clientDepartmentsSection_cityMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("StateIsmandatory", NomenclatureCustomization_clientDepartmentsSection_stateMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("ZipCodeIsmandatory", NomenclatureCustomization_clientDepartmentsSection_zipCodeMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("CountryIsmandatory", NomenclatureCustomization_clientDepartmentsSection_countryMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("FaxNOIsmandatory", NomenclatureCustomization_clientDepartmentsSection_faxNumberMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("PhoneNoIsmandatory", NomenclatureCustomization_clientDepartmentsSection_phoneNumberMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("EmailIDIsmandatory", NomenclatureCustomization_clientDepartmentsSection_emailIDMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("AdditionalInfoIsMandatory", NomenclatureCustomization_clientDepartmentsSection_additionalInfoMandatoryCheckBox.Checked.ToString());

            xmlElementClient.SetAttribute("DepartmentNameIsVisible", NomenclatureCustomization_clientDepartmentsSection_departmentNameVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("DescriptionIsVisible", NomenclatureCustomization_clientDepartmentsSection_descriptionVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("StreetAddressIsVisible", NomenclatureCustomization_clientDepartmentsSection_streetAddressVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("CityIsVisible", NomenclatureCustomization_clientDepartmentsSection_cityVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("StateIsVisible", NomenclatureCustomization_clientDepartmentsSection_stateVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("ZipCodeIsVisible", NomenclatureCustomization_clientDepartmentsSection_zipCodeVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("CountryIsVisible", NomenclatureCustomization_clientDepartmentsSection_countryVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("FaxNOIsVisible", NomenclatureCustomization_clientDepartmentsSection_faxNumberVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("PhoneNoIsVisible", NomenclatureCustomization_clientDepartmentsSection_phoneNumberVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("EmailIDIsVisible", NomenclatureCustomization_clientDepartmentsSection_emailIDVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("AdditionalInfoIsVisible", NomenclatureCustomization_clientDepartmentsSection_additionalInfoVisibleCheckBox.Checked.ToString());

            if (NomenclatureCustomization_clientDepartmentsSection_streetAddressVisibleCheckbox.Checked || NomenclatureCustomization_clientDepartmentsSection_cityVisibleCheckbox.Checked
                || NomenclatureCustomization_clientDepartmentsSection_stateVisibleCheckbox.Checked || NomenclatureCustomization_clientDepartmentsSection_zipCodeVisibleCheckbox.Checked
                || NomenclatureCustomization_clientDepartmentsSection_countryVisibleCheckbox.Checked)
                xmlElementClient.SetAttribute("DepartmentPrimaryAddress", "True");
            else
                xmlElementClient.SetAttribute("DepartmentPrimaryAddress", "False");
        }

        /// <summary>
        /// Sets the client attribute.
        /// </summary>
        /// <param name="xmlElementClient">The XML element client.</param>
        private void SetClientAttribute(XmlElement xmlElementClient)
        {
            xmlElementClient.SetAttribute("Client", NomenclatureCustomization_clientTextbox.Text.Trim());
            xmlElementClient.SetAttribute("ClientName", NomenclatureCustomization_clientSection_clientNameTextbox.Text.Trim());
            xmlElementClient.SetAttribute("StreetAddress", NomenclatureCustomization_clientSection_streetAddressTextbox.Text.Trim());
            xmlElementClient.SetAttribute("City", NomenclatureCustomization_clientSection_cityTextBox.Text.Trim());
            xmlElementClient.SetAttribute("State", NomenclatureCustomization_clientSection_stateTextBox.Text.Trim());
            xmlElementClient.SetAttribute("ZipCode", NomenclatureCustomization_clientSection_zipCodeTextBox.Text.Trim());
            xmlElementClient.SetAttribute("Country", NomenclatureCustomization_clientSection_countryTextBox.Text.Trim());
            xmlElementClient.SetAttribute("FEINNO", NomenclatureCustomization_clientSection_feinTextBox.Text.Trim());
            xmlElementClient.SetAttribute("FaxNO", NomenclatureCustomization_clientSection_faxNumberTextBox.Text.Trim());
            xmlElementClient.SetAttribute("PhoneNo", NomenclatureCustomization_clientSection_phoneNumberTextbox.Text.Trim());
            xmlElementClient.SetAttribute("EmailID", NomenclatureCustomization_clientSection_emailIDTextbox.Text.Trim());
            xmlElementClient.SetAttribute("WebsiteURL", NomenclatureCustomization_clientSection_websiteURLTextbox.Text.Trim());
            xmlElementClient.SetAttribute("AdditionalInfo", NomenclatureCustomization_clientSection_additionalInfoTextbox.Text.Trim());
            xmlElementClient.SetAttribute("ContactName", NomenclatureCustomization_clientSection_contactNameTextBox.Text.Trim());
            xmlElementClient.SetAttribute("MyRecords", NomenclatureCustomization_clientSection_myRecordsTextBox.Text.Trim());
            xmlElementClient.SetAttribute("Location", NomenclatureCustomization_clientSection_locationTextBox.Text.Trim());
            xmlElementClient.SetAttribute("CreatedBy", NomenclatureCustomization_clientSection_createdByTextBox.Text.Trim());
            xmlElementClient.SetAttribute("ClientID", NomenclatureCustomization_clientSection_clientIDTextBox.Text.Trim());
            xmlElementClient.SetAttribute("NoOfDepartments", NomenclatureCustomization_clientSection_noOfDepartmentsTextBox.Text.Trim());
            xmlElementClient.SetAttribute("NoOfContacts", NomenclatureCustomization_clientSection_noOfContactsTextBox.Text.Trim());
            xmlElementClient.SetAttribute("NoOfPositionProfiles", NomenclatureCustomization_clientSection_noOfPositionProfilesTextBox.Text.Trim());
            xmlElementClient.SetAttribute("ListOfDepartments", NomenclatureCustomization_clientSection_listOfDepartmentsTextBox.Text.Trim());

            xmlElementClient.SetAttribute("ClientNameIsmandatory", NomenclatureCustomization_clientSection_clientNameMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("StreetAddressIsmandatory", NomenclatureCustomization_clientSection_streetAddressMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("CityIsmandatory", NomenclatureCustomization_clientSection_cityMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("StateIsmandatory", NomenclatureCustomization_clientSection_stateMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("ZipCodeIsmandatory", NomenclatureCustomization_clientSection_zipCodeMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("CountryIsmandatory", NomenclatureCustomization_clientSection_countryMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("FEINNOIsmandatory", NomenclatureCustomization_clientSection_feinMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("FaxNOIsmandatory", NomenclatureCustomization_clientSection_faxNumberMandatoryCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("PhoneNoIsmandatory", NomenclatureCustomization_clientSection_phoneNumberMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("EmailIDIsmandatory", NomenclatureCustomization_clientSection_emailIDMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("WebsiteURLIsmandatory", NomenclatureCustomization_clientSection_websiteURLMandatoryCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("AdditionalInfoIsmandatory", NomenclatureCustomization_clientSection_additionalInfoMandatoryCheckbox.Checked.ToString());

            xmlElementClient.SetAttribute("ClientNameIsVisible", NomenclatureCustomization_clientSection_clientNameVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("StreetAddressIsVisible", NomenclatureCustomization_clientSection_streetAddressVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("CityIsVisible", NomenclatureCustomization_clientSection_cityVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("StateIsVisible", NomenclatureCustomization_clientSection_stateVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("ZipCodeIsVisible", NomenclatureCustomization_clientSection_zipCodeVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("CountryIsVisible", NomenclatureCustomization_clientSection_countryVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("FEINNOIsVisible", NomenclatureCustomization_clientSection_feinVisibleCheckBox.Checked.ToString());
            xmlElementClient.SetAttribute("FaxNOIsVisible", NomenclatureCustomization_clientSection_faxNumberVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("PhoneNoIsVisible", NomenclatureCustomization_clientSection_phoneNumberVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("EmailIDIsVisible", NomenclatureCustomization_clientSection_emailIDVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("WebsiteURLIsVisible", NomenclatureCustomization_clientSection_websiteURLVisibleCheckbox.Checked.ToString());
            xmlElementClient.SetAttribute("AdditionalInfoIsVisible", NomenclatureCustomization_clientSection_additionalInfoVisibleCheckbox.Checked.ToString());
        }

        /// <summary>
        /// Inserts the role attributes.
        /// </summary>
        private void InsertRoleAttributes()
        {
            NomenClatureBLManager nomenClatureBLManager = new NomenClatureBLManager();

            List<NomenclatureCustomize> roleArttribute = new List<NomenclatureCustomize>();

            if (NomenclatureCustomization_rolesSection_deliverymanagerHiddenField.Value == Constants.RoleCodeConstants.DELIVERY_MANAGER)
            {
                NomenclatureCustomize roleAttributeDetails = new NomenclatureCustomize();

                roleAttributeDetails.RoleID = NomenclatureCustomization_rolesSection_deliverymanagerHiddenField.Value;
                roleAttributeDetails.RoleAliasName = NomenclatureCustomization_rolesSection_deliverymanagerTextBox.Text;
                roleAttributeDetails.RoleName = NomenclatureCustomization_rolesSection_deliverymanagerLabel.Text;
                roleAttributeDetails.TenantID = base.tenantID;
                roleAttributeDetails.UserID = base.userID;

                roleArttribute.Add(roleAttributeDetails);
            }
            if (NomenclatureCustomization_rolesSection_recruiterHiddenField.Value == Constants.RoleCodeConstants.RECRUITER)
            {
                NomenclatureCustomize roleAttributeDetails = new NomenclatureCustomize();

                roleAttributeDetails.RoleID = NomenclatureCustomization_rolesSection_recruiterHiddenField.Value;
                roleAttributeDetails.RoleAliasName = NomenclatureCustomization_rolesSection_recruiterTextBox.Text;
                roleAttributeDetails.RoleName = NomenclatureCustomization_rolesSection_recruiterLabel.Text;
                roleAttributeDetails.TenantID = base.tenantID;
                roleAttributeDetails.UserID = base.userID;

                roleArttribute.Add(roleAttributeDetails);
            }
            if (NomenclatureCustomization_rolesSection_contentAuthorHiddenField.Value == Constants.RoleCodeConstants.CONTENT_AUTHOR)
            {
                NomenclatureCustomize roleAttributeDetails = new NomenclatureCustomize();

                roleAttributeDetails.RoleID = NomenclatureCustomization_rolesSection_contentAuthorHiddenField.Value;
                roleAttributeDetails.RoleAliasName = NomenclatureCustomization_rolesSection_contentAuthorTextBox.Text;
                roleAttributeDetails.RoleName = NomenclatureCustomization_rolesSection_contentAuthorLabel.Text;
                roleAttributeDetails.TenantID = base.tenantID;
                roleAttributeDetails.UserID = base.userID;

                roleArttribute.Add(roleAttributeDetails);
            }

            nomenClatureBLManager.InsertRolesAttribute(roleArttribute);
        }
        /// <summary>
        /// Validates the text fields.
        /// </summary>
        /// <returns></returns>
        private bool ValidateTextFields()
        {
            if (Utility.IsNullOrEmpty(NomenclatureCustomization_clientTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_clientNameTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_streetAddressTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_cityTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_stateTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_zipCodeTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_countryTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_feinTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_faxNumberTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_phoneNumberTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_emailIDTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_websiteURLTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_additionalInfoTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_contactNameTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_myRecordsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_locationTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_createdByTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_clientIDTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_noOfDepartmentsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_noOfContactsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_noOfPositionProfilesTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientSection_listOfDepartmentsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_departmentNameTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_descriptionTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_streetAddressTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_cityTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_stateTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_zipCodeTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_countryTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_faxNumberTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_phoneNumberTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_emailIDTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_additionalInfoTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_contactNameTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_myRecordsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_createdByTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_clientIDTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_noOfContactsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientDepartmentsSection_noOfPositionProfilesTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_firstNameTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_lastNameTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_middleNameTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_streetAddressTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_cityTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_stateTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_zipCodeTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_countryTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_faxNumberTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_phoneNumberTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_emailIDTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_departmentsTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_additionalInfoTextbox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_myRecordsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_createdByTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_clientIDTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_noOfDepartmentsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_noOfPositionProfilesTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(NomenclatureCustomization_clientContactsSection_listOfDepartmentsTextBox.Text.Trim()))
            {
                return true;
            }

            return false;
        }
        
        #endregion

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {



        }

        #endregion Protected Overridden Methods

    }
}