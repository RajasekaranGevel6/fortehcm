﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NomenclatureCustomization.aspx.cs"
    MasterPageFile="~/MasterPages/PositionProfileMaster.Master" Inherits="Forte.HCM.UI.NomenclatureCustomization.NomenclatureCustomization" %>

<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="NomenclatureCustomization_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="NomenclatureCustomization_headerLiteral" runat="server" Text="Nomenclature Customization"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="NomenclatureCustomization_topSaveButton" runat="server" Text="Save"
                                            SkinID="sknButtonId" OnClick="NomenclatureCustomization_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="NomenclatureCustomization_topRestoreDefaultsLinkButton" runat="server"
                                            Text="Restore Defaults" ToolTip="Click here to restore default attributes" SkinID="sknActionLinkButton"
                                            OnClick="NomenclatureCustomization_restoreDefaultsLinkbutton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="NomenclatureCustomization_topResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="NomenclatureCustomization_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="NomenClatureCustomization_topCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="NomenclatureCustomization_topMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="NomenclatureCustomization_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="NomenclatureCustomization_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="panel_inner_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="NomenclatureCustomization_tabHeadersLiteral" runat="server" Text="Tab Headers"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="left">
                                        <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 6%">
                                                    <asp:Label ID="NomenclatureCustomization_clientLabel" runat="server" Text="Client"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientTextbox" runat="server" Width="175px"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentLabel" runat="server" Text="Client Departments"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentTextbox" Width="175px"
                                                        runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsLabel" runat="server" Text="Client Contacts"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsTextbox" Width="175px" runat="server"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="NomenclatureCustomization_clientsection_clientLiteral" runat="server"
                                Text="Client"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="left">
                                        <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_mandatoryLabel" runat="server"
                                                        Text="Mandatory" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_visibleLabel" runat="server"
                                                        Text="Visible" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 30%">
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_clientNameLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Client Name"></asp:Label>
                                                </td>
                                                <td style="width: 30%">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_clientNameTextbox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td style="width: 20%" align="left">
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_clientNameMandatoryCheckbox"
                                                        Enabled="false" Checked="true" />
                                                </td>
                                                <td style="width: 20%" align="left">
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_clientNameVisibleCheckbox"
                                                        Enabled="false" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="margin-left: 40px">
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_streetAddressLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Street Address"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_streetAddressTextbox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_streetAddressMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_streetAddressVisibleCheckbox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_streetAddressMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_cityLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="City"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_cityTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_cityMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_cityVisibleCheckBox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_cityMandatoryCheckBox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_stateLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="State"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_stateTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_stateMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_stateVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_stateMandatoryCheckBox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_zipCodeLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Zip Code"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_zipCodeTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_zipCodeMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_zipCodeVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_zipCodeMandatoryCheckBox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_countryLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Country"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_countryTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_countryMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_countryVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_countryMandatoryCheckBox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_feinLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="FEIN"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_feinTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_feinMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_feinVisibleCheckBox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_feinMandatoryCheckBox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_faxNumberLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Fax Number"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_faxNumberTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_faxNumberMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_faxNumberVisibleCheckbox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_faxNumberMandatoryCheckBox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_phoneNumberLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Phone Number"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_phoneNumberTextbox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_phoneNumberMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_phoneNumberVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_phoneNumberMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_emailIDLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Email ID"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_emailIDTextbox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_emailIDMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_emailIDVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_emailIDMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_websiteURLLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Website URL"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_websiteURLTextbox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_websiteURLMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_websiteURLVisibleCheckbox"
                                                         onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_websiteURLMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_additionalInfoLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Additional Info"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_additionalInfoTextbox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_additionalInfoMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientSection_additionalInfoVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientSection_additionalInfoMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_contactNameLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Contact Name"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_contactNameTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_myRecordsLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="My Records"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_myRecordsTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_locationLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Location"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_locationTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_createdByLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Created By"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_createdByTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_clientIDLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="Client ID"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_clientIDTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_noOfDepartmentsLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="No Of Departments"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_noOfDepartmentsTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_noOfContactsLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="No Of Contacts"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_noOfContactsTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_noOfPositionProfilesLabel"
                                                        runat="server" SkinID="sknLabelFieldHeaderText" Text="No Of Position Profiles"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_noOfPositionProfilesTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientSection_listOfDepartmentsLabel" runat="server"
                                                        SkinID="sknLabelFieldHeaderText" Text="List Of Departments"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientSection_listOfDepartmentsTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="NomenclatureCustomization_clientDepartmentsSection_clientDepartmentsLiteral"
                                runat="server" Text="Client Departments"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="left">
                                        <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_mandatoryLabel"
                                                        runat="server" Text="Mandatory" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_visibleLabel" runat="server"
                                                        Text="Visible" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 30%">
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_departmentNameLabel"
                                                        runat="server" Text="Department Name" SkinID="sknLabelFieldHeaderText" MaxLength="50"></asp:Label>
                                                </td>
                                                <td style="width: 30%">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_departmentNameTextbox"
                                                        runat="server" Width="175px"></asp:TextBox>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_departmentNameMandatoryCheckbox"
                                                        Enabled="false" Checked="true" />
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_departmentNameVisibleCheckbox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_departmentNameMandatoryCheckbox',this);"  Enabled="false" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_descriptionLabel"
                                                        runat="server" Text="Description" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_descriptionTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_descriptionMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_descriptionVisibleCheckbox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_descriptionMandatoryCheckbox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_streetAddressLabel"
                                                        runat="server" Text="Street Address" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_streetAddressTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_streetAddressMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_streetAddressVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_streetAddressMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_cityLabel" runat="server"
                                                        Text="City" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_cityTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_cityMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_cityVisibleCheckbox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_cityMandatoryCheckbox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_stateLabel" runat="server"
                                                        Text="State" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_stateTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_stateMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_stateVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_stateMandatoryCheckbox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_zipCodeLabel" runat="server"
                                                        Text="Zip Code" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_zipCodeTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_zipCodeMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_zipCodeVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_zipCodeMandatoryCheckbox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_countryLabel" runat="server"
                                                        Text="Country" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_countryTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_countryMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_countryVisibleCheckbox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_countryMandatoryCheckbox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_faxNumberLabel"
                                                        runat="server" Text="Fax Number" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_faxNumberTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_faxNumberMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_faxNumberVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_faxNumberMandatoryCheckbox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_phoneNumberLabel"
                                                        runat="server" Text="Phone Number" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_phoneNumberTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_phoneNumberMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_phoneNumberVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_phoneNumberMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_emailIDLabel" runat="server"
                                                        Text="Email ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_emailIDTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_emailIDMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_emailIDVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_emailIDMandatoryCheckBox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_additionalInfoLabel"
                                                        runat="server" Text="Additional Info" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_additionalInfoTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_additionalInfoMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientDepartmentsSection_additionalInfoVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientDepartmentsSection_additionalInfoMandatoryCheckBox',this);"
                                                        Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_contactNameLabel"
                                                        runat="server" Text="Contact Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_contactNameTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_myRecordsLabel"
                                                        runat="server" Text="My Records" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_myRecordsTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_createdByLabel"
                                                        runat="server" Text="Created By" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_createdByTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_clientIDLabel"
                                                        runat="server" Text="Client ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_clientIDTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_noOfContactsLabel"
                                                        runat="server" Text="No Of Contacts" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_noOfContactsTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientDepartmentsSection_noOfPositionProfilesLabel"
                                                        runat="server" Text="No Of Position Profiles" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientDepartmentsSection_noOfPositionProfilesTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="NomenclatureCustomization_clientContactsSection_clientContactsLiteral"
                                runat="server" Text="Client Contacts"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="left">
                                        <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_mandatoryLabel" runat="server"
                                                        Text="Mandatory" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_visibleLabel" runat="server"
                                                        Text="Visible" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 30%">
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_firstNameLabel" runat="server"
                                                        Text="First Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 30%">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_firstNameTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_firstNameMandatoryCheckbox"
                                                        Enabled="false" Checked="true" />
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_firstNameVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_firstNameMandatoryCheckbox',this);" Enabled="false" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_lastNameLabel" runat="server"
                                                        Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_lastNameTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_lastNameMandatoryCheckbox"
                                                        Enabled="false" Checked="true" />
                                                </td>
                                                <td style="width: 50%">
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_lastNameVisibleCheckbox"
                                                         onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_lastNameMandatoryCheckbox',this);"  Enabled="false" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_middleNameLabel" runat="server"
                                                        Text="Middle Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_middleNameTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_middleNameMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_middleNameVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_middleNameMandatoryCheckBox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_streetAddressLabel"
                                                        runat="server" Text="Street Address" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_streetAddressTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_streetAddressMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_streetAddressVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_streetAddressMandatoryCheckBox',this);"   Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_cityLabel" runat="server"
                                                        Text="City" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_cityTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_cityMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_cityVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_cityMandatoryCheckBox',this);"   Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_zipCodeLabel" runat="server"
                                                        Text="Zip Code" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_zipCodeTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_zipCodeMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_zipCodeVisibleCheckBox"
                                                         onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_zipCodeMandatoryCheckBox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_stateLabel" runat="server"
                                                        Text="State" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_stateTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_stateMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_stateVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_stateMandatoryCheckBox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_countryLabel" runat="server"
                                                        Text="Country" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_countryTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_countryMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_countryVisibleCheckBox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_countryMandatoryCheckBox',this);"  Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_faxNumberLabel" runat="server"
                                                        Text="Fax Number" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_faxNumberTextBox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_faxNumberMandatoryCheckBox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_faxNumberVisibleCheckBox"
                                                       onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_faxNumberMandatoryCheckBox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_phoneNumberLabel"
                                                        runat="server" Text="Phone number" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_phoneNumberTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_phoneNumberMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_phoneNumberVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_phoneNumberMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_emailIDLabel" runat="server"
                                                        Text="Email ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_emailIDTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_emailIDMandatoryCheckbox"
                                                        Enabled="false" Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_emailIDVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_emailIDMandatoryCheckbox',this);" Enabled="false" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_departmentsLabel"
                                                        runat="server" Text="Departments" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_departmentsTextbox"
                                                        runat="server" Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_departmentsMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_departmentsVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_departmentsMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_additionalInfoLabel"
                                                        runat="server" Text="Additional Info" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_additionalInfoTextbox"
                                                        Width="175px" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_additionalInfoMandatoryCheckbox"
                                                        Checked="true" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="NomenclatureCustomization_clientContactsSection_additionalInfoVisibleCheckbox"
                                                        onclick="ValidateVisiblity('NomenclatureCustomization_clientContactsSection_additionalInfoMandatoryCheckbox',this);" Checked="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_myRecordsLabel"
                                                        runat="server" Text="My Records" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_myRecordsTextBox"
                                                        Width="175px" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_createdByLabel"
                                                        runat="server" Text="Created By" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_createdByTextBox"
                                                        Width="175px" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_clientIDLabel"
                                                        runat="server" Text="Client ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_clientIDTextBox"
                                                        Width="175px" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_noOfDepartmentsLabel"
                                                        runat="server" Text="No Of Departments" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_noOfDepartmentsTextBox"
                                                        Width="175px" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_noOfPositionProfilesLabel"
                                                        runat="server" Text="No Of Position Profiles" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_noOfPositionProfilesTextBox"
                                                        Width="175px" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_clientContactsSection_listOfDepartmentsLabel"
                                                        runat="server" Text="List Of Departments" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_clientContactsSection_listOfDepartmentsTextBox"
                                                        Width="175px" runat="server" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="NomenclatureCustomization_rolesSection_rolesLiteral" runat="server"
                                Text="Roles"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="left">
                                        <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 6%">
                                                    <asp:Label ID="NomenclatureCustomization_rolesSection_deliverymanagerLabel" runat="server"
                                                        Text="Delivery Manager" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    <asp:HiddenField ID="NomenclatureCustomization_rolesSection_deliverymanagerHiddenField"
                                                        Value="HCMR04" runat="server" />
                                                </td>
                                                <td style="width: 35%">
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_rolesSection_deliverymanagerTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_rolesSection_recruiterLabel" runat="server"
                                                        Text="Recruiter" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    <asp:HiddenField ID="NomenclatureCustomization_rolesSection_recruiterHiddenField"
                                                        runat="server" Value="HCMR05" />
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_rolesSection_recruiterTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="NomenclatureCustomization_rolesSection_contentAuthorLabel" runat="server"
                                                        Text="Content Author" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    <asp:HiddenField ID="NomenclatureCustomization_rolesSection_contentAuthorHiddenField"
                                                        runat="server" Value="HCMR01" />
                                                </td>
                                                <td>
                                                    <span class='mandatory'>*</span>
                                                    <asp:TextBox ID="NomenclatureCustomization_rolesSection_contentAuthorTextBox" runat="server"
                                                        Width="175px" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="NomenclatureCustomization_bottomMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="NomenclatureCustomization_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="NomenclatureCustomization_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_5">
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="2" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="NomenclatureCustomizationBottomSave_Button" runat="server" Text="Save"
                                            SkinID="sknButtonId" OnClick="NomenclatureCustomization_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="NomenclatureCustomization_bottomRestoreDefaultsLinkButton" runat="server"
                                            Text="Restore Defaults" ToolTip="Click here to restore default attributes" SkinID="sknActionLinkButton"
                                            OnClick="NomenclatureCustomization_restoreDefaultsLinkbutton_Click">
                                        </asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="NomenclatureCustomizationtop_bottomResetLinkButton" runat="server"
                                            Text="Reset" SkinID="sknActionLinkButton" OnClick="NomenclatureCustomization_resetLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="NomenclatureCustomization_botttomCancelLinkButton" runat="server"
                                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
