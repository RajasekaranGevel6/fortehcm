﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientManagement.cs
// File that represents the user interface for the Create,Edit and View the client.

#endregion Header

#region Directives
using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;
using System.Web.UI;
#endregion Directives

namespace Forte.HCM.UI.ClientManagement
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ClientManagement : PageBase
    {
        #region Event Handler

        #region Common Events
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               
              
                LoadValues();

                // Set user ID to the client control.
                ClientManagement_clientControl.TenantID = base.tenantID;
                Page.MaintainScrollPositionOnPostBack = true;
                ClientManagement_Client_clientNameTextBox.Focus();
                if (!IsPostBack)
                {
                    if (!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]) &&
                       (Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.MENU ||
                       Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING))
                    {
                        base.ClearSearchCriteriaSession();
                    }

                    if (!Utility.IsNullOrEmpty(base.NomenclatureAllAttributes))
                    {
                        ClientManagement_clinetTabPanelHeaderLabel.Text = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.Client;
                        ClientManagement_client_department_TabPanelHeaderLabel.Text = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.ClientDepartment;
                        ClientManagement_client_contact_TabPanelHeaderLabel.Text = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.ClientContact;
                        ClientManagement_viewCliendNameHiddenField.Value = base.NomenclatureAllAttributes.CommonNomenclatureAttributes.ClientName;
                    }
                    if (Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT] != null)
                    {
                        FillSearchCriteria(Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT]
                          as ClientManagementState);
                    }
                    else
                    {
                        GetClientManagement();
                        GetClientDepartmentManagement();
                        GetClientContactManagement();
                    }

                    BindNomenclatureLable();
                    ClientManagement_mainTabContainerUpdatePanel.Update();

                    // Show the 'add client' window, when launched from 
                    // clicking on 'add client' option from landing page.
                    if (!Utility.IsNullOrEmpty(Request.QueryString["parentpage"]) &&
                       Request.QueryString["parentpage"].ToUpper() == Constants.ParentPage.WORKFLOW_LANDING &&
                       !Utility.IsNullOrEmpty(Request.QueryString["addclient"]) &&
                       Request.QueryString["addclient"].ToUpper() == "Y")
                    {
                        ClientManagement_clientControl.ClientDataSource = null;
                        ClientManagement_client_ModalpPopupExtender();
                    }
                  
                }
              
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }

        }
      

        private void FillSearchCriteria(ClientManagementState clientManagementState)
        {
            ClientManagement_Client_clientNameTextBox.Text = clientManagementState.ClientClientName;
            ClientManagement_Client_contactNameTextBox.Text = clientManagementState.ClientContactName;
            ClientManagement_Client_createdByTextBox.Text = clientManagementState.ClientCreatedBy;
            ClientManagement_Client_createdByHiddenField.Value = clientManagementState.ClientCreatedHiddenField;
            ClientManagement_Client_locationTextBox.Text = clientManagementState.ClientLocation;

            ViewState["SORT_FIELD"] = clientManagementState.ClientOrderBy;
            ViewState["SORT_ORDER"] = clientManagementState.ClientOrderDirection;
            ViewState["PAGENUMBER"] = clientManagementState.ClientPageNo;
            ClientManagement_Client_userEnableCheckBox.Checked = clientManagementState.ClientUserCheckbox;
            ClientManagement_viewContactIDHiddenField.Value = clientManagementState.ClientContactId == "" ? null : clientManagementState.ClientContactId;
            ClientManagement_viewDepartmentIDHiddenField.Value = clientManagementState.ClientDeparmentId == "" ? null : clientManagementState.ClientDeparmentId;

            ClientManagement_department_clientNameTextBox.Text = clientManagementState.DepartmentClientName;
            ClientManagement_department_contactNameTextBox.Text = clientManagementState.DepartmentContactName;
            ClientManagement_department_createdByTextBox.Text = clientManagementState.DepartmentCreatedBy;
            ClientManagement_department_createdByHiddenField.Value = clientManagementState.DepartmentCreatedHiddenField;
            ClientManagement_departmentNameTextBox.Text = clientManagementState.DepartmentName;
            ViewState["DEPARTMENT_SORT_FIELD"] = clientManagementState.DepartmentOrderBy;
            ViewState["DEPARTMENT_SORT_ORDER"] = clientManagementState.DepartmentDirection;
            ViewState["DEPARTMENT_PAGENUMBER"] = clientManagementState.DepartmentPageNo;
            ClientManagement_Client_userEnableCheckBox.Checked = clientManagementState.DepartmentUserCheckbox;
            ClientManagement_contactDepartmentIDHiddenField.Value = clientManagementState.DepartmentContactId == "" ? null : clientManagementState.DepartmentContactId;
            ClientManagement_department_createdByHiddenField.Value = clientManagementState.DeparmentClientId == "" ? null : clientManagementState.DeparmentClientId;

            ClientManagement_department_clientNameTextBox.Text = clientManagementState.ContactClientName;
            ClientManagement_department_contactNameTextBox.Text = clientManagementState.ContactContactName;
            ClientManagement_department_createdByTextBox.Text = clientManagementState.ContactCreatedBy;
            ClientManagement_department_createdByHiddenField.Value = clientManagementState.ContactCreatedHiddenField;
            ClientManagement_departmentNameTextBox.Text = clientManagementState.ContactDepartment;
            ViewState["DEPARTMENT_SORT_FIELD"] = clientManagementState.ContactOrderBy;
            ViewState["DEPARTMENT_SORT_ORDER"] = clientManagementState.ContactOrderDirection;
            ViewState["DEPARTMENT_PAGENUMBER"] = clientManagementState.ContactPageNo;
            ClientManagement_Client_userEnableCheckBox.Checked = clientManagementState.ContactUserCheckbox;
            ClientManagement_viewClientIDHiddenField.Value = clientManagementState.ContactClientId == "" ? null : clientManagementState.ContactClientId;
            ClientManagement_viewClientIDHiddenField.Value = clientManagementState.ContactDepartmentId == "" ? null : clientManagementState.ContactDepartmentId;

            GetClientManagement();
            ClientManagement_client_bottomPagingNavigator.MoveToPage
               (int.Parse(clientManagementState.ClientPageNo));
            GetClientDepartmentManagement();
            ClientManagement_department_bottomPagingNavigator.MoveToPage
                   (int.Parse(clientManagementState.DepartmentPageNo));

            GetClientContactManagement();
            ClientManagement_contact_bottomPagingNavigator.MoveToPage
                   (int.Parse(clientManagementState.ContactPageNo));
        }


        protected void ClientManagement_serachResultsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ShowSelectedDepartmentHiddenField();
                if (Utility.IsNullOrEmpty(e.CommandName) || e.CommandName == "Sort")
                    return;
                
                int id = int.Parse(e.CommandArgument.ToString());
                switch (e.CommandName)
                {
                    case Constants.ClientManagementConstants.VIEW_CLIENT_CONTACT:
                        ViewClientContact(id);
                        break;
                    case Constants.ClientManagementConstants.VIEW_CLIENT_DEPARTMENT:
                        ViewClientDepartment(id);
                        break;
                    case Constants.ClientManagementConstants.VIEW_CLIENT_POSITION_PROFILE:
                        ViewClientPositionProfile(id);
                        break;
                    case Constants.ClientManagementConstants.VIEW_DEPARTMENT_CONTACT:
                        ViewDepartmentContact(id);
                        break;
                    case Constants.ClientManagementConstants.VIEW_DEPARTMENT_POSITION_PROFILE:
                        ViewDepartmentPositionProfile(id);
                        break;
                    case Constants.ClientManagementConstants.VIEW_CONTACT_DEPARTMENT:
                        ViewContactDepartment(id);
                        break;
                    case Constants.ClientManagementConstants.VIEW_CONTACT_POSITION_PROFILE:
                        ViewContactPositionProfile(id);
                        break;
                    case Constants.ClientManagementConstants.EDIT_DEPARTMENT:
                        ClientManagement_department_ModalpPopupExtender();
                        ClientManagement_departmentControl.DepartmentDataSource = new ClientBLManager().GetDepartment(id);
                        break;
                    case Constants.ClientManagementConstants.ADD_DEPARTMENT_CONTACT:
                        AddDepartmentContact(id);
                        break;
                    case Constants.ClientManagementConstants.VIEW_DEPARTMENT_CLIENT:
                        ShowBackButton(ClientManagementType.Client);
                        ClientManagement_viewDepartmentIDHiddenField.Value = id.ToString();
                        ViewState["SORT_ORDER"] = SortType.Ascending;
                        ViewState["PAGENUMBER"] = "1";
                        ViewState["SORT_FIELD"] = "CLIENT_NAME";
                        GetClientManagement();
                        ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
                        ClientManagement_mainTabContainerUpdatePanel.Update();
                        break;
                    case Constants.ClientManagementConstants.ADD_DEPARTMENT_POSITION_PROFILE:
                        Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING + "=" + id + "&m=1&s=0", false);
                        break;
                    case Constants.ClientManagementConstants.DELETE_DEPARTMENT:
                        ShowSelectedDepartmentHiddenField();
                        ViewState["CURRENTTAB"] = Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING;
                        ClientManagement_viewDepartmentIDHiddenField.Value = id.ToString();
                        RadioButton ClientManagement_confirmDepartmentDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
                        ClientManagement_confirmDepartmentDeleteAllRadioButton.Checked = true;
                        GetDepartmentDetails(Convert.ToInt16(ClientManagement_viewDepartmentIDHiddenField.Value));
                        ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_department_TabPanel;
                        ClientManagement_mainTabContainerUpdatePanel.Update();
                        GetClientDepartmentManagement();
                        break;
                    case Constants.ClientManagementConstants.EDIT_CLIENT:
                        ClientManagement_client_ModalpPopupExtender();
                        ClientManagement_clientControl.ClientDataSource = new ClientBLManager().GetClientInformationByClientId(id);
                        break;
                    case Constants.ClientManagementConstants.ADD_CLIENT_DEPARTMENT:
                        AddDepartement(id);
                        break;
                    case Constants.ClientManagementConstants.ADD_CLIENT_CONTACT:
                        ClientManagement_departIDHiddenField.Value = null;
                        AddContact(id);
                        //ClientManagement_contactControl.DepartmentDataSource = null;
                        //ClientManagement_contactControl.ClientDataSource = new ClientBLManager().GetClientInformationByClientId(int.Parse(e.CommandArgument.ToString()));
                        break;
                    case Constants.ClientManagementConstants.ADD_CLIENT_POSITION_PROFILE:
                        Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING + "=" + id + "&m=1&s=0", false);
                        break;
                    case Constants.ClientManagementConstants.DELETE_CLIENT:
                        ViewState["CURRENTTAB"] = Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING;
                        ClientManagement_viewClientIDHiddenField.Value = id.ToString();
                        RadioButton ClientManagement_confirmClientDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
                        ClientManagement_confirmClientDeleteAllRadioButton.Checked = true;
                        GetClientDetails(Convert.ToInt16(ClientManagement_viewClientIDHiddenField.Value));
                        ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
                        ClientManagement_mainTabContainerUpdatePanel.Update();
                        GetClientManagement();
                        break;
                    case Constants.ClientManagementConstants.EDIT_CONTACT:
                        ClientManagement_contact_ModalpPopupExtender();
                        ClientManagement_contactControl.clientContactInformationDataSource = new ClientBLManager().GetContact(id);
                        break;
                    case Constants.ClientManagementConstants.VIEW_CONTACT_CLIENT:
                        ShowBackButton(ClientManagementType.Client);
                        ClientManagement_viewContactIDHiddenField.Value = id.ToString();
                        ViewState["SORT_ORDER"] = SortType.Ascending;
                        ViewState["PAGENUMBER"] = "1";
                        ViewState["SORT_FIELD"] = "CLIENT_NAME";
                        GetClientManagement();
                        ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
                        ClientManagement_mainTabContainerUpdatePanel.Update();
                        break;
                    case Constants.ClientManagementConstants.ADD_CONTACT_POSITION_PROFILE:
                       
                       //DSK Changes                        
                                          
                       string dept_id = "0"; 

                       HiddenField Dept_HiddenField = (HiddenField)((((ImageButton)e.CommandSource).NamingContainer).FindControl("HiddenField_Deptid"));
                      
                      if (Dept_HiddenField != null)
                          dept_id = Dept_HiddenField.Value;                       

                       Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING + "=" + id + "&" + Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING + "=" + dept_id + "&m=1&s=0", false);                     
                        break;

                    case Constants.ClientManagementConstants.DELETE_CONTACT:
                        ViewState["CURRENTTAB"] = Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING;
                        ClientManagement_viewContactIDHiddenField.Value = id.ToString();
                        RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
                        ClientManagement_confirmContactDeleteAllRadioButton.Checked = true;
                        GetContactDetails(Convert.ToInt16(ClientManagement_viewContactIDHiddenField.Value));
                        ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_contact_TabPanel;
                        ClientManagement_mainTabContainerUpdatePanel.Update();
                        GetClientContactManagement();
                        break;

                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }     

        protected void ClientManagement_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ClientManagement.aspx?m=0&s=0&parentpage=MENU", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        protected void ClientManagement_backLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Utility.IsNullOrEmpty(ClientManagement_viewClientIDHiddenField.Value) || !Utility.IsNullOrEmpty(ClientManagement_clientDepartmentIDHiddenField.Value))
                    ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
                else if (!Utility.IsNullOrEmpty(ClientManagement_viewContactIDHiddenField.Value) || !Utility.IsNullOrEmpty(ClientManagement_contactDepartmentIDHiddenField.Value))
                    ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_contact_TabPanel;
                else if (!Utility.IsNullOrEmpty(ClientManagement_viewDepartmentIDHiddenField.Value))
                    ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_department_TabPanel;
                ClientManagement_mainTabContainerUpdatePanel.Update();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        protected void ClientManagement_searchButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                ClientManagement_viewClientIDHiddenField.Value = null;
                ClientManagement_viewContactIDHiddenField.Value = null;
                ClientManagement_viewDepartmentIDHiddenField.Value = null;
                ClientManagement_contactDepartmentIDHiddenField.Value = null;
                ClientManagement_clientDepartmentIDHiddenField.Value = null;
                ShowBackButton(ClientManagementType.All);
                switch (e.CommandName)
                {
                    case Constants.ClientManagementConstants.SEARCH_CLIENT:
                        ViewState["SORT_ORDER"] = SortType.Ascending;
                        ViewState["PAGENUMBER"] = "1";
                        ViewState["SORT_FIELD"] = "CLIENT_NAME";
                        GetClientManagement();
                        ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
                        break;
                    case Constants.ClientManagementConstants.SEARCH_DEPARTMENT:
                        ViewState["DEPARTMENT_SORT_ORDER"] = SortType.Ascending;
                        ViewState["DEPARTMENT_PAGENUMBER"] = "1";
                        ViewState["DEPARTMENT_SORT_FIELD"] = "CLIENT_NAME";
                        GetClientDepartmentManagement();
                        ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_department_TabPanel;
                        break;
                    case Constants.ClientManagementConstants.SEARCH_CONTACT:
                        ViewState["CONTACT_SORT_ORDER"] = SortType.Ascending;
                        ViewState["CONTACT_PAGENUMBER"] = "1";
                        ViewState["CONTACT_SORT_FIELD"] = "CLIENT_NAME";
                        GetClientContactManagement();
                        ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_contact_TabPanel;
                        break;
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }

        protected void ClientManagement_departmentListOnRepeaterCommandClick(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument == null)
                    return;
                ClientManagement_contactDepartmentIDHiddenField.Value = null;
                ClientManagement_clientDepartmentIDHiddenField.Value = null;
                if (e.CommandName == Constants.ClientManagementConstants.SHOW_CLIENT_DEPARTMENT)
                    ClientManagement_clientDepartmentIDHiddenField.Value = e.CommandArgument.ToString();
                else if (e.CommandName == Constants.ClientManagementConstants.SHOW_CONTACT_DEPARTMENT)
                    ClientManagement_contactDepartmentIDHiddenField.Value = e.CommandArgument.ToString();

                ViewDepartment();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        #endregion

        #region Client Events

        protected void ClientManagement_createClientButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case Constants.ClientManagementConstants.ADD_CLIENT:
                        ClientManagement_clientControl.ClientDataSource = null;
                        ClientManagement_client_ModalpPopupExtender();
                        break;
                    case Constants.ClientManagementConstants.ADD_DEPARTMENT:
                       

                        ClientManagement_department_ModalpPopupExtender();
                        if (ClientManagement_viewClientIDHiddenField.Value.Trim() == "")
                            ClientManagement_viewClientIDHiddenField.Value = "0";

                            ClientManagement_departmentControl.DepartmentDataSource = null;
                            ClientManagement_departmentControl.ClientDataSource = new ClientBLManager().GetClientInformationByClientId(Convert.ToInt32(ClientManagement_viewClientIDHiddenField.Value));
                      

                            break;
                    case Constants.ClientManagementConstants.ADD_CONTACT:                                            
                      
                        ClientManagement_contact_ModalpPopupExtender();                        
                      
                        if (ClientManagement_viewClientIDHiddenField.Value.Trim() == "")
                            ClientManagement_viewClientIDHiddenField.Value = "0";

                         ClientManagement_contactControl.clientContactInformationDataSource = null;
                         ClientInformation clientInformation = new ClientInformation();
                         clientInformation = new ClientBLManager().GetClientInformationByClientId(Convert.ToInt32(ClientManagement_viewClientIDHiddenField.Value));
                         if (!Utility.IsNullOrEmpty(ClientManagement_departIDHiddenField.Value))
                                clientInformation.sellectedDepartmentID = int.Parse(ClientManagement_departIDHiddenField.Value.ToString());
                                ClientManagement_contactControl.ClientDataSource = clientInformation;


                        break;
                }

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }

        /// <summary>
        /// Handles the OkCommand event of the ClientManagement_viewControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        protected void ClientManagement_viewControl_OkCommand(object sender, CommandEventArgs e)
        {
            //try
            //{
            //    int clientID = int.Parse(e.CommandArgument.ToString());
            //    switch (e.CommandName)
            //    {
            //        case Constants.ClientManagementConstants.EDIT_CLIENT:
            //            ClientManagement_client_ModalpPopupExtender();
            //            ClientManagement_clientControl.ClientDataSource = new ClientBLManager().GetClientInformationByClientId(clientID);
            //            break;
            //        case Constants.ClientManagementConstants.ADD_CLIENT_DEPARTMENT:
            //            AddDepartement(clientID);
            //            break;
            //        case Constants.ClientManagementConstants.ADD_CLIENT_CONTACT:
            //            ClientManagement_departIDHiddenField.Value = null;
            //            AddContact(clientID);
            //            //ClientManagement_contactControl.DepartmentDataSource = null;
            //            //ClientManagement_contactControl.ClientDataSource = new ClientBLManager().GetClientInformationByClientId(int.Parse(e.CommandArgument.ToString()));
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_CLIENT_DEPARTMENT:
            //            ViewClientDepartment(clientID);
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_CLIENT_CONTACT:
            //            ViewClientContact(clientID);
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_CLIENT_POSITION_PROFILE:
            //            ViewClientPositionProfile(clientID);
            //            break;
            //        case Constants.ClientManagementConstants.ADD_CLIENT_POSITION_PROFILE:
            //            Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING + "=" + clientID, false);
            //            break;
            //        case Constants.ClientManagementConstants.DELETE_CLIENT:
            //            ViewState["CURRENTTAB"] = Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING;
            //            ClientManagement_viewClientIDHiddenField.Value = clientID.ToString();
            //            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            //            ClientManagement_confirmContactDeleteAllRadioButton.Checked = true;
            //            GetClientDetails(Convert.ToInt16(ClientManagement_viewClientIDHiddenField.Value));
            //            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
            //            ClientManagement_mainTabContainerUpdatePanel.Update();
            //            GetClientManagement();
            //            break;
            //    }
            //}
            //catch (Exception exp)
            //{
            //    Logger.ExceptionLog(exp);
            //    base.ShowMessage(ClientManagement_topErrorMessageLabel,
            //              ClientManagement_bottomErrorMessageLabel,
            //              exp.Message);
            //}
        }
        /// <summary>
        /// Handles the Sorting event of the ClientManagement_clientSerachResultsGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void ClientManagement_clientSerachResultsGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                ClientManagement_stateExpandHiddenField.Value = "0";
                ClientManagement_client_ExpandLinkButton.Text = "Expand All";

                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] = ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                ViewState["PAGENUMBER"] = "1";
                ClientManagement_client_bottomPagingNavigator.Reset();
                //LoadQuestion(1);
                GetClientManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                ClientManagement_bottomErrorMessageLabel, exp.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void ClientManagement_clientSerachResultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                ClientManagement_clientSerachResultsGridView.Columns[0].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientID;
                ClientManagement_clientSerachResultsGridView.Columns[1].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientName;
                ClientManagement_clientSerachResultsGridView.Columns[2].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.City;
                ClientManagement_clientSerachResultsGridView.Columns[3].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.state;
                ClientManagement_clientSerachResultsGridView.Columns[4].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.NoOfDepartments;
                ClientManagement_clientSerachResultsGridView.Columns[5].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.NoOfContacts;
                ClientManagement_clientSerachResultsGridView.Columns[6].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.NoOfPositionProfiles;
                ClientManagement_clientSerachResultsGridView.Columns[7].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.CreatedBy;

                int sortColumnIndex = GetSortColumnIndex
                    (ClientManagement_clientSerachResultsGridView,
                    (string)ViewState["SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                ClientManagement_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the ClientManagement_clientSerachResultsGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void ClientManagement_clientSerachResultsGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ClientDetailView ClientManagement_viewControl =
                    (ClientDetailView)e.Row.FindControl("ClientManagement_viewControl");
                ClientManagement_viewControl.ClientNomenclatureAttribute =
                    base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes;
                ClientManagement_viewControl.DeleteButtonVisibility = base.IsClientManagementDelete;
                Label ClientManagement_clientIdlabel =
                    (Label)e.Row.FindControl("ClientManagement_clientIdlabel");
                LinkButton ClientManagement_clientNameLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_clientNameLinkButton");

                LinkButton ClientManagement_noOfDepartmentsLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_noOfDepartmentsLinkButton");
                LinkButton ClientManagement_noOfContactLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_noOfContactLinkButton");
                LinkButton ClientManagement_noOfPositionProfileLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_noOfPositionProfileLinkButton");
                HtmlContainerControl ClientManagement_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("ClientManagement_detailsDiv");
                HtmlAnchor ClientManagement_focusDownLink =
                    (HtmlAnchor)e.Row.FindControl("ClientManagement_focusDownLink");

                Label ClientManagement_noOfDepartmentsLabel = (Label)e.Row.FindControl("ClientManagement_noOfDepartmentsLabel");
                Label ClientManagement_noOfContactLabel = (Label)e.Row.FindControl("ClientManagement_noOfContactLabel");
                Label ClientManagement_noOfPositionProfileLabel = (Label)e.Row.FindControl("ClientManagement_noOfPositionProfileLabel");
                if (ClientManagement_noOfDepartmentsLabel.Text != "0")
                {
                    ClientManagement_noOfDepartmentsLinkButton.Visible = true;
                    ClientManagement_noOfDepartmentsLinkButton.CommandName = Constants.ClientManagementConstants.VIEW_CLIENT_DEPARTMENT;
                    ClientManagement_noOfDepartmentsLinkButton.CommandArgument = ClientManagement_clientIdlabel.Text;
                }
                else
                    ClientManagement_noOfDepartmentsLabel.Visible = true;

                if (ClientManagement_noOfContactLabel.Text != "0")
                {
                    ClientManagement_noOfContactLinkButton.Visible = true;
                    ClientManagement_noOfContactLinkButton.CommandName = Constants.ClientManagementConstants.VIEW_CLIENT_CONTACT;
                    ClientManagement_noOfContactLinkButton.CommandArgument = ClientManagement_clientIdlabel.Text;
                }
                else
                    ClientManagement_noOfContactLabel.Visible = true;
                if (ClientManagement_noOfPositionProfileLabel.Text != "0")
                {
                    ClientManagement_noOfPositionProfileLinkButton.Visible = true;
                    ClientManagement_noOfPositionProfileLinkButton.CommandName = Constants.ClientManagementConstants.VIEW_CLIENT_POSITION_PROFILE;
                    ClientManagement_noOfPositionProfileLinkButton.CommandArgument = ClientManagement_clientIdlabel.Text;
                }
                else
                    ClientManagement_noOfPositionProfileLabel.Visible = true;


                ClientManagement_clientNameLinkButton.Attributes.Add("onclick", "return ClientHideDetails('"
                    + ClientManagement_client_ExpandLinkButton.ClientID + "','"
                    + ClientManagement_detailsDiv.ClientID + "','"
                    + ClientManagement_focusDownLink.ClientID + "','"
                    + ClientManagement_stateExpandHiddenField.ClientID + "','"
                    + ClientManagement_clientSerachResultsDiv.ClientID
                    + "','ClientManagement_detailsDiv')");

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        /// <summary>
        /// Handles the OkCommandClick event of the ClientManagement_clientControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        void ClientManagement_clientControl_OkCommandClick(object sender, CommandEventArgs e)
        {
            try
            {
                int clientID = 0;
                bool editflag = true;
                ClientInformation clientInformation = new ClientInformation();
                clientInformation = ClientManagement_clientControl.ClientDataSource;
                clientInformation.CreatedBy = base.userID;
                clientInformation.TenantID = base.tenantID;
                if (e.CommandName == "edit")
                {
                    clientInformation.ClientID = int.Parse(e.CommandArgument.ToString());
                    new ClientBLManager().UpdateClient(clientInformation, out clientID);
                }
                else
                {
                    editflag = false;
                    new ClientBLManager().InsertClient(clientInformation, out clientID);
                }
                if (clientID > 0)
                {
                    if (editflag)
                        base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                                 ClientManagement_bottomSuccessMessageLabel,
                                             "Client updated successfully");
                    else
                    {
                        base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                                ClientManagement_bottomSuccessMessageLabel,
                                "Client created successfully");
                        ClientManagement_clientIDHiddenField.Value = clientID.ToString();
                        ClientManagement_ConfirmMessageControl.Message = "Client created successfully";
                        ClientManagement_ConfirmMessageControl.Title = "Client Control";
                        ClientManagement_ConfirmMessageControl.Type = MessageBoxType.ClientControl;
                        ClientManagement_ConfirmMessageControl.Client_ID = clientID;
                        ClientManagement_confirmMessageModalpPopupExtender.Show();
                        ClientManagement_confirmMessageModalPopUpUpdatePanel.Update();
                    }
                }
                else
                {
                    ClientManagement_clientControl.ShowMessage = "Client Name already exist for this tenant";
                    ClientManagement_client_ModalpPopupExtender();
                }
                GetClientManagement();
                GetClientDepartmentManagement();
                GetClientContactManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        /// <summary>
        /// Handles the PageNumberClick event of the ClientManagement_client_bottomPagingNavigator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Forte.HCM.EventSupport.PageNumberEventArgs"/> instance containing the event data.</param>
        void ClientManagement_client_bottomPagingNavigator_PageNumberClick(object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;
                GetClientManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        #endregion

        #region Department Events
        protected void ClientManagement_departmentViewControl_OkCommand(object sender, CommandEventArgs e)
        {
            //try
            //{
            //    int departmentID = int.Parse(e.CommandArgument.ToString());
            //    switch (e.CommandName)
            //    {
            //        case Constants.ClientManagementConstants.EDIT_DEPARTMENT:
            //            ClientManagement_department_ModalpPopupExtender();
            //            ClientManagement_departmentControl.DepartmentDataSource = new ClientBLManager().GetDepartment(departmentID);
            //            break;
            //        case Constants.ClientManagementConstants.ADD_DEPARTMENT_CONTACT:
            //            AddDepartmentContact(departmentID);
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_DEPARTMENT_CLIENT:
            //            ShowBackButton(ClientManagementType.Client);
            //            ClientManagement_viewDepartmentIDHiddenField.Value = departmentID.ToString();
            //            GetClientManagement();
            //            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
            //            ClientManagement_mainTabContainerUpdatePanel.Update();
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_DEPARTMENT_CONTACT:
            //            ViewDepartmentContact(departmentID);
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_DEPARTMENT_POSITION_PROFILE:
            //            ViewDepartmentPositionProfile(departmentID);
            //            break;
            //        case Constants.ClientManagementConstants.ADD_DEPARTMENT_POSITION_PROFILE:
            //            Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING + "=" + departmentID, false);
            //            break;
            //        case Constants.ClientManagementConstants.DELETE_DEPARTMENT:
            //            ShowSelectedDepartmentHiddenField();
            //            ViewState["CURRENTTAB"] = Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING;
            //            ClientManagement_viewDepartmentIDHiddenField.Value = departmentID.ToString();
            //            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            //            ClientManagement_confirmContactDeleteAllRadioButton.Checked = true;
            //            GetDepartmentDetails(Convert.ToInt16(ClientManagement_viewDepartmentIDHiddenField.Value));
            //            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_department_TabPanel;
            //            ClientManagement_mainTabContainerUpdatePanel.Update();
            //            GetClientDepartmentManagement();
            //            break;
            //    }
            //}
            //catch (Exception exp)
            //{
            //    Logger.ExceptionLog(exp);
            //    base.ShowMessage(ClientManagement_topErrorMessageLabel,
            //              ClientManagement_bottomErrorMessageLabel,
            //              exp.Message);
            //}
        }
        protected void ClientManagement_departmentSerachResultsGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ClientDepartmentDetailView ClientManagement_department_viewControl =
                    (ClientDepartmentDetailView)e.Row.FindControl("ClientManagement_department_viewControl");
                ClientManagement_department_viewControl.DepartmentNomenClature =
                    base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes;

                ClientManagement_department_viewControl.DeleteButtonVisibility = base.IsClientManagementDelete;
                LinkButton ClientManagement_department_deparmentNameLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_department_deparmentNameLinkButton");
                HtmlContainerControl ClientManagement_department_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("ClientManagement_department_detailsDiv");
                HtmlAnchor ClientManagement_department_focusmeLink =
                    (HtmlAnchor)e.Row.FindControl("ClientManagement_department_focusmeLink");

                ClientManagement_department_deparmentNameLinkButton.Attributes.Add("onclick", "return TestHideDetails('" +
                ClientManagement_department_detailsDiv.ClientID + "','" + ClientManagement_department_focusmeLink.ClientID + "')");

                ClientManagement_department_deparmentNameLinkButton.Attributes.Add("onclick", "return ClientHideDetails('"
                   + ClientManagement_client_ExpandLinkButton.ClientID + "','"
                   + ClientManagement_department_detailsDiv.ClientID + "','"
                   + ClientManagement_department_focusmeLink.ClientID + "','"
                   + ClientManagement_department_stateExpandHiddenField.ClientID + "','"
                   + ClientManagement_department_clientSerachResultsDiv.ClientID

                   + "','ClientManagement_department_detailsDiv')");

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                HiddenField ClientManagement_departmentIDHiddenField = (HiddenField)e.Row.FindControl("ClientManagement_departmentIDHiddenField");

                LinkButton ClientManagement_department_noOfContactLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_department_noOfContactLinkButton");
                LinkButton ClientManagement_department_noOfPositionProfileLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_department_noOfPositionProfileLinkButton");


                Label ClientManagement_department_noOfContactLabel = (Label)e.Row.FindControl("ClientManagement_department_noOfContactLabel");
                Label ClientManagement_department_noOfPositionProfileLabel = (Label)e.Row.FindControl("ClientManagement_department_noOfPositionProfileLabel");


                if (ClientManagement_department_noOfContactLabel.Text != "0")
                {
                    ClientManagement_department_noOfContactLinkButton.Visible = true;
                    ClientManagement_department_noOfContactLinkButton.CommandName = Constants.ClientManagementConstants.VIEW_DEPARTMENT_CONTACT;
                    ClientManagement_department_noOfContactLinkButton.CommandArgument = ClientManagement_departmentIDHiddenField.Value;
                }
                else
                    ClientManagement_department_noOfContactLabel.Visible = true;
                if (ClientManagement_department_noOfPositionProfileLabel.Text != "0")
                {
                    ClientManagement_department_noOfPositionProfileLinkButton.Visible = true;
                    ClientManagement_department_noOfPositionProfileLinkButton.CommandName = Constants.ClientManagementConstants.VIEW_DEPARTMENT_POSITION_PROFILE;
                    ClientManagement_department_noOfPositionProfileLinkButton.CommandArgument = ClientManagement_departmentIDHiddenField.Value;
                }
                else
                    ClientManagement_department_noOfPositionProfileLabel.Visible = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        protected void ClientManagement_departmentSerachResultsGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                ClientManagement_stateExpandHiddenField.Value = "0";
                ClientManagement_client_ExpandLinkButton.Text = "Expand All";
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["DEPARTMENT_SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["DEPARTMENT_SORT_ORDER"] = ((SortType)ViewState["DEPARTMENT_SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["DEPARTMENT_SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["DEPARTMENT_SORT_ORDER"] = SortType.Ascending;

                ViewState["DEPARTMENT_SORT_FIELD"] = e.SortExpression;
                ViewState["DEPARTMENT_PAGENUMBER"] = "1";
                ClientManagement_department_bottomPagingNavigator.Reset();
                GetClientDepartmentManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                ClientManagement_bottomErrorMessageLabel, exp.Message);
            }
        }
        protected void ClientManagement_departmentSerachResultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                ClientManagement_department_clientSerachResultsGridView.Columns[0].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientID;
                ClientManagement_department_clientSerachResultsGridView.Columns[1].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientName;
                ClientManagement_department_clientSerachResultsGridView.Columns[2].HeaderText = base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes.DepartmentName;
                ClientManagement_department_clientSerachResultsGridView.Columns[3].HeaderText = base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes.City;
                ClientManagement_department_clientSerachResultsGridView.Columns[4].HeaderText = base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes.state;
                ClientManagement_department_clientSerachResultsGridView.Columns[5].HeaderText = base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes.NoOfContacts;
                ClientManagement_department_clientSerachResultsGridView.Columns[6].HeaderText = base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes.NoOfPositionProfiles;
                ClientManagement_department_clientSerachResultsGridView.Columns[7].HeaderText = base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes.CreatedBy;

                int sortColumnIndex = GetSortColumnIndex
                    (ClientManagement_department_clientSerachResultsGridView,
                    (string)ViewState["DEPARTMENT_SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["DEPARTMENT_SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                ClientManagement_bottomErrorMessageLabel, exp.Message);
            }
        }
        void ClientManagement_departmentControl_OkCommandClick(object sender, CommandEventArgs e)
        {
            try
            {
                int departmentID = 0;
                bool editflag = true;
                Department department = new Department();
                department = ClientManagement_departmentControl.DepartmentDataSource;
                department.CreatedBy = base.userID;
                if (e.CommandName == "edit")
                {
                    department.DepartmentID = int.Parse(e.CommandArgument.ToString());
                    new ClientBLManager().UpdateDepartment(department, out departmentID);
                }
                else
                {
                    ClientManagement_clientIDHiddenField.Value = department.ClientID.ToString();
                    editflag = false;
                    new ClientBLManager().InsertDepartment(department, out departmentID);
                    ClientManagement_departIDHiddenField.Value = departmentID.ToString();
                }
                if (departmentID > 0)
                {
                    if (!editflag)
                    {
                        base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                            ClientManagement_bottomSuccessMessageLabel,
                            "Department created successfully");
                        ClientManagement_ConfirmMessageControl.Message = "Department created successfully";
                        ClientManagement_ConfirmMessageControl.Title = "Department";
                        ClientManagement_ConfirmMessageControl.Type = MessageBoxType.DepartmentControl;
                        ClientManagement_ConfirmMessageControl.Client_ID = department.ClientID;
                        ClientManagement_confirmMessageModalPopUpUpdatePanel.Update();
                        ClientManagement_confirmMessageModalpPopupExtender.Show();
                        ClientManagement_departIDHiddenField.Value = departmentID.ToString();
                    }
                    else
                        base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                                 ClientManagement_bottomSuccessMessageLabel,
                                             "Department updated successfully");
                }
                else
                {
                    ClientManagement_departmentControl.ShowMessage = "Department name already exist for this client";
                    ClientManagement_department_ModalpPopupExtender1();
                }

                ShowSelectedDepartmentHiddenField();
                GetClientManagement();
                GetClientDepartmentManagement();
                GetClientContactManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        void ClientManagement_department_bottomPagingNavigator_PageNumberClick(object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                ViewState["DEPARTMENT_PAGENUMBER"] = e.PageNumber;
                GetClientDepartmentManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        void ClientManagement_departmentControl_SellectedIndexChanged(object sender, EventArgs e)
        {
            AddDepartement(ClientManagement_departmentControl.Client_ID);
        }
        #endregion

        #region Contact Events
        protected void ClientManagement_contactViewControl_OkCommand(object sender, CommandEventArgs e)
        {
            //try
            //{
            //    int contactID = int.Parse(e.CommandArgument.ToString());
            //    switch (e.CommandName)
            //    {
            //        case Constants.ClientManagementConstants.EDIT_CONTACT:
            //            ClientManagement_contact_ModalpPopupExtender();
            //            ClientManagement_contactControl.clientContactInformationDataSource = new ClientBLManager().GetContact(contactID);
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_CONTACT_CLIENT:
            //            ShowBackButton(ClientManagementType.Client);
            //            ClientManagement_viewContactIDHiddenField.Value = contactID.ToString();
            //            GetClientManagement();
            //            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
            //            ClientManagement_mainTabContainerUpdatePanel.Update();
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_CONTACT_DEPARTMENT:
            //            ViewContactDepartment(contactID);
            //            break;
            //        case Constants.ClientManagementConstants.VIEW_CONTACT_POSITION_PROFILE:
            //            ViewContactPositionProfile(contactID);
            //            break;
            //        case Constants.ClientManagementConstants.ADD_CONTACT_POSITION_PROFILE:
            //            Response.Redirect("~/PositionProfile/PositionProfileBasicInfo.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING + "=" + contactID, false);
            //            break;
            //        case Constants.ClientManagementConstants.DELETE_CONTACT:
            //            ViewState["CURRENTTAB"] = Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING;
            //            ClientManagement_viewContactIDHiddenField.Value = contactID.ToString();
            //            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            //            ClientManagement_confirmContactDeleteAllRadioButton.Checked = true;
            //            GetContactDetails(Convert.ToInt16(ClientManagement_viewContactIDHiddenField.Value));
            //            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_contact_TabPanel;
            //            ClientManagement_mainTabContainerUpdatePanel.Update();
            //            GetClientContactManagement();
            //            break;
            //    }
            //}
            //catch (Exception exp)
            //{
            //    Logger.ExceptionLog(exp);
            //    base.ShowMessage(ClientManagement_topErrorMessageLabel,
            //              ClientManagement_bottomErrorMessageLabel,
            //              exp.Message);
            //}
        }
        protected void ClientManagement_contactSerachResultsGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.DataRow)
                    return;

                ClientContactDetailView ClientManagement_contact_viewControl =
                  (ClientContactDetailView)e.Row.FindControl("ClientManagement_contact_viewControl");
                ClientManagement_contact_viewControl.ContactNomenclatureAttribute =
                    base.NomenclatureAllAttributes.ContactNomenClatureAttrubutes;
                ClientManagement_contact_viewControl.DeleteButtonVisibility = base.IsClientManagementDelete;
                LinkButton ClientManagement_contact_contactNameLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_contact_contactNameLinkButton");
                HtmlContainerControl ClientManagement_contact_detailsDiv =
                    (HtmlContainerControl)e.Row.FindControl("ClientManagement_contact_detailsDiv");
                HtmlAnchor ClientManagement_contact_focusmeLink =
                    (HtmlAnchor)e.Row.FindControl("ClientManagement_contact_focusmeLink");
                ClientManagement_contact_contactNameLinkButton.Attributes.Add("onclick", "return ClientHideDetails('"
                   + ClientManagement_client_ExpandLinkButton.ClientID + "','"
                   + ClientManagement_contact_detailsDiv.ClientID + "','"
                   + ClientManagement_contact_focusmeLink.ClientID + "','"
                   + ClientManagement_contact_stateExpandHiddenField.ClientID + "','"
                   + ClientManagement_contact_clientSerachResultsDiv.ClientID

                   + "','ClientManagement_contact_detailsDiv')");

                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                HiddenField ClientManagement_contactIDHiddenField = (HiddenField)e.Row.FindControl("ClientManagement_contactIDHiddenField");

                LinkButton ClientManagement_contact_noOfDepartmentsLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_contact_noOfDepartmentsLinkButton");
                LinkButton ClientManagement_contact_noOfPositionProfileLinkButton =
                       (LinkButton)e.Row.FindControl("ClientManagement_contact_noOfPositionProfileLinkButton");


                Label ClientManagement_contact_noOfPositionProfileLabel = (Label)e.Row.FindControl("ClientManagement_contact_noOfPositionProfileLabel");
                Label ClientManagement_contact_noOfDepartmentsLabel = (Label)e.Row.FindControl("ClientManagement_contact_noOfDepartmentsLabel");


                if (ClientManagement_contact_noOfDepartmentsLabel.Text != "0")
                {
                    ClientManagement_contact_noOfDepartmentsLinkButton.Visible = true;
                    ClientManagement_contact_noOfDepartmentsLinkButton.CommandName = Constants.ClientManagementConstants.VIEW_CONTACT_DEPARTMENT;
                    ClientManagement_contact_noOfDepartmentsLinkButton.CommandArgument = ClientManagement_contactIDHiddenField.Value;
                }
                else
                    ClientManagement_contact_noOfDepartmentsLabel.Visible = true;
                if (ClientManagement_contact_noOfPositionProfileLabel.Text != "0")
                {
                    ClientManagement_contact_noOfPositionProfileLinkButton.Visible = true;
                    ClientManagement_contact_noOfPositionProfileLinkButton.CommandName = Constants.ClientManagementConstants.VIEW_CONTACT_POSITION_PROFILE;
                    ClientManagement_contact_noOfPositionProfileLinkButton.CommandArgument = ClientManagement_contactIDHiddenField.Value;
                }
                else
                    ClientManagement_contact_noOfPositionProfileLabel.Visible = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        protected void ClientManagement_contactSerachResultsGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                ClientManagement_contact_stateExpandHiddenField.Value = "0";
                ClientManagement_contact_ExpandLinkButton.Text = "Expand All";
                string sortOrder = string.Empty;
                if (e.SortExpression.IndexOf(' ') >= 0)
                {
                    sortOrder = e.SortExpression.Split(' ')[1];
                    e.SortExpression = e.SortExpression.Split(' ')[0];
                }

                if (ViewState["CONTACT_SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["CONTACT_SORT_ORDER"] = ((SortType)ViewState["CONTACT_SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else if (sortOrder == "DESC")
                    ViewState["CONTACT_SORT_ORDER"] = SortType.Descending;
                else
                    ViewState["CONTACT_SORT_ORDER"] = SortType.Ascending;

                ViewState["CONTACT_SORT_FIELD"] = e.SortExpression;
                ViewState["CONTACT_PAGENUMBER"] = "1";
                ClientManagement_contact_bottomPagingNavigator.Reset();
                GetClientContactManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                ClientManagement_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void ClientManagement_contactSerachResultsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;

                ClientManagement_contact_SerachResultsGridView.Columns[0].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientID;
                ClientManagement_contact_SerachResultsGridView.Columns[1].HeaderText = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientName;
                ClientManagement_contact_SerachResultsGridView.Columns[2].HeaderText = base.NomenclatureAllAttributes.ContactNomenClatureAttrubutes.ContactName;
                ClientManagement_contact_SerachResultsGridView.Columns[3].HeaderText = base.NomenclatureAllAttributes.ContactNomenClatureAttrubutes.City;
                ClientManagement_contact_SerachResultsGridView.Columns[4].HeaderText = base.NomenclatureAllAttributes.ContactNomenClatureAttrubutes.state;
                ClientManagement_contact_SerachResultsGridView.Columns[5].HeaderText = base.NomenclatureAllAttributes.ContactNomenClatureAttrubutes.NoOfDepartments;
                ClientManagement_contact_SerachResultsGridView.Columns[6].HeaderText = base.NomenclatureAllAttributes.ContactNomenClatureAttrubutes.NoOfPositionProfiles;
                ClientManagement_contact_SerachResultsGridView.Columns[7].HeaderText = base.NomenclatureAllAttributes.ContactNomenClatureAttrubutes.CreatedBy;

                int sortColumnIndex = GetSortColumnIndex
                    (ClientManagement_contact_SerachResultsGridView,
                    (string)ViewState["CONTACT_SORT_FIELD"]);

                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row,
                        ((SortType)ViewState["CONTACT_SORT_ORDER"]));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                ClientManagement_bottomErrorMessageLabel, exp.Message);
            }
        }

        void ClientManagement_contact_bottomPagingNavigator_PageNumberClick(object sender, EventSupport.PageNumberEventArgs e)
        {
            try
            {
                ViewState["CONTACT_PAGENUMBER"] = e.PageNumber;
                GetClientContactManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }

        void ClientManagement_ConfirmMessageControl_CommandEventAdd_Click(object sender, CommandEventArgs e)
        {
            int clientID = 0;
            //if (Utility.IsNullOrEmpty(ClientManagement_clientIDHiddenField.Value))
            clientID = int.Parse(e.CommandArgument.ToString());
            //else
            //    clientID = int.Parse(ClientManagement_clientIDHiddenField.Value);

            switch (e.CommandName)
            {
                case Constants.ClientManagementConstants.ADD_CLIENT_DEPARTMENT:
                    AddDepartement(clientID);
                    break;
                case Constants.ClientManagementConstants.ADD_CLIENT_CONTACT:
                    AddContact(clientID);
                    break;
                case Constants.ClientManagementConstants.ADD_DEPARTMENT_CONTACT:
                    AddDepartmentContact(clientID);
                    break;
            }
        }

        void ClientManagement_contactControl_OkCommandClick(object sender, CommandEventArgs e)
        {
            try
            {
                int contactID = 0;
                bool editflag = true;
                ClientContactInformation clientContactInformation = new ClientContactInformation();

                clientContactInformation = ClientManagement_contactControl.clientContactInformationDataSource;
                clientContactInformation.CreatedBy = base.userID;
                clientContactInformation.TenantID = base.tenantID;

                if (e.CommandName == "edit")
                {
                    clientContactInformation.ContactID = int.Parse(e.CommandArgument.ToString());
                    new ClientBLManager().UpdateContact(clientContactInformation, out contactID);
                }
                else
                {
                    editflag = false;
                    new ClientBLManager().InsertContact(clientContactInformation, out contactID);
                }
                if (contactID > 0)
                {
                    if (!editflag)
                    {
                        base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                            ClientManagement_bottomSuccessMessageLabel,
                            "Contact created successfully");

                        ClientManagement_ConfirmMessageControl.Message = "Contact created successfully <br/> Do you want to create more contacts?";
                        ClientManagement_ConfirmMessageControl.Title = "Contact";
                        ClientManagement_ConfirmMessageControl.Client_ID = clientContactInformation.ClientID;
                        ClientManagement_ConfirmMessageControl.Type = MessageBoxType.DepartmentControl;
                        ClientManagement_confirmMessageModalPopUpUpdatePanel.Update();
                        ClientManagement_confirmMessageModalpPopupExtender.Show();
                    }
                    else
                        base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                                 ClientManagement_bottomSuccessMessageLabel,
                                             "Contact updated successfully");
                }
                else
                {
                    ClientManagement_contactControl.ShowMessage = "Contact email id already exist for this tenant";
                    ClientManagement_contact_ModalpPopupExtender1();
                    // ClientManagement_contactControl.clientContactInformationDataSource = clientContactInformation;
                }
                GetClientManagement();
                GetClientDepartmentManagement();
                GetClientContactManagement();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }


        void ClientManagement_contactControl_SellectedIndexChanged(object sender, EventArgs e)
        {
            AddContact(ClientManagement_contactControl.Client_ID);
        }

        #endregion

        #endregion

        #region Public Methods
        /// <summary>
        /// Clients the management_client_ modalp popup extender.
        /// </summary>
        public void ClientManagement_client_ModalpPopupExtender()
        {
            try
            {
                ClientManagement_clientControl.ClientAttributesDataSouce = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENT);
                //ClientManagement_clientControl.ClientAttributesDataSouce = new ControlUtility().LoadTest();
                ClientManagement_clientModalPopUpUpdatePanel.Update();
                ClientManagement_clientModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        public void ClientManagement_department_ModalpPopupExtender()
        {
            try
            {
                NomenclatureCustomize nomenclatureCustomize = new NomenclatureCustomize();
                nomenclatureCustomize = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT);
                nomenclatureCustomize.ClientName = ClientManagement_viewCliendNameHiddenField.Value;
                ClientManagement_departmentControl.DepartmenAttributesDataSource = nomenclatureCustomize;                
                ClientManagement_departmentControl.ClientInformationListDatasource = new ClientBLManager().GetClientName(base.tenantID);
                ClientManagement_departmentModalPopUpUpdatePanel.Update();
                ClientManagement_departmentModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        public void ClientManagement_contact_ModalpPopupExtender()
        {
            try
            {
                NomenclatureCustomize nomenclatureCustomize = new NomenclatureCustomize();
                nomenclatureCustomize = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS);
                nomenclatureCustomize.ClientName = ClientManagement_viewCliendNameHiddenField.Value;
                ClientManagement_contactControl.ContactAttributesDataSource = nomenclatureCustomize;
                ClientManagement_contactControl.ClientInformationListDatasource = new ClientBLManager().GetClientName(base.tenantID);
                ClientManagement_contactModalPopUpUpdatePanel.Update();
                ClientManagement_contactModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        public string GetHeaderText(string gridHeaderName)
        {
            string test = "123";

            if (gridHeaderName == "ClientID")
                test = "sundar";
            else
                test = "sundar11111";
            return test;
        }


        public void ClientManagement_department_ModalpPopupExtender1()
        {
            try
            {
                NomenclatureCustomize nomenclatureCustomize = new NomenclatureCustomize();
                nomenclatureCustomize = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTDEPARTMENT);
                nomenclatureCustomize.ClientName = ClientManagement_viewCliendNameHiddenField.Value;
                ClientManagement_departmentControl.DepartmenAttributesDataSource = nomenclatureCustomize;
                ClientManagement_departmentModalPopUpUpdatePanel.Update();
                ClientManagement_departmentModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }

        public void ClientManagement_contact_ModalpPopupExtender1()
        {
            try
            {
                NomenclatureCustomize nomenclatureCustomize = new NomenclatureCustomize();
                nomenclatureCustomize = new ControlUtility().LoadAttributeDetails(base.tenantID, Constants.NomenclatureAttribute.NOMENCLATURE_CLIENTCONTACTS);
                nomenclatureCustomize.ClientName = ClientManagement_viewCliendNameHiddenField.Value;
                ClientManagement_contactControl.ContactAttributesDataSource = nomenclatureCustomize;
                ClientManagement_contactModalPopUpUpdatePanel.Update();
                ClientManagement_contactModalpPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                          ClientManagement_bottomErrorMessageLabel,
                          exp.Message);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Clears the controls.
        /// </summary>
        private void clearControls(ClientManagementType clientManagementType)
        {
            switch (clientManagementType)
            {
                case ClientManagementType.Client:
                    ClientManagement_Client_clientNameTextBox.Text = string.Empty;
                    ClientManagement_Client_contactNameTextBox.Text = string.Empty;
                    ClientManagement_Client_userEnableCheckBox.Checked = true;
                    ClientManagement_Client_locationTextBox.Text = string.Empty;
                    ClientManagement_Client_createdByTextBox.Text = string.Empty;
                    ClientManagement_Client_createdByummyAuthorIdHidenField.Value = null;
                    break;
                case ClientManagementType.Departement:
                    ClientManagement_department_clientNameTextBox.Text = string.Empty;
                    ClientManagement_department_contactNameTextBox.Text = string.Empty;
                    ClientManagement_department_userEnableCheckBox.Checked = true;
                    ClientManagement_departmentNameTextBox.Text = string.Empty;
                    ClientManagement_department_createdByTextBox.Text = string.Empty;
                    ClientManagement_department_createdByummyAuthorIdHidenField.Value = null;
                    break;
                case ClientManagementType.Contact:
                    ClientManagement_contact_clientNameTextBox.Text = string.Empty;
                    ClientManagement_contact_contactNameTextBox.Text = string.Empty;
                    ClientManagement_contact_userEnableCheckBox.Checked = true;
                    ClientManagement_contact_departmentTextBox.Text = string.Empty;
                    ClientManagement_contact_createdByummyAuthorIdHidenField.Value = null;
                    break;
            }

            ClientManagement_topSuccessMessageLabel.Text = string.Empty;
            ClientManagement_bottomErrorMessageLabel.Text = string.Empty;
        }
       
        /// <summary>
        /// Gets the client management.
        /// </summary>
        private void GetClientManagement()
        {
            bool clearControlFlag = false;
            int? createdBy = null;
            int? departmentID = null;
            int? contactID = null;
            ClientManagement_stateExpandHiddenField.Value = "0";
            ClientManagement_client_ExpandLinkButton.Text = "Expand All";
            ClientManagement_client_ExpandLinkButton.Visible = true;
            ClientSearchCriteria clientSearchCriteria = new ClientSearchCriteria();
            int total = 0;


            if (!Utility.IsNullOrEmpty(ClientManagement_viewDepartmentIDHiddenField.Value))
            {
                departmentID = int.Parse(ClientManagement_viewDepartmentIDHiddenField.Value);
                clearControlFlag = true;
                ShowBackButton(ClientManagementType.Client);
            }

            if (!Utility.IsNullOrEmpty(ClientManagement_viewContactIDHiddenField.Value))
            {
                contactID = int.Parse(ClientManagement_viewContactIDHiddenField.Value);
                clearControlFlag = true;
                ShowBackButton(ClientManagementType.Client);
            }
            if (clearControlFlag)
                clearControls(ClientManagementType.Client);

            // Assign client name into search criteria, if client name is present
            // in session. This is applicable when navigating by clicking on the
            // 'more..' link in the view client popup
            if (!Utility.IsNullOrEmpty(Session["CLIENT_MANAGEMENT_CLIENT_NAME"]) && 
                !Utility.IsNullOrEmpty(Request.QueryString["viewtype"]) && 
                Request.QueryString["viewtype"] == "C")
            {
                ClientManagement_Client_clientNameTextBox.Text = 
                    Session["CLIENT_MANAGEMENT_CLIENT_NAME"].ToString();
            }

            clientSearchCriteria.ClientName = ClientManagement_Client_clientNameTextBox.Text.Trim();
            clientSearchCriteria.ContactName = ClientManagement_Client_contactNameTextBox.Text.Trim();

            clientSearchCriteria.ClientID = null;
            clientSearchCriteria.DepartmentID = departmentID;
            clientSearchCriteria.ContactID = contactID;

            if (ClientManagement_Client_userEnableCheckBox.Checked)
                createdBy = base.userID;
            else if (!Utility.IsNullOrEmpty(ClientManagement_Client_createdByHiddenField.Value) && 
                ClientManagement_Client_createdByHiddenField.Value != "0")
            {
                createdBy = int.Parse(ClientManagement_Client_createdByHiddenField.Value);
            }
            clientSearchCriteria.CreatedBy = createdBy;
            clientSearchCriteria.Location = ClientManagement_Client_locationTextBox.Text.Trim();
            clientSearchCriteria.TenantID = base.tenantID;

            SortType sortType = (SortType)ViewState["SORT_ORDER"];
            AssignClientSearchField();
            ClientManagement_clientSerachResultsGridView.DataSource =
                new ClientBLManager().GetClientInformation(clientSearchCriteria, int.Parse(ViewState["PAGENUMBER"].ToString()), base.GridPageSize, ViewState["SORT_FIELD"].ToString(), sortType, out total);
            ClientManagement_clientSerachResultsGridView.DataBind();
            ClientManagement_client_bottomPagingNavigator.PageSize = base.GridPageSize;
            ClientManagement_client_bottomPagingNavigator.TotalRecords = total;

            if (total == 0)
            {
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                    ClientManagement_bottomErrorMessageLabel, "No client data found to display");
                ClientManagement_client_ExpandLinkButton.Visible = false;
            }

            ClientManagement_clientSerachResultsUpdatePanel.Update();
            ClientManagement_mainTabContainerUpdatePanel.Update();
        }
        private void GetClientDepartmentManagement()
        {
            bool clearControlFlag = false;
            int? createdBy = null;
            int? viewClientID = null;
            int? contactID = null;
            int? departmentID = null;

            ClientManagement_department_stateExpandHiddenField.Value = "0";
            ClientManagement_department_ExpandLinkButton.Text = "Expand All";
            ClientSearchCriteria clientSearchCriteria = new ClientSearchCriteria();
            int total = 0;


            if (ClientManagement_department_userEnableCheckBox.Checked)
                createdBy = base.userID;
            else if (ClientManagement_department_createdByHiddenField.Value != "0")
            {
                createdBy = int.Parse(ClientManagement_department_createdByHiddenField.Value);
            }
            clientSearchCriteria.CreatedBy = createdBy;
            clientSearchCriteria.Location = ClientManagement_Client_locationTextBox.Text.Trim();
            clientSearchCriteria.TenantID = base.tenantID;

            if (!Utility.IsNullOrEmpty(ClientManagement_viewClientIDHiddenField.Value))
            {
                viewClientID = int.Parse(ClientManagement_viewClientIDHiddenField.Value.ToString());
                clearControlFlag = true;
                ShowBackButton(ClientManagementType.Departement);
            }
            clientSearchCriteria.ClientID = viewClientID;

            if (!Utility.IsNullOrEmpty(ClientManagement_contactDepartmentIDHiddenField.Value))
            {
                clearControlFlag = true;
                departmentID = int.Parse(ClientManagement_contactDepartmentIDHiddenField.Value.ToString());
                ShowBackButton(ClientManagementType.Departement);
            }
            else if (!Utility.IsNullOrEmpty(ClientManagement_clientDepartmentIDHiddenField.Value))
            {
                clearControlFlag = true;
                departmentID = int.Parse(ClientManagement_clientDepartmentIDHiddenField.Value.ToString());
                ShowBackButton(ClientManagementType.Departement);
            }

            if (clearControlFlag)
                clearControls(ClientManagementType.Departement);

            clientSearchCriteria.DepartmentID = departmentID;

            clientSearchCriteria.DeparmentName = ClientManagement_departmentNameTextBox.Text.Trim();
            clientSearchCriteria.ClientName = ClientManagement_department_clientNameTextBox.Text.Trim();
            clientSearchCriteria.ContactName = ClientManagement_department_contactNameTextBox.Text.Trim();


            if (!Utility.IsNullOrEmpty(ClientManagement_viewContactIDHiddenField.Value))
                contactID = int.Parse(ClientManagement_viewContactIDHiddenField.Value.ToString());
            clientSearchCriteria.ContactID = contactID;
            AssignDepartmentSearchField();
            ClientManagement_department_clientSerachResultsGridView.DataSource =
                new ClientBLManager().GetClientDepartmentInformation(clientSearchCriteria,
                int.Parse(ViewState["DEPARTMENT_PAGENUMBER"].ToString()), base.GridPageSize,
                ViewState["DEPARTMENT_SORT_FIELD"].ToString(), (SortType)ViewState["DEPARTMENT_SORT_ORDER"], out total);

            ClientManagement_department_clientSerachResultsGridView.DataBind();
            ClientManagement_department_bottomPagingNavigator.PageSize = base.GridPageSize;
            ClientManagement_department_bottomPagingNavigator.TotalRecords = total;

            if (total == 0)
            {
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
                ClientManagement_bottomErrorMessageLabel, "No department data found to display");
                ClientManagement_department_ExpandLinkButton.Visible = false;
            }

            ClientManagement_department_clientSerachResultsUpdatePanel.Update();
            ClientManagement_mainTabContainerUpdatePanel.Update();

        }
        private void GetClientContactManagement()
        {
            bool clearControlFlag = false;
            int? departmentID = null;
            int? viewClientID = null;
            int? createdBy = null;
            ClientManagement_contact_stateExpandHiddenField.Value = "0";
            ClientManagement_contact_ExpandLinkButton.Text = "Expand All";
            ClientSearchCriteria clientSearchCriteria = new ClientSearchCriteria();
            int total = 0;
            //clientSearchCriteria.DeparmentName = ClientManagement_contact_departmentTextBoxText.Trim();

            if (ClientManagement_contact_userEnableCheckBox.Checked)
                createdBy = base.userID;
            else if (ClientManagement_contact_createdByHiddenField.Value != "0")
            {
                createdBy = int.Parse(ClientManagement_contact_createdByHiddenField.Value);
            }
            clientSearchCriteria.CreatedBy = createdBy;

            clientSearchCriteria.TenantID = base.tenantID;

            if (!Utility.IsNullOrEmpty(ClientManagement_viewClientIDHiddenField.Value))
            {
                clearControlFlag = true;
                viewClientID = int.Parse(ClientManagement_viewClientIDHiddenField.Value.ToString());
                ShowBackButton(ClientManagementType.Contact);
            }

            clientSearchCriteria.ClientID = viewClientID;

            if (!Utility.IsNullOrEmpty(ClientManagement_viewDepartmentIDHiddenField.Value))
            {
                clearControlFlag = true;
                departmentID = int.Parse(ClientManagement_viewDepartmentIDHiddenField.Value.ToString());
                ShowBackButton(ClientManagementType.Contact);
            }

            if (clearControlFlag)
                clearControls(ClientManagementType.Contact);
            clientSearchCriteria.ClientName = ClientManagement_contact_clientNameTextBox.Text.Trim();
            clientSearchCriteria.ContactName = ClientManagement_contact_contactNameTextBox.Text.Trim();
            clientSearchCriteria.DepartmentID = departmentID;

            AssignContactSearchField();
            ClientManagement_contact_SerachResultsGridView.DataSource =
                new ClientBLManager().GetClientContactInformation(clientSearchCriteria,
                int.Parse(ViewState["CONTACT_PAGENUMBER"].ToString()), base.GridPageSize,
                ViewState["CONTACT_SORT_FIELD"].ToString(), (SortType)ViewState["CONTACT_SORT_ORDER"], out total);

            ClientManagement_contact_SerachResultsGridView.DataBind();
            ClientManagement_contact_bottomPagingNavigator.PageSize = base.GridPageSize;
            ClientManagement_contact_bottomPagingNavigator.TotalRecords = total;

            if (total == 0)
            {
                base.ShowMessage(ClientManagement_topErrorMessageLabel,
               ClientManagement_bottomErrorMessageLabel, "No contact data found to display");
                ClientManagement_contact_ExpandLinkButton.Visible = false;
            }

            ClientManagement_contact_clientSerachResultsUpdatePanel.Update();
            ClientManagement_mainTabContainerUpdatePanel.Update();
        }
       
        private void AssignClientSearchField()
        {
            ClientManagementState clientManagementState = new ClientManagementState();
            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT]))
                clientManagementState = (ClientManagementState)Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT];

            clientManagementState.ClientClientName = ClientManagement_Client_clientNameTextBox.Text.Trim();
            clientManagementState.ClientContactName = ClientManagement_Client_contactNameTextBox.Text.Trim(); ;
            clientManagementState.ClientCreatedBy = ClientManagement_Client_createdByTextBox.Text.Trim();
            clientManagementState.ClientCreatedHiddenField = ClientManagement_Client_createdByHiddenField.Value;
            clientManagementState.ClientLocation = ClientManagement_Client_locationTextBox.Text.Trim();
            clientManagementState.ClientOrderBy = ViewState["SORT_FIELD"].ToString();
            clientManagementState.ClientOrderDirection = (SortType)ViewState["SORT_ORDER"];
            clientManagementState.ClientPageNo = ViewState["PAGENUMBER"].ToString();
            clientManagementState.ClientUserCheckbox = ClientManagement_Client_userEnableCheckBox.Checked;
            clientManagementState.ClientContactId = ClientManagement_viewContactIDHiddenField.Value;
            clientManagementState.ClientDeparmentId = ClientManagement_viewDepartmentIDHiddenField.Value;

            Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT] = clientManagementState;
        }
        private void AssignDepartmentSearchField()
        {
            ClientManagementState clientManagementState = new ClientManagementState();
            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT]))
                clientManagementState = (ClientManagementState)Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT];

            clientManagementState.DepartmentClientName = ClientManagement_department_clientNameTextBox.Text.Trim();
            clientManagementState.DepartmentContactName = ClientManagement_department_contactNameTextBox.Text.Trim(); ;
            clientManagementState.DepartmentCreatedBy = ClientManagement_department_createdByTextBox.Text.Trim();
            clientManagementState.DepartmentCreatedHiddenField = ClientManagement_department_createdByHiddenField.Value;
            clientManagementState.DepartmentName = ClientManagement_departmentNameTextBox.Text.Trim();
            clientManagementState.DepartmentOrderBy = ViewState["DEPARTMENT_SORT_FIELD"].ToString();
            clientManagementState.DepartmentDirection = (SortType)ViewState["DEPARTMENT_SORT_ORDER"];
            clientManagementState.DepartmentPageNo = ViewState["DEPARTMENT_PAGENUMBER"].ToString();
            clientManagementState.DepartmentUserCheckbox = ClientManagement_Client_userEnableCheckBox.Checked;
            clientManagementState.DepartmentContactId = ClientManagement_contactDepartmentIDHiddenField.Value;
            clientManagementState.DeparmentClientId = ClientManagement_department_createdByHiddenField.Value;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT] = clientManagementState;
        }
        private void AssignContactSearchField()
        {
            ClientManagementState clientManagementState = new ClientManagementState();
            if (!Utility.IsNullOrEmpty(Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT]))
                clientManagementState = (ClientManagementState)Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT];

            clientManagementState.ContactClientName = ClientManagement_department_clientNameTextBox.Text.Trim();
            clientManagementState.ContactContactName = ClientManagement_department_contactNameTextBox.Text.Trim(); ;
            clientManagementState.ContactCreatedBy = ClientManagement_department_createdByTextBox.Text.Trim();
            clientManagementState.ContactCreatedHiddenField = ClientManagement_department_createdByHiddenField.Value;
            clientManagementState.ContactDepartment = ClientManagement_departmentNameTextBox.Text.Trim();
            clientManagementState.ContactOrderBy = ViewState["DEPARTMENT_SORT_FIELD"].ToString();
            clientManagementState.ContactOrderDirection = (SortType)ViewState["DEPARTMENT_SORT_ORDER"];
            clientManagementState.ContactPageNo = ViewState["DEPARTMENT_PAGENUMBER"].ToString();
            clientManagementState.ContactUserCheckbox = ClientManagement_Client_userEnableCheckBox.Checked;
            clientManagementState.ContactClientId = ClientManagement_viewClientIDHiddenField.Value;
            clientManagementState.ContactDepartmentId = ClientManagement_viewClientIDHiddenField.Value;
            Session[Constants.SearchCriteriaSessionKey.SEARCH_CLIENT_MANAGEMENT] = clientManagementState;
        }

        private void AddContact(int clientID)
        {
            ClientManagement_contact_ModalpPopupExtender();
            ClientManagement_contactControl.clientContactInformationDataSource = null;
            ClientInformation clientInformation = new ClientInformation();
            clientInformation = new ClientBLManager().GetClientInformationByClientId(clientID);
            if (!Utility.IsNullOrEmpty(ClientManagement_departIDHiddenField.Value))
                clientInformation.sellectedDepartmentID = int.Parse(ClientManagement_departIDHiddenField.Value.ToString());
            ClientManagement_contactControl.ClientDataSource = clientInformation;
        }
        private void AddDepartmentContact(int departmentID)
        {
            ClientManagement_contact_ModalpPopupExtender();
            ClientManagement_contactControl.clientContactInformationDataSource = null;
            Department department = new Department();
            department = new ClientBLManager().GetDepartment(departmentID);
            department.DepartmentList = new ClientBLManager().GetDepartments(department.ClientID);
            department.DepartmentIsSelected = departmentID.ToString();
            ClientManagement_contactControl.DepartmentListDataSource = department;

            //ClientInformation clientInformation = new ClientInformation();
            //clientInformation = new ClientBLManager().GetClientInformationByClientId(department.ClientID);
            //clientInformation.sellectedDepartmentID = department.DepartmentID;
            //ClientManagement_contactControl.ClientDataSource = clientInformation;
        }
        private void AddDepartement(int clientID)
        {
            ClientManagement_department_ModalpPopupExtender();
            ClientManagement_departmentControl.DepartmentDataSource = null;
            ClientManagement_departmentControl.ClientDataSource = new ClientBLManager().GetClientInformationByClientId(clientID);
        }

        private void ViewClientPositionProfile(int clientID)
        {
            Response.Redirect("~/PositionProfile/SearchPositionProfile.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING + "=" + clientID + "&m=1&s=1", false);
        }
        private void ViewClientDepartment(int clientID)
        {
            clearControls(ClientManagementType.Departement);
            ShowSelectedDepartmentHiddenField();
            ClientManagement_viewClientIDHiddenField.Value = clientID.ToString();
            ShowBackButton(ClientManagementType.Departement);

            ViewState["DEPARTMENT_SORT_ORDER"] = SortType.Ascending;
            ViewState["DEPARTMENT_PAGENUMBER"] = "1";
            ViewState["DEPARTMENT_SORT_FIELD"] = "DEPARTMENT_NAME";

            GetClientDepartmentManagement();
            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_department_TabPanel;
            ClientManagement_mainTabContainerUpdatePanel.Update();
        }
        private void ViewClientContact(int clientID)
        {
            ShowBackButton(ClientManagementType.Contact);
            ClientManagement_viewClientIDHiddenField.Value = clientID.ToString();

            ViewState["CONTACT_SORT_ORDER"] = SortType.Ascending;
            ViewState["CONTACT_PAGENUMBER"] = "1";
            ViewState["CONTACT_SORT_FIELD"] = "CONTACT_NAME";

            GetClientContactManagement();
            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_contact_TabPanel;
            ClientManagement_mainTabContainerUpdatePanel.Update();
        }
        private void ViewDepartmentContact(int departmentID)
        {

           
            ShowBackButton(ClientManagementType.Contact);
            ClientManagement_viewDepartmentIDHiddenField.Value = departmentID.ToString();
           
            ViewState["CONTACT_SORT_ORDER"] = SortType.Ascending;
            ViewState["CONTACT_PAGENUMBER"] = "1";
            ViewState["CONTACT_SORT_FIELD"] = "CONTACT_NAME";

            GetClientContactManagement();
            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_contact_TabPanel;
            ClientManagement_mainTabContainerUpdatePanel.Update();
        }
        private void ViewDepartmentPositionProfile(int departmentID)
        {
            Response.Redirect("~/PositionProfile/SearchPositionProfile.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING + "=" + departmentID + "&m=1&s=1", false);
        }
        private void ViewContactDepartment(int contactID)
        {
            clearControls(ClientManagementType.Departement);
            ShowSelectedDepartmentHiddenField();
            ShowBackButton(ClientManagementType.Departement);
            ClientManagement_viewContactIDHiddenField.Value = contactID.ToString();

            ViewState["DEPARTMENT_SORT_ORDER"] = SortType.Ascending;
            ViewState["DEPARTMENT_PAGENUMBER"] = "1";
            ViewState["DEPARTMENT_SORT_FIELD"] = "DEPARTMENT_NAME";

            GetClientDepartmentManagement();
            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_department_TabPanel;
            ClientManagement_mainTabContainerUpdatePanel.Update();
        }
        private void ViewDepartment()
        {
            ShowBackButton(ClientManagementType.Departement);

            ViewState["DEPARTMENT_SORT_ORDER"] = SortType.Ascending;
            ViewState["DEPARTMENT_PAGENUMBER"] = "1";
            ViewState["DEPARTMENT_SORT_FIELD"] = "DEPARTMENT_NAME";

            GetClientDepartmentManagement();
            ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_department_TabPanel;
            ClientManagement_mainTabContainerUpdatePanel.Update();
        }
        private void ViewContactPositionProfile(int contactID)
        {
            Response.Redirect("~/PositionProfile/SearchPositionProfile.aspx?parentpage=" + Constants.ParentPage.CLIENT_MANAGEMENT + "&" + Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING + "=" + contactID + "&m=1&s=1", false);
        }

        private void ExpandClient()
        {
            if ((!Utility.IsNullOrEmpty(ClientManagement_viewDepartmentIDHiddenField.Value) || !Utility.IsNullOrEmpty(ClientManagement_viewContactIDHiddenField.Value)) && ClientManagement_mainTabContainer.ActiveTab == ClientManagement_clinetTabPanel)
            {
                string javaScript1 = "<script language=JavaScript>javascript:return ExpORCollapseRows('" + ClientManagement_client_ExpandLinkButton.ClientID + "','" + ClientManagement_stateExpandHiddenField.ClientID + "','" + ClientManagement_clientSerachResultsDiv.ClientID + "','ClientManagement_detailsDiv'));" + "</script>";
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ShowCallScript", javaScript, false);
                //Page.RegisterStartupScript(null, javaScript1);
                ClientScript.RegisterStartupScript(GetType(), "", javaScript1);

            }
        }
        private void ShowSelectedDepartmentHiddenField()
        {
            ClientManagement_contactDepartmentIDHiddenField.Value = null;
            ClientManagement_clientDepartmentIDHiddenField.Value = null;
        }
        private void ShowBackButton(ClientManagementType clientManagementType)
        {
            ClientManagement_departmentBackLinkButton.Visible = false;
            ClientManagement_contactBackLinkButton.Visible = false;
            ClientManagement_Client_backLinktButton.Visible = false;
            ClientManagement_departmentBackLinkSeperator.Visible = false;
            ClientManagement_Client_backLinkSeperator.Visible = false;
            ClientManagement_contactBackLinkSeperator.Visible = false;
            switch (clientManagementType)
            {
                case ClientManagementType.All:
                    break;
                case ClientManagementType.Client:
                    ClientManagement_Client_backLinktButton.Visible = true;
                    ClientManagement_Client_backLinkSeperator.Visible = true;
                    break;
                case ClientManagementType.Departement:
                    ClientManagement_departmentBackLinkButton.Visible = true;
                    ClientManagement_departmentBackLinkSeperator.Visible = true;
                    break;
                case ClientManagementType.Contact:
                    ClientManagement_contactBackLinkSeperator.Visible = true;
                    ClientManagement_contactBackLinkButton.Visible = true;
                    break;
            }

        }

        private void BindNomenclatureLable()
        {
            ClientManagement_Client_clientNameHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientName;
            ClientManagement_Client_contactNameHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ContactName;
            ClientManagement_Client_userEnableCheckBox.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.MyRecords;
            ClientManagement_Client_locationHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.Location;
            ClientManagement_Client_createdByHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.CreatedBy;

            ClientManagement_department_clientNameHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientName;
            ClientManagement_department_contactNameHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ContactName;
            ClientManagement_department_userEnableCheckBox.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.MyRecords;
            ClientManagement_department_departmentNameHeadLabel.Text = base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes.DepartmentName;
            ClientManagement_department_createdByHeadLabel.Text = base.NomenclatureAllAttributes.DepartmentNomenClatureAttrubutes.CreatedBy;


            ClientManagement_contact_clientNameHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ClientName;
            ClientManagement_contact_contactNameHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.ContactName;
            ClientManagement_contact_departmentHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.DepartmentName;
            ClientManagement_contact_createdByHeadLabel.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.CreatedBy;
            ClientManagement_contact_userEnableCheckBox.Text = base.NomenclatureAllAttributes.ClientNomenClatureAttrubutes.MyRecords;
        }
        #endregion

        #region Protected Overriden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

            Master.SetPageCaption("Client Management");

            if (!Utility.IsNullOrEmpty(Request.QueryString.Get(Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING)))
                ClientManagement_mainTabContainer.ActiveTab = ClientManagement_clinetTabPanel;
            else if (!Utility.IsNullOrEmpty(Request.QueryString.Get(Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING)))
                ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_contact_TabPanel;
            else if (!Utility.IsNullOrEmpty(Request.QueryString.Get(Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING)))
                ClientManagement_mainTabContainer.ActiveTab = ClientManagement_client_department_TabPanel;

            ClientManagement_Client_createClientButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT;
            ClientManagement_department_createClientButton.CommandName = Constants.ClientManagementConstants.ADD_DEPARTMENT;
            ClientManagement_contact_createClientButton.CommandName = Constants.ClientManagementConstants.ADD_CONTACT;

            ClientManagement_topSuccessMessageLabel.Text = string.Empty;
            ClientManagement_topErrorMessageLabel.Text = string.Empty;
            ClientManagement_bottomErrorMessageLabel.Text = string.Empty;
            ClientManagement_bottomSuccessMessageLabel.Text = string.Empty;

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "CLIENT_NAME";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                ViewState["PAGENUMBER"] = "1";

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["DEPARTMENT_SORT_ORDER"]))
                ViewState["DEPARTMENT_SORT_ORDER"] = SortType.Ascending;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["DEPARTMENT_SORT_FIELD"]))
                ViewState["DEPARTMENT_SORT_FIELD"] = "CLIENT_NAME";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["DEPARTMENT_PAGENUMBER"]))
                ViewState["DEPARTMENT_PAGENUMBER"] = "1";

            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["CONTACT_SORT_ORDER"]))
                ViewState["CONTACT_SORT_ORDER"] = SortType.Ascending;
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["CONTACT_SORT_FIELD"]))
                ViewState["CONTACT_SORT_FIELD"] = "CLIENT_NAME";
            if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["CONTACT_PAGENUMBER"]))
                ViewState["CONTACT_PAGENUMBER"] = "1";

            if (ClientManagement_Client_userEnableCheckBox.Checked)
            {                
                ClientManagement_createdByDiv.Style.Add("display", "none");
            }
            else
            {
                ClientManagement_createdByDiv.Style.Add("display", "block");
            }

            if (ClientManagement_department_userEnableCheckBox.Checked)
            {
                ClientManagement_department_createdByDiv.Style.Add("display", "none");
            }
            else
            {
                ClientManagement_department_createdByDiv.Style.Add("display", "block");
            }
            if (ClientManagement_contact_userEnableCheckBox.Checked)
            {
                ClientManagement_contact_createdByDiv.Style.Add("display", "none");
            }
            else
            {
                ClientManagement_contact_createdByDiv.Style.Add("display", "block");
            }

            ClientManagement_Client_userEnableCheckBox.Attributes.Add("onclick",
                "javascript:ShowCreatedByDiv('" + ClientManagement_Client_userEnableCheckBox.ClientID + "','" + ClientManagement_createdByDiv.ClientID + "','" + ClientManagement_Client_createdByTextBox.ClientID + "');");

            ClientManagement_department_userEnableCheckBox.Attributes.Add("onclick",
                "javascript:ShowCreatedByDiv('" + ClientManagement_department_userEnableCheckBox.ClientID + "','" + ClientManagement_department_createdByDiv.ClientID + "','" + ClientManagement_department_createdByTextBox.ClientID + "');");

            ClientManagement_contact_userEnableCheckBox.Attributes.Add("onclick",
                "javascript:ShowCreatedByDiv('" + ClientManagement_contact_userEnableCheckBox.ClientID + "','" + ClientManagement_contact_createdByDiv.ClientID + "','" + ClientManagement_contact_createdByTextBox.ClientID + "');");

            ClientManagement_client_bottomPagingNavigator.PageNumberClick += new PageNavigator.PageNumberClickEventHandler(ClientManagement_client_bottomPagingNavigator_PageNumberClick);

            ClientManagement_Client_createdByImageButton.Attributes.Add("onclick", "return LoadUserForClientManagement('"
                 + ClientManagement_Client_createdByummyAuthorIdHidenField.ClientID + "','"
                 + ClientManagement_Client_createdByHiddenField.ClientID + "','"
                 + ClientManagement_Client_createdByTextBox.ClientID + "','QA')");

            ClientManagement_department_createdByImageButton.Attributes.Add("onclick", "return LoadUserForClientManagement('"
               + ClientManagement_department_createdByummyAuthorIdHidenField.ClientID + "','"
               + ClientManagement_department_createdByHiddenField.ClientID + "','"
               + ClientManagement_department_createdByTextBox.ClientID + "','QA')");

            ClientManagement_contact_createdByImageButton.Attributes.Add("onclick", "return LoadUserForClientManagement('"
              + ClientManagement_contact_createdByummyAuthorIdHidenField.ClientID + "','"
              + ClientManagement_contact_createdByHiddenField.ClientID + "','"
              + ClientManagement_contact_createdByTextBox.ClientID + "','QA')");


            ClientManagement_clientControl.OkCommandClick += new ClientControl.Button_CommandClick(ClientManagement_clientControl_OkCommandClick);
            ClientManagement_departmentControl.OkCommandClick += new DepartmentControl.Button_CommandClick(ClientManagement_departmentControl_OkCommandClick);
            ClientManagement_contactControl.OkCommandClick += new ContactControl.Button_CommandClick(ClientManagement_contactControl_OkCommandClick);

            ClientManagement_ConfirmMessageControl.CommandEventAdd_Click += new ConfirmMsgControl.Button_CommandClick(ClientManagement_ConfirmMessageControl_CommandEventAdd_Click);
            ClientManagement_department_bottomPagingNavigator.PageNumberClick += new PageNavigator.PageNumberClickEventHandler(ClientManagement_department_bottomPagingNavigator_PageNumberClick);
            ClientManagement_contact_bottomPagingNavigator.PageNumberClick += new PageNavigator.PageNumberClickEventHandler(ClientManagement_contact_bottomPagingNavigator_PageNumberClick);

            ClientManagement_client_ExpandLinkButton.Attributes.Add("onclick", "javascript:return ExpORCollapseRows('" + ClientManagement_client_ExpandLinkButton.ClientID + "','" + ClientManagement_stateExpandHiddenField.ClientID + "','" + ClientManagement_clientSerachResultsDiv.ClientID + "','ClientManagement_detailsDiv')");
            ClientManagement_department_ExpandLinkButton.Attributes.Add("onclick", "javascript:return ExpORCollapseRows('" + ClientManagement_department_ExpandLinkButton.ClientID + "','" + ClientManagement_department_stateExpandHiddenField.ClientID + "','" + ClientManagement_department_clientSerachResultsDiv.ClientID + "','ClientManagement_department_detailsDiv')");
            ClientManagement_contact_ExpandLinkButton.Attributes.Add("onclick", "javascript:return ExpORCollapseRows('" + ClientManagement_contact_ExpandLinkButton.ClientID + "','" + ClientManagement_contact_stateExpandHiddenField.ClientID + "','" + ClientManagement_contact_clientSerachResultsDiv.ClientID + "','ClientManagement_contact_detailsDiv')");

            ClientManagement_departmentControl.SellectedIndexChanged += new DepartmentControl.DropDown_sellectedIndexChanged(ClientManagement_departmentControl_SellectedIndexChanged);
            ClientManagement_contactControl.SellectedIndexChanged += new ContactControl.DropDown_sellectedIndexChanged(ClientManagement_contactControl_SellectedIndexChanged);
        }

        #endregion Protected Overriden Methods

        #region Delete Method
        private void GetClientDetails(short clientID)
        {
            ClientContactInformation clientInfo = new ClientContactInformation();

            clientInfo.NoOfContacts = new ClientBLManager().GetClientContactDetails(clientID);

            if (clientInfo.NoOfContacts > 0)
            {
                ClientManagement_deleteBusinessConfirmMsgControl.Message = "There are Job description associated with this client.Choose the following delete options";
                RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
                RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");
                ClientManagement_confirmContactDeleteInSpecificRadioButton.Text = "Delete client but retain Job descriptions";
                ClientManagement_confirmContactDeleteAllRadioButton.Text = "Delete client and associated Job descriptions";
                ClientManagement_deleteBusinessConfirmMsgControl.Type = MessageBoxType.DeleteContact;
                ClientManagement_deleteBusinessConfirmMsgControl.Title = "Warning";
                ClientManagement_deleteFormModalPopupExtender.Show();
            }
            else
            {
                ClientManagement_deleteContactConfirmMsgControl.Message = "Are you sure to delete the client?";
                ClientManagement_deleteContactConfirmMsgControl.Type = MessageBoxType.DeleteContact;
                ClientManagement_deleteContactConfirmMsgControl.Title = "Delete Client";
                ClientManagement_deleteContactModalPopupExtender.Show();
            }
        }

        private void GetDepartmentDetails(short departmentID)
        {
            ClientContactInformation departmetInfo = new ClientContactInformation();

            departmetInfo.NoOfDepartments = new ClientBLManager().GetDepartmentCOntactsDetails(departmentID);

            if (departmetInfo.NoOfDepartments > 0)
            {
                ClientManagement_deleteBusinessConfirmMsgControl.Message = "There are associated client contacts present.Choose your delete option";
                RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
                RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");
                ClientManagement_confirmContactDeleteInSpecificRadioButton.Text = "Delete client department but retain contacts";
                ClientManagement_confirmContactDeleteAllRadioButton.Text = "Delete client department and associated contacts";
                ClientManagement_deleteBusinessConfirmMsgControl.Type = MessageBoxType.DeleteContact;
                ClientManagement_deleteBusinessConfirmMsgControl.Title = "Warning";
                ClientManagement_deleteFormModalPopupExtender.Show();
            }
            else
            {
                ClientManagement_deleteContactConfirmMsgControl.Message = "Are you sure to delete the department?";
                ClientManagement_deleteContactConfirmMsgControl.Type = MessageBoxType.DeleteContact;
                ClientManagement_deleteContactConfirmMsgControl.Title = "Delete Department";
                ClientManagement_deleteContactModalPopupExtender.Show();
            }
        }

        private void GetContactDetails(int contactID)
        {

            ClientContactInformation ContactInfo = new ClientContactInformation();

            ContactInfo.NoOfContacts = new ClientBLManager().GetClientContactProstionProfileDetails(contactID);

            if (ContactInfo.NoOfContacts > 0)
            {
                ClientManagement_deleteBusinessConfirmMsgControl.Message = "There are Job descriptions associated with this contact. Choose your delete option";
                RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
                RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");
                ClientManagement_confirmContactDeleteInSpecificRadioButton.Text = "Delete contact but retain Job descriptions";
                ClientManagement_confirmContactDeleteAllRadioButton.Text = "Delete contact and associated Job descriptions";
                ClientManagement_deleteBusinessConfirmMsgControl.Type = MessageBoxType.DeleteContact;
                ClientManagement_deleteBusinessConfirmMsgControl.Title = "Warning";
                ClientManagement_deleteFormModalPopupExtender.Show();
            }
            else
            {
                ClientManagement_deleteContactConfirmMsgControl.Message = "Are you sure to delete the contact?";
                ClientManagement_deleteContactConfirmMsgControl.Type = MessageBoxType.DeleteContact;
                ClientManagement_deleteContactConfirmMsgControl.Title = "Delete Contact";
                ClientManagement_deleteContactModalPopupExtender.Show();
            }

        }

        protected void ClientManagement_deleteConfirmMsgControl_okClick
       (object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CURRENTTAB"].ToString() == Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING)
                    DeleteContactDetails();
                if (ViewState["CURRENTTAB"].ToString() == Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING)
                    DeleteDepartmentDetails();
                if (ViewState["CURRENTTAB"].ToString() == Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING)
                    DeleteClientDetails();
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void ClientManagement_deleteConfirmMsgControl_cancelClick
        (object sender, EventArgs e)
        {

        }
        protected void ClientManagement_deleteContactConfirmMsgControl_okClick
       (object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CURRENTTAB"].ToString() == Constants.ClientManagementConstants.CONTACT_ID_QUERYSTRING)
                    DeleteContact();
                if (ViewState["CURRENTTAB"].ToString() == Constants.ClientManagementConstants.DEPARTMENT_ID_QUERYSTRING)
                    DeleteDepartment();
                if (ViewState["CURRENTTAB"].ToString() == Constants.ClientManagementConstants.CLIENT_ID_QUERYSTRING)
                    DeleteClient();
            }
            catch (Exception)
            {

                throw;
            }

        }
        protected void ClientManagement_deleteContactConfirmMsgControl_cancelClick
         (object sender, EventArgs e)
        {

        }

        private void DeleteClientDetails()
        {
            int deleteCondition = 0;
            ClientContactInformation ContactInfo = new ClientContactInformation();
            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");

            if (ClientManagement_confirmContactDeleteInSpecificRadioButton.Checked)
                deleteCondition = 1;

            ContactInfo.NoOfContacts = new ClientBLManager().DeleteClientPositionProfiletDetails
                (Convert.ToInt16(ClientManagement_viewClientIDHiddenField.Value), deleteCondition);

            if (ContactInfo.NoOfContacts > 0)
            {
                base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                ClientManagement_bottomSuccessMessageLabel, "Client deleted successfully");
            }
            GetClientManagement();


        }

        private void DeleteDepartmentDetails()
        {

            int deleteCondition = 0;
            ClientContactInformation ContactInfo = new ClientContactInformation();
            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");

            if (ClientManagement_confirmContactDeleteInSpecificRadioButton.Checked)
                deleteCondition = 1;

            ContactInfo.NoOfDepartments = new ClientBLManager().DeleteDepartmentClientContactDetails
                (Convert.ToInt16(ClientManagement_viewDepartmentIDHiddenField.Value), deleteCondition);
            ShowSelectedDepartmentHiddenField();
            GetClientDepartmentManagement();

            if (ContactInfo.NoOfDepartments > 0)
            {
                base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                ClientManagement_bottomSuccessMessageLabel, "Department deleted successfully");
            }

        }

        private void DeleteContactDetails()
        {

            int deleteCondition = 0;
            ClientContactInformation ContactInfo = new ClientContactInformation();
            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");

            if (ClientManagement_confirmContactDeleteInSpecificRadioButton.Checked)
                deleteCondition = 1;

            ContactInfo.NoOfContacts = new ClientBLManager().DeleteClientContactProstionProfileDetails
                (Convert.ToInt16(ClientManagement_viewContactIDHiddenField.Value), deleteCondition);

            GetClientContactManagement();
            if (ContactInfo.NoOfContacts > 0)
            {
                base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                ClientManagement_bottomSuccessMessageLabel, "Contact deleted successfully");
            }

        }

        private void DeleteClient()
        {
            int deleteCondition = 0;
            ClientContactInformation ContactInfo = new ClientContactInformation();
            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");

            deleteCondition = 2;

            ContactInfo.NoOfContacts = new ClientBLManager().DeleteClientPositionProfiletDetails
                (Convert.ToInt16(ClientManagement_viewClientIDHiddenField.Value), deleteCondition);

            if (ContactInfo.NoOfContacts > 0)
            {
                base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                ClientManagement_bottomSuccessMessageLabel, "Client deleted successfully");
            }

            GetClientManagement();


        }

        private void DeleteContact()
        {
            int deleteCondition = 0;
            ClientContactInformation ContactInfo = new ClientContactInformation();
            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");

            deleteCondition = 2;

            ContactInfo.NoOfContacts = new ClientBLManager().DeleteClientContactProstionProfileDetails
                (Convert.ToInt16(ClientManagement_viewContactIDHiddenField.Value), deleteCondition);

            GetClientContactManagement();
            if (ContactInfo.NoOfContacts > 0)
            {
                base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                ClientManagement_bottomSuccessMessageLabel, "Contact deleted successfully");
            }
        }

        private void DeleteDepartment()
        {
            int deleteCondition = 0;
            ClientContactInformation ContactInfo = new ClientContactInformation();
            RadioButton ClientManagement_confirmContactDeleteAllRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteAllRadioButton");
            RadioButton ClientManagement_confirmContactDeleteInSpecificRadioButton = (RadioButton)ClientManagement_deleteBusinessConfirmMsgControl.FindControl("ClientManagement_confirmContactDeleteInSpecificRadioButton");

            deleteCondition = 2;

            ContactInfo.NoOfDepartments = new ClientBLManager().DeleteDepartmentClientContactDetails
                (Convert.ToInt16(ClientManagement_viewDepartmentIDHiddenField.Value), deleteCondition);
            ShowSelectedDepartmentHiddenField();
            GetClientDepartmentManagement();
            if (ContactInfo.NoOfContacts > 0)
            {
                base.ShowMessage(ClientManagement_topSuccessMessageLabel,
                ClientManagement_bottomSuccessMessageLabel, "Department deleted successfully");
            }
        }

        #endregion
    }
}