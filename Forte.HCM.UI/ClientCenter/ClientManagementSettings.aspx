﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PositionProfileMaster.Master"
    CodeBehind="ClientManagementSettings.aspx.cs" Inherits="Forte.HCM.UI.ClientCenter.ClientManagementSettings"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/MasterPages/PositionProfileMaster.Master" %>
<asp:Content ID="ClientManagementSettings_bodyContent" runat="server" ContentPlaceHolderID="PositionProfileMaster_body">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="ClientManagementSettings_headerLiteral" runat="server" Text="Settings"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="4" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="ClientManagementSettings_topSaveButton" runat="server" Text="Save"
                                            SkinID="sknButtonId" OnClick="ClientManagementSettings_saveButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ClientManagementSettings_topResetLinkButton" runat="server" Text="Reset"
                                            OnClick="ClientManagementSettings_resetLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                    <td align="center" class="link_button">
                                        |
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ClientManagementSettings_topCancelLinkButton" runat="server"
                                            Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="ClientManagementSettings_messageUpdatePanel" runat="server"
                    UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="ClientManagementSettings_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ClientManagementSettings_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ClientManagementSettings_topSaveButton" />
                        <asp:AsyncPostBackTrigger ControlID="ClientManagementSettings_bottomSaveButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <tr>
            <td class="panel_inner_body_bg">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="header_bg">
                            <asp:Literal ID="ClientManagementSettings_generalSettingsLiteral" runat="server"
                                Text="General Settings"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab_body_bg">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="left">
                                        <div style="float: left; padding-right: 5px; padding-top: 6px">
                                            <asp:Label ID="ClientManagementSettings_generalSettingsDeleteClientLabel" runat="server"
                                                Text="Delete Client" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </div>
                                        <div style="float: left; padding-top: 6px">
                                            <asp:ImageButton ID="ClientManagementSettings_generalSettingsDeleteHelpImageButton"
                                                SkinID="sknHelpImageButton" runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                ToolTip="Select the option here to set whether the Delete Client button will be active for all users or only for those users with the Tenant Admin role" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <div style="float: left; padding-left: 12px;">
                                            <asp:RadioButtonList ID="ClientManagementSettings_generalSettingsDeleteRadioButtonList"
                                                runat="server" SkinID="sknRadioButtonList">
                                                <asp:ListItem Text="Allow for all users" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Allow only for Tenant administrators" Value="1" Selected="True"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="msg_align">
                            <asp:UpdatePanel ID="ClientManagementSettings_bottomMessageUpdatePanel" runat="server"
                                UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:Label ID="ClientManagementSettings_bottomSuccessMessageLabel" runat="server"
                                        SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="ClientManagementSettings_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label></ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ClientManagementSettings_topSaveButton" />
                                    <asp:AsyncPostBackTrigger ControlID="ClientManagementSettings_bottomSaveButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="td_height_8">
            </td>
        </tr>
        <td class="header_bg">
            <table border="0" cellpadding="0" cellspacing="4" align="right">
                <tr>
                    <td>
                        <asp:Button ID="ClientManagementSettings_bottomSaveButton" runat="server" Text="Save"
                            SkinID="sknButtonId" OnClick="ClientManagementSettings_saveButton_Click" />
                    </td>
                    <td>
                        <asp:LinkButton ID="ClientManagementSettings_bottomResetLinkButton" runat="server"
                            Text="Reset" SkinID="sknActionLinkButton" OnClick="ClientManagementSettings_resetLinkButton_Click"></asp:LinkButton>
                    </td>
                    <td align="center" class="link_button">
                        |
                    </td>
                    <td>
                        <asp:LinkButton ID="ClientManagementSettings_bottomCancelLinkButton" runat="server"
                            Text="Cancel" SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
        </tr>
    </table>
</asp:Content>
