﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// BatchUploadClients.cs
// File that represents the user interface forBatch Question Entry page
// This will helps to upload a set of Clients
#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Ionic.Zip;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;


#endregion Directives

namespace Forte.HCM.UI.ClientCenter
{
    /// <summary> 
    /// 
    /// </summary>
    public partial class BatchUploadClients : PageBase
    {
        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {

        }

        #endregion Protected Overridden Methods
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set default button and focus
                Page.Form.DefaultButton = BatchUploadClients_uploadButton.UniqueID;

                Page.Form.DefaultFocus = BatchUploadClients_fileUpload.UniqueID;

                BatchUploadClients_fileUpload.Focus();

                Master.SetPageCaption("Batch Question Entry");

                if (!IsPostBack)
                {
                    //Assign the page number in viewstate
                    ViewState["PAGE_NUMBER"] = 1;

                    //Assign the invalid grid page number
                    ViewState["INVALID_Clients_PAGE_NUMBER"] = 1;

                    BatchUploadClients_isSavedHiddenField.Value = "0";

                    BatchUploadClients_saveButton.Enabled = true;

                    UserDetail userDetail = new CommonBLManager().GetUserDetail(userID);

                    Session["USERNAME"] = userDetail.FirstName;
                }
                BatchUploadClients_topSuccessMessageLabel.Attributes["Tag"] = "0";

                BatchUploadClients_topErrorMessageLabel.Text = string.Empty;
                BatchUploadClients_topSuccessMessageLabel.Text = string.Empty;
                BatchUploadClients_bottomSuccessMessageLabel.Text = string.Empty;
                BatchUploadClients_bottomErrorMessageLabel.Text = string.Empty;
                BatchUploadClients_inCompleteClientsTabPanel_errorMessageLabel
                 .Text = string.Empty;
                BatchUploadClients_clientsTabPanel_errorMessageLabel.Text
                    = string.Empty;

                BatchUploadClients_pageNavigator.PageNumberClick +=
                    new PageNavigator.PageNumberClickEventHandler
                    (BatchUploadClients_pageNavigator_PageNumberClick);


            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BatchUploadClients_topErrorMessageLabel,
                    BatchUploadClients_bottomErrorMessageLabel,
                    exception.Message);
            }
        }
        /// <summary>
        /// Handler method that will be called when page number is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the event data.
        /// </param>
        void BatchUploadClients_pageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                UpdateOldInvalidClientsToSession(Convert.ToInt32(ViewState["INVALID_Clients_PAGE_NUMBER"]));
                ViewState["INVALID_Clients_PAGE_NUMBER"] = e.PageNumber;
                BindInvalidClients(e.PageNumber);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BatchUploadClients_topErrorMessageLabel,
                    BatchUploadClients_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// This method gets the value from the grid view and updates
        /// the session of invalid question details
        /// </summary>
        /// <param name="PreviousPageNumber">
        /// A <see cref="integer"/> that contains the page number
        /// of previous records.
        /// </param>
        private void UpdateOldInvalidClientsToSession(int PreviousPageNumber)
        {
            List<ClientInformation> inValidClients = null;
            inValidClients = Session["INVALID_Clients"] as List<ClientInformation>;
            if (inValidClients == null || inValidClients.Count == 0)
                return;
            if (PreviousPageNumber == 0)
                PreviousPageNumber = 1;
            int firstRecord = (PreviousPageNumber * GridPageSize) - GridPageSize;
            if (BatchUploadClients_inCompleteClientsGridView.Rows.Count != 0)
            {
                inValidClients.RemoveRange(firstRecord,
                    BatchUploadClients_inCompleteClientsGridView.Rows.Count);
                inValidClients.InsertRange(firstRecord, GetInvalidClients());
                Session["INVALID_Clients"] = null;
                Session["INVALID_Clients"] = inValidClients;
            }
        }
        /// <summary>
        /// Method to save the invalid Clients
        /// </summary>
        private List<ClientInformation> GetInvalidClients()
        {
            ClientInformation client;

            List<ClientInformation> clientDetails = new List<ClientInformation>();

            foreach (GridViewRow row in BatchUploadClients_inCompleteClientsGridView.Rows)
            {
                client = new ClientInformation();
            }

            return clientDetails;
        }
        protected void BatchUploadClients_resetLinkButton_Click(object sender, EventArgs e)
        {

        }
        protected void BatchUploadClients_uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear the Clients in the valid question sessions
                Session["VALID_CLIENT"] = null;

                //Clear the Clients in the invalid question sessions
                Session["INVALID_CLIENT"] = null;

                //Clear the record label text
                BatchUploadClients_recordLabel.Text = string.Empty;

                //Make the total question count as 0
                ViewState["TOTAL_CLIENT_COUNT"] = 0;

                //Make the page number as 1
                ViewState["PAGE_NUMBER"] = 1;

                //Clear the path name from view state
                ViewState["PATH_NAME"] = string.Empty;

                //Set the isSavedHiddenField to default value of 0
                BatchUploadClients_isSavedHiddenField.Value = "0";

                //Make the question tab as active tab
                BatchUploadClients_mainTabContainer.ActiveTabIndex = 0;

                //Save file in the server and get the file name
                string fileName = SaveFileAs();

               
                //If file name is empty return
                if (fileName == string.Empty)
                    return;

                //Save the file name in viewstate
                ViewState["PATH_NAME"] = fileName;

                //Get the sheet name and the total question in each sheet
                GetSheetName(fileName);

                //Load the Clients from excel
                LoadExcelFile(fileName);

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BatchUploadClients_topErrorMessageLabel,
                    BatchUploadClients_bottomErrorMessageLabel, exception.Message);

                BatchUploadClients_uploadExcelDiv.Style["display"] = "block";

                BatchUploadClients_resultDiv.Style["display"] = "none";

                BatchUploadClients_resultDiv.Visible = false;
                BatchUploadClients_uploadExcelDiv.Visible = true;
            }
        }
        /// <summary>
        /// Method that is used to read the Clients from the excel
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/>that holds the file name
        /// </param>
        private void LoadExcelFile(string fileName)
        {
            if (Session["USERNAME"] == null)
            {
                //ShowMessage(BatchUploadClients_topErrorMessageLabel,
                //    BatchUploadClients_bottomErrorMessageLabel, Resources.HCMResource.BatchQuestionEntry_InvalidUserName);

                return;
            }
            ClientExcelBatchReader excelBatchReader = new ClientExcelBatchReader(fileName);

            ViewState["TOTAL_CLIENT_COUNT"] = excelBatchReader.GetCount("Sheet1$");

            List<ClientInformation> clientDetails =
             excelBatchReader.GetClients
             (int.Parse(ViewState["PAGE_NUMBER"].ToString()), GridPageSize,
             Session["USERNAME"].ToString().Trim(), ViewState["CURRENT_SHEET_NAME"].ToString(), userID, fileName);

            GetValidClient(clientDetails);

            //DisplayRecordLabel(ViewState["TOTAL_QUESTION_COUNT"].ToString(), clientDetails.Count);

        }
        /// <summary>
        /// Method that is used to separate valid and invalid 
        /// Clients taken from the excel.
        /// </summary>
        /// <param name="Clients">
        /// A<see cref="List<QuestionDetail>"/>
        /// that holds the list of Clients.
        /// </param>
        private void GetValidClient(List<ClientInformation> clientDetails)
        {
            List<ClientInformation> validClients = new List<ClientInformation>();

            List<ClientInformation> inValidClients = new List<ClientInformation>();

            //For the first time store the list of invalid clients in session    
            if (Session["INVALID_CLIENTS"] != null)
            {
                inValidClients = Session["INVALID_CLIENTS"] as List<ClientInformation>;
            }

            inValidClients.AddRange(clientDetails.FindAll(delegate(ClientInformation clientDetail)
            {
                return clientDetail.IsValid == false;
            }));

            validClients = clientDetails.FindAll(delegate(ClientInformation clientDetail)
            {
                return clientDetail.IsValid == true;
            });

            BatchUploadClients_questionGridView.DataSource = validClients;
            BatchUploadClients_questionGridView.DataBind();
            Session["VALID_CLIENTS"] = validClients;


            Session["INVALID_CLIENTS"] = inValidClients;
            BatchUploadClients_pageNavigator.TotalRecords = inValidClients.Count;
            BatchUploadClients_pageNavigator.PageSize = GridPageSize;

            BatchUploadClients_pageNavigator.Reset();
            BindInvalidClients(1);
        }
        /// <summary>
        /// Method that is used to bind the invalid Clients in the grid
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the current page number
        /// </param>
        private void BindInvalidClients(int pageNumber)
        {
            List<ClientInformation> invalidClients = new List<ClientInformation>();
            int lastRecord = pageNumber * GridPageSize;
            int firstRecord = lastRecord - GridPageSize;
            invalidClients = Session["INVALID_CLIENTS"] as List<ClientInformation>;
            if (invalidClients.Count == 0)
            {
                BatchUploadClients_inCompleteClientsGridView.DataSource = invalidClients;
                BatchUploadClients_inCompleteClientsGridView.DataBind();
                return;
            }

            int remainingRecord = invalidClients.Count - ((pageNumber - 1) * GridPageSize);

            lastRecord = lastRecord - firstRecord;

            lastRecord = lastRecord > remainingRecord ?
                invalidClients.Count % GridPageSize : lastRecord;

            //Get the certain number of Clients from invalid Clients
            BatchUploadClients_inCompleteClientsGridView.DataSource =
                invalidClients.GetRange(firstRecord, lastRecord);

            BatchUploadClients_inCompleteClientsGridView.DataBind();
        }
        /// <summary>
        /// This method helps to save the file in the server.
        /// </summary>
        private string SaveFileAs()
        {
            string fileName = BatchUploadClients_fileUpload.FileName;

            if ((fileName == string.Empty) ||
                 (BatchUploadClients_fileUpload.PostedFile.ContentType
                 != "application/vnd.ms-excel")
                  || (Path.GetExtension(fileName) != ".xls"))
            {
                if (BatchUploadClients_fileUpload.PostedFile.ContentType
                 != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                 Path.GetExtension(fileName) != ".xlsx")
                {
                    base.ShowMessage(BatchUploadClients_topErrorMessageLabel,
                        BatchUploadClients_bottomErrorMessageLabel,
                        Resources.HCMResource.BatchQuesionEntry_PleaseProvideAExcelFile);
                    return string.Empty;
                }
            }
            //else
            {
                //To create a directory in the server
                Directory.CreateDirectory(Server.MapPath("~/") + "\\BatchUploadClients");

                //To save excel file as 
                string saveAsPath = Server.MapPath("~/") + "BatchUploadClients\\"
                    + userID + "_" + DateTime.Now.TimeOfDay.Milliseconds + "_" + fileName;

                BatchUploadClients_fileUpload.SaveAs(saveAsPath);

                BatchUploadClients_uploadExcelDiv.Style["display"] = "none";

                BatchUploadClients_resultDiv.Style["display"] = "block";

                BatchUploadClients_resultDiv.Visible = true;

                return saveAsPath;

            }
        }
        /// <summary>
        /// Method to get the current sheet name 
        /// </summary>
        /// <param name="fileName">
        /// A<see cref="string"/>that stores the name of the file
        /// </param>
        private void GetSheetName(string fileName)
        {
            //Get the list of sheet name in the excel 
            List<string> sheetName = new ExcelBatchReader(fileName).
                GetSheetNames();

            //Stores the sheet details in session
            Session["SHEET_NAME"] = sheetName;

            //Store the current sheet name
            ViewState["CURRENT_SHEET_NAME"] = sheetName[0];

            //Gets the total question count for the sheet
            GetSheetQuestionCount(sheetName[0], fileName);
        }
        /// <summary>
        /// Method to get the question count in current sheet
        /// </summary>
        /// <param name="sheetName">
        /// A<see cref="string"/>that holds the sheet name
        /// </param>
        /// <param name="fileName">
        /// A<see cref="string"/>that holds the file name
        /// </param>
        private void GetSheetQuestionCount(string sheetName, string fileName)
        {
            //Get the total question count of the first sheet
            //and store it in view state
            ViewState["TOTAL_QUESTION_COUNT"] = new ExcelBatchReader
                                              (fileName).GetCount(sheetName);

            //Checks if the number of question in the current sheet is zero,
            //if it is not then it will save the question count in session

            if (int.Parse(ViewState["TOTAL_QUESTION_COUNT"].ToString()) != 0)
            {
                ViewState["CURRENT_SHEET_NAME"] = sheetName;
                return;
            }

            //If the current sheet total question is zero.it checks the next sheet

            //Gets the list of sheet name from session 
            List<string> sheetNames = Session["SHEET_NAME"] as List<string>;

            //Get the index of the current sheet
            int sheetIndex = sheetNames.FindIndex(delegate(string sheet)
            {
                return sheet == sheetName;
            });
            //if the current sheet is the last sheet , it will return
            if (sheetNames.Count - 1 == sheetIndex)
                return;

            //else it will find count for next sheet
            GetSheetQuestionCount(sheetNames[sheetIndex + 1], fileName);
        }
        protected void BatchUploadClients_saveButton_Click(object sender, EventArgs e)
        {

        }
        
        protected void BatchUploadClients_questionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
        protected void BatchUploadClients_ClientsDataList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void BatchUploadClients_nextLinkButton_Click(object sender, EventArgs e)
        {

        }
        protected void BatchUploadClients_removeQuestionLinkButton_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Handler that will be called when the datalist is binded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void BatchUploadClients_subject_categoryDataList_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType != ListItemType.Item
                   && e.Item.ItemType != ListItemType.AlternatingItem)
                    return;

               
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

               
            }
        }
        /// <summary>
        /// Handler method that is called when invalid 
        /// Clients remove all button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void BatchUploadClients_removeInvalidQuestionLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                  
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BatchUploadClients_topErrorMessageLabel,
                    BatchUploadClients_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler method that is called when invalid 
        /// Clients save button is clicked
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the object
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the Event Args
        /// </param>
        protected void BatchUploadClients_invalidSaveButton_Click
            (object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BatchUploadClients_topErrorMessageLabel,
                    BatchUploadClients_bottomErrorMessageLabel, exception.Message);
            }
        }
        /// <summary>
        /// Handler that will be called on subjectcategoryQuestionDataList is binded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the object.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void BatchUploadClients_subjectcategoryQuestionDataList_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                    return;

                
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                // Show error messages to the user
                base.ShowMessage(BatchUploadClients_topErrorMessageLabel,
                    BatchUploadClients_bottomErrorMessageLabel, exception.Message);
            }
        }
}
}

