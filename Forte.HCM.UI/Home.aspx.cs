﻿
#region Namespace

using System;
using System.Web.UI;

using Resources;
using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using AjaxControlToolkit;

#endregion Namespace

namespace Forte.HCM.UI
{
    public partial class Home : PageBase
    {
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption(HCMResource.Home_PageTitile);

                HomePage_tryImageButton.Attributes.Add("onmouseover", "this.src='App_Themes/DefaultTheme/Images/try_btn_hover_fortehcm.gif'");
                HomePage_tryImageButton.Attributes.Add("onmouseout", "this.src='App_Themes/DefaultTheme/Images/try_btn_fortehcm.gif'");
                HomePage_contactImageButton.Attributes.Add("onmouseover", "this.src='App_Themes/DefaultTheme/Images/contact_btn_hover_fortehcm.gif'");
                HomePage_contactImageButton.Attributes.Add("onmouseout", "this.src='App_Themes/DefaultTheme/Images/contact_btn_fortehcm.gif'");
                HomePage_tourImageButton.Attributes.Add("onmouseover", "this.src='App_Themes/DefaultTheme/Images/qtour_btn_hover_fortehcm.gif'");
                HomePage_tourImageButton.Attributes.Add("onmouseout", "this.src='App_Themes/DefaultTheme/Images/qtour_btn_fortehcm.gif'");
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        protected void HomePage_signUpImage_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(@"~/Subscription/SubscriptionType.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }

        #endregion Events

        #region Private Methods

        private void ShowErrorMessage(Exception exp)
        {
            Logger.ExceptionLog(exp);
        }

        /// <summary>
        /// Method that will return a userdetail object for the 
        /// given username and password.
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> that contains the username.
        /// </param>
        /// <param name="Password">
        /// A <see cref="string"/> that contains the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> object that contains the 
        /// information for the given username and password.
        /// </returns>
        private UserDetail AuthenticateUserDetails(string UserName, string Password)
        {
            AuthenticationBLManager authenticationBLManager = new AuthenticationBLManager();
            Authenticate authenticate = new Authenticate();
            authenticate.UserName = UserName;
            authenticate.Password = Password;
            UserDetail userDetails = new UserDetail();
            userDetails = authenticationBLManager.GetAuthenticateUser(authenticate);
            return userDetails;
        }

        #endregion Private Methods

        #region Override Methods

        protected override bool IsValidData()
        {
            bool isValidData = true;
            //if (HomePage_userNametextbox.Text.Trim() == "")
            //{
            //    //base.ShowMessage(Login_errorMessageLabel,
            //    //    Resources.HCMResource.Login_UserNameTextBoxEmpty);
            //    isValidData = false;
            //}
            //if (HomePage_passwordtextbox.Text.Trim() == "")
            //{
            //    //base.ShowMessage(Login_errorMessageLabel,
            //    //    Resources.HCMResource.Login_PasswordTextBoxEmpty);
            //    isValidData = false;
            //}
            return isValidData;
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Override Methods

        public void ForgotPasswordModalpPopupExtender()
        {
            
            ModalPopupExtender modal = (ModalPopupExtender)this.Master.FindControl("HomeMaster_headerControl").
                FindControl("HomeHeaderControl_forgotPassword_ModalpPopupExtender");
            if (modal == null)
                return;
            modal.Show();
            //HomeHeaderControl_forgotPassword_ModalpPopupExtender
        }

        protected void HomePage_tryImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(@"~/Subscription/SubscriptionType.aspx", false);
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp);
            }
        }
    }
}