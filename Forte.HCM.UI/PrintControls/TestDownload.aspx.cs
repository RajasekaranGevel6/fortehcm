﻿#region Directives
using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;
using iTextSharp.text.html;

using System.Text.RegularExpressions;
using System.Text;
using System.Drawing;

#endregion Directives

namespace Forte.HCM.UI.PrntControls
{
    public partial class TestDowload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Method to load the values in test statistics tab
               // if (!IsPostBack)
                {  
                    //LoadNewFormat();

                    //DataTable dt = new DataTable();
                    //dt.Columns.Add("Temp", typeof(string));
                    //DataRow dr = dt.NewRow();
                    //dr[0] = GetHtmlText("http://localhost/HCM/PrntControls/test.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003");
                    //dt.Rows.Add(dr);
                    //grdView1.DataSource = dt;
                    //grdView1.DataBind();
                    //ExportToPDF(grdView1, false);

                    //Document Doc = new Document();
                    //PdfWriter.GetInstance(Doc, Response.OutputStream);
                    //Doc.Open(); Doc.Add(grdTable);
                    //Doc.Close();
                    //Response.ContentType = "application/pdf";
                    //Response.AddHeader
                    //("content-disposition", "attachment; filename=Sample.pdf");
                    //Response.End();

                    // ExportHTMLToPDF(GetHtmlText("http://localhost/HCM/PrntControls/test.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003"));

                    // ConvertImage(Label1.Text);
                 //   lable2.Text = GetHtmlText("http://localhost/HCM/PrintPages/TestResultPrint.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003");

                    //byte[] attachment = Encoding.ASCII.GetBytes(GetHtmlText(Label1.Text));
                    //byte[] attachment = Encoding.ASCII.GetBytes(GetHtmlText("http://localhost/HCM/CandidateCenter/CandidateTestResult.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003"));
                    //byte[] attachment = Encoding.ASCII.GetBytes(GetHtmlText("http://localhost/HCM/PrntControls/test.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003"));
                    //MemoryStream m = new MemoryStream(attachment);
                    //Response.Clear();
                    //Response.AddHeader("Content-Length", attachment.Length.ToString());
                    //Response.ContentType = "application/pdf";
                    //Response.AddHeader("Expires", "0");
                    //Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                    //Response.AddHeader("Pragma", "public");
                    //Response.AddHeader("Content-Disposition", "attachment; filename=sample.pdf");
                    //Response.BinaryWrite(attachment);
                    //Response.Flush();
                    //Response.End();

                   
                    //Response.Clear();
                    //Response.ContentType = "application/pdf";
                    //Response.AddHeader("Content-Disposition", "attachment; filename=sample.pdf");
                    //Response.BufferOutput = true;
                    //Response.AddHeader("Content-Length", attachment.Length.ToString());
                    //Response.BinaryWrite(attachment);
                    //Response.End();

                    //byteArrayToImage(attachment);


                   //byte[] downloadBytes = pdfConverter.GetPdfFromUrlBytes(MyURL);
                    //byte[] downloadBytes = Encoding.ASCII.GetBytes(GetHtmlText("http://localhost/HCM/PrntControls/test.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003"));
                    //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    //Response.Clear();
                    //Response.AddHeader("Content-Type", "binary/octet-stream");
                    //Response.AddHeader("Content-Disposition", "attachment; filename=" + "Rendered.pdf" + "; size="
                    //    + downloadBytes.Length.ToString());
                    //Response.Flush();
                    //Response.BinaryWrite(downloadBytes);
                    //Response.Flush();
                    //Response.End();



                }

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                //.ShowMessage(CandidateTestResult_bottomErrorMessageLabel,
                //CandidateTestResult_topErrorMessageLabel, exception.Message);
            }
        }

        protected void TestDownload_Click(object sender, EventArgs e)
        {

            //byte[] downloadBytes = Encoding.ASCII.GetBytes(GetHtmlText("http://localhost/HCM/PrintPages/TestResultPrint.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003"));
            //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            //Response.Clear();
            //Response.AddHeader("Content-Type", "binary/octet-stream");
            //Response.AddHeader("Content-Disposition", "attachment; filename=" + "Rendered.pdf" + "; size="
            //    + downloadBytes.Length.ToString());
            //Response.Flush();
            //Response.BinaryWrite(downloadBytes);
            //Response.Flush();
            //Response.End();
            // lable2.Text = GetHtmlText("http://localhost/HCM/PrintPages/TestResultPrint.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003");
          //  Response.Redirect("../PrintPages/TestResultPrint.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003");

           // used to build entire input
		StringBuilder sb  = new StringBuilder();

		// used on each read operation
		byte[]        buf = new byte[8192];

		// prepare the web page we will be asking for
		HttpWebRequest  request  = (HttpWebRequest)
            WebRequest.Create("http://localhost/HCM/PrintPages/TestResultPrint.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003");

		// execute the request
		HttpWebResponse response = (HttpWebResponse)
			request.GetResponse();

		// we will read data via the response stream
		Stream resStream = response.GetResponseStream();

		string tempString = null;
		int    count      = 0;

		do
		{
			// fill the buffer with data
			count = resStream.Read(buf, 0, buf.Length);

			// make sure we read some data
			if (count != 0)
			{
				// translate from bytes to ASCII text
				tempString = Encoding.ASCII.GetString(buf, 0, count);

				// continue building the string
				sb.Append(tempString);
			}
		}
		while (count > 0); // any more data to read?
            lable2.Text = sb.ToString();
		// print out page source
		//Console.WriteLine(sb.ToString());
	}

     

        public void StringWriteMethod()
        {
            Response.ContentType = "application/pdf";

            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
            System.Web.UI.HtmlControls.HtmlForm CustomForm = new System.Web.UI.HtmlControls.HtmlForm();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringBuilder strBuild = new StringBuilder();
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Server.Execute("test.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003", htw);
            //lable2.Text = sw.ToString();
            Document pdfDoc = new Document(iTextSharp.text.PageSize.A3, 4f, 4f, 4f, 4f);

            strBuild.Append(sw.ToString());
            StringReader sr = new StringReader(strBuild.ToString());
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            htmlparser.Style.LoadStyle("123", "color", "red");
            htmlparser.Style.LoadStyle("PDFHide", "color", "Black");
            //htmlparser.Style.LoadStyle("PDFShow", "style", "color:Red;background-color:Green;width:25px;position:absolute;top:10px;left:20px;");
            htmlparser.Style.LoadStyle("PDFShow", "color", "White");
            htmlparser.Style.LoadStyle("check_box_text", "color", "White");

            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

            pdfDoc.Open();

            htmlparser.Parse(sr);

            Response.Write(pdfDoc);

            pdfDoc.Close();

            Response.End();
        
        
        }

        public void LoadNewFormat()
        {
            //bind data to data bound controls and do other stuff

            Response.Clear(); 
            Response.Buffer = true; 

            Response.ContentType = "application/vnd.ms-excel";
            StringWriter stringWriter = new StringWriter(); 
            HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter);
            Server.Execute("test.aspx?m=2&candidatesession=CTS_00000011&attemptid=1&parentpage=MY_TST&testkey=TST_00000003", htmlTextWriter);
            this.RenderControl(htmlTextWriter);
            Response.Write(stringWriter.ToString());
            Response.End();
        } //end page load

        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;

        }

        private void ExportHTMLToPDF(string HTMLCode)
        {
            Document document = null;
            try
            {
                document = new Document();
                PdfWriter.GetInstance(document, new FileStream(Server.MapPath(@"../Chart/Sample.pdf"), FileMode.Create));
                document.Open();
                List<IElement> arrayList= iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList((
                new StringReader(HTMLCode)),null);

                for (int k = 0; k < arrayList.Count; k++)
                {   
               document.Add(arrayList[k]);
                }

            document.Close();
            System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename= Sample.pdf");
            System.Web.HttpContext.Current.Response.End();
            }
            finally
            {
                if (document != null) document = null;
            }
        }

        protected void ExportToPDF(GridView gvReport, bool LandScape)
        {
            int noOfColumns = 0, noOfRows = 0;
            DataTable tbl = null;

            if (gvReport.AutoGenerateColumns)
            {
                tbl = gvReport.DataSource as DataTable; // Gets the DataSource of the GridView Control.
                noOfColumns = tbl.Columns.Count;
                noOfRows = tbl.Rows.Count;
            }
            else
            {
                noOfColumns = gvReport.Columns.Count;
                noOfRows = gvReport.Rows.Count;
            }

            float HeaderTextSize = 8;
            float ReportNameSize = 10;
            float ReportTextSize = 8;
            float ApplicationNameSize = 7;

            // Creates a PDF document
            Document document = null;
            if (LandScape == true)
            {
                // Sets the document to A4 size and rotates it so that the orientation of the page is Landscape.
                document = new Document(PageSize.A4.Rotate(), 0, 0, 15, 5);
            }
            else
            {
                document = new Document(PageSize.A4, 0, 0, 15, 5);
            }

            // Creates a PdfPTable with column count of the table equal to no of columns of the gridview or gridview datasource.
            iTextSharp.text.pdf.PdfPTable mainTable = new iTextSharp.text.pdf.PdfPTable(noOfColumns);

            // Sets the first 4 rows of the table as the header rows which will be repeated in all the pages.
            mainTable.HeaderRows = 4;

            // Creates a PdfPTable with 2 columns to hold the header in the exported PDF.
            iTextSharp.text.pdf.PdfPTable headerTable = new iTextSharp.text.pdf.PdfPTable(2);

            // Creates a phrase to hold the application name at the left hand side of the header.
            Phrase phApplicationName = new Phrase("Report", FontFactory.GetFont("Arial", ApplicationNameSize, iTextSharp.text.Font.NORMAL));

            // Creates a PdfPCell which accepts a phrase as a parameter.
            PdfPCell clApplicationName = new PdfPCell(phApplicationName);
            // Sets the border of the cell to zero.
            clApplicationName.Border = PdfPCell.NO_BORDER;
            // Sets the Horizontal Alignment of the PdfPCell to left.
            clApplicationName.HorizontalAlignment = Element.ALIGN_LEFT;

            // Creates a phrase to show the current date at the right hand side of the header.
            Phrase phDate = new Phrase(DateTime.Now.Date.ToString("dd/MM/yyyy"), FontFactory.GetFont("Arial", ApplicationNameSize, iTextSharp.text.Font.NORMAL));

            // Creates a PdfPCell which accepts the date phrase as a parameter.
            PdfPCell clDate = new PdfPCell(phDate);
            // Sets the Horizontal Alignment of the PdfPCell to right.
            clDate.HorizontalAlignment = Element.ALIGN_RIGHT;
            // Sets the border of the cell to zero.
            clDate.Border = PdfPCell.NO_BORDER;

            // Adds the cell which holds the application name to the headerTable.
            headerTable.AddCell(clApplicationName);
            // Adds the cell which holds the date to the headerTable.
            headerTable.AddCell(clDate);
            // Sets the border of the headerTable to zero.
            headerTable.DefaultCell.Border = PdfPCell.NO_BORDER;

            // Creates a PdfPCell that accepts the headerTable as a parameter and then adds that cell to the main PdfPTable.
            PdfPCell cellHeader = new PdfPCell(headerTable);
            cellHeader.Border = PdfPCell.NO_BORDER;
            // Sets the column span of the header cell to noOfColumns.
            cellHeader.Colspan = noOfColumns;
            // Adds the above header cell to the table.
            mainTable.AddCell(cellHeader);

            // Creates a phrase which holds the file name.
            Phrase phHeader = new Phrase("", FontFactory.GetFont("Arial", ReportNameSize, iTextSharp.text.Font.BOLD));
            PdfPCell clHeader = new PdfPCell(phHeader);
            clHeader.Colspan = noOfColumns;
            clHeader.Border = PdfPCell.NO_BORDER;
            clHeader.HorizontalAlignment = Element.ALIGN_CENTER;
            mainTable.AddCell(clHeader);

            // Creates a phrase for a new line.
            Phrase phSpace = new Phrase("\n");
            PdfPCell clSpace = new PdfPCell(phSpace);
            clSpace.Border = PdfPCell.NO_BORDER;
            clSpace.Colspan = noOfColumns;
            mainTable.AddCell(clSpace);

            // Sets the gridview column names as table headers.
            for (int i = 0; i < noOfColumns; i++)
            {
                Phrase ph = null;

                if (gvReport.AutoGenerateColumns)
                {
                    ph = new Phrase(tbl.Columns[i].ColumnName, FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD));
                }
                else
                {
                    ph = new Phrase(gvReport.Columns[i].HeaderText, FontFactory.GetFont("Arial", HeaderTextSize, iTextSharp.text.Font.BOLD));
                }

                mainTable.AddCell(ph);
            }

            // Reads the gridview rows and adds them to the mainTable
            for (int rowNo = 0; rowNo < noOfRows; rowNo++)
            {
                for (int columnNo = 0; columnNo < noOfColumns; columnNo++)
                {
                    if (gvReport.AutoGenerateColumns)
                    {
                        string s = gvReport.Rows[rowNo].Cells[columnNo].Text.Trim();
                        Phrase ph = new Phrase(s, FontFactory.GetFont("Arial", ReportTextSize, iTextSharp.text.Font.NORMAL));
                        mainTable.AddCell(ph);
                    }
                    else
                    {
                        if (gvReport.Columns[columnNo] is TemplateField)
                        {
                            Label lbl = (Label)(gvReport.Rows[rowNo].FindControl("Label1"));
                            //DataBoundLiteralControl lc = gvReport.Rows[rowNo].Cells[columnNo].Controls[0] as DataBoundLiteralControl;
                            string s = lbl.Text.Trim();
                            Phrase ph = new Phrase(s, FontFactory.GetFont("Arial", ReportTextSize, iTextSharp.text.Font.NORMAL));
                            mainTable.AddCell(ph);
                        }
                        else
                        {
                            string s = gvReport.Rows[rowNo].Cells[columnNo].Text.Trim();
                            Phrase ph = new Phrase(s, FontFactory.GetFont("Arial", ReportTextSize, iTextSharp.text.Font.NORMAL));
                            mainTable.AddCell(ph);
                        }
                    }
                }

                // Tells the mainTable to complete the row even if any cell is left incomplete.
                mainTable.CompleteRow();
            }

            // Gets the instance of the document created and writes it to the output stream of the Response object.
            PdfWriter.GetInstance(document, System.Web.HttpContext.Current.Response.OutputStream);
            //
            //Code Commented From Here

            //// Creates a footer for the PDF document.
            //HeaderFooter pdfFooter = new HeaderFooter(new Phrase(), true);
            //pdfFooter.Alignment = Element.ALIGN_CENTER;
            //pdfFooter.Border = iTextSharp.text.Rectangle.NO_BORDER;
            //// Sets the document footer to pdfFooter.
            //document.Footer = pdfFooter;
            // Opens the document.
            document.Open();
            // Adds the mainTable to the document.
            document.Add(mainTable);
            // Closes the document.
            document.Close();

            System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename= Sample.pdf");
            System.Web.HttpContext.Current.Response.End();
        }

        //private void Methodone(string HTMLSource)
        //{
        //    Response.ContentType = "application/pdf";

        //    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
        //    System.Web.UI.HtmlControls.HtmlForm CustomForm = new System.Web.UI.HtmlControls.HtmlForm();
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    StringWriter sw = new StringWriter();
        //    HtmlTextWriter hw = new HtmlTextWriter(sw);

        //    // StringBuilder test = new StringBuilder();
        //    // test.Append("<html><head><link href=http://localhost/HCM/App_Themes/DefaultTheme/DefaultStyle.css rel=stylesheet type=text/css /></head><body> <form id='HCM' runat='server'><span class=123>Sundar</span>");
        //    // CustomForm.Controls.Add(CandidateTestResultsPrintControl);
        //    // CandidateTestResultsPrintControl.RenderControl(hw);
        //    // test.Append(sw.ToString());

        //    //// test = test.Replace("../chart/", "http://" + Request.ServerVariables["HTTP_HOST"].ToString() + "/HCM/chart/").Replace("display:block", "visibility:hidden").Replace("display:none", "visibility:visible");

        //    // //  test.Replace("../chart/", "http://" + Request.ServerVariables["HTTP_HOST"].ToString() + "/HCM/chart/");
        //    // //test = test.Replace("show", "none").Replace("hide", "block");
        //    // test.Append("</form></body></html>");

        //    Document pdfDoc = new Document(iTextSharp.text.PageSize.A3, 4f, 4f, 4f, 4f);

        //    //StringReader sr = new StringReader(test.ToString());

        //    //string illegal = "\"M\"\\a/ry/ h**ad:>> a\\/:*?\"| li*tt|le|| la\"mb.?";
        //    //string regexSearch = string.Format("{0}{1}",
        //    //                     new string(Path.GetInvalidFileNameChars()),
        //    //                     new string(Path.GetInvalidPathChars()));
        //    //Regex r = new Regex(string.Format("[{0}]", Regex.Escape(HTMLSource)));
        //    //illegal = r.Replace(illegal, ""); 

        //    StringReader sr = new StringReader(HTMLSource.ToString());


        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    htmlparser.Style.LoadStyle("123", "color", "red");
        //    htmlparser.Style.LoadStyle("PDFHide", "color", "Black");
        //    //htmlparser.Style.LoadStyle("PDFShow", "style", "color:Red;background-color:Green;width:25px;position:absolute;top:10px;left:20px;");
        //    htmlparser.Style.LoadStyle("PDFShow", "color", "White");
        //    htmlparser.Style.LoadStyle("check_box_text", "color", "White");

        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        //    pdfDoc.Open();

        //    htmlparser.Parse(sr);

        //    pdfDoc.Close();

        //    Response.Write(pdfDoc);

        //    Response.End();
        //}

        private void ConvertImage(string strHTML)
        {
            Bitmap objBitMap = new Bitmap(5000, 5000);
            SolidBrush objSolidBrush = new SolidBrush(Color.White);
            Graphics objGraphics = Graphics.FromImage(objBitMap);
            objGraphics.FillRectangle(objSolidBrush, 0, 0, 5000, 5000);
            PointF pp = new PointF(5.0f, 5.0f);
            System.Drawing.Font objFont = new System.Drawing.Font("Times New Roman", 14);
            SolidBrush objForeBrush = new SolidBrush(Color.Black);
            objGraphics.DrawString(strHTML, objFont, objForeBrush, pp);
            objBitMap.Save(Server.MapPath(@"~\Chart\Image1.JPEG"));
        }

        private string GetHtmlText(string uri)
        {
            Stream stream;
            StreamReader reader;
            String response = null;
            WebClient webClient = new WebClient();

            using (webClient)
            {
                try
                {
                    // open and read from the supplied URI
                    stream = webClient.OpenRead(uri);
                    webClient.Credentials = CredentialCache.DefaultCredentials; // new NetworkCredential("rtc@srasys.co.in", "rtc@123");

                    reader = new StreamReader(stream);
                    response = reader.ReadToEnd();
                }
                catch (WebException ex)
                {
                    Logger.ExceptionLog(ex);

                    if (ex.Response is HttpWebResponse)
                    {
                        // Add you own error handling as required
                        switch (((HttpWebResponse)ex.Response).StatusCode)
                        {
                            case HttpStatusCode.NotFound:
                            case HttpStatusCode.Unauthorized:
                                response = null;
                                break;

                            default:
                                throw ex;
                        }
                    }
                }
            }

            return response;
        }

    }
}
