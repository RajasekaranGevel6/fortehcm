﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateTestResultsPrintControl.ascx.cs"
    Inherits="Forte.HCM.UI.PrntControls.CandidateTestResultsPrintControl" %>
<%@ Register Src="~/CommonControls/GeneralTestStatisticsControl.ascx" TagName="TestStatisticsControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/MultipleSeriesChartControl.ascx" TagName="MultipleSeriesChartControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/HistogramChartControl.ascx" TagName="HistogramChartControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/TestResultHeaderControl.ascx" TagName="TestResultHeaderControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/TestResulsCanditateControl.ascx" TagName="TestResulsCanditateControl"
    TagPrefix="uc6" %>
   
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td class="td_height_5">
        </td>
    </tr>
    <tr>
        <td class="tab_body_bg">
            <uc5:TestResultHeaderControl ID="TestResult_testResultHeaderControl" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="td_height_5">
        </td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="msg_align">
                                    Test Statistics
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc2:TestStatisticsControl ID="TestResults_testStatisticsControl" runat="server">
                                    </uc2:TestStatisticsControl>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="td_height_5">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="msg_align">
                        Candidate Statistics
                    </td>
                </tr>
                <tr>
                    <td >
                        <uc6:TestResulsCanditateControl ID="TestResult_testResulsCanditateControl" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="td_height_5">
        </td>
    </tr>
</table>
