﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forte.HCM.TalentScoutSL.DataObjects;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;

namespace Forte.HCM.TalentScoutSL
{
    public partial class LoginJobBoard : ChildWindow
    {
        private TalentScoutServiceClient webservice;
        
        public LoginJobBoard()
        {
            InitializeComponent();
            
            webservice = new TalentScoutServiceClient();
            webservice.CheckJobBoardLoginCredentialsCompleted += new EventHandler<CheckJobBoardLoginCredentialsCompletedEventArgs>(webservice_CheckJobBoardLoginCredentialsCompleted);
        }

        void webservice_CheckJobBoardLoginCredentialsCompleted(object sender, CheckJobBoardLoginCredentialsCompletedEventArgs e)
        {
            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            try
            {
                this.Close();
                    
                SearchJobBoard searchJobBoard = mainPage.searchJobBoard;
                mainPage.SessionToken = e.Result.ToString();
                if (mainPage.SessionToken != null)
                {
                    if (LoginJobBoard_rememberMeCheckBox.IsChecked.Value)
                        IsolatedFileSettings.WriteToIsolatedFile(LoginJobBoard_emailIdTextBox.Text + "$" +
                            LoginJobBoard_passwordTextBox.Password);
                    mainPage.OpenSearchJobBoardWindow();
                }
                else
                {
                    mainPage.DisplayErrorMessage(null, "Invalid credentials in cache");
                }
            }
            catch 
            {
                mainPage.DisplayErrorMessage(null, "Invalid credentials in cache");
            }
        }

        /// <summary>
        /// Handler event when Login Button click is pressed
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="RoutedEventArgs"/> contains the events
        /// </param>
        private void LoginJobBoard_loginButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                webservice.CheckJobBoardLoginCredentialsAsync(LoginJobBoard_emailIdTextBox.Text,
                        LoginJobBoard_passwordTextBox.Password);
                
            }
            catch
            {
               
            }
        }

        private void ChildWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            mainPage.ShowHideLoading(true);
        }
    }
}

