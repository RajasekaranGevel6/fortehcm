﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [EllipseControl.xaml.cs]

#endregion
#region Directives                                                             

using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Controls;
using System.Collections.Generic;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using Forte.HCM.TalentScoutSL.Utils;
using System.Windows.Browser;

#endregion Directives

namespace Forte.HCM.TalentScoutSL
{
    public partial class SearchClientRequestWindow : ChildWindow
    {

        #region Private Variables                                              

        /// <summary>
        /// A<see cref="TalentScoutServiceClient"/> used to call the web service.
        /// </summary>
        private TalentScoutServiceClient webservice = null;
        /// <summary>
        /// A<see cref="bool"/> holds whether the form is closing or not
        /// </summary>
        private bool IsClosing = false;
        /// <summary>
        /// A <see cref="PagedCollectionView"/> holds the result set 
        /// of the data to display to the user.
        /// Note:- this is used for Paging in data grid.
        /// </summary>
        private PagedCollectionView pagedCollectionViewForResultSet = null;
        /// <summary>
        /// A <see cref="bool"/> holds whether page in the data grid
        /// is changing
        /// </summary>
        private bool IsPageChanging = false;

        MainPage mainPage;
        #endregion Private Variables

        #region Constructor                                                    

        /// <summary>
        /// Constructor method 
        /// </summary>
        public SearchClientRequestWindow()
        {
            InitializeComponent();
            ClearErrorMessage();
            this.Closing += new EventHandler<System.ComponentModel.CancelEventArgs>(SearchClientRequestWindow_Closing);
            this.Loaded += new RoutedEventHandler(SearchClientRequestWindow_Loaded);
        }

        void SearchClientRequestWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                HtmlPage.Plugin.Focus();
                ClientRequetWindow_clientRequestNumberTextBox.UpdateLayout();
                ClientRequetWindow_clientRequestNumberTextBox.Focus();
            }
            catch (Exception exp)
            {
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        #endregion Constructor

        #region Events                                                         

        /// <summary>
        /// Handler event when Search Button click is pressed
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="RoutedEventArgs"/> contains the events
        /// </param>
        private void SearchClientRequestWindow_searchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearErrorMessage();
                SearchClientRequest();
                SearchClientRequestWindow_clientRequestDataGrid.SelectedIndex = -1;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method when the date control key down is pressed
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="KeyEventArgs"/> contains the events
        /// </param>
        private void ClientRequestWindow_clientRequestDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                e.Handled = true;
                if (e.Key != Key.Enter)
                    return;
                ClearErrorMessage();
                SearchClientRequest();
                SearchClientRequestWindow_clientRequestDataGrid.SelectedIndex = -1;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method when the client id text box key down is pressed.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="KeyEventArgs"/> contains the events
        /// </param>
        private void ClientRequestWindow_TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;
                ClearErrorMessage();
                SearchClientRequest();
                SearchClientRequestWindow_clientRequestDataGrid.SelectedIndex = -1;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method when the datagrid selected item is changed
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="SelectionChangedEventArgs"/> contains the selection changed
        /// events
        /// </param>
		private void SearchClientRequestWindow_clientRequestDataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
            try
            {
                if (IsPageChanging)
                    return;
                if (IsClosing && e.AddedItems.Count <= 0)
                    return;
                this.Title = string.Empty;
                if (!string.IsNullOrEmpty(((SPGET_CLIENT_REQUEST_INFORMATION_SEARCHResult)e.AddedItems[0]).TAGS))
                {
                    this.Title = ((SPGET_CLIENT_REQUEST_INFORMATION_SEARCHResult)e.AddedItems[0]).TAGS;
                    MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
                    mainPage.positionProfileName = ((SPGET_CLIENT_REQUEST_INFORMATION_SEARCHResult)e.AddedItems[0]).POSITION_PROFILE_NAME;
                    mainPage.PositionProfileId=Convert.ToInt32(((SPGET_CLIENT_REQUEST_INFORMATION_SEARCHResult)e.AddedItems[0]).GEN_ID);
                    mainPage.MainPage_PositionProfileLabel.Text = mainPage.positionProfileName;
                    mainPage.MainPage_ReturnPositionProfile_StackPanel.Visibility = Visibility.Visible;
                }
                ClearControls();
            }
            catch (Exception exp)
            {//BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        } 

        /// <summary>
        /// Handler method when the window is closing
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="System.ComponentModel.CancelEventArgs"/> contains the window closing 
        /// events
        /// </param>
        private void SearchClientRequestWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (IsClosing)
                    return;
                this.Title = "";
                ClearControls();
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler event that fires when data pager page event chaging
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> contains the events
        /// </param>
        private void SearchClientRequestWindow_pagingControlDataPager_PageChanging(object sender, EventArgs e)
        {
            try
            {
                IsPageChanging = true;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler event for the collection object of PagedCollectionView when
        /// Item source changing
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> contains the events
        /// </param>
        private void pagedCollectionViewForResultSet_CurrentChanged(object sender, EventArgs e)
        {
            try
            {
                SearchClientRequestWindow_clientRequestDataGrid.SelectedIndex = -1;
                IsPageChanging = false;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        #endregion Events

        #region Private Methods                                                
        
        /// <summary>
        /// Clears the error message label
        /// </summary>
        private void ClearErrorMessage()
        {
            SearchClientRequestWindow_errorMessageLabel.Text = "";
        }

        /// <summary>
        /// Method clears all controls
        /// </summary>
        private void ClearControls()
        {
            ClearErrorMessage();
            ClientRequetWindow_clientRequestNumberTextBox.Text = "";
            ClientRequestWindow_clientIdTextBox.Text = "";
            ClientRequestWindow_clientPostionName.Text = "";
            ClientRequestWindow_contactNameTextBox.Text = "";
            ClientRequestWindow_departmentNameTextBox.Text = "";
            ClientRequestWindow_locationTextBox.Text = "";
            ClientRequestWindow_createdByTextBox.Text = "";
            ClientRequestWindow_clientRequestDate.ClearValue(DatePicker.SelectedDateProperty);
            IsClosing = true;
            SearchClientRequestWindow_clientRequestDataGrid.ClearValue(DataGrid.SelectedItemProperty);
            SearchClientRequestWindow_clientRequestDataGrid.ItemsSource = null;
            SearchClientRequestWindow_pagingControlDataPager.Source = null;
            SearchClientRequestWindow_pagingControlDataPager.Visibility = Visibility.Collapsed;
            if (pagedCollectionViewForResultSet != null)
            {
                pagedCollectionViewForResultSet.CurrentChanged -= new EventHandler(pagedCollectionViewForResultSet_CurrentChanged);
                pagedCollectionViewForResultSet = null;
            }
            this.Close();
            IsClosing = false;
        }

        /// <summary>
        /// This method calls the service method to get the client request information
        /// </summary>
        private void SearchClientRequest()
        {
            string ClientRequestNumber = null;
            object PositionName = null;
            object DateCreated = null;
            string createdBy=null;

            try
            {
                if(ClientRequestWindow_createdByTextBox.Text.Trim()!="")
                    createdBy=ClientRequestWindow_createdByTextBox.Text.Trim();
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.GetClientRequestNumberCompleted -=
                      new EventHandler<GetClientRequestNumberCompletedEventArgs>(webservice_GetClientRequestNumberCompleted);
                webservice.GetClientRequestNumberCompleted +=
                       new EventHandler<GetClientRequestNumberCompletedEventArgs>(webservice_GetClientRequestNumberCompleted);
                if (ClientRequestWindow_clientRequestDate.SelectedDate.HasValue)
                {
                    try
                    {
                        DateCreated = Convert.ToDateTime(ClientRequestWindow_clientRequestDate.SelectedDate.Value);
                    }
                    catch (NullReferenceException)
                    {
                        throw new NullReferenceException("Enter valid date");
                    }
                }
                //DateTime.TryParse(ClientRequestWindow_clientRequestDate.SelectedDate.Value.ToString(), out RequestedDate);
                if (ClientRequetWindow_clientRequestNumberTextBox.Text.Trim() != "")
                    ClientRequestNumber = ClientRequetWindow_clientRequestNumberTextBox.Text.Trim();
                if (ClientRequestWindow_clientPostionName.Text.Trim() != "")
                    PositionName = ClientRequestWindow_clientPostionName.Text.Trim();
                webservice.GetClientRequestNumberAsync(
                    ClientRequestNumber,
                     ClientRequestWindow_clientIdTextBox.Text.Trim(),
                     PositionName,
                    DateCreated, ClientRequestWindow_contactNameTextBox.Text.Trim(), ClientRequestWindow_locationTextBox.Text.Trim(), ClientRequestWindow_departmentNameTextBox.Text.Trim(), createdBy, null, null, "CLIENT", 'A',
                    ((Application.Current.RootVisual as Grid).Children[0] as MainPage).LoginUserId);
            }
            finally
            {
                if (ClientRequestNumber != null) ClientRequestNumber = null;
                if (PositionName != null) PositionName = null;
                if (DateCreated != null) DateCreated = null;
            }
        }

        #region Web service request completed methods                          

        /// <summary>
        /// This method calls when the client request number web service method is completed
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="GetClientRequestNumberCompletedEventArgs"/> contains 
        /// the class objects
        /// </param>
        void webservice_GetClientRequestNumberCompleted(object sender, GetClientRequestNumberCompletedEventArgs e)
        {
            List<SPGET_CLIENT_REQUEST_INFORMATION_SEARCHResult> result = null;
            try
            {
                result = e.Result.ToList();
                int ToRemoveFromResult = -1;
                int TotalRecords = 0;
                for (int i = 0; i < result.Count; i++)
                {
                    if (result[i].TOTAL >= 0)
                        ToRemoveFromResult = i;
                    if (result[i].CREATED_DATE == null)
                        continue;
                }
                if (ToRemoveFromResult >= 0)
                {
                    TotalRecords = (int)result[ToRemoveFromResult].TOTAL;
                    result.RemoveAt(ToRemoveFromResult);
                }
                if (pagedCollectionViewForResultSet != null)
                    pagedCollectionViewForResultSet = null;
                pagedCollectionViewForResultSet = new PagedCollectionView(result);
                if (App_GlobalResources.TalentScoutResource.TalentScout_SearchClientRequestDataGridPageSize == "")
                    SearchClientRequestWindow_pagingControlDataPager.PageSize = 10;
                else
                    SearchClientRequestWindow_pagingControlDataPager.PageSize =
                        Convert.ToInt32(App_GlobalResources.TalentScoutResource.TalentScout_SearchClientRequestDataGridPageSize);
                SearchClientRequestWindow_clientRequestDataGrid.ItemsSource = pagedCollectionViewForResultSet;
                pagedCollectionViewForResultSet.CurrentChanged += new EventHandler(pagedCollectionViewForResultSet_CurrentChanged);
                SearchClientRequestWindow_pagingControlDataPager.Source = pagedCollectionViewForResultSet;
                if (SearchClientRequestWindow_pagingControlDataPager.PageSize < result.Count)
                    SearchClientRequestWindow_pagingControlDataPager.Visibility = Visibility.Visible;
                else
                    SearchClientRequestWindow_pagingControlDataPager.Visibility = Visibility.Collapsed;
                if (result != null && result.Count == 0)
                    SearchClientRequestWindow_errorMessageLabel.Text = "No data found to display";
                if (mainPage == null)
                 mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
               
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchClientRequestWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
            finally
            {
                if (result != null) result = null;
                if (pagedCollectionViewForResultSet != null) pagedCollectionViewForResultSet = null;
            }
        }

        #endregion Web service request completed methods

        #endregion Private Methods
    }
}

