﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using DivIHN.PRAS.TalentScout.TalentScoutServiceReference;

namespace DivIHN.PRAS.TalentScout
{
    public partial class EmailRecruiter : ChildWindow
    {
        public EmailRecruiter()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
			TalentScoutServiceClient webservice;
            Uri address = new Uri(Application.Current.Host.Source, "../Services/TalentScoutService.svc");

            // Get the report details to load the chart.
            webservice = new TalentScoutServiceClient("BasicHttpBinding_ITalentScoutService", address.AbsoluteUri);

		    webservice.SendMailCompleted += new EventHandler<SendMailCompletedEventArgs>(webservice_SendMailCompleted);
            webservice.SendMailAsync(EmailRecruiter_fromAddressTextBox.Text.Trim(), EmailRecruiter_toAddressTextBox.Text.Trim(), "");

            this.DialogResult = true;
        }
        void webservice_SendMailCompleted(object sender, SendMailCompletedEventArgs e)
        {

        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

