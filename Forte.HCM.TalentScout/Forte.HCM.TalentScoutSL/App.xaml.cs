﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [App.xaml.cs]

#endregion

#region Namespaces                                                              
using System;
using System.Windows;
using System.Threading;
using System.Globalization;
using System.Windows.Controls;
using Forte.HCM.TalentScoutSL.DataObjects;
using System.Collections.Generic;
#endregion

namespace Forte.HCM.TalentScoutSL
{
    public partial class App : Application
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public App()
        {
            InitializeComponent();
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";
            this.Startup += this.Application_Startup;
            this.Exit += this.Application_Exit;
            this.UnhandledException += this.Application_UnhandledException;
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            string InitParamter = string.Empty;
            string ppname = string.Empty;
            if (e.InitParams.Count != 0 && e.InitParams.Keys.Contains("KeyWord"))
                InitParamter = e.InitParams["KeyWord"].Replace('~', ',');
            if (e.InitParams.Count != 0 && e.InitParams.Keys.Contains("ppname"))
                ppname = e.InitParams["ppname"].Replace('~', ',');
          
            Grid root = new Grid();
            this.RootVisual = root;
            //if (IsolatedFileSettings.GetIsolatedFileValue() != "")
            root.Children.Add(new MainPage(InitParamter, Convert.ToInt32(e.InitParams["LoggedInUser"]),
                Convert.ToInt32(e.InitParams["LoggedInTenant"]), e.InitParams["UIUrl"], Convert.ToInt32(e.InitParams["PositionProfileId"]),
                ppname, Convert.ToInt32(e.InitParams["OperationTimeOut"]), e.InitParams["ParentPage"]));
            // else
            //   root.Children.Add(new Login());
        }

        private void Application_Exit(object sender, EventArgs e)
        {
            
        }
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            // If the app is running outside of the debugger then report the exception using
            // the browser's exception mechanism. On IE this will display it a yellow alert 
            // icon in the status bar and Firefox will display a script error.
            if (!System.Diagnostics.Debugger.IsAttached)
            {

                // NOTE: This will allow the application to continue running after an exception has been thrown
                // but not handled. 
                // For production applications this error handling should be replaced with something that will 
                // report the error to the website and stop the application.
                e.Handled = true;
                Deployment.Current.Dispatcher.BeginInvoke(delegate { ReportErrorToDOM(e); });
            }
        }
        private void ReportErrorToDOM(ApplicationUnhandledExceptionEventArgs e)
        {
            try
            {
                string errorMsg = e.ExceptionObject.Message + e.ExceptionObject.StackTrace;
                errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");

                System.Windows.Browser.HtmlPage.Window.Eval("throw new Error(\"Unhandled Error in Silverlight Application " + errorMsg + "\");");
            }
            catch (Exception)
            {
            }
        }
    }
}
