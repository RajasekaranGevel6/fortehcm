﻿using System;
using System.Windows;
using System.Windows.Controls;

using Forte.HCM.TalentScoutSL.Utils;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;

namespace Forte.HCM.TalentScoutSL
{
    public partial class AddNoteWindow : ChildWindow
    {
        int candidateID = 0;
        public event RoutedEventHandler onClick;
        MainPage mainPage;
        public AddNoteWindow()
        {
            InitializeComponent();

            // Clear the message label.
            AddNoteWindow_errorMessageLabel.Text = string.Empty;

            // Set focus to notes textbox.
            AddNoteWindow_notesTextBox.Focus();
        }
        public void SetCandidateDetails(int pcandidateId)
        {
            candidateID = pcandidateId;
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ViewNotesWindow vn = new ViewNotesWindow();
            TalentScoutServiceClient webservice = null;
            AddServiceReference.BindServiceReference(ref webservice);
            webservice.InsertCommentsCompleted += new EventHandler<InsertCommentsCompletedEventArgs>(webservice_InsertCommentsCompleted);
            mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;

            if (AddNoteWindow_notesTextBox.Text.Trim().Length == 0)
            {
                AddNoteWindow_errorMessageLabel.Text = "Notes cannot be empty";
                AddNoteWindow_errorMessageLabel.Style = Application.Current.Resources["AddNoteRedText"] as Style;
            }
            else
            {
                webservice.InsertCommentsAsync("CA_NOTES", candidateID, null,
                    AddNoteWindow_notesTextBox.Text, mainPage.LoginUserId, mainPage.PositionProfileId);
                AddNoteWindow_errorMessageLabel.Text = "Notes added successfully";
                AddNoteWindow_errorMessageLabel.Style = Application.Current.Resources["AddNoteGrayText"] as Style;

            }
        }


        void webservice_InsertCommentsCompleted(object sender, InsertCommentsCompletedEventArgs e)
        {
            CloseWindow();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }
        private void CloseWindow()
        {
            if (onClick != null)
                onClick(this, new RoutedEventArgs());

            this.DialogResult = false;
            ViewNotesWindow vn = new ViewNotesWindow();
            vn.DialogResult = false;
        }
    }
}

