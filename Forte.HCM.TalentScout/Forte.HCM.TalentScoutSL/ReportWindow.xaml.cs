﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [ReportWindow.xaml.cs]

#endregion

#region Namespaces                                                              
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Controls.DataVisualization.Charting;

using Forte.HCM.TalentScoutSL.Utils;
using Forte.HCM.TalentScoutSL.DataObjects;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class displays as child window with all the statistics information
    /// of students for the given search terms.
    /// </summary>
    public partial class ReportWindow : ChildWindow
    {
        MainPage mainPage;
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public ReportWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Call the GetReportDetails() method to generate the statistics of selected candidate.
        /// </summary>
        public void GenerateReportDetails()
        {
            try
            {
                mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
                SetReportWindowStatus(false);
                TalentScoutServiceClient webservice = null;
                AddServiceReference.BindServiceReference(ref webservice);
                string source = "" ;
                if (mainPage.IsResume)
                {
                    source = "Resume";
                    webservice.GetResumeReportDetailsCompleted += new EventHandler<GetResumeReportDetailsCompletedEventArgs>(webservice_GetResumeReportDetailsCompleted);
                    webservice.GetResumeReportDetailsAsync(mainPage.TenantId,mainPage.searchKeywords);
                    
                }
                else
                {
                    source = "Assessment";
                    webservice.GetReportDetailsCompleted += new EventHandler<GetReportDetailsCompletedEventArgs>(webservice_GetReportDetailsCompleted);
                    webservice.GetReportDetailsAsync(mainPage.searchKeywords);
                }
                ReportWindow_minMaxAvgScoreChartHeader.Text = source+" Score Summary";
                RightPane_summaryHeader.Text = "Candidate "+source+" Score For Each Search Term";
                RightPane_histogramChartHeader.Text = "Candidates " + source + " Score Comparison For Each Search Term";
                    
            }
            finally
            {
                if (mainPage != null) mainPage = null;
            }
        }
        /// <summary>
        /// Method that assigns the datasource to summary chart.
        /// </summary>
        /// <param name="scoreRange">
        /// A list of <see cref="ScoreRangeChartValues"/> that holds the score range.
        /// </param>
        /// <param name="data"></param>
        public void SetScoreSummary(List<ScoreRangeChartValues> scoreRange, IEnumerable<SearchTerms_ScorePercentage> data)
        {
            ((PieSeries)ReportWindow_scorePieChart.Series[0]).ItemsSource = data;
            ((ColumnSeries)ReportWindow_scoreChart.Series[0]).ItemsSource = null;
            Thickness margin = new Thickness(-16, 0, 12, 0);
            foreach (ScoreRangeChartValues sr in scoreRange)
            {
                sr.MARGIN = margin;
            }
            ((ColumnSeries)ReportWindow_scoreChart.Series[0]).ItemsSource = scoreRange;
            var maximum = (from item in scoreRange select item.NO_OF_CANDIDATES).Max();

            maximum = Utility.RoundAxisMaxValue(Convert.ToInt32(maximum));

            LinearAxis axis = ReportWindow_scoreChart.Axes[0] as LinearAxis;
            axis.Maximum = maximum;
            axis.Minimum = 0;
            axis.Interval = (maximum) / 5;

            LinearAxis xAxis = ReportWindow_scoreChart.Axes[1] as LinearAxis;
            xAxis.Maximum = scoreRange[scoreRange.Count - 1].SCORE;
            xAxis.Minimum = 0;
            xAxis.Interval = scoreRange[0].SCORE;
        }
        /// <summary>
        /// Method that assigns the report informations in datagrid and chart.
        /// </summary>
        /// <param name="reportDetails">
        /// A <see cref="ReportDetails"/> that holds the report details.
        /// </param>
        private void SetReportDetails(ReportDetails reportDetails)
        {
            try
            {
                List<SearchTerms_ScorePercentage> scoreDetails = reportDetails.SearchTerms_ScoreDistribution.ToList();
                List<SearchTerms_MinMaxReportDetails> minMaxDetails = reportDetails.SearchTerms_MinMaxDetails.ToList();
                ReportWindow_histogramScoreStackPanel.Children.Clear();
                if (mainPage == null)
                    mainPage = (MainPage)((Grid)Application.Current.RootVisual).Children[0];
                foreach (SearchTerm searchTerm in mainPage.searchTerms)
                {
                    if (searchTerm.Weightage == 0)
                        continue;
                    var searchTerm_ScoreDetails = (from item in scoreDetails
                                                   where item.SEARCH_TERM == searchTerm.Keyword.Trim()
                                                   select item);
                    if (searchTerm_ScoreDetails != null && searchTerm_ScoreDetails.Count() != 0)
                        CreateSearchTermHistogramChart(searchTerm_ScoreDetails.ToList());
                }
                ReportWindow_minMaxStackPanel.Children.Clear();

                foreach (SearchTerms_MinMaxReportDetails minMaxAvgTerm in minMaxDetails)
                {
                    CreateSearchTermMinMaxAvgChart(minMaxAvgTerm);
                }

                List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates = mainPage.scoreDetails.CandidateDetails.ToList();
                if (candidates != null && candidates.Count != 0)
                {
                    // Display the chart with min, max and avg values.
                    SearchTerms_MinMaxReportDetails minMaxAvgActualScore = new SearchTerms_MinMaxReportDetails();
                    minMaxAvgActualScore.MIN_SCORE = (from item in candidates select item.SCORE).Min();
                    minMaxAvgActualScore.MAX_SCORE = (from item in candidates select item.SCORE).Max();
                    minMaxAvgActualScore.AVG_SCORE = (from item in candidates select item.SCORE).Average();
                    ReportWindow_minMaxAvgScoreChart.CreateChart(minMaxAvgActualScore);

                    // Display the technical skills in datagrid.
                    List<CompetencyVector> vectors = new List<CompetencyVector>();
                    CompetencyVector vector;
                    foreach (CandidateTechnicalSkills skill in mainPage.technicalSkills)
                    {
                        var searchTerm = (from item in mainPage.searchTerms
                                          where item.Keyword.ToLower() == skill.SKILL_NAME.Trim().ToLower()
                                          select item.Keyword);
                        if (searchTerm.Count() > 0)
                        {
                            vector = new CompetencyVector();
                            vector.Skill = skill.SKILL_NAME;
                            vector.Recency = skill.RECENCY.Trim();
                            vector.NoOfYears = skill.NO_OF_YEAR.Trim();

                            vectors.Add(vector);
                        }
                    }

                    ReportWindow_skillsDataGrid.ItemsSource = vectors.AsEnumerable();
                }
            }
            catch(Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
            finally
            {
                SetReportWindowStatus(true);
            }
        }
        private void SetReportWindowStatus(bool isWindowEnable)
        {
            ReportWindow_progressBar.Name = "Loading...";
            ReportWindow_LayoutGrid.Visibility = (isWindowEnable ? Visibility.Visible:Visibility.Collapsed);
            ReportWindow_progressBar.IsIndeterminate = !isWindowEnable;
            ReportWindow_progressBar.Visibility = (isWindowEnable ? Visibility.Collapsed : Visibility.Visible);
        }
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetResumeReportDetails(). Using retrieved score information, chart controls are loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void webservice_GetResumeReportDetailsCompleted(object sender, GetResumeReportDetailsCompletedEventArgs e)
        {
            SetReportDetails(e.Result);
        }

        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetReportDetails(). Using retrieved score information,
        /// chart controls are loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the results of ReportDetails which contains statistics information.
        /// </param>
        void webservice_GetReportDetailsCompleted(object sender, GetReportDetailsCompletedEventArgs e)
        {
            SetReportDetails(e.Result);
        }
        /// <summary>
        /// Create chart which compares selected candidate's resume score with maximum resume score, 
        /// minimum resume score, average resume score, along with overall rank of candidate
        /// </summary>
        /// <param name="minMaxAvgTerm">
        /// A <see cref="SearchTerms_MinMaxReportDetails"/> that holds the minMaxAvg score values.
        /// </param>
        private void CreateSearchTermMinMaxAvgChart(SearchTerms_MinMaxReportDetails minMaxAvgTerm)
        {
            MinMaxAvgChartControl mc = new MinMaxAvgChartControl();
            mc.Width = 170;
            mc.Height = 150;
            mc.CreateChart(minMaxAvgTerm);
            ReportWindow_minMaxStackPanel.Children.Add(mc);
        }
        /// <summary>
        /// Create histogram chart showing selected candidate's score position amongst the scores of
        /// other matching candidates, along with rank for the search term
        /// </summary>
        /// <param name="scoreDetails">
        /// A list of <see cref="SearchTerms_ScorePercentage"/> that holds the score details.
        /// </param>
        private void CreateSearchTermHistogramChart(List<SearchTerms_ScorePercentage> scoreDetails)
        {

            var xMax = (from item in scoreDetails
                        select item.SCORE).Max();
            var xInterval = 0;

            xMax = Utility.RoundAxisMaxValue(Convert.ToInt32(xMax));

            xInterval = Convert.ToInt32((xMax) / 5);

            List<ScoreRangeChartValues> scores = new List<ScoreRangeChartValues>();

            ScoreRangeChartValues score;
            var selCandidateSearchTermScore = (from item in mainPage.scoreDetails.SearchTerms_ScoreDistribution.ToList()
                                               where item.CANDIDATE_ID == mainPage.selectedCandidate.CandidateId &&
                                               item.SEARCH_TERM == scoreDetails[0].SEARCH_TERM
                                               select item.SCORE);
            int selCandidateScore = 0;
            if (selCandidateSearchTermScore.Count() > 0)
            {
                selCandidateScore = Convert.ToInt32(selCandidateSearchTermScore.First());
            }
            else
            {
                if (mainPage.IsResume)
                    selCandidateScore = Convert.ToInt32(mainPage.SelectedCandidate.ResumeScore);
                else
                    selCandidateScore = Convert.ToInt32(mainPage.SelectedCandidate.AssessmentScore);
            }
            for (int index = 0; index < 5; index++)
            {
                score = new ScoreRangeChartValues();
                score.SCORE = Convert.ToInt32((xInterval * (index + 1)));
                score.NO_OF_CANDIDATES = (from item in scoreDetails
                                          where item.SCORE >= (xInterval * index) && item.SCORE < score.SCORE
                                          select item.SCORE).Count();

                //score.OPACITY = Convert.ToDecimal((1-0.1) * index);
                score.LABEL_VISIBILITY = (score.NO_OF_CANDIDATES == 0) ? "Collapsed" : "Visible";
                if (selCandidateScore <= score.SCORE && selCandidateScore > score.SCORE - xInterval)
                    score.GRADIENT_COLOR = Utility.CreateSelectedGradientBrush();
                else
                    score.GRADIENT_COLOR = Utility.CreateGradientBrush();
                scores.Add(score);
            }
            HistogramChartControl hc = new HistogramChartControl();
            hc.ScoreData = scores;
            hc.XMaximum = Convert.ToInt32(xMax);
            hc.XInterval = xInterval;
            var yMax = (from item in scores select item.NO_OF_CANDIDATES).Max();
            var yInterval = 0;

            yMax = Utility.RoundAxisMaxValue(Convert.ToInt32(yMax));

            yInterval = Convert.ToInt32((yMax) / 5);

            hc.YInterval = yInterval;
            hc.YMaximum = yMax;
            hc.XAxisTitle = scoreDetails[0].SEARCH_TERM;
            hc.CreateChart();
            this.ReportWindow_histogramScoreStackPanel.Children.Add(hc);

        }
    }
}

