﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [ClientRequest.cs]
// [File that represents the client request details with properties like client request number, 
// client id, position name, tag and created date]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the client request details like client request number, 
    /// client id, position name, tag and created date
    /// </summary>
    public class ClientRequest
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public ClientRequest()
        {
        }
        /// <summary>
        /// Property that will get or set the ClientRequestNumber.
        /// </summary>
        public string ClientRequestNumber
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the ClientID.
        /// </summary>
        public int ClientID
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the PositionName.
        /// </summary>
        public string PositionName
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the Tag.
        /// </summary>
        public string Tag
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the CreatedDate.
        /// </summary>
        public string CreatedDate
        {
            get;
            set;
        }
    }
}