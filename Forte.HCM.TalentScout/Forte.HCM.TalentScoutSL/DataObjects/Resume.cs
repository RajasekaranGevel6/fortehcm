﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Resume.cs]
// [File that represents the class with resume informations like name, contact info, executive summary
// technical skills, project, education, competency vector and references.]

#endregion

using System.Collections.Generic;

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the resume informations like name, contact info, executive summary
    /// technical skills, project, education, competency vector and references.
    /// </summary>
	public class Resume
	{
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public Resume()
		{
		}
        /// <summary>
        /// Property that will get or set the Name.
        /// </summary>
		public Name Name { set; get; }
        /// <summary>
        /// Property that will get or set the ContactInformation.
        /// </summary>
        public ContactInformation ContactInformation { set; get; }
        /// <summary>
        /// Property that will get or set the ExecutiveSummary.
        /// </summary>
        public string ExecutiveSummary { set; get; }
        /// <summary>
        /// Property that will get or set the TechnicalSkills.
        /// </summary>
        public TechnicalSkills TechnicalSkills { set; get; }
        /// <summary>
        /// Property that will get or set the Project.
        /// </summary>
        public List<Project> Project { set; get; }
        /// <summary>
        /// Property that will get or set the Education.
        /// </summary>
        public List<Education> Education { set; get; }
        /// <summary>
        /// Property that will get or set the Competencies.
        /// </summary>
        public List<CompetencyVector> Competencies { set; get; }
        /// <summary>
        /// Property that will get or set the References.
        /// </summary>
        public List<Reference> References { set; get; }
	}
}