﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [ContactInformation.cs]
// [File that represents the contact informations with properties like city, 
// country, state, postal code, phone, email address and website address.]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the contact informations like Street Address, City, Country, State
    /// Postal code, Phone, Email Address and Website address.
    /// </summary>
    public class ContactInformation
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public ContactInformation()
        {
        }
        private string _City;
        private string _State;
        private string _EmailAddress;
        /// <summary>
        /// Property that will get or set the StreetAddress.
        /// </summary>
        public string StreetAddress { set; get; }
        /// <summary>
        /// Property that will get or set the City.
        /// </summary>
        public string City
        {
            set { _City = value; }
            get { return _City; }
        }
        /// <summary>
        /// Property that will get or set the Country.
        /// </summary>
        public string Country { set; get; }
        /// <summary>
        /// Property that will get or set the State.
        /// </summary>
        public string State
        {
            set { _State = value; }
            get { return _State; }
        }
        /// <summary>
        /// Property that will get or set the PostalCode.
        /// </summary>
        public string PostalCode { set; get; }
        /// <summary>
        /// Property that will get or set the Phone.
        /// </summary>
        public PhoneNumber Phone { set; get; }
        /// <summary>
        /// Property that will get or set the EmailAddress.
        /// </summary>
        public string EmailAddress
        {
            set { _EmailAddress = value; }
            get { return _EmailAddress; }
        }
        /// <summary>
        /// Property that will get the TrimmedEmailAddress.
        /// </summary>
        public string TrimmedEmailAddress
        {
            get
            {
                if (_EmailAddress != null)
                    return (_EmailAddress.Length < 25) ? _EmailAddress : (_EmailAddress.Substring(0, 25) + "...");
                else
                    return null;
            }
        }
        /// <summary>
        /// Property that will get or set the WebSiteAddress.
        /// </summary>
        public WebSite WebSiteAddress { set; get; }
        /// <summary>
        /// Property that will get the TrimmedCityState.
        /// </summary>
        public string TrimmedCityState
        {
            get
            {
                return ((_City.Length < 10) ? _City : (_City.Substring(0, 10) + "...")) + "," +
                    ((_State.Length < 10) ? _State : (_State.Substring(0, 10) + "..."));
            }
        }
        /// <summary>
        /// Property that will get the ConcatenatedCityState.
        /// </summary>
        public string ConcatenatedCityState
        {
            get
            {
                return _City + " , " + _State;
            }
        }
    }
}