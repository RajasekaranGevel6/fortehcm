﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [SearchSummary.cs]
// [File that represents the class with summary details like number of candidates searched,
// matching candidates, number of sources selected and list of search terms.]

#endregion

#region Namespaces                                                              
using System.Collections.Generic;
using Forte.HCM.TalentScoutSL.DataObjects;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the summary informations like number of candidates searched,
    /// matching candidates, number of sources selected and list of search terms.
    /// </summary>
	public class SearchSummary
	{
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public SearchSummary()
		{
		}
        /// <summary>
        /// Property that will get or set the CandidatesSearched.
        /// </summary>
		public int CandidatesSearched { set; get; }
        /// <summary>
        /// Property that will get or set the MatchingCandidates.
        /// </summary>
		public int MatchingCandidates { set; get; }
        /// <summary>
        /// Property that will get or set the SourcesSearched.
        /// </summary>
		public int SourcesSearched { set; get; }
        /// <summary>
        /// Property that will get or set the SearchTerms.
        /// </summary>
		public List<SearchTerm> SearchTerms { set; get; }
	}
}