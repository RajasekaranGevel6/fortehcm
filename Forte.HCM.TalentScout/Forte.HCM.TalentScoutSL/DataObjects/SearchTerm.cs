﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [SearchTerm.cs]
// [File that represents the class with search terms like keyword, 
// weightage and number of candidates matched.]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the informations about search terms like keyword, 
    /// weightage and number of candidates matched.
    /// </summary>
	public class SearchTerm
	{
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public SearchTerm()
		{
		}
        /// <summary>
        /// Property that will get or set the Keyword.
        /// </summary>
		public string Keyword { set; get; }
        /// <summary>
        /// Property that will get or set the Weightage.
        /// </summary>
		public int Weightage { set; get; }
        /// <summary>
        /// Property that will get or set the NoOfCandidatesMatched.
        /// </summary>
		public int NoOfCandidatesMatched{set; get;}
	}
}