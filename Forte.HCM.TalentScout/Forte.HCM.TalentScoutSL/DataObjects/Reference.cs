﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Reference.cs]
// [File that represents the class with reference informations like name, orgainzation, 
// relation and contact information.]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the reference informations like name, 
    /// organization, relation and contact information.
    /// </summary>
	public class Reference
	{
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public Reference()
		{
		}
        /// <summary>
        /// Property that will get or set the Name.
        /// </summary>
		public string Name {set; get;}
        /// <summary>
        /// Property that will get or set the Organization.
        /// </summary>
        public string Organization {set; get;}
        /// <summary>
        /// Property that will get or set the Relation.
        /// </summary>
        public string Relation {set; get;}
        /// <summary>
        /// Property that will get or set the ContactInformation.
        /// </summary>
        public ContactInformation ContactInformation {set; get;}
	}
}