﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [CompetencyVector.cs]
// [File that represents the competency vector informations with properties like 
// skill, no of years, recency, certification and certification details.]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the competency vector informations like Skill, noOfYears,
    /// Recency, Certification and Certification details.
    /// </summary>
    public class CompetencyVector
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public CompetencyVector()
        {
        }
        /// <summary>
        /// Property that will get or set the Skill.
        /// </summary>
        public string Skill { set; get; }
        /// <summary>
        /// Property that will get or set the NoOfYears.
        /// </summary>
        public string NoOfYears { set; get; }
        /// <summary>
        /// Property that will get or set the Recency.
        /// </summary>
        public string Recency { set; get; }
        /// <summary>
        /// Property that will get or set the Certification.
        /// </summary>
        public string Certification { set; get; }
        /// <summary>
        /// Property that will get or set the Certification Details.
        /// </summary>
        public string CertificationDetails { set; get; }
    }
}