﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Candidate.cs]
// [File that represents the candidate details with properties like candidate id, 
// candidate name, score and resume]

#endregion

using System.Windows.Media.Imaging;
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the candidate informations like CandidateId, Name, 
    /// Score, Percentage and Resume data.
    /// </summary>
    public class Candidate
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public Candidate()
        {
        }
        /// <summary>
        /// Property that will get or set the CandidateId.
        /// </summary>
        public int CandidateId
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the CandidateResumeId.
        /// </summary>
        public int CandidateResumeId
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the Name.
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the FirstName.
        /// </summary>
        public string FirstName
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the Email.
        /// </summary>
        public string Email
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the ResumeScore.
        /// </summary>
        public double ResumeScore
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the Percentage.
        /// </summary>
        public double Percentage
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the AssessmentScore.
        /// </summary>
        public double AssessmentScore
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the CloningScore.
        /// </summary>
        public double CloningScore
        {
            get;
            set;
        }
        /// <summary>
        /// Property that will get or set the Resume.
        /// </summary>
        public Resume Resume { set; get; }
        /// <summary>
        /// Property that will get or set the IsExactMatch.
        /// </summary>
        public bool IsExactMatch { set; get; }

        public double Score
        {
            get;
            set;
        }
        public string IsPicked
        {
            get;
            set;
        }
        public string Recruiter
        {
            get;
            set;
        }
        public string HasActivity
        {
            get;
            set;
        }

        public byte[] CandidateImage
        {
            get;
            set;
        }

        public BitmapImage CandImage
        {
            get;
            set;
        }

        
       
    }
}