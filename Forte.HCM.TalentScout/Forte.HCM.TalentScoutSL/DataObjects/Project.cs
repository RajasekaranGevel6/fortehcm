﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Project.cs]
// [File that represents the class with project informations like start date, end date, client name,
// client industry, project name, project location, position title, project description, role and environment.]

#endregion

using System;

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the project informations like start date, end date, client name,
    /// client industry, project name, project location, position title, project description,
    /// role and enviornment.
    /// </summary>
	public class Project
	{
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public Project()
		{
		}
        /// <summary>
        /// Property that will get or set the StartDate.
        /// </summary>
		public DateTime StartDate {set; get;}
        /// <summary>
        /// Property that will get or set the EndDate.
        /// </summary>
        public DateTime EndDate {set; get;}
        /// <summary>
        /// Property that will get or set the ClientName.
        /// </summary>
        public string ClientName {set; get;}
        /// <summary>
        /// Property that will get or set the ClientIndustry.
        /// </summary>
        public string ClientIndustry {set; get;}
        /// <summary>
        /// Property that will get or set the ProjectName.
        /// </summary>
        public string ProjectName {set; get;}
        /// <summary>
        /// Property that will get or set the ProjectLocation.
        /// </summary>
        public Location ProjectLocation {set; get;}
        /// <summary>
        /// Property that will get or set the PositionTitle.
        /// </summary>
        public string PositionTitle {set; get;}
        /// <summary>
        /// Property that will get or set the ProjectDescription.
        /// </summary>
        public string ProjectDescription {set; get;}
        /// <summary>
        /// Property that will get or set the Role.
        /// </summary>
        public string Role {set; get;}
        /// <summary>
        /// Property that will get or set the Environment.
        /// </summary>
        public string Environment {set; get;}
	}
}