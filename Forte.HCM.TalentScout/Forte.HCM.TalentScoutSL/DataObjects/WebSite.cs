﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [WebSite.cs]
// [File that represents the class with website informations like personal, linkedIn and facebook links.]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the website informations like personal, linkedIn and facebook
    /// </summary>
	public class WebSite
	{
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public WebSite()
		{
		}
        /// <summary>
        /// Property that will get or set the Personal link.
        /// </summary>
		public string Personal {set; get;}
        /// <summary>
        /// Property that will get or set the LinkedIn link.
        /// </summary>
        public string LinkedIn {set; get;}
        /// <summary>
        /// Property that will get or set the FaceBook link.
        /// </summary>
        public string FaceBook {set; get;}
	}
}