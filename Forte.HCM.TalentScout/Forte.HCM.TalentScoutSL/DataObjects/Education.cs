﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Education.cs]
// [File that represents the education details with properties like degree name, school name, 
// location, graduation date and specialization]

#endregion

using System;

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the education informations like Degree name, School name, School location
    /// Graduation date, specialization
    /// </summary>
    public class Education
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public Education()
        {
        }
        /// <summary>
        /// Property that will get or set the DegreeName.
        /// </summary>
        public string DegreeName { set; get; }
        /// <summary>
        /// Property that will get or set the SchoolName.
        /// </summary>
        public string SchoolName { set; get; }
        /// <summary>
        /// Property that will get or set the SchoolLocation.
        /// </summary>
        public Location SchoolLocation;
        /// <summary>
        /// Property that will get or set the GraduationDate.
        /// </summary>
        public DateTime GraduationDate { set; get; }
        /// <summary>
        /// Property that will get or set the Specialization.
        /// </summary>
        public string Specialization { set; get; }
        /// <summary>
        /// Property that will get or set the GPA.
        /// </summary>
        public string GPA { set; get; }
    }
}