﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
namespace Forte.HCM.TalentScoutSL.DataObjects
{
    public class EllipsePositions : System.Collections.Generic.List<EllipsePosition>
    {
        public bool CheckBoundries(Point plottingPosition)
        {
            var isOverlapping = (from position in this
                                where (Math.Abs(plottingPosition.X - position.LeftPosition) < 70 && Math.Abs(plottingPosition.Y - position.TopPosition) < 70)
                                select position).Count();
            if (Convert.ToInt32(isOverlapping) > 0)
                return true;
            else
                return false;
        }
        public EllipsePosition GetEllipsePosition(int candidateIndex)
        {
            foreach (EllipsePosition ellipsePos in this)
            {
                if (ellipsePos.CandidateIndex == candidateIndex)
                    return ellipsePos;
            }
            return null;
        }

    }
}
