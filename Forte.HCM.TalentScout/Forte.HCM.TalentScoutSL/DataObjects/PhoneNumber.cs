﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [PhoneNumber.cs]
// [File that represents the class with phone informations like office, residence and mobile numbers]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the phone informations like office, residence and mobile numbers
    /// </summary>
	public class PhoneNumber
	{
        private string _Office;
        private string _Residence;

        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public PhoneNumber()
		{
		}
        /// <summary>
        /// Property that will get or set the Office.
        /// </summary>
        public string Office
        {
            set { _Office = value; }
            get { return _Office; }
        }
        /// <summary>
        /// Property that will get or set the Residence.
        /// </summary>
        public string Residence
        {
            set { _Residence = value; }
            get { return _Residence; }
        }
        /// <summary>
        /// Property that will get or set the Mobile.
        /// </summary>
        public string Mobile {set; get;}
        /// <summary>
        /// Property that will get the TrimmedOffice.
        /// </summary>
        public string TrimmedOffice
        {
            get
            {
                return (_Office.Length < 15) ? _Office : (_Office.Substring(0, 15) + "...");
            }
        }
        /// <summary>
        /// Property that will get the TrimmedResidence.
        /// </summary>
        public string TrimmedResidence
        {
            get
            {
                return (_Residence.Length < 15) ? _Residence : (_Residence.Substring(0, 15) + "...");
            }
        }
	}
}