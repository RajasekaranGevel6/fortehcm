﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [TechnicalSkills.cs]
// [File that represents the class with technical skill details like language, database, uiTools and operating system.]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the technical skill informations
    /// </summary>
	public class TechnicalSkills
	{
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public TechnicalSkills()
		{
		}
        /// <summary>
        /// Property that will get or set the Language.
        /// </summary>
		public string Language {set; get;}
        /// <summary>
        /// Property that will get or set the Database.
        /// </summary>
        public string Database {set; get;}
        /// <summary>
        /// Property that will get or set the UITools.
        /// </summary>
        public string UITools {set; get;}
        /// <summary>
        /// Property that will get or set the OperatingSystem.
        /// </summary>
        public string OperatingSystem {set; get;}
	}
}