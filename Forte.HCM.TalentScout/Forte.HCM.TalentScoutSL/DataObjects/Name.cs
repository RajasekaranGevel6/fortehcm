﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Name.cs]
// [File that represents the class with name informations like first name, middle name and last name]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the name informations like First name, Middle name and Last name.
    /// </summary>
    public class Name
    {
        private string _FirstName;
        private string _LastName;

        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public Name()
        {
        }
        /// <summary>
        /// Property that will get or set the FirstName.
        /// </summary>
        public string FirstName
        {
            set { _FirstName = value; }
            get { return _FirstName; }
        }
        /// <summary>
        /// Property that will get or set the MiddleName.
        /// </summary>
        public string MiddleName { set; get; }
        /// <summary>
        /// Property that will get or set the LastName.
        /// </summary>
        public string LastName
        {
            set { _LastName = value; }
            get { return _LastName; }
        }
        /// <summary>
        /// Property that will get the ConcatenatedNames.
        /// </summary>
        public string ConcatenatedNames
        {
            get
            {
                return _FirstName + " " + _LastName;
            }
        }
        /// <summary>
        /// Property that will get the TrimedFirstName.
        /// </summary>
        public string TrimedConcatenatedNames
        {
            get {
                if (ConcatenatedNames != null)
                    return (ConcatenatedNames.Length < 25) ? ConcatenatedNames : (ConcatenatedNames.Substring(0, 25) + ((ConcatenatedNames.Length > 25) ? "..." : ""));
                else
                    return "";
            }
        }
    }
}