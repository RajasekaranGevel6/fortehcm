﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Location.cs]
// [File that represents the location details with properties like city, state and country ]

#endregion
namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class contains the location informations like City, State and Country.
    /// </summary>
    public class Location
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public Location()
        {
        }
        /// <summary>
        /// Property that will get or set the City.
        /// </summary>
        public string City { set; get; }
        /// <summary>
        /// Property that will get or set the State.
        /// </summary>
        public string State { set; get; }
        /// <summary>
        /// Property that will get or set the Country.
        /// </summary>
        public string Country { set; get; }
    }
}