﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [IsolatedFileSettings.cs]
// [File that represents the class that write, reads and deletes the session state values in isolated file.]

#endregion

#region Namespaces                                                              
using System;
using System.IO;
using System.IO.IsolatedStorage;
using Forte.HCM.TalentScoutSL.Utils;
#endregion

namespace Forte.HCM.TalentScoutSL.DataObjects
{
    /// <summary>
    /// This class writes the log file, reads and deletes it
    /// </summary>
    public class IsolatedFileSettings
    {
        /// <summary>
        /// Method that writes the log in isolated file.
        /// </summary>
        /// <param name="ValueToWrite"></param>
        public static void WriteToIsolatedFile(string ValueToWrite)
        {
            IsolatedStorageFile FileStorage = null;
            IsolatedStorageFileStream FileStream = null;
            StreamWriter writer = null;
            try
            {
                if (ValueToWrite == "")
                    DeleteIsolatedFile();
                FileStorage = IsolatedStorageFile.GetUserStoreForApplication();
                FileStream = FileStorage.OpenFile("Log.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                writer = new StreamWriter(FileStream);
                //Isolated file is used to write the login credentials (remember me). In this file, time is not required.
                //if (ValueToWrite != "")
                //    ValueToWrite = System.DateTime.Now.ToString() + "--" + ValueToWrite;
                writer.Write(ValueToWrite);
            }
            finally
            {
                if (writer != null)
                {
                    writer.Flush();
                    writer.Close();
                    writer = null;
                }
                if (FileStream != null) { FileStream.Close(); FileStream = null; }
                if (FileStorage != null) FileStorage = null;
            }
        }
        /// <summary>
        /// Method that opens the log file to write the session state value.
        /// </summary>
        /// <returns></returns>
        public static string GetIsolatedFileValue()
        {
            IsolatedStorageFile FileStorage = null;
            IsolatedStorageFileStream FileStream = null;
            StreamReader reader = null;
            try
            {
                FileStorage = IsolatedStorageFile.GetUserStoreForApplication();
                FileStream = FileStorage.OpenFile("Log.txt", FileMode.OpenOrCreate, FileAccess.Read);
                reader = new StreamReader(FileStream);
                string ReturnValue = reader.ReadToEnd();
                //Isolated file is used to write the login credentials (remember me). In this file, time is not required.
                //if (DateTime.Now.Subtract(Convert.ToDateTime(ReturnValue.Substring(0, ReturnValue.IndexOf('-'))))
                //    .Minutes >= Convert.ToInt32(App_GlobalResources.TalentScoutResource.TalentScout_SessionStateValue))
                //    return "";
                //else
                    return ReturnValue;
            }
            catch { }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
                if (FileStream != null) { FileStream.Close(); FileStream = null; }
                if (FileStorage != null) FileStorage = null;

            }
            return "";
        }
        /// <summary>
        /// Method that deletes the log file.
        /// </summary>
        public static void DeleteIsolatedFile()
        {
            IsolatedStorageFile FileStorage = null;
            try
            {
                FileStorage = IsolatedStorageFile.GetUserStoreForApplication();
                if (FileStorage.FileExists("Log.txt"))
                    FileStorage.DeleteFile("Log.txt");
            }
            finally
            {
                if (FileStorage != null) FileStorage = null;
            }
        }
    }
}
