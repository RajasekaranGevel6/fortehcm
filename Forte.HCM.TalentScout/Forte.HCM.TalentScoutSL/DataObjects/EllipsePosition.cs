﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Forte.HCM.TalentScoutSL.DataObjects
{
    public class EllipsePosition
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public EllipsePosition()
		{
		}
        public EllipsePosition(Point ellipsePosition, double angle,int candidateIndex)
        {
            LeftPosition = ellipsePosition.X;
            TopPosition = ellipsePosition.Y;
            Angle = angle;
            CandidateIndex = candidateIndex;
        }
        /// <summary>
        /// Property that will get or set the Left position.
        /// </summary>
        public double LeftPosition { set; get; }
        /// <summary>
        /// Property that will get or set the Top position.
        /// </summary>
        public double TopPosition { set; get; }
        /// <summary>
        /// Property that will get or set the angle.
        /// </summary>
        public double Angle { set; get; }
        /// <summary>
        /// Property that will get or set the candidate index.
        /// </summary>
        public int CandidateIndex { set; get; }
    }
}
