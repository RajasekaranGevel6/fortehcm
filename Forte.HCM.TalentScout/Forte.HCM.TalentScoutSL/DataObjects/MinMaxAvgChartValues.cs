﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [MinMaxAvgChartValues.cs]
// [File that represents the class with details like color, opacity, x-axis and y-axis values for chart display]

#endregion

using System.Windows.Media;

namespace Forte.HCM.TalentScoutSL.DataObjects
{
    /// <summary>
    /// This class contains the details like color, opacity, x-axis and y-axis values for chart display
    /// </summary>
    public class MinMaxAvgChartValues
    {
        private RadialGradientBrush _COLOR;
        private decimal _OPACITY;
        private int _YAXIS_VALUE;
        private string _XAXIS_VALUE;
        private string _LABEL_VISIBILITY;
        /// <summary>
        /// Property that will get or set the LABEL_VISIBILITY.
        /// </summary>
        public string LABEL_VISIBILITY
        {
            get
            {
                return this._LABEL_VISIBILITY;
            }
            set
            {
                if ((this._LABEL_VISIBILITY != value))
                {
                    this._LABEL_VISIBILITY = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the GRADIENT_COLOR.
        /// </summary>
        public RadialGradientBrush GRADIENT_COLOR
        {
            get
            {
                return this._COLOR;
            }
            set
            {
                if ((this._COLOR != value))
                {
                    this._COLOR = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the OPACITY.
        /// </summary>
        public decimal OPACITY
        {
            get
            {
                return this._OPACITY;
            }
            set
            {
                if ((this._OPACITY != value))
                {
                    this._OPACITY = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the YAXIS_VALUE.
        /// </summary>
        public int YAXIS_VALUE
        {
            get
            {
                return this._YAXIS_VALUE;
            }
            set
            {
                if ((this._YAXIS_VALUE != value))
                {
                    this._YAXIS_VALUE = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the XAXIS_VALUE.
        /// </summary>
        public string XAXIS_VALUE
        {
            get
            {
                return this._XAXIS_VALUE;
            }
            set
            {
                if ((this._XAXIS_VALUE != value))
                {
                    this._XAXIS_VALUE = value;
                }
            }
        }
    }
}
