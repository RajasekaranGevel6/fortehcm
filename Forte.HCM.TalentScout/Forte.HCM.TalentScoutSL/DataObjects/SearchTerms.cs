﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [SearchTerms.cs]
// [File that represents the class to get the search keywords string, total assigned weight and balance weight]

#endregion

#region Namespaces                                                              
using System.Text;
using System.Linq;
#endregion

namespace Forte.HCM.TalentScoutSL.DataObjects
{
    /// <summary>
    /// This class contains the informations like search keywords string, total assigned weight and balance weight
    /// </summary>
    public class SearchTerms : System.Collections.Generic.List<SearchTerm>
    {
        int totalAssignedWeight = 0;
        int assignedBalanceWeight = 0;
        string htmlSearchTerms = string.Empty;
        /// <summary>
        /// This property gets/sets the assigned weight balance.
        /// </summary>
        public int AssignedBalanceWeight
        {
            get
            {
                return assignedBalanceWeight;
            }
            set
            {
                assignedBalanceWeight = value;
            }
        }
        /// <summary>
        /// This method returns the total assigned weight by the user.
        /// </summary>
        /// <returns></returns>
        public int GetTotalAssignedWeight()
        {
            totalAssignedWeight = 0;

            totalAssignedWeight = (from item in this select item.Weightage).Sum();
            return totalAssignedWeight;
        }
        /// <summary>
        /// Method that joins the search string by combining the weightage and keywords with '-' and ','
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the search keywords string
        /// </returns>
        public string GetSearchKeywordsWithWeightage()
        {
            StringBuilder searchKeywords = null;
            try
            {
                searchKeywords = new StringBuilder();
                foreach (SearchTerm item in this)
                {
                    if (item.Weightage == 0)
                        continue;
                    searchKeywords.Append(item.Keyword);
                    searchKeywords.Append("-");
                    searchKeywords.Append(item.Weightage);
                    searchKeywords.Append(",");
                }
                return searchKeywords.ToString().TrimEnd(',');
            }
            finally
            {
                if (searchKeywords != null) searchKeywords = null;
            }
        }
        /// <summary>
        /// This method returns the search terms which has 0 weightage
        /// </summary>
        /// <returns>
        /// A <see cref="SearchTerms"/> that holds the search terms with 0 weightage.
        /// </returns>
        private SearchTerms GetWeightNotAssignedSearchTerms()
        {
            SearchTerms weightNotAssignedSearchTerms = new SearchTerms();

            foreach (SearchTerm item in this)
            {
                if (item.Weightage == 0)
                {
                    weightNotAssignedSearchTerms.Add(item);
                }
            }

            return weightNotAssignedSearchTerms;
        }
        /// <summary>
        /// Method that returns the search terms string.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the search terms separated by comma.
        /// </returns>
        public string GetHtmlSearchTerms()
        {
            // Remove the last comma
            if (htmlSearchTerms.EndsWith(","))
                htmlSearchTerms = htmlSearchTerms.Remove(htmlSearchTerms.LastIndexOf(","));

            return htmlSearchTerms;
        }
        /// <summary>
        /// This method returns the search string with balanced weight.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the search string with balanced weightage.
        /// </returns>
        public string GetBalancedWeightSearchTerms()
        {
            int weightNotAssignedTermsCount = 0;

            string equalizedWeightSearchTerms = "";
            SearchTerms WeightNotAssignedSearchTerms = GetWeightNotAssignedSearchTerms();
            weightNotAssignedTermsCount = WeightNotAssignedSearchTerms.Count;

            int equalizedWeight = 0;
            // Calculate the equalizedWeight
            if (weightNotAssignedTermsCount != 0)
            {
                equalizedWeight = (100 - totalAssignedWeight) / weightNotAssignedTermsCount;
                // Assign the remaining weightage to last search term.
                WeightNotAssignedSearchTerms[weightNotAssignedTermsCount - 1].Weightage =
                    equalizedWeight + (100 - (totalAssignedWeight + (equalizedWeight * weightNotAssignedTermsCount)));
            }
            else
                assignedBalanceWeight = 100 - totalAssignedWeight;

            // Assign the weightage value if it is 0. Also concatenate the search string with the weightage.
            foreach (SearchTerm item in this)
            {
                if (item.Weightage == 0)
                    item.Weightage = equalizedWeight;
                if(item.Weightage==0)
                {
                    htmlSearchTerms += "<F>" + item.Keyword + "[" + item.Weightage + "]" + "</F>,";
                }
                else
                    htmlSearchTerms += item.Keyword + "[" + item.Weightage + "],";
                
                equalizedWeightSearchTerms += item.Keyword + "[" + item.Weightage + "],";
            }
            // Remove the last comma
            if (equalizedWeightSearchTerms.EndsWith(","))
                equalizedWeightSearchTerms = equalizedWeightSearchTerms.Remove(equalizedWeightSearchTerms.LastIndexOf(","));

            return equalizedWeightSearchTerms;
        }
        public string GetSearchKeywords()
        {
            StringBuilder searchKeywords = null;
            try
            {
                searchKeywords = new StringBuilder();
                foreach (SearchTerm item in this)
                {
                    if (item.Weightage == 0)
                        continue;
                    searchKeywords.Append(item.Keyword);
                   
                    searchKeywords.Append(",");
                }
                return searchKeywords.ToString().TrimEnd(',');
            }
            finally
            {
                if (searchKeywords != null) searchKeywords = null;
            }
        }
        public string GetKeywordsAlone()
        {
            StringBuilder searchKeywords = null;
            try
            {
                searchKeywords = new StringBuilder();
                foreach (SearchTerm item in this)
                {
                    
                    searchKeywords.Append(item.Keyword);

                    searchKeywords.Append(",");
                }
                return searchKeywords.ToString().TrimEnd(',');
            }
            finally
            {
                if (searchKeywords != null) searchKeywords = null;
            }
        }
    }
}
