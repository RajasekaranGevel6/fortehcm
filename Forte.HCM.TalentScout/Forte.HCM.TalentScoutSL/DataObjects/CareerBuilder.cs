﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Forte.HCM.TalentScoutSL.DataObjects
{
    public class CareerBuilder
    {
        /// <summary>
        /// Gets the job type code.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetJobTypeCode()
        {
            Dictionary<string, string> JobTypeCode = new Dictionary<string, string>();
            JobTypeCode.Add("Select Category...", "");
            JobTypeCode.Add("Accounting", "JN001");
            JobTypeCode.Add("Admin - Clerical", "JN002");
            JobTypeCode.Add("Automotive", "JN054");
            JobTypeCode.Add("Banking", "JN038");
            JobTypeCode.Add("Biotech", "JN053");
            JobTypeCode.Add("Business Development", "JN019");
            JobTypeCode.Add("Business Opportunity", "JN059");
            JobTypeCode.Add("Construction", "JN043");
            JobTypeCode.Add("Consultant", "JN020");
            JobTypeCode.Add("Customer Service", "JN003");
            JobTypeCode.Add("Design", "JN021");
            JobTypeCode.Add("Distribution - Shipping", "JN027");
            JobTypeCode.Add("Education", "JN031");
            JobTypeCode.Add("Engineering", "JN004");
            JobTypeCode.Add("Entry Level", "JN022");
            JobTypeCode.Add("Executive", "JN018");
            JobTypeCode.Add("Facilities", "JN017");
            JobTypeCode.Add("Finance", "JN005");
            JobTypeCode.Add("Franchise", "JN060");
            JobTypeCode.Add("General Business", "JN006");
            JobTypeCode.Add("General Labor", "JN051");
            JobTypeCode.Add("Government", "JN046");
            JobTypeCode.Add("Government - Federal", "JN070");
            JobTypeCode.Add("Grocery", "JN055");
            JobTypeCode.Add("Health Care", "JN023");
            JobTypeCode.Add("Hospitality - Hotel", "JN040");
            JobTypeCode.Add("Human Resources", "JN007");
            JobTypeCode.Add("Information Technology", "JN008");
            JobTypeCode.Add("Installation - Maint - Repair", "JN056");
            JobTypeCode.Add("Insurance", "JN034");
            JobTypeCode.Add("Inventory", "JN015");
            JobTypeCode.Add("Legal", "JN030");
            JobTypeCode.Add("Legal Admin", "JN041");
            JobTypeCode.Add("Management", "JN037");
            JobTypeCode.Add("Manufacturing", "JN029");
            JobTypeCode.Add("Marketing", "JN009");
            JobTypeCode.Add("Media - Journalism - Newspaper", "JN047");
            JobTypeCode.Add("Nonprofit - Social Services", "JN058");
            JobTypeCode.Add("Nurse", "JN050");
            JobTypeCode.Add("Other", "JN010");
            JobTypeCode.Add("Pharmaceutical", "JN049");
            JobTypeCode.Add("Professional Services", "JN024");
            JobTypeCode.Add("Purchasing - Procurement", "JN016");
            JobTypeCode.Add("QA - Quality Control", "JN025");
            JobTypeCode.Add("Real Estate", "JN057");
            JobTypeCode.Add("Research", "JN026");
            JobTypeCode.Add("Restaurant - Food Service", "JN035");
            JobTypeCode.Add("Retail", "JN033");
            JobTypeCode.Add("Sales", "JN011");
            JobTypeCode.Add("Science", "JN012");
            JobTypeCode.Add("Skilled Labor - Trades", "JN013");
            JobTypeCode.Add("Strategy - Planning", "JN028");
            JobTypeCode.Add("Supply Chain", "JN014");
            JobTypeCode.Add("Telecommunications", "JN048");
            JobTypeCode.Add("Training", "JN032");
            JobTypeCode.Add("Transportation", "JN044");
            JobTypeCode.Add("Veterinary Services", "JN069");
            JobTypeCode.Add("Warehouse", "JN045");
            return JobTypeCode;
        }

        /// <summary>
        /// Gets the using dropdown.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetUsingDropdown()
        {
            Dictionary<string, string> _usingDropDownDatasource = new Dictionary<string, string>();
            _usingDropDownDatasource.Add("All of these words", "ALL");
            _usingDropDownDatasource.Add("Any of these words", "ANY");
            _usingDropDownDatasource.Add("Boolean", "BOO");
            _usingDropDownDatasource.Add("Exact Phrase", "EXACT");
            return _usingDropDownDatasource;
        }

        /// <summary>
        /// Gets the state code.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetStateCode()
        {
            Dictionary<string, string> StateCode = new Dictionary<string, string>();
            StateCode.Add("Alabama", "AL");
            StateCode.Add("Alaska", "AK");
            StateCode.Add("Arizona", "AZ");
            StateCode.Add("Arkansas", "AR");
            StateCode.Add("California", "CA");
            StateCode.Add("Colorado", "CO");
            StateCode.Add("Connecticut", "CT");
            StateCode.Add("Delaware", "DE");
            StateCode.Add("Dist of Columbia", "DC");
            StateCode.Add("Florida", "FL");
            StateCode.Add("Georgia", "GA");
            StateCode.Add("Hawaii", "HI");
            StateCode.Add("Idaho", "ID");
            StateCode.Add("Illinois", "IL");
            StateCode.Add("Indiana", "IN");
            StateCode.Add("Iowa", "IA");
            StateCode.Add("Kansas", "KS");
            StateCode.Add("Kentucky", "KY");
            StateCode.Add("Louisiana", "LA");
            StateCode.Add("Maine", "ME");
            StateCode.Add("Maryland", "MD");
            StateCode.Add("Massachusetts", "MA");
            StateCode.Add("Michigan", "MI");
            StateCode.Add("Minnesota", "MN");
            StateCode.Add("Mississippi", "MS");
            StateCode.Add("Missouri", "MO");
            StateCode.Add("Montana", "MT");
            StateCode.Add("Nebraska", "NE");
            StateCode.Add("Nevada", "NV");
            StateCode.Add("New Hampshire", "NH");
            StateCode.Add("New Jersey", "NJ");
            StateCode.Add("New Mexico", "NM");
            StateCode.Add("New York", "NY");
            StateCode.Add("North Carolina", "NC");
            StateCode.Add("North Dakota", "ND");
            StateCode.Add("Ohio", "OH");
            StateCode.Add("Oklahoma", "OK");
            StateCode.Add("Oregon", "OR");
            StateCode.Add("Pennsylvania", "PA");
            StateCode.Add("Rhode Island", "RI");
            StateCode.Add("South Carolina", "SC");
            StateCode.Add("South Dakota", "SD");
            StateCode.Add("Tennessee", "TN");
            StateCode.Add("Texas", "TX");
            StateCode.Add("Utah", "UT");
            StateCode.Add("Vermont", "VT");
            StateCode.Add("Virginia", "VA");
            StateCode.Add("Washington", "WA");
            StateCode.Add("West Virginia", "WV");
            StateCode.Add("Wisconsin", "WI");
            StateCode.Add("Wyoming", "WY");
            return StateCode;
        }

        /// <summary>
        /// Gets the degree.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetDegree()
        {
            Dictionary<string, string> Degree = new Dictionary<string, string>();
            
            Degree.Add("None", "CE3");
            Degree.Add("Vocational", "CE30");
            Degree.Add("High School", "CE31");
            Degree.Add("Associate Degree", "CE32");
            Degree.Add("Bachelor's Degree", "CE321");
            Degree.Add("Master's Degree", "CE3210");
            Degree.Add("Doctorate", "CE3211");
            return Degree;
        }

        /// <summary>
        /// Gets the work experience level of travel.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetWorkExperienceLevelOfTravel()
        {
            Dictionary<string, string> ExperienceLevelOfTravel = new Dictionary<string, string>();
            ExperienceLevelOfTravel.Add("", "Select");
            ExperienceLevelOfTravel.Add("Negligible", "DT3");
            ExperienceLevelOfTravel.Add("Up to 25%", "DT32");
            ExperienceLevelOfTravel.Add("Up to 50%", "DT321");
            ExperienceLevelOfTravel.Add("Road Warrior", "DT3210");
            return ExperienceLevelOfTravel;
        }

        /// <summary>
        /// Gets the work experience level of travel.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetWorkStatus()
        {
            Dictionary<string, string> WorkStatus = new Dictionary<string, string>();
            WorkStatus.Add("Is a citizen of this country", "ctct");
            WorkStatus.Add("Is authorized to work for any employer in this country", "ctay");
            WorkStatus.Add("Is authorized to work for their current employer in this country", "ctem");
            WorkStatus.Add("Is seeking authorization to work in this country", "ctno");
            WorkStatus.Add("Is authorized to work in the EU", "euay");
            WorkStatus.Add("Green Card Holder", "ctgr");
            WorkStatus.Add("Not Specified", "ctns");
            return WorkStatus;
        }

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetLanguage()
        {
            Dictionary<string, string> LanguageCode = new Dictionary<string, string>();
            LanguageCode.Add("Select a Language...", "");
            LanguageCode.Add("Albanian", "LASQ");
            LanguageCode.Add("Arabic", " LAAR");
            LanguageCode.Add("Armenian", "LAHY");
            LanguageCode.Add("Assamese", "LAAS");
            LanguageCode.Add("Bengali", " LABN");
            LanguageCode.Add("Bulgarian", "LABU");
            LanguageCode.Add("Cambodian", "LACM");
            LanguageCode.Add("Catalán", "LACA");
            LanguageCode.Add("Chinese-Cantonese", "LAHK");
            LanguageCode.Add("Chinese-Chinois", "LACH");
            LanguageCode.Add("Chinese-Mandarin", "LAZH");
            LanguageCode.Add("Chinese-Taiwanese", "LATW");
            LanguageCode.Add("Croatian", "LACR");
            LanguageCode.Add("Czech", "LACS");
            LanguageCode.Add("Danish", "LADA");
            LanguageCode.Add("Dutch", "LANL");
            LanguageCode.Add("English", "LAEN");
            LanguageCode.Add("Estonian", "LAET");
            LanguageCode.Add("Euskera", "LAEU");
            LanguageCode.Add("Farsi", "LAFO");
            LanguageCode.Add("Finnish", "LAFI");
            LanguageCode.Add("French", "LAFR");
            LanguageCode.Add("German", "LADE");
            LanguageCode.Add("Greek", "LAEL");
            LanguageCode.Add("Gujarati", "LAGU");
            LanguageCode.Add("Hebrew", "LAHE");
            LanguageCode.Add("Hindi", "LAHI");
            LanguageCode.Add("Hungarian", "LAHU");
            LanguageCode.Add("Icelandic", "LAIS");
            LanguageCode.Add("Indonesian", "LAFJ");
            LanguageCode.Add("Italian", "LAIT");
            LanguageCode.Add("Japanese", "LAJA");
            LanguageCode.Add("Kannada", "LAKN");
            LanguageCode.Add("Kashmiri", "LAKS");
            LanguageCode.Add("Korean", "LAKO");
            LanguageCode.Add("Latvian", "LALV");
            LanguageCode.Add("Lithuanian", "LALT");
            LanguageCode.Add("Macedonian", "LAMC");
            LanguageCode.Add("Malayalam", "LAML");
            LanguageCode.Add("Norwegian", "LANO");
            LanguageCode.Add("Oriya", "LAOR");
            LanguageCode.Add("Pashto", "LAPS");
            LanguageCode.Add("Polish", "LAPL");
            LanguageCode.Add("Portuguese", "LAPT");
            LanguageCode.Add("Punjabi", "LAPA");
            LanguageCode.Add("Romanian", "LARO");
            LanguageCode.Add("Russian", "LARU");
            LanguageCode.Add("Sanskrit", "LASA");
            LanguageCode.Add("Serbian", "LASB");
            LanguageCode.Add("Sindhi", "LASD");
            LanguageCode.Add("Spanish", "LAES");
            LanguageCode.Add("Swedish", "LASV");
            LanguageCode.Add("Tamil", "LATA");
            LanguageCode.Add("Telugu", "LATE");
            LanguageCode.Add("Turkish", "LATR");
            LanguageCode.Add("Ukrainian", "LAKR");
            LanguageCode.Add("Urdu", "LAUR");
            LanguageCode.Add("Uzbek", "LAUZ");
            LanguageCode.Add("Vasco", "LAVA");
            LanguageCode.Add("Vietnamese", "LAVT");
            return LanguageCode;
        }
        /// <summary>
        /// Gets the miltary experience.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetMiltaryExperience()
        {
            Dictionary<string, string> MiltaryExperience = new Dictionary<string, string>();
            MiltaryExperience.Add("Active Duty", "M1");
            MiltaryExperience.Add("Reservist-Drilling", "M2");
            MiltaryExperience.Add("National Guard-Drilling", "M3");
            MiltaryExperience.Add("Inactive Reserve", "M4");
            MiltaryExperience.Add("Inactive National Guard", "M5");
            MiltaryExperience.Add("Retired Military", "M6");
            MiltaryExperience.Add("Veteran", "M7");
            return MiltaryExperience;
        }

        /// <summary>
        /// Gets the last modified.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetLastModified()
        {
            Dictionary<string, string> LastModified = new Dictionary<string, string>();
            LastModified.Add("Within 1 day", "1");
            LastModified.Add("Within 1 week", "7");
            LastModified.Add("Within 1 Month", "30");
            LastModified.Add("Within 2 Months", "60");
            LastModified.Add("Within 3 Months", "90");
            LastModified.Add("Within 6 Months", "180");
            LastModified.Add("Within 9 Months", "270");
            LastModified.Add("Within 1 Year", "365");
            LastModified.Add("More than 1 year ago", "365+");
            LastModified.Add("All Resumes", "999");
            return LastModified;
        }

        public Dictionary<string, string> GetYear(int startYear)
        {
            Dictionary<string, string> yearList = new Dictionary<string, string>();
            for (int i = 0; i <= 50; i++)
            {
                yearList.Add(i.ToString(), i.ToString());
            }
            return yearList;
        }

        public Dictionary<string, string> GetEmploymentType()
        {
            Dictionary<string, string> EmploymentType = new Dictionary<string, string>();
            EmploymentType.Add("Full-Time", "ETFE");
            EmploymentType.Add("Part-Time", "ETPE");
            EmploymentType.Add("Contractor", "ETCT");
            EmploymentType.Add("Intern", "ETIN");
            EmploymentType.Add("Seasonal/Temp", "ETTS");
            return EmploymentType;
        }

        public Dictionary<string, string> GetManagementLevel()
        {
            Dictionary<string, string> ManagementLevel = new Dictionary<string, string>();
            ManagementLevel.Add("None", "m10");
            ManagementLevel.Add("Leader", "m11");
            ManagementLevel.Add("Manager", "m12");
            ManagementLevel.Add("Executive", "m13");
            return ManagementLevel;
        }
    }
}
