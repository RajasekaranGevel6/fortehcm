﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [ScoreRangeChartValues.cs]
// [File that represents the class with details like color, opacity, x-axis and y-axis values for chart display.]

#endregion

#region Namespaces                                                              
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
#endregion

namespace Forte.HCM.TalentScoutSL.DataObjects
{
    /// <summary>
    /// This class contains the informations like color, opacity, x-axis and y-axis values for chart display.
    /// </summary>
    public class ScoreRangeChartValues
    {
        private RadialGradientBrush _COLOR;
        private decimal _OPACITY;
        private int _NO_OF_CANDIDATES;
        private int _SCORE;
        private string _LABEL_VISIBILITY;
        private Thickness _MARGIN;
        /// <summary>
        /// Property that will get or set the LABEL_VISIBILITY.
        /// </summary>
        public string LABEL_VISIBILITY
        {
            get
            {
                return this._LABEL_VISIBILITY;
            }
            set
            {
                if ((this._LABEL_VISIBILITY != value))
                {
                    this._LABEL_VISIBILITY = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the GRADIENT_COLOR.
        /// </summary>
        public RadialGradientBrush GRADIENT_COLOR
        {
            get
            {
                return this._COLOR;
            }
            set
            {
                if ((this._COLOR != value))
                {
                    this._COLOR = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the OPACITY.
        /// </summary>
        public decimal OPACITY
        {
            get
            {
                return this._OPACITY;
            }
            set
            {
                if ((this._OPACITY != value))
                {
                    this._OPACITY = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the MARGIN.
        /// </summary>
        public Thickness MARGIN
        {
            get
            {
                return this._MARGIN;
            }
            set
            {
                if ((this._MARGIN != value))
                {
                    this._MARGIN = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the NO_OF_CANDIDATES.
        /// </summary>
        public int NO_OF_CANDIDATES
        {
            get
            {
                return this._NO_OF_CANDIDATES;
            }
            set
            {
                if ((this._NO_OF_CANDIDATES != value))
                {
                    this._NO_OF_CANDIDATES = value;
                }
            }
        }
        /// <summary>
        /// Property that will get or set the SCORE.
        /// </summary>
        public int SCORE
        {
            get
            {
                return this._SCORE;
            }
            set
            {
                if ((this._SCORE != value))
                {
                    this._SCORE = value;
                }
            }
        }
    }
}
