﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [PickedCandidates.xaml.cs]

#endregion Header

#region Namespaces
using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.Generic;

using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using Forte.HCM.TalentScoutSL.Utils;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    public partial class PickedCandidatesList : ChildWindow
    {
       
        private int candidateID = 0;
        private int positionProfileID=0;
        MainPage mainPage;
        public PickedCandidatesList()
        {
            InitializeComponent();
        }

        public void DisplayCandidates()
        {
            mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            List<PickedCandidate> selList = new List<PickedCandidate>();
            if (mainPage.scoreDetails == null)
                return;
            if (mainPage.scoreDetails.CandidateDetails != null)
            {
                for (int i = 1; i < mainPage.MainPage_searchResultsInnerCanvas.Children.Count; i++)
                {
                    if (mainPage.MainPage_searchResultsInnerCanvas.Children[i] is EllipseControl)
                    {

                        EllipseControl ellipse = ((EllipseControl)mainPage.MainPage_searchResultsInnerCanvas.Children[i]);
                        if (ellipse.CandidateDetails.IsPicked == "Y")
                        {
                            PickedCandidate pc = new PickedCandidate();
                            pc.CandidateId = Convert.ToInt32(ellipse.CandidateDetails.CandidateId);
                            pc.CandidateName = ellipse.CandidateDetails.FirstName;
                            selList.Add(pc);
                        }
                    }
                }
                //PickedCandidatesList_DataGrid.DataContext = selList;
                PagedCollectionView tempListView = new PagedCollectionView(selList);
                PickedCandidatesList_DataGrid.ItemsSource = tempListView;
                PickedCandidatesList_DataGrid.UpdateLayout();

                if (mainPage.PositionProfileId == 0)
                {
                    PositionProfileNameLabel.Visibility = Visibility.Collapsed;
                    PositionProfileTextBlock.Visibility = Visibility.Collapsed;
                }
                if (mainPage.positionProfileName != null)
                    PositionProfileNameLabel.Text = mainPage.positionProfileName;

            }
        }

        private void PickedCandidatesList_DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;
            candidateID = ((PickedCandidate)e.AddedItems[0]).CandidateId;

        }
        private void OKDelete_Click(object sender, RoutedEventArgs e)
        {
            mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            if (mainPage.PositionProfileId != 0)
            {
                positionProfileID = mainPage.PositionProfileId;

                TalentScoutServiceClient webservice = null;
                AddServiceReference.BindServiceReference(ref webservice);

                webservice.DeletePickedCandidatesCompleted += new EventHandler<DeletePickedCandidatesCompletedEventArgs>(webservice_DeletePickedCandidatesCompleted);
                webservice.DeletePickedCandidatesAsync(positionProfileID, candidateID);
            }
            else
            {
                int candidateIDIndex=mainPage.candidateIds.IndexOf(candidateID.ToString());
                mainPage.candidateIds=mainPage.candidateIds.Remove(candidateIDIndex,candidateID.ToString().Length+1);
            }

            EllipseControl ellipse = ((EllipseControl)mainPage.
                MainPage_searchResultsInnerCanvas.FindName(candidateID.ToString()));

            ellipse.CandidateDetails.IsPicked = "N";

            Style style = ellipse.Resources["RedEllipseButton"] as Style;
                ellipse.EllipseButton.Style = style;

            PickedCandidatesList_DataGrid.ItemsSource = null;

            DisplayCandidates();
        }

        void webservice_DeletePickedCandidatesCompleted(object sender, DeletePickedCandidatesCompletedEventArgs e)
        {
            
        }
  
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

  
    }
    public class PickedCandidate 
    {
        private int candidateId;
        private string candidateName;
        public string CandidateName
        {
            get { return candidateName; }
            set { candidateName = value; }
        }
        public int CandidateId
        {
            get { return candidateId; }
            set { candidateId = value; }
        }
    }
}

