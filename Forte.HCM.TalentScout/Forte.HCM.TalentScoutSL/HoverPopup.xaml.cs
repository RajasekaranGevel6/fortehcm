﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [HoverPopup.xaml.cs]
// File that represents the class to display the Hover popup on mouse over the candidate ellipse.
#endregion

#region Namespaces
using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using Forte.HCM.TalentScoutSL.Utils;
using System.Windows.Media.Imaging;
using System.Windows.Browser;
#endregion

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This control is used to display the popup on hovering the 
    /// candidate with all informations like location, photo, scores 
    /// and contact numbers.
    /// </summary>
    public partial class HoverPopup : UserControl
    {
        #region Private Variables
        private CompetencyVectorWindow competencyVector;
        MainPage mainPage;
        #endregion Private Variables

        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public HoverPopup()
        {
            // Required to initialize variables
            InitializeComponent();
            competencyVector = new CompetencyVectorWindow();


        }

        private void HoverPopup_profileButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                competencyVector.Title = "Profile";

                // Calculate the height of popup according to application height. 
                if (App.Current.Host.Content.ActualHeight > 650)
                    competencyVector.Height = 650;
                else
                    competencyVector.Height = App.Current.Host.Content.ActualHeight - 25;
                competencyVector.Show();

                competencyVector.DisplayCompetencyVectors(((Candidate)this.DataContext).CandidateId, ((Candidate)this.DataContext).CandidateResumeId);

            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
        }
        private void HoverPopup_cloneButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
                mainPage.DisplayClonedCandidates(((Candidate)this.DataContext).CandidateId, ((Candidate)this.DataContext).CandidateResumeId);
            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
        }
        private void HoverPopup_pickButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
                mainPage.PickCandidate(((Candidate)this.DataContext).CandidateId);
            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
        }
        private void Hover_nameHyperLink_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            int candidateId = ((Candidate)this.DataContext).CandidateId;
            HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid=" + candidateId, UriKind.Absolute), "_blank");
        }
        /// <summary>
        /// This method sets the position of the arrow mark in hoverpopup.
        /// </summary>
        /// <param name="left">
        /// A <see cref="double"/> holds the left property of the arrow.
        /// </param>
        /// <param name="top">
        /// A <see cref="double"/> holds the top property of the arrow.
        /// </param>
        public void SetHoverArrow(double left, double top, string isPicked, string hasActivity, int candidateID,
            string CandidatePhotoPath, bool CandidateFileExist)
        {
            HoverPopup_arrowRectangle.SetValue(Canvas.LeftProperty, left);
            HoverPopup_arrowRectangle.SetValue(Canvas.TopProperty, top);
            if (isPicked == "Y")
            {
                HoverPopup_pickButton.Visibility = Visibility.Collapsed;
            }
            else
                HoverPopup_pickButton.Visibility = Visibility.Visible;
            if (hasActivity == "Y")
            {
                HoverPopup_noteAvailable.Visibility = Visibility.Visible;
            }
            else
                HoverPopup_noteAvailable.Visibility = Visibility.Collapsed;
            mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            /*BitmapImage bmpCandidatePhoto = null;

             bmpCandidatePhoto = new BitmapImage(
                    new Uri(CandidatePhotoPath + candidateID + ".PNG",
                    UriKind.Absolute));


           if (!CandidateFileExist)
            {
                bmpCandidatePhoto = new BitmapImage(
                  new Uri(CandidatePhotoPath + "photo_not_available.PNG",
                       UriKind.Absolute));
            }
            else
            {
                bmpCandidatePhoto = new BitmapImage(
                  new Uri(CandidatePhotoPath + candidateID + ".PNG",
                  UriKind.Absolute));
            }



            HoverPopup_candidatePhoto.Source = bmpCandidatePhoto;*/

        }
        /// <summary>
        /// This method sets the relevancy bar width.
        /// </summary>
        /// <param name="score">
        /// A <see cref="double"/> holds the score value (either assessment, resume score or clone score)
        /// </param>
        /// <param name="isCloningApplied">
        /// A <see cref="bool"/> holds the boolean value to display the clone score label 
        /// or to display the resume score and assessment score labels.
        /// </param>
        public void SetRelevancyBar(double score, bool isCloningApplied)
        {
            if (isCloningApplied)
            {
                HoverPopup_cloneScoreStackPanel.Visibility = Visibility.Visible;
                HoverPopup_assessmentStackPanel.Visibility = Visibility.Collapsed;
                HoverPopup_assessmentScoreTextBlock.Text = "";
                HoverPopup_resumeScoreTextBlock.Text = "";
            }
            else
            {
                HoverPopup_cloneScoreStackPanel.Visibility = Visibility.Collapsed;
                HoverPopup_assessmentStackPanel.Visibility = Visibility.Visible;
                HoverPopup_resumeScoreTextBlock.SetBinding(TextBlock.TextProperty, new Binding("ResumeScore"));
                HoverPopup_assessmentScoreTextBlock.SetBinding(TextBlock.TextProperty, new Binding("AssessmentScore"));
            }
            HoverPopup_RelevancyInnerRectangle.Width = Convert.ToInt32((score * 65) / 100);
        }

        private void HoverPopup_ViewNoteHyperLink_Click(object sender, RoutedEventArgs e)
        {
            ViewNotesWindow vn = new ViewNotesWindow();
            mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            vn.GetCandidateDetails(((Candidate)this.DataContext).CandidateId, ((Candidate)this.DataContext).FirstName);
            vn.Show();
        }
        private void HoverPopup_emailHyperLink_Click(object sender, RoutedEventArgs e)
        {
            string url = "mailto:" + ((HyperlinkButton)sender).Content.ToString();
            HtmlPage.Window.Navigate(new Uri(url));
        }
    }
}