﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [SearchBarLayoutControl.xaml.cs]

#endregion

#region Namespaces                                                              
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Browser;
using System.Windows.Controls;
using Forte.HCM.TalentScoutSL.DataObjects;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    public partial class SearchBarLayoutControl : UserControl
    {
        private SearchClientRequestWindow searchClientRequest;
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public SearchBarLayoutControl()
        {
            // Required to initialize variables
            InitializeComponent();
            searchClientRequest = new SearchClientRequestWindow();
            searchClientRequest.Closed += new System.EventHandler(searchClientRequest_Closed);
        }
        //private void HtmlTextControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    //HtmlTextBlock_Canvas.Visibility = Visibility.Collapsed;
        //    SearchBarLayoutControl_SearchTextBox.Visibility = Visibility.Visible;
        //}
        /// <summary>
        /// Click event to trigger on search button.
        /// </summary>
        public event RoutedEventHandler onClick;
        /// <summary>
        /// Mouse enter event to trigger on cheat label to display the cheat sheet.
        /// </summary>
        public event MouseEventHandler onMouseEnter;
        /// <summary>
        /// Mouse leave event to trigger on cheat label to hide the cheat sheet.
        /// </summary>
        public event MouseEventHandler onMouseLeave;

        #region Event Handlers                                                  
        private void SearchButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //HtmlTextBlock_Canvas.Visibility = Visibility.Visible;
            //SearchBarLayoutControl_SearchTextBox.Visibility = Visibility.Collapsed;
            if (onClick != null)
                onClick(this, new RoutedEventArgs());
        }

        private void SearchBarLayoutControl_clientRequestImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            searchClientRequest.Title = "Search Position Profile";

            if (App.Current.Host.Content.ActualHeight > 550)
                searchClientRequest.Height = 550;
            else
                searchClientRequest.Height = App.Current.Host.Content.ActualHeight - 30;
            //searchClientRequest.SearchClientRequestWindow_clientRequestDataGrid.ItemsSource=null;
            searchClientRequest.Show();

        }

        private void searchClientRequest_Closed(object sender, System.EventArgs e)
        {
            SearchBarLayoutControl_SearchTextBox.UpdateLayout();
            SearchBarLayoutControl_SearchTextBox.Focus();
            if ((sender as ChildWindow).Title.ToString() == "")
                return;
           
            SearchBarLayoutControl_SearchTextBox.Text = (sender as ChildWindow).Title.ToString();
            
        }

        /// <summary>
        /// This handler gets triggered on clicking the Reset button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBarLayoutControl_resetTextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            mainPage.ResetValues();
            SearchBarLayoutControl_SearchTextBox.Text = "";
            SearchBarLayoutControl_ErrorMessageTextBlock.Text = "";

        }

        /// <summary>
        /// This handler gets triggered on clicking the Logout button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBarLayoutControl_logoutTextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Naviagte to the login page TalentScout
            //HtmlPage.Window.Invoke("SetConditionValue", "1");
            HtmlPage.Window.Navigate(new Uri("TalentScout.aspx", UriKind.Relative), "_self");
            IsolatedFileSettings.WriteToIsolatedFile("");
        }
        
        private void SearchBarLayOut_cheatSheetLabel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (onMouseEnter != null)
                onMouseEnter(this, e);
        }

        private void SearchBarLayOut_cheatSheetLabel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (onMouseLeave != null)
                onMouseLeave(this, e);
        }

        private void TextBlockMouseEnter(object sender, MouseEventArgs e)
        {
            ((TextBlock)sender).Style = Application.Current.Resources["TextBlockMouseOverStyle"] as Style;
        }

        private void TextBlockMouseLeave(object sender, MouseEventArgs e)
        {
            ((TextBlock)sender).Style = Application.Current.Resources["LinkButtonNormalStyle"] as Style;
        }
        #endregion
    }
}