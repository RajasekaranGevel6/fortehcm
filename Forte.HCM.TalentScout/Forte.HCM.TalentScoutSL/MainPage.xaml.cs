﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [MainPage.xaml.cs]

#endregion

#region Directives
using System;
using System.Text;
using System.IO;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Browser;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Imaging;

using Forte.HCM.TalentScoutSL.DataObjects;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using Forte.HCM.TalentScoutSL.Utils;




#endregion Directives

namespace Forte.HCM.TalentScoutSL
{
    public partial class MainPage : UserControl
    {
        #region Private Variables
        private TransformGroup renderTransform;
        private TranslateTransform panTransform;
        private ScaleTransform zoomTransform;

        private double previousX = 0;
        private double previousY = 0;
        private bool isPanStarted = false;
        private string previouslySelectedCandidate = "";
        public List<string> selectedCandidates = null;

        public string candidateIds = string.Empty;
        private const string INVALID_SEARCH_FOREGROUND_HEXCOLOR = "#FFCDB9B1";

        private const int PAN_XVALUE = 30;
        private const int PAN_YVALUE = 30;
        public const int PAGE_SIZE = 2;
        public bool isMoreMatchingCandidate = false;
        public decimal matchingCandidateDistance = 0;
        
        public double Zooming_Constant = 3;
        /// <summary>
        /// A <see cref="int"/> holds the current page number of the Zoom pane
        /// </summary>
        private int Current_Page = 1;

        private SearchBarLayoutControl searchBar;
        private TalentScoutServiceClient webservice;
        private LoginJobBoard loginJobBoard;
        public SearchJobBoard searchJobBoard;

        /// <summary>
        /// A <see cref="boolean"/> that holds the slider control
        /// small value changed or not.
        /// </summary>
        private bool IsSmallChangeValueChanged;
        /// <summary>
        /// A <see cref="int"/> that holds the number of spheres
        /// to be shown to the user setted in pras_option table.
        /// </summary>
        private int NumberOfSpheresFromPrasOptions = 20;

        private int pageNumber;
        private bool isAddCandidate;
        private int pageSize;
        private bool isSearchClick;
        /// <summary>
        /// A <see cref="List"/> holds the slider intervals.
        /// </summary>
        private List<double> SliderIntervals;
        /// <summary>
        /// A <see cref="bool"/> that holds whether the center
        /// mouse wheel roated or not
        /// </summary>
        //private bool IsZoomMouseWheelRotated = false;
        /// <summary>
        /// A <see cref="bool"/> that holds the value of the search selected by the user.
        /// </summary>
        public bool IsResume = false;
        /// <summary>
        /// A <see cref="ScoreDetails"/> that holds the score details for the specified search terms.
        /// </summary>
        public ScoreDetails scoreDetails;
        /// <summary>
        /// A <see cref="SearchTerms"/> that holds the search terms.
        /// </summary>
        public SearchTerms searchTerms;
        /// <summary>
        /// A list of <see cref="CandidateTechnicalSkills"/> that holds the technical skills of the selected candidate.
        /// </summary>
        public List<CandidateTechnicalSkills> technicalSkills;
        /// <summary>
        /// A <see cref="System.Int32"/> that holds the current login Id
        /// </summary>
        public int LoginUserId;

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the logged in tenant id
        /// </summary>
        public int TenantId;

        public string UIUrl;
        /// <summary>
        /// A <see cref="bool"/> that holds the boolean value to specify cloning is applied or not.
        /// </summary>
        public bool IsCloningApplied = false;
        /// <summary>
        /// A <see cref="int"/> that holds the cloned candidate id.
        /// </summary>
        public int ClonedCandidateId;
        /// <summary>
        /// A <see cref="int"/> that holds the cloned candidate resume id.
        /// </summary>
        public int ClonedCandidateResumeId;
        /// <summary>
        /// A <see cref="Candidate"/> that holds the selected candidate details.
        /// </summary>
        public Candidate selectedCandidate;
        /// <summary>
        /// A <see cref="string"/> that holds the search keywords text.
        /// </summary>
        public string searchKeywords;

        /// <summary>
        /// A <see cref="string"/> that holds the position profile name
        /// </summary>
        public string positionProfileName;

        /// <summary>
        /// A <see cref="string"/> that holds the parent page.
        /// </summary>
        public string parentPage;

        public int PositionProfileId = 0;
        public string SessionToken;
        private static int operationTimeout;
        private string CandidatePhotoPath = string.Empty;

        public string CandidatePhotoPath1
        {
            get { return CandidatePhotoPath; }
            set { CandidatePhotoPath = value; }
        }
        public static int OperationTimeout
        {
            get { return operationTimeout; }
            set { operationTimeout = value; }
        }
        public bool CandidateFileExist = false;

        public EllipsePositions ellipsePositionsLst;
        

        BitmapImage bmpCandidatePhoto;
        #endregion

        #region Properties, Constructor
        /// <summary>
        /// This property stores the id of previously selected candidate.
        /// This property is used to change the green color of previously 
        /// selected candidate to blue color theme.
        /// </summary>
        public string PreviouslySelectedCandidate
        {
            get { return previouslySelectedCandidate; }
            set { previouslySelectedCandidate = value; }
        }
        /// <summary>
        /// This property stores the selected candidate details.
        /// </summary>
        public Candidate SelectedCandidate
        {
            get { return selectedCandidate; }
            set { selectedCandidate = value; }
        }

        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        /// <param name="SearchKeyWord">
        /// A <see cref="System.String"/> that holds the search key word
        /// <remarks>
        /// This will be intialized when the page loads from an OTM module
        /// </remarks>
        /// </param>
        /// <param name="_LoggedInUser">
        /// A <see cref="System.Int32"/> that holds the current log in user id
        /// </param>
        /// <param name="_TenantId">
        /// A <see cref="System.Int32"/> that holds the current log in tenant id
        /// </param>
        public MainPage(string SearchKeyWord, int _LoggedInUser, int _TenantId, string uiUrl, int ppID, string ppName, int pOperationTimeOut, string pPage)
        {
            operationTimeout = pOperationTimeOut;
            InitializeComponent();
            LoginUserId = _LoggedInUser;
            TenantId = _TenantId;
            UIUrl = uiUrl;
            PositionProfileId = ppID;
            positionProfileName = ppName;
            parentPage = pPage;

            //MainPage_PositionProfileTextBlock.Text = "";

            if (PositionProfileId != 0)
            {
                MainPage_ReturnPositionProfile_StackPanel.Visibility = Visibility.Visible;
                MainPage_PositionProfileLabel.Text = positionProfileName;
            }
            else
            {
                MainPage_rightPaneUserControl.RightPane_createTestImage.Visibility = Visibility.Collapsed;
                MainPage_rightPaneUserControl.RightPane_positionProfileImage.Visibility = Visibility.Collapsed;
                MainPage_rightPaneUserControl.RightPane_positionProfileFreesearchImage.Visibility = Visibility.Collapsed;
                MainPage_rightPaneUserControl.RightPane_addNoteImage.Visibility = Visibility.Collapsed;
                MainPage_rightPaneUserControl.RightPane_pickedCandidatesListImage.Visibility = Visibility.Collapsed;
                MainPage_ReturnPositionProfile_StackPanel.Visibility = Visibility.Collapsed;
            }
            MainPage_zoomSliderControl.Minimum = 0.3;
            MainPage_zoomSliderControl.Maximum = 1;
            MainPage_zoomSliderControl.Value = 1;
            MainPage_zoomSliderControl.SmallChange = 0.1;
            //MainPage_zoomSliderControl.LargeChange = MainPage_zoomSliderControl.Maximum;
            // Initialize the pan and zoom transforms
            panTransform = new TranslateTransform();
            if (zoomTransform == null)
            {
                zoomTransform = new ScaleTransform();
                zoomTransform.CenterX = 0;
                zoomTransform.CenterY = 0;
            }
            renderTransform = new TransformGroup();
            renderTransform.Children.Add(panTransform);
            renderTransform.Children.Add(zoomTransform);
            MainPage_searchResultsInnerCanvas.RenderTransform = renderTransform;
            searchBar = (SearchBarLayoutControl)MainPage_OuterGrid.FindName
                 ("MainPage_searchBarLayoutControl");
            //MainPage_gridViewGrid.Visibility = Visibility.Visible;
            //MainPage_ellipseViewGrid.Visibility = Visibility.Collapsed;
            
            AddServiceReference.BindServiceReference(ref webservice);
            // Add all the event handlers
            webservice.GetCandidatePhotoPathCompleted += new EventHandler<GetCandidatePhotoPathCompletedEventArgs>(webservice_GetCandidatePhotoPathCompleted);
            webservice.GetCandidatePhotoPathAsync();
            webservice.GetTechnicalSkillsByCandidateIdCompleted += new EventHandler<GetTechnicalSkillsByCandidateIdCompletedEventArgs>(webservice_GetTechnicalSkillsByCandidateIdCompleted);
            webservice.LogExceptionCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(webservice_LogExceptionCompleted);
            webservice.LogExceptionMessageCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(webservice_LogExceptionMessageCompleted);
            webservice.GetClonedCandidatesByCandidateIdCompleted += new EventHandler<GetClonedCandidatesByCandidateIdCompletedEventArgs>(webservice_GetClonedCandidatesByCandidateIdCompleted);
            webservice.GetAdvancedClonedCandidatesCompleted += new EventHandler<GetAdvancedClonedCandidatesCompletedEventArgs>(webservice_GetAdvancedClonedCandidatesCompleted);
            webservice.GetAdminSettingsCompleted += new EventHandler<GetAdminSettingsCompletedEventArgs>(webservice_GetAdminSettingsCompleted);
            webservice.GetAdminSettingsAsync();
            webservice.InsertSearchLogCompleted += new EventHandler<InsertSearchLogCompletedEventArgs>(webservice_InsertSearchLogCompleted);

            webservice.PickCandidateCompleted += new EventHandler<PickCandidateCompletedEventArgs>(webservice_PickCandidateCompleted);

            webservice.InsertCloneLogCompleted += new EventHandler<InsertCloneLogCompletedEventArgs>(webservice_InsertCloneLogCompleted);
            webservice.CheckSearchUsageCompleted += new EventHandler<CheckSearchUsageCompletedEventArgs>(webservice_CheckSearchUsageCompleted);
            webservice.CheckJobBoardLoginCredentialsCompleted += new EventHandler<CheckJobBoardLoginCredentialsCompletedEventArgs>(webservice_CheckJobBoardLoginCredentialsCompleted);
            webservice.PickCandidatesCompleted += new EventHandler<PickCandidatesCompletedEventArgs>(webservice_PickCandidatesCompleted);
            MainPage_OuterGrid.MouseWheel += new MouseWheelEventHandler(MainPage_OuterGrid_MouseWheel);
            
            SetSliderIntervals();
            searchBar.SearchBarLayoutControl_SearchTextBox.Text = SearchKeyWord;
            loginJobBoard = new LoginJobBoard();
            searchJobBoard = new SearchJobBoard();
        }

        void webservice_GetCandidatePhotoPathCompleted(object sender, GetCandidatePhotoPathCompletedEventArgs e)
        {
            CandidatePhotoPath1 = e.Result.ToString();
        }
        void webservice_PickCandidatesCompleted(object sender, PickCandidatesCompletedEventArgs e)
        {
            try
            {
                string status = e.Result.ToString();

                foreach (string candidateId in selectedCandidates)
                {
                    EllipseControl ellipse = ((EllipseControl)MainPage_searchResultsInnerCanvas.FindName(candidateId));
                    Style style = ellipse.Resources["BlueEllipseButton"] as Style;
                    ellipse.EllipseButton.Style = style;
                    ellipse.CandidateDetails.IsPicked = "Y";
                }
                selectedCandidates.Clear();
            }
            catch (Exception ex)
            {
                webservice.LogExceptionMessageAsync(ex.Message.ToString());
                DisplayErrorMessage(null, "Cannot pick candidate");
            }
        }
        void webservice_PickCandidateCompleted(object sender, PickCandidateCompletedEventArgs e)
        {
            try
            {
                List<SPTSINSERT_POSITION_PROFILE_CANDIDATESResult> status = e.Result.ToList();

                if (Convert.ToInt32(status[0].CANDIDATE_ID) == 0)
                {
                    DisplayErrorMessage(null, "This candidate is already picked");
                }
                else
                {
                    EllipseControl ellipse = ((EllipseControl)MainPage_searchResultsInnerCanvas.FindName(status[0].CANDIDATE_ID.ToString()));
                    Style style = ellipse.Resources["BlueEllipseButton"] as Style;
                    ellipse.EllipseButton.Style = style;
                    ellipse.CandidateDetails.IsPicked = "Y";
                }
            }
            catch 
            {
                webservice.LogExceptionMessageAsync("Cannot pick candidate");
            }
        }


        #endregion Properties, Constructor

        #region Private Methods

        private void LogException(Exception exp)
        {
            AddServiceReference.BindServiceReference(ref webservice);
            webservice.LogExceptionMessageAsync(exp.Message);
        }

        /// <summary>
        /// Sets the slider intervals as per the 
        /// slider control small change.
        /// </summary>
        private void SetSliderIntervals()
        {
            double SmallChange = MainPage_zoomSliderControl.Maximum;
            SliderIntervals = null;
            if (SliderIntervals == null)
                SliderIntervals = new List<double>();
            SliderIntervals.Add(MainPage_zoomSliderControl.Maximum);
            do
            {
                SmallChange -= MainPage_zoomSliderControl.SmallChange;
                SliderIntervals.Add(Convert.ToDouble(SmallChange.ToString("0.##")));
            } while (SmallChange > MainPage_zoomSliderControl.Minimum);
            if (SliderIntervals != null)
                if (!SliderIntervals.Contains(Convert.ToDouble(MainPage_zoomSliderControl.Minimum.ToString("0.##"))))
                    SliderIntervals[SliderIntervals.Count - 1] = MainPage_zoomSliderControl.Minimum;
        }

        /// <summary>
        /// Method that gets the maximum page size in the zoom slider 
        /// control.
        /// </summary>
        /// <returns>
        /// A <see cref="int"/> that contains the maximum page size 
        /// for the slider control.
        /// </returns>
        private int GetMaximumPageSizeFromSlider()
        {
            if (SliderIntervals == null)
                return 7;
            return SliderIntervals.Count - 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NoOfCandidates">
        /// A <see cref="int"/> that holds the number of candidates matched
        /// for the current search terms provided by the user.
        /// </param>
        private void CheckAndSetSmallChange(int NoOfCandidates)
        {
            IsSmallChangeValueChanged = true;
            if (NoOfCandidates < GetMaximumPageSizeFromSlider() * NumberOfSpheresFromPrasOptions)
            {
                MainPage_zoomSliderControl.SmallChange = 0.1;
                SetSliderIntervals();
                return;
            }
            MainPage_zoomSliderControl.SmallChange = Convert.ToDouble(NumberOfSpheresFromPrasOptions) / NoOfCandidates;
            SetSliderIntervals();
        }

        public void AddCandidate()
        {
            List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates = scoreDetails.CandidateDetails.ToList();
            if (scoreDetails.SearchTerms_NoOfCandidates != null)
                SetSummaryDetails(scoreDetails.SearchTerms_NoOfCandidates.ToList(), scoreDetails.SearchTermSummaryDetails);
            if (scoreDetails.SearchTermSummaryDetails != null && !IsSmallChangeValueChanged)
                CheckAndSetSmallChange((int)scoreDetails.SearchTermSummaryDetails.MATCHED);
            if (isSearchClick)
                webservice.InsertSearchLogAsync(searchKeywords, positionProfileName, LoginUserId, scoreDetails != null ? scoreDetails.SearchTermSummaryDetails.MATCHED : 0);
            if (candidates != null && candidates.Count != 0)
            {
                // Reset the summary details.
                MainPage_rightPaneUserControl.SetScoreChartData();
                AddCandidates(candidates);
                //SetSummaryDetails(scoreDetails.SearchTerms_NoOfCandidates.ToList(), scoreDetails.SearchTermSummaryDetails);
                EllipseControl ellipse = ((EllipseControl)
                MainPage_searchResultsInnerCanvas.FindName(candidates[0].CANDIDATE_ID.ToString()));
                ellipseClickEventHandler((object)(ellipse), null);

            }
            else if (MainPage_searchResultsInnerCanvas.Children.Count < 2)
            {
                DisplayErrorMessage(null, "No data found to display.");
            }

        }

        /// <summary>
        /// This method checks and allocate zero weightage for the valid search terms 
        /// where weightage not assigned
        /// </summary>
        private void CheckForUnAssignedWeightageSearchTerms()
        {
            string[] splitSearchTermsByComma = null;
            StringBuilder SearchString = null;
            try
            {
                splitSearchTermsByComma = searchBar.SearchBarLayoutControl_SearchTextBox.Text.Trim().Split(',');
                SearchString = new StringBuilder();
                for (int index = 0; index < splitSearchTermsByComma.Length; index++)
                {
                    if (splitSearchTermsByComma[index].IndexOf('[') <= 0 &&
                       splitSearchTermsByComma[index].IndexOf(']') <= 0)
                    {
                        SearchString.Append(splitSearchTermsByComma[index]);
                        SearchString.Append("[0],");
                    }
                    else
                        SearchString.Append(splitSearchTermsByComma[index] + ",");
                }
                searchBar.SearchBarLayoutControl_SearchTextBox.Text = SearchString.ToString().TrimEnd(',');
            }
            finally
            {
                if (splitSearchTermsByComma != null) splitSearchTermsByComma = null;
                if (SearchString != null) SearchString = null;
            }
        }

        /// <summary>
        /// This method checks and selects the user provided search criteria
        /// </summary>
        /// <param name="AssignedWeightages">
        /// A <see cref="bool"/> that contains whether the weightage
        /// for the serach terms are assigned or not. If not this method
        /// will assign those search terms weightage as zero (though assigned 
        /// balance avaliable).
        /// </param>
        private void CheckAndSelectInvalidText(bool AssignedWeightages)
        {
            string[] splitSearchTermsByComma = null;
            int SelectionStart = 0;
            StringBuilder stringBuilder = null;
            List<int> IndexToMove = null;
            try
            {
                if (!AssignedWeightages)
                    CheckForUnAssignedWeightageSearchTerms();
                splitSearchTermsByComma = searchBar.SearchBarLayoutControl_SearchTextBox.Text.Trim().Split(',');
                //for (int index = 0; index < splitSearchTermsByComma.Length; index++)
                //{
                //    if ((splitSearchTermsByComma[index].IndexOf('[') >= 0 &&
                //        splitSearchTermsByComma[index].IndexOf(']') <= 0) || (
                //        splitSearchTermsByComma[index].IndexOf('[') <= 0 &&
                //        splitSearchTermsByComma[index].IndexOf(']') >= 0) ||
                //        splitSearchTermsByComma[index].IndexOf('[') <= 0)
                //    {
                //        SetColorForInValidText(SelectionStart);
                //        return;
                //    }
                //    SelectionStart += splitSearchTermsByComma[index].Length + 1;
                //}
                IndexToMove = CheckAndGetZeroWeightagePositions();
                searchBar.SearchBarLayoutControl_SearchTextBox.Text = "";
                stringBuilder = new StringBuilder();
                for (int index = 0; index < splitSearchTermsByComma.Length; index++)
                {
                    if (IndexToMove.Contains(index))
                        continue;
                    stringBuilder.Append(splitSearchTermsByComma[index]);
                    stringBuilder.Append(",");
                }
                for (int index = 0; index < IndexToMove.Count; index++)
                    stringBuilder.Append(splitSearchTermsByComma[IndexToMove[index]] + ",");
                searchBar.SearchBarLayoutControl_SearchTextBox.Text = stringBuilder.ToString().TrimEnd(',');
                if (IndexToMove.Count <= 0)
                    return;
                SetColorForInValidText(searchBar.SearchBarLayoutControl_SearchTextBox.Text.IndexOf(splitSearchTermsByComma[IndexToMove[0]]));
            }
            catch (FormatException ex)
            {
                SetColorForInValidText(SelectionStart);
                Utility.WriteExceptionLog(ex);
            }
            finally
            {
                if (splitSearchTermsByComma != null) splitSearchTermsByComma = null;
                if (stringBuilder != null) stringBuilder = null;
                if (IndexToMove != null) IndexToMove = null;
            }
        }

        /// <summary>
        /// This method gets the list of search terms indexes that has zero as their
        /// weightage.
        /// </summary>
        /// <returns>
        /// A int list that contains the indexes of search terms that has zero weightage.
        /// </returns>
        private List<int> CheckAndGetZeroWeightagePositions()
        {
            string[] splitSearchTermsByComma = null;
            List<int> StringIndexsToMove = null;
            int Weightage = 0;
            int TotalWeightage = 0;
            try
            {
                splitSearchTermsByComma = searchBar.SearchBarLayoutControl_SearchTextBox.Text.Trim().Split(',');
                StringIndexsToMove = new List<int>();
                for (int index = 0; index < splitSearchTermsByComma.Length; index++)
                {
                    try
                    {
                        Weightage = Convert.ToInt32(splitSearchTermsByComma[index].Substring(
                        splitSearchTermsByComma[index].IndexOf('[') + 1,
                        splitSearchTermsByComma[index].IndexOf(']') - splitSearchTermsByComma[index].IndexOf('[') - 1));
                        if (Weightage <= 0)
                            StringIndexsToMove.Add(index);
                        TotalWeightage += Weightage;
                        if (TotalWeightage > 100)
                            if (!StringIndexsToMove.Contains(index))
                                StringIndexsToMove.Add(index);
                    }
                    catch (Exception)
                    {
                        StringIndexsToMove.Add(index);
                    }
                }
                return StringIndexsToMove;
            }
            finally
            {
                if (splitSearchTermsByComma != null) splitSearchTermsByComma = null;
                if (StringIndexsToMove != null) StringIndexsToMove = null;
            }
        }

        /// <summary>
        /// This method sets the background color for the selected text
        /// </summary>
        /// <param name="StartIndexOfString">
        /// A <see cref="int"/>holds the value of the start index of the
        /// string to select
        /// </param>
        private void SetColorForInValidText(int StartIndexOfString)
        {
            searchBar.SearchBarLayoutControl_SearchTextBox.SelectionStart = StartIndexOfString;
            searchBar.SearchBarLayoutControl_SearchTextBox.SelectionLength =
                searchBar.SearchBarLayoutControl_SearchTextBox.Text.Length - StartIndexOfString;
            searchBar.SearchBarLayoutControl_SearchTextBox.SelectionForeground = new SolidColorBrush(
                Utils.Utility.GetColorFromHex(INVALID_SEARCH_FOREGROUND_HEXCOLOR));
            searchBar.SearchBarLayoutControl_SearchTextBox.SelectionBackground = new SolidColorBrush(Colors.White);
        }

        /// <summary>
        /// This method displays the technical skills and resume details on right panel
        /// </summary>
        /// <param name="id">
        /// This parameter represents the id of the candidate
        /// </param>
        /// <param name="candidateDetails">
        /// This parameter <see cref="Candidate"/> represents the candidate details
        /// </param>
        private void DisplayCandidateDetails(string id, Candidate candidateDetails)
        {

            SelectedCandidate = candidateDetails;
            webservice.GetTechnicalSkillsByCandidateIdAsync(Convert.ToInt32(id));
            if (selectedCandidate != null)
                webservice.CandidateFileExistAsync(selectedCandidate.CandidateId);
            Resume resume = candidateDetails.Resume;
            MainPage_rightPaneUserControl.DataContext = resume;
            MainPage_rightPaneUserControl.CandidateDetails = candidateDetails;


            if (!IsCloningApplied)
            {
                MainPage_rightPaneUserControl.SetPieChartData(scoreDetails.SearchTerms_ScoreDistribution.ToList(), Convert.ToInt32(id));
                MainPage_rightPaneUserControl.SelectCandidate_In_ScoreChart();
            }
            if (SelectedCandidate != null)
            {
                if (SelectedCandidate.HasActivity == "Y")
                    MainPage_rightPaneUserControl.RightPane_noteAvailable.Visibility = Visibility.Visible;
                else
                    MainPage_rightPaneUserControl.RightPane_noteAvailable.Visibility = Visibility.Collapsed;
            }



        }

        private bool TryGetUriAddress(out Uri validAddress, BitmapImage bmpCandidatePhoto)
        {
            bool isValid = false;
            validAddress = null;
            try
            {
                WebClient sc = new WebClient();
                sc.DownloadStringAsync(bmpCandidatePhoto.UriSource);
                validAddress = new Uri(CandidatePhotoPath1 + "51.png",
                                     UriKind.Absolute);
                isValid = true;
            }
            catch 
            {
                isValid = false;
            }
            return isValid;
        }


        void bmpCandidatePhoto_ImageOpened(object sender, RoutedEventArgs e)
        {
            bmpCandidatePhoto = new BitmapImage(
                           new Uri(CandidatePhotoPath1 + SelectedCandidate.CandidateId + ".PNG",
                           UriKind.Absolute));

            MainPage_rightPaneUserControl.RightPane_CandidatePhoto.Source = bmpCandidatePhoto;
        }

        void bmpCandidatePhoto_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {
            bmpCandidatePhoto = new BitmapImage(
                             new Uri(CandidatePhotoPath1 + "photo_not_available.png",
                             UriKind.Absolute));

            MainPage_rightPaneUserControl.RightPane_CandidatePhoto.Source = bmpCandidatePhoto;
        }
        BitmapImage GetImage(byte[] rawImageBytes)
        {
            try
            {
                BitmapImage imageSource = null;
                using (MemoryStream stream = new MemoryStream(rawImageBytes, 1, rawImageBytes.Length - 1))
                {
                    BitmapImage image = new BitmapImage();
                    image.SetSource(stream);
                    imageSource = image;
                }
                return imageSource;
            }
            catch (System.Exception exMessage)
            {
                throw exMessage;
            }
        }


        /// <summary>
        /// This method creates the ellipse and stores the candidate information.
        /// </summary>
        /// <param name="searchedCandidate">
        /// This parameter <see cref="SPTSGET_CANDIDATES_BYSEARCHTERMResult"/> represents the 
        /// searched the candidate
        /// </param>
        /// <param name="index">
        /// This parameter represents the index of the candidate in searched result.
        /// </param>
        private void AddCandidateEllipse(SPTSGET_CANDIDATES_BYSEARCHTERMResult searchedCandidate, int index, Point centerPoint)
        {
            EllipseControl existingEllipse = ((EllipseControl)
                MainPage_searchResultsInnerCanvas.FindName(searchedCandidate.CANDIDATE_ID.ToString()));


            // If candidate ellipse is not present in the search panel, then create the new one.
            // Otherwise reset the position and score values.
            if (existingEllipse == null)
            {
                EllipseControl ellipse = new EllipseControl();
                ellipse.Name = searchedCandidate.CANDIDATE_ID.ToString();
                //Point location = CalculatePostion(Convert.ToDecimal(searchedCandidate.PERCENTAGE), index);
                Canvas.SetLeft(ellipse, centerPoint.X - 65);
                Canvas.SetTop(ellipse, centerPoint.Y - 65);
                Candidate candidate = new Candidate();
                candidate.CandidateId = Convert.ToInt32(searchedCandidate.CANDIDATE_ID);
                candidate.CandidateResumeId = Convert.ToInt32(searchedCandidate.CAND_RESUME_ID);
                candidate.Name = searchedCandidate.CANDIDATE_NAME;
                candidate.FirstName = searchedCandidate.FIRSTNAME;
                candidate.IsPicked = searchedCandidate.ISPICKED;
                candidate.Recruiter = searchedCandidate.RECRUITER;
                candidate.HasActivity = searchedCandidate.HASACTIVITY;



                //if (searchedCandidate.PHOTO != null)
                //    candidate.CandidateImage = searchedCandidate.PHOTO.Bytes;

                // candidate.CandImage = GetImage(searchedCandidate.PHOTO.Bytes);

                /*    memoryStream = new MemoryStream((byte[])candidate.CandidateImage,false);
                   BitmapImage image = new BitmapImage();
                   image.SetSource(memoryStream);
                   candidate.CandImage = image;*/
                //candidate.FirstName = index.ToString() + "-" + (100-searchedCandidate.PERCENTAGE).ToString();

                if (IsCloningApplied)
                {
                    candidate.CloningScore = Convert.ToDouble(searchedCandidate.SCORE);
                }
                else
                {
                    string selSource = searchBar.SearchBarLayoutControl_sourceCombo.SelectionBoxItem.ToString();
                    if (selSource == "Resumes")
                        candidate.ResumeScore = Convert.ToDouble(searchedCandidate.SCORE);
                    else if (selSource == "Specialized Assessment Data")
                        candidate.AssessmentScore = Convert.ToDouble(searchedCandidate.SCORE);
                }
                candidate.Percentage = Convert.ToDouble(searchedCandidate.PERCENTAGE);
                candidate.IsExactMatch = (index == 0 ? true : false);
                Resume resume = new Resume();
                Name name = new Name();
                name.FirstName = searchedCandidate.FIRSTNAME;
                name.LastName = searchedCandidate.LASTNAME;
                resume.Name = name;
                //
                ContactInformation contInfo = new ContactInformation();
                contInfo.City = searchedCandidate.CITY;
                contInfo.Country = searchedCandidate.COUNTRY;
                contInfo.State = searchedCandidate.STATE;
                contInfo.EmailAddress = searchedCandidate.EMAIL_ID;
                //
                PhoneNumber phone = new PhoneNumber();
                phone.Office = searchedCandidate.OFFICE_PHONE;
                phone.Residence = searchedCandidate.HOME_PHONE;
                contInfo.Phone = phone;
                resume.ContactInformation = contInfo;
                //
                resume.ExecutiveSummary = searchedCandidate.SUMMARY;
                candidate.Resume = resume;
                //
                ellipse.CandidateDetails = candidate;
                ellipse.MouseEnter += ellipseMouseEnterEventHandler;
                ellipse.MouseLeave += ellipseMouseLeaveEventHandler;
                ellipse.onClick += ellipseClickEventHandler;
                MainPage_searchResultsInnerCanvas.Children.Add(ellipse);
            }
            else
            {
                // Reset the position and score for the existing candidates
                //Point location = CalculatePostion(Convert.ToDecimal(searchedCandidate.PERCENTAGE), index);
                Canvas.SetLeft(existingEllipse, centerPoint.X - 65);
                Canvas.SetTop(existingEllipse, centerPoint.Y - 65);
                existingEllipse.CandidateDetails.Percentage = Convert.ToDouble(searchedCandidate.PERCENTAGE);
            }
        }

        /// <summary>
        /// Calculates the position by passing the score. TODO : Change this hard coded
        /// location values and arrive the algorithm to position the candidate.
        /// </summary>
        /// <param name="score">
        /// A <see cref="decimal"/> that holds the assessment or resume score
        /// </param>
        /// <param name="index">
        /// A <see cref="int"/> that holds the index of the candidate ellipse.
        /// </param>
        /// <returns>
        /// A <see cref="Point"/> that holds the x and y position of the candidate ellipse.
        /// </returns>
        //private Point CalculatePostion(decimal score, int index)
        //{
        //    Point point = new Point();
        //    //double calculatedValue = (50 * index) + Convert.ToDouble(100 - score) - 60;
        //    double calculatedValue = 35 + Convert.ToDouble(100 - score);
        //    Random rnd = new Random();
        //    double defaultX = -70;
        //    double defaultY = -65;
        //    if (index == 0)
        //    {
        //        point.X = 0;
        //        point.Y = 0;
        //    }
        //    else if (index % 2 == 0)
        //    {
        //        if (index % 8 < 2)
        //        {
        //            point.X = 20 * index;
        //            point.Y = calculatedValue;
        //        }
        //        else if (index % 8 < 4)
        //        {
        //            point.X = -20 * index;
        //            point.Y = -calculatedValue;
        //        }
        //        else if (index % 8 < 6)
        //        {
        //            point.X = -20 * index;
        //            point.Y = calculatedValue;
        //        }
        //        else
        //        {
        //            point.X = 20 * index;
        //            point.Y = -calculatedValue;
        //        }
        //    }
        //    else
        //    {
        //        if (index % 8 > 6)
        //        { 
        //            point.X = -calculatedValue;
        //            point.Y = 20 * index;
        //        }
        //        else if (index % 8 > 4)
        //        {
        //            point.X = calculatedValue;
        //            point.Y = 20 * index;
        //        }
        //        else if (index % 8 > 2)
        //        {
        //            point.X = -calculatedValue;
        //            point.Y = -20 * index;

        //        }
        //        else if (index % 8 > 0)
        //        {
        //            point.X = calculatedValue;
        //            point.Y = -20 * index;

        //        }
        //    }
        //    point.X += defaultX;
        //    point.Y += defaultY;
        //    return point;
        //}

        /// <summary>
        /// This method clears the existing candidates list adnd call the service
        /// request to display the new list.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number
        /// </param>
        /// <param name="isAddCandidate">
        /// </param>
        /// <param name="pageSize"></param>
        private void GetCandidatesBySearchTerm(int pPageNumber, int pPageSize)
        {
            ShowHideLoading(false);

            if (IsCloningApplied)
            {
                int? userID = null;

                if (searchBar.SearchBarLayoutControl_showMyCandidatesCheckBox.IsChecked == true)
                    userID = LoginUserId;

                webservice.GetClonedCandidatesByCandidateIdAsync(TenantId, ClonedCandidateId, ClonedCandidateResumeId, pPageNumber, pPageSize, userID);
            }
            else
            {
                // Request the service to search candidates by passing SearchTextBox value.
                if (searchKeywords != null && searchKeywords != "")
                {
                    string selSource = searchBar.SearchBarLayoutControl_sourceCombo.SelectionBoxItem.ToString();
                    if (selSource == "Resumes")
                    {
                        if (isAddCandidate)
                        {

                            webservice.GetResumeScoreDetailsCompleted -=
                                new EventHandler<GetResumeScoreDetailsCompletedEventArgs>(webservice_add_GetResumeScoreDetailsCompleted);

                            webservice.GetResumeScoreDetailsCompleted +=
                                new EventHandler<GetResumeScoreDetailsCompletedEventArgs>(webservice_add_GetResumeScoreDetailsCompleted);
                        }
                        else
                        {
                            webservice.GetResumeScoreDetailsCompleted -=
                                new EventHandler<GetResumeScoreDetailsCompletedEventArgs>(webservice_remove_GetResumeScoreDetailsCompleted);

                            webservice.GetResumeScoreDetailsCompleted +=
                                new EventHandler<GetResumeScoreDetailsCompletedEventArgs>(webservice_remove_GetResumeScoreDetailsCompleted);
                        }

                        int? userID = null;

                        if (searchBar.SearchBarLayoutControl_showMyCandidatesCheckBox.IsChecked == true)
                            userID = LoginUserId;

                        webservice.GetResumeScoreDetailsAsync(PositionProfileId, searchKeywords, pPageNumber, pPageSize, TenantId, userID);
                    }
                    else if (selSource == "Specialized Assessment Data")
                    {
                        if (isAddCandidate)
                        {
                            webservice.GetScoreDetailsCompleted -=
                                new EventHandler<GetScoreDetailsCompletedEventArgs>(webservice_add_GetScoreDetailsCompleted);

                            webservice.GetScoreDetailsCompleted +=
                                new EventHandler<GetScoreDetailsCompletedEventArgs>(webservice_add_GetScoreDetailsCompleted);
                        }
                        else
                        {
                            webservice.GetScoreDetailsCompleted -=
                                new EventHandler<GetScoreDetailsCompletedEventArgs>(webservice_remove_GetScoreDetailsCompleted);

                            webservice.GetScoreDetailsCompleted +=
                                new EventHandler<GetScoreDetailsCompletedEventArgs>(webservice_remove_GetScoreDetailsCompleted);
                        }

                        int? userID = null;

                        if (searchBar.SearchBarLayoutControl_showMyCandidatesCheckBox.IsChecked == true)
                            userID = LoginUserId;

                        webservice.GetScoreDetailsAsync(PositionProfileId, searchKeywords, pageNumber, pageSize, TenantId, userID);
                    }
                    else if (selSource == "CareerBuilder")
                    {
                        if (!isSearchClick)
                            return;
                        string credentials = IsolatedFileSettings.GetIsolatedFileValue();

                        if (credentials == "")
                        {
                            loginJobBoard.Title = "Login to Career Builder";

                            // Calculate the height of popup according to application height. 
                            if (App.Current.Host.Content.ActualHeight > 650)
                                loginJobBoard.Height = 150;
                            else
                                loginJobBoard.Height = App.Current.Host.Content.ActualHeight - 25;
                            loginJobBoard.Show();
                        }
                        else
                        {
                            string[] strs = credentials.Split('$').ToArray();
                            webservice.CheckJobBoardLoginCredentialsAsync(strs[0], strs[1]);
                        }
                        //OpenSearchJobBoardWindow();
                    }
                    else if (selSource == "Keywords")
                    {
                        if (isAddCandidate)
                        {

                            webservice.GetCandidatesByKeywordsCompleted -=
                                new EventHandler<GetCandidatesByKeywordsCompletedEventArgs>(webservice_add_GetCandidatesByKeywordsCompleted);

                            webservice.GetCandidatesByKeywordsCompleted +=
                                new EventHandler<GetCandidatesByKeywordsCompletedEventArgs>(webservice_add_GetCandidatesByKeywordsCompleted);
                        }
                        else
                        {
                            webservice.GetCandidatesByKeywordsCompleted -=
                                new EventHandler<GetCandidatesByKeywordsCompletedEventArgs>(webservice_remove_GetCandidatesByKeywordsCompleted);

                            webservice.GetCandidatesByKeywordsCompleted +=
                                new EventHandler<GetCandidatesByKeywordsCompletedEventArgs>(webservice_remove_GetCandidatesByKeywordsCompleted);
                        }
                        int? userID = null;

                        if (searchBar.SearchBarLayoutControl_showMyCandidatesCheckBox.IsChecked == true)
                            userID = LoginUserId;

                        webservice.GetCandidatesByKeywordsAsync(PositionProfileId, searchKeywords, pageNumber, pageSize, TenantId, userID);
                    }
                }
            }
        }

        /// <summary>
        /// Method that resets the zoom, pan and other controls in the UI
        /// </summary>
        public void ResetValues()
        {


            //if (IsolatedFileSettings.GetIsolatedFileValue() == "")
            //{
            //    IsolatedFileSettings.WriteToIsolatedFile("");
            //    System.Windows.Browser.HtmlPage.Window.Navigate(new Uri("TalentScout.aspx", UriKind.Relative), "_self");
            //    return;
            //}
            ClearValues(true);
            searchKeywords = "";
            MainPage_zoomSliderControl.Value = 1;
            searchBar.SearchBarLayoutControl_sourceCombo.SelectedIndex = 0;
            panTransform.X = 0;
            panTransform.Y = 0;
            MainPage_rightPaneUserControl.ResetAdvancedSettings();
            MainPage_PositionProfileLabel.Text = "";
            MainPage_ReturnPositionProfile_StackPanel.Visibility = Visibility.Collapsed;
            PositionProfileId = 0;
            MainPage_rightPaneUserControl.RightPane_noteAvailable.Visibility = Visibility.Collapsed;

            MainPage_rightPaneUserControl.RightPane_createTestImage.Visibility = Visibility.Collapsed;
            MainPage_rightPaneUserControl.RightPane_positionProfileImage.Visibility = Visibility.Collapsed;
            MainPage_rightPaneUserControl.RightPane_positionProfileFreesearchImage.Visibility = Visibility.Collapsed;
            MainPage_rightPaneUserControl.RightPane_addNoteImage.Visibility = Visibility.Collapsed;
            MainPage_rightPaneUserControl.RightPane_pickedCandidatesListImage.Visibility = Visibility.Collapsed;

        }

        /// <summary>
        /// Clear the right panel values.
        /// </summary>
        public void ClearValues(bool ClearTransform)
        {
            // Before clearing the InnerCanvas, take the copy of center ellipse 
            // and restore it later.
            Ellipse centerEllp = new Ellipse();
            centerEllp = MainPage_centerEllipse;
            // Reset the InnerCanvas by clearing the ellipses
            MainPage_searchResultsInnerCanvas.Children.Clear();
            MainPage_searchResultsInnerCanvas.Children.Add(centerEllp);
            MainPage_rightPaneUserControl.DataContext = null;
            MainPage_rightPaneUserControl.DataSource = null;
            MainPage_rightPaneUserControl.CandidateDetails = null;
            MainPage_rightPaneUserControl.SetSearchSummary(null);
            MainPage_rightPaneUserControl.ClearScoreChart();
            MainPage_rightPaneUserControl.SetPieChartData(null, 0);
            MainPage_gridViewDataGrid.ItemsSource = null;
            selectedCandidates = null;
            candidateIds = string.Empty;
            bmpCandidatePhoto = new BitmapImage(
                          new Uri(CandidatePhotoPath1 + "photo_not_available.png",
                          UriKind.Absolute));
            MainPage_rightPaneUserControl.RightPane_CandidatePhoto.Source = bmpCandidatePhoto;
            if (!ClearTransform)
                return;
            
            //if (zoomTransform == null)
            //{
            //    zoomTransform = new ScaleTransform();
            //    zoomTransform.CenterX = 0;
            //    zoomTransform.CenterY = 0;
            //}
            zoomTransform.CenterX = 0;
            zoomTransform.CenterY = 0;
            panTransform.X = 0;
            panTransform.Y = 0;
            zoomTransform.ScaleX = 1;
            zoomTransform.ScaleY = 1;
        }
        /// <summary>
        /// Clears the right panel and display the error message.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="errorMessage"></param>
        public void DisplayErrorMessage(Exception ex, string errorMessage)
        {
            if (errorMessage != "")
            {
                MainPage_rightPaneUserControl.DataContext = null;
                MainPage_rightPaneUserControl.CandidateDetails = null;
                searchBar.SearchBarLayoutControl_ErrorMessageTextBlock.Visibility = Visibility.Visible;
                searchBar.SearchBarLayoutControl_ErrorMessageTextBlock.Text = errorMessage;
                searchKeywords = null;
                ShowHideLoading(true);
            }
            //if (ex != null)
            //{
            //    string innerException = (ex.InnerException != null ? ex.InnerException.ToString() : "");
            //    webservice.LogExceptionAsync(ex.Message, innerException, ex.StackTrace);
            //}
            Utility.WriteExceptionLog(ex);
        }
        /// <summary>
        /// Set the summary detail fields.
        /// </summary>
        /// <param name="searchTerm_NoOfCandidates">
        /// This parameter contains informations about number of candidates matched for each search terms. 
        /// </param>
        /// <param name="summaryDetails">
        /// Summary details contains informations about number of candidates searched,
        /// matching candidates and number of sources searched.
        /// </param>
        private void SetSummaryDetails
            (List<SearchTerms_No_Of_Candidates> searchTerm_NoOfCandidates, SearchSummaryDetails summaryDetails)
        {
            //Set search summary values
            SearchSummary searchSummary = new SearchSummary();
            searchSummary.CandidatesSearched = Convert.ToInt32(summaryDetails.SEARCHED);
            searchSummary.MatchingCandidates = Convert.ToInt32(summaryDetails.MATCHED);

            // TODO : Change this hard coded value later.
            searchSummary.SourcesSearched = 1;

            searchSummary.SearchTerms = new List<SearchTerm>();
            SearchTerm term;

            foreach (SearchTerms_No_Of_Candidates item in searchTerm_NoOfCandidates)
            {
                term = new SearchTerm();
                term.Keyword = item.SEARCH_TERM;
                term.NoOfCandidatesMatched = Convert.ToInt32(item.NO_OF_CANDIDATES);
                searchSummary.SearchTerms.Add(term);
            }

            MainPage_rightPaneUserControl.SetSearchSummary(searchSummary);
        }

        /// <summary>
        /// Sets the cursor style
        /// </summary>
        /// <param name="CursorStyle">
        /// A <see cref="Cursors"/> contains the cursors style.
        /// </param>
        private void SetCursorStyle(Cursor CursorStyle)
        {
            this.Cursor = CursorStyle;
        }
        /// <summary>
        /// Method to get the cloned candidates
        /// </summary>
        /// <param name="candidateId">
        /// A <see cref="int"/> that holds the candidate id to clone.
        /// </param>
        public void DisplayClonedCandidates(int candidateId,int candResumeId)
        {
            ShowHideLoading(false);
            IsCloningApplied = true;
            zoomTransform.ScaleX = 1;
            zoomTransform.ScaleY = 1;
            MainPage_zoomSliderControl.Value = MainPage_zoomSliderControl.Maximum;
            ClonedCandidateId = candidateId;
            ClonedCandidateResumeId = candResumeId;
            webservice.InsertCloneLogAsync(searchKeywords, ClonedCandidateId, LoginUserId);

        }
        /// <summary>
        /// Method to pick the candidates
        /// </summary>
        /// <param name="candidateId">
        /// A <see cref="int"/> that holds the candidate id to clone.
        /// </param>
        public void PickCandidate(int candidateId)
        {
            if (PositionProfileId != 0)
                webservice.PickCandidateAsync(candidateId, PositionProfileId, LoginUserId);
            else
            {
                candidateIds += candidateId + ",";
                EllipseControl ellipse = ((EllipseControl)MainPage_searchResultsInnerCanvas.FindName(candidateId.ToString()));
                Style style = ellipse.Resources["BlueEllipseButton"] as Style;
                ellipse.EllipseButton.Style = style;
                ellipse.CandidateDetails.IsPicked = "Y";
            }
        }
        public void PickCandidates()
        {

            ObservableCollection<string> obsSelectedCandidates = new ObservableCollection<string>();
            for (int i = 0; i < selectedCandidates.Count; i++)
            {
                if (PositionProfileId != 0)
                    obsSelectedCandidates.Add(selectedCandidates[i]);
                else
                {
                    candidateIds += selectedCandidates[i] + ",";
                    EllipseControl ellipse = ((EllipseControl)MainPage_searchResultsInnerCanvas.FindName(selectedCandidates[i].ToString()));
                    Style style = ellipse.Resources["BlueEllipseButton"] as Style;
                    ellipse.EllipseButton.Style = style;
                    ellipse.CandidateDetails.IsPicked = "Y";
                }
            }
            if (PositionProfileId != 0)
            {
                webservice.PickCandidatesAsync(obsSelectedCandidates, PositionProfileId, LoginUserId);
            }

        }
        /// <summary>
        /// Method to get the cloned candidates by passing advanced settings.
        /// </summary>
        /// <param name="clone_advanced_settings">
        /// A <see cref="string"/> that holds the clone advanced settings string.
        /// </param>
        public void DisplayAdvancedClonedCandidates(string clone_advanced_settings)
        {
            ShowHideLoading(false);
            IsCloningApplied = true;
            zoomTransform.ScaleX = 1;
            zoomTransform.ScaleY = 1;
            MainPage_zoomSliderControl.Value = MainPage_zoomSliderControl.Maximum;
            ClonedCandidateId = SelectedCandidate.CandidateId;
            ClonedCandidateResumeId = SelectedCandidate.CandidateResumeId;

            int? userID = null;

            if (searchBar.SearchBarLayoutControl_showMyCandidatesCheckBox.IsChecked == true)
                userID = LoginUserId;
            webservice.GetAdvancedClonedCandidatesAsync(clone_advanced_settings, ClonedCandidateId,ClonedCandidateResumeId, 0, PAGE_SIZE, TenantId, userID);
        }

        private void DisplayClonedCandidates(List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates)
        {
            try
            {
                ClearValues(false);
                MainPage_candidateHoverPopup.IsOpen = false;
                if (candidates.Count == 0)
                {
                    searchBar.SearchBarLayoutControl_ErrorMessageTextBlock.Visibility = Visibility.Visible;
                    searchBar.SearchBarLayoutControl_ErrorMessageTextBlock.Text =
                        App_GlobalResources.TalentScoutResource.TalentScout_NoCloneRecordsFound;
                    return;
                }
                //ScoreDetails scoreDetails = new ScoreDetails();
                ObservableCollection<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidateCollection = new ObservableCollection<SPTSGET_CANDIDATES_BYSEARCHTERMResult>();
                for (int i = 0; i < candidates.Count; i++)
                {
                    candidateCollection.Add(candidates[i]);
                }
                scoreDetails.CandidateDetails = candidateCollection;
                PagedCollectionView tempListView = new PagedCollectionView(GetCandidateDetails(scoreDetails));
                MainPage_gridViewDataGrid.ItemsSource = tempListView;
                AddCandidates(candidates);
                //if (candidates.Count == 0)
                //    return;
                EllipseControl ellipse = ((EllipseControl)
                    MainPage_searchResultsInnerCanvas.FindName(candidates[0].CANDIDATE_ID.ToString()));
                ellipseClickEventHandler((object)(ellipse), null);
            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
            finally
            {
                ShowHideLoading(true);
            }
        }

        /// <summary>
        /// This method gets called to add new candidate ellipses.
        /// </summary>
        /// <param name="candidates">
        /// This parameter contains the candidate information like firstname, emailid etc.,
        /// </param>
        public void AddCandidates(List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates)
        {
            if (candidates != null && candidates.Count != 0)
            {
                // Calculate the distance value according to maximum and minimum percentage.
                double distance;
                if(candidates.Count==1)
                    distance=3;
                else
                {
                    double innerCanvasHalfWidth =( (MainPage_OuterGrid.ActualWidth - MainPage_rightPaneUserControl.ActualWidth)/2)-100; //Subtract constant value 100 to avoid displaying the ellipse circle at the end.
                    distance = innerCanvasHalfWidth / Convert.ToDouble(candidates[0].PERCENTAGE - candidates[candidates.Count - 1].PERCENTAGE);
                    distance=distance< 3 ? 3 : distance;
                }
                Zooming_Constant = Convert.ToInt32((Convert.ToDouble(Current_Page - 1) * Convert.ToDouble(0.5)) + distance);
                PlotCandidateEllipse(candidates, ((Convert.ToDouble(Current_Page - 1) * Convert.ToDouble(0.5)) + distance));
            }
            else if (MainPage_searchResultsInnerCanvas.Children.Count < 2)
            {
                DisplayErrorMessage(null, "No data found to display.");
            }
        }
        void PlotCandidateEllipseOnZooming(List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates, double distance)
        {
            for (int candidateIndex = 0; candidateIndex < candidates.Count; candidateIndex++)
            {
                EllipsePosition ellipsePos = ellipsePositionsLst.GetEllipsePosition(candidateIndex);
                if (ellipsePos != null)
                {
                    Point centerPoint = new Point();

                    centerPoint.X = Convert.ToDouble(((100 - candidates[candidateIndex].PERCENTAGE) * Convert.ToDecimal(distance))+ matchingCandidateDistance) * Math.Cos(ellipsePos.Angle) ;
                    centerPoint.Y = Convert.ToDouble(((100 - candidates[candidateIndex].PERCENTAGE) * Convert.ToDecimal(distance))+ matchingCandidateDistance) * Math.Sin(ellipsePos.Angle) ;

                    AddCandidateEllipse(candidates[candidateIndex], candidateIndex, centerPoint);
                }
            }
        }
        void PlotCandidateEllipse(List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates, double distance)
        {
            int level_interval = (SliderIntervals.Count - Current_Page) + 1;
            int candidate_index = 0;
            double radian = 45;
            var candidates_count = 0;
            Dictionary<int, int> intervalLst = new Dictionary<int, int>();
            int index = 0;
            for (index = 0; index < 100 / level_interval; index++)
            {

                candidates_count = (from candidate in candidates
                                    where candidate.PERCENTAGE > index * level_interval && candidate.PERCENTAGE <= (index * level_interval) + level_interval
                                    select candidate).Count();
                if (index == 0)
                    candidates_count = candidates_count + (from candidate in candidates
                                                           where candidate.PERCENTAGE == 0
                                                           select candidate).Count();
                intervalLst.Add(index, candidates_count);
            }
            candidates_count = (from candidate in candidates
                                where candidate.PERCENTAGE > index * level_interval
                                select candidate).Count();
            isMoreMatchingCandidate = ((from candidate in candidates
                                        where candidate.PERCENTAGE == 100
                                        select candidate).Count() > 1 ? true : false);
            matchingCandidateDistance = isMoreMatchingCandidate ? Constants.MACHING_CAND_DISTANCT : 0;
            intervalLst.Add(index, candidates_count);
            double plotting_angle = 0;
            double angle = 0;
            ellipsePositionsLst = new EllipsePositions();
            // Paint  the candidates from last.
            for (int intervalIndex = intervalLst.Count - 1; intervalIndex >= 0; intervalIndex--)
            {
                int value = intervalLst[intervalIndex];
                if (value == 0)
                    continue;
                if (value < 4)
                    radian = 45;
                else
                    radian = 360 / value;
                angle = Math.PI * radian / 180.0;
                for (int i = value - 1; i >= 0; i--)
                {
                    GetEllipseCenterPosition(candidates, ref candidate_index, angle, ref plotting_angle, distance);
                }
            }
        }

        // Get the ellipse center position without overlapping.
        Point GetEllipseCenterPosition(List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates, ref int candidate_index, double angle, ref double plotting_angle, double distance)
        {
            double tempPlotting_angle = 0;
            double xValue = 0;
            double yValue = 0;

            Point centerPoint = new Point();
            Point currentPoint = new Point();
            Point tempCenterPoint = new Point();
            
            xValue = Convert.ToDouble(((100 - candidates[candidate_index].PERCENTAGE) * Convert.ToDecimal(distance)) + matchingCandidateDistance) * Math.Cos(plotting_angle);
            yValue = Convert.ToDouble(((100 - candidates[candidate_index].PERCENTAGE) * Convert.ToDecimal(distance)) + matchingCandidateDistance) * Math.Sin(plotting_angle);
            
            centerPoint.X = xValue;
            centerPoint.Y = yValue;
            
            currentPoint = centerPoint;
            tempCenterPoint = centerPoint;
            tempPlotting_angle = plotting_angle;
            bool secondTrial = false;
            // Check whether the calculated position is already existing (check for overlapping).
            while (ellipsePositionsLst.CheckBoundries(currentPoint))
            {
                xValue = Convert.ToDouble((100 - candidates[candidate_index].PERCENTAGE) * Convert.ToDecimal(distance)) * Math.Cos(tempPlotting_angle);
                yValue = Convert.ToDouble((100 - candidates[candidate_index].PERCENTAGE) * Convert.ToDecimal(distance)) * Math.Sin(tempPlotting_angle);
                currentPoint.X = xValue;
                currentPoint.Y = yValue;
                tempPlotting_angle += angle;
                if (Math.Abs(currentPoint.X - tempCenterPoint.X) < 45 && Math.Abs(currentPoint.Y - tempCenterPoint.Y) < 45)
                {
                    // Check the second trial. If second trial is not completed, then reduce the angle as half and search&rotate again.
                    if (secondTrial)
                    {
                        currentPoint = centerPoint;
                        tempPlotting_angle = plotting_angle;
                        break;
                    }
                    secondTrial = true;
                    tempPlotting_angle += angle / 2;
                    xValue = Convert.ToDouble((100 - candidates[candidate_index].PERCENTAGE) * Convert.ToDecimal(distance)) * Math.Cos(tempPlotting_angle);
                    yValue = Convert.ToDouble((100 - candidates[candidate_index].PERCENTAGE) * Convert.ToDecimal(distance)) * Math.Sin(tempPlotting_angle);
                    tempCenterPoint.X = xValue;
                    tempCenterPoint.Y = yValue;
                    currentPoint = tempCenterPoint;

                }
            }
            plotting_angle = tempPlotting_angle;
            centerPoint = currentPoint;

            AddCandidateEllipse(candidates[candidate_index], candidate_index, centerPoint);
            ellipsePositionsLst.Add(new EllipsePosition(centerPoint, plotting_angle, candidate_index));
            plotting_angle += angle;
            ++candidate_index;
            return centerPoint;
        }
        /// <summary>
        /// This method gets called to remove the candidate ellipse
        /// </summary>
        /// <param name="candidates">
        /// This parameter contains the candidate information like name, address, phone number, email id etc.,
        /// </param>
        void RemoveCandidates(List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates)
        {
            // Before clearing the InnerCanvas, take the copy of center ellipse 
            // and restore it later.
            Ellipse centerEllp = new Ellipse();
            centerEllp = MainPage_centerEllipse;
            // Reset the InnerCanvas by clearing the ellipses
            MainPage_searchResultsInnerCanvas.Children.Clear();

            MainPage_searchResultsInnerCanvas.Children.Add(centerEllp);

            if (candidates != null && candidates.Count != 0)
            {
                double distance;
                if (candidates.Count == 1)
                    distance = 3;
                else
                {
                    double innerCanvasHalfWidth = ((MainPage_OuterGrid.ActualWidth - MainPage_rightPaneUserControl.ActualWidth) / 2) - 100; //Subtract constant value 100 to avoid displaying the ellipse circle at the end.
                    distance = innerCanvasHalfWidth / Convert.ToDouble(candidates[0].PERCENTAGE - candidates[candidates.Count - 1].PERCENTAGE);
                    distance = distance < 3 ? 3 : distance;
                }
                Zooming_Constant = Convert.ToInt32((Convert.ToDouble(Current_Page - 1) * Convert.ToDouble(0.5)) + distance);
                PlotCandidateEllipse(candidates, ((Convert.ToDouble(Current_Page - 1) * Convert.ToDouble(0.5)) + distance));
            }
        }
        public void UnSelectAllCandidates()
        {
            EllipseControl previousEllipse;
            if (selectedCandidates == null || selectedCandidates.Count == 0)
                return;
            foreach (string candidateId in selectedCandidates)
            {
                // Change the previously selected candidates to blue color.     
                previousEllipse = ((EllipseControl)MainPage_searchResultsInnerCanvas.FindName(candidateId));
                if (previousEllipse == null)
                    continue;
                if (previousEllipse.CandidateDetails.IsPicked == "N")
                {
                    Style style = previousEllipse.Resources["RedEllipseButton"] as Style;
                    previousEllipse.EllipseButton.Style = style;
                }
            }
            selectedCandidates.Clear();
        }

        /// <summary>
        /// This method opens the searchJobBoard window and sets the height of the popup
        /// </summary>
        public void OpenSearchJobBoardWindow()
        {
            searchJobBoard.Title = "Search Career Builder";
            searchJobBoard.BindGridValues();
            // Calculate the height of popup according to application height. 
            if (App.Current.Host.Content.ActualHeight > 650)
                searchJobBoard.Height = 600;
            else
                searchJobBoard.Height = App.Current.Host.Content.ActualHeight - 25;
            searchJobBoard.Show();
        }
        void RemoveCandidate()
        {
            List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates = scoreDetails.CandidateDetails.ToList();
            if (candidates != null && candidates.Count != 0)
            {
                // Reset the summary details.

                MainPage_rightPaneUserControl.SetScoreChartData();
                RemoveCandidates(candidates);
                SetSummaryDetails(scoreDetails.SearchTerms_NoOfCandidates.ToList(), scoreDetails.SearchTermSummaryDetails);
                EllipseControl ellipse = ((EllipseControl)
                    MainPage_searchResultsInnerCanvas.FindName(candidates[0].CANDIDATE_ID.ToString()));
                ellipseClickEventHandler((object)(ellipse), null);
            }
            else if (MainPage_searchResultsInnerCanvas.Children.Count < 2)
            {
                DisplayErrorMessage(null, "No data found to display.");
            }
        }

        /// <summary>
        /// This method generates the technical skills for the specified search terms.
        /// </summary>
        /// <returns></returns>
        public List<CompetencyVector> GenerateTechnicalSkillsByCandidateId(int candidateId)
        {
            // Display the technical skills in datagrid.
            List<CompetencyVector> vectors = new List<CompetencyVector>();
            var query = from techDetails in scoreDetails.TechnicalSkills
                        where techDetails.CANDIDATE_ID == candidateId
                        select new CompetencyVector
                        {
                            Skill = techDetails.SKILL_NAME,
                            Recency = techDetails.RECENCY,
                            NoOfYears = techDetails.NO_OF_YEAR,
                            CertificationDetails = techDetails.CERTIFICATION_DETAILS,
                            Certification = techDetails.CERTIFICATION
                        }
                        ;
            return query.ToList();
        }
        public void ShowHideLoading(bool isEnable)
        {
            if (!isEnable)
            {
                // Before clearing the InnerCanvas, take the copy of center ellipse 
                // and restore it later.
                Ellipse centerEllp = new Ellipse();
                centerEllp = MainPage_centerEllipse;
                // Reset the InnerCanvas by clearing the ellipses
                MainPage_searchResultsInnerCanvas.Children.Clear();
                MainPage_searchResultsInnerCanvas.Children.Add(centerEllp);
                MainPage_progressBar.Name = "Fetching, parsing & evaluating...";
                MainPage_OuterGrid.MouseWheel -= new MouseWheelEventHandler(MainPage_OuterGrid_MouseWheel);
                MainPage_OuterGrid.MouseWheel += new MouseWheelEventHandler(MainPage_OuterGrid_MouseWheel);
            }
            else
            {
                Thumb verticalThumb = MainPage_zoomSliderControl.VerticalThumb;
                if (verticalThumb != null && verticalThumb.IsDragging)
                    verticalThumb.CancelDrag();
            }

            this.IsEnabled = isEnable;
            MainPage_progressBar.IsIndeterminate = !isEnable;
            MainPage_progressBar.Visibility = (isEnable ? Visibility.Collapsed : Visibility.Visible);
        }
        private void MainPage_skillsDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            List<CompetencyVector> compVectors = GenerateTechnicalSkillsByCandidateId(((Candidate)dg.DataContext).CandidateId);
            if (compVectors != null && compVectors.Count != 0)
                dg.ItemsSource = compVectors.AsEnumerable();
            else
                dg.Visibility = Visibility.Collapsed;

        }

        private void MainPage_GridView_RelevancyCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            Candidate cand = (((Canvas)sender).DataContext) as Candidate;

            ((Rectangle)((Canvas)sender).Children[1]).Width = Convert.ToInt32((cand.Percentage * 65) / 100);

        }
        #endregion Private Methods

        #region Event Handlers

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (PositionProfileId == 0)
                return;

            if (parentPage == "PP_REVIEW")
            {
                HtmlPage.Window.Navigate(new Uri(UIUrl + "PositionProfile/PositionProfileReview.aspx?m=1&s=1&positionprofileid="
                    + PositionProfileId + " &parentpage=S_POS_PRO&origin=TS", UriKind.Absolute), "_self");
            }
            else if (parentPage == "PP_WORKFLOW")
            {
                HtmlPage.Window.Navigate(new Uri(UIUrl + "PositionProfile/PositionProfileWorkflowSelection.aspx?m=1&s=1&positionprofileid="
                     + PositionProfileId + " &parentpage=PP_REVIEW&origin=TS", UriKind.Absolute), "_self");
            }
            else if (parentPage == "PP_STATUS")
            {
                HtmlPage.Window.Navigate(new Uri(UIUrl + "PositionProfile/PositionProfileStatus.aspx?m=1&s=1&positionprofileid="
                     + PositionProfileId + " &parentpage=PP_REVIEW&origin=TS", UriKind.Absolute), "_self");
            }
        }

        /// <summary>
        /// This method displays the profile window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_profileImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                CompetencyVectorWindow competencyVector = new CompetencyVectorWindow();
                Candidate candidate = (((Canvas)sender).DataContext) as Candidate;
                if (candidate == null)
                    return;
                competencyVector.Title = candidate.FirstName + " Profile";

                // Calculate the height of popup according to application height. 
                if (App.Current.Host.Content.ActualHeight > 650)
                    competencyVector.Height = 650;
                else
                    competencyVector.Height = App.Current.Host.Content.ActualHeight - 25;
                competencyVector.CompetencyVectorWindow_verticalScrollViewer.ScrollToTop();
                competencyVector.Show();
                competencyVector.DisplayCompetencyVectors(candidate.CandidateId, candidate.CandidateResumeId);
            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// Handler event for the mouse leave for 'cheat sheet' label in
        /// search bar layout control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_searchBarLayoutControl_onMouseLeave(object sender, MouseEventArgs e)
        {
            CheatSheetPopUp.IsOpen = false;
        }

        /// <summary>
        /// Handler event for the mouse enter for 'cheat sheet' label in
        /// search bar layout control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_searchBarLayoutControl_onMouseEnter(object sender, MouseEventArgs e)
        {
            CheatSheetPopUp.VerticalOffset = e.GetPosition(null).Y;
            CheatSheetPopUp.HorizontalOffset = e.GetPosition(null).X;
            CheatSheetPopUp.IsOpen = true;
        }

        /// <summary>
        /// This handler gets called on changing the zoom slider value.
        /// According to the interval increment/decrement, add or remove the
        /// candidates in the inner panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_zoomSliderControl_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {

                if (e.NewValue != e.OldValue && e.OldValue != 0)
                {
                    if (zoomTransform == null)
                    {
                        zoomTransform = new ScaleTransform();
                        zoomTransform.CenterX = 0;
                        zoomTransform.CenterY = 0;
                    }
                    // Assign the new value to zoom transform.
                    zoomTransform.ScaleX = e.NewValue;
                    zoomTransform.ScaleY = e.NewValue;


                    if (SliderIntervals == null)
                        return;
                    int PageNo = 0;
                    double NewValue = Convert.ToDouble(e.NewValue.ToString("0.##"));
                    double temp = 0.0;
                    try
                    {
                        if (e.NewValue - e.OldValue < 0)
                            temp = SliderIntervals.Where(p => p < NewValue).ToList()[0];
                        else
                            temp = SliderIntervals.OrderBy(p => p > NewValue).ToList()[0];
                    }
                    catch (ArgumentOutOfRangeException)
                    { }
                    if (temp == 0.0)
                        return;
                    PageNo = SliderIntervals.IndexOf(temp);
                    if (temp != 0.0 && PageNo == 0)
                        PageNo = 1;
                    if (Current_Page == PageNo)
                        return;
                    if (PageNo != 0)
                        Current_Page = PageNo;

                    if (searchKeywords == "")
                        return;
                    //if (IsZoomMouseWheelRotated && MainPage_zoomSliderControl.Value == MainPage_zoomSliderControl.Minimum)
                    //    Current_Page = GetMaximumPageSizeFromSlider();
                    //else if (IsZoomMouseWheelRotated && MainPage_zoomSliderControl.Value == MainPage_zoomSliderControl.Maximum)
                    //    Current_Page = 2;

                    isSearchClick = false;
                    isAddCandidate = (e.NewValue < e.OldValue) ? true : false;
                    // If new value is less than old value, then add new candidates otherwise remove existing candidates.
                    //GetCandidatesBySearchTerm(
                    //    (e.NewValue < e.OldValue ? (IsZoomMouseWheelRotated ? ++Current_Page : Current_Page) :
                    //    Current_Page == 1 ? 1 : (IsZoomMouseWheelRotated ? --Current_Page : Current_Page))
                    //    , PAGE_SIZE);
                    GetCandidatesBySearchTerm(
                        (e.NewValue < e.OldValue ? Current_Page :
                        Current_Page == 1 ? 1 : Current_Page)
                        , PAGE_SIZE);
                }

            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "");
            }
        }
        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            MainPage_searchBarLayoutControl.onClick += new RoutedEventHandler(SearchButton_Click);
            MainPage_searchBarLayoutControl.onMouseEnter += new MouseEventHandler(MainPage_searchBarLayoutControl_onMouseEnter);
            MainPage_searchBarLayoutControl.onMouseLeave += new MouseEventHandler(MainPage_searchBarLayoutControl_onMouseLeave);
            searchBar.SearchBarLayoutControl_SearchTextBox.UpdateLayout();
            searchBar.SearchBarLayoutControl_SearchTextBox.Focus();
        }

        /// <summary>
        /// This handler gets called on clicking on the ellipse. This method changes the
        /// selected candidate color to green and displays the candidate details on 
        /// right panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ellipseClickEventHandler(object sender, RoutedEventArgs e)
        {
            try
            {

                DisplayCandidateDetails(((EllipseControl)sender).Name, ((EllipseControl)sender).CandidateDetails);
                EllipseControl selectedEllipse = ((EllipseControl)sender);
                MainPage_searchResultsInnerCanvas.Children.Remove((EllipseControl)sender);
                MainPage_searchResultsInnerCanvas.Children.Add(selectedEllipse);
                if (selectedCandidates == null)
                    selectedCandidates = new List<string>();
                ModifierKeys keys = Keyboard.Modifiers;
                bool ctrl = (keys & ModifierKeys.Control) != 0;
                if (!ctrl)
                {
                    UnSelectAllCandidates();
                }
                if (((EllipseControl)sender).CandidateDetails.IsPicked == "N")
                {
                    selectedCandidates.Add(((EllipseControl)sender).CandidateDetails.CandidateId.ToString());
                    ((EllipseControl)sender).EllipseButton.Style = (((EllipseControl)sender).Resources["GreenEllipseButton"] as Style);
                }
                if (((EllipseControl)sender).CandidateDetails.IsPicked == "Y")
                {
                    selectedCandidates.Add(((EllipseControl)sender).CandidateDetails.CandidateId.ToString());
                    ((EllipseControl)sender).EllipseButton.Style = (((EllipseControl)sender).Resources["BlueEllipseButton"] as Style);
                }
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "Exception Occurred");
            }
        }
        /// <summary>
        /// On mouse enter of ellipse, this method gets called and displays the hover popup.
        /// This method calculates the x, y position of hoverpopup and displays it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ellipseMouseEnterEventHandler(object sender, RoutedEventArgs e)
        {
            // TODO : Remove the hard-coded values and substitute the control's height/width
            try
            {
                double arrowLeft = 0;
                double arrowTop = 0;
                double rightPaneWidth = this.MainPage_rightPaneUserControl.RenderSize.Width;
                double searchPaneHeight = this.MainPage_searchBarLayoutControl.RenderSize.Height + MainPage_viewTabControl.RenderSize.Height;
                double hoverPopupHeight = this.MainPage_candidateHoverPopupControl.Height;
                double hoverPopupWidth = this.MainPage_candidateHoverPopupControl.Width;


                MainPage_candidateHoverPopup.IsOpen = true;
                EllipseControl ellipse = (sender as EllipseControl);
                ellipse.Cursor = Cursors.Hand;
                // Calculate the left property of ellipse by adding panning
                double left = (Convert.ToInt32(ellipse.GetValue(Canvas.LeftProperty)) + panTransform.X);
                double innerCanvasWidth = ((App.Current.Host.Content.ActualWidth - rightPaneWidth) / 2);
                // Apply scaling.
                left = left * zoomTransform.ScaleX;
                // Check if left goes beyond the screen
                if ((left + hoverPopupWidth) > innerCanvasWidth)
                {
                    left = (left - hoverPopupWidth) + ((ellipse.Width / 2) * zoomTransform.ScaleX);
                    arrowLeft = 195;
                }
                else
                {
                    left = left + ((ellipse.Width + 25) * zoomTransform.ScaleX);
                    arrowLeft = -7;
                }
                // Calculate the top property of ellipse by adding panning
                double top = (Convert.ToInt32(ellipse.GetValue(Canvas.TopProperty)) + panTransform.Y);
                double innerCanvasHeight = (App.Current.Host.Content.ActualHeight - searchPaneHeight) / 2;
                // Apply scaling.
                top = top * zoomTransform.ScaleY;
                // Check if left goes beyond the screen
                if ((top + hoverPopupHeight) > innerCanvasHeight)
                {
                    arrowTop = ((top + hoverPopupHeight) - innerCanvasHeight) + ((ellipse.Height - 30) * zoomTransform.ScaleY);
                    arrowTop = arrowTop > 160 ? 160 + 10 : arrowTop - 10;
                    top = top - ((top + hoverPopupHeight) - innerCanvasHeight);
                }
                else
                {
                    top = top + ((ellipse.Height - 30) * zoomTransform.ScaleY);
                    arrowTop = 0;
                }
                // webservice.CandidateFileExistCompleted += new EventHandler<CandidateFileExistCompletedEventArgs>(webservice_CandidateFileExistCompleted);
                //webservice.CandidateFileExistAsync(((EllipseControl)sender).CandidateDetails.CandidateId);

                MainPage_candidateHoverPopupControl.SetHoverArrow(arrowLeft, arrowTop, ((EllipseControl)sender).CandidateDetails.IsPicked, ((EllipseControl)sender).CandidateDetails.HasActivity,
                     ((EllipseControl)sender).CandidateDetails.CandidateId, CandidatePhotoPath, CandidateFileExist);
                MainPage_candidateHoverPopupControl.SetRelevancyBar(((EllipseControl)sender).CandidateDetails.Percentage, IsCloningApplied);
                MainPage_candidateHoverPopup.HorizontalOffset = left;
                MainPage_candidateHoverPopup.VerticalOffset = top;
                Candidate oCandidate = ((EllipseControl)sender).CandidateDetails;
                MainPage_candidateHoverPopupControl.DataContext = oCandidate;
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "");
            }
        }

        /// <summary>
        /// This handler hides the hover popup on leaving the mouse from candidate ellipse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ellipseMouseLeaveEventHandler(object sender, RoutedEventArgs e)
        {
            //((Ellipse)sender).Cursor = Cursors.Arrow;
            MainPage_candidateHoverPopup.IsOpen = false;

        }

        /// <summary>
        /// This method is called on clicking the search button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            RightPane rp = new RightPane();


            IsCloningApplied = false;
            IsSmallChangeValueChanged = false;
            isSearchClick = true;

            ShowHideLoading(false);
            webservice.CandidateFileExistCompleted += new EventHandler<CandidateFileExistCompletedEventArgs>(webservice_CandidateFileExistCompleted);
            webservice.CheckSearchUsageAsync(LoginUserId);
            //

            /* if (SelectedCandidate != null)
             {
                // webservice.GetCandidatePhotoPathAsync(SelectedCandidate.CandidateId);
                 webservice.GetCandidatePhotoPathCompleted += new EventHandler<GetCandidatePhotoPathCompletedEventArgs>(webservice_GetCandidatePhotoPathCompleted);
             }*/

        }

        void webservice_CandidateFileExistCompleted(object sender, CandidateFileExistCompletedEventArgs e)
        {
            CandidateFileExist = e.Result;

            //if (SelectedCandidate != null)
            //{
            if (!CandidateFileExist)
            {
                bmpCandidatePhoto = new BitmapImage(
                       new Uri(CandidatePhotoPath1 + "photo_not_available.PNG",
                       UriKind.Absolute));
            }
            else
            {
                bmpCandidatePhoto = new BitmapImage(
                       new Uri(CandidatePhotoPath1 + SelectedCandidate.CandidateId + ".PNG",
                       UriKind.Absolute));
            }
            //  }

            MainPage_rightPaneUserControl.RightPane_CandidatePhoto.Source = bmpCandidatePhoto;
        }

        /// <summary>
        /// This method gets called on clicking the hand symbol inside pan buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panHotSpot_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

        }

        /// <summary>
        /// This handler pans the screen to top by PAN_YVALUE pixels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void topHotSpot_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            panTransform.Y -= PAN_YVALUE;
        }

        /// <summary>
        /// This handler pans the screen to bottom by PAN_YVALUE pixels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bottomHotSpot_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            panTransform.Y += PAN_YVALUE;
        }

        /// <summary>
        /// This handler pans the screen to left by PAN_XVALUE pixels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void leftHotSpot_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            panTransform.X -= PAN_XVALUE;
        }

        /// <summary>
        /// This handler pans the screen to right by PAN_XVALUE pixels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rightHotSpot_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            panTransform.X += PAN_XVALUE;
        }

        /// <summary>
        /// This handler displays the hover popup on entering the mouse over hover popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_candidateHoverPopup_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MainPage_candidateHoverPopup.IsOpen = true;
        }

        /// <summary>
        /// This handler hides the hover popup on leaving the mouse from hover popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_candidateHoverPopup_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MainPage_candidateHoverPopup.IsOpen = false;
        }

        /// <summary>
        /// This handler resets the pan values to initial position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_resetImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            panTransform.Y = 0;
            panTransform.X = 0;
            Zooming_Constant = 3;
            MainPage_zoomSliderControl.Value = 1;
        }

        /// <summary>
        /// This handler starts the pan on mouse down of grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                MainPage_OuterGrid.Cursor = Cursors.IBeam;
                if (e.OriginalSource is Grid)
                {
                    if (((Grid)e.OriginalSource).Name == "MainPage_OuterGrid")
                    {
                        previousX = e.GetPosition(MainPage_searchResultsInnerCanvas).X;
                        previousY = e.GetPosition(MainPage_searchResultsInnerCanvas).Y;
                        isPanStarted = true;
                    }
                }
                else if (e.OriginalSource is Ellipse)
                {
                    if (((Ellipse)e.OriginalSource).Name == "MainPage_centerEllipse")
                    {
                        previousX = e.GetPosition(MainPage_searchResultsInnerCanvas).X;
                        previousY = e.GetPosition(MainPage_searchResultsInnerCanvas).Y;
                        isPanStarted = true;
                    }
                }


            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "");
            }
        }

        /// <summary>
        /// This handler calculates the position of screen to move on panning.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_OuterGrid_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                if (((TabItem)MainPage_viewTabControl.SelectedItem).Header.ToString() == "Grid View")
                    return;
                // Check whether the pan is started.
                if (isPanStarted)
                {
                    double currentX = e.GetPosition(MainPage_searchResultsInnerCanvas).X;
                    double currentY = e.GetPosition(MainPage_searchResultsInnerCanvas).Y;
                    double diffX = currentX - previousX;
                    double diffY = currentY - previousY;
                    panTransform.Y += diffY;
                    panTransform.X += diffX;
                }
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "");
            }
        }

        /// <summary>
        /// On releasing the mouse, stop the panning.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_OuterGrid_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (((TabItem)MainPage_viewTabControl.SelectedItem).Header.ToString() == "Grid View")
                return;
            MainPage_OuterGrid.Cursor = Cursors.Arrow;
            isPanStarted = false;
        }

        /// <summary>
        /// Event handler that will be called when Mouse enter either on 
        /// rightpane or searchbar control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_OtherUserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                MainPage_OuterGrid.MouseWheel -= new MouseWheelEventHandler(MainPage_OuterGrid_MouseWheel);
            }
            catch (Exception exp)
            {
                DisplayErrorMessage(exp, "");
            }
        }

        /// <summary>
        /// Event handler that will be called when Mouse leave either on 
        /// rightpane or searchbar control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_OtherUserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                MainPage_OuterGrid.MouseWheel -= new MouseWheelEventHandler(MainPage_OuterGrid_MouseWheel);
                MainPage_OuterGrid.MouseWheel += new MouseWheelEventHandler(MainPage_OuterGrid_MouseWheel);
            }
            catch (Exception exp)
            {
                DisplayErrorMessage(exp, "");
            }
        }

        /// <summary>
        /// Event handler that will be called when Mouse wheel scrolled
        /// by the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_OuterGrid_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            try
            {
                if ((searchBar.SearchBarLayoutControl_SearchTextBox.Text == null) || (searchBar.SearchBarLayoutControl_SearchTextBox.Text == ""))
                {
                    e.Handled = false;
                    return;
                }
                
                if (((TabItem)MainPage_viewTabControl.SelectedItem).Header.ToString() == "Grid View")
                    return;
                //if (e.Delta > 0 && MainPage_zoomSliderControl.Maximum == MainPage_zoomSliderControl.Value)
                //    return;
                //if (e.Delta < 0 && MainPage_zoomSliderControl.Minimum == MainPage_zoomSliderControl.Value)
                //    return;
                //IsZoomMouseWheelRotated = true;
                //if (e.Delta > 0)
                //    MainPage_zoomSliderControl.Value = MainPage_zoomSliderControl.SmallChange + MainPage_zoomSliderControl.Value;
                //else if (e.Delta < 0)
                //    MainPage_zoomSliderControl.Value = MainPage_zoomSliderControl.Value - MainPage_zoomSliderControl.SmallChange;

                if (Convert.ToDouble(Zooming_Constant.ToString("0.#")) == 0.4 && e.Delta < 0)
                    return;
                if (e.Delta >= 0)
                {
                    if (Zooming_Constant < 1)
                        Zooming_Constant += 0.2;
                    else
                        Zooming_Constant += 1;
                }
                else if (e.Delta < 0)
                {
                    if (Zooming_Constant > 3)
                        Zooming_Constant -= 1;
                }

                PlotCandidateEllipseOnZooming(scoreDetails.CandidateDetails.ToList(), Zooming_Constant);

            }
            catch (Exception exp)
            {
                DisplayErrorMessage(exp, "");
            }
            finally
            {

            }
        }

        private void MainPage_gridViewFormatButton_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBlock txt = (TextBlock)sender;
            if (txt.Text == "Advance")
            {
                txt.Text = "Simple";
                MainPage_gridViewDataGrid.RowHeight = 120;
                MainPage_gridViewDataPager.PageSize = 4;
            }
            else
            {
                txt.Text = "Advance";
                MainPage_gridViewDataGrid.RowHeight = 45;
                MainPage_gridViewDataPager.PageSize = 13;
            }
        }
        private void MainPage_viewTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MainPage_gridViewStackPanel == null || MainPage_ellipseViewGrid == null)
                return;
            if (((TabItem)e.AddedItems[0]).Header.ToString() == "Pan View")
            {
                MainPage_gridViewStackPanel.Visibility = Visibility.Collapsed;
                MainPage_ellipseViewGrid.Visibility = Visibility.Visible;
            }
            else
            {
                MainPage_gridViewStackPanel.Visibility = Visibility.Visible;
                MainPage_ellipseViewGrid.Visibility = Visibility.Collapsed;
            }
        }
        private void MainPage_gridViewDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;
            DisplayCandidateDetails(((Candidate)e.AddedItems[0]).CandidateId.ToString(), ((Candidate)e.AddedItems[0]));

        }

        /// <summary>
        /// While loading each row in the GridView, refresh the Search Relavancy Canvas and Skills Datagrid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPage_gridViewDataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            Grid grid = ((Grid)MainPage_gridViewDataGrid.Columns[0].GetCellContent(e.Row));
            if (grid.DataContext == null)
                return;
            Canvas searchRelavancyCanvas = grid.FindName("MainPage_GridView_RelevancyCanvas") as Canvas;
            ((Rectangle)(searchRelavancyCanvas).Children[1]).Width = Convert.ToInt32((((Candidate)grid.DataContext).Percentage * 65) / 100);

            DataGrid skillsGrid = (DataGrid)grid.FindName("MainPage_skillsDataGrid");
            List<CompetencyVector> compVectors = GenerateTechnicalSkillsByCandidateId(((Candidate)grid.DataContext).CandidateId);
            if (compVectors != null && compVectors.Count != 0)
                skillsGrid.ItemsSource = compVectors.AsEnumerable();
            else
                skillsGrid.Visibility = Visibility.Collapsed;

        }
        #endregion Event Handlers

        #region WebService Completed Methods
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetTechnicalSkillsByCandidateId(). Retrieved data gets displayed
        /// in skills grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the results of competency vectors.
        /// </param>
        void webservice_GetTechnicalSkillsByCandidateIdCompleted(object sender, GetTechnicalSkillsByCandidateIdCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                    LogException(e.Error);
                SetCursorStyle(Cursors.Wait);
                MainPage_rightPaneUserControl.DataSource = null;
                if (e.Result == null || e.Result.Count == 0)
                    return;
                technicalSkills = e.Result.ToList();
                List<CompetencyVector> vectors = new List<CompetencyVector>();
                CompetencyVector vector;
                bool flag = false;
                List<CandidateTechnicalSkills> sortedTechSkills = null;
                List<CandidateTechnicalSkills> nonExistingTechSkills = null;
                Regex regEx;
                Match match;
                try
                {
                    foreach (CandidateTechnicalSkills skill in technicalSkills)
                    {
                        flag = false;
                        regEx = new Regex((@"\b" + skill.VECTOR_NAME_ALIAS.Replace(",", @"\b|\b") + @"\b"), RegexOptions.IgnoreCase);

                        foreach (SearchTerm searchTerm in searchTerms)
                        {
                            match = regEx.Match(searchTerm.Keyword);
                            if (match.Success)
                            {
                                flag = true;
                                if (sortedTechSkills == null)
                                    sortedTechSkills = new List<CandidateTechnicalSkills>();
                                sortedTechSkills.Add(skill);
                            }
                        }
                        if (flag == false)
                        {
                            if (nonExistingTechSkills == null)
                                nonExistingTechSkills = new List<CandidateTechnicalSkills>();
                            nonExistingTechSkills.Add(skill);
                        }
                    }
                }
                catch
                {
                    sortedTechSkills = null;
                    nonExistingTechSkills = technicalSkills;
                }
                if (nonExistingTechSkills != null)
                {
                    foreach (CandidateTechnicalSkills skill in nonExistingTechSkills)
                    {
                        if (sortedTechSkills == null)
                            sortedTechSkills = new List<CandidateTechnicalSkills>();
                        sortedTechSkills.Add(skill);
                    }
                }
                if (sortedTechSkills != null)
                {
                    foreach (CandidateTechnicalSkills skill in sortedTechSkills)
                    {

                        vector = new CompetencyVector();
                        vector.Skill = skill.SKILL_NAME;
                        vector.Recency = skill.RECENCY.Trim();
                        vector.NoOfYears = skill.NO_OF_YEAR.Trim();
                        vector.Certification = skill.CERTIFICATION.Trim();
                        vector.CertificationDetails = skill.CERTIFICATION_DETAILS.Trim();

                        vectors.Add(vector);
                    }
                }

                MainPage_rightPaneUserControl.DataSource = vectors.AsEnumerable();
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "Invalid Technical skills.");
            }
            finally
            {
                SetCursorStyle(Cursors.Arrow);
            }
        }

        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetScoreDetails(). Using retrieved candidate information by using 
        /// assessment data, ellipses are created in the main panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the results of ScoreDetails which contains candidate information.
        /// </param>
        void webservice_add_GetScoreDetailsCompleted(object sender, GetScoreDetailsCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                    LogException(e.Error);

                SetCursorStyle(Cursors.Wait);
                scoreDetails = e.Result;
                PagedCollectionView tempListView = new PagedCollectionView(GetCandidateDetails(scoreDetails));
                MainPage_gridViewDataGrid.ItemsSource = tempListView;

                AddCandidate();
                ShowHideRightPaneIcons();
                
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "Invalid details.");
            }
            finally
            {
                ShowHideLoading(true);
                SetCursorStyle(Cursors.Arrow);
            }
        }
        void ShowHideRightPaneIcons()
        {
            MainPage_rightPaneUserControl.RightPane_addNoteImage.Visibility = Visibility.Visible;

            MainPage_rightPaneUserControl.RightPane_createTestImage.Visibility = Visibility.Visible;
            MainPage_rightPaneUserControl.RightPane_pickedCandidatesListImage.Visibility = Visibility.Visible;
            if (PositionProfileId == 0)
            {
                MainPage_rightPaneUserControl.RightPane_positionProfileImage.Visibility = Visibility.Collapsed;
                MainPage_rightPaneUserControl.RightPane_positionProfileFreesearchImage.Visibility = Visibility.Visible;
                MainPage_ReturnPositionProfile_StackPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                MainPage_rightPaneUserControl.RightPane_positionProfileImage.Visibility = Visibility.Visible;
                MainPage_rightPaneUserControl.RightPane_positionProfileFreesearchImage.Visibility = Visibility.Collapsed;
                MainPage_ReturnPositionProfile_StackPanel.Visibility = Visibility.Visible;
                MainPage_PositionProfileLabel.Text = positionProfileName;
                
            }
        }
        void webservice_add_GetCandidatesByKeywordsCompleted(object sender, GetCandidatesByKeywordsCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                    LogException(e.Error);

                SetCursorStyle(Cursors.Wait);
                scoreDetails = e.Result;
                PagedCollectionView tempListView = new PagedCollectionView(GetCandidateDetails(scoreDetails));
                MainPage_gridViewDataGrid.ItemsSource = tempListView;

                AddCandidate();

                ShowHideRightPaneIcons();
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "Invalid details.");
            }
            finally
            {
                ShowHideLoading(true);
                SetCursorStyle(Cursors.Arrow);
            }
        }
        public List<Candidate> GetCandidateDetails(ScoreDetails scoreDetails)
        {
            string selSource = searchBar.SearchBarLayoutControl_sourceCombo.SelectionBoxItem.ToString();
            var query = from candDetails in scoreDetails.CandidateDetails
                        let phone = new PhoneNumber()
                        {
                            Office = candDetails.OFFICE_PHONE,
                            Residence = candDetails.HOME_PHONE
                        }
                        let contact = new ContactInformation
                        {
                            City = candDetails.CITY,
                            State = candDetails.STATE,
                            Country = candDetails.COUNTRY,
                            EmailAddress = candDetails.EMAIL_ID,
                            Phone = phone
                        }
                        let name = new Name
                        {
                            FirstName = candDetails.FIRSTNAME,
                            LastName = candDetails.LASTNAME
                        }
                        let resume = new Resume
                        {
                            ContactInformation = contact,
                            Name = name,
                            ExecutiveSummary = candDetails.SUMMARY
                        }

                        select new Candidate
                        {
                            CandidateId = candDetails.CANDIDATE_ID,
                            CandidateResumeId=candDetails.CAND_RESUME_ID,
                            Name = candDetails.CANDIDATE_NAME,
                            Resume = resume,
                            FirstName = candDetails.FIRSTNAME,
                            AssessmentScore = selSource == "Specialized Assessment Data" ? Convert.ToDouble(candDetails.SCORE) : 0,
                            ResumeScore = selSource == "Resumes" ? Convert.ToDouble(candDetails.SCORE) : 0,
                            CloningScore = IsCloningApplied == true ? Convert.ToDouble(candDetails.SCORE) : 0,
                            Score = Convert.ToDouble(candDetails.SCORE),
                            Percentage = Convert.ToDouble(candDetails.PERCENTAGE),
                            IsPicked = candDetails.ISPICKED,
                            Recruiter = candDetails.RECRUITER,
                            HasActivity = candDetails.HASACTIVITY
                            //,
                            //  CandidateImage = candDetails.PHOTO.Bytes

                        };
            return query.ToList();
        }
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetResumeScoreDetails(). Using retrieved candidate information by using 
        /// resume source data, ellipses are created in the main panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the results of ScoreDetails which contains candidate information.
        /// </param>
        void webservice_add_GetResumeScoreDetailsCompleted(object sender, GetResumeScoreDetailsCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                    LogException(e.Error);
                SetCursorStyle(Cursors.Wait);
                scoreDetails = e.Result;
                PagedCollectionView tempListView = new PagedCollectionView(GetCandidateDetails(scoreDetails));
                MainPage_gridViewDataGrid.ItemsSource = tempListView;
                MainPage_gridViewDataGrid.UpdateLayout();
                AddCandidate();

                ShowHideRightPaneIcons();

            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "Invalid details.");
            }
            finally
            {
                ShowHideLoading(true);
                SetCursorStyle(Cursors.Arrow);
            }
        }

        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetScoreDetails(). Using retrieved candidate information by using  
        /// assessment data, ellipses are created in the main panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the results of ScoreDetails which contains candidate information.
        /// </param>
        void webservice_remove_GetScoreDetailsCompleted
            (object sender, GetScoreDetailsCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                    LogException(e.Error);
                scoreDetails = e.Result;

                RemoveCandidate();
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "No data found to remove.");
            }
        }
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetScoreDetails(). Using retrieved candidate information by using  
        /// assessment data, ellipses are created in the main panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the results of ScoreDetails which contains candidate information.
        /// </param>
        void webservice_remove_GetCandidatesByKeywordsCompleted
            (object sender, GetCandidatesByKeywordsCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                    LogException(e.Error);
                scoreDetails = e.Result;

                RemoveCandidate();
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "No data found to remove.");
            }
        }
        
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetResumeScoreDetails(). Using retrieved candidate information by using  
        /// resume data, ellipses are created in the main panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the results of ScoreDetails which contains candidate information.
        /// </param>
        void webservice_remove_GetResumeScoreDetailsCompleted
            (object sender, GetResumeScoreDetailsCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                    LogException(e.Error);
                scoreDetails = e.Result;


                RemoveCandidate();
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "No data found to remove.");
            }
        }

        void webservice_CloseCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            DisplayErrorMessage(null, "Invalid Data");
        }

        void webservice_GetClonedCandidatesByCandidateIdCompleted(object sender, GetClonedCandidatesByCandidateIdCompletedEventArgs e)
        {
            if (e.Error != null)
                LogException(e.Error);
            List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates = e.Result.ToList();
            DisplayClonedCandidates(candidates);
        }
        void webservice_GetAdvancedClonedCandidatesCompleted(object sender, GetAdvancedClonedCandidatesCompletedEventArgs e)
        {
            if (e.Error != null)
                LogException(e.Error);
            List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates = e.Result.ToList();
            DisplayClonedCandidates(candidates);
        }
        void webservice_GetAdminSettingsCompleted(object sender, GetAdminSettingsCompletedEventArgs e)
        {
            if (e.Error != null)
                LogException(e.Error);
            if (e.Result == null || e.Result.Count == 0)
                return;
            NumberOfSpheresFromPrasOptions = e.Result[0].TALENT_SCOUT_SPHERES;
        }
        private void MainPage_BackImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (PositionProfileId != 0)
                HtmlPage.Window.Navigate(new Uri(UIUrl + "PositionProfile/SearchPositionProfile.aspx?m=1&s=1&parentpage=TS_HOME", UriKind.Absolute), "_self");
        }
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// InsertSearchLog(). 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the status of subscription plan.
        /// </param>
        void webservice_InsertSearchLogCompleted(object sender, InsertSearchLogCompletedEventArgs e)
        {
        }
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// InsertCloneLog(). Using retrieved information, it checks whether the 
        /// maximum limit is exceeded or not. According to the maximum limit validation,
        /// it clones the candidates or displays error message.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the status of subscription plan.
        /// </param>
        void webservice_InsertCloneLogCompleted(object sender, InsertCloneLogCompletedEventArgs e)
        {
            List<SPTSINSERT_LOGResult> featureUsage = e.Result.ToList();

            if (Convert.ToInt32(featureUsage[0].ISEXCEED) == 0)
            {
                int? userID = null;

                if (searchBar.SearchBarLayoutControl_showMyCandidatesCheckBox.IsChecked == true)
                    userID = LoginUserId;

                webservice.GetClonedCandidatesByCandidateIdAsync(TenantId, ClonedCandidateId,ClonedCandidateResumeId, 0, PAGE_SIZE, userID);
            }
            else
            {
                DisplayErrorMessage(null, "You have reached the maximum limit based your subscription plan. Cannot clone candidates.");

            }
        }
        void webservice_CheckSearchUsageCompleted(object sender, CheckSearchUsageCompletedEventArgs e)
        {
            List<SPTSINSERT_LOGResult> featureUsage = e.Result.ToList();

            if (Convert.ToInt32(featureUsage[0].ISEXCEED) == 0)
            {
                SearchTerm searchTerm = null;
                string[] splitSearchTermsByComma = null;
                Current_Page = 1;
                try
                {
                    string selSource = searchBar.SearchBarLayoutControl_sourceCombo.SelectionBoxItem.ToString();
                    if (selSource == "Resumes" || selSource == "CareerBuilder")
                        this.IsResume = true;
                    else if (selSource == "Specialized Assessment Data")
                        this.IsResume = false;
                    //Clear the search panel and right panel.
                    ClearValues(true);
                    if (searchBar.SearchBarLayoutControl_SearchTextBox.Text.Trim() == "")
                    {
                        DisplayErrorMessage(null, "Search terms cannot be empty.");
                        return;
                    }
                    if (SplitSearchString(selSource))
                    {
                        // Reset the panning and zooming.
                        panTransform.Y = 0;
                        panTransform.X = 0;
                        MainPage_zoomSliderControl.Value = 1;
                        searchBar.SearchBarLayoutControl_ErrorMessageTextBlock.Visibility = Visibility.Collapsed;
                        pageNumber = 1;
                        isAddCandidate = true;
                        pageSize = PAGE_SIZE;
                        //webservice.InsertSearchLogAsync(searchKeywords, positionProfileName, LoginUserId);
                        GetCandidatesBySearchTerm(pageNumber, pageSize);
                    }
                }
                catch (Exception ex)
                {
                    DisplayErrorMessage(ex, ex.Message);
                }
                finally
                {
                    if (splitSearchTermsByComma != null) splitSearchTermsByComma = null;
                    if (searchTerm != null) searchTerm = null;
                    searchBar.SearchBarLayoutControl_SearchTextBox.Focus();
                }
            }
            else
            {
                DisplayErrorMessage(null, "You have reached the maximum limit based your subscription plan. Cannot search more candidate");
            }

        }
        bool SplitSearchString(string source)
        {
            SearchTerm searchTerm = null;
            string[] splitSearchTermsByComma = null;
            // Split the search terms and assign in SearchTerm instances.
            searchBar.SearchBarLayoutControl_SearchTextBox.SelectionBackground = new SolidColorBrush(Colors.Black);
            searchBar.SearchBarLayoutControl_SearchTextBox.SelectionForeground = new SolidColorBrush(Colors.White);
            splitSearchTermsByComma = searchBar.SearchBarLayoutControl_SearchTextBox.Text.Trim().Split(',');
            if (searchTerms != null)
                searchTerms = null;
            searchTerms = new SearchTerms();
            int startIndex = 0;
            int endIndex = 0;
            int weightage = 0;
            int totalAssignedWeight = 0;
            try
            {
                for (int index = 0; index < splitSearchTermsByComma.Length; index++)
                {
                    if (splitSearchTermsByComma[index] == "")
                        continue;
                    if (splitSearchTermsByComma[index].IndexOf("[") >= 0 && splitSearchTermsByComma[index].IndexOf("]") >= 0)
                    {
                        startIndex = splitSearchTermsByComma[index].IndexOf("[");
                        endIndex = splitSearchTermsByComma[index].LastIndexOf("]") - 1;
                        if (source != "Keywords")
                        {
                            weightage = Convert.ToInt32(splitSearchTermsByComma[index].Substring(startIndex + 1, endIndex - startIndex));
                            if (weightage < 0)
                            {
                                DisplayErrorMessage(null, "Invalid search string. See Cheat Sheet for sample");
                                CheckAndSelectInvalidText(false);
                                return false;
                            }
                        }
                        searchTerm = new SearchTerm();
                        if (source != "Keywords")
                            searchTerm.Weightage = Convert.ToInt32(splitSearchTermsByComma[index].Substring(startIndex + 1, endIndex - startIndex));
                        else
                            searchTerm.Weightage = 0;
                        searchTerm.Keyword = splitSearchTermsByComma[index].Substring(0, splitSearchTermsByComma[index].IndexOf("[")).Trim();
                        if (searchTerm.Keyword == "")
                        {
                            DisplayErrorMessage(null, "Invalid search string. See Cheat Sheet for sample");
                            CheckAndSelectInvalidText(false);
                            return false;
                        }
                        searchTerms.Add(searchTerm);
                    }
                    else if (splitSearchTermsByComma[index].IndexOf("[") < 0 && splitSearchTermsByComma[index].IndexOf("]") < 0)
                    {
                        searchTerm = new SearchTerm();
                        searchTerm.Keyword = splitSearchTermsByComma[index].Trim();
                        searchTerm.Weightage = 0;
                        searchTerms.Add(searchTerm);
                    }
                    else if ((splitSearchTermsByComma[index].IndexOf("[") >= 0 && splitSearchTermsByComma[index].IndexOf("]") < 0) ||
                        (splitSearchTermsByComma[index].IndexOf("]") >= 0 && splitSearchTermsByComma[index].IndexOf("[") < 0))
                    {
                        DisplayErrorMessage(null, "Invalid search string. See Cheat Sheet for sample");
                        CheckAndSelectInvalidText(false);
                        return false;
                    }
                    startIndex = 0;
                    endIndex = 0;
                    weightage = 0;
                }
                if (source != "Keywords")
                    totalAssignedWeight = searchTerms.GetTotalAssignedWeight();
            }
            catch (FormatException)
            {
                CheckAndSelectInvalidText(false);
                DisplayErrorMessage(null, "Invalid search string. See Cheat Sheet for sample");
                return false;
            }
            if (totalAssignedWeight > 100)
            {
                CheckAndSelectInvalidText(false);
                DisplayErrorMessage(null, "Invalid search string. See Cheat Sheet for sample");
                return false;
            }
            if (source != "Keywords")
            {
                searchBar.SearchBarLayoutControl_SearchTextBox.Text = searchTerms.GetBalancedWeightSearchTerms();
                searchKeywords = searchTerms.GetSearchKeywordsWithWeightage();
                searchBar.SearchBarLayoutControl_assignedWgtBalTextBlock.Text = "assigned weight balance = " + searchTerms.AssignedBalanceWeight.ToString();
            }
            else
            {
                searchKeywords = searchBar.SearchBarLayoutControl_SearchTextBox.Text = searchTerms.GetKeywordsAlone();
            }
            CheckAndSelectInvalidText(true);
            return true;
        }
        void webservice_CheckJobBoardLoginCredentialsCompleted(object sender, CheckJobBoardLoginCredentialsCompletedEventArgs e)
        {
            try
            {
                if (e.Result != null)
                {
                    SessionToken = e.Result.ToString();

                    OpenSearchJobBoardWindow();
                }
                else
                {
                    DisplayErrorMessage(null, "Invalid credentials in cache");
                }
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex, "Invalid credentials in cache");
            }
        }

        /// <summary>
        /// This method triggers on completion of the LogExceptionMessage() method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void webservice_LogExceptionMessageCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            // Do nothing.
        }

        /// <summary>
        /// This method triggers on completion of the LogException() method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void webservice_LogExceptionCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            // Do nothing.
        }

        #endregion WebService Completed Methods

    }
}
