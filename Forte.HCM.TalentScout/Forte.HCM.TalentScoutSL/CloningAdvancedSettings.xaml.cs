﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [CloningAdvancedSettings.xaml.cs]
// File that represents the class to display the slider controls to adjust the 'No. Of Years', 
// Recency and Certification to allow the user to clone the selected candidate.
#endregion

#region Namespaces                                                              
using System;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Controls;
#endregion

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class displays the slider controls to adjust the 'No. Of Years', 
    /// Recency and Certification to allow the user to clone the selected candidate.
    /// </summary>
    public partial class CloningAdvancedSettings : ChildWindow
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public CloningAdvancedSettings()
        {
            InitializeComponent();
        }
        #region Event Handlers                                                  
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder clone_advanced_settings = new StringBuilder();
            clone_advanced_settings.Append("TECHNICALSKILLS-NOOFYEARS:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_TSNoofYears.Value));
            clone_advanced_settings.Append(",");
            clone_advanced_settings.Append("TECHNICALSKILLS-RECENCY:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_TSRecency.Value));
            clone_advanced_settings.Append(",");
            clone_advanced_settings.Append("TECHNICALSKILLS-CERTIFICATION:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_TSCertification.Value));

            clone_advanced_settings.Append(",");

            clone_advanced_settings.Append("VERTICALS-NOOFYEARS:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_VNoofYears.Value));
            clone_advanced_settings.Append(",");
            clone_advanced_settings.Append("VERTICALS-RECENCY:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_VRecency.Value));
            clone_advanced_settings.Append(",");
            clone_advanced_settings.Append("VERTICALS-CERTIFICATION:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_VCertification.Value));

            clone_advanced_settings.Append(",");

            clone_advanced_settings.Append("ROLES-NOOFYEARS:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_RNoofYears.Value));
            clone_advanced_settings.Append(",");
            clone_advanced_settings.Append("ROLES-RECENCY:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_RRecency.Value));
            clone_advanced_settings.Append(",");
            clone_advanced_settings.Append("ROLES-CERTIFICATION:");
            clone_advanced_settings.Append(Convert.ToInt32(CloningAdvancedSettings_RCertification.Value));

            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            mainPage.DisplayAdvancedClonedCandidates(clone_advanced_settings.ToString());
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
#endregion Event Handlers
    }
    /// <summary>
    /// This class will round the decimal values to integer on dragging the slider.
    /// </summary>
	public class SliderValueConverter : IValueConverter
    {        
        /// <summary>
        /// This method converts the decimal values to integer.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, 
            object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            else
                return Math.Round(System.Convert.ToDouble(value.ToString()));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

