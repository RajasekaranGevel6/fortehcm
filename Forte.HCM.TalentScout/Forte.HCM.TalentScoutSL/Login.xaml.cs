﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Login.xaml.cs
// File that represents the user interface for TalentScout System Login.
// This will Authenticate the user and enters in to system based 
// on the role.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using Forte.HCM.TalentScoutSL.DataObjects;
using Forte.HCM.TalentScoutSL.Utils;
using System.Windows.Browser;
#endregion

namespace Forte.HCM.TalentScoutSL
{
	public partial class Login : UserControl
    {

        #region Declarations                                                   

        private TalentScoutServiceReference.TalentScoutServiceClient webservice = null;

        #endregion Declarations

        #region Constructor                                                    
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public Login()
		{
			// Required to initialize variables
			InitializeComponent();
        }

        #endregion Constructor

        #region Events                                                         
        
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                HtmlPage.Plugin.Focus();
                Login_userNameTextBox.UpdateLayout();
                Login_userNameTextBox.Focus();
            }
            catch (Exception exp)
            {
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                Login_bottomErrorMessageLabel.Text = exp.Message;
            }
        }

        private void Login_loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsUserNameEmpty = false;
                ClearErrorMessageLabel();
                if (Login_userNameTextBox.Text.Trim() == "")
                    IsUserNameEmpty = true;
                //Login_bottomErrorMessageLabel.Text = string.Format(App_GlobalResources.TalentScoutResource.Login_TextBoxEmpty, "UserName ");
                if (Login_passwordTextBox.Password.Trim() != "")
                    CallWebServiceToAuthenticateUser();
                else
                    Login_bottomErrorMessageLabel.Text = string.Format(
                        App_GlobalResources.TalentScoutResource.Login_TextBoxEmpty, (IsUserNameEmpty) ? "UserName,Password " : "Password ");
            }
            catch (Exception exp)
            {
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                Login_bottomErrorMessageLabel.Text = exp.Message;
            }
        }

        private void Login_troubleLogin_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // TODO: Add event handler implementation here.
        }

        private void Login_troubleLogin_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                Login_troubleLogin.Style = Application.Current.Resources["TextBlockMouseOverStyle"] as Style;
            }
            catch (Exception exp)
            {
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
            }
        }

        private void Login_troubleLogin_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                Login_troubleLogin.Style = Application.Current.Resources["LinkButtonNormalStyle"] as Style;
            }
            catch (Exception exp)
            {
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
            }
        }

        private void TextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.PlatformKeyCode == 13)
                    Login_loginButton_Click(null, null);
            }
            catch (Exception exp)
            {
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                Login_bottomErrorMessageLabel.Text = exp.Message;
            }
        }

        #endregion Events

        #region Private Methods                                                

        private void CallWebServiceToAuthenticateUser()
        {
            try
            {
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.CheckUserAuthenticationCompleted +=
                    new EventHandler<CheckUserAuthenticationCompletedEventArgs>(webClient_CheckUserAuthenticationCompleted);
                webservice.CheckUserAuthenticationAsync(Login_userNameTextBox.Text, Login_passwordTextBox.Password);
                this.Cursor = Cursors.Wait;
            }
            finally
            {
                if (webservice != null) webservice = null;
            }
        }

        private void ClearErrorMessageLabel()
        {
            Login_bottomErrorMessageLabel.Text = "";
        }

        //private void BindServiceReference()
        //{
        //    if (webservice != null)
        //        return;
        //    Uri address = new Uri(Application.Current.Host.Source, App_GlobalResources.TalentScoutResource.TalentScout_ServiceReference);
        //    webservice = new TalentScoutServiceClient(App_GlobalResources.TalentScoutResource.TalentScout_BasicHTTPBinding,
        //        address.AbsoluteUri);
        //}

        private void GoToMainPage(string UserEmail)
        {
            Grid root = Application.Current.RootVisual as Grid;
            root.Children.RemoveAt(0);
            root.Children.Add(new MainPage(string.Empty, 0, 0, string.Empty,0,string.Empty,2, string.Empty));
            IsolatedFileSettings.WriteToIsolatedFile(UserEmail);
        }

        #region Web Service Completed Methods                                  

        void webClient_CheckUserAuthenticationCompleted(object sender, TalentScoutServiceReference.CheckUserAuthenticationCompletedEventArgs e)
        {
            //USER_ROLE user_Roles = null;
            try
            {
                if (e.Result == null || e.Result.Count == 0)
                {
                    Login_bottomErrorMessageLabel.Text = App_GlobalResources.TalentScoutResource.Login_InvalidUserLogin;
                    this.Cursor = Cursors.Arrow;
                    return;
                }
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.CheckForUserRoleCompleted += new EventHandler<CheckForUserRoleCompletedEventArgs>(webservice_CheckForUserRoleCompleted);
                webservice.CheckForUserRoleAsync(e.Result[0].USERID,
                    App_GlobalResources.TalentScoutResource.TalentScout_TalentScoutFeatureUrl);
            }
            catch (Exception exp)
            {
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                Login_bottomErrorMessageLabel.Text = exp.Message;
            }
        }

        void webservice_CheckForUserRoleCompleted(object sender, CheckForUserRoleCompletedEventArgs e)
        {
            try
            {
                bool ApplicationAccessRight = false;
                if (e.Result == null || e.Result.Count == 0)
                {
                    Login_bottomErrorMessageLabel.Text = App_GlobalResources.TalentScoutResource.Login_AdminLoginMessage;
                    return;
                }
                for (int i = 0; i < e.Result.Count; i++)
                    if (e.Result[i].ROLE_RIGHT_VIEW)
                    {
                        ApplicationAccessRight = true;
                        break;
                    }
                if (ApplicationAccessRight)
                    GoToMainPage(e.Result[0].FEATURE_NAME);
                else
                    Login_bottomErrorMessageLabel.Text = App_GlobalResources.TalentScoutResource.Login_AdminLoginMessage;
            }
            catch (Exception exp)
            {
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                Login_bottomErrorMessageLabel.Text = exp.Message;
            }
            finally
            {
                this.Cursor = Cursors.Arrow;
            }
        }

        #endregion Web Service Completed Methods

        #endregion Private Methods

    }
}