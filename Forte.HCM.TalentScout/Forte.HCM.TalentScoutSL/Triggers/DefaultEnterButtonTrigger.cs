﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [DefaultEnterButtonTrigger.cs]
// [File that represents the class to set the target button as default button. Pressing 
// enter key will trigger the target button click event.]

#endregion

#region Namespaces                                                              
using System.Windows.Input;
using System.Windows.Automation.Peers;
using System.Windows.Interactivity;
using System.Windows.Controls.Primitives;
using System.Windows.Automation.Provider;
#endregion

namespace Forte.HCM.TalentScoutSL.Triggers
{
    /// <summary>
    /// This class represents the trigger action that enables on pressing the enter key and triggers the 
    /// click event of the target button.
    /// </summary>
    public class DefaultEnterButtonTrigger : TargetedTriggerAction<ButtonBase>
    {
        /// <summary>
        /// Gets or sets the peer.
        /// </summary>
        /// <value>The peer.</value>
        private AutomationPeer _peer { get; set; }
        
        /// <summary>
        /// Gets or sets the target button
        /// </summary>
        private ButtonBase _targetedButton { get; set; }
        
        /// <summary>
        /// Called after the TargetedTriggerAction is attached to an AssociatedObject.
        /// </summary>
        /// <remarks>Override this to hook up functionality to the AssociatedObject.</remarks>
        protected override void OnAttached()
        {
            base.OnAttached();

            _targetedButton = this.Target;

            if (null == _targetedButton)
            {
                return;
            }

            // set peer
            this._peer = FrameworkElementAutomationPeer.FromElement(_targetedButton);
            if (this._peer == null)
            {
                this._peer = FrameworkElementAutomationPeer.CreatePeerForElement(_targetedButton);
            }

        }

        /// <summary>
        /// Called after targeted Button change.
        /// </summary>
        /// <remarks>Override this to hook up functionality to the new targeted Button.</remarks>
        protected override void OnTargetChanged(ButtonBase oldTarget, ButtonBase newTarget)
        {
            base.OnTargetChanged(oldTarget, newTarget);

            _targetedButton = newTarget;
            if (null == _targetedButton)
            {
                return;
            }

            // set peer
            this._peer = FrameworkElementAutomationPeer.FromElement(_targetedButton);
            if (this._peer == null)
            {
                this._peer = FrameworkElementAutomationPeer.CreatePeerForElement(_targetedButton);
            }

        }

        /// <summary>
        /// Invokes the targeted Button when Enter key is pressed inside Control.
        /// </summary>
        /// <param name="parameter">KeyEventArgs with Enter key</param>
        protected override void Invoke(object parameter)
        {
            KeyEventArgs keyEventArgs = parameter as KeyEventArgs;
            if (null != keyEventArgs && keyEventArgs.Key == Key.Enter)
            {
                if (null != _peer && _peer.IsEnabled())
                {
                    IInvokeProvider invokeProvider = _peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
                    invokeProvider.Invoke();
                }
            }
        }
    }
}
