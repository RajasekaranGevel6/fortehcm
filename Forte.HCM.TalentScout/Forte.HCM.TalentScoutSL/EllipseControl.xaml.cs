﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [EllipseControl.xaml.cs]
// File that represents the class to display the ellipse control

#endregion

#region Namespaces                                                              
using System.Windows;
using System.Windows.Controls;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class displays the ellipse control with candidate information.
    /// </summary>
	public partial class EllipseControl : UserControl
	{
        /// <summary>
        /// Click event to trigger on candidate ellipse.
        /// </summary>
		public event RoutedEventHandler onClick;
        private Candidate candidate;
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
		public EllipseControl()
		{
			// Required to initialize variables
			InitializeComponent();
		}
        
        /// <summary>
        /// This property stores the CandidateDetails. If candidate is 
        /// exact match, then display in green color.
        /// </summary>
		public Candidate CandidateDetails
		{
			get
			{
				return candidate as Candidate;
			}
			set
			{
                
				candidate= value;
                if (candidate == null)
                    return;
                if (candidate.IsPicked=="Y")
                {
                    Style style = this.Resources["BlueEllipseButton"] as Style;
                    EllipseButton.Style = style;
                }
                else if (candidate.IsExactMatch == true)
                {
                    Style style = this.Resources["GreenEllipseButton"] as Style;
                    EllipseButton.Style = style;
                }
                EllipseButton.Content = GetTrimmedName(candidate.FirstName,candidate.Resume.Name.LastName);
                
                EllipseButton.VerticalAlignment = VerticalAlignment.Center;
                EllipseButton.HorizontalAlignment = HorizontalAlignment.Center;
                EllipseButton.FontSize = 10;
                EllipseButton.FontFamily = new System.Windows.Media.FontFamily("Arial");
                    
			}
		}
        public string GetTrimmedName(string firstName, string lastName)
        {
            string trimmedFirstName = "";
            string trimmedLastName = "";

            if (firstName != null)
                trimmedFirstName = (firstName.Length < 7) ? firstName : (firstName.Substring(0, 7) + ((firstName.Length > 7) ? "..." : ""));
            else
                trimmedFirstName = "";

            if (lastName != null)
                trimmedLastName = (lastName.Length <7) ? lastName : (lastName.Substring(0, 7) + ((lastName.Length > 7) ? "..." : ""));
            else
                trimmedLastName = "";

            return trimmedFirstName + " " + trimmedLastName;
        }
        /// <summary>
        /// This handler triggers on clicking the searched ellipse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void SearchResultEllipse_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
            if (onClick != null)
                onClick(this, new RoutedEventArgs());
		}
	}
}