﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [ViewResume.xaml.cs]

#endregion

#region Namespaces                                                              

using System;
using System.IO;
using System.Windows.Browser;
using System.Windows.Controls;

using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using System.Windows;
using Forte.HCM.TalentScoutSL.Utils;
using System.Text.RegularExpressions;
using System.Windows.Media;

#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    public partial class ViewResume : ChildWindow
    {

        #region Private Variables                                              

        private SPTSGET_RESUME_SOURCE_BYCANDIDATEIDResult resumeResult;
        private MainPage mainPage;
        #endregion Private Variables

        #region Constructor                                                    

        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public ViewResume()
        {
            // Required to initialize variables
            InitializeComponent();
        }

        #endregion Constructor

        #region Event Handlers                                                 

        /// <summary>
        /// This method gets called on completion of the webservice request GetResumeByCandidateId().
        /// It retrieves the Resume_source from db and displays it in textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter consists of the  <see cref="SPTSGET_RESUME_SOURCE_BYCANDIDATEIDResult"/>
        /// results with Candidate_id and Resume_source.
        /// </param>
        void webservice_GetResumeByCandidateIdCompleted(object sender, TalentScoutServiceReference.GetResumeByCandidateIdCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                string result = (e.Result[0] as SPTSGET_RESUME_SOURCE_BYCANDIDATEIDResult).RESUME_SOURCE;
                result = result.Replace("\n", "<br/>");
                result = result.Replace("<B><F>", "");
                result = result.Replace("</F></B>", "");
                for (int index = 0; index < mainPage.searchTerms.Count; index++)
                {
                    result = Utility.ReplaceActual(result, @mainPage.searchTerms[index].Keyword,index);
                }
                
                ViewResume_resumeTextBox.Text = result;
                resumeResult = e.Result[0] as SPTSGET_RESUME_SOURCE_BYCANDIDATEIDResult;
            }
            GenerateKeyLabels();
        }
        private void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
        }
        private void GenerateKeyLabels()
        {
            ViewResume_topPanel.Children.Clear();
            for (int index = mainPage.searchTerms.Count-1; index >=0; index--)
            {
                
                TextBlock txt=new TextBlock();
                txt.Text=mainPage.searchTerms[index].Keyword.ToUpper();
                txt.Margin = new Thickness(5);
                Style style = Application.Current.Resources["GrayTextHeader"] as Style;
                txt.Style = style;

                ViewResume_topPanel.Children.Insert(0, txt);
                Canvas canvas = new Canvas();
                canvas.Width = 6;
                canvas.Height = 6;
                canvas.Margin = new Thickness(10, 5, 0, 5);
                canvas.Background = new SolidColorBrush(Utility.mColors[index]);
                ViewResume_topPanel.Children.Insert(0, canvas);
            }
        }
        /// <summary>
        /// This method downloads the resume string as a text file. It displays the save dialog
        /// to get the file path from user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewResume_download_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (resumeResult == null || resumeResult.RESUME_SOURCE.Length == 0)
                return;
            Uri address = new Uri(Application.Current.Host.Source,
                       "../DownloadResume.aspx?resumeid=" + resumeResult.CANDIDATE_ID);
            HtmlPage.Window.Navigate(new Uri(address.AbsoluteUri, UriKind.Absolute), "_self");
        }
               
        #endregion Event Handlers

        #region Public Methods                                                 

        /// <summary>
        /// This method calls the service request to get the resume by passing candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// This parameter represents the id of the candidate
        /// </param>
        public void DisplayResumeText(int candidateId,int candResumeId)
        {
            ViewResume_resumeTextBox.Text = string.Empty;
            TalentScoutServiceClient webservice = null;
            AddServiceReference.BindServiceReference(ref webservice);
            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            webservice.GetResumeByCandidateIdCompleted += new EventHandler<TalentScoutServiceReference.GetResumeByCandidateIdCompletedEventArgs>(webservice_GetResumeByCandidateIdCompleted);
            webservice.GetResumeByCandidateIdAsync(candidateId, candResumeId);
        }
       
        #endregion Public Methods

        
    }
}