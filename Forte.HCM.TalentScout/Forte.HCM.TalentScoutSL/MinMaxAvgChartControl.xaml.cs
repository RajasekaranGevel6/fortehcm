﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [MinMaxAvgChartControl.xaml.cs]

#endregion

#region Namespaces                                                              
using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Controls.DataVisualization.Charting;

using Forte.HCM.TalentScoutSL.Utils;
using Forte.HCM.TalentScoutSL.DataObjects;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;

#endregion

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class creates the chart control to display the minimum score,
    /// maximum score, average score and selected candidate score.
    /// </summary>
    public partial class MinMaxAvgChartControl : UserControl
    {
        private int minimum;
        private int maximum;
        private int average;
        private string xAxisTitle;
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public MinMaxAvgChartControl()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Creates the MinMaxAvg chart control.
        /// </summary>
        /// <param name="minMaxAvgTerm">
        /// This parameter contains the values for minimum score, maximum score and average score
        /// </param>
        public void CreateChart(SearchTerms_MinMaxReportDetails minMaxAvgTerm)
        {
            minimum = Convert.ToInt32(minMaxAvgTerm.MIN_SCORE);
            maximum = Convert.ToInt32(minMaxAvgTerm.MAX_SCORE);
            average = Convert.ToInt32(minMaxAvgTerm.AVG_SCORE);
            xAxisTitle = minMaxAvgTerm.SEARCH_TERM;

            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            var yMaximum = Utility.RoundAxisMaxValue(Convert.ToInt32(maximum));

            LinearAxis yAxis = MinMaxAvgChart.Axes[0] as LinearAxis;
            yAxis.Maximum = yMaximum;
            yAxis.Minimum = 0;
            yAxis.Interval = (yMaximum) / 5;

            CategoryAxis xAxis = MinMaxAvgChart.Axes[1] as CategoryAxis;
            xAxis.Title = xAxisTitle;

            List<MinMaxAvgChartValues> scores = new List<MinMaxAvgChartValues>();
            MinMaxAvgChartValues score = new MinMaxAvgChartValues();
            score.XAXIS_VALUE = "Min";
            score.YAXIS_VALUE = minimum;
            score.LABEL_VISIBILITY = (score.YAXIS_VALUE == 0) ? "Collapsed" : "Visible";
            score.GRADIENT_COLOR = Utility.CreateGradientBrush();
            scores.Add(score);

            score = new MinMaxAvgChartValues();
            score.XAXIS_VALUE = "Avg";
            score.YAXIS_VALUE = average;
            score.LABEL_VISIBILITY = (score.YAXIS_VALUE == 0) ? "Collapsed" : "Visible";
            score.GRADIENT_COLOR = Utility.CreateGradientBrush();
            scores.Add(score);

            score = new MinMaxAvgChartValues();
            score.XAXIS_VALUE = "Max";
            score.YAXIS_VALUE = maximum;
            score.LABEL_VISIBILITY = (score.YAXIS_VALUE == 0) ? "Collapsed" : "Visible";
            score.GRADIENT_COLOR = Utility.CreateGradientBrush();
            scores.Add(score);


            score = new MinMaxAvgChartValues();
            // Display candidate name with 3 characters and append ".."
            score.XAXIS_VALUE = mainPage.selectedCandidate.FirstName.Length > 3 ?
                mainPage.selectedCandidate.FirstName.Substring(0, 3) + ".." :
                mainPage.selectedCandidate.FirstName;

            var selCandidateSearchTermScore = (from item in mainPage.scoreDetails.SearchTerms_ScoreDistribution.ToList()
                                               where item.CANDIDATE_ID == mainPage.selectedCandidate.CandidateId &&
                                               item.SEARCH_TERM == xAxisTitle
                                               select item.SCORE);
            int selCandidateScore = 0;
            if (selCandidateSearchTermScore.Count() > 0)
            {
                selCandidateScore = Convert.ToInt32(selCandidateSearchTermScore.First());
            }
            else
            {
                if (mainPage.IsResume)
                    selCandidateScore = Convert.ToInt32(mainPage.SelectedCandidate.ResumeScore);
                else
                    selCandidateScore = Convert.ToInt32(mainPage.selectedCandidate.AssessmentScore);
            }
            score.YAXIS_VALUE = selCandidateScore;
            score.LABEL_VISIBILITY = (score.YAXIS_VALUE == 0) ? "Collapsed" : "Visible";
            score.GRADIENT_COLOR = Utility.CreateSelectedGradientBrush();

            // Insert the selected candidate bar between Min and Avg or between Avg and Max bars.
            if (average > selCandidateScore)
                scores.Insert(1, score);
            else
                scores.Insert(2, score);


            ColumnSeries cs = new ColumnSeries();
            cs.IndependentValueBinding = new Binding("XAXIS_VALUE");
            cs.DependentValueBinding = new Binding("YAXIS_VALUE");
            MinMaxAvgChart.Series.Add(cs);


            ((ColumnSeries)MinMaxAvgChart.Series[0]).ItemsSource = scores;

            // Set the tooltip string.
            StringBuilder tooltipStr = new StringBuilder();
            tooltipStr.Append("Chart comparing selected candidate's resume score with maximum resume score, ");

            tooltipStr.Append("minimum resume score, average resume score, along with overall rank of candidate");
            if (xAxisTitle != null)
                tooltipStr.Append(" for " + xAxisTitle + " search term");
            MinMaxAvgChart_tooltipTextBlock.Text = tooltipStr.ToString();
        }
    }
}