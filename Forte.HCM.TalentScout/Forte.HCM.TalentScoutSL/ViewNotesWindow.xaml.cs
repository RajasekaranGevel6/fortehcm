﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forte.HCM.TalentScoutSL.Utils;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using System.Windows.Data;

namespace Forte.HCM.TalentScoutSL
{
    public partial class ViewNotesWindow : ChildWindow
    {

        private TalentScoutServiceClient webservice = null;

        MainPage mainPage;

        int candidateID = 0;
        string candidateName = string.Empty;

        private PagedCollectionView pagedCollectionViewForResultSet = null;
        public ViewNotesWindow()
        {
            InitializeComponent();



            /*  if (mainPage == null)
                  mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
              mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;

              if (mainPage.SelectedCandidate.CandidateId != 0)
                  ViewNotesWindow_CandidateNameTextBlock.Text = mainPage.SelectedCandidate.FirstName;*/
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        #region Events

        /// <summary>
        /// Handler event when Search Button click is pressed
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="RoutedEventArgs"/> contains the events
        /// </param>
        private void SearchViewNotesWindow_searchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearErrorMessage();
                SearchViewNotesRequest();
                SearchViewNotesWindow_clientRequestDataGrid.SelectedIndex = -1;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchViewNotesWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method when the date control key down is pressed
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="KeyEventArgs"/> contains the events
        /// </param>
        private void ViewNotesWindow_clientRequestDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                e.Handled = true;
                if (e.Key != Key.Enter)
                    return;
                ClearErrorMessage();
                SearchViewNotesRequest();
                SearchViewNotesWindow_clientRequestDataGrid.SelectedIndex = -1;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchViewNotesWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method when the client id text box key down is pressed.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="KeyEventArgs"/> contains the events
        /// </param>
        private void ViewNotesWindow_TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;
                ClearErrorMessage();
                SearchViewNotesRequest();
                SearchViewNotesWindow_clientRequestDataGrid.SelectedIndex = -1;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchViewNotesWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        private void SearchViewNotesRequest()
        {

            object ActivityFromDate = "1/1/1753";
            object ActivityToDate = "12/31/9999";
            object ActivityType = null;
            object Comment = null;
            mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            AddServiceReference.BindServiceReference(ref webservice);

            webservice.GetViewNotesRequestCompleted -= new EventHandler<GetViewNotesRequestCompletedEventArgs>
                (webservice_GetViewNotesRequestCompleted);
            webservice.GetViewNotesRequestCompleted += new EventHandler<GetViewNotesRequestCompletedEventArgs>
                (webservice_GetViewNotesRequestCompleted);


            if (ViewNotesWindow_FromDate.SelectedDate.HasValue)
            {
                try
                {
                    ActivityFromDate = Convert.ToDateTime(ViewNotesWindow_FromDate.SelectedDate.Value);
                }
                catch (NullReferenceException)
                {
                    throw new NullReferenceException("Enter valid date");
                }
            }
            if (ViewNotesWindow_clientRequestDate.SelectedDate.HasValue)
            {
                try
                {
                    ActivityToDate = Convert.ToDateTime(ViewNotesWindow_clientRequestDate.SelectedDate.Value).AddDays(1);
                }
                catch (NullReferenceException)
                {
                    throw new NullReferenceException("Enter valid date");
                }
            }

            if (ViewNotesWindow_Activity.SelectionBoxItem != null)
            {
                if (ViewNotesWindow_Activity.SelectionBoxItem.ToString() != "Select activity type")
                {
                    ActivityType = SelectActivityType(ViewNotesWindow_Activity.SelectedIndex);
                }
            }



            if (ViewNotesWindow_commentTextBox.Text.Trim() != "")
                Comment = ViewNotesWindow_commentTextBox.Text.Trim();
            webservice.GetViewNotesRequestAsync(candidateID, ActivityType, null, ActivityFromDate,
                ActivityToDate, Comment, "D", "ACTIVITY_DATE", null, null);


        }

        private object SelectActivityType(int selectedIndex)
        {
            object activityType = null;

            switch (selectedIndex)
            {
                case 1:
                    activityType = "CA_CR_RU";
                    break;
                case 2:
                    activityType = "CA_CR_WBS";
                    break;
                case 3:
                    activityType = "CA_EDIT";
                    break;
                case 4:
                    activityType = "CA_LN_CREA";
                    break;
                case 5:
                    activityType = "CA_RSCH";
                    break;
                case 6:
                    activityType = "CA_SCH";
                    break;
                case 7:
                    activityType = "CA_USCH";
                    break;
                case 8:
                    activityType = "CA_NOTES";
                    break;
                case 9:
                    activityType = "CA_PP_AS";
                    break;
                case 10:
                    activityType = "CA_RSU_WBS";
                    break;
                case 11:
                    activityType = "CA_COMP";
                    break;
                default:
                    break;

            }
            return activityType;

        }

        private object SelectActivityType()
        {
            throw new NotImplementedException();
        }

        void webservice_GetViewNotesRequestCompleted(object sender, GetViewNotesRequestCompletedEventArgs e)
        {
            List<SPSEARCH_CANDIDATE_ACTIVITY_LOGResult> result = null;
            try
            {
                result = e.Result.ToList();
                int ToRemoveFromResult = -1;
                int TotalRecords = 0;
                for (int i = 0; i < result.Count; i++)
                {
                    if (result[i].TOTAL >= 0)
                        ToRemoveFromResult = i;
                    if (result[i].ACTIVITY_DATE == null)
                        continue;
                    //result[i].CREATED_DATE = Convert.ToDateTime(result[i].CREATED_DATE).ToString("MM/dd/yyyy");

                }
                if (ToRemoveFromResult >= 0)
                {
                    TotalRecords = (int)result[ToRemoveFromResult].TOTAL;
                    result.RemoveAt(ToRemoveFromResult);
                }
                if (pagedCollectionViewForResultSet != null)
                    pagedCollectionViewForResultSet = null;
                pagedCollectionViewForResultSet = new PagedCollectionView(result);
                if (App_GlobalResources.TalentScoutResource.TalentScout_SearchClientRequestDataGridPageSize == "")
                    SearchViewNotesWindow_pagingControlDataPager.PageSize = 10;
                else
                    SearchViewNotesWindow_pagingControlDataPager.PageSize =
                        Convert.ToInt32(App_GlobalResources.TalentScoutResource.TalentScout_SearchClientRequestDataGridPageSize);
                SearchViewNotesWindow_clientRequestDataGrid.ItemsSource = pagedCollectionViewForResultSet;
                pagedCollectionViewForResultSet.CurrentChanged += new EventHandler(pagedCollectionViewForResultSet_CurrentChanged);
                SearchViewNotesWindow_pagingControlDataPager.Source = pagedCollectionViewForResultSet;
                if (SearchViewNotesWindow_pagingControlDataPager.PageSize < result.Count)
                    SearchViewNotesWindow_pagingControlDataPager.Visibility = Visibility.Visible;
                else
                    SearchViewNotesWindow_pagingControlDataPager.Visibility = Visibility.Collapsed;
                if (result != null && result.Count == 0)
                    SearchViewNotesWindow_errorMessageLabel.Text = "No data found to display";
                if (mainPage == null)
                    mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchViewNotesWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
            finally
            {
                if (result != null) result = null;
                if (pagedCollectionViewForResultSet != null) pagedCollectionViewForResultSet = null;
            }
        }

        private void ClearErrorMessage()
        {
            SearchViewNotesWindow_errorMessageLabel.Text = "";
        }
        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            addNoteWindow.onClick += new RoutedEventHandler(addNoteWindow_onClick);
        }

        void addNoteWindow_onClick(object sender, RoutedEventArgs e)
        {
            SearchViewNotesRequest();
        }

        /// <summary>
        /// Handler method when the datagrid selected item is changed
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="SelectionChangedEventArgs"/> contains the selection changed
        /// events
        /// </param>
        private void SearchViewNotesWindow_clientRequestDataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
                if (IsPageChanging)
                    return;
                if (IsClosing && e.AddedItems.Count <= 0)
                    return;
                this.Title = string.Empty;
               ClearControls();
            }
            catch (Exception exp)
            {//BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchViewNotesWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method when the window is closing
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="System.ComponentModel.CancelEventArgs"/> contains the window closing 
        /// events
        /// </param>
        private void SearchViewNotesWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (IsClosing)
                    return;
                this.Title = "";
                ClearControls();
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchViewNotesWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        private void ClearControls()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Handler event that fires when data pager page event chaging
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> contains the events
        /// </param>
        private void SearchViewNotesWindow_pagingControlDataPager_PageChanging(object sender, EventArgs e)
        {
            try
            {
                IsPageChanging = true;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchViewNotesWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler event for the collection object of PagedCollectionView when
        /// Item source changing
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> contains the object of the sender
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> contains the events
        /// </param>
        private void pagedCollectionViewForResultSet_CurrentChanged(object sender, EventArgs e)
        {
            try
            {
                SearchViewNotesWindow_clientRequestDataGrid.SelectedIndex = -1;
                IsPageChanging = false;
            }
            catch (Exception exp)
            {
                //BindServiceReference();
                //webservice.LogExceptionAsync(exp.Message, exp.InnerException.ToString(), exp.StackTrace);
                SearchViewNotesWindow_errorMessageLabel.Text = exp.Message;
                Utility.WriteExceptionLog(exp);
            }
        }
        AddNoteWindow addNoteWindow = new AddNoteWindow();
        private void SearchViewNotesWindow_addNoteImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            addNoteWindow.Title = "Add Notes";
            addNoteWindow.AddNoteWindow_errorMessageLabel.Text = "";
            addNoteWindow.AddNoteWindow_notesTextBox.Text = "";
            addNoteWindow.OKButton.Focus();
            addNoteWindow.SetCandidateDetails(candidateID);
            addNoteWindow.Show();
        }
        #endregion Events

        public bool IsPageChanging { get; set; }

        public bool IsClosing { get; set; }

        internal void GetCandidateDetails(int candidate_ID, string candidate_Name)
        {
            candidateID = candidate_ID;
            candidateName = candidate_Name;

            ViewNotesWindow_CandidateNameTextBlock.Text = candidate_Name;

            ClearErrorMessage();
            SearchViewNotesRequest();
            SearchViewNotesWindow_clientRequestDataGrid.SelectedIndex = -1;
        }
    }
}

