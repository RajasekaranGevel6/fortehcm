﻿#region Namespaces
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.Generic;

using Forte.HCM.TalentScoutSL.Utils;
using Forte.HCM.TalentScoutSL.DataObjects;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This window is called on searching the career builder resumes
    /// </summary>
    public partial class SearchJobBoard : ChildWindow
    {
        #region Private Variables
        private TalentScoutServiceClient webservice;
        public string SessionKey;
        private MainPage mainPage;
        public SearchJobBoard()
        {
            InitializeComponent();
            webservice = new TalentScoutServiceClient();
            webservice.GetResumeDetailsCompleted += new EventHandler<GetResumeDetailsCompletedEventArgs>(webservice_GetResumeDetailsCompleted);
        }
        #endregion Private Variables
        
        #region Event Handlers
        /// <summary>
        /// This method is called on loading the OuterGrid. At the time of loading the grid, display
        /// the search keywords in searchJobBoard window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchJobBoard_outerGrid_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            if (mainPage.searchKeywords != null && mainPage.searchKeywords != "")
            {
                SearchJobBoard_simpleKeywordTextBox.Text = mainPage.searchTerms.GetSearchKeywords();
                SearchJobBoard_advanceKeywordTextBox.Text = mainPage.searchTerms.GetSearchKeywords();
            }
            SetJobBoardSearchWindowStatus(true);
        }
        /// <summary>
        /// This method is called on clicking the add location button in advance search.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SearchJobBoard_advanceAddLocationButton_Click(object sender, RoutedEventArgs e)
        {
            StackPanel stp = (StackPanel)SearchJobBoard_advancelocationTextBox.Parent;
            if (stp.Children.Count < 4)
            {
                TextBox txt = new TextBox();
                txt.Width = 150;
                txt.Height = 22;
                txt.Margin = new Thickness(5); ;
                Style style = Application.Current.Resources["ClientRequestPopupTextBox"] as Style;
                txt.Style = style;
                stp.Children.Insert(stp.Children.Count - 1, txt);
            }

        }
        /// <summary>
        /// This method is called on clicking the Search button in Advance tab. It 
        /// passes all parameters to carrer builder to get the suitable candidates.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SearchJobBoard_advanceSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SetJobBoardSearchWindowStatus(false);
                JobBoardSearchTerms searchTerm = new JobBoardSearchTerms();

                int pageSize = 1;
                if (SearchJobBoard_pagesizeTextBox.Text != "" && int.TryParse(SearchJobBoard_pagesizeTextBox.Text.ToString(), out pageSize))
                    pageSize = Convert.ToInt32(SearchJobBoard_pagesizeTextBox.Text);
                searchTerm.RowsPerPage = pageSize.ToString();
                searchTerm.PageNumber = "1";

                searchTerm.SessionToken = mainPage.SessionToken;
                searchTerm.Company = SearchJobBoard_companyNameTextBox.Text;
                searchTerm.ResumeTitle = SearchJobBoard_resumeTitleTextBox.Text;
                searchTerm.JobTitle = SearchJobBoard_advanceJobTitleTextBox.Text;
                searchTerm.Keywords = SearchJobBoard_advanceKeywordTextBox.Text;

                searchTerm.LanguagesSpoken = ((KeyValuePair<string, string>)SearchJobBoard_languagesComboBox.SelectionBoxItem).Value.ToString();
                searchTerm.ManagementExperience = SearchJobBoard_managementExpTextBox.Text;
                searchTerm.MinimumExperience = ((KeyValuePair<string, string>)SearchJobBoard_expMinYearsUsingComboBox.SelectionBoxItem).Value.ToString();
                searchTerm.MaximumExperience = ((KeyValuePair<string, string>)SearchJobBoard_expMaxYearsUsingComboBox.SelectionBoxItem).Value.ToString();
                searchTerm.MaximumSalary = SearchJobBoard_maxSalaryTextBox.Text;

                searchTerm.MinimumSalary = SearchJobBoard_minSalaryTextBox.Text;
                searchTerm.MinimumTravelRequirement = ((KeyValuePair<string, string>)SearchJobBoard_minLevelTravelUsingComboBox.SelectionBoxItem).Value.ToString();
                searchTerm.EmploymentType = GetSelectedItems(SearchJobBoard_employmentTypeItemsControl);
                searchTerm.School = SearchJobBoard_schoolTextBox.Text;
                searchTerm.MilitaryExperience = GetSelectedItems(SearchJobBoard_militaryExpItemsControl);
                searchTerm.ManagementExperience = GetSelectedItems(SearchJobBoard_managementLevelItemsControl);
                
                searchTerm.MinimumDegree = ((KeyValuePair<string, string>)SearchJobBoard_schoolMinDegUsingComboBox.SelectionBoxItem).Value.ToString();
                searchTerm.SearchPattern = ((KeyValuePair<string, string>)SearchJobBoard_advanceKeywordUsingComboBox.SelectionBoxItem).Value.ToString();
                searchTerm.WorkStatus = GetSelectedItems(SearchJobBoard_authorizationItemsControl);

                StackPanel stp = ((StackPanel)SearchJobBoard_advancelocationStackPanel);

                searchTerm.Location = SplitLocationString(((TextBox)stp.Children[0]).Text);
                if (stp.Children.Count == 3)
                {
                    searchTerm.FirstAlternateLocation = SplitLocationString(((TextBox)stp.Children[1]).Text);
                }
                if (SearchJobBoard_advancelocationStackPanel.Children.Count == 4)
                {
                    searchTerm.SecondAlternateLocation = SplitLocationString(((TextBox)stp.Children[2]).Text);
                }
                searchTerm.OrderBy = "+RELV";
                webservice.GetResumeDetailsAsync(searchTerm, mainPage.searchKeywords, 1, MainPage.PAGE_SIZE, mainPage.LoginUserId, mainPage.PositionProfileId, mainPage.TenantId);
            }
            catch (Exception exp)
            {
                mainPage.DisplayErrorMessage(null, exp.Message.ToString());
                this.Cursor = Cursors.Wait;
                this.Close();
                SetJobBoardSearchWindowStatus(true);
            }
        }
        /// <summary>
        /// This method is called on clicking the add location button in simple search.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SearchJobBoard_addLocationButton_Click(object sender, RoutedEventArgs e)
        {
            StackPanel stp = (StackPanel)SearchJobBoard_simpleLocationTextBox.Parent;
            if (stp.Children.Count < 4)
            {
                TextBox txt = new TextBox();
                txt.Width = 150;
                txt.Height = 22;
                txt.Margin = new Thickness(5);

                Style style = Application.Current.Resources["ClientRequestPopupTextBox"] as Style;
                txt.Style = style;
                stp.Children.Insert(stp.Children.Count - 1, txt);
            }
        }
        /// <summary>
        /// This method is called on clicking the Search button in Simple tab. It 
        /// passes all parameters to carrer builder to get the suitable candidates.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SearchJobBoard_simpleSearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SetJobBoardSearchWindowStatus(false);
                JobBoardSearchTerms searchTerm = new JobBoardSearchTerms();

                int pageSize = 1;
                if (SearchJobBoard_pagesizeTextBox.Text != "" && int.TryParse(SearchJobBoard_pagesizeTextBox.Text.ToString(), out pageSize))
                    pageSize = Convert.ToInt32(SearchJobBoard_pagesizeTextBox.Text);
                searchTerm.RowsPerPage = pageSize.ToString();
                searchTerm.PageNumber = "1";

                searchTerm.SessionToken = mainPage.SessionToken;
                searchTerm.Keywords = SearchJobBoard_simpleKeywordTextBox.Text;
                searchTerm.FreshnessInDays = ((KeyValuePair<string, string>)SearchJobBoard_simpleLastModifiedDropDownList.SelectionBoxItem).Value.ToString();

                StackPanel stp = ((StackPanel)SearchJobBoard_simpleLocationStackPanel.Children[0]);

                searchTerm.Location = SplitLocationString(((TextBox)stp.Children[0]).Text);
                if (stp.Children.Count == 3)
                {
                    searchTerm.FirstAlternateLocation = SplitLocationString(((TextBox)stp.Children[1]).Text);
                }
                if (SearchJobBoard_advancelocationStackPanel.Children.Count == 4)
                {
                    searchTerm.SecondAlternateLocation = SplitLocationString(((TextBox)stp.Children[2]).Text);
                }
                searchTerm.OrderBy = "+RELV";
                searchTerm.SearchPattern = "ALL";
                webservice.GetResumeDetailsAsync(searchTerm, mainPage.searchKeywords, 1, MainPage.PAGE_SIZE, mainPage.LoginUserId, mainPage.PositionProfileId, mainPage.TenantId);
            }
            catch (Exception exp)
            {
                mainPage.DisplayErrorMessage(null, exp.Message.ToString());
                this.Cursor = Cursors.Wait;
                this.Close();
                SetJobBoardSearchWindowStatus(true);
            }
        }
        #endregion Event Handlers

        #region Supporting Methods
        /// <summary>
        /// This method is called on opening the SearchJobBoard window. 
        /// It binds all the controls with the data values.
        /// </summary>
        public void BindGridValues()
        {
            CareerBuilder builder = new CareerBuilder();
            Dictionary<string, string> _usingDropDownDatasource = builder.GetUsingDropdown();
            SearchJobBoard_advanceKeywordUsingComboBox.ItemsSource = _usingDropDownDatasource;
            SearchJobBoard_advanceKeywordUsingComboBox.SelectedIndex = 0;


            SearchJobBoard_jobTitleUsingComboBox.ItemsSource = _usingDropDownDatasource;
            SearchJobBoard_jobTitleUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_companyNameUsingComboBox.ItemsSource = _usingDropDownDatasource;
            SearchJobBoard_companyNameUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_resumeTitleUsingComboBox.ItemsSource = _usingDropDownDatasource;
            
            SearchJobBoard_resumeTitleUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_resumeLastModifiedUsingComboBox.ItemsSource = builder.GetLastModified();
            SearchJobBoard_resumeLastModifiedUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_simpleLastModifiedDropDownList.ItemsSource = builder.GetLastModified();
            SearchJobBoard_simpleLastModifiedDropDownList.SelectedIndex = 0;

            SearchJobBoard_jobCategoriesUsingComboBox.ItemsSource = builder.GetJobTypeCode();
            SearchJobBoard_jobCategoriesUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_schoolMinDegUsingComboBox.ItemsSource = builder.GetDegree();
            SearchJobBoard_schoolMinDegUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_schoolMaxDegUsingComboBox.ItemsSource = builder.GetDegree();

            SearchJobBoard_employmentTypeItemsControl.ItemsSource = builder.GetEmploymentType();
            SearchJobBoard_schoolMaxDegUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_expMinYearsUsingComboBox.ItemsSource = builder.GetYear(1980);
            SearchJobBoard_expMinYearsUsingComboBox.SelectedIndex = 0;
            
            SearchJobBoard_expMaxYearsUsingComboBox.ItemsSource = builder.GetYear(1980);
            SearchJobBoard_expMaxYearsUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_minLevelTravelUsingComboBox.ItemsSource = builder.GetWorkExperienceLevelOfTravel();
            SearchJobBoard_minLevelTravelUsingComboBox.SelectedIndex = 0;
            SearchJobBoard_employmentTypeItemsControl.ItemsSource = builder.GetEmploymentType();
            SearchJobBoard_managementLevelItemsControl.ItemsSource = builder.GetManagementLevel();

            SearchJobBoard_authorizationItemsControl.ItemsSource = builder.GetWorkStatus();
            SearchJobBoard_languagesComboBox.ItemsSource = builder.GetLanguage();
            SearchJobBoard_languagesComboBox.SelectedIndex = 0;
            SearchJobBoard_militaryExpItemsControl.ItemsSource = builder.GetMiltaryExperience();
        }
        /// <summary>
        /// This method is used to split the location string as City, ZipCode and State
        /// and assiged the values to <see cref="LocationDetails"/> instance.
        /// </summary>
        /// <param name="pLocation"></param>
        /// <returns></returns>
        private LocationDetails SplitLocationString(string pLocation)
        {
            LocationDetails location = new LocationDetails();
            location.City = "";
            location.State = "";
            location.ZipCode = "";
            if (pLocation == "")
                return location;
            if (pLocation.IndexOf(',') > -1)
            {
                string[] splitLocation = pLocation.Split(',');
                location.City = splitLocation[0];
                int zipCode;
                if (int.TryParse(splitLocation[1], out zipCode))
                {
                    location.ZipCode = zipCode.ToString();
                }
                else
                {
                    location.State = splitLocation[1];
                }
            }
            else
            {
                location.City = pLocation;
            }
            return location;
        }
        /// <summary>
        /// This method concatenates all the selected checkboxes strings and returns the value.
        /// </summary>
        /// <param name="ctrl">This contains the <see cref="ItemsControl"/> with checkboxes</param>
        /// <returns></returns>
        private string GetSelectedItems(ItemsControl ctrl)
        {
            string selectedItems = "";
            foreach (var item in ctrl.Items)
            {
                if (item.GetType() == typeof(CheckBox))
                {
                    var checkbox = (CheckBox)item;
                    if ((bool)checkbox.IsChecked)
                    {
                        selectedItems = checkbox.Content.ToString() + "|";
                    }
                }
            }
            return selectedItems.TrimEnd('|');
        }
        /// <summary>
        /// This sets the status of the JobBoard window. While searching the candidates,
        /// it displays the progressbar and disables the window.
        /// </summary>
        /// <param name="isEnable"></param>
        private void SetJobBoardSearchWindowStatus(bool isEnable)
        {
            SearchJobBoard_searchTabControl.Name = "Loading...";
            SearchJobBoard_searchTabControl.IsEnabled = isEnable;
            SearchJobBoard_progressBar.IsIndeterminate = !isEnable;
            SearchJobBoard_progressBar.Visibility = (isEnable ? Visibility.Collapsed : Visibility.Visible);
        }
        #endregion Supporting Methods
        #region Webservice completion
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// GetResumeDetails(). Using retrieved information, it adds the 
        /// careerbuilder candidates in display.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the details of candidates and scores.
        /// </param>
        void webservice_GetResumeDetailsCompleted(object sender, GetResumeDetailsCompletedEventArgs e)
        {
            try
            {
                SetJobBoardSearchWindowStatus(true);

                if (e.Error != null)
                    Utility.LogException(e.Error);
                this.Cursor = Cursors.Wait;
                this.Close();
                if (e.errorMessage != "")
                {
                    mainPage.DisplayErrorMessage(null, e.errorMessage);
                    return;
                }
                mainPage.scoreDetails = e.Result;
                
                PagedCollectionView tempListView = new PagedCollectionView(mainPage.GetCandidateDetails(mainPage.scoreDetails));
                mainPage.MainPage_gridViewDataGrid.ItemsSource = tempListView;
                mainPage.AddCandidate();
            }
            catch (Exception ex)
            {
                mainPage.DisplayErrorMessage(ex, ex.Message);
            }
            finally
            {
                mainPage.ShowHideLoading(true);
                this.Cursor = Cursors.Arrow;
            }
        }
        #endregion Webservice completion
    }
}

