﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [ScorePieChart.xaml.cs]

#endregion Header

#region Namespaces                                                              
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    public partial class ScorePieChart : ChildWindow
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public ScorePieChart()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Method that assigns the pie chart datasource.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="firstName"></param>
        public void SetChartSource(IEnumerable<SearchTerms_ScorePercentage> data, string firstName)
		{
			((PieSeries)pieChart.Series[0]).ItemsSource = data;
            pieChart.Title = firstName+" - Scores among the search term";
		}
    }
}

