﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [RightPane.xaml.cs]

#endregion Header

#region Namespaces                                                              
using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Controls.DataVisualization.Charting;

using Forte.HCM.TalentScoutSL.Utils;
using Forte.HCM.TalentScoutSL.DataObjects;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using System.Windows.Input;

#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    public partial class RightPane : UserControl
    {
        #region Private Variables                                               
        private CompetencyVectorWindow competencyVector;
        private ViewResume vr;
        private ScoreHistogramChart histogramChart;
        private ScorePieChart pieChart;
        private ReportWindow reportWindow;
        //private EmailRecruiter emailRecruiter;
        private IEnumerable<CompetencyVector> datasource;
        private Candidate candidate;
        private RadialGradientBrush selectedColorLGB;
        private RadialGradientBrush colorLGB;
        private List<ScoreRangeChartValues> scores;
        MainPage mainPage;
        /// <summary>
        /// A <see cref="CloningAdvancedSettings"/> that holds the clone setting values.
        /// </summary>
        public CloningAdvancedSettings cloneSettings;

        #endregion Private Variables

        #region Constructor, Properties                                         
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>                    
        public RightPane()
        {
            // Required to initialize variables
            InitializeComponent();
            competencyVector = new CompetencyVectorWindow();
            vr = new ViewResume();
            cloneSettings = new CloningAdvancedSettings();
            histogramChart = new ScoreHistogramChart();
            pieChart = new ScorePieChart();
            reportWindow = new ReportWindow();

          
            //emailRecruiter = new EmailRecruiter();

        }
        /// <summary>
        /// This property is used to get/set the technical skills. Technical skills
        /// details are displayed in Skills grid.
        /// </summary>
        public IEnumerable<CompetencyVector> DataSource
        {
            get
            {
                return datasource as IEnumerable<CompetencyVector>;
            }
            set
            {
                datasource = value;
                DataGrid oGrid = RightPane_skillsDataGrid;
                if (oGrid != null)
                    oGrid.ItemsSource = datasource;
            }
        }

        /// <summary>
        /// This property stores the candidate details.
        /// </summary>
        public Candidate CandidateDetails
        {
            get
            {
                return candidate;
            }
            set
            {
                candidate = value;
            }
        }
        #endregion Constructor, Properties

        #region Event Handlers                                                  
        /// <summary>
        /// Clicking on twitter link will navigate to http://twitter.com
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPane_twitterImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://twitter.com/divihnpeterpark", UriKind.Absolute), "_blank");

        }
        private void RightPane_pickedCandidatesListImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
          /*  PickedCandidatesList pcl = new PickedCandidatesList();
            pcl.DisplayCandidates();
            pcl.Show();*/

            HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "Scheduler/ScheduleCandidate.aspx?m=2&s=1&parentpage=TS_HOME&positionprofileid=" + mainPage.PositionProfileId, UriKind.Absolute), "_self");

        }
        private void RightPanel_pickButton_Click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://twitter.com/divihnpeterpark", UriKind.Absolute), "_blank");

        }
        private void RightPane_createTestImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if(mainPage==null)
                mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            if (mainPage.PositionProfileId != 0)
                HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "TestMaker/CreateAutomaticTest.aspx?m=1&s=0&positionprofileid=" + mainPage.PositionProfileId + "&parentpage=TS_HOME", UriKind.Absolute), "_self");
            else if(mainPage.candidateIds!=string.Empty)
            {

                TalentScoutServiceClient webservice = null;
                try
                {

                    AddServiceReference.BindServiceReference(ref webservice);
                    webservice.PickCandidatesWithoutPositionProfileCompleted -= new EventHandler<PickCandidatesWithoutPositionProfileCompletedEventArgs>(webservice_PickCandidatesWithoutPositionProfileCompleted); webservice.PickCandidatesWithoutPositionProfileCompleted += new EventHandler<PickCandidatesWithoutPositionProfileCompletedEventArgs>(webservice_PickCandidatesWithoutPositionProfileCompleted);
                    SearchBarLayoutControl searchBar = (SearchBarLayoutControl)mainPage.MainPage_OuterGrid.FindName                 ("MainPage_searchBarLayoutControl");

                    string keyword = searchBar.SearchBarLayoutControl_SearchTextBox.Text.ToString();

                    webservice.PickCandidatesWithoutPositionProfileAsync(mainPage.candidateIds.Substring(0, mainPage.candidateIds.Length - 1), keyword, mainPage.LoginUserId);
                    
                }
                catch (Exception exp)
                {
                    Utility.WriteExceptionLog(exp);
                }
                finally
                {
                    if (webservice != null) webservice = null;
                }

            }

        }

        void webservice_PickCandidatesWithoutPositionProfileCompleted(object sender, PickCandidatesWithoutPositionProfileCompletedEventArgs e)
        {
            try
            {
                if (e.Result != null && e.Result.ToString() != string.Empty)
                {
                    mainPage.candidateIds = string.Empty;
                    HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "TestMaker/CreateAutomaticTest.aspx?m=1&s=0&pp_session_key=" + e.Result.ToString().Trim() + "&parentpage=TS_HOME", UriKind.Absolute), "_self");
                }
            }
            catch { }
        }
        private void RightPane_positionProfileFreesearchImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            PositionProfileOptions ppo = new PositionProfileOptions();

            ppo.Show();

        }

        private void RightPane_positionProfileImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (mainPage == null)
                mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            if (mainPage.PositionProfileId != 0)
                HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "TestMaker/SearchTest.aspx?m=1&s=2&positionprofileid=" + mainPage.PositionProfileId + " &parentpage=TS_HOME", UriKind.Absolute), "_self");
        }
        AddNoteWindow addNoteWindow = new AddNoteWindow();
        private void RightPane_addNoteImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            addNoteWindow.Title = "Add Notes";
            addNoteWindow.AddNoteWindow_errorMessageLabel.Text = "";
            addNoteWindow.AddNoteWindow_notesTextBox.Text = "";
            addNoteWindow.OKButton.Focus();
            addNoteWindow.SetCandidateDetails(mainPage.SelectedCandidate.CandidateId);
            addNoteWindow.Show();
        }

        private void RightPane_emailHyperLink_Click(object sender, RoutedEventArgs e)
        {
            string url = "mailto:" + ((Resume)((HyperlinkButton)sender).DataContext).ContactInformation.EmailAddress;
            HtmlPage.Window.Navigate(new Uri(url));
        }
		private void RightPane_nameHyperLink_Click(object sender, RoutedEventArgs e)
        {
            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            int candidateId= mainPage.SelectedCandidate.CandidateId;
            HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid=" + candidateId, UriKind.Absolute), "_blank");
        }
		
        private void RightPane_viewNoteImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
           ViewNotesWindow vn = new ViewNotesWindow();
           mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
           vn.GetCandidateDetails(mainPage.SelectedCandidate.CandidateId,mainPage.SelectedCandidate.FirstName);
           vn.Show();
        }
        private void RightPane_searchTestImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (mainPage == null)
                mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            if (mainPage.PositionProfileId != 0)
                HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "TestMaker/SearchTest.aspx?m=1&s=2&positionprofileid=" + mainPage.PositionProfileId + "&parentpage=TS_HOME", UriKind.Absolute), "_self");
        }
        
        /// <summary>
        /// Clicking on linked-in will navigate to http://linkedin.com
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPane_linkedinImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://www.linkedin.com", UriKind.Absolute), "_blank");
        }

        /// <summary>
        /// Clicking on facebook will navigate to http://facebook.com
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPane_facebook_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://www.facebook.com", UriKind.Absolute), "_blank");
        }

        /// <summary>
        /// This handler displays the ViewResume popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPane_ViewResumeImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (candidate == null)
                    return;
                vr.Title = "View Resume";
                if (App.Current.Host.Content.ActualHeight > 550)
                    vr.Height = 550;
                else
                    vr.Height = App.Current.Host.Content.ActualHeight - 25;

                vr.ViewResume_verticalScrollBar.ScrollToTop();
                vr.Show();
                vr.DisplayResumeText(CandidateDetails.CandidateId,CandidateDetails.CandidateResumeId);
            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
        }

        private void RightPane_DownloadResumeImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TalentScoutServiceClient webservice = null;
            try
            {
                if (candidate == null)
                    return;
                AddServiceReference.BindServiceReference(ref webservice);
                webservice.GetResumeByCandidateIdCompleted -= new EventHandler<GetResumeByCandidateIdCompletedEventArgs>(webservice_GetResumeByCandidateIdCompleted);
                webservice.GetResumeByCandidateIdCompleted += new EventHandler<GetResumeByCandidateIdCompletedEventArgs>(webservice_GetResumeByCandidateIdCompleted);
                webservice.GetResumeByCandidateIdAsync(CandidateDetails.CandidateId,CandidateDetails.CandidateResumeId);
            }
            catch (Exception exp)
            {
                Utility.WriteExceptionLog(exp);
            }
            finally
            {
                if (webservice != null) webservice = null;
            }
        }
        private void RightPane_pickCandidatesImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TalentScoutServiceClient webservice = null;
            try
            {
                if (candidate == null)
                    return;
                mainPage.PickCandidates();
            }
            catch (Exception exp)
            {
                Utility.WriteExceptionLog(exp);
            }
            finally
            {
                if (webservice != null) webservice = null;
            }
        }

        private void RightPanel_CandidatesPick_buttonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            TalentScoutServiceClient webservice = null;
            try
            {
                if ((candidate == null)||( candidate.CandidateId==0))
                    return;
                mainPage.PickCandidates();
            }
            catch (Exception exp)
            {
                Utility.WriteExceptionLog(exp);
            }
            finally
            {
                if (webservice != null) webservice = null;
            }
        }
       
        /// <summary>
        /// This handler displays the ProfileImage popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPane_profileImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (candidate == null)
                    return;
                competencyVector.Title = CandidateDetails.FirstName + " Profile";

                // Calculate the height of popup according to application height. 
                if (App.Current.Host.Content.ActualHeight > 650)
                    competencyVector.Height = 650;
                else
                    competencyVector.Height = App.Current.Host.Content.ActualHeight - 25;
                competencyVector.CompetencyVectorWindow_verticalScrollViewer.ScrollToTop();
                competencyVector.Show();
                competencyVector.DisplayCompetencyVectors(CandidateDetails.CandidateId,CandidateDetails.CandidateResumeId);
            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
        }


        private void HyperlinkButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //if (mainPage.candidateIds == "")
            //    return;
            PickedCandidatesList pcl = new PickedCandidatesList();
            pcl.DisplayCandidates();
            pcl.Show();
        }
        /// <summary>
        /// This handler displays the clone advanced settings popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPanel_cloneAdvancedSettingsHyperlinkButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (candidate == null)
                    return;
                cloneSettings.Title = "Cloning Advanced Settings";
                // Calculate the height of popup according to application height. 
                if (App.Current.Host.Content.ActualHeight > 700)
                    cloneSettings.Height = 700;
                else
                    cloneSettings.Height = App.Current.Host.Content.ActualHeight - 25;
                cloneSettings.Show();
            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
        }
        /// <summary>
        /// This handler displays the clone advanced settings popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPanel_cloneButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                mainPage.DisplayClonedCandidates(CandidateDetails.CandidateId,CandidateDetails.CandidateResumeId);
            }
            catch (Exception ex)
            { 
                Utility.WriteExceptionLog(ex);
            }
        }
        /// <summary>
        /// This handler displays the Report popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPane_reportButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (candidate == null)
                    return;
                // Calculate the height of popup according to application height. 
                if (App.Current.Host.Content.ActualHeight > 630)
                    reportWindow.Height = 630;
                else
                    reportWindow.Height = App.Current.Host.Content.ActualHeight - 25;

                Chart histogramChart = GetChart(RightPane_scoreExpander, "RightPane_scoreChart");

                Chart pieChart = GetChart(RightPane_scoreExpander, "RightPane_searchSummaryChart");

                //pieChart.SetChartSource(((PieSeries)chart.Series[0]).ItemsSource as IEnumerable<SearchTerms_ScorePercentage>, CandidateDetails.FirstName);
                reportWindow.SetScoreSummary(((ColumnSeries)histogramChart.Series[0]).ItemsSource as List<ScoreRangeChartValues>, ((PieSeries)pieChart.Series[0]).ItemsSource as IEnumerable<SearchTerms_ScorePercentage>);
                reportWindow.GenerateReportDetails();
                reportWindow.LayoutRoot.ScrollToTop();
                reportWindow.Show();
            }
            catch (Exception ex) 
            {
                Utility.WriteExceptionLog(ex);
            }
        }

        /// <summary>
        /// This handler displays the ScoreChart expand view popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPane_scoreChart_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (candidate == null || mainPage.IsCloningApplied)
                return;

            Chart chart = GetChart(RightPane_scoreExpander, "RightPane_scoreChart");
            if (chart != null)
            {
                histogramChart.DataContext = ((ColumnSeries)chart.Series[0]).ItemsSource;
                histogramChart.SetChartSource(CandidateDetails.FirstName);
            }
            histogramChart.Show();
        }

        /// <summary>
        /// This handler displays the SearchSummaryChart expand view popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RightPane_searchSummaryChart_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (candidate == null || mainPage.IsCloningApplied)
                return;
            pieChart.Show();
            Chart chart = GetChart(RightPane_scoreExpander, "RightPane_searchSummaryChart");
            if (chart != null)
            {
                pieChart.SetChartSource(((PieSeries)chart.Series[0]).ItemsSource as IEnumerable<SearchTerms_ScorePercentage>, CandidateDetails.FirstName);
            }
        }
//        private void RightPane_candidateActivity_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
//        {
//            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
//            int candidateId= mainPage.SelectedCandidate.CandidateId;
//
//            HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "ReportCenter/CandidateDashboard.aspx?m=0&s=3&parentpage=MENU&candidateid=" + candidateId, UriKind.Absolute), "_self");
//        }
        #endregion Event Handlers

        #region Public Methods                                                  

        /// <summary>
        /// This method reset the adavanced settings
        /// </summary>
        public void ResetAdvancedSettings()
        {
            cloneSettings = null;
            cloneSettings = new CloningAdvancedSettings();
        }

        /// <summary>
        /// Display the chart showing selected candidate's score position 
        /// amongst the scores of other matching candidates.
        /// </summary>
        public void SetScoreChartData()
        {
            Chart chart = GetChart(RightPane_scoreExpander, "RightPane_scoreChart");
            if (chart != null)
            {

                CreateGradientBrush();
                mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;

                List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> candidates =
                    mainPage.scoreDetails.CandidateDetails.ToList();
                var xMax = (from item in candidates
                            select item.SCORE).Max();
                var xInterval = 0;
                xMax = Utility.RoundAxisMaxValue(Convert.ToInt32(xMax));

                xInterval = Convert.ToInt32((xMax) / 5);
                double candidateScore = 0;
                if (mainPage.SelectedCandidate != null)
                {
                    if (mainPage.IsResume)
                    {
                        candidateScore = mainPage.SelectedCandidate.ResumeScore;
                    }
                    else
                        candidateScore = mainPage.selectedCandidate.AssessmentScore;
                }
                scores = new List<ScoreRangeChartValues>();
                ScoreRangeChartValues score;
                for (int index = 0; index < 5; index++)
                {
                    score = new ScoreRangeChartValues();
                    score.SCORE = Convert.ToInt32((xInterval * (index + 1)));
                    score.NO_OF_CANDIDATES = (from item in candidates
                                              where item.SCORE >= (xInterval * index) && item.SCORE < score.SCORE
                                              select item.SCORE).Count();

                    //score.OPACITY = Convert.ToDecimal((1-0.1) * index);
                    score.MARGIN = new Thickness(-8, 0, 5, 0);
                    score.LABEL_VISIBILITY = (score.NO_OF_CANDIDATES == 0) ? "Collapsed" : "Visible";
                    if (candidateScore != 0)
                    {
                        if (candidateScore <= score.SCORE &&
                            candidateScore > score.SCORE - 10)
                            score.GRADIENT_COLOR = Utility.CreateSelectedGradientBrush();
                        else
                            score.GRADIENT_COLOR = Utility.CreateGradientBrush();
                    }
                    scores.Add(score);
                }
                ((ColumnSeries)chart.Series[0]).ItemsSource = scores;

                LinearAxis axis = chart.Axes[1] as LinearAxis;
                axis.Maximum = Convert.ToDouble(xMax);
                axis.Minimum = 0;
                axis.Interval = xInterval;

                var yMax = (from item in scores select item.NO_OF_CANDIDATES).Max();
                var yInterval = 0;

                yMax = Utility.RoundAxisMaxValue(Convert.ToInt32(yMax));

                yInterval = Convert.ToInt32((yMax) / 5);
                LinearAxis yAxis = chart.Axes[0] as LinearAxis;
                yAxis.Maximum = Convert.ToDouble(yMax);
                yAxis.Minimum = 0;
                yAxis.Interval = yInterval;

            }
        }
        /// <summary>
        /// Display the selected candidate score in different color.
        /// </summary>
        public void SelectCandidate_In_ScoreChart()
        {
            Chart chart = GetChart(RightPane_scoreExpander, "RightPane_scoreChart");
            if (chart != null)
            {
                if (scores != null)
                {
                    ScoreRangeChartValues score; double candidateScore = 0;
                    if (CandidateDetails != null)
                    {
                        if (mainPage.IsResume)
                        {
                            candidateScore = CandidateDetails.ResumeScore;
                        }
                        else
                            candidateScore = CandidateDetails.AssessmentScore;
                    }

                    for (int index = 0; index < scores.Count; index++)
                    {
                        score = scores[index];
                        score.MARGIN = new Thickness(-8, 0, 5, 0);
                        //score.OPACITY = Convert.ToDecimal((1-0.1) * index);
                        if (candidateScore <= score.SCORE && candidateScore > score.SCORE - scores[0].SCORE)
                            score.GRADIENT_COLOR = selectedColorLGB;
                        else
                            score.GRADIENT_COLOR = colorLGB;
                    }
                    ((ColumnSeries)chart.Series[0]).ItemsSource = null;

                    ((ColumnSeries)chart.Series[0]).ItemsSource = scores;
                }
                else
                {
                    SetScoreChartData();
                }
            }
        }
        /// <summary>
        /// Create gradient brush styles for selected and 
        /// default color for the column chart datapoint.
        /// </summary>
        private void CreateGradientBrush()
        {
            selectedColorLGB = Utility.CreateSelectedGradientBrush();
            colorLGB = Utility.CreateGradientBrush();
        }
        /// <summary>
        /// Set the score distribution data for piechart.
        /// </summary>
        /// <param name="searchTerms_ScoreDistribution"></param>
        /// <param name="selectedCandidateId"></param>
        public void SetPieChartData(List<SearchTerms_ScorePercentage> searchTerms_ScoreDistribution, int selectedCandidateId)
        {
            Chart chart = GetChart(RightPane_scoreExpander, "RightPane_searchSummaryChart");
            if (searchTerms_ScoreDistribution != null)
            {
                if (chart != null)
                {
                    var selectedCandidate = (from item in searchTerms_ScoreDistribution
                                             where item.CANDIDATE_ID == selectedCandidateId
                                             select item);

                    ((PieSeries)chart.Series[0]).ItemsSource = selectedCandidate;
                }
            }
            else
            {
                ((PieSeries)chart.Series[0]).ItemsSource = null;
            }
        }
        /// <summary>
        /// Get the child element of parent in visual tree structure.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        private T GetChild<T>(DependencyObject obj) where T : DependencyObject
        {
            DependencyObject child = null;
            for (Int32 i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child.GetType() == typeof(T))
                {
                    break;
                }
                else if (child != null)
                {
                    child = GetChild<T>(child);
                    if (child != null && child.GetType() == typeof(T))
                    {
                        break;
                    }
                }
            }
            return child as T;
        }
        /// <summary>
        /// Get the child chart element inside expander control.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private Chart GetChart(DependencyObject obj, string id)
        {
            DependencyObject child = null;
            for (Int32 i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child.GetType() == typeof(Chart))
                {
                    Chart chart = child as Chart;
                    if (chart.Name == id)
                        break;
                }
                else if (child != null)
                {
                    child = GetChart(child, id);
                    if (child != null && child.GetType() == typeof(Chart))
                    {
                        break;
                    }
                }
            }

            return child as Chart;
        }
        /// <summary>
        /// Get the child textblock element inside expander control.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private TextBlock GetTextBlock(DependencyObject obj, string id)
        {
            DependencyObject child = null;
            for (Int32 i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child.GetType() == typeof(TextBlock) && ((TextBlock)child).Name == id)
                {
                    break;
                }
                else if (child != null)
                {
                    child = GetTextBlock(child, id);
                    if (child != null && child.GetType() == typeof(TextBlock))
                    {
                        break;
                    }
                }
            }

            return child as TextBlock;
        }
        /// <summary>
        /// Display the search summary details.
        /// </summary>
        /// <param name="searchSummary"></param>
        public void SetSearchSummary(SearchSummary searchSummary)
        {
            if (searchSummary == null)
            {
                ClearSearchSummary();
                return;
            }

            TextBlock txtBlock = GetTextBlock(RightPane_searchSummaryExpander, "RightPane_candidatesSearchedTextBlock") as TextBlock;
            txtBlock.Text = searchSummary.CandidatesSearched.ToString();

            TextBlock matchingCandidates = GetTextBlock(RightPane_searchSummaryExpander, "RightPane_matchingCandidatesTextBlock") as TextBlock;
            matchingCandidates.Text = searchSummary.MatchingCandidates.ToString();

            txtBlock = GetTextBlock(RightPane_searchSummaryExpander, "RightPane_sourcesSearchedTextBlock") as TextBlock;
            txtBlock.Text = searchSummary.SourcesSearched.ToString();
            LoadSearchSummaryChartData(searchSummary.SearchTerms);

        }
        /// <summary>
        /// Clear the search summary details.
        /// </summary>
        private void ClearSearchSummary()
        {
            TextBlock txtBlock = GetTextBlock(RightPane_searchSummaryExpander, "RightPane_candidatesSearchedTextBlock") as TextBlock;
            txtBlock.Text = "";
            TextBlock matchingCandidates = GetTextBlock(RightPane_searchSummaryExpander, "RightPane_matchingCandidatesTextBlock") as TextBlock;
            matchingCandidates.Text = "";
            Chart chart = GetChart(RightPane_searchSummaryExpander, "RightPane_summaryChart");
            TextBlock sourcesSearchedLabel = GetTextBlock(RightPane_searchSummaryExpander, "RightPane_sourcesSearchedTextBlock") as TextBlock;
            sourcesSearchedLabel.Text = "";
            if (chart != null)
            {
                ((ColumnSeries)chart.Series[0]).ItemsSource = null;
            }
        }
        /// <summary>
        /// Clear the score chart.
        /// </summary>
        public void ClearScoreChart()
        {
            Chart chart = GetChart(RightPane_scoreExpander, "RightPane_scoreChart");
            if (chart != null)
            {
                ((ColumnSeries)chart.Series[0]).ItemsSource = null;
            }
        }
        /// <summary>
        /// Display the search summary chart data.
        /// </summary>
        /// <param name="searchTerms"></param>
        private void LoadSearchSummaryChartData(List<SearchTerm> searchTerms)
        {
            Chart chart = GetChart(RightPane_searchSummaryExpander, "RightPane_summaryChart");
            if (chart != null && searchTerms != null && searchTerms.Count != 0)
            {
                ((ColumnSeries)chart.Series[0]).ItemsSource = searchTerms;
                var maximum = (from item in searchTerms select item.NoOfCandidatesMatched).Max();
                maximum = Utility.RoundAxisMaxValue(maximum);
                LinearAxis axis = chart.Axes[0] as LinearAxis;
                axis.Maximum = maximum;
                axis.Minimum = 0;
                axis.Interval = (maximum - axis.Minimum) / 5;
            }
        }

        // TODO : Will do later
        //private void RightPane_mailImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    if (candidate == null)
        //        return;
        //    emailRecruiter.Show();
        //}
        #endregion

        #region Webservice Completed Methods                                   

        private void webservice_GetResumeByCandidateIdCompleted(object sender, GetResumeByCandidateIdCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                    throw new Exception(e.Error.Message, e.Error);
                if (e.Result.Count <= 0)
                    return;
                Uri address = new Uri(Application.Current.Host.Source,
                       "../DownloadResume.aspx?resumeid=" + CandidateDetails.CandidateId);
                HtmlPage.Window.Navigate(new Uri(address.AbsoluteUri, UriKind.Absolute), "_self");
            }
            catch (Exception exp)
            {
                Utility.WriteExceptionLog(exp);
            }
        }

        #endregion Webservice Completed Methods

       
    }
}