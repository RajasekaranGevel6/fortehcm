﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [CompetencyVectorWindow.xaml.cs]

#endregion

#region Namespaces                                                              
using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.Generic;

using Forte.HCM.TalentScoutSL.Utils;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class displays the competency vector skills 
    /// </summary>
    public partial class CompetencyVectorWindow : ChildWindow
    {

        #region Private Members
        private int candResumeId;
        private int candidateId;
        private TalentScoutServiceClient webservice;
        #endregion Private Members

        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public CompetencyVectorWindow()
        {
            InitializeComponent();
            AddServiceReference.BindServiceReference(ref webservice);
            webservice.InsertProfileLogCompleted -=
                            new EventHandler<InsertProfileLogCompletedEventArgs>(webservice_InsertProfileLogCompleted);
            webservice.InsertProfileLogCompleted += new EventHandler<InsertProfileLogCompletedEventArgs>(webservice_InsertProfileLogCompleted);
            
            webservice.GetCompetencyVectorsByCandidateResumeIdCompleted -=
                            new EventHandler<GetCompetencyVectorsByCandidateResumeIdCompletedEventArgs>(webservice_GetCompetencyVectorsByCandidateResumeIdCompleted);
            webservice.GetCompetencyVectorsByCandidateResumeIdCompleted += new EventHandler<GetCompetencyVectorsByCandidateResumeIdCompletedEventArgs>(webservice_GetCompetencyVectorsByCandidateResumeIdCompleted);
            webservice.LogExceptionCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(webservice_LogExceptionCompleted);
            webservice.LogExceptionMessageCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(webservice_LogExceptionMessageCompleted);
            
        }
               
        void webservice_LogExceptionMessageCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            
        }

        void webservice_LogExceptionCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            
        }


        #region Event Handlers                                                  
        
        /// <summary>
        /// Apply the styles for Technical skills data grid cells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CompetencyVector_technicalSkillsDataGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
            SetDataGridCellStyle(CompetencyVector_technicalSkillsDataGrid, e,0);
        }
        /// <summary>
        /// Apply the styles for Vertical skills data grid cells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CompetencyVector_verticalsDataGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
            int previousRowsCount=0;
            // Calculate the rows in technical skills to apply the cell style for verticals vectors
            if(CompetencyVector_technicalSkillsDataGrid.ItemsSource!=null)
                previousRowsCount = ((List<CompetencyVector>)CompetencyVector_technicalSkillsDataGrid.ItemsSource).Count;
            SetDataGridCellStyle(CompetencyVector_verticalsDataGrid, e, previousRowsCount);
        }
        /// <summary>
        /// Apply the styles for Roles datagrid cells.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CompetencyVector_rolesDataGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
            int previousRowsCount = 0;
            // Calculate the rows in technical skills and verticals to apply the cell style for roles vectors
            if (CompetencyVector_technicalSkillsDataGrid.ItemsSource != null &&
                CompetencyVector_verticalsDataGrid.ItemsSource!=null)
                previousRowsCount = ((List<CompetencyVector>)CompetencyVector_technicalSkillsDataGrid.ItemsSource).Count +
                ((List<CompetencyVector>)CompetencyVector_verticalsDataGrid.ItemsSource).Count;
            else if (CompetencyVector_technicalSkillsDataGrid.ItemsSource != null &&
                CompetencyVector_verticalsDataGrid.ItemsSource == null)
                previousRowsCount = ((List<CompetencyVector>)CompetencyVector_technicalSkillsDataGrid.ItemsSource).Count;
            else if (CompetencyVector_technicalSkillsDataGrid.ItemsSource == null &&
                CompetencyVector_verticalsDataGrid.ItemsSource != null)
                previousRowsCount = ((List<CompetencyVector>)CompetencyVector_verticalsDataGrid.ItemsSource).Count;

            SetDataGridCellStyle(CompetencyVector_rolesDataGrid, e,previousRowsCount);
        }
        #endregion Event Handlers

        
        #region Public Methods                                                  
        /// <summary>
        /// This method calls the service request to get the competency vector by passing candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// This parameter represents the id of the candidate
        /// </param>
        public void DisplayCompetencyVectors(int pCandidateId,int pCandidateResumeId)
        {
            SetVectorWindowStatus(false);
            candResumeId = pCandidateResumeId;
            candidateId = pCandidateId;
            MainPage mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
            DisplayErrorMessage(null, "Loading...");
            
            webservice.InsertProfileLogAsync(candidateId, mainPage.LoginUserId);
            
        }
        private void SetVectorWindowStatus(bool isWindowEnable)
        {
            CompetencyVectorWindow_progressBar.Name = "Loading...";
            CompetencyVectorWindow_layoutGrid.Visibility = (isWindowEnable ? Visibility.Visible : Visibility.Collapsed);
            CompetencyVectorWindow_progressBar.IsIndeterminate = !isWindowEnable;
            CompetencyVectorWindow_progressBar.Visibility = (isWindowEnable ? Visibility.Collapsed : Visibility.Visible);
        }
        private void DisplayErrorMessage(Exception ex,string errorMessage)
        {
            CompetencyVectorWindow_headerStackPanel.Visibility = Visibility.Collapsed;
            CompetencyVectorWindow_errorDisplayTextBlock.Visibility = Visibility.Visible;
            CompetencyVectorWindow_errorDisplayTextBlock.Text = errorMessage; 
            CompetencyVectorWindow_techSkillsOuterGrid.Visibility = Visibility.Collapsed;
            CompetencyVectorWindow_verticalsOuterGrid.Visibility = Visibility.Collapsed;
            CompetencyVectorWindow_rolesOuterGrid.Visibility = Visibility.Collapsed;
            Utility.WriteExceptionLog(ex);
        }
        /// <summary>
        /// This method is called on completion of the asynchronous call 
        /// InsertProfileLog(). Using retrieved information, it checks whether the 
        /// maximum limit is exceeded or not. According to the maximum limit validation,
        /// it displays the profile image or error message.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">
        /// This parameter contains the status of subscription plan.
        /// </param>
        void webservice_InsertProfileLogCompleted(object sender, InsertProfileLogCompletedEventArgs e)
        {
            try
            {
                List<SPTSINSERT_LOGResult> featureUsage = e.Result.ToList();

                if (Convert.ToInt32(featureUsage[0].ISEXCEED) == 0)
                {

                    webservice.GetCompetencyVectorsByCandidateResumeIdAsync(candResumeId);
                }
                else
                {
                    DisplayErrorMessage(null, "You have reached the maximum limit based your subscription plan. Cannot view profiles");
                }
            }
            catch(Exception ex)
            {
                DisplayErrorMessage(ex,"Profile not found ");
            }
        }
        
        /// <summary>
        /// This method gets called on completion of the webservice request GetCompetencyVectorsByCandidateId().
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void webservice_GetCompetencyVectorsByCandidateResumeIdCompleted(object sender, TalentScoutServiceReference.GetCompetencyVectorsByCandidateResumeIdCompletedEventArgs e)
        {
            try
            {
                if (e.Result == null)
                {
                    DisplayErrorMessage(null, "Profile not found ");
                    return;
                }
                List<CandidateCompetencyVectors> vectors = e.Result.ToList();
                if (vectors == null || vectors.Count == 0)
                {
                    DisplayErrorMessage(null, "Profile not found ");
                    return;
                }
                CompetencyVectorWindow_headerStackPanel.Visibility = Visibility.Visible;
                CompetencyVectorWindow_errorDisplayTextBlock.Visibility = Visibility.Collapsed;
                List<CompetencyVector> rolesVector = new List<CompetencyVector>();
                List<CompetencyVector> technicalSkillsVector = new List<CompetencyVector>();
                List<CompetencyVector> verticalsVector = new List<CompetencyVector>();

                CompetencyVector competencyVector;
                for (int index = 0; index < vectors.Count; index++)
                {
                    competencyVector = new CompetencyVector();
                    competencyVector.Skill = vectors[index].SKILL_NAME.Trim();
                    if (vectors[index].NO_OF_YEAR.Trim() != string.Empty)
                        competencyVector.NoOfYears = vectors[index].NO_OF_YEAR.Trim();
                    if (vectors[index].RECENCY.Trim() != string.Empty)
                        competencyVector.Recency = vectors[index].RECENCY.Trim();
                    competencyVector.Certification = vectors[index].CERTIFICATION.Trim();
                    competencyVector.CertificationDetails = vectors[index].CERTIFICATION_DETAILS.Trim();
                    switch (vectors[index].VECTOR_TYPE.Trim().ToUpper())
                    {
                        case "TECHNICAL SKILLS":
                            technicalSkillsVector.Add(competencyVector);
                            break;
                        case "ROLES":
                            rolesVector.Add(competencyVector);
                            break;
                        case "VERTICALS":
                            verticalsVector.Add(competencyVector);
                            break;
                    }
                }

                if (technicalSkillsVector.Count != 0)
                {
                    CompetencyVectorWindow_techSkillsOuterGrid.Visibility = Visibility.Visible;
                    CompetencyVector_technicalSkillsDataGrid.ItemsSource = technicalSkillsVector;
                }
                else
                    CompetencyVectorWindow_techSkillsOuterGrid.Visibility = Visibility.Collapsed;
                if (verticalsVector.Count != 0)
                {
                    CompetencyVectorWindow_verticalsOuterGrid.Visibility = Visibility.Visible;
                    CompetencyVector_verticalsDataGrid.ItemsSource = verticalsVector;
                }
                else
                    CompetencyVectorWindow_verticalsOuterGrid.Visibility = Visibility.Collapsed;
                if (rolesVector.Count != 0)
                {
                    CompetencyVectorWindow_rolesOuterGrid.Visibility = Visibility.Visible;
                    CompetencyVector_rolesDataGrid.ItemsSource = rolesVector;
                }
                else
                    CompetencyVectorWindow_rolesOuterGrid.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                Utility.WriteExceptionLog(ex);
            }
            finally
            {
                SetVectorWindowStatus(true);
            }
        }
        #endregion Public Methods

        #region Private Methods                                                 
        /// <summary>
        /// This method sets the styles of cells.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="colorValue"></param>
        private void SetCellStyle(DataGridRow row, DataGridColumn column,string colorValue)
		{
			FrameworkElement fe = column.GetCellContent(row);
            FrameworkElement result = GetParent(fe, typeof(DataGridCell));
            if (result != null)
            {
                DataGridCell cell = (DataGridCell)result;
                cell.Background = new SolidColorBrush(Utility.GetColorFromHex(colorValue));
				cell.HorizontalContentAlignment=HorizontalAlignment.Center;
            }
		}
        
		private FrameworkElement GetParent(FrameworkElement child, Type targetType)
        {
            object parent = child.Parent;
            if (parent != null)
            {
                if (parent.GetType() == targetType)
                {
                    return (FrameworkElement)parent;
                }
                else
                {
                    return GetParent((FrameworkElement)parent, targetType);
                }
            }
            return null;
        }

		/// <summary>
		/// Set the background color value for the grid cells.
		/// </summary>
		/// <param name="datagrid">
        /// A <see cref="DataGrid"/> that holds the datagrid to set the cell style.
        /// </param>
		/// <param name="e"></param>
		/// <param name="previousRowsCount">
        /// This parameter is used to color the next row of the grid.
        /// </param>
		private void SetDataGridCellStyle(DataGrid datagrid, System.Windows.Controls.DataGridRowEventArgs e,int previousRowsCount)
		{
			DataGridColumn column;
			datagrid.SelectedIndex = e.Row.GetIndex();
            if ((previousRowsCount+e.Row.GetIndex()) % 2 == 0)
            {
				column = datagrid.Columns[0];
                SetCellStyle(e.Row,column,"#FFDAE6EB");
				
                column = datagrid.Columns[1];
                SetCellStyle(e.Row,column,"#FFD9C56C");
				column = datagrid.Columns[2];
                SetCellStyle(e.Row,column,"#FFA3D5E6");
				
				column = datagrid.Columns[3];
                SetCellStyle(e.Row,column,"#FFD9C56C");
            }
			else
			{
				column = datagrid.Columns[0];
                SetCellStyle(e.Row,column,"#FFBDC9CE");
				
				column = datagrid.Columns[1];
                SetCellStyle(e.Row, column,"#FFA3D5E6");
				column = datagrid.Columns[2];
                SetCellStyle(e.Row, column,"#FFD9C56C");
				
				column = datagrid.Columns[3];
                SetCellStyle(e.Row, column,"#FFA3D5E6");
			}
        }
        #endregion Private Methods

    }
}

