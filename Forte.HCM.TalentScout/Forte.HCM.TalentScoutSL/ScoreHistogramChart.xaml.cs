﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [ScoreHistogramChart.xaml.cs]

#endregion

#region Namespaces                                                              
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Collections.Generic;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using Forte.HCM.TalentScoutSL.DataObjects;
#endregion Namespaces

namespace Forte.HCM.TalentScoutSL
{
    public partial class ScoreHistogramChart : ChildWindow
    {
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public ScoreHistogramChart()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        /// <summary>
        /// Method that assigns the datasource for the histogram chart.
        /// </summary>
        /// <param name="firstName"></param>
        public void SetChartSource(string firstName)
        {
            List<ScoreRangeChartValues> scoreRange = DataContext as List<ScoreRangeChartValues>;
            if (scoreRange != null && scoreRange.Count != 0)
            {
                Thickness margin = new Thickness(-48, 0, 33, 0);
                foreach (ScoreRangeChartValues sr in scoreRange)
                {
                    sr.MARGIN = margin;
                }
                ((ColumnSeries)scoreHistogramChart.Series[0]).ItemsSource = null;
                ((ColumnSeries)scoreHistogramChart.Series[0]).ItemsSource = scoreRange;
                var maximum = (from item in scoreRange select item.NO_OF_CANDIDATES).Max();
                if (maximum < 10)
                    maximum = 10;
                else
                    maximum = ((maximum / 10) * 10) + 10;
                LinearAxis axis = scoreHistogramChart.Axes[0] as LinearAxis;
                axis.Maximum = maximum;
                axis.Minimum = 0;
                axis.Interval = (maximum) / 5;
                ScoreHistogramChart_nameTextBlock.Text = firstName + " - Score Position";

                LinearAxis xAxis = scoreHistogramChart.Axes[1] as LinearAxis;
                xAxis.Maximum = scoreRange[scoreRange.Count - 1].SCORE;
                xAxis.Minimum = 0;
                xAxis.Interval = scoreRange[0].SCORE;
            }
        }
		
    }
}

