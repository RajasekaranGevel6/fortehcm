﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Utility.cs]
// [File that holds the WCF service reference methods used across the application.]

#endregion

#region NameSpace                                                              

using System;
using System.Windows;

using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;

#endregion NameSpace

namespace Forte.HCM.TalentScoutSL.Utils
{
    public class AddServiceReference
    {

        #region Public Static Methods                                          

        public static void BindServiceReference(ref TalentScoutServiceClient webService)
        {
            if (webService != null)
                return;
            //string[] strUri = Application.Current.Host.Source.AbsoluteUri.Split('/');
            //string WcfServiceAddress = strUri[0] + "/" + strUri[1] + "/" + strUri[2] + "/" + strUri[3] +
              //  "/Forte.HCM.TalentScoutServiceReference/Forte.HCM.TalentScoutService.svc";
            Uri address = new Uri(Application.Current.Host.Source,
               App_GlobalResources.TalentScoutResource.TalentScout_ServiceReference);
            webService = new TalentScoutServiceClient(
                App_GlobalResources.TalentScoutResource.TalentScout_BasicHTTPBinding, address.AbsolutePath);
            webService.InnerChannel.OperationTimeout = TimeSpan.FromMinutes(MainPage.OperationTimeout);
                //WcfServiceAddress);
            //webService = new TalentScoutServiceClient();
        }

        #endregion Public Static Methods
    }
}
