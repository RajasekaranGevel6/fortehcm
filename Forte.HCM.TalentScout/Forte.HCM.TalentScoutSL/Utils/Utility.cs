﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [Utility.cs]
// [File that holds the common utility methods used across the application.]

#endregion

#region Namespaces                                                              
using System;
using System.Windows;
using System.Windows.Media;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using System.Text.RegularExpressions;
#endregion

namespace Forte.HCM.TalentScoutSL.Utils
{
    /// <summary>
    /// This class contains the common utility methods used across the application.
    /// </summary>
    public static class Utility
    {
        
        public static Color[] mColors = { Colors.Orange, Colors.Red, Colors.Blue, Colors.Brown, Colors.DarkGray, Colors.Purple, Colors.Green };

        /// <summary>
        /// Returns the gradient brush to apply for selected candidate in bar chart.
        /// </summary>
        /// <returns>
        /// A <see cref="RadialGradientBrush"/> that holds the gradient brush.
        /// </returns>
        public static RadialGradientBrush CreateSelectedGradientBrush()
        {
            RadialGradientBrush selectedColorLGB = new RadialGradientBrush();
            selectedColorLGB.RadiusX = 1.05;
            selectedColorLGB.RadiusY = 0.9;
            selectedColorLGB.GradientOrigin = new Point(-0.1, -0.1);
            // Create and add Gradient stops
            GradientStop sColor1 = new GradientStop();
            sColor1.Color = GetColorFromHex("#FFEF7575");
            selectedColorLGB.GradientStops.Add(sColor1);

            return selectedColorLGB;
           
        }
        /// <summary>
        /// Returns the gradient brush to apply for bar chart.
        /// </summary>
        /// <returns>
        /// A <see cref="RadialGradientBrush"/> that holds the gradient brush.
        /// </returns>
        public static RadialGradientBrush CreateGradientBrush()
        {
            RadialGradientBrush colorLGB = new RadialGradientBrush();
            colorLGB.RadiusX = 1.05;
            colorLGB.RadiusY = 0.9;
            colorLGB.GradientOrigin = new Point(-0.1, -0.1);
            // Create and add Gradient stops
            GradientStop color1 = new GradientStop();
            color1.Color = GetColorFromHex("#FFB9D6F7");
            colorLGB.GradientStops.Add(color1);

            GradientStop color2 = new GradientStop();
            color2.Color = GetColorFromHex("#FF284B70");
            color2.Offset = 1;
            colorLGB.GradientStops.Add(color2);
            return colorLGB;
        }
        /// <summary>
        /// This method converts the color string to <see cref="SolidColorBrush"/>
        /// </summary>
        /// <param name="myColor">
        /// A <see cref="string"/> that holds the hexa decimal color.
        /// </param>
        /// <returns>
        /// A <see cref="Color"/> that holds the color.
        /// </returns>
        public static Color GetColorFromHex(string myColor)
        {
            return
                Color.FromArgb(
                    Convert.ToByte(myColor.Substring(1, 2), 16),
                    Convert.ToByte(myColor.Substring(3, 2), 16),
                    Convert.ToByte(myColor.Substring(5, 2), 16),
                    Convert.ToByte(myColor.Substring(7, 2), 16)

            );
        }
        /// <summary>
        /// Method that rounds the integer value to tens.
        /// </summary>
        /// <param name="maxVal">
        /// A <see cref="int"/> that holds the maximum axis integer value.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the integer value which rounds the max axis value to tens.
        /// </returns>
        public static int RoundAxisMaxValue(int maxVal)
        {
            if (maxVal < 10)
                maxVal = 10;
            else
                maxVal = (Convert.ToInt32(maxVal / 10) * 10) + 10;
            return maxVal;
        }
        private static TalentScoutServiceClient webservice;
        public static void WriteExceptionLog(Exception ex)
        {
            if (webservice == null)
            {
                Uri address = new Uri(Application.Current.Host.Source,
                    App_GlobalResources.TalentScoutResource.TalentScout_ServiceReference);
                webservice = new TalentScoutServiceClient(
                    App_GlobalResources.TalentScoutResource.TalentScout_BasicHTTPBinding, address.AbsoluteUri);
                webservice.LogExceptionCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(webservice_LogExceptionCompleted);
            }
            if (ex != null)
            {
                string innerException = (ex.InnerException != null ? ex.InnerException.ToString() : "");
                webservice.LogExceptionAsync(ex.Message, innerException, ex.StackTrace);
            }
        }
        /// <summary>
        /// This method triggers on completion of the LogException() method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void webservice_LogExceptionCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            // Do nothing.
        }
        public static void LogException(Exception exp)
        {
            AddServiceReference.BindServiceReference(ref webservice);
            webservice.LogExceptionMessageAsync(exp.Message);
        }
        public static string ReplaceActual(string source, string @searchString,int index)
        {
            return Regex.Replace(source, @"\b" + searchString + @"\b", delegate(Match match)
            {
                return @"<B><F Font='" + index.ToString() + "'>" + match.Value + @"</F></B>";
            }, RegexOptions.IgnoreCase);
        }
    }
}
