﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using Forte.HCM.TalentScoutSL.TalentScoutServiceReference;
using Forte.HCM.TalentScoutSL.Utils;

namespace Forte.HCM.TalentScoutSL
{
    public partial class PositionProfileOptions : ChildWindow
    {
        MainPage mainPage;

        private SearchBarLayoutControl searchBar;
        public PositionProfileOptions()
        {
            InitializeComponent();

            if (mainPage == null)
                mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;

        //  if(mainPage.candidateIds==string.Empty)
              
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

             if (mainPage == null)
                 mainPage = (Application.Current.RootVisual as Grid).Children[0] as MainPage;
             /*if (mainPage.PositionProfileId != 0)
                 HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "PositionProfile/SearchPositionProfile.aspx?m=1&s=1&parentpage=WF_LAND", UriKind.Absolute), "_self");*/

            if (mainPage.candidateIds != string.Empty)
            {
                TalentScoutServiceClient webservice = null;
                try
                {

                    AddServiceReference.BindServiceReference(ref webservice);
                    webservice.PickCandidatesWithoutPositionProfileCompleted -= new EventHandler<PickCandidatesWithoutPositionProfileCompletedEventArgs>(webservice_PickCandidatesWithoutPositionProfileCompleted); webservice.PickCandidatesWithoutPositionProfileCompleted += new EventHandler<PickCandidatesWithoutPositionProfileCompletedEventArgs>(webservice_PickCandidatesWithoutPositionProfileCompleted);
                    SearchBarLayoutControl searchBar = (SearchBarLayoutControl)mainPage.MainPage_OuterGrid.FindName("MainPage_searchBarLayoutControl");

                    string keyword = searchBar.SearchBarLayoutControl_SearchTextBox.Text.ToString();

                    webservice.PickCandidatesWithoutPositionProfileAsync(mainPage.candidateIds.Substring(0, mainPage.candidateIds.Length - 1), keyword, mainPage.LoginUserId);

                }
                catch (Exception exp)
                {
                    Utility.WriteExceptionLog(exp);
                }
                finally
                {
                    if (webservice != null) webservice = null;
                }
            }
        }
        void webservice_PickCandidatesWithoutPositionProfileCompleted(object sender, PickCandidatesWithoutPositionProfileCompletedEventArgs e)
        {
            searchBar = (SearchBarLayoutControl)mainPage.MainPage_OuterGrid.FindName
                 ("MainPage_searchBarLayoutControl");

            try
            {
                if (e.Result != null && e.Result.ToString() != string.Empty)
                {
                    mainPage.candidateIds = string.Empty;

                    if (AssociateExistingPositionProfileRadioButton.IsChecked == true)
                    {
                        HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "PositionProfile/SearchPositionProfile.aspx?m=1&s=1&pp_session_key=" + e.Result.ToString().Trim() + "&parentpage=TS_HOME&type=TS_PICK", UriKind.Absolute), "_self");
                    }
                    else
                    {
                        HtmlPage.Window.Navigate(new Uri(mainPage.UIUrl + "PositionProfile/PositionProfileBasicInfo.aspx?m=1&s=0&pp_session_key=" + e.Result.ToString().Trim() + "&parentpage=TS_HOME", UriKind.Absolute), "_self");
                    }
                }
            }
            catch { }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

