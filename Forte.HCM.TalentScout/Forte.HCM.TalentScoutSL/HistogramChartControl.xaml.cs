﻿#region Header                                                                  

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// [HistogramChartControl.xaml.cs]
// File that represents the class to display the histogram chart.

#endregion

#region Namespaces                                                              
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Controls.DataVisualization.Charting;

using Forte.HCM.TalentScoutSL.DataObjects;
#endregion

namespace Forte.HCM.TalentScoutSL
{
    /// <summary>
    /// This class creates the histogram chart control by 
    /// using SCORES and NO_OF_CANDIDATES
    /// </summary>
    public partial class HistogramChartControl : UserControl
    {
        private List<ScoreRangeChartValues> scoreData;
        private int xInterval;
        private int xMaximum;
        private int yInterval;
        private int yMaximum;
        private string xAxisTitle;
        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public HistogramChartControl()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Property that will get or set the ScoreData.
        /// </summary>
        public List<ScoreRangeChartValues> ScoreData
        {
            get{return scoreData;}
            set{scoreData=value;}
        }
        /// <summary>
        /// Property that will get or set the XInterval.
        /// </summary>
        public int XInterval
        {
            get { return xInterval; }
            set { xInterval = value; }
        }
        /// <summary>
        /// Property that will get or set the XMaximum.
        /// </summary>
        public int XMaximum
        {
            get { return xMaximum; }
            set { xMaximum = value; }
        }
        /// <summary>
        /// Property that will get or set the YInterval.
        /// </summary>
        public int YInterval
        {
            get { return yInterval; }
            set { yInterval = value; }
        }
        /// <summary>
        /// Property that will get or set the YMaximum.
        /// </summary>
        public int YMaximum
        {
            get { return yMaximum; }
            set { yMaximum = value; }
        }
        /// <summary>
        /// Property that will get or set the XAxisTitle.
        /// </summary>
        public string XAxisTitle
        {
            get { return xAxisTitle; }
            set { xAxisTitle = value; }
        }
        /// <summary>
        /// Creates the histogram chart control with candidate scores.
        /// </summary>
        public void CreateChart()
        {
            LinearAxis xAxis = new LinearAxis();
            xAxis.Orientation = AxisOrientation.X;
            xAxis.Title = XAxisTitle;
            xAxis.Maximum = XMaximum;
            xAxis.Minimum = 0;
            xAxis.Interval = XInterval;
            HistogramChart.Axes.Add(xAxis);
           

            LinearAxis yAxis = new LinearAxis();
            yAxis.Orientation = AxisOrientation.Y;
            yAxis.Maximum = YMaximum;
            yAxis.Minimum = 0;
            yAxis.Interval = YInterval;
            HistogramChart.Axes.Add(yAxis);

            ColumnSeries cs = new ColumnSeries();
            Binding binding = new Binding("SCORE");
            binding.Mode = BindingMode.OneWay;
            cs.IndependentValueBinding = binding;
            cs.DependentValueBinding = new Binding("NO_OF_CANDIDATES");
            HistogramChart.Series.Add(cs);

            Thickness margin = new Thickness(-17, 0, 12, 0);
            foreach (ScoreRangeChartValues sr in scoreData)
            {
                sr.MARGIN = margin;
            }
            ((ColumnSeries)HistogramChart.Series[0]).ItemsSource = scoreData;

            // Set the tooltip string.
            StringBuilder tooltipStr=new StringBuilder();
            tooltipStr.Append("Histogram chart showing selected candidate's score");
            tooltipStr.Append(" position amongst the scores of other matching candidates,");
            tooltipStr.Append(" along with rank for " + xAxisTitle + " search term");
            HistogramChart_tooltipTextBlock.Text = tooltipStr.ToString();
        }

    }
}
