﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace Forte.HCM.TalentScoutServices
{
    [DataContract]
    public class JobBoardSearchTerms
    {
        [DataMember]
        public string SessionToken { set; get; }
        [DataMember]
        public string Keywords { set; get; }
        [DataMember]
        public string SearchPattern { set; get; }
        [DataMember]
        public string JobCategories { set; get; }
        [DataMember]
        public string JobTitle { set; get; }
        [DataMember]
        public string Company { set; get; }
        [DataMember]
        public string ResumeTitle { set; get; }
        [DataMember]
        public string City { set; get; }
        [DataMember]
        public string ZipCode { set; get; }
        [DataMember]
        public string State { set; get; }
        [DataMember]
        public string Country { set; get; }
        [DataMember]
        public string SearchRadiusInMiles { set; get; }
        [DataMember]
        public string RelocationFilter { set; get; }
        [DataMember]
        public string FreshnessInDays { set; get; }
        [DataMember]
        public string EmploymentType { set; get; }
        [DataMember]
        public string MinimumExperience { set; get; }
        [DataMember]
        public string MaximumExperience { set; get; }
        [DataMember]
        public string MinimumTravelRequirement { set; get; }
        [DataMember]
        public string MinimumDegree { set; get; }
        [DataMember]
        public string CompensationType { set; get; }
        [DataMember]
        public string MinimumSalary { set; get; }
        [DataMember]
        public string MaximumSalary { set; get; }
        [DataMember]
        public string ExcludeResumesWithNoSalary { set; get; }
        [DataMember]
        public string LanguagesSpoken { set; get; }
        [DataMember]
        public string CurrentlyEmployed { set; get; }
        [DataMember]
        public string ManagementExperience { set; get; }
        [DataMember]
        public string MinimumEmployeesManaged { set; get; }
        [DataMember]
        public string MaximumCommute { set; get; }
        [DataMember]
        public string SecurityClearance { set; get; }
        [DataMember]
        public string WorkStatus { set; get; }
        [DataMember]
        public string ExcludeIVRResumes { set; get; }
        [DataMember]
        public string OrderBy { set; get; }
        [DataMember]
        public string PageNumber { set; get; }
        [DataMember]
        public string RowsPerPage { set; get; }
        [DataMember]
        public string CustAcctCode { set; get; }
        [DataMember]
        public string CustomXML { set; get; }
        [DataMember]
        public string MilitaryExperience { set; get; }
        [DataMember]
        public string School { set; get; }
        [DataMember]
        public LocationDetails Location { set; get; }
        [DataMember]
        public LocationDetails FirstAlternateLocation { set; get; }
        [DataMember]
        public LocationDetails SecondAlternateLocation { set; get; }
    }

     [DataContract]
    public class LocationDetails
    {
        [DataMember]
         public string City { set; get; }
        [DataMember]
        public string State { set; get; }
        [DataMember]
        public string ZipCode { set; get; }
     }
    

}