﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.Linq;

namespace Forte.HCM.TalentScoutServices
{
    // NOTE: If you change the interface name "ITalentScoutService" here, you must also update the reference to "ITalentScoutService" in Web.config.
    [ServiceContract]
    public interface ITalentScoutService
    {
        [OperationContract]
        bool CandidateFileExist(int candidateID);
        [OperationContract]
        string GetCandidatePhotoPath();

        [OperationContract]
        List<CandidateTechnicalSkills> GetTechnicalSkillsByCandidateId(int candidateId);

        [OperationContract]
        List<SPTSGET_RESUME_SOURCE_BYCANDIDATEIDResult> GetResumeByCandidateId(int candidateId,int candResumeId);

        [OperationContract]
        List<CandidateCompetencyVectors> GetCompetencyVectorsByCandidateResumeId(int candResumeId);

        [OperationContract]
        List<SPGET_ADMIN_SETTINGSResult> GetAdminSettings();

        [OperationContract]
        ScoreDetails GetScoreDetails(int positionProfileId, string searchTerm, int pageNum, int pageSize, int tenantId, int? userID);

        [OperationContract]
        ScoreDetails GetResumeScoreDetails(int positionProfileId, string searchTerm, int pageNum, int pageSize, int tenantId, int? userID);

        [OperationContract]
        ScoreDetails GetCandidatesByKeywords(int positionProfileId, string searchTerm, int pageNum, int pageSize, int tenantId, int? userID);

        [OperationContract]
        ReportDetails GetReportDetails(string searchTerm);

        [OperationContract]
        ReportDetails GetResumeReportDetails(int tenantId,string SearchTerm);

        [OperationContract]
        List<SPTSINSERT_LOGResult> InsertSearchLog(string searchTerms, string positionProfileName, int userId, int totalCandidatesSearched);

        [OperationContract]
        List<SPTSINSERT_LOGResult> InsertProfileLog(int candidateId, int userId);

        [OperationContract]
        List<SPTSINSERT_LOGResult> InsertCloneLog(string searchTerms, int candidateId, int userId);

        [OperationContract]
        List<SPTSINSERT_LOGResult> CheckSearchUsage(int userId);
        //[OperationContract]
        //USER_ROLES_RESULT CheckUserAuthentication(string UserName, string Password);

        [OperationContract]
        List<SPGET_CHECK_AUTHENTICATEUSERResult> CheckUserAuthentication(string UserName, string Password);

        [OperationContract]
        List<SPGET_FEATURE_ROLESResult> CheckForUserRole(int UserId, string UrlToAuthenticate);

        /// <summary>
        /// This method get the list of client request details
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="System.Int32"/> that holds the logged in userid
        /// </param>
        /// <param name="ClientRequestNumber">
        /// A <see cref="string"/> contains the client request number
        /// </param>
        /// <param name="Client">
        /// A <see cref="string"/> contains the client request id
        /// </param>
        /// <param name="POSTION_NAME">
        /// A <see cref="string"/> contains the position of the client request number
        /// </param>
        /// <param name="CREATED_DATE">
        /// A <see cref="datetime"/> contains the client request created date
        /// </param>
        /// <param name="PAGENUM">
        /// A <see cref="integer"/> contains the page number
        /// </param>
        /// <param name="PAGESIZE">
        /// A <see cref="integer"/> contains the page size
        /// </param>
        /// <param name="ORDERBY">
        /// A<see cref="string"/> contains the order by direction of the list
        /// </param>
        /// <param name="ORDERBYDIRECTION">
        /// A <see cref="char"/> contains whether ascending or descending value
        /// </param>
        /// <returns></returns>
        [OperationContract]
        List<SPGET_CLIENT_REQUEST_INFORMATION_SEARCHResult> GetClientRequestNumber(string PositionProfileName,
            string Client, object POSTION_NAME, object CREATED_DATE, string CONTACT_NAME, string LOCATION, string DEPARTMENT_NAME,
            string CREATED_BY, int? PAGENUM,
            int? PAGESIZE, string ORDERBY, char ORDERBYDIRECTION, int UserId);

        [OperationContract]
        List<SPSEARCH_CANDIDATE_ACTIVITY_LOGResult> GetViewNotesRequest(
             int candidate_id, object activity_type, int? activity_by, object activity_date_from, object activity_date_to,
             object comments, string orderby_direction, string sort_expression, int? page_num, int? page_size);

        //[OperationContract]
        //void SendMail(string toAddresss, string fromAddress, string cc);
        [OperationContract]
        void LogExceptionMessage(string message);

        [OperationContract]
        void LogException(string message, string innerException, string stackTrace);

        [OperationContract]
        List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> GetClonedCandidatesByCandidateId(int tenantId, int candidateId, int candResumeId, int pageNum, int pageSize, int? userID);

        [OperationContract]
        List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> GetAdvancedClonedCandidates(string clone_advanced_settings, int candidateId, int candResumeId, int pageNum, int pageSize, int tenantId, int? userID);
        [OperationContract]
        ScoreDetails GetResumeDetails(JobBoardSearchTerms searchTerms, string searchField, int pageNo, int pageSize, int userId, int positionProfileId, int tenantId, out string errorMessage);

        [OperationContract]
        string CheckJobBoardLoginCredentials(string loginName, string password);

        [OperationContract]
        List<SPTSINSERT_POSITION_PROFILE_CANDIDATESResult> PickCandidate(int candidateId, int positionProfileId, int pickedBy);

        [OperationContract]
        string PickCandidates(List<string> candidateIds, int positionProfileId, int pickedBy);

        [OperationContract]
        string PickCandidatesWithoutPositionProfile(string candidateIds, string keyword, int pickedBy);

        [OperationContract]
        int InsertComments(string activityType, int candidateId, System.Nullable<int> candidateLoginId, string comments, int userId, int positionProfileID);

        [OperationContract]
        int DeletePickedCandidates(int positionProfileID, int canditateID);
    }
}
