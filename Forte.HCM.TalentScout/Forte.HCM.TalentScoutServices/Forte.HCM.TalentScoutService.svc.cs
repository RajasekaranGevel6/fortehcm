﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using Forte.HCM.Trace;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;

#endregion Namespaces
namespace Forte.HCM.TalentScoutServices
{
    // NOTE: If you change the class name "TalentScoutService" here, you must also update the reference to "TalentScoutService" in Web.config.
   
    public class TalentScoutService : ITalentScoutService
    {

        public string GetCandidatePhotoPath()
        {
            return ConfigurationManager.AppSettings["CANDIDATE_PHOTO_PATH"].ToString();
        }
      
        public bool CandidateFileExist(int candidateID)
        {

            //string path = HttpContext.Current.Server.MapPath("~/") +
            //              "CandidatePhotos" +
            //              "\\" + candidateID + ".png";
            //System.Web.Hosting.HostingEnvironment.MapPath(ConfigurationManager.AppSettings["CANDIDATE_PHOTO_PATH"] +
            //candidateID + ".png");
            try
            {
                string localpath = ConfigurationManager.AppSettings["CANDIDATE_PHOTOS_FOLDER"].ToString() + candidateID.ToString() + ".png";
                if (File.Exists(localpath))
                {
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public int GetCommandTimeout()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["COMMAND_TIMEOUT"].ToString());
        }
        
        /// <summary>
        /// This method returns the technical skill by passing candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// This parameter represents the candidate id.
        /// </param>
        /// <returns></returns>
        public List<CandidateTechnicalSkills> GetTechnicalSkillsByCandidateId(int candidateId)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSGET_CANDIDATES_TECHSKILL_BYCANDIDATEID(candidateId);

            return result.ToList();

        }

        /// <summary>
        /// This method returns the candidate details by passing candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// This parameter represents the candidate id.
        /// </param>
        /// <returns></returns>
        public List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> GetClonedCandidatesByCandidateId(int tenantId, int candidateId, int candResumeId, int pageNum, int pageSize, int? userID)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSGET_CANDIDATES_CLONING(tenantId, candidateId,candResumeId, pageNum, pageSize, userID);
            return result.ToList();
        }
        /// <summary>
        /// This method returns the candidate details by passing candidate id and cloning advanced settings.
        /// </summary>
        /// <param name="candidateId">
        /// This parameter represents the candidate id and cloning advanced settings.
        /// </param>
        /// <returns></returns>
        public List<SPTSGET_CANDIDATES_BYSEARCHTERMResult> GetAdvancedClonedCandidates(string clone_advanced_settings, int candidateId,int candResumeId, int pageNum, int pageSize, int tenantId, int? userID)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSGET_CANDIDATES_ADVANCED_CLONING(tenantId, clone_advanced_settings, candidateId,candResumeId, pageNum, pageSize, userID);
            return result.ToList();
        }
        /// <summary>
        /// This method returns the resume details by passing candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// This parameter represents the candidate id.
        /// </param>
        /// <returns></returns>
        public List<SPTSGET_RESUME_SOURCE_BYCANDIDATEIDResult> GetResumeByCandidateId(int candidateId, int candResumeId)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSGET_RESUME_SOURCE_BYCANDIDATEID(candidateId, candResumeId);

            return result.ToList();

        }

        /// <summary>
        /// This method returns the competency vector details by passing candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// This parameter represents the candidate id.
        /// </param>
        /// <returns></returns>
        public List<CandidateCompetencyVectors> GetCompetencyVectorsByCandidateResumeId(int candResumeId)
        {
            try
            {
                TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
                db.CommandTimeout = GetCommandTimeout();
                var result = db.SPTSGET_COMPETENCY_VECTORS_BYCANDIDATEID(candResumeId);

                return result.ToList();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// This method returns the competency vector details by passing candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// This parameter represents the candidate id.
        /// </param>
        /// <returns></returns>
        public List<SPGET_ADMIN_SETTINGSResult> GetAdminSettings()
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPGET_ADMIN_SETTINGS();

            return result.ToList();

        }
        public string GetNextNumber(string objectType, int userId)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            string nextNumber = "";
            var result = db.SPGET_NEXT_NUMBER_OUTPUT(objectType, userId, ref nextNumber);

            return nextNumber;

        }
        public string PickCandidatesWithoutPositionProfile(string candidateIds, string keyword, int pickedBy)
        {
            string nextNumber = GetNextNumber("PPC", pickedBy);
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPINSERT_POSITION_PROFILE_CANDIDATE_TEMP(nextNumber, keyword, candidateIds, pickedBy);
            return nextNumber;
        }
        /// <summary>
        /// This method inserts the entry in search log when user searching the candidate in talentscout.
        /// </summary>
        /// /// <param name="searchTerms">
        /// This parameter represents the search terms
        /// </param>
        /// <param name="userId">
        /// This parameter represents the user id.
        /// </param>
        /// <returns>This returns 1 if entry gets added in search log, otherwise 0.</returns>
        public List<SPTSINSERT_LOGResult> InsertSearchLog(string searchTerms, string positionProfileName, int userId, int totalCandidatesSearched)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSINSERT_SEARCHLOG(searchTerms, positionProfileName, userId, totalCandidatesSearched);

            return result.ToList();

        }

        /// <summary>
        /// This method inserts the entry in clone log when user clones the candidate.
        /// </summary>
        /// <param name="searchTerms">
        /// This parameter represents the search terms
        /// </param>
        /// <param name="candidateId">
        /// This parameter represents the candidate id to clone.
        /// </param>
        /// <param name="userId">
        /// This parameter represents the user id.
        /// </param>
        /// <returns>This returns 1 if entry gets added in search log, otherwise 0.</returns>
        public List<SPTSINSERT_LOGResult> InsertCloneLog(string searchTerms, int candidateId,int userId)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSINSERT_CLONELOG(searchTerms, candidateId, userId);

            return result.ToList();

        }
        /// <summary>
        /// This method inserts the entry in profile log when user views the candidate's profile.
        /// </summary>
        /// <param name="searchTerms">
        /// This parameter represents the search terms
        /// </param>
        /// <param name="userId">
        /// This parameter represents the candidate id.
        /// </param>
        /// <returns>This returns 1 if entry gets added in search log, otherwise 0.</returns>
        public List<SPTSINSERT_LOGResult> InsertProfileLog(int candidateId, int userId)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSINSERT_PROFILELOG(candidateId, userId);

            return result.ToList();

        }

        /// <summary>
        /// This method validates the search usage in FeatureUsage table.
        /// </summary>
        /// <param name="userId">
        /// This parameter represents the login user id.
        /// </param>
        /// <returns>This returns 1 if user exceeds the search usage, otherwise 0.</returns>
        public List<SPTSINSERT_LOGResult> CheckSearchUsage(int userId)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSCHECK_SEARCHUSAGE(userId);

            return result.ToList();

        }
        public int InsertComments(string activityType, int candidateId, System.Nullable<int> candidateLoginId, string comments, int userId, int positionProfileID
            )
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            System.Nullable<int> logid = 0;
            var result = db.SPINSERT_CANDIDATE_ACTIVITY_LOG(activityType, candidateId, candidateLoginId, comments, userId, positionProfileID, ref logid);

            return Convert.ToInt32(result);

        }

        /// <summary>
        ///  /// <summary>
        /// This method returns the assessment score details by passing search term
        /// </summary>
        /// <param name="searchTerm">
        /// This parameter represents the search term.
        /// </param>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>

        public ScoreDetails GetScoreDetails(int positionProfileId, string searchTerm, int pageNum, int pageSize, int tenantId, int? userID)
        {
            try
            {
                TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
                db.CommandTimeout = GetCommandTimeout();
                IMultipleResults result = db.SPTSGET_CANDIDATES_ASSMNT_BY_SEARCHTERMS(positionProfileId, tenantId, searchTerm, pageNum, pageSize, userID);
                var scoreDistribution = result.GetResult<SearchTerms_ScorePercentage>();
                var candidates = result.GetResult<SPTSGET_CANDIDATES_BYSEARCHTERMResult>();
                var searchSummary = result.GetResult<SearchSummaryDetails>();
                var searchTerm_SearchedCandidates = result.GetResult<SearchTerms_No_Of_Candidates>();
                var technicalDetails = result.GetResult<CandidateTechnicalSkills>();
                ScoreDetails details = new ScoreDetails();
                details.SearchTerms_ScoreDistribution = scoreDistribution.ToList();
                details.CandidateDetails = candidates.ToList();
                details.SearchTerms_NoOfCandidates = searchTerm_SearchedCandidates.ToList();
                details.SearchTermSummaryDetails = searchSummary.ToArray()[0];
                details.TechnicalSkills = technicalDetails.ToList();
                return details;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// This method returns the resume score details by passing the search term.
        /// </summary>
        /// <param name="searchTerm">
        /// This parameter represents the search term.
        /// </param>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public ScoreDetails GetResumeScoreDetails(int positionProfileId, string searchTerm, int pageNum, int pageSize, int tenantId, int? userID)
        {
            try
            {
                TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
                db.CommandTimeout = GetCommandTimeout();
                IMultipleResults result = db.SPTSGET_CANDIDATES_RESUMESCORE_BY_SEARCHTERMS(positionProfileId, tenantId, searchTerm, pageNum, pageSize, userID);
                var scoreDistribution = result.GetResult<SearchTerms_ScorePercentage>();
                var candidates = result.GetResult<SPTSGET_CANDIDATES_BYSEARCHTERMResult>();
                var searchSummary = result.GetResult<SearchSummaryDetails>();
                var searchTerm_SearchedCandidates = result.GetResult<SearchTerms_No_Of_Candidates>();
                var technicalDetails = result.GetResult<CandidateTechnicalSkills>();
                ScoreDetails details = new ScoreDetails();
                details.SearchTerms_ScoreDistribution = scoreDistribution.ToList();
                details.CandidateDetails = candidates.ToList();
                details.SearchTerms_NoOfCandidates = searchTerm_SearchedCandidates.ToList();
                details.SearchTermSummaryDetails = searchSummary.ToArray()[0];
                details.TechnicalSkills = technicalDetails.ToList();
                return details;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return null;
            }
        }
        /// <summary>
        /// This method returns the resume score details by passing the search term.
        /// </summary>
        /// <param name="searchTerm">
        /// This parameter represents the search term.
        /// </param>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public ScoreDetails GetCandidatesByKeywords(int positionProfileId, string searchTerm, int pageNum, int pageSize, int tenantId, int? userID)
        {
            try
            {
                TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
                db.CommandTimeout = GetCommandTimeout();
                IMultipleResults result = db.SPTSGET_CANDIDATES_BY_KEYWORDS(positionProfileId, tenantId, searchTerm, pageNum, pageSize, userID);
                var scoreDistribution = result.GetResult<SearchTerms_ScorePercentage>();
                var candidates = result.GetResult<SPTSGET_CANDIDATES_BYSEARCHTERMResult>();
                var searchSummary = result.GetResult<SearchSummaryDetails>();
                var searchTerm_SearchedCandidates = result.GetResult<SearchTerms_No_Of_Candidates>();
                var technicalDetails = result.GetResult<CandidateTechnicalSkills>();
                ScoreDetails details = new ScoreDetails();
                details.SearchTerms_ScoreDistribution = scoreDistribution.ToList();
                details.CandidateDetails = candidates.ToList();
                details.SearchTerms_NoOfCandidates = searchTerm_SearchedCandidates.ToList();
                details.SearchTermSummaryDetails = searchSummary.ToArray()[0];
                details.TechnicalSkills = technicalDetails.ToList();
                return details;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return null;
            }
        }
        /// <summary>
        /// This method returns the resume report details by passing the search term.
        /// </summary>
        /// <param name="searchTerm">
        /// This parameter represents the search term.
        /// </param>
        /// <returns></returns>
        public ReportDetails GetResumeReportDetails(int tenantId,string SearchTerm)
        {
            ReportDetails reportDetails = null;
            try
            {
                TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
                db.CommandTimeout = GetCommandTimeout();
                IMultipleResults result = db.SPTSGET_RESUME_REPORT_DETAILS_BY_SEARCHTERMS(tenantId,SearchTerm);
                var ScorePercentage = result.GetResult<SearchTerms_ScorePercentage>();
                var MinMaxAvg = result.GetResult<SearchTerms_MinMaxReportDetails>();
                reportDetails = new ReportDetails();
                reportDetails.SearchTerms_ScoreDistribution = ScorePercentage.ToList();
                reportDetails.SearchTerms_MinMaxDetails = MinMaxAvg.ToList();
                return reportDetails;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                return null;
            }
            finally
            {
                if (reportDetails != null) reportDetails = null;
            }
        }

        /// <summary>
        /// This method returns the report details of the selected candidate.
        /// </summary>
        /// <param name="searchTerm">
        /// This parameter specifies the search terms provided by user.
        /// </param>
        /// <returns></returns>
        public ReportDetails GetReportDetails(string searchTerm)
        {
            try
            {
                TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
                db.CommandTimeout = GetCommandTimeout();
                IMultipleResults result = db.SPTSGET_REPORT_DETAILS_BY_SEARCHTERMS(searchTerm);
                var scoreDistribution = result.GetResult<SearchTerms_ScorePercentage>();
                var minMaxDetails = result.GetResult<SearchTerms_MinMaxReportDetails>();
                ReportDetails details = new ReportDetails();
                details.SearchTerms_ScoreDistribution = scoreDistribution.ToList();
                details.SearchTerms_MinMaxDetails = minMaxDetails.ToList();

                return details;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return null;
            }
        }
        /// <summary>
        /// This method returns the selected user details
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> contains the username
        /// </param>
        /// <param name="Password">
        /// A <see cref="string"/> contains the password
        /// </param>
        /// <returns></returns>
        public List<SPGET_CHECK_AUTHENTICATEUSERResult> CheckUserAuthentication(string UserName, string Password)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            ISingleResult<SPGET_CHECK_AUTHENTICATEUSERResult> result = db.SPGET_CHECK_AUTHENTICATEUSER(UserName, Password);
            return result.ToList();
        }

        /// <summary>
        /// This method checks the user has privilege to access the application
        /// or not.
        /// </summary>
        /// <param name="UserId">User id of the user to check the privilege</param>
        /// <param name="UrlToAuthenticate">Url to authorize</param>
        /// <returns></returns>
        public List<SPGET_FEATURE_ROLESResult> CheckForUserRole(int UserId, string UrlToAuthenticate)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPGET_FEATURE_ROLES(UserId, UrlToAuthenticate);
            return result.ToList();
        }

        /// <summary>
        /// This method get the list of client request details
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="System.Int32"/> that holds the logged in userid
        /// </param>
        /// <param name="ClientRequestNumber">
        /// A <see cref="string"/> contains the client request number
        /// </param>
        /// <param name="Client">
        /// A <see cref="integer"/> contains the client request id
        /// </param>
        /// <param name="POSTION_NAME">
        /// A <see cref="string"/> contains the position of the client request number
        /// </param>
        /// <param name="CREATED_DATE">
        /// A <see cref="datetime"/> contains the client request created date
        /// </param>
        /// <param name="PAGENUM">
        /// A <see cref="integer"/> contains the page number
        /// </param>
        /// <param name="PAGESIZE">
        /// A <see cref="integer"/> contains the page size
        /// </param>
        /// <param name="ORDERBY">
        /// A<see cref="string"/> contains the order by direction of the list
        /// </param>
        /// <param name="ORDERBYDIRECTION">
        /// A <see cref="char"/> contains whether ascending or descending value
        /// </param>
        /// <returns></returns>
        public List<SPGET_CLIENT_REQUEST_INFORMATION_SEARCHResult> GetClientRequestNumber(string PositionProfileName,
            string Client, object POSTION_NAME, object CREATED_DATE, string CONTACT_NAME, string LOCATION, string DEPARTMENT_NAME,
            string CREATED_BY, int? PAGENUM,
            int? PAGESIZE, string ORDERBY, char ORDERBYDIRECTION, int UserId)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            DateTime? CreatedDate = null;
            if (CREATED_DATE != null)
                CreatedDate = Convert.ToDateTime(CREATED_DATE);
            var ClientRequestInfo = db.SPGET_CLIENT_REQUEST_INFORMATION_SEARCH(UserId, PositionProfileName == null ? string.Empty : PositionProfileName.ToString(),
                Client,
                POSTION_NAME == null ? string.Empty : POSTION_NAME.ToString(), CreatedDate, CONTACT_NAME, LOCATION, DEPARTMENT_NAME, CREATED_BY, PAGENUM, PAGESIZE, ORDERBY,
                ORDERBYDIRECTION.ToString());
            return ClientRequestInfo.ToList();
        }
        /// <summary>
        /// This method send the mail to email recruiter
        /// </summary>
        /// <param name="toAddresss"></param>
        /// <param name="fromAddress"></param>
        /// <param name="cc"></param>
        //public void SendMail(string toAddresss, string fromAddress, string cc)
        //{
        //    EmailHandler email = new EmailHandler();
        //    email.SendMail(toAddresss, fromAddress, cc);
        //}
        public void LogExceptionMessage(string message)
        {
            Forte.HCM.Trace.Logger.ExceptionLog(message);
            //Logger1.ExceptionLog(message);            
        }
        public void LogException(string message, string innerException, string stackTrace)
        {
            Forte.HCM.Trace.Logger.ExceptionLog(new Exception(message, new Exception(innerException)), stackTrace);
        }
        public string CheckJobBoardLoginCredentials(string loginName, string password)
        {
            try
            {
                // TO DO : remove this comment 
                return new Forte.HCM.ExternalResumeManager.CareerBuilderResumeManager().CheckJobBoardLoginCredentials(loginName, password);
            }
            catch (Exception exp)
            {
                Forte.HCM.Trace.Logger.ExceptionLog(new Exception(exp.Message, new Exception(exp.InnerException.ToString())), exp.StackTrace);
                return "";
            }
            // TO DO : remove this Line 
            // return new Forte.HCM.ExternalResumeManager.TestCareerBuilderResumeManager().CheckJobBoardLoginCredentials(loginName, password);
        }
        public ScoreDetails GetResumeDetails(JobBoardSearchTerms searchTerms, string searchField, int pageNo, int pageSize, int userId, int positionProfileId, int tenantId, out string errorMessage)
        {
            try
            {
                //string candidateIds = new Forte.HCM.ExternalResumeManager.CareerBuilderResumeManager().GetCareerBuilderResumeResults("<Packet><SessionToken>" + searchTerms.SessionToken + "</SessionToken><Keywords>" + searchTerms.Keywords + "</Keywords><PageNumber>" + pageNo + "</PageNumber><RowsPerPage>" + pageSize + "</RowsPerPage></Packet>", searchTerms.SessionToken, tenantId, userId, out errorMessage);
                string searchPacket = JobBoardXml(searchTerms);
                Forte.HCM.Trace.Logger.ExceptionLog("Resume Search XML " + searchPacket);
                //TO DO Comment remove
                string candidateIds = new Forte.HCM.ExternalResumeManager.CareerBuilderResumeManager().GetCareerBuilderResumeResults(searchPacket, searchTerms.SessionToken, tenantId, userId, out errorMessage);

                //TO DO For Remove this Line.
                //string candidateIds = new Forte.HCM.ExternalResumeManager.TestCareerBuilderResumeManager().GetCareerBuilderResumeResults(searchPacket, searchTerms.SessionToken, tenantId, userId,pageNo, pageSize, out errorMessage);

                TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
                db.CommandTimeout = GetCommandTimeout();
                IMultipleResults result = db.SPTSGET_CANDIDATES_RESUMESCORE_BY_SEARCHTERMS_CANDIDATES(positionProfileId, tenantId, searchField, candidateIds, pageNo, pageSize);

                var scoreDistribution = result.GetResult<SearchTerms_ScorePercentage>();
                var candidates = result.GetResult<SPTSGET_CANDIDATES_BYSEARCHTERMResult>();
                var searchSummary = result.GetResult<SearchSummaryDetails>();
                var searchTerm_SearchedCandidates = result.GetResult<SearchTerms_No_Of_Candidates>();
                var technicalDetails = result.GetResult<CandidateTechnicalSkills>();
                ScoreDetails details = new ScoreDetails();
                details.SearchTerms_ScoreDistribution = scoreDistribution.ToList();
                details.CandidateDetails = candidates.ToList();
                details.SearchTerms_NoOfCandidates = searchTerm_SearchedCandidates.ToList();
                details.SearchTermSummaryDetails = searchSummary.ToArray()[0];
                details.TechnicalSkills = technicalDetails.ToList();
                return details;

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return null;
        }


        public List<SPTSINSERT_POSITION_PROFILE_CANDIDATESResult> PickCandidate(int candidateId, int positionProfileId, int pickedBy)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPTSINSERT_POSITION_PROFILE_CANDIDATES(candidateId, positionProfileId, pickedBy);
            return result.ToList();
        }

        public string PickCandidates(List<string> selectedCandidates, int positionProfileId, int pickedBy)
        {
            try
            {
                TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
                db.CommandTimeout = GetCommandTimeout();
                if (selectedCandidates == null || selectedCandidates.Count == 0)
                    return "No candidates to pick";
                foreach (string candidateId in selectedCandidates)
                {
                    var result = db.SPTSINSERT_POSITION_PROFILE_CANDIDATES(Convert.ToInt32(candidateId), positionProfileId, pickedBy);
                }
                return "Candidates are picked to add in position profile";
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return ex.Message.ToString();
            }

        }

        public string JobBoardXml(JobBoardSearchTerms jobBoardSearchTerms)
        {
            MemoryStream stream = null;
            TextWriter writer = null;
            try
            {

                stream = new MemoryStream(); // read xml in memory

                writer = new StreamWriter(stream, Encoding.Unicode);
                // get serialise object
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                XmlSerializer serializer = new XmlSerializer(typeof(JobBoardSearchTerms));
                serializer.Serialize(writer, jobBoardSearchTerms, ns); // read object

                int count = (int)stream.Length; // saves object in memory stream

                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                // copy stream contents in byte array

                stream.Read(arr, 0, count);
                UnicodeEncoding utf = new UnicodeEncoding(); // convert byte array to string
                string XML = utf.GetString(arr).Trim();
                XML.Replace("<?xml version=\"1.0\" encoding=\"utf-16\" ?>", "").Replace("JobBoardSearchTerms", "Packet");
                return XML;
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                if (stream != null) stream.Close();
                if (writer != null) writer.Close();
            }
        }

        public int DeletePickedCandidates(int positionProfileID, int canditateID)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            var result = db.SPDELETE_POSITION_PROFILE_CANDIDATE(positionProfileID, canditateID);

            return Convert.ToInt32(result);

        }
        public List<SPSEARCH_CANDIDATE_ACTIVITY_LOGResult> GetViewNotesRequest(
            int candidate_id, object activity_type, int? activity_by, object activity_date_from, object activity_date_to,
            object comments, string orderby_direction, string sort_expression, int? page_num, int? page_size)
        {
            TalentScoutDataClassesDataContext db = new TalentScoutDataClassesDataContext();
            db.CommandTimeout = GetCommandTimeout();
            DateTime? activityFromDate = null;
            DateTime? activityToDate = null;
            if (activity_date_from != null)
                activityFromDate = Convert.ToDateTime(activity_date_from);
            if (activity_date_to != null)
                activityToDate = Convert.ToDateTime(activity_date_to);
            var ViewNotesRequestInfo = db.SPSEARCH_CANDIDATE_ACTIVITY_LOG(candidate_id,
                activity_type == null ? null : activity_type.ToString(),
                activity_by,
                activityFromDate, activityToDate,
                comments == null ? string.Empty : comments.ToString(),
                orderby_direction, sort_expression, page_num, page_size);

            return ViewNotesRequestInfo.ToList();
        }

    }
}
