﻿using System;
using System.Runtime.Serialization;

namespace Forte.HCM.Exceptions
{
    public class EMailException : ApplicationException
    {
     /// <summary>
        /// Default constructor.
        /// </summary>
        public EMailException()
        {
        }

        /// <summary>
        /// Overloaded constructor that will construct the message.
        /// </summary>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        public EMailException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Overloaded constructor that will construct the message and inner
        /// exception.
        /// </summary>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        /// <param name="innerException">
        /// A <see cref="Exception"/> that holds the inner exception.
        /// </param>
        public EMailException
            (string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Overloaded constructor that will construct the serialization
        /// info and streaming context.
        /// </summary>
        /// <param name="info">
        /// A <see cref="SerializationInfo"/> that holds the serialization 
        /// information.
        /// </param>
        /// <param name="context">
        /// A <see cref="StreamingContext"/> that holds the streaming context.
        /// </param>
        protected EMailException(SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
