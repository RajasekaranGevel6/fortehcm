﻿using System;
using System.Runtime.Serialization;

namespace Forte.HCM.Exceptions
{
    [Serializable]
    public class NextNumberException : ApplicationException 
    {
        /// <summary>
        /// Represents the default contructor.
        /// </summary>
        public NextNumberException()
        {
        }

        /// <summary>
		/// Represents the constructor that composes the exception object
        /// with the given message.
		/// </summary>
		/// <param name="message">
        /// A <see cref="string"/> that holds the exception message.
        /// </param>
		public NextNumberException(string message) 
            : base(message)
		{
		}

		/// <summary>
        /// Represents the constructor that composes the exception object
        /// with the given message and inner exception object.
		/// </summary>
		/// <param name="message">
        /// A <see cref="string"/> that holds the exception message.
        /// </param>
		/// <param name="innerException">
        /// A <see cref="Exception"/> that holds the inner exception object.
        /// </param>
        public NextNumberException(string message, Exception innerException)
			: base(message, innerException)
		{
		}	

        /// <summary>
        /// Represents the constructor that composes the exception object
        /// with the given serialization info and streaming context object.
        /// </summary>
        /// <param name="info">
        /// A <see cref="SerializationInfo"/> that holds the serialization info.
        /// </param>
        /// <param name="context">
        /// A <see cref="StreamingContext"/> that holds the streaming context.
        /// </param>
        protected NextNumberException(SerializationInfo info, 
           StreamingContext context) : base(info, context)
        {
        }
    }
}
