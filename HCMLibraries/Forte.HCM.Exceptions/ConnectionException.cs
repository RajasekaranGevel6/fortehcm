﻿#region Header
// Copyright (C) 2010-2011 Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ConnectionException.cs
// This file has the class Logger that provides features to 
// log the trace and exception messages.
//
#endregion

#region Directives

using System;
using System.Runtime.Serialization;

#endregion Directives

namespace Forte.HCM.Exceptions
{
    [Serializable]
    public class ConnectionException : ApplicationException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ConnectionException()
        {
        }

        /// <summary>
        /// This constructor will get the message as 
        /// parameter and it will be used when
        /// this exception class needs to be instantiated 
        /// with exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public ConnectionException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// This constructor will get the message and 
        /// innerException exception as parameter and it will 
        /// be used when this exception class needs to be
        /// instantiated with exception message
        /// and inner exception
        /// </summary>
        /// <param name="message">
        /// exception message</param>
        /// <param name="innerException">
        /// The exception that caused this exception</param>
        public ConnectionException
            (string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// This constructor will get the serialization info and 
        /// streaming context as parameter.
        /// </summary>
        /// <param name="info">
        /// A <see cref="SerializationInfo"/> that holds the serialization info.
        /// </param>
        /// <param name="context">
        /// A <see cref="StreamingContext"/> that holds the streaming context.
        /// </param>
        protected ConnectionException(SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
