﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalDataManager.aspx.cs
// File that process the raw data and constructs the customized xml data.
// The xml data is used as source for contacting external services.

#endregion Header

#region Directives

using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.ExternalDataService
{
    /// <summary>
    /// Class that process the raw data and constructs the customized xml data.
    /// The xml data is used as source for contacting external services.
    /// </summary>
    public class ExternalDataManager
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="int"/> that holds the position segment ID.
        /// </summary>
        private const int POSITION_SEGMENT_ID = 1;

        /// <summary>
        /// A <see cref="int"/> that holds the technical skills segment ID.
        /// </summary>
        private const int TECHNICAL_SKILLS_SEGMENT_ID = 2;

        /// <summary>
        /// A <see cref="int"/> that holds the verticals ID.
        /// </summary>
        private const int VERTICALS_SEGMENT_ID = 3;

        /// <summary>
        /// A <see cref="int"/> that holds the roles segment ID.
        /// </summary>
        private const int ROLES_SEGMENT_ID = 4;

        /// <summary>
        /// A <see cref="int"/> that holds the education segment ID.
        /// </summary>
        private const int EDUCATION_SEGMENT_ID = 8;

        #endregion Private Constants

        /// <summary>
        /// Method that constructs and returns the xml for position profile 
        /// candidate submission process.
        /// </summary>
        /// <param name="dataKey">
        /// A <see cref="string"/> that holds the data key.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="candidateLoginID">
        /// A <see cref="int"/> that holds the candidate login ID.
        /// </param>
        /// <param name="submitterDetail">
        /// A <see cref="UserDetail"/> that holds the submitter detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the xml data.
        /// </returns>
        public string GetPositionProfileCandidateData(string dataKey,
            int positionProfileID, int candidateLoginID, UserDetail submitterDetail)
        {
            // Get the data structure.
            ExternalDataStructure dataStructure = new ExternalDataDLManager().
                GetExternalDataStructure(dataKey);

            if (dataStructure == null || Utility.IsNullOrEmpty(dataStructure.DataStructure))
                throw new Exception("Data structure not exists for the given key");

            // Get position profile detail.
            PositionProfileDetail positionProfileDetail = new ExternalDataDLManager().
                GetPositionProfile(positionProfileID);

            // Check if position profile is null.
            if (positionProfileDetail == null)
                throw new Exception("Position profile detail not found");

            // Get candidate detail.
            CandidateDetail candidateDetail = new ExternalDataDLManager().
                GetCandidate(candidateLoginID);

            // Check if candidate is null.
            if (candidateDetail == null)
                 throw new Exception("Candidate detail not found");

            // Create xml document object.
            XmlDocument document = new XmlDocument();
            document.LoadXml(dataStructure.DataStructure);

            // construct the xml for position profile & candidate detail.
            ConstructXML(document, positionProfileDetail, candidateDetail, submitterDetail);

            return document.OuterXml;
        }

        /// <summary>
        /// Method that constructs and returns the xml for position profile 
        /// candidate submission process.
        /// </summary>
        /// <param name="dataKey">
        /// A <see cref="string"/> that holds the data key.
        /// </param>
        /// <param name="positionProfileDetail">
        /// A <see cref="PositionProfileDashboard"/> that holds the position profile detail.
        /// </param>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateInformation"/> that holds the candidate detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the xml data.
        /// </returns>
        public string GetPositionProfileCandidateData(string dataKey,
            PositionProfileDashboard positionProfileDetail, CandidateInformation candidateDetail)
        {
            string xml = null;

            return xml;
        }

        /// <summary>
        /// Method that constructs and returns the xml for position profile and
        /// candidate detail.
        /// </summary>
        /// <param name="document">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        /// <param name="positionProfileDetail">
        /// A <see cref="PositionProfileDetail"/> that holds the position profile detail.
        /// </param>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </param>
        /// <param name="submitterDetail">
        /// A <see cref="UserDetail"/> that holds the submitter detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the xml data.
        /// </returns>
        private void ConstructXML(XmlDocument document, 
            PositionProfileDetail positionProfileDetail, CandidateDetail candidateDetail,
            UserDetail submitterDetail)
        {
            // Assign position details.
            if (positionProfileDetail.Segments != null)
            {
                // Check if position segment is present.
                SegmentDetail positionSegmentDetail = positionProfileDetail.Segments.Find
                    (item => item.SegmentID == POSITION_SEGMENT_ID);

                if (positionSegmentDetail != null && positionSegmentDetail.DataSource != null)
                {
                    XmlDocument positionSegmentDocument = new XmlDocument();
                    positionSegmentDocument.LoadXml(positionSegmentDetail.DataSource);

                    XmlNode node = null;

                    #region FHCM

                    #region Position Details

                    // Assign position ID.
                    node = document.SelectSingleNode("FHCM//PositionDetails//PositionID");
                    node.InnerText = positionProfileDetail.PositionID.ToString();

                    // Assign position name.
                    node = document.SelectSingleNode("FHCM//PositionDetails//PositionName");
                    node.InnerText = positionProfileDetail.PositionName;

                    // Assign registered date.
                    node = document.SelectSingleNode("FHCM//PositionDetails//RegisteredDate");
                    node.InnerText = GetDateValue(positionProfileDetail.RegistrationDate);

                    // Assign no of position.
                    node = document.SelectSingleNode("FHCM//PositionDetails//NoOfPosition");
                    node.InnerText = GetValue(positionSegmentDocument.SelectSingleNode("ClientPositionDetails//NumberOfPositions").InnerText);

                    // Assign nature of position.
                    node = document.SelectSingleNode("FHCM//PositionDetails//NatureOfPosition");
                    node.InnerText = GetValue(positionSegmentDocument.SelectSingleNode("ClientPositionDetails//NatureOfPosition").InnerText);

                    // Assign requirement.
                    node = document.SelectSingleNode("FHCM//PositionDetails//Requirement");
                    node.InnerText = string.Empty;

                    // Assign additional info.
                    node = document.SelectSingleNode("FHCM//PositionDetails//AdditionalInfo");
                    node.InnerText = GetValue(positionProfileDetail.PositionProfileAdditionalInformation);

                    #region Client Info

                    // Assign client ID.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//ClientID");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.ClientID);

                    // Assign client name.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//ClientName");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.ClientName);

                    // Assign FEIN no.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//FEINNo");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.FeinNo);

                    // Assign phone.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//Phone");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.ContactInformation.Phone.Office);

                    // Assign email.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//EMail");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.ContactInformation.EmailAddress);

                    #region Location

                    // Assign address.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//Location//Address");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.ContactInformation.StreetAddress);

                    // Assign city.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//Location//City");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.ContactInformation.City);

                    // Assign state.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//Location//State");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.ContactInformation.State);

                    // Assign zip.
                    node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//Location//Zip");
                    node.InnerText = GetValue(positionProfileDetail.ClientInformation.ContactInformation.PostalCode);

                    #endregion Location

                    #region Contact Info

                    // Check if client contact information is present. If present took the top contact.
                    if (positionProfileDetail.ClientInformation != null &&
                        positionProfileDetail.ClientInformation.ClientContactInformations != null &&
                        positionProfileDetail.ClientInformation.ClientContactInformations.Count > 0)
                    {
                        ClientContactInformation contactInfo = positionProfileDetail.ClientInformation.ClientContactInformations[0];

                        // Assign first name.
                        node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//ContactInfo//FName");
                        node.InnerText = GetValue(contactInfo.FirstName);

                        // Assign middle name.
                        node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//ContactInfo//MName");
                        node.InnerText = GetValue(contactInfo.MiddleName);

                        // Assign last name.
                        node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//ContactInfo//LName");
                        node.InnerText = GetValue(contactInfo.LastName);

                        // Assign email.
                        node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//ContactInfo//EMail");
                        node.InnerText = GetValue(contactInfo.EmailAddress);

                        // Assign phone.
                        node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//ContactInfo//Phone");
                        node.InnerText = GetValue(contactInfo.PhoneNumber);

                        // Assign mobile.
                        node = document.SelectSingleNode("FHCM//PositionDetails//ClientInfo//ContactInfo//Mobile");
                        node.InnerText = GetValue(contactInfo.MobileNumber);
                    }

                    #endregion Contact Info

                    #endregion Client Info

                    #region Candidate Info

                    // Assign candidate reference ID.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//CandidateReferenceID");
                    node.InnerText = GetIntegerValue(candidateDetail.CandidateLoginID).ToString();

                    // Assign first name.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//FName");
                    node.InnerText = GetValue(candidateDetail.FirstName);

                    // Assign middle name.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//MName");
                    node.InnerText = GetValue(candidateDetail.MiddleName);

                    // Assign last name.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//LName");
                    node.InnerText = GetValue(candidateDetail.LastName);

                    // Assign email.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//EMail");
                    node.InnerText = GetValue(candidateDetail.EMailID);

                    // Assign phone.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//Phone");
                    node.InnerText = GetValue(candidateDetail.Phone);

                    #region Location

                    // Assign address.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//Address");
                    node.InnerText = GetValue(candidateDetail.Address);

                    // Assign city.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//City");
                    node.InnerText = GetValue(candidateDetail.City);

                    // Assign state.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//State");
                    node.InnerText = GetValue(candidateDetail.State);

                    // Assign zip.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//Zip");
                    node.InnerText = GetValue(candidateDetail.ZipCode);

                    #endregion Location

                    // Assign resume text.
                    node = document.SelectSingleNode("FHCM//PositionDetails//CandidateInfo//ResumeText");
                    node.InnerText = GetValue(candidateDetail.ResumeText);

                    #endregion Candidate Info

                    #endregion Position Details

                    // Assign submitted by.
                    node = document.SelectSingleNode("FHCM//SubmittedBy");
                    node.InnerText = GetValue(submitterDetail.FirstName);

                    // Assign submitted date.
                    node = document.SelectSingleNode("FHCM//SubmittedDate");
                    node.InnerText = GetDateValue(DateTime.Now);

                    #endregion FHCM
                }
            }
        }

        /// <summary>
        /// Method that retrieves the non null value from an object.
        /// </summary>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the non null value. If data is
        /// null then it returns empty string.
        /// </returns>
        private string GetValue(object data)
        {
            if (data == null)
                return string.Empty;

            return data.ToString().Trim();
        }

        /// <summary>
        /// Method that retrieves the non null date value from an object.
        /// </summary>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the non null data value. If data is
        /// null then it returns empty string.
        /// </returns>
        private string GetDateValue(object data)
        {
            if (data == null)
                return string.Empty;

            DateTime date = DateTime.MinValue;

            if (DateTime.TryParse(data.ToString(), out date))
            {
                return date.ToString("MM/dd/yyyy").Trim();
            }

            return string.Empty;
        }

        /// <summary>
        /// Method that retrieves the non null integer value from an object.
        /// </summary>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the non null integer value. If data 
        /// is null then it returns 0;
        /// </returns>
        private int GetIntegerValue(object data)
        {
            if (data == null)
                return 0;

            return Convert.ToInt32(data.ToString().Trim());
        }
    }
}
