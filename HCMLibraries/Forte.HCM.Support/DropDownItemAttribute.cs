﻿using System;

namespace ReflectionComboItem
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false,
        Inherited = false)]
    public class DropDownItemAttribute : Attribute
    {
        private string displayText = null;
        private string valueText = null;

        public DropDownItemAttribute(string displayText, string valueText)
        {
            this.displayText = displayText;
            this.valueText = valueText;
        }

        public string DisplayText
        {
            get 
            {
                return displayText;
            }
        }

        public string ValueText
        {
            get
            {
                return valueText;
            }
        }
    }
}