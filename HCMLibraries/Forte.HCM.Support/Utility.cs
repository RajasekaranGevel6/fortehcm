﻿#region Header                                                                 

// Copyright (C) 2010-2011 Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Utility.cs
// File that holds the common utility methods used across the application.

#endregion Header

using System;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Globalization;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;


namespace Forte.HCM.Support
{
    /// <summary>
    /// Represents the class that provide common utility methods 
    /// that will be shared among all classes.
    /// </summary>
    public class Utility
    {
        /// <summary>
        /// Method that constructs the valid file name string from the given 
        /// source of the given length.
        /// </summary>
        /// <param name="source">
        /// A <see cref="string"/> that holds the source string.
        /// </param>
        /// <param name="length">
        /// A <see cref="int"/> that holds the length.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds a valid file name string.
        /// </returns>
        
        public static string GetValidExportFileNameString(string source, int length)
        {
            if (source == null)
                return string.Empty;

            // Trim the file name.
            source = source.Trim();

            // Remove spaces.
            source = source.Replace(" ", "");

            // Remove invalid file name characters with underscore.
            foreach (char invalidCharacter in Path.GetInvalidFileNameChars())
            {
                source = source.Replace(invalidCharacter, '_');
            }

            if (source.Length > length)
                return source.Substring(0, length);
            else
                return source;
        }

        /// <summary>
        /// Method that checks if the given value is null or empty.
        /// </summary>
        /// <param name="value">
        /// A <see cref="object"/> that holds the value.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. True represents null or
        /// empty and false otherwise.
        /// </returns>
        public static bool IsNullOrEmpty(object value)
        {
            if (value == null)
                return true;

            return string.IsNullOrEmpty(value.ToString().Trim());
        }

        /// Methodt that convert the seconds to hour, minute and seconds
        /// format (hh:mm:ss).
        /// </summary>
        /// <param name="seconds">
        /// A <see cref="int"/> that holds the seconds.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the formatted string.
        /// </returns>
        public static string ConvertSecondsToHoursMinutesSeconds(int seconds)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            string hoursMinutesSeconds = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    timeSpan.Hours,
                                    timeSpan.Minutes,
                                    timeSpan.Seconds);

            return hoursMinutesSeconds;
        }

        /// <summary>
        /// Method that convert the seconds to hour, minute format (hh:mm).
        /// </summary>
        /// <param name="seconds">
        /// A <see cref="int"/> that holds the seconds.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the formatted string.
        /// </returns>
        public static string ConvertSecondsToHHMM(int seconds)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            string hoursMinutesSeconds = string.Format("{0:D2}:{1:D2}",
                timeSpan.Hours,
                timeSpan.Minutes);

            return hoursMinutesSeconds;
        }

        /// <summary>
        /// Methodt that convert the seconds to hour and minute textual phrase
        /// format.
        /// </summary>
        /// <param name="seconds">
        /// A <see cref="int"/> that holds the seconds.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the formatted string.
        /// </returns>
        public static string ConvertSecondsToHoursMinutes(int seconds)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            if (timeSpan.Hours == 0 && timeSpan.Minutes > 0)
            {
                if (timeSpan.Minutes == 1)
                    return string.Format("{0} minute", timeSpan.Minutes);
                else
                    return string.Format("{0} minutes", timeSpan.Minutes);
            }
            else if (timeSpan.Hours > 0 && timeSpan.Minutes == 0)
            {
                if (timeSpan.Hours == 1)
                    return string.Format("{0} hour", timeSpan.Hours);
                else
                    return string.Format("{0} hours", timeSpan.Hours);
            }
            else
            {
                if (timeSpan.Hours == 1 && timeSpan.Minutes == 1)
                    return string.Format("{0} hour & {1} minute", timeSpan.Hours, timeSpan.Minutes);
                else if (timeSpan.Hours == 1 && timeSpan.Minutes != 1)
                    return string.Format("{0} hour & {1} minutes", timeSpan.Hours, timeSpan.Minutes);
                else if (timeSpan.Hours != 1 && timeSpan.Minutes == 1)
                    return string.Format("{0} hours & {1} minute", timeSpan.Hours, timeSpan.Minutes);
                else
                    return string.Format("{0} hours & {1} minutes", timeSpan.Hours, timeSpan.Minutes);
            }
        }

        /// <summary>
        /// Methodt that convert the seconds to hour and minute textual phrase
        /// format.
        /// </summary>
        /// <param name="seconds">
        /// A <see cref="int"/> that holds the seconds.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the formatted string.
        /// </returns>
        public static string ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(int seconds)
        {
            string textualPhrase = string.Empty;
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            if (timeSpan.Hours == 1)
                textualPhrase = string.Format("{0} hour ", timeSpan.Hours);
            if (timeSpan.Hours > 1)
                textualPhrase = string.Format("{0} hours ", timeSpan.Hours);
            if (timeSpan.Minutes == 1)
                textualPhrase = string.Concat(textualPhrase, string.Format("{0} minute ", timeSpan.Minutes));
            if (timeSpan.Minutes > 1)
                textualPhrase = string.Concat(textualPhrase, string.Format("{0} minutes ", timeSpan.Minutes));
            if (timeSpan.Seconds == 1)
                textualPhrase = string.Concat(textualPhrase, string.Format("{0} second", timeSpan.Seconds));
            if (timeSpan.Seconds > 1)
                textualPhrase = string.Concat(textualPhrase, string.Format("{0} seconds", timeSpan.Seconds));
            if(textualPhrase==string.Empty)
                textualPhrase = string.Format("{0} second", timeSpan.Seconds);

            return textualPhrase;
        }
        /// <summary>
        /// Convert hh:mm:ss format to seconds 
        /// </summary>
        /// <param name="hoursMinitsSeconds"></param>
        /// <returns></returns>
        public static int ConvertHoursMinutesSecondsToSeconds(string hoursMinitsSeconds)
        {
            if (hoursMinitsSeconds.Contains("."))
                hoursMinitsSeconds = hoursMinitsSeconds.Replace(".", ":");
            string time = hoursMinitsSeconds;//hh:mm:ss 
            int seconds = Convert.ToInt32(TimeSpan.Parse(time).TotalSeconds);
            return seconds;
        }

        /// <summary>
        /// verifing mm:ss within the range
        /// </summary>
        /// <param name="MinitsSeconds"></param>
        /// <returns></returns>
        public static bool VerifyMinutesSeconds(string MinitsSeconds)
        { 
            if(string.IsNullOrEmpty(MinitsSeconds)) return false;

            string[] strTime = MinitsSeconds.Split(':');
            
            if (strTime.Length < 2) return false;

            if (Convert.ToInt32(strTime[0]) > 59 
                || Convert.ToInt32(strTime[1]) > 59)
                return false;
            else
                return true; 
        }


        /// <summary>
        /// Convert mm:ss format to seconds 
        /// </summary>
        /// <param name="hoursMinitsSeconds"></param>
        /// <returns></returns>
        public static int ConvertMinutesSecondsToSeconds(string MinitsSeconds)
        {
            string time = "00:"+ MinitsSeconds;//hh:mm:ss 
            int seconds = Convert.ToInt32(TimeSpan.Parse(time).TotalSeconds);
            return seconds;
        }

        /// <summary>
        /// Convert hh:mm:ss format to seconds 
        /// </summary>
        /// <param name="hoursMinitsSeconds"></param>
        /// <returns></returns>
        public static string GetComplexity(decimal ComplexityValue)
        {
            string complexityName = "";

            if (ComplexityValue > -1 && ComplexityValue < 1.25m)
            {
                complexityName = "SIMPLE";
            }
            else if (ComplexityValue > 1.25m && ComplexityValue < 1.75m)
            {
                complexityName = "MEDIUM";
            }
            else if (ComplexityValue > 1.75m && ComplexityValue < 5)
            {
                complexityName = "COMPLEX";
            }
            else
            {
                complexityName = "SIMPLE";
            }

            return complexityName;
        }

        public static bool IsValidEmailAddress(string strUserEmailId)
        {
            Regex regex = new Regex(ConfigurationManager.AppSettings["VALID_EMAIL_PATTERN"], 
                RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        public static bool IsValidWebsiteURL(string strWebsiteURL)
        {
            Regex regex = new Regex(ConfigurationManager.AppSettings["VALID_WEB_SITE_PATTERN"], 
                RegexOptions.Compiled);
            return regex.IsMatch(strWebsiteURL);
        }

        /// <summary>
        /// Method that returns the number in letters.
        /// </summary>
        /// <param name="number">
        /// An <see cref="int"/> that holds the number.
        /// </param>
        /// </returns>
        /// <remarks>
        /// This method converts numbers from 0 - 100 into equivalent letters.
        /// </remarks>
        public static string GetNumberInLetters(int number)
        {
            if (number < 0 || number > 100)
                return string.Empty;

            string words = string.Empty;
           
            if (number == 0)
                return "zero";
            
            if ((number / 100) > 0)
            {
                words += GetNumberInLetters(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += " " + unitsMap[number % 10];
                }
            }   
                 
            return words;
        }

        /// <summary>
        /// Method that generates random character
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(1, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);
                    if (randomLower.Next(1, 4) == 1)
                    {
                        ch = (char)(ch + 32);
                    }
                    builder.Append(ch);
                    i++;
                }
            }
            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// This returns the online interview is currently active or not
        /// </summary>
        /// <param name="onlineInterviewDate">
        /// <see cref="System.DateTime"/> that holds the interview date
        /// </param>
        /// <param name="timeFrom">
        /// <see cref="System.String"/> that holds the interview start time
        /// </param>
        /// <param name="timeTo">
        /// <see cref="System.String"/> that holds the interview end time
        /// </param>
        /// <returns>
        /// <see cref="System.Boolean"/> returns onterview active status
        /// </returns>
        public bool GetOnlineInterviewAvailable(DateTime onlineInterviewDate,
            string timeFrom, string timeTo)
        {
            if (onlineInterviewDate != DateTime.Today) return false;

            string currentTime = DateTime.Now.ToString("HH:mm:ss");

            DateTime dbStartTime = Convert.ToDateTime(timeFrom.ToString(new DateTimeFormatInfo()));

            dbStartTime =dbStartTime.AddMinutes(-10);

            DateTime dbEndTime = Convert.ToDateTime(timeTo.ToString(new DateTimeFormatInfo()));

            DateTime currentSysTime = Convert.ToDateTime(currentTime);

            DateTime timeExpiry = DateTime.ParseExact(currentTime, "HH:mm:ss", new DateTimeFormatInfo());

            TimeSpan expiryDiff = dbEndTime.Subtract(timeExpiry);

            //if end time is less the current time 
            if (expiryDiff.Minutes <= 15 && expiryDiff.Hours < 0) return false;

            TimeSpan startTimeDiff = currentSysTime.Subtract(dbStartTime); 

            //if start time differnece is less the 10 mins
            if ((startTimeDiff.Minutes < 10 || startTimeDiff.Minutes == 0)) return true; 

            return true ;
        }

        #region "Page Read-Only"

        public static void DisablePageControls(Page curPage)
        {

            foreach (Control cntrl in curPage.Controls)
            {
                DisablePageControls(cntrl);
            }
        }

        private static void DisablePageControls(Control myControl)
        {
            foreach (Control cntrl in myControl.Controls)
            {
                switch (cntrl.GetType().ToString())
                {
                    case "System.Web.UI.WebControls.TextBox":
                        ((TextBox)cntrl).Enabled = false;
                        break;
                    case "System.Web.UI.WebControls.Button":
                        ((Button)cntrl).Enabled = false;
                        break;
                    case "System.Web.UI.WebControls.ImageButton":
                        ((ImageButton)cntrl).Enabled = false;
                        break;
                    case "System.Web.UI.WebControls.DropDownList":
                        ((DropDownList)cntrl).Enabled = false;
                        break;
                    case "System.Web.UI.WebControls.CheckBox":
                        ((CheckBox)cntrl).Enabled = false;
                        break;
                    case "System.Web.UI.WebControls.CheckBoxList":
                        ((CheckBoxList)cntrl).Enabled = false;
                        break;
                    default:
                        break;
                }

                //Call for additional controls:
                if (cntrl.HasControls())
                    DisablePageControls(cntrl);
            }
        }

        #endregion "Page Read-Only"
    }
}
