﻿#region Header

// ApplicationSupportManager.cs
// This class provides features to form connection string 
// during runtime and creating database object.

#endregion

#region Directives

using System;
using System.Configuration;
using System.Collections.Specialized;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

#endregion Directives

namespace Forte.HCM.Support
{
    public class ApplicationSupportManager
    {
        // hold web config section
        private static object webConfigView;

        // collConnStrMode is used to provide the web.config 
        // connection string key collection. 
        private static NameValueCollection collHCMGeneralMode;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="ApplicationSupportManager"/> class.
        /// </summary>
        public ApplicationSupportManager()
        {
        }

        /// <summary>
        /// Method that creates and returns the database object for the default
        /// database given the config file. 
        /// </summary>
        /// <returns>
        /// A <see cref="Database"/> that holds the database object.
        /// </returns>
        public static Database GetDatabase()
        {
            IConfigurationSource configSource =
                ConfigurationSourceFactory.Create();

            // Get the configuration file path.
            string configFilePath = configSource.GetSection
                (Constants.ConfigConstants.CONST_CONNSTR_SECTION).ElementInformation.Source;

            // Create file configuration source.
            FileConfigurationSource dataSource =
                new FileConfigurationSource(configFilePath);

            // Create database provider factory.
            DatabaseProviderFactory dbFactory =
                new DatabaseProviderFactory(dataSource);

            // Retrieve the default connection string key;
            string defaultConnectionStringKey = GetDefaultConnectionStringKey();

            // Create and return the database object for the default connection
            // string key.
            return dbFactory.Create(defaultConnectionStringKey);
        }

        /// <summary>
        /// Method that retrieves the default connection string key.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the connection string key.
        /// </returns>
        private static string GetDefaultConnectionStringKey()
        {
            // Get general settings section of web.config.
            webConfigView = ConfigurationManager.GetSection(
                Constants.ConfigConstants.CONST_CONFIG_GENERAL);

            if (webConfigView == null)
            {
                throw new Exception
                    ("HCMGeneral section group not present in the config file");
            }

            //  Get general settings string key collection.
            collHCMGeneralMode = ((NameValueCollection)webConfigView);

            if (collHCMGeneralMode == null || collHCMGeneralMode.Count == 0)
            {
                throw new Exception
                   ("HCMGeneral section not present in the config file");
            }

            return collHCMGeneralMode.Get("DEFAULT_CONNECTION_STRING");
        }
    }
}
