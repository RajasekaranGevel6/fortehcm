﻿using System;
using System.Reflection;
using System.Collections.Generic;

namespace ReflectionComboItem
{
    public class ReflectionManager
    {
        public List<DropDownItem> GetListItems(object data)
        {
            List<DropDownItem> items = null;

            // Retrieves the type of the object.
            Type type = data.GetType();

            // Retrieves all the properties for the object
            PropertyInfo[] properties = type.GetProperties();

            // Loop through the properties and add the non-hidden properties
            // to the list. Non-hidden properties are identified with the 
            // help of HideProperty attribute.
            foreach (PropertyInfo property in properties)
            {
                // If hidden property move to next.
                DropDownItem item = GetAttribute
                    (property, typeof(DropDownItemAttribute));

                if (item.DisplayText == null || item.DisplayText == string.Empty)
                    continue;

                item.ValueText = property.GetValue(data, null);

                if (items == null)
                    items = new List<DropDownItem>();

                items.Add(item);
            }

            return items;
        }

        private DropDownItem GetAttribute(PropertyInfo property, Type type)
        {
            // Retrieve all the custom attributes of type HideProperty
            // that have been applied to this property.
            object[] attributes = property.GetCustomAttributes
                (type, true);

            if (attributes.Length > 0)
            {
                return new DropDownItem(
                    (attributes[0] as DropDownItemAttribute).DisplayText,
                    (attributes[0] as DropDownItemAttribute).ValueText);
            }
            else
            {
                return new DropDownItem(string.Empty, string.Empty);
            }
        }
    }
}
