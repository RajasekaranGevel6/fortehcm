﻿namespace ReflectionComboItem
{
    public class DropDownItem
    {
        private string displayText = null;
        private object valueText = null;

        public DropDownItem()
        { 

        }

        public DropDownItem(string displayText, string valueText)
        {
            this.displayText = displayText;
            this.valueText = valueText;
        }

        public string DisplayText
        {
            get
            {
                return displayText;
            }
            set
            {
                displayText = value;
            }
        }

        public object ValueText
        {
            get
            {
                return valueText;
            }
            set
            {
                valueText = value;
            }
        }
    }
}
