﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Constants.cs
// File that defines the various constants used across the application.
// Constants are grouped under various categories.

#endregion

namespace Forte.HCM.Support
{
    /// <summary>
    /// Class that holds the various constants used across the application.
    /// Inner classes are used to group constant categories.
    /// </summary>
    public sealed class Constants
    {
        #region Excel Export

        /// <summary>
        /// Class that holds the excel export constants.
        /// </summary>
        public sealed class ExcelExport
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ExcelExport()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds starting cell.
            /// </summary>
            public const string STARTING_CELL = "A1";
        }

        #endregion Excel Export

        #region Excel Export File Name Constants

        /// <summary>
        /// Class that holds the excel export file name constants.
        /// </summary>
        public sealed class ExcelExportFileName
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ExcelExportFileName()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the type candidate activity log
            /// </summary>
            public const string CANDIDATE_ACTIVITY_LOG = "CandidateActivityLog";

            /// <summary>
            /// A <see cref="string"/> that holds the type candidate details
            /// </summary>
            public const string CANDIDATE_DETAILS = "CandidateDetails";

            /// <summary>
            /// A <see cref="string"/> that holds the type position profile activity log
            /// </summary>
            public const string POSITION_PROFILE_ACTIVITY_LOG = "PositionProfileActivityLog";
        }

        #endregion Excel Export File Name Constants

        #region Excel Export Sheet Name Constants

        /// <summary>
        /// Class that holds the excel export sheet name constants.
        /// </summary>
        public sealed class ExcelExportSheetName
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ExcelExportSheetName()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the type candidate activity log
            /// </summary>
            public const string CANDIDATE_ACTIVITY_LOG = "Candidate Activity Log";

            /// <summary>
            /// A <see cref="string"/> that holds the type candidate details.
            /// </summary>
            public const string CANDIDATE_DETAILS = "Candidate Details";

            /// <summary>
            /// A <see cref="string"/> that holds the type position profile activity log
            /// </summary>
            public const string POSITION_PROFILE_ACTIVITY_LOG = "Position Profile Activity Log";

            /// <summary>
            /// A <see cref="string"/> that holds the type user details
            /// </summary>
            public const string USER_DETAILS = "User Details";
        }

        #endregion Excel Export Sheet Name Constants

        #region Excel Export Formats

        /// <summary>
        /// Class that holds the excel export format constants.
        /// </summary>
        public sealed class ExcelExportFormats
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ExcelExportFormats()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the date format.
            /// </summary>
            public const string DATE = "MM/dd/yyyy";

            /// <summary>
            /// A <see cref="string"/> that holds the comma decimal format.
            /// </summary>
            public const string DECIMAL_COMMA = "C";
        }

        #endregion Excel Export Formats

        #region Candidate Activity Log Constants

        /// <summary>
        /// Class that holds the candidate activity log constants.
        /// </summary>
        public sealed class CandidateActivityLogType
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CandidateActivityLogType()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the type 'candidate created through 
            /// resume uploader utility'.
            /// </summary>
            public const string CANDIDATE_CREATED_THROUGH_RESUME_UPLOADER = "CA_CR_RU";

            /// <summary>
            /// A <see cref="string"/> that holds the type 'candidate created through 
            /// web based system'.
            /// </summary>
            public const string CANDIDATE_CREATED_THROUGH_WEB_BASED_SYSTEM = "CA_CR_WBS";

            /// <summary>
            /// A <see cref="string"/> that holds the type 'resume uploaded through
            /// web based system'.
            /// </summary>
            public const string RESUME_UPLOADED_THROUGH_WEB_BASED_SYSTEM_ = "CA_RSU_WBS";

            /// <summary>
            /// A <see cref="string"/> that holds the type 'candidate scheduled'.
            /// </summary>
            public const string CANDIDATE_SCHEDULED = "CA_SCH";

            /// <summary>
            /// A <see cref="string"/> that holds the type 'candidate unscheduled'.
            /// </summary>
            public const string CANDIDATE_UNSCHEDULED = "CA_USCH";

            /// <summary>
            /// A <see cref="string"/> that holds the type 'candidate rescheduled'.
            /// </summary>
            public const string CANDIDATE_RESCHEDULED = "CA_RSCH";

            /// <summary>
            /// A <see cref="string"/> that holds the type 'notes added'.
            /// </summary>
            public const string NOTES_ADDED = "CA_NOTES";

            /// <summary>
            /// A <see cref="string"/> that holds the type 'candidate associated to candidate'.
            /// </summary>
            public const string CANDIDATE_ASSOCIATED_POSITION_PROFILE = "CA_PP_AS";

            /// <summary>
            /// A <see cref="string"/> that holds the type 'candidate Test Completed'.
            /// </summary>
            public const string CANDIDATE_TEST_COMPLETED_STATUS = "CA_COMP   ";
        }

        #endregion Candidate Activity Log Constants

        #region Certifcate Validity Type

        /// <summary>
        /// Class that holds the certificate validity type constants.
        /// </summary>
        public sealed class CertifcateValidityType
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CertifcateValidityType()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the 12 months validity 
            /// of the certificate.
            /// </summary>
            public const string CERTIFICATE_VALID_12MONTHS = "12MONTH";

            /// <summary>
            /// A <see cref="string"/> that holds the 18 months validity 
            /// of the certificate.
            /// </summary>
            public const string CERTIFICATE_VALID_18MONTHS = "18MONTH";

            /// <summary>
            /// A <see cref="string"/> that holds the 24 months validity 
            /// of the certificate.
            /// </summary>
            public const string CERTIFICATE_VALID_24MONTHS = "24MONTH";

            /// <summary>
            /// A <see cref="string"/> that holds the 30 months validity 
            /// of the certificate.
            /// </summary>
            public const string CERTIFICATE_VALID_30MONTHS = "30MONTH";

            /// <summary>
            /// A <see cref="string"/> that holds the 6 months validity 
            /// of the certificate.
            /// </summary>
            public const string CERTIFICATE_VALID_6MONTHS = "6MONTH";
        }

        #endregion Certifcate Validity Type

        #region Private Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <remarks>
        /// Defined as private to avoid initialization.
        /// </remarks>
        private Constants()
        {
        }

        #endregion Private Constructor

        #region General Constants

        /// <summary>
        /// Class that holds the general constants.
        /// </summary>
        public sealed class General
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private General()
            {
            }

            /// <summary>
            /// A <see cref="int"/> that holds the default page size.
            /// </summary>
            public const int DEFAULT_PAGE_SIZE = 10;

            /// <summary>
            /// A <see cref="int"/> that holds the time slot interval minutes.
            /// </summary>
            /// <remarks>
            /// This is applicable for assessor management.
            /// </remarks>
            public const int TIME_SLOT_INTERVAL_MINUTES = 30;

            /// <summary>
            /// 
            /// </summary>
            public const int DEFAULT_TEST_MINIMUM_TIME = 300;

            /// <summary>
            /// A <see cref="int"/> that holds the default page size.
            /// </summary>
            public const int DEFAULT_DASHBOARD_PAGE_SIZE = 1;

            /// <summary>
            /// A <see cref="string"/>that holds the company shown in the email 
            /// subject line.
            /// </summary>
            public const string EMAIL_SUBJECT_COMPANY_NAME = "ForteHCM";
        }

        #endregion General Constants

        #region Attribute Type Constants

        /// <summary>
        /// Class that holds the attribute type constants.
        /// </summary>
        public sealed class AttributeTypes
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private AttributeTypes()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the candidate attempt 
            /// status attribute type.
            /// </summary>
            public const string CANDIDATE_ATTEMPT_STATUS = "ATMPT_STAT";

            /// <summary>
            /// A <see cref="string"/> that holds the certification attribute
            /// type.
            /// </summary>
            public const string CERTIFICATE_TYPE = "CERT_ID";

            /// <summary>
            /// A <see cref="string"/> that holds the complexity attribute 
            /// type.
            /// </summary>
            public const string COMPLEXITY = "COMP";

            /// <summary>
            /// A <see cref="string"/> that holds the question attended 
            /// status attribute type.
            /// </summary>
            public const string QUESTION_ATTENDED_STATUS = "QUES_STAT";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate session status
            /// attribute type.
            /// </summary>
            public const string CANDIDATE_SESSION_STATUS = "SESS_STAT";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate activity status
            /// attribute type.
            /// </summary>
            public const string CANDIDATE_ACTIVITY = "CAND_ACTI";

            /// <summary>
            /// A <see cref="string"/> that holds the test area attribute 
            /// type.
            /// </summary>
            public const string TEST_AREA = "TEST_AREA";

            /// <summary>
            /// A <see cref="string"/> that holds the test reminder attribute
            /// type.
            /// </summary>
            public const string TEST_REMINDER = "TEST_REM";

            /// <summary>
            /// A <see cref="string"/> that holds the crdit 
            /// type.
            /// </summary>
            public const string CREDIT_TEST_SESSION = "CR_TSN";


            public const string POSITIOPROFILE_ACTIVITY = "PP_ACTI";

            /// <summary>
            /// A <see cref="string"/> that holds the resume validate expresssion 
            /// type.
            /// </summary>
            public const string RESUME_VALIDATE_EXPRESSION = "RSM_VLD_EX";

            /// <summary>
            /// A <see cref="string"/> that holds the resume duplication expresssion 
            /// type.
            /// </summary>
            public const string RESUME_DUPLICATE_EXPRESSION = "RSM_DUP_EX";

            /// <summary>
            /// A <see cref="string"/> that holds the resume duplication expresssion option
            /// type.
            /// </summary>
            public const string RESUME_DUPLICATE_EXPRESSION_OPTION = "RSM_DUP_OP";

            /// <summary>
            /// A <see cref="string"/> that holds the position profile status 
            /// type.
            /// </summary>
            public const string POSITION_PROFILE_STATUS = "PP_STATUS";

            /// <summary>
            /// A <see cref="string"/> that holds the skill level 
            /// </summary>
            public const string SKILL_LEVEL = "SKL_LE_TYP";
        }

        #endregion Attribute Type Constants

        #region Candidate Session Status Constants

        /// <summary>
        /// Class that holds the candidate session status constants.
        /// </summary>
        public sealed class CandidateSessionStatus
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CandidateSessionStatus()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the cancelled status.
            /// </summary>
            public const string CANCELLED = "SESS_CANL";

            /// <summary>
            /// A <see cref="string"/> that holds the completed status.
            /// </summary>
            public const string COMPLETED = "SESS_COMP";

            /// <summary>
            /// A <see cref="string"/> that holds the elapsed status.
            /// </summary>
            public const string ELAPSED = "SESS_ELPS";

            /// <summary>
            /// A <see cref="string"/> that holds the in-progress status.
            /// </summary>
            public const string IN_PROGRESS = "SESS_INPR";

            /// <summary>
            /// A <see cref="string"/> that holds the not-scheduled status.
            /// </summary>
            public const string NOT_SCHEDULED = "SESS_NSCHD";

            /// <summary>
            /// A <see cref="string"/> that holds the quit status.
            /// </summary>
            public const string QUIT = "SESS_QUIT";

            /// <summary>
            /// A <see cref="string"/> that holds the scheduled status.
            /// </summary>
            public const string SCHEDULED = "SESS_SCHD";

            /// <summary>
            /// A <see cref="string"/> that holds the end status.
            /// </summary>
            public const string ENDED = "SESS_END";

            /// <summary>
            /// A <see cref="string"/> that holds the end status.
            /// </summary>
            public const string PAUSED = "SESS_PAUS";

            /// <summary>
            /// A <see cref="string"/> that holds the evaluated status.
            /// </summary>
            public const string EVALUATED = "SESS_EVAL";
        }

        #endregion Candidate Session Status Constants

        #region Candidate Attempt Status Constants

        /// <summary>
        /// Class that holds the candidate attempt status constants.
        /// </summary>
        public sealed class CandidateAttemptStatus
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CandidateAttemptStatus()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the cancelled status.
            /// </summary>
            public const string CANCELLED = "ATMPT_CANL";

            /// <summary>
            /// A <see cref="string"/> that holds the completed status.
            /// </summary>
            public const string COMPLETED = "ATMPT_COMP";

            /// <summary>
            /// A <see cref="string"/> that holds the elapsed status.
            /// </summary>
            public const string ELAPSED = "ATMPT_ELPS";

            /// <summary>
            /// A <see cref="string"/> that holds the in-progress status.
            /// </summary>
            public const string IN_PROGRESS = "ATMPT_INPR";

            /// <summary>
            /// A <see cref="string"/> that holds the not-scheduled status.
            /// </summary>
            public const string NOT_SCHEDULED = "ATMPT_NSCH";

            /// <summary>
            /// A <see cref="string"/> that holds the quit status.
            /// </summary>
            public const string QUIT = "ATMPT_QUIT";

            /// <summary>
            /// A <see cref="string"/> that holds the scheduled status.
            /// </summary>
            public const string SCHEDULED = "ATMPT_SCHD";

            /// <summary>
            /// A <see cref="string"/> that holds the cyber proctoring 
            /// initiated status.
            /// </summary>
            public const string CYBER_PROC_INIT = "ATMPT_CPIN";
        }

        #endregion Candidate Attempt Status Constants

        #region Question Attended Status Constants

        /// <summary>
        /// Class that holds the question attended status constants.
        /// </summary>
        public sealed class QuestionAttendedStatus
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private QuestionAttendedStatus()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the completed status.
            /// </summary>
            public const string COMPLETED = "COMPLETED";

            /// <summary>
            /// A <see cref="string"/> that holds the not-attended status.
            /// </summary>
            public const string NOT_ATTENDED = "NOT_ATTD";

            /// <summary>
            /// A <see cref="string"/> that holds the quit status.
            /// </summary>
            public const string QUIT = "QUIT";

            /// <summary>
            /// A <see cref="string"/> that holds the skipped status.
            /// </summary>
            public const string SKIPPED = "SKIPPED";

            /// <summary>
            /// A <see cref="string"/> that holds the timeout status.
            /// </summary>
            public const string TIMEOUT = "TIMEOUT";
        }

        #endregion Question Attended Status Constants

        #region Object Type Constants

        /// <summary>
        /// Class that holds the object type constants.
        /// </summary>
        public sealed class ObjectTypes
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ObjectTypes()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the question object type.
            /// </summary>
            public const string QUESTION = "QSN";

            /// <summary>
            /// A <see cref="string"/> that holds the test object type.
            /// </summary>
            public const string TEST = "TST";

            /// <summary>
            /// A <see cref="string"/> that holds the test session object type.
            /// </summary>
            public const string TEST_SESSION = "TSN";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate test session
            /// object type.
            /// </summary>
            public const string CANDIDATE_TEST_SESSION = "CTS";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate interview session
            /// object type.
            /// </summary>
            public const string CANDIDATE_INTERVIEW_SESSION = "COI";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate question result
            /// object type.
            /// </summary>
            public const string CANDIDATE_QUESTION_RESULT = "CQR";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate test result
            /// object type.
            /// </summary>
            public const string CANDIDATE_TEST_RESULT = "CTR";
        }

        #endregion Object Type Constants

        #region Config Constants

        /// <summary>
        /// Class that holds the config constants.
        /// </summary>
        public sealed class ConfigConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ConfigConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the general config section.
            /// </summary>
            public const string CONST_CONFIG_GENERAL = "HCMAppSettings/HCMGeneral";

            /// <summary>
            /// A <see cref="string"/> that holds the connection strings config
            /// section.
            /// </summary>
            public const string CONST_CONNSTR_SECTION = "connectionStrings";
        }

        #endregion Config Constants

        #region Session Constants

        /// <summary>
        /// Class that holds the session constants.
        /// </summary>
        public sealed class SessionConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private SessionConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the grid page size session 
            /// key.
            /// </summary>
            public const string GRID_PAGE_SIZE = "GRID_PAGE_SIZE";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate results PDF
            /// session key.
            /// </summary>
            public const string CANDIDATE_RESULTS_PDF = "CANDIDATE_RESULTS_PDF";

            /// <summary>
            /// A <see cref="string"/> that holds the resume source to download
            /// </summary>
            public const string DOWNLOAD_RESUME_CONTENT = "DOWNLOAD_RESUME_CONTENT";

            /// <summary>
            /// A <see cref="string"/> that holds the Report results PDF
            /// session key.
            /// </summary>
            public const string REPORT_RESULTS_PDF = "REPORT_RESULTS_PDF";

            /// <summary>
            /// A <see cref="string"/> that holds the certificate image
            /// </summary>
            public const string CERTIFICATE_IMAGE = "CERTIFICATE_IMAGE";

            /// <summary>
            /// A <see cref="string"/> that holds the certificate template image
            /// </summary>
            public const string CERTIFICATE_TEMPLATE_IMAGE = "CERTIFICATE_TEMPLATE_IMAGE";

            /// <summary>
            /// A <see cref="string"/> that holds the certificate image as body content
            /// </summary>
            public const string CERTIFICATE_IMAGE_URL = "CERTIFICATE_IMAGE_URL";

            /// <summary>
            /// A <see cref="string"/> that holds the certificate html content.
            /// </summary>
            public const string CERTIFICATE_HTML_IMAGE = "CERTIFICATE_HTML_IMAGE";

            /// <summary>
            /// A <see cref="string"/> that holds the preview form segments details
            /// </summary>
            public const string PREVIEW_FORM_SEGMENTS = "PREVIEW_FORM_SEGMENTS";

            public const string COMMON_NOMENCLATURE = "COMMON_NOMENCLATURE";

            public const string ALL_NOMENCLATURE = "ALL_NOMENCLATURE";

            public const string IS_CLIENT_MANAGEMENT_DELETE = "IS_CLIENT_MANAGEMENT_DELETE";

            public const string PP_EMAIL_CONTENT = "PP_EMAIL_CONTENT";

            public const string PP_PDF_CONTENT = "PP_PDF_CONTENT";
            public const string PP_PDF_HEADER = "PP_PDF_HEADER";

            public const string PP_STATUS_EMAIL_CONTENT = "PP_STATUS_EMAIL_CONTENT";

            public const string PP_SELECTED_CANDIATE_ID = "PP_SELECTED_CANDIATE_ID";



        }

        #endregion Session Constants

        #region Parent Page Constants

        /// <summary>
        /// Class that holds the parent page key constants.
        /// </summary>
        public sealed class ParentPage
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ParentPage()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds key for menu.
            /// </summary>
            public const string MENU = "MENU";

            /// <summary>
            /// A <see cref="string"/> that holds key for search question page.
            /// </summary>
            public const string SEARCH_QUESTION = "S_QSN";

            /// <summary>
            /// A <see cref="string"/> that holds key for search question page.
            /// </summary>
            public const string SEARCH_INTERVIEW_QUESTION = "S_IQS";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test page.
            /// </summary>
            public const string SEARCH_TEST = "S_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test 
            /// recommendation page.
            /// </summary>
            public const string SEARCH_TEST_RECOMMENDATION = "S_TST_REC";

            /// <summary>
            /// A <see cref="string"/> that holds the key for test recommendation 
            /// summary page.
            /// </summary>
            public const string TEST_RECOMMENDATION_SUMMARY = "TST_REC_SUM";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search interview page.
            /// </summary>
            public const string SEARCH_INTERVIEW = "S_INT";

            /// <summary>
            /// A <see cref="string"/> that holds the key for create interview page.
            /// </summary>
            public const string CREATE_INTERVIEW = "CRE_INT";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate search 
            /// test page.
            /// </summary>
            public const string CANDIDATE_SEARCH_TEST = "CS_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate self 
            /// admin test page.
            /// </summary>
            public const string CANDIDATE_SELF_ADMIN_TEST = "CSA_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate 
            /// activities  page.
            /// </summary>
            public const string CANDIDATE_ACTIVITIES = "C_ACTI";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate 
            /// test results  page.
            /// </summary>
            public const string CANDIDATE_TEST_RESULTS = "C_TST_RST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test
            /// session page.
            /// </summary>
            public const string SEARCH_TEST_SESSION = "S_TSN";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search interview
            /// session page.
            /// </summary>
            public const string SEARCH_INTERVIEW_TEST_SESSION = "S_ITSN";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search online
            /// interview session page.
            /// </summary>
            public const string SEARCH_ONLINE_INTERVIEW_SESSION = "S_OITSN";

            /// <summary>
            /// A <see cref="string"/> that holds the key for my tests page.
            /// </summary>
            public const string MY_TESTS = "MY_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for my interviews page.
            /// </summary>
            public const string MY_INTERVIEWS = "MY_INT";

            /// <summary>
            /// A <see cref="string"/> that holds the key for my availability page.
            /// </summary>
            public const string MY_AVAILABILITY = "MY_AVB";

            /// <summary>
            /// A <see cref="string"/> that holds the key for schedule 
            /// candidate page.
            /// </summary>
            public const string SCHEDULE_CANDIDATE = "SCH_CAND";

            /// <summary>
            /// A <see cref="string"/> that holds the key for interview schedule 
            /// candidate page.
            /// </summary>
            public const string INTERVIEW_SCHEDULE_CANDIDATE = "ISCH_CAND";

            /// <summary>
            /// A <see cref="string"/> that holds the key for test introduction
            /// page.
            /// </summary>
            public const string TEST_INTRODUCTION = "TST_INTRO";

            /// <summary>
            /// A <see cref="string"/> that holds the key for interview introduction
            /// page.
            /// </summary>
            public const string INTERVIEW_INTRODUCTION = "INT_INTRO";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate home
            /// page.
            /// </summary>
            public const string CANDIDATE_HOME = "CAND_HOME";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate 
            /// sign in .
            /// </summary>
            public const string CANDIDATE_SIGN_IN = "C_SIGNIN";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate 
            /// dashboard page.
            /// </summary>
            public const string CANDIDATE_DASHBOARD = "C_DASHBOARD";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate edit 
            /// profile page.
            /// </summary>
            public const string CANDIDATE_EDIT_PROFILE = "C_EDIT_PROFILE";

            /// <summary>
            /// A <see cref="string"/> that holds the key for view test page
            /// </summary>
            public const string VIEW_TEST = "V_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for view test page
            /// </summary>
            public const string VIEW_INTERVIEW_TEST = "V_INT_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for view test page
            /// </summary>
            public const string CREATE_MANUAL_TEST = "CM_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for view test page
            /// </summary>
            public const string EDIT_TEST = "E_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for edit interview test page
            /// </summary>
            public const string EDIT_INTERVIEW_TEST = "E_INT_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for edit interview test page
            /// </summary>
            public const string EDIT_ADAPTIVEINTERVIEW_TEST = "E_ADPTINT_TST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for test report page.
            /// </summary>
            public const string TEST_REPORT = "T_REP";

            /// <summary>
            /// A <see cref="string"/> that holds the key for interview test report page.
            /// </summary>
            public const string INTERVIEW_TEST_REPORT = "INT_T_REP";

            /// <summary>
            /// A <see cref="string"/> that holds the key for test statistics
            /// information page.
            /// </summary>
            public const string TEST_STATISTICS_INFO = "T_STAT";

            /// <summary>
            /// A <see cref="string"/> that holds the key for interview test statistics
            /// information page.
            /// </summary>
            public const string INTERVIEW_TEST_STATISTICS_INFO = "INT_T_STAT";

            /// <summary>
            /// A <see cref="string"/> that holds the key for interview test automatic interview test
            /// information page.
            /// </summary>
            public const string INTERVIEW_TEST_CREATE_AUTO_QUESTIONS = "INT_T_AOTO_QUST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for interview test manual question key
            /// information page.
            /// </summary>
            public const string INTERVIEW_TEST_CREATE_MANU_QUESTIONS = "INT_T_MANU_QUST";

            /// <summary>
            /// A <see cref="string"/> that holds the key for test scheduler page.
            /// </summary>
            public const string TEST_SCHEDULER = "TST_SCHD";

            /// <summary>
            /// A <see cref="string"/> that holds the key for interview test scheduler page.
            /// </summary>
            public const string INTERVIEW_TEST_SCHEDULER = "INT_TST_SCHD";

            /// <summary>
            /// A <see cref="string"/> that holds the key for enroll assessor page.
            /// </summary>
            public const string ENROLL_ASSESSOR = "E_ASS";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate search 
            /// test page.
            /// </summary>
            public const string CANDIDATE_REPORT = "C_REP";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate search 
            /// test page.
            /// </summary>
            public const string CANDIDATE_ADAPTIVE_SUMMARY = "C_ADP_SUM";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate search 
            /// test page.
            /// </summary>
            public const string CANDIDATE_ADAPTIVE_TEST_RECOMMENDATION = "C_ADP_TST_REC";

            /// <summary>
            /// a<see cref="string"/>that holds the key for the search
            /// position profile form 
            /// </summary>
            public const string SEARCH_POSITION_PROFILE_FORM = "S_POS_PRO_FORM";

            /// <summary>
            /// a<see cref="string"/>that holds the key for the search
            /// position profile
            /// </summary>
            public const string SEARCH_POSITION_PROFILE = "S_POS_PRO";

            /// <summary>
            /// Create / Edit Poistion Profile.
            /// </summary>
            public const string POSITION_PROFILE = "C_PP";

            /// <summary>
            /// Poistion profile review.
            /// </summary>
            public const string POSITION_PROFILE_REVIEW = "PP_REVIEW";

            /// <summary>
            /// Poistion profile workflow.
            /// </summary>
            public const string POSITION_PROFILE_WORKFLOW = "PP_WORKFLOW";

            /// <summary>
            /// A <see cref="System.String"/> that holds the value
            /// of view account parent page value
            /// </summary>
            public const string VIEW_ACCOUNT = "V_ACC";

            /// <summary>
            /// A <see cref="System.String"/> that holds the value
            /// of edit account parent page value
            /// </summary>
            public const string EDIT_ACCOUNT = "E_ACC";

            /// <summary>
            /// A <see cref="System.String"/> that holds the value
            /// of upgrade account parent page value
            /// </summary>
            public const string UPGRADE_ACCOUNT = "U_ACC";

            /// <summary>
            /// A <see cref="System.String"/> that holds the value
            /// of search customer page value
            /// </summary>
            public const string SEARCH_CUSTOMER = "SRCH_CUST";

            /// <summary>
            /// A <see cref="System.String"/> that holds the value
            /// of upgrade account parent page value
            /// </summary>
            public const string EDIT_FORM = "E_FRM";

            /// <summary>
            /// A <see cref="System.String"/> that holds the value
            /// of search careerbuilder resume parent page value
            /// </summary>
            public const string SEARCH_CAREER_BUILDER_RESUME = "S_CB_R";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search candidate report.
            /// </summary>
            public const string SEARCH_CANDIDATE = "SRCH_CAND";

            /// <summary>
            /// A <see cref="string"/> that holds the key for review resume.
            /// </summary>
            public const string REVIEW_RESUME = "REVIEW_RESUME";

            /// <summary>
            /// A <see cref="string"/> that holds the key for upload resume.
            /// </summary>
            public const string UPLOAD_RESUME = "UPLOAD_RESUME";

            /// <summary>
            /// A <see cref="string"/> that holds the key for the user dashboard.
            /// </summary>
            public const string USER_DASHBOARD = "U_DASHBOARD";

            /// <summary>
            /// a<see cref="string"/> that holds the key for the major work flow.
            /// </summary>
            public const string WORKFLOW_MAJOR = "WF_MAJOR";

            /// <summary>
            /// a<see cref="string"/> that holds the key for the landing page.
            /// </summary>
            public const string WORKFLOW_LANDING = "WF_LAND";

            /// <summary>
            /// a<see cref="string"/> that holds the key for the assessment 
            /// workflow.
            /// </summary>
            public const string WORKFLOW_ASSESSMENT = "WF_ASSESS";

            /// <summary>
            /// a<see cref="string"/> that holds the key for the interview 
            /// workflow.
            /// </summary>
            public const string WORKFLOW_INTERVIEW = "WF_INTERVIEW";

            /// <summary>
            /// A <see cref="string"/> that holds the key for the assessment 
            /// ClientManagement.
            /// </summary>
            public const string CLIENT_MANAGEMENT = "CLNT_MANAGE";

            /// <summary>
            /// A <see cref="string"/> that holds the key for the candidate 
            /// activity home.
            /// </summary>
            public const string ACTIVITY_HOME = "ACT_HOME";

            /// <summary>
            /// A <see cref="string"/> that holds the key for the candidate 
            /// activity home.
            /// </summary>
            public const string POSITION_PROFILE_STATUS = "PP_STATUS";

            /// <summary>
            /// A <see cref="string"/> that holds the key for the edit interview session 
            /// </summary>
            public const string EDIT_INTERVIEW_SESSION = "E_INT_S";

            public const string TALENT_SCOUT = "TS_HOME";

            public const string CORPORATE_ADMIN = "COR_ADMIN";

            public const string MY_ASSESSMENT = "MY_ASMT";

            public const string EDIT_ASSESSOR = "EDIT_ASS";

            /// <summary>
            /// A <see cref="string"/> that holds the key for online assessor page.
            /// </summary>
            public const string ONLINE_ASSESSOR = "ONLINE_ASS";

        }

        #endregion Parent Page Constants

        #region Candidate Test Type Constants

        /// <summary>
        /// Class that holds the candidate test type constants.
        /// </summary>
        public sealed class CandidateTestTypes
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CandidateTestTypes()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the pending type.
            /// </summary>
            public const string PENDING = "PENDING";

            /// <summary>
            /// A <see cref="string"/> that holds the completed type.
            /// </summary>
            public const string COMPLETED = "COMPLETED";

            /// <summary>
            /// A <see cref="string"/> that holds the expired type.
            /// </summary>
            public const string EXPIRED = "EXPIRED";
        }

        #endregion Candidate Test Type Constants

        #region Sort Type Constants

        /// <summary>
        /// Class that holds the sort type constants.
        /// </summary>
        public sealed class SortTypeConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private SortTypeConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the ascending order type.
            /// </summary>
            public const string ASCENDING = "A";

            /// <summary>
            /// A <see cref="string"/> that holds the descending order type.
            /// </summary>
            public const string DESCENDING = "D";
        }


        #endregion Sort Type Constants

        #region Disclaimer Message Constants

        /// <summary>
        /// Class that holds the disclaimer message constants.
        /// </summary>
        public sealed class DisclaimerMessageConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private DisclaimerMessageConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the save question type.
            /// </summary>
            public const string SAVE_QUESTION = "QSN_SAVE";

            /// <summary>
            /// A <see cref="string"/> that holds the test introduction type.
            /// </summary>
            public const string TEST_INTRODUCTION = "TEST_INTRO";

            /// <summary>
            /// A <see cref="string"/> that holds the hardware instructions 
            /// type.
            /// </summary>
            public const string HARDWARE_INSTRUCTIONS = "HRDWAR_INS";

            /// <summary>
            /// A <see cref="string"/> that holds the test instructions 
            /// type.
            /// </summary>
            public const string TEST_INSTRUCTIONS = "TEST_INS";

            /// <summary>
            /// A <see cref="string"/> that holds the cyper proctoring
            /// intructions type.
            /// </summary>
            public const string CYBER_PROCTORING_INSTRUCTIONS = "CYBER_INS";

            /// <summary>
            /// A <see cref="string"/> that holds the test warning intructions 
            /// type.
            /// </summary>
            public const string TEST_WARNING_INSTRUCTIONS = "WARN_INS";

        }

        #endregion Disclaimer Message Constants

        #region Options Default Values Constants

        /// <summary>
        /// Class that holds the options default values constants.
        /// </summary>
        public sealed class OptionsDefaultValuesConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private OptionsDefaultValuesConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// computation factor.
            /// </summary>
            public const string COMPUTATION_FACTOR = "0.10";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// complexity type simple.
            /// </summary>
            public const string SIMPLE_COMPLEXITY = "1.00";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// complexity type medium.
            /// </summary>
            public const string MEDIUM_COMPLEXITY = "1.50";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// complexity type complex.
            /// </summary>
            public const string COMPLEX_COMPLEXITY = "2.00";
        }

        #endregion Options Default Values Constants

        #region Role Code Constants

        /// <summary>
        /// Class that holds the role code constants.
        /// </summary>
        public sealed class RoleCodeConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private RoleCodeConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the question author role type.
            /// </summary>
            public const string CONTENT_AUTHOR = "HCMR01";

            /// <summary>
            /// Content validator role type
            /// </summary>
            public const string CONTENT_VALIDATOR = "HCMR02";

            /// <summary>
            /// A <see cref="string"/> that holds the test author role type.
            /// </summary>
            public const string TEST_AUTHOR = "HCMR03";

            /// <summary>
            /// A <see cref="string"/> that holds the DELIVERY MANAGER role type.
            /// </summary>
            public const string DELIVERY_MANAGER = "HCMR04";

            /// <summary>
            /// A <see cref="string"/> that holds the 
            /// </summary>
            public const string RECRUITER = "HCMR05";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate role type.
            /// </summary>
            public const string CANDIDATE = "HCMR06";

            /// <summary>
            /// A <see cref="string"/> that holds the candidate role type.
            /// </summary>
            public const string ADMIN_USERSETUP = "HCMR07";

            /// <summary>
            /// A <see cref="string"/> that holds the Admin (OTM/TS management)  role type.
            /// </summary>
            public const string ADMIN_OTM_TS = "HCMR08";

            /// <summary>
            /// A <see cref="string"/> that holds the Admin (credits management)  role type.
            /// </summary>
            public const string ADMIN_CRD_MGMT = "HCMR09";

            /// <summary>
            /// A <see cref="string"/> that holds the Admin (candidate management)  role type.
            /// </summary>
            public const string ADMIN_CAND_MGMT = "HCMR10";

            /// <summary>
            /// A <see cref="string"/> that holds the administrator role type.
            /// </summary>
            public const string ADMINISTRATOR = "HCMR11";

            /// <summary>
            /// A <see cref="string"/> that holds the free subscription type.
            /// </summary>
            public const string FREE_SUBS = "HCMR12";

            /// <summary>
            /// A <see cref="string"/> that holds the standard subscription type.
            /// </summary>
            public const string STANDARD_SUBS = "HCMR13";

            /// <summary>
            /// A <see cref="string"/> that holds the corporate subscription type (admin).
            /// </summary>
            public const string CORPORATE_SUBS_ADMIN = "HCMR14";

            /// <summary>
            /// A <see cref="string"/> that holds the corporate subscription type (user).
            /// </summary>
            public const string CORPORATE_SUBS_USER = "HCMR15";

            /// <summary>
            /// A <see cref="string"/> that holds the general user.
            /// </summary>
            public const string GENERAL_USER = "HCMR16";

            /// <summary>
            /// A <see cref="string"/> that holds the sales person.
            /// </summary>
            public const string SALES_PERSON = "HCMR17";

            /// <summary>
            /// A <see cref="string"/> that holds the technical specialist.
            /// </summary>
            public const string TECHNICAL_SPECIALIST = "HCMR18";

            /// <summary>
            /// A <see cref="string"/> that holds the recruiter - java.
            /// </summary>
            public const string RECRUITER_JAVA = "HCMR19";

            /// <summary>
            /// A <see cref="string"/> that holds the recruiter - sap.
            /// </summary>
            public const string RECRUITER_SAP = "HCMR20";

            /// <summary>
            /// A <see cref="string"/> that holds the assessor.
            /// </summary>
            public const string ASSESSOR = "ASS01";

            /// <summary>
            /// A <see cref="string"/> that holds the site admin.
            /// </summary>
            public const string SITE_ADMIN = "SITEADMIN";

        }

        #endregion Role Code Constants

        #region Subscription Feature Constants

        /// <summary>
        /// Class that holds the subscription feature constants.
        /// </summary>
        public sealed class FeatureConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private FeatureConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds 'Number of position profiles' feature.
            /// </summary>
            public const int NUMBER_OF_POSITION_PROFILES = 1;

            /// <summary>
            /// A <see cref="string"/> that holds 'Number of candidate searches' feature.
            /// </summary>
            public const int NUMBER_OF_CANDIDATE_SEARCHES = 2;

            /// <summary>
            /// A <see cref="string"/> that holds 'Number of resume downloads' feature.
            /// </summary>
            public const int NUMBER_OF_RESUME_DOWNLOADS = 3;

            /// <summary>
            /// A <see cref="string"/> that holds 'Online storage space' feature.
            /// </summary>
            public const int ONLINE_STORAGE_SPACE = 4;

            /// <summary>
            /// A <see cref="string"/> that holds 'Standard forms for position profile generation' feature.
            /// </summary>
            public const int STANDARD_FORMS_FOR_POSITION_PROFILE_GENERATION = 5;

            /// <summary>
            /// A <see cref="string"/> that holds 'Ability to customize forms for position profile' feature.
            /// </summary>
            public const int CUSTOMIZE_POSITION_PROFILE_FORMS = 6;

            /// <summary>
            /// A <see cref="string"/> that holds 'Parsing owned resumes' feature.
            /// </summary>
            public const int PARSING_OWNED_RESUMES = 7;

            /// <summary>
            /// A <see cref="string"/> that holds 'Job board access' feature.
            /// </summary>
            public const int JOB_BOARD_ACCESS = 8;

            /// <summary>
            /// A <see cref="string"/> that holds 'Ability to download XML records of candidates' feature.
            /// </summary>
            public const int DOWNLOAD_CANDIDATE_XML_RECORDS = 9;

            /// <summary>
            /// A <see cref="string"/> that holds 'Viewing profile images of candidates' feature.
            /// </summary>
            public const int VIEW_CANDIDATE_PROFILE_IMAGES = 10;

            /// <summary>
            /// A <see cref="string"/> that holds 'Viewing reports' feature.
            /// </summary>
            public const int VIEW_REPORTS = 11;

            /// <summary>
            /// A <see cref="string"/> that holds 'Cloning (number of times)' feature.
            /// </summary>
            public const int NUMBER_OF_TIMES_CLONING = 12;

            /// <summary>
            /// A <see cref="string"/> that holds 'Access to central candidate repository' feature.
            /// </summary>
            public const int CENTRAL_CANDIDATE_REPOSITORY_ACCESS = 13;

            /// <summary>
            /// A <see cref="string"/> that holds 'Nature of support (phone/chat/email)' feature.
            /// </summary>
            public const int NATURE_OF_SUPPORT = 14;

            /// <summary>
            /// A <see cref="string"/> that holds 'Branding' feature.
            /// </summary>
            public const int BRANDING = 15;

            /// <summary>
            /// A <see cref="string"/> that holds 'Monthly pricing' feature.
            /// </summary>
            public const int MONTHLY_PRICING = 16;

            /// <summary>
            /// A <see cref="string"/> that holds 'Cost per position profile' feature.
            /// </summary>
            public const int COST_PER_POSITION_PROFILE = 17;

            /// <summary>
            /// A <see cref="string"/> that holds 'Cost per search' feature.
            /// </summary>
            public const int COST_PER_SEARCH = 18;

            /// <summary>
            /// A <see cref="string"/> that holds 'Online test administered' feature.
            /// </summary>
            public const int ONLINE_TEST_ADMINISTERED = 23;

            /// <summary>
            /// A <see cref="string"/> that holds 'Create test' feature.
            /// </summary>
            public const int CREATE_TEST = 24;

            /// <summary>
            /// A <see cref="string"/> that holds 'Create client' alias 
            /// 'Client management' feature.
            /// </summary>
            public const int CREATE_CLIENT = 27;

            /// <summary>
            /// A <see cref="string"/> that holds 'Cyber proctor' feature.
            /// </summary>
            public const int CYBER_PROCTOR = 28;
        }

        #endregion Subscription Feature Constants

        #region Search Criteria Session Key

        /// <summary>
        /// Class that holds the search criteria session key constants.
        /// </summary>
        public sealed class SearchCriteriaSessionKey
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private SearchCriteriaSessionKey()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds key for search question.
            /// </summary>
            public const string SEARCH_QUESTION = "SEARCH_QUESTION_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test.
            /// </summary>
            public const string SEARCH_TEST = "SEARCH_TEST_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test recommendation.
            /// </summary>
            public const string SEARCH_TEST_RECOMMENDATION = "SEARCH_TEST_RECOMMENDATION";


            /// <summary>
            /// A <see cref="string"/> that holds the key for test recommendation summary.
            /// </summary>
            public const string TEST_RECOMMENDATION_SUMMARY = "TEST_RECOMMENDATION_SUMMARY";

            /// <summary>
            /// A <see cref="string"/> that holds the key for candidate search 
            /// test.
            /// </summary>
            public const string CANDIDATE_SEARCH_TEST = "CANDIDATE_SEARCH_TEST_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test
            /// session (by test).
            /// </summary>
            public const string SEARCH_TEST_SESSION_BY_TEST = "SEARCH_TEST_SESSION_BY_TEST_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test
            /// session (by session).
            /// </summary>
            public const string SEARCH_TEST_SESSION_BY_SESSION = "SEARCH_TEST_SESSION_BY_SESSION_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test report.
            /// </summary>
            public const string SEARCH_TEST_REPORT = "SEARCH_TEST_REPORT";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test scheduler.
            /// </summary>
            public const string SEARCH_TEST_SCHEDULER = "SEARCH_TEST_SCHEDULER";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search schedule candidate.
            /// </summary>
            public const string SEARCH_SCHEDULE_CANDIDATE = "SEARCH_SCHEDULE_CANDIDATE";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test statistics info.
            /// </summary>
            public const string SEARCH_TEST_STATISTICS = "SEARCH_TEST_STATISTICS";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search Candidate report.
            /// </summary>
            public const string SEARCH_CANDIDATE_REPORT = "SEARCH_CANDIDATE_REPORT";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search Candidate report.
            /// </summary>
            public const string SEARCH_POSITION_PROFILE_FORM = "SEARCH_POSITION_PROFILE_FORM";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search Candidate report.
            /// </summary>
            public const string SEARCH_POSITION_PROFILE = "SEARCH_POSITION_PROFILE";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search customer report.
            /// </summary>
            public const string SEARCH_CUSTOMER = "SEARCH_CUSTOMER";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search customer report.
            /// </summary>
            public const string SEARCH_CANDIDATE = "SEARCH_CANDIDATE";

            /// <summary>
            /// A <see cref="string"/> that holds key for Client Management.
            /// </summary>
            public const string SEARCH_CLIENT_MANAGEMENT = "SEARCH_CLIENT_MANAGEMENT";

            /// <summary>
            /// A <see cref="string"/> that holds key for Position Profile Status.
            /// </summary>
            public const string POSITION_PROFILE_STATUS = "POSITION_PROFILE_STATUS";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test.
            /// </summary>
            public const string SEARCH_INTERVIEW_TEST = "SEARCH_INTERVIEW_TEST_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search online interview.
            /// </summary>
            public const string SEARCH_ONLINE_INTERVIEW = "SEARCH_ONLINE_INTERVIEW";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search test.
            /// </summary>
            public const string SEARCH_ASSESOR_ASSESSMENT = "SEARCH_ASSESOR_ASSESSMENT_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search interview
            /// session (by interview).
            /// </summary>
            public const string SEARCH_INTERVIEW_SESSION_BY_INTERVIEW = "SEARCH_INTERVIEW_SESSION_BY_INTERVIEW_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search online interview
            /// session (by online interview).
            /// </summary>
            public const string SEARCH_ONLINE_INTERVIEW_SESSION_BY_ONLINE_INTERVIEW = "SEARCH_ONLINE_INTERVIEW_SESSION_BY_ONLINE_INTERVIEW_CRITERIA";

            /// <summary>
            /// A <see cref="string"/> that holds the key for search candidate resume details.
            /// </summary>
            public const string SEARCH_CANDIDATE_REVIEW_RESUMES = "SEARCH_CANDIDATE_REVIEW_RESUMES";

            /// <summary>
            /// A <see cref="string"/> that holds the key for resume uploader page.
            /// </summary>
            public const string RESUME_UPLOADER_SELECTED_CANDIDATE = "RESUME_UPLOADER_SELECTED_CANDIDATE";
        }

        #endregion Search Criteria Session Key

        #region Dashboard Constants

        /// <summary>
        /// Class that holds the Dashboard constants.
        /// </summary>
        public sealed class DashboardConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private DashboardConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds theDesign Report.
            /// </summary>
            public const string DESIGN_REPORT = "Design Report";

            /// <summary>
            /// A <see cref="string"/> that holds the Group Report.
            /// </summary>
            public const string GROUP_REPORT = "Group Report";

            /// <summary>
            /// A <see cref="string"/> that holds the Comparison Report.
            /// </summary>
            public const string COMPARISON_REPORT = "Comparison Report";

            /// <summary>
            /// A <see cref="string"/> that holds the Canditate Report.
            /// </summary>
            public const string CANDIDATE_REPORT = "Canditate Report";
        }

        #endregion Dashboard Constants

        #region Chart Constants

        /// <summary>
        /// Class that holds the Dashboard constants.
        /// </summary>
        public sealed class ChartConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ChartConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the Category.
            /// </summary>
            public const string CHART_CATEGORY = "Category";

            /// <summary>
            /// A <see cref="string"/> that holds the Subject.
            /// </summary>
            public const string CHART_SUBJECT = "Subject";

            /// <summary>
            /// A <see cref="string"/> that holds the TestArea.
            /// </summary>
            public const string CHART_TESTAREA = "TestArea";

            /// <summary>
            /// A <see cref="string"/> that holds the Complexity.
            /// </summary>
            public const string CHART_COMPLEXITY = "Complexity";

            /// <summary>
            /// A <see cref="string"/> that holds the Histogram.
            /// </summary>
            public const string CHART_HISTOGRAM = "Histogram";

            #region Report common
            /// <summary>
            /// A <see cref="string"/> that holds the Report Category.
            /// </summary>
            public const string REPORT_CHART_CATEGORY = "Report-Category";

            /// <summary>
            /// A <see cref="string"/> that holds the Report Subject.
            /// </summary>
            public const string REPORT_CHART_SUBJECT = "Report-Subject";

            /// <summary>
            /// A <see cref="string"/> that holds the Report TestArea.
            /// </summary>
            public const string REPORT_CHART_TESTAREA = "Report-TestArea";

            /// <summary>
            /// A <see cref="string"/> that holds the Report Complexity.
            /// </summary>
            public const string REPORT_CHART_COMPLEXITY = "Report-Complexity";

            #endregion

            #region Design Report

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall comparative absolute.
            /// </summary>
            public const string DESIGN_REPORT_OVERALL_COMPARATIVE_ABSOLUTE =
                "Design-Report-OverAll-Comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall comparative relative.
            /// </summary>
            public const string DESIGN_REPORT_OVERALL_COMPARATIVE_RELATIVE =
                "Design-Report-OverAll-Comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall absolute.
            /// </summary>
            public const string DESIGN_REPORT_OVERALL_ABSOLUTE =
                "Design-Report-OverAll-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Design Report OverAll Relative.
            /// </summary>
            public const string DESIGN_REPORT_OVERALL_RELATIVE =
                "Design-Report-OverAll-Relative";


            /// <summary>
            /// A <see cref="string"/> that holds the Design Report Histogram Relative.
            /// </summary>
            public const string DESIGN_REPORT_HISTOGRAM_RELATIVE =
                "Design-Report-Histogram-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the Design Report Histogram Absolute.
            /// </summary>
            public const string DESIGN_REPORT_HISTOGRAM_ABSOLUTE =
                "Design-Report-Histogram-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category comparative absolute.
            /// </summary>
            public const string DESIGN_REPORT_CATEGORY_COMPARATIVE_ABSOLUTE =
                "Design-Report-category-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category comparative relative.
            /// </summary>
            public const string DESIGN_REPORT_CATEGORY_COMPARATIVE_RELATIVE =
                "Design-Report-category-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category absolute.
            /// </summary>
            public const string DESIGN_REPORT_CATEGORY_ABSOLUTE =
                "Design-Report-category-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Design Report category  Relative.
            /// </summary>
            public const string DESIGN_REPORT_CATEGORY_RELATIVE =
                "Design-Report-category-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject comparative absolute.
            /// </summary>
            public const string DESIGN_REPORT_SUBJECT_COMPARATIVE_ABSOLUTE =
                "Design-Report-subject-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject comparative relative.
            /// </summary>
            public const string DESIGN_REPORT_SUBJECT_COMPARATIVE_RELATIVE =
                "Design-Report-subject-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject absolute.
            /// </summary>
            public const string DESIGN_REPORT_SUBJECT_ABSOLUTE =
                "Design-Report-subject-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Design Report subject  Relative.
            /// </summary>
            public const string DESIGN_REPORT_SUBJECT_RELATIVE =
                "Design-Report-subject-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea comparative absolute.
            /// </summary>
            public const string DESIGN_REPORT_TESTAREA_COMPARATIVE_ABSOLUTE =
                "Design-Report-TestArea-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea comparative relative.
            /// </summary>
            public const string DESIGN_REPORT_TESTAREA_COMPARATIVE_RELATIVE =
                "Design-Report-TestArea-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea absolute.
            /// </summary>
            public const string DESIGN_REPORT_TESTAREA_ABSOLUTE =
                "Design-Report-TestArea-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Design Report TestArea  Relative.
            /// </summary>
            public const string DESIGN_REPORT_TESTAREA_RELATIVE =
                "Design-Report-TestArea-Relative";

            # endregion

            #region Group Analysis Report

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall comparative absolute.
            /// </summary>
            public const string GROUP_REPORT_OVERALL_COMPARATIVE_ABSOLUTE =
                "Group-Report-OverAll-Comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall comparative relative.
            /// </summary>
            public const string GROUP_REPORT_OVERALL_COMPARATIVE_RELATIVE =
                "Group-Report-OverAll-Comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall absolute.
            /// </summary>
            public const string GROUP_REPORT_OVERALL_ABSOLUTE =
                "Group-Report-OverAll-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Group Report OverAll Relative.
            /// </summary>
            public const string GROUP_REPORT_OVERALL_RELATIVE =
                "Group-Report-OverAll-Relative";


            /// <summary>
            /// A <see cref="string"/> that holds the Group Report Histogram Relative.
            /// </summary>
            public const string GROUP_REPORT_HISTOGRAM_RELATIVE =
                "Group-Report-Histogram-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the Group Report Histogram Absolute.
            /// </summary>
            public const string GROUP_REPORT_HISTOGRAM_ABSOLUTE =
                "Group-Report-Histogram-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category comparative absolute.
            /// </summary>
            public const string GROUP_REPORT_CATEGORY_COMPARATIVE_ABSOLUTE =
                "Group-Report-category-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category comparative relative.
            /// </summary>
            public const string GROUP_REPORT_CATEGORY_COMPARATIVE_RELATIVE =
                "Group-Report-category-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category absolute.
            /// </summary>
            public const string GROUP_REPORT_CATEGORY_ABSOLUTE =
                "Group-Report-category-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Group Report category  Relative.
            /// </summary>
            public const string GROUP_REPORT_CATEGORY_RELATIVE =
                "Group-Report-category-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject comparative absolute.
            /// </summary>
            public const string GROUP_REPORT_SUBJECT_COMPARATIVE_ABSOLUTE =
                "Group-Report-subject-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject comparative relative.
            /// </summary>
            public const string GROUP_REPORT_SUBJECT_COMPARATIVE_RELATIVE =
                "Group-Report-subject-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject absolute.
            /// </summary>
            public const string GROUP_REPORT_SUBJECT_ABSOLUTE =
                "Group-Report-subject-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Group Report subject  Relative.
            /// </summary>
            public const string GROUP_REPORT_SUBJECT_RELATIVE =
                "Group-Report-subject-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea comparative absolute.
            /// </summary>
            public const string GROUP_REPORT_TESTAREA_COMPARATIVE_ABSOLUTE =
                "Group-Report-TestArea-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea comparative relative.
            /// </summary>
            public const string GROUP_REPORT_TESTAREA_COMPARATIVE_RELATIVE =
                "Group-Report-TestArea-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea absolute.
            /// </summary>
            public const string GROUP_REPORT_TESTAREA_ABSOLUTE =
                "Group-Report-TestArea-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Group Report TestArea  Relative.
            /// </summary>
            public const string GROUP_TESTAREA_SUBJECT_RELATIVE =
                "Group-Report-TestArea-Relative";

            # endregion

            #region Comparison Analysis Report

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall comparative absolute.
            /// </summary>
            public const string COMPARISON_REPORT_OVERALL_COMPARATIVE_ABSOLUTE =
                "Comparison-Report-OverAll-Comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall comparative relative.
            /// </summary>
            public const string COMPARISON_REPORT_OVERALL_COMPARATIVE_RELATIVE =
                "Comparison-Report-OverAll-Comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report overall absolute.
            /// </summary>
            public const string COMPARISON_REPORT_OVERALL_ABSOLUTE =
                "Comparison-Report-OverAll-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Comparison Report OverAll Relative.
            /// </summary>
            public const string COMPARISON_REPORT_OVERALL_RELATIVE =
                "Comparison-Report-OverAll-Relative";


            /// <summary>
            /// A <see cref="string"/> that holds the Comparison Report Histogram Relative.
            /// </summary>
            public const string COMPARISON_REPORT_HISTOGRAM_RELATIVE =
                "Comparison-Report-Histogram-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the Comparison Report Histogram Absolute.
            /// </summary>
            public const string COMPARISON_REPORT_HISTOGRAM_ABSOLUTE =
                "Comparison-Report-Histogram-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category comparative absolute.
            /// </summary>
            public const string COMPARISON_REPORT_CATEGORY_COMPARATIVE_ABSOLUTE =
                "Comparison-Report-category-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category comparative relative.
            /// </summary>
            public const string COMPARISON_REPORT_CATEGORY_COMPARATIVE_RELATIVE =
                "Comparison-Report-category-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report category absolute.
            /// </summary>
            public const string COMPARISON_REPORT_CATEGORY_ABSOLUTE =
                "Comparison-Report-category-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Comparison Report category  Relative.
            /// </summary>
            public const string COMPARISON_REPORT_CATEGORY_RELATIVE =
                "Comparison-Report-category-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject comparative absolute.
            /// </summary>
            public const string COMPARISON_REPORT_SUBJECT_COMPARATIVE_ABSOLUTE =
                "Comparison-Report-subject-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject comparative relative.
            /// </summary>
            public const string COMPARISON_REPORT_SUBJECT_COMPARATIVE_RELATIVE =
                "Comparison-Report-subject-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report subject absolute.
            /// </summary>
            public const string COMPARISON_REPORT_SUBJECT_ABSOLUTE =
                "Comparison-Report-subject-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Comparison Report subject  Relative.
            /// </summary>
            public const string COMPARISON_REPORT_SUBJECT_RELATIVE =
                "Comparison-Report-subject-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea comparative absolute.
            /// </summary>
            public const string COMPARISON_REPORT_TESTAREA_COMPARATIVE_ABSOLUTE =
                "Comparison-Report-TestArea-comparative-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea comparative relative.
            /// </summary>
            public const string COMPARISON_REPORT_TESTAREA_COMPARATIVE_RELATIVE =
                "Comparison-Report-TestArea-comparative-Relative";

            /// <summary>
            /// A <see cref="string"/> that holds the design report TestArea absolute.
            /// </summary>
            public const string COMPARISON_REPORT_TESTAREA_ABSOLUTE =
                "Comparison-Report-TestArea-Absolute";

            /// <summary>
            /// A <see cref="string"/> that holds the Comparison Report TestArea  Relative.
            /// </summary>
            public const string COMPARISON_TESTAREA_SUBJECT_RELATIVE =
                "Comparison-Report-TestArea-Relative";

            # endregion


        }

        #endregion Chart Constants

        #region Credit Info Constants

        /// <summary>
        /// Class that holds the credit info constants.
        /// </summary>
        public sealed class CreditInfoConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CreditInfoConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> credit approval for request.
            /// </summary>
            public const string REQUEST_CREDIT_APPROVAL = "By credit request approval";

            /// <summary>
            /// A <see cref="string"/> credit approval for adding questions.
            /// </summary>
            public const string QUESTION_CREDIT_APPROVAL = "By adding question";

            /// <summary>
            /// A <see cref="string"/> credit approval for adding tests.
            /// </summary>
            public const string TEST_CREDIT_APPROVAL = "By adding test";

            /// <summary>
            /// A <see cref="string"/> credit approval for adding tests.
            /// </summary>
            public const string TEST_SESSION_CREATION = "By creating test session";
        }

        #endregion Credit Info Constants

        #region Credit Type Constants

        /// <summary>
        /// Class that holds the credit info constants.
        /// </summary>
        public sealed class CreditTypeConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CreditTypeConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> credit approval for request.
            /// </summary>
            public const string CREDIT_EARNED = "CR_EARNED";

            /// <summary>
            /// A <see cref="string"/> credit approval for adding questions.
            /// </summary>
            public const string CREDIT_USED = "CR_USED";
        }

        #endregion Credit Type Constants

        #region Credit Request Constants

        /// <summary>
        /// Class that holds the credit info constants.
        /// </summary>
        public sealed class CreditRequestConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CreditRequestConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/>candidate request type for credit
            /// </summary>
            public const string CANDIDATE_REQUEST = "CR_REQ_CAN";
        }

        #endregion Credit Type Constants

        #region Question Credit Constants

        /// <summary>
        /// Class that holds the question credit constants.
        /// </summary>
        public sealed class QuestionCreditConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private QuestionCreditConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the simple question credit type.
            /// </summary>
            public const string SIMPLE = "CR_QSN_S";

            /// <summary>
            /// A <see cref="string"/> that holds the medium question credit type.
            /// </summary>
            public const string MEDIUM = "CR_QSN_M";

            /// <summary>
            /// A <see cref="string"/> that holds the complex question credit type.
            /// </summary>
            public const string COMPLEX = "CR_QSN_C";
        }

        #endregion Question Credit Constants

        #region Test Maker Constant
        /// <summary>
        /// Class that holds the question credit constants.
        /// </summary>
        public sealed class TestMakerConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private TestMakerConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the Create Manual Test With Adapitve.
            /// </summary>
            public const string CREATE_TEST_WA = "CreateManualTest.aspx";

            /// <summary>
            /// A <see cref="string"/> that holds the Create Interview Test With Adapitve.
            /// </summary>
            public const string CREATE_INTERVIEW_TEST_WA = "CreateInterviewTestWithAdaptive.aspx";


            /// <summary>
            /// A <see cref="string"/> that holds the Create Manual Test.
            /// </summary>
            public const string CREATE_TEST = "CreateManualTestWithoutAdaptive.aspx";

            /// <summary>
            /// A <see cref="string"/> that holds the Edit Manual Test With Adapitve.
            /// </summary>
            public const string EDIT_TEST_WA = "EditManualTest.aspx";

            /// <summary>
            /// A <see cref="string"/> that holds the Edit Manual Test With Adapitve.
            /// </summary>
            public const string EDIT_INTERIVIEW_TEST_WA = "EditInterviewTestWithAdaptive.aspx";

            /// <summary>
            /// A <see cref="string"/> that holds the Edit Manual Test.
            /// </summary>
            public const string EDIT_TEST = "EditManualTestWithoutAdaptive.aspx";

            /// <summary>
            /// A <see cref="string"/> that holds the Edit Manual Interview Test.
            /// </summary>
            public const string EDIT_INTERVIEW_TEST = "EditManualInterviewTest.aspx";

        }

        #endregion

        #region Test session Credit Constants

        /// <summary>
        /// Class that holds the question credit constants.
        /// </summary>
        public sealed class TestSessionCreditConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private TestSessionCreditConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the test session 
            /// created by candidate
            /// </summary>
            public const string TEST_SESSION_CANDIDATE_CREDIT = "CR_TSN_CAN";

            /// <summary>
            /// A <see cref="string"/> that holds the test session 
            /// created by the user
            /// </summary>
            public const string TEST_SESSION_USER_CREDIT = "CR_TSN_USR";
        }

        #endregion Test session Credit Constants

        #region Chart Attributes Constants
        /// <summary>
        /// Class that holds the question credit constants.
        /// </summary>
        public sealed class ChartAttributesConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ChartAttributesConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the default chart width
            /// </summary>
            public const int CHART_DEFAULT_WIDTH = 33;

            /// <summary>
            /// A <see cref="string"/> that holds the default chart height
            /// </summary>
            public const int CHART_DEFAULT_HEIGHT = 23;

            /// <summary>
            /// A <see cref="string"/> that holds the increased width
            /// </summary>
            public const int CHART_INCREASED_WIDTH = 15;

            /// <summary>
            /// A <see cref="string"/> that holds the increased height
            /// </summary>
            public const int CHART_INCREASED_HEIGHT = 20;

        }

        #endregion

        #region Chart Default Constants
        /// <summary>
        /// Class that holds the question credit constants.
        /// </summary>
        public sealed class DefaultChartConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private DefaultChartConstants()
            {
            }
            /// <summary>
            /// A <see cref="string"/> that holds the default chart width
            /// </summary>
            public const int CHART_WIDTH = 250;

            /// <summary>
            /// A <see cref="string"/> that holds the default chart length
            /// </summary>
            public const int CHART_LENGTH = 250;

            /// <summary>
            /// A <see cref="string"/> that holds the default chart area position width 
            /// </summary>
            public const float CHART_AREA_POSITION_WIDTH = 35;

            /// <summary>
            /// A <see cref="string"/> that holds the default legend item column spacing
            /// </summary>
            public const int LEGEND_ITEM_COLUMN_SPACING = 50;

            /// <summary>
            /// A <see cref="string"/> that holds the legend maximum auto size
            /// </summary>
            public const int LEGEND_MAXIMUM_AUTO_SIZE = 70;

            /// <summary>
            /// A <see cref="string"/> that holds the text wrap threshold
            /// </summary>
            public const int LEGEND_TEXT_WRAP_THRESHOLD = 70;

        }

        #endregion

        #region Segments Constants
        /// <summary>
        /// Class that holds the question credit constants.
        /// </summary>
        public sealed class DefaultSegmentsConstants
        {

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private DefaultSegmentsConstants()
            {

            }
            /// <summary>
            /// A <see cref="string"/> that holds the default chart width
            /// </summary>
            public const int ClientPosistion = 250;

            /// <summary>
            /// A <see cref="string"/> that holds the default chart length
            /// </summary>
            public const int CHART_LENGTH = 250;

            /// <summary>
            /// A <see cref="string"/> that holds the default chart area position width 
            /// </summary>
            public const float CHART_AREA_POSITION_WIDTH = 35;

            /// <summary>
            /// A <see cref="string"/> that holds the default legend item column spacing
            /// </summary>
            public const int LEGEND_ITEM_COLUMN_SPACING = 50;

            /// <summary>
            /// A <see cref="string"/> that holds the legend maximum auto size
            /// </summary>
            public const int LEGEND_MAXIMUM_AUTO_SIZE = 70;

            /// <summary>
            /// A <see cref="string"/> that holds the text wrap threshold
            /// </summary>
            public const int LEGEND_TEXT_WRAP_THRESHOLD = 70;

        }

        #endregion

        #region Position Profile Constants

        /// <summary>
        /// Class that holds the question credit constants.
        /// </summary>
        public sealed class PositionProfileConstants
        {

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private PositionProfileConstants()
            {

            }
            /// <summary>
            /// 
            /// </summary>
            public const string VIEW_STATE_FORM_SEGMENTS_ID = "VIEW_STATE_FORM_SEGMENTS_ID";

            /// <summary>
            /// 
            /// </summary>
            public const string FORM_ID_QUERYSTRING = "form_id";

            /// <summary>
            /// 
            /// </summary>
            public const string POSITION_PROFILE_ID_QUERYSTRING = "positionprofileid";

            /// <summary>
            /// 
            /// </summary>
            public const string FORM_ID = "formID";
            /// <summary>
            /// 
            /// </summary>
            public const string FORM_NAME = "formName";
        }

        #endregion Position Profile Constants

        #region Chart Constants

        /// <summary>
        /// Class that holds the Dashboard constants.
        /// </summary>
        public sealed class RoleRightMatrixConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private RoleRightMatrixConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the sub module name.
            /// </summary>
            public const string SUB_MODULE_NAME = "Sub Module Name";

            /// <summary>
            /// A <see cref="string"/> that holds the sub module name.
            /// </summary>
            public const string FEATURE_NAME = "Feature Name";

            /// <summary>
            /// A <see cref="string"/> that holds the sub module name.
            /// </summary>
            public const string FEATURE_ID = "FeatureID";

            /// <summary>
            /// A<see cref="int"/> that holds the column id for 
            /// processing . This is assigned as 3 , because the processing starts from
            /// the third column. First three columns are 0[Submodule Name], 1[Feature Name]
            /// [2]Feature Id
            /// </summary>
            public const int STARTING_COLUMN_ID = 3;

            /// <summary>
            /// A<see cref="int"/>that holds the column number to hide. 
            /// Hide the second column of the datatable, as it contains the 
            /// feature ID
            /// </summary>
            public const int FEATURE_ID_COLUMN = 2;

        }

        #endregion Chart Constants

        #region Compentency Vector Group Constants

        /// <summary>
        /// Class that holds the role code constants.
        /// </summary>
        public sealed class CompetencyVectorGroupConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            public CompetencyVectorGroupConstants()
            {

            }

            /// <summary>
            /// A <see cref="int"/> that holds the technical skills id
            /// </summary>
            public const int TECHNICAL_SKILLS = 1;

            /// <summary>
            /// A<see cref="int"/>that holds the Roles ID
            /// </summary>
            public const int ROLES = 3;

        }

        #endregion Compentency Vector Group Constants

        #region NomenclatureAttribute
        public sealed class NomenclatureAttribute
        {
              /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private NomenclatureAttribute()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the question author role type.
            /// </summary>
            public const string NOMENCLATURE_CLIENT = "Client";

            /// <summary>
            /// Content validator role type
            /// </summary>
            public const string NOMENCLATURE_CLIENTDEPARTMENT = "ClientDepartment";

            /// <summary>
            /// A <see cref="string"/> that holds the question author role type.
            /// </summary>
            public const string NOMENCLATURE_CLIENTCONTACTS = "ClientContacts";

            /// <summary>
            /// Content validator role type
            /// </summary>
            public const string NOMENCLATURE_ROLES = "Roles";
        }
        #endregion

        #region ClientManagemnt
        public sealed class ClientManagementConstants
        {
            private ClientManagementConstants()
            {
            }
            public const string ADD_CLIENT = "addclient";
            public const string EDIT_CLIENT = "editclient";
            public const string DELETE_CLIENT = "deleteclient";
            public const string VIEW_CLIENT_DEPARTMENT = "viewclientdepartment";
            public const string ADD_CLIENT_DEPARTMENT = "addclientdepartment";
            public const string VIEW_CLIENT_CONTACT = "viewclientcontact";
            public const string ADD_CLIENT_CONTACT = "addclientcontact";
            public const string VIEW_CLIENT_POSITION_PROFILE = "viewclientpositionprofile";
            public const string ADD_CLIENT_POSITION_PROFILE = "addclientpositionprofile";

            public const string ADD_DEPARTMENT = "adddepartment";
            public const string EDIT_DEPARTMENT = "editdepartment";
            public const string DELETE_DEPARTMENT = "deletedepartment";
            public const string VIEW_DEPARTMENT_CLIENT = "viewdepartmentclient";
            public const string VIEW_DEPARTMENT_CONTACT = "viewdepartmentcontact";
            public const string ADD_DEPARTMENT_CONTACT = "adddepartmentcontact";
            public const string VIEW_DEPARTMENT_POSITION_PROFILE = "viewdepartmentpositionprofile";
            public const string ADD_DEPARTMENT_POSITION_PROFILE = "adddepartmentpositionprofile";

            public const string ADD_CONTACT = "addcontact";
            public const string EDIT_CONTACT = "editcontact";
            public const string DELETE_CONTACT = "deletecontact";
            public const string VIEW_CONTACT_CLIENT = "viewcontactclient";
            public const string VIEW_CONTACT_DEPARTMENT = "viewcontactdepartment";
            public const string VIEW_CONTACT_POSITION_PROFILE = "viewcontactpositionprofile";
            public const string ADD_CONTACT_POSITION_PROFILE = "addcontactpositionprofile";
            public const string SHOW_CONTACT_DEPARTMENT = "showcontatdepartment";
            public const string SHOW_CLIENT_DEPARTMENT = "showclientdepartment";

            public const string CLIENT_ID_QUERYSTRING = "CLT_ID";
            public const string DEPARTMENT_ID_QUERYSTRING = "DEP_ID";
            public const string CONTACT_ID_QUERYSTRING = "CONT_ID";

            public const string SEARCH_CLIENT = "searchclient";
            public const string SEARCH_DEPARTMENT = "searchdepartment";
            public const string SEARCH_CONTACT = "searchcontact";


            
        }
        #endregion

        #region Time Slot Reference Type Constants

        /// <summary>
        /// Class that holds the time slot reference types.
        /// </summary>
        public sealed class TimeSlotReferenceType
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private TimeSlotReferenceType()
            {
            }

            /// <summary>
            /// A <see cref="string"/> string that holds the unknown type.
            /// </summary>
            public const string UNKNOWN = "TSR_UN_KNO";

            /// <summary>
            /// A <see cref="string"/> string that holds the online interview type.
            /// </summary>
            public const string ON_LINE_INTERVIEW = "TSR_ON_INT";

            /// <summary>
            /// A <see cref="string"/> string that holds the offline interview type.
            /// </summary>
            public const string OFF_LINE_INTERVIEW = "TSR_OF_INT";

            /// <summary>
            /// A <see cref="string"/> string that holds the position profile type.
            /// </summary>
            public const string POSITION_PROFILE = "TSR_PS_PRO";
        }

        #endregion Time Slot Reference Type Constants

        #region Time Slot Request Status Constants

        /// <summary>
        /// Class that holds the time slot request status.
        /// </summary>
        public sealed class TimeSlotRequestStatus
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private TimeSlotRequestStatus()
            {
            }

            /// <summary>
            /// A <see cref="string"/> string that holds the accepted type code.
            /// </summary>
            public const string ACCEPTED_CODE = "A";

            /// <summary>
            /// A <see cref="string"/> string that holds the pending type code.
            /// </summary>
            public const string PENDING_CODE = "I";

            /// <summary>
            /// A <see cref="string"/> string that holds the rejected type code.
            /// </summary>
            public const string REJECTED_CODE = "R";

            /// <summary>
            /// A <see cref="string"/> string that holds the accepted type name.
            /// </summary>
            public const string ACCEPTED_NAME = "Accepted";

            /// <summary>
            /// A <see cref="string"/> string that holds the pending type name.
            /// </summary>
            public const string PENDING_NAME = "Pending";

            /// <summary>
            /// A <see cref="string"/> string that holds the rejected type name.
            /// </summary>
            public const string REJECTED_NAME = "Rejected";
        }

        #endregion Time Slot Request Status Constants

        #region Session Type Constants

        /// <summary>
        /// Class that holds the session types
        /// </summary>
        public sealed class SessionType
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private SessionType()
            {
            }

            /// <summary>
            /// A <see cref="string"/> string that holds the online interview type.
            /// </summary>
            public const string ON_LINE_INTERVIEW = "TSR_ON_INT";

            /// <summary>
            /// A <see cref="string"/> string that holds the offline interview type.
            /// </summary>
            public const string OFF_LINE_INTERVIEW = "TSR_OF_INT";
        }

        #endregion Time Slot Reference Type Constants

        #region Candidate Self Admin Test Constants

        /// <summary>
        /// Class that holds the candidate self admin test constants.
        /// </summary>
        public sealed class CandidateSelfAdminTestConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CandidateSelfAdminTestConstants()
            {
            }

            /// <summary>
            /// A <see cref="int"/> that holds the attempt ID.
            /// </summary>
            public const int ATTEMPT_ID = 1;

            /// <summary>
            /// A <see cref="string"/> that holds the session description.
            /// </summary>
            public const string SESSION_DESCRIPTION = "Self administration test";

            /// <summary>
            /// A <see cref="string"/> that holds the test instructions.
            /// </summary>
            public const string TEST_INSTRUCTIONS = "Self administration test";
        }

        #endregion Candidate Self Admin Test Constants

        #region Candidate Self Admin Test Status

        /// <summary>
        /// Class that holds the candidate self admin test status.
        /// </summary>
        public sealed class CandidateSelfAdminTestStatus
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CandidateSelfAdminTestStatus()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the saved status.
            /// </summary>
            public const string SAVED = "SAT_SAVD";

            /// <summary>
            /// A <see cref="string"/> that holds the completed status.
            /// </summary>
            public const string COMPLETED = "SAT_COMP";

            /// <summary>
            /// A <see cref="string"/> that holds the scheduled status.
            /// </summary>
            public const string SCHEDULED = "SAT_SCHD";
        }

        #endregion Candidate Self Admin Test Status

        #region My Pending Activities Constants

        /// <summary>
        /// Class that holds the my pending activities constants.
        /// </summary>
        public sealed class MyPendingActivitiesConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private MyPendingActivitiesConstants()
            {
            }

            /// <summary>
            /// A <see cref="int"/> that holds the attempt ID.
            /// </summary>
            public const int PAGE_SIZE = 1;
        }

        #endregion My Pending Activities Constants

        #region Candidate Options Default Values Constants

        /// <summary>
        /// Class that holds the candidate options default values constants.
        /// </summary>
        public sealed class CandidateOptionsDefaultValuesConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CandidateOptionsDefaultValuesConstants()
            {
            }

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// max number of retakes.
            /// </summary>
            public const string MAX_NO_OF_RETAKES = "2";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// max number of tests.
            /// </summary>
            public const string MAX_NO_OF_TESTS = "5";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// session expiry days.
            /// </summary>
            public const string SESSION_EXPIRY_DAYS = "10";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// resume expiry days.
            /// </summary>
            public const string RESUME_EXPIRY_DAYS = "180";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// activities grid page size.
            /// </summary>
            public const string ACTIVITIES_GRID_PAGE_SIZE = "10";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// search test grid page size.
            /// </summary>
            public const string SEARCH_TEST_GRID_PAGE_SIZE = "1";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// min number of questions for self admin test .
            /// </summary>
            public const string MIN_QUESTION_PER_SELF_ADMIN_TEST = "5";

            /// <summary>
            /// A <see cref="string"/> that holds the default value for
            /// max number of questions for self admin test .
            /// </summary>
            public const string MAX_QUESTION_PER_SELF_ADMIN_TEST = "30";
        }

        #endregion Candidate Options Default Values Constants

        #region Candidate Resume Repository Default Contants Values
            /// <summary>
        /// Class that holds the candidate options default values constants.
        /// </summary>
        public sealed class  CandidateResumeRepositoryConstants
        { 
              /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private CandidateResumeRepositoryConstants()
            { }

            /// <summary>
            /// A <see cref="string"/> that holds the normal resume skill type.
            /// </summary>
            public const string CANDIDATE_NORMAL_RESUME_SKILL= "NORMAL";

            /// <summary>
            /// A <see cref="string"/> that holds the temporary resume skill type.
            /// </summary>
            public const string CANDIDATE_TEMPORARY_RESUME_SKILL = "TEMP";
        }
        #endregion

        #region Resume Email Reminder Settings
        /// <summary>
        /// Class that holds the resume reminder default values constants.
        /// </summary>
        public sealed class ResumeEmailReminderConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ResumeEmailReminderConstants()
            { }

            /// <summary>
            /// A <see cref="string"/> that holds the resume uploader.
            /// </summary>
            public const string RESUME_EMAIL_REMINDER_UPLOADER = "REU_UPR";

            /// <summary>
            /// A <see cref="string"/> that holds the resume admin.
            /// </summary>
            public const string RESUME_EMAIL_REMINDER_ADMIN = "REU_ADM";

            /// <summary>
            /// A <see cref="string"/> that holds the resume email option for immediately.
            /// </summary>
            public const string RESUME_EMAIL_REMINDER_IMM = "RER_IMM";

            /// <summary>
            /// A <see cref="string"/> that holds the resume email option for daily.
            /// </summary>
            public const string RESUME_EMAIL_REMINDER_DLY = "RER_DLY";

            /// <summary>
            /// A <see cref="string"/> that holds the resume email option for weekly.
            /// </summary>
            public const string RESUME_EMAIL_REMINDER_WLY = "RER_WLY";

            /// <summary>
            /// A <see cref="string"/> that holds the resume duplicate email option for immediately.
            /// </summary>
            public const string RESUME_EMAIL_REMINDER_DUP_IMM = "RDR_IMM";

            /// <summary>
            /// A <see cref="string"/> that holds the duplicate resume email option for daily.
            /// </summary>
            public const string RESUME_EMAIL_REMINDER_DUP_DLY = "RDR_DLY";

            /// <summary>
            /// A <see cref="string"/> that holds the dupilcate resume email option for weekly.
            /// </summary>
            public const string RESUME_EMAIL_REMINDER_DUP_WLY = "RDR_WLY";
        }
        #endregion

        #region Resume Mandatory Settings
        /// <summary>
        /// Class that holds the resume mandatory constants.
        /// </summary>
        public sealed class ResumeMandatoryConstants
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ResumeMandatoryConstants()
            { }

            /// <summary>
            /// A <see cref="string"/> that holds the contact info
            /// </summary>
            public const string RESUME_MANDATORY_CONTACT_INFO = "ContactInfo";

            /// <summary>
            /// A <see cref="string"/> that holds the executive info
            /// </summary>
            public const string RESUME_MANDATORY_EXECUTIVE_SUMMARY = "ExecutiveSummary";

            /// <summary>
            /// A <see cref="string"/> that holds the technical skills
            /// </summary>
            public const string RESUME_MANDATORY_TECHNICAL_SKILL = "TechnicalSkills";

            /// <summary>
            /// A <see cref="string"/> that holds the project history
            /// </summary>
            public const string RESUME_MANDATORY_PROJECT_HISTORY = "ProjectHistory";

            /// <summary>
            /// A <see cref="string"/> that holds the references info
            /// </summary>
            public const string RESUME_MANDATORY_REFERENCES = "References";

            /// <summary>
            /// A <see cref="string"/> that holds the education info
            /// </summary>
            public const string RESUME_MANDATORY_EDUCATION = "EducationHistory";
        }
        #endregion

        #region Email Subject Heading
        /// <summary>
        /// Class that holds the email subject heading constants.
        /// </summary>
        public sealed class EmailSubjectHeading
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private EmailSubjectHeading()
            { }

            /// <summary>
            /// A <see cref="string"/>that holds the company name
            /// </summary>
            public const string EMAIL_SUBJECT_COMPANY_NAME = "ForteHCM";
        }
        #endregion Email Subject Heading

        #region Resume Duplication Rule Actions

        /// <summary>
        /// Class that holds the resume duplication rule actions constants.
        /// </summary>
        public sealed class ResumeDuplicationRuleActions
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ResumeDuplicationRuleActions()
            { }

            /// <summary>
            /// A <see cref="string"/>that holds the add to existing candidate record code
            /// </summary>
            public const string ADD_TO_EXISTING_CANDIDATE_RECORD = "RDA_AEC";

            /// <summary>
            /// A <see cref="string"/>that holds the mark as duplicate code
            /// </summary>
            public const string MARK_AS_DUPLICATE = "RDA_MAD";

            /// <summary>
            /// A <see cref="string"/>that holds the send mail to admin code
            /// </summary>
            public const string SEND_MAIL_TO_ADMIN = "RDA_SMA";

        }
        #endregion Resume Duplication Rule Actions

        #region Resume Duplication Rule Expression Type

        /// <summary>
        /// Class that holds the resume duplication rule expresssion type constants.
        /// </summary>
        public sealed class ResumeDuplicationRuleExpressionType
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ResumeDuplicationRuleExpressionType()
            { }

            /// <summary>
            /// A <see cref="string"/>that holds the match constants
            /// </summary>
            public const string MATCHES = "RDE_MAT";

            /// <summary>
            /// A <see cref="string"/>that holds the not matches constants
            /// </summary>
            public const string NOT_MATCHES = "RDE_NMA";

        }

        #endregion Resume Duplication Rule Expression Type

        #region Resume Rule Logical Operator

        /// <summary>
        /// Class that holds the resume rule logical operator
        /// </summary>
        public sealed class ResumeDuplicationRuleLogicalOperator
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private ResumeDuplicationRuleLogicalOperator()
            { }

            /// <summary>
            /// A <see cref="string"/>that holds the and constants
            /// </summary>
            public const string AND = "RDO_AND";

            /// <summary>
            /// A <see cref="string"/>that holds the or constants
            /// </summary>
            public const string OR = "RDO_OR";

        }

        #endregion Resume Rule Logical Operator

        #region Position Profile Owners Constants

        /// <summary>
        /// Class that holds the position profile owners constants.
        /// </summary>
        public sealed class PositionProfileOwners
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <remarks>
            /// Defined as private to avoid initialization.
            /// </remarks>
            private PositionProfileOwners()
            {
            }

            /// <summary>
            /// A <see cref="string"/> string that holds the recruiter assignment task owner.
            /// </summary>
            public const string RECRUITER_ASSIGNMENT_CREATOR = "POT_REC_CR";

            /// <summary>
            /// A <see cref="string"/> string that holds the candidate associate task owner.
            /// </summary>
            public const string CANDIDATE_ASSOCIATION_CREATOR = "POT_CA_CR";

            /// <summary>
            /// A <see cref="string"/> string that holds the interview creater task owner.
            /// </summary>
            public const string INTERVIEW_CREATOR = "POT_INT_CR";

            /// <summary>
            /// A <see cref="string"/> string that holds the interview session creater task owner.
            /// </summary>
            public const string INTERVIEW_SESSION_CREATOR = "POT_ISN_CR";

            /// <summary>
            /// A <see cref="string"/> string that holds the interview schedule creater task owner.
            /// </summary>
            public const string INTERVIEW_SCHEDULER = "POT_ISC_CR";

            /// <summary>
            /// A <see cref="string"/> string that holds the assessor creation task owner.
            /// </summary>
            public const string ASSESSOR_ASSIGNMENT = "POT_IAS_AS";

            /// <summary>
            /// A <see cref="string"/> string that holds the test creater task owner.
            /// </summary>
            public const string TEST_CREATOR = "POT_TST_CR";

            /// <summary>
            /// A <see cref="string"/> string that holds the test session creater task owner.
            /// </summary>
            public const string TEST_SESSION_CREATOR = "POT_TSN_CR";

            /// <summary>
            /// A <see cref="string"/> string that holds the test schedule creater task owner.
            /// </summary>
            public const string TEST_SCHEDULER = "POT_TSC_CR";

            /// <summary>
            /// A <see cref="string"/> string that holds the submittal to client task owner.
            /// </summary>
            public const string SUBMITTAL_TO_CLIENT = "POT_SUB_CL";

            /// <summary>
            /// A <see cref="string"/> string that holds position profile candidate recruiter.
            /// </summary>
            public const string CANDIDATE_RECRUITER = "POT_CAN_RE";

            /// <summary>
            /// A <see cref="string"/> string that holds position profile co-owner.
            /// </summary>
            public const string CO_OWNER = "POT_CO_OWR";

              /// <summary>
            /// A <see cref="string"/> string that holds position profile owner.
            /// </summary>
            public const string OWNER = "POT_OWR";
        }

        #endregion Position Profile Owners Constants
    }
}