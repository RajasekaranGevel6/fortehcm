﻿#region Directives

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

#endregion

namespace Forte.HCM.Support
{
    /// <summary>
    /// This file is for grid view templates
    /// when gridview is dynamically loading in the page and 
    /// in that if you need to use Template fields you can 
    /// use this class.
    /// </summary>
    public class GridViewTemplate : ITemplate
    {

        #region Declarations

        private ListItemType _templateType;
        private string _colName;
        private string _toolTip;
        private DataTable _VectorTable;
        private string _Controltype;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor method for the class
        /// </summary>
        /// <param name="listItemType">List item type 
        /// it's an enum</param>
        /// <param name="ColName">Name of the column for the item template</param>
        /// <param name="ToolTip">Tool tip to display for the column</param>
        /// <param name="VectorTable">Vector table
        /// Note:- this is used at the time of edit template.
        /// for item template you pass this as null</param>
        /// <param name="ControlType">Type of the control;.
        /// whetehr it is button or label or text box</param>
        public GridViewTemplate(ListItemType listItemType, string ColName, string ToolTip,
            DataTable VectorTable, string ControlType)
        {
            _templateType = listItemType;
            _colName = ColName;
            _toolTip = ToolTip;
            if (VectorTable != null)
                _VectorTable = VectorTable;
            _Controltype = ControlType;
        }

        #endregion

        #region ITemplate Signature Method

        /// <summary>
        /// This is the signature method for the class which inherits
        /// ITemplate interface.
        /// </summary>
        /// <param name="Container">object to contain the instances of controls</param>
        void ITemplate.InstantiateIn(Control Container)
        {
            switch (_templateType)
            {
                case ListItemType.Header:
                    Label lblHeader = new Label();
                    lblHeader.Text = _colName;
                    lblHeader.Width = new Unit(100, UnitType.Percentage);
                    lblHeader.Height = new Unit(100, UnitType.Percentage);
                    Container.Controls.Add(lblHeader);
                    break;
                case ListItemType.Item:
                    switch (_Controltype)
                    {
                        case "EditButton":
                            ImageButton imgEditButton = new ImageButton();
                            imgEditButton.ID = "EditImageButton";
                            imgEditButton.CommandName = "edit";
                            imgEditButton.ImageUrl = @"~/App_Themes/DefaultTheme/Images/icon_edit_vector.gif";
                            imgEditButton.ToolTip = "Edit";
                            imgEditButton.Width = new Unit(15, UnitType.Pixel);
                            Container.Controls.Add(imgEditButton);
                            //    break;
                            //case "DeleteButton":
                            ImageButton imgDeleteButton = new ImageButton();
                            imgDeleteButton.ID = "DeleteImageButton";
                            imgDeleteButton.CommandName = "delete";
                            imgDeleteButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_delete_vector.gif";
                            imgDeleteButton.ToolTip = "Delete";
                            imgDeleteButton.Width = new Unit(15, UnitType.Pixel);
                            //                            imgDeleteButton.Attributes.Add("onclick", "return confirm('Are you sure want to delete details?');");
                            Container.Controls.Add(imgDeleteButton);
                            break;
                        default:
                            Label lblItem = new Label();
                            lblItem.ID = _colName.Trim() + "_Label";
                            lblItem.Text = _colName;
                            lblItem.ToolTip = _toolTip;
                            lblItem.Width = new Unit(80, UnitType.Pixel);
                            lblItem.DataBinding += new EventHandler(Label_DataBinding);
                            Container.Controls.Add(lblItem);
                            break;
                    }
                    break;
                case ListItemType.EditItem:
                    if (_colName == "VECTOR_NAME")
                    {
                        DropDownList ddl = new DropDownList();
                        ddl.ID = "Vector_Name_DropDownList";
                        _VectorTable.Rows.InsertAt(_VectorTable.NewRow(), 0);
                        _VectorTable.Rows[0]["VECTOR_ID"] = "0";
                        _VectorTable.Rows[0]["VECTOR_NAME"] = "--Select--";
                        ddl.DataSource = _VectorTable;
                        ddl.DataTextField = "VECTOR_NAME";
                        ddl.DataValueField = "VECTOR_ID";
                        ddl.Style.Add("overflow", "auto");
                        ddl.Width = new Unit(80, UnitType.Pixel);
                        ddl.DataBind();
                        Container.Controls.Add(ddl);
                    }
                    else
                    {
                        switch (_Controltype)
                        {
                            case "UpdateButton":
                                ImageButton imgUpdateButton = new ImageButton();
                                imgUpdateButton.ID = "UpdateImageButton";
                                imgUpdateButton.CommandName = "update";
                                imgUpdateButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_save_vector.gif";
                                imgUpdateButton.ToolTip = "Update";
                                imgUpdateButton.Width = new Unit(15, UnitType.Pixel);
                                Container.Controls.Add(imgUpdateButton);
                                //    break;
                                //case "CancelButton":
                                ImageButton imgCancelButton = new ImageButton();
                                imgCancelButton.ID = "CancelImageButton";
                                imgCancelButton.CommandName = "cancel";
                                imgCancelButton.ImageUrl = "~/App_Themes/DefaultTheme/Images/icon_cancel_vector.gif";
                                imgCancelButton.ToolTip = "Cancel";
                                imgCancelButton.Width = new Unit(15, UnitType.Pixel);
                                Container.Controls.Add(imgCancelButton);
                                break;
                            default:
                                if (_colName.Contains("ID"))
                                {
                                    TextBox txt = new TextBox();
                                    txt.ID = _colName.Trim() + "_TextBox";
                                    txt.Text = _colName;
                                    //txt.Width = new Unit(90);
                                    txt.DataBinding += new EventHandler(TextBox_DataBinding);
                                    //
                                    Container.Controls.Add(txt);
                                }
                                else
                                {
                                    HtmlTable table = new HtmlTable();
                                    HtmlTableRow valueRow = new HtmlTableRow();
                                    HtmlTableRow remarksRow = new HtmlTableRow();
                                    HtmlTableCell valueCell = new HtmlTableCell();
                                    HtmlTableCell remarksCell = new HtmlTableCell();
                                    valueCell.Style.Add("style", "vertical-align: middle;");
                                    remarksCell.Style.Add("style", "vertical-align: middle;");
                                    TextBox txt = new TextBox();
                                    txt.ID = _colName.Trim().Replace(' ', '_') + "_TextBox";
                                    txt.Text = _colName;
                                    txt.Width = new Unit(30, UnitType.Pixel);
                                    txt.Height = new Unit(15, UnitType.Pixel);
                                    if (txt.ID.Contains("Years"))
                                        txt.MaxLength = 4;
                                    else if (txt.ID.Contains("Recency"))
                                        txt.MaxLength = 4;
                                    else if (txt.ID.Contains("Certification"))
                                        txt.MaxLength = 1;
                                    txt.DataBinding += new EventHandler(TextBox_DataBinding);
                                    //
                                    TextBox txtRemarks = new TextBox();
                                    txtRemarks.ID = _colName.Trim().Replace(' ', '_') + "_Remarks_TextBox";
                                    txtRemarks.TextMode = TextBoxMode.MultiLine;
                                    txtRemarks.Height = new Unit(80, UnitType.Pixel);
                                    txtRemarks.Width = new Unit(80, UnitType.Pixel);
                                    txtRemarks.Text = _toolTip;
                                    txtRemarks.Attributes.Add("onchange", "CommentsCount('200',this);");
                                    txtRemarks.Attributes.Add("onkeyup", "CommentsCount('200',this);");
                                    txtRemarks.DataBinding += new EventHandler(TextBoxRemarks_DataBinding);
                                    valueCell.Controls.Add(txt);
                                    remarksCell.Controls.Add(txtRemarks);
                                    valueRow.Cells.Add(valueCell);
                                    remarksRow.Cells.Add(remarksCell);
                                    table.Rows.Add(valueRow);
                                    table.Rows.Add(remarksRow);
                                    Container.Controls.Add(table);
                                }
                                break;
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Binding Methods

        /// <summary>
        /// This method will be called when the text box remarks type
        /// is binding to the grid.
        /// </summary>
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void TextBoxRemarks_DataBinding(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            GridViewRow container = (GridViewRow)txt.NamingContainer;
            object toolTipValue = null;
            if (_toolTip == "")
                toolTipValue = "1";
            else
                toolTipValue = DataBinder.Eval(container.DataItem, _toolTip);
            if (toolTipValue != DBNull.Value)
                txt.Text = toolTipValue.ToString();
            else
                txt.Text = "";
        }

        /// <summary>
        /// This method will be called when the normal text box type
        /// is binding to the grid.
        /// </summary>
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void TextBox_DataBinding(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            GridViewRow container = (GridViewRow)txt.NamingContainer;
            object dataValue = DataBinder.Eval(container.DataItem, _colName);
            if (dataValue != DBNull.Value)
                txt.Text = dataValue.ToString().Trim();
            else
                txt.Text = "";
            if (_colName == "GROUP_ID")
                ((ImageButton)container.FindControl("UpdateImageButton")).CommandArgument += ((TextBox)
                    container.FindControl("GROUP_ID_TextBox")).Text.Trim();
            if (_colName == "VECTOR_ID")
            {
                if (dataValue != DBNull.Value)
                    ((DropDownList)container.FindControl("Vector_Name_DropDownList")).Items.FindByValue(
                        ((TextBox)container.FindControl("VECTOR_ID_TextBox")).Text).Selected = true;
                else
                    ((DropDownList)container.FindControl("Vector_Name_DropDownList")).SelectedIndex = 0;
                ((ImageButton)container.FindControl("UpdateImageButton")).CommandArgument += "|" + ((TextBox)
                    container.FindControl("VECTOR_ID_TextBox")).Text.Trim();
            }
        }

        /// <summary>
        /// This method will be called when the label type
        /// is binding to the grid.
        /// </summary>
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void Label_DataBinding(object sender, EventArgs e)
        {
            Label lbldata = (Label)sender;
            GridViewRow container = (GridViewRow)lbldata.NamingContainer;
            object dataValue = DataBinder.Eval(container.DataItem, _colName);
            object toolTipValue = null;
            if ((_toolTip == "") || (_colName.Contains("ID")))
                toolTipValue = "-10000";
            else
                toolTipValue = DataBinder.Eval(container.DataItem, _toolTip);
            if (dataValue != DBNull.Value)
                lbldata.Text = dataValue.ToString().Trim() == "0" ? "" : dataValue.ToString().Trim();
            else
                lbldata.Text = "";
            if (toolTipValue == DBNull.Value)
                lbldata.ToolTip = "";
            else if (toolTipValue.ToString() == "-10000")
                lbldata.ToolTip = "";
            else
                lbldata.ToolTip = toolTipValue.ToString();
            if (_colName == "GROUP_ID")
                ((ImageButton)container.FindControl("EditImageButton")).CommandArgument += ((Label)
                    container.FindControl("GROUP_ID_LABEL")).Text;
            if (_colName == "VECTOR_ID")
                ((ImageButton)container.FindControl("EditImageButton")).CommandArgument += "|" + ((Label)
                    container.FindControl("VECTOR_ID_LABEL")).Text;
        }

        #endregion
    }
}
