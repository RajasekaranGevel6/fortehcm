﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SubscriptionManagementService.cs
// File that represents the Windows Service class that perform the subscription
// management actions. This includes downgrade expired subscriptions, send 
// expiry alerts, etc. This service runs at specified time that was defined
// in the config file.

#endregion

#region Directives                                                             

using System;
using System.Text;
using System.Timers;
using System.Configuration;
using System.ServiceProcess;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.SubscriptionManagementService
{
    /// <summary>
    /// Class that represents the Windows Service class that perform the 
    /// subscription management actions. This includes downgrade expired 
    /// subscriptions, send expiry alerts, etc. This service runs at specified
    /// time that was defined in the config file.
    /// </summary>
    public partial class ManagementService : ServiceBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="Timer"/> that holds the timer object.
        /// </summary>
        private Timer timer = null;

        /// <summary>
        /// A <see cref="string"/> that holds the outgoing email address.
        /// </summary>
        private string outgoingEmailAddress = null;

        /// <summary>
        /// A <see cref="string"/> that holds the admin email address.
        /// </summary>
        private string adminEmailAddress = null;

        /// <summary>
        /// A <see cref="string"/> that holds the copy email address.
        /// </summary>
        private string copyEmailAddress = null;

        /// <summary>
        /// A <seealso cref="int"/> that holds the user ID.
        /// </summary>
        private int userID = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the list of process hours. Multiple
        /// process hours can be set using comma separated format. When the 
        /// system reaches this hour, the process will takes place.
        /// </summary>
        private List<int> processHours = null;

        /// <summary>
        /// A <seealso cref="int"/> that holds the max expiry days.
        /// </summary>
        /// <remarks>
        /// If this is 5 then, users those who are expiring in the next
        /// 5 days will be considered, with respect to the given date.
        /// </remarks>
        private int maxExpiryDays = 0;

        /// <summary>
        /// A <seealso cref="int"/> that holds the downgrade subscription ID.
        /// </summary>
        private int downgradeSubscriptionID = 0;

        /// <summary>
        /// A <see cref="string"/> that holds the downgrade subscription code.
        /// This is currently 'free' subscription type, and can be changed in
        /// the config file.
        /// </summary>
        private string downgradeSubscriptionCode = null;

        /// <summary>
        /// A <see cref="SubscriptionBLManager"/> that holds the business layer
        /// component.
        /// </summary>
        private SubscriptionBLManager blManager = null;

        /// <summary>
        /// A <see cref="SubscriptionBLManager"/> that holds the email handler
        /// component.
        /// </summary>
        private EmailHandler emailHandler = null;

        #endregion Private Variables

        #region Constructor                                                    

        /// <summary>
        /// Method that represents the constructor.
        /// </summary>
        public ManagementService()
        {
            InitializeComponent();
        }

        #endregion Constructor

        #region Event Handlers                                                 

        /// <summary>
        /// Method that is called when the service is being started.
        /// </summary>
        /// <param name="args">
        /// An array of <see cref="string"/> that holds the arguments.
        /// </param>
        /// <remarks>
        /// This will start the ubscription management service timer.
        /// </remarks>
        protected override void OnStart(string[] args)
        {
            try
            {
                Logger.TraceLog("Trying to start subscription management service");

                // Instantiate the business layer component.
                blManager = new SubscriptionBLManager();

                // Instantiate the email handler component.
                emailHandler = new EmailHandler();

                // Check whether the user ID is valid or not.
                if (!int.TryParse(ConfigurationManager.AppSettings
                    ["USER_ID"], out userID))
                {
                    Logger.ExceptionLog("User ID not configured properly in config file. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Check whether the process hours is valid or not.
                if (Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                    ["PROCESS_HOURS"]))
                {
                    Logger.ExceptionLog("Process hour not configured properly in config file. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Split the process hours using comma.
                string []splitted = ConfigurationManager.AppSettings["PROCESS_HOURS"].Trim().Split(',');

                if (splitted == null || splitted.Length == 0)
                {
                    Logger.ExceptionLog("Process hour not configured properly in config file. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Instantiate the process hours list.
                processHours = new List<int>();

                // Add the process hour to the list.
                foreach (string split in splitted)
                {
                    try
                    {
                        processHours.Add(Convert.ToInt32(split));
                    }
                    catch
                    {
                        Logger.ExceptionLog("Process hour not configured properly in config file. " +
                            "Cannot start the subscription management service");

                        return;
                    }
                }

                // Check whether the max expiry days is valid or not.
                if (!int.TryParse(ConfigurationManager.AppSettings
                    ["MAX_EXPIRY_DAYS"], out maxExpiryDays))
                {
                    Logger.ExceptionLog("Max expiry days not configured properly in config file. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Check whether the downgrade subscription ID is valid or not.
                if (!int.TryParse(ConfigurationManager.AppSettings
                    ["DOWNGRADE_SUBSCRIPTION_ID"], out downgradeSubscriptionID))
                {
                    Logger.ExceptionLog("Downgrade subscription ID not configured properly in config file. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Check whether the downgrade subscription code is valid or not.
                if (Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                    ["DOWNGRADE_SUBSCRIPTION_CODE"]))
                {
                    Logger.ExceptionLog("Downgrade subscription code is not configured properly in config file. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Get the downgrade subscription code.
                downgradeSubscriptionCode = ConfigurationManager.AppSettings
                    ["DOWNGRADE_SUBSCRIPTION_CODE"].Trim(); 

                // Check whether the user ID is valid or not.
                if (new CommonBLManager().GetUserDetail(userID) == null)
                {
                    Logger.ExceptionLog("User ID present in the config file is not valid. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Check whether the outgoing email address is valid or not.
                if (Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                    ["OUTGOING_EMAIL_ADDRESS"]))
                {
                    Logger.ExceptionLog("Outgoing email address in the config file is empty. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Get the outgoing email address.
                outgoingEmailAddress = ConfigurationManager.AppSettings
                    ["OUTGOING_EMAIL_ADDRESS"].Trim();

                // Check whether the admin email address valid or not.
                if (Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                    ["ADMIN_EMAIL_ADDRESS"]))
                {
                    Logger.ExceptionLog("Admin email address in the config file is empty. " +
                        "Cannot start the subscription management service");

                    return;
                }

                // Get the admin email address.
                adminEmailAddress = ConfigurationManager.AppSettings
                    ["ADMIN_EMAIL_ADDRESS"].Trim();

                // Get the copy email address.
                copyEmailAddress = string.Empty;
                if (!Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                    ["COPY_EMAIL_ADDRESS"]))
                {
                    copyEmailAddress = ConfigurationManager.AppSettings
                        ["COPY_EMAIL_ADDRESS"].Trim();
                }

                // Instantiate email handler object.
                emailHandler = new EmailHandler();

                // Instantiate and start the timer.                
                timer = new Timer(3600);
                timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                timer.Start();

                Logger.TraceLog("Subscription management service started successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that is called when the service is being stopped.
        /// </summary>
        /// <remarks>
        /// This will stop the ubscription management service timer.
        /// </remarks>
        protected override void OnStop()
        {
            try
            {
                Logger.TraceLog("Trying to stop subscription management service");

                if (timer != null)
                {
                    timer.Stop();
                    timer = null;
                }

                Logger.TraceLog("Subscription management service stopped successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that is called when the subscription management service 
        /// timer is being elapsed.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that hold the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ElapsedEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will retrieve the send expiry and downgrade subscriptions
        /// if time of the the specific hour.
        /// </remarks>
        protected void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                // Check if the hour is the process hour.
                if (!IsProcessHour(e.SignalTime))
                    return;

                // Get the list of users expired or about to expiry.
                List<UserDetail> users = new SubscriptionBLManager().
                    GetExpiryUsers(maxExpiryDays, DateTime.Now);

                if (users == null || users.Count == 0)
                {
                    Logger.TraceLog(string.Format("No users found to process at {0}", e.SignalTime));
                    return;
                }

                Logger.TraceLog(string.Format("{0} users found to process at {0}", users.Count, e.SignalTime));

                // Send expiry alert emails.
                ProcessUsers(users);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that checks if the current time falls on the process hour.
        /// </summary>
        /// <param name="currentTime">
        /// A <see cref="DateTime"/> that holds the current time.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. True indicates the 
        /// current time falls under process hour and false indicates not.
        /// </returns>
        private bool IsProcessHour(DateTime currentTime)
        {
            if (processHours == null || processHours.Count == 0)
                return false;

            // Loop through the process hours and check if any one entry 
            // matches with the current hour.
            foreach (int hour in processHours)
            {
                if (hour == currentTime.TimeOfDay.Hours)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Method that the process the users. Based on the flag it will send
        /// expiry alert email or downgrade the subscription.
        /// </summary>
        /// <param name="users">
        /// A list of <see cref="UserDetail"/> that holds the users.
        /// </param>
        private void ProcessUsers(List<UserDetail> users)
        {
            try
            {
                foreach (UserDetail userDetail in users)
                {
                    Logger.TraceLog(string.Format("Started processing User# {0}: {1}",
                        userDetail.UserID, userDetail.UserName));

                    bool result = false;

                    if (userDetail.ExpiresInDays == 0)
                    {
                        // Downgrade subcription.
                        result = DowngradeSubscription(userDetail);
                    }
                    else
                    {
                        // Send expiry alert email.
                        result = SendExpiryAlertEmail(userDetail);
                    }

                    if (result)
                    {
                        Logger.TraceLog(string.Format("Successfully processed User# {0}: {1}",
                            userDetail.UserID, userDetail.UserName));
                    }
                    else
                    {
                        Logger.TraceLog(string.Format("Failed in processing User# {0}: {1}",
                        userDetail.UserID, userDetail.UserName));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that sends the expiry alert email.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. True returns success
        /// and false failure.
        /// </returns>
        private bool SendExpiryAlertEmail(UserDetail userDetail)
        {
            try
            {
                string subject = string.Empty;
                string message = string.Empty;

                // Compose the email.
                ComposeMail(userDetail, out subject, out message);

                // Send mail.
                if (copyEmailAddress == string.Empty)
                    emailHandler.SendMail(outgoingEmailAddress, userDetail.Email, subject, message);
                else
                    emailHandler.SendMail(outgoingEmailAddress, userDetail.Email, copyEmailAddress, subject, message);

                return true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                return false;
            }
        }

        /// <summary>
        /// Method that downgrade the subscription.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. True returns success
        /// and false failure.
        /// </returns>
        private bool DowngradeSubscription(UserDetail userDetail)
        {
            try
            {
                // Downgrade the subscription.
                blManager.UpdateUserSubscription(userDetail.UserID,
                    downgradeSubscriptionID, downgradeSubscriptionCode);

                // Send mail to the user indicating that the subcription was
                // downgraded.
                SendExpiryAlertEmail(userDetail);

                return true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                return false;
            }
        }

        /// <summary>
        /// Method that compose the mail for given parameters.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject as an output 
        /// parameter.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message as an output 
        /// parameter.
        /// </param>
        /// <remarks>
        /// If 'ExpiresInDays' property of the user detail object indicates
        /// the mail type. If this is 0 then compose a 'expired' type, else
        /// 'about to expire' type.
        /// </remarks>
        private void ComposeMail(UserDetail userDetail, out string subject, out string message)
        {
            // Initiate the output parameters.
            subject = string.Empty;
            message = string.Empty;

            StringBuilder body = new StringBuilder();
            if (userDetail == null)
                return;

            if (userDetail.ExpiresInDays == 0)
            {
                // Compose subject.
                subject = "ForteHCM - Your account has been expired";

                // Compose message.
                body.Append(string.Format("Dear {0} {1}", userDetail.FirstName,
                    userDetail.LastName).Trim() + ",");
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("Your account has been expired and downgraded to {0}",
                    GetSubscriptionName()));
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append("You can still use the application but with limited features");
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("Contact your administrator at {0} for further details",
                    adminEmailAddress));
            }
            else
            {
                // Compose subject.
                subject = string.Format("ForteHCM - Your account will expire in {0} day(s)",
                    userDetail.ExpiresInDays);

                // Compose message.
                body.Append(string.Format("Dear {0} {1}", userDetail.FirstName,
                    userDetail.LastName).Trim() + ",");
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("You have only {0} more day(s) to use your subscription", userDetail.ExpiresInDays));
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("Your account will expire on {0}", userDetail.ExpiryDate.ToString("dd/MMM/yyyy")));
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append("Upgrade your account to retain your subscription");
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("Contact your administrator at {0} for further details",
                    adminEmailAddress));
            }

            // Closure message.
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append(string.Format("This is a system generated message"));
            body.Append(Environment.NewLine);
            body.Append(string.Format("No need to reply to this message"));

            // Get disclaimer message.
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("============================== Disclaimer ==============================");
            body.Append(Environment.NewLine);
            body.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
            body.Append(Environment.NewLine);
            body.Append("otherwise private information. If you have received it in error, please notify the sender");
            body.Append(Environment.NewLine);
            body.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
            body.Append(Environment.NewLine);
            body.Append("=========================== End Of Disclaimer ============================");

            // Assign the message.
            message = body.ToString();
        }

        /// <summary>
        /// Method that retrieves the subscription name based on the downgrade
        /// subscription code.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the subscription name.
        /// </returns>
        private string GetSubscriptionName()
        {
            switch (downgradeSubscriptionCode.ToUpper().Trim())
            {
                case "SR_COR_AMN":
                    return "Corporate";

                case "SR_COR_USR":
                    return "Corporate";

                case "SR_FRE":
                    return "Free";

                case "SR_STA":
                    return "Standard";

                default:
                    return "Free";
            }
        }

        #endregion Private Methods
    }
}
