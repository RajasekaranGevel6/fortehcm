﻿using System;
using System.Text;
using System.Timers;
using System.Configuration;
using System.ServiceProcess;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Exceptions;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

namespace Forte.HCM.SubscriptionManagementService
{
    public class SubscriptionManagementServiceConsole
    {
        /// <summary>
        /// A <see cref="string"/> that holds the outgoing email address.
        /// </summary>
        private static string outgoingEmailAddress = null;

        /// <summary>
        /// A <see cref="string"/> that holds the admin email address.
        /// </summary>
        private static string adminEmailAddress = null;

        /// <summary>
        /// A <see cref="string"/> that holds the copy email address.
        /// </summary>
        private static string copyEmailAddress = null;

        /// <summary>
        /// A <seealso cref="int"/> that holds the user ID.
        /// </summary>
        private static int userID = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the list of process hours. Multiple
        /// process hours can be set using comma separated format. When the 
        /// system reaches this hour, the process will takes place.
        /// </summary>
        private static List<int> processHours = null;

        /// <summary>
        /// A <seealso cref="int"/> that holds the max expiry days.
        /// </summary>
        /// <remarks>
        /// If this is 5 then, users those who are expiring in the next
        /// 5 days will be considered, with respect to the given date.
        /// </remarks>
        private static int maxExpiryDays = 0;

        /// <summary>
        /// A <seealso cref="int"/> that holds the downgrade subscription ID.
        /// </summary>
        private static int downgradeSubscriptionID = 0;

        /// <summary>
        /// A <see cref="string"/> that holds the downgrade subscription code.
        /// This is currently 'free' subscription type, and can be changed in
        /// the config file.
        /// </summary>
        private static string downgradeSubscriptionCode = null;

        /// <summary>
        /// A <see cref="SubscriptionBLManager"/> that holds the business layer
        /// component.
        /// </summary>
        private static SubscriptionBLManager blManager = null;

        /// <summary>
        /// A <see cref="SubscriptionBLManager"/> that holds the email handler
        /// component.
        /// </summary>
        private static EmailHandler emailHandler = null;

        public static void _Main(string[] args)
        {
            // Instantiate the business layer component.
            blManager = new SubscriptionBLManager();

            // Instantiate the email handler component.
            emailHandler = new EmailHandler();

            // Check whether the user ID is valid or not.
            if (!int.TryParse(ConfigurationManager.AppSettings
                ["USER_ID"], out userID))
            {
                Logger.ExceptionLog("User ID not configured properly in config file. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Check whether the process hours is valid or not.
            if (Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                ["PROCESS_HOURS"]))
            {
                Logger.ExceptionLog("Process hour not configured properly in config file. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Split the process hours using comma.
            string[] splitted = ConfigurationManager.AppSettings["PROCESS_HOURS"].Trim().Split(',');

            if (splitted == null || splitted.Length == 0)
            {
                Logger.ExceptionLog("Process hour not configured properly in config file. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Instantiate the process hours list.
            processHours = new List<int>();

            // Add the process hour to the list.
            foreach (string split in splitted)
            {
                try
                {
                    processHours.Add(Convert.ToInt32(split));
                }
                catch
                {
                    Logger.ExceptionLog("Process hour not configured properly in config file. " +
                        "Cannot start the subscription management service");

                    return;
                }
            }

            // Check whether the max expiry days is valid or not.
            if (!int.TryParse(ConfigurationManager.AppSettings
                ["MAX_EXPIRY_DAYS"], out maxExpiryDays))
            {
                Logger.ExceptionLog("Max expiry days not configured properly in config file. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Check whether the downgrade subscription ID is valid or not.
            if (!int.TryParse(ConfigurationManager.AppSettings
                ["DOWNGRADE_SUBSCRIPTION_ID"], out downgradeSubscriptionID))
            {
                Logger.ExceptionLog("Downgrade subscription ID not configured properly in config file. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Check whether the downgrade subscription code is valid or not.
            if (Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                ["DOWNGRADE_SUBSCRIPTION_CODE"]))
            {
                Logger.ExceptionLog("Downgrade subscription code is not configured properly in config file. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Get the downgrade subscription code.
            downgradeSubscriptionCode = ConfigurationManager.AppSettings
                ["DOWNGRADE_SUBSCRIPTION_CODE"].Trim();

            // Check whether the user ID is valid or not.
            if (new CommonBLManager().GetUserDetail(userID) == null)
            {
                Logger.ExceptionLog("User ID present in the config file is not valid. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Check whether the outgoing email address is valid or not.
            if (Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                ["OUTGOING_EMAIL_ADDRESS"]))
            {
                Logger.ExceptionLog("Outgoing email address in the config file is empty. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Get the outgoing email address.
            outgoingEmailAddress = ConfigurationManager.AppSettings
                ["OUTGOING_EMAIL_ADDRESS"].Trim();

            // Check whether the admin email address valid or not.
            if (Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                ["ADMIN_EMAIL_ADDRESS"]))
            {
                Logger.ExceptionLog("Admin email address in the config file is empty. " +
                    "Cannot start the subscription management service");

                return;
            }

            // Get the admin email address.
            adminEmailAddress = ConfigurationManager.AppSettings
                ["ADMIN_EMAIL_ADDRESS"].Trim();

            // Get the copy email address.
            copyEmailAddress = string.Empty;
            if (!Utility.IsNullOrEmpty(ConfigurationManager.AppSettings
                ["COPY_EMAIL_ADDRESS"]))
            {
                copyEmailAddress = ConfigurationManager.AppSettings
                    ["COPY_EMAIL_ADDRESS"].Trim();
            }

            // Get the list of users expired or about to expiry.
            List<UserDetail> users = new SubscriptionBLManager().
                GetExpiryUsers(maxExpiryDays, DateTime.Now);

            if (users == null || users.Count == 0)
            {
                return;
            }

            // Send expiry alert emails.
            ProcessUsers(users);
        }

        /// <summary>
        /// Method that the process the users. Based on the flag it will send
        /// expiry alert email or downgrade the subscription.
        /// </summary>
        /// <param name="users">
        /// A list of <see cref="UserDetail"/> that holds the users.
        /// </param>
        private static void ProcessUsers(List<UserDetail> users)
        {
            try
            {
                foreach (UserDetail userDetail in users)
                {
                    if (userDetail.ExpiresInDays == 0)
                    {
                        // Downgrade subcription.
                        DowngradeSubscription(userDetail);
                    }
                    else
                    {
                        // Send expiry alert email.
                        SendExpiryAlertEmail(userDetail);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that sends the expiry alert email.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        private static void SendExpiryAlertEmail(UserDetail userDetail)
        {
            try
            {
                string subject = string.Empty;
                string message = string.Empty;

                // Compose the email.
                ComposeMail(userDetail, out subject, out message);

                // Send mail.
                if (copyEmailAddress == string.Empty)
                    emailHandler.SendMail(outgoingEmailAddress, userDetail.Email, subject, message);
                else
                    emailHandler.SendMail(outgoingEmailAddress, userDetail.Email, copyEmailAddress, subject, message);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that downgrade the subscription.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        private static void DowngradeSubscription(UserDetail userDetail)
        {
            try
            {
                // Downgrade the subscription.
                blManager.UpdateUserSubscription(userDetail.UserID,
                    downgradeSubscriptionID, downgradeSubscriptionCode);

                // Send mail to the user indicating that the subcription was
                // downgraded.
                SendExpiryAlertEmail(userDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that compose the mail for given parameters.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject as an output 
        /// parameter.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message as an output 
        /// parameter.
        /// </param>
        /// <remarks>
        /// If 'ExpiresInDays' property of the user detail object indicates
        /// the mail type. If this is 0 then compose a 'expired' type, else
        /// 'about to expire' type.
        /// </remarks>
        private static void ComposeMail(UserDetail userDetail, out string subject, out string message)
        {
            // Initiate the output parameters.
            subject = string.Empty;
            message = string.Empty;

            StringBuilder body = new StringBuilder();
            if (userDetail == null)
                return;

            if (userDetail.ExpiresInDays == 0)
            {
                // Compose subject.
                subject = "ForteHCM - Your account has been expired";

                // Compose message.
                body.Append(string.Format("Dear {0} {1}", userDetail.FirstName,
                    userDetail.LastName).Trim() + ",");
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("Your account has been expired and downgraded to {0}",
                    GetSubscriptionName()));
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append("You can still use the application but with limited features");
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("Contact your administrator at {0} for further details",
                    adminEmailAddress));
            }
            else
            {
                // Compose subject.
                subject = string.Format("ForteHCM - Your account will expire in {0} day(s)",
                    userDetail.ExpiresInDays);

                // Compose message.
                body.Append(string.Format("Dear {0} {1}", userDetail.FirstName,
                    userDetail.LastName).Trim() + ",");
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("You have only {0} more day(s) to use your subscription", userDetail.ExpiresInDays));
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("Your account will expire on {0}", userDetail.ExpiryDate.ToString("dd/MMM/yyyy")));
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append("Upgrade your account to retain your subscription");
                body.Append(Environment.NewLine);
                body.Append(Environment.NewLine);
                body.Append(string.Format("Contact your administrator at {0} for further details",
                    adminEmailAddress));
            }

            // Closure message.
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append(string.Format("This is a system generated message"));
            body.Append(Environment.NewLine);
            body.Append(string.Format("No need to reply to this message"));

            // Get disclaimer message.
            body.Append(Environment.NewLine);
            body.Append(Environment.NewLine);
            body.Append("============================== Disclaimer ==============================");
            body.Append(Environment.NewLine);
            body.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
            body.Append(Environment.NewLine);
            body.Append("otherwise private information. If you have received it in error, please notify the sender");
            body.Append(Environment.NewLine);
            body.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
            body.Append(Environment.NewLine);
            body.Append("=========================== End Of Disclaimer ============================");

            // Assign the message.
            message = body.ToString();
        }

        /// <summary>
        /// Method that retrieves the subscription name based on the downgrade
        /// subscription code.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the subscription name.
        /// </returns>
        private static string GetSubscriptionName()
        {
            switch (downgradeSubscriptionCode.ToUpper().Trim())
            {
                case "SR_COR_AMN":
                    return "Corporate";

                case "SR_COR_USR":
                    return "Corporate";

                case "SR_FRE":
                    return "Free";

                case "SR_STA":
                    return "Standard";

                default:
                    return "Free";
            }
        }
    }
}
