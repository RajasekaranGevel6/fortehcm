﻿namespace Forte.HCM.SubscriptionManagementService
{
    partial class ServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServiceProcessInstaller_subscriptionManagementServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ServiceInstaller_subscriptionManagementServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ServiceProcessInstaller_subscriptionManagementServiceProcessInstaller
            // 
            this.ServiceProcessInstaller_subscriptionManagementServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ServiceProcessInstaller_subscriptionManagementServiceProcessInstaller.Password = null;
            this.ServiceProcessInstaller_subscriptionManagementServiceProcessInstaller.Username = null;
            // 
            // ServiceInstaller_subscriptionManagementServiceInstaller
            // 
            this.ServiceInstaller_subscriptionManagementServiceInstaller.Description = "Runs once on a day and perform the subscription management actions. This includes" +
                " downgrade expired subscriptions, send expiry alerts, etc.";
            this.ServiceInstaller_subscriptionManagementServiceInstaller.DisplayName = "ForteHCM Subscription Management Service";
            this.ServiceInstaller_subscriptionManagementServiceInstaller.ServiceName = "ForteHCM Subscription Management Service";
            this.ServiceInstaller_subscriptionManagementServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.ServiceInstaller_subscriptionManagementServiceInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ServiceInstaller_subscriptionManagementServiceInstaller_AfterInstall);
            // 
            // ServiceInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ServiceProcessInstaller_subscriptionManagementServiceProcessInstaller,
            this.ServiceInstaller_subscriptionManagementServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ServiceProcessInstaller_subscriptionManagementServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ServiceInstaller_subscriptionManagementServiceInstaller;
    }
}