﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ServiceInstaller.cs
// File that represents the ServiceInstaller class that installs the 
// subscription management service.

#endregion

#region Directives                                                             

using System;
using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;

using Forte.HCM.Trace;

#endregion Directives

namespace Forte.HCM.SubscriptionManagementService
{
    /// <summary>
    /// Class that represents the service installer that installs the 
    /// subscription management Windows service.
    /// </summary>
    [RunInstaller(true)]
    public partial class ServiceInstaller : Installer
    {
        #region Constructor                                                    

        /// <summary>
        /// Method that represents the constructor.
        /// </summary>
        public ServiceInstaller()
        {
            InitializeComponent();
        }

        #endregion Constructor

        #region Event Handlers                                                 

        /// <summary>
        /// Method that is called after the service was installed. 
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="InstallEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will start the subscription management Windows service, once it 
        /// get installed.
        /// </remarks>
        private void ServiceInstaller_subscriptionManagementServiceInstaller_AfterInstall
            (object sender, InstallEventArgs e)
        {
            try
            {
                ServiceController service = new ServiceController
                    ("ForteHCM Subscription Management Service");
                service.Start();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Event Handlers
    }
}
