﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Program.cs
// File that represents the executable program that start the failed 
// email sender executable.

#endregion

#region Directives

using System;
using System.Windows.Forms;

#endregion Directives

namespace Forte.HCM.FailedEmailSender
{
    /// <summary>
    /// Class that represents the executable program that start the failed 
    /// eamil sender executable.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Sender());
        }
    }
}
