﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Sender.cs
// File that represents the user interface layout and functionalities
// for the Sender form. This form helps in resending failed emails.

#endregion Header

#region Directives

using System;
using System.Configuration;
using System.Windows.Forms;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.FailedEmailSender
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the Sender form. This form helps in resending failed emails.
    /// </summary>
    /// <remarks>
    /// This form will not be shown to the users. This executable is assigned
    /// to a scheduler and invokes at specicied time.
    /// </remarks>
    public partial class Sender : Form
    {
        /// <summary>
        /// A <see cref="int"/> that holds the default max attempt.
        /// </summary>
        private const int DEFAULT_MAX_ATTEMPT = 3;

        /// <summary>
        /// Method that represents the constructor.
        /// </summary>
        public Sender()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that is called when the form is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void Sender_Load(object sender, EventArgs e)
        {
            try
            {
                // Hide the window.
                this.Visible = false;

                Logger.TraceLog("Started failed email sender @" + DateTime.Now);

                // Create an email handler object.
                EmailHandler emailHandler = new EmailHandler();

                // Create a BL manager object.
                ResumeRepositoryBLManager blManager = new ResumeRepositoryBLManager();

                // Retrieve max attempt.
                int maxAttempt = 0;

                if (!int.TryParse(ConfigurationManager.AppSettings["MAX_ATTEMPT"], out maxAttempt))
                {
                    // Set the default max limit.
                    maxAttempt = DEFAULT_MAX_ATTEMPT;
                }

                #region Resend Failed Emails

                // Retrieve the list of failed emails.
                List<FailedEmailDetail> failedEmails = blManager.GetFailedEmails(maxAttempt);

                if (failedEmails == null || failedEmails.Count == 0)
                {
                    Logger.TraceLog("No failed emails found to resend");
                }
                else
                {
                    Logger.TraceLog(string.Format("{0} failed emails found to resend", failedEmails.Count));

                    // Loop through the list and resend the failed emails.
                    foreach (FailedEmailDetail failedEmail in failedEmails)
                    {
                        try
                        {
                            emailHandler.SendMail(EntityType.FailedEmailResend, failedEmail);

                            // After successful sending, delete the failed log.
                            blManager.DeleteFailedEmail(failedEmail.FailureID);
                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);

                            // On failure update the attempt. 
                            int attempt = failedEmail.Attempt + 1;
                            blManager.UpdateFailedEmailAttempt(failedEmail.FailureID, attempt);

                            // When max limit is reached, send the mail to admin.
                            if (attempt == maxAttempt)
                            {
                                try
                                {
                                    emailHandler.SendMail(EntityType.FailedEmailToAdmin, failedEmail);
                                }
                                catch (Exception failedExp)
                                {
                                    Logger.TraceLog("Sending failed email to admin failed");
                                    Logger.ExceptionLog(failedExp);
                                }
                            }
                        }
                    }
                }

                #endregion Resend Failed Emails

                Logger.TraceLog("Stopped failed email sender @" + DateTime.Now);

                // Close the application.
                this.Close();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                Logger.TraceLog("Stopped failed email sender @" + DateTime.Now);

                // Close the application.
                this.Close();
            }
        }
    }
}
