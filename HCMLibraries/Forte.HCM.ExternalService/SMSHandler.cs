﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SMSHandler.cs
// File that represents the SMS manager utility. This file provides sending 
// mails on various scenarios.

#endregion

#region Directives

using System;
using System.IO;
using System.Net;
using System.Web;
using System.Text;
using System.Linq;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Generic;
using System.Security.Cryptography;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support; 
using Forte.HCM.Exceptions;
using Forte.HCM.DataObjects;
using Forte.HCM.Common.DL;
using System.Threading;
using System.Collections.Specialized;

#endregion Directives

namespace Forte.HCM.ExternalService
{
    /// <summary>
    /// Class that represents the SMS manager utility. This class provides 
    /// functionalities to send SMS on various scenarios.
    /// </summary>
    public class SMSHandler
    {
        #region Public Methods

        /// <summary>
        /// Method that compose and send SMS for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        public void SendSMS(EntityType entityType, string entityID)
        {
            try
            {
                SMSDetail smsDetail = ComposeSMS(entityType, entityID, null);

                if (smsDetail == null)
                    return;

                Send(smsDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that compose and send SMS for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        /// <param name="subEntityID">
        /// A <see cref="string"/> that holds the sub entity ID.
        /// </param>
        public void SendSMS(EntityType entityType, string entityID, string subEntityID)
        {
            try
            {
                SMSDetail smsDetail = ComposeSMS(entityType, entityID, subEntityID);

                if (smsDetail == null)
                    return;

                Send(smsDetail);
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Method that send the mail.
        /// </summary>
        /// <param name="message">
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </param>
        private void Send(SMSDetail smsDetail)
        {
            // Do not send SMS, if flag is switched off.
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["SEND_SMS"]) != true)
                return;

            try
            {
                using (var wb = new WebClient())
                {
                    byte[] response = wb.UploadValues(ConfigurationManager.AppSettings["TEXT_LOCAL_URL"], new NameValueCollection()
                    {
                        {"apikey" , ConfigurationManager.AppSettings["TEXT_LOCAL_SMS_API_KEY"]},
                        {"numbers" , smsDetail.To},
                        {"message" , smsDetail.Message},
                        {"sender" , ConfigurationManager.AppSettings["TEXT_LOCAL_SENDER"]}
                        });
                    
                    Logger.TraceLog(System.Text.Encoding.UTF8.GetString(response));
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that compose the <see cref="SMSDetail"/> object for the given entity ID and entity type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        /// <param name="subEntityID">
        /// A <see cref="string"/> that holds the sub entity ID.
        /// </param>
        /// <returns>
        /// A <see cref="SMSDetail"/> that holds the SMS detail.
        /// </returns>
        private SMSDetail ComposeSMS(EntityType entityType, string entityID, string subEntityID)
        {
            SMSDetail smsDetail = null;

            try
            {
                switch (entityType)
                {
                    case EntityType.CandidateScheduled:
                    case EntityType.CandidateReScheduled:
                        {
                            #region CandidateScheduled & CandidateReScheduled
                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                            smsDetail = new SMSDetail();

                            CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerDLManager().
                                GetCandidateTestSessionEmailDetail(entityID, attemptID);

                            if (candidateTestSessionDetail == null)
                                break;

                            smsDetail.To = candidateTestSessionDetail.CandidateMobile;
                            smsDetail.Message = string.Format("Dear {0},", candidateTestSessionDetail.CandidateName);
                            smsDetail.Message += Environment.NewLine;

                            if (entityType == EntityType.CandidateScheduled)
                            {
                                smsDetail.Message += string.Format("Test '{0} - {1}' scheduled for you",
                                    candidateTestSessionDetail.TestID,
                                    candidateTestSessionDetail.TestName);
                            }
                            else if (entityType == EntityType.CandidateReScheduled)
                            {
                                smsDetail.Message += string.Format("Test '{0} - {1}' re-scheduled for you",
                                    candidateTestSessionDetail.TestID,
                                    candidateTestSessionDetail.TestName);
                            }
                            smsDetail.Message += Environment.NewLine;
                            smsDetail.Message += "Please check your mail for more detail";

                            #endregion CandidateScheduled & CandidateReScheduled
                        }
                        break;


                    default:
                        break;
                }

                return smsDetail;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        #endregion Private Methods
    }
}