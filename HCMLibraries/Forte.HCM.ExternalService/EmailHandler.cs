﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EmailHandler.cs
// File that represents the email manager utility. This file provides sending 
// mails on various scenarios like, test session created, candidate scheduled 
// etc. 

#endregion

#region Directives

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Generic;
using System.Security.Cryptography;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Exceptions;
using Forte.HCM.DataObjects;
using Forte.HCM.Common.DL;
using System.Threading;

#endregion Directives

namespace Forte.HCM.ExternalService
{
    /// <summary>
    /// Class that represents the email manager utility. This class provides 
    /// functionalities to send mails on various scenarios like, test 
    /// session created, candidate scheduled etc. This class also provides
    /// additional feature to construct mail subject and message for various
    /// scenarios.
    /// </summary>
    public class EmailHandler
    {
        #region Public Methods

        /// <summary>
        /// Method that compose and send for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        public void SendMail(EntityType entityType, string entityID)
        {
            try
            {
                MailDetail mailDetail = ComposeMail(entityType, entityID, null);

                if (mailDetail == null)
                    return;

                Send(GetMailMessage(mailDetail));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that compose and send for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        /// <param name="ccAddress">
        /// A <see cref="string"/> that holds the CC address.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        public void SendMail(EntityType entityType, string entityID,
            string ccAddress, string message)
        {
            try
            {
                MailDetail mailDetail = ComposeMail(entityType, entityID, null, ccAddress, message);

                Send(GetMailMessage(mailDetail));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that compose and send for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data. This will be type 
        /// casted to specific type based on entity type.
        /// </param>
        public void SendMail(EntityType entityType, object data)
        {
            try
            {
                MailDetail mailDetail = ComposeMail(entityType, data);

                if (mailDetail == null)
                    return;

                Send(GetMailMessage(mailDetail));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that compose and send for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        /// <param name="subEntityID">
        /// A <see cref="string"/> that holds the sub entity ID.
        /// </param>
        public void SendMail(EntityType entityType, string entityID, string subEntityID, int tenantID = 2)
        {
            try
            {
                MailDetail mailDetail = ComposeMail(entityType, entityID, subEntityID, tenantID);
                if(mailDetail!=null)
                    Send(GetMailMessage(mailDetail));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that send mail based on the details given.
        /// </summary>
        /// <param name="mailDetail">
        /// A <see cref="MailDetail"/> that holds the mail detail.
        /// </param>
        public void SendMail(MailDetail mailDetail)
        {
            try
            {
                Send(GetMailMessage(mailDetail));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that send mail based on the details given.
        /// </summary>
        /// <param name="toAddress">
        /// A <see cref="string"/> that holds the to-address.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        public void SendMail(string toAddress, string subject, string message)
        {
            try
            {
                Send(GetMailMessage(
                    ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"],
                    toAddress, null, subject, message));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that send mail based on the details given.
        /// </summary>
        /// <param name="toAddress">
        /// A <see cref="string"/> that holds the to-address.
        /// </param>
        /// <param name="ccAddress">
        /// A <see cref="string"/> that holds the cc-address.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        public void SendMail(string toAddress, string ccAddress, string subject, string message)
        {
            try
            {
                Send(GetMailMessage(
                    ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"],
                    toAddress, ccAddress, subject, message));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that send mail based on the details given.
        /// </summary>
        /// <param name="fromAddress">
        /// A <see cref="string"/> that holds the from-address.
        /// </param>
        /// <param name="toAddress">
        /// A <see cref="string"/> that holds the to-address.
        /// </param>
        /// <param name="ccAddress">
        /// A <see cref="string"/> that holds the cc-address.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        public void SendMail(string fromAddress, string toAddress,
            string ccAddress, string subject, string message)
        {
            try
            {
                Send(GetMailMessage
                    (fromAddress, toAddress, ccAddress, subject, message));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                if (exp.Message.Contains("rejected for policy reasons"))
                    throw new Exception("<br>Mail send rejected due to policy reasons");
                else if (exp.Message.Contains("Failure sending mail"))
                    throw new Exception("<br>The connection to the SMTP server failed");
                else
                    throw new Exception("<br>Unable to send mail.");

                //throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that send mail based on the details given.
        /// </summary>
        /// <param name="toAddresses">
        /// A <see cref="string"/> that holds the list of to-address.
        /// </param>
        /// <param name="ccAddresses">
        /// A <see cref="string"/> that holds the list of cc-address.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        public void SendMail(List<string> toAddresses,
            List<string> ccAddresses, string subject, string message)
        {
            try
            {
                Send(GetMailMessage
                    (toAddresses, ccAddresses, subject, message));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        /// <summary>
        /// Method that send mail based on the details given.
        /// </summary>
        /// <param name="toAddresses">
        /// A <see cref="string"/> that holds the list of to-address.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        public void SendMail(List<string> toAddresses,
            string subject, string message)
        {
            try
            {
                Send(GetMailMessage
                    (toAddresses, null, subject, message));
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Method that construct and returns the <see cref="MailMessage"/>
        /// object for the given <see cref="MailDetail"/> object.
        /// </summary>
        /// <param name="mailDetail">
        /// A <see cref="MailDetail"/> that holds the mail detail.
        /// </param>
        /// <returns>
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </returns>
        private MailMessage GetMailMessage(MailDetail mailDetail)
        {
            MailMessage mailMessage = new MailMessage();

            // From address.
            if (Utility.IsNullOrEmpty(mailDetail.From))
            {
                mailMessage.From = new MailAddress(ConfigurationManager.
                    AppSettings["EMAIL_FROM_ADDRESS"]);
            }
            else
            {
                mailMessage.From = new MailAddress(mailDetail.From);
            }

            // To address.
            if (!Utility.IsNullOrEmpty(mailDetail.To) && mailDetail.To.Count != 0)
            {
                foreach (string toAddress in mailDetail.To)
                    mailMessage.To.Add(new MailAddress(toAddress));
            }

            // CC address.
            if (!Utility.IsNullOrEmpty(mailDetail.CC) && mailDetail.CC.Count != 0)
            {
                foreach (string ccAddress in mailDetail.CC)
                    mailMessage.CC.Add(new MailAddress(ccAddress));
            }

            // BCC address.
            if (!Utility.IsNullOrEmpty(mailDetail.BCC) && mailDetail.BCC.Count != 0)
            {
                foreach (string bccAddress in mailDetail.BCC)
                    mailMessage.Bcc.Add(new MailAddress(bccAddress));
            }

            // Subject.
            mailMessage.Subject = mailDetail.Subject;

            // Message.
            mailMessage.Body = mailDetail.Message;
            mailMessage.IsBodyHtml = mailDetail.isBodyHTML;
            if (mailDetail.isBodyHTMLPositionProfile && mailMessage.IsBodyHtml)
            {
                mailMessage.Body = mailMessage.Body.Replace("\r\n", "<br />");
                mailMessage.Body = mailMessage.Body.Replace("\n\n", "<br /><br />");
            }

            if (mailDetail.Attachments != null)
            {
                foreach (MailAttachment mailAttachment in mailDetail.Attachments)
                {
                    if (mailAttachment.FileContent == null ||
                        mailAttachment.FileContent.Length == 0)
                    {
                        continue;
                    }

                    MemoryStream memoryStream = new MemoryStream(mailAttachment.FileContent);
                    mailMessage.Attachments.Add(new Attachment(memoryStream,
                        mailAttachment.FileName, GetMimeType(mailAttachment.FileName.Substring
                        (mailAttachment.FileName.LastIndexOf('.') + 1))));
                }
            }

            return mailMessage;
        }

        /// <summary>
        /// Method that construct and returns the <see cref="MailMessage"/>
        /// object for the given details.
        /// </summary>
        /// <param name="fromAddress">
        /// A <see cref="string"/> that holds the from-address.
        /// </param>
        /// <param name="toAddress">
        /// A <see cref="string"/> that holds the to-address.
        /// </param>
        /// <param name="ccAddress">
        /// A <see cref="string"/> that holds the cc-address.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        /// <returns>
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </returns>
        private MailMessage GetMailMessage(string fromAddress, string toAddress,
            string ccAddress, string subject, string message)
        {
            MailMessage mailMessage = new MailMessage();

            // From address.
            mailMessage.From = new MailAddress(fromAddress);

            // To address.
            if (!Utility.IsNullOrEmpty(toAddress))
                mailMessage.To.Add(new MailAddress(toAddress));

            // CC address.
            if (!Utility.IsNullOrEmpty(ccAddress))
            {
                foreach (string cc in ccAddress.Split(new char[] { ',' }))
                {
                    mailMessage.CC.Add(new MailAddress(cc));
                }
            }

            // Subject.
            mailMessage.Subject = subject;

            // Message.
            mailMessage.Body = message.Replace("\r\n", "<br />");
            mailMessage.IsBodyHtml = true;

            return mailMessage;
        }

        /// <summary>
        /// Method that construct and returns the <see cref="MailMessage"/>
        /// object for the given details.
        /// </summary>
        /// <param name="toAddresses">
        /// A <see cref="string"/> that holds lsit of the to-address.
        /// </param>
        /// <param name="ccAddresses">
        /// A <see cref="string"/> that holds the list of cc-address.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        /// <returns>
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </returns>
        private MailMessage GetMailMessage(List<string> toAddresses,
            List<string> ccAddresses, string subject, string message)
        {
            MailMessage mailMessage = new MailMessage();

            // From address.
            mailMessage.From = new MailAddress(ConfigurationManager.
                AppSettings["EMAIL_FROM_ADDRESS"]);

            // To address.
            if (toAddresses != null && toAddresses.Count != 0)
            {
                foreach (string toAddress in toAddresses)
                    mailMessage.To.Add(new MailAddress(toAddress));
            }

            // CC address.
            if (ccAddresses != null && ccAddresses.Count != 0)
            {
                foreach (string ccAddress in ccAddresses)
                    mailMessage.CC.Add(new MailAddress(ccAddress));
            }

            // Subject.
            mailMessage.Subject = subject;
            mailMessage.IsBodyHtml = true;
            // Message.
            mailMessage.Body = message.Replace("\r\n", "<br />");
            return mailMessage;
        }

        /// <summary>
        /// Method that send the mail.
        /// </summary>
        /// <param name="message">
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </param>
        private void Send(MailMessage message)
        {
            SmtpClient smtpClient = new SmtpClient(
                ConfigurationManager.AppSettings["SMTP_CLIENT"],
                Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_PORT"]));
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(
                ConfigurationManager.AppSettings["SMTP_USER"],
                ConfigurationManager.AppSettings["SMTP_PASSWORD"]);

            smtpClient.ServicePoint.MaxIdleTime = Convert.ToInt32
                (ConfigurationManager.AppSettings["MAX_IDLE_TIME"]);

            smtpClient.EnableSsl = Convert.ToBoolean
                (ConfigurationManager.AppSettings["USE_SECURE_SOCKET_LAYER"]);

            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            try
            {
                smtpClient.Send(message);
            }
            catch (SmtpFailedRecipientsException smtpFailedRecipientsException)
            {
                string failedRecipientList = string.Empty;
                for (int i = 0; i < smtpFailedRecipientsException.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = smtpFailedRecipientsException.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy
                            || status == SmtpStatusCode.MailboxUnavailable)
                    {
                        if (failedRecipientList == string.Empty)
                            failedRecipientList += GetMailID(smtpFailedRecipientsException.InnerExceptions[i].FailedRecipient);
                        else
                            failedRecipientList += ", " + GetMailID(smtpFailedRecipientsException.InnerExceptions[i].FailedRecipient);
                    }
                }

                EmailFailureLog(message, failedRecipientList, smtpFailedRecipientsException.InnerException.ToString(), 1);

                throw new EMailException(string.Format
                    ("<br>The message could not be delivered to the recipient(s) {0}", failedRecipientList));
            }
            catch (SmtpException smtpException)
            {
                Logger.ExceptionLog(smtpException);

                string emailFailedList = message.To.ToString() + "," + message.CC.ToString();

                EmailFailureLog(message, emailFailedList, smtpException.InnerException.ToString(), 1);

                if (smtpException.StatusCode == SmtpStatusCode.MailboxUnavailable)
                {
                    throw new EMailException(string.Format
                        ("<br>The message could not be delivered to the recipient(s) {0}",
                        GetMailID(((SmtpFailedRecipientException)(smtpException)).FailedRecipient)));
                }
                else
                {
                    throw smtpException;
                }
            }
        }

        private string GetMailID(string recipient)
        {
            if (Utility.IsNullOrEmpty(recipient))
                return string.Empty;

            return recipient.Replace("<", string.Empty).Replace(">", string.Empty);
        }

        /// <summary>
        /// Method that compose the <see cref="MailDetail"/> object for the 
        /// given entity ID and entity type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data. This will be type 
        /// casted to specific type based on entity type.
        /// </param>
        /// <returns>
        /// A <see cref="MailDetail"/> that holds the mail detail.
        /// </returns>
        private MailDetail ComposeMail(EntityType entityType, object data)
        {
            string positionProfileURL = ConfigurationManager.AppSettings["VIEW_POSITION_PROFILE_URL"];

            MailDetail mailDetail = null;
            int logID = 0;
            CandidateActivityLog activityLog = null;

            string defaultUrl = ConfigurationManager.AppSettings["DEFAULT_URL"] + "default.aspx";
            string candidateUrl = ConfigurationManager.AppSettings["CANDIDATE_URL"] + "signin.aspx";

            switch (entityType)
            {
                case EntityType.CandidateUnScheduled:
                    {
                        #region Candidate UnScheduled

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type case data to specific type.
                        CandidateTestSessionDetail candidateTestSessionDetail = data
                            as CandidateTestSessionDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();
                        mailDetail.To.Add(candidateTestSessionDetail.Email);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = string.Format("Test '{0} - {1}' un-scheduled for you",
                            candidateTestSessionDetail.TestID,
                            candidateTestSessionDetail.TestName);

                        mailDetail.Message = string.Format("Dear {0},", candidateTestSessionDetail.CandidateName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message += string.Format("Sub: Test '{0} - {1}' un-scheduled for you",
                            candidateTestSessionDetail.TestID,
                            candidateTestSessionDetail.TestName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("You will not be allowed to take the test");
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("For details contact your care taker");
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();

                        #endregion Candidate UnScheduled
                    }

                    break;

                case EntityType.InterviewCandidateUnScheduled:
                    {
                        #region Interview Candidate UnScheduled

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type case data to specific type.
                        CandidateInterviewSessionDetail candidateTestSessionDetail = data
                            as CandidateInterviewSessionDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();
                        mailDetail.To.Add(candidateTestSessionDetail.Email);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = string.Format("Interview '{0} - {1}' un-scheduled for you",
                            candidateTestSessionDetail.InterviewTestID,
                            candidateTestSessionDetail.InterviewTestName);

                        mailDetail.Message = string.Format("Dear {0},", candidateTestSessionDetail.CandidateName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message += string.Format("Sub: Interview '{0} - {1}' un-scheduled for you",
                            candidateTestSessionDetail.InterviewTestID,
                            candidateTestSessionDetail.InterviewTestName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("You will not be allowed to take the interview");
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("For details contact your care taker");
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();

                        #endregion Interview Candidate UnScheduled
                    }
                    break;

                case EntityType.CreditApproved:
                case EntityType.CreditDisapproved:
                    {
                        #region Credit Approved & Disapproved

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type case data to specific type.
                        CreditRequestDetail creditRequestDetail = data
                            as CreditRequestDetail;

                        mailDetail = new MailDetail();
                        mailDetail.isBodyHTML = true;

                        mailDetail.To = new List<string>();
                        mailDetail.To.Add(creditRequestDetail.CandidateEMail);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        // Configure from address.
                        mailDetail.From = new CommonDLManager().GetUserDetail
                            (creditRequestDetail.CreatedBy).Email;

                        if (creditRequestDetail.IsApproved)
                            mailDetail.Subject = "Credit request approved for you";
                        else
                            mailDetail.Subject = "Credit request disapproved for you";

                        mailDetail.Message = string.Format("Dear {0},", creditRequestDetail.CandidateFirstName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        if (creditRequestDetail.IsApproved)
                            mailDetail.Message += "Sub: Credit request approved for you";
                        else
                            mailDetail.Message += "Sub: Credit request disapproved for you";

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        if (creditRequestDetail.CreditRequestID == Constants.QuestionCreditConstants.SIMPLE ||
                            creditRequestDetail.CreditRequestID == Constants.QuestionCreditConstants.MEDIUM ||
                            creditRequestDetail.CreditRequestID == Constants.QuestionCreditConstants.COMPLEX)
                        {
                            if (creditRequestDetail.IsApproved)
                                mailDetail.Message += string.Format("Your request for credit through adding a question was approved");
                            else
                                mailDetail.Message += string.Format("Your request for credit through adding a question was disapproved");
                        }
                        else if (creditRequestDetail.CreditRequestID == Constants.CreditRequestConstants.CANDIDATE_REQUEST)
                        {
                            if (creditRequestDetail.IsApproved)
                                mailDetail.Message += string.Format("Your request for credit through mail was approved");
                            else
                                mailDetail.Message += string.Format("Your request for credit through mail was disapproved");
                        }
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("The approver provided remarks as follows:");
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "\'";
                        mailDetail.Message += creditRequestDetail.Remarks;
                        mailDetail.Message += "\'";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "Login to <a href='" + ConfigurationManager.AppSettings["DEFAULT_URL"] + "home.aspx' alt='' >ForteHCM</a> with your user name & password and view your credits";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();

                        #endregion Credit Approved & Disapproved
                    }
                    break;
                case EntityType.Feedback:
                    {
                        #region FeedbackMail

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        FeedbackDetail feedbackDetails = data
                            as FeedbackDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();
                        mailDetail.To.Add(ConfigurationManager.AppSettings["FEEDBACK_TO_ADDRESS"]);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = string.Format("Feedback : '{0}'", feedbackDetails.Subject);

                        mailDetail.Message = "Dear Admin,";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message += string.Format("Sub : Feedback on '{0}'", feedbackDetails.Subject);

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "You have received a feedback and the details are given below:";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("From : {0} {1}({2})", feedbackDetails.FirstName,
                            feedbackDetails.LastName, feedbackDetails.Email);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("Phone No : {0}", feedbackDetails.PhoneNumber);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("Comments : {0} ",
                           feedbackDetails.Comments);

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();

                        #endregion FeedbackMail
                    }
                    break;

                case EntityType.BrowserInstructionsMissing:
                    {
                        #region BrowserInstructionsMissing

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        BrowserDetail browserDetail = data
                            as BrowserDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();
                        mailDetail.To.Add(ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"]);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = string.Format
                            ("Browser instructions missing for type {0}", browserDetail.Type);

                        mailDetail.Message = "Dear Admin,";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message = string.Format
                            ("Sub: Browser instructions missing for type {0}", browserDetail.Type);

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "The cyber proctoring browser instructions are missing for:";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("Browser Type : {0}", browserDetail.Type);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("Version : {0}", browserDetail.Version);

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();

                        #endregion BrowserInstructionsMissing
                    }
                    break;

                case EntityType.NotesAdded:
                    {
                        #region NotesAdded

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be empty");

                        logID = 0;
                        if (!int.TryParse(data.ToString(), out logID))
                            throw new Exception("Data is invalid. Expected log ID as integer");

                        mailDetail = new MailDetail();

                        // Get candidate activity log.
                        activityLog = new CommonDLManager().GetCandidateActivityLog(logID);

                        if (activityLog.SendMail == false)
                            return null;

                        if (activityLog == null)
                            break;

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();

                        // Add candidate scheduler.
                        if (!Utility.IsNullOrEmpty(activityLog.CandidateOwnerEmail))
                            mailDetail.To.Add(activityLog.CandidateOwnerEmail);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = string.Format("Notes added to candidate '{0}'",
                            activityLog.CandidateName);

                        mailDetail.Message = string.Format("Dear {0},", activityLog.CandidateOwnerName);

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message += string.Format("Notes added to candidate '{0}'",
                            activityLog.CandidateName);

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "Details given below:";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("Added By : {0}", activityLog.NotesAddedByName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("Date : {0}", activityLog.Date);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("Notes : {0}", activityLog.Comments);
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();
                        mailDetail.isBodyHTML = true;

                        #endregion NotesAdded
                    }
                    break;

                case EntityType.IncompleteResumeReminderList:
                    {
                        #region IncompleteResumeReminderList

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        ResumeReminderDetail reminderDetail = data
                            as ResumeReminderDetail;

                        if (reminderDetail == null)
                            break;

                        mailDetail = new MailDetail();

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();

                        if (!Utility.IsNullOrEmpty(reminderDetail.EmailIDs))
                        {
                            List<string> emailList = new List<string>(reminderDetail.EmailIDs.Split(new char[] { ',' }));
                            mailDetail.To = emailList.Distinct().ToList();
                        }

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = string.Format("[{0}] There are incomplete resume records requiring your attention",
                            Constants.General.EMAIL_SUBJECT_COMPANY_NAME);

                        mailDetail.Message = string.Format("Dear {0},", reminderDetail.RecruiterFirstName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        // Compose viewer URL.
                        mailDetail.Message += "The following resumes are missing certain mandatory pieces of information. Please click <a href='" +
                            ConfigurationManager.AppSettings["INCOMPLETE_RESUME_SEARCH_URL"] +
                            "' alt=''>here</a> to view and enter the missing information.";

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "Please note that these resumes will not be activated until all of the mandatory information has been entered.";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += reminderDetail.ProfileNames.Replace("|", Environment.NewLine);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();
                        mailDetail.isBodyHTML = true;

                        #endregion IncompleteResumeReminderList
                    }
                    break;

                case EntityType.DuplicateResumeReminderList:
                    {
                        #region DuplicateResumeReminderList

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        ResumeReminderDetail reminderDetail = data
                            as ResumeReminderDetail;

                        if (reminderDetail == null)
                            break;

                        mailDetail = new MailDetail();

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();

                        if (!Utility.IsNullOrEmpty(reminderDetail.EmailIDs))
                        {
                            List<string> emailList = new List<string>(reminderDetail.EmailIDs.Split(new char[] { ',' }));
                            mailDetail.To = emailList.Distinct().ToList();
                        }

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = string.Format("[{0}] There are duplicate resume records requiring your attention",
                            Constants.General.EMAIL_SUBJECT_COMPANY_NAME);

                        mailDetail.Message = string.Format("Dear {0},", reminderDetail.AdminFirstName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        // Compose viewer URL.
                        mailDetail.Message += "The system has detected existing candidate record(s) that match the following resumes. Please click <a href='" +
                            ConfigurationManager.AppSettings["DUPLICATE_RESUME_SEARCH_URL"]
                            + "' alt=''>here</a> to view the records, and select an appropriate action. Please note that the resumes will not be activated until this step has been completed.";

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += reminderDetail.ProfileNames.Replace("|", Environment.NewLine);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();
                        mailDetail.isBodyHTML = true;

                        #endregion DuplicateResumeReminderList
                    }
                    break;

                case EntityType.FailedEmailResend:
                    {
                        #region FailedEmailResend

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        FailedEmailDetail failedEmail = data
                            as FailedEmailDetail;

                        if (failedEmail == null)
                            break;

                        mailDetail = new MailDetail();

                        mailDetail.From = failedEmail.FromEmailID;

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();

                        // Add to list.
                        if (!Utility.IsNullOrEmpty(failedEmail.ToEmailID))
                        {
                            List<string> emailList = new List<string>(failedEmail.ToEmailID.Split(new char[] { ',' }));
                            mailDetail.To = emailList.Distinct().ToList();
                        }

                        // Add cc list (this will also get added to the to list)
                        if (!Utility.IsNullOrEmpty(failedEmail.CCEmailID))
                        {
                            List<string> emailList = new List<string>(failedEmail.CCEmailID.Split(new char[] { ',' }));
                            mailDetail.To.AddRange(emailList.Distinct().ToList());
                        }

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = failedEmail.MailSubject;

                        mailDetail.Message = failedEmail.MailMessage;

                        mailDetail.isBodyHTML = failedEmail.IsHtml;

                        #endregion FailedEmailResend
                    }
                    break;

                case EntityType.FailedEmailToAdmin:
                    {
                        #region FailedEmailResend

                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        FailedEmailDetail failedEmail = data
                            as FailedEmailDetail;

                        if (failedEmail == null)
                            break;

                        mailDetail = new MailDetail();

                        mailDetail.From = failedEmail.FromEmailID;

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();

                        // Add admin email to to list.
                        mailDetail.To = new List<string>(new string[] { ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"] });

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = failedEmail.MailSubject;

                        mailDetail.Message = "Dear Admin,";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "Unable to resend the below email to the following recipients:";
                        mailDetail.Message += Environment.NewLine;

                        // Construct recipients.
                        string recipients = string.Empty;

                        if (!Utility.IsNullOrEmpty(failedEmail.ToEmailID))
                        {
                            recipients = failedEmail.ToEmailID.Trim();
                        }

                        if (!Utility.IsNullOrEmpty(failedEmail.CCEmailID))
                        {
                            if (Utility.IsNullOrEmpty(recipients))
                                recipients = failedEmail.CCEmailID.Trim();
                            else
                                recipients += "," + failedEmail.CCEmailID.Trim();
                        }
                        mailDetail.Message += recipients;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "------------------------------------------------------------";
                        mailDetail.Message += Environment.NewLine;

                        mailDetail.Message += failedEmail.MailMessage;

                        mailDetail.isBodyHTML = failedEmail.IsHtml;

                        #endregion FailedEmailToAdmin
                    }
                    break;


                case EntityType.DailyTestReminder:
                    {
                        #region DailyTestReminder

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. Candidate test session detail object is expected");

                        CandidateTestSessionDetail candidateTestSessionDetail = data as CandidateTestSessionDetail;

                        if (candidateTestSessionDetail == null)
                            break;

                        mailDetail = new MailDetail();

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();

                        // Add candidate email.
                        if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateEmail))
                            mailDetail.To.Add(candidateTestSessionDetail.CandidateEmail);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        // Add candidate scheduler.
                        if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.SchedulerEmail))
                            mailDetail.CC.Add(candidateTestSessionDetail.SchedulerEmail);

                        mailDetail.Subject = string.Format("Reminder for test named ‘{0}’",
                            candidateTestSessionDetail.TestName);

                        mailDetail.Message = string.Format("Dear {0},", candidateTestSessionDetail.CandidateFirstName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("This is a reminder to you for the web-based test named '{0}' scheduled by {1} of {2}. This is an objective type online test.",
                            candidateTestSessionDetail.TestName,
                            candidateTestSessionDetail.SchedulerName, candidateTestSessionDetail.SchedulerCompany);

                        string url = ConfigurationManager.AppSettings["TEST_INSTRUCTION_URL"] + "?n=" + candidateTestSessionDetail.NavigateKey;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "Please <a href='" + url + "' alt='' >login</a> to Forte HCM with the following credentials to review and start your test. We encourage you to read the detailed guidelines that you will be presented with on the screen just before starting your test";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        // Get user name and password.
                        UserRegistrationInfo info = new UserRegistrationDLManager().GetUserRegistrationInfo
                            (Convert.ToInt32(candidateTestSessionDetail.CandidateID));
                        if (info != null)
                        {
                            mailDetail.Message += string.Format("User name: {0}", info.UserEmail);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Password: {0}", Decrypt(info.Password));
                        }

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += String.Format("Please note that the test has an expiry date of {0}", candidateTestSessionDetail.ExpiryDateFormatted);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "This is a system generated message. Please do not reply to this email. Should you have any questions related to this test, please contact the person that set it up for you.";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetDisclaimerMessage();
                        mailDetail.isBodyHTML = true;

                        #endregion DailyTestReminder
                    }
                    break;

                case EntityType.DailyInterviewReminder:
                    {
                        #region DailyInterviewReminder

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. Candidate interview session detail object is expected");

                        CandidateInterviewSessionDetail candidateInterviewSessionDetail = data as CandidateInterviewSessionDetail;

                        if (candidateInterviewSessionDetail == null)
                            break;

                        mailDetail = new MailDetail();

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();

                        // Add candidate email.
                        if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.CandidateEmail))
                            mailDetail.To.Add(candidateInterviewSessionDetail.CandidateEmail);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        // Add candidate scheduler.
                        if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.SchedulerEmail))
                            mailDetail.CC.Add(candidateInterviewSessionDetail.SchedulerEmail);

                        //A test named ‘CP - Network Support Professional Preliminary’ has been scheduled for you
                        mailDetail.Subject = string.Format("Reminder for video interview named '{0}'",
                            candidateInterviewSessionDetail.InterviewTestName);

                        mailDetail.Message = string.Format("Dear {0},", candidateInterviewSessionDetail.CandidateFirstName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("This is a reminder to you for the video interview named '{0}' scheduled by {1} of {2}. This is a web-based video interview where your response to questions (that will be presented one at a time on your screen) will be captured through your webcam/microphone and transmitted back for review.",
                            candidateInterviewSessionDetail.InterviewTestName,
                            candidateInterviewSessionDetail.SchedulerName, candidateInterviewSessionDetail.SchedulerCompany);

                        string url = ConfigurationManager.AppSettings["INTERVIEW_INSTRUCTION_URL"] + "?n=" + candidateInterviewSessionDetail.NavigateKey;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "Please <a href='" + url + "' alt='' >login</a> to Forte HCM with the following credentials to review and start your interview. We encourage you to read the detailed guidelines that you will be presented with on the screen just before starting your interview.";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        // Get user name and password.
                        UserRegistrationInfo info = new UserRegistrationDLManager().GetUserRegistrationInfo
                            (Convert.ToInt32(candidateInterviewSessionDetail.CandidateID));
                        if (info != null)
                        {
                            mailDetail.Message += string.Format("User name: {0}", info.UserEmail);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Password: {0}", Decrypt(info.Password));
                        }

                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += String.Format("Please note that the video interview has an expiry date of {0}", candidateInterviewSessionDetail.ExpiryDateFormatted);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "This is a system generated message. Please do not reply to this email. Should you have any questions related to this interview, please contact the person that set it up for you.";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetDisclaimerMessage();
                        mailDetail.isBodyHTML = true;

                        #endregion DailyInterviewReminder
                    }
                    break;

                case EntityType.PositionProfileDeleted:
                    {
                        #region Position Profile Deleted

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. Position profile detail object is expected");

                        // Retrieve position profile details.
                        PositionProfileDetail positionProfileDetail = data as PositionProfileDetail;

                        // Retrieve position profile owner details.
                        List<PositionProfileOwnerDetail> owners = new PositionProfileDLManager().
                            GetPositionProfileOwners(positionProfileDetail.PositionProfileID);

                        if (owners == null || owners.Count == 0)
                            return null;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        // Add to list.
                        foreach (PositionProfileOwnerDetail owner in owners)
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.To.IndexOf(owner.EMail) == -1)
                                mailDetail.To.Add(owner.EMail);
                        }

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = string.Format("Position profile '{0}' deleted", positionProfileDetail.PositionProfileName);

                        mailDetail.Message = string.Format("Dear Position Profile Associate,");
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("Please be informed that the position profile '{0}' has been deleted", positionProfileDetail.PositionProfileName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();
                        mailDetail.isBodyHTML = true;

                        #endregion Position Profile Deleted
                    }
                    break;

                case EntityType.PositionProfileStatusChanged:
                    {
                        #region Position Profile Status Changed

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. Position profile detail object is expected");

                        // Retrieve position profile details.
                        PositionProfileDetail positionProfileDetail = data as PositionProfileDetail;

                        // Retrieve position profile owner details.
                        List<PositionProfileOwnerDetail> owners = new PositionProfileDLManager().
                            GetPositionProfileOwners(positionProfileDetail.PositionProfileID);

                        if (owners == null || owners.Count == 0)
                            return null;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        // Add to list.
                        foreach (PositionProfileOwnerDetail owner in owners)
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.To.IndexOf(owner.EMail) == -1)
                                mailDetail.To.Add(owner.EMail);
                        }

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        if (!Utility.IsNullOrEmpty(positionProfileDetail.ClientName))
                        {
                            mailDetail.Subject = string.Format("Status of '{0}' ({1}) position changed to '{2}'",
                                positionProfileDetail.PositionProfileName,
                                positionProfileDetail.ClientName,
                                positionProfileDetail.PositionProfileStatusName);
                        }
                        else
                        {
                            mailDetail.Subject = string.Format("Status of '{0}' position changed to '{1}'",
                               positionProfileDetail.PositionProfileName,
                               positionProfileDetail.PositionProfileStatusName);
                        }

                        // Compose message.
                        StringBuilder message = new StringBuilder();

                        message.AppendLine(string.Format("{0} has changed the status of the following position to '{1}'",
                            positionProfileDetail.ModifiedByName,
                            positionProfileDetail.PositionProfileStatusName));

                        message.AppendLine();

                        message.AppendLine(string.Format("Position name: {0}",
                            positionProfileDetail.PositionProfileName));
                        message.AppendLine(string.Format("Client name: {0}",
                            positionProfileDetail.ClientName));

                        message.AppendLine();

                        message.AppendLine(GetClosureMessage());
                        message.AppendLine(GetDisclaimerMessage());

                        mailDetail.Message = message.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Position Profile Status Changed
                    }
                    break;

                case EntityType.AssociatedWithPositionProfile:
                    {
                        #region Associated With Position Profile

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. Position profile detail object is expected");

                        // Retrieve position profile details.
                        PositionProfileDetail positionProfileDetail = data as PositionProfileDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(positionProfileDetail.TaskOwnerEMail);


                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        if (!Utility.IsNullOrEmpty(positionProfileDetail.ClientName))
                        {
                            mailDetail.Subject = string.Format("Associated with position '{0}' ({1})",
                                positionProfileDetail.PositionProfileName,
                                positionProfileDetail.ClientName);
                        }
                        else
                        {
                            mailDetail.Subject = string.Format("Associated with position '{0}'",
                               positionProfileDetail.PositionProfileName);
                        }

                        // Compose message.
                        StringBuilder message = new StringBuilder();

                        message.AppendLine(string.Format("Dear {0},",
                           positionProfileDetail.TaskOwnerName));

                        message.AppendLine();

                        message.AppendLine(string.Format("You have been associated with the following position with the role '{0}'",
                            GetTaskOwnerRoleMessage(positionProfileDetail.TaskOwnerType)));

                        message.AppendLine();

                        message.AppendLine(string.Format("Position name: {0}",
                            positionProfileDetail.PositionProfileName));
                        message.AppendLine(string.Format("Client name: {0}",
                            positionProfileDetail.ClientName));

                        message.AppendLine();
                        message.AppendLine();

                        // Compose URL.
                        string url = string.Format(positionProfileURL, positionProfileDetail.PositionProfileID);
                        message.AppendLine("Click <a href='" + url + "' alt=''> here </a> to view the position profile.");
                        message.AppendLine();

                        message.AppendLine(GetClosureMessage());
                        message.AppendLine(GetDisclaimerMessage());

                        mailDetail.Message = message.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Associated With Position Profile
                    }
                    break;

                case EntityType.DissociatedFromPositionProfile:
                    {
                        #region Dissociated From Position Profile

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. Position profile detail object is expected");

                        // Retrieve position profile details.
                        PositionProfileDetail positionProfileDetail = data as PositionProfileDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(positionProfileDetail.TaskOwnerEMail);


                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        if (!Utility.IsNullOrEmpty(positionProfileDetail.ClientName))
                        {
                            mailDetail.Subject = string.Format("Dissociated from position '{0}' ({1})",
                                positionProfileDetail.PositionProfileName,
                                positionProfileDetail.ClientName);
                        }
                        else
                        {
                            mailDetail.Subject = string.Format("Dissociated from position '{0}'",
                               positionProfileDetail.PositionProfileName);
                        }

                        // Compose message.
                        StringBuilder message = new StringBuilder();

                        message.AppendLine(string.Format("Dear {0},",
                           positionProfileDetail.TaskOwnerName));

                        message.AppendLine();

                        message.AppendLine(string.Format("You have been dissociated from the following position from the role '{0}'",
                          GetTaskOwnerRoleMessage(positionProfileDetail.TaskOwnerType)));

                        message.AppendLine();

                        message.AppendLine(string.Format("Position name: {0}",
                            positionProfileDetail.PositionProfileName));
                        message.AppendLine(string.Format("Client name: {0}",
                            positionProfileDetail.ClientName));

                        message.AppendLine();
                        message.AppendLine();

                        message.AppendLine(GetClosureMessage());
                        message.AppendLine(GetDisclaimerMessage());

                        mailDetail.Message = message.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Dissociated From Position Profile
                    }
                    break;

                case EntityType.RequestToExternalAssessor:
                    {
                        #region Send request to external assessor

                        string externalAssessorURL = ConfigurationManager.AppSettings["EXTERNAL_ASSESSOR_URL"];

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. External assessor detail object is expected");

                        // Retrieve key and email id.
                        ExternalAssessorDetail externalAssessorDetail = data as ExternalAssessorDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(externalAssessorDetail.Email);


                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }
                        mailDetail.Subject = "ForteHCM - Request to assess the candidate";

                        // Compose message.
                        StringBuilder message = new StringBuilder();

                        message.AppendLine(string.Format("Dear {0},", externalAssessorDetail.Email));

                        message.AppendLine();

                        message.AppendLine("Welcome to ForteHCM !");
                        message.AppendLine();

                        // Compose URL.
                        string url = string.Format(externalAssessorURL, externalAssessorDetail.ExternalKey);
                        message.AppendLine("Click <a href='" + url + "' alt=''> here </a> to assess the candidate.");

                        message.AppendLine();

                        message.AppendLine("Thank you for your interest in ForteHCM.");

                        message.AppendLine();

                        message.AppendLine(GetClosureMessage());
                        message.AppendLine(GetDisclaimerMessage());

                        mailDetail.Message = message.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Send request to external assessor
                    }
                    break;

                case EntityType.RequestToAssessor:
                    {
                        #region Send request to assessor

                        string assessorURL = ConfigurationManager.AppSettings["ASSESSOR_URL"];

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. Assessor detail object is expected");

                        // Retrieve key and email id.
                        ExternalAssessorDetail externalAssessorDetail = data as ExternalAssessorDetail;

                        //Get interview key & session key
                        InterviewDetail interviewDetail = new CandidateDLManager().
                            GetInterviewDetail(externalAssessorDetail.CandidateInterviewSessionkey,
                            externalAssessorDetail.AttemptID);

                        // Compose URL.
                        string url = string.Format(assessorURL, externalAssessorDetail.CandidateInterviewSessionkey,
                            interviewDetail.InterviewTestKey, interviewDetail.InterviewSessionKey,
                            externalAssessorDetail.ExternalAssessorID, externalAssessorDetail.AttemptID);

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(externalAssessorDetail.Email);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'cc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = "ForteHCM - Request to assess the candidate";

                        // Compose message.
                        StringBuilder message = new StringBuilder();

                        message.AppendLine("Dear Viewer");

                        message.AppendLine();

                        message.AppendLine("Welcome to ForteHCM !");
                        message.AppendLine();


                        message.AppendLine("Click <a href='" + url + "' alt=''> here </a> to assess the candidate.");

                        message.AppendLine();

                        message.AppendLine("Thank you for your interest in ForteHCM.");

                        message.AppendLine();

                        message.AppendLine(GetClosureMessage());
                        message.AppendLine(GetDisclaimerMessage());

                        mailDetail.Message = message.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Send request to assessor
                    }
                    break;
                case EntityType.ResendPassword:
                    {
                        #region Resend Password

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");

                        // Retrieve the user details
                        UserDetail userDetail = data as UserDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(userDetail.Email);


                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }
                        mailDetail.Subject = "ForteHCM: Your account password";

                        // Compose message.
                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear " + userDetail.FirstName + " " + userDetail.LastName);
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This mail is sent to you for the resend password request");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Your user: " + userDetail.UserName);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Your password: " + userDetail.Password);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");


                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Resend Password
                    }
                    break;
                case EntityType.ForgotPassword:
                    {
                        #region Forgot Password

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");

                        // Retrieve the user details
                        UserDetail userDetail = data as UserDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(userDetail.Email);


                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }
                        mailDetail.Subject = "ForteHCM: Your account password";

                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear " + Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(userDetail.FirstName.ToLower()));
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This mail is being sent to you in response to your ‘forgot password’ request.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Your password is: " + userDetail.Password);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("If you did not place this request, please let us know immediately by emailing us at <a href='mailto:support@fortehcm.com'>support@fortehcm.com</a>");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message. Please do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");

                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Forgot Password
                    }
                    break;
                case EntityType.RequestAvailabilityDate:
                    {
                        #region Request Availability Date

                        string timeSlotRequestURL = ConfigurationManager.AppSettings["TIME_SLOT_REQUEST"];

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");

                        // Retrieve the assessor details
                        UserDetail userDetail = data as UserDetail;

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(userDetail.Email);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        mailDetail.Subject = "ForteHCM - Request for assessment timeslots";

                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear " + Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(userDetail.FirstName.ToLower()));
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(userDetail.RequestedBy + " has placed a request for your availability to conduct one or more assessments.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.AppendLine("Please click <a href='" + timeSlotRequestURL + "' alt=''> here </a> to review and approve the timeslots.You will also have the option of rejecting a timeslot and proposing alternative timeslots on this screen.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message. Please do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");

                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Request Availability Date
                    }
                    break;
                case EntityType.CandidateSlotApproval:
                    {
                        #region Candidate Slot Approval

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");

                        // Retrieve the candidate details
                        CandidateDetail candidateDeail = data as CandidateDetail;

                        // Construct interview publish url
                        string slotUrl = string.Format(ConfigurationManager.AppSettings["CANDIDATE_TIME_SLOT_APPROVAL"], candidateDeail.SlotKey);

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(candidateDeail.EMailID);


                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }
                        mailDetail.Subject = "ForteHCM: Confirm Slots";

                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear " + Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(candidateDeail.FirstName.ToLower()));
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This mail is being sent to you to get your approval of the requested slots.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.AppendLine("Click <a href='" + slotUrl + "' alt=''> here </a> to approve the slots.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.AppendLine("Thank you for your interest in ForteHCM.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message. Please do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");

                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Candidate Slot Approval
                    }
                    break;
                case EntityType.OnlineInterviewScheduledToAssessor:
                    {
                        #region Send mail to scheduled assessor

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");

                        // Retrieve the assessor details
                        AssessorDetail assessorDetail = data as AssessorDetail;

                        // Construct interview url for candidate
                        string assessorURL = string.Format(ConfigurationManager.AppSettings["ONLINE_ASSESSOR_URL"],
                            assessorDetail.ChatRoomKey);


                        // Get candidate interview detail
                        CandidateInterviewSessionDetail candidateInterviewDetail = new CandidateInterviewSessionDetail();

                        candidateInterviewDetail = new OnlineInterviewDLManager().
                            GetCandidateOnlineInterviewDetail(assessorDetail.CandidateInterviewID);

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(assessorDetail.UserEmail);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }
                        mailDetail.Subject = "ForteHCM - Interview Scheduled";

                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear " + Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(assessorDetail.FirstName.ToLower()));
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("An online interview that you are registered as a participating assessor has been confirmed for the following date and time.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Date:" + candidateInterviewDetail.ScheduledDate.ToString("MMMM dd, yyyy"));
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Time:" + string.Format("{0} - {1} CST", candidateInterviewDetail.TimeSlotTextFrom,
                            candidateInterviewDetail.TimeSlotTextTo));
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Interview name:" + candidateInterviewDetail.InterviewTestName);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        if (!Utility.IsNullOrEmpty(candidateInterviewDetail.PositionProfileName))
                        {
                            mailBody.Append("Position name:" + candidateInterviewDetail.PositionProfileName);
                            mailBody.Append(Environment.NewLine);
                            mailBody.Append(Environment.NewLine);
                        }
                        string candidateName = (Utility.IsNullOrEmpty(candidateInterviewDetail.CandidateLastName) ? candidateInterviewDetail.CandidateFirstName :
                              string.Format("{0} {1}", candidateInterviewDetail.CandidateFirstName, candidateInterviewDetail.CandidateLastName));

                        mailBody.Append("Candidate name:" + candidateName);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        string schedulerName = (Utility.IsNullOrEmpty(candidateInterviewDetail.SchedulerLastName) ? candidateInterviewDetail.SchedulerFirstName :
                               string.Format("{0} {1}", candidateInterviewDetail.SchedulerFirstName, candidateInterviewDetail.SchedulerLastName));

                        mailBody.Append("Interview scheduler:" + schedulerName);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Please click on the following link to login and join the interview session at least 5 minutes before the start time indicated above.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.AppendLine("<a href='" + assessorURL + "' alt=''>Join Interview</a>");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message. Please do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");

                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Send mail to scheduled assessor
                    }
                    break;
                case EntityType.OnlineInterviewScheduledToCandidate:
                    {
                        #region Send mail to scheduled candidate

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");

                        // Retrieve the candidate details
                        CandidateDetail candidateDetail = data as CandidateDetail;

                        // Construct interview url for candidate
                        string candidateURL = string.Format(ConfigurationManager.AppSettings["ONLINE_CANDIDATE_URL"],
                            candidateDetail.ChatRoomKey);

                        // Get candidate interview detail
                        CandidateInterviewSessionDetail candidateInterviewDetail = new CandidateInterviewSessionDetail();

                        candidateInterviewDetail = new OnlineInterviewDLManager().
                            GetCandidateOnlineInterviewDetail(candidateDetail.CandidateInterviewID);

                        mailDetail = new MailDetail();

                        mailDetail.To = new List<string>();

                        mailDetail.To.Add(candidateDetail.EMailID);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }
                        mailDetail.Subject = "ForteHCM - Interview Schedule Confirmed";

                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear " + Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(candidateDetail.FirstName.ToLower()));
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This email is being sent to you to confirm that the following interview has been scheduled for the date and time indicated below. Should you have any questions, please contact the interview scheduler named below");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Date:" + candidateInterviewDetail.ScheduledDate.ToString("MMMM dd, yyyy"));
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Time:" + string.Format("{0} - {1} CST", candidateInterviewDetail.TimeSlotTextFrom,
                            candidateInterviewDetail.TimeSlotTextTo));
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Interview name:" + candidateInterviewDetail.InterviewTestName);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);

                        if (!Utility.IsNullOrEmpty(candidateInterviewDetail.PositionProfileName))
                        {
                            mailBody.Append("Position name:" + candidateInterviewDetail.PositionProfileName);
                            mailBody.Append(Environment.NewLine);
                            mailBody.Append(Environment.NewLine);
                        }
                        string schedulerName = (Utility.IsNullOrEmpty(candidateInterviewDetail.SchedulerLastName) ? candidateInterviewDetail.SchedulerFirstName :
                               string.Format("{0} {1}", candidateInterviewDetail.SchedulerFirstName, candidateInterviewDetail.SchedulerLastName));

                        mailBody.Append("Interview scheduler:" + schedulerName);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("Please click on the following link to login and join the interview session at least 10 minutes before the start time indicated above.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.AppendLine("<a href='" + candidateURL + "' alt=''>Join Interview</a>");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message. Please do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");

                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Send mail to scheduled candidate
                    }
                    break;
                case EntityType.OnlineInterviewCompleted:
                    {
                        #region Send mail to scheduler/cadidate owner/position profiel owner

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");

                        // Retrieve the candidate detail
                        CandidateInterviewSessionDetail onlineInterviewDetail = data as CandidateInterviewSessionDetail;

                        // Construct online interview results url.
                        string resultURL = string.Format(
                            ConfigurationManager.AppSettings["ONLINE_INTERVIEW_RESULT_URL"],
                            onlineInterviewDetail.InterviewTestKey, onlineInterviewDetail.CandidateInterviewID,
                            onlineInterviewDetail.ChatRoomKey);

                        mailDetail = new MailDetail();


                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();
                        // Add candidate scheduler as 'to' address.
                        if (!Utility.IsNullOrEmpty(onlineInterviewDetail.SchedulerEmail))
                            mailDetail.To.Add(onlineInterviewDetail.SchedulerEmail);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        // Add candidate owner as 'cc' address.
                        if (!Utility.IsNullOrEmpty(onlineInterviewDetail.CandidateOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(onlineInterviewDetail.CandidateOwnerEmail) == -1)
                                mailDetail.CC.Add(onlineInterviewDetail.CandidateOwnerEmail);
                        }

                        // Add position profile owner as 'cc' address.
                        if (!Utility.IsNullOrEmpty(onlineInterviewDetail.PositionProfileOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(onlineInterviewDetail.PositionProfileOwnerEmail) == -1)
                                mailDetail.CC.Add(onlineInterviewDetail.PositionProfileOwnerEmail);
                        }
                        UserRegistrationInfo candidateInfo;
                        candidateInfo = new UserRegistrationDLManager().GetUserRegistrationInfo
                            (Convert.ToInt32(onlineInterviewDetail.CandidateID));

                        // Add subject.
                        mailDetail.Subject = "ForteHCM - Interview has been completed";


                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear Viewer");
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("The following interview was completed on " + DateTime.Now.ToString("MMMM dd, yyyy"));
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);

                        mailBody.Append("Interview name:" + onlineInterviewDetail.InterviewTestName);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        if (!Utility.IsNullOrEmpty(onlineInterviewDetail.PositionProfileName))
                        {
                            mailBody.Append("Position name:" + onlineInterviewDetail.PositionProfileName);
                            mailBody.Append(Environment.NewLine);
                            mailBody.Append(Environment.NewLine);
                        }
                        if (!Utility.IsNullOrEmpty(onlineInterviewDetail.ClientName))
                        {
                            mailBody.Append("Client name:" + onlineInterviewDetail.ClientName);
                            mailBody.Append(Environment.NewLine);
                            mailBody.Append(Environment.NewLine);
                        }
                        string candidateName = (Utility.IsNullOrEmpty(onlineInterviewDetail.CandidateLastName) ? onlineInterviewDetail.CandidateFirstName :
                              string.Format("{0} {1}", onlineInterviewDetail.CandidateFirstName, onlineInterviewDetail.CandidateLastName));

                        mailBody.Append("Candidate name:" + candidateName);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);

                        string schedulerName = (Utility.IsNullOrEmpty(onlineInterviewDetail.SchedulerLastName) ? onlineInterviewDetail.SchedulerFirstName :
                               string.Format("{0} {1}", onlineInterviewDetail.SchedulerFirstName, onlineInterviewDetail.SchedulerLastName));

                        //mailBody.Append("Assessors:" + schedulerName);
                        //mailBody.Append(Environment.NewLine);
                        //mailBody.Append(Environment.NewLine);
                        //mailBody.Append(Environment.NewLine);

                        mailBody.AppendLine("Please click <a href='" + resultURL + "' alt=''> here </a> to view the interview  assessment summary.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message. Please do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");

                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Send mail to scheduler/cadidate owner/position profiel owner
                    }
                    break;
                case EntityType.AcceptedAvailabilityDate:
                    {
                        #region Send mail to requester stating that date accepted

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");


                        // Retrieve the assessor detail
                        AssessorDetail assessorDetail = data as AssessorDetail;

                        string assessorFullName = (Utility.IsNullOrEmpty(assessorDetail.LastName) ? assessorDetail.FirstName :
                             string.Format("{0} {1}", assessorDetail.FirstName, assessorDetail.LastName));

                        // Get time slot detail.
                        AssessorTimeSlotDetail timeSlot = new AssessorDLManager().GetAssessorTimeSlot(assessorDetail.RequestedSlotID);

                        mailDetail = new MailDetail();


                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();
                        // Add candidate scheduler as 'to' address.
                        if (!Utility.IsNullOrEmpty(timeSlot.Email))
                            mailDetail.To.Add(timeSlot.Email);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }

                        }
                        string slotsDesc = string.Empty;

                        if (timeSlot.assessorTimeSlotDetails != null && timeSlot.assessorTimeSlotDetails.Count > 0)
                        {
                            foreach (AssessorTimeSlotDetail slotsDetail in timeSlot.assessorTimeSlotDetails)
                            {
                                slotsDesc = slotsDesc + string.Format("{0} to {1}", slotsDetail.TimeSlotTextFrom, slotsDetail.TimeSlotTextTo) + ",";
                            }
                        }
                        if (!Utility.IsNullOrEmpty(slotsDesc))
                        {
                            slotsDesc = slotsDesc.TrimEnd(',');
                        }

                        // Add subject.
                        mailDetail.Subject = "ForteHCM - Timeslots confirmed by assessor";


                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear " + Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(timeSlot.FirstName.ToLower()));
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(assessorFullName + " has confirmed availability to interview candidates during the following days / time.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);

                        if (!Utility.IsNullOrEmpty(timeSlot.AvailabilityDate))
                        {
                            mailBody.Append("Date:" + timeSlot.AvailabilityDate.ToString("MMMM dd, yyyy"));
                            mailBody.Append(Environment.NewLine);
                            mailBody.Append(Environment.NewLine);
                        }

                        if (!Utility.IsNullOrEmpty(timeSlot.IsAvailableFullTime) && timeSlot.IsAvailableFullTime.Trim() == "Y")
                        {
                            mailBody.Append("Timeslots:Available full time");
                        }
                        else
                        {
                            mailBody.Append("Timeslots:" + slotsDesc);
                        }
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message. Please do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");

                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Send mail to requester stating that date accepted
                    }
                    break;
                case EntityType.RejectRequestedAvailabilityDate:
                    {
                        #region Send mail to requester stating that date accepted

                        if (Utility.IsNullOrEmpty(data))
                            throw new Exception("Data cannot be null. User detail object is expected");


                        // Retrieve the assessor detail
                        AssessorDetail assessorDetail = data as AssessorDetail;

                        string assessorFullName = (Utility.IsNullOrEmpty(assessorDetail.LastName) ? assessorDetail.FirstName :
                             string.Format("{0} {1}", assessorDetail.FirstName, assessorDetail.LastName));

                        // Get time slot detail.
                        AssessorTimeSlotDetail timeSlot = new AssessorDLManager().GetAssessorTimeSlot(assessorDetail.RequestedSlotID);

                        mailDetail = new MailDetail();


                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();
                        // Add candidate scheduler as 'to' address.
                        if (!Utility.IsNullOrEmpty(timeSlot.Email))
                            mailDetail.To.Add(timeSlot.Email);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Instantiate 'bcc' address.
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        string slotsDesc = string.Empty;

                        if (timeSlot.assessorTimeSlotDetails != null && timeSlot.assessorTimeSlotDetails.Count > 0)
                        {
                            foreach (AssessorTimeSlotDetail slotsDetail in timeSlot.assessorTimeSlotDetails)
                            {
                                slotsDesc = slotsDesc + string.Format("{0} to {1}", slotsDetail.TimeSlotTextFrom, slotsDetail.TimeSlotTextTo) + ",";
                            }
                        }
                        if (!Utility.IsNullOrEmpty(slotsDesc))
                        {
                            slotsDesc = slotsDesc.TrimEnd(',');
                        }

                        // Add subject.
                        mailDetail.Subject = "ForteHCM - Timeslots rejected by assessor";


                        StringBuilder mailBody = new StringBuilder();

                        mailBody.Append("Dear " + Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(timeSlot.FirstName.ToLower()));
                        mailBody.Append(",");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(assessorFullName + " has rejected your request to interview candidates during the following days / time.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);

                        if (!Utility.IsNullOrEmpty(timeSlot.AvailabilityDate))
                        {
                            mailBody.Append("Date:" + timeSlot.AvailabilityDate.ToString("MMMM dd, yyyy"));
                            mailBody.Append(Environment.NewLine);
                            mailBody.Append(Environment.NewLine);
                        }

                        if (!Utility.IsNullOrEmpty(timeSlot.IsAvailableFullTime) && timeSlot.IsAvailableFullTime.Trim() == "Y")
                        {
                            mailBody.Append("Timeslots:Not available full time");
                        }
                        else
                        {
                            mailBody.Append("Timeslots:" + slotsDesc);
                        }
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This is a system generated message. Please do not reply to this message");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("============================== Disclaimer ==============================");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                        mailBody.Append(Environment.NewLine);
                        mailBody.Append("=========================== End Of Disclaimer ============================");

                        mailDetail.Message = mailBody.ToString();

                        mailDetail.isBodyHTML = true;

                        #endregion Send mail to requester stating that date accepted
                    }
                    break;
                default:
                    break;
            }

            return mailDetail;
        }

        /// <summary>
        /// Method that compose the <see cref="MailDetail"/> object for the 
        /// given entity ID and entity type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        /// <param name="subEntityID">
        /// A <see cref="string"/> that holds the sub entity ID.
        /// </param>
        /// <returns>
        /// A <see cref="MailDetail"/> that holds the mail detail.
        /// </returns>
        private MailDetail ComposeMail(EntityType entityType, string entityID, string subEntityID, int tenantID = 2)
        {
            string defaultUrl = ConfigurationManager.AppSettings["DEFAULT_URL"] + "default.aspx";
            string candidateUrl = ConfigurationManager.AppSettings["CANDIDATE_URL"] + "signin.aspx";
            string testResultUrl = ConfigurationManager.AppSettings["TEST_RESULT_URL"];
            string candidateTestResultUrl = ConfigurationManager.AppSettings["CANDIDATE_TEST_RESULT_URL"];
            string viewTestUrl = ConfigurationManager.AppSettings["VIEW_TEST_URL"];
            string viewInterviewUrl = ConfigurationManager.AppSettings["VIEW_INTERVIEW_URL"];
            string dashboardHomeUrl = ConfigurationManager.AppSettings["DASHBOARD_HOME"];

            MailDetail mailDetail = null;

            try
            {
                switch (entityType)
                {
                    case EntityType.None:
                        break;

                    case EntityType.UserActivated:
                        {
                            #region User Activated

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            int userID = 0;
                            if (!int.TryParse(entityID, out userID))
                                throw new Exception("Entity ID is invalid. Expected user ID as integer");

                            mailDetail = new MailDetail();
                            UserRegistrationInfo info = new UserRegistrationDLManager().GetUserRegistrationInfo(userID);

                            if (info == null)
                                break;

                            mailDetail.To = new List<string>();
                            mailDetail.To.Add(info.UserEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = "User account activated";
                            mailDetail.Message = string.Format("Dear {0},", info.FirstName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Sub: User account activated";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Your account has been activated";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "You can login using <a href='" + defaultUrl + "' alt=''> ForteHCM </a>";
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion User Activated
                        }
                        break;

                    case EntityType.UserDeactivated:
                        {
                            #region User Dectivated

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            int userID = 0;
                            if (!int.TryParse(entityID, out userID))
                                throw new Exception("Entity ID is invalid. Expected user ID as integer");

                            mailDetail = new MailDetail();
                            UserRegistrationInfo info = new UserRegistrationDLManager().GetUserRegistrationInfo(userID);

                            if (info == null)
                                break;

                            mailDetail.To = new List<string>();
                            mailDetail.To.Add(info.UserEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = "User account deactivated";
                            mailDetail.Message = string.Format("Dear {0},", info.FirstName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Sub: User account deactivated";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Your account has been deactivated";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "For further details contact your administrator";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion User Deactivated
                        }
                        break;

                    case EntityType.TestSessionCreated:
                        {
                            #region Test Session Created

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");
                            mailDetail = new MailDetail();
                            TestSessionDetail testSessionDetail = new TestSessionDLManager().
                                GetCandidateSessions(entityID);
                            if (testSessionDetail == null)
                                break;
                            mailDetail.To = new List<string>();
                            mailDetail.To.Add(testSessionDetail.Email);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = string.Format("Test session '{0}' created", entityID);
                            mailDetail.Message = string.Format("Dear {0},", testSessionDetail.UserName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Sub: Test session '{0}' created", entityID);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("The test session {0} was created by you", entityID);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "The following candidate sessions are associated:";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += testSessionDetail.CandidateSessions;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            //
                            #endregion Test Session Created
                        }
                        break;

                    case EntityType.InterviewSessionCreated:
                        {
                            #region Interview Session Created

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");
                            mailDetail = new MailDetail();
                            TestSessionDetail testSessionDetail = new InterviewSessionDLManager().
                                GetInterviewCandidateSessions(entityID);
                            if (testSessionDetail == null)
                                break;
                            mailDetail.To = new List<string>();
                            mailDetail.To.Add(testSessionDetail.Email);
                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = string.Format("Interview session '{0}' created", entityID);
                            mailDetail.Message = string.Format("Dear {0},", testSessionDetail.UserName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Sub: Interview session '{0}' created", entityID);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("The Interview session {0} was created by you", entityID);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "The following candidate sessions are associated:";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += testSessionDetail.CandidateSessions;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            //
                            #endregion Test Session Created
                        }
                        break;

                    case EntityType.OnlineInterviewSessionCreated:
                        {
                            #region Online Interview Session Created

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");
                            mailDetail = new MailDetail();
                            TestSessionDetail testSessionDetail = new OnlineInterviewDLManager().
                                GetOnlineInterviewCandidateSessions(entityID);
                            if (testSessionDetail == null)
                                break;
                            mailDetail.To = new List<string>();

                            mailDetail.To.Add(testSessionDetail.Email);
                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = string.Format("Online interview session '{0}' created", entityID);
                            mailDetail.Message = string.Format("Dear {0},", testSessionDetail.UserName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Sub: Online interview session '{0}' created", entityID);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("The online interview session {0} was created by you", entityID);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "The following candidate sessions are associated:";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += testSessionDetail.CandidateSessions;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            //
                            #endregion Online Test Session Created
                        }
                        break;

                    case EntityType.TestSessionModified:
                        {
                            #region Test Session Modified

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            mailDetail = new MailDetail();

                            TestSessionDetail testSessionDetail = new TestSessionDLManager().
                                GetCandidateSessions(entityID);

                            if (testSessionDetail == null)
                                break;

                            mailDetail.To = new List<string>();
                            mailDetail.To.Add(testSessionDetail.Email);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = string.Format("Test session '{0}' modified", entityID);

                            mailDetail.Message = string.Format("Dear {0},", testSessionDetail.UserName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            mailDetail.Message += string.Format("Sub: Test session '{0}' modified", entityID);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("The test session {0} was modified by you", entityID);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "The following candidate sessions are associated:";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += testSessionDetail.CandidateSessions;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "The following candidate sessions are newly created";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += subEntityID;

                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();

                            #endregion Test Session Modified
                        }
                        break;

                    case EntityType.CandidateScheduled:
                    case EntityType.CandidateReScheduled:
                        {
                            #region CandidateScheduled & CandidateReScheduled

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                           

                            CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerDLManager().
                                GetCandidateTestSessionEmailDetail(entityID, attemptID);

                            if (candidateTestSessionDetail == null)
                                break;
                            if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateEmail))
                            {
                                mailDetail = new MailDetail();
                                // Instantiate 'to' address.
                                mailDetail.To = new List<string>();

                                // Add candidate email.
                                if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateEmail))
                                    mailDetail.To.Add(candidateTestSessionDetail.CandidateEmail);

                                // Instantiate 'cc' address.
                                mailDetail.CC = new List<string>();

                                // Instantiate 'bcc' address.
                                mailDetail.BCC = new List<string>();

                                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                                {
                                    foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                    {
                                        mailDetail.BCC.Add(cc);
                                    }
                                }

                                // Add candidate scheduler.
                                if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.SchedulerEmail))
                                    mailDetail.CC.Add(candidateTestSessionDetail.SchedulerEmail);

                                // Add candidate owner.
                                if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateOwnerEmail))
                                {
                                    // Check if mail ID already exist.
                                    if (mailDetail.CC.IndexOf(candidateTestSessionDetail.CandidateOwnerEmail) == -1)
                                        mailDetail.CC.Add(candidateTestSessionDetail.CandidateOwnerEmail);
                                }

                                // Add position profile owner.
                                if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.PositionProfileOwnerEmail))
                                {
                                    // Check if mail ID already exist.
                                    if (mailDetail.CC.IndexOf(candidateTestSessionDetail.PositionProfileOwnerEmail) == -1)
                                        mailDetail.CC.Add(candidateTestSessionDetail.PositionProfileOwnerEmail);
                                }

                                if (entityType == EntityType.CandidateScheduled)
                                {
                                    mailDetail.Subject = string.Format("Test '{0} - {1}' scheduled for you",
                                        candidateTestSessionDetail.TestID,
                                        candidateTestSessionDetail.TestName);
                                }
                                else if (entityType == EntityType.CandidateReScheduled)
                                {
                                    mailDetail.Subject = string.Format("Test '{0} - {1}' re-scheduled for you",
                                        candidateTestSessionDetail.TestID,
                                        candidateTestSessionDetail.TestName);
                                }

                                mailDetail.Message = string.Format("Dear {0},", candidateTestSessionDetail.CandidateName);
                                mailDetail.Message += Environment.NewLine;
                                mailDetail.Message += Environment.NewLine;

                                if (entityType == EntityType.CandidateScheduled)
                                {
                                    mailDetail.Message += string.Format("Sub: Test '{0} - {1}' scheduled for you",
                                        candidateTestSessionDetail.TestID,
                                        candidateTestSessionDetail.TestName);
                                }
                                else if (entityType == EntityType.CandidateReScheduled)
                                {
                                    mailDetail.Message += string.Format("Sub: Test '{0} - {1}' re-scheduled for you",
                                        candidateTestSessionDetail.TestID,
                                        candidateTestSessionDetail.TestName);
                                }
                                mailDetail.Message += Environment.NewLine;
                                mailDetail.Message += Environment.NewLine;
                                mailDetail.Message += string.Format("Your test will expire on {0}", candidateTestSessionDetail.ExpiryDate);
                                mailDetail.Message += Environment.NewLine;
                                mailDetail.Message += Environment.NewLine;
                                mailDetail.Message += "Your login credentials are given below:";
                                mailDetail.Message += Environment.NewLine;

                                // Get user name and password.
                                UserRegistrationInfo info = new UserRegistrationDLManager().GetUserRegistrationInfo
                                    (Convert.ToInt32(candidateTestSessionDetail.CandidateID));
                                if (info != null)
                                {
                                    mailDetail.Message += string.Format("User Name: {0}", info.UserEmail);
                                    mailDetail.Message += Environment.NewLine;
                                    mailDetail.Message += string.Format("Password: {0}", Decrypt(info.Password));
                                }

                                mailDetail.Message += Environment.NewLine;
                                mailDetail.Message += Environment.NewLine;
                                string url = ConfigurationManager.AppSettings["TEST_INSTRUCTION_URL"] + "?n=" + candidateTestSessionDetail.NavigateKey + "&tkey=" + tenantID;
                                mailDetail.Message += "Login to <a href='" + url + "' alt='' >ForteHCM</a> with your user name & password and take the test";
                                mailDetail.Message += GetClosureMessage();
                                mailDetail.Message += GetDisclaimerMessage();
                                mailDetail.isBodyHTML = true;
                            }

                            #endregion CandidateScheduled & CandidateReScheduled
                        }
                        break;


                    //case EntityType.InterviewCandidateScheduled:
                    case EntityType.InterviewCandidateReScheduled:
                        {
                            #region InterviewCandidateScheduled & InterviewCandidateReScheduled
                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                            mailDetail = new MailDetail();

                            CandidateInterviewSessionDetail candidateInterviewSessionDetail = new InterviewSchedulerDLManager().
                                GetCandidateInterviewSessionEmailDetail(entityID, attemptID);

                            if (candidateInterviewSessionDetail == null)
                                break;

                            // Instantiate 'to' address.
                            mailDetail.To = new List<string>();

                            // Add candidate email.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.CandidateEmail))
                                mailDetail.To.Add(candidateInterviewSessionDetail.CandidateEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }

                            // Add candidate scheduler.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.SchedulerEmail))
                                mailDetail.CC.Add(candidateInterviewSessionDetail.SchedulerEmail);

                            // Add candidate owner.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.CandidateOwnerEmail))
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(candidateInterviewSessionDetail.CandidateOwnerEmail) == -1)
                                    mailDetail.CC.Add(candidateInterviewSessionDetail.CandidateOwnerEmail);
                            }

                            // Add position profile owner.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.PositionProfileOwnerEmail))
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(candidateInterviewSessionDetail.PositionProfileOwnerEmail) == -1)
                                    mailDetail.CC.Add(candidateInterviewSessionDetail.PositionProfileOwnerEmail);
                            }

                            if (entityType == EntityType.InterviewCandidateScheduled)
                            {
                                mailDetail.Subject = string.Format("Interview '{0} - {1}' scheduled for you",
                                    candidateInterviewSessionDetail.InterviewTestID,
                                    candidateInterviewSessionDetail.InterviewTestName);
                            }
                            else if (entityType == EntityType.InterviewCandidateReScheduled)
                            {
                                mailDetail.Subject = string.Format("Interview '{0} - {1}' re-scheduled for you",
                                    candidateInterviewSessionDetail.InterviewTestID,
                                    candidateInterviewSessionDetail.InterviewTestName);
                            }

                            mailDetail.Message = string.Format("Dear {0},", candidateInterviewSessionDetail.CandidateName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            if (entityType == EntityType.InterviewCandidateScheduled)
                            {
                                mailDetail.Message += string.Format("Sub: Interview '{0} - {1}' scheduled for you",
                                    candidateInterviewSessionDetail.InterviewTestID,
                                    candidateInterviewSessionDetail.InterviewTestName);
                            }
                            else if (entityType == EntityType.InterviewCandidateReScheduled)
                            {
                                mailDetail.Message += string.Format("Sub: Interview '{0} - {1}' re-scheduled for you",
                                    candidateInterviewSessionDetail.InterviewTestID,
                                    candidateInterviewSessionDetail.InterviewTestName);
                            }
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Your interview will expire on {0}", candidateInterviewSessionDetail.ExpiryDate);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Your login credentials are given below:";
                            mailDetail.Message += Environment.NewLine;

                            // Get user name and password.
                            UserRegistrationInfo info = new UserRegistrationDLManager().GetUserRegistrationInfo
                                (Convert.ToInt32(candidateInterviewSessionDetail.CandidateID));
                            if (info != null)
                            {
                                mailDetail.Message += string.Format("User Name: {0}", info.UserEmail);
                                mailDetail.Message += Environment.NewLine;
                                mailDetail.Message += string.Format("Password: {0}", Decrypt(info.Password));
                            }

                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Login to <a href='" + candidateUrl + "' alt='' >ForteHCM</a> with your user name & password and take the interview";
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;
                            #endregion CandidateScheduled & CandidateReScheduled
                        }
                        break;

                    case EntityType.InterviewCandidateUnScheduled:
                        {
                            #region InterviewCandidateUnScheduled

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                            mailDetail = new MailDetail();

                            CandidateInterviewSessionDetail candidateInterviewSessionDetail = new InterviewSchedulerDLManager().
                                GetCandidateInterviewSessionEmailDetail(entityID, attemptID);

                            if (candidateInterviewSessionDetail == null)
                                break;

                            // Instantiate 'to' address.
                            mailDetail.To = new List<string>();

                            // Add candidate email.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.CandidateEmail))
                                mailDetail.To.Add(candidateInterviewSessionDetail.CandidateEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }

                            // Add candidate scheduler.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.SchedulerEmail))
                                mailDetail.CC.Add(candidateInterviewSessionDetail.SchedulerEmail);

                            // Add candidate owner.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.CandidateOwnerEmail))
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(candidateInterviewSessionDetail.CandidateOwnerEmail) == -1)
                                    mailDetail.CC.Add(candidateInterviewSessionDetail.CandidateOwnerEmail);
                            }

                            // Add position profile owner.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.PositionProfileOwnerEmail))
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(candidateInterviewSessionDetail.PositionProfileOwnerEmail) == -1)
                                    mailDetail.CC.Add(candidateInterviewSessionDetail.PositionProfileOwnerEmail);
                            }

                            mailDetail.Subject = string.Format("Interview '{0} - {1}' un-scheduled for you",
                                candidateInterviewSessionDetail.InterviewTestID,
                                candidateInterviewSessionDetail.InterviewTestName);

                            mailDetail.Message = string.Format("Dear {0},", candidateInterviewSessionDetail.CandidateName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            mailDetail.Message += string.Format("Interview '{0} - {1}' un-scheduled for you",
                                candidateInterviewSessionDetail.InterviewTestID,
                                candidateInterviewSessionDetail.InterviewTestName);

                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("You will not be allowed to take the interview");
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("For details contact your care taker");
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();

                            #endregion InterviewCandidateUnScheduled
                        }
                        break;

                    case EntityType.InterviewInitiated:
                        {
                            #region Interview Initiated

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                            mailDetail = new MailDetail();

                            CandidateInterviewSessionDetail candidateInterviewSessionDetail = new InterviewSchedulerDLManager().
                                GetCandidateInterviewSessionEmailDetail(entityID, attemptID);

                            if (candidateInterviewSessionDetail == null)
                                break;

                            // Instantiate 'to' address.
                            mailDetail.To = new List<string>();

                            // Add candidate email.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.CandidateEmail))
                                mailDetail.To.Add(candidateInterviewSessionDetail.CandidateEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = string.Format("Interview '{0}' initiated and security code generated",
                                candidateInterviewSessionDetail.InterviewTestName);

                            mailDetail.Message = string.Format("Dear {0},", candidateInterviewSessionDetail.CandidateName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            mailDetail.Message += string.Format("Sub: Interview '{0}' initiated and security code generated",
                                candidateInterviewSessionDetail.InterviewTestName);

                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Security code for the interview session is {0}", candidateInterviewSessionDetail.SecurityCode);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "You can now start the offline interview recording by entering the security code";
                            mailDetail.Message += Environment.NewLine;

                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion Interview Initiated
                        }
                        break;

                    case EntityType.InterviewInitiatedRegenerated:
                        {
                            #region Interview Initiated Regenerated

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                            mailDetail = new MailDetail();

                            CandidateInterviewSessionDetail candidateInterviewSessionDetail = new InterviewSchedulerDLManager().
                                GetCandidateInterviewSessionEmailDetail(entityID, attemptID);

                            if (candidateInterviewSessionDetail == null)
                                break;

                            // Instantiate 'to' address.
                            mailDetail.To = new List<string>();

                            // Add candidate email.
                            if (!Utility.IsNullOrEmpty(candidateInterviewSessionDetail.CandidateEmail))
                                mailDetail.To.Add(candidateInterviewSessionDetail.CandidateEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }

                            }
                            mailDetail.Subject = string.Format("Interview '{0}' initiated and security code regenerated",
                                candidateInterviewSessionDetail.InterviewTestName);

                            mailDetail.Message = string.Format("Dear {0},", candidateInterviewSessionDetail.CandidateName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            mailDetail.Message += string.Format("Sub: Interview '{0}' initiated and security code regenerated",
                                candidateInterviewSessionDetail.InterviewTestName);

                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Regenerated security code for the interview session is {0}", candidateInterviewSessionDetail.SecurityCode);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "You can now start the offline interview recording by entering the security code";
                            mailDetail.Message += Environment.NewLine;

                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion Interview Initiated Regenerated
                        }
                        break;

                    case EntityType.TestCompleted:
                        {
                            #region TestCompleted

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                            mailDetail = new MailDetail();

                            CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerDLManager().
                                GetCandidateTestSessionEmailDetail(entityID, attemptID);

                            if (candidateTestSessionDetail == null)
                                break;

                            // Instantiate 'to' address.
                            mailDetail.To = new List<string>();

                            // Add candidate scheduler.
                            if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.SchedulerEmail))
                                mailDetail.To.Add(candidateTestSessionDetail.SchedulerEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }

                            // Add position profile owner.
                            if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.PositionProfileOwnerEmail))
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.To.IndexOf(candidateTestSessionDetail.PositionProfileOwnerEmail) == -1)
                                    mailDetail.To.Add(candidateTestSessionDetail.PositionProfileOwnerEmail);
                            }

                            mailDetail.Subject = string.Format("Test '{0} - {1}' completed by '{2}'",
                                candidateTestSessionDetail.TestID,
                                candidateTestSessionDetail.TestName,
                                candidateTestSessionDetail.CandidateName);

                            mailDetail.Message = "Dear Viewer,";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            mailDetail.Message += string.Format("Test '{0} - {1}' completed by '{2}'",
                                candidateTestSessionDetail.TestID,
                                candidateTestSessionDetail.TestName,
                                candidateTestSessionDetail.CandidateName);

                            // Construct test results url.
                            testResultUrl += "?m=3&s=0";
                            testResultUrl += "&testkey=" + candidateTestSessionDetail.TestID;
                            testResultUrl += "&candidatesession=" + entityID;
                            testResultUrl += "&attemptid=" + attemptID;
                            testResultUrl += "&parentpage=mail";

                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Click <a href='" + testResultUrl + "' alt=''> here </a> to view the test results.";
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion TestCompleted
                        }
                        break;

                    case EntityType.TestRetakeRequest:
                        {
                            #region TestRetakeRequest

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                            mailDetail = new MailDetail();

                            CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerDLManager().
                                GetCandidateTestSessionEmailDetail(entityID, attemptID);

                            if (candidateTestSessionDetail == null)
                                break;

                            // Instantiate 'to' address.
                            mailDetail.To = new List<string>();

                            // Add candidate email.
                            if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateEmail))
                                mailDetail.To.Add(candidateTestSessionDetail.CandidateEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }

                            // Add candidate scheduler.
                            if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.SchedulerEmail))
                                mailDetail.CC.Add(candidateTestSessionDetail.SchedulerEmail);

                            // Add candidate owner.
                            if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateOwnerEmail))
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(candidateTestSessionDetail.CandidateOwnerEmail) == -1)
                                    mailDetail.CC.Add(candidateTestSessionDetail.CandidateOwnerEmail);
                            }

                            // Add position profile owner.
                            if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.PositionProfileOwnerEmail))
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(candidateTestSessionDetail.PositionProfileOwnerEmail) == -1)
                                    mailDetail.CC.Add(candidateTestSessionDetail.PositionProfileOwnerEmail);
                            }

                            if (entityType == EntityType.TestRetakeRequest)
                            {
                                mailDetail.Subject = string.Format("Test '{0} - {1}' scheduled for you as you requested to retake",
                                    candidateTestSessionDetail.TestID,
                                    candidateTestSessionDetail.TestName);
                            }
                            mailDetail.Message = string.Format("Dear {0},", candidateTestSessionDetail.CandidateName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            if (entityType == EntityType.TestRetakeRequest)
                            {
                                mailDetail.Message += string.Format("Sub: Test '{0} - {1}' scheduled for you as you requested to retake",
                                    candidateTestSessionDetail.TestID,
                                    candidateTestSessionDetail.TestName);
                            }

                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Your test will expire on {0}", candidateTestSessionDetail.ExpiryDate);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Login to <a href='" + candidateUrl + "' alt='' >ForteHCM</a> with your user name & password and take the test";
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion TestRetakeRequest
                        }
                        break;

                    case EntityType.TestCreatedForPositionProfile:
                        {
                            #region Test Created For Position Profile

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty. Test key is expected as entity ID");

                            // Get position profile test detail.
                            PositionProfileDetail positionProfileDetail = new TestDLManager().
                                GetPositionProfileTestDetail(entityID);

                            mailDetail = new MailDetail();

                            mailDetail.To = new List<string>();
                            mailDetail.To.Add(positionProfileDetail.PositionProfileOwnerEmail);
                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }

                            mailDetail.Subject = "Test created/associated with position profile";
                            mailDetail.Message = string.Format("Dear {0},", positionProfileDetail.PositionProfileOwnerName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Sub: Test created/associated with position profile";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "A test has been created/associated with the position profile you have created. The details are given below:";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Position Profile: " + positionProfileDetail.PositionProfileName;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Test Name: " + positionProfileDetail.TestName;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Test Description: " + positionProfileDetail.TestDescription;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            // Construct view test url.
                            viewTestUrl += "?m=1&s=2";
                            viewTestUrl += "&testkey=" + entityID;
                            viewTestUrl += "&parentpage=mail";

                            mailDetail.Message += "Click <a href='" + viewTestUrl + "' alt=''> here </a> to view the test.";
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion Test Created For Position Profile
                        }
                        break;
                    case EntityType.InterviewCreatedForPositionProfile:
                        {
                            #region Interview Created For Position Profile

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty. Interview key is expected as entity ID");

                            // Get position profile interview test detail.
                            PositionProfileDetail positionProfileDetail = new InterviewDLManager().
                                GetPositionProfileInterviewDetail(entityID);

                            mailDetail = new MailDetail();

                            mailDetail.To = new List<string>();
                            mailDetail.To.Add(positionProfileDetail.PositionProfileOwnerEmail);
                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = "Interview created/associated with position profile";
                            mailDetail.Message = string.Format("Dear {0},", positionProfileDetail.PositionProfileOwnerName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Sub: Interview created/associated with position profile";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "A interview has been created/associated with the position profile you have created. The details are given below:";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Position Profile: " + positionProfileDetail.PositionProfileName;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Interview Name: " + positionProfileDetail.InterviewName;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Interview Description: " + positionProfileDetail.InterviewDescription;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            // Construct view interview url.
                            viewInterviewUrl += "?m=1&s=2";
                            viewInterviewUrl += "&testkey=" + entityID;
                            viewInterviewUrl += "&parentpage=mail";

                            mailDetail.Message += "Click <a href='" + viewInterviewUrl + "' alt=''> here </a> to view the interview.";
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion Interview Created For Position Profile
                        }
                        break;
                    case EntityType.SelfAdminTestRecommendationSaved:
                        {
                            #region Self Admin Test Recommendation Saved

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            int genID = 0;
                            if (!int.TryParse(entityID, out genID))
                                throw new Exception("Entity ID is invalid. Expected test recommendation gen ID as integer");

                            mailDetail = new MailDetail();
                            TestRecommendationDetail info = new TestDLManager().GetCandidateRecommendedTest(genID);

                            if (info == null)
                                break;

                            mailDetail.To = new List<string>();
                            mailDetail.To.Add(info.CandidateEmail);
                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }
                            mailDetail.Subject = "Test recommendation saved";
                            mailDetail.Message = string.Format("Dear {0},", info.CandidateName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Sub: Test recommendation saved";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Your have successfully saved a test recommendation and the details are given below:";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Test Name : {0}", info.TestName);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += string.Format("Skills : {0}", info.Skill);
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "You can view the details under the section 'My Tests' at <a href='" + dashboardHomeUrl + "' alt=''> ForteHCM </a>";
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion Self Admin Test Recommendation Saved
                        }

                        break;

                    case EntityType.SelfAdminTestCompleted:
                        {
                            #region Self Admin Test Completed

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            if (Utility.IsNullOrEmpty(subEntityID))
                                throw new Exception("Sub entity ID cannot be empty");

                            int attemptID = 0;
                            if (!int.TryParse(subEntityID, out attemptID))
                                throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");

                            mailDetail = new MailDetail();

                            CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerDLManager().
                                GetCandidateTestSessionEmailDetail(entityID, attemptID);

                            if (candidateTestSessionDetail == null)
                                break;

                            // Instantiate 'to' address.
                            mailDetail.To = new List<string>();

                            // Add candidate email ID.
                            if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateEmail))
                                mailDetail.To.Add(candidateTestSessionDetail.CandidateEmail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }

                            mailDetail.Subject = string.Format("Test '{0}' completed",
                                candidateTestSessionDetail.TestName);

                            mailDetail.Message = "Dear " + candidateTestSessionDetail.CandidateName + ",";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;

                            mailDetail.Message += string.Format("You have completed the test '{0}",
                                candidateTestSessionDetail.TestName);

                            // Construct test results url.
                            candidateTestResultUrl += "?testkey=" + candidateTestSessionDetail.TestID;
                            candidateTestResultUrl += "&candidatesession=" + entityID;
                            candidateTestResultUrl += "&attemptid=" + attemptID;
                            candidateTestResultUrl += "&parentpage=MY_TST";

                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "You can view the results at <a href='" + candidateTestResultUrl + "' alt=''> ForteHCM </a>";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += "Please login to the application to share/publish your results into facebook or linkedin";
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += Environment.NewLine;
                            mailDetail.Message += GetClosureMessage();
                            mailDetail.Message += GetDisclaimerMessage();
                            mailDetail.isBodyHTML = true;

                            #endregion Self Admin Test Completed
                        }
                        break;

                    case EntityType.PositionProfileCandidateStatusChanged:
                        {
                            #region Position Profile Candidate Status Changed

                            if (Utility.IsNullOrEmpty(entityID))
                                throw new Exception("Entity ID cannot be empty");

                            int genID = 0;
                            if (!int.TryParse(entityID, out genID))
                                throw new Exception("Entity ID is invalid. Expected gen ID of position profile candidates table as integer");

                            mailDetail = new MailDetail();

                            // Retrieve position profile candidate detail.
                            PositionProfileCandidate candidateDetail = new PositionProfileDLManager().
                                GetPositionProfileCandidateDetailByGenID(genID);

                            if (candidateDetail == null)
                                break;

                            // Instantiate 'to' address.
                            mailDetail.To = new List<string>();

                            // Add recruiter mail ID.
                            if (!Utility.IsNullOrEmpty(candidateDetail.RecruiterEMail))
                                mailDetail.To.Add(candidateDetail.RecruiterEMail);

                            // Instantiate 'cc' address.
                            mailDetail.CC = new List<string>();

                            // Instantiate 'bcc' address.
                            mailDetail.BCC = new List<string>();

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                            {
                                foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                                {
                                    mailDetail.BCC.Add(cc);
                                }
                            }

                            mailDetail.Subject = string.Format("Status of candidate '{0}' for position '{1}' updated",
                               candidateDetail.CandidateName, candidateDetail.PositionProfileName);

                            // Compose message.
                            StringBuilder message = new StringBuilder();

                            message.AppendLine(string.Format("Dear {0},", candidateDetail.RecruiterFirstName));
                            message.AppendLine();

                            message.AppendLine(string.Format("The status of candidate '{0}' associated with the following position has been updated to ‘{1}’.",
                              candidateDetail.CandidateName, candidateDetail.CandidateStatusName));

                            message.AppendLine();

                            message.AppendLine(string.Format("Position name: {0}",
                                candidateDetail.PositionProfileName));
                            message.AppendLine(string.Format("Client name: {0}",
                                candidateDetail.ClientName));

                            message.AppendLine();

                            message.AppendLine(GetClosureMessage());
                            message.AppendLine(GetDisclaimerMessage());

                            mailDetail.Message = message.ToString();

                            mailDetail.isBodyHTML = true;

                            #endregion Position Profile Candidate Status Changed
                        }
                        break;

                    default:
                        break;
                }

                return mailDetail;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// Method that compose the <see cref="MailDetail"/> object for the 
        /// given entity ID and entity type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        /// <param name="subEntityID">
        /// A <see cref="string"/> that holds the sub entity ID.
        /// </param>
        /// <param name="ccAddress">
        /// A <see cref="string"/> that holds the CC address.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        /// <returns>
        /// A <see cref="MailDetail"/> that holds the mail detail.
        /// </returns>
        private MailDetail ComposeMail(EntityType entityType, string entityID,
            string subEntityID, string ccAddress, string message)
        {
            MailDetail mailDetail = null;

            try
            {
                switch (entityType)
                {
                    case EntityType.None:
                        break;
                    case EntityType.TestSessionCreated:
                        break;
                    case EntityType.CandidateScheduled:
                        break;
                    case EntityType.CandidateUnScheduled:
                        break;
                    case EntityType.CandidateReScheduled:
                        break;
                    case EntityType.TestRetakeRequest:
                        break;
                    case EntityType.TestActivationRequest:
                        break;
                    case EntityType.TestReminder:
                        break;
                    case EntityType.TestResults:
                        break;
                    case EntityType.Certificate:
                        break;
                    case EntityType.Report:
                        break;
                    case EntityType.CreditRequest:
                        #region Credit Request

                        if (Utility.IsNullOrEmpty(entityID))
                            throw new Exception("Entity ID cannot be empty");

                        int userID = 0;
                        if (!int.TryParse(entityID, out userID))
                            throw new Exception("Entity ID is invalid. Expected user ID as integer");

                        mailDetail = new MailDetail();
                        UserDetail userDetail = new CommonDLManager().GetUserDetail(userID);

                        mailDetail.From = userDetail.Email;

                        mailDetail.To = new List<string>();
                        mailDetail.To.Add(ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"]);

                        mailDetail.Subject = "Request for credits";
                        mailDetail.CC = new List<string>();
                        mailDetail.BCC = new List<string>();

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"]))
                        {
                            foreach (string cc in ConfigurationManager.AppSettings["TEST_ADMIN_EMAIL"].Split(new char[] { ',' }))
                            {
                                mailDetail.BCC.Add(cc);
                            }
                        }

                        // CC address.
                        if (!Utility.IsNullOrEmpty(ccAddress))
                        {
                            foreach (string cc in ccAddress.Split(new char[] { ',' }))
                            {
                                mailDetail.CC.Add(cc);
                            }
                        }

                        mailDetail.Message = "Dear Recruiter/Caretaker,";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "Sub: Request for credits";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += string.Format("A credit request was made by {0} with the request message:", userDetail.FirstName);
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += "\'";
                        mailDetail.Message += message;
                        mailDetail.Message += "\'";
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += Environment.NewLine;
                        mailDetail.Message += GetClosureMessage();
                        mailDetail.Message += GetDisclaimerMessage();

                        #endregion Credit Request
                        break;
                    default:
                        break;
                }

                return mailDetail;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// Method that will construct and returns the closure message.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the closure message.
        /// </returns>
        private string GetClosureMessage()
        {
            string message = Environment.NewLine;
            message += Environment.NewLine;
            message += "This is a system generated message. Please do not reply to this message.";
            message += Environment.NewLine;

            return message;
        }

        /// <summary>
        /// Method that will construct and returns the disclaimer message.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the disclaimer message.
        /// </returns>
        private string GetDisclaimerMessage()
        {
            string message = Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "=========================== End Of Disclaimer ============================";

            return message;
        }

        /// <summary>
        /// Method that retrieves the mime type for the given file extension.
        /// </summary>
        /// <param name="fileExtenion">
        /// A <see cref="string"/> that holds the file extension.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the mime type.
        /// </returns>
        private string GetMimeType(string fileExtenion)
        {
            string mimeType = string.Empty;
            switch (fileExtenion.Trim().ToLower())
            {
                case "doc":
                    mimeType = "application/msword";
                    break;
                case "pdf":
                    mimeType = "application/pdf";
                    break;
                case "rtf":
                    mimeType = "text/rtf";
                    break;
                case "jpg":
                    mimeType = "image/jpeg";
                    break;
                case "jpeg":
                    mimeType = "image/jpeg ";
                    break;
                case "bmp":
                    mimeType = "image/bmp";
                    break;
                case "tif":
                    mimeType = "image/tiff";
                    break;
                default:
                    mimeType = "application/octet-stream";
                    break;
            }
            return mimeType;
        }

        private string Decrypt(string encryptedText)
        {
            if ((encryptedText == "") || (encryptedText == null))
                throw new Exception("Please Provide Some Encrypted Text For Decryption");

            const string SALTVALUE = "FORT";
            const string STRINGIVVALUE = "FORTE-HCMENCRYPTI";
            const string PASSWORD = "SRALT";
            const string HASHALGORITHMNAME = "SHA1";

            byte[] bytIV = null;
            byte[] bytSalt = null;
            byte[] bytText = null;
            byte[] bytKey = null;
            byte[] bytOriginal = null;
            byte[] bytReturn = null;
            int OriginalDataLenght;
            MemoryStream objMememoryStream = null;
            CryptoStream objCryptptoStream = null;
            try
            {
                bytIV = Encoding.ASCII.GetBytes(STRINGIVVALUE);
                bytSalt = Encoding.ASCII.GetBytes(SALTVALUE);
                bytText = Convert.FromBase64String(encryptedText);
                PasswordDeriveBytes objPass = new PasswordDeriveBytes(PASSWORD, bytSalt, HASHALGORITHMNAME, 5);
                RijndaelManaged objRinjandel = new RijndaelManaged();
                bytKey = objPass.GetBytes(objRinjandel.KeySize / 8);
                objMememoryStream = new MemoryStream(bytText);
                objCryptptoStream = new CryptoStream(objMememoryStream, objRinjandel.CreateDecryptor(bytKey, bytIV), CryptoStreamMode.Read);
                bytOriginal = new byte[bytText.Length];
                OriginalDataLenght = objCryptptoStream.Read(bytOriginal, 0, bytOriginal.Length);
                objCryptptoStream.Close();
                objMememoryStream.Close();
                bytReturn = new byte[OriginalDataLenght];
                for (int i = 0; i < bytOriginal.Length; i++)
                {
                    if (Convert.ToString(bytOriginal[i]) == "0")
                    {
                        break;
                    }
                    bytReturn[i] = bytOriginal[i];
                }
                return Encoding.ASCII.GetString(bytReturn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objMememoryStream != null)
                    objMememoryStream = null;
                if (objCryptptoStream != null)
                    objCryptptoStream = null;
                bytIV = null;
                bytKey = null;
                bytOriginal = null;
                bytReturn = null;
                bytSalt = null;
                bytText = null;
            }
        }

        #endregion Private Methods

        #region "OfflineInterview Methods"

        /// <summary>
        /// Method that construct and returns the <see cref="MailMessage"/>
        /// object for the given details.
        /// </summary>
        /// <param name="toAddresses">
        /// A <see cref="string"/> that holds lsit of the to-address.
        /// </param>
        /// <param name="ccAddresses">
        /// A <see cref="string"/> that holds the list of cc-address.
        /// </param>
        /// <param name="subject">
        /// A <see cref="string"/> that holds the subject.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        /// <returns>
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </returns>
        private MailMessage OfflineInterviewGetMailMessage(List<string> toAddresses,
            List<string> ccAddresses, string subject, string message)
        {
            MailMessage mailMessage = new MailMessage();

            // From address.
            mailMessage.From = new MailAddress(ConfigurationManager.
                AppSettings["EMAIL_FROM_ADDRESS"]);

            // To address.
            if (toAddresses != null && toAddresses.Count != 0)
            {
                foreach (string toAddress in toAddresses)
                    mailMessage.To.Add(new MailAddress(toAddress));
            }

            // CC address.
            if (ccAddresses != null && ccAddresses.Count != 0)
            {
                foreach (string ccAddress in ccAddresses)
                    mailMessage.CC.Add(new MailAddress(ccAddress));
            }

            // Subject.
            mailMessage.Subject = subject;

            return mailMessage;
        }

        /// <summary>
        /// Method that send the mail.
        /// </summary>
        /// <param name="message">
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </param>
        private void OfflineInterviewSend(MailMessage message)
        {
            SmtpClient smtpClient = new SmtpClient(
                ConfigurationManager.AppSettings["SMTP_CLIENT"],
                Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_PORT"]));
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(
                ConfigurationManager.AppSettings["SMTP_USER"],
                ConfigurationManager.AppSettings["SMTP_PASSWORD"]);

            smtpClient.ServicePoint.MaxIdleTime = Convert.ToInt32
                (ConfigurationManager.AppSettings["MAX_IDLE_TIME"]);

            smtpClient.EnableSsl = Convert.ToBoolean
                (ConfigurationManager.AppSettings["USE_SECURE_SOCKET_LAYER"]);

            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            try
            {
                smtpClient.Send(message);
            }
            catch (SmtpFailedRecipientsException smtpFailedRecipientsException)
            {
                //Logger.ExceptionLog(smtpFailedRecipientsException);
                string failedRecipientList = string.Empty;
                for (int i = 0; i < smtpFailedRecipientsException.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = smtpFailedRecipientsException.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy
                            || status == SmtpStatusCode.MailboxUnavailable)
                    {
                        if (failedRecipientList == string.Empty)
                            failedRecipientList += OfflineInterviewGetMailID(smtpFailedRecipientsException.InnerExceptions[i].FailedRecipient);
                        else
                            failedRecipientList += ", " + OfflineInterviewGetMailID(smtpFailedRecipientsException.InnerExceptions[i].FailedRecipient);
                    }
                }

                EmailFailureLog(message, failedRecipientList, smtpFailedRecipientsException.InnerException.ToString(), 1);


                throw new Exception(string.Format
                    ("<br>The message could not be delivered to the recipient(s) {0}", failedRecipientList));


            }
            catch (SmtpException smtpException)
            {
                Logger.ExceptionLog(smtpException);

                string emailFailedList = message.To.ToString() + "," + message.CC.ToString();

                EmailFailureLog(message,
                    emailFailedList, smtpException.InnerException.ToString(), 1);

                if (smtpException.StatusCode == SmtpStatusCode.MailboxUnavailable)
                {
                    /*throw new Exception(string.Format
                        ("<br>The message could not be delivered to the recipient(s) {0}",
                        OfflineInterviewGetMailID(((SmtpFailedRecipientException)(smtpException)).FailedRecipient)));*/
                }
                /*else
                {
                    throw smtpException;
                }*/
            }
        }

        private string OfflineInterviewGetMailID(string recipient)
        {
            if (Utility.IsNullOrEmpty(recipient))
                return string.Empty;

            return recipient.Replace("<", string.Empty).Replace(">", string.Empty);
        }

        /// <summary>
        /// Method that compose the <see cref="MailDetail"/> object for the 
        /// given entity ID and entity type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data. This will be type 
        /// casted to specific type based on entity type.
        /// </param>
        /// <returns>
        /// A <see cref="MailDetail"/> that holds the mail detail.
        /// </returns>
        private MailMessage OfflineInterviewComposeMail(EntityType entityType, object data)
        {
            MailMessage mailDetail = null;

            switch (entityType)
            {
                case EntityType.Completed:
                case EntityType.TransmitCompleted:
                    {
                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        CandidateInterviewSessionDetail sessionDetail = data
                            as CandidateInterviewSessionDetail;

                        if (sessionDetail == null)
                            throw new Exception("Data cannot be null or empty");

                        mailDetail = new MailMessage();

                        // Add candidate scheduler as 'to' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.SchedulerEmail))
                            mailDetail.To.Add(new MailAddress(sessionDetail.SchedulerEmail));

                        // Add candidate owner as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.CandidateOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(new MailAddress(sessionDetail.CandidateOwnerEmail)) == -1)
                                mailDetail.CC.Add(new MailAddress(sessionDetail.CandidateOwnerEmail));
                        }

                        // Add position profile owner as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.PositionProfileOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(new MailAddress(sessionDetail.PositionProfileOwnerEmail)) == -1)
                                mailDetail.CC.Add(new MailAddress(sessionDetail.PositionProfileOwnerEmail));
                        }

                        // Add assessors as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.AssessorEmails))
                        {
                            // Get comma separated emails.
                            string[] assessorEmails = sessionDetail.AssessorEmails.Split(new char[] { ',' });

                            foreach (string assessorEmail in assessorEmails)
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(new MailAddress(assessorEmail)) == -1)
                                    mailDetail.CC.Add(new MailAddress(assessorEmail));
                            }
                        }

                        // Add subject.
                        mailDetail.Subject = string.Format("Interview '{0}' completed by '{1}'",
                            sessionDetail.InterviewTestName,
                            sessionDetail.CandidateName);

                        // Add message.
                        mailDetail.Body = "Dear Viewer,";
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;

                        mailDetail.Body += string.Format("Sub: Interview '{0}' completed by '{1}'",
                            sessionDetail.InterviewTestName,
                            sessionDetail.CandidateName);

                        // Construct interview results url.
                        string url = string.Format(
                            ConfigurationManager.AppSettings["INTERVIEW_RESULT_URL"],
                            sessionDetail.CandidateTestSessionID,
                            sessionDetail.InterviewTestKey,
                            sessionDetail.InterviewTestSessionID,
                            sessionDetail.AttemptID);

                        // Construct interview publish url
                        string urlWithScore = string.Format(
                            ConfigurationManager.AppSettings["INTERVIEW_RESULT_PUBLISH_URL"],
                            string.Concat(sessionDetail.InterviewPublishCode, "S"));

                        string urlWithOutScore = string.Format(
                            ConfigurationManager.AppSettings["INTERVIEW_RESULT_PUBLISH_URL"],
                            string.Concat(sessionDetail.InterviewPublishCode, "H"));

                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += "Click <a href='" + url + "' alt=''> here </a> to view the interview results.";
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;

                        mailDetail.Body += "Click here to view candidate interview response with score.";
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += urlWithScore;
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += "Click here to view candidate interview response without score.";
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += urlWithOutScore;
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;

                        mailDetail.Body += OfflineInterviewGetClosureMessage();
                        mailDetail.Body += OfflineInterviewGetDisclaimerMessage();
                        mailDetail.IsBodyHtml = true;
                        mailDetail.Body = mailDetail.Body.Replace("\r\n", "<br />");
                    }

                    break;

                case EntityType.AbandonInterview:
                    {
                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        CandidateInterviewSessionDetail sessionDetail = data
                            as CandidateInterviewSessionDetail;

                        if (sessionDetail == null)
                            throw new Exception("Data cannot be null or empty");

                        mailDetail = new MailMessage();

                        // Add candidate scheduler as 'to' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.SchedulerEmail))
                            mailDetail.To.Add(new MailAddress(sessionDetail.SchedulerEmail));

                        // Add candidate owner as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.CandidateOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(new MailAddress(sessionDetail.CandidateOwnerEmail)) == -1)
                                mailDetail.CC.Add(new MailAddress(sessionDetail.CandidateOwnerEmail));
                        }

                        // Add position profile owner as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.PositionProfileOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(new MailAddress(sessionDetail.PositionProfileOwnerEmail)) == -1)
                                mailDetail.CC.Add(new MailAddress(sessionDetail.PositionProfileOwnerEmail));
                        }

                        // Add assessors as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.AssessorEmails))
                        {
                            // Get comma separated emails.
                            string[] assessorEmails = sessionDetail.AssessorEmails.Split(new char[] { ',' });

                            foreach (string assessorEmail in assessorEmails)
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(new MailAddress(assessorEmail)) == -1)
                                    mailDetail.CC.Add(new MailAddress(assessorEmail));
                            }
                        }

                        // Add subject.
                        mailDetail.Subject = string.Format("Interview '{0}' abandoned by '{1}'",
                            sessionDetail.InterviewTestName,
                            sessionDetail.CandidateName);

                        // Add message.
                        mailDetail.Body = "Dear Viewer,";
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;

                        mailDetail.Body += string.Format("Sub: Interview '{0}' abandoned by '{1}'",
                            sessionDetail.InterviewTestName,
                            sessionDetail.CandidateName);
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;

                        mailDetail.Body += OfflineInterviewGetClosureMessage();
                        mailDetail.Body += OfflineInterviewGetDisclaimerMessage();
                        mailDetail.IsBodyHtml = true;
                        mailDetail.Body = mailDetail.Body.Replace("\r\n", "<br />");
                    }

                    break;

                case EntityType.Paused:
                    {
                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        CandidateInterviewSessionDetail sessionDetail = data
                            as CandidateInterviewSessionDetail;

                        if (sessionDetail == null)
                            throw new Exception("Data cannot be null or empty");

                        mailDetail = new MailMessage();

                        // Add candidate as 'to' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.CandidateEmail))
                            mailDetail.To.Add(new MailAddress(sessionDetail.CandidateEmail));

                        // Add candidate scheduler as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.SchedulerEmail))
                            mailDetail.CC.Add(new MailAddress(sessionDetail.SchedulerEmail));

                        // Add subject.
                        mailDetail.Subject = string.Format("Interview '{0}' paused",
                            sessionDetail.InterviewTestName);

                        // Add message.
                        mailDetail.Body = string.Format("Dear {0},", sessionDetail.CandidateName);
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;

                        mailDetail.Body += string.Format("Sub: Interview '{0}' paused",
                            sessionDetail.InterviewTestName);
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += "You have paused the interview";
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += "You can continue the offline interview recording later";
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += string.Format("Security code for the interview session is {0}", sessionDetail.SecurityCode);
                        mailDetail.Body += Environment.NewLine;
                        mailDetail.Body += Environment.NewLine;

                        mailDetail.Body += OfflineInterviewGetClosureMessage();
                        mailDetail.Body += OfflineInterviewGetDisclaimerMessage();
                        mailDetail.IsBodyHtml = true;
                        mailDetail.Body = mailDetail.Body.Replace("\r\n", "<br />");
                    }

                    break;

                default:
                    break;
            }

            return mailDetail;
        }

        /// <summary>
        /// Method that will construct and returns the closure message.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the closure message.
        /// </returns>
        private string OfflineInterviewGetClosureMessage()
        {
            string message = Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("This is a system generated message");
            message += Environment.NewLine;
            message += string.Format("No need to reply to this message");
            message += Environment.NewLine;

            return message;
        }

        /// <summary>
        /// Method that will construct and returns the disclaimer message.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the disclaimer message.
        /// </returns>
        private string OfflineInterviewGetDisclaimerMessage()
        {
            string message = Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "=========================== End Of Disclaimer ============================";

            return message;
        }

        /// <summary>
        /// Method that retrieves the mime type for the given file extension.
        /// </summary>
        /// <param name="fileExtenion">
        /// A <see cref="string"/> that holds the file extension.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the mime type.
        /// </returns>
        private string OfflineInterviewGetMimeType(string fileExtenion)
        {
            string mimeType = string.Empty;
            switch (fileExtenion.Trim().ToLower())
            {
                case "doc":
                    mimeType = "application/msword";
                    break;
                case "pdf":
                    mimeType = "application/pdf";
                    break;
                case "rtf":
                    mimeType = "text/rtf";
                    break;
                case "jpg":
                    mimeType = "image/jpeg";
                    break;
                case "jpeg":
                    mimeType = "image/jpeg ";
                    break;
                case "bmp":
                    mimeType = "image/bmp";
                    break;
                case "tif":
                    mimeType = "image/tiff";
                    break;
                default:
                    mimeType = "application/octet-stream";
                    break;
            }
            return mimeType;
        }

        /// <summary>
        /// Method that compose and send for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data. This will be type 
        /// casted to specific type based on entity type.
        /// </param>
        public bool OfflineInterviewSendMail(EntityType entityType, object data)
        {
            try
            {
                // Compose mail.
                MailMessage mailMessage = OfflineInterviewComposeMail(entityType, data);

                if (mailMessage == null)
                    return false;

                // Assign from address.
                mailMessage.From = new MailAddress
                    (ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"]);

                // Send mail.
                OfflineInterviewSend(mailMessage);
                return true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                return false;
                //Logger.ExceptionLog(exp);
                //throw new Exception("Email could not be sent "+exp);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <param name="ccId"></param>
        /// <param name="faileReceints"></param>
        /// <param name="exceptionMsg"></param>
        /// <param name="userId"></param>
        private void EmailFailureLog(MailMessage mailMsg, string faileReceints,
            string exceptionMsg, int userId)
        {
            EmailExceptionDLManager emailExceptionManager =
                        new EmailExceptionDLManager();

            string mailBody = null;
            string mailFromAddr = null;
            string mailToAddr = null;
            string mailCCAddr = null;

            if (!Utility.IsNullOrEmpty(mailMsg.Body))
                mailBody = mailMsg.Body.ToString();

            if (!Utility.IsNullOrEmpty(mailMsg.From))
                mailFromAddr = mailMsg.From.ToString();

            if (!Utility.IsNullOrEmpty(mailMsg.To))
                mailToAddr = mailMsg.To.ToString();

            if (!Utility.IsNullOrEmpty(mailMsg.CC))
                mailCCAddr = mailMsg.CC.ToString();

            string msgType = "N";
            if (mailMsg.IsBodyHtml)
                msgType = "Y";

            emailExceptionManager.InsertEmailFailureLog(mailBody,
                  mailFromAddr, mailToAddr, mailCCAddr, faileReceints, exceptionMsg, userId,
                  mailMsg.Subject.Trim(), msgType);
        }

        /// <summary>
        /// Method that constructs and returns the task owner role message for  
        /// email alerts.
        /// </summary>
        /// <param name="taskOwnerRole">
        /// A <see cref="string"/> that holds the task owner role name.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the message to be embedded
        /// in the email contents.
        /// </returns>
        private string GetTaskOwnerRoleMessage(string taskOwnerRole)
        {
            switch (taskOwnerRole)
            {
                case Constants.PositionProfileOwners.RECRUITER_ASSIGNMENT_CREATOR:
                    return "Recruiter Assigner";

                case Constants.PositionProfileOwners.CANDIDATE_ASSOCIATION_CREATOR:
                    return "Candidate Associator";

                case Constants.PositionProfileOwners.INTERVIEW_CREATOR:
                    return "Interview Creator";

                case Constants.PositionProfileOwners.INTERVIEW_SESSION_CREATOR:
                    return "Interview Session Creator";

                case Constants.PositionProfileOwners.INTERVIEW_SCHEDULER:
                    return "Interview Scheduler";

                case Constants.PositionProfileOwners.ASSESSOR_ASSIGNMENT:
                    return "Assessor Assigner";

                case Constants.PositionProfileOwners.TEST_CREATOR:
                    return "Test Creator";

                case Constants.PositionProfileOwners.TEST_SESSION_CREATOR:
                    return "Test Session Creator";

                case Constants.PositionProfileOwners.TEST_SCHEDULER:
                    return "Test Scheduler";

                case Constants.PositionProfileOwners.SUBMITTAL_TO_CLIENT:
                    return "Submittal To Client";

                case Constants.PositionProfileOwners.CANDIDATE_RECRUITER:
                    return "Recruiter";

                case Constants.PositionProfileOwners.CO_OWNER:
                    return "Co-Owner";

                case Constants.PositionProfileOwners.OWNER:
                    return "Owner";

                default:
                    return "";
            }
        }

        #endregion
    }
}