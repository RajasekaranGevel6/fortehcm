﻿namespace Forte.HCM.ReminderService
{
    partial class ServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServiceProcessInstaller_testReminderServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ServiceInstaller_testReminderServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ServiceProcessInstaller_testReminderServiceProcessInstaller
            // 
            this.ServiceProcessInstaller_testReminderServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ServiceProcessInstaller_testReminderServiceProcessInstaller.Password = null;
            this.ServiceProcessInstaller_testReminderServiceProcessInstaller.Username = null;
            // 
            // ServiceInstaller_testReminderServiceInstaller
            // 
            this.ServiceInstaller_testReminderServiceInstaller.Description = "Runs on specific timer intervals and send reminder mails to candidates based on t" +
                "heir test reminder & resume reminder settings";
            this.ServiceInstaller_testReminderServiceInstaller.DisplayName = "ForteHCM Reminder Service";
            this.ServiceInstaller_testReminderServiceInstaller.ServiceName = "ForteHCM Reminder Service";
            this.ServiceInstaller_testReminderServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.ServiceInstaller_testReminderServiceInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ServiceInstaller_testReminderServiceInstaller_AfterInstall);
            // 
            // ServiceInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ServiceProcessInstaller_testReminderServiceProcessInstaller,
            this.ServiceInstaller_testReminderServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ServiceProcessInstaller_testReminderServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ServiceInstaller_testReminderServiceInstaller;
    }
}