﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestReminderService.cs
// File that represents the Windows Service class that sends the reminder mails
// to the candidates. The reminders already set by the candidates. This service
// runs at specified intervals that was defined in the config file.

#endregion

#region Directives                                                             

using System;
using System.Timers;
using System.Configuration;
using System.ServiceProcess;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.Exceptions;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.ReminderService
{
    /// <summary>
    /// Class that represents Windows Service that sends the reminder mails to
    /// the candidates. The reminders already set by the candidates. This 
    /// service runs at specified intervals that was defined in the config 
    /// file.
    /// </summary>
    public partial class TestReminderService : ServiceBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="Timer"/> that holds the timer object.
        /// </summary>
        private Timer reminderTimer = null;

        /// <summary>
        /// A <see cref="int"/> that holds the relative time.
        /// </summary>
        private int relativeTime = 0;

        /// <summary>
        /// A <see cref="EmailHandler"/> that holds the email handler object.
        /// </summary>
        private EmailHandler emailHandler = null;

        /// <summary>
        /// A <see cref="string"/> that holds the from-email address.
        /// </summary>
        private string fromEmailAddress = null;

        /// <summary>
        /// A <seealso cref="int"/> that holds the user ID.
        /// </summary>
        private int userID = 0;

        #endregion Private Variables

        #region Private Constants                                              

        /// <summary>
        /// A <see cref="int"/> that holds the milli seconds conversion unit 
        /// factor.
        /// </summary>
        private const int MILLI_SECONDS_CONVERSION_UNIT = 1000;

        /// <summary>
        /// A <see cref="int"/> that holds the minimum timer interval 
        /// (in seconds).
        /// </summary>
        private const int MINIMUM_TIMER_INTERVAL_SECONDS = 60;

        /// <summary>
        /// A <see cref="int"/> that holds the default timer interval 
        /// (in seconds).
        /// </summary>
        private const int DEFAULT_TIMER_INTERVAL_SECONDS = 300;

        /// <summary>
        /// A <see cref="int"/> that holds the minimum relative time difference
        /// (in seconds).
        /// </summary>
        private const int MINIMUM_RELATIVE_TIME_DIFFERENCE_SECONDS = 300;

        /// <summary>
        /// A <see cref="int"/> that holds the default relative time difference
        /// (in seconds).
        /// </summary>
        private const int DEFAULT_RELATIVE_TIME_DIFFERENCE_SECONDS = 300;

        #endregion Private Constants

        #region Constructor                                                    

        /// <summary>
        /// Method that represents the constructor.
        /// </summary>
        public TestReminderService()
        {
            InitializeComponent();
        }

        #endregion Constructor

        #region Event Handlers                                                 

        /// <summary>
        /// Method that is called when the service is being started.
        /// </summary>
        /// <param name="args">
        /// An array of <see cref="string"/> that holds the arguments.
        /// </param>
        /// <remarks>
        /// This will start the test reminder service timer.
        /// </remarks>
        protected override void OnStart(string[] args)
        {
            try
            {
                Logger.TraceLog("Trying to start test reminder service");

                // Retrieve and convert the user ID from config file.
                if (!int.TryParse(ConfigurationManager.AppSettings
                    ["USER_ID"], out userID))
                {
                    Logger.ExceptionLog("User ID not configured properly in config file. " +
                        "Cannot start the reminder service");

                    return;
                }

                // Validate is the user ID is valid or not.
                if (new CommonBLManager().GetUserDetail(userID) == null)
                {
                    Logger.ExceptionLog("User ID present in the config file is not valid. " +
                        "Cannot start the reminder service");

                    return;
                }

                // Retrieve and convert timer interval seconds from config file.
                int timerInterval = 0;
                if (!int.TryParse(ConfigurationManager.AppSettings
                    ["TIMER_INTERVAL_SECONDS"], out timerInterval))
                {
                    timerInterval = DEFAULT_TIMER_INTERVAL_SECONDS;
                    Logger.ExceptionLog(string.Format("Timer interval seconds not configured properly in config file. " +
                        "The default seconds {0} is considered", DEFAULT_TIMER_INTERVAL_SECONDS));
                }

                // Check if timer interval is lesser than the minimum setting.
                if (timerInterval < MINIMUM_TIMER_INTERVAL_SECONDS)
                {
                    timerInterval = MINIMUM_TIMER_INTERVAL_SECONDS;
                    Logger.ExceptionLog(string.Format("Timer interval seconds is less than the minumum setting in the config file. " +
                        "The minimum seconds {0} is considered", MINIMUM_TIMER_INTERVAL_SECONDS));
                }

                // Convert timer interval seconds to milli seconds.
                timerInterval = timerInterval * MILLI_SECONDS_CONVERSION_UNIT;

                // Retrieve and convert relative time difference seconds from config file.
                if (!int.TryParse(ConfigurationManager.AppSettings
                    ["RELATIVE_TIME_DIFFERENCE_SECONDS"], out relativeTime))
                {
                    relativeTime = DEFAULT_RELATIVE_TIME_DIFFERENCE_SECONDS;
                    Logger.ExceptionLog(string.Format("Relative time difference seconds not configured properly in config file. " +
                        "The default seconds {0} is considered", DEFAULT_RELATIVE_TIME_DIFFERENCE_SECONDS));
                }

                // Check if relative time difference is lesser than the minimum setting.
                if (relativeTime < MINIMUM_RELATIVE_TIME_DIFFERENCE_SECONDS)
                {
                    relativeTime = MINIMUM_RELATIVE_TIME_DIFFERENCE_SECONDS;
                    Logger.ExceptionLog(string.Format("Relative time difference seconds is less than the minumum setting in the config file. " +
                        "The minimum seconds {0} is considered", MINIMUM_RELATIVE_TIME_DIFFERENCE_SECONDS));
                }

                // Retrieve from email address from config file.
                fromEmailAddress = ConfigurationManager.AppSettings
                    ["EMAIL_FROM_ADDRESS"].Trim();

                if (Utility.IsNullOrEmpty(fromEmailAddress))
                {
                    Logger.ExceptionLog("From email address in the config file is empty. " +
                        "Cannot start the reminder service");

                    return;
                }

                // Instantiate email handler object.
                emailHandler = new EmailHandler();

                // Instantiate and start the timer.                
                reminderTimer = new Timer(timerInterval);
                reminderTimer.Elapsed += new ElapsedEventHandler(reminderTimer_Elapsed);
                reminderTimer.Start();

                Logger.TraceLog("Test reminder service started successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that is called when the service is being stopped.
        /// </summary>
        /// <remarks>
        /// This will stop the test reminder service timer.
        /// </remarks>
        protected override void OnStop()
        {
            try
            {
                Logger.TraceLog("Trying to stop test reminder service");

                if (reminderTimer != null)
                {
                    reminderTimer.Stop();
                    reminderTimer = null;
                }

                Logger.TraceLog("Test reminder service stopped successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that is called when the test reminder service timer is being
        /// elapsed.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that hold the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ElapsedEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will retrieve the list of reminder list for the specific time
        /// and send the reminder mail to them.
        /// </remarks>
        protected void reminderTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                Logger.TraceLog("Elapsed : " + e.SignalTime);

                // Process test reminders.
                ProcessTestReminders();

                // Process interview reminders.
                ProcessInterviewReminders();

                // Process resume reminders.
                ProcessResumeReminders();

                // Process online interview reminders.
                ProcessOnlineInterviewReminders();

                // Process expired slot request
                ProcessExpiredSlotRequest();

                // Process expired online interview sessions
                ProcessExpiredOnlineInterview();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that process the test reminders.
        /// </summary>
        private void ProcessTestReminders()
        {
            // Retrieve the list of test reminder list for the current date/time
            // and relative time difference.
            List<TestReminderDetail> testReminderList = new
                TestSchedulerBLManager().GetTestReminderSenderList
                (DateTime.Now, relativeTime);

            // Check if the reminder sender list is empty or not.
            if (testReminderList == null || testReminderList.Count == 0)
            {
                Logger.TraceLog(string.Format("No test reminder list found for the date/time {0}",
                    DateTime.Now));

                return;
            }

            // Log the reminder list count.
            Logger.TraceLog(string.Format("{0} test reminders found for the date/time {1}",
                testReminderList.Count, DateTime.Now));

            // Instantiate BL object.
            TestSchedulerBLManager blManager = new TestSchedulerBLManager();

            // Loop through the list and send reminder mails.
            foreach (TestReminderDetail reminderDetail in testReminderList)
            {
                try
                {
                    // Send email.
                    emailHandler.SendMail(
                        fromEmailAddress,
                        reminderDetail.EmailID,
                        null,
                        GetSubject(reminderDetail),
                        GetMessage(reminderDetail));
                }
                catch (EMailException emailException)
                {
                    Logger.ExceptionLog(emailException);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }

                try
                {
                    // Update test reminder sent status.
                    blManager.UpdateTestReminderStatus(reminderDetail.CandidateSessionID,
                        reminderDetail.AttemptID, reminderDetail.IntervalID, true, userID);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }
            }
        }

        /// <summary>
        /// Method that process the interview reminders.
        /// </summary>
        private void ProcessInterviewReminders()
        {
            // Retrieve the list of interview reminder list for the current date/time
            // and relative time difference.
            List<InterviewReminderDetail> interviewReminderList = new
                InterviewSchedulerBLManager().GetInterviewReminderSenderList
                (DateTime.Now, relativeTime);

            // Check if the reminder sender list is empty or not.
            if (interviewReminderList == null || interviewReminderList.Count == 0)
            {
                Logger.TraceLog(string.Format("No interview reminder list found for the date/time {0}",
                    DateTime.Now));

                return;
            }

            // Log the reminder list count.
            Logger.TraceLog(string.Format("{0} interview reminders found for the date/time {1}",
                interviewReminderList.Count, DateTime.Now));

            // Instantiate BL object.
            InterviewSchedulerBLManager blManager = new InterviewSchedulerBLManager();

            // Loop through the list and send reminder mails.
            foreach (InterviewReminderDetail reminderDetail in interviewReminderList)
            {
                try
                {
                    // Send email.
                    emailHandler.SendMail(
                        fromEmailAddress,
                        reminderDetail.EmailID,
                        "rtc@srasys.co.in",
                        GetSubject(reminderDetail),
                        GetMessage(reminderDetail));
                }
                catch (EMailException emailException)
                {
                    Logger.ExceptionLog(emailException);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }

                try
                {
                    // Update interview reminder sent status.
                    blManager.UpdateInterviewReminderStatus(reminderDetail.CandidateSessionID,
                        reminderDetail.AttemptID, reminderDetail.IntervalID, true, userID);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }
            }
        }

        /// <summary>
        /// Method that process the online interview reminders.
        /// </summary>
        private void ProcessOnlineInterviewReminders()
        {
            // Retrieve the list of online interview reminder list for the current date/time
            // and relative time difference.
            List<InterviewReminderDetail> onlineInterviewReminderList = new
                OnlineInterviewBLManager().GetOnlineInterviewReminderSenderList
                (DateTime.Now, relativeTime);

            // Check if the reminder sender list is empty or not.
            if (onlineInterviewReminderList == null || onlineInterviewReminderList.Count == 0)
            {
                Logger.TraceLog(string.Format("No online interview reminder list found for the date/time {0}",
                    DateTime.Now));

                return;
            }

            // Log the reminder list count.
            Logger.TraceLog(string.Format("{0} online interview reminders found for the date/time {1}",
                onlineInterviewReminderList.Count, DateTime.Now));

            // Instantiate BL object.
            OnlineInterviewBLManager blManager = new OnlineInterviewBLManager();

            // Loop through the list and send reminder mails.
            foreach (InterviewReminderDetail reminderDetail in onlineInterviewReminderList)
            {
                try
                {
                    // Send reminder email to candidate.
                    emailHandler.SendMail(
                        fromEmailAddress,
                        reminderDetail.EmailID,
                        null,
                        GetSubject(reminderDetail),
                        GetMessage(reminderDetail));

                    // Send reminder email to assessor
                    List<AssessorDetail> assessors = new List<AssessorDetail>();
                    assessors = blManager.GetOnlineInterviewScheduledAssessorByCandidateID(reminderDetail.CandidateInterviewID);

                    if (assessors != null && assessors.Count > 0)
                    {
                        foreach (AssessorDetail assessor in assessors)
                        {
                            emailHandler.SendMail(
                                fromEmailAddress,
                                assessor.UserEmail,
                                null,
                                GetSubject(reminderDetail),
                                GetMessage(reminderDetail,assessor.FirstName)
                                );
                        }
                    }
                }
                catch (EMailException emailException)
                {
                    Logger.ExceptionLog(emailException);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }

                try
                {
                    // Update online interview reminder sent status.
                    blManager.UpdateOnlineInterviewReminderStatus(reminderDetail.CandidateInterviewID,
                         reminderDetail.IntervalID, true, userID);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }
            }
        }

        /// <summary>
        /// Method that updates the online interview status to expired/session end.
        /// </summary>
        private void ProcessExpiredOnlineInterview()
        {
            // Retrieve the list of online interview expired list for the current date/time
            // and relative time difference.
            List<CandidateDetail> expiredOnlineInterviewList = new
                OnlineInterviewBLManager().GetExpiredOnlineInterviewList
                (DateTime.Now, relativeTime);

            // Check if the expired online interview list is empty or not.
            if (expiredOnlineInterviewList == null || expiredOnlineInterviewList.Count == 0)
            {
                Logger.TraceLog(string.Format("No expired online interview list found for the date/time {0}",
                    DateTime.Now));

                return;
            }

            // Log the expired list count.
            Logger.TraceLog(string.Format("{0} expired online interview  found for the date/time {1}",
                expiredOnlineInterviewList.Count, DateTime.Now));

            // Instantiate BL object.
            OnlineInterviewBLManager blManager = new OnlineInterviewBLManager();

            // Loop through the list and send reminder mails.
            foreach (CandidateDetail candidateDetail in expiredOnlineInterviewList)
            {
                try
                {
                    // Update the expired online interview session status
                    blManager.UpdateOnlineInterviewSessionStatus(candidateDetail.CandidateInterviewID);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }
            }
        }

        /// <summary>
        /// Method that process the expired slots requests.
        /// </summary>
        private void ProcessExpiredSlotRequest()
        {
            // Retrieve the list of expired slot request list for the current date/time
            // and relative time difference.
            List<CandidateDetail> expiredSlotRequestList = new
                OnlineInterviewBLManager().GetExpiredSlotRequestList
                (DateTime.Now, relativeTime);

            // Check if the expired slot list is empty or not.
            if (expiredSlotRequestList == null || expiredSlotRequestList.Count == 0)
            {
                Logger.TraceLog(string.Format("No expired slot request list found for the date/time {0}",
                    DateTime.Now));

                return;
            }

            // Log the reminder list count.
            Logger.TraceLog(string.Format("{0} expired slot request list found for the date/time {1}",
                expiredSlotRequestList.Count, DateTime.Now));

            // Instantiate BL object.
            OnlineInterviewBLManager blManager = new OnlineInterviewBLManager();

            // Loop through the list and send reminder mails.
            foreach (CandidateDetail candidateDetail in expiredSlotRequestList)
            {
                try
                {
                    // release all the slot requested agaist this candidate interview id
                    blManager.UnScheduleOlineInterviewCandidate(candidateDetail.CandidateInterviewID);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }
            }
        }

        /// <summary>
        /// Method that process the resume reminders.
        /// </summary>
        private void ProcessResumeReminders()
        {
            // Retrieve the list of resume reminder list for the current date/time
            // and relative time difference.
            List<ResumeReminderDetail> resumeReminderList = new
                CandidateBLManager().GetResumeReminderSenderList(DateTime.Now);

            // Check if the reminder sender list is empty or not.
            if (resumeReminderList == null || resumeReminderList.Count == 0)
            {
                Logger.TraceLog(string.Format("No resume reminder list found for the date/time {0}",
                    DateTime.Now));

                return;
            }

            // Log the reminder list count.
            Logger.TraceLog(string.Format("{0} resume reminders found for the date/time {1}",
                resumeReminderList.Count, DateTime.Now));

            // Instantiate BL object.
            CandidateBLManager blManager = new CandidateBLManager();

            // Loop through the list and send reminder mails.
            foreach (ResumeReminderDetail reminderDetail in resumeReminderList)
            {
                try
                {
                    // Send email.
                    emailHandler.SendMail(
                        fromEmailAddress,
                        reminderDetail.EmailID,
                        "rtc@srasys.co.in",
                        GetSubject(),
                        GetMessage(reminderDetail));
                }
                catch (EMailException emailException)
                {
                    Logger.ExceptionLog(emailException);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }

                try
                {
                    // Update resume reminder sent status.
                    blManager.UpdateResumeReminderStatus(reminderDetail.ID, true, userID);
                }
                catch (Exception exception)
                {
                    Logger.ExceptionLog(exception);
                }
            }
        }

        /// <summary>
        /// Method that constructs and returns the mail subject for the given
        /// test reminder detail.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="TestReminderDetail"/> that holds the test reminder 
        /// detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed subject. Returns 
        /// empty string when the reminder detail object is null.
        /// </returns>
        private string GetSubject(TestReminderDetail reminderDetail)
        {
            if (reminderDetail == null)
                return string.Empty;

            return string.Format("ForteHCM: Reminder for test '{0} - {1}'", 
                reminderDetail.TestID, reminderDetail.TestName);
        }

        /// <summary>
        /// Method that constructs and returns the mail subject for the given
        /// interview reminder detail.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="InterviewReminderDetail"/> that holds the interview 
        /// reminder detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed subject. Returns 
        /// empty string when the reminder detail object is null.
        /// </returns>
        private string GetSubject(InterviewReminderDetail reminderDetail)
        {
            if (reminderDetail == null)
                return string.Empty;

            return string.Format("ForteHCM: Reminder for interview '{0} - {1}'",
                reminderDetail.InterviewID, reminderDetail.InterviewName);
        }


        /// <summary>
        /// Method that constructs and returns the mail subject for the given
        /// resume reminder detail.
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds the composed subject.
        /// </returns>
        private string GetSubject()
        {
            return string.Format("ForteHCM: Reminder for uploading resume");
        }

        /// <summary>
        /// Method that constructs and returns the mail message for the given
        /// test reminder detail.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="TestReminderDetail"/> that holds the test reminder 
        /// detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed message. Returns 
        /// empty string when the reminder detail object is null.
        /// </returns>
        private string GetMessage(TestReminderDetail reminderDetail)
        {
            if (reminderDetail == null)
                return string.Empty;

            string message = string.Format("Dear {0},", reminderDetail.CandidateName);
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("Sub: Reminder for test '{0} - {1}'", reminderDetail.TestID, reminderDetail.TestName);
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("Your test will expire on {0}", reminderDetail.ExpiryDate);
            message += Environment.NewLine;
            message += string.Format("You planned to take test on {0}", reminderDetail.ExpectedDate);
            message += Environment.NewLine;
            message += string.Format("This reminder is sent to you before '{0}' from your planned date", reminderDetail.IntervalDescription);
            message += Environment.NewLine;
            message += string.Format("Login with your user ID & password and take the test");
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("This is a system generated message");
            message += Environment.NewLine;
            message += string.Format("No need to reply to this message");
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "============================ End Of Disclaimer ============================";

            return message;
        }

        /// <summary>
        /// Method that constructs and returns the mail message for the given
        /// interview reminder detail.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="InterviewReminderDetail"/> that holds the interview reminder
        /// detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed message. Returns 
        /// empty string when the reminder detail object is null.
        /// </returns>
        private string GetMessage(InterviewReminderDetail reminderDetail)
        {
            if (reminderDetail == null)
                return string.Empty;

            string message = string.Format("Dear {0},", reminderDetail.CandidateName);
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("Sub: Reminder for interview '{0} - {1}'", reminderDetail.InterviewID, reminderDetail.InterviewName);
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("Your interview will expire on {0}", reminderDetail.ExpiryDate);
            message += Environment.NewLine;
            message += string.Format("You planned to take interview on {0}", reminderDetail.ExpectedDate);
            message += Environment.NewLine;
            message += string.Format("This reminder is sent to you before '{0}' from your planned date", reminderDetail.IntervalDescription);
            message += Environment.NewLine;
            message += string.Format("Login with your user ID & password and take the interview");
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("This is a system generated message");
            message += Environment.NewLine;
            message += string.Format("No need to reply to this message");
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "============================ End Of Disclaimer ============================";

            return message;
        }

        /// <summary>
        /// Method that constructs and returns the mail message for the given
        /// interview reminder detail.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="InterviewReminderDetail"/> that holds the interview reminder
        /// detail.
        /// </param>
        /// <param name="assessorName">
        /// A <see cref="String"/> that holds the assessor name
        /// detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed message. Returns 
        /// empty string when the reminder detail object is null.
        /// </returns>
        private string GetMessage(InterviewReminderDetail reminderDetail,
            string assessorName)
        {
            if (reminderDetail == null)
                return string.Empty;

            string message = string.Format("Dear {0},", assessorName);
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("Sub: Reminder for interview '{0} - {1}'", reminderDetail.InterviewID, reminderDetail.InterviewName);
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("Your interview will expire on {0}", reminderDetail.ExpiryDate);
            message += Environment.NewLine;
            message += string.Format("You planned to conduct interview on {0}", reminderDetail.ExpectedDate);
            message += Environment.NewLine;
            message += string.Format("This reminder is sent to you before '{0}' from your planned date", reminderDetail.IntervalDescription);
            message += Environment.NewLine;
            message += string.Format("Login with your user ID & password and conduct the interview");
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("This is a system generated message");
            message += Environment.NewLine;
            message += string.Format("No need to reply to this message");
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "============================ End Of Disclaimer ============================";

            return message;
        }

        /// <summary>
        /// Method that constructs and returns the mail message for the given
        /// resume reminder detail.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="ResumeReminderDetail"/> that holds the interview reminder
        /// detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed message. Returns 
        /// empty string when the reminder detail object is null.
        /// </returns>
        private string GetMessage(ResumeReminderDetail reminderDetail)
        {
            if (reminderDetail == null)
                return string.Empty;

            string message = string.Format("Dear {0},", reminderDetail.CandidateName);
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "Sub: Reminder for uploading resume";
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("You have set a reminder to upload your resume on {0}", reminderDetail.ReminderDate.ToString("MM/dd/yyyy"));
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "Your login credentials are given below:";
            message += Environment.NewLine;
            message += string.Format("User Name: {0}", reminderDetail.UserName);
            message += Environment.NewLine;
            message += string.Format("Password: {0}", new EncryptAndDecrypt().DecryptString(reminderDetail.Password));
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "Click <a href='" + ConfigurationManager.AppSettings["CANDIDATE_URL"] + "' alt='' > here </a> to login with your user name & password and upload the resume";
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += string.Format("This is a system generated message");
            message += Environment.NewLine;
            message += string.Format("No need to reply to this message");
            message += Environment.NewLine;
            message += Environment.NewLine;
            message += "============================== Disclaimer ==============================";
            message += Environment.NewLine;
            message += "This message is for the designated recipient only and may contain privileged, proprietary, or ";
            message += Environment.NewLine;
            message += "otherwise private information. If you have received it in error, please notify the sender";
            message += Environment.NewLine;
            message += "immediately and delete the original. Any other use of the email by you is prohibited.";
            message += Environment.NewLine;
            message += "============================ End Of Disclaimer ============================";

            return message;
        }

        #endregion Private Methods
    }
}
