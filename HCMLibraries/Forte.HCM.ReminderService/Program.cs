﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Program.cs
// File that represents the Program class that starts the test reminder Windows
// service.

#endregion

#region Directives

using System.ServiceProcess;

#endregion Directives

namespace Forte.HCM.ReminderService
{
    /// <summary>
    /// Class that represents the executable program that start the test
    /// reminder Windows service.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] servicesToRun;
            servicesToRun = new ServiceBase[] 
			{ 
				new TestReminderService() 
			};
            ServiceBase.Run(servicesToRun);
        }
    }
}
