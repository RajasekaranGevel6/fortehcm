﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Reminder.cs
// File that represents the user interface layout and functionalities
// for the Reminder form. This form helps in sending incomplete and 
// duplicate resume reminders.

#endregion Header

#region Directives

using System;
using System.Windows.Forms;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.ResumeReminder
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the Reminder form. This form helps in sending incomplete and 
    /// duplicate resume reminders.
    /// </summary>
    /// <remarks>
    /// This form will not be shown to the users. This executable is assigned
    /// to a scheduler and invokes at specicied time.
    /// </remarks>
    public partial class Reminder : Form
    {
        /// <summary>
        /// Method that represents the constructor.
        /// </summary>
        public Reminder()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that is called when the form is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void Reminder_Load(object sender, EventArgs e)
        {
            try
            {
                // Hide the window.
                this.Visible = false;

                Logger.TraceLog("Started resume reminder @" + DateTime.Now);

                // Retrieve the reminder type (daily or weekly).
                string []args = Environment.GetCommandLineArgs();

                // Assign the default reminder type.
                string reminderType = "D";

                if (args == null || args.Length != 2)
                {
                    Logger.TraceLog("Command line argument missing to identify the reminder type (daily or weekly). The default value daily is taken");
                }
                else
                {
                    reminderType = args[1].Trim().ToUpper();
                }

                // Create an email handler object.
                EmailHandler emailHandler = new EmailHandler();

                #region Send Incomplete Resume Reminders                       

                // Retrieve the list of incomplete resume reminders.
                List<ResumeReminderDetail> incompleteReminders = new ResumeRepositoryBLManager().
                    GetIncompleteResumeReminders(reminderType);

                if (incompleteReminders == null || incompleteReminders.Count == 0)
                {
                    if (reminderType == "D")
                        Logger.TraceLog("No incomplete resumes found for 'daily' reminder type");
                    else
                        Logger.TraceLog("No incomplete resumes found for 'weekly' reminder type");
                }
                else
                {
                    if (reminderType == "D")
                        Logger.TraceLog(string.Format("{0} incomplete resumes found for 'daily' reminder type", incompleteReminders.Count));
                    else
                        Logger.TraceLog(string.Format("{0} incomplete resumes found for 'weekly' reminder type", incompleteReminders.Count));

                    // Loop through the list and send the reminders.
                    foreach (ResumeReminderDetail reminder in incompleteReminders)
                    {
                        try
                        {
                            emailHandler.SendMail(EntityType.IncompleteResumeReminderList, reminder);
                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                        }
                    }
                }

                #endregion Send Incomplete Resume Reminders

                #region Send Duplicate Resume Reminders                        

                // Retrieve the list of duplicate resume reminders.
                List<ResumeReminderDetail> duplicateReminders = new ResumeRepositoryBLManager().
                    GetDuplicateResumeReminders(reminderType);

                if (duplicateReminders == null || duplicateReminders.Count == 0)
                {
                    if (reminderType == "D")
                        Logger.TraceLog("No duplicate resumes found for 'daily' reminder type");
                    else
                        Logger.TraceLog("No duplicate resumes found for 'weekly' reminder type");
                }
                else
                {
                    if (reminderType == "D")
                        Logger.TraceLog(string.Format("{0} duplicate resumes found for 'daily' reminder type", duplicateReminders.Count));
                    else
                        Logger.TraceLog(string.Format("{0} duplicate resumes found for 'weekly' reminder type", duplicateReminders.Count));

                    // Loop through the list and send the reminders.
                    foreach (ResumeReminderDetail reminder in duplicateReminders)
                    {
                        try
                        {
                            emailHandler.SendMail(EntityType.DuplicateResumeReminderList, reminder);
                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                        }
                    }
                }

                #endregion Send Duplicate Resume Reminders

                #region Send Duplicate Resume Reminders For Recruiters         

                // Retrieve the list of duplicate resume reminders.
                List<ResumeReminderDetail> duplicateRecruiterReminders = new ResumeRepositoryBLManager().
                    GetRecruiterDuplicateResumeReminders(reminderType);

                if (duplicateReminders == null || duplicateReminders.Count == 0)
                {
                    if (reminderType == "D")
                        Logger.TraceLog("No duplicate resumes found for 'daily' reminder type for recruiters");
                    else
                        Logger.TraceLog("No duplicate resumes found for 'weekly' reminder type for recruiters");
                }
                else
                {
                    if (reminderType == "D")
                        Logger.TraceLog(string.Format("{0} duplicate resumes found for 'daily' reminder type for recruiters", duplicateReminders.Count));
                    else
                        Logger.TraceLog(string.Format("{0} duplicate resumes found for 'weekly' reminder type for recruiters", duplicateReminders.Count));

                    // Loop through the list and send the reminders.
                    foreach (ResumeReminderDetail reminder in duplicateReminders)
                    {
                        try
                        {
                            emailHandler.SendMail(EntityType.DuplicateResumeReminderList, reminder);
                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                        }
                    }
                }

                #endregion Send Duplicate Resume Reminders For Recruiters

                Logger.TraceLog("Stopped resume reminder @" + DateTime.Now);

                // Close the application.
                this.Close();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                Logger.TraceLog("Stopped resume reminder @" + DateTime.Now);

                // Close the application.
                this.Close();
            }
        }
    }
}
