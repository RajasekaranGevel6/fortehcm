﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// DatabaseConnectionManager.cs
// File that represents the DatabaseConnectionManager class that manages the 
// database connection. This class helps to create database connection.

#endregion Header  

#region Directives                                                             

using System;

using Forte.HCM.Support;
using Forte.HCM.Exceptions;

using Microsoft.Practices.EnterpriseLibrary.Data;

#endregion Directives

namespace Forte.HCM.Common.DL
{
    /// <summary>
    /// Class that represents the database connection manager object that 
    /// manages the database connection. This class helps to create database 
    /// connection.
    /// </summary>
    public class DatabaseConnectionManager
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="Database"/> that holds the database object.
        /// </summary>
        private static Database hcmDatabase;

        #endregion Private Variables

        #region Public Properties                                              

        /// <summary>
        /// Property that sets or gets the <see cref="Database"/> object.
        /// </summary>
        /// <value>
        /// A <see cref="Database"/> that holds the database object.
        /// </value>
        public static Database HCMDatabase
        {
            get
            {
                if (hcmDatabase == null)
                {
                    CreateConnection();
                }
                return hcmDatabase;
            }
        }

        #endregion Public Properties

        #region Private Methods                                                

        /// <summary>
        /// Method that creates the connection to the database.
        /// </summary>
        private static void CreateConnection()
        {
            try
            {
                hcmDatabase = ApplicationSupportManager.
                    GetDatabase();
            }
            catch (Exception exception)
            {
                throw new HCMException("Unable to obtain a connection", exception);
            }
        }

        #endregion Private Methods
    }
}
