﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TransactionManager.cs
// File that represents the TransactionManager class that manages the database 
// transaction. This class helps to create, commit and rollback transactions.
// This class inherits Forte.HCM.Common.DL.DatabaseConnectionManager class.

#endregion Header  

#region Directives                                                             

using System.Data;
using System.Data.Common;

using Forte.HCM.Exceptions;

#endregion Directives

namespace Forte.HCM.Common.DL
{
    /// <summary>
    /// Class that represents the transaction manager object that manages the
    /// database transaction. This class helps to create, commit and rollback 
    /// transactions.
    /// </summary>
    public class TransactionManager : DatabaseConnectionManager
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </summary>
        private IDbTransaction sqlTransaction;

        /// <summary>
        /// A <see cref="DbConnection"/> that holds the database connection 
        /// object.
        /// </summary>
        private DbConnection dbConnection;

        #endregion Private Variables

        #region Constructor                                                    

        /// <summary>
        /// Constructor that constructs the <see cref="TransactionManager"/> object.
        /// </summary>
        public TransactionManager()
        {
            try
            {
                dbConnection = HCMDatabase.CreateConnection();

                try
                {
                    dbConnection.Open();
                }
                catch 
                {
                    throw new HCMException("Unable to open connection.");
                }

                sqlTransaction = dbConnection.BeginTransaction();
            }
            catch
            {
                try
                {
                    if (dbConnection.State != ConnectionState.Closed)
                        dbConnection.Close();
                }
                catch
                { 
                    throw; 
                }
                throw; 
            }
        }

        #endregion Constructor

        #region Public Properties                                              

        /// <summary>
        /// Property that sets or gets the <see cref="IDbTransaction"/> object.
        /// </summary>
        /// <value>
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </value>
        public IDbTransaction Transaction
        {
            get
            {
                return sqlTransaction;
            }
        }

        #endregion Public Properties

        #region Public Methods                                                 

        /// <summary>
        /// Method that commits the transaction.
        /// </summary>
        public void Commit()
        {
            try
            {
                sqlTransaction.Commit();
            }
            finally
            {
                if (sqlTransaction.Connection != null)
                {
                    if (sqlTransaction.Connection.State == ConnectionState.Open)
                        sqlTransaction.Connection.Close();

                    sqlTransaction.Connection.Dispose();
                }
                sqlTransaction.Dispose();
            }
        }

        /// <summary>
        /// Method that rollbacks the transaction.
        /// </summary>
        public void Rollback()
        {
            try
            {
                sqlTransaction.Rollback();
            }
            finally
            {
                if (sqlTransaction.Connection != null)
                {
                    if (sqlTransaction.Connection.State == ConnectionState.Open)
                        sqlTransaction.Connection.Close();

                    sqlTransaction.Connection.Dispose();
                }
                sqlTransaction.Dispose();
            }
        }

        #endregion Public Methods
    }
}
