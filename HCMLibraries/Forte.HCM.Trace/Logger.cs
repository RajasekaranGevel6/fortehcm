﻿#region Header
// Copyright (C) 2004-2008 ADM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Logger.cs
// This file has the class Logger that provides features to 
// log the trace and exception messages.
//
#endregion

#region Directives

using System;
using System.Text;
using System.Diagnostics;

using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

#endregion Directives

namespace Forte.HCM.Trace
{
    /// <summary>
    /// Represents the class that provides features to 
    /// log trace and exception messages.
    /// </summary>
    [ConfigurationElementType(typeof(CustomTraceListenerData))]
    public class Logger : CustomTraceListener
    {
        /// <summary>
        /// Empty constructor
        /// </summary>
        public Logger()
        {
        }

        /// <summary>
        /// Represents the method that logs the message
        /// in the event log.
        /// </summary>
        /// <param name="logMessage">holds log message</param>
        /// <param name="appTitle">The application title.</param>
        public static void EventLog(string logMessage, string appTitle)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("EventLog");
                logEntry.Title = appTitle;
                logEntry.Message = logMessage;
                Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(logEntry);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the message
        /// in the event log.
        /// </summary>
        /// <param name="logMessage">holds log message.</param>
        public static void EventLog(string logMessage)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("EventLog");
                logEntry.Message = logMessage;
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the message
        /// in the event log.
        /// </summary>
        /// <param name="exceptionMsg">The exception message.</param>
        /// <param name="appTitle">The applicaiton title.</param>
        public static void EventLog(Exception exceptionMsg, string appTitle)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("EventLog");
                logEntry.Title = appTitle;
                StringBuilder logMessage = new StringBuilder(Environment.NewLine);
                logMessage.Append("InnerException");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMsg.InnerException);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMsg.Message);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Source");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(exceptionMsg.Source);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("StackTrace");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMsg.StackTrace);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Method Name");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMsg.TargetSite);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the message
        /// in the event log.
        /// </summary>
        /// <param name="exceptionMsg">holds exception message.</param>
        public static void EventLog(Exception exceptionMsg)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("EventLog");
                StringBuilder logMessage =
                    new StringBuilder(Environment.NewLine);
                logMessage.Append("InnerException");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMsg.InnerException);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMsg.Message);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Source");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(exceptionMsg.Source);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("StackTrace");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMsg.StackTrace);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Method Name");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMsg.TargetSite);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the message
        /// as trace.
        /// </summary>
        /// <param name="logMessage">The message.</param>
        /// <param name="appTitle">The application title.</param>
        public static void TraceLog(string logMessage, string appTitle)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Trace");
                logEntry.Title = appTitle;
                logEntry.Message = logMessage;
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the message
        /// as trace.
        /// </summary>
        /// <param name="logMessage">holds log message.</param>
        public static void TraceLog(string logMessage)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Trace");
                logEntry.Message = logMessage;
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the message
        /// as trace.
        /// </summary>
        /// <param name="exceptionMessage">The exp MSG.</param>
        /// <param name="appTitle">The app title.</param>
        public static void TraceLog
            (Exception exceptionMessage, string appTitle)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Trace");
                logEntry.Title = appTitle;
                StringBuilder logMessage =
                    new StringBuilder(Environment.NewLine);
                logMessage.Append("InnerException");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.InnerException);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.Message);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Source");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(exceptionMessage.Source);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("StackTrace");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.StackTrace);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Method Name");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.TargetSite);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the message
        /// as trace.
        /// </summary>
        /// <param name="exceptionMessage">The exp MSG.</param>
        public static void TraceLog(Exception exceptionMessage)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Trace");
                StringBuilder logMessage =
                    new StringBuilder(Environment.NewLine);
                logMessage.Append("InnerException");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.InnerException);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.Message);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Source");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(exceptionMessage.Source);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("StackTrace");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.StackTrace);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Method Name");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.TargetSite);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the message
        /// as trace.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <param name="exceptionMessage">The exp MSG.</param>
        public static void TraceLog(string UserID, Exception exceptionMessage)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Trace");
                StringBuilder logMessage =
                    new StringBuilder(Environment.NewLine);
                logMessage.Append("User ID");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(UserID);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("InnerException");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.InnerException);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.Message);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Source");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(exceptionMessage.Source);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("StackTrace");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.StackTrace);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Method Name");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.TargetSite);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the exceptions.        
        /// </summary>
        /// <param name="exceptionMessage">The exp MSG.</param>
        /// <param name="appTitle">The app title.</param>
        public static void ExceptionLog
            (Exception exceptionMessage, string appTitle)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Exception");
                logEntry.Title = appTitle;
                StringBuilder logMessage =
                    new StringBuilder(Environment.NewLine);
                logMessage.Append("InnerException");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.InnerException);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.Message);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Source");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(exceptionMessage.Source);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("StackTrace");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.StackTrace);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Method Name");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.TargetSite);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the exceptions.        
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <param name="exceptionMessage">The exp MSG.</param>
        public static void ExceptionLog
            (string UserID, Exception exceptionMessage)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Exception");
                StringBuilder logMessage =
                    new StringBuilder(Environment.NewLine);
                logMessage.Append("User ID");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(UserID);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("InnerException");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.InnerException);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.Message);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Source");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(exceptionMessage.Source);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("StackTrace");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.StackTrace);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Method Name");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.TargetSite);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);

                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the exceptions.        
        /// </summary>
        /// <param name="exceptionMessage">holds exception message.</param>
        public static void ExceptionLog(Exception exceptionMessage)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Exception");
                StringBuilder logMessage =
                    new StringBuilder(Environment.NewLine);
                logMessage.Append("InnerException");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.InnerException);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.Message);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Source");
                logMessage.Append("\t\t\t\t\t:");
                logMessage.Append(exceptionMessage.Source);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("StackTrace");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.StackTrace);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logMessage.Append("Method Name");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage.TargetSite);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Represents the method that logs the exceptions.        
        /// </summary>
        /// <param name="exceptionMessage">holds exception message.</param>
        public static void ExceptionLog(String exceptionMessage)
        {
            try
            {
                LogEntry logEntry = new LogEntry();
                logEntry.Categories.Add("Exception");
                StringBuilder logMessage =
                    new StringBuilder(Environment.NewLine);
                logMessage.Append("App Message");
                logMessage.Append("\t\t\t\t:");
                logMessage.Append(exceptionMessage);
                logMessage.Append(Environment.NewLine);
                logMessage.Append(Environment.NewLine);
                logEntry.Message = logMessage.ToString();
                Microsoft.Practices.EnterpriseLibrary.Logging
                    .Logger.Write(logEntry);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Writes a message to the debug window 
        /// </summary>
        /// <param name="message">
        /// The string to write to the debug window</param>
        public override void Write(string message)
        {
            Debug.Write(message);
        }

        /// <summary>
        /// Writes a message to the debug window 
        /// </summary>
        /// <param name="message">
        /// The string to write to the debug window</param>
        public override void WriteLine(string message)
        {
            Debug.WriteLine(message);
        }
    }
}
