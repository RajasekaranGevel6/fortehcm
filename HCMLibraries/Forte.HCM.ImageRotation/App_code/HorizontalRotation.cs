﻿namespace RotationManager
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    public class HorizontalRotation
    {
        protected double center_x;
        protected double center_y;
        protected double initial;
        protected double objectif;
        protected double perspective;
        protected double radius_x;
        protected double radius_y;
        protected double speed;
        protected Canvas working;
        protected bool workWithOpacity;

        public HorizontalRotation(Canvas wk)
        {
            this.initial = 1.5707963267948966;
            this.objectif = 1.5707963267948966;
            this.speed = 0.025;
            this.perspective = 2.0;
            this.radius_x = 250.0;
            this.radius_y = 150.0;
            this.working = wk;
            for (int i = 0; i < this.working.get_Children().get_Count(); i++)
            {
                this.working.get_Children().get_Item(i).add_MouseLeftButtonDown(new MouseButtonEventHandler(this, (IntPtr) this.Image_MouseLeftButtonDown));
            }
            this.UpdateRotor();
            CompositionTarget.add_Rendering(new EventHandler(this.CompositionTarget_Rendering));
        }

        public HorizontalRotation(Canvas wk, double cx, double cy)
        {
            this.initial = 1.5707963267948966;
            this.objectif = 1.5707963267948966;
            this.speed = 0.025;
            this.perspective = 2.0;
            this.radius_x = 250.0;
            this.radius_y = 150.0;
            this.center_x = cx;
            this.center_y = cy;
            this.working = wk;
            for (int i = 0; i < this.working.get_Children().get_Count(); i++)
            {
                this.working.get_Children().get_Item(i).add_MouseLeftButtonDown(new MouseButtonEventHandler(this, (IntPtr) this.Image_MouseLeftButtonDown));
            }
            this.UpdateRotor();
            CompositionTarget.add_Rendering(new EventHandler(this.CompositionTarget_Rendering));
        }

        private void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            if (Math.Abs((double) (Math.Abs(this.initial) - Math.Abs(this.objectif))) > 0.0001)
            {
                if (this.objectif > this.initial)
                {
                    this.initial += this.speed;
                    if (this.initial > this.objectif)
                    {
                        this.initial = this.objectif;
                    }
                }
                else
                {
                    this.initial -= this.speed;
                    if (this.initial < this.objectif)
                    {
                        this.initial = this.objectif;
                    }
                }
                this.UpdateRotor();
            }
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.initial = this.initial % 6.2831853071795862;
            if (this.initial < 0.0)
            {
                this.initial += 6.2831853071795862;
            }
            this.objectif = 1.5707963267948966 - (((this.working.get_Children().IndexOf((UIElement) sender) * 3.1415926535897931) * 2.0) / ((double) this.working.get_Children().get_Count()));
            if (this.objectif < 0.0)
            {
                this.objectif += 6.2831853071795862;
            }
            if (Math.Abs((double) (this.objectif - this.initial)) > 3.1415926535897931)
            {
                if (this.initial > this.objectif)
                {
                    this.initial -= 6.2831853071795862;
                }
                else
                {
                    this.initial += 6.2831853071795862;
                }
            }
        }

        public void Left()
        {
            this.objectif += 6.2831853071795862 / ((double) this.working.get_Children().get_Count());
        }

        public void Right()
        {
            this.objectif -= 6.2831853071795862 / ((double) this.working.get_Children().get_Count());
        }

        public void UpdateRotor()
        {
            double num = 6.2831853071795862 / ((double) this.working.get_Children().get_Count());
            for (int i = 0; i < this.working.get_Children().get_Count(); i++)
            {
                double num3;
                if (this.workWithOpacity)
                {
                    this.working.get_Children().get_Item(i).set_Opacity(1.0 + (((Math.Sin((num * i) + this.initial) * this.radius_y) - this.radius_y) / (this.perspective * this.radius_y)));
                }
                ScaleTransform transform = new ScaleTransform();
                transform.set_ScaleY(num3 = 1.0 + (((Math.Sin((num * i) + this.initial) * this.radius_y) - this.radius_y) / (this.perspective * this.radius_y)));
                transform.set_ScaleX(num3);
                this.working.get_Children().get_Item(i).set_RenderTransform(transform);
                Canvas.SetLeft(this.working.get_Children().get_Item(i), ((Math.Cos((num * i) + this.initial) * this.radius_x) + this.center_x) - ((((FrameworkElement) this.working.get_Children().get_Item(i)).get_Width() * transform.get_ScaleX()) / 2.0));
                Canvas.SetTop(this.working.get_Children().get_Item(i), ((Math.Sin((num * i) + this.initial) * this.radius_y) + this.center_y) - ((((FrameworkElement) this.working.get_Children().get_Item(i)).get_Height() * transform.get_ScaleX()) / 2.0));
                Canvas.SetZIndex(this.working.get_Children().get_Item(i), (int) ((Math.Sin((num * i) + this.initial) * this.radius_y) + (2.0 * this.radius_y)));
            }
        }

        public double CenterX
        {
            get
            {
                return this.center_x;
            }
            set
            {
                this.center_x = value;
                this.UpdateRotor();
            }
        }

        public double CenterY
        {
            get
            {
                return this.center_y;
            }
            set
            {
                this.center_y = value;
                this.UpdateRotor();
            }
        }

        public double Perspective
        {
            get
            {
                return this.perspective;
            }
            set
            {
                if (this.perspective != 0.0)
                {
                    this.perspective = value;
                }
                else
                {
                    this.perspective = 1.0;
                }
                this.UpdateRotor();
            }
        }

        public double RadiusX
        {
            get
            {
                return this.radius_x;
            }
            set
            {
                if (this.radius_x != 0.0)
                {
                    this.radius_x = value;
                }
                else
                {
                    this.radius_x = double.MinValue;
                }
                this.UpdateRotor();
            }
        }

        public double RadiusY
        {
            get
            {
                return this.radius_y;
            }
            set
            {
                if (this.radius_y != 0.0)
                {
                    this.radius_y = value;
                }
                else
                {
                    this.radius_y = double.MinValue;
                }
                this.UpdateRotor();
            }
        }

        public double Speed
        {
            get
            {
                return this.speed;
            }
            set
            {
                this.speed = value;
            }
        }

        public bool WorkWithOpacity
        {
            get
            {
                return this.workWithOpacity;
            }
            set
            {
                this.workWithOpacity = value;
                if (this.workWithOpacity)
                {
                    this.UpdateRotor();
                }
                else
                {
                    for (int i = 0; i < this.working.get_Children().get_Count(); i++)
                    {
                        this.working.get_Children().get_Item(i).set_Opacity(1.0);
                    }
                }
            }
        }
    }
}

