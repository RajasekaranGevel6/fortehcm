﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;

namespace Forte.HCM.ImageRotation
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            this.Loaded += new System.Windows.RoutedEventHandler(MainPage_Loaded);
        }


        RotationManager.HorizontalRotation rotor;
        private void MainPage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            // create rotor
            rotor = new RotationManager.HorizontalRotation(RotorCanvas, 320.0, 320.0);
            rotor.WorkWithOpacity = true;
            rotor.Speed = SlideSpeed.Value / 10000;
            rotor.RadiusY = 20;
            rotor.RadiusX = 150;
            rotor.Perspective = 7;

            SlidePers.ValueChanged += new RoutedPropertyChangedEventHandler<double>(SlidePers_ValueChanged);
            SlideX.ValueChanged += new RoutedPropertyChangedEventHandler<double>(SlideX_ValueChanged);
            SlideY.ValueChanged += new RoutedPropertyChangedEventHandler<double>(SlideY_ValueChanged);
            SlideSpeed.ValueChanged += new RoutedPropertyChangedEventHandler<double>(SlideSpeed_ValueChanged);

            MouseWheel += new MouseWheelEventHandler(MainPage_MouseWheel);

            if(((System.Windows.FrameworkElement)(((System.Windows.RoutedEventArgs)(e)).OriginalSource))!=null)
            ((System.Windows.FrameworkElement)(((System.Windows.RoutedEventArgs)(e)).OriginalSource)).MouseLeftButtonDown 
                += new MouseButtonEventHandler(MainPage_MouseLeftButtonDown);

        }

        void MainPage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://silverlight.net/Default?param=pippo")); 
        }

      

        void SlideSpeed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            rotor.Speed = SlideSpeed.Value / 10000;


        }

        void SlideY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            rotor.RadiusY = SlideY.Value;
        }

        void SlideX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            rotor.RadiusX = SlideX.Value;
        }

        void SlidePers_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            rotor.Perspective = SlidePers.Value;
        }
        
        void MainPage_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //if (e.Delta > 0) rotor.Right();
            //else rotor.Left();
        }

      
      

        private void prev_img_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            rotor.Left();
            TalentScout_Tool_Button.Visibility = Visibility.Collapsed;
            OTM_Button.Visibility = Visibility.Collapsed;
            cand_repository_tool_Button.Visibility = Visibility.Collapsed;
            PositionProfile_Tool.Visibility = Visibility.Collapsed;

        }

        private void next_img_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            rotor.Right();
            TalentScout_Tool_Button.Visibility = Visibility.Collapsed;
            OTM_Button.Visibility = Visibility.Collapsed;
            cand_repository_tool_Button.Visibility = Visibility.Collapsed;
            PositionProfile_Tool.Visibility = Visibility.Collapsed;
           
        }

       
        private void TalentScout_Tool_Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (TalentScout_Tool_Button.Visibility == Visibility.Collapsed)
            {
                TalentScout_Tool_Button.Visibility = Visibility.Visible;
                OTM_Button.Visibility = Visibility.Collapsed;
                cand_repository_tool_Button.Visibility = Visibility.Collapsed;
                PositionProfile_Tool.Visibility = Visibility.Collapsed;
                return;
            }
            else
            {
                TalentScout_Tool_Button.Visibility = Visibility.Collapsed;

                Uri address = new Uri(Application.Current.Host.Source,
                     "../overview/TalentScoutOverview.aspx" );
                HtmlPage.Window.Navigate(new Uri(address.AbsoluteUri, UriKind.Absolute), "_self");
            }
        }

        private void OTM_Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (OTM_Button.Visibility == Visibility.Collapsed)
            {
                OTM_Button.Visibility = Visibility.Visible;
                cand_repository_tool_Button.Visibility = Visibility.Collapsed;
                PositionProfile_Tool.Visibility = Visibility.Collapsed;
                TalentScout_Tool_Button.Visibility = Visibility.Collapsed;
                return;
            }
            else
            {
                OTM_Button.Visibility = Visibility.Collapsed;
                Uri address = new Uri(Application.Current.Host.Source,
                   "../overview/OTMOverview.aspx");
                HtmlPage.Window.Navigate(new Uri(address.AbsoluteUri, UriKind.Absolute), "_self");
            }
        }

        private void cand_repository_tool_Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (cand_repository_tool_Button.Visibility == Visibility.Collapsed)
            {
                cand_repository_tool_Button.Visibility = Visibility.Visible;
                OTM_Button.Visibility = Visibility.Collapsed;
                TalentScout_Tool_Button.Visibility = Visibility.Collapsed;
                PositionProfile_Tool.Visibility = Visibility.Collapsed;
                return;
            }
            else
            {
                cand_repository_tool_Button.Visibility = Visibility.Collapsed;

                Uri address = new Uri(Application.Current.Host.Source,
                 "../overview/ResumeRepositoryOverview.aspx");
                HtmlPage.Window.Navigate(new Uri(address.AbsoluteUri, UriKind.Absolute), "_self");
            }

        }

        private void PositionProfile_Tool_Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (PositionProfile_Tool.Visibility == Visibility.Collapsed)
            {
                PositionProfile_Tool.Visibility = Visibility.Visible;
                OTM_Button.Visibility = Visibility.Collapsed;
                cand_repository_tool_Button.Visibility = Visibility.Collapsed;
                TalentScout_Tool_Button.Visibility = Visibility.Collapsed;
                return;
            }
            else
            {
                PositionProfile_Tool.Visibility = Visibility.Collapsed;
                  Uri address = new Uri(Application.Current.Host.Source,
                 "../overview/PositionProfileOverview.aspx");
                HtmlPage.Window.Navigate(new Uri(address.AbsoluteUri, UriKind.Absolute), "_self");
            }

        }

        

    }
}
