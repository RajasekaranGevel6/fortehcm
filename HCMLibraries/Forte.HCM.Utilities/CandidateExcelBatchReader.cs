﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExcelBatchReader.cs
// File that represents the excel reader object that will helps to read
// data from the excel file. The utility also helps to retrieve the list
// of sheets present in the excel file, the count of rows, etc.

#endregion Header

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System.IO;
using System.Data.OleDb;
using System.Globalization;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Represents the class that helps to read data from the excel file.
    /// The utility also helps to retrieve the list of sheets present in 
    /// the excel file, the count of rows, etc
    /// </summary>
    public class CandidateExcelBatchReader : ExcelBatchReader
    {
        #region Public Methods

        /// <summary>
        /// Represents the constructor.
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the excel file name.
        /// </param>
        public CandidateExcelBatchReader(string fileName)
            : base(fileName)
        {

        }


        /// <summary>
        /// Represents the method that returns the list of question for the 
        /// given page number and page size.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInformation"/> that holds the questions.
        /// </returns>
        public List<CandidateInformation> GetCandidates(int pageNumber, int pageSize,
            string userName, string sheetName, int userID, string fileName)
        {
            if (!CheckIsValidExcel(sheetName))
                throw new Exception("Please enter a valid excel. Please use the ForteHCM supplied excel template");

            //List of questions to store the questions
            List<CandidateInformation> candidates = new List<CandidateInformation>();

            string strCommand = string.Empty;

            int lastRecord = pageNumber * pageSize + 1;

            int firstRecord = lastRecord - pageSize + 1;

            int candidateID = 0;

            if (pageNumber == 1)
            {
                firstRecord = 1;

                lastRecord = pageSize + 1;

                candidateID = 1;
            }
            else
            {
                lastRecord = pageNumber * pageSize + 1;

                firstRecord = lastRecord - pageSize;

                candidateID = firstRecord;
            }

            string choice = string.Empty;
            string candidateImageName = string.Empty;
            IDataReader candidateReader;

            DbCommand selectCandidateCommand;

            try
            {

                //Command to select the question from sheet
                //Questions will be selected based on first record and last record
                strCommand = "select * from [" + sheetName + "A" + firstRecord + ":M" + lastRecord + "]";

                database.CreateConnection();

                //Gets the command 
                selectCandidateCommand = database.GetSqlStringCommand(strCommand);

                //Executes the reader
                candidateReader = database.ExecuteReader(selectCandidateCommand);

                CandidateInformation candidateDetail;


                string xlsFilePath = Path.GetDirectoryName(fileName);
                string imagesFolder = Path.Combine(xlsFilePath, Path.GetFileNameWithoutExtension(fileName));
                //Defines a candidate id for candidate     
                int row = 1;
                while (candidateReader.Read())
                {

                    //This condition is to check whether all the columns are empty
                    //If found empty should not read that line
                    //if (!(Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.FIRSTNAME]) &&
                    //        Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.LASTNAME]) &&
                    //        Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.EMAIL]) &&
                    //        Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.GENDER]) &&
                    //        Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.DOB]) &&
                    //        Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.USERNAME]) &&
                    //        Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.PASSWORD]) &&                           
                    //        Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.MOBILE_NUMBER])))
                    //{

                    //Creats a new candidate
                    candidateDetail = new CandidateInformation();

                    candidateDetail.caiType = "CT_MANUAL";
                    //Assign the author id for the question 
                    candidateDetail.caiCreatedBy = userID;

                    //Assign the active status
                    candidateDetail.caiLimitedAccess = false;
                    candidateDetail.caiIsActive = 'Y';
                    candidateDetail.caiEmployee = 'Y';
                     
                    string invalidRemarks = "";
                    bool isValidCandidate = true;

                    if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.FIRSTNAME]))
                    {
                        //Assign the first name for the candidate 
                        candidateDetail.caiFirstName = candidateReader[CandidateExcelConstants.FIRSTNAME].ToString();
                    }
                    else
                    {
                        isValidCandidate = false;
                        invalidRemarks = "Row "+row+": First Name is empty";
                    }

                    if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.LASTNAME]))
                    {
                        //Assign the last name for the candidate 
                        candidateDetail.caiLastName = candidateReader[CandidateExcelConstants.LASTNAME].ToString();
                    }
                    else
                    {
                        isValidCandidate = false;
                        invalidRemarks = invalidRemarks != "" ? invalidRemarks + " ,Last Name is empty" : "Row " + row + ": Last Name is empty";
                    }

                    if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.EMAIL]))
                    {
                        //Assign the Email for the candidate
                        candidateDetail.caiEmail = candidateReader[CandidateExcelConstants.EMAIL].ToString();
                    }
                    else
                    {
                        isValidCandidate = false;
                        invalidRemarks = invalidRemarks != "" ? invalidRemarks + " ,Email is empty" : "Row " + row + ": Email is empty";
                    }

                    if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.GENDER]))
                    {
                        //Assign the gender for the candidate
                        candidateDetail.caiGender = candidateReader[CandidateExcelConstants.GENDER] == null ? 0 : candidateReader[CandidateExcelConstants.GENDER].ToString() == "Male" ? 1 : 2;
                    }

                    if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.DOB]))
                    {
                        //Asssign  the DOB for the candidate
                        candidateDetail.caiDOB = DateTime.ParseExact(candidateReader[CandidateExcelConstants.DOB].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }


                    if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.USERNAME]))
                    {
                        //Asssign  the user name for the candidate
                        candidateDetail.UserName = candidateReader[CandidateExcelConstants.USERNAME].ToString().Trim();
                    }
                    else
                    {
                        isValidCandidate = false;
                        invalidRemarks = invalidRemarks != "" ? invalidRemarks + " ,User Name is empty" : "Row " + row + ": User Name is empty";
                    }

                    if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.PASSWORD]))
                    {
                        //Asssign  the password for the candidate
                        candidateDetail.Password = new EncryptAndDecrypt().EncryptString(candidateReader[CandidateExcelConstants.PASSWORD].ToString().Trim());
                    }
                    else
                    {
                        isValidCandidate = false;
                        invalidRemarks = invalidRemarks != "" ? invalidRemarks + " ,Password is empty" : "Row " + row + ": Password is empty";
                    }


                    //if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.USERTYPE]))
                    //{
                    //Asssign  the user type for the candidate
                    // candidateDetail.caiEmployee = 'Y'; //candidateReader[CandidateExcelConstants.USERTYPE].ToString() == "Candidate" ? 'N' : 'Y';
                    //}


                    if (!Utility.IsNullOrEmpty(candidateReader[CandidateExcelConstants.MOBILE_NUMBER]))
                    {
                        //Asssign  the mobile number for the candidate 
                        candidateDetail.caiCell = candidateReader[CandidateExcelConstants.MOBILE_NUMBER].ToString();
                    }

                    if (!isValidCandidate)
                    {
                        candidateDetail.caiIsValid = false;
                        candidateDetail.caiInvalidCandidatesRemarks = invalidRemarks;
                    }
                    
                    //Add candidate to the candidate list
                    candidates.Add(candidateDetail);

                    //Increment the candidate Id for next candidate 
                    row = row + 1;
                    candidateID = candidateID + 1;
                    //}

                }
                candidateReader.Close();

                return candidates;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }
        }

        private bool CheckIsValidExcel(string sheetName)
        {
            //List of candidate to store the candidates
            List<CandidateInformation> candidates = new List<CandidateInformation>();

            string strCommand = string.Empty;


            IDataReader candidateReader;

            DbCommand selectCandidateCommand;

            //Command to select the candidate from sheet
            //candidates will be selected based on first record and last record
            strCommand = "select * from [" + sheetName + "]";

            database.CreateConnection();

            //Gets the command 
            selectCandidateCommand = database.GetSqlStringCommand(strCommand);

            //Executes the reader
            candidateReader = database.ExecuteReader(selectCandidateCommand);

            int count = candidateReader.FieldCount;

            if (!((candidateReader.GetName(CandidateExcelConstants.FIRSTNAME).Trim() == "First Name") &&
                (candidateReader.GetName(CandidateExcelConstants.LASTNAME).Trim() == "Last Name") &&
                (candidateReader.GetName(CandidateExcelConstants.EMAIL).Trim() == "Email") &&
                (candidateReader.GetName(CandidateExcelConstants.GENDER).Trim() == "Gender") &&
                (candidateReader.GetName(CandidateExcelConstants.DOB).Trim() == "DOB") &&
                (candidateReader.GetName(CandidateExcelConstants.USERNAME).Trim() == "User Name") &&
                (candidateReader.GetName(CandidateExcelConstants.PASSWORD).Trim() == "Password") &&
                (candidateReader.GetName(CandidateExcelConstants.MOBILE_NUMBER).Trim() == "Mobile Number")))
            {
                return false;
            }

            return true;
        }


        #endregion Public Methods
    }


    internal class CandidateExcelConstants
    {
        /// <summary>
        /// A <see cref="int"/> that holds the
        /// column number of firstname
        /// </summary>
        public const int FIRSTNAME = 0;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of lastname
        /// </summary>
        public const int LASTNAME = 1;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of email 
        /// </summary>
        public const int EMAIL = 2;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of gender
        /// </summary>
        public const int GENDER = 3;

        /// <summary>
        /// A<see cref="int"/>that holds the
        /// column number of dob
        /// </summary>
        public const int DOB = 4;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of username
        /// </summary>
        public const int USERNAME = 5;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of password
        /// </summary>
        public const int PASSWORD = 6;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of user type
        /// </summary>
        public const int USERTYPE = 8;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of mobile number
        /// </summary>
        public const int MOBILE_NUMBER = 7;

    }




}