﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileReviewDataManager.aspx.cs
// File that process the raw data and constructs the customized html table
// for position profile review.

#endregion Header 

#region Directives                                                             

using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using System.Text.RegularExpressions;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Class that process the raw data and constructs the customized data
    /// for position profile review.
    /// </summary>
    public class PositionProfileReviewDataManager
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="bool"/> that holds whether to show links or not.
        /// </summary>
        private bool showLinks = false;

        /// <summary>
        /// A <see cref="int"/> that holds the position profile ID.
        /// </summary>
        private int positionProfileID = 0;

        /// <summary>
        /// A <see cref="bool"/> that holds the has owner rights status.
        /// </summary>
        private bool hasOwnerRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has recruiter assignment rights status.
        /// </summary>
        private bool hasRecruiterAssignmentRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has recruiter rights status.
        /// </summary>
        private bool hasRecruiterRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has candidate association rights status.
        /// </summary>
        private bool hasCandidateAssociationRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has interview creation rights status.
        /// </summary>
        private bool hasInterviewCreationRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has interview session creation rights status.
        /// </summary>
        private bool hasInterviewSesssionCreationRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has interview scheduling rights status.
        /// </summary>
        private bool hasInterviewSchedulingRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has test creation rights status.
        /// </summary>
        private bool hasTestCreationRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has test session creation rights status.
        /// </summary>
        private bool hasTestSesssionCreationRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has test scheduling rights status.
        /// </summary>
        private bool hasTestSchedulingRights = false;

        /// <summary>
        /// A <see cref="bool"/> that holds the has submittal to client rights status.
        /// </summary>
        private bool hasSubmittalToClientRights = false;

        #endregion Private Variables

        #region Private Constants                                              

        /// <summary>
        /// A <see cref="int"/> that holds the tables count.
        /// </summary>
        private const int TABLES_COUNT = 10;

        /// <summary>
        /// A <see cref="int"/> that holds the header detail table index.
        /// </summary>
        private const int HEADER_DETAIL_TABLE_INDEX = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the segment detail table index.
        /// </summary>
        private const int SEGMENT_DETAIL_TABLE_INDEX = 1;

        /// <summary>
        /// A <see cref="int"/> that holds the recruiter detail table index.
        /// </summary>
        private const int RECRUITER_DETAIL_TABLE_INDEX = 2;

        /// <summary>
        /// A <see cref="int"/> that holds the co owner etail table index.
        /// </summary>
        private const int CO_OWNER_DETAIL_TABLE_INDEX = 3;

        /// <summary>
        /// A <see cref="int"/> that holds the action detail table index.
        /// </summary>
        private const int ACTION_DETAIL_TABLE_INDEX = 4;

        /// <summary>
        /// A <see cref="int"/> that holds the data summary detail table index.
        /// </summary>
        private const int DATA_SUMMARY_TABLE_INDEX = 5;

        /// <summary>
        /// A <see cref="int"/> that holds the test details table index.
        /// </summary>
        private const int TEST_DETAILS_TABLE_INDEX = 6;

        /// <summary>
        /// A <see cref="int"/> that holds the test session details table index.
        /// </summary>
        private const int TEST_SESSION_DETAILS_TABLE_INDEX = 7;

        /// <summary>
        /// A <see cref="int"/> that holds the interview details table index.
        /// </summary>
        private const int INTERIVIEW_DETAILS_TABLE_INDEX = 8;

        /// <summary>
        /// A <see cref="int"/> that holds the interview session details table index.
        /// </summary>
        private const int INTERIVIEW_SESSION_DETAILS_TABLE_INDEX = 9;

        /// <summary>
        /// A <see cref="int"/> that holds the position segment ID.
        /// </summary>
        private const int POSITION_SEGMENT_ID = 1;

        /// <summary>
        /// A <see cref="int"/> that holds the technical skills segment ID.
        /// </summary>
        private const int TECHNICAL_SKILLS_SEGMENT_ID = 2;

        /// <summary>
        /// A <see cref="int"/> that holds the verticals ID.
        /// </summary>
        private const int VERTICALS_SEGMENT_ID = 3;

        /// <summary>
        /// A <see cref="int"/> that holds the roles segment ID.
        /// </summary>
        private const int ROLES_SEGMENT_ID = 4;

        /// <summary>
        /// A <see cref="int"/> that holds the education segment ID.
        /// </summary>
        private const int EDUCATION_SEGMENT_ID = 8;

        #endregion Private Constants

        #region Public Methods                                                 

        /// <summary>
        /// Method that constructs and returns the position profile summary
        /// html table.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="showLinks">
        /// A <see cref="bool"/> that holds whether to show links or not. For 
        /// page display links required for click actions that launches popups
        /// to show additional information. For PDF generation links are shown
        /// as simple text.
        /// </param>
        /// <param name="showStatus">
        /// A <see cref="bool"/> that holds whether to show the status or not.
        /// Status will be shown only for PDF & email attachment. For review
        /// page static controls are present.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the position profile sumary html
        /// table.
        /// <returns>
        public Table GetPositionProfileSummary(DataSet rawData, bool showLinks, bool showStatus)
        {
            this.showLinks = showLinks;

            // Retrieve values.
            RetrieveValues(rawData);

            Table htmlTable = new Table();
            htmlTable.Width = new Unit(100, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_table"))
            {
                htmlTable.Style.Add(style.Key, style.Value);
            }

            htmlTable.CellPadding = 0;
            htmlTable.CellSpacing = (showLinks) ? 4 : 0;

            htmlTable.BorderStyle = BorderStyle.None;
            htmlTable.BorderWidth = new Unit(0, UnitType.Pixel);

            #region Status Detail

            // Add status detail.
            TableRow statusRow = new TableRow();
            TableCell statusCell = new TableCell();
            statusCell.BorderStyle = BorderStyle.None;

            statusCell.Controls.Add(GetStatusDetailTable
                (rawData.Tables[HEADER_DETAIL_TABLE_INDEX]));
            statusRow.Cells.Add(statusCell);
            htmlTable.Rows.Add(statusRow);

            // Status row will be shown only for PDF & email attachment. For review page
            // static controls are present.
            if (showStatus == false)
                statusRow.Visible = false;

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_status_background"))
            {
                statusRow.Style.Add(style.Key, style.Value);
            }

            #endregion Status Detail

            #region Header Detail

            // Add header detail.
            TableRow headerRow = new TableRow();
            TableCell headerCell = new TableCell();
            headerCell.BorderStyle = BorderStyle.None;

            headerCell.Controls.Add(GetHeaderDetailTable
                (rawData.Tables[HEADER_DETAIL_TABLE_INDEX], 
                rawData.Tables[RECRUITER_DETAIL_TABLE_INDEX], 
                rawData.Tables[CO_OWNER_DETAIL_TABLE_INDEX]));
            headerRow.Cells.Add(headerCell);
            htmlTable.Rows.Add(headerRow);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_header_background"))
            {
                headerRow.Style.Add(style.Key, style.Value);
            }

            #endregion Header Detail

            #region Segment Detail

            // Add segment detail.
            TableRow segmentRow = new TableRow();
            TableCell segmentCell = new TableCell();
            segmentCell.Controls.Add(GetSegmentDetailTable(rawData.Tables[SEGMENT_DETAIL_TABLE_INDEX]));
            segmentRow.Cells.Add(segmentCell);
            htmlTable.Rows.Add(segmentRow);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segments_background"))
            {
                segmentRow.Style.Add(style.Key, style.Value);
            }

            #endregion Segment Detail

            #region Action Detail

            // Add action detail.
            TableRow actionRow = new TableRow();
            TableCell actionCell = new TableCell();
            actionCell.BorderStyle = BorderStyle.None;

            actionCell.Controls.Add(GetActionDetailTable(rawData.Tables[HEADER_DETAIL_TABLE_INDEX]));
            actionRow.Cells.Add(actionCell);
            htmlTable.Rows.Add(actionRow);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_actions_background"))
            {
                actionRow.Style.Add(style.Key, style.Value);
            }

            #endregion Action Detail

            #region Workflow Summary

            // Add workflow summary row.
            TableRow workflowSummaryRow = new TableRow();
            TableCell workflowSummaryCell = new TableCell();
            workflowSummaryCell.BorderStyle = BorderStyle.None;

            workflowSummaryCell.Controls.Add(GetWorkflowSummaryTable(rawData));
            workflowSummaryRow.Cells.Add(workflowSummaryCell);
            htmlTable.Rows.Add(workflowSummaryRow);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_background"))
            {
                workflowSummaryRow.Style.Add(style.Key, style.Value);
            }

            #endregion Workflow Summary

            return htmlTable;
        }

        #endregion Public Methods

        #region Private Methods                                                

        #region Header Detail                                                  

        /// <summary>
        /// Method that constructs and returns the status detail table.
        /// </summary>
        /// <param name="dataTable">
        /// A <see cref="DataTable"/> that holds the header details.
        /// </param>
        /// <param name="recruitersTable">
        /// A <see cref="DataTable"/> that holds the recruiters table.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the header detail table.
        /// </returns>
        private Table GetStatusDetailTable(DataTable dataTable)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            // Retreive 0th row.
            DataRow dataRow = dataTable.Rows[0];

            TableRow row = new TableRow();

            // Status caption cell.
            TableCell statusCaptionCell = new TableCell();
            statusCaptionCell.Width = new Unit(7, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_status_caption_cell"))
            {
                statusCaptionCell.Style.Add(style.Key, style.Value);
            }

            statusCaptionCell.Text = "Position Status";
            statusCaptionCell.VerticalAlign = VerticalAlign.Middle;
            statusCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(statusCaptionCell);

            // Status value cell.
            TableCell statusValueCell = new TableCell();
            statusValueCell.Width = new Unit(10, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_status_value_cell"))
            {
                statusValueCell.Style.Add(style.Key, style.Value);
            }

            statusValueCell.Text = GetValue(dataRow["POSITION_PROFILE_STATUS_NAME"]);
            statusValueCell.VerticalAlign = VerticalAlign.Middle;
            statusValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(statusValueCell);

            // Change status link cell.
            TableCell statusLinkCell = new TableCell();
            statusLinkCell.Width = new Unit(80, UnitType.Percentage);

            if (showLinks && hasOwnerRights)
            {
                HyperLink statusLinkButton = new HyperLink();
                statusLinkButton.Text = "Change Status";
                statusLinkButton.ToolTip = "Change Status";
                statusLinkButton.NavigateUrl = "~/PositionProfile/PositionProfileStatus.aspx?positionprofileid=" +
                    positionProfileID + "&m=1&s=1&parentpage=PP_REVIEW";
                statusLinkButton.Target = "_blank";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_status_edit_link"))
                {
                    statusLinkButton.Style.Add(style.Key, style.Value);
                }

                statusLinkCell.VerticalAlign = VerticalAlign.Middle;
                statusLinkCell.HorizontalAlign = HorizontalAlign.Right;
                statusLinkCell.Controls.Add(statusLinkButton);
            }

            row.Cells.Add(statusLinkCell);

            table.Rows.Add(row);

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the header detail table.
        /// </summary>
        /// <param name="dataTable">
        /// A <see cref="DataTable"/> that holds the header details.
        /// </param>
        /// <param name="recruitersTable">
        /// A <see cref="DataTable"/> that holds the recruiters table.
        /// </param>
        /// <param name="coOwnersTable">
        /// A <see cref="DataTable"/> that holds the co-owners table.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the header detail table.
        /// </returns>
        private Table GetHeaderDetailTable(DataTable dataTable, DataTable recruitersTable, DataTable coOwnersTable)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            if (dataTable.Rows == null || dataTable.Rows.Count == 0)
                throw new Exception("Raw data is invalid");

            DataRow dataRow = dataTable.Rows[0];

            TableRow[] rows = new TableRow[7];

            // Add edit basic info.
            rows[0] = new TableRow();
            rows[0].Cells.Add(GetEditBasicInfoCell());

            if (showLinks == false)
                rows[0].Visible = false;

            // Add position profile name.
            rows[1] = new TableRow();
            rows[1].Cells.Add(GetPositionProfileNameCell(dataRow));

            // Add registered by & co-owner
            rows[2] = new TableRow();
            rows[2].Cells.Add(GetRegisteredByCell(dataRow, coOwnersTable));

            // Add date of registration and submittal deadline.
            rows[3] = new TableRow();
            rows[3].Cells.Add(GetDateOfRegistrationCell(dataRow));

            // Add client, department, contact details
            rows[4] = new TableRow();
            rows[4].Cells.Add(GetClientCell(dataRow));

            // Add edit recruiter assignment cell.
            rows[5] = new TableRow();
            rows[5].Cells.Add(GetEditRecruiterAssignmentCell());

            if (showLinks == false)
                rows[5].Visible = false;

            // Add recruiter details
            rows[6] = new TableRow();
            rows[6].Cells.Add(GetRecruiterCell(recruitersTable));

            table.Rows.AddRange(rows);

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the edit basic info cell.
        /// </summary>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetEditBasicInfoCell()
        {
            TableCell cell = new TableCell();
            cell.Width = new Unit(100, UnitType.Percentage);

            if (showLinks && hasOwnerRights)
            {
                LinkButton editBasicInfoLink = new LinkButton();
                editBasicInfoLink.Text = "Edit Basic Info";
                editBasicInfoLink.ToolTip = "Edit Basic Info";
                editBasicInfoLink.PostBackUrl = "~/PositionProfile/PositionProfileBasicInfo.aspx?positionprofileid=" +
                    positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_profile_edit_link"))
                {
                    editBasicInfoLink.Style.Add(style.Key, style.Value);
                }

                cell.VerticalAlign = VerticalAlign.Middle;
                cell.HorizontalAlign = HorizontalAlign.Right;

                cell.Controls.Add(editBasicInfoLink);
            }

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the edit detailed info cell.
        /// </summary>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetEditDetailedInfoCell()
        {
            TableCell cell = new TableCell();
            cell.Width = new Unit(100, UnitType.Percentage);

            if (showLinks && hasOwnerRights)
            {
                LinkButton editDetailedInfoLink = new LinkButton();
                editDetailedInfoLink.Text = "Edit Detailed Info";
                editDetailedInfoLink.ToolTip = "Edit Detailed Info";
                editDetailedInfoLink.PostBackUrl = "~/PositionProfile/PositionProfileDetailInfo.aspx?positionprofileid=" +
                    positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_profile_edit_link"))
                {
                    editDetailedInfoLink.Style.Add(style.Key, style.Value);
                }

                cell.VerticalAlign = VerticalAlign.Middle;
                cell.HorizontalAlign = HorizontalAlign.Right;

                cell.Controls.Add(editDetailedInfoLink);
            }

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the edit recruiter assignment
        /// cell.
        /// </summary>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetEditRecruiterAssignmentCell()
        {
            TableCell cell = new TableCell();
            cell.Width = new Unit(100, UnitType.Percentage);

            if (showLinks && hasRecruiterAssignmentRights)
            {
                LinkButton assignRecruiterLink = new LinkButton();
                assignRecruiterLink.Text = "Edit Recruiter Assignment";
                assignRecruiterLink.ToolTip = "Edit Recruiter Assignment";
                assignRecruiterLink.PostBackUrl = "~/PositionProfile/PositionProfileRecruiterAssignment.aspx?positionprofileid=" +
                        positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_profile_edit_link"))
                {
                    assignRecruiterLink.Style.Add(style.Key, style.Value);
                }

                cell.VerticalAlign = VerticalAlign.Middle;
                cell.HorizontalAlign = HorizontalAlign.Right;

                cell.Controls.Add(assignRecruiterLink);
            }

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the position profile name cell.
        /// </summary>
        /// <param name="dataRow">
        /// A <see cref="DataRow"/> that holds the source data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetPositionProfileNameCell(DataRow dataRow)
        {
            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Name cell.
            TableCell nameCell = new TableCell();
            nameCell.Width = new Unit(80, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_profile_name"))
            {
                nameCell.Style.Add(style.Key, style.Value);
            }

            nameCell.Text = GetValue(dataRow["POSITION_PROFILE_NAME"]);
            nameCell.VerticalAlign = VerticalAlign.Middle;
            nameCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(nameCell);

            // Position profile ID cell.
            TableCell positinProfileIDCaptionCell = new TableCell();
            positinProfileIDCaptionCell.Width = new Unit(12, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                positinProfileIDCaptionCell.Style.Add(style.Key, style.Value);
            }

            positinProfileIDCaptionCell.Text = "Position Profile ID";
            positinProfileIDCaptionCell.VerticalAlign = VerticalAlign.Middle;
            positinProfileIDCaptionCell.HorizontalAlign = HorizontalAlign.Right;
            row.Cells.Add(positinProfileIDCaptionCell);

            // Position profile ID value cell.
            TableCell positinProfileIDValueCell = new TableCell();
            positinProfileIDValueCell.Width = new Unit(12, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_blue_cell"))
            {
                positinProfileIDValueCell.Style.Add(style.Key, style.Value);
            }

            positinProfileIDValueCell.Text = GetValue(dataRow["POSITION_PROFILE_KEY"]);
            positinProfileIDValueCell.VerticalAlign = VerticalAlign.Middle;
            positinProfileIDValueCell.HorizontalAlign = HorizontalAlign.Right;
            row.Cells.Add(positinProfileIDValueCell);

            cell.Controls.Add(table);

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the registered by and co-owners cell.
        /// </summary>
        /// <param name="dataRow">
        /// A <see cref="DataRow"/> that holds the source data.
        /// </param>
        /// <param name="coOwnersTable">
        /// A <see cref="DataTable"/> that holds the co-owners table.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetRegisteredByCell(DataRow dataRow, DataTable coOwnersTable)
        {
            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Add table to the cell.
            cell.Controls.Add(table);

            #region Registered By

            // Registered by caption cell.
            TableCell registeredByCaptionCell = new TableCell();
            registeredByCaptionCell.Width = new Unit(16, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string,string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                registeredByCaptionCell.Style.Add(style.Key, style.Value);
            }
            
            registeredByCaptionCell.Text = "Registered By";
            registeredByCaptionCell.VerticalAlign = VerticalAlign.Middle;
            registeredByCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(registeredByCaptionCell);

            // Registered by value cell.
            TableCell registeredByValueCell = new TableCell();
            registeredByValueCell.Width = new Unit(34, UnitType.Percentage);
            
            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                registeredByValueCell.Style.Add(style.Key, style.Value);
            }

            registeredByValueCell.Text = GetValue(dataRow["REGISTERED_BY_NAME"]);
            registeredByValueCell.VerticalAlign = VerticalAlign.Middle;
            registeredByValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(registeredByValueCell);

            #endregion Registered By

            #region Co-Owners

            // Co-Owners caption cell.
            TableCell coOwnersCaptionCell = new TableCell();
            coOwnersCaptionCell.Width = new Unit(16, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                coOwnersCaptionCell.Style.Add(style.Key, style.Value);
            }

            coOwnersCaptionCell.Text = "Co-Owners";
            coOwnersCaptionCell.VerticalAlign = VerticalAlign.Top;
            coOwnersCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(coOwnersCaptionCell);

            // Co-Owners value cell.
            TableCell coOwnersValueCell = new TableCell();
            coOwnersValueCell.Width = new Unit(34, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                coOwnersValueCell.Style.Add(style.Key, style.Value);
            }

            // Construct co-owners.
            if (coOwnersTable == null || coOwnersTable.Rows.Count == 0)
            {
                coOwnersValueCell.Text = "Co-owner(s) not assigned";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_missing_cell"))
                {
                    coOwnersValueCell.Style.Add(style.Key, style.Value);
                }
            }
            else
            {
                // Loop through the co-owners and add as link buttons.
                for (int coOwnerRowIndex = 0; coOwnerRowIndex < coOwnersTable.Rows.Count; coOwnerRowIndex++)
                {
                    // Add a comma literal.
                    if (coOwnerRowIndex != 0)
                    {
                        Literal literal = new Literal();
                        literal.Text = ", ";

                        // Add literal control to the cell.
                        coOwnersValueCell.Controls.Add(literal);

                    }
                    DataRow coOwnerRow = coOwnersTable.Rows[coOwnerRowIndex];

                    string coOwnerID = GetValue(coOwnerRow["CO_OWNER_ID"]);
                    string coOwnerName = GetValue(coOwnerRow["CO_OWNER_NAME"]);

                    if (showLinks == true)
                    {
                        LinkButton linkButton = new LinkButton();
                        linkButton.Text = coOwnerName;

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
                        {
                            linkButton.Style.Add(style.Key, style.Value);
                        }

                        linkButton.ToolTip = "Click here to view work load";
                        linkButton.Attributes.Add("onclick", "javascript:return ShowRecruiterProfile('" + coOwnerID + "','WL');");

                        // Add link button control to the cell.
                        coOwnersValueCell.Controls.Add(linkButton);
                    }
                    else
                    {
                        Label label = new Label();
                        label.Text = coOwnerName;

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
                        {
                            label.Style.Add(style.Key, style.Value);
                        }

                        // Add label control to the cell.
                        coOwnersValueCell.Controls.Add(label);
                    }
                }
            }

            coOwnersValueCell.VerticalAlign = VerticalAlign.Top;
            coOwnersValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(coOwnersValueCell);

            #endregion Co-Owners

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the date of registration and 
        /// submittal deadline cell.
        /// </summary>
        /// <param name="dataRow">
        /// A <see cref="DataRow"/> that holds the source data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetDateOfRegistrationCell(DataRow dataRow)
        {
            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Add table to the cell.
            cell.Controls.Add(table);

            #region Date Of Registration

            // Date of registration caption cell.
            TableCell dateOfRegistrationCaptionCell = new TableCell();
            dateOfRegistrationCaptionCell.Width = new Unit(16, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                dateOfRegistrationCaptionCell.Style.Add(style.Key, style.Value);
            }

            dateOfRegistrationCaptionCell.Text = "Date Of Registration";
            dateOfRegistrationCaptionCell.VerticalAlign = VerticalAlign.Middle;
            dateOfRegistrationCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(dateOfRegistrationCaptionCell);

            // Date of registration value cell.
            TableCell dateOfRegistrationValueCell = new TableCell();
            dateOfRegistrationValueCell.Width = new Unit(34, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                dateOfRegistrationValueCell.Style.Add(style.Key, style.Value);
            }

            dateOfRegistrationValueCell.Text = GetDateValue(dataRow["REGISTRATION_DATE"]);
            dateOfRegistrationValueCell.VerticalAlign = VerticalAlign.Middle;
            dateOfRegistrationValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(dateOfRegistrationValueCell);

            #endregion Date Of Registration

            #region Submittal Deadline

            // Submittal deadline caption cell.
            TableCell submittalDeadlineCaptionCell = new TableCell();
            submittalDeadlineCaptionCell.Width = new Unit(16, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                submittalDeadlineCaptionCell.Style.Add(style.Key, style.Value);
            }

            submittalDeadlineCaptionCell.Text = "Submittal Deadline";
            submittalDeadlineCaptionCell.VerticalAlign = VerticalAlign.Middle;
            submittalDeadlineCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(submittalDeadlineCaptionCell);

            // Submittal deadline value cell.
            TableCell submittalDeadlineValueCell = new TableCell();
            submittalDeadlineValueCell.Width = new Unit(34, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                submittalDeadlineValueCell.Style.Add(style.Key, style.Value);
            }

            submittalDeadlineValueCell.Text = GetDateValue(dataRow["SUBMITTAL_DEADLINE"]);
            submittalDeadlineValueCell.VerticalAlign = VerticalAlign.Middle;
            submittalDeadlineValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(submittalDeadlineValueCell);

            #endregion Submittal Deadline

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the client cell.
        /// </summary>
        /// <param name="dataRow">
        /// A <see cref="DataRow"/> that holds the source data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetClientCell(DataRow dataRow)
        {
            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Add table to the cell.
            cell.Controls.Add(table);

            #region Client

            // Client cell.
            TableCell clientCell = new TableCell();
            clientCell.Width = new Unit(16, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_bold_cell"))
            {
                clientCell.Style.Add(style.Key, style.Value);
            }

            string client = GetValue(dataRow["CLIENT"]);

            string clientID = string.Empty;
            string clientName = string.Empty;

            if (!Utility.IsNullOrEmpty(client))
            {
                Regex regex = new Regex(@"(^\d*)");
                Match match = regex.Match(client);

                if (match.Success)
                {
                    clientID = match.Value;
                    clientName = regex.Replace(client, string.Empty);
                    clientName = clientName.Trim(new char[] { '~' });
                }
            }

            clientCell.VerticalAlign = VerticalAlign.Top;
            clientCell.HorizontalAlign = HorizontalAlign.Left;

            if (Utility.IsNullOrEmpty(clientName))
            {
                clientCell.Text = "Client not associated";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_missing_cell"))
                {
                    clientCell.Style.Add(style.Key, style.Value);
                }
            }
            else
            {
                if (showLinks == true)
                {
                    LinkButton clientLinkButton = new LinkButton();
                    clientLinkButton.Text = clientName;

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_bold_cell"))
                    {
                        clientLinkButton.Style.Add(style.Key, style.Value);
                    }

                    clientLinkButton.ToolTip = "Click here to view client details";
                    clientLinkButton.Attributes.Add("onclick", "javascript:return ShowViewClientInformation('" + clientID + "','CI');");
                    clientCell.Controls.Add(clientLinkButton);
                }
                else
                {
                    clientCell.Text = clientName;

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_bold_cell"))
                    {
                        clientCell.Style.Add(style.Key, style.Value);
                    }
                }
            }

            row.Cells.Add(clientCell);

            #endregion Client

            #region Client Department

            // Client department cell.
            TableCell clientDepartmentsCell = new TableCell();
            clientDepartmentsCell.Width = new Unit(34, UnitType.Percentage);

            if (Utility.IsNullOrEmpty(GetValue(dataRow["CLIENT_DEPARTMENTS"])))
            {
                clientDepartmentsCell.Text = "Client department(s) not associated";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_missing_cell"))
                {
                    clientDepartmentsCell.Style.Add(style.Key, style.Value);
                }
            }
            else
            {
               
                // Get comma separated departments.
                string[] departements = GetValue(dataRow["CLIENT_DEPARTMENTS"]).Split(new char[] { '~' });

                int departmentCnt = 0;

                foreach (string departement in departements)
                {
                    // Add a comma literal.
                    if (departmentCnt != 0)
                    {
                        Literal literal = new Literal();
                        literal.Text = ", ";

                        // Add literal control to the cell.
                        clientDepartmentsCell.Controls.Add(literal);
                    }

                    if (!Utility.IsNullOrEmpty(departement))
                    {
                        // Get comma separated department details.
                        string[] departementInfo = departement.Split(new char[] { '#' });

                        string departmentID = departementInfo[0];
                        string departmentName = departementInfo[1];

                        if (showLinks == true)
                        {
                            LinkButton linkButton = new LinkButton();
                            linkButton.Text = departmentName;

                            // Apply style.
                            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
                            {
                                linkButton.Style.Add(style.Key, style.Value);
                            }

                            linkButton.ToolTip = "Click here to view department";
                            linkButton.Attributes.Add("onclick", "javascript:return ShowViewClientInformation('" + departmentID + "','CD');");

                            // Add link button control to the cell.
                            clientDepartmentsCell.Controls.Add(linkButton);
                        }
                        else
                        {
                            Label label = new Label();
                            label.Text = departmentName;

                            // Apply style.
                            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
                            {
                                label.Style.Add(style.Key, style.Value);
                            }

                            // Add label control to the cell.
                            clientDepartmentsCell.Controls.Add(label);
                        }
                    }
                    departmentCnt++;
                }
            }

            clientDepartmentsCell.VerticalAlign = VerticalAlign.Top;
            clientDepartmentsCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(clientDepartmentsCell);

            #endregion Client Department

            #region Client Contacts

            // Client contacts cell.
            TableCell clientContactsCell = new TableCell();
            clientContactsCell.Width = new Unit(50, UnitType.Percentage);

            if (Utility.IsNullOrEmpty(GetValue(dataRow["CLIENT_CONTACTS"])))
            {
                clientContactsCell.Text = "Client contact(s) not associated";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_missing_cell"))
                {
                    clientContactsCell.Style.Add(style.Key, style.Value);
                }
            }
            else
            {
                // Get comma separated contacts.
                string[] contacts = GetValue(dataRow["CLIENT_CONTACTS"]).Split(new char[] { '~' });

                int contactCnt = 0;

                foreach (string contact in contacts)
                {
                    // Add a comma literal.
                    if (contactCnt != 0)
                    {
                        Literal literal = new Literal();
                        literal.Text = ", ";

                        // Add literal control to the cell.
                        clientContactsCell.Controls.Add(literal);
                    }

                    if (!Utility.IsNullOrEmpty(contact))
                    {
                        // Get comma separated contact details.
                        string[] contactInfo = contact.Split(new char[] { '#' });

                        string contactID = contactInfo[0];
                        string contactName = contactInfo[1];

                        if (showLinks == true)
                        {
                            LinkButton linkButton = new LinkButton();
                            linkButton.Text = contactName;

                            // Apply style.
                            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
                            {
                                linkButton.Style.Add(style.Key, style.Value);
                            }

                            linkButton.ToolTip = "Click here to view contact";
                            linkButton.Attributes.Add("onclick", "javascript:return ShowViewClientInformation('" + contactID + "','CC');");

                            // Add link button control to the cell.
                            clientContactsCell.Controls.Add(linkButton);
                        }
                        else
                        {
                            Label label = new Label();
                            label.Text = contactName;

                            // Apply style.
                            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
                            {
                                label.Style.Add(style.Key, style.Value);
                            }

                            // Add label control to the cell.
                            clientContactsCell.Controls.Add(label);
                        }
                    }
                    contactCnt++;
                }
            }

            clientContactsCell.VerticalAlign = VerticalAlign.Top;
            clientContactsCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(clientContactsCell);

            #endregion Client Contacts

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the recruiter details.
        /// </summary>
        /// <param name="recruitersTable">
        /// A <see cref="DataTable"/> that holds the recruiters table.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetRecruiterCell(DataTable recruitersTable)
        {
            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            #region Recruiters

            // Recruiters caption cell.
            TableCell recruitersCaptionCell = new TableCell();
            recruitersCaptionCell.Width = new Unit(16, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                recruitersCaptionCell.Style.Add(style.Key, style.Value);
            }

            recruitersCaptionCell.Text = "Recruiters";
            recruitersCaptionCell.VerticalAlign = VerticalAlign.Top;
            recruitersCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(recruitersCaptionCell);

            // Recruiters value cell.
            TableCell recruitersValueCell = new TableCell();
            recruitersValueCell.Width = new Unit(84, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                recruitersValueCell.Style.Add(style.Key, style.Value);
            }

            // Construct recruiters.
            if (recruitersTable == null || recruitersTable.Rows.Count == 0)
            {
                recruitersValueCell.Text = "Recruiter(s) not assigned";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_missing_cell"))
                {
                    recruitersValueCell.Style.Add(style.Key, style.Value);
                }
            }
            else
            {
                // Loop through the recruiters and add as link buttons.
                for (int recruiterRowIndex = 0 ; recruiterRowIndex < recruitersTable.Rows.Count; recruiterRowIndex++)
                {
                    // Add a comma literal.
                    if (recruiterRowIndex != 0)
                    {
                        Literal literal = new Literal();
                        literal.Text = ", ";

                        // Add literal control to the cell.
                        recruitersValueCell.Controls.Add(literal);

                    }
                    DataRow recruiterRow = recruitersTable.Rows[recruiterRowIndex];

                    string recruiterID = GetValue(recruiterRow["RECRUITER_ID"]);
                    string recruiterName = GetValue(recruiterRow["RECRUITER_NAME"]);

                    if (showLinks == true)
                    {
                        LinkButton linkButton = new LinkButton();
                        linkButton.Text = recruiterName;

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
                        {
                            linkButton.Style.Add(style.Key, style.Value);
                        }

                        linkButton.ToolTip = "Click here to view recruiter profile";
                        linkButton.Attributes.Add("onclick", "javascript:return ShowRecruiterProfile('" + recruiterID + "','RP');");

                        // Add link button control to the cell.
                        recruitersValueCell.Controls.Add(linkButton);
                    }
                    else
                    {
                        Label label = new Label();
                        label.Text = recruiterName;

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
                        {
                            label.Style.Add(style.Key, style.Value);
                        }

                        // Add label control to the cell.
                        recruitersValueCell.Controls.Add(label);
                    }
                }
            }

            recruitersValueCell.VerticalAlign = VerticalAlign.Top;
            recruitersValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(recruitersValueCell);

            #endregion Recruiters

            // Add table to the cell.
            cell.Controls.Add(table);

            return cell;
        }

        #endregion Header Detail

        #region Segment Detail                                                 

        /// <summary>
        /// Method that constructs and returns the segment detail table
        /// </summary>
        /// <param name="segmentsTable">
        /// A <see cref="DataTable"/> that holds the segment details.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the segment detail table.
        /// </returns>
        private Table GetSegmentDetailTable(DataTable segmentsTable)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            if (segmentsTable.Rows == null || segmentsTable.Rows.Count == 0)
            {
                // Add an empty 'N/A' row.
                TableRow emptyRow = new TableRow();
                TableCell emptyCell = new TableCell();
                emptyCell.Text = "No segments available";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_missing_cell"))
                {
                    emptyCell.Style.Add(style.Key, style.Value);
                }
                emptyRow.Cells.Add(emptyCell);
                table.Rows.Add(emptyRow);

                return table;
            }

            // Construct rows.
            TableRow[] rows = new TableRow[segmentsTable.Rows.Count + 1];

            // Add edit detailed info link.
            TableCell editDetailedInfoCell = GetEditDetailedInfoCell();
            rows[0] = new TableRow();
            rows[0].Cells.Add(editDetailedInfoCell);

            if (showLinks == false)
                rows[0].Visible = false;

            // Loop through the segment rows and construct the table cells.
            for (int rowIndex = 0; rowIndex < segmentsTable.Rows.Count; rowIndex++)
            {
                // Identify segment type.
                int segmentID = GetIntegerValue(segmentsTable.Rows[rowIndex]["SEGMENT_ID"]);

                Table innerTable = null;

                switch (segmentID)
                {
                    case POSITION_SEGMENT_ID:
                        innerTable = GetPositionSegmentTable(segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString());
                        break;

                    case TECHNICAL_SKILLS_SEGMENT_ID:
                        innerTable = GetTechnicalSkillsSegmentTable(segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString());
                        break;

                    case VERTICALS_SEGMENT_ID:
                        innerTable = GetVerticalBackgroundSegmentTable(segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString());
                        break;

                    case ROLES_SEGMENT_ID:
                        innerTable = GetRolesSegmentTable(segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString());
                        break;

                    case EDUCATION_SEGMENT_ID:
                        innerTable = GetEducationSegmentTable(segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString());
                        break;
                }

                rows[rowIndex + 1] = new TableRow();

                if (innerTable != null)
                {
                    TableCell tableCell = new TableCell();
                    tableCell.Controls.Add(innerTable);

                    rows[rowIndex + 1].Cells.Add(tableCell);
                }
            }

            table.Rows.AddRange(rows);

            return table;
        }

        #endregion Segment Detail

        #region Action Detail                                                  

        /// <summary>
        /// Method that constructs and returns the action detail table.
        /// </summary>
        /// <param name="dataTable">
        /// A <see cref="DataTable"/> that holds the source data.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the action detail table.
        /// </returns>
        private Table GetActionDetailTable(DataTable dataTable)
        {
            Table table = new Table();
            table.CellSpacing = 2;
            table.CellPadding = 0;

            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            if (dataTable.Rows == null || dataTable.Rows.Count == 0)
                throw new Exception("Raw data is invalid");

            DataRow dataRow = dataTable.Rows[0];

            // Add action caption.
            TableRow captionRow = new TableRow();
            captionRow.Cells.Add(GetSectionCaptionCell("Actions Required"));
            table.Rows.Add(captionRow);

            // Add general action value.
            TableCell generalActionTableCell = GetGeneralActionsValueCell(dataRow);
            if (generalActionTableCell != null)
            {
                TableRow generalActionRow = new TableRow();
                generalActionRow.Cells.Add(generalActionTableCell);
                table.Rows.Add(generalActionRow);
            }

            // Add test action value.
            TableCell testActionTableCell = GetTestActionsValueCell(dataRow);
            if (testActionTableCell != null)
            {
                TableRow testActionRow = new TableRow();
                testActionRow.Cells.Add(testActionTableCell);
                table.Rows.Add(testActionRow);
            }

            // Add interview action value.
            TableCell interviewActionTableCell = GetInterviewActionsValueCell(dataRow);
            if (interviewActionTableCell != null)
            {
                TableRow interviewActionRow = new TableRow();
                interviewActionRow.Cells.Add(interviewActionTableCell);
                table.Rows.Add(interviewActionRow);
            }

            // If all actions are not present then show a N/A cell.
            if (generalActionTableCell == null && testActionTableCell == null && interviewActionTableCell == null)
            {
                TableCell emptyCell = new TableCell();
                emptyCell.Text = "No actions available";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_missing_cell"))
                {
                    emptyCell.Style.Add(style.Key, style.Value);
                }

                // Add empty row.
                TableRow emptyRow = new TableRow();
                emptyRow.Cells.Add(emptyCell);
                table.Rows.Add(emptyRow);
            }

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the section caption cell.
        /// </summary>
        /// <param name="title">
        /// A <see cref="string"/> that holds the title.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetSectionCaptionCell(string title)
        {
            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            table.CellPadding = 0;
            table.CellSpacing = 0;
            TableRow row = new TableRow();

            // Name cell.
            TableCell nameCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_actions_caption_cell"))
            {
                nameCell.Style.Add(style.Key, style.Value);
            }

            nameCell.Text = title;
            nameCell.VerticalAlign = VerticalAlign.Middle;
            nameCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(nameCell);
            table.Rows.Add(row);
            cell.Controls.Add(table);

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the workflow legend cell.
        /// </summary>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the table cell.
        /// </returns>
        private TableCell GetWorkflowLegendCell()
        {
            TableCell cell = new TableCell();
            cell.HorizontalAlign = HorizontalAlign.Right;

            #region Action Missing

            // Action missing color.
            Label actionColorCell = new Label();
            actionColorCell.Width = new Unit(16, UnitType.Pixel);
            actionColorCell.Text = "[ ]";

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_action_missing_legend_cell"))
            {
                actionColorCell.Style.Add(style.Key, style.Value);
            }

            cell.Controls.Add(actionColorCell);

            // Action missing caption.
            Label actionCaptionCell = new Label();
            actionCaptionCell.Text = " Action Pending";

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_legend_caption_cell"))
            {
                actionCaptionCell.Style.Add(style.Key, style.Value);
            }

            cell.Controls.Add(actionCaptionCell);
            cell.Controls.Add(GetSpaceControl());

            #endregion Action Missing

            return cell;
        }

        /// <summary>
        /// Method that returns the workflow title label.
        /// </summary>
        /// <param name="title">
        /// A <see cref="string"/> that holds the title.
        /// </param>
        /// <returns>
        /// A <see cref="Label"/> that holds the title label.
        /// </returns>
        private Label GetWorkflowTitleLabel(string title)
        {
            Label label = new Label();
            label.Text = title;

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_title_cell"))
            {
                label.Style.Add(style.Key, style.Value);
            }

            return label;
        }
        /// <summary>
        /// Method that constructs and returns the general actions value cell.
        /// </summary>
        /// <param name="dataRow">
        /// A <see cref="DataRow"/> that holds the source data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the test actions value cell.
        /// </returns>
        private TableCell GetGeneralActionsValueCell(DataRow dataRow)
        {
            string actionValue = GetValue(dataRow["GENERAL_ACTIONS_REQUIRED"]);

            if (Utility.IsNullOrEmpty(actionValue))
                return null;

            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Test action value cell.
            TableCell testActionValueCell = new TableCell();
            testActionValueCell.Width = new Unit(30, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_actions_value_cell"))
            {
                testActionValueCell.Style.Add(style.Key, style.Value);
            }

            testActionValueCell.Text = actionValue;
            testActionValueCell.VerticalAlign = VerticalAlign.Middle;
            testActionValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(testActionValueCell);

            // Add table to the cell.
            cell.Controls.Add(table);

            return cell;
        }


        /// <summary>
        /// Method that constructs and returns the test actions value cell.
        /// </summary>
        /// <param name="dataRow">
        /// A <see cref="DataRow"/> that holds the source data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the test actions value cell.
        /// </returns>
        private TableCell GetTestActionsValueCell(DataRow dataRow)
        {
            string actionValue = GetValue(dataRow["TEST_ACTIONS_REQUIRED"]);

            if (Utility.IsNullOrEmpty(actionValue))
                return null;

            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Test action value cell.
            TableCell testActionValueCell = new TableCell();
            testActionValueCell.Width = new Unit(30, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_actions_value_cell"))
            {
                testActionValueCell.Style.Add(style.Key, style.Value);
            }

            testActionValueCell.Text = actionValue;
            testActionValueCell.VerticalAlign = VerticalAlign.Middle;
            testActionValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(testActionValueCell);

            // Add table to the cell.
            cell.Controls.Add(table);

            return cell;
        }

        /// <summary>
        /// Method that constructs and returns the interview actions value cell.
        /// </summary>
        /// <param name="dataRow">
        /// A <see cref="DataRow"/> that holds the source data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the interview actions value cell.
        /// </returns>
        private TableCell GetInterviewActionsValueCell(DataRow dataRow)
        {
            string actionValue = GetValue(dataRow["INTERVIEW_ACTIONS_REQUIRED"]);

            if (Utility.IsNullOrEmpty(actionValue))
                return null;

            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Interview action value cell.
            TableCell interviewActionValueCell = new TableCell();
            interviewActionValueCell.Width = new Unit(30, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_actions_value_cell"))
            {
                interviewActionValueCell.Style.Add(style.Key, style.Value);
            }

            interviewActionValueCell.Text = actionValue;
            interviewActionValueCell.VerticalAlign = VerticalAlign.Middle;
            interviewActionValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(interviewActionValueCell);

            // Add table to the cell.
            cell.Controls.Add(table);

            return cell;
        }

        #endregion Action Detail

        #region Workflow Summary                                               

        /// <summary>
        /// Method that constructs and returns the action detail table.
        /// </summary>
          /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the workflow summary html table.
        /// </returns>
        private Table GetWorkflowSummaryTable(DataSet rawData)
        {
            DataTable actionDetailTable = rawData.Tables[ACTION_DETAIL_TABLE_INDEX];
            DataTable dataSummaryTable = rawData.Tables[DATA_SUMMARY_TABLE_INDEX];



            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            #region Edit Workflow Link

            TableRow editWorkflowRow = new TableRow();

            TableCell editWorkflowCell = new TableCell();

            if (showLinks && hasOwnerRights)
            {
                // Add edit workflow link button.
                LinkButton editBasicInfoLink = new LinkButton();
                editBasicInfoLink.Text = "Edit Workflow Selection";
                editBasicInfoLink.ToolTip = "Edit Workflow Selection";
                editBasicInfoLink.PostBackUrl = "~/PositionProfile/PositionProfileWorkflowSelection.aspx?positionprofileid=" +
                    positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_profile_edit_link"))
                {
                    editBasicInfoLink.Style.Add(style.Key, style.Value);
                }

                editWorkflowCell.Controls.Add(editBasicInfoLink);
            }

            editWorkflowCell.VerticalAlign = VerticalAlign.Middle;
            editWorkflowCell.HorizontalAlign = HorizontalAlign.Right;

            editWorkflowRow.Cells.Add(editWorkflowCell);
            table.Rows.Add(editWorkflowRow);

            #endregion Edit Workflow Link

            #region Caption

            TableRow captionRow = new TableRow();
            TableCell captionCell = new TableCell();
            captionRow.Cells.Add(captionCell);

            Table captionInnerTable = new Table();
            captionInnerTable.Width = new Unit(100, UnitType.Percentage);
            captionInnerTable.CellSpacing = 0;
            captionInnerTable.CellPadding = 0;

            TableRow captionInnerRow = new TableRow();
            captionInnerTable.Rows.Add(captionInnerRow);

            TableCell captionInnerCell = new TableCell();
            captionInnerCell.HorizontalAlign = HorizontalAlign.Left;

            // Add workflow summary caption.
            TableCell workflowSummaryCell = GetSectionCaptionCell("Workflow Summary");
            captionInnerCell.Controls.Add(workflowSummaryCell);

            // Add workflow legend.
            TableCell workflowLegendCell = GetWorkflowLegendCell();
            captionInnerCell.Controls.Add(workflowLegendCell);

            captionInnerRow.Cells.Add(captionInnerCell);

            captionCell.Controls.Add(captionInnerTable);
            
            table.Rows.Add(captionRow);

            #endregion Caption

            // If actions are not present then show a N/A cell.
            if (actionDetailTable == null || actionDetailTable.Rows == null || actionDetailTable.Rows.Count == 0)
            {
                // Hide the legend cell.
                workflowLegendCell.Visible = false;

                TableCell emptyCell = new TableCell();
                emptyCell.Text = "No summary available";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_missing_cell"))
                {
                    emptyCell.Style.Add(style.Key, style.Value);
                }

                // Add empty row.
                TableRow emptyRow = new TableRow();
                emptyRow.Cells.Add(emptyCell);
                table.Rows.Add(emptyRow);

                return table;
            }

            if (dataSummaryTable.Rows == null || dataSummaryTable.Rows.Count == 0)
                return table;

            // Get data summary row.
            DataRow dataSummaryRow = dataSummaryTable.Rows[0];

            // Loop through the workflow statuses and show the appropriate details.
            foreach (DataRow dataRow in actionDetailTable.Rows)
            {
                // Get action type.
                string actionType = GetValue(dataRow["ACTION_TYPE"]);
                bool actionAvailable = GetValue(dataRow["ACTION_AVAILABLE"]) == "Y" ? true : false;
                bool taskOwnerAvailable = GetValue(dataRow["TASK_OWNER_AVAILABLE"]) == "Y" ? true : false;

                TableRow valueRow = null;

                switch (actionType)
                {
                    // Recruiter assignment
                    case "PAT_REC_AS":
                        {
                            valueRow = GetRecruiterAssignmentSummaryRow(taskOwnerAvailable, dataSummaryRow, 
                                rawData.Tables[RECRUITER_DETAIL_TABLE_INDEX]);
                            table.Rows.Add(valueRow);
                        }
                        break;

                    // Candidate association.
                    case "PAT_CAN_AS":
                        {
                            valueRow = GetCandidateAssociationSummaryRow(actionAvailable, taskOwnerAvailable, dataSummaryRow);
                            table.Rows.Add(valueRow);
                        }
                        break;

                    // Interview creation.
                    case "PAT_INT_RQ":
                        {
                            valueRow = GetInterviewRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow, 
                                rawData.Tables[INTERIVIEW_DETAILS_TABLE_INDEX]);
                            table.Rows.Add(valueRow);
                        }
                        break;

                    // Interview session required.
                    case "PAT_INT_SS":
                        {
                            valueRow = GetInterviewSessionRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow, 
                                rawData.Tables[INTERIVIEW_SESSION_DETAILS_TABLE_INDEX], rawData.Tables[INTERIVIEW_DETAILS_TABLE_INDEX]);
                            table.Rows.Add(valueRow);
                        }
                        break;

                    // Interview schedule required.
                    case "PAT_INT_SD":
                        {
                            valueRow = GetInterviewScheduleRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow, rawData.Tables[INTERIVIEW_SESSION_DETAILS_TABLE_INDEX]);
                            table.Rows.Add(valueRow);
                        }
                        break;

                        // Interview assessment
                    case "PAT_INT_AS":
                        {
                            valueRow = GetInterviewAssessmentSummaryRow(taskOwnerAvailable, dataSummaryRow, rawData.Tables[INTERIVIEW_SESSION_DETAILS_TABLE_INDEX]);
                            table.Rows.Add(valueRow);
                        }
                        break;

                    // Test creation.
                    case "PAT_TST_RQ":
                        {
                            valueRow = GetTestRequiredSummaryRow(taskOwnerAvailable, 
                                dataSummaryRow, rawData.Tables[TEST_DETAILS_TABLE_INDEX]);
                            table.Rows.Add(valueRow);
                        }
                        break;

                    // Test session required.
                    case "PAT_TST_SS":
                        {
                            valueRow = GetTestSessionRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow, rawData.Tables[TEST_SESSION_DETAILS_TABLE_INDEX], rawData.Tables[TEST_DETAILS_TABLE_INDEX]);
                            table.Rows.Add(valueRow);
                        }
                        break;

                    // Test schedule required.
                    case "PAT_TST_SD":
                        {
                            valueRow = GetTestScheduleRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow, rawData.Tables[TEST_SESSION_DETAILS_TABLE_INDEX]);
                            table.Rows.Add(valueRow);
                        }
                        break;

                    // Submittal to client.
                    case "PAT_SUB_CL":
                        {
                            valueRow = GetCandidateSubmittedSummaryRow(actionAvailable, taskOwnerAvailable, dataSummaryRow);
                            table.Rows.Add(valueRow);
                        }
                        break;
                }

                if (valueRow != null)
                {
                    if (actionAvailable == false)
                    {
                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_action_missing_background"))
                        {
                            valueRow.Style.Add(style.Key, style.Value);
                        }
                    }
                }
            }

            return table;
        }

        /// <summary>
        /// Method that retrieves the test required summary row.
        /// </summary>
        /// <param name="actionAvailable">
        /// A <see cref="bool"/> that holds the action available status.
        /// </param>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="testDetailsTable">
        /// A <see cref="DataTable"/> that holds the test details table.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetTestRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable testDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (testDetailsTable != null && testDetailsTable.Rows != null && testDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;
            valueRow.Cells.Add(valueCell);

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Test Creation: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Test not created.";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);
            }
            else if (actionAvailable == true)
            {
                // Add caption label.
                Label preCaptionLabel = new Label();

                if (testDetailsTable.Rows.Count == 1)
                    preCaptionLabel.Text = "1 test";
                else
                    preCaptionLabel.Text = string.Format("{0} tests", testDetailsTable.Rows.Count);

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    preCaptionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(preCaptionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Loop through the table and add the test names.
                for (int rowIndex = 0 ; rowIndex < testDetailsTable.Rows.Count ; rowIndex++)
                {
                    DataRow testDetailRow = testDetailsTable.Rows[rowIndex];

                    if (showLinks && hasTestCreationRights)
                    {
                        HyperLink captionLink = new HyperLink();
                        captionLink.Text = GetValue(testDetailRow["TEST_NAME"]);
                        captionLink.ToolTip = "Click here to view test";
                        captionLink.NavigateUrl = "~/TestMaker/ViewTest.aspx?m=1&s=2&testkey=" + GetValue(testDetailRow["TEST_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        captionLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            captionLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(captionLink);
                    }
                    else
                    {
                        // Add test name label.
                        Label testNameLabel = new Label();
                        testNameLabel.Text = GetValue(testDetailRow["TEST_NAME"]);

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            testNameLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(testNameLabel);
                    }

                    if (rowIndex + 1 < testDetailsTable.Rows.Count)
                    {
                        // Add a comma.
                        valueCell.Controls.Add(GetCommaControl());
                    }
                }

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add caption label.
                Label postCaptionLabel = new Label();
                postCaptionLabel.Text = "created.";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    postCaptionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(postCaptionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());
            }

            // Show create test link always.
            if (showLinks == true && hasTestCreationRights)
            {
                // Add create test label.
                Label createTestLabel = new Label();
                createTestLabel.Text = "To create test click ";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    createTestLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(createTestLabel);

                HyperLink createTestLink = new HyperLink();
                createTestLink.Text = "Create Test";
                createTestLink.ToolTip = "Click here to create test";
                createTestLink.NavigateUrl = "~/TestMaker/CreateAutomaticTest.aspx?m=1&s=0&positionprofileid=" +
                    positionProfileID + "&parentpage=PP_REVIEW";
                createTestLink.Target = "_blank";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                {
                    createTestLink.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(createTestLink);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the test session required summary row.
        /// </summary>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="testSessionDetailsTable">
        /// A <see cref="DataTable"/> that holds the test session details table.
        /// </param>
        /// <param name="testDetailsTable">
        /// A <see cref="DataTable"/> that holds the test details table.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetTestSessionRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable testSessionDetailsTable, DataTable testDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (testSessionDetailsTable != null && testSessionDetailsTable.Rows != null && testSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;
            valueRow.Cells.Add(valueCell);

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Test Session Creation: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Test session not created. ";

                if (testDetailsTable == null || testDetailsTable.Rows == null || testDetailsTable.Rows.Count == 0)
                {
                    captionLabel.Text = captionLabel.Text.Trim();
                    captionLabel.Text += " Test needs to be associated before creating test session. ";
                }

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);
            }

            if (actionAvailable == true)
            {
                // Add caption label.
                Label preCaptionLabel = new Label();

                if (testSessionDetailsTable.Rows.Count == 1)
                    preCaptionLabel.Text = "1 test session";
                else
                    preCaptionLabel.Text = string.Format("{0} test sessions", testSessionDetailsTable.Rows.Count);

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    preCaptionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(preCaptionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                 // Loop through the table and add the test sessions..
                for (int rowIndex = 0; rowIndex < testSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow testSessionDetailRow = testSessionDetailsTable.Rows[rowIndex];

                    if (showLinks && (hasTestSchedulingRights || hasTestSesssionCreationRights))
                    {
                        // Add caption link.
                        HyperLink captionLink = new HyperLink();
                        captionLink.Text = string.Format("{0}({1})",
                            GetValue(testSessionDetailRow["SESSION_KEY"]), GetValue(testSessionDetailRow["SESSION_COUNT"]));

                        captionLink.ToolTip = "Click here to view test sessions";
                        captionLink.NavigateUrl = "~/Scheduler/ScheduleCandidate.aspx?m=2&s=1&index=3&testsessionid=" + GetValue(testSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        captionLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            captionLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(captionLink);
                    }
                    else
                    {
                        // Add count label.
                        Label countLabel = new Label();
                        countLabel.Text = string.Format("{0}({1})",
                            GetValue(testSessionDetailRow["SESSION_KEY"]), GetValue(testSessionDetailRow["SESSION_COUNT"]));

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            countLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(countLabel);
                    }

                    if (rowIndex + 1 < testSessionDetailsTable.Rows.Count)
                    {
                        // Add a comma.
                        valueCell.Controls.Add(GetCommaControl());
                    }
                }

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = " created.";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add link
                if (showLinks == true && hasTestSesssionCreationRights)
                {
                    // Add caption label.
                    Label preEditCaptionLabel = new Label();

                    if (testSessionDetailsTable.Rows.Count == 1)
                        preEditCaptionLabel.Text = "To edit test session click ";
                    else
                        preEditCaptionLabel.Text = "To edit test sessions click ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        preEditCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(preEditCaptionLabel);

                     // Loop through the table and add the edit test session link
                    for (int rowIndex = 0; rowIndex < testSessionDetailsTable.Rows.Count; rowIndex++)
                    {
                        DataRow testSessionDetailRow = testSessionDetailsTable.Rows[rowIndex];

                        HyperLink valueLink = new HyperLink();
                        valueLink.Text = GetValue(testSessionDetailRow["SESSION_KEY"]);
                        valueLink.ToolTip = "Click here to edit test session";
                        valueLink.NavigateUrl = "~/TestMaker/EditTestSession.aspx?m=1&s=3&testsessionid=" + GetValue(testSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        valueLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            valueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(valueLink);

                        if (rowIndex + 1 < testSessionDetailsTable.Rows.Count)
                        {
                            // Add a comma.
                            valueCell.Controls.Add(GetCommaControl());
                        }
                    }

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }
            }

            // Add link to create test sessions.
            if (testDetailsTable != null && testDetailsTable.Rows != null && testDetailsTable.Rows.Count > 0)
            {
                if (showLinks == true && hasTestSesssionCreationRights)
                {
                    // Add caption label.
                    Label preCreateSessionCaptionLabel = new Label();
                    preCreateSessionCaptionLabel.Text = "To create test session click ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        preCreateSessionCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(preCreateSessionCaptionLabel);

                    // Loop through the table and add the create session link
                    for (int rowIndex = 0; rowIndex < testDetailsTable.Rows.Count; rowIndex++)
                    {
                        DataRow testDetailRow = testDetailsTable.Rows[rowIndex];

                        HyperLink valueLink = new HyperLink();
                        valueLink.Text = GetValue(testDetailRow["TEST_NAME"]);
                        valueLink.ToolTip = "Click here to create test session";
                        valueLink.NavigateUrl = "~/TestMaker/CreateTestSession.aspx?m=1&s=0&positionprofileid=" + positionProfileID + "&testkey=" +
                            GetValue(testDetailRow["TEST_KEY"]) + "&parentpage=PP_REVIEW";
                        valueLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            valueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(valueLink);

                        if (rowIndex + 1 < testDetailsTable.Rows.Count)
                        {
                            // Add a comma.
                            valueCell.Controls.Add(GetCommaControl());
                        }
                    }

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the test schedule required summary row.
        /// </summary>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="testSessionDetailsTable">
        /// A <see cref="DataTable"/> that holds the test session details table.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetTestScheduleRequiredSummaryRow( bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable testSessionDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (testSessionDetailsTable != null && testSessionDetailsTable.Rows != null && testSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;
            valueRow.Cells.Add(valueCell);

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Test Scheduling: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Test not scheduled ";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);
            }

            if (actionAvailable == true)
            {
                // Loop through all test sessions and show the summary.
                for (int rowIndex = 0; rowIndex < testSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow testSessionDetailRow = testSessionDetailsTable.Rows[rowIndex];

                    // Scheduled candidates count.
                    if (showLinks && (hasTestSchedulingRights || hasTestSesssionCreationRights))
                    {
                        HyperLink scheduledValueLink = new HyperLink();
                        scheduledValueLink.Text = GetIntegerValue(testSessionDetailRow["SCHEDULED_COUNT"]).ToString();
                        scheduledValueLink.ToolTip = "Click here to view scheduled candidates";
                        scheduledValueLink.NavigateUrl = "~/Scheduler/ScheduleCandidate.aspx?m=2&s=1&index=3&testsessionid=" + GetValue(testSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        scheduledValueLink.Target = "_blank";
    
                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            scheduledValueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(scheduledValueLink);
                    }
                    else
                    {
                        Label scheduledValueLabel = new Label();
                        scheduledValueLabel.Text = GetIntegerValue(testSessionDetailRow["SCHEDULED_COUNT"]).ToString();

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            scheduledValueLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(scheduledValueLabel);
                    }

                    Label scheduledCaptionLabel = new Label();
                    scheduledCaptionLabel.Text = " candidate(s) scheduled & ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        scheduledCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(scheduledCaptionLabel);

                    // Completed candidates count.
                    if (showLinks && (hasTestSchedulingRights || hasTestSesssionCreationRights))
                    {
                        HyperLink completedValueLink = new HyperLink();
                        completedValueLink.Text = GetIntegerValue(testSessionDetailRow["COMPLETED_COUNT"]).ToString();
                        completedValueLink.ToolTip = "Click here to view completed candidates";
                        completedValueLink.NavigateUrl = "~/Scheduler/ScheduleCandidate.aspx?m=2&s=1&index=3&testsessionid=" + GetValue(testSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        completedValueLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            completedValueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(completedValueLink);
                    }
                    else
                    {
                        Label completedValueLabel = new Label();
                        completedValueLabel.Text = GetIntegerValue(testSessionDetailRow["COMPLETED_COUNT"]).ToString();

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            completedValueLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(completedValueLabel);
                    }

                    Label completedCaptionLabel = new Label();
                    completedCaptionLabel.Text = " candidate(s) completed for session ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        completedCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(completedCaptionLabel);

                    // Completed candidates count.
                    if (showLinks && (hasTestSchedulingRights || hasTestSesssionCreationRights))
                    {
                        HyperLink scheduleLink = new HyperLink();
                        scheduleLink.Text = GetValue(testSessionDetailRow["SESSION_KEY"]).ToString();
                        scheduleLink.ToolTip = "Click here to schedule candidates";
                        scheduleLink.NavigateUrl = "~/Scheduler/ScheduleCandidate.aspx?m=2&s=1&index=3&testsessionid=" + GetValue(testSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        scheduleLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            scheduleLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(scheduleLink);
                    }
                    else
                    {
                        Label completedValueLabel = new Label();
                        completedValueLabel.Text = GetValue(testSessionDetailRow["SESSION_KEY"]);

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            completedValueLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(completedValueLabel);
                    }

                    if (rowIndex + 1 < testSessionDetailsTable.Rows.Count)
                    {
                        // Add a comma.
                        valueCell.Controls.Add(GetCommaControl());
                    }
                }
                valueCell.Controls.Add(GetSpaceControl());
            }
           
            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the test schedule required summary row.
        /// </summary>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="interviewSessionDetailsTable">
        /// A <see cref="DataTable"/> that holds the test session details table.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetInterviewScheduleRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable interviewSessionDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (interviewSessionDetailsTable != null && interviewSessionDetailsTable.Rows != null && interviewSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;
            valueRow.Cells.Add(valueCell);

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Interview Scheduling: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Interview not scheduled ";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);
            }

            if (actionAvailable == true)
            {
                // Loop through all test sessions and show the summary.
                for (int rowIndex = 0; rowIndex < interviewSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow interviewSessionDetailRow = interviewSessionDetailsTable.Rows[rowIndex];

                    // Scheduled candidates count.
                    if (showLinks && (hasInterviewSchedulingRights || hasInterviewSesssionCreationRights))
                    {
                        HyperLink scheduledValueLink = new HyperLink();
                        scheduledValueLink.Text = GetIntegerValue(interviewSessionDetailRow["SCHEDULED_COUNT"]).ToString();
                        scheduledValueLink.ToolTip = "Click here to view scheduled candidates";
                        scheduledValueLink.NavigateUrl = "~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&index=3&interviewsessionid=" + GetValue(interviewSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        scheduledValueLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            scheduledValueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(scheduledValueLink);
                    }
                    else
                    {
                        Label scheduledValueLabel = new Label();
                        scheduledValueLabel.Text = GetIntegerValue(interviewSessionDetailRow["SCHEDULED_COUNT"]).ToString();

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            scheduledValueLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(scheduledValueLabel);
                    }

                    Label scheduledCaptionLabel = new Label();
                    scheduledCaptionLabel.Text = " candidate(s) scheduled & ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        scheduledCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(scheduledCaptionLabel);

                    // Completed candidates count.
                    if (showLinks && (hasInterviewSchedulingRights || hasInterviewSesssionCreationRights))
                    {
                        HyperLink completedValueLink = new HyperLink();
                        completedValueLink.Text = GetIntegerValue(interviewSessionDetailRow["COMPLETED_COUNT"]).ToString();
                        completedValueLink.ToolTip = "Click here to view completed candidates";
                        completedValueLink.NavigateUrl = "~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&index=3&interviewsessionid=" + GetValue(interviewSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        completedValueLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            completedValueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(completedValueLink);
                    }
                    else
                    {
                        Label completedValueLabel = new Label();
                        completedValueLabel.Text = GetIntegerValue(interviewSessionDetailRow["COMPLETED_COUNT"]).ToString();

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            completedValueLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(completedValueLabel);
                    }

                    Label completedCaptionLabel = new Label();
                    completedCaptionLabel.Text = " candidate(s) completed for session ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        completedCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(completedCaptionLabel);

                    // Completed candidates count.
                    if (showLinks && (hasInterviewSchedulingRights || hasInterviewSesssionCreationRights))
                    {
                        HyperLink testScheduleLink = new HyperLink();
                        testScheduleLink.Text = GetValue(interviewSessionDetailRow["SESSION_KEY"]);
                        testScheduleLink.ToolTip = "Click here to schedule candidates";
                        testScheduleLink.NavigateUrl = "~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&index=3&interviewsessionid=" + GetValue(interviewSessionDetailRow["SESSION_KEY"]) +
                             "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        testScheduleLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            testScheduleLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(testScheduleLink);
                    }
                    else
                    {
                        Label completedValueLabel = new Label();
                        completedValueLabel.Text = GetValue(interviewSessionDetailRow["SESSION_KEY"]);

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            completedValueLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(completedValueLabel);
                    }

                    if (rowIndex + 1 < interviewSessionDetailsTable.Rows.Count)
                    {
                        // Add a comma.
                        valueCell.Controls.Add(GetCommaControl());
                    }
                }
                valueCell.Controls.Add(GetSpaceControl());
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the test session required summary row.
        /// </summary>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="interviewSessionDetailsTable">
        /// A <see cref="DataTable"/> that holds the test session details table.
        /// </param>
        /// <param name="interviewDetailsTable">
        /// A <see cref="DataTable"/> that holds the test details table.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetInterviewSessionRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable interviewSessionDetailsTable, DataTable interviewDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (interviewSessionDetailsTable != null && interviewSessionDetailsTable.Rows != null && interviewSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;
            valueRow.Cells.Add(valueCell);

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Interview Session Creation: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Interview session not created. ";

                if (interviewDetailsTable == null || interviewDetailsTable.Rows == null || interviewDetailsTable.Rows.Count == 0)
                {
                    captionLabel.Text = captionLabel.Text.Trim();
                    captionLabel.Text += " Interview needs to be associated before creating interview session. ";
                }

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);
            }

            if (actionAvailable == true)
            {
                // Add caption label.
                Label preCaptionLabel = new Label();

                if (interviewSessionDetailsTable.Rows.Count == 1)
                    preCaptionLabel.Text = "1 interview session";
                else
                    preCaptionLabel.Text = string.Format("{0} interview sessions", interviewSessionDetailsTable.Rows.Count);

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    preCaptionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(preCaptionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Loop through the table and add the test sessions..
                for (int rowIndex = 0; rowIndex < interviewSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow interviewSessionDetailRow = interviewSessionDetailsTable.Rows[rowIndex];

                    if (showLinks && (hasInterviewSchedulingRights || hasInterviewSesssionCreationRights))
                    {
                        // Add caption link.
                        HyperLink captionLink = new HyperLink();
                        captionLink.Text = string.Format("{0}({1})", GetValue(interviewSessionDetailRow["SESSION_KEY"]), GetValue(interviewSessionDetailRow["SESSION_COUNT"]));

                        captionLink.ToolTip = "Click here to view interview sessions";
                        captionLink.NavigateUrl = "~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=2&s=1&index=3&interviewsessionid=" + GetValue(interviewSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        captionLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            captionLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(captionLink);
                    }
                    else
                    {
                        // Add count label.
                        Label countLabel = new Label();
                        countLabel.Text = string.Format("{0}({1})",
                            GetValue(interviewSessionDetailRow["SESSION_KEY"]), GetValue(interviewSessionDetailRow["SESSION_COUNT"]));

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            countLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(countLabel);
                    }

                    if (rowIndex + 1 < interviewSessionDetailsTable.Rows.Count)
                    {
                        // Add a comma.
                        valueCell.Controls.Add(GetCommaControl());
                    }
                }

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = " created.";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add link
                if (showLinks == true && hasInterviewSesssionCreationRights)
                {
                    // Add caption label.
                    Label preEditCaptionLabel = new Label();

                    if (interviewSessionDetailsTable.Rows.Count == 1)
                        preEditCaptionLabel.Text = "To edit interview session click ";
                    else
                        preEditCaptionLabel.Text = "To edit interview sessions click ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        preEditCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(preEditCaptionLabel);

                    // Loop through the table and add the edit test session link
                    for (int rowIndex = 0; rowIndex < interviewSessionDetailsTable.Rows.Count; rowIndex++)
                    {
                        DataRow testSessionDetailRow = interviewSessionDetailsTable.Rows[rowIndex];

                        HyperLink valueLink = new HyperLink();
                        valueLink.Text = GetValue(testSessionDetailRow["SESSION_KEY"]);
                        valueLink.ToolTip = "Click here to edit interview session";
                        valueLink.NavigateUrl = "~/InterviewTestMaker/EditInterviewTestSession.aspx?m=1&s=3&testsessionid=" + GetValue(testSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        valueLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            valueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(valueLink);

                        if (rowIndex + 1 < interviewSessionDetailsTable.Rows.Count)
                        {
                            // Add a comma.
                            valueCell.Controls.Add(GetCommaControl());
                        }
                    }

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }
            }

            // Add link to create test sessions.
            if (interviewDetailsTable != null && interviewDetailsTable.Rows != null && interviewDetailsTable.Rows.Count > 0)
            {
                if (showLinks == true && hasInterviewSesssionCreationRights)
                {
                    // Add caption label.
                    Label preCreateSessionCaptionLabel = new Label();
                    preCreateSessionCaptionLabel.Text = "To create interview session click ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        preCreateSessionCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(preCreateSessionCaptionLabel);

                    // Loop through the table and add the create session link
                    for (int rowIndex = 0; rowIndex < interviewDetailsTable.Rows.Count; rowIndex++)
                    {
                        DataRow interviewDetailRow = interviewDetailsTable.Rows[rowIndex];

                        HyperLink valueLink = new HyperLink();
                        valueLink.Text = GetValue(interviewDetailRow["INTERVIEW_NAME"]);
                        valueLink.ToolTip = "Click here to create interview session";
                        valueLink.NavigateUrl = "~/InterviewTestMaker/CreateInterviewTestSession.aspx?m=1&s=0&positionprofileid=" + positionProfileID + "&interviewtestkey=" +
                            GetValue(interviewDetailRow["INTERVIEW_KEY"]) + "&parentpage=PP_REVIEW";
                        valueLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            valueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(valueLink);

                        if (rowIndex + 1 < interviewDetailsTable.Rows.Count)
                        {
                            // Add a comma.
                            valueCell.Controls.Add(GetCommaControl());
                        }
                    }

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the test required summary row.
        /// </summary>
        /// <param name="actionAvailable">
        /// A <see cref="bool"/> that holds the action available status.
        /// </param>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="interviewDetailsTable">
        /// A <see cref="DataTable"/> that holds the test details table.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetInterviewRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable interviewDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (interviewDetailsTable != null && interviewDetailsTable.Rows != null && interviewDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;
            valueRow.Cells.Add(valueCell);

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Interview Creation: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Interview not created.";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);
            }
            else if (actionAvailable == true)
            {
                // Add caption label.
                Label preCaptionLabel = new Label();

                if (interviewDetailsTable.Rows.Count == 1)
                    preCaptionLabel.Text = "1 interview";
                else
                    preCaptionLabel.Text = string.Format("{0} interviews", interviewDetailsTable.Rows.Count);

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    preCaptionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(preCaptionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Loop through the table and add the test names.
                for (int rowIndex = 0; rowIndex < interviewDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow interviewDetailRow = interviewDetailsTable.Rows[rowIndex];

                    if (showLinks && hasInterviewCreationRights)
                    {
                        HyperLink captionLink = new HyperLink();
                        captionLink.Text = GetValue(interviewDetailRow["INTERVIEW_NAME"]);
                        captionLink.ToolTip = "Click here to view interview";
                        captionLink.NavigateUrl = "~/InterviewTestMaker/ViewInterviewTest.aspx?m=1&s=2&testkey=" + GetValue(interviewDetailRow["INTERVIEW_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        captionLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            captionLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(captionLink);
                    }
                    else
                    {
                        // Add test name label.
                        Label testNameLabel = new Label();
                        testNameLabel.Text = GetValue(interviewDetailRow["INTERVIEW_NAME"]);

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            testNameLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(testNameLabel);
                    }

                    if (rowIndex + 1 < interviewDetailsTable.Rows.Count)
                    {
                        // Add a comma.
                        valueCell.Controls.Add(GetCommaControl());
                    }
                }

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add caption label.
                Label postCaptionLabel = new Label();
                postCaptionLabel.Text = "created.";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    postCaptionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(postCaptionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());
            }

            // Show create interview link always.
            if (showLinks == true && hasInterviewCreationRights)
            {
                // Add create interview label.
                Label createInterviewLabel = new Label();
                createInterviewLabel.Text = "To create interview click ";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    createInterviewLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(createInterviewLabel);

                HyperLink createInterviewLink = new HyperLink();
                createInterviewLink.Text = "Create Interview";
                createInterviewLink.ToolTip = "Click here to create interview";
                createInterviewLink.NavigateUrl = "~/InterviewTestMaker/CreateAutomaticInterviewTest.aspx?m=1&s=0&positionprofileid=" +
                    positionProfileID + "&parentpage=PP_REVIEW";
                createInterviewLink.Target = "_blank";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                {
                    createInterviewLink.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(createInterviewLink);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the candidate association summary row.
        /// </summary>
        /// <param name="actionAvailable">
        /// A <see cref="bool"/> that holds the action available status.
        /// </param>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetCandidateAssociationSummaryRow(bool actionAvailable, bool taskOwnerAvailable,
            DataRow dataSummaryRow)
        {
            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Candidate Association: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Candidate not associated ";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);

                // Add link
                if (showLinks && (hasCandidateAssociationRights || hasRecruiterRights))
                {
                    HyperLink valueLink = new HyperLink();
                    valueLink.Text = "Associate Candidates";
                    valueLink.ToolTip = "Click here to associate candidates using search candidate utility";
                    valueLink.NavigateUrl = "~/ResumeRepository/SearchCandidateRepository.aspx?m=0&s=1&positionprofileid=" +
                        positionProfileID + "&parentpage=PP_REVIEW";
                    valueLink.Target = "_blank";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        valueLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(valueLink);

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());

                    HyperLink valueIntelliSPOTLink = new HyperLink();
                    valueIntelliSPOTLink.Text = "Associate Candidates (intelliSPOT)";
                    valueIntelliSPOTLink.ToolTip = "Click here to associate candidates using intelliSPOT utlity";
                    valueIntelliSPOTLink.NavigateUrl = "~/TalentScoutHome.aspx?positionprofileid=" +
                        positionProfileID + "&parentpage=PP_REVIEW";
                    valueIntelliSPOTLink.Target = "_blank";
                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        valueIntelliSPOTLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(valueIntelliSPOTLink);

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }

                valueRow.Cells.Add(valueCell);
            }
            else if (actionAvailable == true)
            {
                // Anybody can view associated candidates. Validation will be done at status page.
                if (showLinks)
                {
                    // Add caption link
                    HyperLink countLink = new HyperLink();
                    countLink.Text = GetValue(dataSummaryRow["ASSOCIATED_CANDIDATES_COUNT"]);
                    countLink.ToolTip = "Click here to view associated candidates";
                    countLink.NavigateUrl = "~/PositionProfile/PositionProfileStatus.aspx?positionprofileid=" +
                            positionProfileID + "&m=1&s=1&parentpage=PP_REVIEW";
                    countLink.Target = "_blank";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        countLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(countLink);
                }
                else
                {
                    // Add count label.
                    Label countLabel = new Label();
                    countLabel.Text = GetValue(dataSummaryRow["ASSOCIATED_CANDIDATES_COUNT"]);

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                    {
                        countLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(countLabel);
                }

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "candidate(s) associated";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add link
                if (showLinks && (hasCandidateAssociationRights || hasRecruiterRights))
                {
                    HyperLink valueLink = new HyperLink();
                    valueLink.Text = "Associate Candidates";
                    valueLink.ToolTip = "Click here to associate candidate using search candidate utility";
                    valueLink.NavigateUrl = "~/ResumeRepository/SearchCandidateRepository.aspx?m=0&s=1&positionprofileid=" +
                        positionProfileID + "&parentpage=PP_REVIEW";
                    valueLink.Target = "_blank";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        valueLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(valueLink);

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());

                    HyperLink valueIntelliSPOTLink = new HyperLink();
                    valueIntelliSPOTLink.Text = "Associate Candidates (intelliSPOT)";
                    valueIntelliSPOTLink.ToolTip = "Click here to associate candidate using intelliSPOT utlity";
                    valueIntelliSPOTLink.NavigateUrl = "~/TalentScoutHome.aspx?positionprofileid=" +
                        positionProfileID + "&parentpage=PP_REVIEW";
                    valueIntelliSPOTLink.Target = "_blank";
                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        valueIntelliSPOTLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(valueIntelliSPOTLink);

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }

                valueRow.Cells.Add(valueCell);
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the recruiter assignment summary row.
        /// </summary>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="recruitersTable">
        /// A <see cref="DataTable"/> that holds the recruiters table.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetRecruiterAssignmentSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable recruitersTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (recruitersTable != null && recruitersTable.Rows != null && recruitersTable.Rows.Count > 0)
                actionAvailable = true;

            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Recruiter Assignment: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Recruiter not assigned ";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);

                // Add link
                if (showLinks && hasRecruiterAssignmentRights)
                {
                    LinkButton assignRecruiterLink = new LinkButton();
                    assignRecruiterLink.Text = "Assign Recruiters";
                    assignRecruiterLink.ToolTip = "Click here to assign recruiters";
                    assignRecruiterLink.PostBackUrl = "~/PositionProfile/PositionProfileRecruiterAssignment.aspx?positionprofileid=" +
                            positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        assignRecruiterLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(assignRecruiterLink);

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }

                valueRow.Cells.Add(valueCell);
            }
            else if (actionAvailable == true)
            {
                if (showLinks && hasRecruiterAssignmentRights)
                {
                    // Add count link.
                    LinkButton countLink = new LinkButton();
                    countLink.Text = recruitersTable.Rows.Count.ToString();
                    countLink.ToolTip = "Click here to view/assign recruiters";
                    countLink.PostBackUrl = "~/PositionProfile/PositionProfileRecruiterAssignment.aspx?positionprofileid=" +
                            positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        countLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(countLink);
                }
                else
                {
                    // Add count label.
                    Label countLabel = new Label();
                    countLabel.Text = recruitersTable.Rows.Count.ToString();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                    {
                        countLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(countLabel);
                }

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "recruiter(s) assigned";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add link
                if (showLinks && hasRecruiterAssignmentRights)
                {
                    LinkButton assignRecruiterLink = new LinkButton();
                    assignRecruiterLink.Text = "Change/Assign Recruiters";
                    assignRecruiterLink.ToolTip = "Click here to change or assign recruiters";
                    assignRecruiterLink.PostBackUrl = "~/PositionProfile/PositionProfileRecruiterAssignment.aspx?positionprofileid=" +
                            positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        assignRecruiterLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(assignRecruiterLink);

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }

                valueRow.Cells.Add(valueCell);
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner
            
            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the candidate submitted summary row.
        /// </summary>
        /// <param name="actionAvailable">
        /// A <see cref="bool"/> that holds the action available status.
        /// </param>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetCandidateSubmittedSummaryRow(bool actionAvailable, bool taskOwnerAvailable,
            DataRow dataSummaryRow)
        {
            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Submittal To Client: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "No candidates submitted";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add link
                if (showLinks == true && hasSubmittalToClientRights)
                {
                    HyperLink submitCandidateLink = new HyperLink();
                    submitCandidateLink.Text = "Submit Candidate";
                    submitCandidateLink.ToolTip = "Click here to submit candidates";
                    submitCandidateLink.NavigateUrl = "~/PositionProfile/PositionProfileStatus.aspx?positionprofileid=" +
                        positionProfileID + "&m=1&s=1&parentpage=PP_REVIEW";
                    submitCandidateLink.Target = "_blank";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        submitCandidateLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(submitCandidateLink);

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }

                valueRow.Cells.Add(valueCell);
            }
            else if (actionAvailable == true)
            {
                if (showLinks && hasSubmittalToClientRights)
                {
                    // Add caption link
                    HyperLink captionLink = new HyperLink();
                    captionLink.Text = GetValue(dataSummaryRow["SUBMITTED_CANDIDATES_COUNT"]);
                    captionLink.ToolTip = "Click here to view submitted candidates";
                    captionLink.NavigateUrl = "~/PositionProfile/PositionProfileStatus.aspx?positionprofileid=" +
                            positionProfileID + "&m=1&s=1&type=S&parentpage=PP_REVIEW";
                    captionLink.Target = "_blank";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        captionLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(captionLink);
                }
                else
                {
                    // Add completed label.
                    Label valueLabel = new Label();
                    valueLabel.Text = GetValue(dataSummaryRow["SUBMITTED_CANDIDATES_COUNT"]);

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                    {
                        valueLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(valueLabel);
                }

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "candidate(s) submitted";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);

                // Add a space.
                valueCell.Controls.Add(GetSpaceControl());

                // Add link
                if (showLinks == true && hasSubmittalToClientRights)
                {
                    HyperLink submitCandidateLink = new HyperLink();
                    submitCandidateLink.Text = "Submit Candidate";
                    submitCandidateLink.ToolTip = "Click here to submit candidates";
                    submitCandidateLink.NavigateUrl = "~/PositionProfile/PositionProfileStatus.aspx?positionprofileid=" +
                        positionProfileID + "&m=1&s=1&parentpage=PP_REVIEW";
                    submitCandidateLink.Target = "_blank";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        submitCandidateLink.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(submitCandidateLink);

                    // Add a space.
                    valueCell.Controls.Add(GetSpaceControl());
                }

                valueRow.Cells.Add(valueCell);
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        /// <summary>
        /// Method that retrieves the interview assessment summary row.
        /// </summary>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="interviewSessionDetailsTable">
        /// A <see cref="DataTable"/> that holds the test session details table.
        /// </param>
        /// <returns>
        /// A <see cref="TableRow"/> that holds the html row.
        /// </returns>
        private TableRow GetInterviewAssessmentSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable interviewSessionDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (interviewSessionDetailsTable != null && interviewSessionDetailsTable.Rows != null && interviewSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            TableRow valueRow = new TableRow();
            TableCell valueCell = new TableCell();
            valueCell.VerticalAlign = VerticalAlign.Middle;
            valueCell.HorizontalAlign = HorizontalAlign.Left;
            valueRow.Cells.Add(valueCell);

            // Get title label.
            valueCell.Controls.Add(GetWorkflowTitleLabel("Interview Assessment: "));

            #region Action Data

            if (actionAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Interview not assessed ";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                valueCell.Controls.Add(captionLabel);
            }

            if (actionAvailable == true)
            {
                // Loop through all test sessions and show the summary.
                for (int rowIndex = 0; rowIndex < interviewSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow interviewSessionDetailRow = interviewSessionDetailsTable.Rows[rowIndex];

                    // Assessed candidates count.
                    if (showLinks)
                    {
                        LinkButton assessedValueLink = new LinkButton();
                        assessedValueLink.Text = GetIntegerValue(interviewSessionDetailRow["ASSESSED_COUNT"]).ToString();
                        assessedValueLink.ToolTip = "Click here to view assessed candidates";

                        assessedValueLink.Attributes.Add("onclick", "javascript:return ShowViewAssessedCandidates('"
                            + positionProfileID + "','" + GetValue(interviewSessionDetailRow["SESSION_KEY"])  + "');");

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            assessedValueLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(assessedValueLink);
                    }
                    else
                    {
                        Label assessedValueLabel = new Label();
                        assessedValueLabel.Text = GetIntegerValue(interviewSessionDetailRow["ASSESSED_COUNT"]).ToString();

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            assessedValueLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(assessedValueLabel);
                    }

                    Label completedCaptionLabel = new Label();
                    completedCaptionLabel.Text = " candidate(s) assessed for session ";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                    {
                        completedCaptionLabel.Style.Add(style.Key, style.Value);
                    }

                    valueCell.Controls.Add(completedCaptionLabel);

                    // Interview session.
                    if (showLinks && (hasInterviewSesssionCreationRights || hasInterviewSchedulingRights))
                    {
                        HyperLink testSessionLink = new HyperLink();
                        testSessionLink.Text = GetValue(interviewSessionDetailRow["SESSION_KEY"]);
                        testSessionLink.ToolTip = "Click here to view interview session";
                        testSessionLink.NavigateUrl = "~/InterviewScheduler/InterviewScheduleCandidate.aspx?m=1&s=3&interviewsessionid=" + GetValue(interviewSessionDetailRow["SESSION_KEY"]) +
                            "&positionprofileid=" + positionProfileID + "&parentpage=PP_REVIEW";
                        testSessionLink.Target = "_blank";

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                        {
                            testSessionLink.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(testSessionLink);
                    }
                    else
                    {
                        Label completedValueLabel = new Label();
                        completedValueLabel.Text = GetValue(interviewSessionDetailRow["SESSION_KEY"]);

                        // Apply style.
                        foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
                        {
                            completedValueLabel.Style.Add(style.Key, style.Value);
                        }

                        valueCell.Controls.Add(completedValueLabel);
                    }

                    if (rowIndex + 1 < interviewSessionDetailsTable.Rows.Count)
                    {
                        // Add a comma.
                        valueCell.Controls.Add(GetCommaControl());
                    }
                }
            }

            #endregion Action Data

            #region Task Owner

            // Add task owner controls.
            foreach (Control control in GetTaskOwnerControls(taskOwnerAvailable))
                valueCell.Controls.Add(control);

            #endregion Task Owner

            return valueRow;
        }

        #endregion Workflow Summary

        #region Position Segment                                               

        /// <summary>
        /// Method that constructs and returns the position segment details
        /// html table.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment data as XML string.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the position segment details html
        /// table.
        /// </returns>
        private Table GetPositionSegmentTable(string xml)
        {
            Table table = new Table();

            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            TableRow[] rows = new TableRow[2];

            // Add position segment title.
            rows[0] = new TableRow();
            TableCell titleCell = new TableCell();
            titleCell.Controls.Add(GetSegmentTitleCell("Position Details"));
            rows[0].Cells.Add(titleCell);

            // Add position segment data.
            rows[1] = new TableRow();
            rows[1].Cells.Add(GetPositionSegmentDataCell(xml));

            table.Rows.AddRange(rows);

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the position segment data html cell.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment xml data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the position segment data html
        /// cell.
        /// </returns>
        private TableCell GetPositionSegmentDataCell(string xml)
        {
            string targetedStartDate = string.Empty;
            string noOfPositions = string.Empty;
            string natureOfPosition = string.Empty;

            if (!Utility.IsNullOrEmpty(xml))
            {
                try
                {
                    // Get details from xml.
                    XmlDocument document = new XmlDocument();
                    document.LoadXml(xml);

                    targetedStartDate = GetDateValue(document.SelectSingleNode("ClientPositionDetails//TargetedStartDate").InnerText);
                    noOfPositions = GetValue(document.SelectSingleNode("ClientPositionDetails//NumberOfPositions").InnerText);
                    natureOfPosition = GetValue(document.SelectSingleNode("ClientPositionDetails//NatureOfPosition").InnerText);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }

            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // No of position caption cell.
            TableCell noOfPositionCaptionCell = new TableCell();
            noOfPositionCaptionCell.Width = new Unit(15, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                noOfPositionCaptionCell.Style.Add(style.Key, style.Value);
            }

            noOfPositionCaptionCell.Text = "No. Of Positions";
            noOfPositionCaptionCell.VerticalAlign = VerticalAlign.Middle;
            noOfPositionCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(noOfPositionCaptionCell);

            // No of position value cell.
            TableCell noOfPositionValueCell = new TableCell();
            noOfPositionValueCell.Width = new Unit(5, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                noOfPositionValueCell.Style.Add(style.Key, style.Value);
            }

            noOfPositionValueCell.Text = noOfPositions;
            noOfPositionValueCell.VerticalAlign = VerticalAlign.Middle;
            noOfPositionValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(noOfPositionValueCell);

            // Targeted start date caption cell.
            TableCell targetedStartDateCaptionCell = new TableCell();
            targetedStartDateCaptionCell.Width = new Unit(18, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                targetedStartDateCaptionCell.Style.Add(style.Key, style.Value);
            }

            targetedStartDateCaptionCell.Text = "Targeted Start Date";
            targetedStartDateCaptionCell.VerticalAlign = VerticalAlign.Middle;
            targetedStartDateCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(targetedStartDateCaptionCell);

            //  Targeted start date value cell.
            TableCell targetedStartDateValueCell = new TableCell();
            targetedStartDateValueCell.Width = new Unit(12, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                targetedStartDateValueCell.Style.Add(style.Key, style.Value);
            }

            targetedStartDateValueCell.Text = targetedStartDate;
            targetedStartDateValueCell.VerticalAlign = VerticalAlign.Middle;
            targetedStartDateValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(targetedStartDateValueCell);

            // Nature of position caption cell.
            TableCell natureOfPositionCaptionCell = new TableCell();
            natureOfPositionCaptionCell.Width = new Unit(15, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                natureOfPositionCaptionCell.Style.Add(style.Key, style.Value);
            }

            natureOfPositionCaptionCell.Text = "Nature Of Position";
            natureOfPositionCaptionCell.VerticalAlign = VerticalAlign.Middle;
            natureOfPositionCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(natureOfPositionCaptionCell);

            // Nature of position value cell.
            TableCell natureOfPositionValueCell = new TableCell();
            natureOfPositionValueCell.Width = new Unit(35, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                natureOfPositionValueCell.Style.Add(style.Key, style.Value);
            }

            string natureOfPositions = string.Empty;

            if (!Utility.IsNullOrEmpty(natureOfPosition))
            {
                // Loop through the comma separated level and construct the names.
                foreach (string level in natureOfPosition.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    switch (level)
                    {
                        case "1":
                            natureOfPositions = natureOfPositions + ", " + "Contract";
                            break;

                        case "2":
                            natureOfPositions = natureOfPositions + ", " + "Direct Hire";
                            break;

                        case "3":
                            natureOfPositions = natureOfPositions + ", " + "Contract To Hire";
                            break;
                    }
                }
            }

            // Trim the education levels.
            natureOfPositions = natureOfPositions.Trim();
            natureOfPositions = natureOfPositions.Trim(new char[] { ',' });
            natureOfPositions = natureOfPositions.Trim();

            natureOfPositionValueCell.Text = natureOfPositions;
            natureOfPositionValueCell.VerticalAlign = VerticalAlign.Middle;
            natureOfPositionValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(natureOfPositionValueCell);

            // Add table to the cell.
            cell.Controls.Add(table);

            return cell;
        }

        #endregion Position Segment

        #region Roles Segment                                                  

        /// <summary>
        /// Method that constructs and returns the roles segment details table.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment data as XML string.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the roles segment details table.
        /// </returns>
        private Table GetRolesSegmentTable(string xml)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            TableRow[] rows = new TableRow[2];

            // Add position segment title.
            rows[0] = new TableRow();
            TableCell titleCell = new TableCell();
            titleCell.Controls.Add(GetSegmentTitleCell("Roles"));
            rows[0].Cells.Add(titleCell);

            // Add position segment data.
            rows[1] = new TableRow();
            rows[1].Cells.Add(GetRolesSegmentDataCell(xml));

            table.Rows.AddRange(rows);

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the roles segment data cell.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment xml data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the roles segment data cell.
        /// </returns>
        private TableCell GetRolesSegmentDataCell(string xml)
        {
            string primaryRole = string.Empty;
            string secondaryRole = string.Empty;

            if (!Utility.IsNullOrEmpty(xml))
            {
                try
                {
                    // Get details from xml.
                    XmlDocument document = new XmlDocument();
                    document.LoadXml(xml);

                    primaryRole = GetValue(document.SelectSingleNode("RoleRequirementDetails//PrimaryRole").InnerText);
                    secondaryRole = GetValue(document.SelectSingleNode("RoleRequirementDetails//SecondaryRole").InnerText);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }

            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Primary role caption cell.
            TableCell primaryRoleCaptionCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                primaryRoleCaptionCell.Style.Add(style.Key, style.Value);
            }

            primaryRoleCaptionCell.Text = "Primary Role";
            primaryRoleCaptionCell.VerticalAlign = VerticalAlign.Middle;
            primaryRoleCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(primaryRoleCaptionCell);

            // Primary role value cell.
            TableCell primaryRoleValueCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                primaryRoleValueCell.Style.Add(style.Key, style.Value);
            }

            primaryRoleValueCell.Text = primaryRole;
            primaryRoleValueCell.VerticalAlign = VerticalAlign.Middle;
            primaryRoleValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(primaryRoleValueCell);

            // Secondary role caption cell.
            TableCell secondaryRoleCaptionCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                secondaryRoleCaptionCell.Style.Add(style.Key, style.Value);
            }

            secondaryRoleCaptionCell.Text = "Secondary Role";
            secondaryRoleCaptionCell.VerticalAlign = VerticalAlign.Middle;
            secondaryRoleCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(secondaryRoleCaptionCell);

            // Secondary role value cell.
            TableCell secondaryRoleValueCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                secondaryRoleValueCell.Style.Add(style.Key, style.Value);
            }

            secondaryRoleValueCell.Text = secondaryRole;
            secondaryRoleValueCell.VerticalAlign = VerticalAlign.Middle;
            secondaryRoleValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(secondaryRoleValueCell);

            // Add table to the cell.
            cell.Controls.Add(table);

            return cell;
        }

        #endregion Roles Segment

        #region Technical Skills Segment                                       

        /// <summary>
        /// Method that constructs and returns the technical skills details table.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment data as XML string.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the technical skills details html
        /// table.
        /// </returns>
        private Table GetTechnicalSkillsSegmentTable(string xml)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            // Add technical skills segment title.
            TableRow titleRow = new TableRow();
            table.Rows.Add(titleRow);

            Table titleTable = GetSegmentTitleCell("Technical Skills");
            TableCell titleCell = new TableCell();
            titleCell.Controls.Add(titleTable);
            titleRow.Cells.Add(titleCell);

            // Add position segment data.
            TableRow detailRow = new TableRow();
            table.Rows.Add(detailRow);

            Table detailTable = GetTechnicalSkillsSegmentDataTable(xml);
            TableCell detailCell = new TableCell();
            detailCell.Controls.Add(detailTable);
            detailRow.Cells.Add(detailCell);

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the technical skills segment 
        /// data table.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment xml data.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the technical skills segment data
        /// html table.
        /// </returns>
        private Table GetTechnicalSkillsSegmentDataTable(string xml)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            XmlDocument document = null;
            XmlNodeList technicalSkillRows = null;

            if (!Utility.IsNullOrEmpty(xml))
            {
                try
                {
                    // Get details from xml.
                    document = new XmlDocument();
                    document.LoadXml(xml);

                    technicalSkillRows = document.SelectNodes
                        ("TechnicalSkillDetails/TechnicalSkillDetail");
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }

            // Add technical skill total years.
            TableRow totalYearsRow = new TableRow();
            Table totalYearsTable = GetTechnicalSkillsTotalYearsTable(document);
            totalYearsTable.Width = new Unit(100, UnitType.Percentage);
            TableCell totalYearsCell = new TableCell();
            totalYearsCell.Controls.Add(totalYearsTable);
            totalYearsRow.Cells.Add(totalYearsCell);
            table.Rows.Add(totalYearsRow);

            // Add technical skill grid columns.
            TableRow gridHeaderRow = new TableRow();
            Table gridHeaderTable = GetTechnicalSkillsGridHeaderTable();
            gridHeaderTable.Width = new Unit(100, UnitType.Percentage);
            TableCell gridHeaderCell = new TableCell();
            gridHeaderCell.Width = new Unit(100, UnitType.Percentage);
            gridHeaderCell.Controls.Add(gridHeaderTable);
            gridHeaderRow.Cells.Add(gridHeaderCell);
            table.Rows.Add(gridHeaderRow);

            Table rowTable = null;
            
            if (technicalSkillRows != null)
            {
                rowTable = new Table();
                rowTable.Width = new Unit(100, UnitType.Percentage);

                for (int nodeIndex = 0; nodeIndex < technicalSkillRows.Count; nodeIndex++)
                {
                    TableRow rowRow = new TableRow();

                    // Skill category/name column cell.
                    TableCell skillCategoryNameColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        skillCategoryNameColumnCell.Style.Add(style.Key, style.Value);
                    }

                    skillCategoryNameColumnCell.Text = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("CategoryName").InnerText) + " - " +
                        GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("SkillName").InnerText);
                    skillCategoryNameColumnCell.VerticalAlign = VerticalAlign.Middle;
                    skillCategoryNameColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    skillCategoryNameColumnCell.Width = new Unit(20, UnitType.Percentage);
                    rowRow.Cells.Add(skillCategoryNameColumnCell);

                    // Years required column cell.
                    TableCell yearsRequiredColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        yearsRequiredColumnCell.Style.Add(style.Key, style.Value);
                    }

                    yearsRequiredColumnCell.Text = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("NoOfYearsExp").InnerText);
                    yearsRequiredColumnCell.VerticalAlign = VerticalAlign.Middle;
                    yearsRequiredColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    yearsRequiredColumnCell.Width = new Unit(20, UnitType.Percentage);
                    rowRow.Cells.Add(yearsRequiredColumnCell);

                    // Skill level column cell.
                    TableCell skillLevelColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        skillLevelColumnCell.Style.Add(style.Key, style.Value);
                    }

                    skillLevelColumnCell.Text = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("SkillRequired").InnerText);
                    skillLevelColumnCell.VerticalAlign = VerticalAlign.Middle;
                    skillLevelColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    skillLevelColumnCell.Width = new Unit(20, UnitType.Percentage);
                    rowRow.Cells.Add(skillLevelColumnCell);

                    // Mandatory column cell.
                    TableCell mandatoryColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        mandatoryColumnCell.Style.Add(style.Key, style.Value);
                    }

                    mandatoryColumnCell.Text = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("IsMandatory").InnerText);
                    mandatoryColumnCell.VerticalAlign = VerticalAlign.Middle;
                    mandatoryColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    mandatoryColumnCell.Width = new Unit(20, UnitType.Percentage);
                    rowRow.Cells.Add(mandatoryColumnCell);

                    // Weightage column cell.
                    TableCell weightageColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        weightageColumnCell.Style.Add(style.Key, style.Value);
                    }

                    weightageColumnCell.Text = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("Weightage").InnerText);
                    if (!Utility.IsNullOrEmpty(weightageColumnCell.Text))
                        weightageColumnCell.Text = weightageColumnCell.Text + " %";

                    weightageColumnCell.VerticalAlign = VerticalAlign.Middle;
                    weightageColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    mandatoryColumnCell.Width = new Unit(20, UnitType.Percentage);
                    rowRow.Cells.Add(weightageColumnCell);

                    rowTable.Rows.Add(rowRow);
                }
            }

            if (rowTable != null)
            {
                TableRow lastRow = new TableRow();
                TableCell rowCell = new TableCell();
                rowCell.Controls.Add(rowTable);
                lastRow.Cells.Add(rowCell);
                table.Rows.Add(lastRow);
            }

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the technical skills total 
        /// years table.
        /// </summary>
        /// <param name="document">
        /// A <see cref="XmlDocument"/> that holds the segment data XML 
        /// document.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the technical skills total years
        /// html table.
        /// </returns>
        private Table GetTechnicalSkillsTotalYearsTable(XmlDocument document)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            string totalYears = string.Empty;

            if (document != null)
            {
                try
                {
                    // Get details from xml.
                    totalYears = document.SelectSingleNode("TechnicalSkillDetails//TotalITExperience").InnerText;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }

            // Add total years row.
            TableRow totalYearsRow = new TableRow();
            table.Rows.Add(totalYearsRow);

            // Total years caption cell.
            TableCell totalYearsCaptionCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                totalYearsCaptionCell.Style.Add(style.Key, style.Value);
            }

            totalYearsCaptionCell.Text = "Total Years Of IT Experience Required";
            totalYearsCaptionCell.VerticalAlign = VerticalAlign.Middle;
            totalYearsCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            totalYearsCaptionCell.Width = new Unit(30, UnitType.Percentage);
            totalYearsRow.Cells.Add(totalYearsCaptionCell);

            // Total years value cell.
            TableCell totalYearsValueCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                totalYearsValueCell.Style.Add(style.Key, style.Value);
            }

            totalYearsValueCell.Text = GetValue(totalYears);
            totalYearsValueCell.VerticalAlign = VerticalAlign.Middle;
            totalYearsValueCell.HorizontalAlign = HorizontalAlign.Left;
            totalYearsRow.Cells.Add(totalYearsValueCell);

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the technical skills grid header
        /// html table.
        /// </summary>
        /// <returns>
        /// A <see cref="Table"/> that holds the technical skills grid header
        /// html table.
        /// </returns>
        private Table GetTechnicalSkillsGridHeaderTable()
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            // Add grid header row.
            TableRow gridHeaderRow = new TableRow();
            table.Rows.Add(gridHeaderRow);

            // Skill category/name column cell.
            TableCell skillCategoryNameColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                skillCategoryNameColumnCell.Style.Add(style.Key, style.Value);
            }

            skillCategoryNameColumnCell.Text = "Skill Category/Name";
            skillCategoryNameColumnCell.VerticalAlign = VerticalAlign.Middle;
            skillCategoryNameColumnCell.HorizontalAlign = HorizontalAlign.Left;
            skillCategoryNameColumnCell.Width = new Unit(20, UnitType.Percentage);
            gridHeaderRow.Cells.Add(skillCategoryNameColumnCell);

            // Years required column cell.
            TableCell yearsRequiredColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                yearsRequiredColumnCell.Style.Add(style.Key, style.Value);
            }

            yearsRequiredColumnCell.Text = "Years Required";
            yearsRequiredColumnCell.VerticalAlign = VerticalAlign.Middle;
            yearsRequiredColumnCell.HorizontalAlign = HorizontalAlign.Left;
            yearsRequiredColumnCell.Width = new Unit(20, UnitType.Percentage);
            gridHeaderRow.Cells.Add(yearsRequiredColumnCell);

            // Skill level column cell.
            TableCell skillLevelColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                skillLevelColumnCell.Style.Add(style.Key, style.Value);
            }

            skillLevelColumnCell.Text = "Skill Level";
            skillLevelColumnCell.VerticalAlign = VerticalAlign.Middle;
            skillLevelColumnCell.HorizontalAlign = HorizontalAlign.Left;
            skillLevelColumnCell.Width = new Unit(20, UnitType.Percentage);
            gridHeaderRow.Cells.Add(skillLevelColumnCell);

            // Mandatory column cell.
            TableCell mandatoryColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                mandatoryColumnCell.Style.Add(style.Key, style.Value);
            }

            mandatoryColumnCell.Text = "Mandatory / Desired";
            mandatoryColumnCell.VerticalAlign = VerticalAlign.Middle;
            mandatoryColumnCell.HorizontalAlign = HorizontalAlign.Left;
            mandatoryColumnCell.Width = new Unit(20, UnitType.Percentage);
            gridHeaderRow.Cells.Add(mandatoryColumnCell);

            // Weightage column cell.
            TableCell weightageColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                weightageColumnCell.Style.Add(style.Key, style.Value);
            }

            weightageColumnCell.Text = "Weightage";
            weightageColumnCell.VerticalAlign = VerticalAlign.Middle;
            weightageColumnCell.HorizontalAlign = HorizontalAlign.Left;
            weightageColumnCell.Width = new Unit(20, UnitType.Percentage);
            gridHeaderRow.Cells.Add(weightageColumnCell);

            return table;
        }

        #endregion Technical Skills Segment

        #region Vertical Background Segment                                    

        /// <summary>
        /// Method that constructs and returns the vertical background details table.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment data as XML string.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the vertical background details 
        /// html table.
        /// </returns>
        private Table GetVerticalBackgroundSegmentTable(string xml)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            // Add technical skills segment title.
            TableRow titleRow = new TableRow();
            table.Rows.Add(titleRow);

            Table titleTable = GetSegmentTitleCell("Vertical Background Requirement");
            TableCell titleCell = new TableCell();
            titleCell.Controls.Add(titleTable);
            titleRow.Cells.Add(titleCell);

            // Add vertical background segment data.
            TableRow detailRow = new TableRow();
            table.Rows.Add(detailRow);

            Table detailTable = GetVerticalBackgroundSegmentDataTable(xml);
            TableCell detailCell = new TableCell();
            detailCell.Controls.Add(detailTable);
            detailRow.Cells.Add(detailCell);

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the vertical background segment
        /// data table.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment data as XML string.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the vertical background segment 
        /// data html table.
        /// </returns>
        private Table GetVerticalBackgroundSegmentDataTable(string xml)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            XmlDocument document = null;
            XmlNodeList verticalsRows = null;

            if (!Utility.IsNullOrEmpty(xml))
            {
                try
                {
                    // Get details from xml.
                    document = new XmlDocument();
                    document.LoadXml(xml);

                    verticalsRows = document.SelectNodes
                       ("VerticalDetails/VerticalDetail");
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }

            // Add vertical background grid columns.
            TableRow gridHeaderRow = new TableRow();
            Table gridHeaderTable = GetVerticalBackgroundGridHeaderTable();
            gridHeaderTable.Width = new Unit(100, UnitType.Percentage);
            TableCell gridHeaderCell = new TableCell();
            gridHeaderCell.Width = new Unit(100, UnitType.Percentage);
            gridHeaderCell.Controls.Add(gridHeaderTable);
            gridHeaderRow.Cells.Add(gridHeaderCell);
            table.Rows.Add(gridHeaderRow);

            Table rowTable = null;

            if (verticalsRows != null)
            {
                rowTable = new Table();
                rowTable.Width = new Unit(100, UnitType.Percentage);

                for (int nodeIndex = 0; nodeIndex < verticalsRows.Count; nodeIndex++)
                {
                    TableRow rowRow = new TableRow();

                    // Vertical column cell.
                    TableCell skillCategoryNameColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        skillCategoryNameColumnCell.Style.Add(style.Key, style.Value);
                    }

                    skillCategoryNameColumnCell.Text = GetValue(verticalsRows.Item(nodeIndex).SelectSingleNode("VerticalSegment").InnerText) + " - " +
                        GetValue(verticalsRows.Item(nodeIndex).SelectSingleNode("SubVerticalSegment").InnerText);
                    skillCategoryNameColumnCell.VerticalAlign = VerticalAlign.Middle;
                    skillCategoryNameColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    skillCategoryNameColumnCell.Width = new Unit(25, UnitType.Percentage);
                    rowRow.Cells.Add(skillCategoryNameColumnCell);

                    // Years required column cell.
                    TableCell yearsRequiredColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        yearsRequiredColumnCell.Style.Add(style.Key, style.Value);
                    }

                    yearsRequiredColumnCell.Text = GetValue(verticalsRows.Item(nodeIndex).SelectSingleNode("NoOfYearsExperience").InnerText);
                    yearsRequiredColumnCell.VerticalAlign = VerticalAlign.Middle;
                    yearsRequiredColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    yearsRequiredColumnCell.Width = new Unit(25, UnitType.Percentage);
                    rowRow.Cells.Add(yearsRequiredColumnCell);

                    // Mandatory column cell.
                    TableCell mandatoryColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        mandatoryColumnCell.Style.Add(style.Key, style.Value);
                    }

                    mandatoryColumnCell.Text = GetValue(verticalsRows.Item(nodeIndex).SelectSingleNode("IsMandatory").InnerText);
                    mandatoryColumnCell.VerticalAlign = VerticalAlign.Middle;
                    mandatoryColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    mandatoryColumnCell.Width = new Unit(25, UnitType.Percentage);
                    rowRow.Cells.Add(mandatoryColumnCell);

                    // Weightage column cell.
                    TableCell weightageColumnCell = new TableCell();

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_row"))
                    {
                        weightageColumnCell.Style.Add(style.Key, style.Value);
                    }

                    weightageColumnCell.Text = GetValue(verticalsRows.Item(nodeIndex).SelectSingleNode("Weightage").InnerText);
                    if (!Utility.IsNullOrEmpty(weightageColumnCell.Text))
                        weightageColumnCell.Text = weightageColumnCell.Text + " %";

                    weightageColumnCell.VerticalAlign = VerticalAlign.Middle;
                    weightageColumnCell.HorizontalAlign = HorizontalAlign.Left;
                    mandatoryColumnCell.Width = new Unit(25, UnitType.Percentage);
                    rowRow.Cells.Add(weightageColumnCell);

                    rowTable.Rows.Add(rowRow);
                }
            }

            if (rowTable != null)
            {
                TableRow lastRow = new TableRow();
                TableCell rowCell = new TableCell();
                rowCell.Controls.Add(rowTable);
                lastRow.Cells.Add(rowCell);
                table.Rows.Add(lastRow);
            }

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the vertical background grid 
        /// header html table.
        /// </summary>
        /// <returns>
        /// A <see cref="Table"/> that holds the vertical background  grid 
        /// header html table.
        /// </returns>
        private Table GetVerticalBackgroundGridHeaderTable()
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            // Add grid header row.
            TableRow gridHeaderRow = new TableRow();
            table.Rows.Add(gridHeaderRow);

            // Vertical column cell.
            TableCell verticalColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                verticalColumnCell.Style.Add(style.Key, style.Value);
            }

            verticalColumnCell.Text = "Vertical / Sub Vertical";
            verticalColumnCell.VerticalAlign = VerticalAlign.Middle;
            verticalColumnCell.HorizontalAlign = HorizontalAlign.Left;
            verticalColumnCell.Width = new Unit(25, UnitType.Percentage);
            gridHeaderRow.Cells.Add(verticalColumnCell);

            // Years required column cell.
            TableCell yearsRequiredColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                yearsRequiredColumnCell.Style.Add(style.Key, style.Value);
            }

            yearsRequiredColumnCell.Text = "Years Required";
            yearsRequiredColumnCell.VerticalAlign = VerticalAlign.Middle;
            yearsRequiredColumnCell.HorizontalAlign = HorizontalAlign.Left;
            yearsRequiredColumnCell.Width = new Unit(25, UnitType.Percentage);
            gridHeaderRow.Cells.Add(yearsRequiredColumnCell);

            // Mandatory column cell.
            TableCell mandatoryColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                mandatoryColumnCell.Style.Add(style.Key, style.Value);
            }

            mandatoryColumnCell.Text = "Mandatory / Desired";
            mandatoryColumnCell.VerticalAlign = VerticalAlign.Middle;
            mandatoryColumnCell.HorizontalAlign = HorizontalAlign.Left;
            mandatoryColumnCell.Width = new Unit(25, UnitType.Percentage);
            gridHeaderRow.Cells.Add(mandatoryColumnCell);

            // Weightage column cell.
            TableCell weightageColumnCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_grid_column"))
            {
                weightageColumnCell.Style.Add(style.Key, style.Value);
            }

            weightageColumnCell.Text = "Weightage";
            weightageColumnCell.VerticalAlign = VerticalAlign.Middle;
            weightageColumnCell.HorizontalAlign = HorizontalAlign.Left;
            weightageColumnCell.Width = new Unit(25, UnitType.Percentage);
            gridHeaderRow.Cells.Add(weightageColumnCell);

            return table;
        }

        #endregion Vertical Background Segment

        #region Education Segment                                              

        /// <summary>
        /// Method that constructs and returns the education segment details 
        /// html table.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment data as XML string.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the education segment details 
        /// html table.
        /// </returns>
        private Table GetEducationSegmentTable(string xml)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);

            TableRow[] rows = new TableRow[2];

            // Add position segment title.
            rows[0] = new TableRow();
            TableCell titleCell = new TableCell();
            titleCell.Controls.Add(GetSegmentTitleCell("Education"));
            rows[0].Cells.Add(titleCell);

            // Add position segment data.
            rows[1] = new TableRow();
            rows[1].Cells.Add(GetEducationSegmentDataCell(xml));

            table.Rows.AddRange(rows);

            return table;
        }

        /// <summary>
        /// Method that constructs and returns the education segment data html
        /// cell.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the segment xml data.
        /// </param>
        /// <returns>
        /// A <see cref="TableCell"/> that holds the education segment data 
        /// html  cell.
        /// </returns>
        private TableCell GetEducationSegmentDataCell(string xml)
        {
            string educationLevel = string.Empty;
            string specialization = string.Empty;

            if (!Utility.IsNullOrEmpty(xml))
            {
                try
                {
                    // Get details from xml.
                    XmlDocument document = new XmlDocument();
                    document.LoadXml(xml);

                    educationLevel = GetValue(document.SelectSingleNode("EducationalDetails//EducationLevel").InnerText);
                    specialization = GetValue(document.SelectSingleNode("EducationalDetails//Specialization").InnerText);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }
            }

            TableCell cell = new TableCell();
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Education level caption cell.
            TableCell educationLevelCaptionCell = new TableCell();
            educationLevelCaptionCell.Width = new Unit(25, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                educationLevelCaptionCell.Style.Add(style.Key, style.Value);
            }

            educationLevelCaptionCell.Text = "Minimum Level Of Education Required";
            educationLevelCaptionCell.VerticalAlign = VerticalAlign.Middle;
            educationLevelCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(educationLevelCaptionCell);

            // Education level value cell.
            TableCell educationLevelValueCell = new TableCell();
            educationLevelValueCell.Width = new Unit(35, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                educationLevelValueCell.Style.Add(style.Key, style.Value);
            }

            string educationLevels = string.Empty;

            if (!Utility.IsNullOrEmpty(educationLevel))
            {
                // Loop through the comma separated level and construct the names.
                foreach (string level in educationLevel.Split(new char[]{ ','}, StringSplitOptions.RemoveEmptyEntries))
                {
                    switch (level)
                    {
                        case "1":
                            educationLevels = educationLevels + ", " + "Masters";
                            break;

                        case "2":
                            educationLevels = educationLevels + ", " + "Bachelors";
                            break;

                        case "3":
                            educationLevels = educationLevels + ", " + "Associate";
                            break;

                        case "4":
                            educationLevels = educationLevels + ", " + "Diploma";
                            break;
                    }
                }
            }

            // Trim the education levels.
            educationLevels = educationLevels.Trim();
            educationLevels = educationLevels.Trim(new char[] {','});
            educationLevels = educationLevels.Trim();

            educationLevelValueCell.Text = educationLevels;
            educationLevelValueCell.VerticalAlign = VerticalAlign.Middle;
            educationLevelValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(educationLevelValueCell);

            // Specialization caption cell.
            TableCell specializationCaptionCell = new TableCell();
            specializationCaptionCell.Width = new Unit(15, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_caption_cell"))
            {
                specializationCaptionCell.Style.Add(style.Key, style.Value);
            }

            specializationCaptionCell.Text = "Specialization";
            specializationCaptionCell.VerticalAlign = VerticalAlign.Middle;
            specializationCaptionCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(specializationCaptionCell);

            // Specialization value cell.
            TableCell specializationValueCell = new TableCell();
            specializationValueCell.Width = new Unit(25, UnitType.Percentage);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_value_cell"))
            {
                specializationValueCell.Style.Add(style.Key, style.Value);
            }

            specializationValueCell.Text = specialization;
            specializationValueCell.VerticalAlign = VerticalAlign.Middle;
            specializationValueCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(specializationValueCell);

            // Add table to the cell.
            cell.Controls.Add(table);

            return cell;
        }

        #endregion Education Segment

        #region General Methods                                                

        /// <summary>
        /// Method that retrives the important values and status from the raw data.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        private void RetrieveValues(DataSet rawData)
        {
            // Check for validity of raw data.
            if (rawData == null || rawData.Tables.Count == 0)
                throw new Exception("Raw data cannot be null");

            if (rawData.Tables.Count != TABLES_COUNT)
                throw new Exception("Raw data is invalid");

            // Retrieve has owner rights status.
            if (rawData.Tables[HEADER_DETAIL_TABLE_INDEX].Rows == null || rawData.Tables[HEADER_DETAIL_TABLE_INDEX].Rows.Count == 0)
                throw new Exception("Raw data is invalid");

            // Retreive 0th row.
            DataRow dataRow = rawData.Tables[HEADER_DETAIL_TABLE_INDEX].Rows[0];

            // Retrieve position profile ID.
            positionProfileID = GetIntegerValue(dataRow["POSITION_PROFILE_ID"]);

            hasOwnerRights = GetValue(dataRow["HAS_OWNER_RIGHTS"]) == "Y" ? true : false;

            hasRecruiterRights = GetValue(dataRow["HAS_RECRUITER_RIGHTS"]) == "Y" ? true : false;

            hasRecruiterAssignmentRights = GetValue(dataRow["HAS_RECRUITER_ASSIGNMENT_RIGHTS"]) == "Y" ? true : false;

            hasCandidateAssociationRights = GetValue(dataRow["HAS_CANDIDATE_ASSOCIATION_RIGHTS"]) == "Y" ? true : false;

            hasInterviewCreationRights = GetValue(dataRow["HAS_INTERVIEW_CREATION_RIGHTS"]) == "Y" ? true : false;

            hasInterviewSesssionCreationRights = GetValue(dataRow["HAS_INTERVIEW_SESSSION_CREATION_RIGHTS"]) == "Y" ? true : false;

            hasInterviewSchedulingRights = GetValue(dataRow["HAS_INTERVIEW_SCHEDULING_RIGHTS"]) == "Y" ? true : false;

            hasTestCreationRights = GetValue(dataRow["HAS_TEST_CREATION_RIGHTS"]) == "Y" ? true : false;

            hasTestSesssionCreationRights = GetValue(dataRow["HAS_TEST_SESSSION_CREATION_RIGHTS"]) == "Y" ? true : false;

            hasTestSchedulingRights = GetValue(dataRow["HAS_TEST_SCHEDULING_RIGHTS"]) == "Y" ? true : false;

            hasSubmittalToClientRights = GetValue(dataRow["HAS_SUBMITTAL_TO_CLIENT_RIGHTS"]) == "Y" ? true : false; 
        }

        /// <summary>
        /// Method that constructs and returns the segment title html table.
        /// </summary>
        /// <param name="title">
        /// A <see cref="string"/> that holds the title.
        /// </param>
        /// <returns>
        /// A <see cref="Table"/> that holds the segment title html table.
        /// </returns>
        private Table GetSegmentTitleCell(string title)
        {
            Table table = new Table();
            table.BorderStyle = BorderStyle.None;
            table.Width = new Unit(100, UnitType.Percentage);
            TableRow row = new TableRow();
            table.Rows.Add(row);

            // Additional info cell.
            TableCell additionalInfoCell = new TableCell();

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_name"))
            {
                additionalInfoCell.Style.Add(style.Key, style.Value);
            }
           
            additionalInfoCell.Text = title;
            additionalInfoCell.VerticalAlign = VerticalAlign.Middle;
            additionalInfoCell.HorizontalAlign = HorizontalAlign.Left;
            row.Cells.Add(additionalInfoCell);

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_segment_border"))
            {
                table.Style.Add(style.Key, style.Value);
            }

            return table;
        }

        /// <summary>
        /// Method that retrieves the task owner controls.
        /// </summary>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <returns>
        /// A list <see cref="Control"/> that holds the task owner controls.
        /// </returns>
        private List<Control> GetTaskOwnerControls(bool taskOwnerAvailable)
        {
            List<Control> controls = new List<Control>();

            if (taskOwnerAvailable == false)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Only default task owner assigned ";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                controls.Add(captionLabel);

                // Add link
                if (showLinks && hasOwnerRights)
                {
                    LinkButton valueLink = new LinkButton();
                    valueLink.Text = "Assign Task Owner";
                    valueLink.ToolTip = "Click here to assign task owner";
                    valueLink.PostBackUrl = "~/PositionProfile/PositionProfileWorkflowSelection.aspx?positionprofileid=" +
                        positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        valueLink.Style.Add(style.Key, style.Value);
                    }

                    controls.Add(valueLink);
                }
            }
            else if (taskOwnerAvailable == true)
            {
                // Add caption label.
                Label captionLabel = new Label();
                captionLabel.Text = "Task owner assigned.";

                // Apply style.
                foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_cell"))
                {
                    captionLabel.Style.Add(style.Key, style.Value);
                }

                controls.Add(captionLabel);

                // Add link
                if (showLinks && hasOwnerRights)
                {
                    LinkButton valueLink = new LinkButton();
                    valueLink.Text = "Change Task Owner";
                    valueLink.ToolTip = "Click here to change task owner";
                    valueLink.PostBackUrl = "~/PositionProfile/PositionProfileWorkflowSelection.aspx?positionprofileid=" +
                        positionProfileID + "&m=1&s=0&parentpage=PP_REVIEW";

                    // Apply style.
                    foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_edit_link"))
                    {
                        valueLink.Style.Add(style.Key, style.Value);
                    }

                    controls.Add(valueLink);
                }
            }

            return controls;
        }

        /// <summary>
        /// Method that retrieves the non null value from an object.
        /// </summary>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the non null value. If data is
        /// null then it returns empty string.
        /// </returns>
        private string GetValue(object data)
        {
            if (data == null || data == DBNull.Value)
                return string.Empty;

            return data.ToString().Trim();
        }

        /// <summary>
        /// Method that retrieves the non null date value from an object.
        /// </summary>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the non null data value. If data is
        /// null then it returns empty string.
        /// </returns>
        private string GetDateValue(object data)
        {
            if (data == null || data == DBNull.Value)
                return string.Empty;

            DateTime date = DateTime.MinValue;

            if (DateTime.TryParse(data.ToString(), out date))
            {
                return date.ToString("MM/dd/yyyy").Trim();
            }

            return string.Empty;
        }

        /// <summary>
        /// Method that retrieves the non null integer value from an object.
        /// </summary>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the non null integer value. If data 
        /// is null then it returns 0;
        /// </returns>
        private int GetIntegerValue(object data)
        {
            if (data == null || data == DBNull.Value)
                return 0;

            return Convert.ToInt32(data.ToString().Trim());
        }

        /// <summary>
        /// Method that returns a literal control with one space in it.
        /// </summary>
        /// <returns>
        /// A <see cref="Label"/> that holds the literal control.
        /// </returns>
        /// <remarks>
        /// This is used to add space between html text.
        /// </remarks>
        private Literal GetSpaceControl()
        {
            Literal spaceControl = new Literal();
            spaceControl.Text = " ";

            return spaceControl;
        }

        /// <summary>
        /// Method that returns a label control with comma in it.
        /// </summary>
        /// <returns>
        /// A <see cref="Label"/> that holds the label control.
        /// </returns>
        /// <remarks>
        /// This is used to add a comma between multiple action items.
        /// </remarks>
        private Label GetCommaControl()
        {
            Label commaControl = new Label();
            commaControl.Text = ", ";

            // Apply style.
            foreach (KeyValuePair<string, string> style in GetStyles("position_profile_summary_workflow_value_orange_cell"))
            {
                commaControl.Style.Add(style.Key, style.Value);
            }

            return commaControl;
        }

        /// <summary>
        /// Method that constructs and returns the styles for the given style key.
        /// </summary>
        /// <param name="styleKey">
        /// A <see cref="string"/> that holds the style key.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the style key and value.
        /// </returns>
        private List<KeyValuePair<string, string>> GetStyles(string styleKey)
        {
            List<KeyValuePair<string, string>> styles = new List<KeyValuePair<string, string>>();

            switch (styleKey)
            {
                case "position_profile_summary_table":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "11px !important"));
                    styles.Add(new KeyValuePair<string, string>("color", "#33424B !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "none"));
                    break;

                case "position_profile_summary_profile_name":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "15px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "900"));
                    styles.Add(new KeyValuePair<string, string>("color", "#FE5206 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_profile_edit_link":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "11px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("color", "#FE5206 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "underline"));
                    break;

                case "position_profile_summary_segment_name":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "14px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "bold"));
                    styles.Add(new KeyValuePair<string, string>("color", "#267BB1 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_caption_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("color", "#000000 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "none"));
                    break;

                case "position_profile_summary_status_caption_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "13px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("color", "#000000 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "none"));
                    break;

                case "position_profile_summary_status_value_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "13px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "400"));
                    styles.Add(new KeyValuePair<string, string>("color", "#267BB1 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "none"));
                    break;

                case "position_profile_summary_caption_bold_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "bolder"));
                    styles.Add(new KeyValuePair<string, string>("color", "#000000 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "none"));
                    break;

                case "position_profile_summary_value_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("color", "#6E6E6E !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "none"));
                    break;

                case "position_profile_summary_value_missing_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "10px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("color", "#FC4355 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "none"));
                    break;

                case "position_profile_summary_value_blue_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("color", "#267BB1 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "none"));
                    break;

                case "position_profile_summary_value_bold_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "bolder"));
                    styles.Add(new KeyValuePair<string, string>("color", "#6E6E6E !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_actions_caption_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "14px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "bold"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("color", "#267BB1 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_actions_value_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("color", "#6E6E6E !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_actions_value_orange_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("color", "#FE5206 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_segment_grid_row":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("color", "#6E6E6E !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_segment_grid_column":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("color", "#267BB1 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_workflow_caption_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "14px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "bold"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("color", "#267BB1 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_workflow_value_title_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "bolder"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("color", "#6E6E6E !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_workflow_value_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("color", "#6E6E6E !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_workflow_value_orange_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "12px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("color", "#FE5206 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;

                case "position_profile_summary_workflow_edit_link":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "11px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal !important"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("font-bold", "false"));
                    styles.Add(new KeyValuePair<string, string>("color", "#FE5206 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "underline"));
                    break;

                case "position_profile_summary_status_edit_link":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "11px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal !important"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "normal"));
                    styles.Add(new KeyValuePair<string, string>("font-bold", "false"));
                    styles.Add(new KeyValuePair<string, string>("color", "#FE5206 !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    styles.Add(new KeyValuePair<string, string>("text-decoration", "underline"));
                    break;

                case "position_profile_summary_status_background":
                    styles.Add(new KeyValuePair<string, string>("background-color", "#D5DEE3"));
                    break;

                case "position_profile_summary_header_background":
                    styles.Add(new KeyValuePair<string, string>("background-color", "#E9EDF0"));
                    break;

                case "position_profile_summary_segments_background":
                    styles.Add(new KeyValuePair<string, string>("background-color", "#FFFFFF"));
                    break;

                case "position_profile_summary_actions_background":
                    styles.Add(new KeyValuePair<string, string>("background-color", "#D5DEE3"));
                    break;

                case "position_profile_summary_workflow_background":
                    styles.Add(new KeyValuePair<string, string>("background-color", "#E9EDF0"));
                    break;

                case "position_profile_summary_segment_border":
                    styles.Add(new KeyValuePair<string, string>("border-bottom", "1px solid #D8D8D8"));
                    break;

                case "position_profile_summary_action_missing_background":
                    styles.Add(new KeyValuePair<string, string>("background-color", "#FBDECD"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #74787A"));
                    break;

                case "position_profile_summary_action_missing_legend_cell":
                    styles.Add(new KeyValuePair<string, string>("background-color", "#FBDECD"));
                    styles.Add(new KeyValuePair<string, string>("color", "#FBDECD"));
                    styles.Add(new KeyValuePair<string, string>("border", "1px solid #74787A"));
                    break;

                case "position_profile_summary_legend_caption_cell":
                    styles.Add(new KeyValuePair<string, string>("font-family", "Arial,sans-serif !important"));
                    styles.Add(new KeyValuePair<string, string>("font-size", "11px !important"));
                    styles.Add(new KeyValuePair<string, string>("font-weight", "normal"));
                    styles.Add(new KeyValuePair<string, string>("font-style", "italic"));
                    styles.Add(new KeyValuePair<string, string>("color", "#6E6E6E !important"));
                    styles.Add(new KeyValuePair<string, string>("border", "0px none #D8D8D8"));
                    break;
            }

            return styles;
        }

        #endregion General Methods

        #endregion Private Methods
    }
}