﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileParser.cs
// File that represents the position profile parser object that will helps to
// parse the position profile data from the position profile requirement given
// as a formatted/non-formatted text. This text is taken from a source file 
// which can be of any one of the following type: excel/word/pdf/txt/eml.

#endregion Header

#region Directives

using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Represents the class that helps to parse the position profile data from
    /// the position profile requirement given as a formatted/non-formatted 
    /// text. This text is taken from a source file which can be of any one
    /// of the following type: excel/word/pdf/txt/eml.
    /// </summary>
    public class PositionProfileParser
    {
        public PositionProfileDetail GetParsedPositionProfile(string rawData)
        {
            return null;
        }

        private void BuildIndexes()
        {

        }
    }
}

