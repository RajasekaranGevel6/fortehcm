﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExcelBatchReader.cs
// File that represents the excel reader object that will helps to read
// data from the excel file. The utility also helps to retrieve the list
// of sheets present in the excel file, the count of rows, etc.

#endregion Header

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System.IO;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Represents the class that helps to read data from the excel file.
    /// The utility also helps to retrieve the list of sheets present in 
    /// the excel file, the count of rows, etc
    /// </summary>
    public class ClientExcelBatchReader:ExcelBatchReader 
    {
        #region Public Methods

        /// <summary>
        /// Represents the constructor.
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the excel file name.
        /// </param>
        public ClientExcelBatchReader(string fileName)
            : base(fileName)
        {
            
        }


        /// <summary>
        /// Represents the method that returns the list of question for the 
        /// given page number and page size.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the questions.
        /// </returns>
        public List<ClientInformation> GetClients(int pageNumber, int pageSize,
            string userName, string sheetName, int userID,string fileName)
        {
            // List of clients to store the questions
            List<ClientInformation> clients = new List<ClientInformation>();

            try
            {
                Department dept = new Department();
                dept.ClientID = 1;
                dept.DepartmentID = 1;
                dept.DepartmentName = "DepartmentName1";
                
                dept.FaxNumber = "234324";
               
                
                ContactInformation cont = new ContactInformation();
                cont.City = "city";
                cont.Country = "country";
                cont.EmailAddress = "email";

                dept.ContactInformation = cont;
                PhoneNumber phone = new PhoneNumber();
                phone.Mobile = "3432432";
                cont.Phone = phone;

                ClientContactInformation clientCont = new ClientContactInformation();
                clientCont.FirstName = "name1";
                clientCont.LastName = "lastname1";
                clientCont.MiddleName = "MiddleName1";
                clientCont.ContactID = 1;
                clientCont.ContactInformation.StreetAddress= "contact address1";
                
                clientCont.ContactInformation.Phone.Office= "343141#";

                dept.ClientContactInformations.Add(clientCont);

                ClientInformation client = new ClientInformation();
                client.ClientName = "ClientName";
                client.ContactInformation = cont;
                client.Departments.Add(dept);
                clients.Add(client);
                return clients;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }
        }     
        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Method that is used to check the choices of the questions.
        /// It checks whether the choice is not empty and whether the choice is 
        /// the answer of the question.Then it add the choice to the choice list
        /// </summary>
        /// <param name="choice">
        /// A<see cref="string"/>holds the choice of question
        /// </param>
        /// <param name="answer">
        /// A<see cref="string"/>holds the correct answer
        /// </param>
        /// <param name="answerChoices">
        /// A<see cref="List<AnswerChoice>"/>holds the answer
        /// choice of the question
        /// </param>
        private void CheckAnswerChoices(string choice, string answer,
            List<AnswerChoice> answerChoices, int choiceID)
        {
            //if choice is null then return
            if (choice.Trim().Length == 0)
                return;
            ////check whether the answer is mentioned in excel or not
            //if (answer.Length == 0)
            //{
            //    answerChoices.Add(new AnswerChoice
            //   (choice, answerChoices.Count + 1, false));
            //    return;
            //}
            //else add the choice to the answer choice list
            answerChoices.Add(new AnswerChoice
                (choice, answerChoices.Count + 1,
                choiceID == int.Parse(answer.Substring(answer.Length - 1, 1))));
        }

        /// <summary>
        /// Method that is used check the attribute of the question such as category, 
        /// subject, testarea , complexity .It checks whether the attrbutes entered 
        /// correct and if it so it will get back the corresponding ID of the attributes
        /// </summary>
        /// <param name="questionDetails">
        /// A<see cref="List<QuestionDetails>"/>list of questions
        /// </param>
        /// <returns>
        /// list of questions that contain the collected id of the attributes
        /// </returns>
        private List<QuestionDetail> CheckQuestionAttribute(List<QuestionDetail> questionDetails, int tenantID)
        {
            QuestionBLManager questionBLManager = new QuestionBLManager();
            foreach (QuestionDetail question in questionDetails)
            {
                questionBLManager.CheckQuestionAttribute(question, tenantID);

                AnswerChoice answer = question.AnswerChoices.Find(delegate
                    (AnswerChoice answerChoice)
                    {
                        return answerChoice.IsCorrect == true;
                    });

                if (answer == null)
                {
                    question.IsValid = false;
                }
                if (question.HasImage && question.QuestionImage == null)
                {
                    question.IsValid = false;
                }
            }
            return questionDetails;
        }

        #endregion Private Methods
    }

    internal class ClientExcelConstants
    {
        /// <summary>
        /// A <see cref="int"/> that holds the
        /// column number of category
        /// </summary>
        public const int CATEGORY = 0;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of subject
        /// </summary>
        public const int SUBJECT = 1;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of test area 
        /// </summary>
        public const int TESTAREA = 2;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of question
        /// </summary>
        public const int QUESTION = 3;

        /// <summary>
        /// A<see cref="int"/>that holds the
        /// column number of choice 1
        /// </summary>
        public const int CHOICE_1 = 4;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of choice 2
        /// </summary>
        public const int CHOICE_2 = 5;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of choice 3
        /// </summary>
        public const int CHOICE_3 = 6;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of choice 4
        /// </summary>
        public const int CHOICE_4 = 7;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of correct answer
        /// </summary>
        public const int CORRECT_ANSWER = 8;

        /// <summary>
        /// A<see cref="int"/>that holds the
        /// column number of complexity
        /// </summary>
        public const int COMPLEXITY = 9;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of tag
        /// </summary>
        public const int TAG = 10;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of tag
        /// </summary>
        public const int IMAGE = 11;
    }
}