﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// HRXMLManager.cs
// File that represents the component for retrive resume information
// from HRXml.  

#endregion Header

#region Directives                                                             

using System.Xml;

using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.Utilities
{
    public class HRXMLManager
    {
        #region Private Variables                                              

        private string xml;
        private Resume resume = null;

        #endregion Private Variables

        #region Private Methods                                                
       
        /// <summary>
        /// This method helps to extract the hrxml data contents.
        /// </summary>
        /// <param name="oXmlDoc">
        /// A <see cref="XmlDocument"/> that holds the xml document.
        /// </param>
        private void Process(XmlDocument oXmlDoc)
        {
            resume = new Resume();            

            XmlNodeList oPersonNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/PersonName");
            if (oPersonNodeList.Count > 0)
            {
                Name oName = new Name();
                XmlNodeList oPersonFirstNameNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/FirstName");
                XmlNodeList oPersonMiddleNameNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/MiddleName");
                XmlNodeList oPersonLastNameNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/LastName");
                if (oPersonFirstNameNodeList.Count > 0)
                {
                    oName.FirstName = oPersonFirstNameNodeList.Item(0).InnerText;                  
                }
                if (oPersonMiddleNameNodeList.Count > 0)
                {
                    oName.MiddleName = oPersonMiddleNameNodeList.Item(0).InnerText;
                }
                if (oPersonLastNameNodeList.Count > 0)
                {
                    oName.LastName = oPersonLastNameNodeList.Item(0).InnerText;
                }
                resume.Name = oName;
            }

            XmlNodeList oContactInfoNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod");
            if (oContactInfoNodeList.Count > 0)
            {
                ContactInformation oContactInfo = new ContactInformation();
                PhoneNumber oPhoneNo = new PhoneNumber();
                WebSite oWebSite = new WebSite();

                XmlNodeList oContactInfoTelephoneNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/Telephone");
                XmlNodeList oContactInfoFixedLineNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/Fax");
                XmlNodeList oContactInfoEmailNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/InternetEmailAddress");
                XmlNodeList oContactInfoWebSiteNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/InternetWebAddress");
                XmlNodeList oContactInfoCountryCodeNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress/CountryCode");
                XmlNodeList oContactInfoPostalCodeNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress/PostalCode");
                XmlNodeList oContactInfoCityNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress/Municipality");
                XmlNodeList oContactInfoAddressNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress/Street");
                XmlNodeList oContactInfoStateNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ContactInfo/ContactMethod/PostalAddress/State");

                oContactInfo.City = oContactInfoCityNodeList.Item(0).InnerText;
                oContactInfo.Country = oContactInfoCountryCodeNodeList.Item(0).InnerText;
                oContactInfo.PostalCode = oContactInfoPostalCodeNodeList.Item(0).InnerText;
                oContactInfo.State=oContactInfoStateNodeList.Item(0).InnerText;               
                oContactInfo.StreetAddress = oContactInfoAddressNodeList.Item(0).InnerText;
                oContactInfo.EmailAddress = oContactInfoEmailNodeList.Item(0).InnerText;

                oPhoneNo.Mobile = oContactInfoTelephoneNodeList.Item(0).InnerText;
                oPhoneNo.Office = oContactInfoFixedLineNodeList.Item(0).InnerText;
                oPhoneNo.Residence = oContactInfoFixedLineNodeList.Item(0).InnerText;

                oWebSite.Personal=oContactInfoWebSiteNodeList.Item(0).InnerText;
                
                oContactInfo.Phone = oPhoneNo;
                oContactInfo.WebSiteAddress = oWebSite;
                resume.ContactInformation = oContactInfo;                                                   
            }

            
            XmlNodeList oExecSummaryNodeList = oXmlDoc.SelectNodes("/Resume/StructuredXMLResume/ExecutiveSummary");
            if (oExecSummaryNodeList.Count > 0)
            {
                resume.ExecutiveSummary = oExecSummaryNodeList.Item(0).InnerText;
            }
        }

        #endregion Private Methods

        #region Public Methods                                                 

        /// <summary>
        /// This method helps to contruct the resume contents.
        /// </summary>
        /// <param name="xml">
        /// A <see cref="string"/> that holds the xml document.
        /// </param>
        public HRXMLManager(string xml)
        {
            this.xml = xml;
            XmlDocument oXmlDoc = new XmlDocument();
            oXmlDoc.LoadXml(xml);
            // Process the HR-XML and construct the resume object.
            Process(oXmlDoc);
        }

        public Resume Resume
        {
            get
            {
                return resume;
            }
        }

        #endregion "Public Methods"
    }
}