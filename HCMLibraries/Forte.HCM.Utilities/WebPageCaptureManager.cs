using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Forte.HCM.Utilities
{
    public class WebPageCaptureManager
    {
        public static Bitmap GetWebPageCaptureImage(string Url, int BrowserWidth, int BrowserHeight, int ThumbnailWidth, int ThumbnailHeight)
        {
            WebPageCaptureImage thumbnailGenerator = new WebPageCaptureImage(Url, BrowserWidth, BrowserHeight, ThumbnailWidth, ThumbnailHeight);
            return thumbnailGenerator.GenerateWebPageImage();
        }

        private class WebPageCaptureImage
        {
            public WebPageCaptureImage(string Url, int BrowserWidth, int BrowserHeight, int ThumbnailWidth, int ThumbnailHeight)
            {
                this.m_Url = Url;
                this.m_BrowserWidth = BrowserWidth;
                this.m_BrowserHeight = BrowserHeight;
                this.m_ThumbnailHeight = ThumbnailHeight;
                this.m_ThumbnailWidth = ThumbnailWidth;
            }
                                    
            private string m_Url = null;
            public string Url
            {
                get
                {
                    return m_Url;
                }
                set
                {
                    m_Url = value;
                }
            }

            private Bitmap m_Bitmap = null;
            public Bitmap ThumbnailImage
            {
                get
                {
                    return m_Bitmap;
                }
            }

            private int m_ThumbnailWidth;
            public int ThumbnailWidth
            {
                get
                {
                    return m_ThumbnailWidth;
                }
                set
                {
                    m_ThumbnailWidth = value;
                }
            }

            private int m_ThumbnailHeight;
            public int ThumbnailHeight
            {
                get
                {
                    return m_ThumbnailHeight;
                }
                set
                {
                    m_ThumbnailHeight = value;
                }
            }

            private int m_BrowserWidth;
            public int BrowserWidth
            {
                get
                {
                    return m_BrowserWidth;
                }
                set
                {
                    m_BrowserWidth = value;
                }
            }

            private int m_BrowserHeight;
            public int BrowserHeight
            {
                get
                {
                    return m_BrowserHeight;
                }
                set
                {
                    m_BrowserHeight = value;
                }
            }

            public Bitmap GenerateWebPageImage()
            {
                Thread m_thread = new Thread(new ThreadStart(ProcessWebPageImage));
                m_thread.SetApartmentState(ApartmentState.STA);
                m_thread.Start();
                m_thread.Join();
                return m_Bitmap;
            }

            private void ProcessWebPageImage()
            {
                WebBrowser webBrowser = new WebBrowser();
                webBrowser.ScrollBarsEnabled = false;
                webBrowser.Navigate(m_Url);
                webBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(OnDocumentCompleted);
                while (webBrowser.ReadyState != WebBrowserReadyState.Complete)
                    Application.DoEvents();
                webBrowser.Dispose();
            }

            private void OnDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
            {
                WebBrowser webBrowser = (WebBrowser)sender;
                webBrowser.ClientSize = new Size(this.m_BrowserWidth, this.m_BrowserHeight);
                webBrowser.ScrollBarsEnabled = false;
                m_Bitmap = new Bitmap(webBrowser.Bounds.Width, webBrowser.Bounds.Height);
                webBrowser.BringToFront();
                webBrowser.DrawToBitmap(m_Bitmap, webBrowser.Bounds);
                m_Bitmap = (Bitmap)m_Bitmap.GetThumbnailImage(m_ThumbnailWidth, m_ThumbnailHeight, null, IntPtr.Zero);                
            }
        }
    }
}


