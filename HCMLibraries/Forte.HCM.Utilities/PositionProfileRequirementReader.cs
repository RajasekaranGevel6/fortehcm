﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileRequirementReader.cs
// File that represents the position profile requirement reader object that 
// will helps to read the position profile requirement data from any of the
// following file type: excel/word/pdf/txt/eml. This will convert the file 
// contents to a text format. 

#endregion Header

#region Directives
using System;
using System.IO;

using org.pdfbox.pdmodel;
using org.pdfbox.util;
using Microsoft.Office.Interop.Word;
#endregion Directives



namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Represents the class that helps to read the position profile requirement 
    /// data from any of the following file type: excel/word/pdf/txt/eml. This 
    /// will convert the file contents to a text format.
    /// </summary>
    public class PositionProfileRequirementReader
    {
        public string GetPositionProfileRequirement(string fileName)
        {
            string content = string.Empty;
            switch (Path.GetExtension(fileName))
            {
                case ".doc":
                    {
                        content = ReadWordFile(fileName, content);
                    }
                    break;
                case ".docx":
                    content = ReadWordFile(fileName, content);
                    break;
                case ".pdf":
                    {
                        PDDocument pdfDoc = PDDocument.load(fileName);
                        PDFTextStripper stripper = new PDFTextStripper();
                        //Return the text
                        content = stripper.getText(pdfDoc);

                    }
                    break;
                case ".txt":
                    {
                        content = File.ReadAllText(fileName);
                    }
                    break;
                case ".eml":
                    {
                        content = File.ReadAllText(fileName);
                    }
                    break;
            }
            return content;
        }

        private static string ReadWordFile(string fileName, string content)
        {
            object nullobject = Type.Missing;
            object refFileName = fileName;
            ApplicationClass applicationClass = new ApplicationClass();
            Document wordDocument = applicationClass.Documents.Open(ref refFileName,
                 ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject);
            content = wordDocument.Content.Text;
            applicationClass.Quit(ref nullobject, ref nullobject, ref nullobject);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(applicationClass);
            return content;
        }

        public bool ConvertDocument(string documentFile, string parserFolder)
        {
            _Application application = null;
            _Document document = null;

            object format = WdSaveFormat.wdFormatDOSTextLineBreaks;
            object nullobject = null;
            object docFileName = null;
            object txtFileName = null;

            try
            {
                nullobject = Type.Missing;

                docFileName = documentFile;

                application = new Application();

                document = application.Documents.Open
                    (ref docFileName, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject);

                txtFileName = @parserFolder + "\\" + Path.GetFileNameWithoutExtension(documentFile) + ".txt";

                application.ActiveDocument.SaveAs
                    (ref txtFileName, ref format, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject);

                // Close document.
                document.Close(ref nullobject, ref nullobject, ref nullobject);
                document = null;

                // Close application.
                application.Quit(ref nullobject, ref nullobject, ref nullobject);
                application = null;

                return true;
            }
            catch (Exception exp)
            {
                if (document != null)
                {
                    document.Close(ref nullobject, ref nullobject, ref nullobject);
                    document = null;
                }

                if (application != null)
                {
                    application.Quit(ref nullobject, ref nullobject, ref nullobject);
                    application = null;
                }

                throw exp;
            }
        }
    }
}
