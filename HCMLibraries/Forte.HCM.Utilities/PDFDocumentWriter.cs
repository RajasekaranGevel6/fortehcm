﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PDFDocumentWriter.cs
// File that holds the functionalities to build and write PDF tables
// for report generation.  

#endregion Header

#region Directives

using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.Trace;

using iTextSharp.text;
using iTextSharp.text.pdf;





#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Class that holds the functionalities to build and write PDF tables
    /// for report generation.
    /// </summary>
    public class PDFDocumentWriter
    {
        #region Private Constants

        /// <summary>
        /// A <see cref="int"/> that holds the tables count.
        /// </summary>
        private const int TABLES_COUNT = 10;

        /// <summary>
        /// A <see cref="int"/> that holds the header detail table index.
        /// </summary>
        private const int HEADER_DETAIL_TABLE_INDEX = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the segment detail table index.
        /// </summary>
        private const int SEGMENT_DETAIL_TABLE_INDEX = 1;

        /// <summary>
        /// A <see cref="int"/> that holds the recruiter detail table index.
        /// </summary>
        private const int RECRUITER_DETAIL_TABLE_INDEX = 2;

        /// <summary>
        /// A <see cref="int"/> that holds the co owner etail table index.
        /// </summary>
        private const int CO_OWNER_DETAIL_TABLE_INDEX = 3;

        /// <summary>
        /// A <see cref="int"/> that holds the action detail table index.
        /// </summary>
        private const int ACTION_DETAIL_TABLE_INDEX = 4;

        /// <summary>
        /// A <see cref="int"/> that holds the data summary detail table index.
        /// </summary>
        private const int DATA_SUMMARY_TABLE_INDEX = 5;

        /// <summary>
        /// A <see cref="int"/> that holds the test details table index.
        /// </summary>
        private const int TEST_DETAILS_TABLE_INDEX = 6;

        /// <summary>
        /// A <see cref="int"/> that holds the test session details table index.
        /// </summary>
        private const int TEST_SESSION_DETAILS_TABLE_INDEX = 7;

        /// <summary>
        /// A <see cref="int"/> that holds the interview details table index.
        /// </summary>
        private const int INTERIVIEW_DETAILS_TABLE_INDEX = 8;

        /// <summary>
        /// A <see cref="int"/> that holds the interview session details table index.
        /// </summary>
        private const int INTERIVIEW_SESSION_DETAILS_TABLE_INDEX = 9;

        /// <summary>
        /// A <see cref="int"/> that holds the position segment ID.
        /// </summary>
        private const int POSITION_SEGMENT_ID = 1;

        /// <summary>
        /// A <see cref="int"/> that holds the technical skills segment ID.
        /// </summary>
        private const int TECHNICAL_SKILLS_SEGMENT_ID = 2;

        /// <summary>
        /// A <see cref="int"/> that holds the verticals ID.
        /// </summary>
        private const int VERTICALS_SEGMENT_ID = 3;

        /// <summary>
        /// A <see cref="int"/> that holds the roles segment ID.
        /// </summary>
        private const int ROLES_SEGMENT_ID = 4;

        /// <summary>
        /// A <see cref="int"/> that holds the education segment ID.
        /// </summary>
        private const int EDUCATION_SEGMENT_ID = 8;
        #endregion Private Constants

        #region Static Variables
        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item color.
        /// </summary>
        static BaseColor itemColor = new BaseColor(20, 142, 192);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the header color.
        /// </summary>
        static BaseColor headerColor = new BaseColor(40, 48, 51);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item black color.
        /// </summary>
        static BaseColor blackColor = new BaseColor(0, 0, 0);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item black color.
        /// </summary>
        static BaseColor orangeColor = new BaseColor(247, 105, 24);

        /// <summary>
        /// A <see cref="BaseColor"/> that holds the item black color.
        /// </summary>
        static BaseColor blueColor = new BaseColor(0, 101, 214);

        static BaseColor SmokeBackColor = new BaseColor(246,246,246);

        static BaseColor rowBackColor = new BaseColor(232, 236, 237);
        static BaseColor redColor = new BaseColor(252, 67, 85);
        static BaseColor clientNameColor = new BaseColor(64, 64, 64);
        static BaseColor interviewNameColor = new BaseColor(105, 105, 105);
        static BaseColor grayColor = new BaseColor(225, 225, 225);
        static BaseColor TestDetailBlueColor = new BaseColor(0, 101, 214);
        static BaseColor PP_bluecolor = new BaseColor(0, 91, 160);
        static BaseColor TestDetailBlackBold = new BaseColor(0, 0, 0);
        static BaseColor PP_bluegrid = new BaseColor(0, 118, 208);
        /// <summary>
        ///  A <see cref="float"/> that holds the page left margin
        /// </summary>
        public static float leftMargin = 7.75f;
        /// <summary>
        ///  A <see cref="float"/> that holds the page right margin
        /// </summary>
        public static float rightMargin = 7.75f;
        /// <summary>
        ///  A <see cref="float"/> that holds the page top margin
        /// </summary>
        public static float topMargin = 30.5f;
        /// <summary>
        ///  A <see cref="float"/> that holds the page bottom margin
        /// </summary>
        public static float bottomMargin = 7.5f;

        public static iTextSharp.text.Rectangle paper =
            iTextSharp.text.PageSize.A4.Rotate();

        public static iTextSharp.text.Rectangle PP_pap =
            iTextSharp.text.PageSize.A4;
        #endregion Static Variables

        #region Private Methods

        #region Styles
        private iTextSharp.text.Font PdfCaptionStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                      ("calibri,arial,sans-serif", 16, iTextSharp.text.Font.NORMAL, blackColor);
        } 

        private iTextSharp.text.Font pdfMissingCellStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                      ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, redColor);
        }

        private iTextSharp.text.Font PdfHeaderStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, headerColor);
        }

        private iTextSharp.text.Font PdfItemStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, itemColor);
        }

        private iTextSharp.text.Font PdfSmallItemStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, interviewNameColor);
        }
        private iTextSharp.text.Font PdfSmallItemStyle_italic()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 8, iTextSharp.text.Font.ITALIC, interviewNameColor);
        }
        private iTextSharp.text.Font PdfSmallItemStyle_italic_bold()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 9, iTextSharp.text.Font.BOLDITALIC, interviewNameColor);
        }
        private iTextSharp.text.Font TestDetailPdfSmallItemStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 9, iTextSharp.text.Font.NORMAL, interviewNameColor);
        }


        private iTextSharp.text.Font PdfCategoryStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.BOLD, itemColor);
        }

        private iTextSharp.text.Font PdfItalicStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.ITALIC, blackColor);
        }

        private iTextSharp.text.Font PdfNormalStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, blackColor);
        }

        private iTextSharp.text.Font PdfOrangeStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 15, iTextSharp.text.Font.NORMAL, orangeColor);
        }
        private iTextSharp.text.Font PdfOrangeSmallStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, orangeColor);
        }
        private iTextSharp.text.Font PdfOrangeSmallStyleWihtoutBackColor()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, orangeColor);
        }

        private iTextSharp.text.Font PdfBlueStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 12, iTextSharp.text.Font.NORMAL, blueColor);
        }

        private iTextSharp.text.Font PdfBlackStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, blackColor);
        }

        private iTextSharp.text.Font PdfBlueStyleSmall()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, blueColor);
        }


        private iTextSharp.text.Font PdfBlueStylelarger()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 11, iTextSharp.text.Font.NORMAL, PP_bluecolor);
        }
        private iTextSharp.text.Font PdfBlueStylelarger_itallic()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 11, iTextSharp.text.Font.ITALIC, PP_bluecolor);
        }

        private iTextSharp.text.Font PdfTitleStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 14, iTextSharp.text.Font.NORMAL, blackColor);
        }

        private iTextSharp.text.Font PdfTitleStyle1()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 11, iTextSharp.text.Font.NORMAL, blackColor);
        }

        private iTextSharp.text.Font PdfClientNameStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 11, iTextSharp.text.Font.NORMAL, clientNameColor);
        }

        private iTextSharp.text.Font PdfInterviewNameStyle()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 11, iTextSharp.text.Font.NORMAL, interviewNameColor);
        }

        private iTextSharp.text.Font PP_gridviewcolumname()
        {
            return iTextSharp.text.FontFactory.GetFont
                    ("calibri,arial,sans-serif", 9, iTextSharp.text.Font.NORMAL, PP_bluegrid);
        }
        private iTextSharp.text.Font PdffontTestDetail()
        {
            return iTextSharp.text.FontFactory.GetFont("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, TestDetailBlueColor);
        }
        private iTextSharp.text.Font PdffontTestDetailDecimel()
        {
            return iTextSharp.text.FontFactory.GetFont("calibri,arial,sans-serif", 10, iTextSharp.text.Font.NORMAL, TestDetailBlueColor);
        }
        private iTextSharp.text.Font PdffontTestDetailStatus()
        {
            return iTextSharp.text.FontFactory.GetFont("calibri,arial,sans-serif", 10, iTextSharp.text.Font.BOLD, TestDetailBlueColor);
        }
        private iTextSharp.text.Font PdffontTestDetailBlack()
        {
            return iTextSharp.text.FontFactory.GetFont("calibri,arial,sans-serif", 9, iTextSharp.text.Font.NORMAL, TestDetailBlackBold);
        }
        private iTextSharp.text.Font PdffontTestDetailBlackSmall()
        {
            return iTextSharp.text.FontFactory.GetFont("calibri,arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, TestDetailBlackBold);
        }
        #endregion Styles


        /// <summary>
        /// Method that retrieves the non null date value from an object.
        /// </summary>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the non null data value. If data is
        /// null then it returns empty string.
        /// </returns>
        private string GetDateValue(object data)
        {
            if (data == null)
                return string.Empty;

            DateTime date = DateTime.MinValue;

            if (DateTime.TryParse(data.ToString(), out date))
            {
                return date.ToString("MM/dd/yyyy").Trim();
            }

            return string.Empty;
        }


        //Check Whether data is null
        private int GetIntegerValue(object data)
        {
            if (data == null)
                return 0;

            return Convert.ToInt32(data.ToString().Trim());
        }

        private PdfPCell PdfImageCell(iTextSharp.text.Image image,
            int rowSpan, int columnSpan)
        {
            PdfPCell customImageCell = null;
            customImageCell = new PdfPCell(image);

            if (rowSpan > 0) customImageCell.Rowspan = rowSpan;
            if (columnSpan > 0) customImageCell.Colspan = columnSpan;
            customImageCell.Border = 0;
            //customImageCell.BorderWidth = 0.00001f;
            customImageCell.HorizontalAlignment = Element.ALIGN_LEFT;
            return customImageCell;
        }

        private PdfPCell CreatePdfCell(string columnName,
            int rowSpan, int columnSpan, float border,
            int txtAlign, int padding, string styleType)
        {
            PdfPCell customCell = null;
            /*
             * txtAlign=1=>Left
             * txtAlign=2=>Center
             * txtAlign=3=>Right
             */
            double dec = 0;
            bool isNum = false;
            isNum = Double.TryParse(columnName, out dec);
            if (isNum)
            {
                if (dec > 0)
                {
                    if ((styleType == "NormalStyle1"))
                        columnName = string.Format("{0:0}", dec);
                    else if (styleType == "TestDetailBlue")
                    {
                        columnName = string.Format("{0:0}", dec);
                    }
                    else if (styleType == "SmallItemStyle_PP")
                    {
                        columnName = string.Format("{0:0}", dec);
                    }
                    else if (styleType == "TestDetailBlueDecimel")
                    {
                        columnName = string.Format("{0:0.00}", dec);
                    }
                    else
                        columnName = string.Format("{0:0.00}", dec) + "%";
                }
                else if ((dec == 0) && (styleType == "TestDetailBlue"))
                {
                    columnName = string.Format("{0:0}", dec);
                }
                else if ((dec == 0) && (styleType == "TestDetailBlueDecimel"))
                {
                    columnName = string.Format("{0:0.00}", dec);
                }

                else
                    columnName = " ";//N/R
            }

            if (styleType == "NormalStyle1") styleType = "NormalStyle";

            switch (styleType)
            {
                case "ItemStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfItemStyle()));
                    customCell.Padding = 3;
                    break;
                case "SmallItemStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfSmallItemStyle()));
                    customCell.Padding = 3;
                    break;
                case "SmallItemStyle_PP":
                    customCell = new PdfPCell(new Phrase(columnName, PdfSmallItemStyle()));
                    break;
                case "SmallItemStyle_PP_italic":
                    customCell = new PdfPCell(new Phrase(columnName, PdfSmallItemStyle_italic()));
                    break;
                case "CategoryStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfCategoryStyle()));
                    customCell.Padding = 5;
                    break;
                case "HeaderStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfHeaderStyle()));
                    break;
                case "CaptionStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfCaptionStyle()));
                    break;
                case "NormalStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfNormalStyle()));
                    customCell.Padding = 3;
                    break;
                case "ItalicStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfItalicStyle()));
                    customCell.Padding = 3;
                    break;
                case "TitleStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfTitleStyle()));
                    break;
                case "TitleStyle1":
                    customCell = new PdfPCell(new Phrase(columnName, PdfTitleStyle1()));
                    break;
                case "OrangeStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfOrangeStyle()));
                    customCell.Padding = 5;
                    break;
                case "BlueStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfBlueStyle()));
                    break;
                case "BlueStyleSmall":
                    customCell = new PdfPCell(new Phrase(columnName, PdfBlueStyleSmall()));
                    break;
                case "BackgroundWithItemStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfItemStyle()));
                    customCell.BackgroundColor = rowBackColor;
                    customCell.Padding = 5;
                    break;
                case "OrangeSmallStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfOrangeSmallStyle()));
                    customCell.BackgroundColor = rowBackColor;
                    customCell.Padding = 5;
                    break;
                case "OrangeSmallStyleWithoutBackColor":
                    customCell = new PdfPCell(new Phrase(columnName, PdfOrangeSmallStyleWihtoutBackColor()));
                    customCell.Padding = 5;
                    break;
                case "FinalRowStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfItemStyle()));
                    customCell.BackgroundColor = rowBackColor;
                    if (!isNum) txtAlign = 3;
                    break;
                case "ClientNameStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfClientNameStyle()));
                    break;
                case "InteviewNameStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfInterviewNameStyle()));
                    break;
                case "ScoreColumnStyle":
                    customCell = new PdfPCell(new Phrase(columnName, PdfBlackStyle()));
                    customCell.BackgroundColor = grayColor;
                    break;
                case "TestDetailBlue":
                    customCell = new PdfPCell(new Phrase(columnName, PdffontTestDetail()));
                    break;
                case "TestDetailBlueDecimel":
                    customCell = new PdfPCell(new Phrase(columnName, PdffontTestDetailDecimel()));
                    break;

                case "TestDetailBlueBold":
                    customCell = new PdfPCell(new Phrase(columnName, PdffontTestDetailStatus()));
                    break;

                case "TestDetailBlackBold":
                    customCell = new PdfPCell(new Phrase(columnName, PdffontTestDetailBlack()));
                    break;
                case "PdffontTestDetailBlackSmall":
                    customCell = new PdfPCell(new Phrase(columnName, PdffontTestDetailBlackSmall()));
                    break;
                case "TestDetailPdfSmallItemStyle":
                    customCell = new PdfPCell(new Phrase(columnName, TestDetailPdfSmallItemStyle()));
                    break;
                case "PP_blueheader":
                    customCell = new PdfPCell(new Phrase(columnName, PdfBlueStylelarger()));
                    break;
                case "PP_gridcolumnheader":
                    customCell = new PdfPCell(new Phrase(columnName, PP_gridviewcolumname()));
                    break;
                case "PP_gridcolumnheader_italic":
                    customCell = new PdfPCell(new Phrase(columnName, PdfBlueStylelarger_itallic()));
                    break;
                case "PP_MissingValue":
                    customCell = new PdfPCell(new Phrase(columnName, pdfMissingCellStyle()));
                    break;
                case "pp_italic_bold":
                    customCell = new PdfPCell(new Phrase(columnName, PdfSmallItemStyle_italic_bold()));
                    break;




                default:
                    break;
            }
            customCell.BorderWidth = border;
            customCell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;

            switch (txtAlign)
            {
                case 1:
                    customCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    customCell.PaddingLeft = padding;
                    break;
                case 2:
                    customCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    break;
                case 3:
                    customCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    customCell.VerticalAlignment = Element.ALIGN_CENTER;
                    break;
                case 4:
                    customCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    customCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    break;
                default:
                    break;
            }

            if (rowSpan > 0) customCell.Rowspan = rowSpan;
            if (columnSpan > 0) customCell.Colspan = columnSpan;

            return customCell;
        }


        /// <summary>
        /// Method that retrieves the recruiter assignment summary row.
        /// </summary>
        /// <param name="taskOwnerAvailable">
        /// A <see cref="bool"/> that holds the task owner available status.
        /// </param>
        /// <param name="dataSummaryRow">
        /// A <see cref="DataRow"/> that holds the data summary row.
        /// </param>
        /// <param name="recruitersTable">
        /// A <see cref="DataTable"/> that holds the recruiters table.
        /// </param>
        /// <returns>
        /// A <see cref="PdfPTable"/> that holds the Pdf table.
        /// </returns>
        private PdfPTable GetRecruiterAssignmentSummaryRow(bool taskOwnerAvailable,
           DataRow dataSummaryRow, DataTable recruitersTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (recruitersTable != null && recruitersTable.Rows != null && recruitersTable.Rows.Count > 0)
                actionAvailable = true;

            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Recruiter Assignment:", 0, 0, 0, 1, 1, "pp_italic_bold"));

            #region ActionData
            if (actionAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Recruiter not assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (actionAvailable == true)
            {
                string Value = string.Format("{0} recruiter(s) assigned", GetValue(recruitersTable.Rows.Count));
                Maintable.AddCell(CreatePdfCell(Value, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner

            return Maintable;
        }

        /// <summary>
        /// Method that add Candidate Association information
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetCandidateAssociationSummaryRow(bool actionAvailable, bool taskOwnerAvailable,
           DataRow dataSummaryRow)
        {
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Candidate Association:", 0, 0, 0, 1, 1, "pp_italic_bold"));
            #region ActionData
            if (actionAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Candidate not associated ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (actionAvailable == true)
            {
                string AssoCount = string.Format("{0} candidate(s) associated", GetValue(dataSummaryRow["ASSOCIATED_CANDIDATES_COUNT"]));
                Maintable.AddCell(CreatePdfCell(AssoCount, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner
            return Maintable;
        }

        /// <summary>
        /// Method that add interview creation information
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetInterviewRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable interviewDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (interviewDetailsTable != null && interviewDetailsTable.Rows != null && interviewDetailsTable.Rows.Count > 0)
                actionAvailable = true;
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Interview Creation: ", 0, 0, 0, 1, 1, "pp_italic_bold"));

            #region ActionData
            if (actionAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Interview not created", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (actionAvailable == true)
            {
                string preCaptiontext = string.Empty;
                string Value = string.Empty;
                if (interviewDetailsTable.Rows.Count == 1)
                    preCaptiontext = " 1 Interview";
                else
                    preCaptiontext = string.Format("{0} interviews", interviewDetailsTable.Rows.Count);
                Value += preCaptiontext;
                // Loop through the table and add the test names.
                for (int rowIndex = 0; rowIndex < interviewDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow interviewDetailRow = interviewDetailsTable.Rows[rowIndex];
                    Value += " " + GetValue(interviewDetailRow["INTERVIEW_NAME"]);
                    if (rowIndex + 1 < interviewDetailsTable.Rows.Count)
                    {
                        Value = Value + ", ";
                    }
                    if (rowIndex + 1 == interviewDetailsTable.Rows.Count)
                    {
                        Value = Value + " created.";
                    }
                }
                Maintable.AddCell(CreatePdfCell(Value, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));

            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner
            return Maintable;
        }

        /// <summary>
        /// Method that adds Interview Session Creation infromation 
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetInterviewSessionRequiredSummaryRow(bool taskOwnerAvailable,
          DataRow dataSummaryRow, DataTable interviewSessionDetailsTable, DataTable interviewDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (interviewSessionDetailsTable != null && interviewSessionDetailsTable.Rows != null && interviewSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Interview Session Creation: ", 0, 0, 0, 1, 1, "pp_italic_bold"));
            #region ActionData
            if (actionAvailable == false)
            {
                string Interviewsessionreq = "Interview session not created";
                Maintable.AddCell(CreatePdfCell(Interviewsessionreq, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));

                if (interviewDetailsTable == null || interviewDetailsTable.Rows == null || interviewDetailsTable.Rows.Count == 0)
                {
                    Interviewsessionreq += ". Interview needs to be associated before creating interview session. ";
                    Maintable.AddCell(CreatePdfCell(Interviewsessionreq, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
                }
            }
            else if (actionAvailable == true)
            {
                string preCaptiontext = string.Empty;
                string Value = string.Empty;
                if (interviewSessionDetailsTable.Rows.Count == 1)
                    preCaptiontext = "1 interview session";
                else
                    preCaptiontext = string.Format("{0} interview sessions", interviewSessionDetailsTable.Rows.Count);
                Value += preCaptiontext;
                for (int rowIndex = 0; rowIndex < interviewSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow interviewSessionDetailRow = interviewSessionDetailsTable.Rows[rowIndex];
                    Value += " " + string.Format("{0}({1})",
                         GetValue(interviewSessionDetailRow["SESSION_KEY"]), GetValue(interviewSessionDetailRow["SESSION_COUNT"]));
                    if (rowIndex + 1 < interviewDetailsTable.Rows.Count)
                    {
                        Value = Value + ", ";
                    }
                    if (rowIndex + 1 == interviewDetailsTable.Rows.Count)
                    {
                        Value = Value + " created.";
                    }
                }
                Maintable.AddCell(CreatePdfCell(Value, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner
            return Maintable;
        }
        /// <summary>
        /// Method that truncates and source string to the max characters given.
        /// For truncated characters the ... symbol will post fixed.
        /// </summary>
        /// <param name="sourceString">
        /// A <see cref="string"/> that holds the source string.
        /// </param>
        /// <param name="maxCharacters">
        /// A <see cref="int"/> that holds the max characters.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the truncated string.
        /// </returns>
        /// <remarks>
        /// If source string length is less than the given max characters, then
        /// the entire source string will be returned. Else the truncated 
        /// string will be returned.
        /// </remarks>
        private string GetTruncatedString(string sourceString, int maxCharacters)
        {
            if (Utility.IsNullOrEmpty(sourceString))
                return string.Empty;

            if (sourceString.Length > maxCharacters)
                return sourceString.Substring(0, maxCharacters) + "...";
            else
                return sourceString;
        }


        /// <summary>
        /// Method that add Interview Scheduling information
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetInterviewScheduleRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable interviewSessionDetailsTable)
        {
            bool actionAvailable = false;
            // Assign action available.
            if (interviewSessionDetailsTable != null && interviewSessionDetailsTable.Rows != null && interviewSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 25, 50, 5, 10 };
            Maintable.AddCell(CreatePdfCell("Interview Scheduling: ", 0, 0, 0, 1, 1, "pp_italic_bold"));

            #region ActionData
            if (actionAvailable == false)
            {
                string Caption = "Interview not scheduled";
                Maintable.AddCell(CreatePdfCell(Caption, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (actionAvailable == true)
            {
                string Value = string.Empty;
                // Loop through all test sessions and show the summary.
                for (int rowIndex = 0; rowIndex < interviewSessionDetailsTable.Rows.Count; rowIndex++)
                {

                    DataRow interviewSessionDetailRow = interviewSessionDetailsTable.Rows[rowIndex];
                    string interviewScheduledCount = GetIntegerValue(interviewSessionDetailRow["SCHEDULED_COUNT"]).ToString();
                    interviewScheduledCount = Value + interviewScheduledCount + " candidate(s) scheduled & ";

                    string interviewCompletedCount = GetIntegerValue(interviewSessionDetailRow["COMPLETED_COUNT"]).ToString();
                    Value = interviewScheduledCount + interviewCompletedCount + " candidate(s) completed for session ";

                    string sessionKey = GetValue(interviewSessionDetailRow["SESSION_KEY"]);
                    Value = Value + sessionKey;
                    if (rowIndex + 1 < interviewSessionDetailsTable.Rows.Count)
                    {
                        Value = Value + ", ";
                    }
                }
                Maintable.AddCell(CreatePdfCell(Value, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner

            return Maintable;
        }

        /// <summary>
        /// Method that add Interview Assessment information
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetInterviewAssessmentSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable interviewSessionDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (interviewSessionDetailsTable != null && interviewSessionDetailsTable.Rows != null && interviewSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Interview Assessment: ", 0, 0, 0, 1, 1, "pp_italic_bold"));
            #region ActionData
            if (actionAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Interview not assessed", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (actionAvailable == true)
            {
                string value = string.Empty;
                PdfPTable interviewAssesmentpdf = new PdfPTable(interviewSessionDetailsTable.Rows.Count);
                interviewAssesmentpdf.DefaultCell.BorderWidth = 0;
                // Loop through all test sessions and show the summary.
                for (int rowIndex = 0; rowIndex < interviewSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow interviewSessionDetailRow = interviewSessionDetailsTable.Rows[rowIndex];
                    string assessdCount = GetIntegerValue(interviewSessionDetailRow["ASSESSED_COUNT"]).ToString();
                    string sessionKey = GetValue(interviewSessionDetailRow["SESSION_KEY"]);
                    value += assessdCount + " candidate(s) assessed for session " + sessionKey;
                    if (rowIndex + 1 < interviewSessionDetailsTable.Rows.Count)
                    {
                        value = value + ", ";
                    }
                }
                Maintable.AddCell(CreatePdfCell(value, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner
            return Maintable;
        }

        /// <summary>
        /// Method that add Test Creation informations
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetTestRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable testDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (testDetailsTable != null && testDetailsTable.Rows != null && testDetailsTable.Rows.Count > 0)
                actionAvailable = true;
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Test Creation: ", 0, 0, 0, 1, 1, "pp_italic_bold"));
            #region ActionData
            if (actionAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Test not created ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (actionAvailable == true)
            {
                string preCaption = string.Empty;
                string Value = string.Empty;
                if (testDetailsTable.Rows.Count == 1)
                    preCaption = "1 test";
                else
                    preCaption = string.Format("{0} tests", testDetailsTable.Rows.Count);
                Value += preCaption;
                // Loop through the table and add the test names.
                for (int rowIndex = 0; rowIndex < testDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow testDetailRow = testDetailsTable.Rows[rowIndex];
                    Value += " " + GetValue(testDetailRow["TEST_NAME"]);
                    if (rowIndex + 1 < testDetailsTable.Rows.Count)
                    {
                        Value = Value + ", ";
                    }
                    if (rowIndex + 1 == testDetailsTable.Rows.Count)
                    {
                        Value = Value + " created.";
                    }
                }
                Maintable.AddCell(CreatePdfCell(Value, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner
            return Maintable;
        }

        /// <summary>
        /// Method that add Testsession information to pdf  maintable
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetTestSessionRequiredSummaryRow(bool taskOwnerAvailable,
            DataRow dataSummaryRow, DataTable testSessionDetailsTable, DataTable testDetailsTable)
        {
            bool actionAvailable = false;

            // Assign action available.
            if (testSessionDetailsTable != null && testSessionDetailsTable.Rows != null && testSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;

            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Test Session Creation: ", 0, 0, 0, 1, 1, "pp_italic_bold"));

            #region ActionData
            if (actionAvailable == false)
            {
                string caption = "Test session not created";
                Maintable.AddCell(CreatePdfCell(caption, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
                if (testDetailsTable == null || testDetailsTable.Rows == null || testDetailsTable.Rows.Count == 0)
                {
                    caption += ". Test needs to be associated before creating test session. ";
                    Maintable.AddCell(CreatePdfCell(caption, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
                }
            }
            else if (actionAvailable == true)
            {
                string preCaption = string.Empty;
                string Value = string.Empty;
                if (testSessionDetailsTable.Rows.Count == 1)
                    preCaption = "1 test session";
                else
                    preCaption = string.Format("{0} test sessions", testSessionDetailsTable.Rows.Count);
                Value += preCaption;
                // Loop through the table and add the test sessions..
                for (int rowIndex = 0; rowIndex < testSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow testSessionDetailRow = testSessionDetailsTable.Rows[rowIndex];
                    Value += " " + string.Format("{0}({1})",
                         GetValue(testSessionDetailRow["SESSION_KEY"]), GetValue(testSessionDetailRow["SESSION_COUNT"]));

                    if (rowIndex + 1 < testSessionDetailsTable.Rows.Count)
                    {
                        Value = Value + ", ";
                    }
                    if (rowIndex + 1 == testSessionDetailsTable.Rows.Count)
                    {
                        Value = Value + " created.";
                    }
                }
                Maintable.AddCell(CreatePdfCell(Value, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner
            return Maintable;
        }
        /// <summary>
        /// Method that add Test Scheduling information to pdf maintable
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetTestScheduleRequiredSummaryRow(bool actionAvailable, bool taskOwnerAvailable,
          DataRow dataSummaryRow, DataTable testSessionDetailsTable)
        {
            // Assign action available.
            if (testSessionDetailsTable != null && testSessionDetailsTable.Rows != null && testSessionDetailsTable.Rows.Count > 0)
                actionAvailable = true;
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Test Scheduling: ", 0, 0, 0, 1, 1, "pp_italic_bold"));

            #region ActionData
            if (actionAvailable == false)
            {
                string Caption = "Test not scheduled";
                Maintable.AddCell(CreatePdfCell(Caption, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (actionAvailable == true)
            {
                string Value = string.Empty;
                // Loop through all test sessions and show the summary.
                for (int rowIndex = 0; rowIndex < testSessionDetailsTable.Rows.Count; rowIndex++)
                {
                    DataRow testSessionDetailRow = testSessionDetailsTable.Rows[rowIndex];
                    string testSessionCount = GetIntegerValue(testSessionDetailRow["SCHEDULED_COUNT"]).ToString();
                    Value += string.Format("{0} candidate(s) scheduled & ", testSessionCount);

                    string testSessionCompleted = GetIntegerValue(testSessionDetailRow["COMPLETED_COUNT"]).ToString();
                    Value += string.Format("{0} candidate(s) completed for session ", testSessionCompleted);
                    string sessionKey = GetValue(testSessionDetailRow["SESSION_KEY"]);
                    Value += sessionKey;

                    if (rowIndex + 1 < testSessionDetailsTable.Rows.Count)
                    {
                        Value = Value + ", ";
                    }
                }
                Maintable.AddCell(CreatePdfCell(Value, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner

            return Maintable;
        }

        /// <summary>
        /// Method that add candidate submitted summary
        /// </summary>
        /// <param name="actionAvailable"></param>
        /// <param name="taskOwnerAvailable"></param>
        /// <param name="dataSummaryRow"></param>
        /// <returns></returns>
        private PdfPTable GetCandidateSubmittedSummaryRow(bool actionAvailable, bool taskOwnerAvailable,
           DataRow dataSummaryRow)
        {
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            Maintable.WidthPercentage = 90;
            int[] WorkflowSumamry_columnwidth = { 90 };
            Maintable.AddCell(CreatePdfCell("Submittal To Client:", 0, 0, 0, 1, 1, "pp_italic_bold"));

            #region ActionData
            if (actionAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("No candidates submitted", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (actionAvailable == true)
            {
                string AssoCount = string.Format("{0} candidates(s) submitted", GetValue(dataSummaryRow["SUBMITTED_CANDIDATES_COUNT"]));
                Maintable.AddCell(CreatePdfCell(AssoCount, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion ActionData

            #region Taskowner
            if (taskOwnerAvailable == false)
            {
                Maintable.AddCell(CreatePdfCell("Only default task owner assigned ", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            else if (taskOwnerAvailable == true)
            {
                Maintable.AddCell(CreatePdfCell("Task owner assigned", 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            #endregion Taskowner
            return Maintable;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Method returns PDF header table
        /// </summary>
        /// <param name="serverPath">
        /// A<see cref="string"/> that holds the server path
        /// </param>
        /// <returns></returns>
        public PdfPTable PageHeader(string serverPath, string pdfTitle)
        {
            PdfPTable headerTable = new PdfPTable(2);
            headerTable.WidthPercentage = 90;
            string logoPath = serverPath + "/App_Themes/DefaultTheme/Images/fortehcm_logo.jpg";

            iTextSharp.text.Image logoImage = iTextSharp.text.Image.GetInstance(logoPath);
            PdfPCell imageCell = null;
            imageCell = new PdfPCell(logoImage);
            imageCell.Border = 0;
            imageCell.Padding = 5f;
            headerTable.AddCell(imageCell);

            headerTable.AddCell(CreatePdfCell(pdfTitle, 0, 0, 0, 4, 1, "CaptionStyle"));
            return headerTable;
        }

        public PdfPTable PageTitle(string pageTitle)
        {
            PdfPTable titleTable = new PdfPTable(1);
            titleTable.AddCell(CreatePdfCell(pageTitle, 0, 0, 0.0001f, 1, 1, "CaptionStyle"));
            titleTable.WidthPercentage = 90;
            return titleTable;
        }

        public PdfPTable TableHeader(CandidateInterviewSessionDetail sessionDetail,
            string candidateSessionID, int attemptID, string photoPath, out DataTable summaryTable)
        {

            decimal totalScore = 0m;
            decimal totalWeightedScore = 0m;

            // Get rating summary.
            DataSet rawData = new AssessmentSummaryBLManager().
                GetRatingSummary(candidateSessionID, attemptID);

            // Get rating summary table.
            if (rawData == null || rawData.Tables[1].Rows.Count == 0)
                summaryTable = new DataTable();
            else
                summaryTable = new DataManager().GetRatingSummaryTable(rawData, out totalScore, out totalWeightedScore);

            PdfPTable tableHeader = new PdfPTable(5);
            tableHeader.WidthPercentage = 90;
            int[] tblColumnWidth = { 45, 5, 130, 25, 160 };
            tableHeader.SetWidths(tblColumnWidth);

            iTextSharp.text.Image candidateImage =
                iTextSharp.text.Image.GetInstance(photoPath);

            candidateImage.Alignment = Element.ALIGN_LEFT;
            candidateImage.ScaleToFit(125f, 85f);

            tableHeader.AddCell(PdfImageCell(candidateImage, 4, 0));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.CandidateName, 0, 0, 0, 1, 1, "TitleStyle"));
            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.PositionProfileName, 0, 0, 0, 1, 1, "BlueStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            if (sessionDetail.DateCompleted.ToString() != "1/1/0001 12:00:00 AM")
                tableHeader.AddCell(CreatePdfCell("Interview completed on " + sessionDetail.DateCompleted.ToString("MMMM dd,yyyy"), 0, 0, 0, 1, 1, "SmallItemStyle"));
            else
                tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "SmallItemStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.ClientName, 0, 0, 0, 1, 1, "ClientNameStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(" ", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(" ", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(" ", 0, 0, 0, 1, 1, "ItemStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            if (!string.IsNullOrEmpty(totalWeightedScore.ToString()))
                tableHeader.AddCell(CreatePdfCell("Total Score : " + totalWeightedScore.ToString() + " %", 0, 0, 0, 1, 1, "OrangeStyle"));
            else
                tableHeader.AddCell(CreatePdfCell(" ", 0, 0, 0, 1, 1, "OrangeStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.InterviewTestName, 0, 0, 0, 1, 1, "InteviewNameStyle"));


            /*PdfPTable tableHeader = new PdfPTable(5);
            int[] tblColumnWidth = { 110, 120, 180, 120, 180 };
            tableHeader.SetWidths(tblColumnWidth);
             
            iTextSharp.text.Image candidateImage =
                iTextSharp.text.Image.GetInstance(photoPath);
             
            candidateImage.Alignment = Element.ALIGN_LEFT;
            candidateImage.ScaleToFit(45f, 85f);

            tableHeader.AddCell(PdfImageCell(candidateImage, 6, 0));
              tableHeader.AddCell(CreatePdfCell("Interview Name", 0, 0, 0, 1, 1, "HeaderStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.InterviewTestName, 0, 0, 0, 1, 1, "TitleStyle"));
            
            tableHeader.AddCell(CreatePdfCell("Interview Description", 6, 0, 0, 1, 1, "HeaderStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.InterviewTestDescription, 6, 0, 0, 1, 1, "ItemStyle"));

            tableHeader.AddCell(CreatePdfCell("Position Profile", 0, 0, 0, 1, 1, "HeaderStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.PositionProfileName, 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell("Client", 0, 0, 0, 1, 1, "HeaderStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.ClientName , 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell("Candidate Name", 0, 0, 0, 1, 1, "HeaderStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.CandidateName, 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell("Total Weighted Score", 0, 0, 0, 1, 1, "HeaderStyle"));
            tableHeader.AddCell(CreatePdfCell(totalWeightedScore.ToString(), 0, 0, 0, 1, 1, "OrangeStyle"));   */

            return tableHeader;
        }

        public PdfPTable TableDetail(DataTable assessmenttable)
        {
            PdfPTable tableDetail = null;


            if (assessmenttable == null || assessmenttable.Rows.Count == 0)
            {
                tableDetail = new PdfPTable(1);
                tableDetail.WidthPercentage = 90;
                tableDetail.AddCell(CreatePdfCell("No assessor summary data found to display", 0, 0, 0.0001f, 2, 10, "CaptionStyle"));
            }
            else
            {
                tableDetail = new PdfPTable(assessmenttable.Columns.Count - 1);
                tableDetail.WidthPercentage = 90;

                int[] tableCellWidth = null;
                string[] exceptCation = { "title", "Score", "Weight", "FLAG", "Weighted Subject Score" };
                List<string> cationList = new List<string>();
                cationList.Add("title");
                cationList.Add("Score");
                cationList.Add("Weight");
                cationList.Add("FLAG");
                cationList.Add("Weighted Subject Score");

                if (assessmenttable.Columns.Count - 1 == 5)
                    tableCellWidth = new int[] { 4, 1, 1, 1, 1 };
                else if (assessmenttable.Columns.Count - 1 == 6)
                    tableCellWidth = new int[] { 4, 1, 1, 1, 1, 1 };
                else if (assessmenttable.Columns.Count - 1 == 7)
                    tableCellWidth = new int[] { 4, 1, 1, 1, 1, 1, 1 };
                else if (assessmenttable.Columns.Count - 1 == 8)
                    tableCellWidth = new int[] { 4, 1, 1, 1, 1, 1, 1, 1 };
                else if (assessmenttable.Columns.Count - 1 == 9)
                    tableCellWidth = new int[] { 4, 1, 1, 1, 1, 1, 1, 1, 1 };
                else if (assessmenttable.Columns.Count - 1 == 10)
                    tableCellWidth = new int[] { 4, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

                tableDetail.SetWidths(tableCellWidth);

                for (int i = 0; i < assessmenttable.Columns.Count - 1; i++)
                {
                    if (i == 0)
                        tableDetail.AddCell(CreatePdfCell("Subject/Question", 0, 0, 0.0001f, 2, 1, "BackgroundWithItemStyle"));
                    else
                    {
                        if (!cationList.Contains(assessmenttable.Columns[i].Caption.ToString()))
                            tableDetail.AddCell(CreatePdfCell(assessmenttable.Columns[i].Caption.ToString(), 0, 0, 0.0001f, 2, 1, "OrangeSmallStyle"));
                        else
                            tableDetail.AddCell(CreatePdfCell(assessmenttable.Columns[i].Caption.ToString(), 0, 0, 0.0001f, 2, 1, "BackgroundWithItemStyle"));
                    }
                }

                string weightCaption = string.Empty;
                for (int j = 0; j < assessmenttable.Rows.Count; j++)
                {
                    weightCaption = string.Empty;
                    for (int k = 0; k < assessmenttable.Columns.Count - 1; k++)
                    {
                        weightCaption = assessmenttable.Columns[k].Caption.ToString();
                        if (k == 0)
                        {
                            if (Convert.ToString(assessmenttable.Rows[j]["FLAG"]) == "S")
                                tableDetail.AddCell(CreatePdfCell(assessmenttable.Rows[j][k].ToString(), 0, 0, 0.0001f, 1, 3, "CategoryStyle"));
                            else if (Convert.ToString(assessmenttable.Rows[j]["FLAG"]) == "O")
                                tableDetail.AddCell(CreatePdfCell(assessmenttable.Rows[j][k].ToString(), 0, 0, 0.0001f, 2, 1, "BackgroundWithItemStyle"));
                            else
                            {
                                string questionValue = assessmenttable.Rows[j][k].ToString();

                                if (questionValue.IndexOf("$") > 0)
                                    questionValue = questionValue.Remove(questionValue.IndexOf("$"));

                                tableDetail.AddCell(CreatePdfCell(questionValue, 0, 0, 0.0001f, 1, 8, "ItalicStyle"));
                            }
                        }
                        else
                        {
                            if (assessmenttable.Columns.Contains("FLAG"))
                            {
                                if (Convert.ToString(assessmenttable.Rows[j]["FLAG"]) == "O")
                                {
                                    tableDetail.AddCell(CreatePdfCell(assessmenttable.Rows[j][k].ToString(), 0, 0, 0.0001f, 2, 1, "FinalRowStyle"));
                                }
                                else
                                    if (weightCaption == "Weight")
                                        tableDetail.AddCell(CreatePdfCell(assessmenttable.Rows[j][k].ToString(), 0, 0, 0.0001f, 2, 1, "NormalStyle1"));
                                    else
                                        tableDetail.AddCell(CreatePdfCell(assessmenttable.Rows[j][k].ToString(), 0, 0, 0.0001f, 2, 1, "NormalStyle"));

                            }
                        }
                    }
                }
            }

            return tableDetail;
        }

        /// <summary>
        /// Method Returns CandidateStatisctics Details in Table
        /// </summary>
        /// <param name="candidateStatisticsDetail"></param>
        /// <returns>CandidateDetailTable</returns>
        public PdfPTable TestResultSummary(CandidateStatisticsDetail candidateStatisticsDetail)
        {
            PdfPTable tableDetail = null;
            if (candidateStatisticsDetail == null)
            {
                tableDetail = new PdfPTable(1);
                tableDetail.WidthPercentage = 90;
                tableDetail.AddCell(CreatePdfCell("No data found to display", 0, 0, 0.0001f, 2, 10, "CaptionStyle"));
            }
            else
            {
                tableDetail = new PdfPTable(7);
                tableDetail.WidthPercentage = 90;
                tableDetail.DefaultCell.BorderWidth = 0;
                int[] tblColumnWidth = { 24, 1, 21, 1, 28, 1, 20 };
                tableDetail.SetWidths(tblColumnWidth);

                tableDetail.AddCell(CreatePdfCell("Candidate Name", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.CandidateFullName.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Test Name", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TestName.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));

                tableDetail.AddCell(CreatePdfCell("Number Of Questions", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TotalQuestion.ToString(), 0, 0, 0, 0, 0, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("My Score", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(candidateStatisticsDetail.MyScore).ToString() + "%", 0, 0, 0, 1, 1, "OrangeSmallStyleWithoutBackColor"));

                tableDetail.AddCell(CreatePdfCell("Total Questions Attended", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TotalQuestionAttended.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Absolute Score", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(candidateStatisticsDetail.AbsoluteScore).ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));

                tableDetail.AddCell(CreatePdfCell("Total Questions Skipped", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TotalQuestionSkipped.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Relative Score", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.RelativeScore.ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));

                tableDetail.AddCell(CreatePdfCell("Questions Answered Correctly", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.QuestionsAnsweredCorrectly.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Percentile", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(candidateStatisticsDetail.Percentile).ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));

                tableDetail.AddCell(CreatePdfCell("Questions Answered Wrongly", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.QuestionsAnsweredWrongly.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Total Time Taken", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                decimal totaltimetaken = 0;
                if (!Utility.IsNullOrEmpty(candidateStatisticsDetail.TimeTaken))
                {
                    tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TimeTaken.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                }
                else
                    tableDetail.AddCell(CreatePdfCell(totaltimetaken.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));

                tableDetail.AddCell(CreatePdfCell("Questions Answered Correctly (in %)", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.AnsweredCorrectly.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Average Time Taken To Answer Each Question", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");

                decimal averageTimeTaken = 0;
                if (!Utility.IsNullOrEmpty(candidateStatisticsDetail.AverageTimeTaken))
                {
                    averageTimeTaken = decimal.Parse(candidateStatisticsDetail.AverageTimeTaken.ToString());
                    string AverageTimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds(decimal.ToInt32(averageTimeTaken));
                    tableDetail.AddCell(CreatePdfCell(AverageTimeTaken, 0, 0, 0, 1, 1, "TestDetailBlue"));
                }
                else
                    tableDetail.AddCell(CreatePdfCell("0", 0, 0, 0, 1, 1, "TestDetailBlue"));

                tableDetail.AddCell(CreatePdfCell("Test Status:", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TestStatus.ToString(), 0, 0, 0, 1, 1, "TestDetailBlueBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(" ");
                tableDetail.AddCell(" ");
                tableDetail.AddCell(" ");
            }
            return tableDetail;
        }

        public PdfPTable OpenTextTestResultSummary(CandidateStatisticsDetail candidateStatisticsDetail)
        {
            PdfPTable tableDetail = null;
            if (candidateStatisticsDetail == null)
            {
                tableDetail = new PdfPTable(1);
                tableDetail.WidthPercentage = 90;
                tableDetail.AddCell(CreatePdfCell("No data found to display", 0, 0, 0.0001f, 2, 10, "CaptionStyle"));
            }
            else
            {
                tableDetail = new PdfPTable(7);
                tableDetail.WidthPercentage = 90;
                tableDetail.DefaultCell.BorderWidth = 0;
                int[] tblColumnWidth = { 24, 1, 21, 1, 28, 1, 20 };
                tableDetail.SetWidths(tblColumnWidth);

                tableDetail.AddCell(CreatePdfCell("Candidate Name", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.CandidateFullName.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Test Name", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TestName.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));

                tableDetail.AddCell(CreatePdfCell("Number Of Questions", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TotalQuestion.ToString(), 0, 0, 0, 0, 0, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Marks Scored", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(candidateStatisticsDetail.MarksObtained).ToString(), 0, 0, 0, 1, 1, "OrangeSmallStyleWithoutBackColor"));

                tableDetail.AddCell(CreatePdfCell("Total Questions Attended", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TotalQuestionAttended.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Marks", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(candidateStatisticsDetail.Marks).ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));

                tableDetail.AddCell(CreatePdfCell("Total Questions Skipped", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TotalQuestionSkipped.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Total Time Taken", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                decimal totaltimetaken = 0;
                if (!Utility.IsNullOrEmpty(candidateStatisticsDetail.TimeTaken))
                {
                    tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TimeTaken.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                }
                else
                    tableDetail.AddCell(CreatePdfCell(totaltimetaken.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                
                tableDetail.AddCell(CreatePdfCell("Score Percentage(%)", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.AnsweredCorrectly.ToString()+"%", 0, 0, 0, 1, 1, "TestDetailBlue"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell("Average Time Taken To Answer Each Question", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");

                decimal averageTimeTaken = 0;
                if (!Utility.IsNullOrEmpty(candidateStatisticsDetail.AverageTimeTaken))
                {
                    averageTimeTaken = decimal.Parse(candidateStatisticsDetail.AverageTimeTaken.ToString());
                    string AverageTimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds(decimal.ToInt32(averageTimeTaken));
                    tableDetail.AddCell(CreatePdfCell(AverageTimeTaken, 0, 0, 0, 1, 1, "TestDetailBlue"));
                }
                else
                    tableDetail.AddCell(CreatePdfCell("0", 0, 0, 0, 1, 1, "TestDetailBlue"));

                tableDetail.AddCell(CreatePdfCell("Test Status:", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(CreatePdfCell(candidateStatisticsDetail.TestStatus.ToString(), 0, 0, 0, 1, 1, "TestDetailBlueBold"));
                tableDetail.AddCell(" ");
                tableDetail.AddCell(" ");
                tableDetail.AddCell(" ");
                tableDetail.AddCell(" ");
            }
            return tableDetail;
        }

        /// <summary>
        /// Methos that returns the question details for the candidate test results summary.
        /// </summary>
        /// <param name="serverPath">
        /// A <see cref="string"/> that holds the server path.
        /// </param>
        /// <param name="questionDetails">
        /// A list of <see cref="QuestionDetail"/> that holds the questions.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <returns>
        /// A <see cref="PdfPTable"/> that holds the candidate test results 
        /// summary details. This consist of questions, choices and other 
        /// related details.
        /// </returns>
        public PdfPTable TestResultSummaryQuestions(string serverPath,
            List<QuestionDetail> questionDetails, string testKey, int attemptID,
            string candidateSessionKey)
        {
            PdfPTable tableDetail = null;
            PdfPTable questionTable = null;
            PdfPTable questionMainTable = null;
            PdfPTable choiceTable = null;
            if (questionDetails == null)
            {
                tableDetail = new PdfPTable(1);
                tableDetail.WidthPercentage = 100;
                tableDetail.AddCell(CreatePdfCell("No data found to display", 0, 0, 0.0001f, 2, 10, "CaptionStyle"));
            }
            else
            {
                questionMainTable = new PdfPTable(1);
                questionMainTable.WidthPercentage = 90;
                questionMainTable.DefaultCell.BorderWidth = 0;
                questionMainTable.SpacingBefore = 30f;

                for (int i = 0; i < questionDetails.Count; i++)
                {
                    questionTable = new PdfPTable(4);
                    questionTable.WidthPercentage = 90;
                    questionTable.DefaultCell.BorderWidth = 0;
                    int[] tblColumnWidth = { 3, 3, 1, 77 };
                    questionTable.SetWidths(tblColumnWidth);

                    string questionuimagepath = serverPath + "/App_Themes/DefaultTheme/Images/question_icon_popup.gif";
                    iTextSharp.text.Image logoImage = iTextSharp.text.Image.GetInstance(questionuimagepath);
                    logoImage.ScaleToFit(18, 20);
                    PdfPCell imageCell = null;
                    imageCell = new PdfPCell(logoImage);
                    imageCell.Border = 0;
                    imageCell.Padding = 5f;
                    questionTable.DefaultCell.BorderWidth = 0;

                    questionTable.AddCell(imageCell);
                    questionTable.AddCell(Convert.ToString(i + 1));
                    questionTable.AddCell(" ");
                    questionTable.AddCell(CreatePdfCell(questionDetails[i].Question.ToString(), 0, 0, 0, 1, 1, "ItalicStyle"));

                    PdfPTable answerHeader = new PdfPTable(2);
                    answerHeader.WidthPercentage = 90;
                    answerHeader.DefaultCell.BorderWidth = 0;
                    int[] answerHeaderColumnWidth = { 15, 80 };
                    answerHeader.SetWidths(answerHeaderColumnWidth);
                    answerHeader.AddCell(CreatePdfCell("Answer", 0, 0, 0, 1, 1, "TestDetailBlackBold"));

                    QuestionDetail questionDetailAnswer = new ReportBLManager().GetSingleQuestionDetails(testKey, questionDetails[i].QuestionKey.ToString(), attemptID, candidateSessionKey);
                    choiceTable = new PdfPTable(6);
                    int[] choiceTableColumnWidth = { 4, 5, 50, 4, 5, 50 };
                    choiceTable.SetWidths(choiceTableColumnWidth);
                    choiceTable.DefaultCell.BorderWidth = 0;

                    int oddRow = questionDetailAnswer.AnswerChoices.Count % 2;
                    for (int j = 0; j < questionDetailAnswer.AnswerChoices.Count; j++)
                    {
                        string answerImagePath = string.Empty;
                        iTextSharp.text.Image answerImage = null;
                        PdfPCell answerImageCell = null;
                        if (questionDetailAnswer.AnswerChoices[j].IsCorrect)
                        {
                            answerImagePath = serverPath + "/App_Themes/DefaultTheme/Images/correct_answer.png";
                            answerImage = iTextSharp.text.Image.GetInstance(answerImagePath);
                            answerImage.ScaleToFit(10, 10);
                            answerImageCell = new PdfPCell(answerImage);
                            answerImageCell.Border = 0;
                            choiceTable.AddCell(answerImageCell);
                        }
                        else
                        {
                            answerImagePath = serverPath + "/App_Themes/DefaultTheme/Images/wrong_answer.png";
                            answerImage = iTextSharp.text.Image.GetInstance(answerImagePath);
                            answerImage.ScaleToFit(10, 10);
                            answerImageCell = new PdfPCell(answerImage);
                            answerImageCell.Border = 0;
                            choiceTable.AddCell(answerImageCell);
                        }

                        if (questionDetailAnswer.AnswerChoices[j].ChoiceID == questionDetailAnswer.AnswerChoices[j].IsCandidateCorrect)
                        {
                            if (questionDetailAnswer.AnswerChoices[j].IsCorrect)
                            {
                                answerImagePath = serverPath + "/App_Themes/DefaultTheme/Images/blue_correct_answer.png";
                                answerImage = iTextSharp.text.Image.GetInstance(answerImagePath);
                                answerImage.ScaleToFit(10, 10);
                                answerImageCell = new PdfPCell(answerImage);
                                answerImageCell.Border = 0;
                                choiceTable.AddCell(answerImageCell);
                            }
                            else
                            {
                                answerImagePath = serverPath + "/App_Themes/DefaultTheme/Images/blue_wrong_answer.png";
                                answerImage = iTextSharp.text.Image.GetInstance(answerImagePath);
                                answerImage.ScaleToFit(10, 10);
                                answerImageCell = new PdfPCell(answerImage);
                                answerImageCell.Border = 0;
                                choiceTable.AddCell(answerImageCell);
                            }
                        }
                        else
                        {
                            answerImageCell = new PdfPCell(new Phrase(" "));
                            answerImageCell.Border = 0;
                            choiceTable.AddCell(answerImageCell);
                        }
                        choiceTable.AddCell(CreatePdfCell(questionDetailAnswer.AnswerChoices[j].Choice.ToString(), 0, 0, 0, 0, 0, "TestDetailPdfSmallItemStyle"));
                        if ((j + 1) == questionDetailAnswer.AnswerChoices.Count)
                        {
                            if (oddRow == 1)
                            {
                                choiceTable.AddCell(" ");
                                choiceTable.AddCell(" ");
                                choiceTable.AddCell(" ");
                                choiceTable.AddCell(" ");
                                choiceTable.AddCell(" ");
                            }
                        }
                    }
                    answerHeader.AddCell(choiceTable);

                    tableDetail = new PdfPTable(8);
                    tableDetail.WidthPercentage = 90;
                    tableDetail.DefaultCell.BorderWidth = 0;
                    int[] tabledetailcolumnwidth = { 10, 12, 10, 12, 10, 12, 10, 12 };
                    tableDetail.SetWidths(tabledetailcolumnwidth);

                    tableDetail.AddCell(CreatePdfCell("Category", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].CategoryName.ToString(), 0, 0, 0, 0, 0, "TestDetailBlue"));
                    tableDetail.AddCell(CreatePdfCell("Subject", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].SubjectName.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                    tableDetail.AddCell(CreatePdfCell("Test Area", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].TestAreaName.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                    tableDetail.AddCell(CreatePdfCell("Complexity", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].Complexity.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));

                    tableDetail.AddCell(CreatePdfCell("Absolute Score", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(questionDetails[i].AbsoluteScore).ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));
                    tableDetail.AddCell(CreatePdfCell("Relative Score", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(questionDetails[i].RelativeScore).ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));
                    tableDetail.AddCell(CreatePdfCell("Time Taken", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    decimal timetaken = 0;
                    if (!Utility.IsNullOrEmpty(questionDetails[i].TimeTaken))
                    {
                        timetaken = decimal.Parse(questionDetails[i].TimeTaken.ToString());
                        string TotalTimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds(decimal.ToInt32(timetaken));
                        tableDetail.AddCell(CreatePdfCell(TotalTimeTaken, 0, 0, 0, 1, 1, "TestDetailBlue"));
                    }
                    else
                        tableDetail.AddCell(CreatePdfCell(timetaken.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));

                    tableDetail.AddCell(CreatePdfCell("Skipped", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].Skipped.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));

                    tableDetail.AddCell(CreatePdfCell("Question Weightage", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(questionDetails[i].QuestionWeightage).ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");

                    PdfPCell emptyCell = new PdfPCell(new Phrase(" "));
                    emptyCell.BorderWidth = 0;

                    PdfPCell questionAnswerCell = new PdfPCell();
                    questionAnswerCell.BorderWidth = 0;
                    questionAnswerCell.Colspan = 2;
                    questionAnswerCell.AddElement(answerHeader);

                    questionTable.AddCell(emptyCell);
                    questionTable.AddCell(emptyCell);
                    questionTable.AddCell(questionAnswerCell);

                    PdfPCell questionTypeCell = new PdfPCell();
                    questionTypeCell.BorderWidth = 0;
                    questionTypeCell.Colspan = 2;
                    questionTypeCell.AddElement(tableDetail);

                    questionTable.AddCell(emptyCell);
                    questionTable.AddCell(emptyCell);
                    questionTable.AddCell(questionTypeCell);

                    //Cell That Add Line after Each Question
                    PdfPCell LineCell = new PdfPCell();
                    LineCell.BorderWidth = 2;
                    LineCell.BorderWidthTop = 0;
                    LineCell.BorderWidthLeft = 0;
                    LineCell.BorderWidthRight = 0;
                    LineCell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    LineCell.Colspan = 1;

                    questionMainTable.AddCell(questionTable);
                    questionMainTable.AddCell(LineCell);
                }
            }
            return questionMainTable;
        }

        public PdfPTable OpenTextTestResultSummaryQuestions(string serverPath,
           List<QuestionDetail> questionDetails, string testKey, int attemptID,
           string candidateSessionKey)
        {
            PdfPTable tableDetail = null;
            PdfPTable questionTable = null;
            PdfPTable questionMainTable = null;
            //PdfPTable choiceTable = null;
            if (questionDetails == null)
            {
                tableDetail = new PdfPTable(1);
                tableDetail.WidthPercentage = 100;
                tableDetail.AddCell(CreatePdfCell("No data found to display", 0, 0, 0.0001f, 2, 10, "CaptionStyle"));
            }
            else
            {
                questionMainTable = new PdfPTable(1);
                questionMainTable.WidthPercentage = 90;
                questionMainTable.DefaultCell.BorderWidth = 0;
                questionMainTable.SpacingBefore = 30f;

                for (int i = 0; i < questionDetails.Count; i++)
                {
                    questionTable = new PdfPTable(4);
                    questionTable.WidthPercentage = 90;
                    questionTable.DefaultCell.BorderWidth = 0;
                    int[] tblColumnWidth = { 3, 3, 1, 77 };
                    questionTable.SetWidths(tblColumnWidth);

                    string questionuimagepath = serverPath + "/App_Themes/DefaultTheme/Images/question_icon_popup.gif";
                    iTextSharp.text.Image logoImage = iTextSharp.text.Image.GetInstance(questionuimagepath);
                    logoImage.ScaleToFit(18, 20);
                    PdfPCell imageCell = null;
                    imageCell = new PdfPCell(logoImage);
                    imageCell.Border = 0;
                    imageCell.Padding = 5f;
                    questionTable.DefaultCell.BorderWidth = 0;

                    questionTable.AddCell(imageCell);
                    questionTable.AddCell(Convert.ToString(i + 1));
                    questionTable.AddCell(" ");
                    questionTable.AddCell(CreatePdfCell(questionDetails[i].Question.ToString(), 0, 0, 0, 1, 1, "ItalicStyle"));
                    questionTable.AddCell(" ");

                    PdfPTable answerHeader = new PdfPTable(2);
                    answerHeader.WidthPercentage = 90;
                    answerHeader.DefaultCell.BorderWidth = 0;
                    int[] answerHeaderColumnWidth = { 10, 80 };
                    answerHeader.SetWidths(answerHeaderColumnWidth);

                    QuestionDetail questionDetailAnswer = new ReportBLManager().GetSingleQuestionDetailsOpenText
                          (testKey, questionDetails[i].QuestionKey.ToString(), attemptID, candidateSessionKey);

                    answerHeader.AddCell(CreatePdfCell("Answer Reference", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    answerHeader.AddCell(CreatePdfCell(questionDetailAnswer.QuestionAttribute.AnswerReference == null ? "" : questionDetailAnswer.QuestionAttribute.AnswerReference.ToString(), 0, 0, 0, 1, 1, "ItalicStyle"));
                   // answerHeader.AddCell(" ");
                    answerHeader.AddCell(CreatePdfCell("Answer", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    answerHeader.AddCell(CreatePdfCell(questionDetailAnswer.QuestionAttribute.Answer == null ? "" : questionDetailAnswer.QuestionAttribute.Answer.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                    answerHeader.AddCell(" ");
                    answerHeader.AddCell(" ");
                    //answerHeader.AddCell(CreatePdfCell("Answer", 0, 0, 0, 1, 1, "TestDetailBlackBold"));       

                    tableDetail = new PdfPTable(8);
                    tableDetail.WidthPercentage = 90;
                    tableDetail.DefaultCell.BorderWidth = 0;
                    int[] tabledetailcolumnwidth = { 10, 12, 10, 12, 10, 12, 10, 12 };
                    tableDetail.SetWidths(tabledetailcolumnwidth);

                    tableDetail.AddCell(CreatePdfCell("Category", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].CategoryName.ToString(), 0, 0, 0, 0, 0, "TestDetailBlue"));
                    tableDetail.AddCell(CreatePdfCell("Subject", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].SubjectName.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                    tableDetail.AddCell(CreatePdfCell("Test Area", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].TestAreaName.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                    tableDetail.AddCell(CreatePdfCell("Complexity", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].Complexity.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));

                    tableDetail.AddCell(CreatePdfCell("Marks", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(questionDetails[i].QuestionAttribute.Marks).ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));
                    tableDetail.AddCell(CreatePdfCell("Marks Scored", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    tableDetail.AddCell(CreatePdfCell(Convert.ToDecimal(questionDetails[i].QuestionAttribute.MarksObtained).ToString(), 0, 0, 0, 1, 1, "TestDetailBlueDecimel"));
                    tableDetail.AddCell(CreatePdfCell("Time Taken", 0, 0, 0, 1, 1, "TestDetailBlackBold"));
                    decimal timetaken = 0;
                    if (!Utility.IsNullOrEmpty(questionDetails[i].TimeTaken))
                    {
                        timetaken = decimal.Parse(questionDetails[i].TimeTaken.ToString());
                        string TotalTimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds(decimal.ToInt32(timetaken));
                        tableDetail.AddCell(CreatePdfCell(TotalTimeTaken, 0, 0, 0, 1, 1, "TestDetailBlue"));
                    }
                    else
                        tableDetail.AddCell(CreatePdfCell(timetaken.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));

                    tableDetail.AddCell(CreatePdfCell("Skipped", 0, 0, 0, 1, 1, "PdffontTestDetailBlackSmall"));
                    tableDetail.AddCell(CreatePdfCell(questionDetails[i].Skipped.ToString(), 0, 0, 0, 1, 1, "TestDetailBlue"));
                   
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");
                    tableDetail.AddCell("");

                    PdfPCell emptyCell = new PdfPCell(new Phrase(" "));
                    emptyCell.BorderWidth = 0;

                    PdfPCell questionAnswerCell = new PdfPCell();
                    questionAnswerCell.BorderWidth = 0;
                    questionAnswerCell.Colspan = 2;
                    questionAnswerCell.AddElement(answerHeader);

                    //questionTable.AddCell(emptyCell);
                    questionTable.AddCell(emptyCell);
                    questionTable.AddCell(questionAnswerCell);

                    PdfPCell questionTypeCell = new PdfPCell();
                    questionTypeCell.BorderWidth = 0;
                    questionTypeCell.Colspan = 2;
                    questionTypeCell.AddElement(tableDetail);

                    questionTable.AddCell(emptyCell);
                    questionTable.AddCell(emptyCell);
                    questionTable.AddCell(questionTypeCell);

                    //Cell That Add Line after Each Question
                    PdfPCell LineCell = new PdfPCell();
                    LineCell.BorderWidth = 2;
                    LineCell.BorderWidthTop = 0;
                    LineCell.BorderWidthLeft = 0;
                    LineCell.BorderWidthRight = 0;
                    LineCell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    LineCell.Colspan = 1;

                    questionMainTable.AddCell(questionTable);
                    questionMainTable.AddCell(LineCell);
                }
            }
            return questionMainTable;
        }

        /// <summary>
        /// Method sends the byte array to create pdf file
        /// </summary>
        /// <param name="rawdata"></param>
        /// <param name="logopath"></param>
        /// <param name="candidateReportDetail"></param>
        /// <returns>dataAsByteArray</returns>
        public byte[] GetPostionprofileReview(DataSet rawdata, string logopath,
             CandidateReportDetail candidateReportDetail)
        {
            Document doc = new Document(PDFDocumentWriter.PP_pap,
                PDFDocumentWriter.leftMargin, PDFDocumentWriter.rightMargin,
                PDFDocumentWriter.topMargin, PDFDocumentWriter.bottomMargin);
            MemoryStream memoryStream = new MemoryStream();
            PDFHeader pdfHeader = new PDFHeader();
            pdfHeader.candidateReportDetail = candidateReportDetail;
            PDFDocumentWriter pdfWriter = new PDFDocumentWriter();
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            writer.PageEvent = pdfHeader;
            PdfPTable pageHeader = new PdfPTable(pdfWriter.PageHeader(logopath, null));
            pageHeader.SpacingAfter = 10f;
            doc.Open();
            PdfPTable MainTable = new PdfPTable(1);
            MainTable.DefaultCell.BorderWidth = 0;
            MainTable.AddCell(GetPositionProfileworkflow(rawdata));

            MainTable.AddCell(GetWorkflowSummaryTable(rawdata));
            doc.Add(pageHeader);
            doc.Add(MainTable);
            writer.CloseStream = false;
            doc.Close();
            memoryStream.Position = 0;
            // Convert stream to byte array.
            BinaryReader reader = new BinaryReader(memoryStream);
            byte[] dataAsByteArray = new byte[memoryStream.Length];
            dataAsByteArray = reader.ReadBytes(Convert.ToInt32(memoryStream.Length));
            return dataAsByteArray;
        }

        /// <summary>
        /// Method that add cell used for adding lines
        /// </summary>
        /// <returns></returns>
        public PdfPCell Getlinecell()
        {
            //Cell That Add Line
            PdfPCell LineCell = new PdfPCell();
            LineCell.BorderWidth = 0.5f;
            LineCell.BorderWidthTop = 0;
            LineCell.BorderWidthLeft = 0;
            LineCell.BorderWidthRight = 0;
            LineCell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            LineCell.Colspan = 1;
            return LineCell;
        }

        /// <summary>
        /// Method that return the pdftable that contains the position 
        /// profile workflow review
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns>Maintable</returns>
        public PdfPTable GetPositionProfileworkflow(DataSet rawData)
        {
            // Check for validity of raw data.
            if (rawData == null || rawData.Tables.Count == 0)
                throw new Exception("Raw data cannot be null");
            if (rawData.Tables.Count != TABLES_COUNT)
                throw new Exception("Raw data is invalid");

            //Pdftable that holds the entire content with one cell
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.WidthPercentage = 90;
            Maintable.DefaultCell.BorderWidth = 0;
            //Pdftable that contains the position profile status
            PdfPTable Statustable = new PdfPTable(5);
            Statustable.WidthPercentage = 90;
            Statustable.DefaultCell.BorderWidth = 0;
            int[] tablecolumnwidth = { 20, 25, 10, 10, 25 };
            //Pdftable that contains the position profile name 
            PdfPTable Header = new PdfPTable(1);
            Header.WidthPercentage = 90;
            Header.DefaultCell.BorderWidth = 0;
            //pdftable that header details of workflow review
            PdfPTable Headerdetail = new PdfPTable(5);
            Headerdetail.WidthPercentage = 90;
            Headerdetail.DefaultCell.BorderWidth = 0;
            int[] columnwidth = { 10, 40, 5, 10, 25 };
            //pdftable that contains the recruiter information
            PdfPTable recruiterTable = new PdfPTable(5);
            recruiterTable.WidthPercentage = 90;
            recruiterTable.DefaultCell.BorderWidth = 0;
            DataTable TableHeader = rawData.Tables[HEADER_DETAIL_TABLE_INDEX];
            DataTable recruitersTable = rawData.Tables[RECRUITER_DETAIL_TABLE_INDEX];
            DataTable coOwnersTable = rawData.Tables[CO_OWNER_DETAIL_TABLE_INDEX];
            DataRow dataRow = TableHeader.Rows[0];

            // Variables that hold the details from table rows.
            string status = GetValue(dataRow["POSITION_PROFILE_STATUS_NAME"]);
            string profileName = GetValue(dataRow["POSITION_PROFILE_NAME"]);
            string registeredBy = GetValue(dataRow["REGISTERED_BY_NAME"]);
            string submittalDeadline = GetDateValue(dataRow["SUBMITTAL_DEADLINE"]);
            string dateofRegistration = GetDateValue(dataRow["REGISTRATION_DATE"]);
            string client = GetValue(dataRow["CLIENT"]);
            string clientID = string.Empty;
            string clientName = string.Empty;
            if (!Utility.IsNullOrEmpty(client))
            {
                Regex regex = new Regex(@"(^\d*)");
                Match match = regex.Match(client);

                if (match.Success)
                {
                    clientID = match.Value;
                    clientName = regex.Replace(client, string.Empty);
                    clientName = clientName.Trim(new char[] { '~' });
                }
            }
            string clientContactName = GetValue(dataRow["CLIENT_CONTACT_NAMES"]);
            string clientDepartments = GetValue(dataRow["CLIENT_DEPARTMENTS"]);
            string clientContacts = GetValue(dataRow["CLIENT_CONTACTS"]);
            string additionInformation = GetValue(dataRow["ADDITIONAL_INFORMATION"]);
            string testActionRequired = GetValue(dataRow["TEST_ACTIONS_REQUIRED"]);
            string interviewActionRequired = GetValue(dataRow["INTERVIEW_ACTIONS_REQUIRED"]);
            string actionValue = GetValue(dataRow["GENERAL_ACTIONS_REQUIRED"]);
            //Adding status to the maintable
            Statustable.AddCell(CreatePdfCell("Position Status", 0, 0, 0, 1, 1, "NormalStyle"));
            Statustable.AddCell(CreatePdfCell(status, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
            Statustable.AddCell(" ");
            Statustable.AddCell(" ");
            Statustable.AddCell(" ");
            Maintable.AddCell(Statustable);

            // Wrtiting Profile name  to pdftable
            Header.AddCell(CreatePdfCell(profileName, 0, 0, 0, 1, 1, "OrangeStyle"));
            Header.SpacingAfter = 20f;
            Maintable.AddCell(Header);

            Headerdetail.SpacingAfter = 10f;
            Headerdetail.AddCell(CreatePdfCell("Registered By", 0, 0, 0, 1, 1, "NormalStyle"));
            Headerdetail.AddCell(CreatePdfCell(registeredBy, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
            Headerdetail.AddCell(" ");
            Headerdetail.AddCell(CreatePdfCell("Co-Owners", 0, 0, 0, 1, 1, "NormalStyle"));
            if (coOwnersTable == null || coOwnersTable.Rows.Count == 0)
            {
                Headerdetail.AddCell(CreatePdfCell("Co-Owner(s) not assigned", 0, 0, 0, 1, 1, "PP_MissingValue"));
            }
            else
            {
                PdfPTable coOwnerPdf = new PdfPTable(1);
                coOwnerPdf.DefaultCell.BorderWidth = 0;
                int[] CoOwnerColumnWidth = { 40 };

                // Loop through the co-owners and add as link buttons.
                for (int recruiterRowIndex = 0; recruiterRowIndex < coOwnersTable.Rows.Count; recruiterRowIndex++)
                {
                    DataRow coOwnerRow = coOwnersTable.Rows[recruiterRowIndex];
                    string coOwnerName = GetValue(coOwnerRow["CO_OWNER_NAME"]);
                    // Add a comma literal.
                    if (recruiterRowIndex + 1 < coOwnersTable.Rows.Count)
                    {
                        coOwnerName = coOwnerName + ",";
                    }
                    coOwnerPdf.AddCell(CreatePdfCell(coOwnerName, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                }
                Headerdetail.AddCell(coOwnerPdf);
            }
            Headerdetail.AddCell(CreatePdfCell("Date Of Registration", 0, 0, 0, 1, 1, "NormalStyle"));
            Headerdetail.AddCell(CreatePdfCell(dateofRegistration, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
            Headerdetail.AddCell(" ");
            Headerdetail.AddCell(CreatePdfCell("Submittal Deadline", 0, 0, 0, 1, 1, "NormalStyle"));
            Headerdetail.AddCell(CreatePdfCell(submittalDeadline, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));

            if (!Utility.IsNullOrEmpty(clientName))
            {
                Headerdetail.AddCell(CreatePdfCell(clientName, 0, 0, 0, 1, 1, "TestDetailBlackBold"));
            }
            else
            {
                Headerdetail.AddCell(CreatePdfCell("Client not associated", 0, 0, 0, 1, 1, "PP_MissingValue"));
            }
            if (!Utility.IsNullOrEmpty(clientDepartments))
            {
                // Get comma separated departments.
                string[] departements = clientDepartments.Split(new char[] { '~' });

                int pdfDepartmentCnt = 0;
                PdfPTable departmentPdfTable = new PdfPTable(1);
                departmentPdfTable.DefaultCell.BorderWidth = 0;
                foreach (string departement in departements)
                {
                    if (!Utility.IsNullOrEmpty(departement))
                    {
                        // Get comma separated contact details.
                        string[] departmentInfo = departement.Split(new char[] { '#' });

                        string departmentName = departmentInfo[1];

                        //Adding  comma.
                        if ((departements.Length != 0) && (pdfDepartmentCnt != departements.Length - 1))
                        {
                            departmentName = departmentName + ",";
                        }
                        departmentPdfTable.AddCell(CreatePdfCell(departmentName, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));

                        pdfDepartmentCnt++;
                    }
                }

                Headerdetail.AddCell(departmentPdfTable);
            }
            else
            {
                Headerdetail.AddCell(CreatePdfCell("Client department(s) not associated", 0, 0, 0, 1, 1, "PP_MissingValue"));
            }

            Headerdetail.AddCell(" ");

            if (!Utility.IsNullOrEmpty(clientContacts))
            {
                //Construct table for contacts

                // Get comma separated contacts.
                string[] contacts = clientContacts.Split(new char[] { '~' });

                int pdfContactCnt = 0;
                PdfPTable contactPdfTable = new PdfPTable(1);
                contactPdfTable.DefaultCell.BorderWidth = 0;
                foreach (string contact in contacts)
                {
                    if (!Utility.IsNullOrEmpty(contact))
                    {
                        // Get comma separated contact details.
                        string[] contactInfo = contact.Split(new char[] { '#' });

                        string contactName = contactInfo[1];

                        //Adding  comma.
                        if ((contacts.Length != 0) && (pdfContactCnt != contacts.Length - 1))
                        {
                            contactName = contactName + ",";
                        }
                        contactPdfTable.AddCell(CreatePdfCell(contactName, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));

                        pdfContactCnt++;
                    }
                }
                Headerdetail.AddCell(contactPdfTable);
            }
            else
            {
                Headerdetail.AddCell(CreatePdfCell("Client contact(s) not associated", 0, 0, 0, 1, 1, "PP_MissingValue"));
            }

            Headerdetail.AddCell(" ");

            Maintable.AddCell(Headerdetail);


            //Adding recruiter details
            recruiterTable.SpacingAfter = 10f;
            recruiterTable.AddCell(CreatePdfCell("Recruiters", 0, 0, 0, 1, 1, "NormalStyle"));

            PdfPTable recruiterPdfTable = new PdfPTable(1);
            recruiterPdfTable.DefaultCell.BorderWidth = 0;
            if (recruitersTable == null || recruitersTable.Rows.Count == 0)
            {
                recruiterTable.AddCell(CreatePdfCell("Recruiter(s) not assigned", 0, 0, 0, 1, 1, "PP_MissingValue"));
            }
            else
            {
                int recruitersCount = recruitersTable.Rows.Count;
                int lastCount = recruitersCount - 1;
                for (int recruiterRowIndex = 0; recruiterRowIndex < recruitersTable.Rows.Count; recruiterRowIndex++)
                {
                    DataRow recruiterRow = recruitersTable.Rows[recruiterRowIndex];
                    string recruiterName = GetValue(recruiterRow["RECRUITER_NAME"]);
                    //Adding  comma.
                    if ((recruitersCount != 0) && (recruiterRowIndex != lastCount))
                    {
                        recruiterName = recruiterName + ",";
                    }
                    recruiterPdfTable.AddCell(CreatePdfCell(recruiterName, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                    //Headerdetail.AddCell(CreatePdfCell(recruiterName ,0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                }
                recruiterTable.AddCell(recruiterPdfTable);
            }

            recruiterTable.AddCell(" ");
            recruiterTable.AddCell(" ");
            recruiterTable.AddCell(" ");

            Maintable.AddCell(recruiterTable);


            //Creating segment table from raw data with SEGMENT_DETAIL_TABLE_INDEX table
            DataTable SegmentTable = rawData.Tables[SEGMENT_DETAIL_TABLE_INDEX];
            Maintable.AddCell(GetsegmentDetail(SegmentTable));
            Maintable.AddCell(" ");
            Maintable.AddCell(CreatePdfCell("Actions Required", 0, 0, 0, 1, 1, "PP_gridcolumnheader_italic"));
            Maintable.AddCell(Getlinecell());
            if ((Utility.IsNullOrEmpty(actionValue)) && (Utility.IsNullOrEmpty(testActionRequired))
                && (Utility.IsNullOrEmpty(interviewActionRequired)))
            {
                Maintable.AddCell(CreatePdfCell("No actions available", 0, 0, 0, 1, 1, "PP_MissingValue"));
            }
            if (!Utility.IsNullOrEmpty(actionValue))
            {
                Maintable.AddCell(CreatePdfCell(actionValue, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            if (!Utility.IsNullOrEmpty(testActionRequired))
            {
                Maintable.AddCell(CreatePdfCell(testActionRequired, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            if (!Utility.IsNullOrEmpty(interviewActionRequired))
            {
                Maintable.AddCell(CreatePdfCell(interviewActionRequired, 0, 0, 0, 1, 1, "SmallItemStyle_PP_italic"));
            }
            return Maintable;
        }

        /// <summary>
        /// Method that return pdftable that contains the segment details
        /// </summary>
        /// <param name="segmentsTable"></param>
        /// <returns></returns>
        public PdfPTable GetsegmentDetail(DataTable segmentsTable)
        {
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.WidthPercentage = 90;
            Maintable.DefaultCell.BorderWidth = 0;
            if (segmentsTable.Rows == null || segmentsTable.Rows.Count == 0)
            {
                Maintable.AddCell(CreatePdfCell("No segments available", 0, 0, 0, 1, 1, "PP_MissingValue"));
                return Maintable;
            }
            for (int rowIndex = 0; rowIndex < segmentsTable.Rows.Count; rowIndex++)
            {
                // Identify segment type.
                int segmentID = GetIntegerValue(segmentsTable.Rows[rowIndex]["SEGMENT_ID"]);
                switch (segmentID)
                {
                    case POSITION_SEGMENT_ID:
                        string PP_xml = segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString();
                        XmlDocument document = new XmlDocument();
                        document.LoadXml(PP_xml);
                        XmlNode positionNameNode = document.SelectSingleNode("ClientPositionDetails//PositionName");
                        XmlNode noOfPositionNode = document.SelectSingleNode("ClientPositionDetails//NumberOfPositions");
                        string targetedStartDate = GetDateValue(document.SelectSingleNode("ClientPositionDetails//TargetedStartDate").InnerText);
                        string natureOfPosition = natureOfPosition = GetValue(document.SelectSingleNode("ClientPositionDetails//NatureOfPosition").InnerText);
                        string Position_name = GetValue(positionNameNode.InnerText.ToString());
                        string No_ofposition = GetValue(noOfPositionNode.InnerText.ToString());

                        PdfPTable PP_headertable = new PdfPTable(1);
                        PP_headertable.WidthPercentage = 90;
                        PP_headertable.AddCell(CreatePdfCell("Position Details", 0, 0, 0, 1, 1, "PP_blueheader"));
                        PP_headertable.DefaultCell.BorderWidth = 0;
                        Maintable.AddCell(PP_headertable);
                        Maintable.AddCell(Getlinecell());

                        PdfPTable PP_DetailsTable = new PdfPTable(6);
                        PP_DetailsTable.DefaultCell.BorderWidth = 0;
                        PP_DetailsTable.WidthPercentage = 90;
                        int[] PP_Details_columnwidth = { 20, 10, 20, 10, 20, 10 };
                        PP_DetailsTable.AddCell(CreatePdfCell("No. Of Positions", 0, 0, 0, 1, 1, "NormalStyle"));
                        PP_DetailsTable.AddCell(CreatePdfCell(No_ofposition, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                        PP_DetailsTable.AddCell(CreatePdfCell("Targeted Start Date", 0, 0, 0, 1, 1, "NormalStyle"));
                        PP_DetailsTable.AddCell(CreatePdfCell(targetedStartDate, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                        PP_DetailsTable.AddCell(CreatePdfCell("Nature Of Position", 0, 0, 0, 1, 1, "NormalStyle"));
                        PP_DetailsTable.AddCell(CreatePdfCell(natureOfPosition, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                        Maintable.AddCell(PP_DetailsTable);
                        break;

                    case TECHNICAL_SKILLS_SEGMENT_ID:
                        string techXml = segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString();
                        XmlDocument techDocument = new XmlDocument();
                        techDocument.LoadXml(techXml);
                        XmlNodeList technicalSkillRows = techDocument.SelectNodes("TechnicalSkillDetails/TechnicalSkillDetail");
                        XmlNode totalYearsNode = techDocument.SelectSingleNode("TechnicalSkillDetails//TotalITExperience");
                        string Totalyear_experience = GetValue(totalYearsNode.InnerText);

                        PdfPTable techskillsHeader = new PdfPTable(1);
                        techskillsHeader.WidthPercentage = 90;
                        techskillsHeader.AddCell(CreatePdfCell("Technical Skills", 0, 0, 0, 1, 1, "PP_blueheader"));
                        Maintable.AddCell(" ");
                        Maintable.AddCell(techskillsHeader);
                        Maintable.AddCell(Getlinecell());


                        PdfPTable Table_Experience = new PdfPTable(2);
                        Table_Experience.DefaultCell.BorderWidth = 0;
                        Table_Experience.WidthPercentage = 90;
                        int[] Table_Experience_columnwidth = { 70, 20 };
                        Table_Experience.AddCell(CreatePdfCell("Total Years Of IT Experience Required", 0, 0, 0, 1, 1, "NormalStyle"));
                        Table_Experience.AddCell(CreatePdfCell(Totalyear_experience, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                        Maintable.AddCell(Table_Experience);

                        PdfPTable Gridtable_header = new PdfPTable(5);
                        Gridtable_header.SpacingAfter = 10f;
                        Gridtable_header.WidthPercentage = 90;
                        int[] Gridtable_header_columnwidth = { 30, 10, 10, 30, 10 };
                        Gridtable_header.AddCell(CreatePdfCell("Skill Category/Name", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Gridtable_header.AddCell(CreatePdfCell("Years Required", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Gridtable_header.AddCell(CreatePdfCell("Skill Level", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Gridtable_header.AddCell(CreatePdfCell("Mandatory/Desired", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Gridtable_header.AddCell(CreatePdfCell("Weightage", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Maintable.AddCell(Gridtable_header);
                        for (int nodeIndex = 0; nodeIndex < technicalSkillRows.Count; nodeIndex++)
                        {
                            string Categoryname = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("CategoryName").InnerText) + " - " +
                                                  GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("SkillName").InnerText);
                            string YearsRequired = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("NoOfYearsExp").InnerText);
                            string Skilllevel = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("SkillRequired").InnerText);
                            string mandatory = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("IsMandatory").InnerText);
                            string weightagevalue = GetValue(technicalSkillRows.Item(nodeIndex).SelectSingleNode("Weightage").InnerText);
                            string Weightage = string.Empty;
                            if (!Utility.IsNullOrEmpty(weightagevalue))
                            {
                                Weightage = weightagevalue + " %";
                            }

                            PdfPTable Gridtable = new PdfPTable(5);
                            Gridtable.WidthPercentage = 90;
                            int[] Gridtable_columnwidth = { 30, 10, 10, 30, 10 };
                            Gridtable.AddCell(CreatePdfCell(Categoryname, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Gridtable.AddCell(CreatePdfCell(YearsRequired, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Gridtable.AddCell(CreatePdfCell(Skilllevel, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Gridtable.AddCell(CreatePdfCell(mandatory, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Gridtable.AddCell(CreatePdfCell(Weightage, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Maintable.AddCell(Gridtable);
                        }

                        break;

                    case VERTICALS_SEGMENT_ID:
                        string Verticals_xml = segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString();
                        PdfPTable segmentname_vertical = new PdfPTable(1);
                        segmentname_vertical.WidthPercentage = 90;
                        segmentname_vertical.AddCell(CreatePdfCell("Vertical Background Requirement", 0, 0, 0, 1, 1, "PP_blueheader"));
                        Maintable.AddCell(" ");
                        Maintable.AddCell(segmentname_vertical);
                        Maintable.AddCell(Getlinecell());

                        PdfPTable Gridtable_vertical = new PdfPTable(4);
                        Gridtable_vertical.SpacingAfter = 5f;
                        Gridtable_vertical.WidthPercentage = 90;
                        int[] Gridtable_vertical_columnwidth = { 40, 10, 30, 10 };
                        Gridtable_vertical.AddCell(CreatePdfCell("Vertical / Sub Vertical", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Gridtable_vertical.AddCell(CreatePdfCell("Years Required", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Gridtable_vertical.AddCell(CreatePdfCell("Mandatory/Desired", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Gridtable_vertical.AddCell(CreatePdfCell("Weightage", 0, 0, 0, 1, 1, "PP_gridcolumnheader"));
                        Maintable.AddCell(Gridtable_vertical);
                        XmlDocument Vertical_document = new XmlDocument();
                        Vertical_document.LoadXml(Verticals_xml);
                        XmlNodeList technicalSkillRows_verticals = Vertical_document.SelectNodes("VerticalDetails/VerticalDetail");
                        for (int nodeIndex = 0; nodeIndex < technicalSkillRows_verticals.Count; nodeIndex++)
                        {
                            string Vertical_subvertical = GetValue(technicalSkillRows_verticals.Item(nodeIndex).SelectSingleNode("VerticalSegment").InnerText)
                                + " - " + GetValue(technicalSkillRows_verticals.Item(nodeIndex).SelectSingleNode("SubVerticalSegment").InnerText);
                            string yearsrequired = GetValue(technicalSkillRows_verticals.Item(nodeIndex).SelectSingleNode("NoOfYearsExperience").InnerText);
                            string Mandatory = GetValue(technicalSkillRows_verticals.Item(nodeIndex).SelectSingleNode("IsMandatory").InnerText);
                            string weight = GetValue(technicalSkillRows_verticals.Item(nodeIndex).SelectSingleNode("Weightage").InnerText);
                            string weight_value = string.Empty;
                            if (!Utility.IsNullOrEmpty(weight))
                            {
                                weight_value = weight + " %";
                            }
                            PdfPTable Verticalgrid_row = new PdfPTable(4);
                            Verticalgrid_row.SpacingAfter = 5f;
                            Verticalgrid_row.WidthPercentage = 90;
                            int[] Verticalgrid_row_columnwidth = { 40, 10, 30, 10 };
                            Verticalgrid_row.AddCell(CreatePdfCell(Vertical_subvertical, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Verticalgrid_row.AddCell(CreatePdfCell(yearsrequired, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Verticalgrid_row.AddCell(CreatePdfCell(Mandatory, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Verticalgrid_row.AddCell(CreatePdfCell(weight_value, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                            Maintable.AddCell(Verticalgrid_row);
                        }
                        break;

                    case ROLES_SEGMENT_ID:
                        string Roles_xml = segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString();
                        XmlDocument roles = new XmlDocument();
                        roles.LoadXml(Roles_xml);
                        XmlNode primaryRoleNode = roles.SelectSingleNode("RoleRequirementDetails//PrimaryRole");
                        XmlNode secondaryRoleNode = roles.SelectSingleNode("RoleRequirementDetails//SecondaryRole");
                        string primaryrole = GetValue(primaryRoleNode.InnerText);
                        string secondaryrole = GetValue(secondaryRoleNode.InnerText);
                        PdfPTable Roles_table_header = new PdfPTable(1);
                        Roles_table_header.AddCell(CreatePdfCell("Roles", 0, 0, 0, 1, 1, "PP_blueheader"));
                        Maintable.AddCell(" ");
                        Maintable.AddCell(Roles_table_header);
                        Maintable.AddCell(Getlinecell());

                        PdfPTable Roles_tables = new PdfPTable(4);
                        Roles_tables.WidthPercentage = 100;
                        Roles_tables.DefaultCell.BorderWidth = 0;
                        int[] Roles_tables_columnwidth = { 30, 20, 30, 20 };
                        Roles_tables.AddCell(CreatePdfCell("Primary Role", 0, 0, 0, 1, 1, "NormalStyle"));
                        Roles_tables.AddCell(CreatePdfCell(primaryrole, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                        Roles_tables.AddCell(CreatePdfCell("Secondary Role", 0, 0, 0, 1, 1, "NormalStyle"));
                        Roles_tables.AddCell(CreatePdfCell(secondaryrole, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
                        Maintable.AddCell(Roles_tables);
                        break;
                    case EDUCATION_SEGMENT_ID:
                        string EducationSegment_xml = segmentsTable.Rows[rowIndex]["SEGMENT_DATA"].ToString();

                        string Educationlevel = string.Empty;
                        string Specilization = string.Empty;
                        if (!Utility.IsNullOrEmpty(EducationSegment_xml))
                        {
                            try
                            {
                                // Get details from xml.
                                XmlDocument Education = new XmlDocument();
                                Education.LoadXml(EducationSegment_xml);

                                Educationlevel = GetValue(Education.SelectSingleNode("EducationalDetails//EducationLevel").InnerText);
                                Specilization = GetValue(Education.SelectSingleNode("EducationalDetails//Specialization").InnerText);
                            }
                            catch (Exception exp)
                            {
                                Logger.ExceptionLog(exp);
                            }
                        }
                        Maintable.AddCell("");
                        Maintable.AddCell(GetEducation_details(Educationlevel, Specilization));


                        break;
                }
            }

            return Maintable;
        }

        /// <summary>
        /// Method the that return the pdf table that contains education details
        /// </summary>
        /// <param name="Educationlevel"></param>
        /// <param name="Specilizationlevel"></param>
        /// <returns></returns>
        public PdfPTable GetEducation_details(string Educationlevel, string Specilizationlevel)
        {
            PdfPTable Education_header_main = new PdfPTable(1);
            Education_header_main.WidthPercentage = 90;
            Education_header_main.DefaultCell.BorderWidth = 0;
            Education_header_main.AddCell(CreatePdfCell("Education", 0, 0, 0, 1, 1, "PP_blueheader"));
            Education_header_main.AddCell(Getlinecell());
            PdfPTable Education_table = new PdfPTable(4);
            Education_table.SpacingAfter = 5;
            Education_table.DefaultCell.BorderWidth = 0;
            Education_table.WidthPercentage = 90;
            int[] Education_table_columnwidth = { 30, 20, 20, 20 };
            Education_table.AddCell(CreatePdfCell("Minimum Level Of Education Required", 0, 0, 0, 1, 1, "NormalStyle"));
            string Education_level = string.Empty;
            if (!Utility.IsNullOrEmpty(Educationlevel))
            {
                // Loop through the comma separated level and construct the names.
                foreach (string level in Educationlevel.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    switch (level)
                    {
                        case "1":
                            Education_level = Education_level + ", " + "Masters";
                            break;

                        case "2":
                            Education_level = Education_level + ", " + "Bachelors";
                            break;

                        case "3":
                            Education_level = Education_level + ", " + "Associate";
                            break;

                        case "4":
                            Education_level = Education_level + ", " + "Diploma";
                            break;
                    }
                }
            }
            // Trim the education levels.
            Education_level = Education_level.Trim();
            Education_level = Education_level.Trim(new char[] { ',' });
            Education_level = Education_level.Trim();
            Education_table.AddCell(CreatePdfCell(Education_level, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
            Education_table.AddCell(CreatePdfCell("Specialization", 0, 0, 0, 1, 1, "NormalStyle"));
            Education_table.AddCell(CreatePdfCell(Specilizationlevel, 0, 0, 0, 1, 1, "SmallItemStyle_PP"));
            Education_header_main.AddCell(Education_table);
            return Education_header_main;
        }

        /// <summary>
        /// Method that get the workflow summary
        /// </summary>
        /// <param name="actionDetailTable"></param>
        /// <param name="dataSummaryTable"></param>
        /// <returns></returns>
        public PdfPTable GetWorkflowSummaryTable(DataSet rawData)
        {
            DataTable actionDetailTable = rawData.Tables[ACTION_DETAIL_TABLE_INDEX];
            DataTable dataSummaryTable = rawData.Tables[DATA_SUMMARY_TABLE_INDEX];
            PdfPTable Maintable = new PdfPTable(1);
            Maintable.DefaultCell.BorderWidth = 0;
            PdfPTable Workflow_table = new PdfPTable(1);
            Workflow_table.WidthPercentage = 90;
            Workflow_table.AddCell(CreatePdfCell("Workflow Summary", 0, 0, 0, 1, 1, "PP_gridcolumnheader_italic"));
            Maintable.AddCell("");
            Maintable.AddCell(Workflow_table);
            Maintable.AddCell(Getlinecell());

            if (actionDetailTable.Rows == null || actionDetailTable.Rows.Count == 0)
            {
                Maintable.AddCell(CreatePdfCell("No summary available", 0, 0, 0, 1, 1, "PP_MissingValue"));
                return Maintable;
            }

            if (dataSummaryTable.Rows == null || dataSummaryTable.Rows.Count == 0)
            {
                Maintable.AddCell(CreatePdfCell("No summary available", 0, 0, 0, 1, 1, "PP_MissingValue"));
                return Maintable;
            }

            // Get data summary row.
            DataRow dataSummaryRow = dataSummaryTable.Rows[0];

            if (actionDetailTable == null || actionDetailTable.Rows.Count == 0)
            {
                return Maintable;
            }


            // Loop through the workflow statuses and show the appropriate details.
            foreach (DataRow dataRow in actionDetailTable.Rows)
            {
                // Get action type.
                string actionType = GetValue(dataRow["ACTION_TYPE"]);
                bool actionAvailable = GetValue(dataRow["ACTION_AVAILABLE"]) == "Y" ? true : false;
                bool taskOwnerAvailable = GetValue(dataRow["TASK_OWNER_AVAILABLE"]) == "Y" ? true : false;
                switch (actionType)
                {
                    // Recruiter assignment
                    case "PAT_REC_AS":
                        {
                            Maintable.AddCell(GetRecruiterAssignmentSummaryRow(taskOwnerAvailable, dataSummaryRow, rawData.Tables[RECRUITER_DETAIL_TABLE_INDEX]));
                        }
                        break;

                    // Candidate association.
                    case "PAT_CAN_AS":
                        {
                            Maintable.AddCell(GetCandidateAssociationSummaryRow(actionAvailable, taskOwnerAvailable, dataSummaryRow));
                        }
                        break;

                    // Interview creation.
                    case "PAT_INT_RQ":
                        {
                            Maintable.AddCell(GetInterviewRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow,
                                rawData.Tables[INTERIVIEW_DETAILS_TABLE_INDEX]));
                        }
                        break;

                    // Interview session required.
                    case "PAT_INT_SS":
                        {
                            Maintable.AddCell(GetInterviewSessionRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow,
                                rawData.Tables[INTERIVIEW_SESSION_DETAILS_TABLE_INDEX], rawData.Tables[INTERIVIEW_DETAILS_TABLE_INDEX]));
                        }
                        break;

                    // Interview schedule required.
                    case "PAT_INT_SD":
                        {
                            Maintable.AddCell(GetInterviewScheduleRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow,
                                rawData.Tables[INTERIVIEW_SESSION_DETAILS_TABLE_INDEX]));
                        }
                        break;

                    // Interview assessment
                    case "PAT_INT_AS":
                        {
                            Maintable.AddCell(GetInterviewAssessmentSummaryRow(taskOwnerAvailable, dataSummaryRow,
                                rawData.Tables[INTERIVIEW_SESSION_DETAILS_TABLE_INDEX]));
                        }
                        break;

                    // Test creation.
                    case "PAT_TST_RQ":
                        {
                            Maintable.AddCell(GetTestRequiredSummaryRow(taskOwnerAvailable,
                                dataSummaryRow, rawData.Tables[TEST_DETAILS_TABLE_INDEX]));
                        }
                        break;

                    // Test session required.
                    case "PAT_TST_SS":
                        {
                            Maintable.AddCell(GetTestSessionRequiredSummaryRow(taskOwnerAvailable, dataSummaryRow,
                                rawData.Tables[TEST_SESSION_DETAILS_TABLE_INDEX], rawData.Tables[TEST_DETAILS_TABLE_INDEX]));

                        }
                        break;

                    // Test schedule required.
                    case "PAT_TST_SD":
                        {
                            Maintable.AddCell(GetTestScheduleRequiredSummaryRow(actionAvailable, taskOwnerAvailable,
                                dataSummaryRow, rawData.Tables[TEST_SESSION_DETAILS_TABLE_INDEX]));

                        }
                        break;

                    // Submittal to client.
                    case "PAT_SUB_CL":
                        {
                            Maintable.AddCell(GetCandidateSubmittedSummaryRow(actionAvailable, taskOwnerAvailable, dataSummaryRow));

                        }
                        break;
                }

            }

            return Maintable;
        }

        /// <summary>
        /// Method that check whether data is null or not
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string GetValue(object data)
        {
            if (data == null)
                return string.Empty;

            return data.ToString().Trim();
        }

        #endregion Public Methods


        #region OnlineInterview PDF report functions
        public PdfPTable OnlineInterviewTableHeader(CandidateInterviewSessionDetail sessionDetail, int interviewId,
            string photoPath, int rptType, out DataSet rawData)
        { 
            decimal totalWeightedScore = 0m;
            
            // Get rating summary.
            rawData = new AssessmentSummaryBLManager().
               GetOnlineInterviewAssessorySummary(interviewId);
             
            // Get rating summary table.
            /*if (rawData == null || rawData.Tables[1].Rows.Count == 0)
                summaryTable = new DataTable();
            else
                summaryTable = new OnlineInterviewDataManager().GetRatingSummaryPDFTable(rawData, rptType, out totalScore, out totalWeightedScore);*/

            if(Utility.IsNullOrEmpty(rawData.Tables[5].Rows[0][0]))
                totalWeightedScore = 0;
            else
                totalWeightedScore = Convert.ToDecimal(rawData.Tables[5].Rows[0][0]);

            PdfPTable tableHeader = new PdfPTable(5);
            tableHeader.WidthPercentage = 90;
            int[] tblColumnWidth = { 45, 5, 130, 25, 160 };
            tableHeader.SetWidths(tblColumnWidth);

            iTextSharp.text.Image candidateImage =
                iTextSharp.text.Image.GetInstance(photoPath);

            candidateImage.Alignment = Element.ALIGN_LEFT;
            candidateImage.ScaleToFit(125f, 85f);

            tableHeader.AddCell(PdfImageCell(candidateImage, 4, 0));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.CandidateName, 0, 0, 0, 1, 1, "TitleStyle"));
            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.PositionProfileName, 0, 0, 0, 1, 1, "BlueStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            if (sessionDetail.DateCompleted.ToString() != "1/1/0001 12:00:00 AM")
                tableHeader.AddCell(CreatePdfCell("Interview conducted on " + sessionDetail.DateCompleted.ToString("MMMM dd,yyyy"), 0, 0, 0, 1, 1, "SmallItemStyle"));
            else
                tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "SmallItemStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.ClientName, 0, 0, 0, 1, 1, "ClientNameStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(" ", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(" ", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(" ", 0, 0, 0, 1, 1, "ItemStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            if (!string.IsNullOrEmpty(totalWeightedScore.ToString()))
                tableHeader.AddCell(CreatePdfCell("Total Score : " + string.Format("{0:N2}%", totalWeightedScore), 0, 0, 0, 1, 1, "OrangeStyle"));
            else
                tableHeader.AddCell(CreatePdfCell(" ", 0, 0, 0, 1, 1, "OrangeStyle"));

            tableHeader.AddCell(CreatePdfCell("", 0, 0, 0, 1, 1, "ItemStyle"));
            tableHeader.AddCell(CreatePdfCell(sessionDetail.InterviewTestName, 0, 0, 0, 1, 1, "InteviewNameStyle"));
            return tableHeader;
        }

        public DataTable SummaryTable(DataSet rawData, int rptType)
        {
            DataTable summaryTable = new DataTable();

            decimal totalScore = 0;
            decimal totalWeightedScore = 0;
            if (rawData == null || rawData.Tables[0].Rows.Count == 0)
                summaryTable = new DataTable();
            else
                summaryTable = new OnlineInterviewDataManager().GetRatingSummaryPDFTable(rawData, rptType, out totalScore, out totalWeightedScore);

            return summaryTable;
        }

        public PdfPTable OnlineInterviewTableDetail(DataTable assessmenttable)
        {
            PdfPTable tableDetail = null;


            if (assessmenttable == null || assessmenttable.Rows.Count == 0)
            {
                tableDetail = new PdfPTable(1);
                tableDetail.WidthPercentage = 90;
                tableDetail.AddCell(CreatePdfCell("No assessor summary data found to display", 0, 0, 0.0001f, 2, 10, "CaptionStyle"));
            }
            else
            {
                tableDetail = new PdfPTable(assessmenttable.Columns.Count - 1);
                tableDetail.WidthPercentage = 90;

                int[] tableCellWidth = null;
                string[] exceptCation = { "title", "Score", "Weight", "FLAG", "Weighted Subject Score" };
                List<string> cationList = new List<string>();
                cationList.Add("title");
                cationList.Add("Score");
                cationList.Add("Weight");
                cationList.Add("FLAG");
                cationList.Add("Weighted Subject Score");

                switch (assessmenttable.Columns.Count - 1)
                {
                    case 1:
                        tableCellWidth = new int[] { 3 };
                        break;
                    case 2:
                        tableCellWidth = new int[] { 3, 1 };
                        break; 
                    case 3:
                        tableCellWidth = new int[] { 3, 1, 1 };
                        break;
                    case 4:
                        tableCellWidth = new int[] { 3, 1, 1,1 };
                        break;
                    case 5:
                        tableCellWidth = new int[] { 3, 1, 1,1 ,1};
                        break;
                    case 6:
                        tableCellWidth = new int[] { 3, 1, 1,1 ,1,1};
                        break;
                    case 7:
                        tableCellWidth = new int[] { 3, 1, 1,1 ,1,1,1};
                        break;
                    case 8:
                        tableCellWidth = new int[] { 3, 1, 1,1 ,1,1,1,1};
                        break;
                    case 9:
                        tableCellWidth = new int[] { 3, 1, 1,1 ,1,1,1,1,1};
                        break;
                    case 10:
                        tableCellWidth = new int[] { 3, 1, 1,1 ,1,1,1,1,1};
                        break;
                } 

                tableDetail.SetWidths(tableCellWidth);

                for (int i = 0; i < assessmenttable.Columns.Count - 1; i++)
                {
                    if (i == 0)
                        tableDetail.AddCell(CreatePdfCell(" Subject  ", 0, 0, 0.0001f, 2, 1, "BackgroundWithItemStyle"));
                    else
                    {
                        if (!cationList.Contains(assessmenttable.Columns[i].Caption.ToString()))
                            tableDetail.AddCell(CreatePdfCell(assessmenttable.Columns[i].Caption.ToString(), 0, 0, 0.0001f, 2, 1, "OrangeSmallStyle"));
                        else if (assessmenttable.Columns[i].Caption.ToString() == "Score") //MKNNEW
                        {
                            tableDetail.AddCell(CreatePdfCell(assessmenttable.Columns[i].Caption.ToString(), 0, 0, 0.0001f, 2, 1, "ScoreColumnStyle"));
                        }
                        else
                            tableDetail.AddCell(CreatePdfCell(assessmenttable.Columns[i].Caption.ToString(), 0, 0, 0.0001f, 2, 1, "BackgroundWithItemStyle"));
                    }
                }

                string weightCaption = string.Empty;
                for (int j = 0; j < assessmenttable.Rows.Count; j++)
                {
                    weightCaption = string.Empty;
                    for (int k = 0; k < assessmenttable.Columns.Count - 1; k++)
                    {
                        weightCaption = assessmenttable.Columns[k].Caption.ToString();
                        if (k == 0)
                        {
                            if (Convert.ToString(assessmenttable.Rows[j]["FLAG"]) == "S")
                                tableDetail.AddCell(CreatePdfCell(assessmenttable.Rows[j][k].ToString(), 0, 0, 0.0001f, 1, 3, "CategoryStyle"));
                         
                        }
                        else if (weightCaption == "Score")
                        {
                            tableDetail.AddCell(CreatePdfCell(assessmenttable.Rows[j][k].ToString(), 0, 0, 0.0001f, 2, 1, "ScoreColumnStyle"));
                        }
                        else
                        {
                            if (assessmenttable.Columns.Contains("FLAG"))
                                tableDetail.AddCell(CreatePdfCell(assessmenttable.Rows[j][k].ToString(), 0, 0, 0.0001f, 2, 1, "NormalStyle"));
                        }
                    }
                }
            }

            return tableDetail;
        }

        public PdfPTable OnlineInterviewAssessorScoreComments(DataTable table)
        {
            PdfPTable tableDetail = null;


            if (table == null || table.Rows.Count == 0)
            {
                tableDetail = new PdfPTable(1);
                tableDetail.WidthPercentage = 90;
                tableDetail.AddCell(CreatePdfCell("No assessor data found to display", 0, 0, 0.0001f, 2, 10, "CaptionStyle"));
            }
            else
            {
                tableDetail = new PdfPTable(table.Columns.Count);
                tableDetail.WidthPercentage = 90;

                int[] tableCellWidth = null;

                switch (table.Columns.Count)
                {
                    case 2:
                        tableCellWidth = new int[] { 2, 4 };
                        break;
                    case 3:
                        tableCellWidth = new int[] { 2, 1, 4 };
                        break;
                    case 4:
                        tableCellWidth = new int[] { 4, 1, 1, 1 };
                        break;
                    case 5:
                        tableCellWidth = new int[] { 4, 1, 1, 1, 1 };
                        break;
                }

                tableDetail.SetWidths(tableCellWidth);

                for (int c = 0; c < table.Columns.Count; c++)
                {
                    tableDetail.AddCell(CreatePdfCell(table.Columns[c].Caption, 0, 0, 0.0001f, 2, 1, "BackgroundWithItemStyle"));
                }

                foreach (DataRow dr in table.Rows)
                {
                    for (int c1 = 0; c1 < table.Columns.Count; c1++)
                    {  
                        if (dr[c1].ToString().LastIndexOf('%')> 0)
                            tableDetail.AddCell(CreatePdfCell(dr[c1].ToString(), 0, 0, 0.0001f, 2, 1, "NormalStyle"));
                        else
                            tableDetail.AddCell(CreatePdfCell(dr[c1].ToString(), 0, 0, 0.0001f, 1, 1, "NormalStyle"));
                    }
                }
            }

            return tableDetail;
        }

        public PdfPTable TitleTable(string headerName)
        {
            PdfPTable titleTable = new PdfPTable(1);
            titleTable.WidthPercentage = 90;
            PdfPCell titleCell =CreatePdfCell(headerName, 0, 0, 0.0001f, 1, 1, "TitleStyle1");
            titleCell.BackgroundColor = SmokeBackColor;
            titleCell.Padding = 5;
            titleTable.AddCell(titleCell);
            return titleTable;
        }

        public PdfPTable OnlineInterviewQuestionsRow(DataSet rawData)
        {
            decimal totalWeightedScore=0;
            
            decimal totalScore=0;

            if (rawData == null || rawData.Tables.Count == 0) return null;
            
            int columnsCount = rawData.Tables[0].Rows.Count + 1;

            PdfPTable skillTable = null;
            skillTable = new PdfPTable(columnsCount);
            skillTable.WidthPercentage = 90;

            foreach (DataRow dr in rawData.Tables[1].Rows)
            {
                skillTable.AddCell(CreatePdfCell(dr["SKILL_NAME"].ToString(), 0, columnsCount, 0.0001f, 1, 3, "CategoryStyle"));

                // Get assessor details.
                List<KeyValuePair<string, int>> assessors = new OnlineInterviewDataManager().GetAssessors(rawData);

                skillTable.AddCell(CreatePdfCell(" Assesssor  ", 0, 0, 0.0001f, 2, 1, "BackgroundWithItemStyle"));

                foreach (KeyValuePair<string, int> assessor in assessors)
                {
                    skillTable.AddCell(CreatePdfCell(assessor.Key, 0, 0, 0.0001f, 2, 1, "OrangeSmallStyle"));                 
                } 

                //Get Overall score
                DataTable dtOverallScore = new OnlineInterviewDataManager().GetOverAllRating(rawData, Convert.ToInt32(dr["SKILL_ID"]), "T");

                skillTable.AddCell(CreatePdfCell("Overall Subject Score  ", 0, 0, 0.0001f, 1, 1, "InteviewNameStyle"));

                foreach (DataRow drScore in dtOverallScore.Rows)
                {
                    decimal score =Convert.ToDecimal(drScore["SCORE"]);
                    string _score =string.Empty;
                    if(score>0)
                        _score =string.Format("{0:N2}%", score);
                    else
                        _score ="Not Rated";

                    skillTable.AddCell(CreatePdfCell(_score, 0, 0, 0.0001f, 2, 1, "NormalStyle"));
                }

                //Add question header row
                skillTable.AddCell(CreatePdfCell("Questions", 0, columnsCount, 0.0001f, 1, 3, "OrangeSmallStyleWithoutBackColor"));

                // Get distinct questions for the given subject ID.
                List<KeyValuePair<string, string>> questions = new OnlineInterviewDataManager().
                    GetQuestions(rawData, Convert.ToInt32(dr["SKILL_ID"]));


                foreach (KeyValuePair<string, string> question in questions)
                {

                    skillTable.AddCell(CreatePdfCell(question.Key, 0, 0, 0.0001f,1, 1, "ItalicStyle"));

                    int _index = question.Value.Trim().LastIndexOf("$");
                    string questionValue = question.Value.Substring(0, _index);

                     // against the question rating.
                    foreach (KeyValuePair<string, int> assessor in assessors)
                    {
                        string questionRate =  new OnlineInterviewDataManager().GetQuestionRating(rawData, 
                            Convert.ToInt32(dr["SKILL_ID"]), questionValue, assessor.Value);

                        decimal val = 0;
                        string questionRated = string.Empty;

                        decimal.TryParse(questionRate, out val);
                        if (val > 0)
                            questionRated = string.Format("{0:N2}%", val);
                        else
                            questionRated = "Not Rated";

                        skillTable.AddCell(CreatePdfCell(questionRated, 0, 0, 0.0001f, 2, 1, "NormalStyle"));
                    } 
                }


                //adding assessor specific comments
                skillTable.AddCell(CreatePdfCell("Subject Specific Comments", 0, 0, 0.0001f, 1, 1, "InteviewNameStyle"));

                 // against the question rating.
                foreach (KeyValuePair<string, int> assessor in assessors)
                {    
                    string assessorComments = new OnlineInterviewDataManager().GetSubjectSpecificComments(rawData,
                            Convert.ToInt32(dr["SKILL_ID"]), assessor.Value);

                    skillTable.AddCell(CreatePdfCell(assessorComments, 0, 0, 0.0001f, 1, 1, "BlueStyleSmall"));
                } 

                //Empty Cell
                skillTable.AddCell(CreatePdfCell("\n\n ", 0, columnsCount, 0f, 1, 3, "HeaderStyle"));
            }
             

            return skillTable; 
        }
        #endregion
    }
}