﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AttributeManager.cs
// File that represents the attribute manager utility. This provides attribute
// related functionalities and filtering techniques.

#endregion

#region Directives                                                             

using System.Linq;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Class that represents the attribute manager utility. This provides
    /// attribute related functionalities such as convert attributes to 
    /// specific type attributes, convert specific type attributes to 
    /// attributes, etc.
    /// </summary>
    public class AttributeManager
    {
        #region Public Methods                                                 

        /// <summary>
        /// Method that converts the general attribute type to specific
        /// <see cref="TestArea"/> type.
        /// </summary>
        /// <param name="attributes">
        /// A <see cref="AttributeDetail"/> that holds the general attributes
        /// list.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestArea"/> that holds the test area list.
        /// </returns>
        public List<TestArea> GetTestAreas(List<AttributeDetail> attributes)
        {
            // Check if the list is null or empty.
            if (attributes == null || attributes.Count == 0)
                return null;

            var testAreas =
                from testArea in attributes
                where testArea.AttributeType == Constants.AttributeTypes.TEST_AREA
                select new TestArea(testArea.AttributeID, testArea.AttributeName);

            if (testAreas == null || testAreas.Count() == 0)
                return null;

            return testAreas.ToList<TestArea>();
        }

        /// <summary>
        /// Method that converts the general attribute type to specific
        /// <see cref="Complexity"/> type.
        /// </summary>
        /// <param name="attributes">
        /// A <see cref="AttributeDetail"/> that holds the general attributes
        /// list.
        /// </param>
        /// <returns>
        /// A list of <see cref="Complexity"/> that holds the complexity list.
        /// </returns>
        public List<Complexity> GetComplexities
            (List<AttributeDetail> attributes)
        {
            // Check if the list is null or empty.
            if (attributes == null || attributes.Count == 0)
                return null;

            var complexities = 
                from complexity in attributes 
                where complexity.AttributeType == Constants.AttributeTypes.COMPLEXITY 
                select new Complexity(complexity.AttributeID, complexity.AttributeName);

            if (complexities == null || complexities.Count() == 0)
                return null;

            return complexities.ToList<Complexity>();
        }

        /// <summary>
        /// Method that converts filter and returns the specific attribute from
        /// the source attributes list.
        /// </summary>
        /// <param name="sourceAttributes">
        /// A list of <see cref="AttributeDetail"/> that holds the source 
        /// attributes list.
        /// </param>
        /// <returns>
        /// A <see cref="AttributeDetail"/> that holds the filtered attributes
        /// list.
        /// </returns>
        public List<AttributeDetail> GetAttributes
            (List<AttributeDetail> sourceAttributes, AttributeType attributeType)
        {
            // Check if the list is null or empty.
            if (sourceAttributes == null || sourceAttributes.Count == 0)
                return null;

            var attributes = 
                from attribute in sourceAttributes 
                where attribute.AttributeType == GetAttributeTypeName(attributeType)
                select attribute;

            if (attributes == null || attributes.Count() == 0)
                return null;

            return attributes.ToList<AttributeDetail>();
        }

        #endregion Public Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that returns the attribute type name constant from the 
        /// attribute type enumeration.
        /// </summary>
        /// <param name="attributeType">
        /// A <see cref="AttributeType"/> that holds the attribute type.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the attribute type name.
        /// </returns>
        private string GetAttributeTypeName(AttributeType attributeType)
        {
            switch (attributeType)
            {
                case AttributeType.None:
                    return string.Empty;
                    
                case AttributeType.CertificateType:
                    return Constants.AttributeTypes.CERTIFICATE_TYPE;

                case AttributeType.Complexity:
                    return Constants.AttributeTypes.COMPLEXITY;

                case AttributeType.TestArea:
                    return Constants.AttributeTypes.TEST_AREA;
                    
                case AttributeType.TestReminder:
                    return Constants.AttributeTypes.TEST_REMINDER;

                default:
                    return string.Empty;
            }
        }

        #endregion Private Methods
    }
}
