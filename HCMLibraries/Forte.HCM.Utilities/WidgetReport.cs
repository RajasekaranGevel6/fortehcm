﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.

// WidgetReport.cs
// File that represents Generate the PDF and convert to Byte array.

#endregion End Header

#region Directives                                                             

using System;
using System.IO;
using System.Collections.Generic;

using iTextSharp.text;
using iTextSharp.text.pdf;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using System.Reflection;

#endregion End Directives

namespace Forte.HCM.Utilities
{
   public class WidgetReport
   {
       #region Public Methods                                                  

       /// <summary>
       /// Generate the PDF and convert to Byte array.
       /// </summary>
       /// <param name="pageHeader">
       ///  A <see cref="string"/> that holds the Header Name. 
       /// </param>
       /// <param name="chartImagePath">
       ///  A <see cref="string"/> that holds the ChartImage Path. 
       /// </param>
       /// <param name="testKey">
       ///  A <see cref="string"/> that holds the testKey. 
       /// </param>
       /// <param name="candidateReportDetail1">
       ///  A <see cref="CandidateReportDetail"/> that holds the Candidate Details. 
       /// </param>
       /// <param name="widgetDetailsDictionary">
       ///  A <see cref="List<Dictionary<object, object>>"/> that holds the 
       ///  widget Information 
       /// </param>
       /// <returns></returns>
       public byte[] GetPDFReportByteArray(string pageHeader, string chartImagePath, 
           string testKey, CandidateReportDetail candidateReportDetail1, 
           List<Dictionary<object, object>> widgetDetailsDictionary)
       {
           BaseColor itemColor = new BaseColor(20, 142, 192);
           BaseColor headerColor = new BaseColor(40, 48, 51);
           Document doc = new Document(iTextSharp.text.PageSize.A4, 10, 10, 42, 35);
           MemoryStream memoryStream = new MemoryStream();
           PDFHeader pdfHeader = new PDFHeader();
           pdfHeader.candidateReportDetail = candidateReportDetail1;
           PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
           writer.PageEvent = pdfHeader;
           doc.Open();
           string[] candiadteDetailsString = null;
           if (Utility.IsNullOrEmpty(pageHeader))
               pageHeader = candidateReportDetail1.CandidateNames;
           // Set Styles
           iTextSharp.text.Font fontHeaderStyle = iTextSharp.text.FontFactory.
               GetFont("Arial,sans-serif", 8, iTextSharp.text.Font.BOLD, headerColor);
           iTextSharp.text.Font fontCaptionStyle = iTextSharp.text.FontFactory.
               GetFont("Arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, headerColor);
           iTextSharp.text.Font fontItemStyle = iTextSharp.text.FontFactory.
               GetFont("Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, itemColor);
           iTextSharp.text.Font fontSubHeaderStyle = iTextSharp.text.FontFactory.
               GetFont("Arial,sans-serif", 7, iTextSharp.text.Font.BOLD, itemColor);
           //Write some content
           if (widgetDetailsDictionary != null)
           {

               //Craete instance of the pdf table and set the number of column in that table
               //How add the data from datatable to pdf table
               PdfPTable MainTable = new PdfPTable(1);

               PdfPCell topHeaderCell = new PdfPCell(new Phrase(new Chunk(pageHeader, 
                   fontCaptionStyle)));
               topHeaderCell.HorizontalAlignment = 1;
               topHeaderCell.VerticalAlignment = 1;
               topHeaderCell.Border = 0;
               MainTable.AddCell(topHeaderCell);

               PdfPTable CandidateTable = new PdfPTable(2);
               for (int i = 0; i < widgetDetailsDictionary.Count; i++)
               {
                   int header = 0;
                   PdfPTable pdfTable = new PdfPTable(2);
                   PdfPCell pdfPCell = null;
                   foreach (KeyValuePair<object, object> kvp in widgetDetailsDictionary[i])
                   {
                       header++;
                       if (kvp.Value.ToString().Contains("DataSet"))
                       {
                           if (kvp.Key.ToString().Contains("subjectDataSet"))
                           {
                               //Get the data from the database 
                               TestStatistics testStatisticsInformation = new ReportBLManager().
                                   GetGeneralTestStatisticsControlInformation(testKey);

                               //Check if the data is not null 
                               if (testStatisticsInformation != null)
                               {
                                   PdfPTable PdfSubjectTable = new PdfPTable(3);
                                   pdfTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                       "Subject Details",fontCaptionStyle))));
                                   List<Subject> subjectList = new List<Subject>();
                                   subjectList = testStatisticsInformation.SubjectStatistics;

                                   PdfSubjectTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                       "Category Name", fontSubHeaderStyle))));
                                   PdfSubjectTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                       "Subject Name", fontSubHeaderStyle))));
                                   PdfSubjectTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                       "Question Count", fontSubHeaderStyle))));

                                   for (int j = 0; j < subjectList.Count; j++)
                                   {
                                       PdfSubjectTable.AddCell(new PdfPCell(new Phrase(new 
                                           Chunk(subjectList[j].CategoryName, fontCaptionStyle))));
                                       PdfSubjectTable.AddCell(new PdfPCell(new Phrase(new 
                                           Chunk(subjectList[j].SubjectName, fontCaptionStyle))));
                                       PdfSubjectTable.AddCell(new PdfPCell(new Phrase(new 
                                           Chunk(subjectList[j].QuestionCount.ToString(), 
                                           fontCaptionStyle))));
                                   }
                                   pdfTable.AddCell(PdfSubjectTable);
                               }
                           }
                       }
                       else if (kvp.Value.ToString().Contains(".png"))
                       {
                           PdfPCell imageCell = new PdfPCell();
                           imageCell.Colspan = 2;
                           imageCell.PaddingLeft = 90f;
                           imageCell.PaddingRight = 90f;
                           iTextSharp.text.Image imageCategory = iTextSharp.text.Image.GetInstance(
                               chartImagePath + kvp.Value);
                           imageCell.Image = imageCategory;
                           pdfTable.AddCell(imageCell);
                       }
                       else if (kvp.Value.ToString().Contains("CandidateList"))
                       {
                           int totalRecords = 0;
                           CandidateReportDetail candidateReportDetail = new CandidateReportDetail();

                           List<CandidateStatisticsDetail> candidateStatisticsDetail = null;
                           if (kvp.Value.ToString().Contains("ComparisonCandidateList"))
                           {
                               candidateStatisticsDetail = new ReportBLManager().
                                        GetCandidateReportCandidateStatistics(candidateReportDetail1);
                           }
                           else
                           {
                               candidateStatisticsDetail = new ReportBLManager().
                                   GetComparisonCandidateStatistics(candidateReportDetail1,
                                       out  totalRecords, 1, 1000);
                           }
                           for (int k = 0; k < candidateStatisticsDetail.Count; k++)
                           {
                               pdfTable.AddCell("");
                               pdfTable.AddCell("");
                              // pdfPCell = new PdfPCell(new Phrase(new Chunk("Name", fontCaptionStyle)));
                              // pdfTable.AddCell(pdfPCell);
                              // pdfPCell = new PdfPCell(new Phrase(new Chunk(
                              //      candidateStatisticsDetail[k].FirstName, fontItemStyle)));
                              // pdfTable.AddCell(pdfPCell);
                               candiadteDetailsString = kvp.Key.ToString().Split(',');
                               foreach (string word in candiadteDetailsString)
                               {
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                       word, fontCaptionStyle)));
                                   pdfTable.AddCell(pdfPCell);
                                   switch (word)
                                   {
                                       case "Candidate Session ID":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].CandidateSessionID, fontItemStyle)));
                                           break;
                                       case "Test Date":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].TestDate.ToString(),
                                               fontItemStyle)));
                                           break;
                                       case "Total Time":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].TimeTaken, fontItemStyle)));
                                           break;
                                       case "Average Time Taken":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].AverageTimeTaken,
                                               fontItemStyle)));
                                           break;
                                       case "Absolute Score":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].AbsoluteScore.ToString(),
                                               fontItemStyle)));
                                           break;
                                       case "Relative Score":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].RelativeScore.ToString(),
                                               fontItemStyle)));
                                           break;
                                       case "Percentile":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].Percentile.ToString(),
                                               fontItemStyle)));
                                           break;
                                       case "Answered Correctly":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].AnsweredCorrectly.ToString()+" %",
                                               fontItemStyle)));
                                           break;
                                       case "Interview Date":
                                           //pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                           //    candidateStatisticsDetail[k].InterviewDate, 
                                           //    fontCaptionStyle)));
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               "TBD", fontItemStyle)));
                                           
                                           break;
                                       case "Attempt Number":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].AttemptNumber.ToString(),
                                               fontItemStyle)));
                                           break;
                                       case "Candidate Name":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].FirstName,
                                               fontItemStyle)));
                                           break;
                                       case "Middle Name":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].MiddleName,
                                               fontItemStyle)));
                                           break;
                                       case "Last Name":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].LastName,
                                               fontItemStyle)));
                                           break;
                                       case "Address":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].Address,
                                               fontItemStyle)));
                                           break;
                                       case "Phone":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].Phone,
                                               fontItemStyle)));
                                           break;
                                       case "TestKey":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].TestKey,
                                               fontItemStyle)));
                                           break;
                                       case "Location":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].Location,
                                               fontItemStyle)));
                                           break;
                                       case "E-Mail ID":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].EMailID,
                                               fontItemStyle)));
                                           break;
                                       case "Synopsis":
                                           pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               candidateStatisticsDetail[k].Synopsis,
                                               fontItemStyle)));
                                           break;
                                   }
                                   pdfTable.AddCell(pdfPCell);
                                   //pdfTable.AddCell(CandidateTable);
                               }
                           }
                       }
                       else
                       {
                           if (header == 1)
                           {
                               pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Key.ToString(), 
                                   fontHeaderStyle)));
                               pdfPCell.Colspan = 2;
                               pdfPCell.HorizontalAlignment = 1;
                               pdfTable.AddCell(pdfPCell);
                           }
                           else
                           {
                               pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Key.ToString(), 
                                   fontCaptionStyle)));
                               pdfTable.AddCell(pdfPCell);
                               //pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Value.ToString(),
                               //    fontItemStyle)));
                               //pdfTable.AddCell(pdfPCell);

                               if (kvp.Key.ToString() == "Average Time")
                               {
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(Utility.ConvertSecondsToHoursMinutesSeconds(int.Parse(kvp.Value.ToString().Split('.')[0])),
                                       fontItemStyle)));
                               }
                               else if (kvp.Key.ToString() == "Score Range" || kvp.Key.ToString() == "Interview Date")
                               {
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               "TBD", fontItemStyle)));
                               }
                               else if (kvp.Key.ToString() == "Answered Correctly")
                               {
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(
                                               kvp.Value.ToString() + " %", fontItemStyle)));
                               }
                               else
                               {
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Value.ToString(),
                                   fontItemStyle)));
                               }
                               pdfTable.AddCell(pdfPCell);
                           }
                       }
                   }
                   pdfPCell = new PdfPCell(pdfTable);
                   pdfPCell.PaddingBottom = 15f;
                   pdfPCell.Border = 0;
                   MainTable.AddCell(pdfPCell);
               }
               doc.Add(MainTable);
               writer.CloseStream = false;
               doc.Close();
               memoryStream.Position = 0;
           }
           // Convert stream to byte array.
           BinaryReader reader = new BinaryReader(memoryStream);
           byte[] dataAsByteArray = new byte[memoryStream.Length];
           dataAsByteArray = reader.ReadBytes(Convert.ToInt32(memoryStream.Length));
           return dataAsByteArray;
       }



       public byte[] GetPDFPPByteArray(string pageHeader, List<Dictionary<object, object>> widgetDetailsDictionary, CandidateReportDetail candidateReportDetail1)
       {
           BaseColor itemColor = new BaseColor(20, 142, 192);
           BaseColor headerColor = new BaseColor(40, 48, 51);
           Document doc = new Document(iTextSharp.text.PageSize.A4, 10, 10, 42, 35);
           MemoryStream memoryStream = new MemoryStream();
           PDFHeader pdfHeader = new PDFHeader();
           pdfHeader.candidateReportDetail = candidateReportDetail1;
           PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
           writer.PageEvent = pdfHeader;
           doc.Open();
         //  string[] candiadteDetailsString = null;
           if (Utility.IsNullOrEmpty(pageHeader))
               pageHeader = "te";
           // Set Styles
           iTextSharp.text.Font fontHeaderStyle = iTextSharp.text.FontFactory.
               GetFont("Arial,sans-serif", 8, iTextSharp.text.Font.BOLD, headerColor);
           iTextSharp.text.Font fontCaptionStyle = iTextSharp.text.FontFactory.
               GetFont("Arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, headerColor);
           iTextSharp.text.Font fontItemStyle = iTextSharp.text.FontFactory.
               GetFont("Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, itemColor);
           iTextSharp.text.Font fontSubHeaderStyle = iTextSharp.text.FontFactory.
               GetFont("Arial,sans-serif", 7, iTextSharp.text.Font.BOLD, itemColor);
           //Write some content
           if (widgetDetailsDictionary != null)
           {

               PdfPTable MainTable = new PdfPTable(1);
               PdfPCell topHeaderCell = new PdfPCell(new Phrase(new Chunk(pageHeader,
                   fontCaptionStyle)));
               topHeaderCell.HorizontalAlignment = 1;
               topHeaderCell.VerticalAlignment = 1;
               topHeaderCell.Border = 0;
               MainTable.AddCell(topHeaderCell);

               PdfPTable CandidateTable = new PdfPTable(2);
               for (int i = 0; i < widgetDetailsDictionary.Count; i++)
               {
                   int header = 0;
                   PdfPTable pdfTable = new PdfPTable(1);
                   
                   PdfPCell pdfPCell = new PdfPCell();
                   pdfPCell.Border = 0;
                   pdfPCell.BorderColor = BaseColor.PINK;
                   foreach (KeyValuePair<object, object> kvp in widgetDetailsDictionary[i])
                   {
                       header++;
                       if (header == 1)
                       {
                           pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Key.ToString(),
                               fontHeaderStyle)));
                           pdfPCell.Colspan = 1;
                           pdfPCell.HorizontalAlignment = 1;
                           pdfTable.AddCell(pdfPCell);
                       }
                       else
                       {
                          
                           if (kvp.Key.ToString() == "TechDetails")
                           {
                               List<TechnicalSkillDetails> technicalSkillDetailsList = new List<TechnicalSkillDetails>();
                               technicalSkillDetailsList = (List<TechnicalSkillDetails>) kvp.Value;

                               PdfPTable PdfTechDetailTable = new PdfPTable(9);

                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Skill Category ", fontSubHeaderStyle))));
                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Skill Name ", fontSubHeaderStyle))));
                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "No Of Years Exp Req", fontSubHeaderStyle))));
                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Skill Level Required ", fontSubHeaderStyle))));
                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Mandatory / Desired", fontSubHeaderStyle))));
                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Certification Required", fontSubHeaderStyle))));
                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Testing Required", fontSubHeaderStyle))));
                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Interview Required", fontSubHeaderStyle))));
                               PdfTechDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Weightage", fontSubHeaderStyle))));

                               for (int j = 0; j < technicalSkillDetailsList.Count; j++)
                               {
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].CategoryName, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].SkillName, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].NoOfYearsExp, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].SkillRequired, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].IsMandatory, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].CertificationRequired, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].IsTestingRequired, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].IsInterviewRequired, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(technicalSkillDetailsList[j].Weightage, fontItemStyle)));
                                   PdfTechDetailTable.AddCell(pdfPCell);
                               }
                               //pdfTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                               //   "Technical Details", fontCaptionStyle))));

                            //   pdfTable.ResetColumnCount(1);
                               
                               pdfTable.AddCell(PdfTechDetailTable);
                           }
                           else if (kvp.Key.ToString() == "VerticleDetails")
                           {
                               List<VerticalDetails> verticalDetailsSkillDetailsList = new List<VerticalDetails>();
                               verticalDetailsSkillDetailsList = (List<VerticalDetails>)kvp.Value;

                               PdfPTable PdfVertDetailTable = new PdfPTable(7);
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Vertical Segment", fontSubHeaderStyle))));
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Sub Vertical Segment", fontSubHeaderStyle))));
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "No Of Years Exp Req", fontSubHeaderStyle))));
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Mandatory / Desired", fontSubHeaderStyle))));
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Testing Required", fontSubHeaderStyle))));
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Interview Required", fontSubHeaderStyle))));
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Weightage", fontSubHeaderStyle))));

                               for (int j = 0; j < verticalDetailsSkillDetailsList.Count; j++)
                               {
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(verticalDetailsSkillDetailsList[j].VerticalSegment, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(verticalDetailsSkillDetailsList[j].SubVerticalSegment, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(verticalDetailsSkillDetailsList[j].NoOfYearsExperience, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(verticalDetailsSkillDetailsList[j].IsMandatory, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(verticalDetailsSkillDetailsList[j].IsTestingRequired, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(verticalDetailsSkillDetailsList[j].IsInterviewRequired, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(verticalDetailsSkillDetailsList[j].Weightage, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                               }
                               //pdfTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                               //   "Verticle Details", fontCaptionStyle))));

                             //  pdfTable.ResetColumnCount(1);
                               pdfTable.AddCell(PdfVertDetailTable);
                           }
                           else if (kvp.Key.ToString() == "ClientDetails")
                           {
                               List<ClientContactInformation> clientContactInformations = new List<ClientContactInformation>();
                               clientContactInformations = (List<ClientContactInformation>)kvp.Value;

                               PdfPTable PdfVertDetailTable = new PdfPTable(3);
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Client Name", fontSubHeaderStyle))));
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Department Name", fontSubHeaderStyle))));
                               PdfVertDetailTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                                   "Contact Name", fontSubHeaderStyle))));

                               for (int j = 0; j < clientContactInformations.Count; j++)
                               {
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(clientContactInformations[j].ClientName, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(clientContactInformations[j].DepartmentName, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                                   pdfPCell = new PdfPCell(new Phrase(new Chunk(clientContactInformations[j].ContactName, fontItemStyle)));
                                   PdfVertDetailTable.AddCell(pdfPCell);
                               }

                               //pdfTable.AddCell(new PdfPCell(new Phrase(new Chunk(
                               //   "Client Details", fontCaptionStyle))));

                              // pdfTable.ResetColumnCount(1);
                               pdfTable.AddCell(PdfVertDetailTable);
                           }
                               
                           else
                           {
                               if (kvp.Key.ToString() == "ClientName" || kvp.Key.ToString() == "PositionProfileName")
                                   continue;


                               PdfPTable pdfTable1 = new PdfPTable(2);
                               
                               //pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Key.ToString(),
                               //  fontCaptionStyle)));
                               //pdfPCell.Colspan = 1;
                               //pdfPCell.HorizontalAlignment = 0;
                               //pdfTable.AddCell(pdfPCell);

                               //pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Value.ToString(),
                               //fontItemStyle)));
                               //pdfPCell.Colspan = 1;
                               //pdfPCell.HorizontalAlignment = 0;
                               //pdfTable.AddCell(pdfPCell);

                               //pdfTable.ResetColumnCount(2);
                               pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Key.ToString(),
                                   fontCaptionStyle)));
                              

                               pdfTable1.AddCell(pdfPCell);
                               pdfPCell = new PdfPCell(new Phrase(new Chunk(kvp.Value.ToString(),
                                   fontItemStyle)));
                              
                               pdfTable1.AddCell(pdfPCell);
                               pdfTable.SpacingAfter = 0f;
                               pdfTable.SpacingBefore = 0f;
                               pdfTable.AddCell(pdfTable1);
                           }
                         //  pdfTable.AddCell(pdfPCell);
                       }
                   }
                   pdfPCell = new PdfPCell(pdfTable);
                   pdfPCell.PaddingBottom = 15f;
                   pdfPCell.Border = 0;
                   MainTable.AddCell(pdfPCell);
               }
           
               doc.Add(MainTable);
               writer.CloseStream = false;
               doc.Close();
               memoryStream.Position = 0;
           }
           // Convert stream to byte array.
           BinaryReader reader = new BinaryReader(memoryStream);
           byte[] dataAsByteArray = new byte[memoryStream.Length];
           dataAsByteArray = reader.ReadBytes(Convert.ToInt32(memoryStream.Length));
           return dataAsByteArray;
       }

       #endregion End Public Methods
   }
}
