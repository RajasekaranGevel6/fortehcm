﻿using System.IO;
using System.Windows.Forms;

namespace Forte.HCM.Utilities
{
    public class RTFDocumentReader
    {
        /// <summary>
        /// Represents the method that is reads and returns the text contents
        /// of an RTF file.
        /// </summary>
        /// <param name="fileName">
        /// A<see cref="string"/> that holds the file name.
        /// </param>
        /// <returns>
        /// A<see cref="string"/> that holds the textual file contents.
        /// </returns>
        public string GetPreviewText(string fileName)
        {
            // Create a rich text box object, that helps to convert RTF 
            // to TXT format.
            RichTextBox richTextBox = new RichTextBox();

            // Get the contents of the RTF file.
            string fileContents = File.ReadAllText(fileName);

            // Set the RTF contents to the rich text box.
            richTextBox.Rtf = fileContents;

            // Get the text from the rich text box.
            return richTextBox.Text;
        }
    }
}
