﻿#region Header                                                                 

using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

#endregion Header

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Class that encrypts and decrypts a string
    /// using <see cref="System.Security.Cryptography.RijndaelManaged"/>
    /// </summary>
    public class EncryptAndDecrypt
    {

        #region Variables                                                      

        /// <summary>
        /// A <see cref="System.string"/> that holds the salt value for
        /// encryption 
        /// Note:- The value length should exactly 5 characters
        /// </summary>
        private const string SALTVALUE = "FORT";
        /// <summary>
        /// A <see cref="System.string"/> that holds the value for the 
        /// encryptor symmetric algorithm
        /// Note:- The value length should exactly 16 characters
        /// </summary>
        private const string STRINGIVVALUE = "FORTE-HCMENCRYPTI";
        /// <summary>
        /// A <see cref="System.string"/> that holds the Password 
        /// for to generate key
        /// Note:- The value length should exactly 5 characters
        /// </summary>
        private const string PASSWORD = "SRALT";
        /// <summary>
        /// A <see cref="System.String"/> that holds the hash
        /// algorithm to generate key
        /// Note:- the supported Hash algorithm form <see cref="System.Security.Cryptography.PasswordDeriveBytes"/>
        /// are MD5,SHA1,SHA256,SHA384,SHA512,HMACSHA1
        /// </summary>
        private const string HASHALGORITHMNAME = "SHA1";

        #endregion Variables

        #region Constructor                                                   

        public EncryptAndDecrypt()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #endregion Constructor

        #region Public Methods                                                 

        public string EncryptString(string Text)
        {
            return _encrypt(ref Text);
        }

        public string DecryptString(string EncryptedText)
        {
            return _Decrypt(ref EncryptedText);
        }

        #endregion Public Methods

        #region Private Methods                                                

        #region Encrypt Methods                                                

        private string _encrypt(ref string Text)
        {
            if ((Text == "") || (Text == null))
                throw new Exception("Please Provide Some Text to Encrypt");
            byte[] bytSalt = null;
            byte[] bytText = null;
            byte[] bytIV = null;
            byte[] bytKey = null;
            PasswordDeriveBytes objPasswordDerivedBytes = null;
            MemoryStream objMemoryStream = null;
            CryptoStream objCryptoStream = null;
            try
            {
                bytSalt = Encoding.ASCII.GetBytes(SALTVALUE);
                bytText = Encoding.ASCII.GetBytes(Text);
                bytIV = Encoding.ASCII.GetBytes(STRINGIVVALUE);
                objPasswordDerivedBytes = new PasswordDeriveBytes(PASSWORD, bytSalt, HASHALGORITHMNAME, 5);
                RijndaelManaged objRij = new RijndaelManaged();
                bytKey = objPasswordDerivedBytes.GetBytes(objRij.KeySize / 8);
                objMemoryStream = new MemoryStream();
                objCryptoStream = new CryptoStream(objMemoryStream, objRij.CreateEncryptor(bytKey, bytIV), CryptoStreamMode.Write);
                objCryptoStream.Write(bytText, 0, bytText.Length);
                objCryptoStream.FlushFinalBlock();
                objCryptoStream.Flush();
                objCryptoStream.Close();
                return Convert.ToBase64String(objMemoryStream.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objMemoryStream != null)
                    objMemoryStream = null;
                if (objCryptoStream != null)
                    objCryptoStream = null;
                bytSalt = null;
                bytText = null;
                bytIV = null;
                bytKey = null;
            }
        }

        #endregion Encrypt Methods

        #region Decrypt Methods                                                

        private string _Decrypt(ref string EncryptedText)
        {
            if ((EncryptedText == "") || (EncryptedText == null))
                throw new Exception("Please Provide Some Encrypted Text For Decryption");
            byte[] bytIV = null;
            byte[] bytSalt = null;
            byte[] bytText = null;
            byte[] bytKey = null;
            byte[] bytOriginal = null;
            byte[] bytReturn = null;
            int OriginalDataLenght;
            MemoryStream objMememoryStream = null;
            CryptoStream objCryptptoStream = null;
            try
            {
                bytIV = Encoding.ASCII.GetBytes(STRINGIVVALUE);
                bytSalt = Encoding.ASCII.GetBytes(SALTVALUE);
                bytText = Convert.FromBase64String(EncryptedText);
                PasswordDeriveBytes objPass = new PasswordDeriveBytes(PASSWORD, bytSalt, HASHALGORITHMNAME, 5);
                RijndaelManaged objRinjandel = new RijndaelManaged();
                bytKey = objPass.GetBytes(objRinjandel.KeySize / 8);
                objMememoryStream = new MemoryStream(bytText);
                objCryptptoStream = new CryptoStream(objMememoryStream, objRinjandel.CreateDecryptor(bytKey, bytIV), CryptoStreamMode.Read);
                bytOriginal = new byte[bytText.Length];
                OriginalDataLenght = objCryptptoStream.Read(bytOriginal, 0, bytOriginal.Length);
                objCryptptoStream.Close();
                objMememoryStream.Close();
                bytReturn = new byte[OriginalDataLenght];
                for (int i = 0; i < bytOriginal.Length; i++)
                {
                    if (Convert.ToString(bytOriginal[i]) == "0")
                    {
                        break;
                    }
                    bytReturn[i] = bytOriginal[i];
                }
                return Encoding.ASCII.GetString(bytReturn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objMememoryStream != null)
                    objMememoryStream = null;
                if (objCryptptoStream != null)
                    objCryptptoStream = null;
                bytIV = null;
                bytKey = null;
                bytOriginal = null;
                bytReturn = null;
                bytSalt = null;
                bytText = null;
            }
        }

        #endregion Decrypt Methods

        #endregion Private Methods
    }
}