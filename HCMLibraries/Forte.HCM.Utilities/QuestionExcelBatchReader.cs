﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExcelBatchReader.cs
// File that represents the excel reader object that will helps to read
// data from the excel file. The utility also helps to retrieve the list
// of sheets present in the excel file, the count of rows, etc.

#endregion Header

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System.IO;
using System.Data.OleDb;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Represents the class that helps to read data from the excel file.
    /// The utility also helps to retrieve the list of sheets present in 
    /// the excel file, the count of rows, etc
    /// </summary>
    public class QuestionExcelBatchReader : ExcelBatchReader
    {
        #region Public Methods

        /// <summary>
        /// Represents the constructor.
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the excel file name.
        /// </param>
        public QuestionExcelBatchReader(string fileName)
            : base(fileName)
        {

        }


        /// <summary>
        /// Represents the method that returns the list of question for the 
        /// given page number and page size.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the questions.
        /// </returns>
        public List<QuestionDetail> GetQuestions(int pageNumber, int pageSize,
            string userName, string sheetName, int userID, string fileName, int tenantID)
        {
            if (!CheckIsValidExcel(sheetName))
                throw new Exception("");

            //List of questions to store the questions
            List<QuestionDetail> questions = new List<QuestionDetail>();

            string strCommand = string.Empty;

            int lastRecord = pageNumber * pageSize + 1;

            int firstRecord = lastRecord - pageSize + 1;

            int questionID = 0;

            if (pageNumber == 1)
            {
                firstRecord = 1;

                lastRecord = pageSize + 1;

                questionID = 1;
            }
            else
            {
                lastRecord = pageNumber * pageSize + 1;

                firstRecord = lastRecord - pageSize;

                questionID = firstRecord;
            }

            string choice = string.Empty;
            string questionImageName = string.Empty;
            IDataReader questionReader;

            DbCommand selectQuestionCommand;

            try
            {

                //Command to select the question from sheet
                //Questions will be selected based on first record and last record
                strCommand = "select * from [" + sheetName + "A" + firstRecord + ":M" + lastRecord + "]";

                database.CreateConnection();

                //Gets the command 
                selectQuestionCommand = database.GetSqlStringCommand(strCommand);

                //Executes the reader
                questionReader = database.ExecuteReader(selectQuestionCommand);

                QuestionDetail questionDetail;


                string xlsFilePath = Path.GetDirectoryName(fileName);
                string imagesFolder = Path.Combine(xlsFilePath, Path.GetFileNameWithoutExtension(fileName));
                //Defines a question id for question     
                while (questionReader.Read())
                {

                    //This condition is to check whether all the columns are empty
                    //If found empty should not read that line
                    if (!(Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CATEGORY]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.SUBJECT]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.TESTAREA]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.QUESTION]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.TAG]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CORRECT_ANSWER]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_1]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_2]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_3]) &&
                            Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_4])))
                    {

                        //Creats a new question
                        questionDetail = new QuestionDetail();

                        //Assign  the author name for the question
                        questionDetail.AuthorName = userName;

                        //Assign the author id for the question 
                        questionDetail.Author = userID;

                        //Assign the question ID
                        questionDetail.QuestionID = questionID;


                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CATEGORY]))
                        {
                            //Assign the category name for the question 
                            questionDetail.CategoryName = questionReader[QuestionExcelConstants.CATEGORY].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.SUBJECT]))
                        {
                            //Assign the subject name for the question 
                            questionDetail.SubjectName = questionReader[QuestionExcelConstants.SUBJECT].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.TESTAREA]))
                        {
                            //Assign the test area name for the question 
                            questionDetail.TestAreaName = questionReader[QuestionExcelConstants.TESTAREA].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.QUESTION]))
                        {
                            //Assign the question 
                            questionDetail.Question = questionReader[QuestionExcelConstants.QUESTION].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.TAG]))
                        {
                            //Asssign  the tag information for the question 
                            questionDetail.Tag = questionReader[QuestionExcelConstants.TAG].ToString();
                        }

                        string answer;
                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CORRECT_ANSWER]))
                        {
                            //Reads the answer from the excel
                            answer = questionReader[QuestionExcelConstants.CORRECT_ANSWER].ToString().Trim();
                        }
                        else
                        {
                            answer = "0";

                        }
                        questionDetail.AnswerChoices = new List<AnswerChoice>();

                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_1]))
                        {
                            //Reads the choice one for the question
                            choice = questionReader[QuestionExcelConstants.CHOICE_1].ToString();

                            //Checks whether the choice is empty and 
                            //whether the choice is the correct answer
                            CheckAnswerChoices(choice, answer, questionDetail.AnswerChoices,
                                QuestionExcelConstants.CHOICE_1 - 3);

                        }

                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_2]))
                        {
                            //Reads the choice one for the question
                            choice = questionReader[QuestionExcelConstants.CHOICE_2].ToString();

                            //Checks whether the choice is empty and 
                            //whether the choice is the correct answer
                            CheckAnswerChoices(choice, answer, questionDetail.AnswerChoices,
                                QuestionExcelConstants.CHOICE_2 - 3);
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_3]))
                        {
                            //Reads the choice one for the question
                            choice = questionReader[QuestionExcelConstants.CHOICE_3].ToString();

                            //Checks whether the choice is empty and 
                            //whether the choice is the correct answer
                            CheckAnswerChoices(choice, answer, questionDetail.AnswerChoices,
                                QuestionExcelConstants.CHOICE_3 - 3);
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_4]))
                        {
                            //Reads the choice one for the question
                            choice = questionReader[QuestionExcelConstants.CHOICE_4].ToString();

                            //Checks whether the choice is empty and 
                            //whether the choice is the correct answer
                            CheckAnswerChoices(choice, answer, questionDetail.AnswerChoices,
                                QuestionExcelConstants.CHOICE_4 - 3);
                        }

                        //Find correct answer in the collection of choices                     
                        questionDetail.Answer = Convert.ToInt16(answer.Substring(answer.Length - 1, 1));

                        //Assign the complexity of the question
                        questionDetail.ComplexityName = questionReader[QuestionExcelConstants.COMPLEXITY].ToString();
                        questionImageName = questionReader[QuestionExcelConstants.IMAGE].ToString();
                        questionDetail.HasImage = false;
                        questionDetail.Author = userID;
                        questionDetail.AuthorName = userName;
                        questionDetail.ImageName = questionImageName;
                        if (questionDetail.ImageName != "")
                        {
                            questionDetail.HasImage = true;
                            if (File.Exists(Path.Combine(imagesFolder, questionDetail.ImageName)))
                            {
                                try
                                {
                                    FileStream fs = File.OpenRead(Path.Combine(imagesFolder, questionDetail.ImageName));
                                    if (fs.Length > 102400) // if file size greater than 100 KB
                                    {
                                        questionDetail.IsValid = false;
                                        questionDetail.InvalidQuestionRemarks += "IMAGE SIZE EXCEEDED ABOVE 100KB, ";
                                    }
                                    else
                                    {
                                        byte[] data = new byte[fs.Length];
                                        fs.Read(data, 0, data.Length);
                                        questionDetail.QuestionImage = data;
                                    }
                                    fs.Close();
                                }
                                catch
                                {
                                    questionDetail.IsValid = false;
                                    questionDetail.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";

                                }
                            }
                            else
                            {
                                questionDetail.IsValid = false;
                                questionDetail.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";
                            }
                        }


                        //Add question to the question list
                        questions.Add(questionDetail);

                        //Increment the question Id for next question 
                        questionID = questionID + 1;
                    }

                }
                questionReader.Close();

                //Checks the attribute and assign attributes 
                //id to the question
                CheckQuestionAttribute(questions, tenantID);

                return questions;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Represents the method that returns the list of question for the 
        /// given page number and page size.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the questions.
        /// </returns>
        public List<QuestionDetail> GetOpenTextQuestions(int pageNumber, int pageSize,
            string userName, string sheetName, int userID, string fileName, int tenantID)
        {
            if (!CheckIsValidOpenTextExcel(sheetName))
                throw new Exception("");


            //List of questions to store the questions
            List<QuestionDetail> questions = new List<QuestionDetail>();

            string strCommand = string.Empty;

            int lastRecord = pageNumber * pageSize + 1;

            int firstRecord = lastRecord - pageSize + 1;

            int questionID = 0;

            if (pageNumber == 1)
            {
                firstRecord = 1;

                lastRecord = pageSize + 1;

                questionID = 1;
            }
            else
            {
                lastRecord = pageNumber * pageSize + 1;

                firstRecord = lastRecord - pageSize;

                questionID = firstRecord;
            }

            string choice = string.Empty;
            string questionImageName = string.Empty;
            IDataReader questionReader;

            DbCommand selectQuestionCommand;

            try
            {

                //Command to select the question from sheet
                //Questions will be selected based on first record and last record
                strCommand = "select * from [" + sheetName + "A" + firstRecord + ":M" + lastRecord + "]";

                database.CreateConnection();

                //Gets the command 
                selectQuestionCommand = database.GetSqlStringCommand(strCommand);

                //Executes the reader
                questionReader = database.ExecuteReader(selectQuestionCommand);

                QuestionDetail questionDetail;

                string xlsFilePath = Path.GetDirectoryName(fileName);
                string imagesFolder = Path.Combine(xlsFilePath, Path.GetFileNameWithoutExtension(fileName));
                //Defines a question id for question     
                while (questionReader.Read())
                {
                    //This condition is to check whether all the columns are empty
                    //If found empty should not read that line
                    if (!(Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.CATEGORY]) &&
                            Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.SUBJECT]) &&
                            Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.TESTAREA]) &&
                            Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.QUESTION]) &&
                            Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.ANSWER_REFERENCE]) &&
                            Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.MAX_LENGTH]) &&
                            Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.MARKS]) &&
                            Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.COMPLEXITY])))
                    {

                        //Creats a new question
                        questionDetail = new QuestionDetail();
                        questionDetail.AnswerChoices = new List<AnswerChoice>();
                        questionDetail.QuestionAttribute = new QuestionAttribute();

                        //Assign  the author name for the question
                        questionDetail.AuthorName = userName;

                        //Assign the author id for the question 
                        questionDetail.Author = userID;

                        //Assign the question ID
                        questionDetail.QuestionID = questionID;

                        if (!Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.CATEGORY]))
                        {
                            questionDetail.CategoryName = questionReader[OpenTextQuestionExcelConstants.CATEGORY].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.SUBJECT]))
                        {
                            questionDetail.SubjectName = questionReader[OpenTextQuestionExcelConstants.SUBJECT].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.TESTAREA]))
                        {
                            //Assign the test area name for the question 
                            questionDetail.TestAreaName = questionReader[OpenTextQuestionExcelConstants.TESTAREA].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.QUESTION]))
                        {
                            questionDetail.Question = questionReader[OpenTextQuestionExcelConstants.QUESTION].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.TAG]))
                        {
                            questionDetail.Tag = questionReader[OpenTextQuestionExcelConstants.TAG].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.ANSWER_REFERENCE]))
                        {
                            questionDetail.QuestionAttribute.AnswerReference = questionReader[OpenTextQuestionExcelConstants.ANSWER_REFERENCE].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.MAX_LENGTH]))
                        {
                            int maxLength = 0;
                            int.TryParse(questionReader[OpenTextQuestionExcelConstants.MAX_LENGTH].ToString(), out maxLength);
                            questionDetail.QuestionAttribute.MaxLength = maxLength;
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[OpenTextQuestionExcelConstants.MARKS]))
                        {
                            int marks = 0;
                            int.TryParse(questionReader[OpenTextQuestionExcelConstants.MARKS].ToString(), out marks);
                            questionDetail.QuestionAttribute.Marks = marks;
                        }

                        //Assign the complexity of the question
                        questionDetail.ComplexityName = questionReader[OpenTextQuestionExcelConstants.COMPLEXITY].ToString();
                        questionImageName = questionReader[OpenTextQuestionExcelConstants.IMAGE].ToString();
                        questionDetail.HasImage = false;
                        questionDetail.ImageName = questionImageName;
                        if (questionDetail.ImageName != "")
                        {
                            questionDetail.HasImage = true;
                            if (File.Exists(Path.Combine(imagesFolder, questionDetail.ImageName)))
                            {
                                try
                                {
                                    FileStream fs = File.OpenRead(Path.Combine(imagesFolder, questionDetail.ImageName));
                                    if (fs.Length > 102400) // if file size greater than 100 KB
                                    {
                                        questionDetail.IsValid = false;
                                        questionDetail.InvalidQuestionRemarks += "IMAGE SIZE EXCEEDED ABOVE 100KB, ";
                                    }
                                    else
                                    {
                                        byte[] data = new byte[fs.Length];
                                        fs.Read(data, 0, data.Length);
                                        questionDetail.QuestionImage = data;
                                    }
                                    fs.Close();
                                }
                                catch
                                {
                                    questionDetail.IsValid = false;
                                    questionDetail.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";

                                }
                            }
                            else
                            {
                                questionDetail.IsValid = false;
                                questionDetail.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";
                            }
                        }


                        //Add question to the question list
                        questions.Add(questionDetail);

                        //Increment the question Id for next question 
                        questionID = questionID + 1;
                    }

                }
                questionReader.Close();

                //Checks the attribute and assign attributes 
                //id to the question
                CheckOpenTextQuestionAttribute(questions, tenantID);

                return questions;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }
        }

        private bool CheckIsValidExcel(string sheetName)
        {
            //List of questions to store the questions
            List<QuestionDetail> questions = new List<QuestionDetail>();

            string strCommand = string.Empty;

            string choice = string.Empty;
            string questionImageName = string.Empty;
            IDataReader questionReader;

            DbCommand selectQuestionCommand;

            //Command to select the question from sheet
            //Questions will be selected based on first record and last record
            strCommand = "select * from [" + sheetName + "]";

            database.CreateConnection();

            //Gets the command 
            selectQuestionCommand = database.GetSqlStringCommand(strCommand);

            //Executes the reader
            questionReader = database.ExecuteReader(selectQuestionCommand);

            int count = questionReader.FieldCount;

            int maxCount = QuestionExcelConstants.IMAGE + 1;

            if (count != maxCount)
            {
                return false;
            }

            if (!((questionReader.GetName(QuestionExcelConstants.CATEGORY).Trim() == "Category") &&
                (questionReader.GetName(QuestionExcelConstants.SUBJECT).Trim() == "Subject") &&
                (questionReader.GetName(QuestionExcelConstants.TESTAREA).Trim() == "Test Area") &&
                (questionReader.GetName(QuestionExcelConstants.QUESTION).Trim() == "Question") &&
                (questionReader.GetName(QuestionExcelConstants.CHOICE_1).Trim() == "Choice 1") &&
                (questionReader.GetName(QuestionExcelConstants.CHOICE_2).Trim() == "Choice 2") &&
                (questionReader.GetName(QuestionExcelConstants.CHOICE_3).Trim() == "Choice 3") &&
                (questionReader.GetName(QuestionExcelConstants.CHOICE_4).Trim() == "Choice 4") &&
                (questionReader.GetName(QuestionExcelConstants.CORRECT_ANSWER).Trim() == "Correct Answer") &&
                (questionReader.GetName(QuestionExcelConstants.COMPLEXITY).Trim() == "Complexity") &&
                (questionReader.GetName(QuestionExcelConstants.TAG).Trim() == "Tags (separated by commas)") &&
                (questionReader.GetName(QuestionExcelConstants.IMAGE).Trim() == "IMAGE_NAME")))
            {
                return false;
            }

            return true;
        }

        private bool CheckIsValidOpenTextExcel(string sheetName)
        {
            //List of questions to store the questions
            List<QuestionDetail> questions = new List<QuestionDetail>();

            string strCommand = string.Empty;

            string choice = string.Empty;
            string questionImageName = string.Empty;
            IDataReader questionReader;

            DbCommand selectQuestionCommand;

            //Command to select the question from sheet
            //Questions will be selected based on first record and last record
            strCommand = "select * from [" + sheetName + "]";

            database.CreateConnection();

            //Gets the command 
            selectQuestionCommand = database.GetSqlStringCommand(strCommand);

            //Executes the reader
            questionReader = database.ExecuteReader(selectQuestionCommand);

            int count = questionReader.FieldCount;

            int maxCount = OpenTextQuestionExcelConstants.IMAGE + 1;

            if (count != maxCount)
            {
                return false;
            }

            if (!((questionReader.GetName(OpenTextQuestionExcelConstants.CATEGORY).Trim() == "Category") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.SUBJECT).Trim() == "Subject") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.TESTAREA).Trim() == "Test Area") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.QUESTION).Trim() == "Question") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.ANSWER_REFERENCE).Trim() == "Answer Reference") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.MAX_LENGTH).Trim() == "Max Length") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.MARKS).Trim() == "Marks") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.COMPLEXITY).Trim() == "Complexity") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.TAG).Trim() == "Tags (separated by commas)") &&
                (questionReader.GetName(OpenTextQuestionExcelConstants.IMAGE).Trim() == "IMAGE_NAME")))
            {
                return false;
            }

            return true;
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Method that is used to check the choices of the questions.
        /// It checks whether the choice is not empty and whether the choice is 
        /// the answer of the question.Then it add the choice to the choice list
        /// </summary>
        /// <param name="choice">
        /// A<see cref="string"/>holds the choice of question
        /// </param>
        /// <param name="answer">
        /// A<see cref="string"/>holds the correct answer
        /// </param>
        /// <param name="answerChoices">
        /// A<see cref="List<AnswerChoice>"/>holds the answer
        /// choice of the question
        /// </param>
        private void CheckAnswerChoices(string choice, string answer,
            List<AnswerChoice> answerChoices, int choiceID)
        {
            //if choice is null then return
            if (choice.Trim().Length == 0)
                return;
            ////check whether the answer is mentioned in excel or not
            //if (answer.Length == 0)
            //{
            //    answerChoices.Add(new AnswerChoice
            //   (choice, answerChoices.Count + 1, false));
            //    return;
            //}
            //else add the choice to the answer choice list
            answerChoices.Add(new AnswerChoice
                (choice, answerChoices.Count + 1,
                choiceID == int.Parse(answer.Substring(answer.Length - 1, 1))));
        }

        /// <summary>
        /// Method that is used check the attribute of the question such as category, 
        /// subject, testarea , complexity .It checks whether the attrbutes entered 
        /// correct and if it so it will get back the corresponding ID of the attributes
        /// </summary>
        /// <param name="questionDetails">
        /// A<see cref="List<QuestionDetails>"/>list of questions
        /// </param>
        /// <returns>
        /// list of questions that contain the collected id of the attributes
        /// </returns>
        private List<QuestionDetail> CheckQuestionAttribute(List<QuestionDetail> questionDetails, int tenantID)
        {
            QuestionBLManager questionBLManager = new QuestionBLManager();
            foreach (QuestionDetail question in questionDetails)
            {
                questionBLManager.CheckQuestionAttribute(question, tenantID);

                AnswerChoice answer = question.AnswerChoices.Find(delegate
                    (AnswerChoice answerChoice)
                    {
                        return answerChoice.IsCorrect == true;
                    });

                if (answer == null)
                {
                    question.IsValid = false;
                }
                if (question.HasImage && question.QuestionImage == null)
                {
                    question.IsValid = false;
                }
            }
            return questionDetails;
        }

        /// <summary>
        /// Method that is used check the attribute of the question such as category, 
        /// subject, testarea , complexity .It checks whether the attrbutes entered 
        /// correct and if it so it will get back the corresponding ID of the attributes
        /// </summary>
        /// <param name="questionDetails">
        /// A<see cref="List<QuestionDetails>"/>list of questions
        /// </param>
        /// <returns>
        /// list of questions that contain the collected id of the attributes
        /// </returns>
        private List<QuestionDetail> CheckOpenTextQuestionAttribute(List<QuestionDetail> questionDetails, int tenantID)
        {
            QuestionBLManager questionBLManager = new QuestionBLManager();
            foreach (QuestionDetail question in questionDetails)
            {
                questionBLManager.CheckOpenTextQuestionAttribute(question, tenantID);

                if (question.HasImage && question.QuestionImage == null)
                {
                    question.IsValid = false;
                }
            }
            return questionDetails;
        }

        private List<QuestionDetail> CheckInterviewQuestionAttribute(List<QuestionDetail> questionDetails, int tenantID)
        {
            QuestionBLManager questionBLManager = new QuestionBLManager();
            foreach (QuestionDetail question in questionDetails)
            {
                questionBLManager.CheckIntrviewQuestionAttribute(question, tenantID);



                if (question.CorrectAnswer == null)
                {
                    question.IsValid = false;
                }
                if (question.HasImage && question.QuestionImage == null)
                {
                    question.IsValid = false;
                }
            }
            return questionDetails;
        }

        #endregion Private Methods

        public List<QuestionDetail> GetInterviewQuestions(int pageNumber, int pageSize,
            string userName, string sheetName, int userID, string fileName, int tenantID)
        {
            //List of questions to store the questions
            List<QuestionDetail> questions = new List<QuestionDetail>();

            string strCommand = string.Empty;

            int lastRecord = pageNumber * pageSize + 1;

            int firstRecord = lastRecord - pageSize + 1;

            int questionID = 0;

            if (pageNumber == 1)
            {
                firstRecord = 1;

                lastRecord = pageSize + 1;

                questionID = 1;
            }
            else
            {
                lastRecord = pageNumber * pageSize + 1;

                firstRecord = lastRecord - pageSize;

                questionID = firstRecord;
            }

            string choice = string.Empty;
            string questionImageName = string.Empty;
            IDataReader questionReader;

            DbCommand selectQuestionCommand;

            try
            {

                //Command to select the question from sheet
                //Questions will be selected based on first record and last record
                strCommand = "select * from [" + sheetName + "A" + firstRecord + ":M" + lastRecord + "]";

                database.CreateConnection();

                //Gets the command 
                selectQuestionCommand = database.GetSqlStringCommand(strCommand);

                //Executes the reader
                questionReader = database.ExecuteReader(selectQuestionCommand);

                QuestionDetail questionDetail;


                string xlsFilePath = Path.GetDirectoryName(fileName);
                string imagesFolder = Path.Combine(xlsFilePath, Path.GetFileNameWithoutExtension(fileName));
                //Defines a question id for question     
                while (questionReader.Read())
                {

                    //This condition is to check whether all the columns are empty
                    //If found empty should not read that line
                    if (!(Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.CATEGORY]) &&
                            Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.SUBJECT]) &&
                            Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.TESTAREA]) &&
                            Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.QUESTION]) &&
                            Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.TAG]) &&
                            Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.ANSWER])))
                    {

                        //Creats a new question
                        questionDetail = new QuestionDetail();

                        //Assign  the author name for the question
                        questionDetail.AuthorName = userName;

                        //Assign the author id for the question 
                        questionDetail.Author = userID;

                        //Assign the question ID
                        questionDetail.QuestionID = questionID;


                        if (!Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.CATEGORY]))
                        {
                            //Assign the category name for the question 
                            questionDetail.CategoryName = questionReader[InterviewQuestionExcelConstants.CATEGORY].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.SUBJECT]))
                        {
                            //Assign the subject name for the question 
                            questionDetail.SubjectName = questionReader[InterviewQuestionExcelConstants.SUBJECT].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.TESTAREA]))
                        {
                            //Assign the test area name for the question 
                            questionDetail.TestAreaName = questionReader[InterviewQuestionExcelConstants.TESTAREA].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.QUESTION]))
                        {
                            //Assign the question 
                            questionDetail.Question = questionReader[InterviewQuestionExcelConstants.QUESTION].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.TAG]))
                        {
                            //Asssign  the tag information for the question 
                            questionDetail.Tag = questionReader[InterviewQuestionExcelConstants.TAG].ToString();
                        }

                        /* if (!Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.ANSWER]))
                         {
                             //Assign the answer to the question
                             questionDetail.CorrectAnswer = questionReader[InterviewQuestionExcelConstants.ANSWER].ToString();
                         }*/


                        questionDetail.AnswerChoices = new List<AnswerChoice>();

                        if (!Utility.IsNullOrEmpty(questionReader[InterviewQuestionExcelConstants.ANSWER]))
                        {
                            //Reads the choice one for the question
                            choice = questionReader[InterviewQuestionExcelConstants.ANSWER].ToString();

                            //Checks whether the choice is empty and 
                            //whether the choice is the correct answer
                            CheckInterviewAnswerChoices(choice, questionDetail.AnswerChoices);

                        }

                        /*   if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_2]))
                              {
                                  //Reads the choice one for the question
                                  choice = questionReader[QuestionExcelConstants.CHOICE_2].ToString();

                                  //Checks whether the choice is empty and 
                                  //whether the choice is the correct answer
                                  CheckAnswerChoices(choice, answer, questionDetail.AnswerChoices,
                                      QuestionExcelConstants.CHOICE_2 - 3);
                              }

                              if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_3]))
                              {
                                  //Reads the choice one for the question
                                  choice = questionReader[QuestionExcelConstants.CHOICE_3].ToString();

                                  //Checks whether the choice is empty and 
                                  //whether the choice is the correct answer
                                  CheckAnswerChoices(choice, answer, questionDetail.AnswerChoices,
                                      QuestionExcelConstants.CHOICE_3 - 3);
                              }

                              if (!Utility.IsNullOrEmpty(questionReader[QuestionExcelConstants.CHOICE_4]))
                              {
                                  //Reads the choice one for the question
                                  choice = questionReader[QuestionExcelConstants.CHOICE_4].ToString();

                                  //Checks whether the choice is empty and 
                                  //whether the choice is the correct answer
                                  CheckAnswerChoices(choice, answer, questionDetail.AnswerChoices,
                                      QuestionExcelConstants.CHOICE_4 - 3);
                              }*/

                        //Find correct answer in the collection of choices                     
                        questionDetail.CorrectAnswer = questionDetail.AnswerChoices[0].Choice;


                        //Assign the complexity of the question
                        questionDetail.ComplexityName = questionReader[InterviewQuestionExcelConstants.COMPLEXITY].ToString();
                        questionImageName = questionReader[InterviewQuestionExcelConstants.IMAGE].ToString();
                        questionDetail.HasImage = false;
                        questionDetail.ImageName = questionImageName;
                        if (questionDetail.ImageName != "")
                        {
                            questionDetail.HasImage = true;
                            if (File.Exists(Path.Combine(imagesFolder, questionDetail.ImageName)))
                            {
                                try
                                {
                                    FileStream fs = File.OpenRead(Path.Combine(imagesFolder, questionDetail.ImageName));
                                    if (fs.Length > 102400) // if file size greater than 100 KB
                                    {
                                        questionDetail.IsValid = false;
                                        questionDetail.InvalidQuestionRemarks += "IMAGE SIZE EXCEEDED ABOVE 100KB, ";
                                    }
                                    else
                                    {
                                        byte[] data = new byte[fs.Length];
                                        fs.Read(data, 0, data.Length);
                                        questionDetail.QuestionImage = data;
                                    }
                                    fs.Close();
                                }
                                catch
                                {
                                    questionDetail.IsValid = false;
                                    questionDetail.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";

                                }
                            }
                            else
                            {
                                questionDetail.IsValid = false;
                                questionDetail.InvalidQuestionRemarks += "IMAGE NOT PRESENT, ";
                            }
                        }


                        //Add question to the question list
                        questions.Add(questionDetail);

                        //Increment the question Id for next question 
                        questionID = questionID + 1;
                    }

                }
                questionReader.Close();

                //Checks the attribute and assign attributes 
                //id to the question
                CheckInterviewQuestionAttribute(questions, tenantID);

                return questions;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
            }
        }
        private void CheckInterviewAnswerChoices(string choice,
          List<AnswerChoice> answerChoices)
        {
            //if choice is null then return
            if (choice.Trim().Length == 0)
                return;
            ////check whether the answer is mentioned in excel or not
            //if (answer.Length == 0)
            //{
            //    answerChoices.Add(new AnswerChoice
            //   (choice, answerChoices.Count + 1, false));
            //    return;
            //}
            //else add the choice to the answer choice list
            answerChoices.Add(new AnswerChoice
                (choice, answerChoices.Count + 1, true));
        }

    }

    internal class QuestionExcelConstants
    {
        /// <summary>
        /// A <see cref="int"/> that holds the
        /// column number of category
        /// </summary>
        public const int CATEGORY = 0;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of subject
        /// </summary>
        public const int SUBJECT = 1;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of test area 
        /// </summary>
        public const int TESTAREA = 2;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of question
        /// </summary>
        public const int QUESTION = 3;

        /// <summary>
        /// A<see cref="int"/>that holds the
        /// column number of choice 1
        /// </summary>
        public const int CHOICE_1 = 4;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of choice 2
        /// </summary>
        public const int CHOICE_2 = 5;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of choice 3
        /// </summary>
        public const int CHOICE_3 = 6;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of choice 4
        /// </summary>
        public const int CHOICE_4 = 7;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of correct answer
        /// </summary>
        public const int CORRECT_ANSWER = 8;

        /// <summary>
        /// A<see cref="int"/>that holds the
        /// column number of complexity
        /// </summary>
        public const int COMPLEXITY = 9;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of tag
        /// </summary>
        public const int TAG = 10;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of tag
        /// </summary>
        public const int IMAGE = 11;

        /// <summary>
        ///  A<see cref="int"/>that holds the 
        /// column number of tag
        /// </summary>
        public const int ANSWER = 12;
    }

    internal class OpenTextQuestionExcelConstants
    {
        /// <summary>
        /// A <see cref="int"/> that holds the column number of category
        /// </summary>
        public const int CATEGORY = 0;

        /// <summary>
        /// A<see cref="int"/>that holds the column number of subject
        /// </summary>
        public const int SUBJECT = 1;

        /// <summary>
        /// A<see cref="int"/>that holds the column number of test area 
        /// </summary>
        public const int TESTAREA = 2;

        /// <summary>
        /// A<see cref="int"/>that holds the column number of question
        /// </summary>
        public const int QUESTION = 3;

        /// <summary>
        /// A<see cref="int"/>that holds the column number answer reference.
        /// </summary>
        public const int ANSWER_REFERENCE = 4;

        /// <summary>
        /// A<see cref="int"/>that holds the column number of max length.
        /// </summary>
        public const int MAX_LENGTH = 5;

        /// <summary>
        /// A<see cref="int"/>that holds the column number of marks.
        /// </summary>
        public const int MARKS = 6;

        /// <summary>
        /// A<see cref="int"/>that holds the column number of complexity
        /// </summary>
        public const int COMPLEXITY = 7;

        /// <summary>
        /// A<see cref="int"/>that holds the column number of tag
        /// </summary>
        public const int TAG = 8;

        /// <summary>
        /// A<see cref="int"/>that holds the column number of tag
        /// </summary>
        public const int IMAGE = 9;
    }

    internal class InterviewQuestionExcelConstants
    {
        /// <summary>
        /// A <see cref="int"/> that holds the
        /// column number of category
        /// </summary>
        public const int CATEGORY = 0;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of subject
        /// </summary>
        public const int SUBJECT = 1;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of test area 
        /// </summary>
        public const int TESTAREA = 2;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of question
        /// </summary>
        public const int QUESTION = 3;

        /// <summary>
        ///  A<see cref="int"/>that holds the 
        /// column number of tag
        /// </summary>
        public const int ANSWER = 4;

        /// <summary>
        /// A<see cref="int"/>that holds the
        /// column number of complexity
        /// </summary>
        public const int COMPLEXITY = 5;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of tag
        /// </summary>
        public const int TAG = 6;

        /// <summary>
        /// A<see cref="int"/>that holds the 
        /// column number of tag
        /// </summary>
        public const int IMAGE = 7;


    }
}