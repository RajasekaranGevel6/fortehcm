﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdaptiveQuestionCombinations.cs
// File gives the combination of elements
// ex:- if you have a int list conatins {1,2,3,4}
// to get the combinations of length 2 this class 
// returns all the combinations of that element
// like 1-2,1-3,1-4,2-3,2-4,3-4

#endregion Header

#region Directives

using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

#endregion

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Gives the combination of elements
    /// </summary>
    /// <typeparam name="T">Type of the Element</typeparam>
    public class AdaptiveQuestionCombinations<T> : IEnumerable<T[]>
    {

        #region Declarations                                                   

        private int _Choices;
        private SortedList<T, int> _Elements;
        private int[] _ElementsIndices;
        internal int[] ElementsIndices
        {
            get { return _ElementsIndices; }
        }

        #endregion Declarations

        #region Public Methods                                                 

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Elements">List of elements to get the combinations.</param>
        /// <param name="Choices">Number of choice</param>
        public AdaptiveQuestionCombinations(IEnumerable<T> Elements, int Choices)
        {
            if (Elements == null)
                throw new NullReferenceException("Elemets can not be null");
            if (Choices == 0)
                throw new Exception("Choices must atleast 1");
            LoadElement(Elements);
            _Choices = Choices;

        }

        #endregion Public Methods

        #region Private Methods                                                

        /// <summary>
        /// Loads the element in to sorted list
        /// </summary>
        /// <param name="elements"></param>
        private void LoadElement(IEnumerable<T> elements)
        {
            SortedDictionary<T, int> sorted = new SortedDictionary<T, int>();
            foreach (T element in elements)
                sorted[element] = 1;
            _Elements = new SortedList<T, int>(sorted);
            _Elements.Capacity = _Elements.Count;
            if (_Elements.Count <= 0)
                return;
            _ElementsIndices = new int[_Elements.Count];
            _ElementsIndices[_Elements.Count - 1] = _Elements.Values[_Elements.Count - 1];
            for (int i = _Elements.Count - 2; i >= 0; i--)
                _ElementsIndices[i] = _ElementsIndices[i + 1] + _Elements.Values[i];
        }

        #endregion

        #region IEnumerable Class,Methods                                      
        
        #region IEnumerable<T[]> Members

        /// <summary>
        /// Creates and returns an enumerator over the combinations. Each combination's elements will be returned in ascending order as defined by the compararer. The combinations themselves will also be returned in ascending order.
        /// </summary>
        /// <returns>an enumerator</returns>
        /// <seealso cref="IEnumerable{T}.GetEnumerator"/>
        public IEnumerator<T[]> GetEnumerator()
        {
            return new Enumerator(this);
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }

        #endregion

        #region Enumerator Class

        private class Enumerator : IEnumerator<T[]>
        {
            #region Enumerator Members
            private AdaptiveQuestionCombinations<T> _Combinations;
            private int[] _IndicesCombination, _IndicesTotal;
            private bool _isChecked;

            /// <summary>
            /// Constructor for the class Enumerator.
            /// </summary>
            /// <param name="combinations"></param>
            internal Enumerator(AdaptiveQuestionCombinations<T> combinations)
            {
                _Combinations = combinations;
                _IndicesCombination = new int[_Combinations._Choices];
                _IndicesTotal = new int[_Combinations._Elements.Count];
                Reset();
            }

            /// <summary>
            /// This method will be called for the first time to get the 
            /// firt records from the indices.
            /// </summary>
            private void MoveFirst()
            {
                // Move all the indices as far left as possible.
                // Lower-numbered indices go further left.
                int currentIndex = 0;
                int usedCurrent = 0;
                for (int i = 0; i < _IndicesCombination.Length; i++)
                {
                    Debug.Assert(currentIndex < _Combinations._Elements.Count);
                    _IndicesCombination[i] = currentIndex;
                    _IndicesTotal[currentIndex]++;
                    usedCurrent++;
                    if (usedCurrent == _Combinations._Elements.Values[currentIndex])
                    {
                        // We've used all of the current element. Go to the next element.
                        currentIndex++;
                        usedCurrent = 0;
                    }
                }
            }

            /// <summary>
            /// This method will move index a position.
            /// </summary>
            /// <param name="index"></param>
            /// <param name="offset"></param>
            private void MoveIndex(int index, int offset)
            {
                if (_IndicesCombination[index] >= 0 && _IndicesCombination[index] < _IndicesTotal.Length)
                    _IndicesTotal[_IndicesCombination[index]]--;
                _IndicesCombination[index] += offset;
                if (_IndicesCombination[index] >= 0 && _IndicesCombination[index] < _IndicesTotal.Length)
                    _IndicesTotal[_IndicesCombination[index]]++;
            }

            /// <summary>
            /// Checks whether the indices are full or not.
            /// </summary>
            /// <param name="position">Current postion of the indices</param>
            /// <returns>if full return true else false.</returns>
            private bool IsFull(int position)
            {
                // True if (position) has as many indices as it can hold.
                return _IndicesTotal[position] == _Combinations._Elements.Values[position];
            }

            /// <summary>
            /// Checks whether the moved index fits in the indices or not.
            /// </summary>
            /// <param name="index">index to check</param>
            /// <returns>If fits return ture else false.</returns>
            private bool CanFitRemainingIndices(int index)
            {
                return _Combinations.ElementsIndices[_IndicesCombination[index]]
                    >= _IndicesCombination.Length - index;
            }

            private bool AdvanceIndex(int index, int doNotReach)
            {
                // First move the index one position to the right.
                MoveIndex(index, 1);
                // If we've found an existing position with no other index, and there's room at-and-to-the-right-of us for all the indices greater than us, we're good.
                if (_IndicesCombination[index] < doNotReach)
                {
                    if (_IndicesTotal[_IndicesCombination[index]] == 1)
                    {
                        if (CanFitRemainingIndices(index))
                        {
                            return true;
                        }
                    }
                }
                //// We've either fallen off the right hand edge, or hit a position with another index. If we're index 0, we're past the end of the enumeration. If not, we need to advance the next lower index, then push ourself left as far as possible until we hit another occupied position.
                if (index == 0)
                {
                    _isChecked = true;
                    return false;
                }
                if (!AdvanceIndex(index - 1, _IndicesCombination[index]))
                {
                    return false;
                }
                // We must move at least one space to the left. If we can't, the enumeration is over.
                // If the position immediately to the left is already full, we can't move there.
                if (IsFull(_IndicesCombination[index] - 1))
                {
                    _isChecked = true;
                    return false;
                }
                // Move left until the next leftmost element is full, or the current element has an index.
                do
                {
                    MoveIndex(index, -1);
                } while (_IndicesTotal[_IndicesCombination[index]] == 1 && !IsFull(_IndicesCombination[index] - 1));
                return true;
            }
            #endregion

            #region IEnumerator<T[]> Members

            /// <summary>
            /// This public property returns the T[] for each iteration in 
            /// foreach statement.
            /// This is also a part IEnumerable signature 
            /// </summary>
            public T[] Current
            {
                get
                {
                    if (_IndicesCombination[0] == -1 || _isChecked)
                        throw new InvalidOperationException();
                    else
                    {
                        T[] current = new T[_IndicesCombination.Length];
                        for (int i = 0; i < _IndicesCombination.Length; i++)
                        {
                            current[i] = _Combinations._Elements.Keys[_IndicesCombination[i]];
                        }
                        return current;
                    }
                }
            }

            #endregion

            #region IDisposable Method

            public void Dispose()
            {
                // Do nothing.
            }

            #endregion

            /// This Region is signature type when we are inheriting IEnumerable<T[]> interface
            #region IEnumerator Methods

            /// <summary>
            /// Signature when we using IEnumerator interface
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    return Current;
                }
            }

            /// <summary>
            /// This method will be called when this class used in 
            /// 'foreach' statement for every 'in' statement
            /// This is part of signature in IEnumerator
            /// ex:- foreach(string[] str in AdaptiveQuestionCombinationsObject)
            /// </summary>
            /// <returns></returns>
            public bool MoveNext()
            {
                if (_isChecked)
                    return false;
                if (_IndicesCombination[0] == -1)
                {
                    MoveFirst();
                    return true;
                }
                else
                    return AdvanceIndex(_IndicesCombination.Length - 1, _IndicesTotal.Length);
            }

            /// <summary>
            /// This method reset the indices.
            /// This is a part of signature in IEnumerator. 
            /// </summary>
            public void Reset()
            {
                for (int i = 0; i < _IndicesCombination.Length; i++)
                {
                    _IndicesCombination[i] = -1;
                }
                Array.Clear(_IndicesTotal, 0, _IndicesTotal.Length);
                _isChecked = false;
            }
            #endregion
        }
        #endregion

        #endregion IEnumerable Class,Methods
    }
}
