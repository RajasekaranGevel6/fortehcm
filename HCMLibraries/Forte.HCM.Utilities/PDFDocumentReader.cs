﻿#region Directives                                                             

using org.pdfbox.util;
using org.pdfbox.pdmodel;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Represents the class that is used to read the pdf documents
    /// </summary>
    public class PDFDocumentReader
    {
        /// <summary>
        /// Represents the method that is used to get the text from the
        /// pdf file
        /// </summary>
        /// <param name="fileName">
        /// A<see cref="string"/>that holds the file name
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the return string
        /// </returns>
        public string GetPreviewText(string fileName)
        {
            //Load the document
            PDDocument pdfDoc = PDDocument.load(fileName);

            PDFTextStripper stripper = new PDFTextStripper();

            //Return the text
            return stripper.getText(pdfDoc);            
        }
    }
}
