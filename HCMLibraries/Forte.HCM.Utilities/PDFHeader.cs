﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PDFHeader.cs
// File that represents the Writing the Header & footer of the PDF.

#endregion Header

#region Directives                                                             
using System;

using iTextSharp.text;
using iTextSharp.text.pdf;

using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.Utilities
{
    /// <summary>                                                  
    /// Class that represents the Writing the Header & footer of the PDF 
    /// for the Various Report Like Design,Comparison,Group analysis and 
    /// CandiateReport PDF.
    /// </summary>
    
    public class PDFHeader : iTextSharp.text.pdf.PdfPageEventHelper
    {
        #region protected Methods                                              
        /// <summary>
        /// font object to use within PDF Header & footer
        /// </summary>
        protected Font footer
        {
            get
            {
                // create a basecolor to use for the footer font, if needed.
                BaseColor grey = new BaseColor(128, 128, 128);
                Font font = FontFactory.GetFont("Arial", 9, Font.NORMAL, grey);
                return font;
            }
        }
        #endregion Public Methods
       
        #region Public Methods                                                 

        /// <summary>
        /// Get the Header & Footer Contents.
        /// </summary>
        public CandidateReportDetail candidateReportDetail
        {
            set;
            get;
        }
        /// <summary>
        ///override the OnStartPage event handler to add our header 
        /// </summary>
        /// <param name="writer">
        ///  A <see cref="PdfWriter"/> that holds the PdfWriter.
        /// </param>
        /// <param name="doc">
        ///  A <see cref="Document"/> that holds the Document.
        /// </param>
        public override void OnStartPage(PdfWriter writer, Document doc)
        {
            //I use a PdfPtable with 2 columns to position my footer where I want it
            PdfPTable headerTbl = new PdfPTable(2);

            //set the width of the table to be the same as the document
            headerTbl.TotalWidth = doc.PageSize.Width;

            //Center the table on the page
            headerTbl.HorizontalAlignment = Element.ALIGN_CENTER;

            //Create a paragraph that contains the footer text
            Paragraph para = new Paragraph(candidateReportDetail.CandidateName, footer);

            //create a cell instance to hold the text
            PdfPCell cell = new PdfPCell(para);

            //set cell border to 0
            cell.Border = 0;

            //add some padding to bring away from the edge
            cell.PaddingLeft = 10;

            //add cell to table
            headerTbl.AddCell(cell);

            //create new instance of Paragraph for 2nd cell text
            para = new Paragraph(candidateReportDetail.TestName, footer);

            //create new instance of cell to hold the text
            cell = new PdfPCell(para);

            //align the text to the right of the cell
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //set border to 0
            cell.Border = 0;

            // add some padding to take away from the edge of the page
            cell.PaddingRight = 10;

            //add the cell to the table
            headerTbl.AddCell(cell);

            //write the rows out to the PDF output stream.
            headerTbl.WriteSelectedRows(0, -1, 0, (doc.PageSize.Height - 10), writer.DirectContent);
        }
        
        /// <summary>
        /// override the OnPageEnd event handler to add our footer
        /// </summary>
        /// <param name="writer">
        ///  A <see cref="PdfWriter"/> that holds the PdfWriter.
        /// </param>
        /// <param name="doc">
        ///  A <see cref="Document"/> that holds the Document.
        /// </param>
        public override void OnEndPage(PdfWriter writer, Document doc)
        {
            //I use a PdfPtable with 2 columns to position my footer where I want it
            PdfPTable footerTbl = new PdfPTable(2);

            //set the width of the table to be the same as the document
            footerTbl.TotalWidth = doc.PageSize.Width;

            //Center the table on the page
            footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;

            //Create a paragraph that contains the footer text
            Paragraph para = new Paragraph("Prepared by:" + candidateReportDetail.LoginUser  , footer);

            //create a cell instance to hold the text
            PdfPCell cell = new PdfPCell(para);

            //set cell border to 0
            cell.Border = 0;

            //add some padding to bring away from the edge
            cell.PaddingLeft = 10;

            //add cell to table
            footerTbl.AddCell(cell);

            //create new instance of Paragraph for 2nd cell text
            para = new Paragraph("Prepared On:" + DateTime.Now, footer);

            //create new instance of cell to hold the text
            cell = new PdfPCell(para);

            //align the text to the right of the cell
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //set border to 0
            cell.Border = 0;

            // add some padding to take away from the edge of the page
            cell.PaddingRight = 10;

            //add the cell to the table
            footerTbl.AddCell(cell);

            //write the rows out to the PDF output stream.
            footerTbl.WriteSelectedRows(0, -1, 0, (doc.BottomMargin + 10), writer.DirectContent);
        }
        #endregion Public Methods
    }
}
