﻿using System;
using System.Data;
using System.Collections.Generic;

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Class that process the raw data and constructs the customized data
    /// structures.
    /// </summary>
    public class DataManager
    {
        /// <summary>
        /// A <see cref="int"/> that holds the tables count.
        /// </summary>
        private const int TABLES_COUNT = 7;

        /// <summary>
        /// A <see cref="int"/> that holds the assessor details index.
        /// </summary>
        private const int ASSESSOR_DETAILS_INDEX = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the subject details index.
        /// </summary>
        private const int SUBJECT_DETAILS_INDEX = 1;

        /// <summary>
        /// A <see cref="int"/> that holds the subject rating summary index.
        /// </summary>
        private const int SUBJECT_RATING_SUMMARY_INDEX = 2;

        /// <summary>
        /// A <see cref="int"/> that holds the question details index.
        /// </summary>
        private const int QUESTION_DETAILS_INDEX = 3;

        /// <summary>
        /// A <see cref="int"/> that holds the question rating summary index.
        /// </summary>
        private const int QUESTION_RATING_SUMMARY_INDEX = 4;

        /// <summary>
        /// A <see cref="int"/> that holds the overall summary index.
        /// </summary>
        private const int OVERALL_SUMMARY_INDEX = 5;

        /// <summary>
        /// A <see cref="int"/> that holds the overall weighted summary index.
        /// </summary>
        private const int OVERALL_WEIGHTED_SUMMARY_INDEX = 6;

        /// <summary>
        /// Method that constructs the customized data structure for rating
        /// summary.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="totalScore">
        /// A <see cref="decimal"/> that holds the toal score as an output
        /// parameter.
        /// </param>
        /// <param name="totalWeightedScore">
        /// A <see cref="decimal"/> that holds the toal weightage score as an 
        /// output parameter.
        /// </param>
        /// <remarks>
        /// A <see cref="DataTable"/> that holds customized data structure.
        /// </remarks>
        public DataTable GetRatingSummaryTable(DataSet rawData, 
            out decimal totalScore, out decimal totalWeightedScore)
        {
            // Check if the raw data is valid.
            if (rawData == null)
                throw new Exception("Raw data cannot be null");

            if (rawData.Tables.Count != TABLES_COUNT)
                throw new Exception("Raw data is invalid");

            // Get assessor details.
            List<KeyValuePair<string, int>> assessors = GetAssessors(rawData);

            // Get subject details.
            List<KeyValuePair<string, int>> subjects = GetSubjects(rawData);

            // Instantiate a data table object.
            DataTable table = new DataTable();

            // Assign total subject score.
            totalScore = 0m;
            if (rawData.Tables[OVERALL_SUMMARY_INDEX].Rows.Count > 0 &&
                !IsNullOrEmpty(rawData.Tables[OVERALL_SUMMARY_INDEX].Rows[0]["OVERALL_SCORE"]))
            {
                totalScore = Convert.ToDecimal(rawData.Tables[OVERALL_SUMMARY_INDEX].Rows[0]["OVERALL_SCORE"]);
            }

            // Assign total subject weighted score.
            totalWeightedScore = 0m;
            if (rawData.Tables[OVERALL_WEIGHTED_SUMMARY_INDEX].Rows.Count > 0 &&
                !IsNullOrEmpty(rawData.Tables[OVERALL_WEIGHTED_SUMMARY_INDEX].Rows[0]["OVERALL_WEIGHTAGE_SCORE"]))
            {
                totalWeightedScore = Convert.ToDecimal(rawData.Tables[OVERALL_WEIGHTED_SUMMARY_INDEX].Rows[0]["OVERALL_WEIGHTAGE_SCORE"]);
            }

            // Construct columns.
            ConstructColumns(table, assessors);

            // Construct rows.
            ConstructSubjectRows(table, rawData, assessors, subjects);

            // Construct overall assessor rating.
            ConstructOverallAssessorRatingRow(table, rawData, totalScore, totalWeightedScore);

            return table;
        }

        /// <summary>
        /// Method that construct the columns to the data table.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="assessors">
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </param>
        private void ConstructColumns(DataTable table, 
            List<KeyValuePair<string, int>> assessors)
        {
            // Add 'title' column.
            table.Columns.Add("TITLE");

            // Add accessor names as columns.
            foreach (KeyValuePair<string, int> assessor in assessors)
            {
                table.Columns.Add(assessor.Value.ToString());
                table.Columns[assessor.Value.ToString()].Caption = assessor.Key;
            }

            // Add 'score' column.
            table.Columns.Add("SCORE");
            table.Columns["SCORE"].Caption = "Score";

            // Add 'weight' column.
            table.Columns.Add("WEIGHT");
            table.Columns["WEIGHT"].Caption = "Weight";

            // Add 'weighted subject score' column.
            table.Columns.Add("WEIGHTED_SUBJECT_SCORE");
            table.Columns["WEIGHTED_SUBJECT_SCORE"].Caption = "Weighted Subject Score";

            // Add 'FLAG' column.
            table.Columns.Add("FLAG");
        }

        /// <summary>
        /// Method that construct the subject rows.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="assessors">
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </param>
        /// <param name="subjects">
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </param>
        private void ConstructSubjectRows(DataTable table, 
            DataSet rawData,
            List<KeyValuePair<string, int>> assessors, 
            List<KeyValuePair<string, int>> subjects)
        {
            // Loop through the subject and add the subject wise rating.
            foreach (KeyValuePair<string, int> subject in subjects)
            {
                // Create a new row.
                DataRow row = table.NewRow();

                // Set the title.
                row["TITLE"] = subject.Key;

                // Set the 'FLAG' value.
                row["FLAG"] = "S";

                double weightedSubjectScore = 0.0;

                // Retrieve weighted subject score.
                GetSubjectScore(rawData, subject.Value, out weightedSubjectScore);

                // Assign weighted subject score.
                row["WEIGHTED_SUBJECT_SCORE"] = weightedSubjectScore;

                // Add the row to the table.
                table.Rows.Add(row);

                // Construct question rows.
                ConstructQuestionRows(table, rawData, assessors, subject.Value);
            }
        }

        /// <summary>
        /// Method that construct the overall assessor rating row.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="totalSubjectScore">
        /// A <see cref="decimal"/> that holds the total subject score.
        /// </param>
        /// <param name="totalWeightedSubjectScore">
        /// A <see cref="decimal"/> that holds the total weighted subject 
        /// score.
        /// </param>
        private void ConstructOverallAssessorRatingRow(DataTable table,
            DataSet rawData, decimal totalSubjectScore, decimal totalWeightedSubjectScore)
        {
            // Create a new row.
            DataRow row = table.NewRow();

            // Set the 'FLAG' value.
            row["FLAG"] = "O";

            // Set title for 'score' column.
            row["WEIGHT"] = "Total Weighted Score";

            // Assign total weighted subject score.
            row["WEIGHTED_SUBJECT_SCORE"] = totalWeightedSubjectScore;

            // Add the row to the table.
            table.Rows.Add(row);
        }

        /// <summary>
        /// Method that construct the question rows.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="assessors">
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="subjects">
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </param>
        private void ConstructQuestionRows(DataTable table,
            DataSet rawData, List<KeyValuePair<string, int>> assessors, int subjectID)
        {
            // Get distinct questions for the given subject ID.
            List<KeyValuePair<string, int>> questions = GetQuestions(rawData, subjectID);

            // Do not proceed if questions are not present.
            if (questions == null || questions.Count == 0)
                return;

            foreach (KeyValuePair<string, int> question in questions)
            {
                // Create a new row.
                DataRow row = table.NewRow();

                // Set the title.
                row["TITLE"] = question.Key;

                // Set the 'FLAG' value.
                row["FLAG"] = "Q";

                // Loop through the assessor and add the assessor rating
                // against the subject.
                foreach (KeyValuePair<string, int> assessor in assessors)
                {
                    // Get question rating.
                    row[assessor.Value.ToString()] = GetQuestionRating(rawData, subjectID, question.Value, assessor.Value);
                }

                // Assign the question score.
                double weightage = 0.0;
                row["SCORE"] = GetQuestionScore(rawData, subjectID, question.Value, out weightage);

                // Assign weightage.
                row["WEIGHT"] = weightage;

                // Add the row to the table.
                table.Rows.Add(row);
            }
        }

        /// <summary>
        /// Method that retrieves the subject rating for the given subject ID 
        /// and assessor ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the subject rating.
        /// </returns>
        private double GetSubjectRating(DataSet rawData, int subjectID, int assessorID)
        {
            // Construct filter expression.
            string expression = "CAT_SUB_ID=" + subjectID + " and " + "ASSESSOR=" + assessorID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[SUBJECT_DETAILS_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return 0;

            // Check if value if null or empty.
            if (IsNullOrEmpty(filteredRows[0]["SUBJECT_RATING"]))
                return 0;

            return Convert.ToDouble(filteredRows[0][ "SUBJECT_RATING"].ToString());
        }

        /// <summary>
        /// Method that retrieves the assessor rating for the given assessor ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the assessor rating.
        /// </returns>
        private double GetAssessorRating(DataSet rawData, int assessorID)
        {
            // Construct filter expression.
            string expression = "ASSESSOR_ID=" + assessorID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[ASSESSOR_DETAILS_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return 0;

            // Check if value if null or empty.
            if (IsNullOrEmpty(filteredRows[0]["OVERALL_RATING"]))
                return 0;

            return Convert.ToDouble(filteredRows[0]["OVERALL_RATING"].ToString());
        }

        /// <summary>
        /// Method that retrieves the question rating for the given question ID 
        /// and assessor ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="questionID">
        /// A <see cref="int"/> that holds the question ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the subject rating.
        /// </returns>
        private double GetQuestionRating(DataSet rawData, int subjectID, int questionID, int assessorID)
        {
            // Construct filter expression.
            string expression = "CAT_SUB_ID=" + subjectID + " and " + "TEST_QUESTION_ID=" + questionID + " and " + "ASSESSOR=" + assessorID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[QUESTION_DETAILS_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return -1;

            // Check if value if null or empty.
            if (IsNullOrEmpty(filteredRows[0]["QUESTION_RATING"]))
                return 0;

            return Convert.ToDouble(filteredRows[0]["QUESTION_RATING"].ToString());
        }

        /// <summary>
        /// Method that retrieves the subject rating summary for the given 
        /// subject ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the subject rating.
        /// </returns>
        private double GetSubjectRatingSummary(DataSet rawData, int subjectID)
        {
            // Construct filter expression.
            string expression = "CAT_SUB_ID=" + subjectID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[SUBJECT_RATING_SUMMARY_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return 0;

            // Check if value if null or empty.
            if (IsNullOrEmpty(filteredRows[0]["OVERALL_SUBJECT_RATING"]))
                return 0;

            return Convert.ToDouble(filteredRows[0]["OVERALL_SUBJECT_RATING"].ToString());
        }

        /// <summary>
        /// Method that retrieves the question score for the given subject ID 
        /// and question ID. It also retreives the weightage as output 
        /// parameter.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="questionID">
        /// A <see cref="int"/> that holds the question ID.
        /// </param>
        /// <param name="weightage">
        /// A <see cref="double"/> that holds the question weightage as an
        /// output parameter.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the subject rating.
        /// </returns>
        private double GetQuestionScore(DataSet rawData, int subjectID, int questionID, out double weightage)
        {
            // Assign default value to weightage.
            weightage = 0.0;

            // Construct filter expression.
            string expression = "CAT_SUB_ID=" + subjectID + " and " + "TEST_QUESTION_ID=" + questionID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[QUESTION_RATING_SUMMARY_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return -1;

            // Assign weightage.
            if (!IsNullOrEmpty(filteredRows[0]["QUESTION_WEIGHTAGE"]))
            {
                weightage = Convert.ToDouble(filteredRows[0]["QUESTION_WEIGHTAGE"].ToString());
            }

            // Check if value is null or empty.
            if (IsNullOrEmpty(filteredRows[0]["QUESTION_RATING_SUMMARY"]))
                return -1;

            return Convert.ToDouble(filteredRows[0]["QUESTION_RATING_SUMMARY"].ToString());
        }

        /// <summary>
        /// Method that retrieves the subject score for the given subject ID.
        /// It also retreives the weighted subject score as output parameter.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="weightedSubjectScore">
        /// A <see cref="double"/> that holds the weighted subject score as an
        /// output parameter.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the subject score.
        /// </returns>
        private double GetSubjectScore(DataSet rawData, int subjectID,
            out double weightedSubjectScore)
        {
            // Assign default value to weighted subject score.
            weightedSubjectScore = 0.0;

            // Construct filter expression.
            string expression = "CAT_SUB_ID=" + subjectID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[SUBJECT_RATING_SUMMARY_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return 0;

            // Assign weightage.
            if (!IsNullOrEmpty(filteredRows[0]["OVERALL_SUBJECT_WEIGHTAGE_RATING"]))
            {
                weightedSubjectScore = Convert.ToDouble(filteredRows[0]["OVERALL_SUBJECT_WEIGHTAGE_RATING"].ToString());
            }

            // Check if value is null or empty.
            if (IsNullOrEmpty(filteredRows[0]["OVERALL_SUBJECT_RATING"]))
                return 0;

            return Convert.ToDouble(filteredRows[0]["OVERALL_SUBJECT_RATING"].ToString());
        }

        /// <summary>
        /// Method that retrieves the list of accessors.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </returns>
        private List<KeyValuePair<string, int>> GetAssessors(DataSet rawData)
        {
            List<KeyValuePair<string, int>> assessors = new List<KeyValuePair<string, int>>();

            foreach (DataRow row in rawData.Tables[ASSESSOR_DETAILS_INDEX].Rows)
            {
                assessors.Add(new KeyValuePair<string,int>(
                    row["ASSESSOR_NAME"].ToString().Trim(),
                    Convert.ToInt32(row["ASSESSOR_ID"].ToString().Trim())));
            }
            return assessors;
        }

        /// <summary>
        /// Method that retrieves the list of unique subjects.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </returns>
        private List<KeyValuePair<string, int>> GetSubjects(DataSet rawData)
        {
            // Get distinct records from assessor details table.
            DataTable distinctTable = rawData.Tables[SUBJECT_DETAILS_INDEX].
                DefaultView.ToTable(true, "CAT_SUB_ID", "SUBJECT_NAME");

            if (distinctTable == null || distinctTable.Rows.Count == 0)
                throw new Exception("No subject details found");

            List<KeyValuePair<string, int>> subjects = new List<KeyValuePair<string, int>>();

            foreach (DataRow row in distinctTable.Rows)
            {
                subjects.Add(new KeyValuePair<string, int>(
                    row["SUBJECT_NAME"].ToString().Trim(),
                    Convert.ToInt32(row["CAT_SUB_ID"].ToString().Trim())));
            }
            return subjects;
        }

        /// <summary>
        /// Method that retrieves the list of unique questions for the given
        /// subject ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </returns>
        private List<KeyValuePair<string, int>> GetQuestions(DataSet rawData, int subjectID)
        {
            // Construct filter expression.
            string expression = "CAT_SUB_ID=" + subjectID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[QUESTION_DETAILS_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return null;

            // Add the filtered rows to a temporary table.
            DataTable filteredTable = rawData.Tables[QUESTION_DETAILS_INDEX].Clone();
            foreach (DataRow filteredRow in filteredRows)
                filteredTable.ImportRow(filteredRow);
            
            // Get distinct records from question details table.
            DataTable distinctTable = filteredTable.DefaultView.ToTable
                (true, "TEST_QUESTION_ID", "QUESTION_DESC");

            if (distinctTable == null || distinctTable.Rows.Count == 0)
                return null;

            List<KeyValuePair<string, int>> questions = new List<KeyValuePair<string, int>>();

            foreach (DataRow row in distinctTable.Rows)
            {
                questions.Add(new KeyValuePair<string, int>(
                    row["QUESTION_DESC"].ToString().Trim(),
                    Convert.ToInt32(row["TEST_QUESTION_ID"].ToString().Trim())));
            }
            return questions;
        }

        /// <summary>
        /// Method that checks if the given value is null or empty.
        /// </summary>
        /// <param name="value">
        /// A <see cref="object"/> that holds the value.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. True if null or empty 
        /// and false otherwise.
        /// </returns>
        private bool IsNullOrEmpty(object value)
        {
            if (value == null || value == DBNull.Value || value.ToString().Trim().Length == 0)
                return true;

            return false;
        }
    }
}
