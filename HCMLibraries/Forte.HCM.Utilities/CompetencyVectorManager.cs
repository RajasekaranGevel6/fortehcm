﻿#region Directives                                                             

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.Utilities
{
    public class CompetencyVectorManager
    {
        #region Declarations                                                   

        private const int NO_OF_YEARS_PARAMETER_ID = 1;
        private const int RECENCY_PARAMETER_ID = 2;

        /// <summary>
        /// A <see cref="int"/> constant that holds the group ID for
        /// technical skills vector group.
        /// </summary>
        private const int TECHNICAL_SKILLS_GROUP_ID = 1;

        /// <summary>
        /// A <see cref="int"/> constant that holds the group ID for
        /// verticals vector group.
        /// </summary>
        private const int VERTICALS_GROUP_ID = 2;

        /// <summary>
        /// A <see cref="int"/> constant that holds the group ID for
        /// roles vector group.
        /// </summary>
        private const int ROLES_GROUP_ID = 3;

        #endregion Declarations

        #region Public Methods                                                 

        /// <summary>
        /// This method gets the list of candidate competency vector
        /// from the Project details of the candidate.
        /// </summary>
        /// <param name="projectDetails"></param>
        /// <param name="competencyVectors"></param>
        /// <returns></returns>
        public List<CandidateCompetencyVectorDetail> GetCompetencyVectors
            (List<Project> projectDetails, List<CompetencyVectorDetails>
            competencyVectors)
        {
            // Get technical skills group.
            var technicalCompetencyVectors = from competencyVector in competencyVectors
                where Convert.ToInt32(competencyVector.VectorGroupID) == TECHNICAL_SKILLS_GROUP_ID
                select competencyVector;
            
            // Get roles group.
            var rolesCompetencyVectors = from competencyVector in competencyVectors
                                         where Convert.ToInt32(competencyVector.VectorGroupID) == ROLES_GROUP_ID
                                         select competencyVector;

            // Get roles group.
            var verticalsCompetencyVectors = from competencyVector in competencyVectors
                                             where Convert.ToInt32(competencyVector.VectorGroupID) == VERTICALS_GROUP_ID
                                             select competencyVector;

            // Check if project details exist.
            if (projectDetails == null || projectDetails.Count == 0)
                return null;

            // Instantiate candidate competency vector details.
            List<CandidateCompetencyVectorDetail> vectors = new
               List<CandidateCompetencyVectorDetail>();

            List<ProjectSkillDetail> projectSkills = new List<ProjectSkillDetail>();
            ///List<ProjectSkillDetail> rolesProjectSkills = new List<ProjectSkillDetail>();
            //List<ProjectSkillDetail> verticalsProjectSkills = new List<ProjectSkillDetail>();

            // Loop through the project details and construct the competency vectors.
            foreach (Project projectDetail in projectDetails)
            {
                // Construct technical skills.
                ConstructTechnicalSkills(technicalCompetencyVectors.ToList(),
                    projectDetail, projectSkills);

                // Construct roles.
                ConstructRoles(rolesCompetencyVectors.ToList(),
                    projectDetail, projectSkills);

                // Construct verticals.
                ConstructVerticals(verticalsCompetencyVectors.ToList(),
                    projectDetail, projectSkills);
            }

            CandidateCompetencyVectorDetail vector = null;

            // Construct competency vectors for project skills.
            foreach (ProjectSkillDetail projectSkill in projectSkills)
            {
                // No. of years.
                vector = new CandidateCompetencyVectorDetail();
                vector.VectorID = projectSkill.VectorID;
                vector.VectorName = projectSkill.VectorName;
                vector.VectorGroupID = projectSkill.GroupId;
                vector.ParameterID = NO_OF_YEARS_PARAMETER_ID;
                vector.Value = projectSkill.Experience;
                vector.VectorGroup = projectSkill.GroupName;
                vectors.Add(vector);
                // Recency.
                vector = new CandidateCompetencyVectorDetail();
                vector.VectorID = projectSkill.VectorID;
                vector.VectorName = projectSkill.VectorName;
                vector.VectorGroupID = projectSkill.GroupId;
                vector.ParameterID = RECENCY_PARAMETER_ID;
                vector.VectorGroup = projectSkill.GroupName;
                vector.Value = projectSkill.Recency.ToString();
                vectors.Add(vector);
            }

            return vectors;
        }

        #endregion Public Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that constructs technical skills from the project details
        /// </summary>
        /// <param name="technicalCompetencyVectors">
        /// A <see cref="List"/> of competency vectors that contains avaliable
        /// technical skills in database.
        /// </param>
        /// <param name="projectDetail">
        /// A <see cref="List"/> that contains candidate project deatils.
        /// </param>
        /// <param name="projectSkills">
        /// A <see cref="List"/> that contains the vector details of the project
        /// </param>
        private void ConstructTechnicalSkills(List<CompetencyVectorDetails> technicalCompetencyVectors,
            Project projectDetail, List<ProjectSkillDetail> projectSkills)
        {
            // Check if technical competency vectors is null or empty.
            if (technicalCompetencyVectors == null || technicalCompetencyVectors.Count == 0)
                return;
            // Check if contents is null or empty.
            if (projectDetail.Environment == null || projectDetail.Environment.Trim().Length == 0)
                return;
            // Loop through the configured competency vectors and identify 
            // which competency vector is present in the project environment.
            foreach (CompetencyVectorDetails competencyVector in
                technicalCompetencyVectors)
            {
                // Check if the competency vector name or vector name alias present.
                if (Utility.IsNullOrEmpty(competencyVector.VectorName) ||
                    Utility.IsNullOrEmpty(competencyVector.AliasName))
                    continue;
                // Check if the pattern matches. If not matches process the next
                // competency vector.
                if (!IsPatternMatch(projectDetail.Environment,
                    GetPattern(competencyVector.AliasName)))
                    continue;
                // Construct project skills.
                ConstructProjectSkills(competencyVector, projectDetail, projectSkills,
                    competencyVector.VectorGroupID, competencyVector.VectorName, competencyVector.VectorGroup);
            }
        }

        /// <summary>
        /// Method that constructs roles from the project details.
        /// </summary>
        /// <param name="rolesCompetencyVectors">
        /// A <see cref="List"/> of competency vectors that contains avaliable
        /// roles in database.
        /// </param>
        /// <param name="projectDetail">
        /// A <see cref="List"/> that contains candidate project deatils.
        /// </param>
        /// <param name="projectSkills">
        /// A <see cref="List"/> that contains the vector details of the project
        /// </param>
        private void ConstructRoles(List<CompetencyVectorDetails> rolesCompetencyVectors,
            Project projectDetail, List<ProjectSkillDetail> projectSkills)
        {
            // Check if technical competency vectors is null or empty.
            if (rolesCompetencyVectors == null || rolesCompetencyVectors.Count == 0)
                return;
            // Check if contents is null or empty.
            if (projectDetail.PositionTitle == null || projectDetail.PositionTitle.Trim().Length == 0)
                return;
            // Loop through the configured competency vectors and identify 
            // which competency vector is present in the project environment.
            foreach (CompetencyVectorDetails competencyVector in
                rolesCompetencyVectors)
            {
                // Check if the competency vector name or vector name alias present.
                if (Utility.IsNullOrEmpty(competencyVector.VectorName) ||
                    Utility.IsNullOrEmpty(competencyVector.AliasName))
                    continue;
                // Check if the pattern matches. If not matches process the next
                // competency vector.
                if (!IsPatternMatch(projectDetail.PositionTitle,
                    GetPattern(competencyVector.AliasName)))
                    continue;
                // Construct project skills.
                ConstructProjectSkills(competencyVector, projectDetail, projectSkills,
                    competencyVector.VectorGroupID, competencyVector.VectorName, competencyVector.VectorGroup);
            }
        }

        /// <summary>
        /// Method that constructs the verticals from the project details
        /// </summary>
        /// <param name="verticalsCompetencyVectors">
        /// A <see cref="List"/> of competency vectors that contains avaliable
        /// verticals in database.
        /// </param>
        /// <param name="projectDetail">
        /// A <see cref="List"/> that contains candidate project deatils.
        /// </param>
        /// <param name="projectSkills">
        /// A <see cref="List"/> that contains the vector details of the project
        /// </param>
        private void ConstructVerticals(List<CompetencyVectorDetails> verticalsCompetencyVectors,
            Project projectDetail, List<ProjectSkillDetail> projectSkills)
        {
            // Check if technical competency vectors is null or empty.
            if (verticalsCompetencyVectors == null || verticalsCompetencyVectors.Count == 0)
                return;
            // Check if contents is null or empty.
            if (projectDetail.ProjectDescription == null || projectDetail.ProjectDescription.Trim().Length == 0)
                return;
            // Loop through the configured competency vectors and identify 
            // which competency vector is present in the project environment.
            foreach (CompetencyVectorDetails competencyVector in
                verticalsCompetencyVectors)
            {
                // Check if the competency vector name or vector name alias present.
                if (Utility.IsNullOrEmpty(competencyVector.VectorName) ||
                    Utility.IsNullOrEmpty(competencyVector.AliasName))
                    continue;
                // Check if the pattern matches. If not matches process the next
                // competency vector.
                if (!IsPatternMatch(projectDetail.ProjectDescription,
                    GetPattern(competencyVector.AliasName)))
                    continue;
                // Construct project skills.
                ConstructProjectSkills(competencyVector, projectDetail, projectSkills,
                    competencyVector.VectorGroupID, competencyVector.VectorName, competencyVector.VectorGroup);
            }
        }

        /// <summary>
        /// This method contructs the competency vector list
        /// </summary>
        /// <param name="competencyVector">
        /// That contains the competency vector 
        /// to add to the list.
        /// </param>
        /// <param name="projectDetail">
        /// That contains the project deatil adding to the 
        /// return vector list. 
        /// </param>
        /// <param name="projectSkills">
        /// A <see cref="List"/> that contains the matched
        /// vector list
        /// </param>
        /// <param name="GroupId">
        /// A <see cref="string"/> that holds the group id of the current
        /// competency vector
        /// </param>
        /// <param name="VectorName">
        /// A <see cref="string"/> that holds the vector name of the 
        /// current competency vector
        /// </param>
        /// <param name="GroupName">
        /// A <see cref="string"/> that holds the group name of the 
        /// current competency vector
        /// </param>
        private void ConstructProjectSkills(CompetencyVectorDetails competencyVector,
            Project projectDetail, List<ProjectSkillDetail> projectSkills, 
            string GroupId, string VectorName, string GroupName)
        {
            // Build and add the project skill to the list.
            ProjectSkillDetail projectSkill = new ProjectSkillDetail();
            projectSkill.VectorID = competencyVector.VectorID;
            projectSkill.VectorName = competencyVector.VectorName;
            // Build experience.
            if (projectDetail.StartDate != DateTime.MinValue &&
                projectDetail.EndDate != DateTime.MinValue)
            {
                projectSkill.Experience = GetYearDifference
                    (projectDetail.StartDate, projectDetail.EndDate);
            }
            // Build recency.
            if (projectDetail.EndDate != DateTime.MinValue)
                projectSkill.Recency = projectDetail.EndDate.Year;
            // If no project skills already exist, just add it.
            if (projectSkills.Count == 0)
                projectSkills.Add(projectSkill);
            else
            {
                // Check if the vector ID already present.
                ProjectSkillDetail findProjectSkill = projectSkills.Find
                    (item => item.VectorID == projectSkill.VectorID);
                // If not present add it the list. Else update the details. 
                if (findProjectSkill == null)
                    projectSkills.Add(projectSkill);
                else
                {
                    // Update experience.
                    findProjectSkill.Experience = AddExperience
                        (findProjectSkill.Experience, projectSkill.Experience);
                    // Update recency.
                    if (projectSkill.Recency > findProjectSkill.Recency)
                        findProjectSkill.Recency = projectSkill.Recency;
                }
            }
            projectSkill.GroupName = GroupName.Trim();
            projectSkill.GroupId = GroupId;
            projectSkill.VectorName = VectorName.Trim();
        }

        /// <summary>
        /// Method that adds experience to the previous experience.
        /// </summary>
        /// <param name="oldExperience">
        /// A <see cref="string"/> that holds the previous experience
        /// of the vector.
        /// </param>
        /// <param name="newExperience">
        /// A <see cref="string"/> that holds the current experience
        /// of the vector.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that conatins the total of
        /// current and old experience in Years.Month format.
        /// </returns>
        private string AddExperience(string oldExperience, string newExperience)
        {
            if (Utility.IsNullOrEmpty(oldExperience) || Utility.IsNullOrEmpty(newExperience))
                return string.Empty;
            //if (Utility.IsNullOrEmpty(oldExperience))
            //    return newExperience;
            //if (Utility.IsNullOrEmpty(newExperience))
            //    return oldExperience;
            string[] oldUnits = oldExperience.Split('.');
            string[] newUnits = newExperience.Split('.');
            int newYear = Convert.ToInt32(oldUnits[0]) + Convert.ToInt32(newUnits[0]);
            int newMonths = Convert.ToInt32(oldUnits[1]) + Convert.ToInt32(newUnits[1]);
            if (newMonths >= 12)
            {
                newYear++;
                newMonths -= 12;
            }
            return newYear + "." + newMonths;
        }

        /// <summary>
        /// This method Gets the Years and months difference 
        /// between two dates.
        /// </summary>
        /// <param name="from">
        /// A <see cref="DateTime"/> that holds the from date
        /// </param>
        /// <param name="to">
        /// A <see cref="DateTime"/> that holds the to date
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that conatins the difference of
        /// from and to and returns in Years.Month format.
        /// </returns>
        private string GetYearDifference(DateTime from, DateTime to)
        {
            TimeSpan diff = to.Subtract(from);
            int year = diff.Days / 365;
            int months = (diff.Days % 365) / 30;
            return year + "." + months;
        }

        /// <summary>
        /// This method gets the pattern for the regular expression
        /// </summary>
        /// <param name="contents">
        /// A <see cref="string"/> that contains the pattern to apply
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains the regular expression
        /// pattern
        /// </returns>
        private string GetPattern(string contents)
        {
            // Remove comma and empty spaces followed that.
            string pattern = contents.Replace(", ", ",");
            // Change comma to regex pattern separator.
            pattern = pattern.Replace(",", "|");
            // Construct regex pattern.
            pattern = string.Format("{0}{1}{2}", "(", pattern, ")");
            return pattern;
        }

        /// <summary>
        /// This method returns whether the string contains in
        /// the pattern or not.
        /// </summary>
        /// <param name="contents">
        /// A <see cref="string"/> that contains the content 
        /// of the string to search.
        /// </param>
        /// <param name="pattern">
        /// A <see cref="string"/> that contains the pattern for
        /// regular expression
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> states whether the content is present
        /// in pattern or not.
        /// </returns>
        private bool IsPatternMatch(string contents, string pattern)
        {
            //return Regex.IsMatch(contents, pattern, RegexOptions.IgnoreCase);
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(contents);
            if (matches == null || matches.Count == 0)
                return false;
            return true;
        }

        #endregion Private Methods
    }
}
