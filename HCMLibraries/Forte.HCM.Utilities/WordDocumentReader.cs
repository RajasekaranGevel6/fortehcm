﻿#region Directives                                                   
using System;

using Microsoft.Office.Interop.Word;
#endregion  Directives

namespace Forte.HCM.Utilities
{
    public class WordDocumentReader
    {
        /// <summary>
        /// Represents the method that is used to return the text ofthe 
        /// pdf file
        /// </summary>
        /// <param name="fileName">
        /// A<see cref="string"/>that holds the file name
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the text of the file
        /// </returns>
        public string GetPreviewText(string fileName)
        {

            object nullobject = Type.Missing;

            object refFileName = fileName;

            string content = string.Empty;

            ApplicationClass applicationClass = new ApplicationClass();

            Document wordDocument = applicationClass.Documents.Open(ref refFileName,
                 ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject, ref nullobject,
                    ref nullobject, ref nullobject);

            content = wordDocument.Content.Text;

            applicationClass.Quit(ref nullobject, ref nullobject, ref nullobject);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(applicationClass);

            return content;


        }
    }
}
