﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientInfoDataManager.aspx.cs
// File that process the raw data and constructs the customized html table
// for position profile review.

#endregion Header 

#region Directives                                                             

using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using System.Text.RegularExpressions;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Class that process the raw data and constructs the customized data
    /// for position profile review.
    /// </summary>
    public class ClientInfoDataManager
    {
        #region Public Methods                                                 

        /// <summary>
        /// Method to assign client contact info against the position profile id
        /// </summary>
        /// <param name="assignValues">
        /// A <see cref="Label"/> that assigns the value to the passed controls
        /// </param>
        /// <param name="clientInfo">
        /// A <see cref="string"/> that holds the action available status.
        /// </param>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type whether requested info is department or contact.
        /// </param>
        /// <param name="showLinks">
        /// A <see cref="bool"/> that holds the value to enable link / label.
        /// </param>
        public void GetClientDepartmentContactInfo(Label assignValues, string clientInfo,
            string type, bool showLinks)
        {
            if (!Utility.IsNullOrEmpty(clientInfo))
            {
                string toolTipMsg = string.Empty;
                //Department
                if (type == "CD")
                    toolTipMsg = "department";

                if (type == "CC")
                    toolTipMsg = "contact";

                
                // Split the string
                string[] arrayInfo = clientInfo.Split(new char[] { '~' });

                int infoCnt = 0;
                foreach (string info in arrayInfo)
                {
                    // Add a comma literal.
                    if (infoCnt != 0)
                    {
                        Literal literal = new Literal();
                        literal.Text = ", ";

                        // Add literal control to the cell.
                        assignValues.Controls.Add(literal);
                    }

                    if (!Utility.IsNullOrEmpty(info))
                    {
                        // Get comma separated contact details.
                        string[] arrayDisplayContent = info.Split(new char[] { '#' });

                        if (showLinks == true)
                        {
                            LinkButton linkButton = new LinkButton();
                            linkButton.Text = arrayDisplayContent[1];
                            linkButton.CssClass = "label_field_text_link_button";

                            linkButton.ToolTip = string.Format("Click here to view {0}",toolTipMsg);
                            linkButton.Attributes.Add("onclick", "javascript:return ShowViewClientInformation('" + arrayDisplayContent[0].ToString() + "','" + type + "');");

                            // Add link button control to the cell.
                            assignValues.Controls.Add(linkButton);
                        }
                        else
                        {
                            Label label = new Label();
                            label.Text = arrayDisplayContent[1].ToString();
                            label.CssClass = "client_dept_contact_with_no_links";
                            label.ToolTip = toolTipMsg;

                            // Add link button control to the cell.
                            assignValues.Controls.Add(label);
                        }
                    }
                    infoCnt++;
                }
            }
        }

        #endregion Public Methods

    }
}