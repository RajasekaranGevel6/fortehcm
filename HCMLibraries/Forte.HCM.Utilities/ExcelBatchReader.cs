﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExcelBatchReader.cs
// File that represents the excel reader object that will helps to read
// data from the excel file. The utility also helps to retrieve the list
// of sheets present in the excel file, the count of rows, etc.

#endregion Header

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using Microsoft.Practices.EnterpriseLibrary.Data;
using System.IO;

#endregion Directives

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Represents the class that helps to read data from the excel file.
    /// The utility also helps to retrieve the list of sheets present in 
    /// the excel file, the count of rows, etc
    /// </summary>
    public class ExcelBatchReader
    {
        #region Private variables

        /// <summary>
        /// A <see cref="Database"/> that holds the database object.
        /// </summary>
        protected Database database;

        #endregion Private variables

        #region Public Methods

        /// <summary>
        /// Represents the constructor.
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the excel file name.
        /// </param>
        public ExcelBatchReader(string fileName)
        {
            //Creates a generic database for the oledb provider
            database = new GenericDatabase(@"Provider=Microsoft.ACE.OLEDB.12.0;" +
               "Data Source=" + fileName + ";Extended Properties='Excel 12.0;IMEX=1;HDR=YES;'",
               DbProviderFactories.GetFactory("System.Data.OleDb"));
            //database = new GenericDatabase("Provider=Microsoft.Jet.OLEDB.4.0;" +
            //    "Data Source=" + fileName + ";Extended Properties='Excel 8.0;IMEX=1'",
            //    DbProviderFactories.GetFactory("System.Data.OleDb"));
        }

        /// <summary>
        /// Method that is used to get the list sheet name of the excel
        /// and send back the list of sheet name
        /// </summary>
        /// <param name="filename">
        /// A<see cref="string"/>that holds the file name
        /// </param>
        /// <returns>
        /// A<see cref="List<string>"/>that holds the list of sheet name
        /// </returns>
        public List<string> GetSheetNames()
        {
            DataTable dt = new DataTable();

            //Create a connection for a data base
            DbConnection dbConnection = database.CreateConnection();

            //Open connection
            dbConnection.Open();

            //Get the schemar of the datasource.
            //It will return the sheet name as datatable
            dt = dbConnection.GetSchema("Tables", null);

            List<string> excelSheets = new List<string>();

            //Add each excel sheet name and its question count to the list
            foreach (DataRow row in dt.Rows)
            {
                //Data table contains duplicate values.Sheet name always ends with $.
                //To take only correct sheet name
                if (row["TABLE_NAME"].ToString().Contains("$")
                    && row["TABLE_NAME"].ToString().IndexOf("$")
                    == row["TABLE_NAME"].ToString().Length - 1)
                {
                    excelSheets.Add(row["TABLE_NAME"].ToString());
                }
            }

            //Close the connection
            dbConnection.Close();

            //Return the list of excel sheets 
            return excelSheets;
        }

        /// <summary>
        /// Represents the method that returns the records count in the Excel 
        /// file.
        /// </summary>
        /// <returns>
        /// A <see cref="int"/> that holds the records count.
        /// </returns>
        public int GetCount(string sheetName)
        {
            try
            {
                int questionCount = 0;

                //Command to select count of the sheet 
                string command = "Select count(*) from [" + sheetName + "]";

                //Creates a command 
                DbCommand oledbCommand = database.GetSqlStringCommand(command);

                //Get the question count from excel
                questionCount = int.Parse(database.ExecuteScalar(oledbCommand).ToString());

                return questionCount;
            }
            catch (Exception exception)
            {
                if (exception.Message.Contains("parameters"))
                {
                    throw new Exception(string.Format("Sheet {0} contains no" +
                        "entries. Please provide valid excel", sheetName));
                }
                return 0;
            }
        }

        #endregion Public Methods

        #region Private Methods

        #endregion Private Methods
    }

    
}