﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Forte.HCM.Utilities
{
    /// <summary>
    /// Class that process the raw data and constructs the customized data
    /// structures.
    /// </summary>
    public class OnlineInterviewDataManager
    {
        #region Private Constant Variables
        /// <summary>
        /// A <see cref="int"/> that holds the tables count.
        /// </summary>
        private const int TABLES_COUNT = 5;

        /// <summary>
        /// A <see cref="int"/> that holds the assessor details index.
        /// </summary>
        private const int ASSESSOR_DETAILS_INDEX = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the subject details index.
        /// </summary>
        private const int SUBJECT_DETAILS_INDEX = 1;

        /// <summary>
        /// A <see cref="int"/> that holds the subject rating summary index.
        /// </summary>
        private const int SUBJECT_RATING_SUMMARY_INDEX = 2;

        /// <summary>
        /// A <see cref="int"/> that holds the question details index.
        /// </summary>
        private const int QUESTION_DETAILS_INDEX = 2;

        /// <summary>
        /// A <see cref="int"/> that holds the question rating summary index.
        /// </summary>
        private const int QUESTION_RATING_SUMMARY_INDEX = 4;

        /// <summary>
        /// A <see cref="int"/> that holds the overall summary index.
        /// </summary>
        private const int OVERALL_SUMMARY_INDEX = 5;

        /// <summary>
        /// A <see cref="int"/> that holds the overall weighted summary index.
        /// </summary>
        private const int OVERALL_WEIGHTED_SUMMARY_INDEX = 6;
        #endregion

        /// <summary>
        /// Method that constructs the customized data structure for rating
        /// summary.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="totalScore">
        /// A <see cref="decimal"/> that holds the toal score as an output
        /// parameter.
        /// </param>
        /// <param name="totalWeightedScore">
        /// A <see cref="decimal"/> that holds the toal weightage score as an 
        /// output parameter.
        /// </param>
        /// <remarks>
        /// A <see cref="DataTable"/> that holds customized data structure.
        /// </remarks>
        public DataTable GetRatingSummaryTable(DataSet rawData, int subjectID,
            out decimal totalScore, out decimal totalWeightedScore)
        {
            totalScore = 0;
            totalWeightedScore = 0;

            // Check if the raw data is valid.
            if (rawData == null)
                throw new Exception("Raw data cannot be null");

            // Get assessor details.
            List<KeyValuePair<string, int>> assessors = GetAssessors(rawData);

            // Get assessor overall rating details.
            // List<KeyValuePair<int, decimal>> overAllRating = GetOverAllRating(rawData, subjectID);

            DataTable dtOverAllRating = GetOverAllRating(rawData, subjectID, "T");

            // Instantiate a data table object.
            DataTable table = new DataTable();

            //Construct Columns
            ConstructColumns(table, assessors);

            // Construct rows.
            ConstructOverAllRatingRows(table, rawData, assessors, dtOverAllRating);

            ConstructSubjectRows(table, rawData, assessors, dtOverAllRating);

            ConstructAssessorComments(table, rawData, assessors, subjectID);
            return table;
        }

        /// <summary>
        /// Method that construct the subject rows.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="assessors">
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </param>
        /// <param name="subjects">
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </param>
        private void ConstructSubjectRows(DataTable table,
            DataSet rawData,
            List<KeyValuePair<string, int>> assessors,
            DataTable dtOverAllRating)
        {
            DataRow dr = dtOverAllRating.Rows[0];

            // Create a new row.
            DataRow row = table.NewRow();

            // Set the title.
            row["TITLE"] = "Questions";// +"$" + dr["SKILL_ID"].ToString();

            // Set the 'FLAG' value.
            row["FLAG"] = "L";

            // Add the row to the table.
            table.Rows.Add(row);


            // Construct question rows.
            ConstructQuestionRows(table, rawData, assessors, Convert.ToInt32(dr["SKILL_ID"]));
        }

        private void ConstructAssessorComments(DataTable table,
            DataSet rawData, List<KeyValuePair<string, int>> assessors, int subjectID)
        {
            DataRow row = table.NewRow();

            row["TITLE"] = "Subject Specific Comments";
            // Set the 'FLAG' value.
            row["FLAG"] = "S";
            // Loop through the assessor and add the assessor rating
            // against the subject.
            foreach (KeyValuePair<string, int> assessor in assessors)
            {
                // Get question rating.
                row[assessor.Value.ToString()] = GetSubjectSpecificComments(rawData, subjectID, assessor.Value);
            }

            table.Rows.Add(row);
        }

        /// <summary>
        /// Method that retrieves the question rating for the given question ID 
        /// and assessor ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="questionID">
        /// A <see cref="int"/> that holds the question ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the subject rating.
        /// </returns>
        public string GetSubjectSpecificComments(DataSet rawData, int subjectID, int assessorID)
        {
            // Construct filter expression.
            string expression = "SKILL_ID=" + subjectID + " and " + "ASSESSOR_ID=" + assessorID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[2].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return string.Empty;

            // Check if value if null or empty.
            if (IsNullOrEmpty(filteredRows[0]["COMMENTS"]))
                return string.Empty;

            return Convert.ToString(filteredRows[0]["COMMENTS"].ToString());
        }

        /// <summary>
        /// Method that construct the question rows.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="assessors">
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="subjects">
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </param>
        private void ConstructQuestionRows(DataTable table,
            DataSet rawData, List<KeyValuePair<string, int>> assessors, int subjectID)
        {
            // Get distinct questions for the given subject ID.
            List<KeyValuePair<string, string>> questions = GetQuestions(rawData, subjectID);

            // Do not proceed if questions are not present.
            if (questions == null || questions.Count == 0)
                return;

            foreach (KeyValuePair<string, string> question in questions)
            {
                // Create a new row.
                DataRow row = table.NewRow();

                int _index = question.Value.Trim().LastIndexOf("$");
                string questionValue = question.Value.Substring(0, _index);
                // Set the title.
                row["TITLE"] = question.Key + "$" + questionValue;

                // Set the 'FLAG' value.
                row["FLAG"] = "Q" + question.Value.Substring(_index + 1);

                // Loop through the assessor and add the assessor rating
                // against the subject.
                foreach (KeyValuePair<string, int> assessor in assessors)
                {
                    // Get question rating.
                    row[assessor.Value.ToString()] = GetQuestionRating(rawData, subjectID, questionValue, assessor.Value);
                }

                // Add the row to the table.
                table.Rows.Add(row);
            }
        }

        /// <summary>
        /// Method that retrieves the question rating for the given question ID 
        /// and assessor ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="questionID">
        /// A <see cref="int"/> that holds the question ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the subject rating.
        /// </returns>
        public string GetQuestionRating(DataSet rawData, int subjectID, string questionID, int assessorID)
        {
            // Construct filter expression.
            string expression = "SKILL_ID=" + subjectID + " and " + "QUESTION_KEY='" + questionID.Trim() + "' and " + "ASSESSOR_ID=" + assessorID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[3].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return "Not Rated";

            // Check if value if null or empty.
            if (IsNullOrEmpty(filteredRows[0]["RATING"]))
                return "Not Rated";

            return Convert.ToString(filteredRows[0]["RATING"].ToString());
        }

        /// <summary>
        /// Method that retrieves the list of unique questions for the given
        /// subject ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </returns>
        public List<KeyValuePair<string, string>> GetQuestions(DataSet rawData, int subjectID)
        {
            // Construct filter expression.
            string expression = "SKILL_ID=" + subjectID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[4].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return null;

            // Add the filtered rows to a temporary table.
            DataTable filteredTable = rawData.Tables[4].Clone();
            foreach (DataRow filteredRow in filteredRows)
                filteredTable.ImportRow(filteredRow);

            // Get distinct records from question details table.
            DataTable distinctTable = filteredTable.DefaultView.ToTable
                (true, "QUESTION_KEY", "QUESTION_DESC", "VISIBLE");

            if (distinctTable == null || distinctTable.Rows.Count == 0)
                return null;

            List<KeyValuePair<string, string>> questions = new List<KeyValuePair<string, string>>();

            foreach (DataRow row in distinctTable.Rows)
            {
                questions.Add(new KeyValuePair<string, string>(
                    row["QUESTION_DESC"].ToString().Trim(),
                    Convert.ToString(row["QUESTION_KEY"]).Trim() + "$" + row["VISIBLE"].ToString()));
            }
            return questions;
        }

        /// <summary>
        /// Method that construct the subject rows.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="assessors">
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </param>
        /// <param name="subjects">
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </param>
        private void ConstructOverAllRatingRows(DataTable table,
            DataSet rawData,
            List<KeyValuePair<string, int>> assessors,
            List<KeyValuePair<int, decimal>> subjects)
        {
            // Create a new row.
            DataRow row = table.NewRow();

            int tableColumnsCount = table.Columns.Count - 2;

            if (tableColumnsCount != subjects.Count) return;
            int coulmnCount = 1;
            // Loop through the subject and add the subject wise rating.
            foreach (KeyValuePair<int, decimal> subject in subjects)
            {
                if (coulmnCount == 1)
                {    // Set the title.
                    row["TITLE"] = subject.Key;
                    row["FLAG"] = "O";
                }
                row[coulmnCount] = subject.Value;

                coulmnCount++;

                //row[1] = subject.Key;  
            }

            // Add the row to the table.
            table.Rows.Add(row);
        }

        /// <summary>
        /// Method that retrieves the list of unique questions for the given
        /// subject ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </returns>
        private List<KeyValuePair<int, decimal>> GetOverAllRating(DataSet rawData, int subjectID)
        {
            // Construct filter expression.
            string expression = "SKILL_ID=" + subjectID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[QUESTION_DETAILS_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return null;

            // Add the filtered rows to a temporary table.
            DataTable filteredTable = rawData.Tables[QUESTION_DETAILS_INDEX].Clone();
            foreach (DataRow filteredRow in filteredRows)
                filteredTable.ImportRow(filteredRow);

            // Get distinct records from question details table.
            DataTable distinctTable = filteredTable.DefaultView.ToTable
                (true, "ASSESSOR_ID", "SCORE");

            if (distinctTable == null || distinctTable.Rows.Count == 0)
                return null;

            List<KeyValuePair<int, decimal>> questions = new List<KeyValuePair<int, decimal>>();

            foreach (DataRow row in distinctTable.Rows)
            {
                questions.Add(new KeyValuePair<int, decimal>(
                    Convert.ToInt32(row["ASSESSOR_ID"].ToString().Trim()),
                    Convert.ToDecimal(row["SCORE"].ToString().Trim())));
            }
            return questions;
        }

        /// <summary>
        /// Method that retrieves the list of accessors.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </returns>
        public List<KeyValuePair<string, int>> GetAssessors(DataSet rawData)
        {
            List<KeyValuePair<string, int>> assessors = new List<KeyValuePair<string, int>>();

            foreach (DataRow row in rawData.Tables[ASSESSOR_DETAILS_INDEX].Rows)
            {
                assessors.Add(new KeyValuePair<string, int>(
                    row["ASSESSOR_NAME"].ToString().Trim(),
                    Convert.ToInt32(row["ASSESSOR_ID"].ToString().Trim())));
            }
            return assessors;
        }

        /// <summary>
        /// Method that construct the columns to the data table.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="assessors">
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </param>
        private void ConstructColumns(DataTable table,
            List<KeyValuePair<string, int>> assessors)
        {
            // Add 'title' column.
            table.Columns.Add("TITLE");

            // Add accessor names as columns.
            foreach (KeyValuePair<string, int> assessor in assessors)
            {
                table.Columns.Add(assessor.Value.ToString());
                table.Columns[assessor.Value.ToString()].Caption = assessor.Key;
            }

            // Add 'FLAG' column.
            table.Columns.Add("FLAG");

        }

        /// <summary>
        /// Method that retrieves the list of unique questions for the given
        /// subject ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </returns>
        public DataTable GetOverAllRating(DataSet rawData, int subjectID, string T)
        {
            // Construct filter expression.
            string expression = "SKILL_ID=" + subjectID + " AND RATING>=0";

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[QUESTION_DETAILS_INDEX].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return null;


            // Add the filtered rows to a temporary table.
            DataTable filteredTable = rawData.Tables[QUESTION_DETAILS_INDEX].Clone();
            foreach (DataRow filteredRow in filteredRows)
                filteredTable.ImportRow(filteredRow);

            return filteredTable;

            // Get distinct records from question details table.
            /*DataTable distinctTable = filteredTable.DefaultView.ToTable
                (true, "ASSESSOR_ID", "SCORE");

            if (distinctTable == null || distinctTable.Rows.Count == 0)
                return null;

            List<KeyValuePair<int, decimal>> questions = new List<KeyValuePair<int, decimal>>();

            foreach (DataRow row in distinctTable.Rows)
            {
                questions.Add(new KeyValuePair<int, decimal>(
                    Convert.ToInt32(row["ASSESSOR_ID"].ToString().Trim()),
                    Convert.ToDecimal(row["SCORE"].ToString().Trim())));
            }
            return questions;*/
        }

        /// <summary>
        /// Method that construct the subject rows.
        /// </summary>
        /// <param name="table">
        /// A <see cref="DataTable"/> that holds the table to construct.
        /// </param>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="assessors">
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </param>
        /// <param name="subjects">
        /// A list of <see cref="KeyValuePair"/> that holds the subjects list.
        /// </param>
        private void ConstructOverAllRatingRows(DataTable table,
            DataSet rawData,
            List<KeyValuePair<string, int>> assessors,
            DataTable assessorOverAllRating)
        {
            // Create a new row.
            DataRow row = table.NewRow();

            int tableColumnsCount = table.Columns.Count - 2;

            if (tableColumnsCount != assessorOverAllRating.Rows.Count) return;

            int coulmnCount = 1;

            foreach (DataRow dr in assessorOverAllRating.Rows)
            {
                if (coulmnCount == 1)
                {    // Set the title.
                    row["TITLE"] = Convert.ToString(dr["ASSESSOR_ID"]);
                    row["FLAG"] = "O";
                }

                row[coulmnCount] = Convert.ToString(dr["SCORE"]);

                coulmnCount++;
            }

            // Add the row to the table.
            table.Rows.Add(row);
        }

        /// <summary>
        /// Method that checks if the given value is null or empty.
        /// </summary>
        /// <param name="value">
        /// A <see cref="object"/> that holds the value.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. True if null or empty 
        /// and false otherwise.
        /// </returns>
        private bool IsNullOrEmpty(object value)
        {
            if (value == null || value == DBNull.Value || value.ToString().Trim().Length == 0)
                return true;

            return false;
        }


        public DataTable GetRatingSummaryPDFTable(DataSet rawData, int rptType,
           out decimal totalScore, out decimal totalWeightedScore)
        {
            totalScore = 0;
            totalWeightedScore = 0;
            totalWeightedScore = Convert.ToDecimal(rawData.Tables[5].Rows[0][0]);
            // Get assessor details.
            List<KeyValuePair<string, int>> assessors = GetAssessors(rawData);

            //Get Subject details
            List<KeyValuePair<string, int>> subjects = GetSubjects(rawData);

            //GetSubject Score
            List<KeyValuePair<string, int>> subjectsScore = GetSubjectsScore(rawData);
            

            // Instantiate a data table object.
            DataTable table = new DataTable();

            switch (rptType)
            {
                case 1:
                    ContructAssessorScoreColumns(table);
                    ContructAssessorScoreRows(rawData, table, assessors);
                    break;
                case 2:
                    //Construct Columns
                    ConstructColumns(table, assessors);
                    DataColumn Col = table.Columns.Add("Score", System.Type.GetType("System.String"));
                    Col.SetOrdinal(1);

                    ConstructOverAllScorePDFRows(table, rawData, assessors, subjects, subjectsScore);
                    break;
                case 3:
                    //Construct Columns
                    ConstructColumns(table, assessors);
                    //ConstructOverAllScorePDFRows(table, rawData, assessors, subjects);
                    break;
                default:
                    break;
            }

            return table;
        }

        private void ContructAssessorScoreColumns(DataTable table)
        {
            // Add 'Assessor' column.
            table.Columns.Add("ASSESSOR");
            table.Columns.Add("SCORE");
            table.Columns.Add("COMMENTS");
        }

        private void ContructAssessorScoreRows(DataSet rawData, DataTable table,
            List<KeyValuePair<string, int>> assessors)
        {
            // Add accessor names as columns.
            string assessorScoreComments = string.Empty;

            foreach (KeyValuePair<string, int> assessor in assessors)
            {
                // Create a new row.
                DataRow row = table.NewRow();
                row["ASSESSOR"] = assessor.Key;
                assessorScoreComments = ContructAssessorScoreRowsVal(rawData, assessor.Value);
                int dollarIndex = assessorScoreComments.LastIndexOf('$');
                row["SCORE"] = string.Format("{0:N2}%", Convert.ToDecimal(assessorScoreComments.Substring(0, dollarIndex)));
                row["COMMENTS"] = assessorScoreComments.Substring(dollarIndex + 1);
                table.Rows.Add(row);
            }
        }

        private string ContructAssessorScoreRowsVal(DataSet rawData, int assessorID)
        {
            // Construct filter expression.
            string expression = "ASSESSOR_ID=" + assessorID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[0].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return "Not Rated";

            // Check if value if null or empty.
            if (IsNullOrEmpty(filteredRows[0]["OVERALL_RATING"]))
                return "Not Rated";

            // Check if value if Zero.
            if (Convert.ToDecimal(filteredRows[0]["OVERALL_RATING"]) < 0)
                return "Not Rated";

            return Convert.ToString(filteredRows[0]["OVERALL_RATING"]) + "$ " + Convert.ToString(filteredRows[0]["COMMENTS"]);
        }

        /// <summary>
        /// Method that retrieves the list of accessors.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </returns>
        private List<KeyValuePair<string, int>> GetSubjects(DataSet rawData)
        {
            List<KeyValuePair<string, int>> subjects = new List<KeyValuePair<string, int>>();

            foreach (DataRow row in rawData.Tables[1].Rows)
            {
                subjects.Add(new KeyValuePair<string, int>(
                    row["SKILL_NAME"].ToString().Trim(),
                    Convert.ToInt32(row["SKILL_ID"].ToString().Trim())));
            }
            return subjects;
        }

        /// <summary>
        /// Method that retrieves the list of accessors.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <returns>
        /// A list of <see cref="KeyValuePair"/> that holds the assessors list.
        /// </returns>
        private List<KeyValuePair<string, int>> GetSubjectsScore(DataSet rawData)
        {
            List<KeyValuePair<string, int>> subjectsScore = new List<KeyValuePair<string, int>>();

            foreach (DataRow row in rawData.Tables[1].Rows)
            {
                subjectsScore.Add(new KeyValuePair<string, int>(
                    row["SUBJECT_WEIGHTAGE_RATING"].ToString().Trim(),
                    Convert.ToInt32(row["SKILL_ID"].ToString().Trim())));
            }
            return subjectsScore;
        }

        private void ConstructOverAllScorePDFRows(DataTable table,
            DataSet rawData,
            List<KeyValuePair<string, int>> assessors,
            List<KeyValuePair<string, int>> subjects,
            List<KeyValuePair<string, int>> subjectsScore)
        {
            // Loop through the assessor and add the assessor rating
            // against the subject.
            foreach (KeyValuePair<string, int> subject in subjects)
            {
                // Create a new row.
                DataRow row = table.NewRow();
                row["TITLE"] = subject.Key;
                row["FLAG"] = "S";

                // Add accessor names as columns.
                foreach (KeyValuePair<string, int> score in subjectsScore)
                {
                    // Get question rating.
                    if(subject.Value == score.Value)
                        row["Score"] = string.Format("{0:N2}%", Convert.ToDecimal(score.Key));
                } 

                // Add accessor names as columns.
                foreach (KeyValuePair<string, int> assessor in assessors)
                {     
                    // Get question rating.
                    row[assessor.Value.ToString()] = GetOverallRatingPDF(rawData, assessor.Value, subject.Value);
                }

                // Add the row to the table.
                table.Rows.Add(row);
            }
        }

        /// <summary>
        /// Method that retrieves the question rating for the given question ID 
        /// and assessor ID.
        /// </summary>
        /// <param name="rawData">
        /// A <see cref="DataSet"/> that holds the raw data.
        /// </param>
        /// <param name="subjectID">
        /// A <see cref="int"/> that holds the subject ID.
        /// </param>
        /// <param name="questionID">
        /// A <see cref="int"/> that holds the question ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="double"/> that holds the subject rating.
        /// </returns>
        private string GetOverallRatingPDF(DataSet rawData, int assessorID, int skillID)
        {
            // Construct filter expression.
            string expression = "SKILL_ID=" + skillID + " and " + "ASSESSOR_ID=" + assessorID;

            // Get filtered rows.
            DataRow[] filteredRows = rawData.Tables[2].Select(expression);

            if (filteredRows == null || filteredRows.Length == 0)
                return "Not Rated";

            // Check if value if null or empty.
            if (IsNullOrEmpty(filteredRows[0]["SCORE"]))
                return "Not Rated";

            // Check if value if Zero.
            if (Convert.ToInt32(filteredRows[0]["SCORE"]) < 0)
                return "Not Rated";

            return Convert.ToString(filteredRows[0]["SCORE"].ToString());
        }
    }
}
