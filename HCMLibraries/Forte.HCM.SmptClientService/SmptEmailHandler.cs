﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SmptEmailHandler.cs
// File that represents the email hancled compopent, that provides 
// functionalities to send mails on various scenarios like test scheduled,
// interview scheduled etc. Mail format are taken from either html or
// text templates.

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Web;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using System.Security.Cryptography;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Exceptions;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.SmptClientService
{
    /// <summary>
    /// Class that represents the email handler component, that provides 
    /// functionalities to send mails on various scenarios like test scheduled,
    /// interview scheduled etc. Mail format are taken from either html or
    /// text templates.
    /// </summary>
    public class SmptEmailHandler
    {
        #region Public Methods                                                 

        /// <summary>
        /// Method that sends the corresponding mail format according to entity type.
        /// </summary>
        /// A <param name="entityType"> specifies the type of mail whether it is TestCompleted/TestScheduled etc </param>
        /// A <param name="entityID"> that holds the entity id.</param>
        /// A <param name="subEntityID"> that holds the sub entity id.</param>
        public void SendMail(EntityType entityType, string entityID, string subEntityID)
        {
            try
            {
                MailDetail mailDetail = ComposeMail(entityType, entityID, subEntityID);

                new MailHelper().SendMessage(mailDetail);
            }
            catch (EMailException emailException)
            {
                throw emailException;
            }
            catch (Exception exp)
            {
                Forte.HCM.Trace.Logger.ExceptionLog(exp);
                throw new EMailException("Email could not be sent");
            }
        }

        #endregion Public Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that generates the MailDetail instance with all details like to address, from address etc
        /// </summary>
        /// A <param name="entityType"> that holds the entity type.</param>
        /// A <param name="entityID"> that holds the entity id.</param>
        /// A <param name="subEntityID"> that holds the sub entity id.</param>
        /// <returns></returns>
        private MailDetail ComposeMail(EntityType entityType, string entityID, string subEntityID)
        {
            string templatesUrl = HttpContext.Current.Server.MapPath("~/Templates");
            string forteHCMLogoUrl = HttpContext.Current.Server.MapPath(Convert.ToString(ConfigurationManager.AppSettings["FORTE_HCM_LOGO"]));
            templatesUrl += "\\";
            MailHelper mHelper = new MailHelper();
            MailDetail mailDetail = new MailDetail();
            string candidateUrl = ConfigurationManager.AppSettings["CANDIDATE_URL"] + "home.aspx";
            
            mailDetail.MailValues = new System.Collections.Hashtable();
            UserRegistrationInfo candidateInfo;
            try
            {
                if (Utility.IsNullOrEmpty(entityID))
                    throw new Exception("Entity ID cannot be empty");

                if (Utility.IsNullOrEmpty(subEntityID))
                    throw new Exception("Sub entity ID cannot be empty");

                int attemptID = 0;
                if (!int.TryParse(subEntityID, out attemptID))
                    throw new Exception("Sub entity ID is invalid. Expected attempt ID as integer");
                switch (entityType)
                {
                    case EntityType.CandidateScheduled:
                        #region CandidateScheduled

                        string testInstructionUrl = ConfigurationManager.AppSettings["TEST_INSTRUCTION_URL"];
                        CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerDLManager().
                            GetCandidateTestSessionEmailDetail(entityID, attemptID);

                        if (candidateTestSessionDetail == null)
                            break;

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();

                        // Add candidate email.
                        if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateEmail))
                            mailDetail.To.Add(candidateTestSessionDetail.CandidateEmail);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Add candidate scheduler.
                        if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.SchedulerEmail))
                            mailDetail.CC.Add(candidateTestSessionDetail.SchedulerEmail);

                        // Add candidate owner.
                        if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.CandidateOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(candidateTestSessionDetail.CandidateOwnerEmail) == -1)
                                mailDetail.CC.Add(candidateTestSessionDetail.CandidateOwnerEmail);
                        }

                        // Add position profile owner.
                        if (!Utility.IsNullOrEmpty(candidateTestSessionDetail.PositionProfileOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(candidateTestSessionDetail.PositionProfileOwnerEmail) == -1)
                                mailDetail.CC.Add(candidateTestSessionDetail.PositionProfileOwnerEmail);
                        }

                        if (entityType == EntityType.CandidateScheduled)
                        {
                            mailDetail.Subject = string.Format("A test named ‘{0}’ has been scheduled for you",
                                                                   candidateTestSessionDetail.TestName);
                        }
                        mailDetail.MailValues.Add(MailTemplateKeywords.TestName, candidateTestSessionDetail.TestName);
                        mailDetail.MailValues.Add(MailTemplateKeywords.SchedulerFirstName, candidateTestSessionDetail.SchedulerFirstName);
                        mailDetail.MailValues.Add(MailTemplateKeywords.SchedulerLastName, candidateTestSessionDetail.SchedulerLastName);
                        mailDetail.MailValues.Add(MailTemplateKeywords.SchedulerCompany, candidateTestSessionDetail.SchedulerCompany);
                        mailDetail.MailValues.Add(MailTemplateKeywords.ExpiryDate, candidateTestSessionDetail.ExpiryDate.ToShortDateString());
                        mailDetail.MailValues.Add(MailTemplateKeywords.TestInstructionURL, testInstructionUrl+"?n="+candidateTestSessionDetail.NavigateKey);
                        // Get user name and password.
                        candidateInfo = new UserRegistrationDLManager().GetUserRegistrationInfo
                            (Convert.ToInt32(candidateTestSessionDetail.CandidateID));
                        if (candidateInfo != null)
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidateUserName, candidateInfo.UserEmail);
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidatePassword, Decrypt(candidateInfo.Password));
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidateFirstName, candidateInfo.FirstName);
                        }

                        mailDetail.isBodyHTML = Convert.ToBoolean(ConfigurationManager.AppSettings["IS_BODY_HTML"]);

                        mailDetail.ForteHCMLogo = new FileInfo(forteHCMLogoUrl);
                        if (mailDetail.isBodyHTML)
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.ForteHCMLogo, "<img src='cid:ForteHCMLogo'>");

                        }
                        else
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.ForteHCMLogo, "");
                        }
                        if (mailDetail.isBodyHTML)
                        {
                            mailDetail.Message = mHelper.GetMailContentFromTemplate(templatesUrl + ConfigurationManager.AppSettings[MailTemplateType.TestScheduledHtml], mailDetail.MailValues);
                        }
                        else
                        {
                            mailDetail.Message = mHelper.GetMailContentFromTemplate(templatesUrl + ConfigurationManager.AppSettings[MailTemplateType.TestScheduledTxt], mailDetail.MailValues);
                        }

                        #endregion CandidateScheduled
                        break;

                    default:
                        break;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return mailDetail;
        }

        /// <summary>
        /// Method that decrypts the encrypted text 
        /// </summary>
        /// A <param name="encryptedText"> that holds the encrypted text.</param>
        /// <returns>decrypted text</returns>
        private string Decrypt(string encryptedText)
        {
            if ((encryptedText == "") || (encryptedText == null))
                throw new Exception("Please Provide Some Encrypted Text For Decryption");

            const string SALTVALUE = "FORT";
            const string STRINGIVVALUE = "FORTE-HCMENCRYPTI";
            const string PASSWORD = "SRALT";
            const string HASHALGORITHMNAME = "SHA1";

            byte[] bytIV = null;
            byte[] bytSalt = null;
            byte[] bytText = null;
            byte[] bytKey = null;
            byte[] bytOriginal = null;
            byte[] bytReturn = null;
            int OriginalDataLenght;
            MemoryStream objMememoryStream = null;
            CryptoStream objCryptptoStream = null;
            try
            {
                bytIV = Encoding.ASCII.GetBytes(STRINGIVVALUE);
                bytSalt = Encoding.ASCII.GetBytes(SALTVALUE);
                bytText = Convert.FromBase64String(encryptedText);
                PasswordDeriveBytes objPass = new PasswordDeriveBytes(PASSWORD, bytSalt, HASHALGORITHMNAME, 5);
                RijndaelManaged objRinjandel = new RijndaelManaged();
                bytKey = objPass.GetBytes(objRinjandel.KeySize / 8);
                objMememoryStream = new MemoryStream(bytText);
                objCryptptoStream = new CryptoStream(objMememoryStream, objRinjandel.CreateDecryptor(bytKey, bytIV), CryptoStreamMode.Read);
                bytOriginal = new byte[bytText.Length];
                OriginalDataLenght = objCryptptoStream.Read(bytOriginal, 0, bytOriginal.Length);
                objCryptptoStream.Close();
                objMememoryStream.Close();
                bytReturn = new byte[OriginalDataLenght];
                for (int i = 0; i < bytOriginal.Length; i++)
                {
                    if (Convert.ToString(bytOriginal[i]) == "0")
                    {
                        break;
                    }
                    bytReturn[i] = bytOriginal[i];
                }
                return Encoding.ASCII.GetString(bytReturn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objMememoryStream != null)
                    objMememoryStream = null;
                if (objCryptptoStream != null)
                    objCryptptoStream = null;
                bytIV = null;
                bytKey = null;
                bytOriginal = null;
                bytReturn = null;
                bytSalt = null;
                bytText = null;
            }
        }

        /// <summary>
        /// Method that generates the MailDetail instance.
        /// </summary>
        /// A <param name="entityType"> that holds the entity type.</param>
        /// A <param name="data"> that holds the data.</param>
        /// <returns></returns>
        private MailDetail GetMailDetailForOfflineInterview(EntityType entityType, object data)
        {
            MailDetail mailDetail = new MailDetail();
            MailHelper mHelper = new MailHelper();
            mailDetail.MailValues = new System.Collections.Hashtable();
            string templatesUrl = HttpContext.Current.Server.MapPath("~/Templates");
            string forteHCMLogoUrl = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FORTE_HCM_LOGO"]);
            templatesUrl += "\\";
            switch (entityType)
            {
                case EntityType.Completed:
                    {
                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        CandidateInterviewSessionDetail sessionDetail = data
                            as CandidateInterviewSessionDetail;

                        if (sessionDetail == null)
                            throw new Exception("Data cannot be null or empty");

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();
                        // Add candidate scheduler as 'to' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.SchedulerEmail))
                            mailDetail.To.Add(sessionDetail.SchedulerEmail);

                        // Instantiate 'cc' address.
                        mailDetail.CC = new List<string>();

                        // Add candidate owner as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.CandidateOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(sessionDetail.CandidateOwnerEmail) == -1)
                                mailDetail.CC.Add(sessionDetail.CandidateOwnerEmail);
                        }

                        // Add position profile owner as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.PositionProfileOwnerEmail))
                        {
                            // Check if mail ID already exist.
                            if (mailDetail.CC.IndexOf(sessionDetail.PositionProfileOwnerEmail) == -1)
                                mailDetail.CC.Add(sessionDetail.PositionProfileOwnerEmail);
                        }

                        // Add assessors as 'cc' address.
                        if (!Utility.IsNullOrEmpty(sessionDetail.AssessorEmails))
                        {
                            // Get comma separated emails.
                            string[] assessorEmails = sessionDetail.AssessorEmails.Split(new char[] { ',' });

                            foreach (string assessorEmail in assessorEmails)
                            {
                                // Check if mail ID already exist.
                                if (mailDetail.CC.IndexOf(assessorEmail) == -1)
                                    mailDetail.CC.Add(assessorEmail);
                            }
                        }
                        UserRegistrationInfo candidateInfo;
                        candidateInfo = new UserRegistrationDLManager().GetUserRegistrationInfo
                            (Convert.ToInt32(sessionDetail.CandidateID));
                        if (candidateInfo != null)
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidateUserName, candidateInfo.UserEmail);
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidateFirstName, candidateInfo.FirstName);
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidateLastName, candidateInfo.LastName);
                        }
                        // Add subject.
                        mailDetail.Subject = string.Format("{0} Interview ‘{1}’ has been completed by ‘{2} {3}’",
                            Constants.EmailSubjectHeading.EMAIL_SUBJECT_COMPANY_NAME, sessionDetail.InterviewTestName,
                            candidateInfo.FirstName, candidateInfo.LastName);


                        // Construct interview results url.
                        string url = string.Format(
                            ConfigurationManager.AppSettings["INTERVIEW_RESULT_URL"],
                            sessionDetail.CandidateTestSessionID,
                            sessionDetail.InterviewTestKey,
                            sessionDetail.InterviewTestSessionID,
                            sessionDetail.AttemptID);

                        // Construct interview publish url
                        string urlWithScore = string.Format(
                            ConfigurationManager.AppSettings["INTERVIEW_RESULT_PUBLISH_URL"],
                            string.Concat(sessionDetail.InterviewPublishCode, "S"));

                        string urlWithOutScore = string.Format(
                            ConfigurationManager.AppSettings["INTERVIEW_RESULT_PUBLISH_URL"],
                            string.Concat(sessionDetail.InterviewPublishCode, "H"));

                        mailDetail.MailValues.Add(MailTemplateKeywords.InterviewName, sessionDetail.InterviewTestName);
                        mailDetail.MailValues.Add(MailTemplateKeywords.SchedulerCompany, sessionDetail.SchedulerCompany);
                        mailDetail.MailValues.Add(MailTemplateKeywords.ExpiryDate, sessionDetail.ExpiryDate.ToShortDateString());
                        mailDetail.MailValues.Add(MailTemplateKeywords.ResultWithScoreURL, urlWithScore);
                        mailDetail.MailValues.Add(MailTemplateKeywords.ResultWithoutScoreURL, urlWithOutScore);
                        mailDetail.MailValues.Add(MailTemplateKeywords.CandidateInterviewURL, url);
                        mailDetail.MailValues.Add(MailTemplateKeywords.CompletedDate, sessionDetail.DateCompleted.ToShortDateString());


                        mailDetail.isBodyHTML = Convert.ToBoolean(ConfigurationManager.AppSettings["IS_BODY_HTML"]);

                        mailDetail.ForteHCMLogo = new FileInfo(forteHCMLogoUrl);
                        if (mailDetail.isBodyHTML)
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.ForteHCMLogo, "<img src='cid:ForteHCMLogo'>");

                        }
                        else
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.ForteHCMLogo, "");
                        }
                        if (mailDetail.isBodyHTML)
                        {
                            mailDetail.Message = mHelper.GetMailContentFromTemplate(templatesUrl + ConfigurationManager.AppSettings[MailTemplateType.InterviewCompletedHtml], mailDetail.MailValues);
                        }
                        else
                        {
                            mailDetail.Message = mHelper.GetMailContentFromTemplate(templatesUrl + ConfigurationManager.AppSettings[MailTemplateType.InterviewCompletedTxt], mailDetail.MailValues);
                        }
                    }

                    break;

                //case EntityType.AbandonInterview:
                //    {
                //        if (data == null)
                //            throw new Exception("Data cannot be null or empty");

                //        // Type cast data to specific type.
                //        CandidateInterviewSessionDetail sessionDetail = data
                //            as CandidateInterviewSessionDetail;

                //        if (sessionDetail == null)
                //            throw new Exception("Data cannot be null or empty");



                //        // Add candidate scheduler as 'to' address.
                //        if (!Utility.IsNullOrEmpty(sessionDetail.SchedulerEmail))
                //            mailDetail.To.Add(sessionDetail.SchedulerEmail);

                //        // Add candidate owner as 'cc' address.
                //        if (!Utility.IsNullOrEmpty(sessionDetail.CandidateOwnerEmail))
                //        {
                //            // Check if mail ID already exist.
                //            if (mailDetail.CC.IndexOf(sessionDetail.CandidateOwnerEmail) == -1)
                //                mailDetail.CC.Add(sessionDetail.CandidateOwnerEmail);
                //        }

                //        // Add position profile owner as 'cc' address.
                //        if (!Utility.IsNullOrEmpty(sessionDetail.PositionProfileOwnerEmail))
                //        {
                //            // Check if mail ID already exist.
                //            if (mailDetail.CC.IndexOf(sessionDetail.PositionProfileOwnerEmail) == -1)
                //                mailDetail.CC.Add(sessionDetail.PositionProfileOwnerEmail);
                //        }

                //        // Add assessors as 'cc' address.
                //        if (!Utility.IsNullOrEmpty(sessionDetail.AssessorEmails))
                //        {
                //            // Get comma separated emails.
                //            string[] assessorEmails = sessionDetail.AssessorEmails.Split(new char[] { ',' });

                //            foreach (string assessorEmail in assessorEmails)
                //            {
                //                // Check if mail ID already exist.
                //                if (mailDetail.CC.IndexOf(assessorEmail) == -1)
                //                    mailDetail.CC.Add(assessorEmail);
                //            }
                //        }

                //        // Add subject.
                //        mailDetail.Subject = string.Format("Interview '{0}' abandoned by '{1}'",
                //            sessionDetail.InterviewTestName,
                //            sessionDetail.CandidateName);

                //        // Add message.
                //        mailDetail.Body = "Dear Viewer,";
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += Environment.NewLine;

                //        mailDetail.Body += string.Format("Sub: Interview '{0}' abandoned by '{1}'",
                //            sessionDetail.InterviewTestName,
                //            sessionDetail.CandidateName);
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += Environment.NewLine;

                //        mailDetail.Body += OfflineInterviewGetClosureMessage();
                //        mailDetail.Body += OfflineInterviewGetDisclaimerMessage();
                //        mailDetail.IsBodyHtml = true;
                //        mailDetail.Body = mailDetail.Body.Replace("\r\n", "<br />");
                //    }

                //    break;

                //case EntityType.Paused:
                //    {
                //        if (data == null)
                //            throw new Exception("Data cannot be null or empty");

                //        // Type cast data to specific type.
                //        CandidateInterviewSessionDetail sessionDetail = data
                //            as CandidateInterviewSessionDetail;

                //        if (sessionDetail == null)
                //            throw new Exception("Data cannot be null or empty");

                //        mailDetail = new MailMessage();

                //        // Add candidate as 'to' address.
                //        if (!Utility.IsNullOrEmpty(sessionDetail.CandidateEmail))
                //            mailDetail.To.Add(sessionDetail.CandidateEmail));

                //        // Add candidate scheduler as 'cc' address.
                //        if (!Utility.IsNullOrEmpty(sessionDetail.SchedulerEmail))
                //            mailDetail.CC.Add(sessionDetail.SchedulerEmail));

                //        // Add subject.
                //        mailDetail.Subject = string.Format("Interview '{0}' paused",
                //            sessionDetail.InterviewTestName);

                //        // Add message.
                //        mailDetail.Body = string.Format("Dear {0},", sessionDetail.CandidateName);
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += Environment.NewLine;

                //        mailDetail.Body += string.Format("Sub: Interview '{0}' paused",
                //            sessionDetail.InterviewTestName);
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += "You have paused the interview";
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += "You can continue the offline interview recording later";
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += string.Format("Security code for the interview session is {0}", sessionDetail.SecurityCode);
                //        mailDetail.Body += Environment.NewLine;
                //        mailDetail.Body += Environment.NewLine;

                //        mailDetail.Body += OfflineInterviewGetClosureMessage();
                //        mailDetail.Body += OfflineInterviewGetDisclaimerMessage();
                //        mailDetail.IsBodyHtml = true;
                //        mailDetail.Body = mailDetail.Body.Replace("\r\n", "<br />");
                //    }

                //    break;

                default:
                    break;
            }

            return mailDetail;
        }


        /// <summary>
        /// Method that generates the MailDetail instance.
        /// </summary>
        /// A <param name="entityType"> that holds the entity type.</param>
        /// A <param name="data"> that holds the data.</param>
        /// <returns></returns>
        private MailDetail GetMailDetailForCandidateSlotRequest(EntityType entityType, object data)
        {
            MailDetail mailDetail = new MailDetail();
            MailHelper mHelper = new MailHelper();
            mailDetail.MailValues = new System.Collections.Hashtable();
            //string templatesUrl = HttpContext.Current.Server.MapPath("~/Templates");
            string templatesUrl = Path.Combine(HttpRuntime.AppDomainAppPath, "Templates");
            //string forteHCMLogoUrl = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FORTE_HCM_LOGO"]);
            string forteHCMLogoUrl = Path.Combine(HttpRuntime.AppDomainAppPath, ConfigurationManager.AppSettings["FORTE_HCM_LOGO"]);
            forteHCMLogoUrl = forteHCMLogoUrl.Replace("~/", "");
            templatesUrl += "\\";
            switch (entityType)
            {
                case EntityType.CandidateSlotApproval:
                    {
                        if (data == null)
                            throw new Exception("Data cannot be null or empty");

                        // Type cast data to specific type.
                        CandidateInterviewSessionDetail candidateInterviewDetail = data
                            as CandidateInterviewSessionDetail;


                        if (data != null)
                        {
                            string schedulerName = (Utility.IsNullOrEmpty(candidateInterviewDetail.SchedulerLastName) ? candidateInterviewDetail.SchedulerFirstName : 
                                string.Format("{0} {1}",candidateInterviewDetail.SchedulerFirstName,candidateInterviewDetail.SchedulerLastName));

                            mailDetail.MailValues.Add(MailTemplateKeywords.SchedulerFullName, schedulerName);
                            mailDetail.MailValues.Add(MailTemplateKeywords.SchedulerCompany, candidateInterviewDetail.SchedulerCompany);
                        }

                        AssessorTimeSlotDetail assessorSlotDetail = new AssessorTimeSlotDetail();
                        assessorSlotDetail = new AssessorDLManager().GetCandidateSlotDetail(candidateInterviewDetail.SlotKey);
                        
                        StringBuilder sbBody = new StringBuilder();

                        if (assessorSlotDetail != null)
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.OnlineInterviewName, 
                                assessorSlotDetail.onlineInterviewDetail.InterviewName);
                            mailDetail.MailValues.Add(MailTemplateKeywords.OnlineInterviewInstruction, 
                                assessorSlotDetail.onlineInterviewDetail.InterviewInstruction);
                            if (!Utility.IsNullOrEmpty(assessorSlotDetail.onlineInterviewDetail.RequestedBy))
                            {
                                mailDetail.MailValues.Add(MailTemplateKeywords.OnlineInterviewRequestedBy,
                                string.Format("(Requested By {0})", assessorSlotDetail.onlineInterviewDetail.RequestedBy));    
                            }
                            
                            List<AssessorTimeSlotDetail> requestedSltos = assessorSlotDetail.assessorTimeSlotDetails;

                            // Send email.
                            int i = 1;
                            foreach (AssessorTimeSlotDetail assessorDetail in requestedSltos)
                            {
                                sbBody.Append("<tr>");
                                    sbBody.Append("<td style='width:15%;'>");
                                        sbBody.Append("<span style='font-family: Tahoma;font-size: 11px;color: #33424b;text-align: left;'>");
                                            sbBody.Append("Choice "+i);
                                        sbBody.Append("</span>");
                                    sbBody.Append("</td>");

                                    sbBody.Append("<td style='width:20%;'>");
                                        sbBody.Append("<span style='font-family: Tahoma;font-size: 11px;color: #33424b;text-align: left;'>");
                                        sbBody.Append(assessorDetail.AvailabilityDate.ToString("MM/dd/yyyy"));
                                        sbBody.Append("</span>");
                                    sbBody.Append("</td>");

                                    sbBody.Append("<td style='width:30%;'>");
                                        sbBody.Append("<span style='font-family: Tahoma;font-size: 11px;color: #33424b;text-align: left;'>");
                                        sbBody.Append(string.Format("{0} {1}", assessorDetail.TimeSlotTextFrom, assessorDetail.TimeSlotTextTo));
                                        sbBody.Append("</span>");
                                    sbBody.Append("</td>");

                                sbBody.Append("</tr>");
                                i++;
                            }
                        }

                        if (candidateInterviewDetail == null)
                            throw new Exception("Data cannot be null or empty");

                        // Instantiate 'to' address.
                        mailDetail.To = new List<string>();
                        // Add candidate as 'to' address.
                        if (!Utility.IsNullOrEmpty(candidateInterviewDetail.CandidateEmail))
                            mailDetail.To.Add(candidateInterviewDetail.CandidateEmail);

                        UserRegistrationInfo candidateInfo;
                        candidateInfo = new UserRegistrationDLManager().GetUserRegistrationInfo
                            (Convert.ToInt32(candidateInterviewDetail.CandidateID));

                        if (candidateInfo != null)
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidateUserName, candidateInfo.UserEmail);
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidateFirstName, candidateInfo.FirstName);
                            mailDetail.MailValues.Add(MailTemplateKeywords.CandidateLastName, candidateInfo.LastName);
                        }
                        // Add subject.
                        mailDetail.Subject = "ForteHCM-Review and confirm interview timeslots";

                        // Construct interview publish url
                        string slotUrl = string.Format(ConfigurationManager.AppSettings["CANDIDATE_TIME_SLOT_APPROVAL"],
                            candidateInterviewDetail.SlotKey);


                        mailDetail.MailValues.Add(MailTemplateKeywords.CandidateSlotDetail, sbBody);
                        mailDetail.MailValues.Add(MailTemplateKeywords.CandidateSlotURL, slotUrl);
                        

                        mailDetail.isBodyHTML = Convert.ToBoolean(ConfigurationManager.AppSettings["IS_BODY_HTML"]);

                        mailDetail.ForteHCMLogo = new FileInfo(forteHCMLogoUrl);
                        if (mailDetail.isBodyHTML)
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.ForteHCMLogo, "<img src='cid:ForteHCMLogo'>");

                        }
                        else
                        {
                            mailDetail.MailValues.Add(MailTemplateKeywords.ForteHCMLogo, "");
                        }
                        if (mailDetail.isBodyHTML)
                        {
                            mailDetail.Message = mHelper.GetMailContentFromTemplate(templatesUrl + ConfigurationManager.AppSettings[MailTemplateType.OnlineInterviewSlotRequestHtml], mailDetail.MailValues);
                        }
                        else
                        {
                            mailDetail.Message = mHelper.GetMailContentFromTemplate(templatesUrl + ConfigurationManager.AppSettings[MailTemplateType.OnlineInterviewSlotRequestTxt], mailDetail.MailValues);
                        }
                    }

                    break;
                default:
                    break;
            }

            return mailDetail;
        }

        #endregion Private Methods

        #region Offline Interview                                              

        /// <summary>
        /// Method that compose and send for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data. This will be type 
        /// casted to specific type based on entity type.
        /// </param>
        public bool OfflineInterviewSendMail(EntityType entityType, object data)
        {
            try
            {

                MailDetail mailDetail = GetMailDetailForOfflineInterview(entityType, data);

                new MailHelper().SendMessage(mailDetail);

                return true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                return false;
            }
        }

        #endregion Offline Interview

        #region Candidate Slot Request

        /// <summary>
        /// Method that compose and send for the given entity and type.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        /// <param name="data">
        /// A <see cref="object"/> that holds the data. This will be type 
        /// casted to specific type based on entity type.
        /// </param>
        public bool OnlineInterviewSlotRequestSendMail(EntityType entityType, object data)
        {
            try
            {
                MailDetail mailDetail = GetMailDetailForCandidateSlotRequest(entityType, data);

                new MailHelper().SendMessage(mailDetail);

                return true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                return false;
            }
        }

        #endregion Candidate Slot Request
    }
}