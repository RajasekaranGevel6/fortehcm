#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MailHelper.cs
// File that represents the mail helper compopent, that helps in 
// constructing mail contents using the html or text templates.

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.Configuration;
using System.Text.RegularExpressions;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Exceptions;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the mail helper compopent, that helps in 
    /// constructing mail contents using the html or text templates.
    /// </summary>
    public class MailHelper
    {
        #region Public Methods                                                 

        /// <summary>
        /// This method gets the content from template file by passing file name.
        /// </summary>
        /// A <param name="sTemplateFileName"> refers the file name</param>
        /// A <param name="MailValues"> contains the to address, from address, cc and all other mail details.</param>
        /// <returns></returns>
        public string GetMailContentFromTemplate(string sTemplateFileName,Hashtable MailValues)
        {
            
            string sMessage = "";
            if (System.IO.File.Exists(sTemplateFileName))
            {

                string errorMessage = "";
                if (!ReadFileContents(System.Convert.ToString(sTemplateFileName), ref sMessage, ref errorMessage))
                {
                    Logger.TraceLog(string.Format("Error reading the file {0}, ErrorMessage: {1} ", sTemplateFileName), errorMessage);
                }
                else
                {
                    sMessage = ReplaceKeywords(sMessage, MailValues);
                }
            }
            else
            {

                if (!string.IsNullOrEmpty(sTemplateFileName))
                {
                    Logger.TraceLog(string.Format("File {0} not found",sTemplateFileName));
                }

                
                //if (System.Convert.ToString(sTemplateFileName) == Configuration.MailContent.PasswordMessageTemplate)
                //{ 
                //    sMessage = GetDefaultMailContent(EmailNotificationTemplateTypes.PasswordMessageTemplate); 
                //}
                
                sMessage = ReplaceKeywords(sMessage, MailValues);
            }
            return sMessage;
        }

        /// <summary>
        /// This method fills the data with the keywords specified in mail template.
        /// </summary>
        /// A <param name="Template"> contains the content of the template file.</param>
        /// A <param name="KeyTable"> contains all the keys with the data to fill.</param>
        /// <returns></returns>
        public string ReplaceKeywords(string Template, Hashtable KeyTable)
        {
            String data;
            data = Template;
            RegexOptions options = RegexOptions.IgnoreCase;

            IDictionaryEnumerator _enumerator = KeyTable.GetEnumerator();

            while (_enumerator.MoveNext())
            {
                string strPattern = _enumerator.Key.ToString();
                Regex re = new Regex(strPattern, options);

                if (_enumerator.Value != null)
                    data = re.Replace(data, _enumerator.Value.ToString());
            }
            return data;
        }

        /// <summary>
        /// Method that send the mail.
        /// </summary>
        /// <param name="message">
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </param>
        public void SendMessage(MailDetail mailDetail)
        {

            SmtpClient smtpClient = new SmtpClient(
                ConfigurationManager.AppSettings["SMTP_CLIENT"],
                Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_PORT"]));
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(
                ConfigurationManager.AppSettings["SMTP_USER"],
                ConfigurationManager.AppSettings["SMTP_PASSWORD"]);

            smtpClient.ServicePoint.MaxIdleTime = Convert.ToInt32
                (ConfigurationManager.AppSettings["MAX_IDLE_TIME"]);

            smtpClient.EnableSsl = Convert.ToBoolean
                (ConfigurationManager.AppSettings["USE_SECURE_SOCKET_LAYER"]);

            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            /*string failedToAddr = null;
            string failedCCAddr = null;*/
            MailMessage message = null;
            try
            {
                message = GetMailMessage(mailDetail);

                smtpClient.Send(message);
            }
            catch (SmtpFailedRecipientsException smtpFailedRecipientsException)
            {
                string failedRecipientList = string.Empty;
                for (int i = 0; i < smtpFailedRecipientsException.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = smtpFailedRecipientsException.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy
                            || status == SmtpStatusCode.MailboxUnavailable)
                    {
                        if (failedRecipientList == string.Empty)
                            failedRecipientList += GetMailID(smtpFailedRecipientsException.InnerExceptions[i].FailedRecipient);
                        else
                            failedRecipientList += ", " + GetMailID(smtpFailedRecipientsException.InnerExceptions[i].FailedRecipient);
                    }
                }
                if (message != null)
                {
                    EmailFailureLog(message, failedRecipientList,
                        smtpFailedRecipientsException.InnerException.ToString(), 1);
                }


                throw new EMailException(string.Format
                    ("<br>The message could not be delivered to the recipient(s) {0}", failedRecipientList));
            }
            catch (SmtpException smtpException)
            {
                Logger.ExceptionLog(smtpException);

                if (message != null)
                {
                    string emailFailedList = message.To.ToString() + "," + message.CC.ToString();

                    EmailFailureLog(message,
                        emailFailedList, smtpException.InnerException.ToString(), 1);
                }

                if (smtpException.StatusCode == SmtpStatusCode.MailboxUnavailable)
                {
                    throw new EMailException(string.Format
                        ("<br>The message could not be delivered to the recipient(s) {0}",
                        GetMailID(((SmtpFailedRecipientException)(smtpException)).FailedRecipient)));
                }
                else
                {
                    throw smtpException;
                }
            }
        }

        #endregion Public Methods

        #region Public Static Methods                                          

        /// <summary>
        /// This method reads the template file content.
        /// </summary>
        /// A <param name="FullPath"> contains the full path of the template file name.</param>
        /// A <param name="strContents"> is the reference 
        /// parameter to store the content of the file.</param>
        /// <param name="errInfo">This parameter is the reference  
        /// parameter to store the error message.</param>
        /// <returns>It returns true if the file is read otherwise it returns false.</returns>
        public static bool ReadFileContents(string FullPath, ref string strContents, ref string errInfo)
        {

            StreamReader objReader = null;
            try
            {
                objReader = new StreamReader(FullPath);
                strContents = objReader.ReadToEnd();
                objReader.Close();
                return true;
            }
            catch (Exception Ex)
            {
                errInfo = Ex.Message;
                return false;
            }
        }

        #endregion Public Static Methods

        #region Private Methods                                                

        /// <summary>
        /// This method validates the mail id.
        /// </summary>
        /// <param name="recipient"></param>
        /// <returns></returns>
        private string GetMailID(string recipient)
        {
            if (Utility.IsNullOrEmpty(recipient))
                return string.Empty;

            return recipient.Replace("<", string.Empty).Replace(">", string.Empty);
        }

        /// <summary>
        /// Method that construct and returns the <see cref="MailMessage"/>
        /// object for the given <see cref="MailDetail"/> object.
        /// </summary>
        /// <param name="mailDetail">
        /// A <see cref="MailDetail"/> that holds the mail detail.
        /// </param>
        /// <returns>
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </returns>
        private MailMessage GetMailMessage(MailDetail mailDetail)
        {
            MailMessage mailMessage = new MailMessage();

            // From address.
            if (Utility.IsNullOrEmpty(mailDetail.From))
            {
                mailMessage.From = new MailAddress(ConfigurationManager.
                    AppSettings["EMAIL_FROM_ADDRESS"]);
            }
            else
            {
                mailMessage.From = new MailAddress(mailDetail.From);
            }

            // To address.
            if (!Utility.IsNullOrEmpty(mailDetail.To) && mailDetail.To.Count != 0)
            {
                foreach (string toAddress in mailDetail.To)
                    mailMessage.To.Add(new MailAddress(toAddress));
            }

            // CC address.
            if (!Utility.IsNullOrEmpty(mailDetail.CC) && mailDetail.CC.Count != 0)
            {
                foreach (string ccAddress in mailDetail.CC)
                    mailMessage.CC.Add(new MailAddress(ccAddress));
            }

            // Subject.
            mailMessage.Subject = mailDetail.Subject;

            // Message.
            mailMessage.Body = mailDetail.Message;



            if (mailDetail.Attachments != null)
            {
                foreach (MailAttachment mailAttachment in mailDetail.Attachments)
                {
                    if (mailAttachment.FileContent == null ||
                        mailAttachment.FileContent.Length == 0)
                    {
                        continue;
                    }

                    MemoryStream memoryStream = new MemoryStream(mailAttachment.FileContent);
                    mailMessage.Attachments.Add(new Attachment(memoryStream,
                        mailAttachment.FileName, GetMimeType(mailAttachment.FileName.Substring
                        (mailAttachment.FileName.LastIndexOf('.') + 1))));
                }
            }
            AlternateView av;
            bool isBodyHTML = Convert.ToBoolean(ConfigurationManager.AppSettings["IS_BODY_HTML"]);
            if (isBodyHTML)
            {
                mailDetail.isBodyHTML = mailMessage.IsBodyHtml = true;
            }
            else
            {
                mailDetail.isBodyHTML = mailMessage.IsBodyHtml = false;
            }
            //if (mailDetail.isBodyHTMLPositionProfile && mailMessage.IsBodyHtml)
            //{
            //    mailMessage.Body = mailMessage.Body.Replace("\r\n", "<br />");
            //    mailMessage.Body = mailMessage.Body.Replace("\n\n", "<br /><br />");
            //    mailMessage.Body = mailMessage.Body.Replace("\n", "<BR/>");

            //}

            if (isBodyHTML)
            {
                av = AlternateView.CreateAlternateViewFromString(System.Convert.ToString(mailMessage.Body), new System.Net.Mime.ContentType("text/html"));
                if (mailDetail.ForteHCMLogo != null)
                {
                    if (!mailDetail.ForteHCMLogo.Exists)
                    {
                        Logger.TraceLog(string.Format("{0} file not found. ", mailDetail.ForteHCMLogo));
                    }
                    else
                    {
                        LinkedResource embedimage = new LinkedResource(mailDetail.ForteHCMLogo.FullName);
                        embedimage.ContentId = "ForteHCMLogo";
                        embedimage.ContentType.Name = "gif";
                        av.LinkedResources.Add(embedimage);
                    }
                }
            }
            else
            {
                av = AlternateView.CreateAlternateViewFromString(System.Convert.ToString(mailMessage.Body), new System.Net.Mime.ContentType("text/plain"));
            }
            
            mailMessage.AlternateViews.Add(av);


            return mailMessage;
        }

        /// <summary>
        /// Method that retrieves the mime type for the given file extension.
        /// </summary>
        /// <param name="fileExtenion">
        /// A <see cref="string"/> that holds the file extension.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the mime type.
        /// </returns>
        private string GetMimeType(string fileExtenion)
        {
            string mimeType = string.Empty;
            switch (fileExtenion.Trim().ToLower())
            {
                case "doc":
                    mimeType = "application/msword";
                    break;
                case "pdf":
                    mimeType = "application/pdf";
                    break;
                case "rtf":
                    mimeType = "text/rtf";
                    break;
                case "jpg":
                    mimeType = "image/jpeg";
                    break;
                case "jpeg":
                    mimeType = "image/jpeg ";
                    break;
                case "bmp":
                    mimeType = "image/bmp";
                    break;
                case "tif":
                    mimeType = "image/tiff";
                    break;
                default:
                    mimeType = "application/octet-stream";
                    break;
            }
            return mimeType;
        }

        /// <summary>
        /// Method that send the mail.
        /// </summary>
        /// <param name="message">
        /// A <see cref="MailMessage"/> that holds the mail message.
        /// </param>
        private void OfflineInterviewSend(MailMessage message)
        {
            SmtpClient smtpClient = new SmtpClient(
                ConfigurationManager.AppSettings["SMTP_CLIENT"],
                Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_PORT"]));
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(
                ConfigurationManager.AppSettings["SMTP_USER"],
                ConfigurationManager.AppSettings["SMTP_PASSWORD"]);

            smtpClient.ServicePoint.MaxIdleTime = Convert.ToInt32
                (ConfigurationManager.AppSettings["MAX_IDLE_TIME"]);

            smtpClient.EnableSsl = Convert.ToBoolean
                (ConfigurationManager.AppSettings["USE_SECURE_SOCKET_LAYER"]);

            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            try
            {
                smtpClient.Send(message);
            }
            catch (SmtpFailedRecipientsException smtpFailedRecipientsException)
            {
                //Logger.ExceptionLog(smtpFailedRecipientsException);
                string failedRecipientList = string.Empty;
                for (int i = 0; i < smtpFailedRecipientsException.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = smtpFailedRecipientsException.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy
                            || status == SmtpStatusCode.MailboxUnavailable)
                    {
                        if (failedRecipientList == string.Empty)
                            failedRecipientList += OfflineInterviewGetMailID(smtpFailedRecipientsException.InnerExceptions[i].FailedRecipient);
                        else
                            failedRecipientList += ", " + OfflineInterviewGetMailID(smtpFailedRecipientsException.InnerExceptions[i].FailedRecipient);
                    }
                }

                EmailFailureLog(message, failedRecipientList, smtpFailedRecipientsException.InnerException.ToString(), 1);

                throw new Exception(string.Format
                    ("<br>The message could not be delivered to the recipient(s) {0}", failedRecipientList));
            }
            catch (SmtpException smtpException)
            {
                Logger.ExceptionLog(smtpException);

                if (message != null)
                {
                    string emailFailedList = message.To.ToString() + "," + message.CC.ToString();

                    EmailFailureLog(message, emailFailedList, smtpException.InnerException.ToString(), 1);
                }
                if (smtpException.StatusCode == SmtpStatusCode.MailboxUnavailable)
                {
                    /*throw new Exception(string.Format
                        ("<br>The message could not be delivered to the recipient(s) {0}",
                        OfflineInterviewGetMailID(((SmtpFailedRecipientException)(smtpException)).FailedRecipient)));*/
                }
                /*else
                {
                    throw smtpException;
                }*/
            }
        }

        private string OfflineInterviewGetMailID(string recipient)
        {
            if (Utility.IsNullOrEmpty(recipient))
                return string.Empty;

            return recipient.Replace("<", string.Empty).Replace(">", string.Empty);
        }

        /// <summary>
        /// </summary>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <param name="ccId"></param>
        /// <param name="faileReceints"></param>
        /// <param name="exceptionMsg"></param>
        /// <param name="userId"></param>
        private void EmailFailureLog(MailMessage mailMsg, string faileReceints,
            string exceptionMsg, int userId)
        {
            EmailExceptionDLManager emailExceptionManager =
                        new EmailExceptionDLManager();
            mailMsg.IsBodyHtml = false;

            string mailBody = null;
            string mailFromAddr = null;
            string mailToAddr = null;
            string mailCCAddr = null;

            if (!Utility.IsNullOrEmpty(mailMsg.Body))
                mailBody = mailMsg.Body.ToString();

            if (!Utility.IsNullOrEmpty(mailMsg.From))
                mailFromAddr = mailMsg.From.ToString();

            if (!Utility.IsNullOrEmpty(mailMsg.To))
                mailToAddr = mailMsg.To.ToString();

            if (!Utility.IsNullOrEmpty(mailMsg.CC))
                mailCCAddr = mailMsg.CC.ToString();

            string msgType = "N";
            if (mailMsg.IsBodyHtml)
                msgType = "Y";
            emailExceptionManager.InsertEmailFailureLog(mailBody,
                  mailFromAddr, mailToAddr, mailCCAddr, faileReceints, exceptionMsg, userId, mailMsg.Subject, msgType);
        }

        #endregion Private Methods
    }
}

