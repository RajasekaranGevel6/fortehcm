﻿namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the mail template type constants.
    /// </summary>
    public class MailTemplateType
    {
        public const string InterviewScheduledTxt = "INTERVIEW_SCHEDULED_TEXT";
        public const string InterviewScheduledHtml = "INTERVIEW_SCHEDULED_HTML";

        public const string TestScheduledTxt = "TEST_SCHEDULED_TEXT";
        public const string TestScheduledHtml = "TEST_SCHEDULED_HTML";

        public const string TestCompletedTxt = "TEST_COMPLETED_TEXT";
        public const string TestCompletedHtml = "TEST_COMPLETED_HTML";

        public const string InterviewCompletedTxt = "INTERVIEW_COMPLETED_TEXT";
        public const string InterviewCompletedHtml = "INTERVIEW_COMPLETED_HTML";

        public const string OnlineInterviewSlotRequestTxt = "ONLINE_INTERVIEW_SLOT_REQUEST_TEXT";
        public const string OnlineInterviewSlotRequestHtml = "ONLINE_INTERVIEW_SLOT_REQUEST_HTML";
    }
}
