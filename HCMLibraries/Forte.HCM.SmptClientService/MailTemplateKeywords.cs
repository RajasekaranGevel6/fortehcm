﻿namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the mail template keyword constants.
    /// </summary>
    public class MailTemplateKeywords
    {
        public const string InterviewId = @"\$INTERVIEW_ID\$";
        public const string InterviewName = @"\$INTERVIEW_NAME\$";
        public const string ExpiryDate = @"\$EXPIRY_DATE\$";
        public const string CandidateUserName = @"\$CANDIDATE_USERNAME\$";
        public const string CandidatePassword = @"\$CANDIDATE_PASSWORD\$";
        public const string SchedulerCompany= @"\$SCHEDULER_COMPANY\$";
        public const string SchedulerFirstName = @"\$SCHEDULER_FIRST_NAME\$";
        public const string SchedulerLastName = @"\$SCHEDULER_LAST_NAME\$";
        public const string CandidateFirstName = @"\$CANDIDATE_FIRSTNAME\$";
        public const string CandidateLastName = @"\$CANDIDATE_LASTNAME\$";
        public const string ForteHCMLogo = @"\$IMAGE_LOGO\$";
        public const string CandidateURL = @"\$CANDIDATE_URL\$";
        public const string TestInstructionURL = @"\$TEST_INSTRUCTION_URL\$";
        public const string InterviewInstructionURL = @"\$INTERVIEW_INSTRUCTION_URL\$";

        public const string ResultWithoutScoreURL = @"\$RESULT_WITH_SCORE_URL\$";
        public const string ResultWithScoreURL = @"\$RESULT_WITHOUT_SCORE_URL\$";
        public const string CandidateTestResultURL = @"\$CANDIDATE_TEST_RESULT_URL\$";
        public const string CandidateInterviewURL = @"\$CANDIDATE_INTERVIEW_URL\$";
        public const string TestName = @"\$TEST_NAME\$";
        public const string CompletedDate = @"\$COMPLETED_DATE\$";

        public const string CandidateSlotURL = @"\$CANDIDATE_SLOT_URL\$";
        public const string CandidateSlotDetail = @"\$CANDIDATE_SLOT_DETAIL\$";
        public const string OnlineInterviewName = @"\$ONLINE_INTERVIEW_NAME\$";
        public const string OnlineInterviewInstruction = @"\$ONLINE_INTERVIEW_INSTRUCTION\$";
        public const string OnlineInterviewRequestedBy = @"\$ONLINE_INTERVIEW_REQUESTED_BY\$";
        public const string SchedulerFullName = @"\$SCHEDULER_FULL_NAME\$";
    }
}
