﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestSessionDLManager.cs
// File that represents the data layer for the Test Session module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives                                                             
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Test Session module.
    /// This includes functionalities for retrieving Session details 
    /// This will talk to the database for performing these operations.
    /// </summary>
    public class TestSessionBLManager
    {
        #region Public Method                                                  

        /// <summary>
        /// Method that retrieves the list of test sessions for the given 
        /// search criteria.
        /// </summary>
        /// <param name="testSessionSearchCriteria"> 
        /// A <see cref="TestSessionSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="total">
        /// A <see cref="int"/> that holds the total records in the list as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestSessionSearch"/> that holds the test 
        /// sessions.
        /// </returns>
        public List<TestSessionSearch> GetTestSessionDetails(TestSessionSearchCriteria 
            testSessionSearchCriteria,int PageNumber, int PageSize, 
            string orderBy, SortType direction, out int Total)
        {
            return new TestSessionDLManager().GetTestSessionDetails(testSessionSearchCriteria, PageNumber, PageSize, orderBy, direction, out Total);
        }

        /// <summary>
        /// Method that retreives the list of candidate session IDs as comma 
        /// separated and other related details for the given test session ID.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that holds the test session ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that holds the test session detail.
        /// </returns>
        public TestSessionDetail GetCandidateSessions(string testSessionID)
        {
            return new TestSessionDLManager().GetCandidateSessions(testSessionID);
        }

        /// <summary>
        /// Represents the method to get the bool value to show candidate
        /// test score
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the candidate session id
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds whether to display score or not
        /// </returns>
        public bool GetShowTestScore(string candidateSessionID)
        {
            return new TestSessionDLManager().GetShowTestScore(candidateSessionID);
        }
        /// <summary>
        /// Method gets the test session id details using the parameter navigatekey
        /// </summary>
        /// A<param name="navigateKey">holds the navigate key</param>
        /// <returns></returns>
        public TestSessionIdDetails GetTestSessionIdDetails(string navigateKey)
        {
            return new TestSessionDLManager().GetTestSessionIdDetails(navigateKey);
        }


        public TestScheduleSearchCriteria GetPositionProfileTests(int positionProfileId, out int testCount, out string createBy)
        {
            return new TestSessionDLManager().GetPositionProfileTests(positionProfileId, out testCount, out createBy);
        }

        public List<TestSessionDetail> GetPositionProfileTestSessionCount(int positionProfileId)
        {
            return new TestSessionDLManager().GetPositionProfileTestSessionCount(positionProfileId);
        }
        public List<string> GetPositionProfileSubmittalCandidates(int positionProfileId, out int totalCount, out string createdBy)
        {
            return new TestSessionDLManager().GetPositionProfileSubmittalCandidates(positionProfileId,out totalCount, out createdBy);
        }
        #endregion
    }
}
