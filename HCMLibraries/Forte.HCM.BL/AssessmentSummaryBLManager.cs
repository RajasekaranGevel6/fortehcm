﻿using System;
using System.Data;

using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.BL
{
    public class AssessmentSummaryBLManager
    {
        public AssessmentSummary GetAssessorSummary(string candidateInterviewKey,
            int attemptID, int assessorID,char orderBy)
        {
            return new AssessmentSummaryDLManager().GetAssessorSummary(candidateInterviewKey, attemptID, assessorID,orderBy);
        }

        /// <summary>
        /// Method to call the question summary for publish
        /// </summary>
        /// <param name="candidateInterviewKey"></param>
        /// <param name="attemptID"></param>
        /// <param name="assessorID"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public AssessmentSummary GetAssessorSummaryPublish(string candidateInterviewKey,
            int attemptID, char orderBy)
        {
            return new AssessmentSummaryDLManager().GetAssessorSummaryPublish(candidateInterviewKey, attemptID, orderBy);
        }

        /// <summary>
        /// Method to update the interview score code
        /// </summary>
        /// <param name="candidateSessionKey"></param>
        /// <param name="attemptID"></param>
        /// <param name="scoreCode"></param>
        /// <returns></returns>
        public InterviewScoreParamDetail UpdateCandidateInterviewScoreCode(string candidateSessionKey,
            int attemptID, string scoreCode)
        {
            return new AssessmentSummaryDLManager().UpdateCandidateInterviewScoreCode(candidateSessionKey, attemptID, scoreCode);
        }

        /// <summary>
        /// Method to get the candidate interview publish details by score code
        /// </summary>
        /// <param name="scoreCode"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public InterviewScoreParamDetail GetCandidateInterviewPublishDetails(string scoreCode)
        {
            return new AssessmentSummaryDLManager().GetCandidateInterviewPublishDetails(scoreCode);
        }

        public void SaveCandidateAssessmentRatngComments(AssessmentSummary assessmentSummary, int assessorID, 
            string candidateInterviewSessionKey, int attemptID)
        {

            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                new AssessmentSummaryDLManager().SaveCandidateAssessmentRatngComments(assessmentSummary, assessorID,
                  candidateInterviewSessionKey, attemptID, transaction);
                transaction.Commit();

            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the rating summary for the given candidate 
        /// session ID and attempt ID.
        /// </summary>
        /// <param name="candidateSesssionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetRatingSummary(string candidateSesssionID, int attemptID)
        {
            return new AssessmentSummaryDLManager().GetRatingSummary
                (candidateSesssionID, attemptID);
        }

        /// <summary>
        /// Method to update the candidate interview score
        /// </summary>
        /// <param name="interviewTestKey"></param>
        /// <param name="candidateInterviewSessionKey"></param>
        /// <param name="attempID"></param>
        /// <param name="totalSubjectScore"></param>
        /// <param name="totalSubjectWeightageScore"></param>
        /// <param name="userID"></param>
        public void UpdateCanidateInterviewScore(string interviewTestKey, string candidateInterviewSessionKey,
            int attempID, decimal totalSubjectScore, decimal totalSubjectWeightageScore, int userID)
        {
             new AssessmentSummaryDLManager().UpdateCanidateInterviewScore(interviewTestKey,candidateInterviewSessionKey,
                    attempID,totalSubjectScore,totalSubjectWeightageScore,userID);
        }

        /// <summary>
        /// Method that retrieves the rating summary for the given interview id
        /// </summary>
        /// <param name="onlineInterviewId">
        /// A<see cref="int"/> that hold the candidate interview id
        /// </param>
        /// <returns>
        /// A<see cref="Dataset"/> that holds the summary data
        /// </returns>
        public DataSet GetOnlineInterviewAssessorySummary(int onlineInterviewId)
        {
            return new AssessmentSummaryDLManager().GetOnlineInterviewAssessorySummary(onlineInterviewId);  
        }
    }
}
