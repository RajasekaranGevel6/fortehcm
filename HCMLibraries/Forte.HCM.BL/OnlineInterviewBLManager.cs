﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OnlineInterviewBLManager.cs
// File that represents the data layer for the online interview session module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;

#endregion

namespace Forte.HCM.BL
{
    public class OnlineInterviewBLManager
    {
        #region Public Method

        /// <summary>
        /// Saves online interview test session and the candidate session details to the respective repositories.
        /// i.e. ONLINE_INTEVIEW_TEST_SESSION, ONLINE_SESSION_CANDIDATE
        /// </summary>
        /// <param name="onlineInterviewTestSessionDetail">
        /// A <see cref="TestSessionDetail"/> that contains the Online Interview Test Session Detail collection.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creator of the online interview test session.
        /// </param>
        /// <param name="onlineInterviewTestSessionID">
        /// A <see cref="string"/> that contains the Online Interview Test session Id.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate Session IDs.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the Mail Sent Or Not.
        /// </param>
        public void SaveOnlineInterviewSession(OnlineInterviewSessionDetail onlineInterviewSessionDetail,
            List<AssessorTimeSlotDetail> slots,List<AssessorDetail> assessorDetails,
            int userID,
            out string onlineInterviewSessionKey, out string candidateSessionIDs, out bool isMailSent)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            OnlineInterviewDLManager interviewSessionDLManager = new OnlineInterviewDLManager();
            NextNumberDLManager nextNumberDLManager = new NextNumberDLManager();

            onlineInterviewSessionKey = string.Empty;
            candidateSessionIDs = string.Empty;
            isMailSent = false;
            int assessorId = 0;
            string assessorSkillIds = string.Empty;

            try
            {
                if (onlineInterviewSessionDetail.OnlineInterviewSessionKey == null)
                {
                    // Generate online interview session id and assign to a string variable
                    onlineInterviewSessionKey = nextNumberDLManager.GetNextNumber(transaction, "OLS", userID);
                    onlineInterviewSessionDetail.OnlineInterviewSessionKey = onlineInterviewSessionKey;

                    // Insert online interview session
                    interviewSessionDLManager.InsertOnlineInterviewSession(onlineInterviewSessionDetail, userID, transaction);

                    //Insert skill id based on the assessorid
                    foreach (AssessorDetail assessorDetail in assessorDetails)
                    {
                        assessorId = assessorDetail.UserID;
                        assessorSkillIds = assessorDetail.Skill.ToString();
                        if (assessorSkillIds != string.Empty)
                            interviewSessionDLManager.InsertOnlinerInterviewSessionAssessorSkills(onlineInterviewSessionKey,
                                assessorId, assessorSkillIds, userID, transaction);
                    }

                    if (slots != null)
                    {
                        foreach (AssessorTimeSlotDetail slot in slots)
                        {
                            // Assing ref key.
                            slot.SessionKey = onlineInterviewSessionKey;
                            slot.RequestStatus = Constants.TimeSlotRequestStatus.PENDING_CODE;
                            slot.InitiatedBy = userID;
                            new AssessorDLManager().InsertAssessorTimeSlot(0,slot,0, transaction);
                        }
                    }
                    // Insert candidate online interview session
                    for (int candidateSessionCount = 0; candidateSessionCount <
                        onlineInterviewSessionDetail.NumberOfCandidateSessions; candidateSessionCount++)
                    {
                        CandidateInterviewSessionDetail candidateinterviewSessionDetail = new CandidateInterviewSessionDetail();

                        // Get candidate session id
                        string candidateSessionID = nextNumberDLManager.GetNextNumber(transaction, "COL", userID);

                        if (candidateSessionIDs.Trim().Length == 0)
                            candidateSessionIDs = candidateSessionID;
                        else
                            candidateSessionIDs += ", " + candidateSessionID;

                        // Assign candidate test session details
                        candidateinterviewSessionDetail.CandidateTestSessionID = candidateSessionID.ToString().Trim();
                        candidateinterviewSessionDetail.InterviewTestSessionID = onlineInterviewSessionKey.Trim();
                        candidateinterviewSessionDetail.Status = Constants.CandidateSessionStatus.NOT_SCHEDULED;
                        candidateinterviewSessionDetail.CreatedBy = onlineInterviewSessionDetail.CreatedBy;
                        candidateinterviewSessionDetail.ModifiedBy = onlineInterviewSessionDetail.ModifiedBy;

                        // Add candidate test session detail to the list
                        interviewSessionDLManager.InsertCandidateOnlineInterviewSession(candidateinterviewSessionDetail, onlineInterviewSessionDetail.AssessorIDs, transaction);
                    }

                    // Insert test description
                    interviewSessionDLManager.InsertOnlineInterviewSessionDescription(onlineInterviewSessionDetail, transaction);
                }

                // Commit the transaction
                transaction.Commit();

                // Send Mail
                try
                {
                    // Send email to the candidate who is requested to reschedule
                    new EmailHandler().SendMail
                        (EntityType.OnlineInterviewSessionCreated, onlineInterviewSessionKey);

                    // Mail sent successfully.
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        /// <summary>
        /// This method searches the test session details by passing SessionName,
        /// SessionKey,ClientRequestNumber or SchedulerName,Skills
        /// </summary>
        /// <param name="onlineInterviewSessionSearchCriteria">
        /// A list of<see cref="OnlineInterviewSessionSearchCriteria"/> that holds the online interview search Criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>that holds the page Number
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/>that holds the page Size
        /// </param>
        /// <param name="totalRecords">
        ///  A <see cref="int"/>that holds the total Records
        /// </param>
        /// <param name="sortingKey">
        ///  A <see cref="string"/>that holds the sorting Key 
        /// </param>
        /// <param name="sortByDirection">
        ///  A <see cref="SortType"/>that holds the sortBy Direction Like 'A' or 'D'(Asc/dec)
        /// </param>
        /// <returns>
        ///  A list of <see cref="OnlinerInterviewSessionDetail"/>that holds the Online Interview Session Detail
        /// </returns>
        public List<OnlineInterviewSessionDetail> SearchOnlineInterviewSessionDetails(OnlineInterviewSessionSearchCriteria 
            onlineInterviewSessionCriteria,int pageNumber, int pageSize, out int totalRecords, 
            string sortingKey, SortType sortByDirection)
        {
            return new OnlineInterviewDLManager().SearchOnlineInterviewSessionDetails(onlineInterviewSessionCriteria, 
                    pageNumber, pageSize, out totalRecords, sortingKey, sortByDirection);
        }

        /// <summary>
        /// Method that will return the test session and candidate session details
        /// based on the search criteria.
        /// </summary>
        /// <param name="scheduleCandidateSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search criteria details.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that contains the sort expression.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="SortType"/> that contains the sort type either ASC/DESC.
        /// </param>
        /// <returns>
        /// A <see cref="OnlineInterviewSessionDetail"/> that contains the online interview session details information.
        /// </returns>
        public OnlineInterviewSessionDetail GetOnlineInterviewSessionDetail(TestScheduleSearchCriteria scheduleCandidateSearchCriteria,
            string sortExpression, SortType sortDirection)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewSessionDetail(scheduleCandidateSearchCriteria,
                sortExpression, sortDirection);
        }

        /// <summary>
        /// This method passes the SessionKey and CandidateSession keys separated by comma 
        /// and return the invalid candidateSession keys.
        /// </summary>
        /// <param name="sessionKey">
        /// A <see cref="string"/>that holds the session Key
        /// </param>
        /// <param name="candidateSessionKeys">
        /// A <see cref="string"/>that holds the candidate Session Keys
        /// </param>
        /// <returns>
        /// A List of <see cref="String"/>that holds mismatched Candidate Ids.
        /// </returns>
        public List<string> ValidateOnlineInterviewSessionInput
            (string sessionKey, string candidateSessionKeys)
        {
            return new OnlineInterviewDLManager().ValidateOnlineInterviewSessionInput(sessionKey, candidateSessionKeys);
        }


        /// <summary>
        /// Method that will update the scheduled candidate details for online interview.
        /// </summary>
        /// <param name="onlineCandidateSessionDetail">
        /// A <see cref="OnlineCandidateSessionDetail"/> that contains the schedule detail.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <remarks>
        public void ScheduleOnlineInterviewCandidate(OnlineCandidateSessionDetail onlineCandidateSessionDetail,
            List<AssessorTimeSlotDetail> timeSlots, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                new OnlineInterviewDLManager().UpdateOnlineInterviewScheduleCandidate(onlineCandidateSessionDetail,
                    userID, transaction);

                if (timeSlots != null)
                    foreach (AssessorTimeSlotDetail timeSlot in timeSlots)
                        new OnlineInterviewDLManager().InsertOnlineInterviewScheduleCandidateTimeSlots(
                            timeSlot, onlineCandidateSessionDetail.CandidateSessionID, userID, transaction);

                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

       /// <summary>
       /// Method that will insert the timeslots selected at the time of scheduling the candidate.
       /// </summary>
       /// <param name="slots"></param>
       /// <param name="userID"></param>
        public void InsertAssessorTimeSlot(List<AssessorTimeSlotDetail> slots, string onlineInterviewSessionKey, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                if (slots != null)
                {
                    foreach (AssessorTimeSlotDetail slot in slots)
                    {
                        // Assing ref key.
                        slot.SessionKey = onlineInterviewSessionKey;
                        slot.RequestStatus = Constants.TimeSlotRequestStatus.PENDING_CODE;
                        slot.InitiatedBy = userID;
                        new AssessorDLManager().InsertAssessorTimeSlot(0, slot,0, transaction);
                    }
                }
                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        /// <summary>
        /// Method that will insert the timeslots against this candidate.
        /// </summary>
        /// <param name="slots"></param>
        /// <param name="userID"></param>
        public void InsertCandidateTimeSlot(List<AssessorTimeSlotDetail> timeslots, string candidateSessionKey, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                if (timeslots != null)
                    foreach (AssessorTimeSlotDetail timeslot in timeslots)
                        new OnlineInterviewDLManager().InsertOnlineInterviewScheduleCandidateTimeSlots(
                            timeslot, candidateSessionKey, userID, transaction);

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that will get the candidate details.
        /// </summary>
        /// <param name="onlineCandidateSessionDetail">
        /// A <see cref="OnlineCandidateSessionDetail"/> that contains the candidate session detail.
        /// </param>
        public OnlineCandidateSessionDetail GetCandidateDetailsByCandidateSessionKey(string candidateSessionKey)
        {
           return new OnlineInterviewDLManager().GetCandidateDetailsByCandidateSessionKey(candidateSessionKey);
        }

        /// <summary>
        /// Method that checks if assessor is expertise in the given skill set
        /// parameters.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="string"/> that holds the skill ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the expertise status. If 
        /// available returns true and otherwise false.
        /// </returns>
        public bool IsAssessorExpertise(int assessorID, int skillID)
        {
            return new OnlineInterviewDLManager().IsAssessorExpertise(assessorID, skillID);
        }

        /// <summary>
        /// Inserts Online Interview details in [ONLINE_INTERVIEW] table object
        /// </summary>
        /// <param name="onlineInterviewSessionDetail">
        /// A <see cref="OnlineInterviewSessionDetail"/> that holds the online interview detail.
        /// </param>
        /// <param name="skillDetails">
        /// A <see cref="SkillDetail"/>that holds the added skill details.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that contains the interview key.
        /// </param>
        public void SaveOnlineInterview(OnlineInterviewSessionDetail onlineInterviewDetail,
            List<SkillDetail> skillDetails, int userID, out string interviewKey)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            interviewKey = string.Empty;
            try
            {
                if (!Utility.IsNullOrEmpty(onlineInterviewDetail))
                {

                    //Get online interview key
                    if (Utility.IsNullOrEmpty(onlineInterviewDetail.Interviewkey))
                    {
                        //Set the interview key
                        onlineInterviewDetail.Interviewkey = new NextNumberDLManager().
                            GetNextNumber(transaction, "OLS", userID);
                    }

                    interviewKey = onlineInterviewDetail.Interviewkey;

                    // Insert online interview details
                    new OnlineInterviewDLManager().InsertOnlineInterview(onlineInterviewDetail, userID, transaction);

                    if (skillDetails != null && skillDetails.Count > 0)
                    {
                        foreach (SkillDetail skill in skillDetails)
                        {
                            new OnlineInterviewDLManager().
                                InsertOnlineInterviewSkill(skill, interviewKey, userID, transaction);
                        }
                    }
                }

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// This method searches the online interview details by passing interview name,
        /// position profile id, author id
        /// </summary>
        /// <param name="onlineInterviewSessionSearchCriteria">
        /// A list of<see cref="OnlineInterviewSessionSearchCriteria"/> that holds the online interview session criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>that holds the page Number
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/>that holds the page Size
        /// </param>
        /// <param name="totalRecords">
        ///  A <see cref="int"/>that holds the total Records
        /// </param>
        /// <param name="sortingKey">
        ///  A <see cref="string"/>that holds the sorting Key 
        /// </param>
        /// <param name="sortByDirection">
        ///  A <see cref="SortType"/>that holds the sortBy Direction Like 'A' or 'D'(Asc/dec)
        /// </param>
        /// <returns>
        ///  A list of <see cref="OnlineInterviewSessionDetail"/>that holds the Online Interview Session Detail
        /// </returns>
        public List<OnlineInterviewSessionDetail> GetOnlineInterviewDetail(OnlineInterviewSessionSearchCriteria onlineInterviewSessionCriteria,
        int pageNumber, int pageSize, out int totalRecords, string sortingKey, SortType sortByDirection)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewDetail(onlineInterviewSessionCriteria,
                pageNumber, pageSize, out totalRecords, sortingKey, sortByDirection);
        }

        /// <summary>
        /// Method that retrieves the list of skills for the given 
        /// interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetSkillByOnlineInterviewKey(string interviewKey)
        {
            return new OnlineInterviewDLManager().
                GetSkillByOnlineInterviewKey(interviewKey);
        }

        /// <summary>
        /// Method that will return the online interview details
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that contains the interview key.
        /// </param>
        /// <returns>
        /// A <see cref="OnlineInterviewSessionDetail"/> that contains the online interview details.
        /// </returns>
        public OnlineInterviewSessionDetail GetOnlineInterviewDetailByKey(string interviewKey)
        {
            return new OnlineInterviewDLManager().
                GetOnlineInterviewDetailByKey(interviewKey);
        }

        /// <summary>
        /// Insert Online Interview Skill details in [ONLINE_INTERVIEW_SKILL] table object
        /// </summary>
        /// <param name="questionDetails">
        /// A <see cref="QuestionDetail"/> that holds question details
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds interview key
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertOnlineInterviewSkillQuestion(List<QuestionDetail> questionDetails,
            string interviewKey, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                if (!Utility.IsNullOrEmpty(questionDetails) && questionDetails.Count>0)
                {
                    foreach (QuestionDetail questionDetail in questionDetails)
                    {
                        new OnlineInterviewDLManager().
                            InsertOnlineInterviewSkillQuestion(questionDetail, 
                            interviewKey, userID, transaction);
                    }
                    // Commit the transaction
                    transaction.Commit();
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the list of interview skill questios for the given 
        /// interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the skill details.
        /// </returns>
        public List<QuestionDetail> GetOnlineInterviewSkillQuestion(string interviewKey, int skillID)
        {
            return new OnlineInterviewDLManager().
               GetOnlineInterviewSkillQuestion(interviewKey, skillID);
        }

        /// <summary>
        /// Method that retrieves the list of interview skill questios for the given 
        /// interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the skill details.
        /// </returns>
        public List<QuestionDetail> GetOnlineInterviewSearchQuestion(string interviewKey, int skillID,
            int pageNumber, int pageSize, out int totalRecords)
        {
            return new OnlineInterviewDLManager().
              GetOnlineInterviewSearchQuestion(interviewKey, skillID, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Insert Online Interview Skill details in [ONLINE_INTERVIEW_SKILL] table object
        /// </summary>
        /// <param name="questionDetails">
        /// A <see cref="QuestionDetail"/> that holds question details
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds interview key
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertOnlineInterviewSkillQuestion(QuestionDetail questionDetail,
            string interviewKey, int userID, IDbTransaction transaction)
        {
            try
            {
                new OnlineInterviewDLManager().
                    InsertOnlineInterviewSkillQuestion(questionDetail, interviewKey,
                    userID, transaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that deletes a skill question.
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <param name="questionRelationID">
        /// A <see cref="int"/> that holds the question relation id.
        /// </param>
        /// <remarks>
        /// This will do a physical deletion.
        /// </remarks>
        public void DeleteOnlineInterviewSkillQuesionByQuestionKey(string interviewKey,
            int skillID, int questionRelationID)
        {
            new OnlineInterviewDLManager().DeleteOnlineInterviewSkillQuesionByQuestionKey(interviewKey,
                skillID, questionRelationID);
        }

        /// <summary>
        /// Method that copies the online interview detail against the interview key
        /// </summary>
        /// <param name="parentInterviewKey">
        /// A <see cref="string"/> that holds the parent interview key.
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="copySkill">
        /// A <see cref="string"/> that holds the key to copy skill or not.
        /// </param>
        /// <param name="copyQuestionSet">
        /// A <see cref="string"/> that holds the key to copy question or not.
        /// </param>
        /// <param name="copyAssessor">
        /// A <see cref="string"/> that holds the key to copy assessor or not.
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <remarks>
        /// This will do a copy interview.
        /// </remarks>
        public void CopyOnlineInterview(string parentInterviewKey,
            string copySkill, string copyQuestionSet, string copyAssessor,
            int userID, out string interviewKey)
        {

            IDbTransaction transaction = new TransactionManager().Transaction;

            interviewKey = string.Empty;
            try
            {
                if (!Utility.IsNullOrEmpty(parentInterviewKey))
                {

                    //Set the interview key
                    interviewKey = new NextNumberDLManager().
                        GetNextNumber(transaction, "OLS", userID);
                    
                    new OnlineInterviewDLManager().CopyOnlineInterview(parentInterviewKey, interviewKey,
                        copySkill, copyQuestionSet, copyAssessor, userID, transaction);
                }

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// This function inserts the online interview schedule information
        /// </summary>
        /// <param name="onlineSchedule">
        /// A <see cref="onlineSchedule"/> that holds the inline schedule information
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertOnlineInterviewSchedule(OnlineCandidateSessionDetail onlineSchedule, int userID)
        {
            new OnlineInterviewDLManager().InsertOnlineInterviewSchedule(onlineSchedule, userID, null);
        }

        public OnlineCandidateSessionDetail GetOnlineInterviewDateTime(string interviewkey)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewDateTime(interviewkey);
        }

        public int GetChatRoomCount(string chatRoomName)
        {
            return new OnlineInterviewDLManager().GetChatRoomCount(chatRoomName);
        }

        public List<OnlineCandidateSessionDetail> GetOnlineInterviews(string interviewKey, string status, string searchText,
            int pageNumber, int pageSize, string sortingKey, SortType sortByDirection, out int totalRecords)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviews(interviewKey, status, searchText,
                pageNumber, pageSize, sortingKey, sortByDirection, out totalRecords);
        }

        /// <summary>
        /// Method that generates external key
        /// </summary>
        /// <returns>That returns the external key</returns>
        public string GenerateChatRoomKey()
        {
            bool isExist = true;
            string key = string.Empty;

            while (isExist)
            {
                key = Utility.RandomString(8);
                isExist = new OnlineInterviewDLManager().IsChatRoomExists(key);
            }
            return key;
        }


        /// <summary>
        /// Method that schedule the candidate
        /// </summary>
        /// <param name="onlineSchedule">
        /// A <see cref="OnlineCandidateSessionDetail"/> that holds the inline schedule information
        /// </param>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the online schedule information
        /// </param>
        /// <param name="skillList">
        /// A <see cref="SkillDetail"/> that holds the selected skill details
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void ScheduleOnlineInterviewCandidate(OnlineCandidateSessionDetail onlineSchedule, 
            InterviewReminderDetail reminderDetail, List<SkillDetail> skillList, 
            List<AssessorDetail> assessorDetails, int userID, out int candidateInterviewID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                candidateInterviewID = 0;

                //Insert interview details
                candidateInterviewID = new OnlineInterviewDLManager().
                    InsertOnlineInterviewSchedule(onlineSchedule, userID, transaction);

                if (!Utility.IsNullOrEmpty(reminderDetail.IntervalID))
                {
                    reminderDetail.CandidateInterviewID = candidateInterviewID;
                    new OnlineInterviewDLManager().
                        InsertOnlineInterviewReminder(reminderDetail, transaction);
                }

                //Insert interview assessor details
                if (!Utility.IsNullOrEmpty(assessorDetails) && assessorDetails.Count > 0)
                {
                    foreach (AssessorDetail assessorDetail in assessorDetails)
                    {
                        new OnlineInterviewDLManager().
                            InsertOnlineInterviewScheduledAssessor(assessorDetail.AssessorId,
                            candidateInterviewID, userID, transaction);
                    }
                }

                //Insert scheudled interview skill
                string skillids = string.Empty;
                if (!Utility.IsNullOrEmpty(skillList) && skillList.Count > 0)
                {
                    foreach (SkillDetail skill in skillList)
                    {
                        new OnlineInterviewDLManager().
                            InsertOnlineInterviewScheduledSkill(candidateInterviewID, skill.SkillID, skill.Weightage,
                            userID, transaction);
                        skillids = skillids + skill.SkillID + ",";
                    }
                }

                //Insert scheudled interview assessor skill
                if (!Utility.IsNullOrEmpty(assessorDetails) && assessorDetails.Count > 0)
                {
                    if (!Utility.IsNullOrEmpty(skillids))
                    {
                        foreach (AssessorDetail assessorDetail in assessorDetails)
                        {
                            new OnlineInterviewDLManager().
                                InsertOnlineInterviewScheduledAssessorSkill(candidateInterviewID, 
                                assessorDetail.AssessorId, skillids.ToString().Trim().TrimEnd(','),
                                userID, transaction);
                        }
                    }
                }

                //Get the position profile id and assocate the candidate
                int positionProfileID = new PositionProfileDLManager().
                    GetPositionProfileID(candidateInterviewID.ToString(), "O");

                if (positionProfileID > 0)
                {
                    PositionProfileCandidate positionProfileCandidate = new PositionProfileCandidate();
                    positionProfileCandidate.PositionProfileID = positionProfileID;
                    positionProfileCandidate.CandidateID = onlineSchedule.CandidateID;
                    positionProfileCandidate.CandidateOnlineInterviewID = candidateInterviewID;
                    positionProfileCandidate.PickedBy = null;
                    positionProfileCandidate.PickedDate = null;
                    positionProfileCandidate.SchelduleDate = DateTime.Now;
                    positionProfileCandidate.ResumeScore = null;
                    positionProfileCandidate.SourceFrom = "N";
                    positionProfileCandidate.CreatedBy = userID;
                    new TestDLManager().InsertPositionProfileCandidate(positionProfileCandidate,
                        "O", transaction);
                }

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        /// <summary>
        /// Method that gets the list of assessor details against the candidate interview id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview id.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the assessor details.
        /// </returns>
        public List<AssessorDetail> GetOnlineInterviewScheduledAssessorByCandidateID(
            int candidateInterviewID)
        {
            return new OnlineInterviewDLManager().
                GetOnlineInterviewScheduledAssessorByCandidateID(candidateInterviewID);
        }

        /// <summary>
        /// Method that updates the slot key and schedule 
        /// the candidate which is approved by the client
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the intervi.
        /// </param>
        /// <param name="requestSlotID">
        /// A <see cref="int"/> that holds the requested ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public string ApproveSlotIDAndSchedule(int candidateInterviewID,
            int requestSlotID, int userID)
        {
            return new OnlineInterviewDLManager().ApproveSlotIDAndSchedule(candidateInterviewID,
                requestSlotID, userID);
        }

         /// <summary>
        /// Method that unschedule the online interview candidate against the candidate interview id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        public void UnScheduleOlineInterviewCandidate(int candidateInterviewID)
        {
            new OnlineInterviewDLManager().
                UnScheduleOlineInterviewCandidate(candidateInterviewID);
        }
        
        /// <summary>
        /// Method that get the scheduled detail
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <returns>
        /// A list of <see cref="OnlineCandidateSessionDetail"/> that holds the candidate session details.
        /// </returns>
        public OnlineCandidateSessionDetail GetCandidateScheduledDetail(int candidateInterviewID)
        {
            return new OnlineInterviewDLManager().
                GetCandidateScheduledDetail(candidateInterviewID);
        }

        /// <summary>
        /// Method that schedule the candidate
        /// </summary>
        /// <param name="onlineSchedule">
        /// A <see cref="OnlineCandidateSessionDetail"/> that holds the inline schedule information
        /// </param>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the inline schedule information
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void ReScheduleOnlineInterviewCandidate(OnlineCandidateSessionDetail onlineSchedule, 
            InterviewReminderDetail reminderDetail, List<SkillDetail> skillDetail, 
            List<AssessorDetail> assessorDetails, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                //Update interview schedule
                new OnlineInterviewDLManager().
                     UpdateOnlineInterviewSchedule(onlineSchedule, userID, transaction);

                //Update interview reminder time
                if (!Utility.IsNullOrEmpty(reminderDetail.IntervalID))
                {
                    reminderDetail.CandidateInterviewID = onlineSchedule.CandidateInterviewID;
                    new OnlineInterviewDLManager().
                        InsertOnlineInterviewReminder(reminderDetail, transaction);
                }

                if (!Utility.IsNullOrEmpty(assessorDetails) && assessorDetails.Count > 0)
                {
                    string assessorIds = string.Empty;
                    foreach (AssessorDetail assessorDetail in assessorDetails)
                    {
                        new OnlineInterviewDLManager().
                            InsertOnlineInterviewScheduledAssessor(assessorDetail.AssessorId,
                            onlineSchedule.CandidateInterviewID, userID, transaction);
                        assessorIds = assessorIds + assessorDetail.AssessorId + ",";
                    }

                    if (!Utility.IsNullOrEmpty(assessorIds))
                    {
                        //Delete the assessor ids
                        assessorIds = assessorIds.ToString().Trim().TrimEnd(',');
                        new OnlineInterviewDLManager().DeleteOnlineInterviewScheduledAssessor(onlineSchedule.CandidateInterviewID,
                            assessorIds, transaction);
                    }

                    //Delete from scheduled skill

                    string skillids = string.Empty;
                    //Insert scheudled interview skill
                    if (!Utility.IsNullOrEmpty(skillDetail) && skillDetail.Count > 0)
                    {
                        foreach (SkillDetail skill in skillDetail)
                        {
                            new OnlineInterviewDLManager().
                                InsertOnlineInterviewScheduledSkill(onlineSchedule.CandidateInterviewID, skill.SkillID, skill.Weightage,
                                userID, transaction);
                            skillids = skillids + skill.SkillID + ",";
                        }
                    }

                    //Delete inteview assessor skill
                    if(onlineSchedule.CandidateInterviewID!=0)
                    {
                        new OnlineInterviewDLManager().DeleteOnlineInterviewScheduledAssessorSkill(onlineSchedule.CandidateInterviewID,
                            transaction);
                    }

                    //Insert scheudled interview assessor skill
                    if (!Utility.IsNullOrEmpty(assessorDetails) && assessorDetails.Count > 0)
                    {
                        if (!Utility.IsNullOrEmpty(skillids))
                        {
                            foreach (AssessorDetail assessorDetail in assessorDetails)
                            {
                                new OnlineInterviewDLManager().
                                    InsertOnlineInterviewScheduledAssessorSkill(onlineSchedule.CandidateInterviewID,
                                    assessorDetail.AssessorId, skillids.ToString().Trim().TrimEnd(','),
                                    userID, transaction);
                            }
                        }
                    }
                }

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that gets the list of list requested candidated slots
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the slot key.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor time slot details.
        /// </returns>
        public AssessorTimeSlotDetail GetCandidateSlotDetailByCandidateID(int candidateInterviewID)
        {
            return new OnlineInterviewDLManager().GetCandidateSlotDetailByCandidateID(candidateInterviewID);
        }

        /// <summary>
        /// Method that retrieves the list of interview skill questios for the given 
        /// interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the skill details.
        /// </returns>
        public List<QuestionDetail> GetOnlineInterviewConductionSummary(string interviewKey, int candidateInterviewID,
            int assessorID, int skillID)
        {
            return new OnlineInterviewDLManager().
               GetOnlineInterviewConductionSummary(interviewKey, candidateInterviewID,
               assessorID, skillID);
        }

        /// <summary>
        /// Method that inserts the assessor ratings and 
        /// comments against the questions 
        /// </summary>
        /// <param name="questionDetails">
        /// A <see cref="QuestionDetail"/> that holds the question details
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id
        /// </param>
        public void SaveOnlineInterviewQuestionRatingComments(QuestionDetail questionDetails,
           int candidateInterviewID, int assessorID, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                new OnlineInterviewDLManager().SaveOnlineInterviewQuestionRatingComments(questionDetails,
                    candidateInterviewID,  assessorID, userID, transaction);
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that inserts the assessor ratings and 
        /// comments against the skill 
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id
        /// </param>
        /// <param name="rating">
        /// A <see cref="int"/> that holds the rating
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id
        /// </param>
        public void SaveOnlineInterviewSkillRating(int candidateInterviewID, int skillID,
            int assessorID, int rating, string comments)
        {
            IDbTransaction skillTransaction = new TransactionManager().Transaction;

            try
            {
                new OnlineInterviewDLManager().SaveOnlineInterviewSkillRating(candidateInterviewID,
                    skillID, assessorID, rating, comments, skillTransaction);
                skillTransaction.Commit();
            }
            catch (Exception exp)
            {
                skillTransaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that gets the online interview 
        /// candidate and position profile details
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview id
        /// </param>
        public AssessmentSummary GetOnlineInterviewAssessmentCandidate(int candidateInterviewID)
        {
            return new OnlineInterviewDLManager().
                GetOnlineInterviewAssessmentCandidate(candidateInterviewID);
        }

        /// <summary>
        /// Method that retrieves the list of skills, and its rating for the given 
        /// interview key, assessor id, candidate interview id
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetOnlineInterviewKeySkillRating(string interviewKey,
            int candidateInterviewID, int assessorID)
        {
            return new OnlineInterviewDLManager().
                GetOnlineInterviewKeySkillRating(interviewKey, 
                candidateInterviewID, assessorID);
        }

        /// <summary>
        /// Method that gets the other assessor rating and comments 
        /// against this candidate interview id and question id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="questionID">
        /// A <see cref="int"/> that holds the question id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the assessor details.
        /// </returns>
        public List<AssessorDetail> GetOnlineInterviewOtherAssessorRating(
            int candidateInterviewID, int questionID, int assessorID)
        {
            return new OnlineInterviewDLManager().
               GetOnlineInterviewOtherAssessorRating(candidateInterviewID, questionID, assessorID);
        }

        /// <summary>
        /// Method that retrieves the list of interview skills against the candidate id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetOnlineInterviewScheduledSkills(int candidateInterviewID)
        {
            return new OnlineInterviewDLManager().
               GetOnlineInterviewScheduledSkills(candidateInterviewID);
        }

        /// <summary>
        /// Method that gets the rating and comments
        /// against interview id,skill id,assessor id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the candidate session details.
        /// </returns>
        public AssessorDetail GetOnlineInterviewSKillRating(int candidateInterviewID,
            int assessorID, int skillID)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewSKillRating(candidateInterviewID,
                assessorID, skillID);
        }

        /// <summary>
        /// Method that retrieves the list of interview skills against the candidate id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetOnlineInterviewScheduledAssessorSkills(int candidateInterviewID,
            int assessorID)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewScheduledAssessorSkills(candidateInterviewID,
                assessorID);
        }

        
        /// <summary>
        /// Inserts Online Interview details in [ONLINE_INTERVIEW] table object
        /// </summary>
        /// <param name="skillRating">
        /// A <see cref="SkillDetail"/> that holds the skill rating.
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="comments">
        /// A <see cref="string"/> that holds the assessor overall comments.
        /// </param>
        public void SaveOnlineInterviewSkillRating(List<SkillDetail> skillRating, 
            int candidateInterviewID, int assessorID, string comments)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            
            try
            {
                if (skillRating != null && skillRating.Count > 0)
                {
                    foreach (SkillDetail skill in skillRating)
                    {
                        new OnlineInterviewDLManager().SaveOnlineInterviewSkillRating(candidateInterviewID,
                            skill.SkillID, assessorID, skill.SkillRating, skill.SkillComments, transaction);
                    }

                    new OnlineInterviewDLManager().UpdateOnlineInterviewScheduledStatus(candidateInterviewID,
                        assessorID, comments, transaction);
                    
                    // Commit the transaction
                    transaction.Commit();
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the assessment report
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetOnlineInterviewAssessorSkillReport(int candidateInterviewID,
            int assessorID)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewAssessorSkillReport(candidateInterviewID, assessorID);
        }

        /// <summary>
        /// Method that retrieves interview assessor against the interview key
        /// </summary>
        /// <param name="onlineInterviewKey">
        /// A <see cref="string"/> that holds the interview key
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the assessor details.
        /// </returns>
        public List<AssessorDetail> GetOnlineInterviewAssessorByInterviewKey(string onlineInterviewKey)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewAssessorByInterviewKey(onlineInterviewKey);
        }

        /// <summary>
        /// This method communicates with the questiondetail DL Manager
        /// to get the questions for the automated test
        /// </summary>
        /// <param name="testSearchCriteria">search criteria to get the questions</param>
        /// <param name="TotalRecords">Total number of records picked 
        /// (Note:- this should be 'out' parameter)</param>
        /// <returns>List of question details picked from the database</returns>
        public List<QuestionDetail> GetInterviewQuestionDetailsforAutomatedTest(TestSearchCriteria testSearchCriteria,
           int questionAuthor, int tenantID, out int TotalRecords)
        {
            return new QuestionDLManager().
                    GetAutomatedInterviewQuestions(testSearchCriteria, questionAuthor, tenantID, out TotalRecords);
        }

         /// <summary>
        /// Method that retrieves the candidate online interview detail for the given
        /// candidate interview id.
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the candidate
        /// interview detail.
        /// </returns>
        public CandidateInterviewSessionDetail GetCandidateOnlineInterviewDetail
            (int candidateInterviewID)
        {
            return new OnlineInterviewDLManager().GetCandidateOnlineInterviewDetail(candidateInterviewID);
        }

         /// <summary>
        /// Method that retrieves the assessor overall comments
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the 
        /// assessor comments
        /// </returns>
        public string GetAssessorOverAllComments
            (int candidateInterviewID, int assessorID)
        {
            return new OnlineInterviewDLManager().GetAssessorOverAllComments
            (candidateInterviewID, assessorID);
        }

        /// <summary>
        /// Method that gets the list of associated candidates against the interview id
        /// </summary>
        /// <param name="interviewkey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </returns>
        public List<CandidateDetail> GetOnlineInterviewCandidates(
            string interviewkey)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewCandidates(interviewkey);
        }

        /// <summary>
        /// Method that retreives the recruiter detail.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterDetail> GetOnlineInterviewRecruiterSkill(int tenantID, string interviewKey,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewRecruiterSkill(tenantID, interviewKey,
            pageNumber, pageSize, sortField, sordOrder, out totalRecords);
        }

        /// <summary>
        /// Method that retreives the recruiter 
        /// detail who workin similar clients.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile id.
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterDetail> GetOnlineInterviewRecruiterClient(int tenantID, string interviewKey,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewRecruiterClient(tenantID, interviewKey,
            pageNumber, pageSize, sortField, sordOrder, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the assessor overall comments
        /// </summary>
        /// <param name="interviewDetail">
        /// A <see cref="OnlineCandidateSessionDetail"/> that holds the interview detail
        /// </param>
        /// <param name="interviewDate">
        /// A <see cref="DateTime"/> that holds the inte.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the 
        /// assessor comments
        /// </returns>
        public bool GetScheduleAvailableTimes(OnlineCandidateSessionDetail interviewDetail)
        {
            return new OnlineInterviewDLManager().GetScheduleAvailableTimes(interviewDetail);
        }

        /// <summary>
        /// Methot that inserts the list of selected time 
        /// slot against the date(referred with assessor availability date).
        /// </summary>
        /// <param name="timeSlots">
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        /// <param name="slotExpiryInMinutes">
        /// A <see cref="int"/> that holds slot expiry time in minutes.
        /// </param>
        /// <param name="skillList">
        /// A <see cref="SkillDetail"/> that holds skill detail.
        /// </param>
        /// <param name="onlineSchedule">
        /// A <see cref="OnlineCandidateSessionDetail"/> that holds candidate schedule detail.
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="String"/> that holds the interview key.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="Int"/> that holds the candidate ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="Int"/> that holds the user ID.
        /// </param>
        /// <param name="candidateInterviewIDGen">
        /// A <see cref="Int"/> that holds the gen id.
        /// </param>
        public void SaveCandidateRequestDetail(List<AssessorTimeSlotDetail> timeSlots, 
            int slotExpiryInMinutes, List<SkillDetail> skillList,
            OnlineCandidateSessionDetail onlineSchedule, string interviewKey,
            int candidateID, int userID, out int candidateInterviewIDGen)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            string retval = string.Empty;
            int reguestGenID = 0;

            try
            {
                int candidateInterviewID = 0;
                string slotKey = string.Empty;
                if (!Utility.IsNullOrEmpty(interviewKey))
                {
                    if (onlineSchedule != null && onlineSchedule.CandidateInterviewID == 0)
                    {
                        //Generate key
                        slotKey = GenerateKey();
                        candidateInterviewID =
                           new OnlineInterviewDLManager().InsertOnlineInterviewSchedule(onlineSchedule,
                           userID, transaction);
                    }
                    else
                    {
                        new OnlineInterviewDLManager().UpdateOnlineInterviewReminder(
                            onlineSchedule.CandidateInterviewID,
                            onlineSchedule.ReminderType,userID, transaction);
                        candidateInterviewID = onlineSchedule.CandidateInterviewID;
                    }

                    reguestGenID = new OnlineInterviewDLManager().
                           InsertOnlineCandidateRequested(candidateInterviewID, slotKey, slotExpiryInMinutes, userID, transaction);
                }

                //Insert scheudled interview skill
                if (candidateInterviewID != 0 &&
                    !Utility.IsNullOrEmpty(skillList) && skillList.Count > 0)
                {
                    foreach (SkillDetail skill in skillList)
                    {
                        new OnlineInterviewDLManager().
                            InsertOnlineInterviewScheduledSkill(candidateInterviewID, skill.SkillID, skill.Weightage,
                            userID, transaction);
                    }
                }

                candidateInterviewIDGen = candidateInterviewID;

                //Delete existing requested slots
                if (onlineSchedule.CandidateInterviewID != 0)
                {
                    new OnlineInterviewDLManager().DeleteOnlineInterviewCandidateRequested(
                        onlineSchedule.CandidateInterviewID, transaction);
                }

                int requestedSlotID = 0;

                // Loop through the time slots and save.
                foreach (AssessorTimeSlotDetail timeSlot in timeSlots)
                {
                    //Insert the requested slots (from & to time)
                    requestedSlotID = new OnlineInterviewDLManager().InsertOnlineCandidateRequestedSlots(reguestGenID,
                        timeSlot, userID, transaction);

                    //Insert the selected/requestedassessor
                    if (timeSlot.assessorDetail != null &&
                        timeSlot.assessorDetail.Count > 0)
                    {
                        foreach (AssessorDetail assessorDetail in timeSlot.assessorDetail)
                        {
                            new OnlineInterviewDLManager().InsertOnlineCandidateRequestedSlotsAssessor(requestedSlotID,
                                assessorDetail.AssessorId, userID, transaction);
                        }
                    }
                }

                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

         /// <summary>
        /// Method that retrieves the list of online interview reminder sender list 
        /// for the given date/time and relative time differences. Mails needs 
        /// to be sent for this list.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<InterviewReminderDetail> GetOnlineInterviewReminderSenderList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            return new OnlineInterviewDLManager().GetOnlineInterviewReminderSenderList
            (currentDateTime, relativeTimeDifference);
        }

        /// <summary>
        /// Method that will update the reminder sent status for the given 
        /// candidate, attempt number and interval ID.
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        /// <param name="intervalID">
        /// A <see cref="string"/> that holds the interval ID.
        /// </param>
        /// <param name="reminderSent">
        /// A <see cref="bool"/> that holds the reminder sent status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateOnlineInterviewReminderStatus(int candidateInterviewID,
            string intervalID, bool reminderSent, int userID)
        {
            new OnlineInterviewDLManager().UpdateOnlineInterviewReminderStatus
                (candidateInterviewID, intervalID, reminderSent, userID);
        }

        /// <summary>
        /// Delete the online interview skill details in
        /// [ONLINE_INTERVIEW_SKILL] table object
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds interview key
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        public void DeleteOnlineInterviewSkill(string interviewKey, int skillID)
        {
            new OnlineInterviewDLManager().DeleteOnlineInterviewSkill
                (interviewKey, skillID);
        }

        /// <summary>
        /// Method that delete the scheduled skill 
        /// against the candidate id, skill id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        public void DeleteOnlineInterviewScheduledSkill(int candidateInterviewID, int skillID)
        {
            new OnlineInterviewDLManager().DeleteOnlineInterviewScheduledSkill(candidateInterviewID, skillID);
        }

         /// <summary>
        /// Method that retrieves the list of expired slot request list 
        /// for the given date/time and relative time differences.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<CandidateDetail> GetExpiredSlotRequestList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            return new OnlineInterviewDLManager().GetExpiredSlotRequestList
            (currentDateTime, relativeTimeDifference);
        }

        /// <summary>
        /// Method that will update the online interview 
        /// session status for the given candidate interview id 
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        public void UpdateOnlineInterviewSessionStatus(int candidateInterviewID)
        {
            new OnlineInterviewDLManager().
                UpdateOnlineInterviewSessionStatus(candidateInterviewID);
        }

        /// <summary>
        /// Method that retrieves the list of expired online interview list 
        /// for the given date/time and relative time differences.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<CandidateDetail> GetExpiredOnlineInterviewList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
           return new OnlineInterviewDLManager().
                GetExpiredOnlineInterviewList(currentDateTime, relativeTimeDifference);
        }

         /// <summary>
        /// Method that retrieves the assessor overall comments
        /// </summary>
        /// <param name="interviewDetail">
        /// A <see cref="OnlineCandidateSessionDetail"/> that holds the interview detail
        /// </param>
        /// <param name="interviewDate">
        /// A <see cref="DateTime"/> that holds the inte.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the 
        /// assessor comments
        /// </returns>
        public bool IsAssessorAvailableForScheduledTime(int assessorID, DateTime interviewDate, int timeSlotIDFrom, int timeSlotIDTo)
        {
            return new OnlineInterviewDLManager().IsAssessorAvailableForScheduledTime(assessorID, interviewDate, timeSlotIDFrom, timeSlotIDTo);
        }

        #endregion

        #region Private Method

        /// <summary>
        /// Method that generates external key
        /// </summary>
        /// <returns>That returns the external key</returns>
        private string GenerateKey()
        {
            bool isExist = true;
            string key = string.Empty;

            while (isExist)
            {
                key = Utility.RandomString(10);
                isExist = new OnlineInterviewDLManager().IsSlotyKeyExists(key);
            }
            return key;
        }

        #endregion Private Method
    }
}
