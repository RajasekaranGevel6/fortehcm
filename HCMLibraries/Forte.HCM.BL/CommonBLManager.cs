﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CommonBLManager.cs
// File that represents the business layer for the common operations.
// This includes functionalities for retrieving the category, subjects, 
// contacts, users, user option, etc. This will talk to the data layer 
// for performing these operations

#endregion

#region Directives
using System;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using System.Data;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the common operations.
    /// This includes functionalities for retrieving the category, subjects, 
    /// contacts, users, user option, etc. This will talk to the data layer 
    /// for performing these operations.
    /// </summary>
    public class CommonBLManager
    {
        #region Public Method
        /// <summary>
        /// Default constructor for CommonBLManager.
        /// </summary>
        public CommonBLManager()
        {
        }
        /// <summary>
        /// Method that will help to record modified or not.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="string"/> that holds the entity Type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        /// <param name="modifiedDate">
        /// A <see cref="DateTime"/> that holds the modified Date.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds Record Modified or not.
        /// </returns>
        public bool IsRecordModified(string entityType, string entityID,
            DateTime modifiedDate)
        {
            return new CommonDLManager().IsRecordModified(entityType, entityID,
                modifiedDate);
        }


        /// <summary>
        /// Method that will help to interview question record modified or not.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="string"/> that holds the entity Type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds the entity ID.
        /// </param>
        /// <param name="modifiedDate">
        /// A <see cref="DateTime"/> that holds the modified Date.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds Record Modified or not.
        /// </returns>
        public bool IsInterviewRecordModified(string entityType, string entityID,
            DateTime modifiedDate)
        {
            return new CommonDLManager().IsInterviewRecordModified(entityType, entityID,
                modifiedDate);
        }

        /// <summary>
        /// This method helps to retrieve the disclaimer text from DISCLAIMER table based on the 
        /// Disclaimer Type
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds Disclaimer Message.
        /// </returns>
        public string GetDisclaimerMessage(string disclaimerType)
        {
            return new CommonDLManager().GetDisclaimerMessage(disclaimerType);
        }

        /// <summary>
        /// This method helps to retrieve subjects by passing the category ids
        /// </summary>
        /// <param name="CategoryID">
        /// A <see cref="string"/> that holds the category id.
        /// </param>
        /// <returns>
        /// A list for<see cref="Subject"/> that holds the List of Subjects.
        /// </returns>
        public List<Subject> GetSubjects(int CategoryID)
        {
            return (new CommonDLManager().GetSubjects(CategoryID));
        }

        /// <summary>
        /// Helps to get all subjects against the category ids
        /// </summary>
        /// <param name="categories">
        /// A list for<see cref="Category"/>Holds the category id with comma seperator
        /// </param>
        /// <returns>
        /// A list for<see cref="Subject"/> that holds the List of Subjects.
        /// </returns>
        public List<Subject> GetSubjects(List<Category> categories)
        {
            return (new CommonDLManager().GetSubjects(categories));
        }

        /// <summary>
        /// This method helps to retrieve subjects by passing the subject ids
        /// </summary>
        /// <param name="subjectids">
        /// A <see cref="string"/>that holds the subject id.
        /// </param>
        /// <returns>
        /// A list for<see cref="Subject"/> that holds the List of Subjects.
        /// </returns>
        public List<Subject> GetSubjects(string subjectIds)
        {
            return (new CommonDLManager().GetSubjects(subjectIds));
        }

        /// <summary>
        /// Get the categories list according to the prefix text. 
        /// </summary>
        /// <param name="categoryPrefix">
        /// A <see cref="string"/> that contains the prefix text.
        /// </param>
        /// <returns>
        /// A list of <see cref="string"/> that contains the category id and name.
        /// </returns>
        public string[] GetCategoryList(string categoryPrefix,int userID)
        {
            return new CommonDLManager().GetCategoryList(categoryPrefix,userID);
        }

        /// <summary>
        /// Gets the user names list according to the prefix text.
        /// </summary>
        /// <param name="UserNamePrefix">
        /// A <see cref="string"/> that contains the prefix text.
        /// </param>
        /// <returns>
        /// A list of <see cref="string"/> that contains user firstname lastname and id.
        /// </returns>
        public string[] GetUserNameList(string UserNamePrefix)
        {
            return new CommonDLManager().GetUserNameList(UserNamePrefix);
        }

        /// <summary>
        /// Method that will check the user firstname, lastname and user id exists
        /// in user table.
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> that contains the userfirstname and userlastname.
        /// </param>
        /// <param name="UserId">
        /// A <see cref="int"/> that contains the user id
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> represents whether the passed paramters are persent
        /// in database or not.
        /// </returns>
        public bool IsValidUserNameWithId(string UserName, int UserId)
        {
            return new CommonDLManager().IsValidUserNameWithId(UserName, UserId);
        }

        /// <summary>
        /// Method that will check if both category id, and category name exists in the 
        /// category repository.
        /// </summary>
        /// <param name="categoryId">
        /// An <see cref="int"/> that contains the category id.
        /// </param>
        /// <param name="categoryName">
        /// A <see cref="string"/> that contains the category name.
        /// </param>
        /// <returns></returns>
        public bool IsValidCategory(int categoryId, string categoryName)
        {
            return new CommonDLManager().IsValidCategory(categoryId, categoryName);
        }

        /// <summary>
        /// Method that retrieves the list of users for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<UserDetail> GetUsers(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string orderBy, SortType orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new CommonDLManager().GetUsers(userNameLike, firstNameLike,
                middleNameLike, lastNameLike, orderBy, orderByDirection,
                pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of users for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="roleCodes">
        /// A <see cref="string"/> that holds the role Codes, that indicates the 
        /// list of user roles.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<UserDetail> GetUsers(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string orderBy, SortType orderByDirection, int pageNumber,
            int pageSize, string roleCodes, out int totalRecords)
        {
            //string RoleCodes = null;

            //switch (roleCodes)
            //{
            //    case "QA":
            //        RoleCodes = (byte)UserRole.Admin + "," + (byte)UserRole.AdminOTMTS + "," + (byte)UserRole.ContentValidator + "," + (byte)UserRole.ContentAuthor;
            //        break;
            //    case "TA":
            //        RoleCodes = (byte)UserRole.Admin + "," + (byte)UserRole.AdminOTMTS + "," + (byte)UserRole.TestAuthor + "," + (byte)UserRole.DeliveryManager;
            //        break;
            //    case "TS":
            //        RoleCodes = (byte)UserRole.Admin + "," + (byte)UserRole.AdminOTMTS + "," + (byte)UserRole.Recruiter+ "," + (byte)UserRole.DeliveryManager;
            //        break;
            //    case "TC":
            //        RoleCodes = (byte)UserRole.Admin + "," + (byte)UserRole.AdminOTMTS + "," + (byte)UserRole.Recruiter+ "," + (byte)UserRole.DeliveryManager;
            //        break;
            //}

            return new CommonDLManager().GetUsers(userNameLike, firstNameLike,
                middleNameLike, lastNameLike, orderBy, orderByDirection,
                pageNumber, pageSize, roleCodes, out totalRecords);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userNameLike"></param>
        /// <param name="firstNameLike"></param>
        /// <param name="userType"></param>
        /// <param name="lastNameLike"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderByDirection"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="roleCodes"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public List<UserDetail> GetUserInformation(string userNameLike,
            string firstNameLike, string userType, string lastNameLike,
            string orderBy, SortType orderByDirection, int pageNumber,
            int pageSize, string roleCodes, out int totalRecords)
        {
            return new CommonDLManager().GetUserInformation(userNameLike, firstNameLike,
                userType, lastNameLike, orderBy, orderByDirection,
                pageNumber, pageSize, roleCodes, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of customers for the given search
        /// parameters.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name keyword.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name keyword.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name keyword.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name keyword.
        /// </param>
        /// <param name="userType">
        /// A <see cref="string"/> that holds the user type. 
        /// (A - All, S - Subscription, I - Internal).
        /// </param>
        /// <param name="activatedType">
        /// A <see cref="string"/> that holds the user type.
        /// (B - Both, A - Activated, N - Not activated).
        /// </param>
        /// <param name="orderByColumn">
        /// A <see cref="string"/> that holds the order by column
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that holds the order by direction.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        public List<UserDetail> GetCustomers(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string userType, string activatedType, string orderByColumn,
            SortType orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new CommonDLManager().GetCustomers(userNameLike,
                firstNameLike, middleNameLike, lastNameLike,
                userType, activatedType, orderByColumn,
                orderByDirection, pageNumber,
                pageSize, out totalRecords);
        }
        /// <summary>
        /// Method that retrieves the list of users for the given search
        /// parameters.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name keyword.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name keyword.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name keyword.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name keyword.
        /// </param>
        /// <param name="userType">
        /// A <see cref="string"/> that holds the user type. 
        /// (A - All, S - Subscription, I - Internal).
        /// </param>
        /// <param name="activatedType">
        /// A <see cref="string"/> that holds the user type.
        /// (B - Both, A - Activated, N - Not activated).
        /// </param>
        /// <param name="orderByColumn">
        /// A <see cref="string"/> that holds the order by column
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that holds the order by direction.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        public DataTable GetUsers(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string userType, string activatedType, string orderByColumn,
            SortType orderByDirection, int pageNumber,
            int pageSize)
        {
            return new CommonDLManager().GetUsers(userNameLike,
                firstNameLike, middleNameLike, lastNameLike,
                userType, activatedType, orderByColumn,
                orderByDirection, pageNumber,
                pageSize);
        }

        /// <summary>
        /// Method that retrieves the list of candidates for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first Name .
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last Name.
        /// </param>
        /// <param name="emailLike">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the candidate 
        /// details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<UserDetail> GetCandidates(string userNameLike, string firstNameLike, string lastNameLike,
            string emailLike, string orderBy, SortType orderByDirection,
            int pageNumber, int pageSize, out int totalRecords, int tenantID)
        {
            return new CommonDLManager().GetCandidates(userNameLike, firstNameLike, lastNameLike, emailLike,
                orderBy, orderByDirection, pageNumber, pageSize,
                out totalRecords, tenantID);
        }

        /// <summary>
        /// Method that retrieves the list of candidates for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first Name .
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last Name.
        /// </param>
        /// <param name="emailLike">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the session status.
        /// </param>
        /// <param name="pickedBy">
        /// A <see cref="int"/> that holds the picked by user. This will be
        /// zero if 'show my candidate' checkbox is unchecked in the use
        /// interface.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the candidate 
        /// details.
        /// </returns>
        public List<UserDetail> GetPositionProfileCandidates
            (string userNameLike, string firstNameLike, string lastNameLike, 
            string emailLike, int tenantID, int positionProfileID, string status, 
            int pickedBy, string orderBy, SortType orderByDirection, int pageNumber, 
            int pageSize, out int totalRecords)
        {
            return new CommonDLManager().GetPositionProfileCandidates
                (userNameLike, firstNameLike, lastNameLike, emailLike, 
                tenantID, positionProfileID, status, pickedBy, orderBy, 
                orderByDirection, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the user detail for the given user ID.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user ID.
        /// </returns>
        public UserDetail GetUserDetail(int userID)
        {
            return new CommonDLManager().GetUserDetail(userID);
        }


        /// <summary>
        /// Method to get the list of categories and subjects  from the database
        /// </summary>
        /// <param name="pagenumber">
        /// A<see cref="int"/>that holds the page number of the page
        /// </param>
        /// <param name="categoryId">
        /// A<see cref="string"/>that holds the category ID
        /// </param>
        /// <param name="categoryName">
        /// A<see cref="string"/>that holds the category name
        /// </param>
        /// <param name="subjectID">
        /// A<see cref="string"/>that holds the subject ID
        /// </param>
        /// <param name="subjectName">
        /// A<see cref="string"/>that holds the subject name
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size of the page
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total records of categories 
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the expression to sort
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="SortType"/>that holds the sort by direction.
        /// </param>
        /// <param name="keyWord">
        /// A<see cref="string"/>that holds the keyWord.
        /// </param>
        /// <returns>
        /// A List for<see cref="Subject"/>that holds the List of categories and Subjects details 
        /// </returns>
        public List<Subject> GetCategorySubjects(int pagenumber, int tenantID,
            string categoryId, string categoryName, string subjectID,
            string subjectName, int pageSize, out int totalRecords,
            string orderBy, SortType orderByDirection, string keyWord)
        {
            return new CommonDLManager().GetCategorySubjects(pagenumber, tenantID,
                categoryId, categoryName, subjectID, subjectName, pageSize,
                out totalRecords, orderBy, orderByDirection, keyWord);
        }

        /// <summary>
        /// Method to get the list of categories from the Data Access Layer
        /// </summary>
        /// <param name="pagenumber">
        /// A <see cref="int"/>that holds the page number of the page
        /// </param>
        /// <param name="categoryId">
        /// A <see cref="string"/>that holds the category ID
        /// </param>
        /// <param name="categoryName">
        /// A <see cref="string"/>that holds the category name
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size of the page
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/>that holds the total records of categories 
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/>that holds the expression to sort
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/>that holds the sort type.
        /// </param>
        /// <returns>
        /// A list for<see cref="Category"/>that holds the List of categories
        /// </returns>
        public List<Category> GetCategorySubjects(int tenantId, int pagenumber, string categoryId, string categoryName,
            int pageSize, out int totalRecords, string orderBy, SortType orderByDirection)
        {
            return new CommonDLManager().GetCategorySubjects(tenantId, pagenumber, categoryId, categoryName,
                pageSize, out totalRecords, orderBy, orderByDirection);

        }

        /// <summary>
        /// Method to get the list of categories from the database based on search criteria.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="questionRelationId">
        /// A <see cref="int"/>that holds the question Realtion Id
        /// </param>
        /// <returns>
        /// A List for <see cref="Subject"/>that holds the List of Subjects
        /// </returns>
        public List<Subject> GetCategorySubjects(string questionKey, int questionRelationId)
        {
            return new CommonDLManager().GetCategorySubjects(questionKey, questionRelationId);
        }

        /// <summary>
        /// Method that retrieves the list of position profiles for the given
        /// search criteria.
        /// </summary>
        /// <param name="searchCriteria">
        /// A <see cref="ClientRequestSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <param name="pagenumber">
        /// A <see cref="int"/>that holds the page number of the page
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size of the page
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/>that holds the total records.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/>that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that holds the sort order, ascending
        /// or descending.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the logged in user ID
        /// </param>
        /// <returns>
        /// A list for <see cref="ClientRequestDetail"/> that holds the 
        /// position profiles.
        /// </returns>
        public List<ClientRequestDetail> GetClientRequestInformation
            (ClientRequestSearchCriteria searchCriteria, int pagenumber, int pageSize,
            out int totalRecords, string orderBy, SortType orderByDirection, int UserId)
        {
            return new CommonDLManager().GetClientRequestInformation
                (searchCriteria, pagenumber, pageSize, out totalRecords, 
                orderBy, orderByDirection, UserId);
        }

        /// <summary>
        /// Method that retrieves the list of users for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<UserDetail> GetUsersAndCandidates(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string orderBy, SortType orderByDirection, int pageNumber,
            int pageSize,int tenantID, out int totalRecords)
        {
            return new CommonDLManager().GetUsersAndCandidates(userNameLike, firstNameLike,
                middleNameLike, lastNameLike, orderBy, orderByDirection,
                pageNumber, pageSize, tenantID, out totalRecords);
        }

        /// <summary>
        /// Represets the method to get the category id for the given name
        /// </summary>
        /// <param name="categoryName">
        /// A<see cref="string"/>that holds the category name
        /// </param>
        public int GetCategoryID(string categoryName)
        {
            return new CommonDLManager().GetCategoryID(categoryName);
        }


        /// <summary>
        /// Gets the tenant users.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="middleName">Name of the middle.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="sortField">The sort field.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public List<UserDetail> GetTenantUsers(string userName, string firstName, string middleName, string lastName,
            string sortField, SortType sortOrder, int pageNumber, int pageSize, out int totalRecords, int tenantID, int userID)
        {
            return new CommonDLManager().GetTenantUsers(userName, firstName, middleName, lastName, sortField,
                sortOrder, pageNumber, pageSize, out totalRecords, tenantID, userID);
        }

        /// <summary>
        /// Gets the tenant users for position profile forms.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="middleName">Name of the middle.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="sortField">The sort field.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public List<UserDetail> GetPositionProfileFormTenantUsers(string userName, string firstName, string middleName, string lastName,
            string sortField, SortType sortOrder, int pageNumber, int pageSize, out int totalRecords, int tenantID)
        {
            return new CommonDLManager().GetPositionProfileFormTenantUsers(userName, firstName, middleName, lastName, sortField,
                sortOrder, pageNumber, pageSize, out totalRecords, tenantID);
        }

        public bool CheckFeatureApplicableOrNot(int subscriptionId, int featureID)
        {
            return new CommonDLManager().CheckFeatureApplicableOrNot(subscriptionId, featureID);
        }

        public List<UserDetail> GetCandidatesForCandidateReport(string userNameLike, string firstNameLike, string lastNameLike,
           string emailLike, string orderBy, SortType orderByDirection,
           int pageNumber, int pageSize, out int totalRecords, int tenantID)
        {
            return new CommonDLManager().GetCandidatesForCandidateReport(userNameLike, firstNameLike, lastNameLike, emailLike,
                orderBy, orderByDirection, pageNumber, pageSize,
                out totalRecords, tenantID);
        }
        #endregion

        public List<UserDetail> GetUsers(string userNameLike,
             string firstNameLike, string middleNameLike, string lastNameLike,int userID,
             string orderBy, SortType orderByDirection, int pageNumber,
             int pageSize, string roleCodes, out int totalRecords)
        {

            return new CommonDLManager().GetUsers(userNameLike, firstNameLike,
                          middleNameLike, lastNameLike,userID, orderBy, orderByDirection,
                          pageNumber, pageSize, roleCodes, out totalRecords);
        }

        public string[] GetClientList(string prefixText,int count,int tenantID)
        {
            return new CommonDLManager().GetClientList(prefixText,count, tenantID);
        }

        public bool IsValidClient(int clientID, string clientName)
        {
            return new CommonDLManager().IsValidClient(clientID, clientName);
        }

        public List<Department> GetDepartments(int clientID)
        {
            return (new CommonDLManager().GetDepartments(clientID));
        }

        public List<Department> GetDepartmentContacts(string departmentID, string clientID)
        {
            return new CommonDLManager().GetDepartmentContacts(departmentID, clientID);

        }

        public List<Department> GetDepartmentClientIds(short contactID, int departmentID,int clientID)
        {
            return new CommonDLManager().GetDepartmentClientIds(contactID, departmentID,clientID);
        }

        /// <summary>
        /// Method that inserts the candidate activity log.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID which refers to
        /// CANDIDATEINFORMATION table.
        /// </param>
        /// <param name="candidateLoginID">
        /// A <see cref="int"/> that holds the candidate login ID which refers
        /// to PRAS_USER table.
        /// </param>
        /// <param name="comments">
        /// A <see cref="string"/> that holds the comments.
        /// </param>
         /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="activityType">
        /// A <see cref="string"/> that holds the activity type.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the candidate activity log ID.
        /// </returns>
        public int InsertCandidateActivityLog(int candidateID, int candidateLoginID, 
            string comments, int positionProfileID, int userID, string activityType)
        {
            return new CommonDLManager().InsertCandidateActivityLog
                (candidateID, candidateLoginID, comments, positionProfileID, userID, activityType);
        }

        /// <summary>
        /// Method that retrieves the user options.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="UserOptionDetail"/> that holds the user option detail.
        /// </returns>
        public UserOptionDetail GetUserOptions(int userID)
        {
            return new CommonDLManager().GetUserOptions(userID);
        }

        /// <summary>
        /// Method that updates the user option.
        /// </summary>
        /// <param name="userOption">
        /// A <see cref="UserOptionDetail"/> that holds the user option detail.
        /// </param>
        public void UpdateUserOptions(UserOptionDetail userOption)
        {
            CommonDLManager dlManager = new CommonDLManager();

            // Update email notification (send mail on notes added).
            dlManager.UpdateEmailNotification(userOption.UserID, 
                Constants.CandidateActivityLogType.NOTES_ADDED, 
                userOption.SendMailOnNotesAdded);
        }

        /// <summary>
        /// Retrieves the list of email details of the given tenant.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        ///  A list of <see cref="GetEmailDetail"/> that holds the email details.
        /// </returns>
        public List<EmailDetail> GetEmailDetail(int tenantID)
        {
            return new CommonDLManager().GetEmailDetail(tenantID);
        }

         /// <summary>
        /// Retrieves the list of email contacts for the given search criteria.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="keyword">
        /// A <see cref="string"/> that holds the keyword.
        /// </param>
        /// <param name="category">
        /// A <see cref="string"/> that holds the category.
        /// </param>
        /// <param name="roleID">
        /// A <see cref="string"/> that holds the role ID.
        /// </param>
        /// <returns>
        ///  A list of <see cref="GetEmailDetail"/> that holds the email details.
        /// </returns>
        /// <remarks>
        /// This is used in email window to select contacts.
        /// </remarks>
        public List<MailContact> GetMailContacts(
            int tenantID, int userID, string keyword, string category, int roleID)
        {
            return new CommonDLManager().GetMailContacts(tenantID, userID, keyword, category, roleID);
        }

        /// <summary>
        /// Method that retrieves the keyword for mail contacts based on the 
        /// submit type & position profile.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="submitType">
        /// A <see cref="string"/> that holds the submit type. 'C' represents 'client' 
        /// and 'I' represents 'internal'.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the contact keyword.
        /// </returns>
        public string GetPositionProfileContactKeyword(int positionProfileID, string submitType)
        {
            return new CommonDLManager().GetPositionProfileContactKeyword
                (positionProfileID, submitType);
        }

        /// <summary>
        /// Method that retrieves the browser detail and instructions that can 
        /// be shown to the candidates that helps in configure the cyber 
        /// proctor browser settings.
        /// </summary>
        /// <param name="browserType">
        /// A <see cref="string"/> that holds the browser type.
        /// </param>
        /// <returns>
        /// A <see cref="BrowserDetail"/> that holds the browser detail and
        /// instructions.
        /// </returns>
        public BrowserDetail GetBrowserInstructions(string browserType)
        {
            return new CommonDLManager().GetBrowserInstructions(browserType);
        }

        /// <summary>
        /// Method to get the list of categories from the database based on search criteria.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="questionRelationId">
        /// A <see cref="int"/>that holds the question Realtion Id
        /// </param>
        /// <returns>
        /// A List for <see cref="Subject"/>that holds the List of Subjects
        /// </returns>
        public List<Subject> GetInterviewCategorySubjects(string questionKey, int questionRelationId)
        {
            return new CommonDLManager().GetInterviewCategorySubjects(questionKey, questionRelationId);
        }

        /// <summary>
        /// Gets the corporate users.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="middleName">Name of the middle.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="sortField">The sort field.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public List<UserDetail> GetCorporateUsers(string userName, string firstName, string middleName, string lastName,
           string sortField, SortType sortOrder, int pageNumber, int pageSize, out int totalRecords, int tenantID,
            int positionProfielID, string ownerType)
        {
            return new CommonDLManager().GetCorporateUsers(userName, firstName, middleName, lastName, sortField,
                sortOrder, pageNumber, pageSize, out totalRecords, tenantID, positionProfielID, ownerType);
        }


        public List<CandidateInformation> GetLocation(CandidateInformation candidateLocation,
            int pageNumber, int pageSize, out int totalRecords, string sortField, SortType sortType)
        {
            return new CommonDLManager().GetLocation(candidateLocation, pageNumber, pageSize,
                out totalRecords, sortField, sortType);
                
        }

        public UserDetail GetSubscriptionUserDetail(int userID)
        {
            return new CommonDLManager().GetSubscriptionUserDetail(userID);
        }
        /// <summary>
        /// Gets skill for assessor .
        /// </summary>
        /// <param name="prefixKeyword">The prefix keyword.</param>
        /// <returns></returns>
        public string[] GetSkillSearch(string prefixKeyword)
        {
            return new CommonDLManager().GetSkillSearch(prefixKeyword); 
        }

        /// <summary>
        /// Method that flush all data.
        /// </summary>
        public void FlushData()
        {
            new CommonDLManager().FlushData();
        }

        /// <summary>
        /// Method that flush all data by category Id.
        /// </summary>
        public void FlushData(int categoryId)
        {
            new CommonDLManager().FlushData(categoryId);
        }
    }
}