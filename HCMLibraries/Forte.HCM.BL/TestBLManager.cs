﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestBLManager.cs
// File that represents the data layer for the Test respository Manager.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Test respository management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for Test. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// 
    public class TestBLManager
    {
        #region Public Method                                                  

        /// <summary>
        /// Method that will return the test certificate details based on the testkey.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="CertificationDetail"/> that contains the certification details.
        /// </returns>
        public CertificationDetail GetTestCertificateDetail(string testKey)
        {
            return new TestDLManager().GetTestCertificateDetail(testKey);
        }

        /// <summary>
        /// Method that will return the candidate session id, attempt id, and the score 
        /// based on the testkey.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestDetail"/> that contains the candidate test instance.
        /// </returns>
        public CandidateTestDetail GetCandidateTestDetail(string testKey)
        {
            return new TestDLManager().GetCandidateTestDetail(testKey);
        }

        /// <summary>
        /// This method handles the new Test entry functions like Test and testQuestion.
        /// </summary>
        /// <param name="testDetail">
        /// A <see cref="TestDetail"/> that contains Test detail collection
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains Test Creater ID
        /// </param>
        /// <param name="complexity">
        /// A <see cref="string"/> that contains complexity
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains Test Key
        /// </returns>
        public string SaveTest(TestDetail testDetail, int userID, string complexity)
        {
            string testKey = "";
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                testKey = new NextNumberDLManager().GetNextNumber(transaction, "TST", userID);
                testDetail.TestKey = testKey;
                new TestDLManager().InsertTest(testDetail, userID, complexity, transaction);

                // Insert Questions. 
                foreach (QuestionDetail questionDetail in testDetail.Questions)
                {
                    new TestDLManager().InsertTestQuestions(testKey, questionDetail.QuestionKey,
                        questionDetail.QuestionRelationId, userID, transaction);
                }
                if (testDetail.IsCertification == true)
                {
                    // new TestDLManager().InsertTestCertification(testID, questionDetail.QuestionKey, questionDetail.QuestionRelationId, userID, transaction);
                }
                transaction.Commit();
                return testKey;
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// This method handles the Edit Test functions like test And testQuestion.
        /// </summary>
        /// <param name="testDetail">
        /// A <see cref="TestDetail"/> that contains the Test detail collection.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creater of the Test.
        /// </param>
        /// <param name="complexity">
        /// A <see cref="string"/>that contains the complexity Atribute
        /// </param>
        /// <param name="deletedQuestionKey">
        /// A <see cref="string"/>that contains the deleted Question Keys
        /// </param>
        /// <param name="insertedQuestionKey">
        /// A <see cref="string"/>that contains the inserted Question Keys
        /// </param>
        public void UpdateTest(TestDetail testDetail, int userID, string complexity, string deletedQuestionKey, string insertedQuestionKey)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // If existing test.
                new TestDLManager().UpdateTest(testDetail, userID, complexity, transaction);


                if (!Utility.IsNullOrEmpty(deletedQuestionKey))
                    new TestDLManager().DeleteTestQuestion(deletedQuestionKey, testDetail.TestKey, transaction);
                foreach (QuestionDetail questionDetail in testDetail.Questions)
                {
                    // Insert Question. 
                    foreach (string questionKey in insertedQuestionKey.Split(','))
                    {
                        if (questionKey == questionDetail.QuestionKey)
                            new TestDLManager().InsertTestQuestions(testDetail.TestKey, questionDetail.QuestionKey, questionDetail.QuestionRelationId, userID, transaction);
                    }
                }

                transaction.Commit();

            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that updates the copy test status. If questions are same for
		/// both parent and child test, then status and parent test key needs 
        /// to be updated.
        /// </summary>
        /// <param name="parentTestKey">
        /// A <see cref="string"/> that holds the parent test key.
        /// </param>
        /// <param name="childTestKey">
        /// A <see cref="string"/> that holds the child test key.
        /// </param>
        public void UpdateCopyTestStatus(string parentTestKey, string childTestKey)
        {
            new TestDLManager().UpdateCopyTestStatus(parentTestKey, childTestKey);
        }

        /// <summary>
        /// Saves the test session and the candidate session details to the respective repositories.
        /// i.e. TEST_SESSION, SESSION_CANDIDATE
        /// </summary>
        /// <param name="testSessionDetail">
        /// A <see cref="TestSessionDetail"/> that contains the Test Session Detail collection.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creator of the test session.
        /// </param>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the Test session Id.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate Session IDs.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the Mail Sent Or Not.
        /// </param>
        /// <param name="creditRequestDetail">
        /// A <see cref="CreditRequestDetail"/> that contains the credit Request Detail.
        /// </param>
        public void SaveTestSession(TestSessionDetail testSessionDetail, int userID,
            out string testSessionID, out string candidateSessionIDs, out bool isMailSent, CreditRequestDetail creditRequestDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            TestDLManager testDLManager = new TestDLManager();
            NextNumberDLManager nextNumberDLManager = new NextNumberDLManager();

            testSessionID = string.Empty;
            candidateSessionIDs = string.Empty;
            isMailSent = false;

            try
            {
                if (testSessionDetail.TestSessionID == null)
                {
                    // Generate test session id and assign to a string variable
                    testSessionID = nextNumberDLManager.GetNextNumber(transaction, "TSN", userID);
                    testSessionDetail.TestSessionID = testSessionID;

                    // Insert test session
                    testDLManager.InsertTestSession(testSessionDetail, userID, transaction);

                    creditRequestDetail.CandidateId = userID;

                    creditRequestDetail.CreatedBy = testSessionDetail.CreatedBy;

                    creditRequestDetail.CreditRequestDescription = Constants.CreditInfoConstants.
                                                                    TEST_SESSION_CREATION;

                    creditRequestDetail.CreditType = CreditType.Used;

                    //Add entry in histroy table only if it is approved 
                    testDLManager.InsertIntoCreditHistory(creditRequestDetail, transaction);

                    // Insert candidate test session
                    for (int candidateSessionCount = 0; candidateSessionCount <
                        testSessionDetail.NumberOfCandidateSessions; candidateSessionCount++)
                    {
                        CandidateTestSessionDetail candidateSessionDetail = new CandidateTestSessionDetail();

                        // Get candidate session id
                        string candidateSessionID = nextNumberDLManager.GetNextNumber(transaction, Constants.ObjectTypes.CANDIDATE_TEST_SESSION, userID);

                        if (candidateSessionIDs.Trim().Length == 0)
                            candidateSessionIDs = candidateSessionID;
                        else
                            candidateSessionIDs += ", " + candidateSessionID;

                        // Assign candidate test session details
                        candidateSessionDetail.CandidateTestSessionID = candidateSessionID.ToString().Trim();
                        candidateSessionDetail.TestSessionID = testSessionID.Trim();
                        candidateSessionDetail.Status = Constants.CandidateSessionStatus.NOT_SCHEDULED;
                        candidateSessionDetail.CreatedBy = testSessionDetail.CreatedBy;
                        candidateSessionDetail.ModifiedBy = testSessionDetail.ModifiedBy;

                        // Add candidate test session detail to the list
                        testDLManager.InsertCandidateTestSession(candidateSessionDetail, transaction);
                    }

                    // Insert test description
                    testDLManager.InsertTestDescription(testSessionDetail, transaction);
                }

                // Commit the transaction
                transaction.Commit();

                // Send Mail
                try
                {
                    // Send email to the candidate who is requested to reschedule
                    new EmailHandler().SendMail
                        (EntityType.TestSessionCreated, testSessionID);

                    // Mail sent successfully.
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        public void CreateCandidateTestSession(int noOfCandidateSession, int userID, string testSessionID, out string candidateSessionIDs)
        {
            try
            {
                IDbTransaction transaction = new TransactionManager().Transaction;
                TestDLManager testDLManager = new TestDLManager();
                NextNumberDLManager nextNumberDLManager = new NextNumberDLManager();

                
                candidateSessionIDs = string.Empty;
                //isMailSent = false;
                // Insert candidate test session
                for (int candidateSessionCount = 0; candidateSessionCount <
                    noOfCandidateSession; candidateSessionCount++)
                {
                    CandidateTestSessionDetail candidateSessionDetail = new CandidateTestSessionDetail();

                    // Get candidate session id
                    string candidateSessionID = nextNumberDLManager.GetNextNumber(transaction, Constants.ObjectTypes.CANDIDATE_TEST_SESSION, userID);

                    if (candidateSessionIDs.Trim().Length == 0)
                        candidateSessionIDs = candidateSessionID;
                    else
                        candidateSessionIDs += ", " + candidateSessionID;

                    // Assign candidate test session details
                    candidateSessionDetail.CandidateTestSessionID = candidateSessionID.ToString().Trim();
                    candidateSessionDetail.TestSessionID = testSessionID;
                    candidateSessionDetail.Status = Constants.CandidateSessionStatus.NOT_SCHEDULED;
                    candidateSessionDetail.CreatedBy = userID;
                    candidateSessionDetail.ModifiedBy = userID;

                    // Add candidate test session detail to the list
                    testDLManager.InsertCandidateTestSession(candidateSessionDetail, transaction);
                }
                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

        /// <summary>
        /// this mehtod represent update the test session
        /// </summary>
        /// <param name="testSessionDetail">
        /// A <see cref="TestSessionDetail"/> that holds the test session details
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id
        /// </param>
        /// <param name="testSessionID">
        /// A <see cref="string"/>that holds the test session id
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that holds the candidate session id
        /// </param>
        /// <param name="numberOfSessions">
        /// A <see cref="int"/> that holds the session counts out parameter
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> Whether check mail sent or not.
        /// </param>
        public void UpdateTestSession(TestSessionDetail testSessionDetail, int userID,
            string testSessionID, out string candidateSessionIDs, int numberOfSessions, out bool isMailSent)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            TestDLManager testDLManager = new TestDLManager();
            NextNumberDLManager nextNumberDLManager = new NextNumberDLManager();
            isMailSent = false;

            candidateSessionIDs = string.Empty;

            try
            {
                if (numberOfSessions != 0)
                {
                    // Insert candidate test session
                    for (int candidateSessionCount = 0; candidateSessionCount <
                        numberOfSessions; candidateSessionCount++)
                    {
                        CandidateTestSessionDetail candidateSessionDetail = new CandidateTestSessionDetail();

                        // Get candidate session id
                        string candidateSessionID = nextNumberDLManager.GetNextNumber(transaction, Constants.ObjectTypes.CANDIDATE_TEST_SESSION, userID);

                        if (candidateSessionIDs.Trim().Length == 0)
                            candidateSessionIDs = candidateSessionID;
                        else
                            candidateSessionIDs += ", " + candidateSessionID;

                        // Assign candidate test session details
                        candidateSessionDetail.CandidateTestSessionID = candidateSessionID.ToString().Trim();
                        candidateSessionDetail.TestSessionID = testSessionID;
                        candidateSessionDetail.Status = Constants.CandidateSessionStatus.NOT_SCHEDULED;
                        candidateSessionDetail.CreatedBy = testSessionDetail.CreatedBy;
                        candidateSessionDetail.ModifiedBy = testSessionDetail.ModifiedBy;

                        // Add candidate test session detail to the list
                        testDLManager.InsertCandidateTestSession(candidateSessionDetail, transaction);
                    }
                }

                // Update test session
                testDLManager.UpdateTestSession(testSessionDetail, userID, transaction);

                // Update test description
                testDLManager.UpdateTestDescription(testSessionDetail, userID, transaction);

                // Commit the transaction
                transaction.Commit();

                // Send Mail
                try
                {
                    if (candidateSessionIDs != null && candidateSessionIDs.TrimEnd(',').Length > 0)
                    {
                        // Send email to the candidate who is requested to reschedule
                        new EmailHandler().SendMail
                            (EntityType.TestSessionModified, testSessionID, candidateSessionIDs.TrimEnd(','));

                        // Mail sent successfully.
                        isMailSent = true;
                    }
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Saves the test session and the candidate session details to the respective repositories.
        /// i.e. TEST_SESSION, SESSION_CANDIDATE
        /// </summary>
        /// <param name="testSessionDetail">
        /// A <see cref="TestSessionDetail"/> that holds test session detail
        /// </param>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that holds test schedule detail
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/>that holds user id
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that holds candidate session ids
        /// </param>
        public void SaveTestSessionScheduleCandidate(TestSessionDetail testSessionDetail,
            TestScheduleDetail testScheduleDetail, int userID, out string candidateSessionIDs)
        {
            string testSessionID = string.Empty;
            //  string candidateSessionIDs=string.Empty;
            string candidateSessionID = string.Empty;
            IDbTransaction transaction = new TransactionManager().Transaction;
            TestDLManager testDLManager = new TestDLManager();
            NextNumberDLManager nextNumberDLManager = new NextNumberDLManager();
            try
            {
                //Get the credit value for the test session created by the candidate
                decimal creditValue = new TestBLManager().GetCreditValue();

                if (testSessionDetail.TestSessionID == null)
                {
                    // Generate test session id and assign to a string variable
                    testSessionID = nextNumberDLManager.GetNextNumber(transaction, "TSN", userID);
                    testSessionDetail.TestSessionID = testSessionID;

                    // Insert test session
                    testDLManager.InsertTestSession(testSessionDetail, userID, transaction);
                    // Insert candidate test session
                    CandidateTestSessionDetail candidateSessionDetail = new CandidateTestSessionDetail();
                    // Get candidate session id
                    candidateSessionID = nextNumberDLManager.GetNextNumber(transaction, Constants.ObjectTypes.CANDIDATE_TEST_SESSION, userID);

                    // Assign candidate test session details
                    candidateSessionDetail.CandidateTestSessionID = candidateSessionID.ToString().Trim();
                    candidateSessionDetail.TestSessionID = testSessionID.Trim();
                    candidateSessionDetail.Status = Constants.CandidateSessionStatus.NOT_SCHEDULED;
                    candidateSessionDetail.CreatedBy = testSessionDetail.CreatedBy;
                    candidateSessionDetail.ModifiedBy = testSessionDetail.ModifiedBy;

                    // Add candidate test session detail to the list
                    testDLManager.InsertCandidateTestSession(candidateSessionDetail, transaction);

                    CreditRequestDetail creditRequestDetail = new CreditRequestDetail();

                    creditRequestDetail.CreatedBy = candidateSessionDetail.CreatedBy;

                    creditRequestDetail.Amount = creditValue;

                    creditRequestDetail.CandidateId = userID;

                    creditRequestDetail.CreditType = CreditType.Used;

                    creditRequestDetail.CreditRequestDescription = Constants.CreditInfoConstants.TEST_SESSION_CREATION;

                    new TestDLManager().InsertIntoCreditHistory(creditRequestDetail, transaction);

                    testScheduleDetail.CandidateTestSessionID = candidateSessionID;
                    // Add Schedule candidate.
                    new TestSchedulerDLManager().ScheduleCandidate(testScheduleDetail, userID, transaction);
                    int positionProfileID = new PositionProfileDLManager().GetPositionProfileID(testScheduleDetail.CandidateTestSessionID, "T");
                    if (positionProfileID > 0)
                    {
                        PositionProfileCandidate positionProfileCandidate = new PositionProfileCandidate();
                        positionProfileCandidate.PositionProfileID = positionProfileID;
                        positionProfileCandidate.CandidateID = int.Parse(testScheduleDetail.CandidateID);
                        positionProfileCandidate.CandidateSessionID = testScheduleDetail.CandidateTestSessionID;
                        positionProfileCandidate.Status = testScheduleDetail.ScheduleStatus.ToString();
                        positionProfileCandidate.PickedBy = null;
                        positionProfileCandidate.PickedDate = null;
                        positionProfileCandidate.SchelduleDate = testScheduleDetail.ExpiryDate;
                        positionProfileCandidate.ResumeScore = null;
                        positionProfileCandidate.AttemptID = testScheduleDetail.AttemptID;
                        positionProfileCandidate.SourceFrom = "N";
                        positionProfileCandidate.CreatedBy = userID;
                        new TestDLManager().InsertPositionProfileCandidate(positionProfileCandidate, "T", transaction);
                        //if (testScheduleDetail.CandidateID != null && testScheduleDetail.CandidateID.Trim().Length > 0)
                        //{
                        //    new CommonBLManager().InsertCandidateActivityLog
                        //        (0, Convert.ToInt32(testScheduleDetail.CandidateID), "Position profile associated to candidate", userID,
                        //        Constants.CandidateActivityLogType.CANDIDATE_ASSOCIATED_POSITION_PROFILE);
                        //}
                    }
                }

                // Insert test description
                testDLManager.InsertTestDescription(testSessionDetail, transaction);

                candidateSessionIDs = candidateSessionID;
                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Gets test name 
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="TestDetail"/> that contains the Test Detail.
        /// </returns>
        public TestDetail GetTestDetail(string testID)
        {
            return new TestDLManager().GetTestDetail(testID);
        }

        /// <summary>
        /// Method that will return the candidate score details against the candidate 
        /// session key and attemptid.
        /// </summary>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestDetail"/> that contains the candidate test
        /// </returns>
        public CandidateTestDetail GetCandidateScoreDetail(string candidateSessionKey, int attemptID)
        {
            return new TestDLManager().GetCandidateScoreDetail(candidateSessionKey, attemptID);
        }

        /// <summary>
        /// Method that will load the test inclusion details are associated with the question. 
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that contains information based on the 
        /// question key.
        /// </returns>
        public List<TestDetail> GetTestInclusionDetail
            (string questionKey, int pageNumber, int pageSize, string orderBy,
            SortType orderByDirection, out int totalRecords)
        {
            return new TestDLManager().GetTestInclusionDetail(questionKey, pageNumber,
                pageSize, orderBy, orderByDirection, out totalRecords);
        }

        /// <summary>
        /// Helps to retrieve test and certificate information against the test key
        /// </summary>
        /// <param name="testKey">Contains the test key</param>
        /// <returns></returns>
        public TestDetail GetTestAndCertificateDetail(string testKey)
        {
            return new TestDLManager().GetTestAndCertificateDetail(testKey);
        }

        /// <summary>
        /// Returns question object
        /// </summary>
        /// <param name="testKey">Contains test key</param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that holds the sort expression
        /// </param>
        /// <returns></returns>
        public List<QuestionDetail> GetTestQuestionDetail(string testKey, string sortExpression)
        {
            return new TestDLManager().GetTestQuestionDetail(testKey, sortExpression);
        }

        /// <summary>
        /// Method that will return the list of candidate test session details.
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="string"/> that contains the sort order either ASC/DESC.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateTestSessionDetail"/> that contains information
        /// about the candidate test session details.
        /// </returns>
        public List<CandidateTestSessionDetail> GetCandidateTestSessions(string testID, int pageNumber,
            int pageSize, string orderBy, SortType sortDirection,int userID, out int totalRecords)
        {
            return new TestDLManager().GetCandidateTestSessions(testID, pageNumber,
                pageSize, orderBy, sortDirection,userID, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of Questions for the given search 
        /// </summary>
        /// <param name="searchCriteria">
        /// A list of <see cref="QuestionDetailSearchCriteria"/> that holds the questionKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the QuestionDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<QuestionDetail> GetSearchQuestions(QuestionType questionType, QuestionDetailSearchCriteria searchCriteria, int pageSize, int pageNumber, string orderBy, string direction, out int totalRecords)
        {
            return new TestDLManager().GetSearchQuestions(questionType, searchCriteria, pageSize, pageNumber, orderBy, direction, out totalRecords);
        }

        /// <summary>
        /// Method that will cancel the test session for a particular candidate.
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate 
        /// session information.
        /// </param>
        public void CancelTestSession(CandidateTestSessionDetail candidateTestSessionDetail)
        {
            new TestDLManager().CancelTestSession(candidateTestSessionDetail);
            //TODO : Send the reason for cancellation in mail.
            //emailHandler.SendMail(new MailDetail());
        }

        /// <summary>
        /// This method is used to Active question based on the testKey.
        /// </summary>
        /// <param name="testKey">
        ///  A <see cref="string"/> that holds the testKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void ActivateTest(string testKey, int user)
        {
            new TestDLManager().UpdateStatus(testKey, "Y", user);
        }

        /// <summary>
        /// This method is used to DeactivateTest question based on the testKey.
        /// </summary>
        /// <param name="testKey">
        ///  A <see cref="string"/> that holds the testKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeactivateTest(string testKey, int user)
        {
            new TestDLManager().UpdateStatus(testKey, "N", user);
        }

        /// <summary>
        /// This method is used to Delete Test based on the testKey.
        /// </summary>
        /// <param name="testKey">
        ///  A <see cref="string"/> that holds the testKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeleteTest(string testKey, int user)
        {
            new TestDLManager().DeleteTest(testKey, user);
        }

        /// <summary>
        /// Method that retrieves the list of Tests for the given search 
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A list of <see cref="TestSearchCriteria"/> that holds the TestKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that holds the TestDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<TestDetail> GetTests(TestSearchCriteria testSearchCriteria, int pageSize, int pageNumber, string orderBy, SortType direction, out int totalRecords)
        {
            return new TestDLManager().GetTests(testSearchCriteria, pageSize, pageNumber, orderBy, direction, out totalRecords);
        }

        /// <summary>
        /// This method Communicates with the Test DL Manager to get the 
        /// test session records from the DB
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A list of <see cref="TestSearchCriteria"/> that holds the TestKey,Category,Subject,etc..
        /// </param>
        /// /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="OrderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of test details</returns>
        public List<TestDetail> GetTestForTestSession(TestSearchCriteria testSearchCriteria, int PageNumber, int PageSize, string OrderBy, SortType direction, out int TotalNoofRecords)
        {
            return new TestDLManager().GetTestForTestSession(testSearchCriteria, PageNumber, PageSize, OrderBy, direction, out TotalNoofRecords);
        }

        /// <summary>
        /// To get the adaptive questions.
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="int"/> that holds the UserId 
        /// </param>
        /// <param name="SelectedQuestions">
        /// A <see cref="string"/> that holds the user selected questions.
        /// (User moved to Test Draft Panel) 
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size 
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number 
        /// </param>
        /// <param name="TotalNoOfRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// </param>
        /// <returns>
        /// A List for<see cref="QuestionDetail"/> List of question details 
        /// </returns>
        public List<QuestionDetail> GetAdaptiveQuestions(int UserId, string SelectedQuestions, int pageSize, int pageNumber,int questionAuthor, out int TotalNoOfRecords)
        {
            return new TestDLManager().GetAdaptiveQuestions(UserId, SelectedQuestions, pageSize, pageNumber, questionAuthor, out TotalNoOfRecords);
        }

        /// <summary>
        /// Gets the adaptive recommend questions based on the similar candidate
        /// </summary>
        /// <param name="CandidateId">Candidate id which has logged in to the
        /// system</param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="OrderByDirection">
        /// A<see cref="char"/>that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="SortExpression">
        /// A<see cref="string"/>that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of Test Details</returns>
        public List<TestDetail> GetAdaptiveTestBySimilarCandidate(int CandidateId, int PageSize,
            int PageNumber, char OrderByDirection, string SortExpression,
            out int TotalNoofRecords)
        {
            return new TestDLManager().GetAdaptiveTestBySimilarCandidate(CandidateId, PageSize,
                PageNumber, OrderByDirection, SortExpression, out TotalNoofRecords);
        }

        /// <summary>
        /// Gets the adaptive recommend questions based on the similar profile
        /// </summary>
        /// <param name="CandidateId">Candidate id which has logged in to the
        /// system</param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="OrderByDirection">
        /// A<see cref="char"/>that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="SortExpression">
        /// A<see cref="string"/>that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of Test Details</returns>
        public List<TestDetail> GetAdaptiveTestBySimilarProfile(int CandidateId, int PageSize, int PageNumber, char OrderByDirection,
            string SortExpression, out int TotalNoofRecords)
        {
            return new TestDLManager().GetAdaptiveTestBySimilarProfile(CandidateId, PageSize, PageNumber, OrderByDirection,
                SortExpression, out TotalNoofRecords);
        }

        /// <summary>
        /// Gets the adaptive recommend questions based on the assessment history
        /// </summary>
        /// <param name="CandidateId">Candidate id which has logged in to the
        /// system</param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="OrderByDirection">
        /// A<see cref="char"/>that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="SortExpression">
        /// A<see cref="string"/>that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of Test Details</returns>
        public List<TestDetail> GetAdaptiveTestByAssessmentHistory(int CandidateId, int PageSize, int PageNumber, char OrderByDirection,
            string SortExpression, out int TotalNoofRecords)
        {
            return new TestDLManager().GetAdaptiveTestByAssessmentHistory(CandidateId, PageSize, PageNumber, OrderByDirection,
                SortExpression, out TotalNoofRecords);
        }

        /// <summary>
        /// Represents the method to get the credit value for the candidate
        /// who created the test session 
        /// </summary>
        /// <returns>
        /// A<see cref="decimal"/>that holds the credit value
        /// </returns>
        public decimal GetCreditValue()
        {
            return new TestDLManager().GetCreditValue();
        }

        public List<QuestionDetail> GetCandidateTestQuestionDetail(string testKey, string candidateSessionKey, int attemptID)
        {
            return new TestDLManager().GetCandidateTestQuestionDetail(testKey, candidateSessionKey,attemptID);
        }

        #endregion

        /// <summary>
        /// Method that retrieves assoiciated position profile details for the 
        /// given test key.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileDetail"/> that holds the position 
        /// profile and test detail.
        /// </returns>
        public PositionProfileDetail GetPositionProfileTestDetail(string testKey)
        {
            return new TestDLManager().GetPositionProfileTestDetail(testKey);
        }

        public void  UpdateTest(string testKey, int positionProfileID, int userID)
        {
            new TestDLManager().UpdateTest(testKey, positionProfileID, userID);
        }

        public List<TestDetail> GetRecommendedTestDetails(string skills, string sortExpression, string sortOrder, int pageSize, int pageNumber, out int totalPage)
        {

            return new TestDLManager().GetRecommendedTestDetails(skills, sortExpression, sortOrder, pageSize, pageNumber, out totalPage);

        }

        public List<TestSearchCriteria> GetSearchCriteria(int testRecommendedId)
        {
            return new TestDLManager().GetSearchCriteria(testRecommendedId);
        }

        /// <summary>
        /// Method that inserts the candidate recommended test details and 
        /// returns the generated ID.
        /// </summary>
        /// <param name="testDetail">
        /// A <see cref="TestDetail"/> that holds the test detail.
        /// </param>
        /// <param name="isNew">
        /// A <see cref="bool"/> that holds the output parameter which indicates 
        /// whether the record is newly saved or already exist.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the generated ID.
        /// </returns>
        public int InsertCandidateRecommendedTest(TestDetail testDetail, out bool isNew)
        {
            return new TestDLManager().InsertCandidateRecommendedTest(testDetail, out isNew);
        }

        /// <summary>
        /// Method that retrieves the recommended test as well as candidate
        /// details for the given recommended test gen ID.
        /// </summary>
        /// <param name="genID">
        /// A <see cref="int"/> that holds the gen ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestRecommendationDetail"/> that contains recommended 
        /// test and candidate details.
        /// </returns>
        /// <remarks>
        /// This helps in sending email to candidate when a test recommendation
        /// is saved.
        /// </remarks>
        public TestRecommendationDetail GetCandidateRecommendedTest(int genID)
        {
            return new TestDLManager().GetCandidateRecommendedTest(genID);
        }
    }
}