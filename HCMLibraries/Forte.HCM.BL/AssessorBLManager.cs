﻿using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using System.Data.Common;
using Forte.HCM.Support;

namespace Forte.HCM.BL
{
    public class AssessorBLManager
    {
        public List<AssessorAssessmentDetail> GetAssessmentDetailsForAssessor
            (AssessmentSearchCriteria assessmentSearchCriteria, int pageNumber, int pageSize, out int totalRecords)
        {
            return new AssessorDLManager().GetAssessmentDetailsForAssessor(assessmentSearchCriteria, pageNumber, pageSize, out totalRecords);
        }

        public AssessorDetail GetAssessorByUserId(int userId)
        {
            return new AssessorDLManager().GetAssessorByUserId(userId);
        }

        /// <summary>
        /// Method that inserts the assessor skills.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="AssessorDetail"/> that holds the external assessor details.
        /// </returns>
        public ExternalAssessorDetail GetExternalAssessorByID(int userID)
        {
            return new AssessorDLManager().GetExternalAssessorByID(userID);
        }

        public string SaveAssessorDetails(AssessorDetail assessorDetails)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            string retval = string.Empty;

            try
            {
             retval = new AssessorDLManager().
                 SaveAssessorDetails(assessorDetails,transaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }

            // Commit the transaction.
            transaction.Commit();

            return retval;
        }


        public List<AttributeDetail> GetAssessmentTypes()
        {
            return new AssessorDLManager().GetAssessmentTypes();
        }

        /// <summary>
        /// Method that retrieves the list of time slot settings.
        /// </summary>
        /// <param name="timeSlotType">
        /// A <see cref="TimeSlotType"/> that holds the time slot type. This 
        /// determines inclusion/exclusion or first and last slots.
        /// </param>
        /// <returns>
        /// A list of <see cref="TimeSlotDetail"/> that holds the time slot details.
        /// </returns>
        public List<TimeSlotDetail> GetTimeSlotSettings(TimeSlotType timeSlotType)
        {
            List<TimeSlotDetail> timeSlots = new AssessorDLManager().GetTimeSlotSettings();

            if (timeSlots == null)
                return null;

            //if (timeSlotType == TimeSlotType.From)
            //    timeSlots.RemoveAt(timeSlots.Count - 1);
            //else if (timeSlotType == TimeSlotType.To)
            //    timeSlots.RemoveAt(0);

            return timeSlots;
        }

        /// <summary>
        /// Method that inserts a assessor time slot detail.
        /// </summary>
        /// <param name="timeSlot">
        /// A <seealso cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        public void InsertAssessorTimeSlot(int requestedDateID, AssessorTimeSlotDetail timeSlot, int userID)
        {
            new AssessorDLManager().InsertAssessorTimeSlot(0, timeSlot, 0, null);
        }

        /// <summary>
        /// Methot that inserts the list of selected time 
        /// slot against the date(referred with assessor availability date).
        /// </summary>
        /// <param name="timeSlots">
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        public void SaveAssessorTimeSlots(string mode,AssessorTimeSlotDetail proposedDateDetail, 
            List<AssessorTimeSlotDetail> timeSlots)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            string retval = string.Empty;

            try
            {
                AssessorDLManager dlManager = new AssessorDLManager();
                if (mode=="P" && !Utility.IsNullOrEmpty(proposedDateDetail))
                {
                    proposedDateDetail.RequestDateGenID = new OnlineInterviewAssessorDLManager().
                        InsertAssessorAvailabilityDates(proposedDateDetail,transaction);
                }
                else
                {
                    //Update the request date status
                    new AssessorDLManager().UpdateAssessorAvailabilityDates(proposedDateDetail,transaction);
                }
                string availableTimeids = string.Empty;
                // Loop through the time slots and save.
                foreach (AssessorTimeSlotDetail timeSlot in timeSlots)
                {
                    dlManager.InsertAssessorTimeSlot(proposedDateDetail.RequestDateGenID, timeSlot, 
                        proposedDateDetail.InitiatedBy, transaction);
                    availableTimeids = availableTimeids + timeSlot.TimeSlotIDFrom + ",";
                }

                 if (!Utility.IsNullOrEmpty(availableTimeids))
                {
                    //Delete the deselected available times
                    availableTimeids = availableTimeids.ToString().Trim().TrimEnd(',');
                    dlManager.DeleteAssessorTimeSlot(proposedDateDetail.RequestDateGenID, availableTimeids, transaction);
                } 

                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that updates an assessor time slot detail.
        /// </summary>
        /// <param name="timeSlot">
        /// A <seealso cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        public void UpdateAssessorTimeSlot(AssessorTimeSlotDetail timeSlot)
        {
            new AssessorDLManager().UpdateAssessorTimeSlot(timeSlot);
        }

        /// <summary>
        /// Method that retrieves the list of assessor time slot for the
        /// given assessor ID and availability date.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="availabilityDate">
        /// A <see cref="DateTime"/> that holds the availability date.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor time slot details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetAssessorTimeSlots
            (int assessorID, DateTime availabilityDate)
        {
            return new AssessorDLManager().GetAssessorTimeSlots(assessorID, availabilityDate);
        }

        /// <summary>
        /// Method that deletes an assessor time slot.
        /// </summary>
        /// <param name="timeSlotID">
        /// A <see cref="int"/> that holds the time slot ID.
        /// </param>
        public void DeleteAssessorTimeSlot(int timeSlotID)
        {
            new AssessorDLManager().DeleteAssessorTimeSlot(timeSlotID,null,null);
        }

         /// <summary>
        /// Method that retrieves the assessor time slot setting for the given 
        /// time slot ID.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="availabilityDate">
        /// A <see cref="DateTime"/> that holds the availability date.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor time slot details.
        /// </returns>
        public AssessorTimeSlotDetail GetAssessorTimeSlot(int timeSlotID)
        {
            return new AssessorDLManager().GetAssessorTimeSlot(timeSlotID);
        }

             /// <summary>
        /// Method that retrieves the assessor time slot setting for the given 
        /// time slot ID.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="availabilityDate">
        /// A <see cref="DateTime"/> that holds the availability date.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor time slot details.
        /// </returns>
        public AssessorTimeSlotDetail GetAssessorTimeSlot(int assessorId, DateTime selectedDate)
        {
            return new AssessorDLManager().GetAssessorTimeSlot(assessorId, selectedDate);
        }

        /// <summary>
        /// Method that retrieves the list of assessor slot dates occupied with schedule
		/// for the given month and year.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="month">
        /// A <see cref="int"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <param name="includePrevNextMonth">
        /// A <see cref="bool"/> that holds the status whether to include 
        /// previous and next month dates. This helps in highlighting the
        /// other month dates in the calender display
        /// </param>
        /// <returns>
        /// A list of <see cref="DateTime"/> that holds the slot dates.
        /// </returns>
        public List<DateTime> GetAssessorTimeSlotDates(int assessorID, int month,
            int year, bool includePrevNextMonth)
        {
            return new AssessorDLManager().GetAssessorTimeSlotDates
                (assessorID, month, year, includePrevNextMonth);
        }

        /// <summary>
        /// Method that retrieves assesor for the given skills 
        /// </summary>
        /// <param name="skill">
        /// A <see cref="string"/> that holds the skill.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the positionProfileID.
        /// </param>
        /// <returns>
        /// A <see cref="DataTatable"/> that holds the summary data.
        /// </returns>
        public DataTable GetAssessorListBySkill(int tenantID, string skill, int positionProfileID)
        {
            return new AssessorDLManager().GetAssessorListBySkill(tenantID, skill, positionProfileID);
        }

        /// <summary>
        /// Method that retrieves list of assesor for the given position profile id
        /// (Display only assessors that have interviewed for this client in the past) 
        /// </summary>
        /// <param name="positionProfileId">
        /// A <see cref="string"/> that holds the Position Profile Id.
        /// </param>
        /// <returns>
        /// A <see cref="DataTatable"/> that holds the summary data.
        /// </returns>
        public DataTable GetAssessorListByPositionProfile(int positionProfileId)
        {
            return new AssessorDLManager().
                GetAssessorListByPositionProfile(positionProfileId);
        }

        /// <summary>
        /// Method that retrieves the list of time slot requests for the given 
        /// search parameters.
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="AssessorTimeSlotSearchCriteria"/> that holds the 
        /// search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetTimeSlotRequest
            (AssessorTimeSlotSearchCriteria criteria, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new AssessorDLManager().GetTimeSlotRequest
                (criteria, pageNumber, pageSize, sortField, sordOrder, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of time slot requests made by a 
		/// scheduler/recruiter.
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="AssessorTimeSlotSearchCriteria"/> that holds the 
        /// search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetSchedulerTimeSlotRequest
            (AssessorTimeSlotSearchCriteria criteria, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new AssessorDLManager().GetSchedulerTimeSlotRequest
                (criteria, pageNumber, pageSize, sortField, sordOrder, out totalRecords);
        }

        /// <summary>
        /// Method that updates the assessor availability status.
        /// </summary>
        /// <param name="requestDateID">
        /// A <seealso cref="int"/> that holds the time slot ID.
        /// </param>
        /// <param name="requestStatus">
        /// A <see cref="string"/> that holds the request status. This can be
        /// either 'A' (accepted) or 'R' (rejected').
        /// </param>
        /// <param name="isAvailableFullTime">
        /// A <seealso cref="string"/> that holds the available status.
        /// </param>
        /// <param name="userID">
        /// A <seealso cref="int"/> that holds the user id.
        /// </param>
        public void UpdateAssessorAvailabilityDates(AssessorTimeSlotDetail timeSlotDetail)
        {
            new AssessorDLManager().UpdateAssessorAvailabilityDates(timeSlotDetail,null);
        }

        /// <summary>
        /// Method that retrieves the list of skills for the given search
        /// parameters.
        /// </summary>
        /// <param name="skillName">
        /// A <see cref="string"/> that holds the skill name.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetSkills(string skillName, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new AssessorDLManager().GetSkills(skillName, pageNumber,
                pageSize, sortField, sordOrder, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of processed time slots for the 
        /// given parameters.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="sessionKey">
        /// A <see cref="string"/> that holds the session key.
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="string"/> that holds the skill ID.
        /// </param>
        /// <param name="requestStatus">
        /// A <see cref="string"/> that holds the request status.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// processed time slot list.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetProcesssedTimeSlots
            (int assessorID, string sessionKey, int skillID, string requestStatus)
        {
            return new AssessorDLManager().GetProcesssedTimeSlots
                (assessorID, sessionKey, skillID, requestStatus);
        }

        /// <summary>
        /// Method that checks if accepted time slots available for the given
        /// parameters.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="sessionKey">
        /// A <see cref="string"/> that holds the session key.
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="string"/> that holds the skill ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the availability status. If 
        /// available returns true and otherwise false.
        /// </returns>
        public bool IsAcceptedTimeSlotsAvailable(int assessorID,
            string sessionKey, int skillID)
        {
            return new AssessorDLManager().IsAcceptedTimeSlotsAvailable
                (assessorID, sessionKey, skillID);
        }

        /// <summary>
        /// Method that retrieves the list of assessments done by the given
        /// assessor.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorAssessmentDetail"/> that holds the 
        /// assessment details.
        /// </returns>
        /// <remarks>
        /// This will return only the completed and in-progress assessments.
        /// </remarks>
        public List<AssessorAssessmentDetail> GetAssessorAssessments
            (int assessorID, int pageNumber, int pageSize, out int totalRecords)
        {
            return new AssessorDLManager().GetAssessorAssessments
                (assessorID, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of assessors and corporate users for
        /// the given search criteria.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="name">
        /// A <see cref="string"/> that holds the name.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="includeUsers">
        /// A <see cref="bool"/> that holds the status whether to include users
        /// or not. True indicates include users and false do not.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortType"/> that holds the sort order.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the assessor details.
        /// </returns>
        public List<AssessorDetail> GetAssessorsAndUsers(int tenantID,
            string name, string email, bool includeUsers, string sortField, SortType sordOrder,
            int pageNumber, int pageSize, out int totalRecords)
        {
            return new AssessorDLManager().GetAssessorsAndUsers(tenantID,
                name, email, includeUsers, sortField, sordOrder,
                pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retreives the combined info such as assessor detail, 
        /// skill name for given parameters.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        /// <returns>
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </returns>
        public AssessorDetail GetAssessorCombinedInfo(int userID, int skillID)
        {
            return new AssessorDLManager().GetAssessorCombinedInfo(userID, skillID);
        }

         /// <summary>
        /// Method that retreives the non availability dates for the given 
        /// assessor.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the non availability dates as
        /// comma separated.
        /// </returns>
        public string GetAssessorNonAvailabilityDates(int userID)
        {
            return new AssessorDLManager().GetAssessorNonAvailabilityDates(userID);
        }

        /// <summary>
        /// Method that updates the non availability dates for the given 
        /// assessor.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="nonAvailabilityDates">
        /// A <see cref="string"/> that holds non availability dates as comma
        /// separated.
        /// </param>
        public void UpdateAssessorNonAvailabilityDates(int userID,
            string nonAvailabilityDates)
        {
            new AssessorDLManager().UpdateAssessorNonAvailabilityDates
                (userID, nonAvailabilityDates);
        }

        /// <summary>
        /// Method that retrieves the list of candidate schedules for the given
        /// online interview candidate session key.  
        /// </summary>
        /// <param name="candidateInterviewSessionKey">
        /// A <see cref="string"/> that holds the online interview candidate
        /// session key.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID as an output
        /// parameter.
        /// </param>
        /// <param name="candidateName">
        /// A <see cref="string"/> that holds the candidate name as an output
        /// parameter.
        /// </param>
        /// <param name="candidateEmail">
        /// A <see cref="string"/> that holds the candidate email as an output
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="OnlineCandidateSessionDetail"/> that holds the
        /// online interview candidate schedules.
        /// </returns>
        public List<OnlineCandidateSessionDetail> GetOnlineInterviewCandidateSchedules
            (string candidateInterviewSessionKey, string sortField,
            SortType sordOrder, out int candidateID, out string candidateName, 
            out string candidateEmail)
        {
            return new AssessorDLManager().GetOnlineInterviewCandidateSchedules
                (candidateInterviewSessionKey, sortField, sordOrder, 
                out candidateID, out candidateName, out candidateEmail);
        }

        /// <summary>
        /// Method that deletes an online interview candidate schedule.
        /// assessor.
        /// </summary>
        /// <param name="candidateInterviewSessionKey">
        /// A <see cref="string"/> that holds the online interview candidate
        /// session key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="timeSlotID">
        ///  A <see cref="int"/> that holds the time slot ID.
        /// </param>
        public void DeleteOnlineInterviewCandidateSchedule
            (string candidateInterviewSessionKey,
            int skillID, int assessorID, int timeSlotID)
        {
            new AssessorDLManager().DeleteOnlineInterviewCandidateSchedule
                (candidateInterviewSessionKey, skillID, assessorID, timeSlotID);
        }

        /// <summary>
        /// Method that retrieves skill detail for the given skill. If the 
        /// skill is not present then it will be created as new and newly 
        /// added skill will be retrieved.
        /// </summary>
        /// <param name="skill">
        /// A <see cref="string"/> that holds the skill.
        /// </param>
        /// <returns>
        /// A <see cref="SkillDetail"/> that holds the skill detail.
        /// </returns>
        public SkillDetail GetSkillDetail(string skill)
        {
            return new AssessorDLManager().GetSkillDetail(skill);
        }

        public void SaveAssessor(AssessorDetail assessorDetails, List<SkillDetail> skillDetails)
        {
            // Create a transaction object.
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                AssessorDLManager dlManager = new AssessorDLManager();

                // Insert assessor.
                dlManager.SaveAssessorDetails
                    (assessorDetails, transaction);

                // Insert skills.
                if (skillDetails != null && skillDetails.Count > 0)
                {
                    dlManager.InsertAssessorSkill
                        (assessorDetails.UserID, skillDetails, transaction);
                }

                transaction.Commit();
            }
            catch (Exception exp)
            {
                // Rollback the transaction
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method to get the skill matched with assessor or not
        /// </summary>
        /// <param name="assessorID">assessorID</param>
        /// <param name="catSubID">catSubID</param>
        /// <returns></returns>
        public int GetSkillMatchedWithAssessor(int assessorID, int catSubID)
        {
            return new AssessorDLManager().GetSkillMatchedWithAssessor(assessorID, catSubID);
        }

        public List<AttributeDetail> GetAssessorEventsCount(int assessorID)
        { 
            return new AssessorDLManager().GetAssessorEventsCount(assessorID);
        }

        /// <summary>
        /// Method thats save the assessor and skill details details
        /// </summary>
        /// <param name="assessorDetails"></param>
        /// <param name="skillDetails"></param>
        public void SaveAssessor(AssessorDetail assessorDetails,
            List<SkillDetail> skillDetails, DbTransaction transaction)
        {
            try
            {
                AssessorDLManager dlManager = new AssessorDLManager();

                // Insert assessor.
                dlManager.SaveAssessorDetails
                    (assessorDetails, transaction);

                // Insert skills.
                if (skillDetails != null && skillDetails.Count > 0)
                {
                    dlManager.InsertAssessorSkill
                        (assessorDetails.UserID, skillDetails, transaction);
                }
            }
            catch (Exception exp)
            {
                // Rollback the transaction
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that udpate the external assessor to registered user.
        /// </summary>
        /// <param name="externalAssessorID">
        /// A <see cref="int"/> that holds the external assessor ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void UpdateExternalAssessorToRegisteredUser(int externalAssessorID, int userID, string email,
            IDbTransaction transaction)
        {
            new AssessorDLManager().UpdateExternalAssessorToRegisteredUser(
                externalAssessorID, userID, email, transaction);
        }

        /// <summary>
        /// Method that retreives assessor status against 
        /// this candidate session key 
        /// </summary>
        /// <param name="candidateInterviewSessionKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status true or false
        /// [True- Assessor already involved against this session]
        /// </returns>
        public bool IsAssessorInvolvedAgainstThisCandidateSession(string candidateInterviewSessionKey,
            int attemptID, int assessorID)
        {
            return new AssessorDLManager().IsAssessorInvolvedAgainstThisCandidateSession(
                candidateInterviewSessionKey, attemptID, assessorID);
        }

        /// <summary>
        /// Method that search for the assessor against the selected date and time criteria
        /// </summary>
        /// <param name="timeSlotCriteria">
        /// A <see cref="AssessorTimeSlotDetail"/> that holds the time slot search criteria.
        /// </param>
        /// <returns>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortType"/> that holds the sort order.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor time slot details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetAssessorAvailableDates(AssessorTimeSlotSearchCriteria
            timeSlotCriteria, int tenantID, int pageNumber,
            int pageSize, string sortField, SortType sortOrder, out int totalRecords)
        {
            return new AssessorDLManager().
                GetAssessorAvailableDates(timeSlotCriteria, tenantID, 
                sortField, sortOrder, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that gets the list of list requested candidated slots
        /// </summary>
        /// <param name="slotKey">
        /// A <see cref="string"/> that holds the slot key.
        /// </param>
        /// <returns>
        /// <see cref="AssessorTimeSlotDetail"/> that holds the assessor time slot details.
        /// </returns>
        public AssessorTimeSlotDetail GetCandidateSlotDetail(string slotKey)
        {
            return new AssessorDLManager().
                GetCandidateSlotDetail(slotKey);
        }

        /// <summary>
        /// Method that updates the slot key which is approved by the client
        /// </summary>
        /// <param name="requestSlotID">
        /// A <see cref="int"/> that holds the requested id.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id.
        /// </param>
        public void ApproveSlotIDByCandidate(int requestSlotID,
            int userID)
        {
            new AssessorDLManager().
                ApproveSlotIDByCandidate(requestSlotID, userID);
        }


        /// <summary>
        /// Methot that inserts the list of selected time 
        /// slot against the date(referred with assessor availability date).
        /// </summary>
        /// <param name="timeSlots">
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        public void SaveAssessorAvailableDate(string mode, AssessorTimeSlotDetail dateAvailabilty,
            List<AssessorTimeSlotDetail> timeSlots)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            string retval = string.Empty;

            try
            {
                AssessorDLManager dlManager = new AssessorDLManager();

                if (mode == "A" && !Utility.IsNullOrEmpty(dateAvailabilty)) 
                {
                    dateAvailabilty.RequestDateGenID = new OnlineInterviewAssessorDLManager().
                        InsertAssessorAvailabilityDates(dateAvailabilty, transaction);
                }
                else
                {
                    //Update the request date status
                    new AssessorDLManager().UpdateAssessorAvailabilityDates(dateAvailabilty, transaction);
                }

                string availableTimeids = string.Empty;

                // Loop through the time slots and save.
                foreach (AssessorTimeSlotDetail timeSlot in timeSlots)
                {
                    dlManager.InsertAssessorTimeSlot(dateAvailabilty.RequestDateGenID, timeSlot,
                        dateAvailabilty.InitiatedBy, transaction);
                    availableTimeids = availableTimeids + timeSlot.TimeSlotIDFrom + ",";
                }

                if (!Utility.IsNullOrEmpty(availableTimeids))
                {
                    //Delete the deselected available times
                    availableTimeids = availableTimeids.ToString().Trim().TrimEnd(',');
                    dlManager.DeleteAssessorTimeSlot(dateAvailabilty.RequestDateGenID, availableTimeids, transaction);
                }

                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        /// <summary>
        /// Methot that inserts the list of selected time 
        /// slot against the date(referred with assessor availability date).
        /// </summary>
        /// <param name="timeSlots">
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        public void UpdateAssessorAvailableDateTimeSlots(string mode, AssessorTimeSlotDetail proposedDateDetail,
            List<AssessorTimeSlotDetail> timeSlots)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            string retval = string.Empty;

            try
            {
                AssessorDLManager dlManager = new AssessorDLManager();
                if (mode == "P" && !Utility.IsNullOrEmpty(proposedDateDetail))
                {
                    proposedDateDetail.RequestDateGenID = new OnlineInterviewAssessorDLManager().
                        InsertAssessorAvailabilityDates(proposedDateDetail, transaction);
                }
                else
                {
                    //Update the request date status
                    new AssessorDLManager().UpdateAssessorAvailabilityDates(proposedDateDetail, transaction);
                }
                 
                // Loop through the time slots and save.
                foreach (AssessorTimeSlotDetail timeSlot in timeSlots)
                {
                    dlManager.UpdateAssessorAvailableDateTimeSlots(proposedDateDetail.RequestDateGenID,
                        proposedDateDetail.InitiatedBy, timeSlot, transaction); 
                } 
             
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        public int GetAssessorAvailableDateGenId(int assessorId, DateTime requestDate)
        {
            return new AssessorDLManager().GetAssessorAvailableDateGenId(assessorId, requestDate);
        }

    }
}
