﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalDataBLManager.cs
// File that represents the business layer for the external data manager
// component. This will talk to the data layer to perform the operations 
// associated with the external data manager component.

#endregion Header

#region Directives

using Forte.HCM.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the external data manager
    /// component. This will talk to the data layer to perform the operations 
    /// associated with the external data manager component.
    /// </summary>
    public class ExternalDataBLManager
    {
        /// <summary>
        /// Method that represents the constructor.
        /// </summary>
        public ExternalDataBLManager()
        {
        }

        /// <summary>
        /// Method that retreives the external data structure for the given key.
        /// </summary>
        /// <param name="dataKey">
        /// A <see cref="string"/> that holds the data key.
        /// </param> 
        /// <returns>
        /// A <see cref="ExternalDataStructure"/> that holds the external data 
        /// structure.
        /// </returns>
        public ExternalDataStructure GetExternalDataStructure(string dataKey)
        {
            return new ExternalDataDLManager().GetExternalDataStructure(dataKey);
        }


        /// <summary>
        /// Method that retrieves the position profile details.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileDetail"/> that holds the position profile detail.
        /// </returns>
        public PositionProfileDetail GetPositionProfile(int positionProfileID)
        {
            return new ExternalDataDLManager().GetPositionProfile(positionProfileID);
        }

        /// <summary>
        /// Method that retreives the candidate detail.
        /// </summary>
        /// <param name="candidateUserID">
        /// A <see cref="int"/> that holds the candidate login ID.
        /// </param> 
        /// <returns>
        /// A <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </returns>
        public CandidateDetail GetCandidate(int candidateLoginID)
        {
            return new ExternalDataDLManager().GetCandidate(candidateLoginID);
        }
    }
}
