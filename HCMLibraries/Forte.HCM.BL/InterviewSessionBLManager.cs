﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewSessionBLManager.cs
// File that represents the data layer for the Interview Session module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion

namespace Forte.HCM.BL
{
    public class InterviewSessionBLManager
    {
        #region Public Method

        /// <summary>
        /// Method that retrieves the list of interview sessions for the given 
        /// search criteria.
        /// </summary>
        /// <param name="interviewSessionSearchCriteria"> 
        /// A <see cref="InterviewSessionSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="total">
        /// A <see cref="int"/> that holds the total records in the list as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewSessionSearch"/> that holds the interview 
        /// sessions.
        /// </returns>
        public List<TestSessionSearch> GetInterviewSessionDetails(TestSessionSearchCriteria
            testSessionSearchCriteria, int PageNumber, int PageSize,
            string orderBy, SortType direction, out int Total)
        {
            return new InterviewSessionDLManager().GetInterviewSessionDetails(testSessionSearchCriteria, PageNumber, 
                PageSize, orderBy, direction, out Total);
        }

        /// <summary>
        /// Method that retrieves the list of interview sessions for the given 
        /// search criteria.
        /// </summary>
        /// <param name="interviewSessionSearchCriteria"> 
        /// A <see cref="InterviewSessionSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="total">
        /// A <see cref="int"/> that holds the total records in the list as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewSessionSearch"/> that holds the interview 
        /// sessions.
        /// </returns>
        public List<TestSessionSearch> GetOnlineInterviewSessionDetails(TestSessionSearchCriteria
           testSessionSearchCriteria, int pageNumber, int pageSize, string orderBy,
           SortType direction, out int total)
        {
            return new InterviewSessionDLManager().GetOnlineInterviewSessionDetails
                (testSessionSearchCriteria, pageNumber, pageSize, orderBy,
                direction, out total);
        }

        /// <summary>
        /// Represents the method to get the bool value to show candidate
        /// test score
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the candidate session id
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds whether to display score or not
        /// </returns>
        public bool GetInterviewShowTestScore(string candidateSessionID)
        {
            return new InterviewSessionDLManager().GetInterviewShowTestScore(candidateSessionID);
        }

        /// <summary>
        /// This mehtod updates the interview session
        /// </summary>
        /// <param name="interviewSessionDetail">
        /// A <see cref="InterviewTestSessionDetail"/> that holds the interview session details
        /// </param>
        /// <param name="assessorDetails">
        /// A <see cref="AssessorDetail"/> that holds the list of assessor against this interview session
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id
        /// </param>
        /// <param name="interviewSessionID">
        /// A <see cref="string"/>that holds the test session id
        /// </param>
        /// <param name="candidateInterviewStatus">
        /// A <see cref="string"/>that holds the candidate interview status
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that holds the candidate session id
        /// </param>
        /// <param name="numberOfSessions">
        /// A <see cref="int"/> that holds the session counts out parameter
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> Whether check mail sent or not.
        /// </param>
        public void UpdateInterviewTestSession(InterviewTestSessionDetail interviewSessionDetail, 
                List<AssessorDetail> assessorDetails, int userID, string interviewSessionID, 
            string candidateInterviewStatus, out string candidateSessionIDs,
            int numberOfSessions, out bool isMailSent)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            InterviewSessionDLManager interviewSessionDLManager = new InterviewSessionDLManager();
            NextNumberDLManager nextNumberDLManager = new NextNumberDLManager();
            isMailSent = false;

            candidateSessionIDs = string.Empty;
            try
            {
                if (numberOfSessions != 0)
                {
                    // Insert candidate test session
                    for (int candidateSessionCount = 0; candidateSessionCount <
                        numberOfSessions; candidateSessionCount++)
                    {
                        CandidateTestSessionDetail candidateSessionDetail = new CandidateTestSessionDetail();

                        // Get candidate session id
                        string candidateSessionID = nextNumberDLManager.GetNextNumber(transaction,
                            Constants.ObjectTypes.CANDIDATE_INTERVIEW_SESSION, userID);

                        if (candidateSessionIDs.Trim().Length == 0)
                            candidateSessionIDs = candidateSessionID;
                        else
                            candidateSessionIDs += ", " + candidateSessionID;

                        // Assign candidate interview session details
                        candidateSessionDetail.CandidateTestSessionID = candidateSessionID.ToString().Trim();
                        candidateSessionDetail.TestSessionID = interviewSessionID;
                        candidateSessionDetail.Status = Constants.CandidateSessionStatus.NOT_SCHEDULED;
                        candidateSessionDetail.CreatedBy = interviewSessionDetail.CreatedBy;
                        candidateSessionDetail.ModifiedBy = interviewSessionDetail.ModifiedBy;

                        // Add candidate interview session detail to the list
                        interviewSessionDLManager.InsertCandidateTestSession(candidateSessionDetail, transaction);
                    }
                }

                //Update assessor & skill id
                if (!Utility.IsNullOrEmpty(assessorDetails))
                {
                    int assessorID = 0;
                    string skillIds = string.Empty;
                    string assessorIds = string.Empty;

                    foreach (AssessorDetail assessorDetail in assessorDetails)
                    {
                        if (!Utility.IsNullOrEmpty(assessorDetail.Skill))
                        {
                            //Insert if any new skill added
                            //Delete if any existing skill removed from matrix
                            assessorID = assessorDetail.UserID;
                            skillIds = assessorDetail.Skill.ToString();
                            interviewSessionDLManager.UpdateInterviewSessionAssessorSkills(interviewSessionID, assessorID,
                                skillIds, userID, transaction);

                            //Insert/Update newly added assessor and skill to scheduled/completed candidate sessions
                            interviewSessionDLManager.InsertAssessorForScheduledAndCompletedCandidates(interviewSessionID,
                                assessorID, skillIds, candidateInterviewStatus, userID, transaction);

                            assessorIds = assessorIds + assessorID + ",";
                        }
                    }

                    if (!Utility.IsNullOrEmpty(assessorIds))
                    {
                        //Delete assessor and skill if any existing assessor removed from the matrix
                        assessorIds = assessorIds.ToString().Trim().TrimEnd(',');
                        interviewSessionDLManager.DeleteInterviewSessionAssessorSkills(interviewSessionID, assessorIds, transaction);
                    }
                }

                // Update interview session
                interviewSessionDLManager.UpdateInterviewTestSession(interviewSessionDetail, userID, transaction);

                // Update test description
                interviewSessionDLManager.UpdateTestDescription(interviewSessionDetail, userID, transaction);

                // Commit the transaction
                transaction.Commit();

                // Send Mail
                try
                {
                    if (candidateSessionIDs != null && candidateSessionIDs.TrimEnd(',').Length > 0)
                    {
                        // Send email to the candidate who is requested to reschedule
                        new EmailHandler().SendMail
                            (EntityType.TestSessionModified, interviewSessionID, candidateSessionIDs.TrimEnd(','));

                        // Mail sent successfully.
                        isMailSent = true;
                    }
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        /// <summary>
        /// Saves the test session and the candidate session details to the respective repositories.
        /// i.e. INTEVIEW_TEST_SESSION, SESSION_CANDIDATE
        /// </summary>
        /// <param name="testSessionDetail">
        /// A <see cref="TestSessionDetail"/> that contains the Test Session Detail collection.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creator of the test session.
        /// </param>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the Test session Id.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate Session IDs.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the Mail Sent Or Not.
        /// </param>
        public void SaveInterviewSession(InterviewSessionDetail interviewSessionDetail,
                List<AssessorDetail> assessorDetails, int userID,
            out string interviewSessionID, out string candidateSessionIDs, out bool isMailSent)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            InterviewSessionDLManager interviewSessionDLManager = new InterviewSessionDLManager();
            NextNumberDLManager nextNumberDLManager = new NextNumberDLManager();

            interviewSessionID = string.Empty;
            candidateSessionIDs = string.Empty;
            isMailSent = false;
            try
            {
                if (interviewSessionDetail.InterviewTestSessionID == null)
                {
                    // Generate test session id and assign to a string variable
                    interviewSessionID = nextNumberDLManager.GetNextNumber(transaction, "OIS", userID);
                    interviewSessionDetail.InterviewTestSessionID = interviewSessionID;

                    // Insert test session
                    interviewSessionDLManager.InsertInterviewTestSession(interviewSessionDetail, userID, transaction);

                    //Insert skill id based on the assessorid
                    if (assessorDetails != null)
                    {
                        int assessorId = 0;
                        string assessorSkillIds = string.Empty;

                        foreach (AssessorDetail assessorDetail in assessorDetails)
                        {
                            if (!Utility.IsNullOrEmpty(assessorDetail.Skill))
                            {
                                assessorId = assessorDetail.UserID;
                                assessorSkillIds = assessorDetail.Skill.ToString();
                                interviewSessionDLManager.InsertInterviewSessionAssessorSkills(interviewSessionID,
                                    assessorId, assessorSkillIds, userID, transaction);
                            }
                        }
                    }
                   
                    // Insert candidate test session
                    for (int candidateSessionCount = 0; candidateSessionCount <
                        interviewSessionDetail.NumberOfCandidateSessions; candidateSessionCount++)
                    {
                        CandidateInterviewSessionDetail candidateinterviewSessionDetail = new CandidateInterviewSessionDetail();

                        // Get candidate session id
                        string candidateSessionID = nextNumberDLManager.GetNextNumber(transaction, "COI", userID);

                        if (candidateSessionIDs.Trim().Length == 0)
                            candidateSessionIDs = candidateSessionID;
                        else
                            candidateSessionIDs += ", " + candidateSessionID;

                        // Assign candidate test session details
                        candidateinterviewSessionDetail.CandidateTestSessionID = candidateSessionID.ToString().Trim();
                        candidateinterviewSessionDetail.InterviewTestSessionID = interviewSessionID.Trim();
                        candidateinterviewSessionDetail.Status = Constants.CandidateSessionStatus.NOT_SCHEDULED;
                        candidateinterviewSessionDetail.CreatedBy = interviewSessionDetail.CreatedBy;
                        candidateinterviewSessionDetail.ModifiedBy = interviewSessionDetail.ModifiedBy;

                        // Add candidate test session detail to the list
                        interviewSessionDLManager.InsertCandidateInterviewSession(candidateinterviewSessionDetail, interviewSessionDetail.AssessorIDs, transaction);
                    }

                    // Insert test description
                    interviewSessionDLManager.InsertInterviewSessionDescription(interviewSessionDetail, transaction);
                }

                // Commit the transaction
                transaction.Commit();

                // Send Mail
                try
                {
                    // Send email to the candidate who is requested to reschedule
                    new EmailHandler().SendMail
                        (EntityType.InterviewSessionCreated, interviewSessionID);

                    // Mail sent successfully.
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testID"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="sortDirection"></param>
        /// <param name="userID"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public List<CandidateInterviewSessionDetail> GetCandidateInterviewSessions(string interviewTestkey, int pageNumber,
           int pageSize, string orderBy, SortType sortDirection, int userID, out int totalRecords)
        {
            return new InterviewSessionDLManager().GetCandidateInterviewSessions(interviewTestkey, pageNumber,
                pageSize, orderBy, sortDirection, userID, out totalRecords);
        }

        /// <summary>
        /// Method that will return the list of candidate test session details.
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="string"/> that contains the sort order either ASC/DESC.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateTestSessionDetail"/> that contains information
        /// about the candidate test session details.
        /// </returns>
        public List<CandidateTestSessionDetail> GetCandidateTestSessions(string testID, int pageNumber,
            int pageSize, string orderBy, SortType sortDirection, int userID, out int totalRecords)
        {
            return new InterviewDLManager().GetCandidateTestSessions(testID, pageNumber,
                pageSize, orderBy, sortDirection, userID, out totalRecords);
        }

        /// <summary>
        /// Retrieves subject id list 
        /// </summary>
        /// <param name="interviewTestKey"></param>
        /// <returns></returns>
        public string GetInterviewTestSubjectIdsByKey(string interviewTestKey)
        {
            return new InterviewSessionDLManager().GetInterviewTestSubjectIdsByKey(interviewTestKey);
        }
        /// <summary>
        /// Method that retrieves list of assesor 
        /// (Display only assessors that have that have provided time slots) 
        /// </summary>
        /// <param name="referenceType">
        /// A <see cref="string"/> that holds the reference type.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status ids.
        /// </param>
        /// <returns>
        /// A <see cref="DataTatable"/> that holds the summary data.
        /// </returns>
        public DataTable GetAssessorListByHaveProvidedTimeSlots(string status)
        {
            return new InterviewSessionDLManager().GetAssessorListByHaveProvidedTimeSlots(status);
        }

        /// <summary>
        /// Method that retrieves the assessor and skill id against its interview session key
        /// </summary>
        /// <param name="interviewSessionkey">
        /// A <see cref="string"/> that holds the @interviewSessionkey type.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status ids.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetOnlineAssessorAndSkillIdBySessionKey(string interviewSessionkey)
        {
            return new InterviewSessionDLManager().GetOnlineAssessorAndSkillIdBySessionKey(interviewSessionkey);
        }

        /// <summary>
        /// Method that retrieves the assessor and skill id against its interview session key or candidate key
        /// </summary>
        /// <param name="key">
        /// A <see cref="string"/> that holds the key type.
        /// </param>
        /// <param name="type">
        /// A <see cref="type"/> that holds the Type.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the positionProfileID.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetAssessorAndSkillBySessionOrCandidateKey(
            string key, string type, int positionProfileID)
        {
            return new InterviewSessionDLManager().
                GetAssessorAndSkillBySessionOrCandidateKey(key, type, positionProfileID);
        }

        /// <summary>
        /// Method that retrieves the assessor and skill id against its interview session key or candidate key
        /// </summary>
        /// <param name="key">
        /// A <see cref="string"/> that holds the key type.
        /// </param>
        /// <param name="type">
        /// A <see cref="type"/> that holds the Type.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetOnlineAssessorAndSkillBySessionOrCandidateKey(
            string key, string type)
        {
            return new InterviewSessionDLManager().
                GetOnlineAssessorAndSkillBySessionOrCandidateKey(key, type);
        }
        
        /// <summary>
        /// Method gets the interview session id details using the parameter navigatekey
        /// </summary>
        /// A<param name="navigateKey">holds the navigate key</param>
        /// <returns></returns>
        public InterviewSessionIdDetails GetInterviewSessionIdDetails(string navigateKey)
        {
            return new InterviewSessionDLManager().GetInterviewSessionIdDetails(navigateKey);
        }

        /// <summary>
        /// Method that retrieves the list of skills for the given 
        /// candidate session id
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetSkillsByCandidateSessionKey(
            string candidateSessionKey)
        {
            return new InterviewSessionDLManager().
                GetSkillsByCandidateSessionKey(candidateSessionKey);
        }


        /// <summary>
        /// Saves the test session and the candidate session details to the respective repositories.
        /// i.e. INTEVIEW_TEST_SESSION, SESSION_CANDIDATE
        /// </summary>
        /// <param name="CandidateInterviewSessionDetail">
        /// A <see cref="CandidateInterviewSessionDetail"/> that contains the request for the candidate session details.
        /// </param>
        /// <param name="skill">
        /// A <see cref="skill"/> that contains the list of skill ids.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that contains the requested email id.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creator of the test session.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the Mail Sent Or Not.
        /// </param>
        public List<ExternalAssessorDetail> SaveExternalAssessorDetail(List<int> skill, 
            string candidateSessionKey, int attemptID, string email, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            InterviewSessionDLManager interviewSessionDLManager = new InterviewSessionDLManager();
            InterviewSchedulerDLManager interviewSchedulerDLManager = new InterviewSchedulerDLManager();
            
            int externalAssessorID = 0;

            List<ExternalAssessorDetail> externalAssessorDetails = null;
            try
            {
                if (!Utility.IsNullOrEmpty(candidateSessionKey))
                {
                    string[] emailIDs = email.Split(new char[] { ',' });

                    var skillids = string.Join(",", skill.Select(s => s.ToString().Trim()).ToArray());

                    foreach (string emailId in emailIDs)
                    {
                        // Instantiate external assesor list.
                        if (externalAssessorDetails == null)
                            externalAssessorDetails = new List<ExternalAssessorDetail>();

                        // Create a external assesor object.
                        ExternalAssessorDetail externalAssessorDetail = new ExternalAssessorDetail();

                        //Check for existing user
                        externalAssessorID = new CandidateBLManager().GetUserID(emailId.Trim());

                        if (externalAssessorID != 0)
                        {
                            externalAssessorDetail.Email = emailId.Trim();
                            externalAssessorDetail.ExternalKey = null;
                        }
                        else
                        {
                            //Generate key
                            string externalKey = GenerateKey();

                            // Insert external assessor details
                            externalAssessorID = interviewSessionDLManager.
                                InsertExternalAssessorDetail(emailId, userID, transaction);

                            if (externalAssessorID != 0)
                            {
                                externalAssessorDetail.Email = emailId;
                                //Insert external assessor candidate session details
                                externalAssessorDetail.ExternalKey = interviewSessionDLManager.
                                    InsertExternalAssessorCandidateSessionDetail(candidateSessionKey, attemptID,
                                    externalAssessorID, externalKey, userID, transaction);
                            }
                        }

                        externalAssessorDetail.ExternalAssessorID = externalAssessorID;
                        externalAssessorDetail.CandidateInterviewSessionkey = candidateSessionKey;
                        externalAssessorDetail.AttemptID = attemptID;
                        externalAssessorDetails.Add(externalAssessorDetail);

                        //Insert external assessor assigned skill
                        interviewSchedulerDLManager.UpdateInterviewSessionAssessorAndSkill(candidateSessionKey,
                        externalAssessorID, attemptID, skillids, userID, transaction);
                    }
                }

                // Commit the transaction
                transaction.Commit();

                return externalAssessorDetails;
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// This method gets the external assessor details against its unique key.
        /// </summary>
        /// <param name="externalKey">
        /// A <see cref="string"/> that contains external generated key.
        /// </param>
        /// <returns>
        /// A <see cref="ExternalAssessorDetail"/> that contains external assessor details.
        /// </returns>
        public ExternalAssessorDetail GetExternalAssessorDetailByKey(string externalKey)
        {
            return new InterviewSessionDLManager().GetExternalAssessorDetailByKey(externalKey);
        }

        /// <summary>
        /// Method that updates external assessor detail
        /// </summary>
        /// <param name="externalAssessorDetail">
        /// A <see cref="externalAssessorDetail"/> that holds the external assessor detail.
        /// </param>
        public void UpdateExternalAssessorDetail(ExternalAssessorDetail externalAssessorDetail)
        {
            new InterviewSessionDLManager().UpdateExternalAssessorDetail(externalAssessorDetail);
        }

        #endregion

        #region Private Method

        /// <summary>
        /// Method that generates external key
        /// </summary>
        /// <returns>That returns the external key</returns>
        private string GenerateKey()
        {
            bool isExist = true;
            string key = string.Empty;

            while (isExist)
            {
                key = Utility.RandomString(10);
                isExist = new InterviewSessionDLManager().IsExternalKeyExists(key);
            }
            return key;
        }

        #endregion Private Method
    }
}
