﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AuthenticationBLManager.cs
// File that represents the business layer for the Authentication and 
// Authorization module. This includes functionalities for retrieving 
// the authenticate user, role based authorized user, etc. This will talk
// to the data layer for performing these operations.

#endregion

#region Directives
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.DataObjects;
using System.Data;

using System.Linq;
using System;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the Authentication and 
    /// Authorization module. This includes functionalities for retrieving 
    /// the authenticate user, role based authorized user, etc. This will talk
    /// to the data layer for performing these operations.
    /// </summary>
    public class AuthenticationBLManager
    {
        #region Public Methods

        /// <summary>
        /// Method that validates the user and returns the user type and 
        /// session key.
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the User details
        /// </returns>
        public string GetSiteUserDetail(string userName, string password)
        {
            return new AuthenticationDLManager().GetSiteUserDetail(userName, password);
        }

        /// <summary>
        /// Validating the User
        /// </summary>
        /// <param name="authentication">
        /// A <see cref="string"/> that holds the username
        /// A <see cref="string"/> that holds the password
        /// </param>
        /// <returns>Return the User details</returns>
        public UserDetail GetAuthenticateUser(Authenticate authentication)
        {
            return new AuthenticationDLManager().GetAuthenticateUser(authentication);
        }

        /// <summary>
        /// Method that validates the user based on the session key.
        /// </summary>
        /// <param name="sessionKey">
        /// A <see cref="string"/> that holds the session key.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        /// <remarks>
        /// This method helps to validate a user when logged in from the web
        /// site.
        /// </remarks>
        public UserDetail GetAuthenticateUser(string sessionKey)
        {
            return new AuthenticationDLManager().GetAuthenticateUser(sessionKey);
        }

        /// <summary>
        /// Method that authenticates a candidate. This will check for 
        /// user name/password, candidate role and activity count.
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        public UserDetail GetAuthenticateCandidate(string userName, string password, int tenantID)
        {
            return new AuthenticationDLManager().GetAuthenticateCandidate(userName, password, tenantID);
        }

        /// <summary>
        /// Method that validates the candidate based on the session key.
        /// </summary>
        /// <param name="sessionKey">
        /// A <see cref="string"/> that holds the session key.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        /// <remarks>
        /// This method helps to validate a candidate when logged in from the web
        /// site.
        /// </remarks>
        public UserDetail GetAuthenticateCandidate(string sessionKey)
        {
            return new AuthenticationDLManager().GetAuthenticateCandidate(sessionKey);
        }

        /// <summary>
        /// Validating the Role based User
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user Id
        /// </param>
        /// <param name="featureUrl">
        /// A <see cref="string"/> that holds the featureUrl.
        /// </param>
        /// <returns></returns>
        public List<RoleRights> GetAuthorization(int userID, string featureUrl)
        {
            return new AuthenticationDLManager().GetAuthorization(userID, featureUrl);
        }



        /// <summary>
        /// Gets the role authorization.
        /// </summary>
        /// <param name="roleID">The role ID.</param>
        /// <returns></returns>
        public List<RoleManagementDetail> GetRoleAuthorization(int roleID)
        {
            return new AuthenticationDLManager().GetRoleAuthorization(roleID);
        }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <returns>
        /// the list of roles
        /// </returns>
        public List<Roles> GetRoles()
        {
            return new AuthenticationDLManager().GetRoles();
        }



        /// <summary>
        /// Gets the module list.
        /// </summary>
        /// <returns></returns>
        public List<Module> GetModuleList()
        {
            return new AuthenticationDLManager().GetModuleList();
        }

        /// <summary>
        /// Gets the role category list
        /// </summary>
        /// <param name="sortExpression"> that holds the expreession for sorting
        /// </param>
        /// <param name="sortOrder">that holds order for sorting</param>
        /// <param name="pageSize">that holds the page size</param>
        /// <param name="pageNumber">that holds the page number</param>
        /// <param name="totalPage">that holds the total page</param>
        /// <returns></returns>
        public List<RoleManagementDetail> GetRoleCategory(string sortExpression,
            string sortOrder, int pageSize, int? pageNumber, out int totalPage)
        {
            return new AuthenticationDLManager().GetRoleCategory(sortExpression, sortOrder,
                pageSize, pageNumber, out totalPage);
        }

        /// <summary>
        /// Update the role category
        /// </summary>
        /// <param name="roleCategoryCode">that holds the
        /// role category code</param>
        /// <param name="roleCategoryName">that holds the 
        /// role catgory name</param>
        /// <param name="roleCatgoryID">that holds the role
        /// catgory ID</param>
        /// <param name="userID">that holds the user ID</param>
        public int UpdateRoleCategory(string roleCategoryCode, string roleCategoryName, int roleCatgoryID, int userID)
        {
            return new AuthenticationDLManager().UpdateRoleCategory(roleCategoryCode, roleCategoryName, roleCatgoryID, userID);
        }

        /// <summary>
        /// Gets the role rights matrix.
        /// </summary>
        /// <param name="rolesList">The roles list.</param>
        /// <param name="modulesList">The modules list.</param>
        /// <returns></returns>
        public List<RoleRightMatrixDetail> GetRoleRightsMatrix(string rolesList, string modulesList)
        {
            return new AuthenticationDLManager().GetRoleRightMatrix(rolesList, modulesList);
        }
        /// <summary>
        /// Delete the role category
        /// </summary>
        /// <param name="roleID">that holds the role ID</param>
        public void DeleteRoleCategory(int roleID)
        {
            new AuthenticationDLManager().DeleteRoleCategory(roleID);
        }

        /// <summary>
        /// Insert new role catgory
        /// </summary>
        /// <param name="roleCategorycode">that holds the role
        /// category code</param>
        /// <param name="roleCategoryName">that holds the role
        /// category name</param>
        /// <param name="rolecategoryCreatedBy"></param>
        public int InsertNewCategoryRole(string roleCategorycode, string roleCategoryName,
            int rolecategoryCreatedBy)
        {
            return new AuthenticationDLManager().InsertNewCategoryRole(roleCategorycode, roleCategoryName
                 , rolecategoryCreatedBy);
        }

        /// <summary>
        /// Get the list of roles
        /// </summary>
        /// <param name="sortExpression">that holds sorting
        /// expression</param>
        /// <param name="sortOrder">that holds sorting order
        /// </param>
        /// <param name="pageNumber">that holds the page number
        /// </param>
        /// <returns></returns>
        public List<RoleManagementDetail> GetRoles(string sortExpression,
            string sortOrder,
            int pageNumber)
        {
            return new AuthenticationDLManager().GetRole(sortExpression, sortOrder,
                pageNumber);
        }


        /// <summary>
        /// Insert new role
        /// </summary>
        /// <param name="roleCode">holds the role code</param>
        /// <param name="roleCatID">holds the role catgory ID</param>
        /// <param name="roleJobDesc">holds the role job description</param>
        /// <param name="roleName">holds the role name</param>
        /// <param name="createdBy">holds the user ID </param>
        /// <returns></returns>
        public int InsertNewRole(string roleCode, int roleCatID, string roleJobDesc,
                string roleName, int createdBy)
        {
            return new AuthenticationDLManager().InsertNewRole(roleCode, roleCatID,
                roleJobDesc, roleName, createdBy);
        }
        /// <summary>
        /// Update the roles
        /// </summary>
        /// <param name="roleCode">holds the role code
        /// </param>
        /// <param name="roleName">holds the role name
        /// </param>
        /// <param name="roleID">holds the role ID
        /// </param>
        /// <param name="modifiedBy"></param>
        public int UpdateRoles(string roleCode, string roleName, int roleID, int modifiedBy)
        {
            return new AuthenticationDLManager().UpdateRoles(roleCode, roleName, roleID, modifiedBy);
        }

        /// <summary>
        /// Delete the roles
        /// </summary>
        /// <param name="roleID">holds the role ID</param>
        public void DeleteRoles(int roleID)
        {
            new AuthenticationDLManager().DeleteRoles(roleID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleDetails"></param>
        public void UpdateRoleDetails(List<RoleManagementDetail> roleDetails)
        {
            new AuthenticationDLManager().UpdateRoleDetails(roleDetails);
        }


        /// <summary>
        /// Updates the role matrix details.
        /// </summary>
        /// <param name="roleMatrixDetails">The role matrix details.</param>
        public void UpdateRoleMatrixDetails(List<RoleRightMatrixDetail> roleMatrixDetails)
        {
            foreach (RoleRightMatrixDetail role in roleMatrixDetails)
            {
                new AuthenticationDLManager().UpdateRoleMatrixDetails(role);
            }
        }

        /// <summary>
        /// Gets the role rights matrix.
        /// </summary>
        /// <param name="rolesList">The roles list.</param>
        /// <param name="modulesList">The modules list.</param>
        /// <returns></returns>
        public DataTable GetRoleRightsMatrixDataTable(string rolesList, string modulesList)
        {
            List<RoleRightMatrixDetail> matrixDetails = new AuthenticationDLManager().GetRoleRightMatrix(rolesList, modulesList);

            DataTable dt = new DataTable();

            var featrueName = (from rrm in matrixDetails select new { rrm.FeatureID, rrm.FeatureName, rrm.SubModuleName }).Distinct().ToList();

            dt.Columns.Add(Support.Constants.RoleRightMatrixConstants.SUB_MODULE_NAME);

            dt.Columns.Add(Support.Constants.RoleRightMatrixConstants.FEATURE_NAME);

            dt.Columns.Add(Support.Constants.RoleRightMatrixConstants.FEATURE_ID);

            var roleName = (from rrm in matrixDetails select new { rrm.RoleID, rrm.RoleName }).Distinct().ToList();

            foreach (var item in roleName)
            {
                dt.Columns.Add(item.RoleName, Type.GetType("System.Boolean"));
            }

            foreach (var item in featrueName)
            {
                DataRow dataRow = dt.NewRow();

                dataRow[Support.Constants.RoleRightMatrixConstants.SUB_MODULE_NAME] = item.SubModuleName;

                dataRow[Support.Constants.RoleRightMatrixConstants.FEATURE_NAME] = item.FeatureName;

                dataRow[Support.Constants.RoleRightMatrixConstants.FEATURE_ID] = item.FeatureID;

                dt.Rows.Add(dataRow);
            }


            foreach (DataColumn column in dt.Columns)
            {
                if (column.ColumnName == Support.Constants.RoleRightMatrixConstants.FEATURE_NAME ||
                    column.ColumnName == Support.Constants.RoleRightMatrixConstants.FEATURE_ID ||
                    column.ColumnName == Support.Constants.RoleRightMatrixConstants.SUB_MODULE_NAME)
                {
                    continue;
                }

                foreach (DataRow row in dt.Rows)
                {
                    IEnumerable<bool> value = from rrm in matrixDetails
                                              where rrm.FeatureName == row[Support.Constants.RoleRightMatrixConstants.FEATURE_NAME].ToString() &&
                                              rrm.FeatureID == int.Parse(row[Support.Constants.RoleRightMatrixConstants.FEATURE_ID].ToString()) &&
                                              rrm.RoleName == column.ColumnName
                                              select rrm.IsApplicable;

                    foreach (var item in value)
                    {
                        row[column.ColumnName] = item == true ? 1 : 0;
                    }
                }
            }

            dt.Columns["FeatureID"].ColumnMapping = MappingType.Hidden;

            return dt;
        }

        /// <summary>
        /// Updates the role matrix details.
        /// </summary>
        /// <param name="datatable">The datatable.</param>
        public void UpdateRoleMatrixDetails(DataTable datatable)
        {
            RoleRightMatrixDetail role = new RoleRightMatrixDetail();

            //Start searching form the third column as it contains the feature name
            for (int i = Support.Constants.RoleRightMatrixConstants.STARTING_COLUMN_ID; i < datatable.Columns.Count; i++)
            {
                for (int j = 0; j < datatable.Rows.Count; j++)
                {
                    if (datatable.Rows[j][i].ToString() != "")
                    {
                        role = new RoleRightMatrixDetail();
                        role.FeatureID = int.Parse(datatable.Rows[j]["FeatureID"].ToString());
                        role.FeatureName = datatable.Rows[j]["Feature Name"].ToString();
                        role.RoleName = datatable.Columns[i].ColumnName;
                        role.IsApplicable = Convert.ToBoolean(datatable.Rows[j][i].ToString());

                        new AuthenticationDLManager().UpdateRoleMatrixDetails(role);
                    }

                }
            }

        }


        /// <summary>
        /// Delete the role catgory details
        /// </summary>
        /// <param name="roleCategoryID">holds role category ID
        /// </param>
        /// <returns></returns>
        public string DeleteRoleCategoryResult(string roleCategoryID)
        {
            return new AuthenticationDLManager().DeleteRoleCategoryResult(roleCategoryID);
        }

        /// <summary>
        /// Gets the list of user role rights
        /// </summary>
        /// <param name="sortExpression">holds the sorting expression
        /// </param>
        /// <param name="sortOrder">holds the sorting order
        /// </param>
        /// <returns></returns>
        public List<UsersRoleMatrix> GetUserRoleRights(string sortExpression, string sortOrder)
        {
            return new AuthenticationDLManager().GetAllUserRoleRights(sortExpression, sortOrder);
        }


        /// <summary>
        /// Gets the already existing roles rights for the user
        /// </summary>
        /// <param name="sortExpression">holds the sorting expression
        /// </param>
        /// <param name="sortOrder">holds the sorting order</param>
        /// <param name="userID">holds the user ID</param>
        /// <returns></returns>
        public List<UsersRoleMatrix> GetAppliacableUserRoleRights(string sortExpression, string sortOrder, int userID)
        {
            return new AuthenticationDLManager().GetAppliacableUserRoleRights(sortExpression, sortOrder, userID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public List<UsersRoleMatrix> GetAllUserRoleRights(string sortExpression, string sortOrder)
        {
            return new AuthenticationDLManager().GetAllUserRoleRights(sortExpression, sortOrder);
        }

        /// <summary>
        /// Insert new user roles
        /// </summary>
        /// <param name="userID">holds the user ID</param>
        /// <param name="roleID">holds the role ID</param>
        /// <param name="createdBy">holds the  created by</param>
        public void InsertNewUserRoles(int userID, int roleID, int createdBy)
        {
            new AuthenticationDLManager().InsertNewUserRoles(userID, roleID
                , createdBy);
        }

        /// <summary>
        /// Delete user roles
        /// </summary>
        /// <param name="roleID">holds the role ID</param>
        public void DeleteUserRoles(int roleID,int userID)
        {
            new AuthenticationDLManager().DeleteUserRoles(roleID,userID);
        }

        /// <summary>
        /// Gets the user rights matrix details.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <param name="moduleIDs">The module I ds.</param>
        /// <param name="showAllFeature">The show all feature.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="gridPageSize">Size of the grid page.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDirection">The order by direction.</param>
        /// <param name="totalRecordCount">The total record count.</param>
        /// <returns></returns>
        public List<UserRightsMatrix> GetUserRightsMatrixDetails(string userID,
            string moduleIDs, int showAllFeature, int pageNumber, int gridPageSize, string orderBy,
            SortType orderByDirection, out int totalRecordCount)
        {
            return new AuthenticationDLManager().GetUserRightsMatrixDetails(userID,
                moduleIDs, showAllFeature, pageNumber, gridPageSize, orderBy, orderByDirection,
                out totalRecordCount);
        }

        /// <summary>
        /// Updates the user rights.
        /// </summary>
        /// <param name="userMatrixDeatils">
        /// The user matrix deatils.</param>
        public void UpdateUserRights(List<UserRightsMatrix> userMatrixDeatils)
        {
            foreach (UserRightsMatrix userRight in userMatrixDeatils)
            {
                if (!userRight.IsEditable)
                {
                    continue;
                }

                if (userRight.UserRightsID == null && userRight.IsApplicable && userRight.IsEditable)
                {
                    new AuthenticationDLManager().InsertUserRights(userRight);
                }
                else if (userRight.UserRightsID != null && !userRight.IsApplicable && userRight.IsEditable)
                {
                    new AuthenticationDLManager().DeleteUserRights(userRight);
                }
            }
        }

        /// <summary>
        /// Gets the feature rights details.
        /// </summary>
        /// <param name="moduleID">The module ID.</param>
        /// <param name="subModuleID">The sub module ID.</param>
        /// <param name="roleID">The role ID.</param>
        /// <returns></returns>
        public List<RoleRightMatrixDetail> GetFeatureRightsDetails(int moduleID,
            int subModuleID, int roleID)
        {
            return new AuthenticationDLManager().GetFeatureRightsDetails(moduleID, subModuleID, roleID);
        }

        /// <summary>
        /// Gets the sub module based on module.
        /// </summary>
        /// <param name="modID">The mod ID.</param>
        /// <returns></returns>
        public List<SubModule> GetSubModuleBasedOnModule(int modID)
        {
            return new AuthenticationDLManager().GetSubModuleBasedOnModule(modID);
        }

        /// <summary>
        /// Saves the feature rights details.
        /// </summary>
        /// <param name="fearueRightMatrix">The fearue right matrix.</param>
        public void SaveFeatureRightsDetails(List<RoleRightMatrixDetail> fearueRightMatrix)
        {
            foreach (RoleRightMatrixDetail item in fearueRightMatrix)
            {

                if (item.RoleRightID != 0 && !item.IsApplicable)
                {
                    new AuthenticationDLManager().DeleteRoleRights(item.RoleRightID);
                }
                if (item.RoleRightID == 0 && item.IsApplicable)
                {
                    new AuthenticationDLManager().InsertRoleRight(item);
                }

            }
        }

        /// <summary>
        /// Updates the internal user details.
        /// </summary>
        /// <param name="userDetail">The user detail.</param>
        /// <param name="modifiedBy">The modified by.</param>
        public void UpdateInternalUserDetails(UserDetail userDetail, int modifiedBy)
        {
            new AuthenticationDLManager().UpdateInternalUserDetail(userDetail, modifiedBy);
        }
        public void UpdateSubscriptionUserDetails(UserDetail userDetail, int modifiedBy)
        {
            new AuthenticationDLManager().UpdateSubscriptionUserDetail(userDetail, modifiedBy);
        }
        public void UpdateUserRightMatrix(UserRightsMatrix userRight)
        {

            if (userRight.IsActive == false && userRight.IsApplicable == true)
            {
                new AuthenticationDLManager().InsertUserRights(userRight);
            }
            else if (userRight.IsActive == true)
            {
                new AuthenticationDLManager().UpdateUserRights(userRight);
            }
        }

        public void UpdateCorporateUserRightMatrix(UserRightsMatrix userRight)
        {

            if (userRight.IsActive == false && userRight.IsApplicable == true)
            {
                new AuthenticationDLManager().InsertUserRights(userRight);
            }
            else if (userRight.IsActive == true)
            {
                new AuthenticationDLManager().UpdateCorporateUserRights(userRight);
            }
        }
        public string GetRolesForUser(int userID)
        {
            return new AuthenticationDLManager().GetRolesForUser(userID);
        }

        #endregion Public Methods


        public List<Roles> GetCorporateRole()
        {
            return new AuthenticationDLManager().GetCorporateRoles();
        }

        public List<Roles> GetAssignedCorporateRole(int tenantID)
        {
            return new AuthenticationDLManager().GetAssignedCorporateRole(tenantID);
        }
        public List<RoleRightMatrixDetail> GetCorporateFeatureRightsDetails( int roleID,int tenantID)
        {
            return new AuthenticationDLManager().GetCorporateFeatureRightsDetails(roleID,tenantID);
        }

        public List<Roles> GetCorporateSelectRole()
        {
            return new AuthenticationDLManager().GetCorporateSelectRole();
        }

        public List<UsersRoleMatrix> GetAllCorporateUserRoleRights(string sortExpression, string sortOrder, int tenantID)
        {
           
            return new AuthenticationDLManager().GetAllCorporateUserRoleRights(sortExpression, sortOrder,tenantID);

        }

        public void SaveCorporateFeatureRightsDetails(List<RoleRightMatrixDetail> fearueRightMatrix,int tenantID)
        {
           // new AuthenticationDLManager().DeleteCorporateRoleRights(tenantID);

            foreach (RoleRightMatrixDetail item in fearueRightMatrix)
            {

                if (!item.IsApplicable && item.IsChecked)
                 {
                     new AuthenticationDLManager().DeleteCorporateRoleRights(item.RoleRightID);
                 }
                if (item.IsApplicable && !item.IsChecked)
                {
                    new AuthenticationDLManager().InsertCorporateRoleRight(item);
                }

            }
        }

        public void InsertCorporateNewUserRoles(int userID, int roleID, int createdBy)
        {
            new AuthenticationDLManager().InsertCorporateNewUserRoles(userID, roleID
                , createdBy);
        }


    }
}
