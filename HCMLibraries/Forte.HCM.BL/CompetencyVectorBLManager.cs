﻿
#region Directives

using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Competency Vector management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for competency vectors. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    public class CompetencyVectorBLManager
    {
        /// <summary>
        /// Represents the constructor.
        /// </summary>
        public CompetencyVectorBLManager()
        {
        }

        /// <summary>
        /// This method gets the candidate vector table with selected
        /// candidate resume id.
        /// </summary>
        /// <param name="CandidateResumeID">Candidate resume id 
        /// for which to get competency vector table.</param>
        /// <returns>Dataset contains the competency vector table.</returns>
        public DataSet GetCompetencyVectorTable(int CandidateResumeID)
        {
            return new CompetencyVectorDLManager().GetCompetencyVectorTable(CandidateResumeID);
        }

        /// <summary>
        /// This method inserts candidate competency 
        /// in to the database.
        /// </summary>
        /// <param name="newCandidateCompetencyVectorList">
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int InsertCandidateCompetencyVectorWithTransaction(CandidateCompetencyVector newCandidateCompetencyVectorList,
            IDbTransaction transaction)
        {
            return new CompetencyVectorDLManager().InsertCandidateCompetencyVector
                 (newCandidateCompetencyVectorList, transaction);
        }

        /// <summary>
        /// This method updates the candidate competency vector list
        /// to the database.
        /// </summary>
        /// <param name="candidateCompetencyVector">
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int UpdateCandidateCompetencyVectorWithTransaction(CandidateCompetencyVector candidateCompetencyVector,
            IDbTransaction transaction)
        {
            return new CompetencyVectorDLManager().UpdateCandidateCompetencyVector(candidateCompetencyVector, transaction);
        }

        /// <summary>
        /// This method updates the candidate competency vectorId list
        /// to the database.
        /// </summary>
        /// <param name="candidateCompetencyVector">
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int UpdateCandidateCompetencyVectorIdWithTransaction(
            CandidateCompetencyVector CandidateCompetencyVectorChangeList, IDbTransaction transaction)
        {
            return new CompetencyVectorDLManager().UpdateCandidateCompetencyVectorIdWithTransaction(
                CandidateCompetencyVectorChangeList, transaction);
        }

        /// <summary>
        /// This method delete the vector for a candidate
        /// from the database.
        /// </summary>
        /// <param name="candidateCompetencyVector">
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <returns>No of rows effected.</returns>
        public int DeleteCandidateCompetencyVector(CandidateCompetencyVector candidateCompetencyVector)
        {
            return new CompetencyVectorDLManager().DeleteCandidateCompetencyVector(candidateCompetencyVector);
        }

        /// <summary>
        /// This method delete the vector for a candidate
        /// from the database with transaction.
        /// </summary>
        /// <param name="candidateCompetencyVector">
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">Transaction object that holds previous
        /// transaction</param>
        /// <returns>No of rows effected.</returns>
        public int DeleteCandidateCompetencyVectorWithTransaction(CandidateCompetencyVector candidateCompetencyVector,
            IDbTransaction transaction)
        {
            return new CompetencyVectorDLManager().DeleteCandidateCompetencyVectorWithTransaction(
                candidateCompetencyVector, transaction);
        }

        /// <summary>
        /// Gets the vector id from the group id and
        /// vector name
        /// </summary>
        /// <param name="Group_Id">Group id for which to get the
        /// vector id</param>
        /// <param name="Vector_Name">vector name for which to get the
        /// vector id</param>
        /// <returns>Vector id for the passed group id and vector name.</returns>
        public int GetVectorIDFromGroupIdAndVectorName1(int GroupId, string Vector_Name)
        {
            return new CompetencyVectorDLManager().GetVectorIDFromGroupIdAndVectorName(GroupId, Vector_Name);
        }

        /// <summary>
        /// This method handles Insert, Delete, Update, Update 
        /// to the DB
        /// </summary>
        /// <param name="DeleteCandidateCompetencyLists">The list of 
        /// CandidateCompetencyVector dataobject that contains only 
        /// delete candidate vectors</param>
        /// <param name="VectorIdChangeCompetencyList">The list of 
        /// CandidateCompetencyVector dataobject that contains only 
        /// vector change vectors</param>
        /// <param name="ModifiedCandidateCompetencyList">The list of 
        /// CandidateCompetencyVector dataobject that contains only 
        /// modified candidate vectors</param>
        /// <param name="NewCandidateCompetencyList">The list of 
        /// CandidateCompetencyVector dataobject that contains only 
        /// new candidate vectors</param>
        /// <returns>if Successfully inserts returns 1 else -1</returns>
        public int DBOperationsForCandidateCompetencyVector(List<CandidateCompetencyVector> DeleteCandidateCompetencyLists,
            List<CandidateCompetencyVector> VectorIdChangeCompetencyList,
            List<CandidateCompetencyVector> ModifiedCandidateCompetencyList, List<CandidateCompetencyVector> NewCandidateCompetencyList)
        {
            IDbTransaction transaction = null;
            try
            {
                transaction = new TransactionManager().Transaction;
                int LoopCount = 0;
                if (!Utility.IsNullOrEmpty(DeleteCandidateCompetencyLists))
                    for (LoopCount = 0; LoopCount < DeleteCandidateCompetencyLists.Count; LoopCount++)
                        DeleteCandidateCompetencyVectorWithTransaction(
                            DeleteCandidateCompetencyLists[LoopCount], transaction);
                if (!Utility.IsNullOrEmpty(VectorIdChangeCompetencyList))
                    for (LoopCount = 0; LoopCount < VectorIdChangeCompetencyList.Count; LoopCount++)
                        UpdateCandidateCompetencyVectorIdWithTransaction(
                            VectorIdChangeCompetencyList[LoopCount], transaction);
                if (!Utility.IsNullOrEmpty(ModifiedCandidateCompetencyList))
                    for (LoopCount = 0; LoopCount < ModifiedCandidateCompetencyList.Count; LoopCount++)
                        UpdateCandidateCompetencyVectorWithTransaction(
                            ModifiedCandidateCompetencyList[LoopCount], transaction);
                if (!Utility.IsNullOrEmpty(NewCandidateCompetencyList))
                    for (LoopCount = 0; LoopCount < NewCandidateCompetencyList.Count; LoopCount++)
                        InsertCandidateCompetencyVectorWithTransaction(
                            NewCandidateCompetencyList[LoopCount], transaction);
                transaction.Commit();
                return 1;
            }
            catch
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction.Rollback();
                throw;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction = null;
            }
        }

        /// <summary>
        /// This method returns the Competency Vector group table.
        /// </summary>
        /// <returns>Dataset contains the competency vector group table.</returns>
        public DataSet GetCompetencyVectorGroupTable()
        {
            return new CompetencyVectorDLManager().GetCompetencyVectorGroupTable();
        }

        /// <summary>
        /// Represents the method to return the list of competency group 
        /// vector details
        /// </summary>
        /// <returns>
        /// A<see cref="List<CompetencyVectorGroup>"/>that holds the 
        /// list of competencty vector group
        /// </returns>
        public List<CompetencyVectorGroup> GetCompetencyVectorGroup()
        {
            return new CompetencyVectorDLManager().GetCompetencyVectorGroup();
        }

        /// <summary>
        /// Represents the method to get the competency vector details for 
        /// the given search criteria
        /// </summary>
        /// <param name="vectorGroupId">
        /// A<see cref="string"/>that holds the vector group ID
        /// </param>
        /// <param name="vectorName">
        /// A<see cref="string"/>that holds the vector name
        /// </param>
        /// <param name="vectorAliasName">
        /// A<see cref="string"/>that holds the vector alias name
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order by value
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="pgeSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <returns>
        /// A<see cref="List<CompetencyVectorDetails>"/>that holds the 
        /// list of competency vector 
        /// </returns>
        public List<CompetencyVectorDetails> GetCompetencyVectorDetails(string vectorGroupId,
            string vectorName, string vectorAliasName, string orderBy, string sortDirection
            , int pageNumber, object pageSize, out int total)
        {
            return new CompetencyVectorDLManager().GetCompetencyVectorDetails
                (vectorGroupId, vectorName, vectorAliasName, orderBy, sortDirection, pageNumber, pageSize, out total);
        }

        /// <summary>
        /// Represents the method to update the vector manager alias name
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id 
        /// </param>
        /// <param name="vectorAliasName">
        /// A<see cref="string"/>that holds the vector alias name
        /// </param>
        public void UpdateCompetencyVectorManagerAliasName(int vectorID, string vectorAliasName)
        {
            new CompetencyVectorDLManager().UpdateCompetencyVectorManagerAliasName(vectorID, vectorAliasName);
        }

        /// <summary>
        /// Represents the method to check whether the vector id is involved 
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that returns whether id is invloved or not
        /// </returns>
        public bool CheckVectorIDInvolved(int vectorID)
        {
            return new CompetencyVectorDLManager().CheckVectorIDInvolved(vectorID);
        }

        /// <summary>
        /// Represents the method to delete a competency vector
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id
        /// </param>
        public void DeleteCompetencyVector(int vectorID)
        {
            new CompetencyVectorDLManager().DeleteCompetencyVector(vectorID);
        }

        /// <summary>
        /// Represents the method to add a new vector
        /// </summary>
        /// <param name="vectorGroupID">
        /// A<see cref="int"/>that holds the vector Id
        /// </param>
        /// <param name="vectorName">
        /// A<see cref="string"/>that holds the vector name
        /// </param>
        /// <param name="aliasName">
        /// A<see cref="string"/>that holds the alias name
        /// </param>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userid 
        /// </param>
        public bool AddNewVector(int vectorGroupID, string vectorName, string aliasName, int userID)
        {
            return new CompetencyVectorDLManager().AddNewVector(vectorGroupID, vectorName, aliasName, userID);
        }

        /// <summary>
        /// Method that retrieves the group and vector name.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CertificationTitle"/> that holds group and vector name
        /// </returns>
        public List<CertificationTitle> GetGroupName(int vectorID)
        {
            return new CompetencyVectorDLManager().GetGroupName(vectorID);
        }

        /// <summary>
        /// Method that retrieves the certification details.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CertificationTitle"/> that holds certificate details
        /// </returns>
        public List<CertificationTitle> GetCertificationTitle(int vectorID)
        {
            return new CompetencyVectorDLManager().GetCertificationTitle(vectorID);
        }

        /// <summary>
        /// Method that inserts certification details.
        /// </summary>
        /// <param name="certificateDetail">
        /// A <see cref="CertificationTitle"/> that holds the certificate details.
        /// </param>
        public void InserCertification(CertificationTitle certificateDetail)
        {
            new CompetencyVectorDLManager().InserCertification(certificateDetail);
        }

        /// <summary>
        /// Method that updates certification details.
        /// </summary>
        /// <param name="certificateDetail">
        /// A <see cref="CertificationDetail"/> that holds the certificate details.
        /// </param>
        public void UpdateCertification(CertificationTitle certificateDetail)
        {
            new CompetencyVectorDLManager().UpdateCertification(certificateDetail);
        }

        /// <summary>
        /// Method that deletes certification detail.
        /// </summary>
        /// <param name="ID">
        /// A <see cref="int"/> that holds the certification ID .
        /// </param>
        public void DeleteCertification(int ID)
        {
            new CompetencyVectorDLManager().DeleteCertification(ID);
        }
    }
}
