﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TrackingBLManager.cs
// File that represents the business layer for the cyber proctoring and
// offline interview tracking module. This will talk to the data layer 
// to perform these operations

#endregion

#region Directives

using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the cyber proctoring and
    /// offline interview tracking module. This will talk to the data layer 
    /// to perform these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class TrackingBLManager
    {
        /// <summary>
        /// Method that retrieves the common log for the offline interview
        /// session.
        /// </summary>
        /// <param name="systemName">
        /// A <see cref="string"/> that holds the system name to apply like search.
        /// </param>
        /// <param name="macAddress">
        /// A <see cref="string"/> that holds the MAC address to apply like search.
        /// </param>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the log date.
        /// </param>
        /// <param name="sortOrder">
        /// A <see cref="string"/> that holds the sort order.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that holds the sort expression.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="SystemLogDetail"/> that holds the common log.
        /// </returns>
        public List<SystemLogDetail> GetCyberProctorCommonLog(string systemName, string macAddress,
            string date, string sortOrder, string sortExpression, int pageNumber, int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetCyberProctorCommonLog(systemName, macAddress, date, sortOrder,
                sortExpression, pageNumber, pageSize, out totalRecords);
        }

        public List<SystemLogDetail> GetCyberProctorLogMesageDetails(string logID)
        {
            return new TrackingDLManager().GetCyberProctorLogMesageDetails(logID);
        }

        public List<SystemLogDetail> GetCyberProctorLogImages(int logID, string imageType)
        {
            return new TrackingDLManager().GetCyberProctorLogImages(logID, imageType);
        }

        public List<SystemLogDetail> GetOfflineInterviewLogImages(int logID, string imageType)
        {
            return new TrackingDLManager().GetCyberProctorLogImages(logID, imageType);
        }

        public void DeleteLogMessage(int logID)
        {
            new TrackingDLManager().DeleteLogMessage(logID);
        }

        /// <summary>
        /// Method that retreives the log file taken from the candidate machine
        /// during test session.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        /// <returns>
        /// An array of <see cref="byte"/> that holds the file contents.
        /// </returns>
        public byte[] GetCyberProctorLogFile(int logID)
        {
            return new TrackingDLManager().GetCyberProctorLogFile(logID);
        }

        /// <summary>
        /// Method that retreives the log file taken from the candidate machine
        /// during offline interview session.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        /// <returns>
        /// An array of <see cref="byte"/> that holds the file contents.
        /// </returns>
        public byte[] GetOfflineInterviewLogFile(int logID)
        {
            return new TrackingDLManager().GetOfflineInterviewLogFile(logID);
        }

        /// <summary>
        /// Method that retrieves the offline interview message log.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="SystemMessageLogDetail"/> that holds the 
        /// system message logs.
        /// </returns>
        public List<SystemMessageLogDetail> GetOfflineInterviewMessageLog(int logID)
        {
            return new TrackingDLManager().GetOfflineInterviewMessageLog(logID);
        }

        /// <summary>
        /// Method that retrieves the offline interview common log image.
        /// </summary>
        /// <param name="imageID">
        /// A <see cref="int"/> that holds the image ID.
        /// </param>
        /// <param name="imageType">
        /// A <see cref="string"/> that holds the image type.
        /// </param>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        /// <returns>
        /// An array of <see cref="byte"/> that holds the image contents.
        /// </returns>
        public byte[] GetOfflineInterviewCommonLogImage(int imageID, string imageType, int logID)
        {
            return new TrackingDLManager().
                GetOfflineInterviewCommonLogImage(imageID, imageType, logID);
        }

        /// <summary>
        /// Method that deletes the offline interview log.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID to delete.
        /// </param>
        public void DeleteOfflineInterviewLog(int logID)
        {
            new TrackingDLManager().DeleteOfflineInterviewLog(logID);
        }

        /// <summary>
        /// Method that retrieves the common log for the offline interview
        /// session.
        /// </summary>
        /// <param name="systemName">
        /// A <see cref="string"/> that holds the system name to apply like search.
        /// </param>
        /// <param name="macAddress">
        /// A <see cref="string"/> that holds the MAC address to apply like search.
        /// </param>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the log date.
        /// </param>
        /// <param name="sortOrder">
        /// A <see cref="string"/> that holds the sort order.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that holds the sort expression.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="SystemLogDetail"/> that holds the common log.
        /// </returns>
        public List<SystemLogDetail> GetOfflineInterviewCommonLog
            (string systemName, string macAddress, string date, string sortOrder,
            string sortExpression, int pageNumber, int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetOfflineInterviewCommonLog
                (systemName, macAddress, date, sortOrder,
                sortExpression, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// to Get CyberProctor Image Details from database.
        /// </summary>
        /// <param name="cansessionKey">
        /// A<see cref="string"/></param>
        /// <param name="attempID">
        /// A<see cref="int"/></param>
        /// <param name="imageType">
        /// A<see cref="int"/></param>
        /// <param name="pageNo">
        /// A<see cref="int"/></param>
        /// <param name="pageSize">
        /// A<see cref="int"/></param>
        /// <param name="totalRec">
        /// A<see cref="int"/></param>
        /// <returns></returns>
        public List<TrackingImageDetail> GetCyberProctorImage(
            string cansessionKey, int attempID, string imageType, int pageNo, int pageSize, out int totalRec)
        {
            return new TrackingDLManager().GetCyberProctorImageDetail(cansessionKey,
                attempID, imageType, pageNo, pageSize, out totalRec);
        }

        public List<TrackingImageDetail> GetCyberProctorLogImages(int logID, string imgtype, int pageNo,
            int pageSize, out int TotalRec)
        {
            return new TrackingDLManager().GetCyberProctorLogImages(logID, imgtype, pageNo, pageSize, out TotalRec);
        }

        public List<TrackingImageDetail> GetOfflineInterviewLogImages(int logID, string imgtype, int pageNo,
           int pageSize, out int TotalRec)
        {
            return new TrackingDLManager().GetOfflineInterviewLogImages(logID, imgtype, pageNo, pageSize, out TotalRec);
        }

        /// <summary>
        /// Method that retrieves the offline interview tracking image 
        /// details.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/>that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="imageType">
        /// A <see cref="int"/> that holds the image type.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records
        /// </param>
        /// <returns>
        /// A list of <see cref="TrackingImageDetail"/> that holds 
        /// the tracking images.
        /// </returns>
        public List<TrackingImageDetail> GetOfflineInterviewTrackingImageDetails
            (string candidateSessionID, int attempID, string imageType,
            int pageNumber, int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetOfflineInterviewTrackingImageDetails
                (candidateSessionID, attempID, imageType,
                pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the offline interview tracking image.
        /// </summary>
        /// <param name="imageID">
        /// A <see cref="int"/> that holds the image ID.
        /// </param>
        /// <param name="isThumb">
        /// A <see cref="int"/> that holds the status whether the thumbnail or
        /// original image needs to be retrieved. 1 represents thumbnail and
        /// 0 original image.
        /// </param>
        /// <returns>
        /// A <see cref="byte"/> that holds the image contents.
        /// </returns>
        public byte[] GetOfflineInterviewTrackingImage(int imageID, int isThumb)
        {
            return new TrackingDLManager().GetOfflineInterviewTrackingImage
                (imageID, isThumb);
        }

        /// <summary>
        ///  To Get CyberProctor Desktop Image from database.
        /// </summary>
        /// <param name="imageID">
        /// A<see cref="int"/></param>
        /// <param name="isThumb">
        /// A<see cref="int"/></param>
        /// <returns></returns>
        public byte[] GetCyberProctorDesktopImage(int imageID, int isThumb)
        {
            return new TrackingDLManager().GetCyberProctorDesktopImage(imageID, isThumb);
        }

        public byte[] GetCyberProctorDesktopLogImage(int imageid, string imageType, int logID)
        {
            return new TrackingDLManager().GetCyberProctorDesktopLogImage(imageid, imageType, logID);
        }

        /// <summary>
        /// Method that retrieves the cyber proctoring application logs.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="SystemLogDetail"/> that holds the cyber proctoring
        /// application logs.
        /// </returns>
        public List<SystemLogDetail> GetCyberProctorApplicationLogs
            (string candidateSessionKey, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            return new TrackingDLManager().GetCyberProctorApplicationLogs
                (candidateSessionKey, attempID, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// to Get CyberProctor Events from database.
        /// </summary>
        /// <param name="cansessionKey">
        /// A<see cref="string"/></param>
        /// <param name="attempID">
        /// A<see cref="int"/></param>
        /// <param name="pageNumber">
        /// A<see cref="int"/></param>
        /// <param name="pageSize">
        /// A<see cref="int"/></param>
        /// <param name="totalRecords">
        /// A<see cref="int"/></param>
        /// <returns></returns>
        public List<ApplicationEventDetail> GetCyberProctorEvents(string cansessionKey, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            return new TrackingDLManager().GetCyberProctorEvents(cansessionKey, attempID, pageNumber,
                pageSize, out  totalRecords);
        }

        /// <summary>
        /// to Get CyberProctor Exceptions from database.
        /// </summary>
        /// <param name="cansessionKey">
        /// A<see cref="string"/></param>
        /// <param name="attempID">
        /// A<see cref="int"/></param>
        /// <param name="pageNum">
        /// A<see cref="int"/>
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/></param>
        /// <param name="totalRecords">
        /// A<see cref="int"/></param>
        /// <returns></returns>
        public List<ApplicationExceptionDetail> GetCyberProctorExceptions(
            string cansessionKey, int attempID, int pageNum, int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetCyberProctorExceptions(cansessionKey, attempID, pageNum, pageSize, out totalRecords);
        }

        /// <summary>
        /// to Get CyberProctor ExistApplications from database.
        /// </summary>
        /// <param name="cansessionKey">
        /// A<see cref="string"/></param>
        /// <param name="attempID">
        /// A<see cref="int"/></param>
        /// <param name="pageNum">
        /// A<see cref="int"/></param>
        /// <param name="pageSize">
        /// A<see cref="int"/></param>
        /// <param name="totalRecords">
        /// A<see cref="int"/></param>
        /// <returns></returns>
        public List<ApplicationDetail> GetCyberProctorExistApplications(
            string cansessionKey, int attempID, int pageNum, int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetCyberProctorExistApplications(cansessionKey, attempID, pageNum, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the offline interview application logs.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ApplicationLogDetail"/> that holds the 
        /// application logs.
        /// </returns>
        public List<ApplicationLogDetail> GetApplicationLogs
            (string candidateSessionID, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetApplicationLogs
                (candidateSessionID, attempID, pageNumber,
                pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the offline interview application events.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ApplicationEventDetail"/> that holds the 
        /// application events.
        /// </returns>
        public List<ApplicationEventDetail> GetApplicationEvents
            (string candidateSessionID, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetApplicationEvents
                (candidateSessionID, attempID, pageNumber,
                    pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the offline interview application exceptions.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ApplicationExceptionDetail"/> that holds the 
        /// application exception.
        /// </returns>
        public List<ApplicationExceptionDetail> GetApplicationExceptions
            (string candidateSessionID, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetApplicationExceptions
                (candidateSessionID, attempID, pageNumber,
                    pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the offline interview running application 
        /// details.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ApplicationDetail"/> that holds the running
        /// application details.
        /// </returns>
        public List<ApplicationDetail> GetRunningApplications
            (string candidateSessionID, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new TrackingDLManager().GetRunningApplications
                (candidateSessionID, attempID, pageNumber,
                pageSize, out totalRecords);
        }
    }
}
