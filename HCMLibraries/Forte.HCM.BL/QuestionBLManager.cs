﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionBLManager.cs
// File that represents the data layer for the Question respository Manager.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Text;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Question respository management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for Question. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    public class QuestionBLManager
    {
        #region Public Method                                                  
        /// <summary>
        /// It helps to save question which includes Generate Next 
        /// </summary>
        /// <param name="questionDetail">
        /// A list of <see cref="QuestionDetail"/> that holds the question Detail.
        /// </param>
        public void SaveQuestion(QuestionDetail questionDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                QuestionDLManager questionDLManager = new QuestionDLManager();
                // Generate new question ID.
                string questionID =
                    new NextNumberDLManager().GetNextNumber(transaction, "QSN", questionDetail.CreatedBy);
                questionDetail.QuestionKey = questionID.ToString();

                // Insert question detail.
                questionDLManager.InsertQuestion(questionDetail, transaction);

                
                // Insert answer choices. 
                // Loop through entire choices
                if (questionDetail.QuestionType == QuestionType.MultipleChoice)
                {
                    foreach (AnswerChoice answerChoice in questionDetail.AnswerChoices)
                    {
                        questionDLManager.InsertAnswerChoice(questionDetail.QuestionKey,
                              answerChoice, questionDetail.CreatedBy, transaction);
                    }
                }

                // Insert question relation according to the subjects
                questionDLManager.InsertQuestionRelation
                    (questionDetail.QuestionKey, questionDetail.SelectedSubjectIDs,
                    questionDetail.CreatedBy, questionDetail.ModifiedBy, transaction);

                if (questionDetail.QuestionImage != null)
                    questionDLManager.InsertQuestionImage(questionDetail.QuestionKey, questionDetail.QuestionImage, questionDetail.CreatedBy, transaction);
                // Commit the transaction.

                if (questionDetail.QuestionType == QuestionType.OpenText)
                {
                    questionDLManager.InsertQuestionAttribute(questionDetail, transaction);
                }
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method allows us to update the question information in the question repository.
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="QuestionDetail"/>that holds the question Detail
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="choiceIdsToBeDeleted">
        /// A <see cref="string"/>that holds the answer choice Ids.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/>that holds the user id.
        /// </param>
        public void UpdateQuestion(QuestionDetail questionDetail, string questionKey, string choiceIdsToBeDeleted, int user)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // Update the question header details to the question repository
                new QuestionDLManager().UpdateQuestion(questionDetail, questionKey, transaction);

                if (questionDetail.QuestionType == QuestionType.MultipleChoice)
                {
                    // Update the answer choices in the question option
                    foreach (AnswerChoice answerChoice in questionDetail.AnswerChoices)
                    {
                        // Modified or newly added choice will be determined by using the question_option_id
                        // If the question_option_id is found, the choice will be considered as modified.
                        // If it is not found, the choice will be considered as new choice
                        if (answerChoice.RecordStatus == RecordStatus.Modified)
                        {
                            new QuestionDLManager().UpdateAnswerChoices(answerChoice, user, transaction);
                        }
                        else if (answerChoice.RecordStatus == RecordStatus.New)
                        {
                            new QuestionDLManager().InsertAnswerChoice(questionKey, answerChoice, user, transaction);
                        }
                    }

                    // Delete answer choices
                    // This method will be called if anyone choice is removed from the choice gridview.
                    if (choiceIdsToBeDeleted.Length != 0)
                    {
                        new QuestionDLManager().DeleteAnswerChoices(choiceIdsToBeDeleted, transaction);
                    }
                }

                // This is used to delete the subject ids from the question relation table
                // based on the question key
                if (questionDetail.DeletedCategories != null
                    || questionDetail.DeletedCategories.Length != 0)
                {
                    new QuestionDLManager().DeleteSubjectsByCategoryID(questionKey, transaction);
                }

                // Insert the subjects which are selected by the user.
                new QuestionDLManager().InsertQuestionRelation
                    (questionKey, questionDetail.SelectedSubjectIDs,
                    questionDetail.CreatedBy, questionDetail.ModifiedBy, transaction);

                if (questionDetail.QuestionImage != null)
                    new QuestionDLManager().InsertQuestionImage(questionKey, questionDetail.QuestionImage, questionDetail.CreatedBy, transaction);
                else
                    new QuestionDLManager().DeleteQuestionImage(questionKey, transaction);

                if (questionDetail.QuestionType == QuestionType.OpenText)
                {
                    new QuestionDLManager().UpdateQuestionAttribute(questionDetail, questionKey, transaction);
                }
                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        /// <summary>
        /// Method allows us to update the interview question information in the question repository.
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="QuestionDetail"/>that holds the question Detail
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="choiceIdsToBeDeleted">
        /// A <see cref="string"/>that holds the answer choice Ids.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/>that holds the user id.
        /// </param>
        public void UpdateInterviewQuestion(QuestionDetail questionDetail, string questionKey, string choiceIdsToBeDeleted, int user)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // Update the question header details to the question repository
                new QuestionDLManager().UpdateInterviewQuestion(questionDetail, questionKey, transaction);

                // Update the answer choices in the question option
                foreach (AnswerChoice answerChoice in questionDetail.AnswerChoices)
                {
                    // Modified or newly added choice will be determined by using the question_option_id
                    // If the question_option_id is found, the choice will be considered as modified.
                    // If it is not found, the choice will be considered as new choice
                    if (answerChoice.RecordStatus == RecordStatus.Modified)
                    {
                        new QuestionDLManager().UpdateInterviewAnswerChoices(answerChoice, user, transaction);
                    }
                    else if (answerChoice.RecordStatus == RecordStatus.New)
                    {
                        new QuestionDLManager().InsertInterviewAnswerChoice(questionKey, answerChoice, user, transaction);
                    }
                }

                // Delete answer choices
                // This method will be called if anyone choice is removed from the choice gridview.
                if (choiceIdsToBeDeleted.Length != 0)
                {
                    new QuestionDLManager().DeleteInterviewAnswerChoices(choiceIdsToBeDeleted, transaction);
                }

                // This is used to delete the subject ids from the question relation table
                // based on the question key
                if (questionDetail.DeletedCategories != null
                    || questionDetail.DeletedCategories.Length != 0)
                {
                    new QuestionDLManager().DeleteInterviewSubjectsByCategoryID(questionKey, transaction);
                }

                // Insert the subjects which are selected by the user.
                new QuestionDLManager().InsertInterviewQuestionRelation
                    (questionKey, questionDetail.SelectedSubjectIDs,
                    questionDetail.CreatedBy, questionDetail.ModifiedBy, transaction);

                if (questionDetail.QuestionImage != null)
                    new QuestionDLManager().InsertInterviewQuestionImage(questionKey, questionDetail.QuestionImage, questionDetail.CreatedBy, transaction);
                else
                    new QuestionDLManager().DeleteInterviewQuestionImage(questionKey, transaction);

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
       
        /// <summary>
        /// Get question details by giving a question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        ///  A <see cref="QuestionDetail"/> that holds the question details. 
        /// </returns>
        public QuestionDetail GetQuestion(string questionKey)
        {
            return new QuestionDLManager().GetQuestion(questionKey);
        }

        /// <summary>
        /// Get interview question details by giving a question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        ///  A <see cref="QuestionDetail"/> that holds the question details. 
        /// </returns>
        public QuestionDetail GetInterviewQuestion(string questionKey)
        {
            return new QuestionDLManager().GetInterviewQuestion(questionKey);
        }

        /// <summary>
        /// Get question image by giving a question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        ///  A <see cref="QuestionDetail"/> that holds the question details. 
        /// </returns>
        public byte[] GetQuestionImage(string questionKey)
        {
            return new QuestionDLManager().GetQuestionImage(questionKey);
        }

        /// <summary>
        /// Helps to check if the question key already involved in test question.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        /// A <see cref="bool"/>that holds the question Key involved in test question.
        /// </returns>
        public bool IsQuestionExistsInTest(string questionKey)
        {
            return new QuestionDLManager().IsQuestionExistsInTest(questionKey);
            //return true;
        }


        /// <summary>
        /// Helps to check if the question key already involved in interview test question.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        /// A <see cref="bool"/>that holds the question Key involved in test question.
        /// </returns>
        public bool IsInterviewQuestionExistsInTest(string questionKey)
        {
            return new QuestionDLManager().IsInterviewQuestionExistsInTest(questionKey);
            //return true;
        }

        /// <summary>
        /// This method is used to Get all the Answer choices based on the question key.
        /// </summary>
        /// <param name="questionKey">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <returns>
        /// A list of <see cref="AnswerChoice"/> that holds the AnswerChoice regarding this question.
        /// </returns>
        /// <remarks>
        /// Supports providing data.
        /// </remarks>
        public List<AnswerChoice> GetQuestionOptions(string questionKey)
        {
            return new QuestionDLManager().GetQuestionOptions(questionKey);
        }

        /// <summary>
        /// This method is used to Delete question based on the question key.
        /// </summary>
        /// <param name="questionID">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeleteQuestion(string questionID, int user)
        {
            new QuestionDLManager().DeleteQuestion(questionID, user);
        }

        /// <summary>
        /// Represents the method to get the contributor summary
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userId
        /// </param>
        /// <returns>
        /// A<see cref="ContributorSummary"/>that holds the contributor summary details
        /// </returns>
        public ContributorSummary GetContributorSummary(int userID)
        {
            return new QuestionDLManager().GetContributorSummary(userID);
        }

        /// <summary>
        /// Represents the method to get the interview contributor summary
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userId
        /// </param>
        /// <returns>
        /// A<see cref="ContributorSummary"/>that holds the contributor summary details
        /// </returns>
        public ContributorSummary GetInterviewContributorSummary(int userID)
        {
            return new QuestionDLManager().GetInterviewContributorSummary(userID);
        }

        /// <summary>
        /// Method that retrieves the category summary based on the question author.
        /// </summary>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that holds the pagenumber.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains total number of pages.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that takes the total number of records returned by the SP.
        /// </param>
        /// <returns>
        /// A list of <see cref="Category"/> that holds the categories.
        /// </returns>
        public List<Category> GetCategoriesContributed(int user, int pageNumber, int pageSize, out int totalRecords)
        {
            return new QuestionDLManager().GetCategoriesContributed(user, pageNumber, pageSize, out totalRecords);
        }


        /// <summary>
        /// Method that retrieves the interview category summary based on the question author.
        /// </summary>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that holds the pagenumber.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains total number of pages.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that takes the total number of records returned by the SP.
        /// </param>
        /// <returns>
        /// A list of <see cref="Category"/> that holds the categories.
        /// </returns>
        public List<Category> GetInterviewCategoriesContributed(int user, int pageNumber, int pageSize, out int totalRecords)
        {
            return new QuestionDLManager().GetInterviewCategoriesContributed(user, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Methat that retrieves the subject summary for the given question author.
        /// </summary>
        /// <param name="user">
        /// A <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the current pagenumber.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that contains total number of pages.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that takes the total number of records returned by the SP.
        /// </param>
        /// <returns>
        /// A list of <see cref="Subject"/> that holds the subjects.
        /// </returns>
        public List<Subject> GetSubjectsContributed(int user, int pageNumber, int pageSize, out int totalRecords)
        {
            return new QuestionDLManager().GetSubjectsContributed(user, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Methat that retrieves the interview subject summary for the given question author.
        /// </summary>
        /// <param name="user">
        /// A <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the current pagenumber.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that contains total number of pages.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that takes the total number of records returned by the SP.
        /// </param>
        /// <returns>
        /// A list of <see cref="Subject"/> that holds the subjects.
        /// </returns>
        public List<Subject> GetInterviewSubjectsContributed(int user, int pageNumber, int pageSize, out int totalRecords)
        {
            return new QuestionDLManager().GetInterviewSubjectsContributed(user, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of Questions for the given search 
        /// </summary>
        /// <param name="searchCriteria">
        /// A list of <see cref="QuestionDetailSearchCriteria"/> that holds the questionKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the QuestionDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<QuestionDetail> GetQuestions(QuestionDetailSearchCriteria searchCriteria, QuestionType questionType, int pageSize, int pageNumber, string orderBy, SortType direction, out int totalRecords)
        {
            return new QuestionDLManager().GetQuestions(searchCriteria, questionType, pageSize, pageNumber, orderBy, direction, out totalRecords);
        }

        /// <summary>
        /// This method is used to Active question based on the question key.
        /// </summary>
        /// <param name="questionID">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void ActivateQuestion(string questionID, int user)
        {
            //new QuestionDLManager().UpdateStatus(questionID, QuestionStatus.Active,user);
            new QuestionDLManager().UpdateStatus(questionID, "Y", user);
        }

        /// <summary>
        /// This method is used to Inactivate question based on the question key.
        /// </summary>
        /// <param name="questionID">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeactivateQuestion(string questionID, int user)
        {
            new QuestionDLManager().UpdateStatus(questionID, "N", user);
        }

        /// <summary>
        /// Method that retrieves the list of test for the given questionkey Mapped. 
        /// criteria.
        /// </summary>
        /// <param name="questionID">
        /// A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pagesize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that holds the TestDetails regarding this question.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<TestDetail> GetQuestionUsageSummary(string questionID,
            int pageNumber, int pagesize, string orderBy, SortType orderByDirection,
            out int totalRecords)
        {
            return new QuestionDLManager().GetQuestionUsageSummary
                (questionID, pageNumber, pagesize, orderBy, orderByDirection,
                out totalRecords);
        }


        /// <summary>
        /// Method that checks whether the attributes of a question
        /// are valid or not
        /// </summary>
        /// <param name="question">
        /// A<see cref="QuestionDetail"/>QuestionDetail that contains 
        /// the detail of the question.
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>value that contain question status
        /// </returns>
        //public QuestionDetail CheckInterviewQuestionAttribute(QuestionDetail question)
        //{
        //    QuestionDetail questionDetail =
        //     new QuestionDLManager().CheckQuestionAttribute(question);

        //    if (!Utility.IsNullOrEmpty(questionDetail.Question))
        //    {
        //        if (questionDetail.Question.Trim().Length == 0)
        //        {
        //            questionDetail.InvalidQuestionRemarks += "Question not present, ";
        //        }
        //    }
        //    else
        //    {
        //        questionDetail.InvalidQuestionRemarks += "Question not present, ";
        //    }
        //    if (!Utility.IsNullOrEmpty(questionDetail.CorrectAnswer))
        //    {
        //        if (questionDetail.CorrectAnswer.Trim().Length == 0)
        //        {
        //            questionDetail.InvalidQuestionRemarks += "Answer not present, ";
        //        }
        //    }
        //    else
        //    {
        //        questionDetail.InvalidQuestionRemarks += "Answer not present, ";
        //    }
        //    //if (!Utility.IsNullOrEmpty(questionDetail.InvalidQuestionRemarks))
        //    //{
        //    //    questionDetail.InvalidQuestionRemarks = questionDetail.InvalidQuestionRemarks.
        //    //   Remove(questionDetail.InvalidQuestionRemarks.Length - 2, 2);
        //    //}
        //    questionDetail.InvalidQuestionRemarks = questionDetail.InvalidQuestionRemarks.TrimEnd(' ').TrimEnd(',');
        //    return questionDetail;
        //}
        
        /// <summary>
        /// Method that checks whether the attributes of a question
        /// are valid or not
        /// </summary>
        /// <param name="question">
        /// A<see cref="QuestionDetail"/>QuestionDetail that contains 
        /// the detail of the question.
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>value that contain question status
        /// </returns>
        public QuestionDetail CheckQuestionAttribute(QuestionDetail question, int tenantID)
        {
            QuestionDetail questionDetail =
             new QuestionDLManager().CheckQuestionAttribute(question, tenantID);

            if (!Utility.IsNullOrEmpty(questionDetail.Question))
            {
                if (questionDetail.Question.Trim().Length == 0)
                {
                    questionDetail.InvalidQuestionRemarks += "Question not present, ";
                }
            }
            else
            {
                questionDetail.InvalidQuestionRemarks += "Question not present, ";
            }
            if (questionDetail.Answer == 0)
            {
                questionDetail.InvalidQuestionRemarks += "No Choice marked, ";
            }
            if (questionDetail.AnswerChoices.Count == 0)
            {
                questionDetail.InvalidQuestionRemarks += "Choices are empty, ";
            }
            //if (!Utility.IsNullOrEmpty(questionDetail.InvalidQuestionRemarks))
            //{
            //    questionDetail.InvalidQuestionRemarks = questionDetail.InvalidQuestionRemarks.
            //   Remove(questionDetail.InvalidQuestionRemarks.Length - 2, 2);
            //}
            questionDetail.InvalidQuestionRemarks = questionDetail.InvalidQuestionRemarks.TrimEnd(' ').TrimEnd(',');
            return questionDetail;
        }

        /// <summary>
        /// Method that checks whether the attributes of a question
        /// are valid or not
        /// </summary>
        /// <param name="question">
        /// A<see cref="QuestionDetail"/>QuestionDetail that contains 
        /// the detail of the question.
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>value that contain question status
        /// </returns>
        public QuestionDetail CheckOpenTextQuestionAttribute(QuestionDetail question, int tenantID)
        {
            QuestionDetail questionDetail =
             new QuestionDLManager().CheckQuestionAttribute(question, tenantID);

            if (!Utility.IsNullOrEmpty(questionDetail.Question))
            {
                if (questionDetail.Question.Trim().Length == 0)
                {
                    questionDetail.InvalidQuestionRemarks += "Question not present, ";
                }
            }
            else
            {
                questionDetail.InvalidQuestionRemarks += "Question not present, ";
            }
            questionDetail.InvalidQuestionRemarks = questionDetail.InvalidQuestionRemarks.TrimEnd(' ').TrimEnd(',');
            return questionDetail;
        }

        /// <summary>
        /// Method that checks whether the attributes of a question
        /// are valid or not
        /// </summary>
        /// <param name="question">
        /// A<see cref="QuestionDetail"/>QuestionDetail that contains 
        /// the detail of the question.
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>value that contain question status
        /// </returns>
        public QuestionDetail CheckIntrviewQuestionAttribute(QuestionDetail question, int tenantID)
        {
            QuestionDetail questionDetail =
             new QuestionDLManager().CheckQuestionAttribute(question, tenantID);

            if (!Utility.IsNullOrEmpty(questionDetail.Question))
            {
                if (questionDetail.Question.Trim().Length == 0)
                {
                    questionDetail.InvalidQuestionRemarks += "Question not present, ";
                }
            }
            else
            {
                questionDetail.InvalidQuestionRemarks += "Question not present, ";
            }


            if (!Utility.IsNullOrEmpty(questionDetail.CorrectAnswer))
            {
                if (questionDetail.CorrectAnswer.Trim().Length == 0)
                {
                    questionDetail.InvalidQuestionRemarks += "Answer not present, ";
                }
            }
            else
            {
                questionDetail.InvalidQuestionRemarks += "Answer not present, ";
            }
       
            questionDetail.InvalidQuestionRemarks = questionDetail.InvalidQuestionRemarks.TrimEnd(' ').TrimEnd(',');
            return questionDetail;
        }

        /// <summary>
        /// Method that is used to check the validations and attributes of the question
        /// </summary>
        /// <param name="questions">
        /// A list for<see cref="QuestionDetail"/>that has the list of questions
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the error message
        /// </returns>
        public string CheckValidQuestions(List<QuestionDetail> questions, int tenantID)
        {
            QuestionDetail questionDetails;


            StringBuilder errorMessage = null;
            StringBuilder returnMessage = null;
            foreach (QuestionDetail question in questions)
            {
                question.InvalidQuestionRemarks = "";
                questionDetails = new QuestionDLManager().CheckQuestionAttribute(question, tenantID);
                if (Utility.IsNullOrEmpty(errorMessage))
                    errorMessage = new StringBuilder();
                if (questionDetails.Question.Trim().Length == 0)
                    errorMessage.Append("Please enter question, ");
                //Checks whether the anser is there 
                if (questionDetails.Answer == 0)
                {
                    errorMessage.Append("No answer has been marked, ");
                }
                //Checks whether the category and subject is valid
                if (questionDetails.SubjectID == 0)
                {
                    errorMessage.Append("Please select correct category and subject, ");
                }
                //Checks whether the complexity is correct
                if (questionDetails.Complexity == "" || questionDetails.Complexity == "--Select--")
                {
                    errorMessage.Append("Please select complexity, ");
                }

                //Checks whether the test area is correct
                if (questionDetails.TestAreaID == "" || questionDetails.TestAreaID == "--Select--")
                {
                    errorMessage.Append("Please select test area, ");
                }
                //Checks whether the test area is correct
                if (questionDetails.AnswerChoices.Count == 0)
                {
                    errorMessage.Append("No answer choices, ");
                }
                List<AnswerChoice> answers = new List<AnswerChoice>();
                answers = (question.AnswerChoices.FindAll(delegate(AnswerChoice answer)
                {
                    return answer.Choice.Trim().Length == 0;
                }));

                if ((answers.Count > 2 && question.NoOfChoices > 2)
                    || (answers.Count >= 1 && question.NoOfChoices == 2))
                {
                    errorMessage.Append("Please enter choices, ");
                }
                if (question.AnswerChoices.Count == 1)
                {
                    errorMessage.Append("Quesion with single option cannot be added., ");
                }
                if (answers.Count >= 2 && question.NoOfChoices == 3)
                {
                    errorMessage.Append("Please enter choices, ");
                }
                AnswerChoice correctAnswer = questionDetails.AnswerChoices.Find(delegate(AnswerChoice answer)
                {
                    return answer.ChoiceID == question.Answer;
                });
                if (correctAnswer != null && correctAnswer.Choice.Trim().Length == 0)
                {
                    errorMessage.Append("Please enter correct choice, ");
                }
                if (errorMessage.ToString().Length == 0)
                {
                    errorMessage = null;
                    continue;
                }
                if (Utility.IsNullOrEmpty(returnMessage))
                    returnMessage = new StringBuilder();
                returnMessage.Append("Question - ");
                returnMessage.Append(question.QuestionID);
                returnMessage.Append(" ");
                returnMessage.Append(errorMessage.ToString().TrimEnd(' ').TrimEnd(','));
                returnMessage.Append("<br />");
                errorMessage = null;
            }
            if (returnMessage == null)
                return string.Empty;
            return returnMessage.ToString();
        }

        /// <summary>
        /// Method that is used to check the validations and attributes of the question
        /// </summary>
        /// <param name="questions">
        /// A list for<see cref="QuestionDetail"/>that has the list of questions
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the error message
        /// </returns>
        public string CheckOpenTextValidQuestions(List<QuestionDetail> questions, int tenantID)
        {
            QuestionDetail questionDetails;

            StringBuilder errorMessage = null;
            StringBuilder returnMessage = null;
            foreach (QuestionDetail question in questions)
            {
                question.InvalidQuestionRemarks = "";
                questionDetails = new QuestionDLManager().CheckQuestionAttribute(question, tenantID);
                if (Utility.IsNullOrEmpty(errorMessage))
                    errorMessage = new StringBuilder();
                if (questionDetails.Question.Trim().Length == 0)
                    errorMessage.Append("Please enter question, ");
                //Checks whether the anser is there 
                //Checks whether the category and subject is valid
                if (questionDetails.SubjectID == 0)
                {
                    errorMessage.Append("Please select correct category and subject, ");
                }
                //Checks whether the complexity is correct
                if (questionDetails.Complexity == "" || questionDetails.Complexity == "--Select--")
                {
                    errorMessage.Append("Please select complexity, ");
                }

                //Checks whether the test area is correct
                if (questionDetails.TestAreaID == "" || questionDetails.TestAreaID == "--Select--")
                {
                    errorMessage.Append("Please select test area, ");
                }
                if (errorMessage.ToString().Length == 0)
                {
                    errorMessage = null;
                    continue;
                }
                if (Utility.IsNullOrEmpty(returnMessage))
                    returnMessage = new StringBuilder();
                returnMessage.Append("Question - ");
                returnMessage.Append(question.QuestionID);
                returnMessage.Append(" ");
                returnMessage.Append(errorMessage.ToString().TrimEnd(' ').TrimEnd(','));
                returnMessage.Append("<br />");
                errorMessage = null;
            }
            if (returnMessage == null)
                return string.Empty;
            return returnMessage.ToString();
        }
       
        /// <summary>
        /// Represents the method to get the author id and name of the question author
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userId
        /// </param>
        /// <returns>
        /// A<see cref="UserDetail"/>that holds the user details
        /// </returns>
        public UserDetail GetAuthorIDAndName(int userID)
        {
            //Get the author id and name and return 
            return new QuestionDLManager().GetAuthorIDAndName(userID);
        }


        /// <summary>
        /// Represents the method to get the contributor summary question details
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userID 
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number 
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page Size 
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction like asc/desc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records out parameter
        /// </param>
        /// <returns>
        /// A List for<see cref="QuestionDetail"/>that holds the Question Detail
        /// </returns>
        public List<QuestionDetail> GetContributorSummaryQuestionDetails(int userID,
            int pageNumber, int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            //Get and return the contributor summary details
            return new QuestionDLManager().GetContributorSummaryQuestionDetails(userID, pageNumber
                , pageSize, orderBy, orderByDirection, out totalRecords);
        }


        /// <summary>
        /// Represents the method to get the interview contributor summary question details
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userID 
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number 
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page Size 
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction like asc/desc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records out parameter
        /// </param>
        /// <returns>
        /// A List for<see cref="QuestionDetail"/>that holds the Question Detail
        /// </returns>
        public List<QuestionDetail> GetInterviewContributorSummaryQuestionDetails(int userID,
            int pageNumber, int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            //Get and return the contributor summary details
            return new QuestionDLManager().GetInterviewContributorSummaryQuestionDetails(userID, pageNumber
                , pageSize, orderBy, orderByDirection, out totalRecords);
        }

        /// <summary>
        /// Represents the method to get the contributor chart details
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user ID 
        /// </param>
        /// <returns>
        /// A<see cref="ContributorSummary"/>that hold the contributor 
        /// summary details
        /// </returns>
        public ContributorSummary GetContributorChartDetails(int userID)
        {
            //Get and return the data from the database
            return new QuestionDLManager().GetContributorChartDetails(userID);
        }

        /// <summary>
        /// Represents the method to get the Interview contributor chart details
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user ID 
        /// </param>
        /// <returns>
        /// A<see cref="ContributorSummary"/>that hold the contributor 
        /// summary details
        /// </returns>
        public ContributorSummary GetInterviewContributorChartDetails(int userID)
        {
            //Get and return the data from the database
            return new QuestionDLManager().GetInterviewContributorChartDetails(userID);
        }

        /// <summary>
        /// Gets questions for the automated test
        /// </summary>
        /// <param name="testSearchCrierias">search criteria given by the 
        /// user to generate questions</param>
        /// <param name="NoofQuestionsToPick">
        /// A <see cref="int"/> Number of questions in the test
        /// </param>
        /// <param name="dtSearchCriteria">
        /// search segment data table
        /// (Note this should be passed by ref: keyword)</param>
        /// <param name="StartIndex">
        /// A <see cref="int"/> that holds start index question.
        /// </param>
        /// <returns>
        /// A list for<see cref="QuestionDetail"/> 
        /// returns the final list of questions that should be shown to the user
        /// </returns>
        public List<QuestionDetail> GetAutomatedQuestions(QuestionType questionType, List<TestSearchCriteria> testSearchCrierias,
            int NoofQuestionsToPick, ref DataTable dtSearchCriteria, int StartIndex, int questionAuthor)
        {
            List<QuestionDetail> questionDetails = null;
            List<QuestionDetail> returnQuestionDetails = null;
            List<QuestionDetail> SearchSegementsPickedQuestions = null;
            try
            {
                questionDetails = new List<QuestionDetail>();
                returnQuestionDetails = new List<QuestionDetail>();
                dtSearchCriteria.Columns.Add("ListStartIndex", typeof(int));
                dtSearchCriteria.Columns.Add("ListEndIndex", typeof(int));
                dtSearchCriteria.Columns.Add("Ratio", typeof(double));
                for (int i = 0; i < testSearchCrierias.Count; i++)
                {
                    int TotalRecords;
                    testSearchCrierias[i].TestStartIndex =
                            (StartIndex * Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"])) + 1;
                    testSearchCrierias[i].TestEndIndex =
                        (StartIndex * Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"])) +
                        NoofQuestionsToPick;
                    questionDetails = GetQuestionDetailsforAutomatedTest(questionType, testSearchCrierias[i], questionAuthor, out TotalRecords);
                    if (Utility.IsNullOrEmpty(questionDetails))
                    {
                        dtSearchCriteria.Rows[i]["PickedQuestions"] = 0;
                        dtSearchCriteria.Rows[i]["Remarks"] = "No questions found for the search criteria";
                    }
                    else
                    {
                        if (SearchSegementsPickedQuestions == null)
                            SearchSegementsPickedQuestions = new List<QuestionDetail>();
                        dtSearchCriteria.Rows[i]["TotalRecordsinDB"] = TotalRecords;
                        dtSearchCriteria.Rows[i]["ListStartIndex"] = SearchSegementsPickedQuestions.Count;
                        SearchSegementsPickedQuestions.AddRange(questionDetails);
                        dtSearchCriteria.Rows[i]["ListEndIndex"] = questionDetails.Count;
                        TotalRecords = 0;
                        questionDetails = null;
                    }
                }
                PrepareQuestionListBasedOnRatioForAutomatedTest(ref dtSearchCriteria,
                    ref SearchSegementsPickedQuestions, ref returnQuestionDetails);
                return returnQuestionDetails;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(questionDetails)) questionDetails = null;
                if (!Utility.IsNullOrEmpty(returnQuestionDetails)) returnQuestionDetails = null;
                if (!Utility.IsNullOrEmpty(SearchSegementsPickedQuestions)) SearchSegementsPickedQuestions = null;
                if (dtSearchCriteria.Columns.Contains("ListStartIndex"))
                    dtSearchCriteria.Columns.Remove("ListStartIndex");
                if (dtSearchCriteria.Columns.Contains("ListEndIndex"))
                    dtSearchCriteria.Columns.Remove("ListEndIndex");
                if (dtSearchCriteria.Columns.Contains("Ratio"))
                    dtSearchCriteria.Columns.Remove("Ratio");
            }
        }

       /// <summary>
       /// This method sorts the search criteria table according to the ratio and
       /// picks the questions.
       /// </summary>
       /// <param name="SearchSegmentTable">
       /// A <see cref="DataTable"/> contains the user provided search criteria
       /// </param>
       /// <param name="SegementsQuesionDetails">
       /// A List contains the question details picked by the search 
       /// criteria.
       /// </param>
       /// <param name="returnQuestionDetails">
        /// Final question details List to be shown 
        /// to the user
       /// </param>
        private void PrepareQuestionListBasedOnRatioForAutomatedTest
            (ref DataTable SearchSegmentTable, ref List<QuestionDetail> SegementsQuesionDetails,
            ref List<QuestionDetail> returnQuestionDetails)
        {
            int QuestionsAdded = 0;
            bool IsAnotherSegmentPicked = false;
            DataTable SortedRatioTable = null;
            DataView SortedRatioDataView = null;
            try
            {
                for (int LoopI = 0; LoopI < SearchSegmentTable.Rows.Count; LoopI++)
                {
                    if (Convert.ToInt32(SearchSegmentTable.Rows[LoopI]["TotalRecordsinDB"]) == 0)
                        SearchSegmentTable.Rows[LoopI]["Ratio"] = 0;
                    else
                        SearchSegmentTable.Rows[LoopI]["Ratio"] =
                            Convert.ToDouble(SearchSegmentTable.Rows[LoopI]["ExpectedQuestions"]) /
                            Convert.ToDouble(SearchSegmentTable.Rows[LoopI]["TotalRecordsinDB"]);
                }
                SortedRatioDataView = SearchSegmentTable.DefaultView;
                SortedRatioDataView.Sort = "Ratio DESC, TotalRecordsinDB ASC";
                SortedRatioTable = SortedRatioDataView.ToTable();
                SortedRatioDataView = null;
                for (int LoopI = 0; LoopI < SortedRatioTable.Rows.Count; LoopI++)
                {
                    if ((int)SortedRatioTable.Rows[LoopI]["TotalRecordsinDB"] != 0)
                        CheckandInsertQuestionDetails(SegementsQuesionDetails.GetRange(
                              (int)SortedRatioTable.Rows[LoopI]["ListStartIndex"],
                             (int)SortedRatioTable.Rows[LoopI]["ListEndIndex"]), ref returnQuestionDetails,
                        (int)SortedRatioTable.Rows[LoopI]["ExpectedQuestions"],
                           out QuestionsAdded, out IsAnotherSegmentPicked);
                    SortedRatioTable.Rows[LoopI]["PickedQuestions"] = QuestionsAdded;
                    SortedRatioTable.Rows[LoopI]["QuestionsDifference"] =
                        Convert.ToInt32(SortedRatioTable.Rows[LoopI]["ExpectedQuestions"]) - QuestionsAdded;
                    //
                    #region Error Messages or Information or Warnings                    
                    if (QuestionsAdded == 0)
                        SortedRatioTable.Rows[LoopI]["Remarks"] = "No questions found for the search criteria";
                    else if ((IsAnotherSegmentPicked) && (QuestionsAdded < (int)SortedRatioTable.Rows[LoopI]["ExpectedQuestions"]))
                        SortedRatioTable.Rows[LoopI]["Remarks"] = "This segment questions picked by another segment";
                    else if (QuestionsAdded < (int)SortedRatioTable.Rows[LoopI]["ExpectedQuestions"])
                        SortedRatioTable.Rows[LoopI]["Remarks"] = "Only " + QuestionsAdded.ToString() +
                            (QuestionsAdded.ToString() == "1" ? " question " : " questions ") + "found for this search criteria";
                    else if (QuestionsAdded == (int)SortedRatioTable.Rows[LoopI]["ExpectedQuestions"])
                        SortedRatioTable.Rows[LoopI]["Remarks"] = "Search Successfull";
                    #endregion Error Messages or Information or Warnings
                    //
                    QuestionsAdded = 0;
                    IsAnotherSegmentPicked = false;
                }
                SortedRatioDataView = SortedRatioTable.DefaultView;
                SortedRatioDataView.Sort = "SNO ASC";
                SearchSegmentTable = null;
                SearchSegmentTable = SortedRatioDataView.ToTable();
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SortedRatioTable)) SortedRatioTable = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SortedRatioDataView)) SortedRatioDataView = null;
            }
        }

        /// <summary>
        /// This method communicates with the questiondetail DL Manager
        /// to get the questions for the automated test
        /// </summary>
        /// <param name="testSearchCriteria">search criteria to get the questions</param>
        /// <param name="TotalRecords">Total number of records picked 
        /// (Note:- this should be 'out' parameter)</param>
        /// <returns>List of question details picked from the database</returns>
        private List<QuestionDetail> GetQuestionDetailsforAutomatedTest(QuestionType questionType, TestSearchCriteria testSearchCriteria,
           int questionAuthor, out int TotalRecords)
        {
            return new QuestionDLManager().GetAutomatedQuestions(questionType, testSearchCriteria, questionAuthor, out TotalRecords);
        }

        /// <summary>
        /// This method insert the checks and inserts the question details.
        /// This method will check for unique ness.
        /// </summary>
        /// <param name="questionDetails">question details to be add to the result</param>
        /// <param name="resultsQuestionDetails">Final question details list to be shown 
        /// to the user</param>
        /// <param name="NoofQuestionsToAdd">Total number of question to add
        /// according to the weightage for the test segment</param>
        /// <param name="QuestionAdded">Total number of questions added to the result</param>
        /// <param name="DidAnotherSegmentPicked">
        /// A <see cref="bool"/> whether another segment picker or check.
        /// </param>
        private void CheckandInsertQuestionDetails
            (List<QuestionDetail> questionDetails, ref List<QuestionDetail> resultsQuestionDetails,
            int NoofQuestionsToAdd, out int QuestionAdded, out bool DidAnotherSegmentPicked)
        {
            QuestionAdded = 0;
            DidAnotherSegmentPicked = false;
            for (int QuestionDeatailsI = 0; QuestionDeatailsI < questionDetails.Count; QuestionDeatailsI++)
            {
                if (QuestionAdded >= NoofQuestionsToAdd)
                    break;
                if (resultsQuestionDetails.FindIndex
                    (p => p.QuestionKey == questionDetails[QuestionDeatailsI].QuestionKey) >= 0)
                {
                    DidAnotherSegmentPicked = true;
                    continue;
                }
                resultsQuestionDetails.Add(questionDetails[QuestionDeatailsI]);
                QuestionAdded++;
            }
        }
        
        #endregion


        /// <summary>
        /// It helps to save question which includes Generate Next 
        /// </summary>
        /// <param name="questionDetail">
        /// A list of <see cref="QuestionDetail"/> that holds the question Detail.
        /// </param>
        public void SaveInterviewQuestion(QuestionDetail questionDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                QuestionDLManager questionDLManager = new QuestionDLManager();
                // Generate new question ID.
                string questionID =
                    new NextNumberDLManager().GetNextNumber(transaction, "IQN", questionDetail.CreatedBy);
                questionDetail.QuestionKey = questionID.ToString();

                // Insert question detail.
                questionDLManager.InsertInterviewQuestion(questionDetail, transaction);

                foreach (AnswerChoice answerChoice in questionDetail.AnswerChoices)
                {
                    questionDLManager.InsertInterviewAnswerChoice(questionDetail.QuestionKey,
                          answerChoice, questionDetail.CreatedBy, transaction);
                }
               
                // Insert question relation according to the subjects
                questionDLManager.InsertInterviewQuestionRelation
                    (questionDetail.QuestionKey, questionDetail.SelectedSubjectIDs,
                    questionDetail.CreatedBy, questionDetail.ModifiedBy, transaction);

                if (questionDetail.QuestionImage != null)
                    questionDLManager.InsertInterviewQuestionImage(questionDetail.QuestionKey, questionDetail.QuestionImage, questionDetail.CreatedBy, transaction);
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        /// <summary>
        /// Method that is used to check the validations and attributes of the question
        /// </summary>
        /// <param name="questions">
        /// A list for<see cref="QuestionDetail"/>that has the list of questions
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the error message
        /// </returns>
        public string CheckValidInterviewQuestions(List<QuestionDetail> questions)
        {
            QuestionDetail questionDetails;

            StringBuilder errorMessage = null;
            StringBuilder returnMessage = null;
            foreach (QuestionDetail question in questions)
            {
                question.InvalidQuestionRemarks = "";
                questionDetails = new QuestionDLManager().CheckInterviewQuestionAttribute(question);
                if (Utility.IsNullOrEmpty(errorMessage))
                    errorMessage = new StringBuilder();
                if (questionDetails.Question.Trim().Length == 0)
                    errorMessage.Append("Please enter question, ");
               
                //Checks whether the category and subject is valid
                if (questionDetails.SubjectID == 0)
                {
                    errorMessage.Append("Please select correct category and subject, ");
                }
                //Checks whether the complexity is correct
                if (questionDetails.Complexity == "" || questionDetails.Complexity == "--Select--")
                {
                    errorMessage.Append("Please select complexity, ");
                }

                //Checks whether the test area is correct
                if (questionDetails.TestAreaID == "" || questionDetails.TestAreaID == "--Select--")
                {
                    errorMessage.Append("Please select test area, ");
                }

                if (questionDetails.CorrectAnswer != null && questionDetails.CorrectAnswer.Trim().Length == 0)
                {
                    errorMessage.Append("Please enter answer, ");
                }
                if (errorMessage.ToString().Length == 0)
                {
                    errorMessage = null;
                    continue;
                }
                if (Utility.IsNullOrEmpty(returnMessage))
                    returnMessage = new StringBuilder();
                returnMessage.Append("Question - ");
                returnMessage.Append(question.QuestionID);
                returnMessage.Append(" ");
                returnMessage.Append(errorMessage.ToString().TrimEnd(' ').TrimEnd(','));
                returnMessage.Append("<br />");
                errorMessage = null;
            }
            if (returnMessage == null)
                return string.Empty;
            return returnMessage.ToString();
        }
        /// <summary>
        /// Method that retrieves the list of Questions for the given search 
        /// </summary>
        /// <param name="searchCriteria">
        /// A list of <see cref="QuestionDetailSearchCriteria"/> that holds the questionKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the QuestionDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<QuestionDetail> GetInterviewQuestions(QuestionDetailSearchCriteria searchCriteria, int pageSize, int pageNumber, string orderBy, SortType direction, out int totalRecords)
        {
            return new QuestionDLManager().GetInterviewQuestions(searchCriteria, pageSize, pageNumber, orderBy, direction, out totalRecords);
        }
        /// <summary>
        /// This method is used to Inactivate question based on the question key.
        /// </summary>
        /// <param name="questionID">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeactivateInterviewQuestion(string questionID, int user)
        {
            new QuestionDLManager().UpdateInterviewStatus(questionID, "N", user);
        }
        /// <summary>
        /// This method is used to Active question based on the question key.
        /// </summary>
        /// <param name="questionID">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void ActivateInterviewQuestion(string questionID, int user)
        {
            //new QuestionDLManager().UpdateStatus(questionID, QuestionStatus.Active,user);
            new QuestionDLManager().UpdateInterviewStatus(questionID, "Y", user);
        }
        /// <summary>
        /// Gets the question relation ID.
        /// </summary>
        /// <param name="QuestionKey">The question key.</param>
        /// <returns></returns>
        public int GetQuestionRelationID(string QuestionKey)
        {
            return new QuestionDLManager().GetQuestionRelationID(QuestionKey);

        }

        /// <summary>
        /// Gets the category.
        /// </summary>
        /// <param name="QuestionKey">The question key.</param>
        /// <returns></returns>
        public string GetCategory(string QuestionKey)
        {
            return new QuestionDLManager().GetCategory(QuestionKey);
        }
        /// <summary>
        /// Gets the subject.
        /// </summary>
        /// <param name="QuestionKey">The question key.</param>
        /// <returns></returns>
        public string GetSubject(string QuestionKey)
        {
            return new QuestionDLManager().GetSubject(QuestionKey);
        }
        /// <summary>
        /// This method is used to Delete question based on the question key.
        /// </summary>
        /// <param name="questionID">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeleteInterviewQuestion(string questionID, int user)
        {
            new QuestionDLManager().DeleteInterviewQuestion(questionID, user);
        }
       
        /// <summary>
        /// This method is used to Get all the Answer choices based on the question key.
        /// </summary>
        /// <param name="questionKey">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <returns>
        /// A list of <see cref="AnswerChoice"/> that holds the AnswerChoice regarding this question.
        /// </returns>
        /// <remarks>
        /// Supports providing data.
        /// </remarks>
        public List<AnswerChoice> GetInterviewQuestionOptions(string questionKey)
        {
            return new QuestionDLManager().GetInterviewQuestionOptions(questionKey);
        }
        /// <summary>
        /// Gets questions for the automated test
        /// </summary>
        /// <param name="testSearchCrierias">search criteria given by the 
        /// user to generate questions</param>
        /// <param name="NoofQuestionsToPick">
        /// A <see cref="int"/> Number of questions in the test
        /// </param>
        /// <param name="dtSearchCriteria">
        /// search segment data table
        /// (Note this should be passed by ref: keyword)</param>
        /// <param name="StartIndex">
        /// A <see cref="int"/> that holds start index question.
        /// </param>
        /// <returns>
        /// A list for<see cref="QuestionDetail"/> 
        /// returns the final list of questions that should be shown to the user
        /// </returns>
        public List<QuestionDetail> GetAutomatedInterviewQuestions(List<TestSearchCriteria> testSearchCrierias,
            int NoofQuestionsToPick, ref DataTable dtSearchCriteria, int StartIndex, int questionAuthor,int tenantID)
        {
            List<QuestionDetail> questionDetails = null;
            List<QuestionDetail> returnQuestionDetails = null;
            List<QuestionDetail> SearchSegementsPickedQuestions = null;
            try
            {
                questionDetails = new List<QuestionDetail>();
                returnQuestionDetails = new List<QuestionDetail>();
                dtSearchCriteria.Columns.Add("ListStartIndex", typeof(int));
                dtSearchCriteria.Columns.Add("ListEndIndex", typeof(int));
                dtSearchCriteria.Columns.Add("Ratio", typeof(double));
                for (int i = 0; i < testSearchCrierias.Count; i++)
                {
                    int TotalRecords;
                    testSearchCrierias[i].TestStartIndex =
                            (StartIndex * Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"])) + 1;
                    testSearchCrierias[i].TestEndIndex =
                        (StartIndex * Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"])) +
                        NoofQuestionsToPick;
                    questionDetails = GetInterviewQuestionDetailsforAutomatedTest(testSearchCrierias[i], questionAuthor,tenantID, out TotalRecords);
                    if (Utility.IsNullOrEmpty(questionDetails))
                    {
                        dtSearchCriteria.Rows[i]["PickedQuestions"] = 0;
                        dtSearchCriteria.Rows[i]["Remarks"] = "No questions found for the search criteria";
                    }
                    else
                    {
                        if (SearchSegementsPickedQuestions == null)
                            SearchSegementsPickedQuestions = new List<QuestionDetail>();
                        dtSearchCriteria.Rows[i]["TotalRecordsinDB"] = TotalRecords;
                        dtSearchCriteria.Rows[i]["ListStartIndex"] = SearchSegementsPickedQuestions.Count;
                        SearchSegementsPickedQuestions.AddRange(questionDetails);
                        dtSearchCriteria.Rows[i]["ListEndIndex"] = questionDetails.Count;
                        TotalRecords = 0;
                        questionDetails = null;
                    }
                }
                PrepareInterviewQuestionListBasedOnRatioForAutomatedTest(ref dtSearchCriteria,
                    ref SearchSegementsPickedQuestions, ref returnQuestionDetails);
                return returnQuestionDetails;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(questionDetails)) questionDetails = null;
                if (!Utility.IsNullOrEmpty(returnQuestionDetails)) returnQuestionDetails = null;
                if (!Utility.IsNullOrEmpty(SearchSegementsPickedQuestions)) SearchSegementsPickedQuestions = null;
                if (dtSearchCriteria.Columns.Contains("ListStartIndex"))
                    dtSearchCriteria.Columns.Remove("ListStartIndex");
                if (dtSearchCriteria.Columns.Contains("ListEndIndex"))
                    dtSearchCriteria.Columns.Remove("ListEndIndex");
                if (dtSearchCriteria.Columns.Contains("Ratio"))
                    dtSearchCriteria.Columns.Remove("Ratio");
            }
        }

        /// <summary>
        /// This method communicates with the questiondetail DL Manager
        /// to get the questions for the automated test
        /// </summary>
        /// <param name="testSearchCriteria">search criteria to get the questions</param>
        /// <param name="TotalRecords">Total number of records picked 
        /// (Note:- this should be 'out' parameter)</param>
        /// <returns>List of question details picked from the database</returns>
        private List<QuestionDetail> GetInterviewQuestionDetailsforAutomatedTest(TestSearchCriteria testSearchCriteria,
           int questionAuthor,int tenantID, out int TotalRecords)
        {
            return new QuestionDLManager().
                    GetAutomatedInterviewQuestions(testSearchCriteria, questionAuthor,tenantID, out TotalRecords);
        }
        /// <summary>
        /// This method sorts the search criteria table according to the ratio and
        /// picks the questions.
        /// </summary>
        /// <param name="SearchSegmentTable">
        /// A <see cref="DataTable"/> contains the user provided search criteria
        /// </param>
        /// <param name="SegementsQuesionDetails">
        /// A List contains the question details picked by the search 
        /// criteria.
        /// </param>
        /// <param name="returnQuestionDetails">
        /// Final question details List to be shown 
        /// to the user
        /// </param>
        private void PrepareInterviewQuestionListBasedOnRatioForAutomatedTest
            (ref DataTable SearchSegmentTable, ref List<QuestionDetail> SegementsQuesionDetails,
            ref List<QuestionDetail> returnQuestionDetails)
        {
            int QuestionsAdded = 0;
            bool IsAnotherSegmentPicked = false;
            DataTable SortedRatioTable = null;
            DataView SortedRatioDataView = null;
            try
            {
                for (int LoopI = 0; LoopI < SearchSegmentTable.Rows.Count; LoopI++)
                {
                    if (Convert.ToInt32(SearchSegmentTable.Rows[LoopI]["TotalRecordsinDB"]) == 0)
                        SearchSegmentTable.Rows[LoopI]["Ratio"] = 0;
                    else
                        SearchSegmentTable.Rows[LoopI]["Ratio"] =
                            Convert.ToDouble(SearchSegmentTable.Rows[LoopI]["ExpectedQuestions"]) /
                            Convert.ToDouble(SearchSegmentTable.Rows[LoopI]["TotalRecordsinDB"]);
                }
                SortedRatioDataView = SearchSegmentTable.DefaultView;
                SortedRatioDataView.Sort = "Ratio DESC, TotalRecordsinDB ASC";
                SortedRatioTable = SortedRatioDataView.ToTable();
                SortedRatioDataView = null;
                for (int LoopI = 0; LoopI < SortedRatioTable.Rows.Count; LoopI++)
                {
                    if ((int)SortedRatioTable.Rows[LoopI]["TotalRecordsinDB"] != 0)
                        CheckandInsertQuestionDetails(SegementsQuesionDetails.GetRange(
                              (int)SortedRatioTable.Rows[LoopI]["ListStartIndex"],
                             (int)SortedRatioTable.Rows[LoopI]["ListEndIndex"]), ref returnQuestionDetails,
                        (int)SortedRatioTable.Rows[LoopI]["ExpectedQuestions"],
                           out QuestionsAdded, out IsAnotherSegmentPicked);
                    SortedRatioTable.Rows[LoopI]["PickedQuestions"] = QuestionsAdded;
                    SortedRatioTable.Rows[LoopI]["QuestionsDifference"] =
                        Convert.ToInt32(SortedRatioTable.Rows[LoopI]["ExpectedQuestions"]) - QuestionsAdded;
                    //
                    #region Error Messages or Information or Warnings
                    if (QuestionsAdded == 0)
                        SortedRatioTable.Rows[LoopI]["Remarks"] = "No questions found for the search criteria";
                    else if ((IsAnotherSegmentPicked) && (QuestionsAdded < (int)SortedRatioTable.Rows[LoopI]["ExpectedQuestions"]))
                        SortedRatioTable.Rows[LoopI]["Remarks"] = "This segment questions picked by another segment";
                    else if (QuestionsAdded < (int)SortedRatioTable.Rows[LoopI]["ExpectedQuestions"])
                        SortedRatioTable.Rows[LoopI]["Remarks"] = "Only " + QuestionsAdded.ToString() +
                            (QuestionsAdded.ToString() == "1" ? " question " : " questions ") + "found for this search criteria";
                    else if (QuestionsAdded == (int)SortedRatioTable.Rows[LoopI]["ExpectedQuestions"])
                        SortedRatioTable.Rows[LoopI]["Remarks"] = "Search Successfull";
                    #endregion Error Messages or Information or Warnings
                    //
                    QuestionsAdded = 0;
                    IsAnotherSegmentPicked = false;
                }
                SortedRatioDataView = SortedRatioTable.DefaultView;
                SortedRatioDataView.Sort = "SNO ASC";
                SearchSegmentTable = null;
                SearchSegmentTable = SortedRatioDataView.ToTable();
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SortedRatioTable)) SortedRatioTable = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(SortedRatioDataView)) SortedRatioDataView = null;
            }
        }



        /// <summary>
        /// Gets questions for the automated test
        /// </summary>
        /// <param name="testSearchCrierias">search criteria given by the 
        /// user to generate questions</param>
        /// <param name="NoofQuestionsToPick">
        /// A <see cref="int"/> Number of questions in the test
        /// </param>
        /// <param name="dtSearchCriteria">
        /// search segment data table
        /// (Note this should be passed by ref: keyword)</param>
        /// <param name="StartIndex">
        /// A <see cref="int"/> that holds start index question.
        /// </param>
        /// <returns>
        /// A list for<see cref="QuestionDetail"/> 
        /// returns the final list of questions that should be shown to the user
        /// </returns>
        public List<QuestionDetail> GetRecommendedAutomatedQuestions(QuestionType questionType, List<TestSearchCriteria> testSearchCrierias,
            int NoofQuestionsToPick, ref DataTable dtSearchCriteria, int StartIndex, int questionAuthor)
        {
            List<QuestionDetail> questionDetails = null;
            List<QuestionDetail> returnQuestionDetails = null;
            List<QuestionDetail> SearchSegementsPickedQuestions = null;
            try
            {
                questionDetails = new List<QuestionDetail>();
                returnQuestionDetails = new List<QuestionDetail>();
                dtSearchCriteria.Columns.Add("ListStartIndex", typeof(int));
                dtSearchCriteria.Columns.Add("ListEndIndex", typeof(int));
                dtSearchCriteria.Columns.Add("Ratio", typeof(double));
                if (testSearchCrierias == null)
                    return null;
                for (int i = 0; i < testSearchCrierias.Count; i++)
                {
                    int TotalRecords;
                    testSearchCrierias[i].TestStartIndex =
                       (StartIndex * Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"])) + 1;
                    testSearchCrierias[i].TestEndIndex = (StartIndex * Convert.ToInt32(dtSearchCriteria.Rows[i]["ExpectedQuestions"])) +
                        NoofQuestionsToPick;
                    questionDetails = GetQuestionDetailsforAutomatedTest(questionType, testSearchCrierias[i], questionAuthor, out TotalRecords);
                    /*if (Utility.IsNullOrEmpty(questionDetails))
                    {
                        dtSearchCriteria.Rows[i]["PickedQuestions"] = 0;
                        dtSearchCriteria.Rows[i]["Remarks"] = "No questions found for the search criteria";
                    }
                    else
                    {*/
                    if (SearchSegementsPickedQuestions == null)
                        SearchSegementsPickedQuestions = new List<QuestionDetail>();

                        //questionDetails = GetQuestionDetailsforAutomatedTest(testSearchCrierias[i], questionAuthor, out TotalRecords);

                    
                    dtSearchCriteria.Rows[i]["TotalRecordsinDB"] = TotalRecords;
                    dtSearchCriteria.Rows[i]["ListStartIndex"] = SearchSegementsPickedQuestions.Count;

                    if (questionDetails != null)
                    {
                        SearchSegementsPickedQuestions.AddRange(questionDetails);
                        dtSearchCriteria.Rows[i]["ListEndIndex"] = questionDetails.Count;
                    }
                    else 
                    {
                        /*Added MKN 25 Feb 2012--> For avoiding ListEndindex empty value*/
                        dtSearchCriteria.Rows[i]["ListEndIndex"] = 0;
                    }
                    TotalRecords = 0;
                    questionDetails = null;
                    //}
                }
                PrepareQuestionListBasedOnRatioForAutomatedTest(ref dtSearchCriteria,
                    ref SearchSegementsPickedQuestions, ref returnQuestionDetails);
                return returnQuestionDetails;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(questionDetails)) questionDetails = null;
                if (!Utility.IsNullOrEmpty(returnQuestionDetails)) returnQuestionDetails = null;
                if (!Utility.IsNullOrEmpty(SearchSegementsPickedQuestions)) SearchSegementsPickedQuestions = null;
                if (dtSearchCriteria.Columns.Contains("ListStartIndex"))
                    dtSearchCriteria.Columns.Remove("ListStartIndex");
                if (dtSearchCriteria.Columns.Contains("ListEndIndex"))
                    dtSearchCriteria.Columns.Remove("ListEndIndex");
                if (dtSearchCriteria.Columns.Contains("Ratio"))
                    dtSearchCriteria.Columns.Remove("Ratio");
            }
        }
    }
}
