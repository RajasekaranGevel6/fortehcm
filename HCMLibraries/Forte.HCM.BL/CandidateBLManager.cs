﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateBLManager.cs
// File that represents the business layer for the Candidate module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the Candidate module.
    /// This includes functionalities for retrieving the Test information ,
    /// schelduler Details,credit information, create self test session,
    /// resumes details, etc. 
    /// This will talk to the data layer for performing these operations.
    /// </summary>
    public class CandidateBLManager
    {
        #region Public Methods                                                 

        /// <summary>
        /// Retrieve the test instructions disclaimer message.
        /// </summary>
        /// <returns>
        /// A <see cref="DisclaimerMessage"/> that holds the disclaimer
        /// messages.
        /// </returns>
        public DisclaimerMessage GetTestInstructionsDisclaimerMessage()
        {
            return new CandidateDLManager().GetTestInstructionsDisclaimerMessage();
        }

        /// <summary>
        /// Retrieve the interview instructions disclaimer message.
        /// </summary>
        /// <returns>
        /// A <see cref="DisclaimerMessage"/> that holds the disclaimer
        /// messages.
        /// </returns>
        public DisclaimerMessage GetInterviewInstructionsDisclaimerMessage()
        {
            return new CandidateDLManager().GetInterviewInstructionsDisclaimerMessage();
        }


        /// <summary>
        /// Retrive the Candidate Details 
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate id.
        /// </param>
        /// <returns>
        /// Candidate Details
        /// </returns>
        public CandidateSummary GetCandidateSummary(int candidateID)
        {
            return new CandidateDLManager().GetCandidateSummary(candidateID);
        }

        /// <summary>
        /// Method that retrieves the candidate activity summary.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateActivitySummary"/> that holds the candidate
        /// activity summary.
        /// </returns>
        public CandidateActivitySummary GetCandidateActivitySummary(int candidateID)
        {
            return new CandidateDLManager().GetCandidateActivitySummary(candidateID);
        }

        // <summary>
        /// Method that retrieves the candidate pending test activities
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateTestSessionDetail"/> that holds the 
        /// candidate pending activities.
        /// </returns>
        public List<CandidateTestSessionDetail> GetPendingTestActivities
            (int candidateID, int pageNumber, int pageSize, out int totalRecords)
        {
            return new CandidateDLManager().GetPendingTestActivities
                (candidateID, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the candidate pending interview activities
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInterviewSessionDetail"/> that holds the 
        /// candidate pending interview activities.
        /// </returns>
        public List<CandidateInterviewSessionDetail> GetPendingInterviewActivities
            (int candidateID)
        {
            return new CandidateDLManager().GetPendingInterviewActivities(candidateID);
        }

        /// <summary>
        /// Retrive the test introduction details.
        /// </summary>
        /// <param name="candidateSessionID">
        ///  A <see cref="string"/> that holds the candidate Session ID.
        /// </param>
        /// <returns>
        ///  <see cref="TestIntroductionDetail"/>Test Introduction Detail
        /// </returns>
        public TestIntroductionDetail GetTestIntroduction(string candidateSessionID)
        {
            return new CandidateDLManager().GetTestIntroduction(candidateSessionID);
        }

        /// <summary>
        /// Retrieves the interview introduction detail.
        /// </summary>
        /// <param name="candidateSessionID">
        ///  A <see cref="string"/> that holds the candidate Session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewIntroductionDetail"/> that holds the interview
        /// introduction detail.
        /// </returns>
        public InterviewIntroductionDetail GetInterviewIntroduction
            (string candidateSessionID, int attemptID)
        {
            return new CandidateDLManager().GetInterviewIntroduction(candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method that will help to retrieve the scheduler, candidate and test details
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session id.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <returns>
        /// A <see cref="TestScheduleDetail"/> that contains the scheduler information.
        /// </returns>
        public TestScheduleDetail GetSchedulerDetails(string candidateSessionID, int attemptID)
        {
            return new CandidateDLManager().GetSchedulerDetails(candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method that retrieves the test scheduer email detail, that allows
        /// candidates to send email to them.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestScheduleDetail"/> that holds the scheduler detail.
        /// </returns>
        public TestScheduleDetail GetTestSchedulerEmailDetail
            (string candidateSessionID, int attemptID)
        {
            return new CandidateDLManager().GetTestSchedulerEmailDetail
                (candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method that retrieves the interview scheduer email detail, that allows
        /// candidates to send email to them.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestScheduleDetail"/> that holds the scheduler detail.
        /// </returns>
        public TestScheduleDetail GetInterviewSchedulerEmailDetail
            (string candidateSessionID, int attemptID)
        {
            return new CandidateDLManager().GetInterviewSchedulerEmailDetail
                (candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method that retrieves the recruiter email detail, that allows
        /// candidates to send email to them.
        /// </summary>
        /// <param name="candidateLoginID">
        /// A <see cref="int"/> that holds the candidate login ID.
        /// </param>
        /// <returns>
        /// A <see cref="RecruiterDetail"/> that holds the recruiter detail.
        /// </returns>
        public RecruiterDetail GetRecruiterEmailDetail
            (int candidateLoginID)
        {
            return new CandidateDLManager().GetRecruiterEmailDetail(candidateLoginID);
        }

        /// <summary>
        /// Method that will help to retrieve the Candidate Test Details Like Pending tests,
        /// Expired Tests and completed Tests
        /// </summary>
        /// <param name="candidateTestStatus">
        /// A <see cref="CandidateTestStatus"/> that holds the Candidate Test Status Like Completed ,
        /// inprogrss etc...
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="string"/> that holds the candidate ID.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order By.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the orderBy Direction Like 'A' Or 'D'.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the candidate total Records.
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateTestDetail"/> that holds the List of Candidate Test Details.
        /// </returns>
        public List<CandidateTestDetail> GetTests(CandidateTestStatus candidateTestStatus,
            int candidateID, string orderBy, SortType orderByDirection,
            int pageNumber, int pageSize, out int totalRecords)
        {
            totalRecords = 0;

            switch (candidateTestStatus)
            {
                case CandidateTestStatus.All:
                    return null;

                case CandidateTestStatus.Pending:
                    return new CandidateDLManager().GetPendingTests(candidateID, orderBy,
                        orderByDirection, pageNumber, pageSize, out totalRecords);

                case CandidateTestStatus.Completed:
                    return new CandidateDLManager().GetCompletedTests(candidateID, orderBy,
                        orderByDirection, pageNumber, pageSize, out totalRecords);

                case CandidateTestStatus.Expired:
                    return new CandidateDLManager().GetExpiredTests(candidateID, orderBy,
                        orderByDirection, pageNumber, pageSize, out totalRecords);

                default:
                    return null;
            }

        }

        /// <summary>
        /// check whether Cyber Proctor Enabled or not 
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains Cyber Proctor Enabled or not.
        /// </returns>
        public bool IsCyberProctorEnabled(string cansessionKey, int attempID)
        {
            return new CandidateDLManager().IsCyberProctorEnabled(cansessionKey, attempID);
        }

        /// <summary>
        /// check whether Cyber Proctor Started or not 
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains Cyber Proctor Started or not.
        /// </returns>
        public bool IsCyberProctorStarted(string cansessionKey, int attempID)
        {
            return new CandidateDLManager().IsCyberProctorStarted(cansessionKey, attempID);
        }

        /// <summary>
        /// Update the session Status. 
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains the cansessionKey.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains the attempt ID.
        /// </param>
        public void UpdateTestStatus(string cansessionKey, int attempID)
        {
            //new CandidateDLManager().UpdateTestStatus(cansessionKey, attempID);
        }

        /// <summary>
        /// Method that will be called when the user wants to retake/reactivate
        /// a test.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains candidate session key.
        /// </param>
        /// <param name="requestType">
        /// A <see cref="string"/> that contains an request type either 
        /// retake/reactivate request.
        /// </param>
        public void UpdateTestRequest(string candidateSessionKey, string requestType)
        {
            new CandidateDLManager().UpdateTestRequest(candidateSessionKey, requestType);
        }
        /// <summary>
        /// Update the authetication code.
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains attempt ID.
        /// </param>
        /// <param name="authCode">
        /// A <see cref="string"/> that contains authendication code.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains authendication Code is true or false.
        /// </returns>
        public bool UpdateAuthCode(string cansessionKey, int attempID, string authCode)
        {
            return new CandidateDLManager().UpdateAuthCode(cansessionKey, attempID, authCode);
        }


        /// <summary>
        /// Method that updates the interview security code.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID>
        /// </param>
        /// <param name="securityCode">
        /// A <see cref="string"/> that holds the security code.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status whether security code 
        /// already exist or not. True if already exist and false not exist.
        /// </returns>
        public bool UpdateInterviewSecurityCode(string candidateSessionID,
            int attemptID, string securityCode)
        {
            return new CandidateDLManager().UpdateInterviewSecurityCode
                (candidateSessionID, attemptID, securityCode);
        }
        
        /// <summary>
        /// Method that updates the interview session tracking status.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID>
        /// </param>
        /// <param name="sessionStatus">
        /// A <see cref="string"/> that holds the session status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status whether security code 
        /// already exist or not. True if already exist and false not exist.
        /// </returns>
        public void UpdateInterviewSessionTrackingStatus(string candidateSessionID,
            int attemptID, string sessionStatus, int userID)
        {
            new CandidateDLManager().UpdateInterviewSessionTrackingStatus
                (candidateSessionID, attemptID, sessionStatus, userID);
        }

        /// <summary>
        /// Method that retrieves the test detail.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <returns>
        ///  A <see cref="TestDetail"/> that holds the test detail.
        /// </returns>
        public TestDetail GetTestDetail(string candidateSessionID)
        {
            return new CandidateDLManager().GetTestDetail(candidateSessionID);
        }

        /// <summary>
        /// Method that retrieves the interview detail.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        ///  A <see cref="InterviewDetail"/> that holds the interview detail.
        /// </returns>
        public InterviewDetail GetInterviewDetail(string candidateSessionID, int attemptID)
        {
            return new CandidateDLManager().GetInterviewDetail(candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method that retrieves the test and session details.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds attempt ID.
        /// </param>
        /// <returns>
        ///  A <see cref="TestDetail"/> that contains Test Detail.
        /// </returns>
        public TestDetail GetTestSessionDetail(string candidateSessionID, int attemptID)
        {
            return new CandidateDLManager().GetTestSessionDetail
                (candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method to get the candidate name for the given candidate id
        /// </summary>
        /// <param name="candidateId">
        /// A<see cref="string"/>that holds the candidate id 
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the candidate name
        /// </returns>
        public string GetCandidateName(string candidateId)
        {
            return new CandidateDLManager().GetCandidateName(candidateId);
        }

        /// <summary>
        /// Method to get the candidate name for the given candidate id
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the Candidate Session id
        /// </param>
        /// <returns>
        /// A<see cref="CandidateDetail"/>that holds the candidate detail
        /// </returns>
        public CandidateDetail GetCandidateDetail(string candidateSessionID)
        {
            return new CandidateDLManager().GetCandidateDetail(candidateSessionID);
        }

        public CandidateDetail GetCandidateInformation(int candidateID)
        {
            return new CandidateDLManager().GetCandidateInformation(candidateID);
        }

        public List<CandidateTestSessionDetail> GetTestActivityDashboard(int candidateID,
            int pageNumber, int pageSize,string searchText, out int totalRecords)
        {
            return new CandidateDLManager().GetTestActivityDashboard(candidateID,
                pageNumber, pageSize,searchText, out totalRecords);
        }

        public List<CandidateInterviewSessionDetail> GetInterviewActivityDashboard(int candidateID,
            int pageNumber, int pageSize,string searchText, out int totalRecords)
        {
            return new CandidateDLManager().GetInterviewActivityDashboard(candidateID,
                pageNumber, pageSize, searchText, out totalRecords);
        } 

        /// <summary>
        /// Method to get the candidate name for the given candidate id
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the Candidate Session id
        /// </param>
        /// <returns>
        /// A<see cref="CandidateDetail"/>that holds the candidate detail
        /// </returns>
        public CandidateDetail GetCandidateBasicDetail(int candidateID)
        {
            return new CandidateDLManager().GetCandidateBasicDetail(candidateID);
        }

        /// <summary>
        /// Method that inserts a credit request.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertCreditRequest(int userID)
        {
            new CandidateDLManager().InsertCreditRequest(userID);
        }

        /// <summary>
        /// Method that checks if a credit request is already exists for 
        /// an user.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public bool IsCreditRequestExist(int userID)
        {
            return new CandidateDLManager().IsCreditRequestExist(userID);
        }

        /// <summary>
        /// Method to get the candidate resume for the given candidate session id
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="int"/>that holds the candidate session id
        /// </param>
        /// <param name="mimeType">
        /// A <see cref="string"/> that holds the mime type as an output 
        /// parameter.
        /// </param>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the filename as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A<see cref="byte"/>that holds the resume data.
        /// </returns>
        public byte[] GetResumeContents(string candidateSessionID,
            out string mimeType, out string fileName)
        {
            return new CandidateDLManager().GetResumeContents
                (candidateSessionID, out mimeType, out fileName);
        }

        public byte[] GetResumeContentsByCandidiateID(int candidateID,
            out string mimeType, out string fileName)
        {
            return new CandidateDLManager().GetResumeContentsByCandidiateID
                (candidateID, out mimeType, out fileName);
        }

        /// <summary>
        /// Method that retrives additional document content
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public byte[] GetDocumentContentsByDocumentId(int documentId,out string fileName)
        {
            return new CandidateDLManager().GetDocumentContent(documentId, out fileName);
        }

        public List<CareerBuilderResume> GetAdditionalDocumentContent(string candidateIds,
            int positionProfileID)
        {
            return new CandidateDLManager().GetAdditionalDocument(candidateIds, positionProfileID); 
        }
 

        /// <summary>
        /// Method to get the candidate HRXml for the given candidate session id. 
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the candidate session id 
        /// </param>        
        /// <returns>
        /// A<see cref="string"/>that holds the HRXml source.
        /// </returns>
        public string GetCandidateHRXml(string candidateSessionID)
        {
            return new CandidateDLManager().GetCandidateHRXml(candidateSessionID);
        }


        public string GetCandidateHRXLDetail(int candidateID)
        {
            return new CandidateDLManager().GetCandidateHRXLDetail(candidateID);
        }

        /// <summary>
        /// Method to get the candidate HRXml for the given candidate session id. 
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the candidate session id 
        /// </param>        
        /// <returns>
        /// A<see cref="string"/>that holds the HRXml source.
        /// </returns>
        public string GetInterviewCandidateHRXml(string candidateSessionID)
        {
            return new CandidateDLManager().GetInterviewCandidateHRXml(candidateSessionID);
        }

        /// <summary>
        /// Method that retrieves the validation objects.
        /// </summary>
        /// <param name="candidateid">
        /// An <see cref="string"/> that holds the specific candidate id.
        /// </param>
        ///  <param name="testID">
        /// An <see cref="string"/> that holds the specific test id.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the pending attempts as an output 
        /// parameter.
        /// </param>
        /// <param name="isCertificateTest">
        /// A <see cref="string"/> that holds is Certificate Test as an output 
        /// parameter.
        /// </param>
        /// <param name="retakeCount">
        /// A <see cref="string"/> that holds retake count as an output 
        /// parameter.
        /// </param>
        /// <param name="dicRetakeDetails">
        /// that holds the retake details as an output 
        /// parameter.
        /// </param>
        public string TestRetakeValidation(string candidateid, string testID, int attemptID,
            out string retakeCount, out string isCertificateTest, out Dictionary<string, string> dicRetakeDetails)
        {
            return new CandidateDLManager().TestRetakeValidation(candidateid, testID, attemptID,
                out retakeCount, out isCertificateTest, out dicRetakeDetails);
        }

        /// <summary>
        /// Method that saves the interview reminder.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="InterviewReminderDetail"/> that holds the interview 
        /// reminder detail
        /// </param>        
        public void SaveInterviewReminder(InterviewReminderDetail reminderDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                for (int i = 0; i < reminderDetail.Intervals.Count; i++)
                {
                    reminderDetail.IntervalID = reminderDetail.Intervals[i].IntervalID.ToString();
                    if (reminderDetail.Intervals[i].RecordStatus == RecordStatus.New)
                    {   
                        // Insert interview reminder detail.
                        new CandidateDLManager().InsertInterviewReminder(reminderDetail, transaction);
                    }
                    else if (reminderDetail.Intervals[i].RecordStatus == RecordStatus.Deleted)
                    {   
                        // Delete interview reminder detail.
                        new CandidateDLManager().DeleteInterviewReminder(reminderDetail, transaction);
                    }

                    else if (reminderDetail.Intervals[i].RecordStatus == RecordStatus.Modified)
                    {   
                        // Delete interview reminder detail.
                        new CandidateDLManager().UpdateInterviewReminder(reminderDetail, transaction);
                    }
                }
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the interview reminder detail for the given 
        /// candidate session ID and attempt ID.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID>
        /// </param>
        /// <param name="attemptID"> 
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewReminderDetail"/> that holds the interview
        /// reminder detail.
        /// </returns>
        public InterviewReminderDetail GetInterviewReminder
            (string candidateSessionID, int attemptID)
        {
            return new CandidateDLManager().GetInterviewReminder
                (candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method that deletes all interview reminders.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds attempt ID.
        /// </param>
        /// </param>
        public void DeleteAllInterviewReminders(string candidateSessionID, int attemptID)
        {
            new CandidateDLManager().DeleteAllInterviewReminders
                (candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method that retrieves the pending interviews.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="string"/> that holds the candidate ID.
        /// </param>
        /// <param name="orderByColumn">
        /// A <see cref="string"/> that holds the order by column
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInterviewDetail"/> that holds the 
        /// pending interview details.
        /// </returns>
        public List<CandidateInterviewDetail> GetPendingInterviews(int candidateID,
            string orderByColumn, SortType orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new CandidateDLManager().GetPendingInterviews(candidateID,
                orderByColumn, orderByDirection, pageNumber,
                pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the completed interviews.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="string"/> that holds the candidate ID.
        /// </param>
        /// <param name="orderByColumn">
        /// A <see cref="string"/> that holds the order by column
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInterviewDetail"/> that holds the 
        /// completed interview details.
        /// </returns>
        public List<CandidateInterviewDetail> GetCompletedInterviews(int candidateID,
            string orderByColumn, SortType orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new CandidateDLManager().GetCompletedInterviews(candidateID,
               orderByColumn, orderByDirection, pageNumber,
               pageSize, out totalRecords);
        }

        /// <summary>
        /// Retrive the Candidate Details 
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate id.
        /// </param>
        /// <returns>
        /// Candidate Details
        /// </returns>
        public CandidateSummary GetCandidateActivities(int candidateID)
        {
            return new CandidateDLManager().GetCandidateActivities(candidateID);
        }

        /// <summary>
        /// Method that update the user profile details such as first name, 
        /// last name, email.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        public void UpdateUserProfile(UserDetail userDetail)
        {
            new CandidateDLManager().UpdateUserProfile(userDetail);
        }

        /// <summary>
        /// Method that deletes the candidate resume.
        /// </summary>
        /// <param name="candidateInfoID">
        /// A <see cref="int"/> that holds the candidate reume ID.
        /// </param>
        public void DeleteCandidateResume(int candidateResumeID)
        {
            new CandidateDLManager().DeleteCandidateResume(candidateResumeID);
        }

         /// <summary>
        /// Method that retrieves the candidate test recommendation detail.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="testRecommendationID">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestDetail"/> that holds the test recommendation detail.
        /// </returns>
        public TestDetail GetCandidateTestRecommendationDetail(int userID, int testRecommendationID)
        {
            return new CandidateDLManager().GetCandidateTestRecommendationDetail(userID, testRecommendationID);
        }

        /// <summary>
        /// Method that updates the candidate test recommendation status.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="testRecommendationID">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status. The possible statuses are:
        /// SAT_COMP, SAT_SAVD, SAT_SCHD.
        /// </param>
        public void UpdateCandidateTestRecommendationStatus(int userID, int testRecommendationID,
            string testKey, string status)
        {
            new CandidateDLManager().UpdateCandidateTestRecommendationStatus(userID, testRecommendationID,
                testKey, status);
        }

         /// <summary>
        /// Method that deletes the candidate recommended test.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="recommendedTestID">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        public void DeleteCandidateRecommendedTest(int userID, int testRecommendationID)
        {
            new CandidateDLManager().DeleteCandidateRecommendedTest(userID, testRecommendationID);
        }

        /// <summary>
        /// Method that retrieves the count of saved and completed self admin
        /// test for the given month and year.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the candidate user ID.
        /// </param>
        /// <param name="month">
        /// A <see cref="int"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the count.
        /// </returns>
        public int GetSelfAdminTestCount(int userID, int month, int year)
        {
            return new CandidateDLManager().GetSelfAdminTestCount(userID, month, year);
        }

        /// <summary>
        /// Method that inserts the resume reminder detail.
        /// </summary>
        /// <param name="resumeReminderDetail">
        /// A <see cref="ResumeReminderDetail"/> that holds the resume reminder 
        /// detail.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the status. 0 indicates reminder
        /// created successfully and 1 indicates already a reminder available 
        /// for the same reminder date.
        /// </returns>
        public int InsertResumeReminder(ResumeReminderDetail resumeReminderDetail)
        {
            return new CandidateDLManager().InsertResumeReminder(resumeReminderDetail);
        }

        /// <summary>
        /// Method that retrieves the list of resume reminder sender list 
        /// for the given date/time. Mails needs to be sent for this list.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<ResumeReminderDetail> GetResumeReminderSenderList(DateTime currentDateTime)
        {
            return new CandidateDLManager().GetResumeReminderSenderList(currentDateTime);
        }

        /// <summary>
        /// Method that will update the resume reminder sent status for the ID.
        /// </summary>
        /// <param name="genID">
        /// A <see cref="int"/> that holds the gen ID.
        /// </param>
        /// <param name="reminderSent">
        /// A <see cref="bool"/> that holds the reminder sent status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateResumeReminderStatus(int genID, bool reminderSent, int userID)
        {
            new CandidateDLManager().UpdateResumeReminderStatus(genID, reminderSent, userID);
        }

        public int CreateCandidate(int candidateID)
        {
            return  new CandidateDLManager().CreateCandidate(candidateID);
        }

        public void CreateCandidateNewResume(int temporaryCandiateID, int candidateID)
        {
              new CandidateDLManager().CreateCandidateNewResume(temporaryCandiateID,candidateID);
        }

        /// <summary>
        /// get the candidate id using prasuser id or testKey
        /// </summary>
        /// <param name="prasUserID"></param>
        /// <param name="sessionID"></param>
        /// <returns></returns>
        public int GetCandidateID(int prasUserID, string sessionKey)
        {
            return new CandidateDLManager().GetCandidateID(prasUserID, sessionKey);
        }

        /// <summary>
        /// Method that retrieves the daily test reminder list for mailing to 
        /// candidates.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CandidateTestSessionDetail"/> that holds the mailing 
        /// list.
        /// </returns>
        public List<CandidateTestSessionDetail> GetDailyTestReminders()
        {
            return new CandidateDLManager().GetDailyTestReminders();
        }

        /// <summary>
        /// Method that retrieves the daily interview reminder list for mailing to 
        /// candidates.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CandidateInterviewSessionDetail"/> that holds the mailing 
        /// list.
        /// </returns>
        public List<CandidateInterviewSessionDetail> GetDailyInterviewReminders()
        {
            return new CandidateDLManager().GetDailyInterviewReminders();
        }

        public int DeleteCandidate(int candidateID)
        {
            return new CandidateDLManager().DeleteCandidate(candidateID);  
        }


        /// <summary>
        /// Method retrives the online interview details for the candidate
        /// </summary>
        /// <param name="candidateId">
        /// <see cref="System.Int32"/>that holds the candidate id
        /// </param>
        /// <returns>
        /// Object <see cref="OnlineCandidateSessionDetail"/> holds the interview details
        /// </returns>
        public OnlineCandidateSessionDetail GetOnlineInterviewDetail(int candidateId, int userId, string chatRoomId)
        {
            return new CandidateDLManager().GetOnlineInterviewDetail(candidateId,userId,chatRoomId); 
        }

        #endregion Public Methods

        #region Signup Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="signupCandidate"></param>
        /// <returns></returns>
        public int InsertSignupCandidateSkill(
            SignupCandidate signupCandidate, IDbTransaction transaction)
        {
            return new CandidateDLManager().
                InsertSignupCandidateSkill(signupCandidate, transaction);
        }

        public List<SignupCandidate> GetCandidateSkills(int CandidateID)
        {
            return new CandidateDLManager().GetCandidateSkill(CandidateID);
        }

        public int DeleteCandidateSkill(int CandidateID, int SkillID
            , IDbTransaction transaction)
        {
            return new CandidateDLManager().
                DeleteCandidateSkill(CandidateID, SkillID, transaction);
        }

        public bool VerifyOpenEmailID(string userName)
        {
            return new CandidateDLManager().
                VerifyOpenEmailID(userName);
        }

        /// <summary>
        /// Gets the candidate skills.
        /// </summary>
        /// <param name="prefixKeyword">The prefix keyword.</param>
        /// <returns></returns>
        public string[] GetCandidateSearchSkill(string prefixKeyword)
        {
            return new CandidateDLManager().GetCandidateSearchSkill(prefixKeyword);
        }

        /// <summary>
        /// Gets the self test details.
        /// </summary>
        /// <param name="candidateID"></param>
        /// <returns></returns>
        public CandidateSummary GetCandidateSelfTestDetail(int candidateID)
        {
            return new CandidateDLManager().GetCandidateSelfTestDetail(candidateID);
        }

        /// <summary>
        /// Method that retrieves the candidate resume status.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="ResumeStatus"/> that holds the resume status.
        /// </returns>
        public ResumeStatus GetCandidateResumeStatus(int userID)
        {
            return new CandidateDLManager().GetCandidateResumeStatus(userID);
        }

        /// <summary>
        /// Retrive the Candidate Details 
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate id.
        /// </param>
        /// <returns>
        /// Candidate Details
        /// </returns>
        public CandidateSummary GetCandidateActivity(int candidateID)
        {
            return new CandidateDLManager().GetCandidateActivity(candidateID);
        }

        public List<SignupCandidate> GetUserInfomation(int userID)
        {
            return new CandidateDLManager().GetUserInfomation(userID);
        }
        public int GetUserID(string userName)
        {
            return new CandidateDLManager().GetUserID(userName);
        }

        /// <summary>
        /// Method to get the candidate user name
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds the candidate candidate id 
        /// </param>        
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        public UserDetail GetUserNameByCandidateID(int candidateID)
        {
            return new CandidateDLManager().GetUserNameByCandidateID(candidateID);
        }

        /// <summary>
        /// Method to update the user name
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id
        /// </param>        
        /// <param name="userName">
        /// A<see cref="string"/>that holds the user name
        /// </param>
        /// <param name="modifiedBy">
        /// A<see cref="int"/>that holds the modified by
        /// </param>        
        public void UpdateUserName(int userID, string userName,
            int modifiedBy)
        {
            new CandidateDLManager().UpdateUserName(userID, userName, modifiedBy);
        }


        public void InsertAdditionalDocuments(AdditionalDocuments addiDoc)
        {
            new CandidateDLManager().InsertAdditionalDocuments(addiDoc);
        }

        public List<AdditionalDocuments> GetCandidateAdditionalDocuments(int candidateId)
        {
          return   new CandidateDLManager().GetCandidateAdditionalDocuments(candidateId);
        }

        public void DeleteAdditionalDocument(int docId)
        {
            new CandidateDLManager().DeleteAdditionalDocument(docId);
        }
        #endregion Signup Methods
    }
}