﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientBLManager.cs
// File that represents the business layer for the Client managment module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;
using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.BL
{
    public class ClientBLManager
    {
        /// <summary>
        /// Inserts the client.
        /// </summary>
        /// <param name="clientInformation">The client information.</param>
        public void InsertClient(ClientInformation clientInformation,out int clinetID)
        {
            new ClientDLManager().InsertClient(clientInformation,out clinetID);
        }


        public List<ClientInformation> GetClientInformation(ClientSearchCriteria clientSearchCriteria, int pageNumber,
            int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            return new ClientDLManager().GetClientInformation(clientSearchCriteria, pageNumber,
           pageSize, orderBy, orderByDirection, out totalRecords);
        }
        public ClientInformation GetClientInformationByClientId(int clientId)
        {
            return new ClientDLManager().GetClientInformationByClientId(clientId);
        }
       
        public void UpdateClient(ClientInformation clientInfo,out int clientCount)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // If existing test.
                new ClientDLManager().UpdateClient(clientInfo,out clientCount, transaction);
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        public void UpdateDepartment(Department departmentInfo, out int departmentCount)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // If existing test.
                new ClientDLManager().UpdateDepartment(departmentInfo, out departmentCount, transaction);
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }



        public void InsertDepartment(Department clientInformation, out int departmentID)
        {
            new ClientDLManager().InsertDepartment(clientInformation, out departmentID);
        }

        public List<Department> GetDepartments(int clientID)
        {
           return new ClientDLManager().GetDepartments(clientID);
        }

        public Department GetDepartment(int departmentID)
        {
            return new ClientDLManager().GetDepartment(departmentID);
        }


        public void InsertContact(ClientContactInformation clientContactInformation, out int contactID )
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                new ClientDLManager().InsertContact(clientContactInformation, out contactID);
                if (contactID > 0)
                {
                    new ClientDLManager().InsertClientContact(clientContactInformation.ClientID, contactID, clientContactInformation.CreatedBy);
                    if (!Utility.IsNullOrEmpty(clientContactInformation.SellectedDeparments))
                        new ClientDLManager().InsertDepartmentContact(clientContactInformation.TenantID, contactID, clientContactInformation.SellectedDeparments, clientContactInformation.CreatedBy);
                }
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        public List<Department> GetClientDepartmentInformation(ClientSearchCriteria clientSearchCriteria, int pageNumber,
            int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            return new ClientDLManager().GetClientDepartmentInformation(clientSearchCriteria, pageNumber,
           pageSize, orderBy, orderByDirection, out totalRecords);
        }

        public List<ClientContactInformation> GetClientContactInformation(ClientSearchCriteria clientSearchCriteria, int pageNumber,
            int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            return new ClientDLManager().GetClientContactInformation(clientSearchCriteria, pageNumber,
           pageSize, orderBy, orderByDirection, out totalRecords);
        } 
        

        #region Client Management Settings

        /// <summary>
        /// Method that updates the client management settings
        /// </summary>
        /// <param name="settings">
        /// A <see cref="ClientManagementSettingsDetail"/> that holds the client 
        /// management settings.
        /// </param>
        public void UpdateClientManagementSettings(ClientManagementSettingsDetail settings)
        {
            new ClientDLManager().UpdateClientManagementSettings(settings);
        }

        /// <summary>
        /// Method that retreives the client management settings.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A <see cref="ClientManagementSettingsDetail"/> that holds the client 
        /// management settings.
        /// </returns>
        public ClientManagementSettingsDetail GetClientManagementSettings(int tenantID)
        {
            return new ClientDLManager().GetClientManagementSettings(tenantID);
        }

        #endregion Client Management Settings

        public List<ClientInformation> GetClientName(int tenantID)
        {
            return new ClientDLManager().GetClientName(tenantID);
        }

        public ClientContactInformation GetContact(int contactID)
        {
            return new ClientDLManager().GetContact(contactID);
        }

        public void UpdateContact(ClientContactInformation clientContactInformation, out int contactID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {

                new ClientDLManager().UpdateContact(clientContactInformation, out contactID, transaction);
                if (!Utility.IsNullOrEmpty(clientContactInformation.SellectedDeparments))
                    new ClientDLManager().UpdateDepartments(clientContactInformation ,transaction);
                
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        public int GetClientContactProstionProfileDetails(int contactID)
        {
            return new ClientDLManager().GetClientContactProstionProfileDetails(contactID);            

        }
        public int GetDepartmentCOntactsDetails(short departmentID)
        {
            return new ClientDLManager().GetDepartmentCOntactsDetails(departmentID);      
        }
        public int GetClientContactDetails(short clientID)
        {
            return new ClientDLManager().GetClientContactDetails(clientID);   
        }
        public int DeleteClientContactProstionProfileDetails(short contact_id,int deleteCondition)
        {
            return new ClientDLManager().DeleteClientContactProstionProfileDetails(contact_id, deleteCondition);
        }

        public int DeleteDepartmentClientContactDetails(short department_id, int deleteCondition)
        {
            return new ClientDLManager().DeleteDepartmentClientContactDetails(department_id, deleteCondition);
        }


        public int DeleteClientPositionProfiletDetails(short clientID, int deleteCondition)
        {
            return new ClientDLManager().DeleteClientPositionProfiletDetails(clientID, deleteCondition);
        }
        public List<ClientContactInformation> GetClientDerpartmentContacts(int clientID)
        {
            return new ClientDLManager().GetClientDerpartmentContacts(clientID);
        }

        public void GetClientName(int? clientID, int? departmentID, int? contactID, out int clientNewId, out string clientName)
        {
           new ClientDLManager().GetClientName(clientID, departmentID, contactID, out clientNewId, out clientName);
        }

        /// <summary>
        /// Method that retrieves the list of clients for the given search
        /// parameters.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="clientName">
        /// A <see cref="string"/> that holds the client name.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ClientInformation"/>that holds the client information.
        /// </returns>
        public List<ClientInformation> GetClients(int tenantID, string clientName, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new ClientDLManager().GetClients(tenantID, clientName, pageNumber,
                pageSize, sortField, sordOrder, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of departments 
        /// info against its contact id
        /// </summary>
        /// <param name="contactID">
        /// A <see cref="int"/> that holds the contact id.
        /// </param>
        /// <returns>
        /// A list of <see cref="Department"/> that holds the department details.
        /// </returns>
        public List<Department> GetDepartmentsByContactID(int contactID)
        {
            return new ClientDLManager().GetDepartmentsByContactID(contactID);
        }

        /// <summary>
        /// Method that retreives the client details against the position profile id.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the positionProfile ID.
        /// </param>
        /// <returns>
        /// A <see cref="ClientInformation"/> that holds the client details.
        /// </returns>
        public ClientInformation GetClientInfoPositionProfileID(int positionProfileID)
        {
            return new ClientDLManager().GetClientInfoPositionProfileID(positionProfileID);
        }
    }
}
