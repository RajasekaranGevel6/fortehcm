﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdminBLManager.cs
// File that represents the business layer for the attributes management.
// This will talk to the data layer to perform the operations associated
// with attributes management technique.

#endregion

#region Directives                                                             

using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the attributes management.
    /// This includes functionalities for retrieving and updating settings 
    /// values for attributes such as complexity, test area, etc. This will
    /// talk to the data layer for performing these operations.
    /// </summary>
    public class AttributeBLManager
    {
        #region Public Method                                                  
        /// <summary>
        /// Method that retrieves the attributes based on the given attribute
        /// type ID.
        /// </summary>
        /// <param name="attributeTypeID">
        /// A <see cref="string"/> that holds the attribute type ID.
        /// </param>
        /// <param name="sortingOrder">
        /// A <see cref="string"/> that holds the sorting order. 'A' for 
        /// ascending and 'D' for descending.
        /// </param>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the attributes.
        /// </returns>
        public List<AttributeDetail> GetAttributesByType(string attributeTypeID,
            string sortingOrder)
        {
            return new AttributeDLManager().GetAttributesByType
                (attributeTypeID, sortingOrder);
        }

        /// <summary>
        /// Method that retrives the attributes based on the given attribute
        /// type ID.
        /// </summary>
        /// <param name="attributeTypeID">
        /// A <see cref="string"/> that holds the attribute type ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the attributes.
        /// </returns>
        public List<AttributeDetail> GetAttributesByType(string attributeTypeID)
        {
            return new AttributeDLManager().GetAttributesByType
                (attributeTypeID, "A");
        }

        /// <summary>
        /// Method that retrieves the list of test areas from the attributes.
        /// </summary>
        /// <param name="sortingOrder">
        /// A <see cref="string"/> that holds the sorting order. 'A' for 
        /// ascending and 'D' for descending.
        /// </param>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the test areas.
        /// </returns>
        public List<AttributeDetail> GetTestAreas(int tenantID, string sortingOrder)
        {
            return new AttributeDLManager().GetAttributes
                (tenantID, "TEST_AREA", sortingOrder);
        }

        /// <summary>
        /// Method that retrieves the list of test areas from the attributes.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the test areas.
        /// </returns>
        public List<AttributeDetail> GetTestAreas(int tenantID)
        {
            return new AttributeDLManager().GetAttributes
                (tenantID, "TEST_AREA", "A");
        }

        /// <summary>
        /// Method that retrieves the list of complexities from the attributes.
        /// </summary>
        /// <param name="sortingOrder">
        /// A <see cref="string"/> that holds the sorting order. 'A' for 
        /// ascending and 'D' for descending.
        /// </param>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the 
        /// complexities.
        /// </returns>
        public List<AttributeDetail> GetComplexities(string sortingOrder)
        {
            return new AttributeDLManager().GetAttributesByType
                ("COMP", sortingOrder);
        }

        /// <summary>
        /// Method that retrieves the list of complexities from the attributes.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the 
        /// complexities.
        /// </returns>
        public List<AttributeDetail> GetComplexities()
        {
            return new AttributeDLManager().GetAttributesByType
                ("COMP", "A");
        }

        /// <summary>
        /// Method that retrieves the list of attribute types.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the attribute 
        /// types.
        /// </returns>
        public List<AttributeDetail> GetAttributeTypes()
        {
            return new AttributeDLManager().GetAttributeTypes();
        }

        /// <summary>
        /// Method that checks if the given attribute already exist.
        /// </summary>
        /// <param name="attributeName">
        /// A <see cref="string"/> that holds the attribute ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the ATTRIBUTE table.
        /// </remarks>
        public bool IsAttributeExist(int tenantID, string attributeName)
        {
            return new AttributeDLManager().IsAttributeExist(tenantID, attributeName);
        }

        /// <summary>
        /// Method that checks if the given test area attribute ID used in 
        /// questions.
        /// </summary>
        /// <param name="testAreaID">
        /// A <see cref="string"/> that holds the test area attribute ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the QUESTION table.
        /// </remarks>
        public bool IsTestAreaUsed(string testAreaID)
        {
            return new AttributeDLManager().IsTestAreaUsed(testAreaID);
        }

        /// <summary>
        /// Method that retrieves the list of Reminder attributes.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the 
        /// reminder attributes..
        /// </returns>
        public List<AttributeDetail> GetReminderAttributes()
        {
            return new AttributeDLManager().GetReminderAttributes();
        }

        /// <summary>
        /// Method that retrieves the list of Credit Type attributes.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the 
        /// Credit attributes..
        /// </returns>
        public List<AttributeDetail> GetCreditTypeAttributes()
        {
            return new AttributeDLManager().GetCreditTypeAttributes();
        }

        /// <summary>
        /// Method that retrieves the credit type from the database
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the 
        /// Credit attributes..
        /// </returns>
        public List<AttributeDetail> GetAttributeCreditType()
        {
            return new AttributeDLManager().GetAttributeCreditType();
        }
        #endregion 
    }
}
