﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Forte.HCM.DL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using System.Data;

namespace Forte.HCM.BL
{
    public class OfflineInterviewBLManager
    { 
        
        public bool UpdateInterviewConductionStatus(OfflineInterviewParams interviewParams)
        { 
            return new OfflineInterviewDLManager().UpdateInterviewConductionStatus(interviewParams);
        }

        public DataTable GetQuestions(OfflineInterviewParams interviewParams)
        {
            return new OfflineInterviewDLManager().GetQuestions(interviewParams);
        }

        public bool UpdateInterviewTestResultFact(OfflineInterviewParams interviewParams)
        {
            return new OfflineInterviewDLManager().UpdateInterviewTestResultFact(interviewParams);
        }

        public bool UpdateInterviewTest(OfflineInterviewParams interviewParams)
        {
            return new OfflineInterviewDLManager().UpdateInterviewTest(interviewParams);
        } 

        public DataTable GetDisclaimerMessages()
        {
            return new OfflineInterviewDLManager().GetDisclaimerMessages();
        }

        public CandidateInterviewSessionDetail GetCandidateInterviewSessionDetail(string candidateTestSessionID, int attemptID)
        {
            return new OfflineInterviewDLManager().GetCandidateInterviewSessionDetail(candidateTestSessionID, attemptID);
        }

        public DataTable GetInterviewDetails(OfflineInterviewParams interviewParams)
        {
            return new OfflineInterviewDLManager().GetInterviewDetails(interviewParams);
        }
    }
}
