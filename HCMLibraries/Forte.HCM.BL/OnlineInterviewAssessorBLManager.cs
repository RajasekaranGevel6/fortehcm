﻿using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.Support; 

namespace Forte.HCM.BL
{
    public class OnlineInterviewAssessorBLManager
    {
        public void InsertOnlineInterviewAssessor(AssessorTimeSlotDetail assessorTimeSlots )
        {
            new OnlineInterviewAssessorDLManager().
                InsertOnlineInterviewAssessor(assessorTimeSlots);  
        }

        public int InsertAssessorAvailabilityDates(AssessorTimeSlotDetail assessorAvailDates)
        {
            return new OnlineInterviewAssessorDLManager().
               InsertAssessorAvailabilityDates(assessorAvailDates,null);  
        }

        public void InsertOnlineInterviewAssessorSkill(AssessorTimeSlotDetail assessorSkill)
        {
            new OnlineInterviewAssessorDLManager().
               InsertOnlineInterviewAssessorSkill(assessorSkill);  
        }

        public void DeleteOnlineInterviewAssessor(int assessorId, string interviewKey)
        {
            new OnlineInterviewAssessorDLManager().
                DeleteOnlineInterviewAssessor(assessorId, interviewKey);
        }

         public List<AssessorTimeSlotDetail> GetOnlineInterviewAssessorList(string interviewKey,
            int pageNo, int pageSize,out int totalRecords)
         {
           return new OnlineInterviewAssessorDLManager().
               GetOnlineInterviewAssessorList(interviewKey, pageNo, pageSize, out totalRecords);    
        }

         public List<AssessorTimeSlotDetail> GetAssessorAvailableDates(int assessorId,int monthId,
             int pageNo, int pageSize, out int totalRecords)
         {
             return new OnlineInterviewAssessorDLManager().
               GetAssessorAvailableDates(assessorId, monthId,pageNo, pageSize, out totalRecords);    
         }

         public List<AssessorTimeSlotDetail> GetOnlineAssessorTimeSlotDates(int assessorId, int monthId,
         int pageNo, int pageSize, out int totalRecords)
         {
             return new OnlineInterviewAssessorDLManager().
              GetOnlineAssessorTimeSlotDates(assessorId, monthId, pageNo, pageSize, out totalRecords);
         }

        /// <summary>
        /// This functions returns assessor time slots with status for particular date
        /// </summary>
        /// <param name="genId">
        /// A <see cref="System.Int32"/> that holds the request date auto generated id
        /// </param>
        /// <returns>
        /// A<see cref="Object"/> that holds the time details
        /// </returns>
         public List<AssessorTimeSlotDetail> GetAssessorTimeSlots(int genId)
         {
             return new OnlineInterviewAssessorDLManager().GetAssessorTimeSlots(genId);
         }

         public List<int> GetOnlineInterviewAssessorSkill(int assessorGenId)
         {
             return new OnlineInterviewAssessorDLManager().
               GetOnlineInterviewAssessorSkill(assessorGenId);    
         }

         public List<InterviewDetail> GetAssessorInterviewDetail(int assessorId, DateTime requestDate)
         {
             return new OnlineInterviewAssessorDLManager().
              GetAssessorInterviewDetail(assessorId, requestDate); 
         }

        
 
         public List<OnlineCandidateSessionDetail> GetOnlineInterviews(InterviewSearchCriteria interviewSearch, out int totalRecords)
         {
             return new OnlineInterviewAssessorDLManager().GetOnlineInterviews(interviewSearch, out totalRecords);
         }

         /// <summary>
         ///  return rows depend on position
         ///  if you need 10th to 20th you need to pass start=10 and end=20
         /// </summary>
         /// <param name="start">database start position of one row</param>
         /// <param name="next">database end position of one row</param>
         /// <returns></returns>
         public string GetAjaxContent(InterviewSearchCriteria interviewSearch)
         {
             string result = string.Empty;
             
             int totalRecords=0;

             List<OnlineCandidateSessionDetail> sessionDetail = new OnlineInterviewAssessorDLManager().
                 GetOnlineInterviews(interviewSearch, out totalRecords);
             
             if (sessionDetail == null) return null;

            if (sessionDetail.Count > 0)
             {
                 foreach (OnlineCandidateSessionDetail candidateDetail in sessionDetail)
                 {
                     result += string.Format(@"<tr>
                        <td>
                             <table cellpadding=5 cellspacing=0 border=0 width='440px' align=left style='border-top: 1px solid #ccc'>
                                <tr>
                                    <td class='panel_body_bg' style='height: 16px'>{0}</td>
                                </tr>
                                <tr>
                                    <td valign=top class='panel_body_bg'>
                                        <table cellpadding=5 cellspacing=0 border=0 width='440px'>
                                            <tr>
                                                <td width='30%'>Scheduled By 
                                                </td>
                                                <td width='70%'>{1}</td>
                                            </tr>
                                            <tr>
                                                <td>Interview Date</td>
                                                <td>{2}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Interviewer(s) Name
                                                </td>
                                                <td valign='top' align='left'>
                                                    <div  id='OnlineInterviewSchduleCandidate_scheduledCandidateList_DataList_InterivewerName_Div'
                                                        style ='width:280px;'> 
                                                    </div> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Status
                                                </td>
                                                <td>{3}</td>
                                            </tr>
                                            <tr>
                                                <td colspan='2' align='right'>
                                                    <a href='' id='OnlineInterviewSchduleCandidate_scheduledCandidateList_ViewDetail_HyperLink'>View Details</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>", candidateDetail.FirstName, candidateDetail.ScheduledBy, candidateDetail.InterviewDate, candidateDetail.SessionStatus);
                 }
             } 
            /* if (dataTable.Rows.Count > 0)
             {
                 for (int i = 0; i < dataTable.Rows.Count; i++)
                 {
                     result += string.Format(@"<tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td style='width:50px;'>{0}</td><td style='width:400px;'>{1}</td><td style='width:150px;'>{2}</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                   </tr>", dataTable.Rows[i][0].ToString(), dataTable.Rows[i][1].ToString(), dataTable.Rows[i][2].ToString());
                 }

             }*/
             //this string is going to append on Datalist on client.
             return result;
         }

         

         public CandidateInterviewSessionDetail GetOnlineInterviewDetail(string interviewKey, string chatRoomId)
         {
             return new OnlineInterviewAssessorDLManager().GetOnlineInterviewDetail(interviewKey,chatRoomId);
         }

         public string GetCandidateUserName(int interviewId)
         {
             return new OnlineInterviewAssessorDLManager().GetCandidateUserName(interviewId);
         }
         public string GetUserEmailID(int userId)
         {
             return new OnlineInterviewAssessorDLManager().GetUserEmailID(userId);
         }
    }
}
