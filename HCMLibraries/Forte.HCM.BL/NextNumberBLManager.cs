﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// NextNumberBLManager.cs
// File that represents the business layer for the next number generator
// manager. This will talk to the data layer to perform the operations 
// associated with next number generation processes.

#endregion

#region Directives                                                             

using System.Data;

using Forte.HCM.DL;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the next number generator
    /// manager. This will talk to the data layer to perform the operations 
    /// associated with next number generation processes.
    /// </summary>
    public class NextNumberBLManager
    {
        #region Public Method                                                  
        /// <summary>
        /// Calls the DL method to generate the next numbers
        /// </summary>
        /// <param name="transaction">
        /// A<see cref="IDbTransaction"/>that holds the transaction
        /// </param>
        /// <param name="objectType">
        /// A<see cref="string"/>that holds the objectType
        /// </param>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the User id
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the Next Number.
        /// </returns>
        public string GetNextNumber(IDbTransaction transaction, string objectType, int userID)
        {
            return new NextNumberDLManager().GetNextNumber(transaction, objectType, userID);
        }
        #endregion Public Method
    }
}
