﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// DashBoardBLManager.cs
// File that represents the data layer for the DashBoard module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives                                                             
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the Dashboard management.
    /// This will talk to the data layer to perform the operations associated
    /// with credit management processes.
    /// </summary>
   public class DashBoardBLManager
   {
       #region Public Method                                                   
       /// <summary>
        /// Get the Dashboard ID.
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the User id
        /// </param>
        /// <param name="groupName">
        /// A<see cref="string"/>that holds the group Name
        /// </param>
        /// <returns>
        /// A<see cref="string"/>
        /// Get the Dashboard ID based on the userid.
        /// </returns>
       public DashBoard GetDashBoardId(string userID,string groupName)
       {
           return new DashBoardDLManager().GetDashBoardId(userID, groupName);
       }

       /// <summary>
       /// delete the widget instance based on the dashBoardID
       /// </summary>
       /// <param name="userID">
       /// A<see cref="string"/>that holds the dashBoardID
       /// </param>
       public void DeleteWidgetInstance(string userID)
       {
           new DashBoardDLManager().DeleteWidgetInstance(userID);
       }

       /// <summary>
       /// Get the Dashboard ID.
       /// </summary>
       /// <param name="groupName">
       /// A<see cref="string "/>that holds the group name
       /// </param>
       /// <returns>
       /// A List of<see cref="WidgetTypes"/>
       /// Get the Dashboard ID based on the userid.
       /// </returns>
       public List<WidgetTypes> GetWidgetTypes(string groupName)
       {
           return new DashBoardDLManager().GetWidgetTypes(groupName);
       }

       ///// <summary>
       ///// Get the HCM dashboard.
       ///// </summary>
       ///// <param name="userID">The user ID.</param>
       ///// <param name="totalCount">The total count.</param>
       ///// <returns>dashboard details </returns>
       public HCMDashboard GetHCMDashboard(int userID, int totalCount)
       {
           return new DashBoardDLManager().GetHCMDashboard(userID, totalCount);
       }

       #endregion Public Method
   }
}
