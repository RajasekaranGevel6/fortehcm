﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewQuestionBLManager.cs
// File that represents the business layer for the interview question 
// repository module. This includes functionalities for create, retrieve, 
// update, delete questions. This will talk to the data layer for performing
// these operations.

#endregion

#region Directives

using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the interview question 
    /// repository module. This includes functionalities for create, retrieve, 
    /// update, delete questions. This will talk to the data layer for 
    /// performing these operations.
    /// </summary>
    public class InterviewQuestionBLManager
    {
        /// <summary>
        /// Method that retrieves the list of test for the given question key. 
        /// criteria.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that holds the TestDetails regarding this question.
        /// </returns>
        public List<TestDetail> GetQuestionUsageSummary(string questionKey,
            int pageNumber, int pageSize, string orderBy, SortType orderByDirection,
            out int totalRecords)
        {
            return new InterviewQuestionDLManager().GetQuestionUsageSummary(questionKey,
                pageNumber, pageSize, orderBy, orderByDirection, out totalRecords);
        }

        /// <summary>
        /// Get question image by giving a question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        ///  A <see cref="QuestionDetail"/> that holds the question details. 
        /// </returns>
        public byte[] GetInterviewQuestionImage(string questionKey)
        {
            return new InterviewQuestionDLManager().GetInterviewQuestionImage(questionKey);
        }
    }
}
