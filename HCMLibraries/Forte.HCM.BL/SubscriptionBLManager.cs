﻿#region Directives

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the subscripiton management module.
    /// This includes functionalities for retrieving, updating, delete and add 
    /// values for subscriptions and features.
    /// This will talk to the database for performing these operations.
    /// </summary>
    public class SubscriptionBLManager
    {
        #region Constructor                                                    
        
        /// <summary>
        /// Constructor
        /// </summary>
        public SubscriptionBLManager()
        {
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// Method that picks all the subscription features based on the selected subscription id.
        /// </summary>
        /// <param name="SubscriptionId">
        /// A <see cref="System.Int32"/> that holds user selected subscription id
        /// </param>
        /// <param name="Total_Records">
        /// A <see cref="System.Int32"/> out parameter that holds the total number of records
        /// in the repository
        /// </param>
        /// <param name="PageSize">
        /// A <see cref="System.Int32"/> that holds the page size of the grid
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="System.Int32"/> that holds the page number of the grid
        /// </param>
        /// <returns></returns>
        public IList<SubscriptionFeatures> GetSelectedSubscriptionFeatures(int SubscriptionId, out int Total_Records, int? PageSize, int PageNumber)
        {
            return new SubscriptionDLManager().GetSelectedSubscriptionFeatures(SubscriptionId, out Total_Records, PageSize, PageNumber);
        }

        /// <summary>
        /// A Method that updates the feature value for a subscription
        /// </summary>
        /// <param name="subscriptionFeatures">
        /// A <see cref="Forte.HCM.DataObjects.SubscriptionFeatures"/> that holds the
        /// subscription features
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of records
        /// effected in the repository
        /// </returns>
        public int UpdateSubscriptionFeaturePricing(SubscriptionFeatures subscriptionFeatures)
        {
            return new SubscriptionDLManager().UpdateSubscriptionFeaturePricing(subscriptionFeatures);
        }

        /// <summary>
        /// A Method that updates the list of subscription features with transaction
        /// </summary>
        /// <param name="subscriptionFeatures">
        /// A <see cref="Forte.HCM.DataObjects.SubscriptionFeatures"/> that holds the
        /// subscription features
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of records
        /// effected in the repository
        /// </returns>
        public int UpdateSubscriptionFeaturePricingListWithTransaction(List<SubscriptionFeatures> subscriptionFeatures)
        {
            IDbTransaction transaction = null;
            int AffectedRectords = 0;
            try
            {
                transaction = new TransactionManager().Transaction;
                for (int Count = 0; Count < subscriptionFeatures.Count; Count++)
                    AffectedRectords += new SubscriptionDLManager().UpdateSubscriptionFeaturePricing(subscriptionFeatures[Count], transaction);
                transaction.Commit();
                return AffectedRectords;
            }
            catch
            {
                if (!Utility.IsNullOrEmpty(transaction))
                    transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Method that retrieves the list of features applicable for the given
        /// subscription type.
        /// </summary>
        /// <param name="subscriptionID">
        /// A <see cref="int"/> that holds the subscription type.
        /// </param>
        /// <returns>
        /// A list of <see cref="SubscriptionFeatures"/> that holds the subscription 
        /// features.
        /// </returns>
        public List<SubscriptionFeatures> GetSubscriptionFeatures(int subscriptionID)
        {
            return new SubscriptionDLManager().GetSubscriptionFeatures(subscriptionID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<SubscriptionFeatureTypes> GetAllFeatureTypes()
        {
            return new SubscriptionDLManager().GetAllFeatureTypes();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionFeatureTypes"></param>
        /// <returns></returns>
        public int InsertFeatureSubscriptionType(SubscriptionFeatureTypes subscriptionFeatureTypes)
        {
            return new SubscriptionDLManager().InsertFeatureSubscriptionType(subscriptionFeatureTypes);
        }

        /// <summary>
        /// A Method that updates the subscription feature type
        /// </summary>
        /// <param name="subscriptionFeatureTypes">
        /// A <see cref="Forte.HCM.DataObjects.SubscriptionFeatureTypes"/> object that contains
        /// the records to be updated to the DB
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of effected records in the DB
        /// </returns>
        public int UpdateFeatureSubscriptionType(SubscriptionFeatureTypes subscriptionFeatureTypes)
        {
            return new SubscriptionDLManager().UpdateFeatureSubscriptionType(subscriptionFeatureTypes);
        }

        /// <summary>
        /// A Method that updates the list of subscription feature types with transaction
        /// </summary>
        /// <param name="subscriptionFeatureTypes">
        /// A List of <see cref="Forte.HCM.DataObjects.SubscriptionFeatureTypes"/> that contains
        /// the records to be updated to the DB
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of effected records in the DB
        /// </returns>
        public int UpdateFeatureSubscriptionType(List<SubscriptionFeatureTypes> subscriptionFeatureTypes)
        {
            return new SubscriptionDLManager().UpdateFeatureSubscriptionType(subscriptionFeatureTypes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionFeatureTypes"></param>
        /// <returns></returns>
        public int DeleteFeatureSubscriptionType(SubscriptionFeatureTypes subscriptionFeatureTypes)
        {
            return new SubscriptionDLManager().DeleteFeatureSubscriptionType(subscriptionFeatureTypes);
        }

        /// <summary>
        /// Gets the subscription types with the features
        /// </summary>
        /// <returns></returns>
        public List<SubscriptionFeatures> GetSubscriptionWithFeatures(out int freeSubscriptionTrialDays, out int freeStandardSubscriptionTrialDays, out int freeCorporateSubscriptonTrialDays)
        {
            return new SubscriptionDLManager().GetSubscriptionWithFeatures(out freeSubscriptionTrialDays, out freeStandardSubscriptionTrialDays, out freeCorporateSubscriptonTrialDays);
        }

        /// <summary>
        /// Gets all the subscription Types
        /// </summary>
        /// <returns>
        /// A List of subscription types available in the repository
        /// </returns>
        public IList<SubscriptionTypes> GetAllSubscriptionTypes()
        {
            return new SubscriptionDLManager().GetAllSubscriptionTypes();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionType"></param>
        /// <returns></returns>
        public int InsertSubscriptionType(SubscriptionTypes subscriptionType)
        {
            return new SubscriptionDLManager().InsertSubscriptionType(subscriptionType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionType"></param>
        /// <returns></returns>
        public int UpdateSubscriptionTypes(SubscriptionTypes subscriptionType)
        {
            return new SubscriptionDLManager().UpdateSubscriptionTypes(subscriptionType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionType"></param>
        /// <returns></returns>
        public int DeleteSubscriptionTypes(SubscriptionTypes subscriptionType)
        {
            return new SubscriptionDLManager().DeleteSubscriptionTypes(subscriptionType);
        }

        /// <summary>
        /// Method that updates the publish status of the subscription
        /// </summary>
        /// <param name="subscriptionTypes"></param>
        /// <returns></returns>
        public int UpdateSubscriptionPublishStatus(SubscriptionTypes subscriptionTypes)
        {
            return new SubscriptionDLManager().UpdateSubscriptionPublishStatus(subscriptionTypes);
        }


        /// <summary>
        /// Method that retrieves the list of users that are expired or about
        /// to expire in the given period of time.
        /// </summary>
        /// <param name="days">
        /// A <see cref="int"/> that holds the number of days about to expire.
        /// </param>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the date on which the relative
        /// expiry day is calculated.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        public List<UserDetail> GetExpiryUsers(int days, DateTime date)
        {
            return new SubscriptionDLManager().GetExpiryUsers(days, date);
        }

        /// <summary>
        /// Method that updates the user to the given subscription.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="subcriptionID">
        /// A <see cref="int"/> that holds the subscription ID.
        /// </param>
        /// <param name="subscriptionCode">
        /// A <see cref="string"/> that holds the suscription code.
        /// </param>
        public void UpdateUserSubscription(int userID, int subcriptionID, string subscriptionCode)
        {
            new SubscriptionDLManager().UpdateUserSubscription
                (userID, subcriptionID, subscriptionCode);
        }

        #endregion Public Methods
    }
}
