﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdminBLManager.cs
// File that represents the business layer for the admin module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>
    /// Class that represents the business layer for the admin module.
    /// This includes functionalities for retrieving and updating settings
    /// values for question, competency vector, credit management, options
    /// etc. This will talk to the data layer for performing these operations.
    /// </summary>
    public class AdminBLManager
    {
        #region Public Methods

        /// <summary>
        /// Method that updates the admin settings.
        /// </summary>
        /// <param name="adminSettings">
        /// A <see cref="AdminSettings"/> that holds the admin settings.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the user id.
        /// </param>
        public void UpdateAdminSettings(AdminSettings adminSettings, int user)
        {
            new AdminDLManager().UpdateAdminSettings(adminSettings, user);
        }

        /// <summary>
        /// Method that updates the candidate admin settings.
        /// </summary>
        /// <param name="adminSettings">
        /// A <see cref="CandidateAdminSettings"/> that holds the candidate 
        /// admin settings.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the user.
        /// </param>
        public void UpdateCandidateAdminSettings(CandidateAdminSettings adminSettings, int user)
        {
            new AdminDLManager().UpdateCandidateAdminSettings(adminSettings, user);
        }

        /// <summary>
        /// Method that retrieves the admin settings.
        /// </summary>
        /// <returns>
        /// A <see cref="AdminSettings"/> that holds the admin settings.
        /// </returns>
        public AdminSettings GetAdminSettings()
        {
            return new AdminDLManager().GetAdminSettings();
        }

        /// <summary>
        /// Method that retrieves the candidate admin settings.
        /// </summary>
        /// <returns>
        /// A <see cref="CandidateAdminSettings"/> that holds the candidate 
        /// admin settings.
        /// </returns>
        public CandidateAdminSettings GetCandidateAdminSettings()
        {
            return new AdminDLManager().GetCandidateAdminSettings();
        }

        /// <summary>
        /// Method that retrieves the list of categories.
        /// </summary>
        /// <returns>
        /// A list of <see cref="Category"/> object that holds the categories.
        /// </returns>
        public List<Category> GetCategories(int tenantID)
        {
            return new AdminDLManager().GetCategories(tenantID);
        }

        /// <summary>
        /// Method that insert a new category into the database.
        /// </summary>
        /// <param name="category">
        /// A <see cref="Category"/> object that holds the category details.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> object that holds the user ID.
        /// </param>
        public void InsertCategory(Category category, int userID, int tenantID)
        {
            new AdminDLManager().InsertCategory(category, userID, tenantID);
        }

        /// <summary>
        /// Method that will insert the certificate details.
        /// </summary>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationDetail"/> that contains the certification detail instance.
        /// </param>
        /// <param name="userid">
        /// An <see cref="int"/> that contains the userid.
        /// </param>
        public void InsertCertificate(CertificationDetail certificationDetail, int userid)
        {
            new AdminDLManager().InsertCertificate(certificationDetail, userid);
        }

        /// <summary>
        /// Method that will update the certificate details.
        /// </summary>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationDetail"/> that contains the certification detail instance.
        /// </param>
        /// <param name="userid">
        /// An <see cref="int"/> that contains the userid.
        /// </param>
        /// <param name="certificateID">
        /// An <see cref="int"/> that contains the certificate id.
        /// </param>
        public void UpdateCertificate(CertificationDetail certificationDetail, int userid, int certificateID)
        {
            new AdminDLManager().UpdateCertificate(certificationDetail,
                    userid, certificateID);
        }

        /// <summary>
        /// Method that will return the certitication details.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CertificationDetail"/> that contains the certification details.
        /// </returns>
        public List<CertificationDetail> GetCertificateDetails()
        {
            return new AdminDLManager().GetCertificateDetails();
        }

        /// <summary>
        /// Method that will return the certitication details based on the certificate id
        /// </summary>
        /// <param name="certificateID"></param>
        /// <returns>
        /// A list of <see cref="CertificationDetail"/> that contains the certification details.
        /// </returns>
        public CertificationDetail GetCertificateDetails(int certificateID)
        {
            return new AdminDLManager().GetCertificateDetails(certificateID);
        }

        /// <summary>
        /// Method that returns the test certification details instance.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="CertificationDetail"/> that contains the certification details.
        /// </returns>
        public CertificationDetail GetTestCertificationDetails(string testKey)
        {
            return new AdminDLManager().GetTestCertificationDetails(testKey);
        }

         /// <summary>
        /// Method that returns the certification details for the given 
        /// candidate session ID.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CertificationDetail"/> that holds the certification details.
        /// </returns>
        public CertificationDetail GetCertificateByCandidateSession
            (string candidateSessionID, int attemptID)
        {
            return new AdminDLManager().GetCertificateByCandidateSession
                (candidateSessionID, attemptID);
        }

        /// <summary>
        /// Method that returns the certificate image as bytes.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <returns>
        /// Certificate Details in Byte[]
        /// </returns>
        public byte[] GetCertificateImage(string candidateSessionKey, int attemptID)
        {
            return new AdminDLManager().GetCertificateImage(candidateSessionKey, attemptID);
        }

        /// <summary>
        /// Method that updates a category into the database.
        /// </summary>
        /// <param name="category">
        /// A <see cref="Category"/> object that holds the category details.
        /// </param>
        public void UpdateCategory(Category category)
        {
            new AdminDLManager().UpdateCategory(category);
        }

        /// <summary>
        /// Method that deletes a category from the database.
        /// </summary>
        /// <param name="categoryID">
        /// An <see cref="int"/> that holds the category ID to be deleted.
        /// </param>
        public void DeleteCategory(int categoryID)
        {
            new AdminDLManager().DeleteCategory(categoryID);
        }

        /// <summary>
        /// Method that retrieves the list of subject for the given 
        /// category ID.
        /// </summary>
        /// <returns>
        /// A list of <see cref="Subject"/> object that holds the subject.
        /// </returns>
        public List<Subject> GetSubjects(int categoryID)
        {
            return new AdminDLManager().GetSubjects(categoryID);
        }

        /// <summary>
        /// Method that insert a new subject into the database.
        /// </summary>
        /// <param name="subject">
        /// A <see cref="Subject"/> object that holds the subject details.
        /// </param>
        public void InsertSubject(Subject subject, int tenantID)
        {
            new AdminDLManager().InsertSubject(subject, tenantID);
        }

        /// <summary>
        /// Method that checks if the given subject ID used in questions or
        /// contributor summary.
        /// </summary>
        /// <param name="subjectID">
        /// A <see cref="string"/> that holds the subject ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the QUESTION_RELATION and 
        /// CONTRIBUTOR_CATEGORY_SUBJECT tables.
        /// </remarks>
        public bool IsSubjectUsed(int subjectID)
        {
            return new AdminDLManager().IsSubjectUsed(subjectID);
        }

        /// <summary>
        /// Method that deletes a subject from the database.
        /// </summary>
        /// <param name="subjectID">
        /// An <see cref="int"/> that holds the subject ID to be deleted.
        /// </param>
        public void DeleteSubject(int subjectID)
        {
            new AdminDLManager().DeleteSubject(subjectID);
        }

        /// <summary>
        /// Method that checks whether the subjects already exists
        /// </summary>
        /// <param name="categoryID">
        /// A<see cref="int"/>that holds the category id
        /// </param>
        /// <param name="subjectID">
        /// A<see cref="int"/>that holds the subject id
        /// </param>
        /// <param name="subjectName">
        /// A<see cref="string"/>that holds the subject Name
        /// </param>
        /// <returns>
        /// A<see cref="int"/>that represents whether 
        /// the subject exists or not
        /// </returns>
        public int CheckSubject(int categoryID, int subjectID, string subjectName)
        {
            //Call the method in DL Manager
            return new AdminDLManager().CheckSubject(categoryID, subjectID, subjectName);
        }

        /// <summary>
        /// Insert the test area details.
        /// </summary>        
        /// <param name="testAreaName">
        /// A <see cref="string"/> object that holds the testArea Name.
        /// </param>
        /// <param name="userId">
        /// A <see cref="int"/> object that holds the User Id.
        /// </param>
        public void InsertTestArea(string testAreaName, int tenantID, int userId)
        {
            //Create new transaction for the method
            IDbTransaction transaction = new TransactionManager().Transaction;

            //Get the test area auto generated ID
            string testAreaId = new NextNumberDLManager().GetNextNumber(transaction, "TA", userId);

            //Call the method in Dl Manager
            new AdminDLManager().InsertTestArea(testAreaId, testAreaName, tenantID, userId, transaction);
        }

        /// <summary>
        /// Delete the test area based on the test area id.
        /// </summary>
        /// <param name="testAreaID">
        /// A <see cref="string"/> object that holds the testArea Id.
        /// </param>
        public void DeleteTestArea(string testAreaID)
        {
            //Call the method in Dl Manager
            new AdminDLManager().DeleteTestArea(testAreaID);
        }

        /// <summary>
        /// Method that retrieves the list of competency vector group.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AdminCompetencyVectors"/> that holds the 
        /// Vector group attributes..
        /// </returns>
        public List<CompetencyVectorGroup> GetCompetencyVectorGroup()
        {
            return new AdminDLManager().GetCompetencyVectorGroup();
        }

        /// <summary>
        /// Method that retrieves the list of competency vector name based on group id.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AdminCompetencyVectors"/> that holds the 
        /// Vector attributes..
        /// </returns>
        public List<CompetencyVector> GetCompetencyVector(int groupID)
        {
            return new AdminDLManager().GetCompetencyVector(groupID);
        }

        /// <summary>
        /// Method that retrieves the list of competency vector Parameters.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AdminCompetencyVectors"/> that holds the 
        /// vector paramaters.
        /// </returns>
        public List<CompetencyVectorParameter> GetCompetencyVectorParameter()
        {
            return new AdminDLManager().GetCompetencyVectorParameter();
        }

        /// <summary>
        /// This method Insert Competency vector group details.
        /// </summary>
        /// <param name="adminCompetencyVectorsDetail">
        /// A <see cref="CompetencyVectorGroup"/> that holds the admin Competency Vectors Detail
        /// </param>        
        public void InsertCompetencyVectorGroup(CompetencyVectorGroup adminCompetencyVectorsDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // Insert Reminder detail.
                new AdminDLManager().InsertCompetencyVectorGroup(adminCompetencyVectorsDetail, transaction);
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// This method Insert Competency vector details.
        /// </summary>
        /// <param name="adminCompetencyVectorsDetail">
        /// A list of <see cref="CompetencyVector"/> that holds the admin Competency Vectors Detail
        /// </param>        
        public void InsertCompetencyVector(CompetencyVector adminCompetencyVectorsDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // Insert Reminder detail.
                new AdminDLManager().InsertCompetencyVector(adminCompetencyVectorsDetail, transaction);
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// This method Insert Competency vector parameters details.
        /// </summary>
        /// <param name="adminCompetencyVectorsDetail">
        /// A list of <see cref="CompetencyVectorParameter"/> that holds the admin Competency Vectors Detail
        /// </param>        
        public void InsertCompetencyVectorParameter(CompetencyVectorParameter adminCompetencyVectorsDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // Insert Reminder detail.
                new AdminDLManager().InsertCompetencyVectorParameter(adminCompetencyVectorsDetail, transaction);
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that deletes a Competency vector group from the database.
        /// </summary>
        /// <param name="groupID">
        /// An <see cref="int"/> that holds the Group id to be deleted.
        /// </param>
        public void DeleteCompetencyVectorGroup(int groupID)
        {
            new AdminDLManager().DeleteCompetencyVectorGroup(groupID);
        }

        /// <summary>
        /// Method that deletes a Competency vectors from the database.
        /// </summary>
        /// <param name="vectorID">
        /// An <see cref="int"/> that holds the vector id to be deleted.
        /// </param>
        public void DeleteCompetencyVector(string vectorID)
        {
            new AdminDLManager().DeleteCompetencyVector(vectorID);
        }

        /// <summary>
        /// Method that deletes a Competency parameters from the database.
        /// </summary>
        /// <param name="parameterID">
        /// An <see cref="int"/> that holds the vector parameter id to be deleted.
        /// </param>
        public void DeleteCompetencyVectorParameter(string parameterID)
        {
            new AdminDLManager().DeleteCompetencyVectorParameter(parameterID);
        }

        /// <summary>
        /// Method that retrieves the list of Competency Group name,vectors and parameters for skill matrix.
        /// </summary>
        /// <param name="candidateid">
        /// An <see cref="string"/> that holds the specific candidate id.
        /// </param>
        /// <returns>
        /// A set of <seealso cref="DataSet"/> that holds the 
        /// Competency Group name,vectors and parameters for skill matrix.
        /// </returns>
        public DataSet GetCompetencyVectorSkillMatrix(string candidateid)
        {
            return new AdminDLManager().GetCompetencyVectorSkillMatrix(candidateid);
        }

        /// <summary>
        /// Method that retrieves the list of Competency Group name,vectors and parameters for skill matrix for interview.
        /// </summary>
        /// <param name="candidateID">
        /// An <see cref="int"/> that holds the specific candidate id.
        /// </param>
        /// <returns>
        /// A set of <seealso cref="DataSet"/> that holds the 
        /// Competency Group name,vectors and parameters for skill matrix for interview
        /// </returns>
        public DataSet GetCandidateSkillSubstance(int candidateID,int resumeID)
        {
            return new AdminDLManager().GetCandidateSkillSubstance(candidateID, resumeID);
        }

        public DataSet GetTemporaryResumeSkills(int candidateID)
        {
            return new AdminDLManager().GetTemporaryResumeSkills(candidateID);
        }

        /// <summary>
        /// Method that retrieves the list of Competency Group name,vectors and parameters for skill matrix for interview.
        /// </summary>
        /// <param name="candidateid">
        /// An <see cref="string"/> that holds the specific candidate id.
        /// </param>
        /// <returns>
        /// A set of <seealso cref="DataSet"/> that holds the 
        /// Competency Group name,vectors and parameters for skill matrix for interview
        /// </returns>
        public DataSet GetInterviewCompetencyVectorSkillMatrix(string candidateid)
        {
            return new AdminDLManager().GetInterviewCompetencyVectorSkillMatrix(candidateid);
        }

        /// <summary>
        /// Method that checks if the given VEctor ID used in 
        /// skill matrix.
        /// </summary>
        /// <param name="vectorID">
        /// A <see cref="string"/> that holds the Vector ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the Candidate competency vector table.
        /// </remarks>
        public bool IsVectorIDUsed(string vectorID)
        {
            return new AdminDLManager().IsVectorIDUsed(vectorID);
        }

        /// <summary>
        /// Method that checks if the given parameter ID used in 
        /// skill matrix.
        /// </summary>
        /// <param name="vectorparameterID">
        /// A <see cref="string"/> that holds the Parameter ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the Candidate competency vector table.
        /// </remarks>
        public bool IsParameterIDUsed(string vectorparameterID)
        {
            return new AdminDLManager().IsParameterIDUsed(vectorparameterID);
        }

        /// <summary>
        /// Method that updates proximity factor values.
        /// </summary>
        /// <param name="proximityValue">
        /// A <see cref="decimal"/> object that holds the proximity value.
        /// </param>
        ///  /// <param name="attributeID">
        /// A <see cref="string"/> object that holds the attribute ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> object that holds the user ID.
        /// </param>
        public void UpdateProximityFactor(decimal proximityValue, string attributeID, int userID)
        {
            new AdminDLManager().UpdateProximityFactor(proximityValue, attributeID, userID);
        }
        #endregion Public Methods

        /// <summary>
        /// Method that retrieves the available segments.
        /// </summary>
        /// <returns>
        /// A list of <see cref="SegmentDetail"/> that holds the segment details.
        /// </returns>
        public List<Segment> GetAvailableSegments()
        {
            return new AdminDLManager().GetAvailableSegments();
        }

        /// <summary>
        /// Method that retrieves the available active segments.
        /// </summary>
        /// <returns>
        /// A list of <see cref="SegmentDetail"/> that holds the segment details.
        /// </returns>
        public List<Segment> GetAvailableActiveSegments()
        {
            return new AdminDLManager().GetAvailableActiveSegments();
        }

        /// <summary>
        /// Represents the method to get the segment path for 
        /// the corresponding segment ID
        /// </summary>
        /// <param name="segmentID">
        /// A<see cref="int"/>that holds the segment ID
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the segment path 
        /// </returns>
        public string GetSegmentPath(string segmentID)
        {
            return new AdminDLManager().GetSegmentPath(segmentID);
        }



        /// <summary>
        /// Inserts the new form.
        /// </summary>
        /// <param name="formDetail">The form detail.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int InsertNewForm(FormDetail formDetail, int tenantID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {

                // Check if limit exceeds.
                bool isLimitExceeds = IsFeatureUsageLimitExceeds
                   (tenantID, Constants.FeatureConstants.STANDARD_FORMS_FOR_POSITION_PROFILE_GENERATION);

                if (isLimitExceeds)
                {
                    return -1;
                }

                // Check whether the form with name already exist
                int formIsExist = new AdminDLManager().CheckForm(formDetail, tenantID);

                if (formIsExist != 0)
                {
                    return 0;
                }

                // insert new form
                List<FormDetail> PredefinedForms = null;
                int formID = new AdminDLManager().InsertNewForm(formDetail, tenantID, transaction, out PredefinedForms);
                short InsertPredefinedSegment = 0;
                if (Utility.IsNullOrEmpty(PredefinedForms))
                    InsertPredefinedSegment = 0;
                else
                    InsertPredefinedSegment = 1;
                foreach (Segment segment in formDetail.segmentList)
                {
                    segment.CreatedBy = formDetail.CreatedBy;
                    new AdminDLManager().InsertFormSegments(segment, transaction, formID);
                }
                if (InsertPredefinedSegment == 1)
                {
                    for (int i = 0; i < PredefinedForms.Count; i++)
                    {
                        foreach (Segment segment in formDetail.segmentList)
                        {
                            segment.CreatedBy = formDetail.CreatedBy;
                            new AdminDLManager().InsertFormSegments(segment, transaction, PredefinedForms[i].FormID);
                        }
                    }
                }

                // Commit the transaction.
                transaction.Commit();

                return formID;
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that checks if the subscription feature usage limit exceeds.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="featureID">
        /// A <see cref="int"/> that holds the feature ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. If limit exceeds return
        /// true else returns false.
        /// </returns>
        public bool IsFeatureUsageLimitExceeds(int tenantID, int featureID)
        {
            return (new AdminDLManager().GetCheckFeatureUsage(tenantID, featureID) > 0);
        }

        /// <summary>
        /// Inserts the segment to form.
        /// </summary>
        /// <param name="segmentIdList">The segment id list.</param>
        /// <param name="formID">The form ID.</param>
        public void InsertSegmentToForm1(List<int> segmentIdList, int formID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                // insert new form

                foreach (int ID in segmentIdList)
                {
                    Segment segment = new Segment();
                    segment.SegmentID = ID;
                    segment.DisplayOrder = 0;
                    segment.CreatedBy = 1;
                    new AdminDLManager().InsertFormSegments(segment, transaction, formID);
                }

                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Inserts the segment to form.
        /// </summary>
        /// <param name="segmentList">The segment list.</param>
        /// <param name="formID">The form ID.</param>
        public void InsertSegmentToForm(List<Segment> segmentList, int formID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // insert new form

                foreach (Segment segment in segmentList)
                {
                    new AdminDLManager().InsertFormSegments(segment, transaction, formID);
                }

                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }



        /// <summary>
        /// Method that searches and retrieves the position profile forms.
        /// </summary>
        /// <param name="formDetail">
        /// A <see cref="FormDetail"/> that holds the form search parameters.
        /// </param>
        /// <param name="orderDirection">
        /// A <see cref="string"/> that holds the order direction.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that holds the sort expression.
        /// </param>
        /// <param name="pageNumer">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="gridPageSize">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="FormDetail"/> that holds the searched forms.
        /// </returns>
        public List<FormDetail> GetPositionProfileForms(FormDetail formDetail, 
            string sortOrder, string sortExpression, 
            int pageNumer, int gridPageSize, out int totalRecords)
        {
            return new AdminDLManager().GetPositionProfileForms(formDetail, sortOrder,
                sortExpression, pageNumer, gridPageSize, out totalRecords);
        }

        /// <summary>
        /// Deletes the form.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        public void DeleteForm(int formID)
        {
            new AdminDLManager().DeleteForm(formID);
        }

        /// <summary>
        /// Gets the existing segments.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <returns></returns>
        public FormDetail GetExistingSegments(int formID)
        {
            return new AdminDLManager().GetExistingSegments(formID);

        }



        /// <summary>
        /// Updates the form detail.
        /// </summary>
        /// <param name="formDetail">The form detail.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int UpdateFormDetail(FormDetail formDetail, int tenantID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                int formID = new AdminDLManager().CheckFormNameAlreadyExist(formDetail, tenantID);

                if (formID != 0)
                {
                    return 0;
                }

                //Update thr form details in the form table 
                new AdminDLManager().UpdateFormDetail(formDetail, transaction);

                //Deletes the segments in the form_segments table

                new AdminDLManager().DeleteSegmentForForm(formDetail.FormID, transaction);

                //Insert the segments in to the form_segmets table
                foreach (Segment segment in formDetail.segmentList)
                {
                    segment.CreatedBy = formDetail.CreatedBy;
                    new AdminDLManager().InsertFormSegments(segment, transaction, formDetail.FormID);
                }

                transaction.Commit();

                return 1;

            }

            catch (Exception exception)
            {
                transaction.Rollback();
                throw exception;
            }

        }

        /// <summary>
        /// Gets the business types.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="totalPage">The total page.</param>
        /// <returns></returns>
        public List<BusinessTypeDetail> GetBusinessTypes(string businessName, string sortExpression,
            string sortOrder, int pageSize, int? pageNumber, out int totalPage)
        {
            return new AdminDLManager().GetBusinessTypes(businessName, sortExpression, sortOrder, pageSize, pageNumber, out totalPage);
        }

        /// <summary>
        /// Deletes the type of the business.
        /// </summary>
        /// <param name="businessID">The business ID.</param>
        public void DeleteBusinessType(int businessID)
        {
            new AdminDLManager().DeleteBusinessType(businessID);
        }


        /// <summary>
        /// Updates the name of the business type.
        /// </summary>
        /// <param name="businessTypeName">Name of the business type.</param>
        /// <param name="businessID">The business ID.</param>
        public void UpdateBusinessTypeName(string businessTypeName, int businessID)
        {
            new AdminDLManager().UpdateBusinessTypeName(businessTypeName, businessID);
        }

        /// <summary>
        /// Inserts the new name of the business type.
        /// </summary>
        /// <param name="businessTypeName">Name of the business type.</param>
        /// <param name="userID">The user ID.</param>
        public void InsertNewBusinessTypeName(string businessTypeName, int userID)
        {
            new AdminDLManager().InsertNewBusinessTypeName(businessTypeName, userID);
        }

        /// <summary>
        /// Updates the subscription options.
        /// </summary>
        /// <param name="enrollmentDetail">The enrollment detail.</param>
        public void UpdateSubscriptionOptions(CustomerEnrollmentDetail enrollmentDetail)
        {
            new AdminDLManager().UpdateSubscriptionOptions(enrollmentDetail);

            new AdminDLManager().UpdateCorporateRoles(enrollmentDetail);
        }

        /// <summary>
        /// Gets the default subscrption details.
        /// </summary>
        /// <returns></returns>
        public CustomerEnrollmentDetail GetDefaultSubscrptionDetails()
        {
            return new AdminDLManager().GetDefaultSubscrptionDetails();
        }
        /// <summary>
        /// Gets the subscription details.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalPage">The total page.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns></returns>
        public List<UserRegistrationInfo> GetSubscriptionDetails(UserRegistrationInfo user,
            int pageNumber, int pageSize, out int totalPage, string sortExpression, string sortOrder)
        {
            return new AdminDLManager().GetSubscriptionDetails(user, pageNumber, pageSize, out totalPage, sortExpression, sortOrder);
        }

        /// <summary>
        /// Gets the default form.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public int GetDefaultForm(int userID)
        {
            return new AdminDLManager().GetDefaultForm(userID);
        }

        /// <summary>
        /// Updates the default form.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdateDefaultForm(int formID, int userID)
        {
            new AdminDLManager().UpdateDefaultForm(formID, userID);
        }

        /// <summary>
        /// Activates the users.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <param name="activateID">The activate ID.</param>
        /// <param name="modifiedBy">The modified by.</param>
        public void ActivateUsers(int userID, Int16 activateID, int modifiedBy)
        {
            new AdminDLManager().ActivateUsers(userID, activateID, modifiedBy);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public UsageSummary GetUserSubscriptionDetails(int userID)
        {
            return new AdminDLManager().GetUserSubscriptionDetails(userID);
        }


        /// <summary>
        /// Method the retrieves the feature usage details for the given user, month and year.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="month">
        /// A <see cref="string"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <returns>
        /// A list of <see cref="UsageSummary"/> that holds the usage details.
        /// </returns>
        public List<UsageSummary> GetFeatureUsageDetails(int userID, string month, int year)
        {
            return new AdminDLManager().GetFeatureUsageDetails(userID, month, year);
        }

        /// <summary>
        /// Gets the year
        /// </summary>
        /// <returns></returns>
        public List<UsageSummary> GetYear()
        {
            return new AdminDLManager().GetYear();
        }

        /// <summary>
        /// Gets the Position profile usage details
        /// </summary>
        /// <param name="sortOrder">holds sort order</param>
        /// <param name="sortExpression">holds sort expression
        /// </param>
        /// <param name="user">holds user id</param>
        /// <param name="mmmyyyy">holds month and year</param>
        /// <returns></returns>
        public List<UsageSummary> GetPositionProfileUsageDetails(string sortOrder, 
                string sortExpression, int user, int mmmyyyy)
        {
            return new AdminDLManager().GetPositionProfileUsageDetails(sortOrder, 
                sortExpression, user, mmmyyyy);
        }
        /// <summary>
        /// Gets the form usage details
        /// </summary>
        /// <param name="sortOrder">holds sort order</param>
        /// <param name="sortExpression">holds sort expression
        /// </param>
        /// <param name="user">holds user id</param>
        /// <param name="mmmyyyy">holds month and year</param>
        /// <returns></returns>
        public List<UsageSummary> GetFormUsageDetails(string sortOrder, 
            string sortExpression, int user, int mmmyyyy)
        {
            return new AdminDLManager().GetFormUsageDetails(sortOrder,
                sortExpression, user, mmmyyyy);
        }
        /// <summary>
        /// Gets the talent scout search usage details
        /// </summary>
        /// <param name="sortOrder">holds sort order</param>
        /// <param name="sortExpression">holds sort expression
        /// </param>
        /// <param name="user">holds user id</param>
        /// <param name="mmmyyyy">holds month and year</param>
        /// <returns></returns>
        public List<UsageSummary> GetTalentSearchUsageDetails(string sortOrder, 
            string sortExpression, int user, int mmmyyyy)
        {
            return new AdminDLManager().GetTalentSearchUsageDetails(sortOrder, 
                sortExpression, user, mmmyyyy);
        }
        /// <summary>
        /// Gets the view profile usage details
        /// </summary>
        /// <param name="sortOrder">holds sort order</param>
        /// <param name="sortExpression">holds sort expression
        /// </param>
        /// <param name="user">holds user id</param>
        /// <param name="mmmyyyy">holds month and year</param>
        /// <returns></returns>
        public List<UsageSummary> GetViewProfileImageUsageDetails(string sortOrder, 
            string sortExpression, int user, int mmmyyyy)
        {
            return new AdminDLManager().GetViewProfileImageUsageDetails(sortOrder,
                sortExpression, user, mmmyyyy);
        }

        /// <summary>
        /// Method that checks if the given feature is applicable for the
        /// tenant ID.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="featureID">
        /// A <see cref="int"/> that holds the feature ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. If applicable returns 
        /// true else returns false.
        /// </returns>
        public bool IsFeatureApplicable(int tenantID, int featureID)
        {
            return new AdminDLManager().IsFeatureApplicable(tenantID, featureID);
        }

        public int InsertInternalUser(UserRegistrationInfo userRegistrationInfo,
            RegistrationConfirmation registrationConfirmation,int tenantUserID,IDbTransaction transaction)
        {
            return new AdminDLManager().InsertInternalUser(userRegistrationInfo, registrationConfirmation,tenantUserID, transaction);
        }



        /// <summary>
        /// Gets the modules.
        /// </summary>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="totalPage">The total page.</param>
        /// <returns></returns>
        public List<ModuleManagementDetails> GetModules(string sortExpression, string sortOrder, 
            int pageSize, int pageNumber, out int totalPage)
        {
            return new AdminDLManager().
                GetModules(sortExpression, sortOrder,pageSize,pageNumber,out totalPage);
        }

        /// <summary>
        /// Updates the module.
        /// </summary>
        /// <param name="moduleCode">The module code.</param>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="moduleID">The module ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public int UpdateModule(string moduleCode, string moduleName, int moduleID, int userID)
        {
            return new AdminDLManager().UpdateModule(moduleCode, moduleName, moduleID, userID);
        }

        /// <summary>
        /// Deletes the module.
        /// </summary>
        /// <param name="moduleID">The module ID.</param>
        /// <returns></returns>
        public void DeleteModule(string moduleID)
        {
             new AdminDLManager().DeleteModule(moduleID);
        }

        /// <summary>
        /// Inserts the new module.
        /// </summary>
        /// <param name="moduleCode">The module code.</param>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="moduleCreatedBy">The module created by.</param>
        /// <returns></returns>
        public int InsertNewModule(string moduleCode, string moduleName, int moduleCreatedBy)
        {
            return new AdminDLManager().InsertNewModule(moduleCode, moduleName, moduleCreatedBy);
          
        }

        /// <summary>
        /// Updates the sub module details.
        /// </summary>
        /// <param name="submoduleDetails">The submodule details.</param>
        public void UpdateSubModuleDetails(List<ModuleManagementDetails> submoduleDetails)
        {
            new AdminDLManager().UpdateSubModuleDetails(submoduleDetails);
        }

        /// <summary>
        /// Gets the module.
        /// </summary>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="totalPage">The total page.</param>
        /// <returns></returns>
        public List<ModuleManagementDetails> GetModule(string sortExpression,
            string sortOrder, int pageSize,
            int pageNumber, out int totalPage)
        {
            
            return new AdminDLManager().GetModule(sortExpression, sortOrder,
               pageSize, pageNumber, out totalPage);
        }

        /// <summary>
        /// Gets the sub modules.
        /// </summary>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <returns></returns>
        public List<ModuleManagementDetails> GetSubModules(string sortExpression,
            string sortOrder, int pageNumber)
        { 
            return new AdminDLManager().GetSubModules(sortExpression, sortOrder,
               pageNumber);
        }

        /// <summary>
        /// Inserts the new sub module.
        /// </summary>
        /// <param name="subModuleCode">The sub module code.</param>
        /// <param name="moduleID">The module ID.</param>
        /// <param name="subModuleName">Name of the sub module.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public int InsertNewSubModule(string subModuleCode, int moduleID,
            string subModuleName, int userID)
        {        
            return new AdminDLManager().InsertNewModule(subModuleCode, moduleID,
            subModuleName, userID);
        }

        /// <summary>
        /// Deletes the sub module.
        /// </summary>
        /// <param name="modID">The mod ID.</param>
        public void DeleteSubModule(int modID)
        {
            new AdminDLManager().DeleteSubModule(modID);
        }

        /// <summary>
        /// Gets the features.
        /// </summary>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="totalPage">The total page.</param>
        /// <returns></returns>
        public List<FeatureManagementDetails> GetFeatures(string sortExpression, 
            string sortOrder, int pageSize, int pageNumber, out int totalPage)
        {
            
            return new AdminDLManager().
              GetFeatures(sortExpression, sortOrder, pageSize, pageNumber, out totalPage);
        }

        /// <summary>
        /// Updates the feature.
        /// </summary>
        /// <param name="moduleID">The module ID.</param>
        /// <param name="subModuleID">The sub module ID.</param>
        /// <param name="featureName">Name of the feature.</param>
        /// <param name="featureURL">The feature URL.</param>
        /// <param name="featureID">The feature ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public int UpdateFeature(string featureName, 
            string featureURL, int featureID, int userID)
        {
            return new AdminDLManager().UpdateFeature(
                featureName, featureURL, featureID, userID);
        }

        /// <summary>
        /// Inserts the new feature.
        /// </summary>
        /// <param name="moduleID">The module ID.</param>
        /// <param name="submoduleID">The submodule ID.</param>
        /// <param name="featureName">Name of the feature.</param>
        /// <param name="featureURL">The feature URL.</param>
        /// <param name="featueCreatedBy">The featue created by.</param>
        /// <returns></returns>
        public int InsertNewFeature(int moduleID, int submoduleID, string featureName, 
            string featureURL, int featueCreatedBy)
        {
            return new AdminDLManager().InsertNewFeature(moduleID, submoduleID, featureName, featureURL, featueCreatedBy);
        }

        /// <summary>
        /// Deletes the feature.
        /// </summary>
        /// <param name="featureID">The feature ID.</param>
        public void DeleteFeature(int featureID)
        {
            new AdminDLManager().DeleteFeature(featureID);
        }

        /// <summary>
        /// Deletes the module results.
        /// </summary>
        /// <param name="moduleID">The module ID.</param>
        /// <returns></returns>
        public string DeleteModuleResults(string moduleID)
        {
            return new AdminDLManager().DeleteModuleResults(moduleID);
        }

        /// <summary>
        /// Gets the view cloning usage details.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="user">The user.</param>
        /// <param name="monthYear">The month year.</param>
        /// <returns></returns>
        public List<UsageSummary> GetViewCloningUsageDetails(string sortOrder, string sortExpression, int user, int monthYear)
        {
           
            return new AdminDLManager().GetViewCloningUsageDetails(sortOrder,
               sortExpression, user, monthYear);
            
        }


        public UserRegistrationInfo GetSubscriptionUserDetails(short userID)
        {
            return new AdminDLManager().GetSubscriptionUserDetails(userID);
        }

        public List<Roles> GetCorporateAdminAssignedRoles(short userID)
        {
            return new AdminDLManager().GetCorporateAdminAssignedRoles(userID);
            
        }

        public UserRegistrationInfo GetCorporateUserDetails(int userID)
        {
            return new AdminDLManager().GetCorporateDetails(userID);
        }


        /// <summary>
        /// Method that retrieves the list of candidate self skill which is not approved.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CandidateInformation"/> that holds the 
        /// CandidateInformation attributes..
        /// </returns>
        public List<CandidateInformation> GetCandidateSelfSkillPending()
        {
            return new AdminDLManager().GetCandidateSelfSkillPending();
        }

        /// <summary>
        /// Method that retrieves the list of skills for the given search
        /// parameters.
        /// </summary>
        /// <param name="skillName">
        /// A <see cref="string"/> that holds the skill name.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetRelatedCandidateSelfSkills(string skillName)
        {
            return new AdminDLManager().GetRelatedCandidateSelfSkills(skillName);
        }

        /// <summary>
        /// Method that retreives the candidate self skill name 
        /// </summary>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the skill name
        /// </returns>
        public string GetCandidateSelfSkillBySkillId(int skillID)
        {
            return new AdminDLManager().GetCandidateSelfSkillBySkillId(skillID);
        }

        /// <summary>
        /// Method that insert skill.
        /// </summary>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the candidateID
        /// </param>
        /// <param name="skillName">
        /// A <see cref="string"/> that holds the skill Name.
        /// </param>
        public void InsertSkillFromCandidateSelfSkill(int candidateSkillID, string skillName, int userID)
        {
            new AdminDLManager().InsertSkillFromCandidateSelfSkill(candidateSkillID, skillName, userID);
        }

        /// <summary>
        /// Method that updates candidate self skill detail.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidateID
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skillID
        /// </param>
        /// <param name="skillName">
        /// A <see cref="int"/> that holds the skillName
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the candidateID
        /// </param>
        public void UpdateCandidateSelfSkill(int candidateSkillID, int skillID, string skillName,int userID)
        {
            new AdminDLManager().UpdateCandidateSelfSkill(candidateSkillID,skillID,skillName,userID);
        }

        #region Customer Admin

        /// <summary>
        /// Method that retrieves the list of users for the given tenant.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the list of user details.
        /// </returns>
        public List<UserDetail> GetCustomerUser(int tenantID)
        {
            return new AdminDLManager().GetCustomerUser(tenantID);
        }

        /// <summary>
        /// Method that retrieves the list of user rights for the given tenant. 
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="showAllFeature">
        /// A <see cref="int"/> that holds the show all feature flag.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="totalRecordCount">
        /// A <see cref="int"/> that holds the total record count as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserRightsMatrix"/> that holds the user rights.
        /// </returns>
        public List<UserRightsMatrix> GetCustomerRightsDetails(string userID, int showAllFeature, int tenantID,
            out int totalRecordCount)
        {
            return new AdminDLManager().GetCustomerRightsDetails(userID, showAllFeature, tenantID, out totalRecordCount);
        }

        #endregion Customer Admin
    }
}   