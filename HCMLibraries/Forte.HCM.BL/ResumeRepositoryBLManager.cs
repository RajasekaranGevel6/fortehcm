﻿using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Resume Repository management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for resume details. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    public class ResumeRepositoryBLManager
    {
        /// <summary>
        /// Method to update resume status for the given candidate id
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate candidate id
        /// </param>                
        public void ApproveResume(int candidateID)
        {
            new ResumeRepositoryDLManager().ApproveResume(candidateID);
        }

        /// <summary>
        /// Method to get the candidate resume for the given candidate id. 
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds the candidate resume id 
        /// </param>        
        /// <returns>
        /// A<see cref="List<string>"/>that holds the resume id and resume source.
        /// </returns>
        public List<string> GetCandidateHRXml(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetCandidateHRXml(candidateID);
        }

        public List<string> GetTemporaryResumeHRXML(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetTemporaryResumeHRXML(candidateID);
        }

        public int UpdateTemporaryResumeStatus(int candidateID, string status)
        {
            return new ResumeRepositoryDLManager().UpdateTemporaryResumeStatus(candidateID, status);
        }
        
        public int DeleteTemporaryResume(int candidateID)
        {
            return new ResumeRepositoryDLManager().DeleteTemporaryResume(candidateID);
        }

        /// <summary>
        /// Method to set the candidate resume for the given candidate id
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds the candidate resume id 
        /// </param>
        /// <param name="oFileBytes">
        /// A <see cref="byte"/> that holds the resume data.
        /// </param>
        /// <param name="dblScore">
        /// A <see cref="string"/> that holds the score.
        /// </param>
        /// <param name="strMimeType">
        /// A <see cref="byte"/> that holds the mime type.
        /// </param>
        /// <param name="strFileName">
        /// A <see cref="string"/> that holds the filename name.
        /// </param>
        /// <returns>
        /// A<see cref="int"/>that holds the candidate resume id.
        /// </returns>
        public int InsertResumeSourceByBytes(int candidateID, byte[] oFileBytes,
            decimal dblScore, string MimeType, string strFileName, int userId)
        {
            return new ResumeRepositoryDLManager().InsertResumeSourceByBytes(candidateID, oFileBytes, dblScore, MimeType, strFileName, userId);
        }

        /// <summary>
        /// Method to set the candidate resume for the given candidate id
        /// </summary>
        /// <param name="oXmlDoc">
        /// A<see cref="string"/>that holds the resume source in xml.
        /// </param>
        /// <param name="candidateResumeID">
        /// A <see cref="byte"/> that holds the candidate resume id.
        /// </param>                
        public void InsertCandidateHRXml(string oXmlDoc, int candidateResumeID, int userId)
        {
            new ResumeRepositoryDLManager().InsertCandidateHRXml(oXmlDoc, candidateResumeID, userId);
        }

        /// <summary>
        /// Represents the method that is used to get the lneage item 
        /// from the database
        /// </summary>
        /// <returns>
        /// A<see cref="List<LineageItem>"/>that holds the list of lineage item
        /// </returns>
        public List<LineageItem> GetLineageItem()
        {
            return new ResumeRepositoryDLManager().GetLineageItem();
        }

        /// <summary>
        /// Method that retrieves the list of candidates for the given search 
        /// criteria.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="emailLike">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the candidate 
        /// details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<CandidateDetail> GetCandidateInformation(int tenantID, string firstNameLike,
            string middleNameLike, string lastNameLike, string emailLike,
            string orderBy, SortType orderByDirection,
            int pageNumber, int pageSize, out int totalRecords)
        {
            return new ResumeRepositoryDLManager().GetCandidateInformation(tenantID, firstNameLike, middleNameLike,
               lastNameLike, emailLike, orderBy, orderByDirection, pageNumber, pageSize,
               out totalRecords);
        }

        /// <summary>
        /// Method to get the candidate resume for the given candidate resume id
        /// </summary>
        /// <param name="candidateResumeID">
        /// A<see cref="int"/>that holds the candidate resume id 
        /// </param>
        /// <param name="mimeType">
        /// A <see cref="string"/> that holds the mime type as an output 
        /// parameter.
        /// </param>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the filename as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A<see cref="byte"/>that holds the resume data.
        /// </returns>
        public byte[] GetResumeContents(int candidateResumeID,
            out string mimeType, out string fileName)
        {
            return new ResumeRepositoryDLManager().GetResumeContents
                (candidateResumeID, out mimeType, out fileName);
        }

        /// <summary>
        /// Method that inserts the upload resume log entry into the upload 
        /// resume log table.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that hold the candidate ID.
        /// </param>
        /// <param name="candidateName">
        /// A <see cref="string"/> that holds the candidate name.
        /// </param>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the file name.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the candidate activity log ID.
        /// </returns>
        public int InsertUploadResumeLog(int candidateID, string candidateName, 
            string fileName, int userID,string profileName)
        {
            // Insert resume log.
            new ResumeRepositoryDLManager().InsertUploadResumeLog
                (candidateID, candidateName, fileName, userID,profileName);

            // Insert candidate activity log.
            return new CommonDLManager().InsertCandidateActivityLog(candidateID, 0,
                string.Format("New resume '{0}' uploaded for candidate", fileName), 0, userID, 
                Constants.CandidateActivityLogType.RESUME_UPLOADED_THROUGH_WEB_BASED_SYSTEM_);
        }

        /// <summary>
        /// Inserts the new candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int InsertNewCandidate(CandidateInformation candidateInformation, 
            int userID, int tenantID)
        {
            return new ResumeRepositoryDLManager().InsertNewCandidate
                (candidateInformation, userID, tenantID);
        }

        /// <summary>
        /// Inserts the new signup candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// <returns></returns>
        public int InsertSignupCandidate(CandidateInformation candidateInformation,
            int userID, int tenantID, IDbTransaction transaction)
        {
            return new ResumeRepositoryDLManager().InsertSignupCandidate
                (candidateInformation, userID, tenantID,transaction);
        }

        /// <summary>
        /// Inserts the new signup candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// <returns></returns>
        public int InsertSignupUsers(int tenantID,
            CandidateInformation candidateInformation, int createdBy,
            string confirmationCode, IDbTransaction transaction)
        { 
            return new ResumeRepositoryDLManager().InsertSignupUsers
                (tenantID,candidateInformation, createdBy, confirmationCode, transaction);
        }


        /// <summary>
        /// Updates the candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdateCandidate(CandidateInformation candidateInformation, int userID)
        {
            new ResumeRepositoryDLManager().UpdateCandidate(candidateInformation, userID);
        }


        /// <summary>
        /// Updates the candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>         
        public void UpdateCandidateNames(CandidateInformation candidateInformation, int userID)
        {
            new ResumeRepositoryDLManager().UpdateCandidateNames(candidateInformation, userID);
        }

        /// <summary>
        /// Gets the search candidate information.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="firstNameLike">The first name like.</param>
        /// <param name="middleNameLike">The middle name like.</param>
        /// <param name="lastNameLike">The last name like.</param>
        /// <param name="emailLike">The email like.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDirection">The order by direction.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <returns></returns>
        public List<CandidateInformation> GetSearchCandidateInformation(int tenantID,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string emailLike, string orderBy, SortType orderByDirection,
             int pageNumber, int pageSize,string recruiterID, bool searchType, string resumeType, string candidateActive,
            DateTime fromDate, DateTime toDate, int positionId, out int totalRecords)
        {
            return new ResumeRepositoryDLManager().GetCandidateDetailsInformation(tenantID,
                firstNameLike, middleNameLike, lastNameLike, emailLike, orderBy,
                orderByDirection, pageNumber, pageSize,recruiterID,searchType, resumeType,candidateActive,
                fromDate,toDate,positionId, out totalRecords);
        }

        /// <summary>
        /// Method that search for candidates by keywords
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="keywords">
        /// A <see cref="string"/> that holds the keyword.
        /// </param>
        /// <param name="fromDate">
        /// A <see cref="DateTime"/> that holds the from date.
        /// </param>
        /// <param name="toDate">
        /// A <see cref="DateTime"/> that holds the to date.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds teh order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that holds the sort type.
        /// </param>
        /// <param name="candidateActive">
        /// A <see cref="string"/> that holds the candidate active status.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="positionProfielID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInformation"/> that holds the candidate details.
        /// </returns>
        public List<CandidateInformation> GetSearchCandidateInformation(int tenantID, int userID,
           string keywords, DateTime fromDate, DateTime toDate, string orderBy, SortType orderByDirection, string candidateActive,
           int pageNumber, int pageSize, int positionProfielID, out int totalRecords)
        {
            return new ResumeRepositoryDLManager().GetCandidateDetailsInformation(tenantID, userID,
                keywords, fromDate, toDate, orderBy, orderByDirection, candidateActive,
                pageNumber, pageSize, positionProfielID, out totalRecords);
        }
        /// <summary>
        /// Inserts the candidate users.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="createdBy">The created by.</param>
        /// <param name="confirmationCode">The confirmation code.</param>
        public int InsertCandidateUsers(int tenantID,
            CandidateInformation candidateInformation, int createdBy, string confirmationCode)
        {
             return new ResumeRepositoryDLManager().InsertCandidateUsers(tenantID,
                candidateInformation, createdBy, confirmationCode);

        } 

        /// <summary>
        /// Gets the candidate information.
        /// </summary>
        /// <param name="candidateID">The candidate ID.</param>
        /// <returns></returns>
        public CandidateInformation GetCandidateInformation(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetCandidateInformation(candidateID);
        }

        /// <summary>
        /// Inserts the downloadload resume log.
        /// </summary>
        /// <param name="candidateID">The candidate ID.</param>
        /// <param name="candidateName">Name of the candidate.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="userID">The user ID.</param>
        public void InsertDownloadloadResumeLog(int candidateID, string candidateName, string fileName, int userID)
        {
            new ResumeRepositoryDLManager().InsertDownloadloadResumeLog(candidateID, candidateName, fileName, userID);
        }

        public List<TechnicalSkillsGroup> GetTechnicalGroup()
        {
            return new ResumeRepositoryDLManager().GetTechnicalGroup();
        }

        public List<TechnicalSkillsDetails> GetTechSkillsDetails(int techGroupId, string techSkillName,
            string techSkillAliasName, string sortField, string sortOrder, int pageNumber, int pageSize,
            out int total)
        {
            return new ResumeRepositoryDLManager().GetTechSkillsDetails(techGroupId, techSkillName,
                techSkillAliasName, sortField, sortOrder, pageNumber, pageSize, out total);
        }

        public bool AddNewSkills(int techGroupID, string skillName, string aliasName, int userID)
        {
            return new ResumeRepositoryDLManager().AddNewSkills(techGroupID, skillName, aliasName, userID);
        }

        /// <summary>
        /// Represents the method to update the skill alias name
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the skill id 
        /// </param>
        /// <param name="skillAliasName">
        /// A<see cref="string"/>that holds the skill alias name
        /// </param>
        public void UpdateTechnicalSkillsAliasName(int skillID, string skillAliasName)
        {
            new ResumeRepositoryDLManager().UpdateSkillAliasName(skillID, skillAliasName);
        }
        /// <summary>
        /// Represents the method that is used to check whether 
        /// skill id is invloved in candidate skill id
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the skill id 
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds the status
        /// </returns>
        public bool CheckSkillIDInvolved(int skillID)
        {
            return new ResumeRepositoryDLManager().CheckSkillIDInvolved(skillID);
        }

        /// <summary>
        /// Represents the method that is used to delete the technical skills
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the skill id
        /// </param>
        public void DeleteTechnicalSkill(int skillID)
        {
            new ResumeRepositoryDLManager().DeleteTechnicalSkill(skillID);
        }

        /// <summary>
        /// Represent the method that is used to get the nodes 
        /// based on the selected node type
        /// </summary>
        /// <param name="nodeType">
        /// A<see cref="string"/>that holds the node type
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="gridPageSize">
        /// A<see cref="int"/>that holds the grid page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total records
        /// </param>
        /// <param name="searchKeyWord">
        /// A<see cref="string"/>that holds the search key word 
        /// </param>
        /// <returns></returns>
        public List<NodeDetail> GetNodes(string nodeType,
            int pageNumber, int gridPageSize, out int totalRecords, string searchKeyWord)
        {
            List<NodeDetail> nodes = null;

            totalRecords = 0;

            switch (nodeType)
            {
                case "1":
                    nodes = new ResumeRepositoryDLManager().GetPositionTitleNodes(pageNumber, gridPageSize,
                        out totalRecords, searchKeyWord);
                    break;
                case "2":
                    nodes = new ResumeRepositoryDLManager().GetDegree(pageNumber, gridPageSize,
                        out totalRecords, searchKeyWord);
                    break;
                case "3":
                    nodes = new ResumeRepositoryDLManager().GetNegativeTitles(pageNumber, gridPageSize,
                        out totalRecords, searchKeyWord);
                    break;
                case "4":
                    nodes = new ResumeRepositoryDLManager().GetDisciplines(pageNumber, gridPageSize,
                        out totalRecords, searchKeyWord);
                    break;
            }
            return nodes;
        }

        /// <summary>
        /// Method that inserts node details.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the node details.
        /// </param>
        public void InserNode(NodeDetail nodeDetail, string nodeType)
        {
            switch (nodeType)
            {
                case "1":
                    new ResumeRepositoryDLManager().InsertPositionTitle(nodeDetail);
                    //Insert the position title into the Competency vector 
                    new CompetencyVectorBLManager().AddNewVector(Constants.CompetencyVectorGroupConstants.ROLES,
                        nodeDetail.Name, nodeDetail.AliasName, nodeDetail.CreatedBy);
                    break;
                case "2":
                    new ResumeRepositoryDLManager().InsertDegree(nodeDetail);
                    break;
                case "3":
                    new ResumeRepositoryDLManager().InsertNegativeTitle(nodeDetail);
                    break;
                case "4":
                    new ResumeRepositoryDLManager().InsertDisciplines(nodeDetail);
                    break;
            }
        }

        /// <summary>
        /// Method that updates node details.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the node details.
        /// </param>
        public void UpdateNode(NodeDetail nodeDetail, string nodeType)
        {
            switch (nodeType)
            {
                case "1":
                    new ResumeRepositoryDLManager().UpdatePositionTitle(nodeDetail);
                    break;
                case "2":
                    new ResumeRepositoryDLManager().UpdateDegree(nodeDetail);
                    break;
                case "3":
                    new ResumeRepositoryDLManager().UpdateNegativeTitle(nodeDetail);
                    break;
                case "4":
                    new ResumeRepositoryDLManager().UpdateDiscipline(nodeDetail);
                    break;
            }
        }

        /// <summary>
        /// Method that deletes node detail.
        /// </summary>
        /// <param name="ID">
        /// A <see cref="int"/> that holds the nodes ID .
        /// </param>
        public void DeleteNode(int ID, string nodeType)
        {
            switch (nodeType)
            {
                case "1":
                    new ResumeRepositoryDLManager().DeletePositionTitle(ID);
                    break;
                case "2":
                    new ResumeRepositoryDLManager().DeleteDegree(ID);
                    break;
                case "3":
                    new ResumeRepositoryDLManager().DeleteNegativeTitle(ID);
                    break;
                case "4":
                    new ResumeRepositoryDLManager().DeleteDiscipline(ID);
                    break;
            }
        }

        /// <summary>
        /// Represents the method to get the full node list from the database
        /// </summary>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/>that holds the list of nodes 
        /// </returns>
        public List<NodeDetail> GetNodesFullList(string nodeType)
        {
            List<NodeDetail> nodes = null;

            switch (nodeType)
            {
                case "1":
                    nodes = new ResumeRepositoryDLManager().GetPositionNodesFullList();
                    break;
                case "2":
                    nodes = new ResumeRepositoryDLManager().GetDegreeNodesFullList();
                    break;
                case "3":
                    nodes = new ResumeRepositoryDLManager().GetNegativeFullList();
                    break;
                case "4":
                    nodes = new ResumeRepositoryDLManager().GetDisciplineFullList();
                    break;
            }
            return nodes;
        }

        /// <summary>
        /// Method that returns the list of location types from the Database
        /// </summary>
        /// <returns>
        /// A<see cref="List<LocationDetails>"/>that holds the list of location types
        /// </returns>
        public List<LocationDetails> GetLocationType()
        {
            return new ResumeRepositoryDLManager().GetLocationType();
        }

        /// <summary>
        /// Method that returns the list of location details from the database,
        /// based on the search criteria
        /// </summary>
        /// <param name="locationId">
        /// A<see cref="int"/>that holds the location ID
        /// </param>
        /// <param name="locationName">
        /// A<see cref="string"/>that holds the location name
        /// </param>
        /// <param name="locationAliasName">
        /// A<see cref="string"/>that holds the location alias name
        /// </param>
        /// <param name="sortField">
        /// A<see cref="string"/>that holds the sort field
        /// </param>
        /// <param name="sortOrder">
        /// A<see cref="string"/>that holds the sort order
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pagenumber
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <param name="total">
        /// A<see cref="int"/>that holds the total
        /// </param>
        /// <returns>
        /// A<see cref="List<LocationDetails>"/>that holds the 
        /// list of location details
        /// </returns>
        public List<LocationDetails> GetLocationDetails(int locationId, string locationName,
          string locationAliasName, string sortField, string sortOrder, int pageNumber, int pageSize,
          out int total)
        {
            return new ResumeRepositoryDLManager().GetLocationDetails(locationId, locationName,
                locationAliasName, sortField, sortOrder, pageNumber, pageSize, out total);
        }

        /// <summary>
        /// Method to add the new location to the database
        /// </summary>
        /// <param name="locationGroupID">
        /// A<see cref="int"/>that holds the locationGroupID
        /// </param>
        /// <param name="locationName">
        /// A<see cref="string"/>that holds the location name
        /// </param>
        /// <param name="aliasName">
        /// A<see cref="string"/>that holds the alias name
        /// </param>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userID
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds bool value whether the 
        /// location already exist or not
        /// </returns>
        public bool AddNewLocation(int locationGroupID, string locationName, string aliasName, int userID)
        {
            return new ResumeRepositoryDLManager().AddNewLocation(locationGroupID, locationName, aliasName, userID);
        }

        /// <summary>
        /// Represents the method to update the location alias name
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the skill id 
        /// </param>
        /// <param name="skillAliasName">
        /// A<see cref="string"/>that holds the skill alias name
        /// </param>
        public void UpdateLocationAliasName(int locationID, string locationAliasName)
        {
            new ResumeRepositoryDLManager().UpdateLocationAliasName(locationID, locationAliasName);
        }

        /// <summary>
        /// Represents the method that is used to delete the location
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the skill id
        /// </param>
        public void DeleteLocation(int skillID)
        {
            new ResumeRepositoryDLManager().DeleteLocation(skillID);
        }

        /// <summary>
        /// Method that retrieves the list dictionary details.
        /// </summary>
        /// <param name="dictionaryTypeID">
        /// A <see cref="int"/> that holds the dictionary type ID.
        /// </param>
        /// <param name="headLike">
        /// A <see cref="string"/> that holds the head keywords.
        /// </param>
        /// <param name="aliasLike">
        /// A <see cref="string"/> that holds the alias keywords.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="DictionaryDetail"/> that holds the dictionary 
        /// details.
        /// </returns>
        public List<DictionaryDetail> GetDictionaryDetails(int dictionaryTypeID,
            string headLike, string aliasLike, string orderBy,
            SortType orderByDirection, int pageNumber, int pageSize,
            out int totalRecords)
        {
            return new ResumeRepositoryDLManager().GetDictionaryDetails
                (dictionaryTypeID, headLike, aliasLike, orderBy,
                orderByDirection, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of element types.
        /// </summary>
        /// <returns>
        /// A list of <see cref="ElementTypeDetail"/> that holds the element
        /// type details.
        /// </returns>
        public List<ElementTypeDetail> GetElementTypes()
        {
            return new ResumeRepositoryDLManager().GetElementTypes();
        }

        /// <summary>
        /// Method that retrieves the list of elements for the given element
        /// type ID.
        /// </summary>
        /// <param name="elementTypeID">
        /// A <see cref="int"/> that holds the element type ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="ElementDetail"/> that holds the element details.
        /// </returns>
        public List<ElementDetail> GetElements(int elementTypeID)
        {
            return new ResumeRepositoryDLManager().GetElements(elementTypeID);
        }

        /// <summary>
        /// Method that retrieves the attribute dictionary list for the given 
        /// element ID.
        /// </summary>
        /// <param name="elementID">
        /// A <see cref="int"/> that holds the element ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="AliasDetail"/> that holds the attribute 
        /// dictionary details.
        /// </returns>
        public List<AliasDetail> GetAttributeDictionary(int elementID)
        {
            return new ResumeRepositoryDLManager().GetAttributeDictionary(elementID);
        }

        /// <summary>
        /// Method that retrieves the title dictionary list for the given 
        /// element ID.
        /// </summary>
        /// <param name="elementID">
        /// A <see cref="int"/> that holds the element ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="AliasDetail"/> that holds the title 
        /// dictionary details.
        /// </returns>
        public List<AliasDetail> GetTitleDictionary(int elementID)
        {
            return new ResumeRepositoryDLManager().GetTitleDictionary(elementID);
        }

        /// <summary>
        /// Method that inserts record into title dictionary.
        /// </summary>
        /// <param name="aliasDetail">
        /// A <see cref="AliasDetail"/> that holds the alias detail.
        /// </param>
        public void InsertTitleDictionary(AliasDetail aliasDetail)
        {
            new ResumeRepositoryDLManager().InsertTitleDictionary(aliasDetail);
        }

        /// <summary>
        /// Method that inserts record into attribute dictionary.
        /// </summary>
        /// <param name="aliasDetail">
        /// A <see cref="AliasDetail"/> that holds the alias detail.
        /// </param>
        public void InsertAttributeDictionary(AliasDetail aliasDetail)
        {
            new ResumeRepositoryDLManager().InsertAttributeDictionary(aliasDetail);
        }

        /// <summary>
        /// Method that updates record into title dictionary.
        /// </summary>
        /// <param name="aliasDetail">
        /// A <see cref="AliasDetail"/> that holds the alias detail.
        /// </param>
        public void UpdateTitleDictionary(AliasDetail aliasDetail)
        {
            new ResumeRepositoryDLManager().UpdateTitleDictionary(aliasDetail);
        }

        /// <summary>
        /// Method that updates record into attribute dictionary.
        /// </summary>
        /// <param name="aliasDetail">
        /// A <see cref="AliasDetail"/> that holds the alias detail.
        /// </param>
        public void UpdateAttributeDictionary(AliasDetail aliasDetail)
        {
            new ResumeRepositoryDLManager().UpdateAttributeDictionary(aliasDetail);
        }

        /// <summary>
        /// Method that delete record from the title dictionary.
        /// </summary>
        /// <param name="titleID">
        /// A <see cref="int"/> that holds the title ID.
        /// </param>
        public void DeleteTitleDictionary(int titleID)
        {
            new ResumeRepositoryDLManager().DeleteTitleDictionary(titleID);
        }

        /// <summary>
        /// Method that delete record from the attribute dictionary.
        /// </summary>
        /// <param name="attributeID">
        /// A <see cref="int"/> that holds the attribute ID.
        /// </param>
        public void DeleteAttributeDictionary(int attributeID)
        {
            new ResumeRepositoryDLManager().DeleteAttributeDictionary(attributeID);
        }
        
        /// <summary>
        /// Represents the method to get the parser options
        /// </summary>
        /// <returns>
        /// A<see cref="ParserSettings"/>that holds the options details
        /// </returns>
        public ParserSettings GetParserOptions()
        {
            return new ResumeRepositoryDLManager().GetParserOptions();
        }

        /// <summary>
        /// Represents the method that is used to update the 
        /// parser settings
        /// </summary>
        /// <param name="parserSetting">
        /// A<see cref="ParserSettings"/>that holds the parser settings details
        /// </param>
        public void UpdateParserSettings(ParserSettings parserSetting)
        {
            new ResumeRepositoryDLManager().UpdateParserSettings(parserSetting);
        }

        /// <summary>
        /// Method that retrieves the list of candidate activities for the
        /// given search parameters.
        /// </summary>
        /// <param name="searchCriteria">
        /// A <see cref="CandidateActivityLogSearchCriteria"/> that holds the
        /// search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pageNumber
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the pageSize
        /// </param>
        /// <param name="sortField">
        /// A<see cref="string"/>that holds the sort Field
        /// </param>
        /// <param name="sortOrder">
        /// A<see cref="SortType"/>that holds the sordOrder like Asc/Dsc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateActivityLog"/> that holds candidate 
        /// activity log details.
        /// </returns>
        public List<CandidateActivityLog> GetCandidateActivities
            (CandidateActivityLogSearchCriteria searchCriteria, int pageNumber,
            int pageSize, string sortField, SortType sortOrder,
            out int totalRecords)
        {
            return new ResumeRepositoryDLManager().GetCandidateActivities
                (searchCriteria, pageNumber, pageSize, sortField, 
                sortOrder, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the list of candidate activities for the
        /// given search parameters as a data table for export to excel.
        /// </summary>
        /// <param name="searchCriteria">
        /// A <see cref="CandidateActivityLogSearchCriteria"/> that holds the
        /// search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pageNumber
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the pageSize
        /// </param>
        /// <param name="sortField">
        /// A<see cref="string"/>that holds the sort Field
        /// </param>
        /// <param name="sortOrder">
        /// A<see cref="SortType"/>that holds the sordOrder like Asc/Dsc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateActivityLog"/> that holds candidate 
        /// activity log details.
        /// </returns>
        public DataTable GetCandidateActivitiesTable
            (CandidateActivityLogSearchCriteria searchCriteria, int pageNumber,
            int pageSize, string sortField, SortType sortOrder,
            out int totalRecords)
        {
            return new ResumeRepositoryDLManager().GetCandidateActivitiesTable
                (searchCriteria, pageNumber, pageSize, sortField, 
                sortOrder, out totalRecords);
        }

        /// <summary>
        /// Method that retrieves the resume detail for the given candidate.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateResumeDetails"/> that holds the candidate
        /// resume detail.
        /// </returns>
        public CandidateResumeDetails GetResumeSource(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetResumeSource(candidateID);
        }

        /// <summary>
        /// Method that retrieves the all resume detail for the given candidate.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateResumeDetails"/> that holds the candidate
        /// resume detail.
        /// </returns>
        public List<CandidateResumeDetails> GetCandidateReviewResumes(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetCandidateReviewResumes(candidateID);
        }

        /// Method that retrieves the candidate type for the
        /// given search parameters.
        /// </summary> 
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds candidateID
        /// </param> 
        public CandidateSearchCriteria GetCandidateTypeDetail(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetCandidateTypeDetail(candidateID);
        }

        
        public List<CandidateResumeDetails> GetTemporaryResumes(ReviewResumeDetail reviewResumeDetail, out int totalRecords)
        {
            return new ResumeRepositoryDLManager().GetTemporaryResumes(reviewResumeDetail, out totalRecords);
        }

        /// <summary>
        /// Method to get the candidate temporary resume for the given candidate id
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds the candidate  id 
        /// </param>
        /// <param name="mimeType">
        /// A <see cref="string"/> that holds the mime type as an output 
        /// parameter.
        /// </param>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the filename as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A<see cref="byte"/>that holds the resume data.
        /// </returns>
        public byte[] GetTemporaryResumeContents(int candidateID,
            out string mimeType, out string fileName)
        {
            return new ResumeRepositoryDLManager().GetTemporaryResumeContents
                (candidateID, out mimeType, out fileName);
        }

        /// <summary>
        /// Method that retrieves the resume node and its elements
        /// </summary>
        /// <returns></returns>
        public List<ResumeElementsDetail> GetResumeElements(int tenantId)
        {
            return new ResumeRepositoryDLManager().GetResumeElements(tenantId);
        }

        /// <summary>
        /// Method to insert the resume validation rule
        /// </summary>
        /// <param name="resumeElementDetails">resumeElementDetails</param>
        /// <param name="tenantId">tenantId</param>
        /// <param name="userID">userID</param>
        public void InsertResumeValidationRule(List<ResumeElementsDetail> resumeElementDetails, int tenantId, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                string elementIds = string.Empty;
                ResumeElementsDetail resumeInsertDetail = null;
                foreach (ResumeElementsDetail resumeElementDetail in resumeElementDetails)
                {
                    resumeInsertDetail = new ResumeElementsDetail();
                    resumeInsertDetail.ElementId = resumeElementDetail.ElementId;
                    resumeInsertDetail.ValidateExprType = resumeElementDetail.ValidateExprType;
                    resumeInsertDetail.ValidateExprValue = resumeElementDetail.ValidateExprValue;
                    resumeInsertDetail.ValidateReason = resumeElementDetail.ValidateReason;
                    new ResumeRepositoryDLManager().InsertResumeValidationRule(resumeInsertDetail, tenantId, userID, transaction);
                    elementIds = elementIds + resumeElementDetail.ElementId + ",";
                }

                if (!Utility.IsNullOrEmpty(elementIds))
                {
                    //Delete unchecked element id against tenant id
                    elementIds = elementIds.ToString().Trim().TrimEnd(',');
                    new ResumeRepositoryDLManager().DeleteValidationRules(tenantId, elementIds, transaction);
                }
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the external data detail for the given candidate.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateResumeDetails"/> that holds the candidate external data source
        /// </returns>
        public CandidateResumeDetails GetCandidateExternalDataStore(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetCandidateExternalDataStore(candidateID);
        }

        public List<CandidateResumeDetails> GetDuplicateResumeCandidate(int tenantID, string firstName,
            string lastName, string emailId, string phoneNo, int pageNo, int pageSize,out int totalRecords)
        {
            return new ResumeRepositoryDLManager().GetDuplicateResumeCandidate(tenantID,firstName,
            lastName, emailId, phoneNo, pageNo, pageSize, out totalRecords);
        }

        public List<CandidateResumeDetails> GetCandidateResumeList(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetCandidateResumeList(candidateID);
        }

        /// <summary>
        /// Method to get the email reminder settings
        /// </summary>
        /// <param name="tenantID"></param>
        /// <returns></returns>
        public List<ResumeEmailReminderDetail> GetResumeEmailReminderSettings(int tenantID)
        {
            return new ResumeRepositoryDLManager().GetResumeEmailReminderSettings(tenantID);
        }

        /// <summary>
        /// Method to insert the resume email reminder settings
        /// </summary>
        /// <param name="resumeEmailReminderDetail">resumeEmailReminderDetail</param>
        /// <param name="tenantID">tenantID</param>
        /// <param name="userID">userID</param>
        /// <param name="transaction"></param>
        public void InsertResumeEmailReminderSettings(List<ResumeEmailReminderDetail> resumeEmailReminderDetails, 
            int tenantID, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                
                //Delete existing email reminder settings
                new ResumeRepositoryDLManager().DeleteResumeEmailReminderSettings(tenantID);

                if (resumeEmailReminderDetails != null && resumeEmailReminderDetails.Count > 0)
                {
                    ResumeEmailReminderDetail insertEmailReminderDetail = null;
                    foreach (ResumeEmailReminderDetail resumeEmailReminderDetail in resumeEmailReminderDetails)
                    {
                        insertEmailReminderDetail = new ResumeEmailReminderDetail();
                        insertEmailReminderDetail.UserType = resumeEmailReminderDetail.UserType;
                        insertEmailReminderDetail.EmailOption = resumeEmailReminderDetail.EmailOption;
                        new ResumeRepositoryDLManager().InsertResumeEmailReminderSettings(insertEmailReminderDetail,
                            tenantID, userID, transaction);
                    }
                }
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the list of incomplete resume reminders.
        /// </summary>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. 'D' represents daily
        /// and 'W' represents weekly.
        /// </param>
        /// <returns>
        /// A list of <see cref="ResumeReminderDetail"/> that holds the 
        /// reminder email and files list.
        /// </returns>
        public List<ResumeReminderDetail> GetIncompleteResumeReminders(string type)
        {
            return new ResumeRepositoryDLManager().GetIncompleteResumeReminders(type);
        }

        /// <summary>
        /// Method that retrieves the list of duplicate resume reminders.
        /// </summary>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. 'D' represents daily
        /// and 'W' represents weekly.
        /// </param>
        /// <returns>
        /// A list of <see cref="ResumeReminderDetail"/> that holds the 
        /// reminder email and files list.
        /// </returns>
        public List<ResumeReminderDetail> GetDuplicateResumeReminders(string type)
        {
            return new ResumeRepositoryDLManager().GetDuplicateResumeReminders(type);
        }

        /// <summary>
        /// Method that retrieves the list of duplicate resume reminders for recruiters.
        /// </summary>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. 'D' represents daily
        /// and 'W' represents weekly.
        /// </param>
        /// <returns>
        /// A list of <see cref="ResumeReminderDetail"/> that holds the 
        /// reminder email and files list.
        /// </returns>
        public List<ResumeReminderDetail> GetRecruiterDuplicateResumeReminders(string type)
        {
            return new ResumeRepositoryDLManager().GetRecruiterDuplicateResumeReminders(type);
        }

        /// <summary>
        /// Method that updates the candidate information with the data taken
        /// from parsed HRXML.
        /// </summary>
        /// <param name="transaction">
        /// A <see cref="DbTransaction"/> that holds the transaction object.
        /// </param>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="resumeUploadType">
        /// A <see cref="string"/> that holds the resume upload type.
        /// </param>
        public void UpdateCandidateInformation(CandidateDetail candidateDetail,
            Int64 candidateID, int userID, string resumeUploadType)
        {
            new ResumeRepositoryDLManager().UpdateCandidateInformation(candidateDetail, candidateID, userID, resumeUploadType);
        }

         /// <summary>
        /// Method that retrieves the candidate resume versions.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="ResumeVersion"/> that holds the resume versions.
        /// </returns>
        public List<ResumeVersion> GetResumeVersions(int candidateID)
        {
            return new ResumeRepositoryDLManager().GetResumeVersions(candidateID);
        }

        /// <summary>
        /// Method that retrieves the candidate resume detail such as HRXML, 
        /// approved status, etc.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/>that holds the candidate resume ID.
        /// </param>        
        /// <returns>
        /// A <see cref="List<string>"/>that holds the resume id and resume source.
        /// </returns>
        public ResumeDetail GetCandidateResumeDetail(int candidateResumeID)
        {
            return new ResumeRepositoryDLManager().GetCandidateResumeDetail(candidateResumeID);
        }

        /// <summary>
        /// Method updating the temporary candidate basic information
        /// </summary>
        /// <param name="candidateInfo"><see cref="candidateinformation"/></param>
        /// <returns>
        /// if updated it returns true else false
        /// </returns>
        public bool UpdateTemporaryCandidateContactInfo(CandidateInformation candidateInfo)
        {
            return new ResumeRepositoryDLManager().UpdateTemporaryCandidateContactInfo(candidateInfo);
        }

        /// <summary>
        /// Method that insert record into parser candidate information table and 
        /// returns the parser candidate ID.
        /// </summary>
        /// <param name="candidateInfoID">
        /// A <see cref="int"/> that holds the candidate information ID.
        /// </param>
        /// <param name="uploadType">
        /// A <see cref="string"/> that holds the upload type.
        /// </param>
        /// <param name="resumeName">
        /// A <see cref="string"/> that holds the resume name.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the parser candidate ID.
        /// </returns>
        public int InsertParserCandidateInformation(int candidateInfoID, string uploadType,
            string resumeName, int recruiterID)
        {
            return new ResumeRepositoryDLManager().InsertParserCandidateInformation
                (candidateInfoID, uploadType, resumeName, recruiterID);
        }

        public List<ResumeRuleDetail> GetResumeRulesList(ResumeRuleDetail resumeRule, out int totalRecords)
        { 
            return new ResumeRepositoryDLManager().GetResumeRulesList(resumeRule,out totalRecords);
        }

        public int DeleteResumeRule(int ruleId)
        {
            return new ResumeRepositoryDLManager().DeleteResumeRule(ruleId);
        }

        public int UpdateResumeRuleStatus(int ruleId, string status)
        {
            return new ResumeRepositoryDLManager().UpdateResumeRuleStatus(ruleId, status);
        }

        
        /// <summary>
        /// Represents the method to get the list of resume rules
        /// </summary>
        /// <returns>
        /// A<see cref="ResumeElementsDetail"/>that holds the resume rule element details
        /// </returns>
        public List<ResumeElementsDetail> GetResumeRuleElements()
        {
            return new ResumeRepositoryDLManager().GetResumeRuleElements();
        }

        /// <summary>
        /// Method that insert record into resume duplication rule table and 
        /// returns the rule ID.
        /// </summary>
        /// <param name="resumeRuleDetail">
        /// A <see cref="resumeRuleDetail"/> that holds the resume rule detail.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        public void InsertResumeDuplicationRule(List<ResumeRuleDetail> resumeRuleDetails,
            ResumeRuleDetail resumeRuleDetail,string actions,int tenantID, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                int ruleID = 0;
                //Insert resume duplication rule
                ruleID = new ResumeRepositoryDLManager().
                    InsertResumeDuplicationRule(resumeRuleDetail, tenantID, userID, transaction);

                //Insert resume duplication rule entries
                if (!Utility.IsNullOrEmpty(resumeRuleDetails) && resumeRuleDetails.Count > 0)
                {
                    foreach (ResumeRuleDetail resumeRule in resumeRuleDetails)
                    {
                        new ResumeRepositoryDLManager().
                            InsertResumeDuplicationRuleEntries(resumeRule, ruleID, userID, transaction);
                    }
                }
                //Insert resume duplication rule actions
                if (!Utility.IsNullOrEmpty(actions))
                {
                    foreach (string action in actions.Split(new char[] { ',' }))
                    {
                        new ResumeRepositoryDLManager().InsertResumeDuplicationRuleActions(
                            ruleID, action.ToString().Trim(), userID, transaction);
                    }
                }

                //Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// Method that updates record in resume duplication 
        /// rule table against its rule id
        /// </summary>
        /// <param name="resumeRuleDetail">
        /// A <see cref="resumeRuleDetail"/> that holds the resume rule detail.
        /// </param>
        /// <param name="ruleID">
        /// A <see cref="int"/> that holds the rule ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        public void UpdateResumeDuplicationRule(List<ResumeRuleDetail> resumeRuleDetails,
            ResumeRuleDetail resumeRuleDetail, int ruleID, string actions, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                
                //Update resume duplication rule
                new ResumeRepositoryDLManager().UpdateResumeDuplicationRule(resumeRuleDetail, ruleID, userID, transaction);

                //Delete duplication rule entries
                new ResumeRepositoryDLManager().DeleteReumeDuplicationRuleEntries(ruleID);

                //Insert resume duplication rule entries
                if (!Utility.IsNullOrEmpty(resumeRuleDetails) && resumeRuleDetails.Count > 0)
                {
                    foreach (ResumeRuleDetail resumeRule in resumeRuleDetails)
                    {
                        new ResumeRepositoryDLManager().
                            InsertResumeDuplicationRuleEntries(resumeRule, ruleID, userID, transaction);
                    }
                }

                //Delete duplication rule actions
                new ResumeRepositoryDLManager().DeleteReumeDuplicationRuleActions(ruleID);

                //Insert resume duplication rule actions
                if (!Utility.IsNullOrEmpty(actions))
                {
                    foreach (string action in actions.Split(new char[] { ',' }))
                    {
                        new ResumeRepositoryDLManager().InsertResumeDuplicationRuleActions(
                            ruleID, action.ToString().Trim(), userID, transaction);
                    }
                }

                //Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// Method to set the xml node the given element id
        /// </summary>
        /// <param name="elementID">
        /// A<see cref="int"/>that holds the element ID 
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the xml node and sql field.
        /// </returns>
        public List<string> GetRumeRuleXmlNode(int elementID)
        {
            return new ResumeRepositoryDLManager().GetRumeRuleXmlNode(elementID);
        }

        /// <summary>
        /// Represents the method to get the list of resume rules
        /// </summary>
        /// <returns>
        /// A<see cref="ResumeElementsDetail"/>that holds the resume rule element details
        /// </returns>
        public ResumeRuleDetail GetResumeRuleDetail(int ruleID)
        {
            return new ResumeRepositoryDLManager().GetResumeRuleDetail(ruleID);
        }

         /// <summary>
        /// Method that retrieves the failed email list.
        /// </summary>
        /// <param name="maxAttempt">
        /// A <see cref="int"/> that holds the max attempt. Failed emails with 
        /// less than this limit only will get retrieved.
        /// </param>
        /// <returns>
        /// A list of <see cref="FailedEmailDetail"/> that holds the failed email.
        /// </returns>
        public List<FailedEmailDetail> GetFailedEmails(int maxAttempt)
        {
            return new ResumeRepositoryDLManager().GetFailedEmails(maxAttempt);
        }

         /// <summary>
        /// Method that deletes the failed mail log.
        /// </summary>
        /// <param name="failureLogID">
        /// A <see cref="int"/> that holds the failure log ID.
        /// </param>
        public void DeleteFailedEmail(int failureLogID)
        {
            new ResumeRepositoryDLManager().DeleteFailedEmail(failureLogID);
        }

        /// <summary>
        /// Method that updates the failed email log attempt.
        /// </summary>
        /// <param name="failureLogID">
        /// A <see cref="int"/> that holds the failure log ID.
        /// </param>
        /// <param name="attempt">
        /// A <see cref="int"/> that holds the attempt.
        /// </param>
        public void UpdateFailedEmailAttempt(int failureLogID, int attempt)
        {
            new ResumeRepositoryDLManager().UpdateFailedEmailAttempt(failureLogID, attempt);
        }

        public int CheckIfUserExist(CandidateInformation candidateInformation, int tenantID)
        {
            return new ResumeRepositoryDLManager().CheckIfUserExist(candidateInformation, tenantID);
        }
    }
}