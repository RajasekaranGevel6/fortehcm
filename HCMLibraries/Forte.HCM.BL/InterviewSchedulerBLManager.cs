﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewSchedulerBLManager.cs
// File that represents the data layer for the Test Scheduler module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.SmptClientService;

#endregion

namespace Forte.HCM.BL
{
    public class InterviewSchedulerBLManager
    {

        /// <summary>
        /// Searches the interview test session details.
        /// </summary>
        /// <param name="testSessionCriteria">The test session criteria.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="sortingKey">The sorting key.</param>
        /// <param name="sortByDirection">The sort by direction.</param>
        /// <returns></returns>
        public List<InterviewSessionDetail> SearchInterviewTestSessionDetails(TestSessionSearchCriteria testSessionCriteria,
         int pageNumber, int pageSize, out int totalRecords, string sortingKey, SortType sortByDirection)
        {
            return new InterviewSchedulerDLManager().SearchInterviewTestSessionDetails(testSessionCriteria, 
                pageNumber, pageSize, out totalRecords, sortingKey, sortByDirection);
        }

        /// <summary>
        /// Searches the assessor details.
        /// </summary>
        /// <param name="searchAssessor">The search assessor.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="sortingKey">The sorting key.</param>
        /// <param name="sortByDirection">The sort by direction.</param>
        /// <returns></returns>
        public List<TestSessionSearchCriteria> SearchAssessorDetails(TestSessionSearchCriteria searchAssessor,
           int pageNumber, int pageSize, out int totalRecords, string sortingKey, SortType sortByDirection)
        {
            return new InterviewSchedulerDLManager().SearchAssessorDetails(searchAssessor, pageNumber, pageSize,
                out totalRecords, sortingKey, sortByDirection);
        }
        /// <summary>
        /// Method that will return the test session and candidate interview session details
        /// based on the search criteria.
        /// </summary>
        /// <param name="scheduleCandidateSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search criteria details.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that contains the sort expression.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="SortType"/> that contains the sort type either ASC/DESC.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewSessionDetail"/> that contains the interview session details information.
        /// </returns>
        public InterviewSessionDetail GetInteviewSessionDetail(string interviewSessionID,string candidateSessionID,
            string sortExpression, SortType sortDirection)
        {
            return new InterviewSchedulerDLManager().GetInteviewSessionDetail(interviewSessionID,candidateSessionID,
                sortExpression, sortDirection);
        }

        /// <summary>
        /// Method that will call whenever the user requests to schedule/reschedule a candidate.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the test schedule details.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the mail sent status.
        /// </param>
        public void ScheduleCandidate(TestScheduleDetail testScheduleDetail,List<AssessorDetail> assessorDetails, 
             int modifiedBy, out bool isMailSent)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                int assessorId = 0;
                string assessorSkillIds = string.Empty;

                if (assessorDetails != null && assessorDetails.Count > 0 && testScheduleDetail.IsRescheduled == false)
                    testScheduleDetail.DefaultAssessor = "N";
                else
                    testScheduleDetail.DefaultAssessor = "Y";

                new InterviewSchedulerDLManager().ScheduleCandidate(testScheduleDetail, modifiedBy, transaction);
                //Insert skill id based on the assessor id

                if (assessorDetails != null && assessorDetails.Count > 0 && testScheduleDetail.IsRescheduled == false)
                {
                    foreach (AssessorDetail assessorDetail in assessorDetails)
                    {
                        assessorId = assessorDetail.UserID;
                        assessorSkillIds = assessorDetail.Skill.ToString();
                        if (assessorSkillIds != string.Empty)
                            new InterviewSchedulerDLManager().InsertInterviewSessionAssessorSkillForCandidate(
                                testScheduleDetail.CandidateTestSessionID, assessorId, testScheduleDetail.AttemptID,
                                assessorSkillIds, modifiedBy, transaction);
                    }
                }
                if (testScheduleDetail.IsRescheduled == true)
                {
                    try
                    {
                        // Send email to the candidate who is requested to reschedule
                        new EmailHandler().SendMail(EntityType.InterviewCandidateReScheduled,
                            testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString(),2);
                        isMailSent = true;
                    }
                    catch (Exception exp)
                    {
                        isMailSent = false;
                    }
                }
                else
                {
                    int positionProfileID = new PositionProfileDLManager().GetPositionProfileID(testScheduleDetail.CandidateTestSessionID, "I");
                    if (positionProfileID > 0)
                    {
                        PositionProfileCandidate positionProfileCandidate = new PositionProfileCandidate();
                        positionProfileCandidate.PositionProfileID = positionProfileID;
                        positionProfileCandidate.CandidateID = int.Parse(testScheduleDetail.CandidateID);
                        positionProfileCandidate.CandidateSessionID = testScheduleDetail.CandidateTestSessionID;
                        positionProfileCandidate.Status = testScheduleDetail.ScheduleStatus.ToString();
                        positionProfileCandidate.PickedBy = null;
                        positionProfileCandidate.PickedDate = null;
                        positionProfileCandidate.SchelduleDate = testScheduleDetail.ExpiryDate;
                        positionProfileCandidate.ResumeScore = null;
                        positionProfileCandidate.AttemptID = testScheduleDetail.AttemptID;
                        positionProfileCandidate.SourceFrom = "N";
                        positionProfileCandidate.CreatedBy = modifiedBy;
                        new TestDLManager().InsertPositionProfileCandidate(positionProfileCandidate, "I", transaction);
                        //if (testScheduleDetail.CandidateID != null && testScheduleDetail.CandidateID.Trim().Length > 0)
                        //{
                        //    new CommonBLManager().InsertCandidateActivityLog
                        //        (0, Convert.ToInt32(testScheduleDetail.CandidateID), "Position profile associated to candidate", modifiedBy,
                        //        Constants.CandidateActivityLogType.CANDIDATE_ASSOCIATED_POSITION_PROFILE);
                        //}
                    }
                    try
                    {
                        // Send email to the candidate who is requested to schedule.
                        new SmptEmailHandler().SendMail(EntityType.InterviewCandidateScheduled,
                            testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString());
                        isMailSent = true;
                    }
                    catch (Exception exp)
                    {
                        isMailSent = false;
                    }
                }
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                Logger.ExceptionLog(exp);
                isMailSent = false;
                throw exp;

            }
        }

        /// <summary>
        /// Represents the method to check whether the same candidate
        /// has been assigned for the test session ID
        /// </summary>
        /// <param name="testSessionID">A
        /// <see cref="string"/>that holds the test session ID
        /// </param>
        /// <param name="candidateID">
        /// A<see cref="string"/>that holds the candidate ID
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds  candidate already assigned scheduler name
        /// </returns>
        public string CheckCandidateAlreadyAssigned(string inteviewSessionID, string candidateID)
        {
            return new InterviewSchedulerDLManager().CheckCandidateAlreadyAssigned(inteviewSessionID, candidateID);
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that contains the candidate session id.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate session details.
        /// </returns>
        public CandidateInterviewSessionDetail GetCandidateInterviewSession(string candidateInterviewSessionID, int attemptID)
        {
            return new InterviewSchedulerDLManager().GetCandidateInterviewSession(candidateInterviewSessionID, attemptID);
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID and attempt ID.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the candidate
        /// interview session details.
        /// </returns>
        public CandidateInterviewSessionDetail GetCandidateInterviewSessionDetail
            (string candidateTestSessionID, int attemptID)
        {
            return new InterviewSchedulerDLManager().GetCandidateInterviewSessionDetail
                (candidateTestSessionID, attemptID);
        }

        /// <summary>
        /// Method that will call to load the candidate details 
        /// who scheduled for the tests.
        /// </summary>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/>that contains the current page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the candidate Information Like Name...
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column to be sorted.
        /// </param>
        /// <param name="orderDirection">
        /// A <see cref="SortType"/> that contains the sort direction either Asc/Desc.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestScheduleDetail"/> that contains candidate details.
        /// </returns>
        public List<TestScheduleDetail> GetInterviewScheduler
            (int user, int pageNumber, int pageSize, out int totalRecords,
            TestScheduleSearchCriteria scheduleSearchCriteria, string orderBy, SortType orderDirection)
        {
            return new InterviewSchedulerDLManager().GetInterviewScheduler
                (user, pageNumber, pageSize, out totalRecords,
                scheduleSearchCriteria, orderBy, orderDirection);
        }

        /// <summary>
        /// Method that will return test session and candidates session details
        /// for the given test session id or candidate session id/s.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the test session id.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate session ids 
        /// separated by comma.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the test session detail 
        /// with the list of CandidateTestSessionDetail instances.
        /// </returns>
        public InterviewTestSessionDetail GetInterviewTestSessionDetail(string testSessionID, string candidateSessionIDs)
        {
            return new InterviewSchedulerDLManager().GetInterviewTestSessionDetail(testSessionID, candidateSessionIDs);
        }

        /// <summary>
        /// Method that will retrieves the interview session and associated 
        /// candidate sessions for the given parameters.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the interview session ID.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate session IDs 
        /// separated by comma.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewSessionDetail"/> that contains the interview 
        /// session and associated candidate sessions.
        /// </returns>
        public InterviewSessionDetail GetInterviewTestSessionDetailScheduler(string testSessionID, string candidateSessionIDs)
        {
            return new InterviewSchedulerDLManager().GetInterviewTestSessionDetailScheduler(testSessionID, candidateSessionIDs);
        }


        /// <summary>
        /// Method that retrieves the list of interview reminder sender list 
        /// for the given date/time and relative time differences. Mails needs 
        /// to be sent for this list.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<InterviewReminderDetail> GetInterviewReminderSenderList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            return new InterviewSchedulerDLManager().GetInterviewReminderSenderList
                (currentDateTime, relativeTimeDifference);
        }

        /// <summary>
        /// Method that will update the interview reminder sent status for the 
        /// given candidate, attempt number and interval ID.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="intervalID">
        /// A <see cref="string"/> that holds the interval ID.
        /// </param>
        /// <param name="reminderSent">
        /// A <see cref="bool"/> that holds the reminder sent status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateInterviewReminderStatus(string candidateSessionID,
            int attemptID, string intervalID, bool reminderSent, int userID)
        {
            new InterviewSchedulerDLManager().UpdateInterviewReminderStatus
                (candidateSessionID, attemptID, intervalID,
                reminderSent, userID);
        }

        /// <summary>
        /// Method that will call whenever the user requests to schedule/reschedule a candidate.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the test schedule details.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the mail sent status.
        /// </param>
        public void InterviewScheduleCandidate(TestScheduleDetail testScheduleDetail, int modifiedBy, out bool isMailSent)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                new InterviewSchedulerDLManager().InterviewScheduleCandidate(testScheduleDetail, modifiedBy, transaction);
                if (testScheduleDetail.IsRescheduled == true)
                {
                    try
                    {
                        // Send email to the candidate who is requested to reschedule
                        new EmailHandler().SendMail(EntityType.InterviewCandidateReScheduled,
                            testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString());
                        isMailSent = true;
                    }
                    catch (Exception exp)
                    {
                        isMailSent = false;
                    }
                }
                else
                {
                    int positionProfileID = new PositionProfileDLManager().GetInterviewPositionProfileID(testScheduleDetail.CandidateTestSessionID);
                    if (positionProfileID > 0)
                    {
                        PositionProfileCandidate positionProfileCandidate = new PositionProfileCandidate();
                        positionProfileCandidate.PositionProfileID = positionProfileID;
                        positionProfileCandidate.CandidateID = int.Parse(testScheduleDetail.CandidateID);
                        positionProfileCandidate.CandidateSessionID = testScheduleDetail.CandidateTestSessionID;
                        positionProfileCandidate.Status = testScheduleDetail.ScheduleStatus.ToString();
                        positionProfileCandidate.PickedBy = null;
                        positionProfileCandidate.PickedDate = null;
                        positionProfileCandidate.SchelduleDate = testScheduleDetail.ExpiryDate;
                        positionProfileCandidate.ResumeScore = null;
                        positionProfileCandidate.AttemptID = testScheduleDetail.AttemptID;
                        positionProfileCandidate.SourceFrom = "N";
                        positionProfileCandidate.CreatedBy = modifiedBy;
                        new TestDLManager().InsertPositionProfileCandidate(positionProfileCandidate, "I", transaction);
                        //if (testScheduleDetail.CandidateID != null && testScheduleDetail.CandidateID.Trim().Length > 0)
                        //{
                        //    new CommonBLManager().InsertCandidateActivityLog
                        //        (0, Convert.ToInt32(testScheduleDetail.CandidateID), "Position profile associated to candidate", modifiedBy,
                        //        Constants.CandidateActivityLogType.CANDIDATE_ASSOCIATED_POSITION_PROFILE);
                        //}
                    }
                    try
                    {
                        // Send email to the candidate who is requested to schedule.
                        new EmailHandler().SendMail(EntityType.InterviewCandidateScheduled,
                            testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString());
                        isMailSent = true;
                    }
                    catch (Exception exp)
                    {
                        isMailSent = false;
                    }
                }
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                Logger.ExceptionLog(exp);
                isMailSent = false;
                throw exp;

            }
        }

        /// <summary>
        /// Method that will update the candidate session status if he is unscheduled.
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate session detail.
        /// </param>
        /// <param name="candidateAttemptStatus">
        /// A <see cref="string"/> that contains the candidate attempt status.
        /// </param>
        /// <param name="candidateSessionStatus">
        /// A <see cref="string"/> that contains the candidate session status.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="isUnscheduled">
        /// A <see cref="bool"/> that contains the unscheduled status.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the mail sent status.
        /// </param>
        public void UpdateInterviewSessionStatus(CandidateInterviewSessionDetail candidateInterviewSessionDetail,
            string candidateAttemptStatus, string candidateSessionStatus, int modifiedBy,
            bool isUnscheduled, out bool isMailSent)
        {
            new InterviewSchedulerDLManager().UpdateSessionStatus(candidateInterviewSessionDetail,
                candidateAttemptStatus, candidateSessionStatus, modifiedBy);
            isMailSent = false;

            try
            {
                // If the author unschedule a candidate, the mail should be sent to the
                // corresponding candidate.
                if (isUnscheduled)
                {
                    // Send email to the candidate who is requested to reschedule
                    new EmailHandler().SendMail
                        (EntityType.InterviewCandidateUnScheduled, candidateInterviewSessionDetail);

                    // Mail sent successfully.
                    isMailSent = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                isMailSent = false;
            }
        }

        /// <summary>
        /// Method that will update the test status for the given candidate session id
        /// and attempt id.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attemptId">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <param name="sessionTrackingStatus">
        /// A <see cref="string"/> that contains the session tracking status.
        /// </param>
        /// <param name="sessionCandidateStatus">
        /// A <see cref="string"/> that contains the candidate session status.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the mail sent status.
        /// </param>
        public void UpdateInterviewSessionStatus(string candidateSessionKey, int attemptId,
            string sessionTrackingStatus, string sessionCandidateStatus, int modifiedBy, out bool isMailSent)
        {
            new InterviewSchedulerDLManager().UpdateSessionStatus(candidateSessionKey, attemptId,
                sessionTrackingStatus, sessionCandidateStatus, modifiedBy);

            // Mail sent successfully.
            isMailSent = true;
        }

        /// <summary>
        /// Method to insert the assessor and skill against the candidate session key
        /// </summary>
        /// <param name="assessorDetails"></param>
        /// <param name="candidateSessionKey"></param>
        /// <param name="userID"></param>
        /// <param name="attemptId"></param>
        public void SaveInterviewSessionAssessorSkillForCandidate(List<AssessorDetail> assessorDetails, 
            string candidateSessionKey, int attemptId,int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            InterviewSchedulerDLManager interviewSchedulerDLManager = new InterviewSchedulerDLManager();
            NextNumberDLManager nextNumberDLManager = new NextNumberDLManager();

            int assessorId = 0;
            string assessorSkillIds = string.Empty;
            string assessorIds = string.Empty;
            try
            {
                //Insert skill id based on the assessor id
                foreach (AssessorDetail assessorDetail in assessorDetails)
                {
                    if (!Utility.IsNullOrEmpty(assessorDetail.Skill))
                    {
                        assessorId = assessorDetail.UserID;
                        assessorSkillIds = assessorDetail.Skill.ToString();
                        interviewSchedulerDLManager.UpdateInterviewSessionAssessorAndSkill(candidateSessionKey,
                            assessorId, attemptId, assessorSkillIds, userID, transaction);
                        
                        assessorIds = assessorIds + assessorId + ",";
                    }
                }
                if (!Utility.IsNullOrEmpty(assessorIds))
                {
                    //Delete assessor and skill if any existing assessor removed from the matrix(against candidate session key)
                    assessorIds = assessorIds.ToString().Trim().TrimEnd(',');
                    interviewSchedulerDLManager.DeleteInterviewCandidateSessionAssessorSkill(candidateSessionKey, assessorIds, attemptId, transaction);
                }

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
    }
}
