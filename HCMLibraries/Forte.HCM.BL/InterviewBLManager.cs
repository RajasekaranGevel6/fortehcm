﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewBLManager.cs
// File that represents the data layer for the Test respository Manager.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives
namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Test respository management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for Test. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// 
    public class InterviewBLManager
    {
        /// <summary>
        /// Method that updates the copy test status. If questions are same for
        /// both parent and child test, then status and parent test key needs 
        /// to be updated.
        /// </summary>
        /// <param name="parentTestKey">
        /// A <see cref="string"/> that holds the parent test key.
        /// </param>
        /// <param name="childTestKey">
        /// A <see cref="string"/> that holds the child test key.
        /// </param>
        public void UpdateCopyTestStatus(string parentTestKey, string childTestKey)
        {
            new TestDLManager().UpdateCopyTestStatus(parentTestKey, childTestKey);
        }

        /// <summary>
        /// Method that save the interview test detail and its attributes
        /// </summary>
        /// <param name="testDetail"></param>
        /// <param name="userID"></param>
        /// <param name="complexity"></param>
        /// <returns></returns>
        public string SaveInterviewTest(TestDetail testDetail, int userID, string complexity)
        {
            string testKey = "";
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                testKey = new NextNumberDLManager().GetNextNumber(transaction, "ITT", userID);
                testDetail.TestKey = testKey;
                new InterviewDLManager().InsertInterviewTest(testDetail, userID, complexity, transaction);

                // Insert Questions. 
                foreach (QuestionDetail questionDetail in testDetail.Questions)
                {
                    new InterviewDLManager().InsertInterviewTestQuestions(testKey, questionDetail.QuestionKey,
                        questionDetail.TimeLimit,
                        questionDetail.QuestionRelationId, userID, transaction);
                    new InterviewDLManager().InsertInterviewTestQuestionsAttributes(testDetail.TestKey,
                        questionDetail.QuestionKey, questionDetail.TimeLimit, questionDetail.InterviewTestRatings,
                        questionDetail.Weightage, questionDetail.InterviewTestQuestionComments,
                        userID, transaction);
                }
                if (testDetail.IsCertification == true)
                {
                    // new TestDLManager().InsertTestCertification(testID, questionDetail.QuestionKey, questionDetail.QuestionRelationId, userID, transaction);
                }
                transaction.Commit();
                return testKey;
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        public string SaveInterviewTestWithRatings(TestDetail testDetail, int userID, string complexity, List<QuestionDetail> questionDetail)
        {
            string testKey = "";
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                testKey = new NextNumberDLManager().GetNextNumber(transaction, "ITT", userID);
                testDetail.TestKey = testKey;
                new InterviewDLManager().InsertInterviewTest(testDetail, userID, complexity, transaction);

                // Insert Questions. 
                for (int i = 0; i < questionDetail.Count; i++)
                {
                    QuestionDetail questionDetail_list = new QuestionDetail();
                    questionDetail_list = questionDetail[i];
                    new InterviewDLManager().InsertInterviewTestQuestions(testKey, questionDetail_list.QuestionKey,
                        questionDetail_list.TimeLimit, questionDetail_list.QuestionRelationId,
                        userID, transaction);
                    new InterviewDLManager().InsertInterviewTestQuestionsAttributes(testKey, questionDetail_list.QuestionKey,
                        questionDetail_list.TimeLimit, questionDetail_list.InterviewTestRatings,
                        questionDetail_list.Weightage,
                        questionDetail_list.InterviewTestQuestionComments, userID, transaction);
                }
                if (testDetail.IsCertification == true)
                {
                    // new TestDLManager().InsertTestCertification(testID, questionDetail.QuestionKey, questionDetail.QuestionRelationId, userID, transaction);
                }
                transaction.Commit();
                return testKey;
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that updates the copy test status. If questions are same for
        /// both parent and child test, then status and parent test key needs 
        /// to be updated.
        /// </summary>
        /// <param name="parentTestKey">
        /// A <see cref="string"/> that holds the parent test key.
        /// </param>
        /// <param name="childTestKey">
        /// A <see cref="string"/> that holds the child test key.
        /// </param>
        public void UpdateCopyInterviewTestStatus(string parentTestKey, string childTestKey)
        {
            new InterviewDLManager().UpdateCopyInterviewTestStatus(parentTestKey, childTestKey);
        }

        public List<AssessorDetail> GetInterviewTestAssessorDetail(string testSessionID, int userID)
        {
            return new InterviewDLManager().GetInterviewTestAssessorDetail(testSessionID, userID);
        }

        /// <summary>
        /// This method Communicates with the Test DL Manager to get the 
        /// test session records from the DB
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A list of <see cref="TestSearchCriteria"/> that holds the TestKey,Category,Subject,etc..
        /// </param>
        /// /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="OrderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of interview test details</returns>
        public List<TestDetail> GetInterviewsForInterviewSession(TestSearchCriteria testSearchCriteria,
            int PageNumber, int PageSize, string OrderBy, SortType direction, out int TotalNoofRecords)
        {
            return new InterviewDLManager().GetInterviewsForInterviewSession(testSearchCriteria, 
                PageNumber, PageSize, OrderBy, direction, out TotalNoofRecords);
        }

        /// <summary>
        /// Method that will cancel the interview test session for a particular candidate.
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate 
        /// interview session information.
        /// </param>
        public void CancelInterviewTestSession(CandidateTestSessionDetail candidateTestSessionDetail)
        {
            new InterviewDLManager().CancelInterviewTestSession(candidateTestSessionDetail);
        }

        /// <summary>
        /// Method that will load the interview test inclusion details are 
        /// associated with the question. 
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that contains information based on the 
        /// question key.
        /// </returns>
        public List<TestDetail> GetInterviewTestInclusionDetail
            (string questionKey, int pageNumber, int pageSize, string orderBy,
            SortType orderByDirection, out int totalRecords)
        {
            return new InterviewDLManager().GetInterviewTestInclusionDetail
                (questionKey, pageNumber, pageSize, orderBy, 
                orderByDirection, out totalRecords);
        }

        /// <summary>
        /// Gets Interview test Details
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewTestDetail"/> that contains the Test Detail.
        /// </returns>
        public TestDetail GetInterviewTestDetail(string testID)
        {
            return new InterviewDLManager().GetInterviewTestDetail(testID);
        }

       

        /// <summary>
        /// Method that retrieves the list of Questions for the given search 
        /// </summary>
        /// <param name="searchCriteria">
        /// A list of <see cref="QuestionDetailSearchCriteria"/> that holds the questionKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the QuestionDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<QuestionDetail> GetInterviewSearchQuestions(QuestionDetailSearchCriteria searchCriteria, int pageSize, int pageNumber, string orderBy, string direction, out int totalRecords)
        {
            return new InterviewDLManager().GetInterviewSearchQuestions(searchCriteria, pageSize, pageNumber, orderBy, direction, out totalRecords);
        }

        /// <summary>
        /// This method is used to Active based on the interview test key.
        /// </summary>
        /// <param name="interviewTestKey">
        ///  A <see cref="string"/> that holds the interviewTestKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void ActivateInterviewTest(string interviewTestKey, int user)
        {
            new InterviewDLManager().UpdateInterviewStatus(interviewTestKey, "Y", user);
        }

        /// <summary>
        /// This method is used to DeactivateInterviewTest based on the interviewTestKey.
        /// </summary>
        /// <param name="interviewTestKey">
        ///  A <see cref="string"/> that holds the interviewTestKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeactivateInterviewTest(string interviewTestKey, int user)
        {
            new InterviewDLManager().UpdateInterviewStatus(interviewTestKey, "N", user);
        }

        /// <summary>
        /// This method is used to Delete Interview Test based on the interviewTestKey.
        /// </summary>
        /// <param name="interviewTestKey">
        ///  A <see cref="string"/> that holds the interviewTestKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeleteInterviewTest(string interviewTestKey, int user)
        {
            new InterviewDLManager().DeleteInterviewTest(interviewTestKey, user);
        }

        /// <summary>
        /// Returns question attribute object
        /// </summary>
        /// <param name="interviewTestKey">Contains interview test key</param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that holds the sort expression
        /// </param>
        /// <returns></returns>
        public List<QuestionDetail> GetInterviewTestQuestionAttributes(string interviewTestKey)
        {
            return new InterviewDLManager().GetInterviewTestQuestionAttributes(interviewTestKey);
        }

        /// <summary>
        /// This method handles the Edit Interview Test functions like test And test Question.
        /// </summary>
        /// <param name="testDetail">
        /// A <see cref="TestDetail"/> that contains the Test detail collection.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creater of the Test.
        /// </param>
        /// <param name="complexity">
        /// A <see cref="string"/>that contains the complexity Atribute
        /// </param>
        /// <param name="deletedQuestionKey">
        /// A <see cref="string"/>that contains the deleted Question Keys
        /// </param>
        /// <param name="insertedQuestionKey">
        /// A <see cref="string"/>that contains the inserted Question Keys
        /// </param>
        public void UpdateInterviewTest(TestDetail testDetail, int userID,
            string complexity, bool isReviewQuestion)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // If existing interview test.
                new InterviewDLManager().UpdateInterviewTest(testDetail, userID, complexity, transaction);

                if (!Utility.IsNullOrEmpty(testDetail.TestKey))
                    new InterviewDLManager().DeleteInterviewTestQuestion(testDetail.TestKey, transaction);

                if (isReviewQuestion)
                {
                    if (!Utility.IsNullOrEmpty(testDetail.TestKey))
                        new InterviewDLManager().DeleteInterviewTestQuestionAttributes(testDetail.TestKey, transaction);
                }
                foreach (QuestionDetail questionDetail in testDetail.Questions)
                {
                    new InterviewDLManager().InsertInterviewTestQuestions(testDetail.TestKey,
                         questionDetail.QuestionKey, questionDetail.TimeLimit,
                         questionDetail.QuestionRelationId, userID, transaction);

                    if (isReviewQuestion)
                        new InterviewDLManager().InsertInterviewTestQuestionsAttributes(testDetail.TestKey,
                        questionDetail.QuestionKey, questionDetail.TimeLimit,
                        questionDetail.InterviewTestRatings,
                        questionDetail.Weightage,
                        questionDetail.InterviewTestQuestionComments,
                        userID, transaction);
                }
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testKey"></param>
        /// <returns></returns>
        public TestDetail GetInterviewTestDetailView(string testKey)
        {
            return new InterviewDLManager().GetInterviewTestDetailView(testKey);
        }

        /// <summary>
        /// To get the adaptive interview questions.
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="int"/> that holds the UserId 
        /// </param>
        /// <param name="SelectedQuestions">
        /// A <see cref="string"/> that holds the user selected questions.
        /// (User moved to Test Draft Panel) 
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size 
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number 
        /// </param>
        /// <param name="TotalNoOfRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// </param>
        /// <returns>
        /// A List for<see cref="QuestionDetail"/> List of interview question details 
        /// </returns>
        public List<QuestionDetail> GetAdaptiveInterviewQuestions(int tenantId, int UserId, string SelectedQuestions,
            int pageSize, int pageNumber, int questionAuthor, out int TotalNoOfRecords)
        {
            return new InterviewDLManager().GetAdaptiveInterviewQuestions(tenantId, UserId,
                SelectedQuestions, pageSize, pageNumber, questionAuthor, out TotalNoOfRecords);
        }

       

        /// <summary>
        /// Method that retrieves the list of interview Tests for the given search 
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A list of <see cref="TestSearchCriteria"/> that holds the TestKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that holds the TestDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<TestDetail> GetInterviewTests(TestSearchCriteria testSearchCriteria, int pageSize, int pageNumber, string orderBy, SortType direction, out int totalRecords)
        {
            return new InterviewDLManager().GetInterviewTests(testSearchCriteria, pageSize, pageNumber, orderBy, direction, out totalRecords);
        }

        public InterviewDetail GetViewInterviewTestDetail(string testKey)
        {
            return new InterviewDLManager().GetViewInterviewTestDetail(testKey);
        }

        public List<QuestionDetail> GetInterviewTestQuestionDetail(string testKey, string sortExpression)
        {
            return new InterviewDLManager().GetInterviewTestQuestionDetail(testKey, sortExpression);
        }

    
        /// <summary>
        /// Return category id for subject or subject not there return general id
        /// </summary>
        /// <see cref="string"/> this holds the assessor new/existing skill
        /// <param name="skill"></param>
        /// <returns></returns>
        public DataTable getSubjectCategoryID(string skill)
        {
            return new InterviewDLManager().getSubjectCategoryID(skill);        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="AssessorSkill_Table"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int InsertAssessorSkill(int UserID, DataTable
            AssessorSkill_Table, IDbTransaction transaction)
        {   
            return new InterviewDLManager().InsertAssosserSKill(
                UserID,AssessorSkill_Table,transaction);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public DataTable getAssessorSkills(int UserID)
        {
            return new InterviewDLManager().getAssessorSkills(UserID);

        } 

    } 

}