﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestRecommendationBLManager.cs
// File that represents the data layer for the Test Recommendation respository Manager.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.BL
{
    #region Public Methods
    /// <summary>                                                  
    /// Class that represents the data layer for the Test Recommendation management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for Test. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// 
    public class TestRecommendationBLManager
    {
        /// <summary>
        /// This method handles the new recommendation entry.
        /// </summary>
        /// <param name="testRecommendationDetail">that contains Test Recommendation collection</param>
        /// <param name="dtTestSearchCriteria">that contains segments collections as datatable</param>
        /// <param name="userId">that contains Test Creater ID</param>
        public void SaveTestSegments(TestRecommendationDetail testRecommendationDetail,
             DataTable dtTestSearchCriteria, int userId)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                // Check if test recommendation ID is present or not.
                if (testRecommendationDetail.TestRecommendId == 0)
                {
                    // Insert test detail.
                    testRecommendationDetail.TestRecommendId = new TestRecommendationDLManager().
                        InsertTestSegmentDetail(testRecommendationDetail, transaction);
                }
                else
                {
                    // Update test detail.
                    new TestRecommendationDLManager().
                        UpdateTestSegmentDetail(testRecommendationDetail, transaction);

                    // Delete existing test segments (criteria).
                    new TestRecommendationDLManager().DeleteTestSegmentSkills
                        (testRecommendationDetail.TestRecommendId, transaction);
                }

                // Loop through the test segments (criteria) and construct the table.
                foreach (DataRow row in dtTestSearchCriteria.Rows)
                {
                    testRecommendationDetail.SubjectId = (string.IsNullOrEmpty(row["Cat_Sub_Id"].ToString()))
                        ? 0 : Convert.ToInt32(row["Cat_Sub_Id"].ToString());
                    testRecommendationDetail.Complexity = row["Complexity"].ToString();
                    testRecommendationDetail.TestArea = row["TestArea"].ToString();
                    testRecommendationDetail.Weightage = (string.IsNullOrEmpty(row["Weightage"].ToString()))
                        ? 0 : Convert.ToInt32(row["Weightage"].ToString());
                    testRecommendationDetail.Keyword = row["Keyword"].ToString();

                    // Insert test segments (criteria).
                    new TestRecommendationDLManager().
                        InsertTestSegmentSkill(testRecommendationDetail, userId, transaction);
                }

                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the list of self admin test recommendations for the 
        /// given search parameters.
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="TestRecommendationDetailSearchCriteria"/> that holds the search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestRecommendationDetail"/> that holds the self 
        /// admin test recommendations.
        /// </returns>
        public List<TestRecommendationDetail> GetSelfAdminTestRecommendations
            (TestRecommendationDetailSearchCriteria criteria, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new TestRecommendationDLManager().GetSelfAdminTestRecommendations
                (criteria, pageNumber, pageSize, sortField, sordOrder, out totalRecords);
        }

        /// <summary>
        /// Method that updates the self admin test recommendation status
        /// (either active or inactive).
        /// </summary>
        /// <param name="testRecommendationID">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateSelfAdminTestRecommendationStatus(int testRecommendationID,
            string status, int userID)
        {
            new TestRecommendationDLManager().UpdateSelfAdminTestRecommendationStatus
                (testRecommendationID, status, userID);
        }


        /// <summary>
        /// Method that retrieves the recommended test and criteria segments for the 
        /// given test recommendation ID.
        /// </summary>
        /// <param name="testRecommendation">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestRecommendationDetail"/> that holds test and criteria segment
        /// details.
        /// </returns>
        public TestRecommendationDetail GetTestSegmentDetail(int testRecommendation)
        {
            return new TestRecommendationDLManager().GetTestSegmentDetail(testRecommendation);
        }


        /// <summary>
        /// Method that retrieves the list of candidates associated with a self admin
		/// test for the given search parameters
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="TestRecommendationDetailSearchCriteria"/> that holds the search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestRecommendationDetail"/> that holds the candidate details.
        /// </returns>
        public List<TestRecommendationDetail> GetSelfAdminTestCandidates
            (TestRecommendationDetailSearchCriteria criteria, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new TestRecommendationDLManager().GetSelfAdminTestCandidates
                (criteria, pageNumber, pageSize, sortField, sordOrder, out totalRecords);
        }

        #endregion Public Methods
    }
}
