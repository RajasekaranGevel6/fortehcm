﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// NomenclatureBLManager.cs
// File that represents the business layer for the nomenclature customization
// feature. This will talk to the data layer to perform the operations
// associated with this feature.

#endregion Header

#region Directives                                                             

using System.Collections.Generic;

using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>
    /// Class that represents the business layer for the nomenclature
    /// customization eature. This will talk to the data layer to perform the 
    /// operations associated with this feature
    /// </summary>
    public class NomenClatureBLManager
    {
        #region Public Methods                                                 

        /// <summary>
        /// Method that inserts the nomenclature details.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="data">
        /// A <see cref="string"/> that holds the data as XML.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertNomenClature(int tenantID, string clatureDetails, int userID)
        {
            new NomenClatureDLManager().InsertNomenclatureDetail(tenantID, clatureDetails, userID);
        }

        /// <summary>
        /// Method that retrieves the nomenclature customization attributes for
        /// the given tenant ID.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="NomenclatureCustomize"/> that holds the
        /// nomenclature customization attributes.
        /// </returns>
        public List<NomenclatureCustomize> GetAttributes(int tenantID)
        {
            return new NomenClatureDLManager().GetAttributes(tenantID);
        }

        /// <summary>
        /// Method that retrieves the nomenclature customization role 
        /// attributes for the given tenant ID.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="NomenclatureCustomize"/> that holds the
        /// nomenclature customization role attributes.
        /// </returns>
        public List<NomenclatureCustomize> GetRoleAttributes(int tenantID)
        {
            return new NomenClatureDLManager().GetRoleAttributes(tenantID);
        }

        /// <summary>
        /// Method that inserts the role attributes.
        /// </summary>
        /// <param name="roleArttributes">
        /// A list of <see cref="NomenclatureCustomize"/> that holds the role
        /// attributes.
        /// </param>
        public void InsertRolesAttribute(List<NomenclatureCustomize> roleArttribute)
        {
            new NomenClatureDLManager().InsertRoleAttributes(roleArttribute);
        }

        #endregion Public Methods
    }
}
    