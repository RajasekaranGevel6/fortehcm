﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.

// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestConductionBLManager.cs
// File that represents the business layer for the Test Conduction module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

#region Directives                                                             
using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the Test Conduction module.
    /// This includes functionalities for updating the test result , test status 
    ///  This will talk to the data layer for performing these operations.
    /// </summary>
    public class TestConductionBLManager
    {
        #region Public Method                                                  
        /// <summary>
        /// This method is used update the CANDIDATE_TEST_RESULT_FACT table.
        /// </summary>
        /// <param name="candidateTestResultFact">
        /// This parameter contains all the details like
        /// Start_Time, End_Time, Status etc
        /// </param>
        /// <param name="submittedAnswer">
        /// Submitted answer by candidate is passed to DB to compare with correct answer
        /// </param>
        public void UpdateCandidateTestResultFact(CandidateTestResultFact candidateTestResultFact, int submittedAnswer, string answer)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                new TestConductionDLManager().UpdateCandidateTestResultFact(candidateTestResultFact, submittedAnswer, answer);
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        /// <summary>
        /// Method that adds the image details into the database.
        /// </summary>
        /// <param name="image">
        /// An array of <see cref="byte"/> that holds the image.
        /// </param>
        /// <param name="sessionInfo">
        /// A <see cref="string"/> that holds the session info.
        /// </param>
        /// <returns>
        /// <param name="proctorType">
        /// A <see cref="short"/> that holds the proctor type.
        /// </param>
        /// A <see cref="short"/> that holds the status.
        /// </returns>
        public void AddImageDetails(string canSessionKey, int attemptID, string imageType, byte[] image, int createdBy, int modifiedBy, out Int64 CP_IMAGE_ID)
        {
            new TestConductionDLManager().AddImageDetails(canSessionKey,
               attemptID, imageType, image, createdBy, modifiedBy, out CP_IMAGE_ID);
        }

        
        /// <summary>
        /// This method is used update the CANDIDATE_TEST_RESULT table.
        /// </summary>
        /// <param name="candidateTestResult">
        /// This parameter contains all the details like
        /// Total_Questions_Viewed, Total_Questions_Not_Viewed, Status etc
        /// </param>
       
        public void UpdateCandidateTestResult(CandidateTestResult candidateTestResult)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                new TestConductionDLManager().UpdateCandidateTestResult(candidateTestResult);
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        /// <summary>
        /// This method is used to get the Test_Question_Id and QuestionKey details
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate Session Key.
        /// </param>
        /// <returns>
        /// A Dictionary that contains the question Ids and test question id.
        /// </returns>
        public Dictionary<string, string> GetQuestionIDs(string candidateSessionKey)
        {
            return new TestConductionDLManager().GetQuestionIDs(candidateSessionKey);
        }

        /// <summary>
        /// This method is used to get the Question details by passing QuestionKey
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        /// <returns>
        /// A <see cref="QuestionDetail"/> that contains the Question Detail.
        /// </returns>
        public QuestionDetail GetQuestionByQuestionKey(string questionKey)
        {
            return new TestConductionDLManager().GetQuestionByQuestionKey(questionKey);
        }

        /// <summary>
        /// Method that will insert the certification image.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <param name="imageData">
        /// that contains the image data as byte.
        /// </param>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        public void InsertCertificateImage(string candidateSessionKey, int attemptID, byte[] imageData, int user)
        {
            new TestConductionDLManager().InsertCertificateImage(candidateSessionKey, attemptID, imageData, user);
        }

        /// <summary>
        /// This method stores the test details in DB
        /// </summary>
        /// <param name="candidateTestResult">
        /// A <see cref="CandidateTestResult"/> that contains the Candidate Test Result.
        /// </param>
        public void InsertCandidateTestResult(CandidateTestResult candidateTestResult)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                string candidateResultKey =
                        new NextNumberDLManager().GetNextNumber(transaction, "CTR", candidateTestResult.CreatedBy);

                candidateTestResult.CandidateResultKey = candidateResultKey;

                new TestConductionDLManager().InsertCandidateTestResult(candidateTestResult);
                // Commit the transaction.
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// This method is called when the test is started. 
        /// It passes the inprogress status to store it in DB
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate Session Key.
        /// </param>
        /// <param name="attemptId">
        /// A <see cref="CandidateTestResult"/> that contains the Candidate Test Result.
        /// </param>
        /// <param name="sessionTrackingStatus">
        /// A <see cref="string"/> that contains the values can be ATMPT_INPR
        /// </param>
        /// <param name="sessionCandidateStatus">
        /// A <see cref="string"/> that contains the values can be SESS_INPR
        /// </param>
        /// <param name="modifiedBy">
        /// A <see cref="int"/> that contains the Logged in user id.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the Test Session Detail.
        /// </returns>
        public TestSessionDetail StartTestConduction(string candidateSessionKey, int attemptId, 
            string sessionTrackingStatus, string sessionCandidateStatus, int modifiedBy)
        {
            return new TestConductionDLManager().StartTestConduction(candidateSessionKey, attemptId, sessionTrackingStatus, sessionCandidateStatus, modifiedBy);
        }

        /// <summary>
        /// Method that will update the test status for the given candidate session id
        /// and attempt id.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attemptId">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <param name="sessionTrackingStatus">
        /// A <see cref="string"/> that contains the session tracking status.
        /// </param>
        /// <param name="sessionCandidateStatus">
        /// A <see cref="string"/> that contains the candidate session status.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the mail sent status.
        /// </param>
        public void UpdateSessionStatus(string candidateSessionKey, int attemptId,
            string sessionTrackingStatus, string sessionCandidateStatus, int modifiedBy, out bool isMailSent)
        {
            new TestConductionDLManager().UpdateSessionStatus(candidateSessionKey, attemptId,
                sessionTrackingStatus, sessionCandidateStatus, modifiedBy);

            // Mail sent successfully.
            isMailSent = true;
        }

        /// <summary>
        /// Method that will update the candidate session status if he is unscheduled.
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate session detail.
        /// </param>
        /// <param name="candidateAttemptStatus">
        /// A <see cref="string"/> that contains the candidate attempt status.
        /// </param>
        /// <param name="candidateSessionStatus">
        /// A <see cref="string"/> that contains the candidate session status.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="isUnscheduled">
        /// A <see cref="bool"/> that contains the unscheduled status.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the mail sent status.
        /// </param>
        public void UpdateSessionStatus(CandidateTestSessionDetail candidateTestSessionDetail,
            string candidateAttemptStatus, string candidateSessionStatus, int modifiedBy,
            bool isUnscheduled, out bool isMailSent)
        {
            new TestConductionDLManager().UpdateSessionStatus(candidateTestSessionDetail,
                candidateAttemptStatus, candidateSessionStatus, modifiedBy);
            isMailSent = false;

            try
            {
                // If the author unschedule a candidate, the mail should be sent to the
                // corresponding candidate.
                if (isUnscheduled)
                {
                    // Send email to the candidate who is requested to reschedule
                    new EmailHandler().SendMail
                        (EntityType.CandidateUnScheduled, candidateTestSessionDetail);

                    // Mail sent successfully.
                    isMailSent = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                isMailSent = false;
            }
        }

        /// <summary>
        /// Method that updates the candidate rating.
        /// </summary>
        /// <param name="ratingDetail">
        /// A <see cref="CandidateRatingDetail"/> that contains the candidate rating detail.
        /// </param>
        public void UpdateRating(CandidateRatingDetail ratingDetail)
        {
            new TestConductionDLManager().UpdateRating(ratingDetail);
        }

        #endregion Public Method

        /// <summary>
        /// Method that returns the candidate questiondetails by candidate_sessionKey.
        /// </summary>
        /// <param name="candSessionKey">
        /// A <see cref="candSessionKey"/> that contains the candidate session key.
        /// </param>
        public List<QuestionDetail> GetQuestions(string candSessionKey)
        {
            return new TestConductionDLManager().GetQuestions(candSessionKey);
        }
    }
}