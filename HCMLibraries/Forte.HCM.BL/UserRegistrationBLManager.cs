﻿
#region Directives


using Forte.HCM.DL;
using Forte.HCM.DataObjects;

using System.Data;
using System.Data.Common;
using System.Collections.Generic;


#endregion

namespace Forte.HCM.BL
{
    /// <summary>
    /// 
    /// </summary>
    public class UserRegistrationBLManager
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public UserRegistrationBLManager()
        {
        }

        #endregion Constructor

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="User_Email"></param>
        /// <returns></returns>
        public bool CheckUserEmailExists(string usrEmail, int tenantID)
        {
            return new UserRegistrationDLManager().CheckUserEmailExists(usrEmail, tenantID);
        }

        /// <summary>
        /// A Method that inserts a user to the registration repository
        /// </summary>
        /// <param name="TENANT_USER_ID">
        /// A <see cref="System.Int32"/> that holds the tenant user id
        /// </param>
        /// <param name="userRegistrationInfo">
        /// A <see cref="System.String"/> that holds the user email
        /// A <see cref="System.String"/> that holds the user password
        /// A <see cref="System.String"/> that holds the user first name
        /// A <see cref="System.String"/> that holds the user last name
        /// A <see cref="System.String"/> that holds the user Email
        /// A <see cref="System.String"/> that holds the user phone number
        /// A <see cref="System.String"/> that holds the user company name
        /// A <see cref="System.String"/> that holds the user title
        /// A <see cref="System.Int32"/> that holds the user subscription type id
        /// A <see cref="System.Char"/> that holds the Activation Status
        /// A <see cref="System.Int16"/> that holds the number of users
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <param name="registrationConfiration">
        /// A <see cref="System.Int32"/> that holds the user registration id
        /// A <see cref="System.String"/> that holds the user confirmation code
        /// A <see cref="System.Int32"/> that holds the number of attempts
        /// A <see cref="System.DateTime"/> that holds the verification date
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <param name="transaction">
        /// A <see cref="System.Data.Common.DbTransaction"/> that holds the transaction
        /// object
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of effected
        /// records in the registration Repository
        /// </returns>
        public int InsertUser(int? TENANT_USER_ID, UserRegistrationInfo userRegistrationInfo, RegistrationConfirmation registrationConfiration,
            DbTransaction transaction)
        {
            return new UserRegistrationDLManager().InsertUser(TENANT_USER_ID, userRegistrationInfo, registrationConfiration, transaction);
        }

        /// <summary>
        /// A Method that checks and activates the user account
        /// </summary>
        /// <param name="User_Email">
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <param name="Confirmation_Code">
        /// A <see cref="System.String"/> that holds the confirmation code
        /// for the user email id
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that holds the user first name
        /// </returns>
        public string CheckForActivationCode(string User_Email, string Confirmation_Code)
        {
            return new UserRegistrationDLManager().CheckForActivationCode(User_Email, Confirmation_Code);
        }

        /// <summary>
        /// A Method that gets the tenant name for the user id
        /// </summary>
        /// <param name="Tenant_user_Id">
        /// A <see cref="System.Int32"/> that holds the user id
        /// </param>
        /// <param name="SortExpression">
        /// A <see cref="System.String"/> that holds the sort expression
        /// </param>
        /// <param name="sortType">
        /// A <see cref="Forte.HCM.DataObjects"/> that holds the sort direction
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        public List<UserRegistrationInfo> GetSelectedCorporateAccountUsers(int Tenant_user_Id, string SortExpression, SortType sortType)
        {
            return new UserRegistrationDLManager().GetSelectedCorporateAccountUsers(Tenant_user_Id, SortExpression, sortType);
        }

        /// <summary>
        /// A Method that updates the corporate users
        /// details
        /// </summary>
        /// <param name="userRegistrationInfo">
        /// A <see cref="System.String"/> that holds the user email
        /// A <see cref="System.String"/> that holds the user password
        /// A <see cref="System.String"/> that holds the user first name
        /// A <see cref="System.String"/> that holds the user last name
        /// A <see cref="System.String"/> that holds the user Email
        /// A <see cref="System.String"/> that holds the user phone number
        /// A <see cref="System.String"/> that holds the user company name
        /// A <see cref="System.String"/> that holds the user title
        /// A <see cref="System.Int32"/> that holds the user subscription type id
        /// A <see cref="System.Char"/> that holds the Activation Status
        /// A <see cref="System.Int16"/> that holds the number of users
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of records updated
        /// </returns>
        public int UpdateCorporateUser(UserRegistrationInfo userRegistrationInfo)
        {
            return new UserRegistrationDLManager().UpdateCorporateUser(userRegistrationInfo);
        }

        /// <summary>
        /// A Method that deletes the tenant user
        /// </summary>
        /// <param name="Tenant_User_Id">
        /// A <see cref="System.Int32"/> that holds the tenant user id
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the effected records for the
        /// command
        /// </returns>
        public int DeleteTenantUser(int Tenant_User_Id)
        {
            return new UserRegistrationDLManager().DeleteTenantUser(Tenant_User_Id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="System.Int32"/> that holds the user id of the 
        /// logged in user
        /// </param>
        /// <param name="TenantId">
        /// A <see cref="System.Int32"/> that hold the tenant id of the 
        /// logged in user
        /// </param>
        /// <param name="UserInfo">
        /// A <see cref="System.String"/> that holds the user email
        /// A <see cref="System.String"/> that holds the user password
        /// A <see cref="System.String"/> that holds the user first name
        /// A <see cref="System.String"/> that holds the user last name
        /// A <see cref="System.String"/> that holds the user Email
        /// A <see cref="System.String"/> that holds the user phone number
        /// A <see cref="System.String"/> that holds the user company name
        /// A <see cref="System.String"/> that holds the user title
        /// A <see cref="System.Int32"/> that holds the user subscription type id
        /// A <see cref="System.Char"/> that holds the Activation Status
        /// A <see cref="System.Int16"/> that holds the number of users
        /// A <see cref="System.DateTime"/> that holds the account activated date
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <param name="subscriptionTypes">
        /// A <see cref="System.Int32"/> that holds the Id of the Subscription
        /// A <see cref="System.String"/> that holds the name of the Subscription
        /// A <see cref="System.String"/> that holds the description of the Subscription
        /// A <see cref="System.String"/> that holds the published status of the Subscription
        /// A <see cref="System.Int32"/> that holds the created user id of the Subscription
        /// A <see cref="System.DateTime"/> that holds the created date of the Subscription
        /// A <see cref="System.Int32"/> that holds the modified user id of the Subscription
        /// A <see cref="System.DateTime"/> that holds the modified date of the Subscription
        /// A <see cref="System.Boolean"/> that holds the whether the subscription is deleted or not
        /// A <see cref="System.String"/> that holds the delted user id of the subscription
        /// A <see cref="System.String"/> that holds the concatenated values
        /// of subscription id and published status
        /// </param>
        /// <param name="subscriptionFeatures">
        /// A <see cref="System.Int32"/> that holds the Id of the subscription feature
        /// A <see cref="System.Int32"/> that holds the Id of the subscription
        /// A <see cref="System.Int32"/> that holds the Id of the feature
        /// A <see cref="System.String"/> that holds the feature name of the subscription
        /// A <see cref="System.String"/> that holds the feature value
        /// A <see cref="System.String"/> that holds the unit id 
        /// of the subscription feature
        /// A <see cref="System.String"/> that holds the Unit name 
        /// of the subscription feature
        /// A <see cref="System.Int16"/> that holds the not applicable
        /// value of the subscription feature
        /// A <see cref="System.String"/> that holds the default value
        /// of the subscription feature.
        /// A <see cref="System.Int16"/> that holds whether
        /// the feature is unlimited or not.
        /// A <see cref="System.Int32"/> that holds the Id of the created user
        /// A <see cref="System.DateTime"/> that holds the created date of the subscription feature
        /// A <see cref="System.Int32"/> that holds the id of the modified user 
        /// A <see cref="System.DateTime"/> that holds the modified date of the subscription feature
        /// </param>
        /// <param name="SubscriptionRole">
        /// A <see cref="System.String"/> that holds the subscription role.
        /// </param>
        public void GetSubscriptionAccountDetailsUser(int UserId, int TenantId, out UserRegistrationInfo UserInfo,
            out SubscriptionTypes subscriptionTypes, out List<SubscriptionFeatures> subscriptionFeatures, string SubscriptionRole)
        {
            new UserRegistrationDLManager().GetSubscriptionAccountDetailsUser(UserId, TenantId, out UserInfo, out subscriptionTypes,
                out subscriptionFeatures, SubscriptionRole);
        }

        /// <summary>
        /// A Method that updates the corporate user subscription details
        /// to the repository
        /// </summary>
        /// <param name="UserInfo">
        /// A <see cref="System.String"/> that holds the user email
        /// A <see cref="System.String"/> that holds the user password
        /// A <see cref="System.String"/> that holds the user first name
        /// A <see cref="System.String"/> that holds the user last name
        /// A <see cref="System.String"/> that holds the user Email
        /// A <see cref="System.String"/> that holds the user phone number
        /// A <see cref="System.String"/> that holds the user company name
        /// A <see cref="System.String"/> that holds the user title
        /// A <see cref="System.Int32"/> that holds the user subscription type id
        /// A <see cref="System.Char"/> that holds the Activation Status
        /// A <see cref="System.Int16"/> that holds the number of users
        /// A <see cref="System.DateTime"/> that holds the account activated date
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of effected
        /// records in the repository
        /// </returns>
        public int UpdateCorporateUserSubscriptionDetails(UserRegistrationInfo UserInfo)
        {
            return new UserRegistrationDLManager().UpdateCorporateUserSubscriptionDetails(UserInfo);
        }

        /// <summary>
        /// A Method that get the user registration info based on 
        /// the user id
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="System.Int32"/> that holds the user id
        /// </param>
        /// <returns>
        /// A <see cref="Forte.HCM.DataObjects.UserRegistrationInfo"/> that holds
        /// the user registration details
        /// </returns>
        public UserRegistrationInfo GetUserRegistrationInfo(int UserId)
        {
            return new UserRegistrationDLManager().GetUserRegistrationInfo(UserId);
        }

        /// <summary>
        /// A Method that updates the subscription for a user
        /// </summary>
        /// <param name="userRegistrationInfo">
        /// A <see cref="Forte.HCM.DataObjects.UserRegistrationInfo"/> that holds
        /// the user registration details
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the effected records in the
        /// repository
        /// </returns>
        public int UpgradeAccountSubscriptionDetails(UserRegistrationInfo userRegistrationInfo)
        {
            return new UserRegistrationDLManager().UpgradeAccountSubscriptionDetails(userRegistrationInfo);
        }

        /// <summary>
        /// A Method that gets the system maximum corporate users
        /// </summary>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the maximum corporate users
        /// can created by a user
        /// </returns>
        public int GetMaximumCorporateUsers()
        {
            return new UserRegistrationDLManager().GetMaximumCorporateUsers();
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <returns></returns>
        public UserDetail GetPassword(int UserID)
        {
            return new UserRegistrationDLManager().GetPassword(UserID);
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <param name="UserID">The user name.</param>
        /// <returns></returns>
        public UserDetail GetPassword(string  UserName)
        {
            return new UserRegistrationDLManager().GetPassword(UserName);
        }

        #endregion Public Methods


        /// <summary>
        /// Checks for activation code with transaction.
        /// </summary>
        /// <param name="User_Email">The user_ email.</param>
        /// <param name="Confirmation_Code">The confirmation_ code.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public string CheckForActivationCodeWithTransaction(string User_Email,
            string Confirmation_Code, IDbTransaction transaction)
        {
            return new UserRegistrationDLManager().CheckForActivationCodeWithTransaction(User_Email,
                Confirmation_Code, transaction);
        }

        /// <summary>
        /// Updates the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdatePassword(string password, int userID)
        {
            new UserRegistrationDLManager().UpdatePassword(password,userID);
        }

       
        public int InsertCorporateUser(int? TENANT_USER_ID, UserRegistrationInfo userRegistrationInfo, RegistrationConfirmation registrationConfiration,
          DbTransaction transaction)
        {
            return new UserRegistrationDLManager().InsertCorporateUser(TENANT_USER_ID, userRegistrationInfo, registrationConfiration, transaction);


        }

        public bool GetCustomerAdmin(int userid)
        {
            return new UserRegistrationDLManager().GetCustomerAdmin(userid);
        }

        public int UpdateCorporateUserAdmin(UserRegistrationInfo userRegistrationInfo)
        {
            return new UserRegistrationDLManager().UpdateCorporateUserAdmin(userRegistrationInfo);
        }

        public string GetcorporateRoles(int userid)
        {
            return new UserRegistrationDLManager().GetcorporateRoles(userid);
        }

        public int InsertCorporateUserRoles(UserRegistrationInfo userRegistrationInfo, DbTransaction dbTransaction)
        {
            return new UserRegistrationDLManager().InsertCorporateUserRoles(userRegistrationInfo, dbTransaction);
        }

        public void UpdateUserInfo(UserRegistrationInfo userEditInfo)
        {
            new UserRegistrationDLManager().UpdateUserInfo(userEditInfo);
        }

        public List<UserRegistrationInfo> GetCorporateChileUsers(short user_ID)
        {
          
            return new UserRegistrationDLManager().GetCorporateChileUsers(user_ID);
        }

        /// <summary>
        /// Method that updates the legal accepted status.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="accepted">
        /// A <see cref="bool"/> that holds the accepted status. True 
        /// represents accepted and false not.
        /// </param>
        public void UpdateLegalAcceptedStatus(int userID, bool accepted)
        {
            new UserRegistrationDLManager().UpdateLegalAcceptedStatus
                (userID, accepted);
        }

        /// <summary>
        /// Method that checks and activates the user account along with legal
        /// acceptance.
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="string"/> that holds the confirmation code.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the user first name + last name.
        /// </returns>
        public string CheckForActivationCodeWithLegalAcceptance(string userName, string confirmationCode)
        {
            return new UserRegistrationDLManager().CheckForActivationCodeWithLegalAcceptance
                (userName, confirmationCode);
        }

        public double GetExpiryDate(int subscriptionId)
        {
            return new UserRegistrationDLManager().GetExpiryDate(subscriptionId);

        }

        /// <summary>
        /// Method that gets the role id against its role code
        /// </summary>
        /// <param name="roleCode">
        /// A <see cref="string"/> that holds the role code.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds role id
        /// </returns>
        public int GetRoleIDByRoleCode(string roleCode)
        {
            return new UserRegistrationDLManager().GetRoleIDByRoleCode(roleCode);
        }
    }
}
