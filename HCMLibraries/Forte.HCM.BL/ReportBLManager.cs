﻿#region Header                                                                 
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ReportBLManager.cs
// File that represents the data layer for the Test Session module.
// This will talk to the database to perform the operations associated
// with the module.
#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the report module.
    /// This includes functionalities for design report , candidate report etc.
    /// This will talk to the database for performing these operations.
    /// </summary>
    public class ReportBLManager
    {
        #region Public Method                                                  
        /// <summary>
        /// this method represent the Test details based on search criteria.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A<see cref="string"/>that holds the test Search Criteria
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="string"/>that holds the page size
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="string"/>that holds the page number
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction Asc/desc
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total Records out parameter.
        /// </param>
        /// <returns>
        /// A list for <see cref="TestDetail"/> that holds the test detail
        /// </returns>
        public List<TestDetail> GetTests(TestSearchCriteria testSearchCriteria,
            int pageSize, int pageNumber, string orderBy, SortType orderByDirection,
            out int totalRecords)
        {
            return new ReportDLManager().GetTests(testSearchCriteria, pageSize, pageNumber,
                orderBy, orderByDirection, out totalRecords);
        }

        /// <summary>
        /// this method represent the Test details based on search criteria.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A<see cref="string"/>that holds the test Search Criteria
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="string"/>that holds the page size
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="string"/>that holds the page number
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction Asc/desc
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total Records out parameter.
        /// </param>
        /// <returns>
        /// A list for <see cref="TestDetail"/> that holds the test detail
        /// </returns>
        public List<TestDetail> GetTestsByQuestionType(QuestionType questionType, TestSearchCriteria testSearchCriteria,
           int pageSize, int pageNumber, string orderBy, SortType orderByDirection,
           out int totalRecords)
        {
            return new ReportDLManager().GetTestsByQuestionType(questionType,testSearchCriteria, pageSize, pageNumber,
                orderBy, orderByDirection, out totalRecords);
        }

        /// <summary>
        /// Method to get the question statistics deatils for the question.
        /// In second tab of the test statistics info page
        /// </summary>
        /// <param name="testID">
        /// A<see cref="int"/> that holds the test id 
        /// </param>
        /// <returns>
        /// A list for <see cref="QuestionStatisticsDetail"/>that holds the 
        /// question statistics details 
        /// </returns>
        public List<QuestionStatisticsDetail> GetQuestionStatistics(string testID)
        {
            List<QuestionStatisticsDetail> questionStatisticsDetails =
                new ReportDLManager().GetQuestionStatistics(testID);

            return questionStatisticsDetails;
        }

        /// <summary>
        /// Method to get the Question Detail based on the test id and questionId.
        /// </summary>
        /// <param name="testID">
        /// A<see cref="int"/> that holds the test id
        /// </param>
        /// <param name="questionID">
        /// A<see cref="string"/>that holds the question Id
        /// </param>
        /// <returns>
        /// A<see cref="QuestionDetail"/>that holds the Question Detail
        /// </returns>
        public QuestionDetail GetSingleQuestionDetails(string testID, string questionID)
        {
            return new ReportDLManager().GetQuestion(testID, questionID);
        }

        public QuestionDetail GetSingleQuestionDetails(string testID, string questionID,int attemptID,string candidateSessionKey)
        {
            return new ReportDLManager().GetQuestion(testID, questionID,attemptID,candidateSessionKey);
        }

        public QuestionDetail GetSingleQuestionDetailsOpenText(string testID, string questionID)
        {
            return new ReportDLManager().GetQuestionOpenText(testID, questionID);
        }
        public QuestionDetail GetSingleQuestionDetailsOpenText(string testID, string questionID, int attemptID, string candidateSessionKey)
        {
            return new ReportDLManager().GetQuestionOpenText(testID, questionID, attemptID, candidateSessionKey);
        }

        /// <summary>
        /// Method to get the Candidate Statistics Detail based on the search creteria.
        /// </summary>
        /// <param name="candidateName">
        /// A<see cref="int"/> that holds the candidate name
        /// </param>
        /// <param name="sessionCreatedBy">
        /// A<see cref="int"/> that holds the session creator user id
        /// </param>
        /// <param name="scheduleCreatedBy">
        /// A<see cref="int"/> that holds the scheldule creator user id
        /// </param>
        /// <param name="testDateFrom">
        /// A<see cref="string"/> that holds the test date from
        /// </param>
        /// <param name="testDateTo">
        /// A<see cref="string"/> that holds the test date to
        /// </param>
        /// <param name="absoluteScoreFrom">
        /// A<see cref="string"/> that holds the absolute score from
        /// </param>
        /// <param name="absoluteScoreTo">
        /// A<see cref="string"/> that holds the absolute score to
        /// </param>
        /// <param name="relativeScoreFrom">
        /// A<see cref="string"/> that holds the relative score from
        /// </param>
        /// <param name="relativeScoreTo">
        /// A<see cref="string"/> that holds the relative score to
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/> that holds the test key
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/> that holds the order by
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/> that holds the Order by direction asc/desc 
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/> that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total records
        /// </param>
        /// <returns>
        /// A List for<see cref="CandidateStatisticsDetail"/> 
        /// that holds the Candidate Statistics Detail
        /// </returns>
        public List<CandidateStatisticsDetail> GetReportCandidateStatisticsDetails
            (string candidateName, string sessionCreatedBy,
            string scheduleCreatedBy, string testDateFrom,
            string testDateTo, string absoluteScoreFrom,
            string absoluteScoreTo, string relativeScoreFrom,
            string relativeScoreTo, string testKey, string orderBy,
            string orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {
            return new ReportDLManager().GetReportCandidateStatisticsDetails(
                candidateName, sessionCreatedBy, scheduleCreatedBy,
                testDateFrom, testDateTo, absoluteScoreFrom,
                absoluteScoreTo, relativeScoreFrom, relativeScoreTo, testKey,
                orderBy, orderByDirection, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method to get the Candidate Statistics Detail based on the search creteria.
        /// </summary>
        /// <param name="testStatisticsSearchCriteria">
        /// A<see cref="TestStatisticsSearchCriteria"/> that holds the test StatisticsSearch Criteria
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/> that holds the order by field 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/> that holds the order by direction asc/desc
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/> that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total records out parameter
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateStatisticsDetail"/> that holds the candidate statistics detail
        /// </returns>
        public List<CandidateStatisticsDetail> GetReportCandidateStatisticsDetails
          (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy,
          string orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {
            return new ReportDLManager().GetReportCandidateStatisticsDetails(testStatisticsSearchCriteria,
                orderBy, orderByDirection, pageNumber, pageSize, out totalRecords);
        }


        public List<CandidateStatisticsDetail> GetReportCandidateStatisticsDetailsOpenText
          (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy,
          string orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {
            return new ReportDLManager().GetReportCandidateStatisticsDetailsOpenText(testStatisticsSearchCriteria,
                orderBy, orderByDirection, pageNumber, pageSize, out totalRecords);
        }
        /// <summary>
        /// Method to get the Candidate Statistics Detail based on the search creteria.
        /// </summary>
        /// <param name="testStatisticsSearchCriteria">
        /// A<see cref="TestStatisticsSearchCriteria"/> that holds the test StatisticsSearch Criteria
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/> that holds the order by field 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/> that holds the order by direction asc/desc
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/> that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total records out parameter
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateStatisticsDetail"/> that holds the candidate statistics detail
        /// </returns>
        public DataTable GetReportCandidateStatisticsDetailsTable
          (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy,
          string orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {
            return new ReportDLManager().GetReportCandidateStatisticsDetailsTable(testStatisticsSearchCriteria,
                orderBy, orderByDirection, pageNumber, pageSize, out totalRecords);
        }

        public DataTable GetReportCandidateStatisticsDetailsOpenTextTable
         (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy,
         string orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {
            return new ReportDLManager().GetReportCandidateStatisticsDetailsOpenTextTable(testStatisticsSearchCriteria,
                orderBy, orderByDirection, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Method to get the test summary details of the test 
        /// </summary>
        /// <param name="testID">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the summary details
        /// </returns>
        public TestStatistics GetTestSummaryDetails(string testID)
        {
            return new ReportDLManager().GetTestStatisticsDetails(testID);

        }

        /// <summary>
        /// Method to get the additional details of the test 
        /// </summary>
        /// <param name="testID">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the summary details
        /// </returns>
        public TestStatistics GetAdditionalTestDetails(string testID)
        {
            return new ReportDLManager().GetAdditionalTestDetails(testID);
        }

        /// <summary>
        /// Represents a method to get a name of the test
        /// when test id is passed
        /// </summary>
        /// <param name="testID">
        /// A<see cref="string"/>that holds the testid
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the test name
        /// </returns>
        public string GetTestName(string testID)
        {
            return new ReportDLManager().GetTestName(testID);
        }

        /// <summary>
        /// Represents a method to get a completed date of the test
        /// when candidate session and sessionid  passed
        /// </summary>
        /// <param name="candidateSessionId">
        /// A<see cref="string"/>that holds the candidateSessionId
        /// </param>
        /// <param name="attemptId">
        /// A<see cref="string"/>that holds the attemptId
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the completed date
        /// </returns>
        public DateTime GetTestCompletedDateByCandidate(string candidateSessionId, int attemptId)
        {
            return new ReportDLManager().GetTestCompletedDateByCandidate(candidateSessionId, attemptId);
        }

        /// <summary>
        /// Represents a method to get a completed date of the interview test
        /// when candidate session and sessionid  passed
        /// </summary>
        /// <param name="candidateSessionId">
        /// A<see cref="string"/>that holds the candidateSessionId
        /// </param>
        /// <param name="attemptId">
        /// A<see cref="string"/>that holds the attemptId
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the completed date
        /// </returns>
        public DateTime GetInterviewTestCompletedDateByCandidate(string candidateSessionId, int attemptId)
        {
            return new ReportDLManager().GetInterviewTestCompletedDateByCandidate(candidateSessionId, attemptId);
        }
        /// <summary>
        /// Represents the method to get the candidate 
        /// test result statistics details
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="CandidateStatisticsDetail"/>that holds 
        /// the candidate statistics details    
        /// </returns>
        public CandidateStatisticsDetail GetCandidateTestResultStatisticsDetails
            (string sessionKey, string testKey, int attemptID)
        {
            //Call the method in Dl and sent back the results
            return new ReportDLManager().GetCandidateTestResultStatisticsDetails
                 (sessionKey, testKey, attemptID);
        }

        public CandidateStatisticsDetail GetCandidateOpenTextTestResultStatisticsDetails
           (string sessionKey, string testKey, int attemptID)
        {
            //Call the method in Dl and sent back the results
            return new ReportDLManager().GetCandidateOpenTextTestResultStatisticsDetails
                 (sessionKey, testKey, attemptID);
        }

        /// <summary>
        /// Represents the method to get the candidate test result categories 
        /// chart details 
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </returns>
        public MultipleSeriesChartData GetCandidateStatisticsCategoriesChartDetails
            (string sessionKey, string testKey, int attemptID)
        {
            return new ReportDLManager().GetCandidateStatisticsCategoriesChartDetails
                (sessionKey, testKey, attemptID);
        }
        ///<summary>
        /// Represents the method to get the candidate test result subject 
        /// chart details 
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </returns>
        public MultipleSeriesChartData GetCandidateStatisticsSubjectsChartDetails
            (string sessionKey, string testKey, int attemptID)
        {
            return new ReportDLManager().
                GetCandidateStatisticsSubjectsChartDetails(sessionKey, testKey, attemptID);
        }
        
        /// <summary>
        /// Represents the method to get the candidate test result test area 
        /// chart details 
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </returns>
        public MultipleSeriesChartData GetCandidateStatisticsTestAreaChartDetails
            (string sessionKey, string testKey, int attemptID)
        {
            return new ReportDLManager().GetCandidateStatisticsTestAreaChartDetails
             (sessionKey, testKey, attemptID);
        }

        /// <summary>
        /// Represents the method to get the candidate test result complexity
        /// chart details 
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </returns>
        public MultipleSeriesChartData GetCandidateStatisticsComplexityChartDetails
            (string sessionKey, string testKey, int attemptID)
        {
            return new ReportDLManager().GetCandidateStatisticsComplexityChartDetails
             (sessionKey, testKey, attemptID);
        }

        /// <summary>
        /// Method to get the candidate test score 
        /// </summary>
        /// <param name="testKey">
        /// A<see cref="string "/>that holds the test key 
        /// </param>
        /// <returns>
        /// A List for<see cref="ChartData"/>that holds the chart data
        /// </returns>
        public List<ChartData> GetTestCandidateScores(string testKey)
        {
            return new ReportDLManager().GetTestCandidateScores(testKey);
        }

        /// <summary>
        /// Method to get the general test statistics control information 
        /// </summary>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key 
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the test statistics 
        /// information
        /// </returns>
        public TestStatistics GetGeneralTestStatisticsControlInformation(string testKey)
        {
            //Get the information from the data base and return it
            return new ReportDLManager().GetGeneralTestStatisticsControlInformation(testKey);
        }

        /// <summary>
        /// Method that used to get the general statistics chart information
        /// for the given test key
        /// </summary>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the test statistics 
        /// information
        /// </returns>
        public TestStatistics GetGeneralStatisticsChartInformation(string testKey)
        {
            //Get the chart details from the Dl manager and return the same
            return new ReportDLManager().GetGeneralStatisticsChartInformation(testKey);
        }

        /// <summary>
        /// Method that used to get the candidate statistics detail along with the 
        /// test statistics details for the given candidate 
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <returns>
        /// A<see cref="CandidateStatisticsDetail"/>that holds the candidate 
        /// statistics details 
        /// </returns>
        public List<CandidateStatisticsDetail> GetCandidatesStatistics(CandidateReportDetail candidateReportDetail)
        {
            return new ReportDLManager().GetCandidatesStatistics(candidateReportDetail);

        }

        /// <summary>
        /// Represents the method to get the overall candidate score details 
        /// for the given test
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateDetail"/>that holds the candidate details 
        /// </param>
        /// <returns>
        /// A<see cref="OverAllCandidateScoreDetail"/>that holds the score details
        /// </returns>
        public OverAllCandidateScoreDetail GetOverallScoreDetails(CandidateReportDetail candidateReportDetail)
        {
            OverAllCandidateScoreDetail overAllCandidateScoreDetail = new ReportDLManager().GetOverallScoreDetails(candidateReportDetail);

            overAllCandidateScoreDetail.AbsoluteScoreDetails = new List<ChartData>();

            //Add the absolute score chart details 
            overAllCandidateScoreDetail.AbsoluteScoreDetails.Add
                (new ChartData("Candidate Score", overAllCandidateScoreDetail.AbsoluteMyScore));

            overAllCandidateScoreDetail.AbsoluteScoreDetails.Add
                (new ChartData("Min Score", overAllCandidateScoreDetail.AbsoluteMinScore));

            overAllCandidateScoreDetail.AbsoluteScoreDetails.Add
                (new ChartData("Max Score", overAllCandidateScoreDetail.AbsoluteMaxScore));

            overAllCandidateScoreDetail.AbsoluteScoreDetails.Add
                (new ChartData("Average Score", overAllCandidateScoreDetail.AbsoluteAvgScore));

            //Add the relative score chart details 
            overAllCandidateScoreDetail.RelativeScoreDetails = new List<ChartData>();

            overAllCandidateScoreDetail.RelativeScoreDetails.Add
                (new ChartData("Candidate Score", overAllCandidateScoreDetail.RelativeMyScore));

            overAllCandidateScoreDetail.RelativeScoreDetails.Add
                (new ChartData("Min Score", overAllCandidateScoreDetail.RelativeMinScore));

            overAllCandidateScoreDetail.RelativeScoreDetails.Add
                (new ChartData("Max Score", overAllCandidateScoreDetail.RelativeMaxScore));

            overAllCandidateScoreDetail.RelativeScoreDetails.Add
                (new ChartData("Average Score", overAllCandidateScoreDetail.RelativeAvgScore));

            //Add the absolute single chart detail
            overAllCandidateScoreDetail.AbsoluteCandidateScoreDetails = new List<ChartData>();

            overAllCandidateScoreDetail.AbsoluteCandidateScoreDetails.Add(new
                ChartData("Candidate Score", overAllCandidateScoreDetail.AbsoluteMyScore));

            //Add the relative single chart data
            overAllCandidateScoreDetail.RelativeCandidateScoreDetails = new List<ChartData>();

            overAllCandidateScoreDetail.RelativeCandidateScoreDetails.Add
               (new ChartData("Candidate Score", overAllCandidateScoreDetail.RelativeMyScore));

            return overAllCandidateScoreDetail;
        }

        /// <summary>
        /// Represents the method to get the candidate score 
        /// details for the histogram chart
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <returns>
        /// A<see cref="HistogramData"/>that holds the histogram data
        /// </returns>
        public ReportHistogramData GetReportCandidateScoreDetails(CandidateReportDetail candidateReportDetail)
        {
            return new ReportDLManager().GetReportCandidateScoreDetails(candidateReportDetail);
        }

        /// <summary>
        /// Represents the method to get the candidate statistics detail
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <param name="totalRecord">
        /// A<see cref="CandidateReportDetail"/>that holds the total records
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="CandidateReportDetail"/>that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="CandidateReportDetail"/>that holds the page size
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateStatisticsDetail"/>that holds the candidate statistics detail
        /// </returns>
        public List<CandidateStatisticsDetail> GetComparisonCandidateStatistics
            (CandidateReportDetail candidateReportDetail, out int totalRecord, int pageNumber,
                int pageSize)
        {
            return new ReportDLManager().GetComparisonCandidateStatistics
                (candidateReportDetail, out totalRecord, pageNumber, pageSize);
        }
        /// <summary>
        /// Represents the method to get the candidate score 
        /// details for the histogram chart
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <returns>
        /// A<see cref="HistogramData"/>that holds the histogram data
        /// </returns>
        public ReportHistogramData GetComparisonReportHistogramScoreDetails
            (CandidateReportDetail candidateReportDetail)
        {
            return new ReportDLManager().GetComparisonReportHistogramScoreDetails(candidateReportDetail);
        }

        /// <summary>
        /// Represents the method to get the overall candidate score details
        /// for the comparison report
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the 
        /// candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="OverAllCandidateScoreDetail"/>over all candidate score 
        /// details
        /// </returns>
        public OverAllCandidateScoreDetail GetComparisonOverallScoreDetails
            (CandidateReportDetail candidateReportDetail)
        {

            OverAllCandidateScoreDetail overAllCandidateScoreDetail = new ReportDLManager().GetComparisonOverallScoreDetails(candidateReportDetail);

            //overAllCandidateScoreDetail.AbsoluteScoreDetails = new List<ChartData>();

            ////Add the absolute score chart details 
            //overAllCandidateScoreDetail.AbsoluteScoreDetails.Add
            //    (new ChartData("Candidate Score", overAllCandidateScoreDetail.AbsoluteMyScore));

            //overAllCandidateScoreDetail.AbsoluteScoreDetails.Add
            //    (new ChartData("Min Score", overAllCandidateScoreDetail.AbsoluteMinScore));

            //overAllCandidateScoreDetail.AbsoluteScoreDetails.Add
            //    (new ChartData("Max Score", overAllCandidateScoreDetail.AbsoluteMaxScore));

            //overAllCandidateScoreDetail.AbsoluteScoreDetails.Add
            //    (new ChartData("Average Score", overAllCandidateScoreDetail.AbsoluteAvgScore));

            ////Add the relative score chart details 
            //overAllCandidateScoreDetail.RelativeScoreDetails = new List<ChartData>();

            //overAllCandidateScoreDetail.RelativeScoreDetails.Add
            //    (new ChartData("Candidate Score", overAllCandidateScoreDetail.RelativeMyScore));

            //overAllCandidateScoreDetail.RelativeScoreDetails.Add
            //    (new ChartData("Min Score", overAllCandidateScoreDetail.RelativeMinScore));

            //overAllCandidateScoreDetail.RelativeScoreDetails.Add
            //    (new ChartData("Max Score", overAllCandidateScoreDetail.RelativeMaxScore));

            //overAllCandidateScoreDetail.RelativeScoreDetails.Add
            //    (new ChartData("Average Score", overAllCandidateScoreDetail.RelativeAvgScore));

            ////Add the absolute single chart detail
            //overAllCandidateScoreDetail.AbsoluteCandidateScoreDetails = new List<ChartData>();

            //overAllCandidateScoreDetail.AbsoluteCandidateScoreDetails.Add(new
            //    ChartData("Candidate Score", overAllCandidateScoreDetail.AbsoluteMyScore));

            ////Add the relative single chart data
            //overAllCandidateScoreDetail.RelativeCandidateScoreDetails = new List<ChartData>();

            //overAllCandidateScoreDetail.RelativeCandidateScoreDetails.Add
            //   (new ChartData("Candidate Score", overAllCandidateScoreDetail.RelativeMyScore));

            return overAllCandidateScoreDetail;
        }

        /// <summary>
        /// Represents the method to get the dataview of the multiseris chart control 
        /// for the comparison category chart 
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="DataView"/>that holds the dataview of the category details
        /// </returns>
        public DataTable GetComparisonReportCategoryDetails
            (CandidateReportDetail candidateReportDetail)
        {
            DataTable rawDatatable = new ReportDLManager().GetComparisonReportCategoryDetails
                (candidateReportDetail);

            DataTable chartDataTable = new DataTable();

            //Store the candidate names in the list
            List<string> candidateNames = new List<string>();

            //Store the category names in the list
            List<string> categoryNames = new List<string>();

            //Store the category names in the list
            List<string> candidateSessionKeys = new List<string>();

            //Store the category names in the list
            List<int> candidateAttemptId = new List<int>();

            chartDataTable.Columns.Add("ShortName");

            chartDataTable.Columns.Add("Name");

            chartDataTable.Columns.Add("ID");

            chartDataTable.Columns.Add("Max Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Min Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Avg Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Max Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Min Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Avg Score", Type.GetType("System.Decimal"));


            foreach (DataRow objRow in rawDatatable.Rows)
            {
                if (!candidateNames.Contains(objRow["CANDIDATE_NAME"].ToString().Trim()))
                {
                    //add candidate name combined with session key and attempt id and
                    //add it in a column name
                    string combinedSessionKeyCandidateName = string.Empty;
                    string relCombinedSessionKeyCandidateName = string.Empty;
                    if ((objRow["CANDIDATE_NAME"].ToString().Trim() != "MAX SCORE") &&
                      (objRow["CANDIDATE_NAME"].ToString().Trim() != "MIN SCORE") &&
                      (objRow["CANDIDATE_NAME"].ToString().Trim() != "AVG SCORE"))
                    {
                        combinedSessionKeyCandidateName =
                        string.Concat(objRow["CANDIDATE_NAME"].ToString(), "-",
                        objRow["CANDIDATE_SESSION_KEY"].ToString(), "-",
                         objRow["ATTEMPT_ID"].ToString());

                        relCombinedSessionKeyCandidateName = string.Concat("Rel-", objRow["CANDIDATE_NAME"].ToString(), "-",
                        objRow["CANDIDATE_SESSION_KEY"].ToString(), "-",
                         objRow["ATTEMPT_ID"].ToString());
                    }
                    else
                    {
                        combinedSessionKeyCandidateName =
                        objRow["CANDIDATE_NAME"].ToString();
                        relCombinedSessionKeyCandidateName =
                            string.Concat("Rel-", objRow["CANDIDATE_NAME"].ToString());
                    }

                    candidateNames.Add(combinedSessionKeyCandidateName);
                    candidateNames.Add(relCombinedSessionKeyCandidateName);

                    if ((objRow["CANDIDATE_NAME"].ToString().Trim() != "MAX SCORE") &&
                        (objRow["CANDIDATE_NAME"].ToString().Trim() != "MIN SCORE") &&
                        (objRow["CANDIDATE_NAME"].ToString().Trim() != "AVG SCORE"))
                    {
                        int index = chartDataTable.Columns.IndexOf(combinedSessionKeyCandidateName);
                        if (index == -1)
                            chartDataTable.Columns.Add(combinedSessionKeyCandidateName, Type.GetType("System.Decimal"));

                        int relIndex = chartDataTable.Columns.IndexOf(relCombinedSessionKeyCandidateName);
                        if (relIndex == -1)
                            chartDataTable.Columns.Add(relCombinedSessionKeyCandidateName, Type.GetType("System.Decimal"));

                    }
                }
                if (!categoryNames.Contains(objRow["CATEGORY_NAME"].ToString().Trim()))
                {
                    categoryNames.Add(objRow["CATEGORY_NAME"].ToString().Trim());
                    DataRow dataRow = chartDataTable.NewRow();
                    if (objRow["CATEGORY_NAME"].ToString().Trim().Length > 15)
                    {
                        dataRow["ShortName"] = objRow["CATEGORY_NAME"].ToString().Trim().Substring(0, 15);
                    }
                    else
                    {
                        dataRow["ShortName"] = objRow["CATEGORY_NAME"].ToString().Trim();
                    }

                    dataRow["ID"] = objRow["CAT_KEY"].ToString().Trim();

                    dataRow["Name"] = objRow["CATEGORY_NAME"].ToString().Trim();

                    chartDataTable.Rows.Add(dataRow);
                }
            }

            foreach (DataRow dr in chartDataTable.Rows)
            {
                foreach (DataColumn dc in chartDataTable.Columns)
                {
                    if (dc.ColumnName != "Name" && dc.ColumnName != "ShortName" && dc.ColumnName != "ID")
                    {
                        string[] columnName = dc.ColumnName.Split(new char[] { '-' });

                        DataRow[] datarow = null;

                        //if the column name contains relative score 
                        if ((dc.ColumnName.Length > 3) && (dc.ColumnName.Substring(0, 3) == "Rel"))
                        {
                            if (columnName.Length > 2)
                            {
                                datarow = rawDatatable.Select("CATEGORY_NAME = '" + dr["Name"] +
                               "'AND CANDIDATE_NAME = '" + columnName[1] + "'AND CANDIDATE_SESSION_KEY = '"
                                   + (columnName[2]) + "'AND ATTEMPT_ID = '" + (columnName[3]) + "'");
                            }
                            else
                            {
                                datarow = rawDatatable.Select("CATEGORY_NAME = '" + dr["Name"] +
                                "'AND CANDIDATE_NAME = '" + columnName[1] + "'");
                            }
                            if (datarow != null)
                                dr[dc.ColumnName] = Convert.ToDecimal(datarow[0].ItemArray[4].ToString());

                        }
                        else
                        {
                            if (columnName.Length > 1)
                            {
                                datarow = rawDatatable.Select("CATEGORY_NAME = '" + dr["Name"] +
                               "'AND CANDIDATE_NAME = '" + columnName[0] + "'AND CANDIDATE_SESSION_KEY = '"
                                   + (columnName[1]) + "'AND ATTEMPT_ID = '" + (columnName[2]) + "'");
                            }
                            else
                            {
                                datarow = rawDatatable.Select("CATEGORY_NAME = '" + dr["Name"] +
                                "'AND CANDIDATE_NAME = '" + columnName[0] + "'");
                            }
                            if (datarow != null)
                                dr[dc.ColumnName] = Convert.ToDecimal(datarow[0].ItemArray[3].ToString());
                        }
                    }
                }
            }

            return chartDataTable;
        }

        /// <summary>
        /// Represents the method to get the data table for the comparison 
        /// report subject details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A Datatable that holds the comparison's subject chart details
        /// </returns>
        public DataTable GetComparisonReportSubjectDetails(CandidateReportDetail candidateReportDetail)
        {
            DataTable rawDatatable = new ReportDLManager().GetComparisonReportSubjectDetails
               (candidateReportDetail);

            DataTable chartDataTable = new DataTable();

            List<string> candidateNames = new List<string>();

            List<string> subjectNames = new List<string>();

            List<int> subjectIDs = new List<int>();

            chartDataTable.Columns.Add("ShortName");

            chartDataTable.Columns.Add("Name");

            chartDataTable.Columns.Add("ID");

            chartDataTable.Columns.Add("Max Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Min Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Avg Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Max Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Min Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Avg Score", Type.GetType("System.Decimal"));


            foreach (DataRow objRow in rawDatatable.Rows)
            {

                if (!candidateNames.Contains(objRow["CANDIDATE_NAME"].ToString().Trim()))
                {
                    string combinedSessionKeyCandidateName = string.Empty;
                    string relcombinedSessionKeyCandidateName = string.Empty;

                    if ((objRow["CANDIDATE_NAME"].ToString().Trim() != "MAX SCORE") &&
                      (objRow["CANDIDATE_NAME"].ToString().Trim() != "MIN SCORE") &&
                      (objRow["CANDIDATE_NAME"].ToString().Trim() != "AVG SCORE"))
                    {
                        combinedSessionKeyCandidateName =
                        string.Concat(objRow["CANDIDATE_NAME"].ToString(), "-",
                        objRow["CANDIDATE_SESSION_KEY"].ToString(), "-",
                         objRow["ATTEMPT_ID"].ToString());

                        relcombinedSessionKeyCandidateName = string.Concat("Rel-", objRow["CANDIDATE_NAME"].ToString(), "-",
                      objRow["CANDIDATE_SESSION_KEY"].ToString(), "-",
                       objRow["ATTEMPT_ID"].ToString());
                    }
                    else
                    {
                        combinedSessionKeyCandidateName =
                        objRow["CANDIDATE_NAME"].ToString();
                        relcombinedSessionKeyCandidateName =
                            string.Concat("Rel-", objRow["CANDIDATE_NAME"].ToString());
                    }
                    candidateNames.Add(combinedSessionKeyCandidateName);
                    candidateNames.Add(relcombinedSessionKeyCandidateName);

                    if ((objRow["CANDIDATE_NAME"].ToString().Trim() != "MAX SCORE") &&
                        (objRow["CANDIDATE_NAME"].ToString().Trim() != "MIN SCORE") &&
                        (objRow["CANDIDATE_NAME"].ToString().Trim() != "AVG SCORE"))
                    {
                        int index = chartDataTable.Columns.IndexOf(combinedSessionKeyCandidateName);

                        if (index == -1)
                            chartDataTable.Columns.Add(combinedSessionKeyCandidateName,
                                Type.GetType("System.Decimal"));

                        int relIndex = chartDataTable.Columns.IndexOf(relcombinedSessionKeyCandidateName);
                        if (relIndex == -1)
                            chartDataTable.Columns.Add(relcombinedSessionKeyCandidateName, Type.GetType("System.Decimal"));
                    }
                }

                if (!subjectNames.Contains(objRow["SUBJECT_NAME"].ToString().Trim()) ||
                    !subjectIDs.Contains(int.Parse(objRow["CAT_SUB_ID"].ToString())))
                {
                    subjectNames.Add(objRow["SUBJECT_NAME"].ToString().Trim());
                    subjectIDs.Add(int.Parse(objRow["CAT_SUB_ID"].ToString()));
                    DataRow dataRow = chartDataTable.NewRow();
                    if (objRow["SUBJECT_NAME"].ToString().Trim().Length > 15)
                    {
                        dataRow["ShortName"] = objRow["SUBJECT_NAME"].ToString().Trim().Substring(0, 15);
                    }
                    else
                    {
                        dataRow["ShortName"] = objRow["SUBJECT_NAME"].ToString().Trim();
                    }

                    dataRow["ID"] = int.Parse(objRow["CAT_SUB_ID"].ToString().Trim());

                    dataRow["Name"] = objRow["SUBJECT_NAME"].ToString().Trim();

                    chartDataTable.Rows.Add(dataRow);
                }
            }

            foreach (DataRow dr in chartDataTable.Rows)
            {
                foreach (DataColumn dc in chartDataTable.Columns)
                {
                    if (dc.ColumnName != "Name" && dc.ColumnName != "ShortName" && dc.ColumnName != "ID")
                    {
                        string[] columnName = dc.ColumnName.Split(new char[] { '-' });

                        DataRow[] datarow = null;

                        //if the column name contains relative score 
                        if ((dc.ColumnName.Length > 3) && (dc.ColumnName.Substring(0, 3) == "Rel"))
                        {
                            if (columnName.Length > 2)
                            {
                                datarow = rawDatatable.Select("SUBJECT_NAME = '" + dr["Name"] +
                                "'AND CANDIDATE_NAME = '" + columnName[1] + "'AND CANDIDATE_SESSION_KEY = '"
                                + (columnName[2]) + "'AND ATTEMPT_ID = '" + (columnName[3]) + "'AND CAT_SUB_ID ='" +
                                int.Parse(dr["ID"].ToString()) + "'");
                            }
                            else
                            {
                                datarow = rawDatatable.Select("SUBJECT_NAME = '" + dr["Name"] +
                                "'AND CANDIDATE_NAME = '" + columnName[1] + "'AND CAT_SUB_ID ='" +
                                int.Parse(dr["ID"].ToString()) + "'");
                            }
                            if (datarow != null)
                                dr[dc.ColumnName] = Convert.ToDecimal(datarow[0].ItemArray[4].ToString());
                        }
                        else
                        {
                            if (columnName.Length > 1)
                            {
                                datarow = rawDatatable.Select("SUBJECT_NAME = '" + dr["Name"] +
                                "'AND CANDIDATE_NAME = '" + columnName[0] + "'AND CANDIDATE_SESSION_KEY = '"
                                + (columnName[1]) + "'AND ATTEMPT_ID = '" + (columnName[2]) + "'AND CAT_SUB_ID ='" +
                                int.Parse(dr["ID"].ToString()) + "'");
                            }
                            else
                            {
                                datarow = rawDatatable.Select("SUBJECT_NAME = '" + dr["Name"] +
                                "'AND CANDIDATE_NAME = '" + columnName[0] + "'AND CAT_SUB_ID ='" +
                                int.Parse(dr["ID"].ToString()) + "'");
                            }
                            if (datarow != null)
                                dr[dc.ColumnName] = Convert.ToDecimal(datarow[0].ItemArray[3].ToString());
                        }
                    }
                }
            }

            return chartDataTable;
        }

        /// <summary>
        /// Represents the method to get the data table for the comparison 
        /// report test area details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A Datatable that holds the comparison's subject chart details
        /// </returns>
        public DataTable GetComparisonReportTestAreaDetails(CandidateReportDetail candidateReportDetail)
        {
            DataTable rawDatatable = new ReportDLManager().GetComparisonReportTestAreaDetails
                (candidateReportDetail);

            DataTable chartDataTable = new DataTable();

            List<string> candidateNames = new List<string>();

            List<string> testAreaNames = new List<string>();

            List<string> testAreaID = new List<string>();

            chartDataTable.Columns.Add("ShortName");

            chartDataTable.Columns.Add("Name");

            chartDataTable.Columns.Add("ID");

            chartDataTable.Columns.Add("Max Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Min Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Avg Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Max Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Min Score", Type.GetType("System.Decimal"));

            chartDataTable.Columns.Add("Rel-Avg Score", Type.GetType("System.Decimal"));


            foreach (DataRow objRow in rawDatatable.Rows)
            {
                if (!candidateNames.Contains(objRow["CANDIDATE_NAME"].ToString().Trim()))
                {
                    string combinedSessionKeyCandidateName = string.Empty;
                    string relCombinedSessionKeyCandidateName = string.Empty;

                    if ((objRow["CANDIDATE_NAME"].ToString().Trim() != "MAX SCORE") &&
                      (objRow["CANDIDATE_NAME"].ToString().Trim() != "MIN SCORE") &&
                      (objRow["CANDIDATE_NAME"].ToString().Trim() != "AVG SCORE"))
                    {
                        combinedSessionKeyCandidateName =
                        string.Concat(objRow["CANDIDATE_NAME"].ToString(), "-",
                        objRow["CANDIDATE_SESSION_KEY"].ToString(), "-",
                         objRow["ATTEMPT_ID"].ToString());

                        relCombinedSessionKeyCandidateName =
                       string.Concat("Rel-", objRow["CANDIDATE_NAME"].ToString(), "-",
                       objRow["CANDIDATE_SESSION_KEY"].ToString(), "-",
                        objRow["ATTEMPT_ID"].ToString());
                    }
                    else
                    {
                        combinedSessionKeyCandidateName =
                        objRow["CANDIDATE_NAME"].ToString();
                        relCombinedSessionKeyCandidateName =
                           string.Concat("Rel-", objRow["CANDIDATE_NAME"].ToString());
                    }

                    candidateNames.Add(combinedSessionKeyCandidateName);
                    if ((objRow["CANDIDATE_NAME"].ToString().Trim() != "MAX SCORE") &&
                        (objRow["CANDIDATE_NAME"].ToString().Trim() != "MIN SCORE") &&
                        (objRow["CANDIDATE_NAME"].ToString().Trim() != "AVG SCORE"))
                    {
                        int index = chartDataTable.Columns.IndexOf(combinedSessionKeyCandidateName);

                        if (index == -1)
                            chartDataTable.Columns.Add(combinedSessionKeyCandidateName, Type.GetType("System.Decimal"));

                        int relIndex = chartDataTable.Columns.IndexOf(relCombinedSessionKeyCandidateName);
                        if (relIndex == -1)
                            chartDataTable.Columns.Add(relCombinedSessionKeyCandidateName, Type.GetType("System.Decimal"));
                    }
                }

                if (!testAreaNames.Contains(objRow["TEST_AREA_NAME"].ToString().Trim()))
                {
                    testAreaNames.Add(objRow["TEST_AREA_NAME"].ToString().Trim());
                    DataRow dataRow = chartDataTable.NewRow();
                    if (objRow["TEST_AREA_NAME"].ToString().Trim().Length > 15)
                    {
                        dataRow["ShortName"] = objRow["TEST_AREA_NAME"].ToString().Trim().Substring(0, 15);
                    }
                    else
                    {
                        dataRow["ShortName"] = objRow["TEST_AREA_NAME"].ToString().Trim();
                    }

                    dataRow["Name"] = objRow["TEST_AREA_NAME"].ToString().Trim();
                    dataRow["ID"] = objRow["TEST_AREA_ID"].ToString().Trim();

                    chartDataTable.Rows.Add(dataRow);
                }
            }

            foreach (DataRow dr in chartDataTable.Rows)
            {
                foreach (DataColumn dc in chartDataTable.Columns)
                {
                    if (dc.ColumnName != "Name" && dc.ColumnName != "ShortName" && dc.ColumnName != "ID")
                    {
                        string[] columnName = dc.ColumnName.Split(new char[] { '-' });
                        DataRow[] datarow = null;

                        if ((dc.ColumnName.Length > 3) && (dc.ColumnName.Substring(0, 3) == "Rel"))
                        {
                            if (columnName.Length > 2)
                            {
                                datarow = rawDatatable.Select("TEST_AREA_NAME = '" + dr["Name"] +
                      "'AND CANDIDATE_NAME = '" + (columnName[1]) + "' AND CANDIDATE_SESSION_KEY = '"
                      + (columnName[2]) + "'AND ATTEMPT_ID = '" + (columnName[3]) + "'");
                            }
                            else
                            {
                                datarow = rawDatatable.Select("TEST_AREA_NAME = '" + dr["Name"] +
                     "'AND CANDIDATE_NAME = '" + columnName[1] + "'");
                            }
                            if (datarow != null)
                                dr[dc.ColumnName] = Convert.ToDecimal(datarow[0].ItemArray[4].ToString());
                        }
                        else
                        {
                            if (columnName.Length > 1)
                            {
                                datarow = rawDatatable.Select("TEST_AREA_NAME = '" + dr["Name"] +
                      "'AND CANDIDATE_NAME = '" + (columnName[0]) + "' AND CANDIDATE_SESSION_KEY = '"
                      + (columnName[1]) + "'AND ATTEMPT_ID = '" + (columnName[2]) + "'");
                            }
                            else
                            {
                                datarow = rawDatatable.Select("TEST_AREA_NAME = '" + dr["Name"] +
                     "'AND CANDIDATE_NAME = '" + columnName[0] + "'");
                            }
                            if (datarow != null)
                                dr[dc.ColumnName] = Convert.ToDecimal(datarow[0].ItemArray[3].ToString());
                        }
                    }
                }
            }

            return chartDataTable;
        }

        /// <summary>
        /// Represents the method to get the over all score details 
        /// for the group analysis report
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="OverAllCandidateScoreDetail"/>that holds the over all score details 
        /// for the group analysis report
        /// </returns>
        public OverAllCandidateScoreDetail GetGroupAnalysisOverallScoreDetails
            (CandidateReportDetail candidateReportDetail)
        {
            return new ReportDLManager().GetGroupAnalysisOverallScoreDetails(candidateReportDetail);

        }
        /// <summary>
        /// Reprsents the method to get the data table
        /// that holds the group analysis category chart details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="DataTable"/>that holds the group analysis chart 
        /// details 
        /// </returns>
        public DataTable GetGroupAnalysisReportCategoryDetails(CandidateReportDetail candidateReportDetail)
        {
            DataTable chartDataTable = new ReportDLManager().
                GetGroupAnalysisReportCategoryDetails(candidateReportDetail);

            return chartDataTable;
        }

        /// <summary>
        /// Reprsents the method to get the data table
        /// that holds the group analysis subject chart details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="DataTable"/>that holds the group analysis chart 
        /// details 
        /// </returns>
        public DataTable GetGroupAnalysisSubjectDetails(CandidateReportDetail candidateReportDetail)
        {
            DataTable chartDataTable = new ReportDLManager().
                GetGroupAnalysisReportSubjectDetails(candidateReportDetail);

            return chartDataTable;
        }

        /// <summary>
        /// Reprsents the method to get the data table
        /// that holds the group analysis subject chart details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="DataTable"/>that holds the group analysis chart 
        /// details 
        /// </returns>
        public DataTable GetGroupAnalysisTestAreaDetails(CandidateReportDetail candidateReportDetail)
        {
            DataTable chartDataTable = new ReportDLManager().
                GetGroupAnalysisReportTestAreaDetails(candidateReportDetail);
            return chartDataTable;
        }

        /// <summary>
        /// Reprsents the method to get the Candidate Statistics Detail
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail 
        /// </param>
        /// <returns>
        /// A<see cref="CandidateStatisticsDetail"/>that holds the Candidate Statistics Detail 
        /// </returns>
        public List<CandidateStatisticsDetail> GetCandidateReportCandidateStatistics
            (CandidateReportDetail candidateReportDetail)
        {
            return new ReportDLManager().GetCandidateReportCandidateStatistics(candidateReportDetail);

        }

        /// <summary>
        /// Method to get the completed candidate session key
        /// </summary>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the test key
        /// </returns>
        public string GetCompletedTestSessionKeys(string testKey)
        {
            return new ReportDLManager().GetCompletedTestSessionKeys(testKey);
        }
        #endregion






        
    }
}
