﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalResumeServiceBLManager.cs
// File that represents the business layer for the external resume
// service management. This will talk to the data layer to perform the 
// operations associated with external resume management processes.

#endregion

#region Directives                                                             
using System;
using System.Data;


using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the external resume
    /// service management. This will talk to the data layer to perform the 
    /// operations associated with external resume management processes.
    /// </summary>
    public class ExternalResumeServiceBLManager
    {
        #region Public Method                                                  
        /// <summary>
        /// Gets the dash board id.
        /// </summary>
        /// <param name="careerBuilderID">The career builder ID.</param>
        /// <returns></returns>
        public CareerBuilderResume GetDashBoardId(int careerBuilderID)
        {
            return new ExternalResumeServiceDLManager().GetCareerBuilderResume(careerBuilderID);
        }

        /// <summary>
        /// Inserts the career builder resume.
        /// </summary>
        /// <param name="CareerBuilderResume">The career builder resume.</param>
        public void InsertCareerBuilderResume(CareerBuilderResume CareerBuilderResume)
        {
            new ExternalResumeServiceDLManager().InsertCareerBuilderResume(CareerBuilderResume);
        }

        /// <summary>
        /// Inserts the career buider resume.
        /// </summary>
        /// <param name="externalResume">The external resume.</param>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="teanantID">The teanant ID.</param>
        /// <returns></returns>
        public int InsertCareerBuiderResume(ExternalResume externalResume, CandidateInformation candidateInformation, int userID, int teanantID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                int candidateID = 0;
                candidateID = new ExternalResumeServiceDLManager().InsertCandidateInformation(transaction, candidateInformation, userID, out candidateID);
                new ExternalResumeServiceDLManager().InsertTenanatCandidate(transaction, teanantID, candidateID, userID);
                externalResume.CandidateID = candidateID;
                externalResume.CreatedBy = userID;
                new ExternalResumeServiceDLManager().InsertExternalResume(transaction, externalResume);
                transaction.Commit();
                return candidateID;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }

        /// <summary>
        /// Gets the resume count.
        /// </summary>
        /// <param name="resumeID">The resume ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int GetExistingCandidateID(string resumeID, int? tenantID)
        {
            return new ExternalResumeServiceDLManager().GetExistingCandidateID(resumeID, tenantID);
        }

        /// <summary>
        /// Inserts the tenanat candidate.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="candidateID">The candidate ID.</param>
        /// <param name="userID">The user ID.</param>
        public void InsertTenanatCandidate(int tenantID, int candidateID, int userID)
        {

            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                new ExternalResumeServiceDLManager().InsertTenanatCandidate(transaction, tenantID, candidateID, userID);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }

        /// <summary>
        /// Gets the external resume.
        /// </summary>
        /// <param name="resumeID">The resume ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public ExternalResume GetExternalResume(string resumeID)
        {
            return new ExternalResumeServiceDLManager().GetExternalResume(resumeID);
        }

        public List<string> GetResumesID(int pageNo,int pageSize)
        {
            return new ExternalResumeServiceDLManager().GetResumesID(pageNo, pageSize);
        }
        #endregion
    }
}
