﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileBLManager.cs
// File that represents the business layer for the position profile module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the position profile 
    /// module. This will talk to the data layer to perform the operations 
    /// associated with the module.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class PositionProfileBLManager
    {
        /// <summary>
        /// Gets the position profile segment.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <returns></returns>
        public List<SegmentDetail> GetPositionProfileSegment(int formID)
        {
            return new PositionProfileDLManager().GetPositionProfileSegment(formID);
        }

        /// <summary>
        /// Gets the position profile.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="sortField">The sort field.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="clientPositionDetail">The client position detail.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public List<PositionProfileDetail> GetPositionProfile(string sortOrder, string sortField,
            int pageNumber, int pageSize, out int totalRecords, PositionProfileDetailSearchCriteria clientPositionDetail, int userID)
        {
            return new PositionProfileDLManager().GetPositionProfile
                (sortOrder, sortField, pageNumber, pageSize, out totalRecords, clientPositionDetail,userID);
        }

        /// <summary>
        /// Gets the form.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public List<FormDetail> GetForm(int? tenantID)
        {
            return new PositionProfileDLManager().GetForm(tenantID);
        }


        /// <summary>
        /// Gets the form.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public List<FormDetail> GetFormWithUsers(int? userID)
        {
            return new PositionProfileDLManager().GetFormWithUsers(userID);
        }

        /// <summary>
        /// Inserts the position profile and segment datas.
        /// </summary>
        /// <param name="positionProfileDetails">The position profile details.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        public int InsertPositionProfileAndSegmentDatas(PositionProfileDetail positionProfileDetails, List<ClientContactInformation> clientContactInfo,
            List<UserDetail> userDetails, int userID, int tenantID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            int positionProfileId = 0;
            try
            {
                string positionProfileKey =
                   new NextNumberDLManager().GetNextNumber(transaction, "CRN", userID);
                positionProfileDetails.PositionProfileKey = positionProfileKey.ToString();
                positionProfileId = new PositionProfileDLManager().InsertPositionProfile(transaction, positionProfileDetails, userID, tenantID);
                if (positionProfileId > 0)
                {
                    if (positionProfileDetails.Segments != null)
                    {
                        for (int i = 0; i < positionProfileDetails.Segments.Count; i++)
                        {
                            SegmentDetail segmentDetail = new SegmentDetail();
                            segmentDetail = positionProfileDetails.Segments[i];
                            //segmentDetail.DisplayOrder = i + 1;
                            if (!Support.Utility.IsNullOrEmpty(segmentDetail.DataSource))
                            {
                                new PositionProfileDLManager().InsertPositionProfileSegmentData(transaction, segmentDetail, userID, positionProfileId);
                            }
                        }
                    }
                    if (clientContactInfo != null)
                    {
                        for (int i = 0; i < clientContactInfo.Count; i++)
                        {
                            new PositionProfileDLManager().InsertPositionProfileContactDetails(clientContactInfo[i], positionProfileId, userID, transaction);
                        }
                    }

                    positionProfileDetails.PositionProfileKeywords.PositionProfileID = positionProfileId;
                    new PositionProfileDLManager().InsertPositionProfileKeywords(transaction, positionProfileDetails.PositionProfileKeywords, userID);

                    if (!Support.Utility.IsNullOrEmpty(positionProfileDetails.PositionProfileDictinaryKeywords))
                    {
                        foreach (PositionProfileKeywordDictionary keyword in positionProfileDetails.PositionProfileDictinaryKeywords)
                        {
                            new PositionProfileDLManager().InsertPositionProfileKeywordDictionary(transaction, keyword, userID);
                        }
                    }
                    //To add position profile owners
                    if (userDetails != null & userDetails.Count > 0)
                    {
                        foreach (UserDetail userDetail in userDetails)
                        {
                            new PositionProfileDLManager().InsertPositionProfileOwners(positionProfileId, userDetail.OwnerType,
                                userDetail.UserID, userID, transaction);
                        }
                    }

                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }
                return positionProfileId;
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Gets the position profile.
        /// </summary>
        /// <param name="positionProfileID">The position profile ID.</param>
        /// <returns></returns>
        public PositionProfileDetail GetPositionProfile(int positionProfileID)
        {
            return new PositionProfileDLManager().GetPositionProfile(positionProfileID);
        }

        public List<SegmentDetail> GetPositionProfileSegementList(int positionProfileID)
        {
            return new PositionProfileDLManager().GetPositionProfileSegementList(positionProfileID);
        }

        /// <summary>
        /// Updates the position profile and segment datas.
        /// </summary>
        /// <param name="positionProfileDetails">The position profile details.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int UpdatePositionProfileAndSegmentDatas(PositionProfileDetail positionProfileDetails, int userID, int tenantID,
            List<ClientContactInformation> clientContactInfo)
        {
            int positionProfileId = 0;
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                positionProfileId = new PositionProfileDLManager().UpdatePositionProfile(transaction, positionProfileDetails, userID, tenantID);
                if (positionProfileId > 0)
                {
                    new PositionProfileDLManager().DeleteSegmentForPositionProfile(transaction, positionProfileDetails.PositionID);
                    for (int i = 0; i < positionProfileDetails.Segments.Count; i++)
                    {
                        SegmentDetail segmentDetail = new SegmentDetail();
                        segmentDetail = positionProfileDetails.Segments[i];
                        segmentDetail.DisplayOrder = i + 1;
                        if (!Support.Utility.IsNullOrEmpty(segmentDetail.DataSource))
                        {
                            new PositionProfileDLManager().InsertPositionProfileSegmentData(transaction, segmentDetail, userID, positionProfileDetails.PositionID);    
                        }
                    }
                    if (clientContactInfo != null)
                    {
                        new PositionProfileDLManager().DeletePPContactDetails(positionProfileDetails.PositionID, transaction);
                        for (int i = 0; i < clientContactInfo.Count; i++)
                        {
                            new PositionProfileDLManager().InsertPositionProfileContactDetails(clientContactInfo[i], positionProfileDetails.PositionID, userID, transaction);
                        }
                    }

                    positionProfileDetails.PositionProfileKeywords.PositionProfileID = positionProfileDetails.PositionID;
                    new PositionProfileDLManager().UpdatePositionProfileKeywords(transaction, positionProfileDetails.PositionProfileKeywords, userID);
                    if (!Support.Utility.IsNullOrEmpty(positionProfileDetails.PositionProfileDictinaryKeywords))
                    {
                        foreach (PositionProfileKeywordDictionary keyword in positionProfileDetails.PositionProfileDictinaryKeywords)
                        {
                            new PositionProfileDLManager().InsertPositionProfileKeywordDictionary(transaction, keyword, userID);
                        }
                    }
                    transaction.Commit();
                }
                return positionProfileId;
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }


        /// <summary>
        /// Update position profile basic information.
        /// </summary>
        /// <param name="positionProfileDetails">The position profile details.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int UpdatePositionProfileBasicInfo(PositionProfileDetail positionProfileDetails, int userID, int tenantID,
            List<ClientContactInformation> clientContactInfo)
        {
            int positionProfileId = 0;
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                positionProfileId = new PositionProfileDLManager().UpdatePositionProfile(transaction, 
                    positionProfileDetails, userID, tenantID);
                if (positionProfileId > 0)
                {
                    if (clientContactInfo != null)
                    {
                        new PositionProfileDLManager().DeletePPContactDetails(positionProfileDetails.PositionID, transaction);
                        for (int i = 0; i < clientContactInfo.Count; i++)
                        {
                            new PositionProfileDLManager().InsertPositionProfileContactDetails(clientContactInfo[i], 
                                positionProfileDetails.PositionID, userID, transaction);
                        }
                    }
                    transaction.Commit();
                }
                return positionProfileId;
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Insert the position profile segment data.
        /// </summary>
        /// <param name="segmentDetail">The segment detail.</param>
        /// <param name="positionProfileID">The position profile id.</param>
        /// <param name="userID">The user id.</param>
        public void InsertPositionProfileSegment(SegmentDetail segmentDetail,
            int positionProfileID,int userID)
        {
            new PositionProfileDLManager().InsertPositionProfileSegment(segmentDetail,
                positionProfileID, userID);
        }

        /// <summary>
        /// Deletes segment by segment id and position profile id.
        /// </summary>
        /// <param name="positionProfileID">The position profile id.</param>
        /// <param name="segmentID">The segment id.</param>
        public void DeletePositionProfileSegment(int positionProfileID, int segmentID)
        {
            new PositionProfileDLManager().DeletePositionProfileSegment(positionProfileID, segmentID);
        }

        /// <summary>
        /// Update position profile segment display order.
        /// </summary>
        /// <param name="positionProfileID">The position profile id.</param>
        /// <param name="segmentID">The segment id.</param>
        /// <param name="displayOrder">The display order.</param>
        public void UpdatePositionProfileSegmentDisplayOrder(int positionProfileID,
            int segmentID, int displayOrder)
        {
            new PositionProfileDLManager().UpdatePositionProfileSegmentDisplayOrder(positionProfileID,
                segmentID, displayOrder);
        }

        /// <summary>
        /// Updates the position profile user options.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdatePositionProfileUserOptions(int formID, int userID)
        {
            new PositionProfileDLManager().UpdatePositionProfileUserOptions(formID, userID);
        }

        /// <summary>
        /// Gets the predefined form.
        /// </summary>
        /// <returns></returns>
        public List<FormDetail> GetPredefinedForm()
        {
            return new PositionProfileDLManager().GetPredefinedForm();

        }

        /// <summary>
        /// Method that deletes a position profile.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile.
        /// </param>
        /// <remarks>
        /// This will do a soft deletion.
        /// </remarks>
        public void DeletePositionProfile(int positionProfileID)
        {
            new PositionProfileDLManager().DeletePositionProfile(positionProfileID);
        }

        /// <summary>
        /// Update the open/close status in Position profile.
        /// </summary>
        /// <param name="positionProfileID">The position profile ID.</param>
        public void UpdateClosedStatusInPositionProfile(int positionProfileID,string IsClosed,int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                new PositionProfileDLManager().UpdateClosedStatusInPositionProfile(transaction, positionProfileID, IsClosed, userID);
                transaction.Commit();
            }

            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }

        }
        /// <summary>
        /// Gets the position profile keyword dictionary.
        /// </summary>
        /// <param name="prefixKeyword">The prefix keyword.</param>
        /// <param name="skillType">Type of the skill.</param>
        /// <param name="skillCategory">The skill category.</param>
        /// <returns></returns>
        public string[] GetPositionProfileKeywordDictionary(string prefixKeyword, SkillType skillType,string skillCategory)
        {
            return new PositionProfileDLManager().GetPositionProfileKeywordDictionary(prefixKeyword, skillType, skillCategory);
        }

        /// <summary>
        /// Gets the position profile skill categroy keyword.
        /// </summary>
        /// <param name="prefixKeyword">The prefix keyword.</param>
        /// <returns></returns>
        public string[] GetPositionProfileSkillCategroyKeyword(string prefixKeyword)
        {
            return new PositionProfileDLManager().GetPositionProfileSkillCategroy(prefixKeyword);
        }
        
        /// <summary>
        /// A Method that gets the keyword based on the position profile id
        /// </summary>
        /// <param name="PositionProfileId">
        /// A <see cref="System.Int32"/> that holds the position profile id
        /// </param>
        /// <returns>
        /// A <see cref="Forte.HCM.DataObjects.PositionProfileDetail"/> that holds the position profile details
        /// based on the position profile id
        /// </returns>
        public PositionProfileDetail GetPositionProfileKeyWord(int PositionProfileId)
        {
            return new PositionProfileDLManager().GetPositionProfileKeyWord(PositionProfileId);
        }

        /// <summary>
        /// Gets the position profile segments for form.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <returns></returns>
        public List<Segment> GetPositionProfileSegmentsForForm(int formID)
        {
            return new PositionProfileDLManager().GetPositionProfileSegmentsForForm(formID);
        }


        //public List<PositionProfileDetail> GetPositionProfileContactDetails(int positionProfileID)
        //{
        //    return new PositionProfileDLManager().GetPositionProfileContactDetails(positionProfileID);
        //}

        public PositionProfileDashboard GetPositionProfileStatus(int positionProfileID)
        {
          return new PositionProfileDLManager().GetPositionProfileStatus(positionProfileID);
        }

        public List<CandidateInformation> GetPositionProfileCandidates(int positionProfileID,
              string orderBy, string candidateIds, int pageNumber, int pageSize, string filterBy, int recruiterId, string candidateStatus, out int totalRecords)
        {
            return new PositionProfileDLManager().GetPositionProfileCandidates(positionProfileID, orderBy, candidateIds, pageNumber,
                pageSize,filterBy,recruiterId,candidateStatus, out totalRecords);
        }


        /// <summary>
        /// Method that retrieves the position profile candidate skill score
        /// detail for the given position profile and candidate ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileCandidateSkillScoreDetail"/> that holds
        /// the position profile candidate skill score detail.
        /// </returns>
        public PositionProfileCandidateSkillScoreDetail
            GetPositionProfileCandidateSkillScore(int positionProfileID, int candidateID)
        {
            return new PositionProfileDLManager().GetPositionProfileCandidateSkillScore
                (positionProfileID, candidateID);
        }

        public List<PositionProfileActivityLog> GetPositionProfileActivities
            (PositionProfileActivityLog searchCriteria, int pageNumber,
            int pageSize, string sortField, SortType sortOrder,
            out int totalRecords)
        {
            return new PositionProfileDLManager().GetPositionProfileActivities
                (searchCriteria, pageNumber, pageSize, sortField, 
                sortOrder, out totalRecords);
        }

        public DataTable GetPositionProfileActivitiesTable
            (PositionProfileActivityLog searchCriteria, int pageNumber, int pageSize,
            string sortField, SortType sortOrder, out int totalRecords)
        {
            return new PositionProfileDLManager().GetPositionProfileActivitiesTable
                (searchCriteria, pageNumber, pageSize, sortField,
                sortOrder, out totalRecords);
        }

        public PositionProfileDetail GetPositionProfileInformation(int positionProfileId)
        {
            return new PositionProfileDLManager().GetPositionProfileInformation(positionProfileId);
        }

        /// <summary>
        /// Method that retrieves the candidate activity summary for the given
        /// position profile and candidate ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateActivitySummaryDetail"/> that holds
        /// the position profile candidate activity log detail.
        /// </returns>
        public CandidateActivitySummaryDetail
            GetCandidateActivitySummary(int positionProfileID, int candidateID)
        {
            return new PositionProfileDLManager().GetCandidateActivitySummary
                (positionProfileID, candidateID);
        }

        /// <summary>
        /// Method that retrieves the candidate activity summary for the given
        /// position profile and candidate ID as a data table for export to 
        /// excel.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateActivitySummaryDetail"/> that holds
        /// the position profile candidate activity log detail.
        /// </returns>
        public DataTable GetCandidateActivitySummaryTable
            (int positionProfileID, int candidateID)
        {
            return new PositionProfileDLManager().GetCandidateActivitySummaryTable
                (positionProfileID, candidateID);
        }

        public CandidateInformation GetPositionProfileCandidateInformation(int positionProfileStatusID)
        {
            return new PositionProfileDLManager().GetPositionProfileCandidateInformation(positionProfileStatusID);
        }

        public void GetPositionProfileCandidateTempIds(string sessionKey, out string canidateIds, out string keywords)
        {
            new PositionProfileDLManager().GetPositionProfileCandidateTempIds(sessionKey, out canidateIds, out keywords);
        }

        /// <summary>
        /// Method that inserts the scheduled candidates into the position
        /// profile candidate table against candidate test & interview session 
        /// IDs and position profile.
        /// </summary>
        /// <param name="positionProfileCandidate">
        /// A <see cref="string"/> that holds the position profile candidate 
        /// details.
        /// </param>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. Value T indicates test
        /// and I indicates interview.
        /// </param>
        public void InsertPositionProfileCandidate(PositionProfileCandidate positionProfileCandidate, string type)
        {
            new TestDLManager().InsertPositionProfileCandidate(positionProfileCandidate, type, null);
        }

        public void InsertPositionProfileTSCandidate(string sessionKey, int positionProfileID)
        {
            new PositionProfileDLManager().InsertPositionProfileTSCandidate(sessionKey, positionProfileID);
        }

        /// <summary>
        /// Method that retrieves the position profile keyword dictionary pattern. 
        /// The pattern is build for technical skills, verticals and roles.
        /// </summary>
        /// <returns>
        /// A <see cref="PositionProfileKeywordPattern"/> that holds the keyword 
        /// dictionary pattern.
        /// </returns>
        public PositionProfileKeywordPattern GetKeywordDictionaryPattern()
        {
            return new PositionProfileDLManager().GetKeywordDictionaryPattern();
        }

         /// <summary>
        /// Method that retrieves the position profile keyword dictionary list.. 
        /// The list is build for technical skills, verticals and roles.
        /// </summary>
        /// <returns>
        /// A list of <see cref="KeywordSkill"/> that holds the keyword dictionary 
        /// list.
        /// </returns>
        public List<KeywordSkill> GetKeywordDictionaryList()
        {
            return new PositionProfileDLManager().GetKeywordDictionaryList();
        }

        /// <summary>
        /// Method that retrieves the position profile summary for the given 
        /// position profile ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="string"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetPositionProfileSummary(int positionProfileID, int userID)
        {
            return new PositionProfileDLManager().GetPositionProfileSummary
                (positionProfileID, userID);
        }

        /// <summary>
        /// Method that retrieves the position profile segments for the given 
        /// position profile ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <returns>
        /// A <see cref="SegmentDetail"/> that holds the segment data.
        /// </returns>
        public List<SegmentDetail> GetPositionProfileSegementListByPositionProfileID(int positionProfileID)
        {
            return new PositionProfileDLManager().GetPositionProfileSegementListByPositionProfileID(positionProfileID);
        }

        /// <summary>
        /// Method to get the position profile owner by position
        /// profile id and owner type
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail
        /// </returns>
        public List<UserDetail> GetPositionProfileOwnersByIDAndOwnerType(int positionProfileID,
            string ownerType)
        {
            return new PositionProfileDLManager().
                GetPositionProfileOwnersByIDAndOwnerType(positionProfileID, ownerType);
        }

        /// <summary>
        /// Method that retreives the recruiter detail.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="positionStatus">
        /// A <see cref="int"/> that holds the position profile status.
        /// </param>
        /// <param name="type">
        /// A <see cref="int"/> that holds the retrieve list type.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recent month.
        /// </param>
        /// <param name="spType">
        /// A <see cref="int"/> that holds the stored procedure type.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterDetail> GetRecruiter(int tenantID, int positionProfileID, 
                string positionStatus, string type, int recruiterID, int recentMonth, string spType,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new PositionProfileDLManager().GetRecruiter(tenantID, positionProfileID, 
                positionStatus, type, recruiterID, recentMonth, spType,
                pageNumber,  pageSize,  sortField, sordOrder, out totalRecords);
        }


        /// <summary>
        /// Method that retreives the recruiter detail.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterDetail> GetAllRecruiter(int tenantID,
            int pageNumber, int pageSize, string sortField, SortType sordOrder,
            out int totalRecords)
        {
            return new PositionProfileDLManager().GetAllRecruiter(tenantID,
               pageNumber, pageSize, sortField, sordOrder, out totalRecords);
        }

        /// <summary>
        /// Method that retreives the recruiter assessment detail.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="positionStatus">
        /// A <see cref="int"/> that holds the position profile status.
        /// </param>
        /// <param name="type">
        /// A <see cref="int"/> that holds the retrieve list type.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recent month.
        /// </param>
        /// <param name="spType">
        /// A <see cref="int"/> that holds the stored procedure type.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterAssignmentDetail"/> that holds the 
        /// recruiter assessment details.
        /// </returns>
        public List<RecruiterAssignmentDetail> GetRecruiterAssignmentDetail(int tenantID, int positionProfileID,
                string positionStatus, string type, int recruiterID, int recentMonth, string spType,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            return new PositionProfileDLManager().GetRecruiterAssignmentDetail(tenantID, positionProfileID,
                positionStatus, type, recruiterID, recentMonth, spType,
                pageNumber, pageSize, sortField, sordOrder, out totalRecords);
        }

         /// <summary>
        /// Inserts the position profile owners.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="object"/> that holds the user detail.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertPositionProfileOwners(List<UserDetail> userDetails, int positionProfileID, string ownerType, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                if (userDetails != null & userDetails.Count > 0)
                {
                    new PositionProfileDLManager().DeletePositionProfileOwners(positionProfileID, ownerType,
                        transaction);
                    foreach (UserDetail userDetail in userDetails)
                    {
                        new PositionProfileDLManager().InsertPositionProfileOwners(positionProfileID, ownerType,
                            userDetail.UserID, userID, transaction);
                    }
                    transaction.Commit();
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retreives the candidate details.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the position Profile ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInformation"/> that holds the 
        /// candidate information details.
        /// </returns>
        public List<CandidateInformation> GetPositionProfileCandidatesByPositionProfileID(int positionProfileID)
        {
            return new PositionProfileDLManager().
                GetPositionProfileCandidatesByPositionProfileID(positionProfileID);
        }

        /// <summary>
        /// Method that retrives the position profile associate candidates count
        /// </summary>
        /// <param name="positionProfileId">
        /// A<see cref="int"/> that holds the position profile id
        /// </param>
        /// <returns></returns>
        public List<string> GetPositionProfileCandidateAssociationCount(int positionProfileId, out string owner)
        {
            return new PositionProfileDLManager().
                GetPositionProfileCandidateAssociationCount(positionProfileId, out owner);
        }

        public InterviewSessionSearchCriteria GetPositionProfileInterviews(int positionProfileId, out string interviewOwner)
        {
            return new PositionProfileDLManager().GetPositionProfileInterviews(positionProfileId, out interviewOwner);
        }

        public List<InterviewSessionDetail> GetPositionProfileInterviewSessionCount(int positionProfileId )
        {
            return new PositionProfileDLManager().GetPositionProfileInterviewSessionCount(positionProfileId );
        }
        public List<string> GetPositionProfileInterviewAssessment(int positionProfileId, int userId, out string createBy)
        {
            return new PositionProfileDLManager().GetPositionProfileInterviewAssessment(
                positionProfileId, userId, out createBy);
        }
        public void InsertPositionProfileCandidates(PositionProfileCandidate positionProfileCandidate,
           IDbTransaction transaction) 
        {
              new PositionProfileDLManager().InsertPositionProfileCandidates(positionProfileCandidate, transaction);
        }

        public int GetPositionProfileRecruiterAssignment(int positionProfileId, string ownerType)
        {
           return new PositionProfileDLManager().GetPositionProfileRecruiterAssignment(positionProfileId, ownerType);
        }

        /// <summary>
        /// Retrieves position profile workflow selection details.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        public PositionProfieWorkflowDetail GetPositionProfileWorkflowSelection(int positionProfileID)
        {
            return new PositionProfileDLManager().GetPositionProfileWorkflowSelection(positionProfileID);
        }

        /// <summary>
        /// Insert the position profile actions.
        /// </summary>
        /// <param name="PositionProfileDetail">
        /// A <see cref="object"/> that holds the action type details.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertPositionProfileActions(List<PositionProfileDetail> positionProfileActions,
            int positionProfileID, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                if (positionProfileActions != null & positionProfileActions.Count > 0)
                {
                    
                    //Delete position profile actions
                    new PositionProfileDLManager().DeletePositionProfileActions(positionProfileID, transaction);

                    //Insert position profile actions
                    foreach (PositionProfileDetail positionProfileAction in positionProfileActions)
                    {
                        new PositionProfileDLManager().InsertPositionProfileActions(positionProfileID, positionProfileAction.PositionProfileOwnerAction,
                            userID, transaction);
                    }

                    transaction.Commit();
                }
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retreives the task owner details.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile id.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the 
        /// task owner details.
        /// </returns>
        public List<UserDetail> GetPositionProfileTaskOwners(int positionProfileID, int userID)
        {
            return new PositionProfileDLManager().
                GetPositionProfileTaskOwners(positionProfileID, userID);
        }

        /// <summary>
        /// Method to delete the position profile owner against position 
        /// profile id and owner type
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        public void DeletePositionProfileOwner(int positionProfileID,
            string ownerType, int userID)
        {
             new PositionProfileDLManager().
                 DeletePositionProfileOwner(positionProfileID, ownerType,userID);
        }

        /// <summary>
        /// Inserts the position profile owner.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="createdBy">
        /// A <see cref="int"/> that holds the createdBy.
        /// </param>
        public void InsertPositionProfileOwner(int positionProfileID, string ownerType, 
            int userID, int createdBy)
        {
            new PositionProfileDLManager().InsertPositionProfileOwner(positionProfileID, 
                ownerType, userID, createdBy);
        }

        /// <summary>
        /// Insert the position profile default owners.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertPositionProfileDefaultOwner(int positionProfileID, int userID)
        {
            new PositionProfileDLManager().InsertPositionProfileDefaultOwner(positionProfileID,
                userID);
        }

        public void UpdatePositionProfileStatus(string status, string comments, int userId,
            int positionProfileId, int candidateId, string updateType)
        {
              new PositionProfileDLManager().UpdatePositionProfileStatus(status,comments,
               userId, positionProfileId, candidateId, updateType);
        }

        public bool InserttPositionProfileAdditionalDocument(int positionProfileId, int candidateId,
          int userId, string fileName, byte[] documentSource)
        {
            return new PositionProfileDLManager().InserttPositionProfileAdditionalDocument(positionProfileId, 
                candidateId, userId, fileName, documentSource);

        }
        public List<CareerBuilderResume> GetCandidateAdditionalDocuments(int positionProfileId, int candidateId)
        {
            return new PositionProfileDLManager().GetCandidateAdditionalDocuments(positionProfileId, candidateId);
        }

        public void DeleteAdditionalDocument(int documentId)
        {
            new PositionProfileDLManager().DeleteAdditionalDocument(documentId);
        }

        public byte[] GetAdditionalDocumentContent(int documentId)
        {
            return new PositionProfileDLManager().GetAdditionalDocumentContent(documentId);
        }

        /// <summary>
        /// Method that retreives the position profile owner rights for an user
        /// against the position profile.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="string"/> that holds the position profile owner
        /// rights.
        /// </returns>
        public List<string> GetPositionProfileOwnerRights(int positionProfileID, int userID)
        {
            return new PositionProfileDLManager().GetPositionProfileOwnerRights(positionProfileID, userID);
        }

        /// <summary>
        /// Method that retreives the position profile review rights for an user
        /// against the position profile id and tenant ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="string"/> that holds the position profile review
        /// rights.
        /// </returns>
        public List<string> GetPositionProfileReviewRights(int positionProfileID, int tenantID)
        {
            return new PositionProfileDLManager().GetPositionProfileReviewRights(positionProfileID, tenantID);
        }

         /// <summary>
        /// Method that retrieves the position profile owner details for the given
        /// position profile ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// A list of <see cref="PositionProfileOwnerDetail"/> that holds the position 
        /// profile owner details.
        /// </returns>
        public List<PositionProfileOwnerDetail> GetPositionProfileOwners(int positionProfileID)
        {
            return new PositionProfileDLManager().GetPositionProfileOwners(positionProfileID);
        }

        /// <summary>
        /// Method that retrieves the list of recruiter assignments.
        /// </summary>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <param name="positionProfileStatus">
        /// A <see cref="string"/> that holds the position profile status. This
        /// can be of PPS_OPEN, PPS_CLOSED or PPS_HOLD. When null is passed
        /// then will return all statuses.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterAssignmentDetail"/> that holds the 
        /// assignment details.
        /// </returns>
        public List<RecruiterAssignmentDetail> GetRecruiterAssignments
            (int recruiterID, string positionProfileStatus, int pageNumber,
            int pageSize, out int totalRecords)
        {
            return new PositionProfileDLManager().GetRecruiterAssignments
                (recruiterID, positionProfileStatus, pageNumber,
                pageSize, out totalRecords);
        }

        /// <summary>
        /// Method that retreives the recruiter detail.
        /// </summary>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <returns>
        /// A <see cref="RecruiterDetail"/> that holds the recruiter detail.
        /// </returns>
        public RecruiterDetail GetRecruiterByID(int recruiterID)
        {
            return new PositionProfileDLManager().GetRecruiterByID(recruiterID);
        }

        /// <summary>
        /// Method that retreives the position profile candidate detail by gen ID.
        /// </summary>
        /// <param name="genID">
        /// A <see cref="int"/> that holds the gen ID.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileCandidate"/> that holds the position profile 
        /// candidate detail.
        /// </returns>
        public PositionProfileCandidate GetPositionProfileCandidateDetailByGenID(int genID)
        {
            return new PositionProfileDLManager().
                GetPositionProfileCandidateDetailByGenID(genID);
        }

        /// <summary>
        /// Method that retrieves the list of assessed candidates against the
        /// given position profile ID & interview session key.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="showAll">
        /// A <see cref="bool"/> that indicates whether to show candidates across 
        /// all sessions against this position profile. True indicate show all
        /// and false indicates show only for the interview session key.
        /// </param>
        /// <param name="interviewSessionKey">
        /// A <see cref="string"/> that holds the interview session key.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInterviewSessionDetail"/> that holds the assessed candidates.
        /// </returns>
        public List<CandidateInterviewSessionDetail> GetAssessedCandidates
            (int positionProfileID, bool showAll, string interviewSessionKey)
        {
            return new PositionProfileDLManager().GetAssessedCandidates
                (positionProfileID, showAll, interviewSessionKey);
        }
    }
}