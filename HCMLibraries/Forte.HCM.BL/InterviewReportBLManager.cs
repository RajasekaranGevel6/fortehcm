﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewReportBLManager.cs
// File that represents the business layer for the interview report module.
// This will talk to the data layer to perform the operations associated
// with the module.
#endregion

#region Directives

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.Common.DL;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the interview report module.
    /// This includes functionalities to display various reports against the 
    /// interviews. This will talk to the data layer for performing these 
    /// operations.
    /// </summary>
    public class InterviewReportBLManager
    {
        public List<QuestionDetail> GetInterviewCandidateTestQuestionDetail(string testKey, string candidateSessionKey, int attemptID)
        {
            return new InterviewReportDLManager().GetInterviewCandidateTestQuestionDetail(testKey, candidateSessionKey, attemptID);
        }

        public CandidateStatisticsDetail GetInterviewCandidateTestResultStatisticsDetails
            (string candidateSessionKey, string testKey, int attemptID, string sessionKey, int assessorID)
        {
            //Call the method in Dl and sent back the results
            return new InterviewReportDLManager().GetInterviewCandidateTestResultStatisticsDetails
                 (candidateSessionKey, testKey, attemptID, sessionKey, assessorID);
        }

        /// <summary>
        /// Method that retrieves the interview response detail for the given 
        /// question.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="testQuestionID">
        /// A <see cref="int"/> that holds the test question ID.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewResponseDetail"/> that holds the response 
        /// detail.
        /// </returns>
        public InterviewResponseDetail GetInterviewResponseDetail
            (string candidateSessionID, int attemptID, int testQuestionID,int assessorID)
        {
            return new InterviewReportDLManager().GetInterviewResponseDetail
                (candidateSessionID, attemptID, testQuestionID,assessorID);
        }

        public void SaveQuestionDetailsWithRatingComments(QuestionDetail questionDetails,
            string candidateSessionKey, int attemptID, int assessorID, int userID)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                new InterviewReportDLManager().SaveQuestionDetailsWithRatingComments(questionDetails,
                    candidateSessionKey, attemptID, assessorID, userID,transaction);
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// this method represent the Test details based on search criteria.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A<see cref="string"/>that holds the test Search Criteria
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="string"/>that holds the page size
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="string"/>that holds the page number
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction Asc/desc
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total Records out parameter.
        /// </param>
        /// <returns>
        /// A list for <see cref="TestDetail"/> that holds the test detail
        /// </returns>
        public List<TestDetail> GetInterviewTests(TestSearchCriteria interviewSearchCriteria,
            int pageSize, int pageNumber, string orderBy, SortType orderByDirection,
            out int totalRecords)
        {
            return new InterviewReportDLManager().GetInterviewTests(interviewSearchCriteria, pageSize, pageNumber,
                orderBy, orderByDirection, out totalRecords);
        }

        /// <summary>
        /// Represents a method to get a name of the interview test
        /// when interview test id is passed
        /// </summary>
        /// <param name="interviewTestID">
        /// A<see cref="string"/>that holds the interview testid
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the interview test name
        /// </returns>
        public string GetInterviewTestName(string InterviewTestID)
        {
            return new InterviewReportDLManager().GetInterviewTestName(InterviewTestID);
        }

        /// <summary>
        /// Method to get the interview question statistics deatils for the question.
        /// In second tab of the interview test statistics info page
        /// </summary>
        /// <param name="interviewTestId">
        /// A<see cref="int"/> that holds the interview test id
        /// </param>
        /// <returns>
        /// A list for <see cref="QuestionStatisticsDetail"/>that holds the 
        /// question statistics details 
        /// </returns>
        public List<QuestionStatisticsDetail> GetInterviewQuestionStatistics(string InterviewTestId)
        {
            List<QuestionStatisticsDetail> questionStatisticsDetails =
                new InterviewReportDLManager().GetInterviewQuestionStatistics(InterviewTestId);

            return questionStatisticsDetails;
        }

        /// <summary>
        /// Method to get the interview test summary details of the interview test 
        /// </summary>
        /// <param name="interviewTestId">
        /// A<see cref="string"/>that holds the interview test id 
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the summary details
        /// </returns>
        public TestStatistics GetInterviewTestSummaryDetails(string interviewTestId)
        {
            return new InterviewReportDLManager().GetInterviewTestStatisticsDetails(interviewTestId);
        }

        /// <summary>
        /// Method to get the Question Detail based on the interview test id and questionId.
        /// </summary>
        /// <param name="interviewTestId">
        /// A<see cref="int"/> that holds the interview test id
        /// </param>
        /// <param name="questionId">
        /// A<see cref="string"/>that holds the question Id
        /// </param>
        /// <returns>
        /// A<see cref="QuestionDetail"/>that holds the Question Detail
        /// </returns>
        public QuestionDetail GetSingleInterviewQuestionDetails(string interviewTestId, string questionId)
        {
            return new InterviewReportDLManager().GetInterviewQuestion(interviewTestId, questionId);
        }

        public QuestionDetail GetSingleInterviewQuestionDetails(string interviewTestId, 
            string questionId, int attemptID, string candidateSessionKey)
        {
            return new InterviewReportDLManager().GetInterviewQuestion(interviewTestId, questionId, attemptID, candidateSessionKey);
        }

        /// <summary>
        /// Method to get the Interview Candidate Statistics Detail based on the search creteria.
        /// </summary>
        /// <param name="testStatisticsSearchCriteria">
        /// A<see cref="TestStatisticsSearchCriteria"/> that holds the Interview test StatisticsSearch Criteria
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/> that holds the order by field 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/> that holds the order by direction asc/desc
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/> that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total records out parameter
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateStatisticsDetail"/> that holds the interview candidate statistics detail
        /// </returns>
        public List<CandidateStatisticsDetail> GetInterviewReportCandidateStatisticsDetails
            (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy,
            string orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {
            return new InterviewReportDLManager().GetInterviewReportCandidateStatisticsDetails(testStatisticsSearchCriteria,
                orderBy, orderByDirection, pageNumber, pageSize, out totalRecords);
        }

        /// <summary>
        /// Returns question attribute object
        /// </summary>
        /// <param name="interviewTestKey">Contains interview test key</param>
        /// <param name="interviewTestKey">Contains question key</param>
        /// <returns></returns>
        public QuestionDetail GetInterviewTestQuestionAttributesByKeys(
            string interviewTestKey,string questionKey)
        {
            return new InterviewReportDLManager().
                GetInterviewTestQuestionAttributesByKeys(interviewTestKey,questionKey);
        }

        /// <summary>
        /// Method to get the additional details of interview test 
        /// </summary>
        /// <param name="interviewTestID">
        /// A<see cref="string"/>that holds the interview test key
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the summary details
        /// </returns>
        public TestStatistics GetAdditionalInterviewTestDetails(string interviewTestKey)
        {
            return new InterviewReportDLManager().GetAdditionalInterviewTestDetails(interviewTestKey);
        }
        /// <summary>
        ///  Method to get the assessor skills
        /// </summary>
        /// <param name="interviewTestkey">
        /// A<see cref="string"/>that holds the interview test key
        /// </param>
        /// <returns></returns>
        public int GetAssessorSkills(string ostSessionKey,string testQuestionID,string assessorID )
        {  
            return new InterviewReportDLManager().
                GetAssessorSkills(ostSessionKey, testQuestionID, assessorID);
        }
    }
}