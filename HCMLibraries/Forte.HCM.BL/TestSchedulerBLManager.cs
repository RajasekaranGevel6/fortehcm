﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestSchedulerDLManager.cs
// File that represents the data business for the Test Scheduler module.
// This includes functionalities such as schedule candidate, un-schedule
// candidate, re-schedule candidate, retrieve session, set test reminder,
// delete test reminder, etc. This will talk to the data layer to perform 
// these operations.

#endregion

#region Directives

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.SmptClientService;

#endregion

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the Test Scheduler module.
    /// This includes functionalities such as schedule candidate, un-schedule
    /// candidate, re-schedule candidate, retrieve session, set test reminder,
    /// delete test reminder, etc. This will talk to the data layer to perform 
    /// these operations.
    /// </summary>
    public class TestSchedulerBLManager
    {
        #region Public Method
        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestSessionDetail"/> that holds the candidate
        /// test session details.
        /// </returns>
        public CandidateTestSessionDetail GetCandidateTestSession(string candidateTestSessionID)
        {
            return new TestSchedulerBLManager().GetCandidateTestSession(candidateTestSessionID);
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that contains the candidate session id.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate session details.
        /// </returns>
        public CandidateTestSessionDetail GetCandidateTestSession(string candidateTestSessionID, int attemptID)
        {
            return new TestSchedulerDLManager().GetCandidateTestSession(candidateTestSessionID, attemptID);
        }

        /// <summary>
        /// Method that will call to load the candidate details 
        /// who scheduled for the tests.
        /// </summary>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/>that contains the current page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the candidate Information Like Name...
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column to be sorted.
        /// </param>
        /// <param name="orderDirection">
        /// A <see cref="SortType"/> that contains the sort direction either Asc/Desc.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestScheduleDetail"/> that contains candidate details.
        /// </returns>
        public List<TestScheduleDetail> GetScheduler
            (int user, int pageNumber, int pageSize, out int totalRecords,
            TestScheduleSearchCriteria scheduleSearchCriteria, string orderBy, SortType orderDirection)
        {
            return new TestSchedulerDLManager().GetScheduler
                (user, pageNumber, pageSize, out totalRecords,
                scheduleSearchCriteria, orderBy, orderDirection);
        }

        /// <summary>
        /// Method that will call to load the candidate test session details 
        /// </summary>
        /// <param name="testSessionID">
        /// A  <see cref="string"/> that contains the TestSession ID.
        /// </param>
        /// <param name="candidateTestSessionIDs">
        /// A list of <see cref="string"/> that contains the candidate TestSession IDs.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestScheduleDetail"/> that contains candidate details.
        /// </returns>
        public List<TestScheduleDetail> GetCandidateTestSessions(string testSessionID, List<string> candidateTestSessionIDs)
        {
            return new TestSchedulerDLManager().GetCandidateTestSessions(testSessionID, candidateTestSessionIDs);
        }


        /// <summary>
        /// Method that will call whenever the user requests to schedule/reschedule a candidate.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the test schedule details.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the mail sent status.
        /// </param>
        public void ScheduleCandidate(TestScheduleDetail testScheduleDetail, int modifiedBy, int tenantID, out bool isMailSent)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                new TestSchedulerDLManager().ScheduleCandidate(testScheduleDetail, modifiedBy, transaction);
                if (testScheduleDetail.IsRescheduled == true)
                {
                    try
                    {
                        // Send email to the candidate who is requested to reschedule
                        new EmailHandler().SendMail(EntityType.CandidateReScheduled,
                            testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString(), tenantID);
                        isMailSent = true;
                    }
                    catch (Exception exp)
                    {
                        isMailSent = false;
                    }
                    try
                    {
                        // Send SMS to the candidate.
                        new SMSHandler().SendSMS(EntityType.CandidateScheduled,
                            testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString());
                        isMailSent = true;
                    }
                    catch (Exception exp)
                    {
                        isMailSent = false;
                    }
                }
                else
                {
                    int positionProfileID = new PositionProfileDLManager().GetPositionProfileID(testScheduleDetail.CandidateTestSessionID, "T");
                    if (positionProfileID > 0)
                    {
                        PositionProfileCandidate positionProfileCandidate = new PositionProfileCandidate();
                        positionProfileCandidate.PositionProfileID = positionProfileID;
                        positionProfileCandidate.CandidateID = int.Parse(testScheduleDetail.CandidateID);
                        positionProfileCandidate.CandidateSessionID = testScheduleDetail.CandidateTestSessionID;
                        positionProfileCandidate.Status = testScheduleDetail.ScheduleStatus.ToString();
                        positionProfileCandidate.PickedBy = null;
                        positionProfileCandidate.PickedDate = null;
                        positionProfileCandidate.SchelduleDate = testScheduleDetail.ExpiryDate;
                        positionProfileCandidate.ResumeScore = null;
                        positionProfileCandidate.AttemptID = testScheduleDetail.AttemptID;
                        positionProfileCandidate.SourceFrom = "N";
                        positionProfileCandidate.CreatedBy = modifiedBy;
                        new TestDLManager().InsertPositionProfileCandidate(positionProfileCandidate, "T", transaction);
                        //if (testScheduleDetail.CandidateID != null && testScheduleDetail.CandidateID.Trim().Length > 0)
                        //{
                        //    new CommonBLManager().InsertCandidateActivityLog
                        //        (0, Convert.ToInt32(testScheduleDetail.CandidateID), "Position profile associated to candidate", modifiedBy,
                        //        Constants.CandidateActivityLogType.CANDIDATE_ASSOCIATED_POSITION_PROFILE);
                        //}
                    }
                    try
                    {
                        // Send email to the candidate who is requested to schedule.
                        new EmailHandler().SendMail(EntityType.CandidateScheduled,
                            testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString(), tenantID);
                        isMailSent = true;
                    }
                    catch (Exception exp)
                    {
                        isMailSent = false;
                    }

                    try
                    {
                        // Send SMS to the candidate.
                        new SMSHandler().SendSMS(EntityType.CandidateScheduled,
                            testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString());
                        isMailSent = true;
                    }
                    catch (Exception exp)
                    {
                        isMailSent = false;
                    }
                }
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                Logger.ExceptionLog(exp);
                isMailSent = false;
                throw exp;

            }
        }



        

        /// <summary>
        /// Method that will call when request to retake a test.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the test schedule details.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="isMailSent">
        /// A <see cref="bool"/> that contains the mail sent status.
        /// </param>
        public void RetakeTest(TestScheduleDetail testScheduleDetail, int modifiedBy, out bool isMailSent,int TenantID)
        {
            isMailSent = false;
            new TestSchedulerDLManager().RetakeTest(testScheduleDetail, modifiedBy);

            try
            {
                // Send email to the candidate who is requested to retake
                new EmailHandler().SendMail(EntityType.TestRetakeRequest,
                    testScheduleDetail.CandidateTestSessionID, testScheduleDetail.AttemptID.ToString(), TenantID);
                isMailSent = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                isMailSent = false;
            }
        }

        /// <summary>
        /// Method that will return the test session and candidate session details
        /// based on the search criteria.
        /// </summary>
        /// <param name="scheduleCandidateSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search criteria details.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that contains the sort expression.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="SortType"/> that contains the sort type either ASC/DESC.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the test session details information.
        /// </returns>
        public TestSessionDetail GetTestSessionDetail(TestScheduleSearchCriteria scheduleCandidateSearchCriteria,
            string sortExpression, SortType sortDirection,int userID)
        {
            return new TestSchedulerDLManager().GetTestSessionDetail(scheduleCandidateSearchCriteria,
                sortExpression, sortDirection,userID);
        }

        /// <summary>
        /// Method that will return test session and candidates session details
        /// for the given test session id or candidate session id/s.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the test session id.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate session ids 
        /// separated by comma.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the test session detail 
        /// with the list of CandidateTestSessionDetail instances.
        /// </returns>
        public TestSessionDetail GetTestSessionDetail(string testSessionID, string candidateSessionIDs,int userID)
        {
            return new TestSchedulerDLManager().GetTestSessionDetail(testSessionID, candidateSessionIDs,userID);
        }

        /// <summary>
        /// This method searches the test session details by passing TestName,
        /// TestKey,SessionKey,ClientRequestNumber or SchedulerName
        /// </summary>
        /// <param name="testSessionCriteria">
        /// A list of<see cref="TestSessionSearchCriteria"/> that holds the testSession Criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>that holds the page Number
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/>that holds the page Size
        /// </param>
        /// <param name="totalRecords">
        ///  A <see cref="int"/>that holds the total Records
        /// </param>
        /// <param name="sortingKey">
        ///  A <see cref="string"/>that holds the sorting Key 
        /// </param>
        /// <param name="sortByDirection">
        ///  A <see cref="SortType"/>that holds the sortBy Direction Like 'A' or 'D'(Asc/dec)
        /// </param>
        /// <returns>
        ///  A list of <see cref="TestSessionDetail"/>that holds the Test Session Detail
        /// </returns>
        public List<TestSessionDetail> SearchTestSessionDetails(TestSessionSearchCriteria testSessionCriteria,
            int pageNumber, int pageSize, out int totalRecords, string sortingKey, SortType sortByDirection)
        {
            return new TestSchedulerDLManager().SearchTestSessionDetails(testSessionCriteria, pageNumber, pageSize, out totalRecords, sortingKey, sortByDirection);
        }

        /// <summary>
        /// This method gets all the informations about test as well as 
        /// the candidates informations assigned to the test sessions.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="TestSessionDetail"/>Specifies the test session key
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="TestSessionDetail"/>Specifies the candidate session keys
        /// separated by comma.
        /// </param>
        /// <param name="sortingKey">
        /// A <see cref="string"/>Specifies the sorting Key
        /// </param>
        /// <param name="sortBy">
        /// A <see cref="SortType"/>Specifies the sort By Asc/Desc
        /// </param>
        /// <returns>
        /// TestSessionDetail class with list of CandidateTestSessionDetail instances
        /// </returns>
        public List<CandidateTestSessionDetail> GetCandidateTestSessions
            (string testSessionID, string candidateSessionIDs, string sortingKey, SortType sortBy)
        {
            return new TestSchedulerDLManager().GetCandidateTestSessions(testSessionID,
                candidateSessionIDs, sortingKey, sortBy);
        }

        /// <summary>
        /// This method passes the SessionKey and CandidateSession keys separated by comma 
        /// and return the invalid candidateSession keys.
        /// </summary>
        /// <param name="sessionKey">
        /// A <see cref="string"/>that holds the session Key
        /// </param>
        /// <param name="candidateSessionKeys">
        /// A <see cref="string"/>that holds the candidate Session Keys
        /// </param>
        /// <returns>
        /// A List of <see cref="String"/>that holds mismatched Candidate Ids.
        /// </returns>
        public List<string> ValidateTestSessionInput
            (string sessionKey, string candidateSessionKeys,int createdBy)
        {
            return new TestSchedulerDLManager().ValidateTestSessionInput(sessionKey, candidateSessionKeys,createdBy);
        }


        /// <summary>
        /// This method gets informations about Test reminders
        /// </summary>
        /// <param name="candidateSessionID">Specifies the test canditate session ID</param>
        /// <param name="attemptid">Specifies the candidate attemptid ID </param>
        /// A <see cref="TestScheduleDetail"/> that holds the candidate
        /// <returns>
        /// test Reminder details.
        /// </returns>
        public TestReminderDetail GetTestReminder(string candidateSessionID, int attemptid)
        {
            return new TestSchedulerDLManager().GetTestReminder(candidateSessionID, attemptid);
        }

        /// <summary>
        /// This method Insert Test reminder details.
        /// </summary>
        /// <param name="reminderDetail">Holds Test reminder properties</param>        
        public void SaveTestReminder(TestReminderDetail reminderDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                for (int i = 0; i < reminderDetail.Intervals.Count; i++)
                {
                    reminderDetail.IntervalID = reminderDetail.Intervals[i].IntervalID.ToString();
                    if (reminderDetail.Intervals[i].RecordStatus == RecordStatus.New)
                    {   // Insert Reminder detail.
                        new TestSchedulerDLManager().InsertReminder(reminderDetail, transaction);
                    }
                    else if (reminderDetail.Intervals[i].RecordStatus == RecordStatus.Deleted)
                    {   // Delete Reminder detail.
                        new TestSchedulerDLManager().DeleteReminder(reminderDetail, transaction);
                    }

                    else if (reminderDetail.Intervals[i].RecordStatus == RecordStatus.Modified)
                    {   // Update Reminder detail.
                        new TestSchedulerDLManager().UpdateReminder(reminderDetail, transaction);
                    }
                }
                // Commit the transaction.
                transaction.Commit();

            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// This method deletes all reminder details for specific candidate id
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds candidate session id
        /// </param>
        /// <param name="attemptid">
        /// A <see cref="int"/> that holds attempt id
        /// </param>
        public void DeleteReminderAll(string candidateSessionID, int attemptid)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;
            try
            {
                // Deletes all Reminder detail for specific candidate session id.
                new TestSchedulerDLManager().DeleteReminderAll(candidateSessionID, attemptid, transaction);
                // Commit the transaction.

                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the list of test reminder sender list for the
        /// given date/time and relative time differences. Mails needs to be
        /// sent for this list.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<TestReminderDetail> GetTestReminderSenderList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            return new TestSchedulerDLManager().GetTestReminderSenderList
                (currentDateTime, relativeTimeDifference);
        }

       
        /// <summary>
        /// Method that will update the test reminder sent status for the 
        /// given candidate, attempt number and interval ID.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="intervalID">
        /// A <see cref="string"/> that holds the interval ID.
        /// </param>
        /// <param name="reminderSent">
        /// A <see cref="bool"/> that holds the reminder sent status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateTestReminderStatus(string candidateSessionID,
            int attemptID, string intervalID, bool reminderSent, int userID)
        {
            new TestSchedulerDLManager().UpdateTestReminderStatus
                (candidateSessionID, attemptID, intervalID,
                reminderSent, userID);
        }

        /// Represents the method to check whether the same candidate
        /// has been assigned for the test session ID
        /// </summary>
        /// <param name="testSessionID">A
        /// <see cref="string"/>that holds the test session ID
        /// </param>
        /// <param name="candidateID">
        /// A<see cref="string"/>that holds the candidate ID
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds  candidate already assigned scheduler name
        /// </returns>
        public string CheckCandidateAlreadyAssigned(string testSessionID, string candidateID)
        {
            return new TestSchedulerDLManager().CheckCandidateAlreadyAssigned(testSessionID, candidateID);
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID and attempt ID, that helps in composing 
        /// email.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestSessionDetail"/> that holds the candidate
        /// test session details.
        /// </returns>
        public CandidateTestSessionDetail GetCandidateTestSessionEmailDetail
            (string candidateTestSessionID, int attemptID)
        {
            return new TestSchedulerDLManager().GetCandidateTestSessionEmailDetail
                (candidateTestSessionID, attemptID);
        }

         /// <summary>
        /// Method that retrieves the candidate open text response detail.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateOpenTextResponseDetail"/> that holds the candidate
        /// open text response detail.
        /// </returns>
        public List<CandidateOpenTextResponseDetail> GetCandidateOpenTextResponse
            (string candidateTestSessionID, int attemptID)
        {
            return new TestSchedulerDLManager().GetCandidateOpenTextResponse
                (candidateTestSessionID, attemptID);
        }

        /// <summary>
        /// Method that updates the candidate open text response.
        /// </summary>
        public void UpdateCandidateOpenTextResponseScore(string candidateSessionKey, int attemptID, 
            List<CandidateOpenTextResponseDetail> responses)
        {
            if (responses == null || responses.Count == 0)
                return;

            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                TestSchedulerDLManager dlManager = new TestSchedulerDLManager();

                foreach (CandidateOpenTextResponseDetail response in responses)
                {
                    dlManager.UpdateCandidateOpenTextResponseScore(candidateSessionKey, attemptID, response, transaction);
                }

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Update the candidate session status.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains the attempt ID.
        /// </param>
        public void UpdateCandidateSessionStatus(string candidateSessionKey, int attempID, string sessionStatus, int modifiedBy)
        {
            new TestSchedulerDLManager().UpdateCandidateSessionStatus(candidateSessionKey, attempID, sessionStatus, modifiedBy); 
        }

        /// <summary>
        /// Retrive candidate information by ids.
        /// </summary>
        /// <param name="candidateIDs">
        /// A <see cref="string"/> that contains the candidate ids.
        /// </param>
       
        public DataTable GetCandidateInformationByIDs(string candidateIDs,string testSessionKey)
        {
            return new TestSchedulerDLManager().GetCandidateInformationByIDs(candidateIDs, testSessionKey);
        }

        #endregion

      
    }
}