﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreditBLManager.cs
// File that represents the business layer for the credit management.
// This will talk to the data layer to perform the operations associated
// with the credit management processes.

#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Collections.Generic;

using Forte.HCM.DL;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.BL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the credit management.
    /// This will talk to the data layer to perform the operations associated
    /// with credit management processes.
    /// </summary>
    public class CreditBLManager
    {
        #region Public method                                                  
        /// <summary>
        /// Method to get the credits details based on the search criteria
        /// </summary>
        /// <param name="creditSearchCriteria">
        /// A<see cref="CreditRequestSearchCriteria"/>that holds the credit Search Criteria
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pageNumber
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the pageSize
        /// </param>
        /// <param name="sortField">
        /// A<see cref="string"/>that holds the sort Field
        /// </param>
        /// <param name="sordOrder">
        /// A<see cref="SortType"/>that holds the sordOrder like Asc/Dsc 
        /// </param>
        /// <param name="total">
        /// A<see cref="int"/>that holds the total Records
        /// </param>
        /// <returns>
        /// A List for<see cref="CreditRequestDetail"/>that holds the Credit Request Detail
        /// </returns>
        public List<CreditRequestDetail> GetCreditDetails(CreditRequestSearchCriteria creditSearchCriteria,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int total)
        {
            return new CreditDLManager().GetCreditDetails(creditSearchCriteria, pageNumber,
                pageSize, sortField, sordOrder, out total);

        }

        /// <summary>
        ///  Method to get the credit summary from Data Access Layer
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/>that holds the user ID
        /// </param>   
        /// <returns>
        /// A list for<see cref="CreditsSummary"/>that holds the credit summary.
        /// </returns>
        public CreditsSummary GetCreditSummary(int userID)
        {
            return new CreditDLManager().GetCreditSummary(userID);
        }

        /// <summary>
        /// Method to get the list of crdits usage history from data access layer
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/>that holds the user ID
        /// </param>
        /// <param name="creditType">
        /// A <see cref="string"/>that holds the credit Type
        /// </param>
        /// <param name="pagenumber">
        /// A <see cref="int"/>that holds the page number of the page
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size of the page
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/>that holds the total records of credit history table 
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/>that holds the expression to sort
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/>that holds the sort type.
        /// </param>
        /// <returns>
        /// A list for <see cref="CreditUsageDetail"/> that holds credit usage details.
        /// </returns>
        public List<CreditUsageDetail> GetCreditsUsageHistory(int userID, 
            string creditType, int pagenumber, int pageSize, out int totalRecords, 
            string orderBy, SortType orderByDirection)
        {
            return new CreditDLManager().GetCreditsUsageHistory(userID, creditType,
                pagenumber, pageSize, out totalRecords, orderBy, orderByDirection);
        }

        /// <summary>
        /// Represents the method to approve the credit request
        /// </summary>
        /// <param name="creditRequestDetail">
        /// A<see cref="CreditRequestDetail"/>that holds the request detail
        /// for the credit
        /// </param>
        public bool ApproveCreditRequest(CreditRequestDetail creditRequestDetail)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                //Approve the credit request by updating 
                //field in the credit request table
                new CreditDLManager().ApproveCreditRequest(creditRequestDetail, transaction);

                if (creditRequestDetail.IsApproved)
                {
                    //Add entry in histroy table only if it is approved 
                    new CreditDLManager().InsertIntoCreditHistory(creditRequestDetail, transaction);
                }

                // Commit the transaction.
                transaction.Commit();

                return true;
            }
            catch (Exception exception)
            {
                transaction.Rollback();

                throw exception;
            }
        }

        /// <summary>
        /// Represents the method to get credit configuration details
        /// </summary>
        /// <returns>
        /// that holds credit configuration values
        /// </returns>
        public DataSet GetCreditConfig()
        {
            return new CreditDLManager().GetCreditConfig();
        }

        /// <summary>
        /// Method that updates credit config values.
        /// </summary>
        /// <param name="creditValue">
        /// A <see cref="decimal"/> object that holds the credit value.
        /// </param>
        /// <param name="genID">
        /// A <see cref="int"/> object that holds the credit config gen ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> object that holds the user ID.
        /// </param>
        public void UpdateCreditConfig(decimal creditValue, int genID, int userID)
        {
            new CreditDLManager().UpdateCreditConfig(creditValue, genID, userID);
        }
        #endregion
    }
}
