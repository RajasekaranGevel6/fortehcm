﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.DL
{
    public class OnlineInterviewAssessorDLManager : DatabaseConnectionManager
    { 
        public void InsertOnlineInterviewAssessor(AssessorTimeSlotDetail assessorTimeSlots )
        { 
            // Create command object
            DbCommand insertAssessor
                = HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_ASSESSOR");
            // Add input parameter
            HCMDatabase.AddInParameter(insertAssessor,
                "@INTERVIEW_KEY", DbType.String, assessorTimeSlots.CandidateSessionKey);
            HCMDatabase.AddInParameter(insertAssessor,
                "@ASSESSOR_ID", DbType.Int32, assessorTimeSlots.AssessorID);
            HCMDatabase.AddInParameter(insertAssessor,
                "@CREATED_BY", DbType.Int32, assessorTimeSlots.InitiatedBy);

            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAssessor );
        }

        public int InsertAssessorAvailabilityDates(AssessorTimeSlotDetail
            assessorAvailDates, IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertAssessor
                = HCMDatabase.GetStoredProcCommand("SPINSERT_ASSESSOR_AVAILABILITY_DATES");
            // Add input parameter
            HCMDatabase.AddInParameter(insertAssessor,
                "@ASSESSOR_ID", DbType.Int32, assessorAvailDates.AssessorID);
            HCMDatabase.AddInParameter(insertAssessor,
                "@AVAILABLE_DATE", DbType.DateTime, assessorAvailDates.AvailabilityDate);
            HCMDatabase.AddInParameter(insertAssessor,
                "@REQUEST_STATUS", DbType.String, assessorAvailDates.RequestStatus);
            HCMDatabase.AddInParameter(insertAssessor,
                "@ANY_TIME_AVAILABLE", DbType.String, assessorAvailDates.IsAvailableFullTime);
            HCMDatabase.AddInParameter(insertAssessor,
                "@CREATED_BY", DbType.Int32, assessorAvailDates.InitiatedBy);
            HCMDatabase.AddOutParameter(insertAssessor,
                "@REQUEST_DATE_GEN_ID", DbType.Int32, int.MaxValue);

            object returnValue=null;

            if (transaction == null)
            {
                returnValue = HCMDatabase.ExecuteNonQuery(insertAssessor);
            }
            else
            {
                returnValue = HCMDatabase.ExecuteNonQuery(insertAssessor, transaction as DbTransaction);
            }

            returnValue = HCMDatabase.GetParameterValue(insertAssessor, "@REQUEST_DATE_GEN_ID");

            if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                return 0;

            return int.Parse(returnValue.ToString());
        }

        public void InsertOnlineInterviewAssessorSkill(AssessorTimeSlotDetail assessorSkill)
        {  
            // Create command object
            DbCommand insertAssessor
                = HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_ASSESSOR_SKILL");
            // Add input parameter
            HCMDatabase.AddInParameter(insertAssessor,
                "@ASS_GEN_ID", DbType.Int32, assessorSkill.ID);
            HCMDatabase.AddInParameter(insertAssessor,
                "@SKILL_ID", DbType.Int32, assessorSkill.SkillID);
            HCMDatabase.AddInParameter(insertAssessor,
                "@CREATED_BY", DbType.Int32, assessorSkill.InitiatedBy);

            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAssessor);
        }

        public void DeleteOnlineInterviewAssessor(int assessorId, string interviewKey)
        {
            // Create command object
            DbCommand deleteAssessor
                = HCMDatabase.GetStoredProcCommand("SPDELETE_ONLINE_INTERVIEW_ASSESSOR");

            // Add input parameter
            HCMDatabase.AddInParameter(deleteAssessor,
                "@INTERVIEW_KEY", DbType.String, interviewKey);
            HCMDatabase.AddInParameter(deleteAssessor,
                "@ASSESSOR_ID", DbType.Int32, assessorId);
         
            // Execute the command
            HCMDatabase.ExecuteNonQuery(deleteAssessor);
        }
        
        public List<AssessorTimeSlotDetail> GetOnlineInterviewAssessorList(string interviewKey,
            int pageNo, int pageSize,out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                //Create a list
                List<AssessorTimeSlotDetail> assessorTimeSlotDetailList = null;
                AssessorTimeSlotDetail assessorTimeSlotDetail = null;

                // Create a stored procedure command object.
                DbCommand getAssessor = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSOR");
                // Add input parameter
                HCMDatabase.AddInParameter(getAssessor,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);
                HCMDatabase.AddInParameter(getAssessor,
                    "@PAGE_NUM", DbType.Int32, pageNo);
                HCMDatabase.AddInParameter(getAssessor,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessor);
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (assessorTimeSlotDetailList == null)
                            assessorTimeSlotDetailList = new List<AssessorTimeSlotDetail>();

                        assessorTimeSlotDetail = new AssessorTimeSlotDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ASS_GEN_ID"]))
                            assessorTimeSlotDetail.ID =Convert.ToInt32(dataReader["ASS_GEN_ID"]);

                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                            assessorTimeSlotDetail.AssessorID = Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]))
                            assessorTimeSlotDetail.Assessor = dataReader["ASSESSOR_NAME"].ToString();

                        assessorTimeSlotDetailList.Add(assessorTimeSlotDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return assessorTimeSlotDetailList;
            }
            finally 
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public List<AssessorTimeSlotDetail> GetAssessorAvailableDates(int assessorId,int monthId,
            int pageNo, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                //Create a list
                List<AssessorTimeSlotDetail> assessorAvailabilityList = null;

                AssessorTimeSlotDetail assessorAvailability = null;

                // Create a stored procedure command object.
                DbCommand getAssessor = HCMDatabase. GetStoredProcCommand("SPGET_ASSESSOR_AVAILABILITY_DATES");

                // Add input parameter
                HCMDatabase.AddInParameter(getAssessor, "@ASSESSOR_ID", DbType.Int32, assessorId);

                HCMDatabase.AddInParameter(getAssessor, "@MONTH_ID", DbType.Int32, monthId);
                
                HCMDatabase.AddInParameter(getAssessor, "@PAGE_NUM", DbType.Int32, pageNo);

                HCMDatabase.AddInParameter(getAssessor, "@PAGE_SIZE", DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessor);

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (assessorAvailabilityList == null)
                            assessorAvailabilityList = new List<AssessorTimeSlotDetail>();

                        assessorAvailability = new AssessorTimeSlotDetail();


                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                            assessorAvailability.AssessorID
                                = Convert.ToInt32(dataReader["ASSESSOR_ID"]);
                         
                        if (!Utility.IsNullOrEmpty(dataReader["AVAILABILITY_DATE"]))
                            assessorAvailability.AvailabilityDate
                                = Convert.ToDateTime(dataReader["AVAILABILITY_DATE"]);
                    
                        if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                            assessorAvailability.RequestStatus = dataReader["STATUS"].ToString(); 

                        if (!Utility.IsNullOrEmpty(dataReader["NON_AVAILABILITY_DATES"]))
                            assessorAvailability.VacationDates = dataReader["NON_AVAILABILITY_DATES"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED"]))
                            assessorAvailability.Scheduled = dataReader["SCHEDULED"].ToString();

                     if (!Utility.IsNullOrEmpty(dataReader["FROM_TIME"]))
                            assessorAvailability.TimeSlotTextFrom = dataReader["FROM_TIME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["TO_TIME"]))
                            assessorAvailability.TimeSlotTextTo = dataReader["TO_TIME"].ToString(); 

                        assessorAvailabilityList.Add(assessorAvailability);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return assessorAvailabilityList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public List<AssessorTimeSlotDetail> GetAssessorTimeSlots(int genId)
        {
            IDataReader dataReader = null;
             
            try
            {
                //Create a list
                List<AssessorTimeSlotDetail> assessorTimeSlotList = null;

                AssessorTimeSlotDetail assessorTimeSlot = null;

                // Create a stored procedure command object.
                DbCommand getTimeSlots = HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_AVAILABILITY_DATE_TIMESLOTS");

                // Add input parameter
                //HCMDatabase.AddInParameter(getTimeSlots, "@ASSESSOR_ID", DbType.Int32, assessorId);

                HCMDatabase.AddInParameter(getTimeSlots, "@GEN_ID", DbType.Int32, genId); 

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTimeSlots);

                while (dataReader.Read())
                { 
                    // Instantiate the testSession instance.
                    if (assessorTimeSlotList == null)
                        assessorTimeSlotList = new List<AssessorTimeSlotDetail>();

                    assessorTimeSlot = new AssessorTimeSlotDetail();
                          
                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                        assessorTimeSlot.RequestStatus = dataReader["STATUS"].ToString(); 

                    if (!Utility.IsNullOrEmpty(dataReader["FROM_TIME"]))
                        assessorTimeSlot.TimeSlotTextFrom = dataReader["FROM_TIME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["TO_TIME"]))
                        assessorTimeSlot.TimeSlotTextTo = dataReader["TO_TIME"].ToString();

                    assessorTimeSlotList.Add(assessorTimeSlot); 
                }

                return assessorTimeSlotList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public List<AssessorTimeSlotDetail> GetOnlineAssessorTimeSlotDates(int assessorId, int monthId,
          int pageNo, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                //Create a list
                List<AssessorTimeSlotDetail> assessorAvailabilityList = null;

                AssessorTimeSlotDetail assessorAvailability = null;

                // Create a stored procedure command object.
                DbCommand getAssessor = HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSOR_TIMESLOT_DATES");

                // Add input parameter
                HCMDatabase.AddInParameter(getAssessor, "@ASSESSOR_ID", DbType.Int32, assessorId);

                HCMDatabase.AddInParameter(getAssessor, "@MONTH_ID", DbType.Int32, monthId);

                HCMDatabase.AddInParameter(getAssessor, "@PAGE_NUM", DbType.Int32, pageNo);

                HCMDatabase.AddInParameter(getAssessor, "@PAGE_SIZE", DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessor);

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (assessorAvailabilityList == null)
                            assessorAvailabilityList = new List<AssessorTimeSlotDetail>();

                        assessorAvailability = new AssessorTimeSlotDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_GEN_ID"]))
                            assessorAvailability.ID = Convert.ToInt32(dataReader["DATE_GEN_ID"]); 

                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                            assessorAvailability.AssessorID = Convert.ToInt32(dataReader["ASSESSOR_ID"]);

                        if (!Utility.IsNullOrEmpty(dataReader["AVAILABILITY_DATE"]))
                            assessorAvailability.AvailabilityDate
                                = Convert.ToDateTime(dataReader["AVAILABILITY_DATE"]);

                        if (!Utility.IsNullOrEmpty(dataReader["NON_AVAILABILITY_DATES"]))
                            assessorAvailability.VacationDates = dataReader["NON_AVAILABILITY_DATES"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED"]))
                            assessorAvailability.Scheduled = dataReader["SCHEDULED"].ToString(); 

                        assessorAvailabilityList.Add(assessorAvailability);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return assessorAvailabilityList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<int> GetOnlineInterviewAssessorSkill(int assessorGenId)
        {
            IDataReader dataReader = null;
            try
            {
                //Create a list
                List<int> assessorSkillList = null; 

                // Create a stored procedure command object.
                DbCommand getAssessor = HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSOR_SKILL");

                // Add input parameter
                HCMDatabase.AddInParameter(getAssessor, "@ASS_GEN_ID", DbType.Int32, assessorGenId); 

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessor);

                while (dataReader.Read())
                { 
                        // Instantiate the testSession instance.
                    if (assessorSkillList == null)
                        assessorSkillList = new List<int>(); 

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                        assessorSkillList.Add(Convert.ToInt32(dataReader["SKILL_ID"]));  
                }

                return assessorSkillList;
            }
            finally
            {  // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<InterviewDetail> GetAssessorInterviewDetail(int assessorId, DateTime requestDate)
        {
            IDataReader dataReader = null;
            try
            {
                //Create a list
                List<InterviewDetail> interviewDetailList = null;

                InterviewDetail interviewDetail = null;

                // Create a stored procedure command object.
                DbCommand getAssessor = HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_AVAILABLE_INTERVIEWS");

                // Add input parameter
                HCMDatabase.AddInParameter(getAssessor, "@ASSESSOR_ID", DbType.Int32, assessorId);

                HCMDatabase.AddInParameter(getAssessor, "@REQUEST_DATE", DbType.DateTime, requestDate);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessor);
                while (dataReader.Read())
                {
                    // Instantiate the testSession instance.
                    if (interviewDetailList == null)
                        interviewDetailList = new List<InterviewDetail>();

                    interviewDetail = new InterviewDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_KEY"]))
                        interviewDetail.InterviewTestKey
                            = Convert.ToString(dataReader["ONLINE_INTERVIEW_KEY"]);

                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_NAME"]))
                        interviewDetail.InterviewName = dataReader["ONLINE_INTERVIEW_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_NAME"]))
                        interviewDetail.TestAuthor = dataReader["AUTHOR_NAME"].ToString();


                    if (!Utility.IsNullOrEmpty(dataReader["TIMEFROM"]))
                        interviewDetail.TimeSlotTextFrom = dataReader["TIMEFROM"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["TIMETO"]))
                        interviewDetail.TimeSlotTextTo = dataReader["TIMETO"].ToString(); 
                     
                    interviewDetailList.Add(interviewDetail);
                }

                return interviewDetailList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="interviewSearch"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public List<OnlineCandidateSessionDetail> GetOnlineInterviews(InterviewSearchCriteria interviewSearch, out int totalRecords)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getOnlineInterviewCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEWS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@INTERVIEW_KEY", DbType.String,interviewSearch.InterviewSessionKey );

                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@STATUS", DbType.String, interviewSearch.InterviewSessionStatus);

                if (!Utility.IsNullOrEmpty(interviewSearch.Keyword))
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@SEARCH_TEXT", DbType.String, interviewSearch.Keyword);

                if (!Utility.IsNullOrEmpty(interviewSearch.SearchTenantID) && interviewSearch.SearchTenantID>0)
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@TENANT_ID", DbType.Int32, interviewSearch.SearchTenantID);

                if (!Utility.IsNullOrEmpty(interviewSearch.CandidateId) && interviewSearch.CandidateId > 0)
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@CANDIDATE_ID", DbType.Int32, interviewSearch.CandidateId);

                if (!Utility.IsNullOrEmpty(interviewSearch.PositionProfileId) && 
                    Convert.ToInt32(interviewSearch.PositionProfileId)>0)
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@POSITION_PROFILE_ID", 
                        DbType.Int32, interviewSearch.PositionProfileId); 
                
                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@PAGE_NUM", DbType.Int32, interviewSearch.CurrentPage);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@PAGE_SIZE", DbType.Int32, interviewSearch.PageSize);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@ORDER_BY",
                    DbType.String, interviewSearch.SortExpression);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, interviewSearch.SortDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getOnlineInterviewCommand);

                List<OnlineCandidateSessionDetail> onlineInterviewSessionCollection = null;

                OnlineCandidateSessionDetail onlineInterviewSession = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (onlineInterviewSessionCollection == null)
                            onlineInterviewSessionCollection = new List<OnlineCandidateSessionDetail>();

                        onlineInterviewSession = new OnlineCandidateSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                            onlineInterviewSession.RowNo = Convert.ToInt32(dataReader["ROWNUMBER"] );

                        if(!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                            onlineInterviewSession.ScheduledBy = dataReader["SCHEDULED_BY"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_STATUS"]))
                            onlineInterviewSession.SessionStatus = dataReader["ONLINE_INTERVIEW_STATUS"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_DATE"]))
                        {
                            onlineInterviewSession.InterviewDate = Convert.ToDateTime(dataReader["ONLINE_INTERVIEW_DATE"]);

                            if (DateTime.Today > Convert.ToDateTime(dataReader["ONLINE_INTERVIEW_DATE"]))                             
                                onlineInterviewSession.IsExpired = true;
                            else
                                onlineInterviewSession.IsExpired = false; 
                        } 

                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSORS"]))
                            onlineInterviewSession.AssessorName = dataReader["ASSESSORS"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_KEY"]))
                            onlineInterviewSession.CandidateSessionID = dataReader["ONLINE_INTERVIEW_KEY"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        {
                            onlineInterviewSession.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CAND_ONLINE_INTERVIEW_ID"]))
                        {
                            onlineInterviewSession.CandidateInterviewID = Convert.ToInt32(dataReader["CAND_ONLINE_INTERVIEW_ID"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        {
                            onlineInterviewSession.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        {
                            onlineInterviewSession.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        {
                            onlineInterviewSession.EmailId = dataReader["EMAIL"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SLOT_KEY"]))
                        {
                            onlineInterviewSession.SlotKey =
                                dataReader["SLOT_KEY"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CHAT_ROOM_ID"]))
                        {
                            onlineInterviewSession.ChatRoomName =
                                dataReader["CHAT_ROOM_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_NAME"]))
                        {
                            onlineInterviewSession.InterviewName =
                                dataReader["ONLINE_INTERVIEW_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        {
                            onlineInterviewSession.PositionProfileName =
                                dataReader["POSITION_PROFILE_NAME"].ToString().Trim();
                        } 
                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        {
                            onlineInterviewSession.PositionProfileId =
                                Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_DESC"]))
                        {
                            onlineInterviewSession.InterviewDesc =
                                Convert.ToString(dataReader["ONLINE_INTERVIEW_DESC"]);
                        }
                        // Add the testSession to the collection.
                        onlineInterviewSessionCollection.Add(onlineInterviewSession);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return onlineInterviewSessionCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public CandidateInterviewSessionDetail GetOnlineInterviewDetail(string interviewKey,string chatRoomId)
        {
            if (Utility.IsNullOrEmpty(interviewKey))
                throw new Exception("Interview key cannot be empty");

            if (Utility.IsNullOrEmpty(chatRoomId))
                throw new Exception("Chat room id cannot be empty");

            CandidateInterviewSessionDetail session = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_CANDIDATE_INTERVIEW_SESSION");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);

                HCMDatabase.AddInParameter(getCandidateSession,
                    "@CHAT_ROOM_ID", DbType.String, chatRoomId);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                if (dataReader.Read())
                {
                    session = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_ONLINE_INTERVIEW_ID"]))
                    {
                        session.GenID = Convert.ToInt32(dataReader["CAND_ONLINE_INTERVIEW_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_KEY"]))
                    {
                        session.InterviewSessionKey = dataReader["ONLINE_INTERVIEW_KEY"].ToString().Trim();
                    } 

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        session.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INFO_ID"]))
                    {
                        session.CandidateInformationID = Convert.ToInt32(dataReader["CANDIDATE_INFO_ID"]);
                    }  
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        session.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    } 
                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_NAME"]))
                    {
                        session.InterviewTestName = dataReader["ONLINE_INTERVIEW_NAME"].ToString().Trim();
                    }  
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                    {
                        session.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        session.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                    {
                        session.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                    {
                        session.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_DATE"]))
                    {
                        session.DateCompleted = Convert.ToDateTime(dataReader["ONLINE_INTERVIEW_DATE"]);
                    }
                }

                return session;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public string GetCandidateUserName(int interviewId)
        {
            if (Utility.IsNullOrEmpty(interviewId))
                throw new Exception("Interview id cannot be empty"); 

            IDataReader dataReader = null;
            string candidateId = string.Empty;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_CANDIDATE_ID");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@INTERVIEW_ID", DbType.String, interviewId);
                 
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        candidateId = dataReader["CANDIDATE_ID"].ToString().Trim();
                    } 
                }

                return candidateId;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public string GetUserEmailID(int userId)
        {  
             IDataReader dataReader = null;
            string userEmailId = string.Empty;

            try
            {
                // Create a stored procedure command object.
                DbCommand getUser = HCMDatabase.
                    GetStoredProcCommand("SPGET_USER");

                // Add input parameters.
                HCMDatabase.AddInParameter(getUser,
                    "@USER_ID", DbType.Int32, userId);
                 
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getUser);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["USRUSERNAME"]))
                    {
                        userEmailId = dataReader["USRUSERNAME"].ToString().Trim();
                    } 
                }

                return userEmailId;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
    }
}
