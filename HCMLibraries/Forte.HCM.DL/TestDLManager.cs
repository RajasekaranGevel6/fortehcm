﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestDLManager.cs
// File that represents the data layer for the Test respository Manager.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Question respository management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for Question. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class TestDLManager : DatabaseConnectionManager
    {
        #region Public Method
        /// <summary>
        /// This method handles the new Test entry functions
        /// </summary>
        /// <param name="testDetail">
        /// A <see cref="TestDetail"/> that contains Test detail collection
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creater of the Test.
        /// </param>
        /// <param name="complexity">
        /// A <see cref="string"/> that contains the complexity Atribute
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object
        /// </param>
        public void InsertTest(TestDetail testDetail, int userID, string complexity, IDbTransaction transaction)
        {
            if ((bool)testDetail.IsCertification)
                if ((Utility.IsNullOrEmpty(testDetail.CertificationDetail)) ||
                        (testDetail.CertificationDetail.CertificateID <= 0))
                    throw new Exception("Certification details not included for certification test");
            // Create a stored procedure command object.
            DbCommand insertTestCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_TEST");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_KEY", DbType.String, testDetail.TestKey);
            HCMDatabase.AddInParameter(insertTestCommand,
              "@QUESTION_TYPE", DbType.String, (int) testDetail.QuestionType);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_NAME", DbType.String, testDetail.Name);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_DESCRIPTION", DbType.String, testDetail.Description);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@RECOMMENDED_TIME", DbType.Int32, testDetail.RecommendedCompletionTime);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@SYSTEM_RECOMMENDED_TIME", DbType.Int32, testDetail.SystemRecommendedTime);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TOTAL_QUESTION", DbType.Int16, testDetail.NoOfQuestions);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@CERTIFICATION", DbType.String, (testDetail.IsCertification) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_AUTHOR", DbType.Int32, testDetail.TestAuthorID);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_COST", DbType.Decimal, testDetail.TestCost);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@DELETED", DbType.String, (testDetail.IsDeleted) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@ACTIVE", DbType.String, (testDetail.IsActive) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_MODE", DbType.String, testDetail.TestMode);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@USER_ID", DbType.Int32, userID);
            if (testDetail.PositionProfileID == 0)
            {
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, testDetail.PositionProfileID);
            }
            HCMDatabase.AddInParameter(insertTestCommand,
               "@COMPLEXITY", DbType.String, complexity);
            if ((bool)testDetail.IsCertification)
            {
                HCMDatabase.AddInParameter(insertTestCommand, "@QUALIFICATION_SCORE", DbType.Decimal,
                    testDetail.CertificationDetail.MinimumTotalScoreRequired);
                HCMDatabase.AddInParameter(insertTestCommand, "@MAXIMUM_TIME_PERM", DbType.Int32,
                    testDetail.CertificationDetail.MaximumTimePermissible);
                HCMDatabase.AddInParameter(insertTestCommand, "@CERTIFICATION_VALID_ID", DbType.String,
                    testDetail.CertificationDetail.CertificateValidity);
                HCMDatabase.AddInParameter(insertTestCommand, "@NO_OF_RETAKES", DbType.Int16,
                    testDetail.CertificationDetail.PermissibleRetakes);
                HCMDatabase.AddInParameter(insertTestCommand, "@CERTIFICATE_FORMAT_ID", DbType.Int32,
                    testDetail.CertificationDetail.CertificateID);
                HCMDatabase.AddInParameter(insertTestCommand, "@TIME_FOR_NO_OF_RETAKES", DbType.Int32,
                    testDetail.CertificationDetail.DaysElapseBetweenRetakes);
            }
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestCommand, transaction as DbTransaction);
        }


        /// <summary>
        /// This method is used to insert TestQuestion against the given Test 
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the testKey.
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the questionKey.
        /// </param>
        /// <param name="questionRelationId">
        /// A <see cref="int"/> that contains the question Relation Id.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creater of the Test.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction.
        /// </param>
        public void InsertTestQuestions(string testKey, string questionKey, int questionRelationId, int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestQuestionsCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_TEST_QUESTION");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TEST_KEY", DbType.String, testKey);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@QUESTION_RELATION_ID", DbType.Int32, questionRelationId);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestQuestionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method handles the new Test Certificate entry functions.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the testKey
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the questionKey
        /// </param>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationDetail"/> that contains the certification Detail
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creater of the Test.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object
        /// </param>
        public void InsertTestCertification(string testKey, string questionKey,
            CertificationDetail certificationDetail, int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestCertificationCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_TEST_CERTIFICATION");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestCertificationCommand,
                "@TEST_KEY", DbType.String, testKey);
            HCMDatabase.AddInParameter(insertTestCertificationCommand,
                "@CERTIFICATE_TYPE", DbType.String, certificationDetail.CertificateFormat);
            HCMDatabase.AddInParameter(insertTestCertificationCommand,
                "@MAXIMUM_TIME_PERM", DbType.Int32, certificationDetail.MaximumTimePermissible);
            HCMDatabase.AddInParameter(insertTestCertificationCommand,
                "@CERTIFICATION_VALID_ID", DbType.Int32, certificationDetail.CertificateValidity);
            HCMDatabase.AddInParameter(insertTestCertificationCommand,
                "@NO_OF_RETAKES", DbType.Int32, certificationDetail.PermissibleRetakes);
            HCMDatabase.AddInParameter(insertTestCertificationCommand,
                "@CERTIFICATE_FORMAT_ID", DbType.Int32, certificationDetail.CertificateFormat);
            HCMDatabase.AddInParameter(insertTestCertificationCommand,
                "@CREATED_BY", DbType.Int32, userID);
            HCMDatabase.AddInParameter(insertTestCertificationCommand,
                "@MODIFIED_BY", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestCertificationCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method handles the Edit Test functions
        /// </summary>
        /// <param name="testDetail">
        /// Contains Test detail collection
        /// </param>
        /// <param name="userID">
        /// Creater of the Test.
        /// </param>
        /// <param name="complexity">
        /// Holds the complexity Atribute
        /// </param>
        /// <param name="transaction">
        /// Holds the transaction object
        /// </param>
        public void UpdateTest(TestDetail testDetail, int userID, string complexity, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_TEST");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_KEY", DbType.String, testDetail.TestKey);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_NAME", DbType.String, testDetail.Name);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_DESCRIPTION", DbType.String, testDetail.Description);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@RECOMMENDED_TIME", DbType.Int32, testDetail.RecommendedCompletionTime);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@SYSTEM_RECOMMENDED_TIME", DbType.Int32, testDetail.SystemRecommendedTime);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TOTAL_QUESTION", DbType.Int16, testDetail.NoOfQuestions);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@CERTIFICATION", DbType.String, (testDetail.IsCertification) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_AUTHOR", DbType.Int32, testDetail.TestAuthorID);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_COST", DbType.Decimal, testDetail.TestCost);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@DELETED", DbType.String, (testDetail.IsDeleted) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@ACTIVE", DbType.String, (testDetail.IsActive) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_MODE", DbType.String, testDetail.TestMode);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@MODIFIED_BY", DbType.Int32, testDetail.ModifiedBy);
            HCMDatabase.AddInParameter(insertTestCommand,
               "@COMPLEXITY", DbType.String, complexity);

            if (testDetail.PositionProfileID == 0)
            {
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, testDetail.PositionProfileID);
            }
            if ((bool)testDetail.IsCertification)
            {
                HCMDatabase.AddInParameter(insertTestCommand, "@QUALIFICATION_SCORE", DbType.Decimal,
                    testDetail.CertificationDetail.MinimumTotalScoreRequired);
                HCMDatabase.AddInParameter(insertTestCommand, "@MAXIMUM_TIME_PERM", DbType.Int32,
                    testDetail.CertificationDetail.MaximumTimePermissible);
                HCMDatabase.AddInParameter(insertTestCommand, "@CERTIFICATION_VALID_ID", DbType.String,
                    testDetail.CertificationDetail.CertificateValidity);
                HCMDatabase.AddInParameter(insertTestCommand, "@NO_OF_RETAKES", DbType.Int16,
                    testDetail.CertificationDetail.PermissibleRetakes);
                HCMDatabase.AddInParameter(insertTestCommand, "@CERTIFICATE_FORMAT_ID", DbType.Int32,
                    testDetail.CertificationDetail.CertificateID);
                HCMDatabase.AddInParameter(insertTestCommand, "@TIME_FOR_NO_OF_RETAKES", DbType.Int32,
                    testDetail.CertificationDetail.DaysElapseBetweenRetakes);
            }
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that updates the copy test status. If questions are same for
        /// both parent and child test, then status and parent test key needs 
        /// to be updated.
        /// </summary>
        /// <param name="parentTestKey">
        /// A <see cref="string"/> that holds the parent test key.
        /// </param>
        /// <param name="childTestKey">
        /// A <see cref="string"/> that holds the child test key.
        /// </param>
        public void UpdateCopyTestStatus(string parentTestKey, string childTestKey)
        {
            // Create a stored procedure command object.
            DbCommand updateCopyTestStatusCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_COPY_TEST_STATUS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCopyTestStatusCommand,
                "@PARENT_TEST_KEY", DbType.String, parentTestKey);
            HCMDatabase.AddInParameter(updateCopyTestStatusCommand,
                "@CHILD_TEST_KEY", DbType.String, childTestKey);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCopyTestStatusCommand);
        }

        /// <summary>
        /// Saves the test session details to the respective repositories.
        /// </summary>
        /// <param name="testSessionDetail">
        /// A <see cref="TestSessionDetail"/> that contains the TestSessionDetail.
        /// </param>
        /// <param name="userID">
        ///  A <see cref="int"/> that contains the Creator of the test session.
        /// </param>
        /// <param name="transaction">
        ///  A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>
        public void InsertTestSession(TestSessionDetail testSessionDetail,
            int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertTestSessionCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_TEST_SESSION");

            // Add input parameters
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@SESSION_KEY", DbType.String, testSessionDetail.TestSessionID);
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@TEST_KEY", DbType.String, testSessionDetail.TestID);
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@SESSION_COUNT", DbType.Int32, testSessionDetail.NumberOfCandidateSessions);
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@CREDIT", DbType.String, testSessionDetail.TotalCredit);
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                 "@CLIENT_REQUEST_NUMBER", DbType.Int32, testSessionDetail.PositionProfileID);
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@TIME_LIMIT", DbType.Int32, testSessionDetail.TimeLimit);
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@SESSION_EXPIRY", DbType.DateTime, testSessionDetail.ExpiryDate);
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@RANDOM_ORDER", DbType.String, (testSessionDetail.IsRandomizeQuestionsOrdering ? "Y" : "N"));
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@DISPLAY_RESULT", DbType.String, (testSessionDetail.IsDisplayResultsToCandidate ? "Y" : "N"));
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@CYBER_PROCTORING", DbType.String, (testSessionDetail.IsCyberProctoringEnabled ? "Y" : "N"));
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@USER_ID", DbType.Int32, testSessionDetail.CreatedBy);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertTestSessionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Insert Test Description
        /// </summary>
        /// <param name="testSessionDetail">
        /// A <see cref="TestSessionDetail"/> that contains the test Session Detail.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="TestSessionDetail"/> that contains the  transaction object.
        /// </param>
        public void InsertTestDescription(TestSessionDetail testSessionDetail, IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertTestDescriptionCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_TEST_SESSION_DESC");

            // Add input parameter
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@SESSION_KEY", DbType.String, testSessionDetail.TestSessionID);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@TEST_SESSION_DESC", DbType.String, testSessionDetail.TestSessionDesc);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@INSTRUCTIONS", DbType.String, testSessionDetail.Instructions);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@USER_ID", DbType.Int32, testSessionDetail.CreatedBy);

            HCMDatabase.ExecuteNonQuery(insertTestDescriptionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Inserts candidate test session details 
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="TestSessionDetail"/> that contains the TestSessionDetail.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>
        public void InsertCandidateTestSession(CandidateTestSessionDetail candidateTestSessionDetail,
          IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertCandidateTestSession
                = HCMDatabase.GetStoredProcCommand("SPINSERT_CANDIDATE_SESSION");
            // Add input parameter
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@CAND_SESSION_KEY", DbType.String, candidateTestSessionDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@SESSION_KEY", DbType.String, candidateTestSessionDetail.TestSessionID);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@STATUS", DbType.String, candidateTestSessionDetail.Status);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@CREATED_BY", DbType.Int32, candidateTestSessionDetail.CreatedBy);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@MODIFIED_BY", DbType.Int32, candidateTestSessionDetail.ModifiedBy);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertCandidateTestSession, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will update the test session details based on the test session id
        /// </summary>
        /// <param name="testSessionDetail">
        ///  A <see cref="TestSessionDetail"/> that contains the test session detail.
        /// </param>
        /// <param name="userID">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction.
        /// </param>
        public void UpdateTestSession(TestSessionDetail testSessionDetail, int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand updateTestSessionCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_TEST_SESSION");

            // Add input parameters
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@SESSION_KEY", DbType.String, testSessionDetail.TestSessionID);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@SESSION_COUNT", DbType.Int32, testSessionDetail.NumberOfCandidateSessions);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@CREDIT", DbType.String, testSessionDetail.TotalCredit);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@CLIENT_REQUEST_NUMBER", DbType.Int32, testSessionDetail.PositionProfileID);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@TIME_LIMIT", DbType.Int32, testSessionDetail.TimeLimit);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@SESSION_EXPIRY", DbType.DateTime, testSessionDetail.ExpiryDate);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@RANDOM_ORDER", DbType.String, (testSessionDetail.IsRandomizeQuestionsOrdering ? "Y" : "N"));
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@DISPLAY_RESULT", DbType.String, (testSessionDetail.IsDisplayResultsToCandidate ? "Y" : "N"));
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@CYBER_PROCTORING", DbType.String, (testSessionDetail.IsCyberProctoringEnabled ? "Y" : "N"));
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@USER_ID", DbType.Int32, testSessionDetail.ModifiedBy);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(updateTestSessionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will update the test description details against the test session id.
        /// </summary>
        /// <param name="testSessionDetail">
        ///  A <see cref="TestSessionDetail"/> that contains the test session detail.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction.
        /// </param>
        public void UpdateTestDescription(TestSessionDetail testSessionDetail,
            int userID, IDbTransaction transaction)
        {
            // Create command object
            DbCommand updateTestDescriptionCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_TEST_SESSION_DESC");

            // Add input parameter
            HCMDatabase.AddInParameter(updateTestDescriptionCommand,
                "@SESSION_KEY", DbType.String, testSessionDetail.TestSessionID);
            HCMDatabase.AddInParameter(updateTestDescriptionCommand,
                "@TEST_SESSION_DESC", DbType.String, testSessionDetail.TestSessionDesc);
            HCMDatabase.AddInParameter(updateTestDescriptionCommand,
                "@INSTRUCTIONS", DbType.String, testSessionDetail.Instructions);
            HCMDatabase.AddInParameter(updateTestDescriptionCommand,
                "@USER_ID", DbType.Int32, testSessionDetail.ModifiedBy);

            HCMDatabase.ExecuteNonQuery(updateTestDescriptionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Gets test and certificate details based on the test key
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the test Key.
        /// </param>
        /// <returns>
        /// A <see cref="TestDetail"/> that contains the Test Detail.
        /// </returns>
        public TestDetail GetTestAndCertificateDetail(string testKey)
        {
            IDataReader dataReader = null;
            TestDetail testDetail = new TestDetail();

            try
            {
                // Create command object
                DbCommand getTestDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_TEST_AND_CERTIFICATE_DETAILS");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_KEY", DbType.String, testKey);

                // Get data from the database
                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                if (dataReader != null)
                {
                    testDetail.CertificationDetail = new CertificationDetail();

                    while (dataReader.Read())
                    {
                        testDetail.TestKey = testKey.ToString().Trim();

                        testDetail.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_INCLUDED"].ToString().Trim()))
                            testDetail.SessionIncluded =
                                (Convert.ToInt32(dataReader["SESSION_INCLUDED"].ToString()) == 1) ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"].ToString()))
                            testDetail.Name = dataReader["TEST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                            testDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                            testDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"].ToString()))
                            testDetail.Description = dataReader["TEST_DESCRIPTION"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["RECOMMENDED_TIME"].ToString()))
                            testDetail.RecommendedCompletionTime =
                                Convert.ToInt32(dataReader["RECOMMENDED_TIME"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["SYSTEM_RECOMMENDED_TIME"].ToString()))
                            testDetail.SystemRecommendedTime =
                                Convert.ToInt32(dataReader["SYSTEM_RECOMMENDED_TIME"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"].ToString()))
                            testDetail.IsActive = (Convert.ToChar(dataReader["ACTIVE"]) == 'Y') ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["DELETED"].ToString()))
                            testDetail.IsDeleted = (Convert.ToChar(dataReader["DELETED"]) == 'N') ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_INCLUDED_ACTIVE"].ToString()))
                            testDetail.SessionIncludedActive =
                                (int.Parse(dataReader["SESSION_INCLUDED_ACTIVE"].ToString()) == 0) ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"].ToString()))
                            testDetail.CertificationDetail.MinimumTotalScoreRequired =
                                Convert.ToDecimal(dataReader["MINIMUM_SCORE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_TIME_PERM"]))
                            testDetail.CertificationDetail.MaximumTimePermissible = Convert.ToInt32(dataReader["MAXIMUM_TIME_PERM"]);
                        if (!Utility.IsNullOrEmpty(dataReader["NO_OF_RETAKES"].ToString()))
                            testDetail.CertificationDetail.PermissibleRetakes =
                                Convert.ToInt32(dataReader["NO_OF_RETAKES"].ToString());
                        if (!Utility.IsNullOrEmpty(dataReader["TIME_FOR_NO_OF_RETAKES"]))
                            testDetail.CertificationDetail.DaysElapseBetweenRetakes = Convert.ToInt32(
                                dataReader["TIME_FOR_NO_OF_RETAKES"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_NAME"].ToString()))
                            testDetail.CertificationDetail.CertificateFormat =
                                dataReader["CERTIFICATE_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION"]))
                            testDetail.IsCertification = (dataReader["CERTIFICATION"].ToString() == "Y") ? true : false;
                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_VALID_ID"].ToString()))
                            testDetail.CertificationDetail.CertificateValidity = dataReader["CERTIFICATION_VALID_ID"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_VALIDITY"]))
                            testDetail.CertificationDetail.CertificateValiditiyText = dataReader["CERTIFICATE_VALIDITY"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_ID"]))
                            testDetail.CertificationDetail.CertificateID = Convert.ToInt32(dataReader["CERTIFICATE_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AUTHOR"].ToString()))
                            testDetail.TestAuthorID = int.Parse(dataReader["TEST_AUTHOR"].ToString());
                    }
                    dataReader.Close();
                }

                return testDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method helps to retrieve the question detail which is involved in a test
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/>Contains a test key which is incorporated with a question
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/>Contains a sort Expression ase/dec.
        /// </param> 
        /// <returns>
        /// A List for <see cref="QuestionDetail"/>
        /// Returns QuestionDetail object
        /// </returns>
        public List<QuestionDetail> GetTestQuestionDetail(string testKey, string sortExpression)
        {
            IDataReader dataReader = null;
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            try
            {
                // Create command object
                DbCommand getTestQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_QUESTION_DETAILS_BY_TESTKEY");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@ORDERBY", DbType.String, sortExpression);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getTestQuestionDetailCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        QuestionDetail questionDetail = new QuestionDetail();

                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString().Trim();
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                        questionDetail.Complexity = dataReader["COMPLEXITY"].ToString().Trim();

                        questionDetail.QuestionType =(QuestionType)System.Convert.ToInt32(dataReader["QUESTION_TYPE"]);

                        questionDetail.TestAreaName = dataReader["TESTAREANAME"].ToString().Trim();
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                        questionDetail.Choice_Desc = dataReader["CHOICE_DESC"].ToString().Trim();
                        questionDetail.QuestionRelationId = int.Parse(dataReader["RELATION_ID"].ToString().Trim());
                        questionDetail.ComplexityValue = dataReader["COMPLEXITY_VALUE"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["COMPLEXITY_VALUE"].ToString());
                        questionDetail.AverageTimeTaken = dataReader["AVERAGE_TIME"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["AVERAGE_TIME"].ToString());
                        if (Convert.ToBoolean(dataReader["HAS_IMAGE"]))
                        {
                            questionDetail.HasImage = true;
                        }
                        else
                            questionDetail.HasImage = false;
                        questionDetails.Add(questionDetail);
                    }
                    dataReader.Close();
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will return the candidate session id, attempt id, and the score 
        /// based on the testkey.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestDetail"/> that contains the candidate test instance.
        /// </returns>
        public CandidateTestDetail GetCandidateTestDetail(string testKey)
        {
            IDataReader dataReader = null;
            CandidateTestDetail candidataTestDetail = null;

            try
            {
                DbCommand getCandidateTestDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATES_TEST_RESULT");

                // Add input parameter.
                HCMDatabase.AddInParameter(getCandidateTestDetailCommand, "@TEST_KEY", DbType.String, testKey);
                dataReader = HCMDatabase.ExecuteReader(getCandidateTestDetailCommand);

                while (dataReader.Read())
                {
                    candidataTestDetail = new CandidateTestDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_SESSION_KEY"]))
                        candidataTestDetail.CandidateSessionID = dataReader["CAND_SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        candidataTestDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"]); ;

                    if (!Utility.IsNullOrEmpty(dataReader["SCORE"]))
                        candidataTestDetail.MyScore = Convert.ToDecimal(dataReader["SCORE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_COMPLETED_ON"]))
                        candidataTestDetail.CompletedOn = Convert.ToDateTime(dataReader["TEST_COMPLETED_ON"]);
                }

                return candidataTestDetail;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will return the test certificate details based on the testkey.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="CertificationDetail"/> that contains the certification details.
        /// </returns>
        public CertificationDetail GetTestCertificateDetail(string testKey)
        {
            IDataReader dataReader = null;
            CertificationDetail certificationDetail = null;

            try
            {
                DbCommand getCertificateDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_CERTIFICATE_DETAILS_BY_TEST_KEY");

                // Add input parameter
                HCMDatabase.AddInParameter(getCertificateDetailsCommand, "@TEST_KEY", DbType.String, testKey);

                dataReader = HCMDatabase.ExecuteReader(getCertificateDetailsCommand);

                while (dataReader.Read())
                {
                    certificationDetail = new CertificationDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["QUALIFICATION_SCORE"]))
                        certificationDetail.MinimumTotalScoreRequired =
                            Convert.ToDecimal(dataReader["QUALIFICATION_SCORE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_TIME_PERMISIBLE"]))
                        certificationDetail.MaximumTimePermissible =
                            Convert.ToInt32(dataReader["MAXIMUM_TIME_PERMISIBLE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_FORMAT_ID"]))
                        certificationDetail.CertificateFormatID =
                            Convert.ToInt32(dataReader["CERTIFICATE_FORMAT_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["HTML_TEXT"]))
                        certificationDetail.HtmlText = dataReader["HTML_TEXT"].ToString().Trim();
                }
                return certificationDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will return the candidate score details against the candidate 
        /// session key and attemptid.
        /// </summary>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestDetail"/> that contains the candidate test
        /// </returns>
        public CandidateTestDetail GetCandidateScoreDetail(string candidateSessionKey, int attemptID)
        {
            IDataReader dataReader = null;
            CandidateTestDetail candidateTestDetail = null;

            try
            {
                // Create command object
                DbCommand getCandidateScoreCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_SCORE_DETAILS_BY_CANDIDATE_SESSION_KEY");

                // Add input parameter
                HCMDatabase.AddInParameter(getCandidateScoreCommand,
                    "@CAND_SESSION_KEY", DbType.String, candidateSessionKey);
                HCMDatabase.AddInParameter(getCandidateScoreCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateScoreCommand);

                while (dataReader.Read())
                {
                    candidateTestDetail = new CandidateTestDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_RESULT_ID"]))
                    {
                        candidateTestDetail.CandidateResultID
                            = Convert.ToInt32(dataReader["CANDIDATE_RESULT_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MYSCORE"]))
                    {
                        candidateTestDetail.MyScore = Convert.ToDecimal(dataReader["MYSCORE"].ToString());
                    }
                }
                return candidateTestDetail;
            }

            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets test name 
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="TestDetail"/> that contains the Test Detail.
        /// </returns>
        public TestDetail GetTestDetail(string testID)
        {
            IDataReader dataReader = null;
            TestDetail testDetail = null;

            try
            {
                // Create command object
                DbCommand getTestNameCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_TEST_SESSION_NAME_KEY");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestNameCommand, "@TEST_KEY", DbType.String, testID);

                dataReader = HCMDatabase.ExecuteReader(getTestNameCommand);

                if (dataReader.Read())
                {
                    testDetail = new TestDetail();
                    testDetail.Name = dataReader["TEST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION"]))
                        testDetail.IsCertification =
                            dataReader["CERTIFICATION"].ToString().Trim() == "Y" ? true : false;

                    if (dataReader["RECOMMENDED_TIME"].ToString() != string.Empty)
                    {
                        testDetail.RecommendedCompletionTime
                            = Convert.ToInt32(dataReader["RECOMMENDED_TIME"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREDIT_VALUE"]))
                    {
                        testDetail.TestCost = Convert.ToDecimal(dataReader["CREDIT_VALUE"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                    {
                        testDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        testDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    }
                }
                return testDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets test summary.
        /// </summary>
        public TestDetail GetTestSummary(string testKey)
        {
            IDataReader dataReader = null;
            TestDetail testSummary = null;

            try
            {
                // Create command object
                DbCommand getTestSummaryCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_TEST_SUMMARY");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestSummaryCommand, "@TEST_KEY", DbType.String, testKey);

                dataReader = HCMDatabase.ExecuteReader(getTestSummaryCommand);

                if (dataReader.Read())
                {
                    testSummary = new TestDetail();
                    testSummary.Name = dataReader["TEST_NAME"].ToString().Trim();
                    testSummary.QuestionType = (QuestionType)System.Convert.ToInt32(dataReader["QUESTION_TYPE"]);
                }
                //return testSummary;
               // TestDetail details = new TestDetail();
                return testSummary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will load the test inclusion details are associated with the question. 
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that contains information based on the 
        /// question key.
        /// </returns>
        public List<TestDetail> GetTestInclusionDetail(string questionKey, int pageNumber,
            int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            List<TestDetail> testDetails = null;

            try
            {
                DbCommand getTestInclusionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_INCLUSION_DETAILS");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@QUESTIONKEY", DbType.String, questionKey.ToString());

                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getTestInclusionDetailCommand);

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();

                    TestDetail testDetail = new TestDetail();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                            testDetail.Name = dataReader["TEST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                            testDetail.TestKey = dataReader["TEST_KEY"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                            testDetail.TestCreationDate =
                                Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                            testDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString());

                        testDetail.TestAuthorName =
                            new CommonDLManager().GetUserDetail(testDetail.CreatedBy).FirstName;

                        if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                            testDetail.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ADMINSTRED_COUNT"]))
                            testDetail.NoOfCandidatesAdministered =
                                int.Parse(dataReader["TEST_ADMINSTRED_COUNT"].ToString());

                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will return the list of candidate test session details.
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="string"/> that contains the sort order either ASC/DESC.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateTestSessionDetail"/> that contains information
        /// about the candidate test session details.
        /// </returns>
        public List<CandidateTestSessionDetail> GetCandidateTestSessions
            (string testID, int pageNumber, int pageSize, string orderBy,
            SortType sortDirection, int userID, out int totalRecords)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateDetailCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_SESSION_BY_TEST_KEY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@TEST_KEY", DbType.String, testID);
                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                  "@TEST_SESSION_AUTHOR", DbType.String, userID);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                        "@ORDERBYDIRECTION",
                         DbType.String, sortDirection == SortType.Ascending ?
                         Constants.SortTypeConstants.ASCENDING :
                         Constants.SortTypeConstants.DESCENDING);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateDetailCommand);

                List<CandidateTestSessionDetail> candidateDetailColl = null;

                CandidateTestSessionDetail candSessionDetail = null;
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (candidateDetailColl == null)
                            candidateDetailColl = new List<CandidateTestSessionDetail>();
                        candSessionDetail = new CandidateTestSessionDetail();
                        candSessionDetail.TestID = dataReader["TEST_KEY"].ToString();
                        candSessionDetail.TestSessionID = dataReader["SESSION_KEY"].ToString();
                        candSessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());

                        if (dataReader["POSITION_PROFILE_NAME"] != null)
                            candSessionDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["PURCHASE_DATE"]))
                            candSessionDetail.CreatedDate = Convert.ToDateTime(dataReader["PURCHASE_DATE"].ToString());
                        candSessionDetail.TotalCredit = Convert.ToDecimal(dataReader["CREDIT"].ToString());
                        candSessionDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candSessionDetail.CandidateFullName = dataReader["CANDIDATE_FULLNAME"].ToString().Trim();
                        candSessionDetail.TestSessionAuthor = dataReader["ADMINISTERED_BY"].ToString();
                        candSessionDetail.TestSessionAuthorFullName = dataReader["TEST_AUTHOR_FULLNAME"].ToString().Trim();
                        candSessionDetail.CandidateTestSessionID = dataReader["CAND_SESSION_KEY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candSessionDetail.ScheduledDate = Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                        candSessionDetail.Status = dataReader["SESSION_STATUS"].ToString();
                        // Add the testSession to the collection.

                        candidateDetailColl.Add(candSessionDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return candidateDetailColl;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of Questions for the given search 
        /// </summary>
        /// <param name="searchCriteria">
        /// A list of <see cref="QuestionDetailSearchCriteria"/> that holds the questionKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the QuestionDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<QuestionDetail> GetSearchQuestions(QuestionType questionType, QuestionDetailSearchCriteria searchCriteria, int pageSize, int pageNumber, string orderBy, string direction, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                List<QuestionDetail> QuestionDetail = new List<QuestionDetail>();

                DbCommand getQuestionDeatilsCommand = HCMDatabase.GetStoredProcCommand("SPGET_TEST_QUESTIONS");
                string creditEarned = null;
                if (searchCriteria.CreditsEarned > 0)
                {
                    creditEarned = searchCriteria.CreditsEarned.ToString();
                }

                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@QUESTION_TYPE", DbType.Int16, (int)questionType);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@QUESTION_KEY", DbType.String, searchCriteria.QuestionKey);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@AUTHOR", DbType.String, searchCriteria.AuthorName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@AUTHOR_ID", DbType.Int32, searchCriteria.Author);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CAT_SUB", DbType.String, searchCriteria.CategorySubjectsKey);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CATEGORY", DbType.String, searchCriteria.CategoryName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@SUBJECT", DbType.String, searchCriteria.SubjectName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@KEYWORD", DbType.String, searchCriteria.Tag);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@TEST_AREA", DbType.String, searchCriteria.TestAreaID);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@COMPLEXITY", DbType.String, searchCriteria.Complexities);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CREDIT_EARNED", DbType.Decimal, creditEarned);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ACTIVE_FLAG", DbType.String, searchCriteria.ActiveFlag);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ORDERBYDIRECTION", DbType.String, direction);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CLIENT_REQUEST", DbType.String, searchCriteria.ClientRequest);
                dataReader = HCMDatabase.ExecuteReader(getQuestionDeatilsCommand);

                while (dataReader.Read())
                {

                    QuestionDetail questionDetails = new QuestionDetail();
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        int a = int.Parse(dataReader["CORECT_ANSWER"].ToString()) - 1;
                        questionDetails.QuestionID = int.Parse(dataReader["QUESTION_ID"].ToString());
                        questionDetails.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                        questionDetails.Question = dataReader["QUESTION_DESC"].ToString();
                        questionDetails.Complexity = dataReader["COMPLEXITY_NAME"].ToString();
                        questionDetails.Answer = short.Parse(a.ToString());
                        questionDetails.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                        questionDetails.ComplexityValue = dataReader["COMPLEXITY_VALUE"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["COMPLEXITY_VALUE"].ToString());
                        questionDetails.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                        questionDetails.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                        questionDetails.AnswerID = dataReader["CORRECT_ANSWER_DESCRIPTION"].ToString();

                        questionDetails.Status = (QuestionStatus)Convert.ToChar((dataReader["ACTIVE_FLAG"]));
                        questionDetails.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                        questionDetails.CreditsEarned = Convert.ToDecimal(dataReader["CREDIT_EARNED"]);
                        questionDetails.AverageTimeTaken = dataReader["AVG_TIME_TAKEN"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["AVG_TIME_TAKEN"].ToString());
                        questionDetails.QuestionRelationId = int.Parse(dataReader["QUESTION_RELATION_ID"].ToString());
                        questionDetails.DataAccessRights = dataReader["DATA_ACCESS_RIGHTS"].ToString();
                        QuestionDetail.Add(questionDetails);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }

                }
                dataReader.Close();
                //QuestionDetail questionDetails1 = new QuestionDetail();
                //questionDetails1.AnswerChoices = this.GetQuestionOptions("QSN_000014");
                //QuestionDetail.Add(questionDetails1);


                return QuestionDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will cancel the test session for a particular candidate.
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate 
        /// session information.
        /// </param>
        public void CancelTestSession(CandidateTestSessionDetail candidateTestSessionDetail)
        {
            // Create a stored procedure command object.
            DbCommand cancelTestSession = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CANDIDATE_SESSION_STATUS");

            // Add input parameters.

            HCMDatabase.AddInParameter(cancelTestSession,
                "@CAND_SESSION_KEY", DbType.String, candidateTestSessionDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(cancelTestSession,
                "@ATTEMPT_ID", DbType.Int16, candidateTestSessionDetail.AttemptID);

            HCMDatabase.AddInParameter(cancelTestSession,
                "@CANCEL_REASON", DbType.String, candidateTestSessionDetail.CancelReason);

            HCMDatabase.AddInParameter(cancelTestSession,
                "@MODIFIED_BY", DbType.Int32, candidateTestSessionDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(cancelTestSession);
        }

        /// <summary>
        /// This method is used to Active/inactive question based on the testKey.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the testKey.
        /// </param>
        /// <param name="testStatus">
        ///  A <see cref="string"/> that holds the testStatus.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void UpdateStatus(string testKey, string testStatus, int user)
        {
            DbCommand testUpdateStatusCommand = HCMDatabase.
                          GetStoredProcCommand("SPDELETE_TEST");
            HCMDatabase.AddInParameter(testUpdateStatusCommand,
                            "@TEST_KEY", DbType.String, testKey);
            HCMDatabase.AddInParameter(testUpdateStatusCommand,
                            "@ACTIVE_FLAG", DbType.String, testStatus);
            HCMDatabase.AddInParameter(testUpdateStatusCommand,
                            "@DELETED", DbType.String, null);
            HCMDatabase.AddInParameter(testUpdateStatusCommand,
                           "@USERID", DbType.Int32, user);
            HCMDatabase.ExecuteNonQuery(testUpdateStatusCommand);
        }

        /// <summary>
        /// This method is used to Delete Test based on the testKey.
        /// </summary>
        /// <param name="testKey">
        ///  A <see cref="string"/> that holds the testKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeleteTest(string testKey, int user)
        {
            DbCommand testUpdateStatusCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_TEST");
            HCMDatabase.AddInParameter(testUpdateStatusCommand,
                "@TEST_KEY", DbType.String, testKey);
            HCMDatabase.AddInParameter(testUpdateStatusCommand,
                "@ACTIVE_FLAG", DbType.String, null);
            HCMDatabase.AddInParameter(testUpdateStatusCommand,
                "@DELETED", DbType.String, "Y");
            HCMDatabase.AddInParameter(testUpdateStatusCommand,
                "@USERID", DbType.Int32, user);
            HCMDatabase.ExecuteNonQuery(testUpdateStatusCommand);
        }

        /// <summary>
        /// Method that retrieves the list of Tests for the given search 
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A list of <see cref="TestSearchCriteria"/> that holds the TestKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that holds the TestDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<TestDetail> GetTests(TestSearchCriteria testSearchCriteria, int pageSize, int pageNumber, string orderBy, SortType direction, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;
            try
            {
                string isCertification = null;
                if (testSearchCriteria.IsCertification != null)
                    isCertification = testSearchCriteria.IsCertification == true ? "Y" : "N";

                DbCommand getTestDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_TESTS");
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_KEY", DbType.String, testSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@QUESTION_TYPE", DbType.Int16, (int) testSearchCriteria.QuestionType);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AUTHOR", DbType.String, testSearchCriteria.TestAuthorName);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AUTHOR_ID", DbType.Int32, testSearchCriteria.TestAuthorID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CAT_SUB", DbType.String, testSearchCriteria.CategoriesID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CATEGORY", DbType.String, testSearchCriteria.Category);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@SUBJECT", DbType.String, testSearchCriteria.Subject);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AREA", DbType.String, testSearchCriteria.TestAreasID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_NAME", DbType.String, testSearchCriteria.Name);

                if (testSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.String, null);
                else
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.String, testSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestDetailCommand, "@SHOW_COPIED_TESTS", DbType.String, testSearchCriteria.ShowCopiedTests == true ? "Y" : "N");

                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_COST_START", DbType.Int32, testSearchCriteria.TestCostStart);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_COST_END", DbType.Int32, (testSearchCriteria.TestCostEnd == 0) ? 100 : testSearchCriteria.TestCostEnd);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@QUESTION_COUNT_START ", DbType.Int32, testSearchCriteria.TotalQuestionStart);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@QUESTION_COUNT_END", DbType.Int32, (testSearchCriteria.TotalQuestionEnd == 0) ? 100 : testSearchCriteria.TotalQuestionEnd);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CERTIFICATION ", DbType.String, isCertification);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);


                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                List<TestDetail> testDetails = null;
                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();


                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        TestDetail testDetail = new TestDetail();
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                        testDetail.Name = dataReader["TEST_NAME"].ToString();
                        testDetail.TestAuthorName = dataReader["USRNAME"].ToString();
                        testDetail.NoOfQuestions = int.Parse(dataReader["TOTAL_QUESTION"].ToString());
                        testDetail.CreatedDate = DateTime.Parse(dataReader["CREATED_DATE"].ToString());
                        testDetail.TestCost = decimal.Parse(dataReader["TEST_COST"].ToString());
                        testDetail.IsCertification = (Convert.ToChar(dataReader["CERTIFICATION"]) == 'Y') ? true : false;
                        testDetail.IsActive = (Convert.ToChar(dataReader["ACTIVE_FLAG"]) == 'Y') ? true : false;
                        testDetail.SessionIncluded = (dataReader["SESSION_INCLUDED"].ToString() == "0") ? true : false;
                        testDetail.SessionIncludedActive = (int.Parse(dataReader["SESSION_INCLUDED_ACTIVE"].ToString()) == 0) ? true : false;
                        testDetail.TestAuthorID = int.Parse(dataReader["USRID"].ToString());
                        testDetail.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        testDetail.DataAccessRights = dataReader["DATA_ACCESS_RIGHTS"].ToString();
                        testDetail.TenantID = int.Parse(dataReader["TENANT_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_REQUEST_NUMBER"]))
                            testDetail.PositionProfileID = int.Parse(dataReader["CLIENT_REQUEST_NUMBER"].ToString());

                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                dataReader.Close();
                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method is used to Delete  testquestion based on the question keys and testKey.
        /// </summary>
        /// <param name="questionsKeys">
        /// that holds the questionKeys comma sepearted values.
        /// </param>
        /// <param name="testKey">
        /// that holds the TestKey.
        /// </param>
        /// <param name="transaction">
        /// Maintaing tranaction from the Test.
        /// </param>
        /// <remarks>
        /// Delete the multiple Test question from the Test based on the QuestionKey and TestKey.
        /// </remarks>
        public void DeleteTestQuestion(string questionsKeys, string testKey, IDbTransaction transaction)
        {
            DbCommand testQuestionDeleteStatusCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_TEST_QUESTION");
            HCMDatabase.AddInParameter(testQuestionDeleteStatusCommand,
                "@QUESTION_KEY", DbType.String, questionsKeys);
            HCMDatabase.AddInParameter(testQuestionDeleteStatusCommand,
                "@TEST_KEY", DbType.String, testKey);
            HCMDatabase.ExecuteNonQuery(testQuestionDeleteStatusCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method communicates with DB and gets the test session records 
        /// from the DB satisfying the user search criteria
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A list of <see cref="TestSearchCriteria"/> that holds the TestKey,Category,Subject,etc..
        /// </param>
        /// /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="OrderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of test details</returns>
        public List<TestDetail> GetTestForTestSession(TestSearchCriteria testSearchCriteria, int PageNumber, int PageSize, string OrderBy, SortType direction, out int TotalNoofRecords)
        {
            IDataReader dataReader = null;
            try
            {
                TotalNoofRecords = 0;
                DbCommand getTestforTestSessionCommand = HCMDatabase.GetStoredProcCommand("SPGET_SEARCH_BYTEST");
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@TEST_KEY", DbType.String, testSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@QUESTION_TYPE", DbType.Int16, (int) testSearchCriteria.QuestionType);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@TEST_AUTHOR", DbType.String, testSearchCriteria.TestAuthorName);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@TEST_AUTHOR_ID", DbType.String, testSearchCriteria.TestAuthorID);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@CAT_SUB", DbType.String, testSearchCriteria.CategoriesID);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@CATEGORY", DbType.String, testSearchCriteria.Category);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@SUBJECT", DbType.String, testSearchCriteria.Subject);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@TEST_AREA", DbType.String, testSearchCriteria.TestAreasID);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@TEST_NAME", DbType.String, testSearchCriteria.Name);

                if (testSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@SHOW_COPIED_TESTS", DbType.String, testSearchCriteria.ShowCopiedTests == true ? "Y" : "N");

                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@TEST_COST_START", DbType.Int32, testSearchCriteria.TestCostStart);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@TEST_COST_END", DbType.Int32, (testSearchCriteria.TestCostEnd == 0) ? 100 : testSearchCriteria.TestCostEnd);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@QUESTION_COUNT_START ", DbType.Int32, testSearchCriteria.TotalQuestionStart);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@QUESTION_COUNT_END", DbType.Int32, (testSearchCriteria.TotalQuestionEnd == 0) ? 100 : testSearchCriteria.TotalQuestionEnd);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@CERTIFICATION ", DbType.String,
                    Utility.IsNullOrEmpty(testSearchCriteria.IsCertification) ? null : Convert.ToBoolean(testSearchCriteria.IsCertification) ? "Y" : "N");
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@PAGENUM", DbType.Int32, PageNumber);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@PAGESIZE", DbType.Int32, PageSize);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@ORDERBY", DbType.String, OrderBy);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                dataReader = HCMDatabase.ExecuteReader(getTestforTestSessionCommand);
                List<TestDetail> testDetails = null;
                TestDetail testDetail = null;
                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        TotalNoofRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    else
                    {
                        if (testDetail == null)
                            testDetail = new TestDetail();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                            testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                            testDetail.Name = dataReader["TEST_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AUTHOR"]))
                            testDetail.TestAuthorID = Convert.ToInt32(dataReader["TEST_AUTHOR"]);
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AUTHOR_NAME"]))
                            testDetail.TestAuthorName = dataReader["TEST_AUTHOR_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                            testDetail.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["NO_OF_QUESTIONS"]))
                            testDetail.NoOfQuestions = Convert.ToInt32(dataReader["NO_OF_QUESTIONS"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                            testDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_COST"]))
                            testDetail.TestCost = Convert.ToDecimal(dataReader["TEST_COST"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION"]))
                            testDetail.IsCertification = dataReader["CERTIFICATION"].ToString() == "N" ? false : true;
                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]))
                            testDetail.TestStatus = dataReader["ACTIVE"].ToString() == "Y" ? TestStatus.Active : TestStatus.Inactive;
                        testDetails.Add(testDetail);
                        testDetail = null;
                    }
                }
                return testDetails;
            }
            finally
            {
                if (!((Utility.IsNullOrEmpty(dataReader)) && (dataReader.IsClosed))) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
            }
        }


        /// <summary>
        /// To get the adaptive questions.
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="int"/> that holds the UserId 
        /// </param>
        /// <param name="UserSelectedQuestions">
        /// A <see cref="string"/> that holds the user selected questions.
        /// (User moved to Test Draft Panel) 
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size 
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number 
        /// </param>
        /// <param name="TotalNoOfRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// </param>
        /// <returns>
        /// A List for<see cref="QuestionDetail"/> List of question details 
        /// </returns>
        public List<QuestionDetail> GetAdaptiveQuestions(int UserId, string UserSelectedQuestions,
          int pageSize, int pageNumber, int questionAuthor, out int TotalNoOfRecords)
        {
            DbCommand GetAdaptiveQuestionsCommand = null;
            IDataReader dataReader = null;
            List<QuestionDetail> questionDetails = null;
            QuestionDetail questionDetail = null;
            try
            {
                TotalNoOfRecords = 0;
                GetAdaptiveQuestionsCommand = HCMDatabase.GetStoredProcCommand("SPGET_ADAPTIVE_QUESTIONS");
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@TEST_AUTHOR", DbType.Int32, UserId);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@AUTHOR_ID", DbType.Int32, questionAuthor);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@SELECTED_QUESTION_KEYS", DbType.String, UserSelectedQuestions);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@PAGESIZE", DbType.Int32, pageSize);
                dataReader = HCMDatabase.ExecuteReader(GetAdaptiveQuestionsCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        TotalNoOfRecords = Convert.ToInt32(dataReader["TOTAL"]);
                        continue;
                    }
                    if (Utility.IsNullOrEmpty(questionDetail))
                        questionDetail = new QuestionDetail();
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]))
                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                        questionDetail.ComplexityName = dataReader["COMPLEXITY_ID"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TAGS"]))
                        questionDetail.Tag = dataReader["TAGS"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"]))
                        questionDetail.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_RELATION_ID"]))
                        questionDetail.QuestionRelationId = Convert.ToInt32(dataReader["QUESTION_RELATION_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                        questionDetail.Complexity = dataReader["COMPLEXITY_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER_ID"]))
                        questionDetail.Answer = Convert.ToInt16(dataReader["ANSWER_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER_DESCRIPTION"]))
                        questionDetail.AnswerID = dataReader["ANSWER_DESCRIPTION"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["CREDIT_EARNED"]))
                        questionDetail.CreditsEarned = Convert.ToDecimal(dataReader["CREDIT_EARNED"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                        questionDetail.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                        questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                    if (Utility.IsNullOrEmpty(questionDetails))
                        questionDetails = new List<QuestionDetail>();
                    questionDetails.Add(questionDetail);
                    questionDetail = null;
                }
                return questionDetails;
            }
            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(GetAdaptiveQuestionsCommand)) GetAdaptiveQuestionsCommand = null;
                if (!Utility.IsNullOrEmpty(questionDetails)) questionDetails = null;
                if (!Utility.IsNullOrEmpty(questionDetail)) questionDetail = null;
            }
        }

        /// <summary>
        /// Gets the adaptive recommend questions based on the similar candidate
        /// </summary>
        /// <param name="CandidateId">Candidate id which has logged in to the
        /// system</param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="OrderByDirection">
        /// A<see cref="char"/>that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="SortExpression">
        /// A<see cref="string"/>that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of Test Details</returns>
        public List<TestDetail> GetAdaptiveTestBySimilarCandidate(int CandidateId, int PageSize, int PageNumber,
            char OrderByDirection, string SortExpression,
            out int TotalNoofRecords)
        {
            IDataReader dataReader = null;
            DbCommand AdaptiveTestBySimilarCandidateCommand = null;
            TestDetail testDetail = null;
            List<TestDetail> testDetails = null;
            try
            {
                TotalNoofRecords = 0;
                AdaptiveTestBySimilarCandidateCommand = HCMDatabase.GetStoredProcCommand("SPGET_ADAPTIVE_TEST_BY_SIMILAR_CANDIDATE");
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarCandidateCommand, "@CANDIDATE_ID", DbType.Int32, CandidateId);
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarCandidateCommand, "@PAGENUM", DbType.Int32, PageNumber);
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarCandidateCommand, "@PAGESIZE", DbType.Int32, PageSize);
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarCandidateCommand, "@ORDERBYDIRECTION", DbType.String, OrderByDirection);
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarCandidateCommand, "@ORDERBY", DbType.String, SortExpression);
                dataReader = HCMDatabase.ExecuteReader(AdaptiveTestBySimilarCandidateCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        TotalNoofRecords = Convert.ToInt32(dataReader["TOTAL"]);
                        continue;
                    }
                    if (Utility.IsNullOrEmpty(testDetail))
                        testDetail = new TestDetail();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        testDetail.Name = dataReader["TEST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        testDetail.Description = dataReader["TEST_DESCRIPTION"].ToString().Replace("\r\n", "<br />");
                    if (!Utility.IsNullOrEmpty(dataReader["RECOMMENDED_TIME"]))
                        testDetail.RecommendedCompletionTime = Convert.ToInt32(dataReader["RECOMMENDED_TIME"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]))
                        testDetail.IsActive = (dataReader["ACTIVE"].ToString() == "Y") ? true : false;
                    if (!Utility.IsNullOrEmpty(dataReader["DELETED"]))
                        testDetail.IsDeleted = (dataReader["DELETED"].ToString() == "N") ? false : true;
                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION"]))
                        testDetail.IsCertification = (dataReader["CERTIFICATION"].ToString() == "N") ? false : true;
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                        testDetail.NoOfQuestions = Convert.ToInt32(dataReader["TOTAL_QUESTION"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_COST"]))
                        testDetail.TestCost = Convert.ToDecimal(dataReader["TEST_COST"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_CREATED"]))
                        testDetail.CreatedDate = Convert.ToDateTime(dataReader["TEST_CREATED"]);
                    if (Utility.IsNullOrEmpty(testDetails))
                        testDetails = new List<TestDetail>();
                    testDetails.Add(testDetail);
                    testDetail = null;
                }
                return testDetails;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(AdaptiveTestBySimilarCandidateCommand)) AdaptiveTestBySimilarCandidateCommand = null;
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
            }
        }

        /// <summary>
        /// Gets the adaptive recommend questions based on the similar profile
        /// </summary>
        /// <param name="CandidateId">Candidate id which has logged in to the
        /// system</param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="OrderByDirection">
        /// A<see cref="char"/>that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="SortExpression">
        /// A<see cref="string"/>that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of Test Details</returns>
        public List<TestDetail> GetAdaptiveTestBySimilarProfile(int CandidateId, int PageSize, int PageNumber,
            char OrderByDirection, string SortExpression,
            out int TotalNoofRecords)
        {
            IDataReader dataReader = null;
            DbCommand AdaptiveTestBySimilarProfileCommand = null;
            TestDetail testDetail = null;
            List<TestDetail> testDetails = null;
            try
            {
                TotalNoofRecords = 0;
                AdaptiveTestBySimilarProfileCommand = HCMDatabase.GetStoredProcCommand("SPGET_ADAPTIVE_TEST_BY_PROFILE");
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarProfileCommand, "@CANDIDATE_ID", DbType.Int32, CandidateId);
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarProfileCommand, "@PAGENUM", DbType.Int32, PageNumber);
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarProfileCommand, "@PAGESIZE", DbType.Int32, PageSize);
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarProfileCommand, "@ORDERBYDIRECTION", DbType.String, OrderByDirection);
                HCMDatabase.AddInParameter(AdaptiveTestBySimilarProfileCommand, "@ORDERBY", DbType.String, SortExpression);
                dataReader = HCMDatabase.ExecuteReader(AdaptiveTestBySimilarProfileCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        TotalNoofRecords = Convert.ToInt32(dataReader["TOTAL"]);
                        continue;
                    }
                    if (Utility.IsNullOrEmpty(testDetail))
                        testDetail = new TestDetail();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        testDetail.Name = dataReader["TEST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        testDetail.Description = dataReader["TEST_DESCRIPTION"].ToString().Replace("\r\n", "<br />");
                    if (!Utility.IsNullOrEmpty(dataReader["RECOMMENDED_TIME"]))
                        testDetail.RecommendedCompletionTime = Convert.ToInt32(dataReader["RECOMMENDED_TIME"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]))
                        testDetail.IsActive = (dataReader["ACTIVE"].ToString() == "Y") ? true : false;
                    if (!Utility.IsNullOrEmpty(dataReader["DELETED"]))
                        testDetail.IsDeleted = (dataReader["DELETED"].ToString() == "N") ? false : true;
                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION"]))
                        testDetail.IsCertification = (dataReader["CERTIFICATION"].ToString() == "N") ? false : true;
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                        testDetail.NoOfQuestions = Convert.ToInt32(dataReader["TOTAL_QUESTION"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_COST"]))
                        testDetail.TestCost = Convert.ToDecimal(dataReader["TEST_COST"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        testDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                    if (Utility.IsNullOrEmpty(testDetails))
                        testDetails = new List<TestDetail>();
                    testDetails.Add(testDetail);
                    testDetail = null;
                }
                return testDetails;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(AdaptiveTestBySimilarProfileCommand)) AdaptiveTestBySimilarProfileCommand = null;
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
            }
        }

        /// <summary>
        /// Gets the adaptive recommend questions based on the assessment history
        /// </summary>
        /// <param name="CandidateId">Candidate id which has logged in to the
        /// system</param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="OrderByDirection">
        /// A<see cref="char"/>that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="SortExpression">
        /// A<see cref="string"/>that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of Test Details</returns>
        public List<TestDetail> GetAdaptiveTestByAssessmentHistory(int CandidateId, int PageSize, int PageNumber,
            char OrderByDirection, string SortExpression,
            out int TotalNoofRecords)
        {
            IDataReader dataReader = null;
            DbCommand AdaptiveTestByAssessmentHistoryCommand = null;
            TestDetail testDetail = null;
            List<TestDetail> testDetails = null;
            try
            {
                TotalNoofRecords = 0;
                AdaptiveTestByAssessmentHistoryCommand = HCMDatabase.GetStoredProcCommand("SPGET_ADAPTIVE_TEST_BY_ASSESSMENT_HISTORY");
                HCMDatabase.AddInParameter(AdaptiveTestByAssessmentHistoryCommand, "@CANDIDATE_ID", DbType.Int32, CandidateId);
                HCMDatabase.AddInParameter(AdaptiveTestByAssessmentHistoryCommand, "@PAGENUM", DbType.Int32, PageNumber);
                HCMDatabase.AddInParameter(AdaptiveTestByAssessmentHistoryCommand, "@PAGESIZE", DbType.Int32, PageSize);
                HCMDatabase.AddInParameter(AdaptiveTestByAssessmentHistoryCommand, "@ORDERBYDIRECTION", DbType.String, OrderByDirection);
                HCMDatabase.AddInParameter(AdaptiveTestByAssessmentHistoryCommand, "@ORDERBY", DbType.String, SortExpression);
                dataReader = HCMDatabase.ExecuteReader(AdaptiveTestByAssessmentHistoryCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        TotalNoofRecords = Convert.ToInt32(dataReader["TOTAL"]);
                        continue;
                    }
                    if (Utility.IsNullOrEmpty(testDetail))
                        testDetail = new TestDetail();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        testDetail.Name = dataReader["TEST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        testDetail.Description = dataReader["TEST_DESCRIPTION"].ToString().Replace("\r\n", "<br />");
                    if (!Utility.IsNullOrEmpty(dataReader["RECOMMENDED_TIME"]))
                        testDetail.RecommendedCompletionTime = Convert.ToInt32(dataReader["RECOMMENDED_TIME"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]))
                        testDetail.IsActive = (dataReader["ACTIVE"].ToString() == "Y") ? true : false;
                    if (!Utility.IsNullOrEmpty(dataReader["DELETED"]))
                        testDetail.IsDeleted = (dataReader["DELETED"].ToString() == "N") ? false : true;
                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION"]))
                        testDetail.IsCertification = (dataReader["CERTIFICATION"].ToString() == "N") ? false : true;
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                        testDetail.NoOfQuestions = Convert.ToInt32(dataReader["TOTAL_QUESTION"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_COST"]))
                        testDetail.TestCost = Convert.ToDecimal(dataReader["TEST_COST"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        testDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                    if (Utility.IsNullOrEmpty(testDetails))
                        testDetails = new List<TestDetail>();
                    testDetails.Add(testDetail);
                    testDetail = null;
                }
                return testDetails;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(AdaptiveTestByAssessmentHistoryCommand)) AdaptiveTestByAssessmentHistoryCommand = null;
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
            }
        }

        /// <summary>
        /// Represents the method to insert into the credit history table
        /// </summary>
        /// <param name="creditRequestDetail">
        /// A<see cref="CreditRequestDetail"/>that holds the credit request details
        /// </param>
        /// <param name="transaction">
        /// A<see cref="IDbTransaction"/>that holds the transaction
        /// </param>
        public void InsertIntoCreditHistory(CreditRequestDetail creditRequestDetail,
            IDbTransaction transaction)
        {
            DbCommand creditHistoryInsertCommand = HCMDatabase.GetStoredProcCommand
                ("SPINSERT_CREDIT_HISTORY");

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@USER_ID",
                  DbType.Int32, creditRequestDetail.CandidateId);

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@CREDIT_TYPE",
                DbType.String, creditRequestDetail.CreditType == CreditType.Earned ?
                Constants.CreditTypeConstants.CREDIT_EARNED :
                Constants.CreditTypeConstants.CREDIT_USED);

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@CREDIT_INFO",
                DbType.String, creditRequestDetail.CreditRequestDescription);

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@AMOUNT",
                DbType.Decimal, creditRequestDetail.Amount);

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@CREATED_BY",
                DbType.String, creditRequestDetail.CreatedBy);

            HCMDatabase.ExecuteNonQuery(creditHistoryInsertCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Represents the method to get the credit value for the candidate
        /// who created the test session 
        /// </summary>
        /// <returns>
        /// A<see cref="decimal"/>that holds the credit value
        /// </returns>
        public decimal GetCreditValue()
        {
            decimal credit = 0.0M;

            DbCommand creditValueSelectCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_CREDIT_VALUE");

            HCMDatabase.AddInParameter(creditValueSelectCommand, "@CREDIT_ID",
                  DbType.String, Constants.AttributeTypes.CREDIT_TEST_SESSION);

            HCMDatabase.AddInParameter(creditValueSelectCommand, "@CREDIT_TYPE",
                DbType.String, Constants.TestSessionCreditConstants.TEST_SESSION_CANDIDATE_CREDIT);

            object cred = HCMDatabase.ExecuteScalar(creditValueSelectCommand);

            credit = decimal.Parse(cred.ToString());

            return credit;
        }

        public List<QuestionDetail> GetCandidateTestQuestionDetail(string testKey, string candidateSessionKey, int attemptID)
        {
            IDataReader dataReader = null;
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            try
            {
                // Create command object
                DbCommand getCandidateTestQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_TEST_RESULT");

                // Add input parameter
                HCMDatabase.AddInParameter(getCandidateTestQuestionDetailCommand, "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getCandidateTestQuestionDetailCommand, "@CANDIDATE_SESSION_KEY", DbType.String, candidateSessionKey);
                HCMDatabase.AddInParameter(getCandidateTestQuestionDetailCommand, "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getCandidateTestQuestionDetailCommand);
                dataReader = HCMDatabase.ExecuteReader(getCandidateTestQuestionDetailCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        QuestionDetail questionDetail = new QuestionDetail();

                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString().Trim();
                        questionDetail.QuestionType = (QuestionType)System.Convert.ToInt32(dataReader["QUESTION_TYPE"]);
                        if (questionDetail.QuestionType == QuestionType.OpenText)
                        {
                            questionDetail.QuestionAttribute = new QuestionAttribute();
                            questionDetail.QuestionAttribute.Marks = dataReader["MARKS"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["MARKS"].ToString());
                            questionDetail.QuestionAttribute.MarksObtained = dataReader["MARKS_OBTAINED"] == DBNull.Value ? 0 : Convert.ToInt32(dataReader["MARKS_OBTAINED"].ToString());
                        }
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                        questionDetail.Complexity = dataReader["COMPLEXITY"].ToString().Trim();

                        questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString().Trim();
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();

                        questionDetail.AbsoluteScore = dataReader["SCORE"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["SCORE"].ToString());
                        questionDetail.QuestionWeightage = dataReader["QUESTION_SCORE"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["QUESTION_SCORE"].ToString());
                        questionDetail.RelativeScore = dataReader["REL_SCORE"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["REL_SCORE"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                            questionDetail.TimeTaken = Convert.ToInt32(dataReader["TIME_TAKEN"]);

                        questionDetail.Skipped = dataReader["SKIPPED"].ToString() == "N" ? "No" : "Yes";

                        questionDetails.Add(questionDetail);
                    }
                    dataReader.Close();
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts the scheduled candidates into the position
        /// profile candidate table against candidate test & interview session 
        /// IDs and position profile.
        /// </summary>
        /// <param name="positionProfileCandidate">
        /// A <see cref="string"/> that holds the position profile candidate 
        /// details.
        /// </param>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. Value T indicates test
        /// and I indicates interview.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void InsertPositionProfileCandidate(PositionProfileCandidate positionProfileCandidate, 
            string type, IDbTransaction transaction)
        {
            DbCommand positionProfileCandidatesInsertCommand = HCMDatabase.GetStoredProcCommand
                ("SPINSERT_SCHEDULED_POSITION_PROFILE_CANDIDATES");

            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@POSITION_PROFILE_ID",
                DbType.Int32, positionProfileCandidate.PositionProfileID);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@USER_ID",
                  DbType.Int32, positionProfileCandidate.CreatedBy);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@CANDIDATE_ID",
                  DbType.Int32, positionProfileCandidate.CandidateID);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@PICKED_BY",
                DbType.Int32, positionProfileCandidate.PickedBy);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@PICKED_DATE",
                DbType.DateTime, positionProfileCandidate.PickedDate);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@STATUS",
               DbType.String, positionProfileCandidate.Status);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@SCHEDULED_DATE",
                DbType.DateTime, positionProfileCandidate.SchelduleDate);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@RESUME_SCORE",
                DbType.Int32, positionProfileCandidate.ResumeScore);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@CANDIDATE_SESSION_ID",
                DbType.String, positionProfileCandidate.CandidateSessionID);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@ATTEMPT_ID",
                DbType.Int32, positionProfileCandidate.AttemptID);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@SOURCE_FROM",
                DbType.String, positionProfileCandidate.SourceFrom);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@TYPE",
                DbType.String, type);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@CAND_ONLINE_INTERVIEW_ID",
                DbType.Int32, positionProfileCandidate.CandidateOnlineInterviewID);

            HCMDatabase.ExecuteNonQuery(positionProfileCandidatesInsertCommand, transaction as DbTransaction);
        }

        #endregion

        /// <summary>
        /// Method that retrieves assoiciated position profile details for the 
        /// given test key.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileDetail"/> that holds the position 
        /// profile and test detail.
        /// </returns>
        public PositionProfileDetail GetPositionProfileTestDetail(string testKey)
        {
            IDataReader dataReader = null;
            PositionProfileDetail positionProfileDetail = null;

            try
            {
                DbCommand getPositionProfileDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_TEST");

                // Add input parameter.
                HCMDatabase.AddInParameter(getPositionProfileDetailCommand, "@TEST_KEY", DbType.String, testKey);

                // Execute the reader.
                dataReader = HCMDatabase.ExecuteReader(getPositionProfileDetailCommand);

                if (dataReader.Read())
                {
                    positionProfileDetail = new PositionProfileDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        positionProfileDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        positionProfileDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        positionProfileDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_NAME"]))
                        positionProfileDetail.PositionProfileOwnerName = dataReader["POSITION_PROFILE_OWNER_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_EMAIL"]))
                        positionProfileDetail.PositionProfileOwnerEmail = dataReader["POSITION_PROFILE_OWNER_EMAIL"].ToString().Trim();
                }

                return positionProfileDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public void UpdateTest(string testKey, int positionProfileID, int userID)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand insertTestCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_PP_TEST");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@TEST_KEY", DbType.String, testKey);

                HCMDatabase.AddInParameter(insertTestCommand,
                    "@MODIFIED_BY", DbType.Int32, userID);

                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);


                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(insertTestCommand);
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }


        public void InsertInterviewTestQusetions(string testKey, string questionKey, int questionRelationId, int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestQuestionsCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_INTERVIEW_TEST_QUESTION");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, testKey);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@QUESTION_RELATION_ID", DbType.Int32, questionRelationId);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestQuestionsCommand, transaction as DbTransaction);
        }

        public List<TestDetail> GetRecommendedTestDetails(string skills, string sortExpression, string sortOrder, int pageSize, int pageNumber, out int totalPage)
        {
            IDataReader dataReader = null;
            try
            {
                totalPage = 0;
                DbCommand getrecommendedTestCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_RECOMMENDED_TEST");

                HCMDatabase.AddInParameter(getrecommendedTestCommand, "@SKILLS", DbType.String, skills);
                HCMDatabase.AddInParameter(getrecommendedTestCommand, "@ORDERBY", DbType.String, sortExpression);
                HCMDatabase.AddInParameter(getrecommendedTestCommand, "@ORDERBYDIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getrecommendedTestCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getrecommendedTestCommand, "@PAGESIZE", DbType.Int32, pageSize);

                List<TestDetail> recommendedTestDetails = null;

                TestDetail testDetail = null;

                dataReader = HCMDatabase.ExecuteReader(getrecommendedTestCommand);

                while (dataReader.Read())
                {

                    if (recommendedTestDetails == null)
                        recommendedTestDetails = new List<TestDetail>();
                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalPage = int.Parse(dataReader["COUNT"].ToString());
                    }
                    else
                    {
                        testDetail = new TestDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_NAME"]))
                        {
                            testDetail.Name = dataReader["TEST_RECOMMEND_NAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_DESCRIPTION"]))
                        {
                            testDetail.Description = dataReader["TEST_RECOMMEND_DESCRIPTION"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TOTAL_QUESTION"]))
                        {
                            testDetail.NoOfQuestions = Convert.ToInt16(dataReader["TEST_RECOMMEND_TOTAL_QUESTION"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TIME"]))
                        {
                            testDetail.RecommendedCompletionTime = Convert.ToInt16(dataReader["TEST_RECOMMEND_TIME"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_ID"]))
                        {
                            testDetail.TestRecommendedID = Convert.ToInt16(dataReader["TEST_RECOMMEND_ID"].ToString());
                        }


                        recommendedTestDetails.Add(testDetail);
                    }
                }
                /* dataReader.NextResult();

                 List<QuestionDetail> recommendedTestSeachQuestions = new List<QuestionDetail>();

                 QuestionDetail searchQuestion = null;

                 while (dataReader.Read())
                 {
                     searchQuestion = new QuestionDetail();

                     searchQuestion.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                     searchQuestion.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                     searchQuestion.Complexity = dataReader["COMPLEXITY"].ToString();
                 
                     recommendedTestSeachQuestions.Add(searchQuestion);
                 }
                 List<TestStatistics> testStat = new List<TestStatistics>();
                 TestStatistics teststatistics = new TestStatistics();

                 teststatistics.Test = new List<TestDetail>();
                 teststatistics.TestDetails = new List<QuestionDetail>();
                 teststatistics.Test = recommendedTestDetails;
                 teststatistics.TestDetails = recommendedTestSeachQuestions;*/
                return recommendedTestDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        public List<TestSearchCriteria> GetSearchCriteria(int testRecommendedId)
        {
            IDataReader dataReader = null;

            try
            {
                // Construct command object.
                DbCommand getrecommendedTestCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_CANDIDATE_RECOMMENDED_TEST_SEARCHCRITERIA");

                // Add input parameters.
                HCMDatabase.AddInParameter(getrecommendedTestCommand, 
                    "@TEST_RECOMMEND_ID", DbType.String, testRecommendedId);

                List<TestSearchCriteria> criterias = null;

                TestSearchCriteria criteria = null;

                dataReader = HCMDatabase.ExecuteReader(getrecommendedTestCommand);

                while (dataReader.Read())
                {
                    // Instantiate criteria list.
                    if (criterias == null)
                        criterias = new List<TestSearchCriteria>();

                    // Instantiate criteria object.
                    criteria = new TestSearchCriteria();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_SUB_ID"]))
                    {
                        criteria.CategoriesID = dataReader["TEST_SEGMENT_SUB_ID"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_WEIGHTAGE"]))
                    {
                        criteria.Weightage = dataReader["TEST_SEGMENT_WEIGHTAGE"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_KEYWORD"]))
                    {
                        criteria.Keyword = dataReader["TEST_SEGMENT_KEYWORD"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_TESTAREA"]))
                    {
                        criteria.TestAreasID = dataReader["TEST_SEGMENT_TESTAREA"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_COMPLEXITY"]))
                    {
                        criteria.Complexity = dataReader["TEST_SEGMENT_COMPLEXITY"].ToString();
                    }

                    // Add to the criteria list.
                    criterias.Add(criteria);
                }
                return criterias;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that inserts the candidate recommended test details and 
        /// returns the generated ID.
        /// </summary>
        /// <param name="testDetail">
        /// A <see cref="TestDetail"/> that holds the test detail.
        /// </param>
        /// <param name="isNew">
        /// A <see cref="bool"/> that holds the output parameter which indicates 
        /// whether the record is newly saved or already exist.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the generated ID.
        /// </returns>
        public int InsertCandidateRecommendedTest
            (TestDetail testDetail, out bool isNew)
        {
            // Set default value for output parameter.
            isNew = false;

            DbCommand insertRecommendedTestCommand = HCMDatabase.
              GetStoredProcCommand("SPINSERT_CANDIDATE_RECOMMENDED_TEST");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertRecommendedTestCommand,
                "@USER_ID", DbType.Int16, testDetail.UserID);
            HCMDatabase.AddInParameter(insertRecommendedTestCommand,
                "@TEST_RECOMMEND_ID", DbType.Int16, testDetail.TestRecommendedID);         
            HCMDatabase.AddInParameter(insertRecommendedTestCommand,
                "@STATUS", DbType.String, testDetail.Status);
            HCMDatabase.AddInParameter(insertRecommendedTestCommand,
                "@CREATED_BY", DbType.Int32, testDetail.UserID);

            // Add 'out' parameters.
            HCMDatabase.AddOutParameter(insertRecommendedTestCommand, "@GEN_ID", DbType.Int32, 4);
            HCMDatabase.AddOutParameter(insertRecommendedTestCommand, "@RECORD_STATUS", DbType.Int32, 4);

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(insertRecommendedTestCommand);

            // Retrieve the RECORD_STATUS output parameter value.
            if (insertRecommendedTestCommand.Parameters["@RECORD_STATUS"].Value != null &&
                insertRecommendedTestCommand.Parameters["@RECORD_STATUS"].Value != DBNull.Value)
            {
                if (int.Parse(insertRecommendedTestCommand.Parameters["@RECORD_STATUS"].Value.ToString()) == 1)
                    isNew = true;
            }

            // Retrieve the GEN_ID output parameter value.
            if (insertRecommendedTestCommand.Parameters["@GEN_ID"].Value != null &&
                insertRecommendedTestCommand.Parameters["@GEN_ID"].Value != DBNull.Value)
            {
                return int.Parse(insertRecommendedTestCommand.Parameters["@GEN_ID"].Value.ToString());
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Method that retrieves the recommended test as well as candidate
        /// details for the given recommended test gen ID.
        /// </summary>
        /// <param name="genID">
        /// A <see cref="int"/> that holds the gen ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestRecommendationDetail"/> that contains recommended 
        /// test and candidate details.
        /// </returns>
        /// <remarks>
        /// This helps in sending email to candidate when a test recommendation
        /// is saved.
        /// </remarks>
        public TestRecommendationDetail GetCandidateRecommendedTest(int genID)
        {
            IDataReader dataReader = null;
            TestRecommendationDetail candidataTestDetail = null;

            try
            {
                DbCommand getRecommendedTestCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_RECOMMENDED_TEST_BY_ID");

                // Add input parameter.
                HCMDatabase.AddInParameter(getRecommendedTestCommand, 
                    "@GEN_ID", DbType.Int32, genID);

                // Execute the query.
                dataReader = HCMDatabase.ExecuteReader(getRecommendedTestCommand);

                if (dataReader.Read())
                {
                    // Instantiate recommended test detail object.
                    candidataTestDetail = new TestRecommendationDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        candidataTestDetail.UserId = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_ID"]))
                        candidataTestDetail.TestRecommendId = Convert.ToInt32(dataReader["TEST_RECOMMEND_ID"]); ;

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                        candidataTestDetail.Status = dataReader["STATUS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_NAME"]))
                        candidataTestDetail.TestName = dataReader["TEST_RECOMMEND_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_SKILL"]))
                        candidataTestDetail.Skill = dataReader["TEST_RECOMMEND_SKILL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        candidataTestDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                        candidataTestDetail.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString();
                }

                return candidataTestDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
    }
}
