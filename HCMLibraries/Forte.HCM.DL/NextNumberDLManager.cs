﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// NextNumberDLManager.cs
// Class that represents the data layer for the next number generator
// manager. This will talk to the database to perform the operations 
// associated with next number generation processes.

#endregion

#region Directives                                                             
using System;
using System.Data;
using System.Data.Common;

using Forte.HCM.Common.DL;
using Forte.HCM.Exceptions;

#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the next number generator
    /// manager. This will talk to the database to perform the operations 
    /// associated with next number generation processes.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class NextNumberDLManager : DatabaseConnectionManager
    {
        #region Public Method                                                  
        /// <summary>
        /// Represents the default constructor.
        /// </summary>
        public NextNumberDLManager()
        {

        }

        /// <summary>
        /// Represents the method that calls the stored procedure <b>SPGET_NEXT_NUMBER </b> 
        /// and retrieves the next number for the given object type.
        /// </summary>
        /// <param name="transaction">
        /// A <seealso cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        /// <param name="objectType">
        /// A <seealso cref="string"/> that holds the object type.
        /// </param>
        /// <param name="userID">
        /// A <seealso cref="int"/> that holds the user ID. 
        /// </param>
        public string GetNextNumber(IDbTransaction transaction, string objectType, int userID)
        {
            try
            {
                // Check if object type is given.
                if (string.IsNullOrEmpty(objectType) || objectType.Trim().Length == 0)
                {
                    throw new NextNumberException
                        ("Object type cannot be null or empty");
                }

                // This SP helps to generate Question ID 
                DbCommand nextNumberCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_NEXT_NUMBER_OUTPUT");

                HCMDatabase.AddInParameter(
                    nextNumberCommand,
                    "@OBJ_TYPE",
                    DbType.String,
                    objectType.Trim());

                HCMDatabase.AddInParameter(
                   nextNumberCommand,
                   "@MODIFIED_BY",
                   DbType.Int32, userID);

                HCMDatabase.AddOutParameter(
                    nextNumberCommand,
                    "@NEXT_NUMBER",
                    DbType.String,18);

                object returnValue 
                    = HCMDatabase.ExecuteNonQuery(nextNumberCommand, transaction as DbTransaction);

                returnValue = HCMDatabase.GetParameterValue(nextNumberCommand, "@NEXT_NUMBER");

                if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                    return null;

                return returnValue.ToString().Trim();
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        #endregion Public Method
    }
}