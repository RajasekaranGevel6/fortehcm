﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PositionProfileDLManager.cs
// File that represents the data layer for the position profile module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the position profile module.
    /// This will talk to the database to perform the operations associated
    /// with the module.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class PositionProfileDLManager : DatabaseConnectionManager
    {
        /// <summary>
        /// Gets the position profile segment.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <returns></returns>
        public List<SegmentDetail> GetPositionProfileSegment(int formID)
        {
            IDataReader dataReader = null;
            List<SegmentDetail> segmentDetailList = new List<SegmentDetail>();
            try
            {
                // Create command object
                DbCommand getTestQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_ENTRY_FORMSEGMENTS");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@FORM_ID", DbType.Int32, formID);
                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getTestQuestionDetailCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        SegmentDetail segmentDetail = new SegmentDetail();
                        segmentDetail.DisplayOrder = int.Parse(dataReader["DISPLAY_ORDER"].ToString()); ;
                        segmentDetail.SegmentID = int.Parse(dataReader["SEGMENT_ID"].ToString());
                        segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString().Trim();
                        segmentDetail.segmentPath = dataReader["SEGMENT_PATH"].ToString().Trim();
                        segmentDetailList.Add(segmentDetail);
                    }
                    dataReader.Close();
                }
                return segmentDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// Gets the form.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public List<FormDetail> GetForm(int? tenantID)
        {
            IDataReader dataReader = null;
            List<FormDetail> formDetailList = new List<FormDetail>();
            try
            {
                // Create command object
                DbCommand getFomrsCommand =
                    HCMDatabase.GetStoredProcCommand("SPPPGET_FORMS");
                HCMDatabase.AddInParameter(getFomrsCommand, "@TENANT_ID", DbType.Int32, tenantID);
                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getFomrsCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        FormDetail formDetail = new FormDetail();
                        formDetail.FormID = int.Parse(dataReader["FORM_ID"].ToString());
                        formDetail.FormName = dataReader["FORM_NAME"].ToString().Trim();
                        formDetail.Tags = dataReader["TAGS"].ToString();
                        formDetailList.Add(formDetail);
                    }
                    dataReader.Close();
                }
                return formDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the form.
        /// </summary>
        /// <param name="userID">The tenant ID.</param>
        /// <returns></returns>
        public List<FormDetail> GetFormWithUsers(int? userID)
        {
            IDataReader dataReader = null;
            List<FormDetail> formDetailList = new List<FormDetail>();
            try
            {
                // Create command object
                DbCommand getFomrsCommand =
                    HCMDatabase.GetStoredProcCommand("SPPPGET_FORMS");
                HCMDatabase.AddInParameter(getFomrsCommand, "@USRID", DbType.Int32, userID);
                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getFomrsCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        FormDetail formDetail = new FormDetail();
                        formDetail.FormID = int.Parse(dataReader["FORM_ID"].ToString());
                        formDetail.FormName = dataReader["FORM_NAME"].ToString().Trim();
                        formDetail.Tags = dataReader["TAGS"].ToString();
                        formDetailList.Add(formDetail);
                    }
                    dataReader.Close();
                }
                return formDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Gets the position profile.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="sortField">The sort field.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="clientPositionDetail">The client position detail.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public List<PositionProfileDetail> GetPositionProfile(string sortOrder, string sortField,
            int pageNumber, int pageSize, out int totalRecords, PositionProfileDetailSearchCriteria clientPositionDetail, int userID)
        {
            List<PositionProfileDetail> positionProfileDetails = new List<PositionProfileDetail>();
            IDataReader dataReader = null;

            try
            {
                // Create command object
                DbCommand getPositionProfileCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_DETAILS");

                totalRecords = 0;

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@POSITION_ID", DbType.String,
                    clientPositionDetail.PositionID);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@POSITION_NAME", DbType.String,
                    clientPositionDetail.PositionName);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@CLIENT_NAME", DbType.String,
                     clientPositionDetail.ClientName);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@LOCATION", DbType.String,
                    clientPositionDetail.Location);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@NATURE_OF_POSITION", DbType.String,
                    clientPositionDetail.NatureOfPosition);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@POSITION_PROFILE_NAME", DbType.String, clientPositionDetail.PositionProfileName);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@CONTACT_NAME", DbType.String,
                    clientPositionDetail.ContactName);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@DEPARTMENT_NAME", DbType.String,
                    clientPositionDetail.DepartmentName);

                if (clientPositionDetail.ClientID != 0)
                {
                    HCMDatabase.AddInParameter(getPositionProfileCommand, "@CLIENT_ID", DbType.Int32,
                        clientPositionDetail.ClientID);
                }
                else
                {
                    HCMDatabase.AddInParameter(getPositionProfileCommand, "@CLIENT_ID", DbType.Int32,
                        null);
                }

                if (clientPositionDetail.DepartmentID != 0)
                {
                    HCMDatabase.AddInParameter(getPositionProfileCommand, "@DEPARTMENT_ID", DbType.Int32,
                        clientPositionDetail.DepartmentID);
                }
                else
                {
                    HCMDatabase.AddInParameter(getPositionProfileCommand, "@DEPARTMENT_ID", DbType.Int32,
                        null);
                }

                if (clientPositionDetail.ContactID != 0)
                {
                    HCMDatabase.AddInParameter(getPositionProfileCommand, "@CONTACT_ID", DbType.Int32,
                        clientPositionDetail.ContactID);
                }
                else
                {
                    HCMDatabase.AddInParameter(getPositionProfileCommand, "@CONTACT_ID", DbType.Int32,
                        null);
                }

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@PAGE_NUM", DbType.String, pageNumber);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@PAGE_SIZE", DbType.String, pageSize);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@USER_ID", DbType.String, userID);

                if (clientPositionDetail.CreatedBy == 0)
                    HCMDatabase.AddInParameter(getPositionProfileCommand, "@CREATED_BY", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getPositionProfileCommand, "@CREATED_BY", DbType.Int32, clientPositionDetail.CreatedBy);

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@OWNER_TYPE", DbType.String, clientPositionDetail.ShowType);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getPositionProfileCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                     
                    while (dataReader.Read())
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                        {
                            totalRecords = int.Parse(dataReader["COUNT"].ToString());
                            continue;
                        }
                        PositionProfileDetail positionProfile = new PositionProfileDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY_NAME"]))
                        {
                            positionProfile.CreatedByName = dataReader["CREATED_BY_NAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        {
                            positionProfile.PositionProfileID = int.Parse(dataReader["POSITION_PROFILE_ID"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_ID"]))
                        {
                            positionProfile.ClientPositionName = dataReader["POSITION_ID"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        {
                            positionProfile.ClientName = dataReader["CLIENT_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_NAME"]))
                        {
                            positionProfile.PositionName = dataReader["POSITION_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["NUMBER_OF_POSITION"]))
                        {
                            positionProfile.NoOfPositions = int.Parse(dataReader["NUMBER_OF_POSITION"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        {
                            positionProfile.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["REGISTRATION_DATE"]))
                        {   
                            positionProfile.Date = Convert.ToDateTime(dataReader["REGISTRATION_DATE"].ToString()).ToShortDateString();
                        } 
                       
                        if (!Utility.IsNullOrEmpty(dataReader["NO_OF_CANDIDATE"]))
                        {
                            positionProfile.NoOfAssessedCandidates = int.Parse(dataReader["NO_OF_CANDIDATE"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                        {
                            positionProfile.PositionName = dataReader["STATUS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CLOSED"]))
                        {
                            positionProfile.Closed = dataReader["CLOSED"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["DEPARTMENT_NAME"]))
                        {
                            positionProfile.DepartmentName = dataReader["DEPARTMENT_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CONTACT_NAME"]))
                        {
                            positionProfile.ContactName = dataReader["CONTACT_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["KEYS"]))
                            positionProfile.Keys = dataReader["KEYS"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["DATA_ACCESS_RIGHTS"]))
                            positionProfile.DataAccessRights = dataReader["DATA_ACCESS_RIGHTS"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["EDITABLE"]))
                            positionProfile.IsEditable =Convert.ToChar(dataReader["EDITABLE"]);

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS"]))
                            positionProfile.PositionProfileStatus = dataReader["POSITION_PROFILE_STATUS"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS_NAME"]))
                            positionProfile.PositionProfileStatusName = dataReader["POSITION_PROFILE_STATUS_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["HAS_OWNER_RIGHTS"]))
                            positionProfile.HasOwnerRights = dataReader["HAS_OWNER_RIGHTS"].ToString().Trim().ToUpper() == "Y" ? true : false;
                         
                        positionProfileDetails.Add(positionProfile);
                    }
                    dataReader.Close();
                }
                return positionProfileDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// Inserts the position profile.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="positionProfile">The position profile.</param>
        /// <param name="userID">The user id.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int InsertPositionProfile(IDbTransaction transaction, PositionProfileDetail positionProfile, int userID, int tenantID)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand insertPositionProfileCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_POSITION_PROFILE");

                // Add input parameters.

                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                 "@TENANT_ID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                  "@POSITION_PROFILE_NAME", DbType.String, positionProfile.PositionName);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                  "@POSITION_PROFILE_KEY", DbType.String, positionProfile.PositionProfileKey);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                  "@POSITION_ADDITIONAL_INFORMATION", DbType.String, positionProfile.PositionProfileAdditionalInformation);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@REGISTERED_BY", DbType.Int32, positionProfile.RegisteredBy);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@REGISTRATION_DATE", DbType.DateTime, positionProfile.RegistrationDate);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@SUBMITTAL_DEADLINE", DbType.DateTime, positionProfile.SubmittalDate);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@POSITION_PROFILE_STATUS", DbType.String, positionProfile.PositionProfileStatus);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@CREATED_BY", DbType.Int32, userID);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@MODIFIED_BY", DbType.Int32, userID);
                HCMDatabase.AddOutParameter(insertPositionProfileCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, int.MaxValue);

                object returnValue = HCMDatabase.ExecuteNonQuery(insertPositionProfileCommand, transaction as DbTransaction);
                returnValue = HCMDatabase.GetParameterValue(insertPositionProfileCommand, "@POSITION_PROFILE_ID");

                if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                    return 0;

                return int.Parse(returnValue.ToString());
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        /// <summary>
        /// Inserts the position profile segment data.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="segmentDetail">The segment detail.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="positionProfileId">The position profile id.</param>
        public void InsertPositionProfileSegmentData(IDbTransaction transaction, SegmentDetail segmentDetail, int userId, int positionProfileId)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand insertPositionProfileCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_POSITION_PROFILE_SEGMENTS");
                // Add input parameters.
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                  "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);

                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@SEGMENT_ID", DbType.Int32, segmentDetail.SegmentID);

                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@DISPLAY_ORDER", DbType.Int32, segmentDetail.DisplayOrder);
                // String strXml = segmentDetail.DataSource.InnerXml.ToString();
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@SEGMENT_DATA", DbType.Xml, segmentDetail.DataSource);

                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@CREATED_BY", DbType.Int32, userId);
                HCMDatabase.AddInParameter(insertPositionProfileCommand,
                    "@MODIFIED_BY", DbType.Int32, userId);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(insertPositionProfileCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Inserts the position profile segment data.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="segmentDetail">The segment detail.</param>
        /// <param name="positionProfileID">The position profile id.</param>
        /// <param name="userID">The user id.</param>
        public void InsertPositionProfileSegment(SegmentDetail segmentDetail, 
            int positionProfileID, int userID)
        {
            
            // Create a stored procedure command object.
            DbCommand insertPositionProfileCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_POSITION_PROFILE_SEGMENTS");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertPositionProfileCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            HCMDatabase.AddInParameter(insertPositionProfileCommand,
                "@SEGMENT_ID", DbType.Int32, segmentDetail.SegmentID);

            HCMDatabase.AddInParameter(insertPositionProfileCommand,
                "@DISPLAY_ORDER", DbType.Int32, segmentDetail.DisplayOrder);

            // String strXml = segmentDetail.DataSource.InnerXml.ToString();
            HCMDatabase.AddInParameter(insertPositionProfileCommand,
                "@SEGMENT_DATA", DbType.Xml, segmentDetail.DataSource);

            HCMDatabase.AddInParameter(insertPositionProfileCommand,
                "@CREATED_BY", DbType.Int32, userID);

            HCMDatabase.AddInParameter(insertPositionProfileCommand,
                "@MODIFIED_BY", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertPositionProfileCommand);
            
        }

        /// <summary>
        /// Deletes segment by segment id and position profile id.
        /// </summary>
        /// <param name="positionProfileID">The position profile id.</param>
        /// <param name="segmentID">The segment id.</param>
        public void DeletePositionProfileSegment(int positionProfileID, 
            int segmentID)
        {
            // Create a stored procedure command object.
            DbCommand deleteSegmentsCommand =
                HCMDatabase.GetStoredProcCommand("SPDELETE_POSITION_PROFILE_SEGMENT_BY_SEGMENT_ID");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteSegmentsCommand, 
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            HCMDatabase.AddInParameter(deleteSegmentsCommand, "@SEGMENT_ID", 
                DbType.Int32, segmentID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteSegmentsCommand);
        }

        /// <summary>
        /// Update position profile segment display order.
        /// </summary>
        /// <param name="positionProfileID">The position profile id.</param>
        /// <param name="segmentID">The segment id.</param>
        /// <param name="displayOrder">The display order.</param>
        public void UpdatePositionProfileSegmentDisplayOrder(int positionProfileID,
            int segmentID, int displayOrder)
        {
            // Create a stored procedure command object.
            DbCommand deleteSegmentsCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_POSITION_PROFILE_SEGMENT_DISPLAY_ORDER");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteSegmentsCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            HCMDatabase.AddInParameter(deleteSegmentsCommand, "@SEGMENT_ID",
                DbType.Int32, segmentID);

            HCMDatabase.AddInParameter(deleteSegmentsCommand, "@DISPLAY_ORDER",
                DbType.Int32, displayOrder);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteSegmentsCommand);
        }


        /// <summary>
        /// Gets the position profile detail.
        /// </summary>
        /// <param name="positionProfileID">The position profile ID.</param>
        /// <returns></returns>
        public PositionProfileDetail GetPositionProfile(int positionProfileID)
        {
            IDataReader dataReader = null;
            PositionProfileDetail positionProfileDetail = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand positionProfileDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_BY_ID");
                HCMDatabase.AddInParameter(positionProfileDetailCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
                dataReader = HCMDatabase.ExecuteReader(positionProfileDetailCommand);

                if (dataReader.Read())
                {
                    // Construct the admin settings object.
                    positionProfileDetail = new PositionProfileDetail();
                    positionProfileDetail.RegisteredByName = dataReader["REGISTER_NAME"].ToString();
                    positionProfileDetail.PositionName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    positionProfileDetail.PositionProfileKey = dataReader["POSITION_PROFILE_KEY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_ADDITIONAL_INFORMATION"]))
                        positionProfileDetail.PositionProfileAdditionalInformation = 
                            dataReader["POSITION_ADDITIONAL_INFORMATION"].ToString();
                    positionProfileDetail.RegistrationDate = Convert.ToDateTime(dataReader["REGISTRATION_DATE"]);
                    positionProfileDetail.SubmittalDate = Convert.ToDateTime(dataReader["SUBMITTAL_DEADLINE"]);
                    positionProfileDetail.PositionID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);
                    positionProfileDetail.RegisteredBy = Convert.ToInt32(dataReader["REGISTER_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                        positionProfileDetail.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        positionProfileDetail.ClientName = dataReader["CLIENT_NAME"].ToString();
                }
                dataReader.NextResult();
                List<SegmentDetail> segmentDetailList = null;
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(segmentDetailList))
                        segmentDetailList = new List<SegmentDetail>();
                    SegmentDetail segmentDetail = new SegmentDetail();
                    segmentDetail.DisplayOrder = Convert.ToInt32(dataReader["DISPLAY_ORDER"]);
                    string str = dataReader["SEGMENT_DATA"].ToString();
                    /*XmlDocument xml = new XmlDocument();
                    xml.InnerXml = str;*/
                    segmentDetail.DataSource = dataReader["SEGMENT_DATA"].ToString();
                    segmentDetail.SegmentID = Convert.ToInt32(dataReader["SEGMENT_ID"]);
                    segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString().Trim();
                    segmentDetail.segmentPath = dataReader["SEGMENT_PATH"].ToString();
                    segmentDetailList.Add(segmentDetail);
                }
                if (!Utility.IsNullOrEmpty(segmentDetailList))
                    positionProfileDetail.Segments = segmentDetailList;

                dataReader.NextResult();
                PositionProfileKeyword positionProfileKeywords = null;
                if (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(positionProfileKeywords))
                        positionProfileKeywords = new PositionProfileKeyword();
                    positionProfileKeywords.SegmentKeyword = dataReader["SEGMENT_KEYWORD"].ToString();
                    positionProfileKeywords.UserKeyword = dataReader["USER_KEYWORD"].ToString();
                }

                if (!Utility.IsNullOrEmpty(positionProfileKeywords))
                    positionProfileDetail.PositionProfileKeywords = positionProfileKeywords;

                dataReader.NextResult();
                List<int> sellectedDepartment = null;
                List<string> sellectedContact = null;
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["DEPARTMENT_ID"]))
                    {
                        if (Utility.IsNullOrEmpty(sellectedDepartment))
                            sellectedDepartment = new List<int>();
                        sellectedDepartment.Add(int.Parse(dataReader["DEPARTMENT_ID"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CONTACT_IDS"]))
                    {
                        if (Utility.IsNullOrEmpty(sellectedContact))
                            sellectedContact = new List<string>();
                        sellectedContact.Add(dataReader["CONTACT_IDS"].ToString());
                    }
                }
                if(sellectedDepartment!=null)
                    positionProfileDetail.DepartmentIds = sellectedDepartment;

                if(sellectedContact!=null)
                    positionProfileDetail.ContactIds = sellectedContact;

                dataReader.Close();

                return positionProfileDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<SegmentDetail> GetPositionProfileSegementList(int positionProfileID)
        { 
            IDataReader dataReader = null; 

            try
            {
                // Create a stored procedure command object.
                DbCommand positionProfileSegmentCmd = HCMDatabase.GetStoredProcCommand("SPGET_POSITIONPROFILE_SEGMENT_LIST");
                HCMDatabase.AddInParameter(positionProfileSegmentCmd, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
                dataReader = HCMDatabase.ExecuteReader(positionProfileSegmentCmd);
                List<SegmentDetail> segmentDetailList = null;
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(segmentDetailList))
                        segmentDetailList = new List<SegmentDetail>();
                    SegmentDetail segmentDetail = new SegmentDetail();
                    segmentDetail.DisplayOrder = Convert.ToInt32(dataReader["DISPLAY_ORDER"]);  
                    segmentDetail.SegmentID = Convert.ToInt32(dataReader["SEGMENT_ID"]);
                    segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString().Trim(); 
                    segmentDetailList.Add(segmentDetail);
                }

                return segmentDetailList;

            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Updates the position profile.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="positionProfile">The position profile.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int UpdatePositionProfile(IDbTransaction transaction, PositionProfileDetail positionProfile, int userID, int tenantID)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand updatePositionProfileCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_POSITION_PROFILE");

                // Add input parameters.

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                  "@POSITION_PROFILE_NAME", DbType.String, positionProfile.PositionName);

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                  "@POSITION_ADDITIONAL_INFORMATION", DbType.String, 
                    positionProfile.PositionProfileAdditionalInformation);

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@REGISTERED_BY", DbType.Int32, positionProfile.RegisteredBy);

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@REGISTRATION_DATE", DbType.DateTime, positionProfile.RegistrationDate);

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@SUBMITTAL_DEADLINE", DbType.DateTime, positionProfile.SubmittalDate);

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@MODIFIED_BY", DbType.Int32, userID);
                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfile.PositionID);
                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                return (int)HCMDatabase.ExecuteScalar(updatePositionProfileCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        /// <summary>
        /// Deletes the segment for position profile.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="positionProfileId">The position profile id.</param>
        public void DeleteSegmentForPositionProfile(IDbTransaction transaction, int positionProfileId)
        {
            try
            {
                DbCommand deleteFormSegmentsCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_PSOTION_PROFILE_SEGMENTS");

                HCMDatabase.AddInParameter(deleteFormSegmentsCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);

                HCMDatabase.ExecuteNonQuery(deleteFormSegmentsCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        /// <summary>
        /// Updates the open/close status in position profile.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="positionProfileId">The position profile id.</param>
        public void UpdateClosedStatusInPositionProfile(IDbTransaction transaction, int positionProfileId, string isClosed, int userID)
        {
            try
            {
                DbCommand updateCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_POSITION_PROFILE_CLOSEDSTATUS");

                HCMDatabase.AddInParameter(updateCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);
                HCMDatabase.AddInParameter(updateCommand, "@CLOSED", DbType.String, isClosed);
                HCMDatabase.AddInParameter(updateCommand, "@USER_ID", DbType.Int32, userID);

                HCMDatabase.ExecuteNonQuery(updateCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        /// <summary>
        /// Updates the position profile user options.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdatePositionProfileUserOptions(int formID, int userID)
        {

            // Create a stored procedure command object.
            DbCommand updatePositionProfileUserOptions = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_POSITION_PROFILE_SETTINGS");

            // Add input parameters.

            HCMDatabase.AddInParameter(updatePositionProfileUserOptions,
              "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddInParameter(updatePositionProfileUserOptions,
           "@DEFAULT_FORM", DbType.Int32, formID);

            HCMDatabase.ExecuteNonQuery(updatePositionProfileUserOptions);

        }
        /// <summary>
        /// Gets the predefined form.
        /// </summary>
        /// <returns></returns>
        public List<FormDetail> GetPredefinedForm()
        {
            IDataReader dataReader = null;
            try
            {
                List<FormDetail> formDetailList = new List<FormDetail>();

                DbCommand getPredefinedForm = HCMDatabase.GetStoredProcCommand("SPGET_PREDEFINED_FORMS");

                dataReader = HCMDatabase.ExecuteReader(getPredefinedForm);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        FormDetail formDetail = new FormDetail();
                        formDetail.FormID = int.Parse(dataReader["FORM_ID"].ToString());
                        formDetail.FormName = dataReader["FORM_NAME"].ToString().Trim();
                        formDetail.Tags = dataReader["TAGS"].ToString();
                        formDetailList.Add(formDetail);
                    }
                    dataReader.Close();
                }
                return formDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Deletes the position profile.
        /// </summary>
        /// <param name="positionProfileID">The position profile ID.</param>
        /// <param name="transaction">The transaction.</param>
        public void DeletePositionProfile(int positionProfileID, IDbTransaction transaction)
        {
            //Delete the emtry from the position profile keyword table
            DbCommand deletePPKeywordCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_POSITION_PROFILE_KEYWORD");

            HCMDatabase.AddInParameter(deletePPKeywordCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            HCMDatabase.ExecuteNonQuery(deletePPKeywordCommand, transaction as DbTransaction);

            //Delete the entry from the position profile table
            DbCommand deleteFormSegmentsCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_POSITION_PROFILE");

            HCMDatabase.AddInParameter(deleteFormSegmentsCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            HCMDatabase.ExecuteNonQuery(deleteFormSegmentsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that deletes a position profile.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile.
        /// </param>
        /// <remarks>
        /// This will do a soft deletion.
        /// </remarks>
        public void DeletePositionProfile(int positionProfileID)
        {
            // Create command object.
            DbCommand deletePositionProfileCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_POSITION_PROFILE");

            // Add parameters.
            HCMDatabase.AddInParameter(deletePositionProfileCommand, 
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            // Execute statement.
            HCMDatabase.ExecuteNonQuery(deletePositionProfileCommand);
        }

        /// <summary>
        /// Inserts the position profile keywords.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="positionProfileKeyword">The position profile keyword.</param>
        /// <param name="userID">The user ID.</param>
        public void InsertPositionProfileKeywords(IDbTransaction transaction, PositionProfileKeyword positionProfileKeyword, int userID)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand insertPositionProfileKeywordCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_POSITION_PROFILE_KEYWORDS");

                // Add input parameters.

                HCMDatabase.AddInParameter(insertPositionProfileKeywordCommand,
                  "@POSITION_PROFILE_ID", DbType.Int32, positionProfileKeyword.PositionProfileID);

                HCMDatabase.AddInParameter(insertPositionProfileKeywordCommand,
                    "@SEGMENT_KEYWORD", DbType.String, positionProfileKeyword.SegmentKeyword);

                HCMDatabase.AddInParameter(insertPositionProfileKeywordCommand,
                    "@USER_KEYWORD", DbType.String, positionProfileKeyword.UserKeyword);
                HCMDatabase.AddInParameter(insertPositionProfileKeywordCommand,
                    "@KEYWORD", DbType.String, positionProfileKeyword.Keyword);

                HCMDatabase.AddInParameter(insertPositionProfileKeywordCommand,
                    "@CREATED_BY", DbType.Int32, userID);

                HCMDatabase.ExecuteNonQuery(insertPositionProfileKeywordCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        /// <summary>
        /// Updates the position profile keywords.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="positionProfileKeyword">The position profile keyword.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdatePositionProfileKeywords(IDbTransaction transaction, PositionProfileKeyword positionProfileKeyword, int userID)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand updatePositionProfileKeywordCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_POSITION_PROFILE_KEYWORDS");

                // Add input parameters.

                HCMDatabase.AddInParameter(updatePositionProfileKeywordCommand,
                  "@POSITION_PROFILE_ID", DbType.Int32, positionProfileKeyword.PositionProfileID);

                HCMDatabase.AddInParameter(updatePositionProfileKeywordCommand,
                    "@SEGMENT_KEYWORD", DbType.String, positionProfileKeyword.SegmentKeyword);

                HCMDatabase.AddInParameter(updatePositionProfileKeywordCommand,
                    "@USER_KEYWORD", DbType.String, positionProfileKeyword.UserKeyword);

                HCMDatabase.AddInParameter(updatePositionProfileKeywordCommand,
                    "@KEYWORD", DbType.String, positionProfileKeyword.Keyword);

                HCMDatabase.AddInParameter(updatePositionProfileKeywordCommand,
                    "@MODIFIED_BY", DbType.Int32, userID);

                HCMDatabase.ExecuteNonQuery(updatePositionProfileKeywordCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }
        /// <summary>
        /// Gets the position profile keyword dictionary.
        /// </summary>
        /// <param name="prefixKeyword">The prefix keyword.</param>
        /// <param name="skillType">Type of the skill.</param>
        /// <param name="skillCategory">The skill category.</param>
        /// <returns></returns>
        public string[] GetPositionProfileKeywordDictionary(string prefixKeyword, SkillType skillType, string skillCategory)
        {
            IDataReader dataReader = null;
            try
            {
                List<string> positionProfileKeyword = null;
                DbCommand getpositionProfileKeyword = HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_KEYWORD_DICTIONARY");
                HCMDatabase.AddInParameter(getpositionProfileKeyword, "@PREFIX_TEXT", DbType.String, prefixKeyword);
                HCMDatabase.AddInParameter(getpositionProfileKeyword, "@SKILL_TYPE", DbType.String, skillType);
                HCMDatabase.AddInParameter(getpositionProfileKeyword, "@SKILL_CATEGORY", DbType.String, skillCategory);
                dataReader = HCMDatabase.ExecuteReader(getpositionProfileKeyword);
                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        if (positionProfileKeyword == null)
                            positionProfileKeyword = new List<string>();
                        positionProfileKeyword.Add(dataReader["KEYWORD"].ToString());
                    }
                    dataReader.Close();
                }
                if (!Support.Utility.IsNullOrEmpty(positionProfileKeyword) && positionProfileKeyword.Count > 0)
                    return positionProfileKeyword.ToArray();
                else
                    return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the position profile skill categroy.
        /// </summary>
        /// <param name="prefixKeyword">The prefix keyword.</param>
        /// <returns></returns>
        public string[] GetPositionProfileSkillCategroy(string prefixKeyword)
        {
            IDataReader dataReader = null;
            try
            {
                List<string> positionProfileSkillCategory = null;
                DbCommand getpositionProfileSkillCategory = HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_SKILL_CATEGORY_KEYWORDS");
                HCMDatabase.AddInParameter(getpositionProfileSkillCategory, "@PREFIX_TEXT", DbType.String, prefixKeyword);

                dataReader = HCMDatabase.ExecuteReader(getpositionProfileSkillCategory);
                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        if (positionProfileSkillCategory == null)
                            positionProfileSkillCategory = new List<string>();
                        positionProfileSkillCategory.Add(dataReader["SKILL_CATEGORY"].ToString());
                    }
                    dataReader.Close();
                }
                if (!Support.Utility.IsNullOrEmpty(positionProfileSkillCategory) && positionProfileSkillCategory.Count > 0)
                    return positionProfileSkillCategory.ToArray();
                else
                    return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Inserts the position profile keyword dictionary.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="dictionaryKeyword">The dictionary keyword.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public void InsertPositionProfileKeywordDictionary(IDbTransaction transaction, PositionProfileKeywordDictionary dictionaryKeyword, int userID)
        {
            try
            {
                SkillType skilltype = SkillType.Unknown;
                if (dictionaryKeyword.skillTypes == SkillType.Technical || dictionaryKeyword.skillTypes == SkillType.Vertical)
                    skilltype = dictionaryKeyword.skillTypes;
                // Create a stored procedure command object.
                DbCommand insertPositionProfileKeywordDictionaryCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_POSITION_PROFILE_KEYWORD_DICTIONARY");
                // Add input parameters.
                HCMDatabase.AddInParameter(insertPositionProfileKeywordDictionaryCommand,
                  "@KEYWORD", DbType.String, dictionaryKeyword.Keyword);
                HCMDatabase.AddInParameter(insertPositionProfileKeywordDictionaryCommand,
                  "@SKILL_CATEGORY", DbType.String, dictionaryKeyword.SkillCategory);
                HCMDatabase.AddInParameter(insertPositionProfileKeywordDictionaryCommand,
                  "@SKILL_TYPE", DbType.String, skilltype);
                HCMDatabase.AddInParameter(insertPositionProfileKeywordDictionaryCommand,
                    "@USER_ID", DbType.Int32, userID);
                HCMDatabase.ExecuteNonQuery(insertPositionProfileKeywordDictionaryCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// A Method that gets the keyword based on the position profile id
        /// </summary>
        /// <param name="PositionProfileId">
        /// A <see cref="System.Int32"/> that holds the position profile id
        /// </param>
        /// <returns>
        /// A <see cref="Forte.HCM.DataObjects.PositionProfileDetail"/> that holds the position profile details
        /// based on the position profile id
        /// </returns>
        public PositionProfileDetail GetPositionProfileKeyWord(int PositionProfileId)
        {
            DbCommand GetPositionProfileKeywordDbCommand = null;
            IDataReader dataReader = null;
            PositionProfileDetail PositionProfileDetail = null;
            try
            {

                GetPositionProfileKeywordDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_KEYWORD");
                HCMDatabase.AddInParameter(GetPositionProfileKeywordDbCommand, "@POSITION_PROFILE_ID", DbType.Int32, PositionProfileId);
                dataReader = HCMDatabase.ExecuteReader(GetPositionProfileKeywordDbCommand);
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(PositionProfileDetail))
                        PositionProfileDetail = new PositionProfileDetail();
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        PositionProfileDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        PositionProfileDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_KEY"]))
                        PositionProfileDetail.PositionProfileKey = dataReader["POSITION_PROFILE_KEY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["KEYWORD"]))
                        PositionProfileDetail.Keys = dataReader["KEYWORD"].ToString();
                }
                return PositionProfileDetail;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(PositionProfileDetail)) PositionProfileDetail = null;
                if (!Utility.IsNullOrEmpty(GetPositionProfileKeywordDbCommand)) GetPositionProfileKeywordDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Gets the position profile segment.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <returns></returns>
        public List<Segment> GetPositionProfileSegmentsForForm(int formID)
        {
            IDataReader dataReader = null;
            List<Segment> segmentDetailList = new List<Segment>();
            try
            {
                // Create command object
                DbCommand getTestQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_ENTRY_FORMSEGMENTS");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@FORM_ID", DbType.Int32, formID);
                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getTestQuestionDetailCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        Segment segmentDetail = new Segment();
                        segmentDetail.DisplayOrder = int.Parse(dataReader["DISPLAY_ORDER"].ToString()); ;
                        segmentDetail.SegmentID = int.Parse(dataReader["SEGMENT_ID"].ToString());
                        segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString().Trim();
                        segmentDetail.SegmentPath = dataReader["SEGMENT_PATH"].ToString().Trim();
                        segmentDetailList.Add(segmentDetail);
                    }
                    dataReader.Close();
                }
                return segmentDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        public void InsertPositionProfileContactDetails(ClientContactInformation contactinfo, int positionProfileID, int userID, IDbTransaction transaction)
        {
            DbCommand insertPPCommand = HCMDatabase.
                         GetStoredProcCommand("SPINSERT_POSITION_PROFILE_CONTACTS");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertPPCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertPPCommand,
                "@CONTACT_ID", DbType.Int32, contactinfo.ContactID);

            HCMDatabase.AddInParameter(insertPPCommand,
             "@CLIENT_ID", DbType.Int32, contactinfo.ClientID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertPPCommand,
                "@DEPARTMENT_ID", DbType.Int32, contactinfo.DepartmentID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertPPCommand,
                "@CREATED_BY", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertPPCommand,transaction as DbTransaction);
        }

        //public List<PositionProfileDetail> GetPositionProfileContactDetails(int positionProfileID)
        //{


        //    IDataReader dataReader = null;
        //    try
        //    {
        //        List<PositionProfileDetail> positionProfileContactDetails = new List<PositionProfileDetail>();

        //        DbCommand getpositionProfileContactDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_CONTACTS");


        //        HCMDatabase.AddInParameter(getpositionProfileContactDbCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

        //        dataReader = HCMDatabase.ExecuteReader(getpositionProfileContactDbCommand);

        //        // Check if datareader contains not equal to null
        //        if (dataReader != null)
        //        {
        //            while (dataReader.Read())
        //            {
        //                PositionProfileDetail positionProfileContactDetail = new PositionProfileDetail();

        //                positionProfileContactDetail.DepartmentContactID = Convert.ToInt32(dataReader["DEPARTMENT_CONTACT_ID"]);

        //                if (dataReader["DEPARTMENT_ID"].ToString() != "")
        //                    positionProfileContactDetail.DepartmentID = Convert.ToInt32(dataReader["DEPARTMENT_ID"].ToString());
        //                else
        //                    positionProfileContactDetail.DepartmentID = 0;
        //                positionProfileContactDetail.ClientContactID = Convert.ToInt32(dataReader["CLIENT_CONTACT_ID"].ToString());

        //                //positionProfileContactDetail.Department_Client_Contact_ID = dataReader["CLIENT_DEPARTMENT_CONTACT_ID"].ToString();

        //                positionProfileContactDetail.ClientName = dataReader["CLIENT_NAME"].ToString();

        //                positionProfileContactDetails.Add(positionProfileContactDetail);

        //            }
        //            dataReader.Close();
        //        }
        //        return positionProfileContactDetails;
        //    }
        //    finally
        //    {
        //        if (dataReader != null && !dataReader.IsClosed)
        //        {
        //            dataReader.Close();
        //        }
        //    }
        //}

        public void DeletePPContactDetails(int positionProfileID, IDbTransaction transaction)
        {
            try
            {
                DbCommand deletePositionProfileCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_POSITIONPROFILE_CONTACTS");

                HCMDatabase.AddInParameter(deletePositionProfileCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.ExecuteNonQuery(deletePositionProfileCommand,transaction as DbTransaction);
            }
            catch(Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the position profile ID for the given 
        /// candidate session ID. The candidate session ID can be of a test or
        /// interview and it is determined by the type parameter.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that holds the candidate session Key. This
        /// can be either test or interview session ID. 
        /// </param>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. Value T indicates test
        /// and I indicates interview.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the position profile ID.
        /// </returns>
        public int GetPositionProfileID(string candidateSessionKey, string type)
        {
            IDataReader dataReader = null;

            int positionProfileID = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand positionProfileDetailCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_POSITION_PROFILE_ID");

                // Add paramters.
                HCMDatabase.AddInParameter(positionProfileDetailCommand, 
                    "@CAND_SESSION_KEY", DbType.String, candidateSessionKey);
                HCMDatabase.AddInParameter(positionProfileDetailCommand,
                    "@TYPE", DbType.String, type);

                dataReader = HCMDatabase.ExecuteReader(positionProfileDetailCommand);

                if (dataReader.Read())
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_REQUEST_NUMBER"]))
                        positionProfileID = int.Parse(dataReader["CLIENT_REQUEST_NUMBER"].ToString());
                
                return positionProfileID;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int GetInterviewPositionProfileID(string candidateSessionKey)
        {
            IDataReader dataReader = null;

            int positionPrfileID = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand positionProfileDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_POSITION_PROFILE_ID");
                HCMDatabase.AddInParameter(positionProfileDetailCommand, "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);
                dataReader = HCMDatabase.ExecuteReader(positionProfileDetailCommand);

                if (dataReader.Read())
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_REQUEST_NUMBER"]))
                        positionPrfileID = int.Parse(dataReader["CLIENT_REQUEST_NUMBER"].ToString());
                return positionPrfileID;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public PositionProfileDashboard GetPositionProfileStatus(int positionProfileID)
        {
            PositionProfileDashboard positionProfileStatus = null;
            IDataReader dataReader = null;
            try
            {
                // Create command object
                DbCommand getPositionProfileCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_STATUS");

                HCMDatabase.AddInParameter(getPositionProfileCommand, "@POSITION_PROFILE_ID", DbType.Int32,
                    positionProfileID);
                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getPositionProfileCommand);

                // Check if datareader contains not equal to null
                if (dataReader.Read())
                {
                    positionProfileStatus = new PositionProfileDashboard();
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        positionProfileStatus.ClientName = dataReader["CLIENT_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["REGISTRATION_DATE"]))
                        positionProfileStatus.DateOfRegistration = Convert.ToDateTime(dataReader["REGISTRATION_DATE"].ToString());
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        positionProfileStatus.PositionName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        positionProfileStatus.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["REGISTED_BY"]))
                        positionProfileStatus.RegisteredBy = dataReader["REGISTED_BY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBMITTAL_DEADLINE"]))
                        positionProfileStatus.SubmittalDeadline = Convert.ToDateTime(dataReader["SUBMITTAL_DEADLINE"].ToString());
                    if (!Utility.IsNullOrEmpty(dataReader["KEYWORD"]))
                        positionProfileStatus.Keywords = dataReader["KEYWORD"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS"]))
                        positionProfileStatus.PositionStatus = dataReader["POSITION_PROFILE_STATUS"].ToString().Trim();
 
                    dataReader.Close();
                }
                return positionProfileStatus;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<CandidateInformation> GetPositionProfileCandidates(int positionProfileID,
         string orderBy, string candidateIds, int pageNumber, int pageSize, string filterBy, int recruiterId, string candidateStatus, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                //string[] filterByArray = filterBy.Split(',');
                //foreach (string item in filterByArray)
                //{
                //    if (item == "A")
                //    {
                //        filterBy = null;
                //    }
                //    if (filterBy != null)
                //    {

                //    }
                //}

                if (filterBy.Contains("A"))
                {
                    filterBy = null;
                }
                else if (filterBy.Contains("T,I,N"))
                {
                    filterBy = null;
                }
                else if (filterBy.Contains("T,N"))
                {
                    filterBy = null;
                }
                else if (filterBy.Contains("I,N"))
                {
                    filterBy = "N";
                }
                else if (filterBy == "")
                {
                    filterBy = null;
                }

                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_CANDIDATES_BY_ID");

                // Add parameters.

                HCMDatabase.AddInParameter(getUsersCommand,
                  "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@CANDIDIATE_IDS",
                    DbType.String, candidateIds);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@FILTER_BY", DbType.String, filterBy);

                if (!Utility.IsNullOrEmpty(candidateStatus))
                {
                    HCMDatabase.AddInParameter(getUsersCommand,
                   "@CANDIDATE_STATUS", DbType.String, candidateStatus);

                }


                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<CandidateInformation> candidateInformations = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (candidateInformations == null)
                            candidateInformations = new List<CandidateInformation>();

                        CandidateInformation candidateInformation = new CandidateInformation();

                        if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                            candidateInformation.PositionProfileStatusID = Convert.ToInt32(dataReader["ID"].ToString().Trim());
                        if (!Utility.IsNullOrEmpty(dataReader["CAIID"]))
                            candidateInformation.caiID = Convert.ToInt32(dataReader["CAIID"].ToString().Trim());
                        if (!Utility.IsNullOrEmpty(dataReader["CAIFIRSTNAME"]))
                            candidateInformation.caiFirstName = dataReader["CAIFIRSTNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["CAILASTNAME"]))
                            candidateInformation.caiFirstName = candidateInformation.caiFirstName + ' ' + dataReader["CAILASTNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["CAIADDRESS"]))
                            candidateInformation.caiAddress = dataReader["CAIADDRESS"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["CAIZIP"]))
                            candidateInformation.caiZip = dataReader["CAIZIP"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["CAIEMAIL"]))
                            candidateInformation.caiEmail = dataReader["CAIEMAIL"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["CAICELL"]))
                            candidateInformation.caiCell = dataReader["CAICELL"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SCORE"]))
                        {
                            candidateInformation.IsTestScore = true;
                            candidateInformation.TestScore = Convert.ToDecimal(dataReader["TEST_SCORE"].ToString().Trim()) * 100;
                        }
                        else
                        {
                            candidateInformation.IsTestScore = false;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["RESUME_SCORE"]))
                            candidateInformation.ResumeScore = Convert.ToDecimal(dataReader["RESUME_SCORE"].ToString().Trim()) * 100;
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SCORE"]))
                            candidateInformation.InterviewScore = Convert.ToDecimal(dataReader["INTERVIEW_SCORE"].ToString().Trim());
                        if (!Utility.IsNullOrEmpty(dataReader["RECRUITER"]))
                            candidateInformation.RecruiterName = dataReader["RECRUITER"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candidateInformation.ScheduledDate = Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString().Trim());

                        string testScheduler = string.Empty;
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SCHEDULER"]))
                        {
                            testScheduler = dataReader["TEST_SCHEDULER"].ToString();
                        }

                        string interviewScheduler = string.Empty;
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SCHEDULER"]))
                        {
                            interviewScheduler = dataReader["INTERVIEW_SCHEDULER"].ToString();
                        }

                        if (testScheduler != string.Empty && interviewScheduler != string.Empty)
                        {
                            if (testScheduler != interviewScheduler)
                                candidateInformation.SchedulerName = testScheduler + ", " + interviewScheduler;
                            else
                                candidateInformation.SchedulerName = testScheduler;
                        }
                        else if (testScheduler != string.Empty)
                        {
                            candidateInformation.SchedulerName = testScheduler;
                        }
                        else if (interviewScheduler != string.Empty)
                        {
                            candidateInformation.SchedulerName = interviewScheduler;
                        }
                        else
                            candidateInformation.SchedulerName = string.Empty;

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_STATUS"]))
                            candidateInformation.Status = dataReader["SESSION_STATUS"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_STATUS"]))
                            candidateInformation.InterviewStatus = dataReader["INTERVIEW_SESSION_STATUS"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_SESSION_STATUS"]))
                            candidateInformation.OnlineInterviewStatus = dataReader["ONLINE_INTERVIEW_SESSION_STATUS"].ToString().Trim();
                        
                        if (!Utility.IsNullOrEmpty(dataReader["SOURCE_FROM"]))
                            candidateInformation.SourceFrom = dataReader["SOURCE_FROM"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["CAND_SESSION_KEY"]))
                            candidateInformation.candidateSessionID = dataReader["CAND_SESSION_KEY"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                            candidateInformation.TestKey = dataReader["TEST_KEY"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                            candidateInformation.AttemptID = Int16.Parse(dataReader["ATTEMPT_ID"].ToString().Trim());
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_ATTEMPT_ID"]))
                            candidateInformation.InterviewAttemptID = Int16.Parse(dataReader["INTERVIEW_ATTEMPT_ID"].ToString().Trim());
                        if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                            candidateInformation.InterviewCandidateSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_KEY"]))
                        {
                            candidateInformation.OnlineInterviewKey =
                                dataReader["ONLINE_INTERVIEW_KEY"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CHAT_ROOM_KEY"]))
                        {
                            candidateInformation.ChatRoomKey =
                                dataReader["CHAT_ROOM_KEY"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CAND_ONLINE_INTERVIEW_ID"]))
                        {
                            candidateInformation.CandidateOnlineInterviewID =
                                Convert.ToInt32(dataReader["CAND_ONLINE_INTERVIEW_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TOTAL_WEIGHTED_SCORE"]))
                            candidateInformation.InterviewTotalWeightedScore = dataReader["INTERVIEW_TOTAL_WEIGHTED_SCORE"].ToString().Trim() + "%";
                        else
                            candidateInformation.InterviewTotalWeightedScore = "N/R";
                         
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TOTAL_SCORE"]))
                            candidateInformation.InterviewTotalScore = dataReader["INTERVIEW_TOTAL_SCORE"].ToString().Trim() + "%";
                        else
                            candidateInformation.InterviewTotalScore = "N/R";

                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_TOTAL_SCORE"]))
                            candidateInformation.OnlineInterviewTotalScore = dataReader["ONLINE_INTERVIEW_TOTAL_SCORE"].ToString().Trim() + "%";
                        else
                            candidateInformation.OnlineInterviewTotalScore = "N/R";


                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_STATUS"]))
                        {
                            switch (dataReader["CANDIDATE_STATUS"].ToString().Trim())
                            {
                                case "PCS_ASS":
                                    candidateInformation.CandidateStatus = "Associated";  
                                    break;
                                case "PCS_SUB":
                                    candidateInformation.CandidateStatus = "Submitted";  
                                    break;
                                case "PCS_REJ":
                                    candidateInformation.CandidateStatus = "Rejected";  
                                    break;
                                case "PCS_HIR":
                                    candidateInformation.CandidateStatus = "Hired";  
                                    break;
                                case "PCS_DEL":
                                    candidateInformation.CandidateStatus = "Deleted";
                                    break;
                            }
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_STATUS_COMMENTS"]))
                        {
                            candidateInformation.CandidateStatusComments = Convert.ToString(dataReader["CANDIDATE_STATUS_COMMENTS"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_LOGIN_ID"]))
                            candidateInformation.CandidateLoginID = Convert.ToInt32(dataReader["CANDIDATE_LOGIN_ID"]);

                        // Add to the list.
                        candidateInformations.Add(candidateInformation);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return candidateInformations;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the position profile candidate skill score
        /// detail for the given position profile and candidate ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileCandidateSkillScoreDetail"/> that holds
        /// the position profile candidate skill score detail.
        /// </returns>
        public PositionProfileCandidateSkillScoreDetail
            GetPositionProfileCandidateSkillScore(int positionProfileID, int candidateID)
        {
            IDataReader dataReader = null;
            PositionProfileCandidateSkillScoreDetail positionProfileSkillScore = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand skillScoreCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_POSITION_PROFILE_CANDIDATE_SKILL_SCORE");
                HCMDatabase.AddInParameter(skillScoreCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileID);
                HCMDatabase.AddInParameter(skillScoreCommand, "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(skillScoreCommand);

                // Read position profile name and candidate name.
                if (dataReader.Read())
                {
                    // Instantiate skill score object.
                    positionProfileSkillScore = new PositionProfileCandidateSkillScoreDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        positionProfileSkillScore.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        positionProfileSkillScore.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    // Read position profile skills.
                    dataReader.NextResult();
                    while (dataReader.Read())
                    {
                        // Instantiate skill scores list object.
                        if (positionProfileSkillScore.SkillScores == null)
                            positionProfileSkillScore.SkillScores = new List<CandidateSkillScoreDetail>();

                        CandidateSkillScoreDetail skillScore = new CandidateSkillScoreDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL"]))
                            skillScore.Skill = dataReader["SKILL"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SCORE"]))
                            skillScore.TestScore = Convert.ToDouble(dataReader["TEST_SCORE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SCORE"]))
                            skillScore.InterviewScore = Convert.ToDouble(dataReader["INTERVIEW_SCORE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["RESUME_SCORE"]))
                            skillScore.ResumeScore = Convert.ToDouble(dataReader["RESUME_SCORE"].ToString().Trim());

                        // Add to the list.
                        positionProfileSkillScore.SkillScores.Add(skillScore);
                    }

                    // Read overall position profile skill details
                    dataReader.NextResult();
                    if (dataReader.Read())
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["OVERALL_TEST_SCORE"]))
                            positionProfileSkillScore.OverallTestScore = Convert.ToDouble(dataReader["OVERALL_TEST_SCORE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["OVERALL_INTERVIEW_SCORE"]))
                            positionProfileSkillScore.OverallInterviewScore = Convert.ToDouble(dataReader["OVERALL_INTERVIEW_SCORE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["OVERALL_RESUME_SCORE"]))
                            positionProfileSkillScore.OverallResumeScore = Convert.ToDouble(dataReader["OVERALL_RESUME_SCORE"].ToString().Trim());
                    }
                }

                return positionProfileSkillScore;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<PositionProfileActivityLog> GetPositionProfileActivities
            (PositionProfileActivityLog searchCriteria, int pageNumber, int pageSize,
            string sortField, SortType sortOrder, out int totalRecords)
        {
            IDataReader dataReader = null;
            List<PositionProfileActivityLog> positionProfileDetail = null;

            // Assign out parameter default value.
            totalRecords = 0;

            try
            {
                DbCommand getPositionProfileCommand = HCMDatabase
                    .GetStoredProcCommand("SPSEARCH_POSITIONPROFILE_ACTIVITY_LOG");

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                    "@POSITIONPROFILE_ID", DbType.Int32, searchCriteria.PositionProfileID);

                /* HCMDatabase.AddInParameter(getPositionProfileCommand,
                   "@POSITIONPROFILE_NAME", DbType.Int32, searchCriteria.PositionProfileName);*/

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                    "@ACTIVITY_TYPE", DbType.String,
                    (Utility.IsNullOrEmpty(searchCriteria.ActivityType) ?
                    null : searchCriteria.ActivityType.Trim()));

                if (searchCriteria.ActivityBy == 0)
                    HCMDatabase.AddInParameter(getPositionProfileCommand,
                        "@ACTIVITY_BY", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getPositionProfileCommand,
                        "@ACTIVITY_BY", DbType.Int32, searchCriteria.ActivityBy);

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                   "@ACTIVITY_DATE_FROM", DbType.DateTime, searchCriteria.ActivityDateFrom);

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                   "@ACTIVITY_DATE_TO", DbType.DateTime, searchCriteria.ActivityDateTo);

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                   "@COMMENTS", DbType.String, searchCriteria.Comments);

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                    "@PAGE_NUM", DbType.String, pageNumber);

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                   "@SORT_EXPRESSION", DbType.String, sortField);

                HCMDatabase.AddInParameter(getPositionProfileCommand,
                    "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending ? "A" : "D");

                dataReader = HCMDatabase.ExecuteReader(getPositionProfileCommand);

                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {

                        if (positionProfileDetail == null)
                            positionProfileDetail = new List<PositionProfileActivityLog>();

                        PositionProfileActivityLog activityObj = new PositionProfileActivityLog();

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_DATE"]))
                        {
                            activityObj.ActivityDate = Convert.ToDateTime
                                (dataReader["ACTIVITY_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_BY"]))
                        {
                            activityObj.ActivityBy = Convert.ToInt32
                                (dataReader["ACTIVITY_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_BY_NAME"]))
                        {
                            activityObj.ActivityByName = dataReader["ACTIVITY_BY_NAME"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_TYPE"]))
                        {
                            activityObj.ActivityType = dataReader["ACTIVITY_TYPE"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_TYPE_NAME"]))
                        {
                            activityObj.ActivityTypeName = dataReader["ACTIVITY_TYPE_NAME"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                        {
                            activityObj.Comments = dataReader["COMMENTS"].
                                ToString().Trim();
                        }

                        // Add to the list.
                        positionProfileDetail.Add(activityObj);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return positionProfileDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        public DataTable GetPositionProfileActivitiesTable
            (PositionProfileActivityLog searchCriteria, int pageNumber, int pageSize,
            string sortField, SortType sortOrder, out int totalRecords)
        {
            // Assign out parameter default value.
            totalRecords = 0;

            DbCommand getPositionProfileCommand = HCMDatabase
                .GetStoredProcCommand("SPSEARCH_POSITIONPROFILE_ACTIVITY_LOG");

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@POSITIONPROFILE_ID", DbType.Int32, searchCriteria.PositionProfileID);

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@ACTIVITY_TYPE", DbType.String,
                (Utility.IsNullOrEmpty(searchCriteria.ActivityType) ?
                null : searchCriteria.ActivityType.Trim()));

            if (searchCriteria.ActivityBy == 0)
                HCMDatabase.AddInParameter(getPositionProfileCommand,
                    "@ACTIVITY_BY", DbType.Int32, null);
            else
                HCMDatabase.AddInParameter(getPositionProfileCommand,
                    "@ACTIVITY_BY", DbType.Int32, searchCriteria.ActivityBy);

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@ACTIVITY_DATE_FROM", DbType.DateTime, searchCriteria.ActivityDateFrom);

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@ACTIVITY_DATE_TO", DbType.DateTime, searchCriteria.ActivityDateTo);

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@COMMENTS", DbType.String, searchCriteria.Comments);

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@PAGE_NUM", DbType.String, pageNumber);

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@PAGE_SIZE", DbType.Int32, pageSize);

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@SORT_EXPRESSION", DbType.String, sortField);

            HCMDatabase.AddInParameter(getPositionProfileCommand,
                "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending ? "A" : "D");

            DataSet dataSet = HCMDatabase.ExecuteDataSet(getPositionProfileCommand);

            if (dataSet == null || dataSet.Tables == null ||
                dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
            {
                return null;
            }
            return dataSet.Tables[0];
        }

        public PositionProfileDetail GetPositionProfileInformation(int positionProfileId)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITIONPROFILE_DETAILS");

                HCMDatabase.AddInParameter(getUsersCommand,
                  "@POSITIONPROFILE_ID", DbType.Int32, positionProfileId);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                PositionProfileDetail positionProfileDetail = new PositionProfileDetail();

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_KEY"]))
                    {
                        positionProfileDetail.PositionProfileKey = dataReader["POSITION_PROFILE_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        positionProfileDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();
                    }

                }
                return positionProfileDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate activity summary for the given
        /// position profile and candidate ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateActivitySummaryDetail"/> that holds
        /// the position profile candidate activity log detail.
        /// </returns>
        public CandidateActivitySummaryDetail
            GetCandidateActivitySummary(int positionProfileID, int candidateID)
        {
            IDataReader dataReader = null;
            CandidateActivitySummaryDetail summary = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand skillScoreCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_POSITION_PROFILE_CANDIDATE_ACTIVITY_SUMMARY");
                HCMDatabase.AddInParameter(skillScoreCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileID);
                HCMDatabase.AddInParameter(skillScoreCommand, "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(skillScoreCommand);

                // Read position profile name and candidate name.
                if (dataReader.Read())
                {
                    // Instantiate skill score object.
                    summary = new CandidateActivitySummaryDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        summary.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        summary.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    // Read activity logs.
                    dataReader.NextResult();
                    while (dataReader.Read())
                    {
                        if (summary.Activities == null)
                            summary.Activities = new List<CandidateActivityLog>();

                        CandidateActivityLog activity = new CandidateActivityLog();

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_DATE"]))
                        {
                            activity.ActivityDate = Convert.ToDateTime
                                (dataReader["ACTIVITY_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_BY"]))
                        {
                            activity.ActivityBy = Convert.ToInt32
                                (dataReader["ACTIVITY_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_BY_NAME"]))
                        {
                            activity.ActivityByName = dataReader["ACTIVITY_BY_NAME"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_TYPE"]))
                        {
                            activity.ActivityType = dataReader["ACTIVITY_TYPE"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_TYPE_NAME"]))
                        {
                            activity.ActivityTypeName = dataReader["ACTIVITY_TYPE_NAME"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                        {
                            activity.Comments = dataReader["COMMENTS"].
                                ToString().Trim();
                        }

                        // Add to the list.
                        summary.Activities.Add(activity);
                    }
                }

                return summary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate activity summary for the given
        /// position profile and candidate ID as a data table for export to 
        /// excel.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateActivitySummaryDetail"/> that holds
        /// the position profile candidate activity log detail.
        /// </returns>
        public DataTable GetCandidateActivitySummaryTable
            (int positionProfileID, int candidateID)
        {
            // Create a stored procedure command object.
            DbCommand skillScoreCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_POSITION_PROFILE_CANDIDATE_ACTIVITY_LOG");
            HCMDatabase.AddInParameter(skillScoreCommand, "@POSITION_PROFILE_ID",
                DbType.Int32, positionProfileID);
            HCMDatabase.AddInParameter(skillScoreCommand, "@CANDIDATE_ID",
                DbType.Int32, candidateID);

            DataSet dataSet = HCMDatabase.ExecuteDataSet(skillScoreCommand);

            if (dataSet == null || dataSet.Tables == null ||
                dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
            {
                return null;
            }
            return dataSet.Tables[0];
        }

        public CandidateInformation
          GetPositionProfileCandidateInformation(int positionProfileStatusID)
        {
            IDataReader dataReader = null;
            CandidateInformation candidateInformation = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand skillScoreCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_POSITION_PROFILE_CANDIDATES_CANDIDATE_ID");
                HCMDatabase.AddInParameter(skillScoreCommand, "@POSITION_PROFILE_STATUS_ID",
                    DbType.Int32, positionProfileStatusID);
                dataReader = HCMDatabase.ExecuteReader(skillScoreCommand);

                // Read position profile name and candidate name.
                if (dataReader.Read())
                {
                    // Instantiate skill score object.
                    candidateInformation = new CandidateInformation();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        candidateInformation.TestKey = dataReader["TEST_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        candidateInformation.AttemptID = Int16.Parse(dataReader["ATTEMPT_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        candidateInformation.candidateSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        candidateInformation.caiFirstName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        candidateInformation.caiID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_CANDIDATE_SESSION_ID"]))
                        candidateInformation.InterviewCandidateSessionID = dataReader["INTERVIEW_CANDIDATE_SESSION_ID"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_ATTEMPT_ID"]))
                        candidateInformation.InterviewAttemptID = Convert.ToInt16(dataReader["INTERVIEW_ATTEMPT_ID"]);
                    // Read position profile skills.
                }

                return candidateInformation;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public void
          GetPositionProfileCandidateTempIds(string sessionKey, out string canidateIds, out string keywords)
        {
            IDataReader dataReader = null;
            try
            {
                keywords = null;
                canidateIds = null;
                // Create a stored procedure command object.
                DbCommand skillScoreCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_POSITION_PROFILE_CANDIDATE_TEMP_IDS");
                HCMDatabase.AddInParameter(skillScoreCommand, "@SESSION_KEY",
                    DbType.String, sessionKey);
                dataReader = HCMDatabase.ExecuteReader(skillScoreCommand);

                // Read position profile name and candidate name.
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["KEYWORDS"]))
                        keywords = dataReader["KEYWORDS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_IDS"]))
                        canidateIds = dataReader["CANDIDATE_IDS"].ToString().Trim();
                }
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        //public void InsertPositionProfileCanidate( )
        //{
        //    try
        //    {
        //        // Create a stored procedure command object.
        //        DbCommand insertPositionProfileCommand = HCMDatabase.
        //            GetStoredProcCommand("SPINSERT_POSITION_PROFILE");

        //        // Add input parameters.

        //        HCMDatabase.AddInParameter(insertPositionProfileCommand,
        //         "@TENANT_ID", DbType.Int32, tenantID);
        //        HCMDatabase.AddInParameter(insertPositionProfileCommand,
        //          "@POSITION_PROFILE_NAME", DbType.String, positionProfile.PositionName);
        //        HCMDatabase.AddInParameter(insertPositionProfileCommand,
        //          "@POSITION_PROFILE_KEY", DbType.String, positionProfile.PositionProfileKey);

        //        HCMDatabase.AddInParameter(insertPositionProfileCommand,
        //            "@REGISTERED_BY", DbType.Int32, positionProfile.RegisteredBy);

        //        HCMDatabase.AddInParameter(insertPositionProfileCommand,
        //            "@REGISTRATION_DATE", DbType.DateTime, positionProfile.RegistrationDate);

        //        HCMDatabase.AddInParameter(insertPositionProfileCommand,
        //            "@SUBMITTAL_DEADLINE", DbType.DateTime, positionProfile.SubmittalDate);

        //        HCMDatabase.AddInParameter(insertPositionProfileCommand,
        //            "@CREATED_BY", DbType.Int32, userID);
        //        HCMDatabase.AddInParameter(insertPositionProfileCommand,
        //            "@MODIFIED_BY", DbType.Int32, userID);
        //        HCMDatabase.AddOutParameter(insertPositionProfileCommand,
        //            "@POSITION_PROFILE_ID", DbType.Int32, int.MaxValue);

        //        object returnValue = HCMDatabase.ExecuteNonQuery(insertPositionProfileCommand, transaction as DbTransaction);
        //        returnValue = HCMDatabase.GetParameterValue(insertPositionProfileCommand, "@POSITION_PROFILE_ID");

        //        if (returnValue == null || returnValue.ToString().Trim().Length == 0)
        //            return 0;

        //        return int.Parse(returnValue.ToString());
        //    }
        //    catch (Exception exp)
        //    {
        //        transaction.Rollback();
        //        throw exp;
        //    }
        //}

        public void InsertPositionProfileTSCandidate(string sessionKey, int positionProfileID)
        {
            DbCommand positionProfileCandidatesInsertCommand = HCMDatabase.GetStoredProcCommand
                ("SPINSERT_POSITION_PROFILE_TS_CANDIDATES");

            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@POSITION_PROFILE_ID",
          DbType.Int32, positionProfileID);
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@SESSION_KEY",
       DbType.String, sessionKey);

            HCMDatabase.ExecuteNonQuery(positionProfileCandidatesInsertCommand);
        }

        /// <summary>
        /// Method that retrieves the position profile keyword dictionary pattern. 
        /// The pattern is build for technical skills, verticals and roles.
        /// </summary>
        /// <returns>
        /// A <see cref="PositionProfileKeywordPattern"/> that holds the keyword 
        /// dictionary pattern.
        /// </returns>
        public PositionProfileKeywordPattern GetKeywordDictionaryPattern()
        {
            IDataReader dataReader = null;
            PositionProfileKeywordPattern keywordPattern = null;

            try
            {
                // Construct command object
                DbCommand getPatternCommand = HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_KEYWORD_DICTIONARY_PATTERN");

                // Execute the query.
                dataReader = HCMDatabase.ExecuteReader(getPatternCommand);

                if (dataReader.Read())
                {
                    // Instantiate the keyword dictionary pattern object.
                    keywordPattern = new PositionProfileKeywordPattern();

                    if (!Utility.IsNullOrEmpty(dataReader["TECHNICAL_SKILLS_PATTERN"]))
                        keywordPattern.TechnicalSkillsPattern = dataReader["TECHNICAL_SKILLS_PATTERN"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["VERTICALS_PATTERN"]))
                        keywordPattern.VerticalsPattern = dataReader["VERTICALS_PATTERN"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ROLES_PATTERN"]))
                        keywordPattern.RolesPattern = dataReader["ROLES_PATTERN"].ToString();
                }

                return keywordPattern;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the position profile keyword dictionary list.. 
        /// The list is build for technical skills, verticals and roles.
        /// </summary>
        /// <returns>
        /// A list of <see cref="KeywordSkill"/> that holds the keyword dictionary 
        /// list.
        /// </returns>
        public List<KeywordSkill> GetKeywordDictionaryList()
        {
            IDataReader dataReader = null;
            List<KeywordSkill> keywordSkills = null;

            try
            {
                // Construct command object
                DbCommand getPatternCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_POSITION_PROFILE_KEYWORD_DICTIONARY_LIST");

                // Execute the query.
                dataReader = HCMDatabase.ExecuteReader(getPatternCommand);

                while (dataReader.Read())
                {
                    // Instantiate the keyword skill list.
                    if (keywordSkills == null)
                        keywordSkills = new List<KeywordSkill>();

                    // Create and instantiate a keyword skill object.
                    KeywordSkill keywordSkill = new KeywordSkill();

                    if (!Utility.IsNullOrEmpty(dataReader["KEYWORD"]))
                        keywordSkill.SkillName = dataReader["KEYWORD"].ToString().Replace(@"\+", "+");

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_CATEGORY"]))
                        keywordSkill.SkillCategory = dataReader["SKILL_CATEGORY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_TYPE"]))
                        keywordSkill.SkillType = dataReader["SKILL_TYPE"].ToString();

                    // Add to the list.
                    keywordSkills.Add(keywordSkill);
                }

                return keywordSkills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the position profile summary for the given 
        /// position profile ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="string"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetPositionProfileSummary(int positionProfileID, int userID)
        {
            DbCommand getSummaryCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_POSITION_PROFILE_REVIEW");

            HCMDatabase.AddInParameter(getSummaryCommand, "@POSITION_PROFILE_ID", DbType.String, positionProfileID);

            if (userID == 0)
                HCMDatabase.AddInParameter(getSummaryCommand, "@USER_ID", DbType.String, null);
            else
                HCMDatabase.AddInParameter(getSummaryCommand, "@USER_ID", DbType.String, userID);

            // Execute the stored procedure.
            return HCMDatabase.ExecuteDataSet(getSummaryCommand);
        }

        /// <summary>
        /// Method that retrieves the position profile segments for the given 
        /// position profile ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <returns>
        /// A <see cref="SegmentDetail"/> that holds the segment data.
        /// </returns>
        public List<SegmentDetail> GetPositionProfileSegementListByPositionProfileID(int positionProfileID)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand positionProfileSegmentCmd =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_SEGMENT_LIST_BY_POSITION_PROFILE_ID");

                HCMDatabase.AddInParameter(positionProfileSegmentCmd, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                dataReader = HCMDatabase.ExecuteReader(positionProfileSegmentCmd);
                List<SegmentDetail> segmentDetailList = null;

                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(segmentDetailList))
                        segmentDetailList = new List<SegmentDetail>();

                    SegmentDetail segmentDetail = new SegmentDetail();
                    segmentDetail.DisplayOrder = Convert.ToInt32(dataReader["DISPLAY_ORDER"]);
                    segmentDetail.SegmentID = Convert.ToInt32(dataReader["SEGMENT_ID"]);
                    segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString().Trim();
                    segmentDetailList.Add(segmentDetail);
                }

                return segmentDetailList;

            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Inserts the position profile owners.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="createdBy">
        /// A <see cref="int"/> that holds the createdBy.
        /// </param>
        /// <param name="transaction">The transaction.</param>
        public void InsertPositionProfileOwners(int positionProfileID, string ownerType, int userID,
            int createdBy, IDbTransaction transaction)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand insertPositionProfileOwnersCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_POSITION_PROFILE_OWNERS");
                // Add input parameters.
                HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                  "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                    "@OWNER_TYPE", DbType.String, ownerType);

                HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                    "@USER_ID", DbType.Int32, userID);

                HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                    "@CREATED_BY", DbType.Int32, createdBy);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(insertPositionProfileOwnersCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Inserts the position profile owners.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="createdBy">
        /// A <see cref="int"/> that holds the createdBy.
        /// </param>
        public void InsertPositionProfileOwners(int positionProfileID, int userID, string ownerType, 
            int createdBy)
        {
           
                // Create a stored procedure command object.
                DbCommand insertPositionProfileOwnersCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_POSITION_PROFILE_OWNERS");
                // Add input parameters.
                HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                  "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                    "@OWNER_TYPE", DbType.String, ownerType);

                HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                    "@USER_ID", DbType.Int32, userID);

                HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                    "@CREATED_BY", DbType.Int32, createdBy);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(insertPositionProfileOwnersCommand);
        }

        /// <summary>
        /// Method to delete the position profile owners against position 
        /// profile id and owner type
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        public void DeletePositionProfileOwners(int positionProfileID, string ownerType,
            IDbTransaction transaction)
        {
            DbCommand deletePositionProfileOwnersCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_POSITION_PROFILE_OWNERS");
            HCMDatabase.AddInParameter(deletePositionProfileOwnersCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
            HCMDatabase.AddInParameter(deletePositionProfileOwnersCommand,
                "@OWNER_TYPE", DbType.String, ownerType);
            HCMDatabase.ExecuteNonQuery(deletePositionProfileOwnersCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method to get the position profile owner by position
        /// profile id and owner type
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail
        /// </returns>
        public List<UserDetail> GetPositionProfileOwnersByIDAndOwnerType(int positionProfileID,
            string ownerType)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand positionProfileOwnerCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_POSITION_PROFILE_OWNERS_BY_ID_AND_OWNER_TYPE");

                // Add input parameters.
                HCMDatabase.AddInParameter(positionProfileOwnerCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(positionProfileOwnerCommand,
                    "@OWNER_TYPE", DbType.String, ownerType);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(positionProfileOwnerCommand);

                List<UserDetail> userDetails = null;
                UserDetail userDetail = null;

                while (dataReader.Read())
                {
                    // Construct the user detail object.
                    if (userDetails == null)
                        userDetails = new List<UserDetail>();
                    userDetail = new UserDetail();


                    userDetail = new UserDetail();
                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                    {
                        userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        userDetail.FirstName = dataReader["FIRST_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                    {
                        userDetail.LastName = dataReader["LAST_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["OWNER_NAME"]))
                    {
                        userDetail.FullName = dataReader["OWNER_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                    {
                        userDetail.Email = dataReader["EMAIL"].ToString();
                    }

                    // Add the owner to collection
                    userDetails.Add(userDetail);
                }
                return userDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the recruiter detail.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="positionStatus">
        /// A <see cref="int"/> that holds the position profile status.
        /// </param>
        /// <param name="type">
        /// A <see cref="int"/> that holds the retrieve list type.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recent month.
        /// </param>
        /// <param name="spType">
        /// A <see cref="int"/> that holds the stored procedure type.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterDetail> GetRecruiter(int tenantID, int positionProfileID, 
            string positionStatus, string type, int recruiterID, int recentMonth, string spType,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<RecruiterDetail> recruiterDetails = null;

            try
            {
                string spName = string.Empty;
                if (spType == "RS")
                    spName = "SPGET_RECRUITER_DETAILS_BY_SKILL";
                else if (spType == "RC")
                    spName = "SPGET_RECRUITER_DETAILS_BY_CLIENT";
                else
                    spName = "SPGET_RECRUITER_DETAILS";

                DbCommand getRecruiterDetailsCommand =
                    HCMDatabase.GetStoredProcCommand(spName);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@POSITION_STATUS", DbType.String, positionStatus);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@TYPE", DbType.String, type);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@RECRUITER_ID", DbType.Int32, recruiterID);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@LAST_MONTH", DbType.Int32, recentMonth);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand, "@ORDER_BY",
                   DbType.String, sortField);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getRecruiterDetailsCommand);

                // Retreive recruiter details.
                while (dataReader.Read())
                {
                    // Instantiate recruiter details list.
                    if (recruiterDetails == null)
                        recruiterDetails = new List<RecruiterDetail>();

                    // Create recruiter detail object.
                    RecruiterDetail recruiterDetail = new RecruiterDetail();

                    // Instantiate recruiter detail object.
                    recruiterDetail = new RecruiterDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        recruiterDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_NAME"]))
                        recruiterDetail.RecruiterName = dataReader["RECRUITER_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE_POSITION_ASSIGNMENTS"]))
                        recruiterDetail.ActivePositionAssignments =
                            Convert.ToInt32(dataReader["ACTIVE_POSITION_ASSIGNMENTS"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECENT_POSITION_ASSIGNMENT"]))
                        recruiterDetail.RecentPositionAssignment =
                            Convert.ToDateTime(dataReader["RECENT_POSITION_ASSIGNMENT"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_FIRST_NAME"]))
                        recruiterDetail.RecruiterFirstName = dataReader["RECRUITER_FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_EMAIL"]))
                        recruiterDetail.RecruiterEmail = dataReader["RECRUITER_EMAIL"].ToString();

                    recruiterDetails.Add(recruiterDetail);
                }
                return recruiterDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the all corporate users.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterDetail> GetAllRecruiter(int tenantID, 
            int pageNumber, int pageSize, string sortField, SortType sordOrder, 
            out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<RecruiterDetail> recruiterDetails = null;

            try
            {
                DbCommand getRecruiterDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_RECRUITER_DETAILS");

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand, "@ORDER_BY",
                   DbType.String, sortField);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getRecruiterDetailsCommand);

                // Retreive recruiter details.
                while (dataReader.Read())
                {
                    // Instantiate recruiter details list.
                    if (recruiterDetails == null)
                        recruiterDetails = new List<RecruiterDetail>();

                    // Create recruiter detail object.
                    RecruiterDetail recruiterDetail = new RecruiterDetail();

                    // Instantiate recruiter detail object.
                    recruiterDetail = new RecruiterDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        recruiterDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_NAME"]))
                        recruiterDetail.RecruiterName = dataReader["RECRUITER_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_FIRST_NAME"]))
                        recruiterDetail.RecruiterFirstName = dataReader["RECRUITER_FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_EMAIL"]))
                        recruiterDetail.RecruiterEmail = dataReader["RECRUITER_EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE_POSITION_ASSIGNMENTS"]))
                        recruiterDetail.ActivePositionAssignments =
                            Convert.ToInt32(dataReader["ACTIVE_POSITION_ASSIGNMENTS"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECENT_POSITION_ASSIGNMENT"]))
                        recruiterDetail.RecentPositionAssignment =
                            Convert.ToDateTime(dataReader["RECENT_POSITION_ASSIGNMENT"].ToString());

                    recruiterDetails.Add(recruiterDetail);
                }
                return recruiterDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the recruiter detail.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="positionStatus">
        /// A <see cref="int"/> that holds the position profile status.
        /// </param>
        /// <param name="type">
        /// A <see cref="int"/> that holds the retrieve list type.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recent month.
        /// </param>
        /// <param name="spType">
        /// A <see cref="int"/> that holds the stored procedure type.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterAssignmentDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterAssignmentDetail> GetRecruiterAssignmentDetail
            (int tenantID, int positionProfileID,
                string positionStatus, string type, int recruiterID, int recentMonth, string spType,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;
            List<RecruiterAssignmentDetail> assignementDetails = null;

            try
            {
                string spName = string.Empty;
                if (spType == "RS")
                    spName = "SPGET_RECRUITER_DETAILS_BY_SKILL";
                else if (spType == "RC")
                    spName = "SPGET_RECRUITER_DETAILS_BY_CLIENT";

                DbCommand getAssessmentsCommand =
                    HCMDatabase.GetStoredProcCommand(spName);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@POSITION_STATUS", DbType.String, positionStatus);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@TYPE", DbType.String, type);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@RECRUITER_ID", DbType.Int32, recruiterID);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@LAST_MONTH", DbType.Int32, recentMonth);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getAssessmentsCommand, "@ORDER_BY",
                   DbType.String, sortField);

                HCMDatabase.AddInParameter(getAssessmentsCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getAssessmentsCommand);

                while (dataReader.Read())
                {
                    // Instantiate assignment details list.
                    if (assignementDetails == null)
                        assignementDetails = new List<RecruiterAssignmentDetail>();

                    // Create assignment detail object.
                    RecruiterAssignmentDetail assignmentDetail = new RecruiterAssignmentDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        assignmentDetail.PositionProfileID =
                            Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        assignmentDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        assignmentDetail.ClientName = dataReader["CLIENT_NAME"].ToString();

                    string client = string.Empty;
                    string clientID = string.Empty;

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID_NAME"]))
                        client = dataReader["CLIENT_ID_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(client))
                    {
                        Regex regex = new Regex(@"(^\d*)");
                        Match match = regex.Match(client);

                        if (match.Success)
                        {
                            clientID = match.Value;
                            assignmentDetail.ClientID = Convert.ToInt32(clientID);
                        }
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS"]))
                        assignmentDetail.PositionProfileStatus = dataReader["POSITION_PROFILE_STATUS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS_NAME"]))
                        assignmentDetail.PositionProfileStatusName = dataReader["POSITION_PROFILE_STATUS_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSIGNED_DATE"]))
                        assignmentDetail.AssignedDate = Convert.ToDateTime(dataReader["ASSIGNED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATES_ASSOCIATED_BY_RECRUITER"]))
                        assignmentDetail.CandidatesAssociatedByRecruiter = Convert.ToInt32(dataReader["CANDIDATES_ASSOCIATED_BY_RECRUITER"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATES_SUBMITTED_TO_CLIENT_BY_RECRUITER"]))
                        assignmentDetail.CandidatesSubmittedToClientByRecruiter = Convert.ToInt32(dataReader["CANDIDATES_SUBMITTED_TO_CLIENT_BY_RECRUITER"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER"]))
                        assignmentDetail.PositionProfileOwner = dataReader["POSITION_PROFILE_OWNER"].ToString();

                    assignementDetails.Add(assignmentDetail);
                }
                return assignementDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the candidate details.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the position Profile ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInformation"/> that holds the 
        /// candidate information details.
        /// </returns>
        public List<CandidateInformation> GetPositionProfileCandidatesByPositionProfileID(int positionProfileID)
        {
            IDataReader dataReader = null;

            List<CandidateInformation> candidateinformations = null;

            try
            {
                DbCommand getRecruiterDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_CANDIDATES_BY_POSITION_PROFILE_ID");

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                dataReader = HCMDatabase.ExecuteReader(getRecruiterDetailsCommand);

                // Retreive candidate information details.
                while (dataReader.Read())
                {
                    // Instantiate candidate details list.
                    if (candidateinformations == null)
                        candidateinformations = new List<CandidateInformation>();

                    // Create candidate information detail object.
                    CandidateInformation candidateInformation = new CandidateInformation();

                    // Instantiate candidate detail object.
                    candidateInformation = new CandidateInformation();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        candidateInformation.caiID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        candidateInformation.caiFirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MIDDLE_NAME"]))
                        candidateInformation.caiMiddleName = dataReader["MIDDLE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        candidateInformation.caiLastName = dataReader["LAST_NAME"].ToString();

                    candidateinformations.Add(candidateInformation);
                }
                return candidateinformations;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrives the position profile associate candidates count
        /// </summary>
        /// <param name="positionProfileId">
        /// A<see cref="int"/> that holds the position profile id
        /// </param>
        /// <returns></returns>
        public List<string> GetPositionProfileCandidateAssociationCount(int positionProfileId,out string owner)
        {
            IDataReader dataReader = null;
            int candidateCount = 0;
            List<string> CandidateAssociation = null;
            string candidateIds = null;
            owner = null;

            try
            {
                CandidateAssociation = new List<string>();

                DbCommand positionProfileCommand = HCMDatabase.GetStoredProcCommand("SPGET_PP_SOURCE_CANDIDATES");
                
                HCMDatabase.AddInParameter(positionProfileCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileId);

                HCMDatabase.AddInParameter(positionProfileCommand, "@PAGE_NUM",
                    DbType.Int32, 1);

                HCMDatabase.AddInParameter(positionProfileCommand, "@PAGE_SIZE",
                    DbType.Int32, 20); 

                dataReader = HCMDatabase.ExecuteReader(positionProfileCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        candidateCount = Convert.ToInt32(dataReader["TOTAL"]);
                        CandidateAssociation.Add(candidateCount.ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_ID"]))
                    {
                        candidateIds += dataReader["POSITION_ID"].ToString() + ",";
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                    {
                        if (owner == null)
                            owner = dataReader["CREATED_BY"].ToString();
                        else
                        {
                            if (!owner.Contains(dataReader["CREATED_BY"].ToString()))
                            {
                                owner += dataReader["CREATED_BY"].ToString() + ",";
                            } 
                        }
                    }
                }
                /*owner = GetTaskOwner(owner);*/
                if (candidateIds!=null)
                    CandidateAssociation.Add(GetTaskOwner(candidateIds));

                return CandidateAssociation;
            }
            finally 
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }  
        }

        private string GetTaskOwner(string ownerName)
        {
            if (ownerName.LastIndexOf(',') != -1)
                ownerName = ownerName.Remove(ownerName.LastIndexOf(',')); 

            return ownerName;
        }

        public InterviewSessionSearchCriteria GetPositionProfileInterviews(int positionProfileId, out string interviewOwner)
        {
            IDataReader dataReader = null;
            InterviewSessionSearchCriteria interviewDetailList = null;
            interviewOwner = null;
            try
            {  
                DbCommand positionProfileCommand = HCMDatabase.GetStoredProcCommand("SPGET_PP_CREATED_INTERVIEWS");

                HCMDatabase.AddInParameter(positionProfileCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileId); 

                dataReader = HCMDatabase.ExecuteReader(positionProfileCommand);

                while (dataReader.Read())
                {  
                    if (interviewDetailList == null)
                        interviewDetailList = new InterviewSessionSearchCriteria();

                    if(interviewDetailList.ListInterviewDetail ==null)
                        interviewDetailList.ListInterviewDetail = new List<InterviewDetail>();

                    InterviewDetail interviewDetail = new InterviewDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                    {
                        interviewDetail.InterviewName = dataReader["INTERVIEW_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_KEY"]))
                    {
                        interviewDetail.ParentInterviewKey = dataReader["INTERVIEW_KEY"].ToString(); 
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_COUNT"]))
                    {
                        interviewDetail.SessionCount = Convert.ToInt32( dataReader["SESSION_COUNT"]);
                    } 

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                    {
                        if (interviewOwner == null)
                            interviewOwner = dataReader["CREATED_BY"].ToString();
                        else
                        {
                            if (!interviewOwner.Contains(dataReader["CREATED_BY"].ToString()))
                            {
                                interviewOwner += dataReader["CREATED_BY"].ToString() + ",";
                            }
                        }
                    }
                    interviewDetailList.ListInterviewDetail.Add(interviewDetail);
                    
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    if (interviewDetailList == null)
                        interviewDetailList = new InterviewSessionSearchCriteria();

                    if (interviewDetailList.ListInterviewSessionDetail == null)
                        interviewDetailList.ListInterviewSessionDetail = new List<InterviewDetail>();

                    InterviewDetail interviewDetail = new InterviewDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                    {
                        interviewDetail.InterviewTestKey = dataReader["SESSION_KEY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_KEY"]))
                    {
                        interviewDetail.ParentInterviewKey = dataReader["INTERVIEW_KEY"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_COUNT"]))
                    {
                        interviewDetail.SessionCount = Convert.ToInt32(dataReader["SESSION_COUNT"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSED_COUNT"]))
                    {
                        interviewDetail.AssessedCount = Convert.ToInt32(dataReader["ASSESSED_COUNT"]);
                    } 

                    interviewDetailList.ListInterviewSessionDetail.Add(interviewDetail);
                }

                return interviewDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }  
        }

        public List<InterviewSessionDetail> GetPositionProfileInterviewSessionCount(int positionProfileId )
        {
            IDataReader dataReader = null;
            List<InterviewSessionDetail> interviewSessionDetailList = null; 
            try
            {
                DbCommand positionProfileCommand = HCMDatabase.GetStoredProcCommand("SPGET_PP_INTERVIEW_SESSION_COUNT");

                HCMDatabase.AddInParameter(positionProfileCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileId); 

                dataReader = HCMDatabase.ExecuteReader(positionProfileCommand);

                while (dataReader.Read())
                { 
                    if (interviewSessionDetailList == null)
                        interviewSessionDetailList = new List<InterviewSessionDetail>();

                    InterviewSessionDetail interviewDetail = new InterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                    {
                        interviewDetail.InterviewTestID = dataReader["SESSION_KEY"].ToString(); 
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SCHEDULED"]))
                    {
                        interviewDetail.ScheduledInterviews = dataReader["INTERVIEW_SCHEDULED"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_COMPLETED"]))
                    {
                        interviewDetail.CompletedInterviews =  dataReader["INTERVIEW_COMPLETED"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_ID"]))
                    {
                        interviewDetail.ClientRequestID = dataReader["POSITION_ID"].ToString();
                    } 
                    interviewSessionDetailList.Add(interviewDetail); 
                }

                return interviewSessionDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }  
        }

        public List<string> GetPositionProfileInterviewAssessment(int positionProfileId, int userId, out string createBy)
        {
            IDataReader dataReader = null;
            int candidateCount = 0;
            List<string> InterviewAssessment = null;
             
            createBy = null;

            try
            {
                InterviewAssessment = new List<string>();

                DbCommand positionProfileCommand = HCMDatabase.GetStoredProcCommand("SPGET_PP_INTERVIEW_ASSESSMENT_COUNT");

                HCMDatabase.AddInParameter(positionProfileCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileId);

                HCMDatabase.AddInParameter(positionProfileCommand, "@USERID",
                    DbType.Int32, userId); 

                dataReader = HCMDatabase.ExecuteReader(positionProfileCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        candidateCount = Convert.ToInt32(dataReader["TOTAL"]);
                        InterviewAssessment.Add(candidateCount.ToString());
                    }
 

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                    {
                        if (createBy == null)
                            createBy = dataReader["CREATED_BY"].ToString();
                        else
                        {
                            if (!createBy.Contains(dataReader["CREATED_BY"].ToString()))
                            {
                                createBy += dataReader["CREATED_BY"].ToString() + ",";
                            }
                        }
                    }
                }

                /*if (createBy != null)
                {
                    createBy = GetTaskOwner(createBy);
                    InterviewAssessment.Add(GetTaskOwner(createBy));
                }*/

                return InterviewAssessment;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }  
        }


        public void InsertPositionProfileCandidates(PositionProfileCandidate positionProfileCandidate, 
            IDbTransaction transaction)
        {
            DbCommand positionProfileCandidatesInsertCommand = HCMDatabase.GetStoredProcCommand
              ("SPINSERT_POSITION_PROFILE_CANDIDATES");

            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@POSITION_PROFILE_ID",
                DbType.Int32, positionProfileCandidate.PositionProfileID); 
            
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@CANDIDATE_ID",
                  DbType.Int32, positionProfileCandidate.CandidateID);
            
            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@PICKED_BY",
                DbType.Int32, positionProfileCandidate.PickedBy);

            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@PICKED_DATE",
                DbType.DateTime, positionProfileCandidate.PickedDate);

            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@CANDIDATE_STATUS",
                DbType.String, positionProfileCandidate.CandidateStatus);

            HCMDatabase.AddInParameter(positionProfileCandidatesInsertCommand, "@RECRUITER_ID",
                DbType.Int32, positionProfileCandidate.RecruiterID);  

            HCMDatabase.ExecuteNonQuery(positionProfileCandidatesInsertCommand, transaction as DbTransaction);
        }

        public int GetPositionProfileRecruiterAssignment(int positionProfileId,string ownerType)
        {
            IDataReader dataReader = null;
            int totalCount = 0;
            try
            {
                DbCommand getRecruiterDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_PP_RECRUITER_ASSIGNMENT_COUNT");

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@POSITION_PROFILE_ID", DbType.String, positionProfileId);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@OWNER_TYPE", DbType.String, ownerType); 

                dataReader = HCMDatabase.ExecuteReader(getRecruiterDetailsCommand);

                // Retreive recruiter details.
                while (dataReader.Read())
                { 
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalCount = int.Parse(dataReader["TOTAL"].ToString());
                        break;
                    }
                }
                return totalCount;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Retrieves position profile workflow selection details.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        public PositionProfieWorkflowDetail GetPositionProfileWorkflowSelection(int positionProfileID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand positionProfileWorkflowCommand = HCMDatabase.GetStoredProcCommand("SPGET_PP_WORKFLOW_SELECTION");

                HCMDatabase.AddInParameter(positionProfileWorkflowCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileID);

                dataReader = HCMDatabase.ExecuteReader(positionProfileWorkflowCommand);

                PositionProfieWorkflowDetail positionProfieWorkflowDetail =
                    new PositionProfieWorkflowDetail();

                // 1. Interview details
                List<InterviewDetail> interviewDetails = null;

                while (dataReader.Read())
                {
                    if (interviewDetails == null)
                        interviewDetails = new List<InterviewDetail>();

                    InterviewDetail interviewDetail = new InterviewDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                    {
                        interviewDetail.InterviewName = dataReader["INTERVIEW_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_KEY"]))
                    {
                        interviewDetail.InterviewTestKey = dataReader["INTERVIEW_KEY"].ToString();
                    }
                    interviewDetails.Add(interviewDetail);
                }
                positionProfieWorkflowDetail.InterviewDetail = interviewDetails;


                dataReader.NextResult();
                // 2. Interview session/schedule detail

                List<InterviewSessionDetail> interviewSessionDetails = null;
                while (dataReader.Read())
                {
                    if (interviewSessionDetails == null)
                        interviewSessionDetails = new List<InterviewSessionDetail>();

                    InterviewSessionDetail interviewSessionDetail = new InterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_KEY"]))
                    {
                        interviewSessionDetail.InterviewTestID = dataReader["INTERVIEW_KEY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                    {
                        interviewSessionDetail.InterviewTestSessionID = dataReader["SESSION_KEY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_COUNT"]))
                    {
                        interviewSessionDetail.InterviewSessionCount = Convert.ToInt32(dataReader["SESSION_COUNT"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_COUNT"]))
                    {
                        interviewSessionDetail.ScheduledInterviews = dataReader["SCHEDULED_COUNT"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_COUNT"]))
                    {
                        interviewSessionDetail.CompletedInterviews = dataReader["COMPLETED_COUNT"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSED_COUNT"]))
                    {
                        interviewSessionDetail.AssessedCandidates = Convert.ToInt32(dataReader["ASSESSED_COUNT"].ToString());
                    }
                    interviewSessionDetails.Add(interviewSessionDetail);
                }
                positionProfieWorkflowDetail.InterviewSessionDetail = interviewSessionDetails;

                dataReader.NextResult();
                //3. Interview Assessed Candidates

                int assessedCnt = 0;
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        assessedCnt = assessedCnt + 1;
                }
                positionProfieWorkflowDetail.InterviewAssessedCnt = assessedCnt;

                dataReader.NextResult();
                //4. Test Details

                List<TestDetail> testDetails = null;
                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();

                    TestDetail testDetail = new TestDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        testDetail.Name =
                            dataReader["TEST_NAME"].ToString().Trim();

                    testDetails.Add(testDetail);
                }
                positionProfieWorkflowDetail.TestDetail = testDetails;

                dataReader.NextResult();
                //5. Test session/schedule Details

                List<TestSessionDetail> testSessionDetails = null;
                while (dataReader.Read())
                {

                    if (testSessionDetails == null)
                        testSessionDetails = new List<TestSessionDetail>();

                    TestSessionDetail testSessionDetail = new TestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        testSessionDetail.TestID = dataReader["TEST_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                        testSessionDetail.TestSessionID =
                            dataReader["SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_COUNT"]))
                        testSessionDetail.TestSessionCount =
                            Convert.ToInt32(dataReader["SESSION_COUNT"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_COUNT"]))
                    {
                        testSessionDetail.ScheduledTests = dataReader["SCHEDULED_COUNT"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_COUNT"]))
                    {
                        testSessionDetail.CompletedTests = dataReader["COMPLETED_COUNT"].ToString();
                    }

                    testSessionDetails.Add(testSessionDetail);
                }
                positionProfieWorkflowDetail.TestSessionDetail = testSessionDetails;


                dataReader.NextResult();
                //6. Get submitted candidates

                List<CandidateInformation> candidateInformation = null;
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        if (candidateInformation == null)
                            candidateInformation = new List<CandidateInformation>();

                        CandidateInformation candidateInfo = new CandidateInformation();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            candidateInfo.caiID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                            candidateInfo.caiFirstName =
                                dataReader["FIRST_NAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                            candidateInfo.caiLastName =
                                dataReader["LAST_NAME"].ToString().Trim();

                        candidateInformation.Add(candidateInfo);
                    }
                }

                positionProfieWorkflowDetail.SubmittedCandidateInformation = candidateInformation;

                dataReader.NextResult();
                //7. Get interview required option
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_REQUIRED"]))
                        positionProfieWorkflowDetail.IsInterviewRequired = true;
                }

                dataReader.NextResult();
                //8. Get test required option
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_REQUIRED"]))
                        positionProfieWorkflowDetail.IsTestRequired = true;
                }

                return positionProfieWorkflowDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to delete the position profile actions against position 
        /// profile id and action type
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="action">
        /// A <see cref="string"/> that holds the action Type.
        /// </param>
        public void DeletePositionProfileActions(int positionProfileID,
            IDbTransaction transaction)
        {
            DbCommand deletePositionProfileActionsCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_POSITION_PROFILE_ACTIONS");
            HCMDatabase.AddInParameter(deletePositionProfileActionsCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
            HCMDatabase.ExecuteNonQuery(deletePositionProfileActionsCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Inserts the position profile actions.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="actionType">
        /// A <see cref="string"/> that holds the action Type.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">The transaction.</param>
        public void InsertPositionProfileActions(int positionProfileID, string ownerType, int userID,
            IDbTransaction transaction)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand insertPositionProfileActionsCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_POSITION_PROFILE_ACTIONS");
                // Add input parameters.
                HCMDatabase.AddInParameter(insertPositionProfileActionsCommand,
                  "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(insertPositionProfileActionsCommand,
                    "@ACTION_TYPE", DbType.String, ownerType);

                HCMDatabase.AddInParameter(insertPositionProfileActionsCommand,
                    "@USER_ID", DbType.Int32, userID);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(insertPositionProfileActionsCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Method that retreives the task owner details.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile id.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the 
        /// task owner details.
        /// </returns>
        public List<UserDetail> GetPositionProfileTaskOwners(int positionProfileID, int userID)
        {
            IDataReader dataReader = null;

            List<UserDetail> taskOwners = null;

            try
            {
                DbCommand getTaskOwnerDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_TASK_OWNERS_BY_ID");

                HCMDatabase.AddInParameter(getTaskOwnerDetailsCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(getTaskOwnerDetailsCommand,
                    "@USER_ID", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(getTaskOwnerDetailsCommand);

                // Retreive task owner details.
                while (dataReader.Read())
                {
                    // Instantiate task owner details list.
                    if (taskOwners == null)
                        taskOwners = new List<UserDetail>();

                    // Create task owner detail object.
                    UserDetail taskOwner = new UserDetail();

                    // Instantiate task owner detail object.
                    taskOwner = new UserDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        taskOwner.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        taskOwner.FirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        taskOwner.LastName = dataReader["LAST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["OWNER_TYPE"]))
                        taskOwner.OwnerType = dataReader["OWNER_TYPE"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        taskOwner.Email = dataReader["EMAIL"].ToString();

                    taskOwners.Add(taskOwner);
                }
                return taskOwners;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to delete the position profile owner against position 
        /// profile id and owner type
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        public void DeletePositionProfileOwner(int positionProfileID, 
            string ownerType, int userID)
        {
            DbCommand deletePositionProfileOwnerCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_POSITION_PROFILE_OWNER");
            HCMDatabase.AddInParameter(deletePositionProfileOwnerCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
            HCMDatabase.AddInParameter(deletePositionProfileOwnerCommand,
                "@OWNER_TYPE", DbType.String, ownerType);
            HCMDatabase.AddInParameter(deletePositionProfileOwnerCommand,
                "@USER_ID", DbType.Int32, userID);
            HCMDatabase.ExecuteNonQuery(deletePositionProfileOwnerCommand);
        }

        /// <summary>
        /// Inserts the position profile owner.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="ownerType">
        /// A <see cref="string"/> that holds the owner Type.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="createdBy">
        /// A <see cref="int"/> that holds the createdBy.
        /// </param>
        public void InsertPositionProfileOwner(int positionProfileID, string ownerType, int userID,
            int createdBy)
        {
            
            // Create a stored procedure command object.
            DbCommand insertPositionProfileOwnersCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_POSITION_PROFILE_OWNERS");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                "@OWNER_TYPE", DbType.String, ownerType);

            HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddInParameter(insertPositionProfileOwnersCommand,
                "@CREATED_BY", DbType.Int32, createdBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertPositionProfileOwnersCommand);
        }

        /// <summary>
        /// Insert the position profile default owners.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertPositionProfileDefaultOwner(int positionProfileID, int userID)
        {
            // Create a stored procedure command object.
            DbCommand insertPositionProfileDefaultOwnerCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_POSITION_PROFILE_DEFAULT_OWNERS");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertPositionProfileDefaultOwnerCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            HCMDatabase.AddInParameter(insertPositionProfileDefaultOwnerCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertPositionProfileDefaultOwnerCommand);
        }

        public void UpdatePositionProfileStatus(string status,string comments,int userId,
            int positionProfileId,int candidateId, string updateType)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand updatePositionProfileCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_POSITION_PROFILE_STATUS");

                // Add input parameters.
                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                  "@STATUS", DbType.String, status);

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@STATUS_COMMENTS", DbType.String, comments); 

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@MODIFIED_BY", DbType.Int32, userId); 

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);

                HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@UPDATE_TYPE", DbType.String, updateType);

                if (candidateId>0)
                    HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateId);

               HCMDatabase.ExecuteNonQuery(updatePositionProfileCommand );
            }
            catch (Exception exp)
            {   
                throw exp;
            }
        }

        public bool InserttPositionProfileAdditionalDocument(int positionProfileId, int candidateId,
            int userId, string fileName,byte[] documentSource)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand insertPPAdditionalDocumentsCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_POSITION_PROFILE_ADDITIONAL_DOCUMENT");
                 
                // Add input parameters.

                HCMDatabase.AddInParameter(insertPPAdditionalDocumentsCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);

                HCMDatabase.AddInParameter(insertPPAdditionalDocumentsCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateId);

                HCMDatabase.AddInParameter(insertPPAdditionalDocumentsCommand,
                    "@USER_ID", DbType.Int32, userId);

                HCMDatabase.AddInParameter(insertPPAdditionalDocumentsCommand,
                    "@FILE_NAME", DbType.String, fileName);

                HCMDatabase.AddInParameter(insertPPAdditionalDocumentsCommand,
               "@DOCUMENT_SOURCE", DbType.Binary, documentSource); 

                HCMDatabase.AddOutParameter(insertPPAdditionalDocumentsCommand,
               "@ISEXISTS", DbType.String, 1);

                HCMDatabase.ExecuteNonQuery(insertPPAdditionalDocumentsCommand);

                string ifExists = HCMDatabase.GetParameterValue
                (insertPPAdditionalDocumentsCommand, "@ISEXISTS").ToString().Trim();  

                return ifExists == "Y" ? false : true; 

            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public List<CareerBuilderResume> GetCandidateAdditionalDocuments(int positionProfileId, int candidateId)
        {
            IDataReader dataReader = null;

            List<CareerBuilderResume> additionalDocs = null;

            try
            {
                DbCommand additionalDocsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_PP_ADDITIONAL_DOCUMENTS");

                HCMDatabase.AddInParameter(additionalDocsCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);

                HCMDatabase.AddInParameter(additionalDocsCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateId);

                dataReader = HCMDatabase.ExecuteReader(additionalDocsCommand);
                 
                while (dataReader.Read())
                {
                    if (additionalDocs == null)
                        additionalDocs = new List<CareerBuilderResume>();

                    CareerBuilderResume careerBuilderResume = new CareerBuilderResume();



                    if (!Utility.IsNullOrEmpty(dataReader["DOCUMENT_ID"]))
                        careerBuilderResume.ResumeID = dataReader["DOCUMENT_ID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["FILE_NAME"]))
                        careerBuilderResume.ResumeTitle = dataReader["FILE_NAME"].ToString();
                     
                    additionalDocs.Add(careerBuilderResume);
                }
                return additionalDocs;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public void DeleteAdditionalDocument(int documentId)
        { 
            try
            {
                // Create a stored procedure command object.
                DbCommand updatePositionProfileCommand = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_PP_ADDITIONAL_DOCUMENT");

                if (documentId > 0)
                    HCMDatabase.AddInParameter(updatePositionProfileCommand,
                    "@DOCUMENT_ID", DbType.Int32, documentId);

                HCMDatabase.ExecuteNonQuery(updatePositionProfileCommand);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public byte[] GetAdditionalDocumentContent(int documentId)
        { 
            //SPGET_PP_ADDITIONAL_DOCUMENT_CONTENT
            IDataReader dataReader = null; 

            try
            {
                DbCommand additionalDocsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_PP_ADDITIONAL_DOCUMENT_CONTENT");

                HCMDatabase.AddInParameter(additionalDocsCommand,
                    "@DOCUMENT_ID", DbType.Int32, documentId); 

                dataReader = HCMDatabase.ExecuteReader(additionalDocsCommand);

                if (dataReader.Read())
                {  
                    if (!Utility.IsNullOrEmpty(dataReader["DOCUMENT_SOURCE"]))
                        return (byte[])(dataReader["DOCUMENT_SOURCE"]);  
                }
                return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the position profile owner rights for an user
        /// against the position profile.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="string"/> that holds the position profile owner
        /// rights.
        /// </returns>
        public List<string> GetPositionProfileOwnerRights(int positionProfileID, int userID)
        {
            IDataReader dataReader = null;
            List<string> rights = null;

            try
            {
                // Create command object.
                DbCommand getRightsCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_POSITION_PROFILE_OWNER_RIGHTS");

                // Add input parameter
                HCMDatabase.AddInParameter(getRightsCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
                HCMDatabase.AddInParameter(getRightsCommand, "@USER_ID", DbType.Int32, userID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getRightsCommand);
               
                while (dataReader.Read())
                {
                    if (rights == null)
                        rights = new List<string>();

                    if (dataReader["OWNER_TYPE"] != null)
                        rights.Add(dataReader["OWNER_TYPE"].ToString());
                }
                dataReader.Close();

                return rights;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the position profile review rights for an user
        /// against the position profile id and tenant ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="string"/> that holds the position profile review
        /// rights.
        /// </returns>
        public List<string> GetPositionProfileReviewRights(int positionProfileID, int tenantID)
        {
            IDataReader dataReader = null;
            List<string> reviewRights = null;

            try
            {
                // Create command object.
                DbCommand getReviewRightsCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_POSITION_PROFILE_REVIEW_RIGHTS");

                // Add input parameter
                HCMDatabase.AddInParameter(getReviewRightsCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
                HCMDatabase.AddInParameter(getReviewRightsCommand, "@TENANT_ID", DbType.Int32, tenantID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getReviewRightsCommand);

                while (dataReader.Read())
                {
                    if (reviewRights == null)
                        reviewRights = new List<string>();

                    if (dataReader["IN_TENANT"] != null)
                        reviewRights.Add(dataReader["IN_TENANT"].ToString());
                }
                dataReader.Close();

                return reviewRights;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the position profile owner details for the given
        /// position profile ID.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// A list of <see cref="PositionProfileOwnerDetail"/> that holds the position 
        /// profile owner details.
        /// </returns>
        public List<PositionProfileOwnerDetail> GetPositionProfileOwners(int positionProfileID)
        {
            IDataReader dataReader = null;
            List<PositionProfileOwnerDetail> owners = null;

            try
            {
                // Create command object
                DbCommand getOwners = HCMDatabase.GetStoredProcCommand
                    ("SPGET_POSITION_PROFILE_OWNERS");

                // Add input parameter
                HCMDatabase.AddInParameter(getOwners, "@POSITION_PROFILE_ID", 
                    DbType.Int32, positionProfileID);

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getOwners);

                while (dataReader.Read())
                {
                    // Instantiate owner list object.
                    if (owners == null)
                        owners = new List<PositionProfileOwnerDetail>();

                    PositionProfileOwnerDetail owner = new PositionProfileOwnerDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        owner.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["OWNER_TYPE"]))
                        owner.OwnerType = dataReader["OWNER_TYPE"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        owner.FirstName = dataReader["FIRST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        owner.EMail = dataReader["EMAIL"].ToString().Trim();

                    // Add to the list.
                    owners.Add(owner);
                }
                dataReader.Close();

                return owners;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of recruiter assignments.
        /// </summary>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <param name="positionProfileStatus">
        /// A <see cref="string"/> that holds the position profile status. This
        /// can be of PPS_OPEN, PPS_CLOSED or PPS_HOLD. When null is passed
        /// then will return all statuses.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterAssignmentDetail"/> that holds the 
        /// assignment details.
        /// </returns>
        public List<RecruiterAssignmentDetail> GetRecruiterAssignments
            (int recruiterID, string positionProfileStatus, int pageNumber,
            int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<RecruiterAssignmentDetail> assignementDetails = null;

            try
            {
                DbCommand getAssessmentsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_RECRUITER_POSITION_PROFILE_ASSIGNMENTS");

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@RECRUITER_ID", DbType.Int32, recruiterID);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@POSITION_PROFILE_STATUS", DbType.String, positionProfileStatus);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getAssessmentsCommand);

                while (dataReader.Read())
                {
                    // Instantiate assignment details list.
                    if (assignementDetails == null)
                        assignementDetails = new List<RecruiterAssignmentDetail>();

                    // Create assignment detail object.
                    RecruiterAssignmentDetail assignmentDetail = new RecruiterAssignmentDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        assignmentDetail.PositionProfileID =
                            Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        assignmentDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        assignmentDetail.ClientName = dataReader["CLIENT_NAME"].ToString();

                    string client = string.Empty;
                    string clientID = string.Empty;

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID_NAME"]))
                        client = dataReader["CLIENT_ID_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(client))
                    {
                        Regex regex = new Regex(@"(^\d*)");
                        Match match = regex.Match(client);

                        if (match.Success)
                        {
                            clientID = match.Value;
                            assignmentDetail.ClientID = Convert.ToInt32(clientID);
                        }
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS"]))
                        assignmentDetail.PositionProfileStatus = dataReader["POSITION_PROFILE_STATUS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS_NAME"]))
                        assignmentDetail.PositionProfileStatusName = dataReader["POSITION_PROFILE_STATUS_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSIGNED_DATE"]))
                        assignmentDetail.AssignedDate = Convert.ToDateTime(dataReader["ASSIGNED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATES_ASSOCIATED_BY_RECRUITER"]))
                        assignmentDetail.CandidatesAssociatedByRecruiter = Convert.ToInt32(dataReader["CANDIDATES_ASSOCIATED_BY_RECRUITER"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATES_SUBMITTED_TO_CLIENT_BY_RECRUITER"]))
                        assignmentDetail.CandidatesSubmittedToClientByRecruiter = Convert.ToInt32(dataReader["CANDIDATES_SUBMITTED_TO_CLIENT_BY_RECRUITER"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER"]))
                        assignmentDetail.PositionProfileOwner = dataReader["POSITION_PROFILE_OWNER"].ToString();

                    assignementDetails.Add(assignmentDetail);
                }
                return assignementDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the recruiter detail.
        /// </summary>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <returns>
        /// A <see cref="RecruiterDetail"/> that holds the recruiter detail.
        /// </returns>
        public RecruiterDetail GetRecruiterByID(int recruiterID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getContributorQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_RECRUITER_DETAILS_BY_USER_ID");

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@USER_ID", DbType.Int32, recruiterID);

                dataReader = HCMDatabase.ExecuteReader(getContributorQuestionCommand);

                RecruiterDetail recruiterDetail = null;

                // Retreive recruiter details.
                if (dataReader.Read())
                {
                    // Instantiate assessor detail object.
                    recruiterDetail = new RecruiterDetail();

                    recruiterDetail.UserID = recruiterID;

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        recruiterDetail.FirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MIDDLE_NAME"]))
                        recruiterDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        recruiterDetail.LastName = dataReader["LAST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        recruiterDetail.Email = dataReader["EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE_POSITION_ASSIGNMENTS"]))
                        recruiterDetail.ActivePositionAssignments = Convert.ToInt32(dataReader["ACTIVE_POSITION_ASSIGNMENTS"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECENT_POSITION_ASSIGNMENT"]))
                        recruiterDetail.RecentPositionAssignment = Convert.ToDateTime(dataReader["RECENT_POSITION_ASSIGNMENT"].ToString());
                }
                return recruiterDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the position profile candidate detail by gen ID.
        /// </summary>
        /// <param name="genID">
        /// A <see cref="int"/> that holds the gen ID.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileCandidate"/> that holds the position profile 
        /// candidate detail.
        /// </returns>
        public PositionProfileCandidate GetPositionProfileCandidateDetailByGenID(int genID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_CANDIDATE");

                HCMDatabase.AddInParameter(getCandidateCommand,
                    "@ID", DbType.Int32, genID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateCommand);

                PositionProfileCandidate candidateDetail = null;

                // Retreive position profile candidate details.
                if (dataReader.Read())
                {
                    // Instantiate assessor detail object.
                    candidateDetail = new PositionProfileCandidate();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        candidateDetail.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        candidateDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_STATUS"]))
                        candidateDetail.CandidateStatus = dataReader["CANDIDATE_STATUS"].ToString();

                     if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_STATUS_NAME"]))
                        candidateDetail.CandidateStatusName = dataReader["CANDIDATE_STATUS_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        candidateDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        candidateDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_ID"]))
                        candidateDetail.RecruiterID = Convert.ToInt32(dataReader["RECRUITER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_NAME"]))
                        candidateDetail.RecruiterName = dataReader["RECRUITER_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_FIRST_NAME"]))
                        candidateDetail.RecruiterFirstName = dataReader["RECRUITER_FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_EMAIL"]))
                        candidateDetail.RecruiterEMail = dataReader["RECRUITER_EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT"]))
                    {
                        string client = dataReader["CLIENT"].ToString();

                        string clientID = string.Empty;
                        string clientName = string.Empty;

                        if (!Utility.IsNullOrEmpty(client))
                        {
                            Regex regex = new Regex(@"(^\d*)");
                            Match match = regex.Match(client);

                            if (match.Success)
                            {
                                clientID = match.Value;
                                clientName = regex.Replace(client, string.Empty);
                                candidateDetail.ClientName = clientName.Trim(new char[] { '~' });
                            }
                        }
                    }
                }
                return candidateDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of assessed candidates against the
        /// given position profile ID & interview session key.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="showAll">
        /// A <see cref="bool"/> that indicates whether to show candidates across 
        /// all sessions against this position profile. True indicate show all
        /// and false indicates show only for the interview session key.
        /// </param>
        /// <param name="interviewSessionKey">
        /// A <see cref="string"/> that holds the interview session key.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInterviewSessionDetail"/> that holds the assessed candidates.
        /// </returns>
        public List<CandidateInterviewSessionDetail> GetAssessedCandidates
            (int positionProfileID, bool showAll, string interviewSessionKey)
        {
            IDataReader dataReader = null;

            List<CandidateInterviewSessionDetail> assessedCandidates = null;

            try
            {
                DbCommand getAssessmentsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSED_CANDIDATES_BY_SESSION");

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@SHOW_ALL", DbType.String, showAll ? "Y" : "N");

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionKey);

                dataReader = HCMDatabase.ExecuteReader(getAssessmentsCommand);

                while (dataReader.Read())
                {
                    // Instantiate assessed candidates list.
                    if (assessedCandidates == null)
                        assessedCandidates = new List<CandidateInterviewSessionDetail>();

                    // Create assessed candidates object.
                    CandidateInterviewSessionDetail assignmentDetail = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        assignmentDetail.CandidateInterviewSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        assignmentDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        assignmentDetail.CandidateUserID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        assignmentDetail.CandidateFullName = dataReader["CANDIDATE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                        assignmentDetail.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_KEY"]))
                        assignmentDetail.InterviewTestKey = dataReader["INTERVIEW_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                        assignmentDetail.InterviewSessionKey = dataReader["INTERVIEW_SESSION_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                        assignmentDetail.InterviewTestName = dataReader["INTERVIEW_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        assignmentDetail.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        assignmentDetail.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_COMPLETED_DATE"]))
                        assignmentDetail.DateCompleted = Convert.ToDateTime(dataReader["INTERVIEW_COMPLETED_DATE"].ToString());
                    
                    // Add to the list.
                    assessedCandidates.Add(assignmentDetail);
                }
                return assessedCandidates;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
    }
}