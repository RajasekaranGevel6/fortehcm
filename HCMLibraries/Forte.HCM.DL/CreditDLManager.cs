﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreditDLManager.cs
// File that represents the data layer for the credit management.
// This will talk to the database to perform the operations associated
// with the credit management processes.

#endregion

#region Directives                                                             
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the credit management.
    /// This will talk to the database to perform the operations associated
    /// with the credit management processes.
    /// </summary>
    public class CreditDLManager : DatabaseConnectionManager
    {
        #region Public Method                                                  

        /// <summary>
        /// Represents the default constructor.
        /// </summary>
        public CreditDLManager()
        {
        }

        /// <summary>
        /// Method to get the credits details based on the search criteria
        /// </summary>
        /// <param name="creditSearchCriteria">
        /// A<see cref="CreditRequestSearchCriteria"/>that holds the credit Search Criteria
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pageNumber
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the pageSize
        /// </param>
        /// <param name="sortField">
        /// A<see cref="string"/>that holds the sort Field
        /// </param>
        /// <param name="sordOrder">
        /// A<see cref="SortType"/>that holds the sordOrder like Asc/Dsc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records
        /// </param>
        /// <returns>
        /// A List for<see cref="CreditRequestDetail"/>that holds the Credit Request Detail
        /// </returns>
        public List<CreditRequestDetail> GetCreditDetails(CreditRequestSearchCriteria creditSearchCriteria,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<CreditRequestDetail> creditRequestDetails = new List<CreditRequestDetail>();

            CreditRequestDetail creditRequestDetail = null;

            try
            {
                DbCommand getCreditRequestCommand = HCMDatabase.
            GetStoredProcCommand("SPGET_CREDIT_REQUEST");

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                    "@NAME", DbType.String, creditSearchCriteria.CandidateName.Length == 0 ? null : creditSearchCriteria.CandidateName);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                    "@EMAIL", DbType.String, creditSearchCriteria.CandidateEmail.Length == 0 ? null : creditSearchCriteria.CandidateEmail);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                    "@PROCESSED", DbType.String, creditSearchCriteria.ProcessedStatus);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                    "@APPROVED", DbType.String, creditSearchCriteria.ApprovedStatus);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                     "@REQUEST_DATE_FROM", DbType.DateTime, creditSearchCriteria.RequestDateFrom);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                    "@REQUEST_DATE_TO", DbType.DateTime, creditSearchCriteria.RequestDateTo);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
               "@PROCESSED_DATE_FROM", DbType.DateTime, creditSearchCriteria.ProcessedDateFrom);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                     "@PROCESSED_DATE_TO", DbType.DateTime, creditSearchCriteria.ProcessedDateTo);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                  "@AMOUNT_FROM", DbType.Decimal, creditSearchCriteria.AmountFrom);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                     "@AMOUNT_TO", DbType.Decimal, creditSearchCriteria.AmountTo);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                     "@CREDIT_TYPE", DbType.String, creditSearchCriteria.CreditType);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                    "@PAGENUM", DbType.String, pageNumber);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                   "@ORDERBY", DbType.String, sortField);

                HCMDatabase.AddInParameter(getCreditRequestCommand,
                    "@ORDERBYDIRECTION", DbType.String, sordOrder == SortType.Ascending ? "A" : "D");

                dataReader = HCMDatabase.ExecuteReader(getCreditRequestCommand);

                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        creditRequestDetail = new CreditRequestDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["GEN_ID"]))
                        {
                            creditRequestDetail.GenId = int.Parse(dataReader["GEN_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        {
                            creditRequestDetail.CandidateId = int.Parse(dataReader["USER_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        {
                            creditRequestDetail.CandidateFirstName = dataReader["CANDIDATE_NAME"]
                                .ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FULLNAME"]))
                        {
                            creditRequestDetail.CandidateFullName = dataReader["CANDIDATE_FULLNAME"]
                                .ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                        {
                            creditRequestDetail.CreditRequestDescription = dataReader["ATTRIBUTE_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["REQUEST_DATE"]))
                        {
                            creditRequestDetail.RequestDate = Convert.ToDateTime(dataReader["REQUEST_DATE"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PROCESSED"]))
                        {
                            creditRequestDetail.IsProcessed = dataReader["PROCESSED"].ToString()
                                == "N" ? false : true;
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PROCESSED_DATE"]))
                        {
                            creditRequestDetail.ProcessedDate = Convert.ToDateTime(dataReader["PROCESSED_DATE"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["APPROVED"]))
                        {
                            creditRequestDetail.IsApproved = dataReader["APPROVED"].ToString()
                                == "N" ? false : true;
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["AMOUNT"]))
                        {
                            creditRequestDetail.Amount = Convert.ToDecimal(dataReader["AMOUNT"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["REMARKS"]))
                        {
                            creditRequestDetail.Remarks = dataReader["REMARKS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREDIT_VALUE"]))
                        {
                            creditRequestDetail.CreditValue = decimal.Parse(dataReader["CREDIT_VALUE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        {
                            creditRequestDetail.CandidateEMail = dataReader["EMAIL"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                        {
                            creditRequestDetail.CreditRequestID = dataReader["ATTRIBUTE_ID"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ENTITY_KEY"]))
                        {
                            creditRequestDetail.EntityID = dataReader["ENTITY_KEY"].ToString();
                        }

                        creditRequestDetails.Add(creditRequestDetail);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return creditRequestDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }

            }
        }

        /// <summary>
        /// Method to get the credit summary from Database
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/>that holds the user ID
        /// </param> 
        /// <returns>
        /// A List for<see cref="CreditsSummary"/>that holds the credit summary.
        /// </returns>
        public CreditsSummary GetCreditSummary(int userID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCreditsAvailable =
                    HCMDatabase.GetStoredProcCommand("SPGET_CREDIT_SUMMARY");
                HCMDatabase.AddInParameter(getCreditsAvailable, "@USER_ID",
                    DbType.Int32, userID);
                dataReader = HCMDatabase.ExecuteReader(getCreditsAvailable);
                CreditsSummary creditsSummary = null;
                if (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(creditsSummary))
                        creditsSummary = new CreditsSummary();
                    if (!Utility.IsNullOrEmpty(dataReader["USER_FIRSTNAME"]))
                        creditsSummary.UserID = dataReader["USER_FIRSTNAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["AVAILABLE_CREDITS"]))
                        creditsSummary.AvailableCredits = decimal.Parse(dataReader["AVAILABLE_CREDITS"].ToString());
                    if (!Utility.IsNullOrEmpty(dataReader["CREDITS_EARNED"]))
                        creditsSummary.CreditsEarned = decimal.Parse(dataReader["CREDITS_EARNED"].ToString());
                    if (!Utility.IsNullOrEmpty(dataReader["CREDITS_REDEEMED"]))
                        creditsSummary.CreditsRedeemed = decimal.Parse(dataReader["CREDITS_REDEEMED"].ToString());
                }
                return creditsSummary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method to get the list of crdits usage history from database
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/>that holds the user ID
        /// </param>
        /// <param name="creditType">
        /// A <see cref="string"/>that holds the credit Type
        /// </param>
        /// <param name="pagenumber">
        /// A <see cref="int"/>that holds the page number of the page
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size of the page
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/>that holds the total records of credit history table 
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/>that holds the expression to sort
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/>that holds the sort type.
        /// </param>
        /// <returns>
        /// A list for <see cref="CreditUsageDetail"/> that holds credit usage details.
        /// </returns>
        public List<CreditUsageDetail> GetCreditsUsageHistory(int userID, 
            string creditType, int pagenumber, int pageSize, out int totalRecords,
            string orderBy, SortType orderByDirection)
        {
            totalRecords = 0;
            IDataReader dataReader = null;

            try
            {
                DbCommand getCreditsHistory =
                    HCMDatabase.GetStoredProcCommand("SPGET_CREDITS_USAGE_HISTORY");

                HCMDatabase.AddInParameter(getCreditsHistory, "@USER_ID",
                    DbType.Int32, userID);

                HCMDatabase.AddInParameter(getCreditsHistory, "@CREDIT_TYPE",
                      DbType.String, creditType);

                HCMDatabase.AddInParameter(getCreditsHistory, "@PAGENUM",
                    DbType.Int32, pagenumber);

                HCMDatabase.AddInParameter(getCreditsHistory, "@PAGESIZE",
                    DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getCreditsHistory, "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getCreditsHistory, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getCreditsHistory);

                List<CreditUsageDetail> creditsUsageList = null;
                CreditUsageDetail creditsUsage = null;
                while (dataReader.Read())
                {
                    if (creditsUsageList == null)
                        creditsUsageList = new List<CreditUsageDetail>();

                    creditsUsage = new CreditUsageDetail();

                    if (dataReader["TOTAL"] != DBNull.Value)
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                        continue;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["PROCESSED_DATE"]))
                        creditsUsage.Date = Convert.ToDateTime(dataReader["PROCESSED_DATE"].ToString());
                    if (!Utility.IsNullOrEmpty(dataReader["CREDITS"]))
                        creditsUsage.Credit = Convert.ToDecimal((dataReader["CREDITS"]));
                    if (!Utility.IsNullOrEmpty(dataReader["CREDIT_TYPE"]))
                        creditsUsage.UsageHistoryCreditType = dataReader["CREDIT_TYPE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["CREDIT_INFO"]))
                        creditsUsage.Description = dataReader["CREDIT_INFO"].ToString();

                    creditsUsageList.Add(creditsUsage);
                }
                return creditsUsageList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Represents the method to approve the credit request
        /// </summary>
        /// <param name="creditRequestDetail">
        /// A<see cref="CreditRequestDetail"/>that holds the request detail
        /// for the credit
        /// </param>
        /// <param name="transaction">
        /// A<see cref="IDbTransaction"/>that holds the transaction object
        /// </param>
        public void ApproveCreditRequest(CreditRequestDetail creditRequestDetail,
            IDbTransaction transaction)
        {
            DbCommand creditUpdateCommand = HCMDatabase.GetStoredProcCommand
                ("SPUPDATE_CREDIT_REQUEST");

            HCMDatabase.AddInParameter(creditUpdateCommand, "@CREDIT_REQUEST_GENID",
              DbType.Int32, creditRequestDetail.GenId);

            HCMDatabase.AddInParameter(creditUpdateCommand, "@USER_ID",
                  DbType.Int32, creditRequestDetail.CandidateId);

            HCMDatabase.AddInParameter(creditUpdateCommand, "@PROCESSED",
                DbType.String, creditRequestDetail.IsProcessed == true ? "Y" : "N");

            HCMDatabase.AddInParameter(creditUpdateCommand, "@APPROVED",
                DbType.String, creditRequestDetail.IsApproved == true ? "Y" : "N");

            HCMDatabase.AddInParameter(creditUpdateCommand, "@AMOUNT",
                DbType.Decimal, creditRequestDetail.Amount);

            HCMDatabase.AddInParameter(creditUpdateCommand, "@REMARKS",
                DbType.String, creditRequestDetail.Remarks);

            HCMDatabase.AddInParameter(creditUpdateCommand, "@MODIFIED_BY",
                DbType.String, creditRequestDetail.ModifiedBy);

            HCMDatabase.ExecuteNonQuery(creditUpdateCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Represents the method to insert into the credit history table
        /// </summary>
        /// <param name="creditRequestDetail">
        /// A<see cref="CreditRequestDetail"/>that holds the credit request details
        /// </param>
        /// <param name="transaction">
        /// A<see cref="IDbTransaction"/>that holds the transaction
        /// </param>
        public void InsertIntoCreditHistory(CreditRequestDetail creditRequestDetail,
            IDbTransaction transaction)
        {
            DbCommand creditHistoryInsertCommand = HCMDatabase.GetStoredProcCommand
                ("SPINSERT_CREDIT_HISTORY");

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@USER_ID",
                  DbType.Int32, creditRequestDetail.CandidateId);

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@CREDIT_TYPE",
                DbType.String, Constants.CreditTypeConstants.CREDIT_EARNED);

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@CREDIT_INFO",
                DbType.String, Constants.CreditInfoConstants.REQUEST_CREDIT_APPROVAL);

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@AMOUNT",
                DbType.Decimal, creditRequestDetail.Amount);

            HCMDatabase.AddInParameter(creditHistoryInsertCommand, "@CREATED_BY",
                DbType.String, creditRequestDetail.CreatedBy);

            HCMDatabase.ExecuteNonQuery(creditHistoryInsertCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Represents the method to get credit configuration details
        /// </summary>
        /// <returns>
        /// that holds credit configuration details 
        /// </returns>
        public DataSet GetCreditConfig()
        {
            DataSet dataSet = null;
            DbCommand creditConfigCommand = HCMDatabase.
                GetStoredProcCommand("SPGET_CREDITS_CONFIG");
            // Execute the stored procedure.
            dataSet = HCMDatabase.ExecuteDataSet(creditConfigCommand);
            return dataSet;
        }

        /// <summary>
        /// Method that updates credit values into the database.
        /// </summary>
        /// <param name="creditValue">
        /// A <see cref="decimal"/> object that holds the credit value.
        /// </param>
        ///  /// <param name="genID">
        /// A <see cref="int"/> object that holds the credit config gen ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> object that holds the user ID.
        /// </param>
        public void UpdateCreditConfig(decimal creditValue, int genID, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateCreditCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CREDIT_CONFIG_VALUE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCreditCommand,
                "@CREDIT_VALUE", DbType.Decimal, creditValue);
            HCMDatabase.AddInParameter(updateCreditCommand,
                "@CREDIT_CONFIG_GENID", DbType.Int32, genID);
            HCMDatabase.AddInParameter(updateCreditCommand,
                "@MODIFIED_BY", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCreditCommand);
        }
        #endregion
    }
}
