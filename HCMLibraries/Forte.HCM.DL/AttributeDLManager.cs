﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdminDLManager.cs
// File that represents the data layer for the attributes management.
// This will talk to the database to perform the operations associated
// with attributes management technique.

#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the attributes management.
    /// This includes functionalities for retrieving and updating settings 
    /// values for attributes such as complexity, test area, etc. This will 
    /// talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class AttributeDLManager : DatabaseConnectionManager
    {
        #region Public Method                                                  
        
        /// <summary>
        /// Method that retrieves the attributes based on the given attribute
        /// type ID.
        /// </summary>
        /// <param name="attributeTypeID">
        /// A <see cref="string"/> that holds the attribute type ID.
        /// </param>
        /// <param name="sortingOrder">
        /// A <see cref="string"/> that holds the sorting order. 'A' for 
        /// ascending and 'D' for descending 
        /// </param>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the attributes.
        /// </returns>
        public List<AttributeDetail> GetAttributesByType(string attributeTypeID,
            string sortingOrder)
        {
            IDataReader dataReader = null;
            List<AttributeDetail> attributesList = null;

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_ATTRIBUTE_BY_TYPE");

                HCMDatabase.AddInParameter(attributeCommand,
                    "@ATTRIBUTE_TYPE_ID",
                    DbType.String,
                    (Utility.IsNullOrEmpty(attributeTypeID) ? null : attributeTypeID));

                HCMDatabase.AddInParameter(attributeCommand,
                    "@ORDER_BY",
                    DbType.String,
                    (Utility.IsNullOrEmpty(sortingOrder) ? null : sortingOrder));

                //HCMDatabase.AddInParameter(attributeCommand,
                //    "@USERID",
                //    DbType.Int32,
                //    (Utility.IsNullOrEmpty(userID) ? 0 : userID));

                dataReader = HCMDatabase.ExecuteReader(attributeCommand);

                AttributeDetail attributeDetail;
                while (dataReader.Read())
                {
                    // Instantiate the attributes collection.
                    if (attributesList == null)
                        attributesList = new List<AttributeDetail>();

                    // Instantiate the attibute detail.
                    attributeDetail = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                    {
                        attributeDetail.AttributeID =
                            dataReader["ATTRIBUTE_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_TYPE"]))
                    {
                        attributeDetail.AttributeType =
                            dataReader["ATTRIBUTE_TYPE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        attributeDetail.AttributeName =
                            dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_VALUE"]))
                    {
                        attributeDetail.AttributeValue =
                            dataReader["ATTRIBUTE_VALUE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                    {
                        attributeDetail.ModifiedDate =
                            Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                    }

                    // Add the attribute to the collection.
                    attributesList.Add(attributeDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return attributesList;
        }

        /// <summary>
        /// Method that retrieves the attributes based on the given tenant ID & attribute type ID.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="attributeTypeID">
        /// A <see cref="string"/> that holds the attribute type ID.
        /// </param>
        /// <param name="sortingOrder">
        /// A <see cref="string"/> that holds the sorting order. 'A' for 
        /// ascending and 'D' for descending 
        /// </param>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the attributes.
        /// </returns>
        public List<AttributeDetail> GetAttributes(int tenantID, string attributeTypeID,
            string sortingOrder)
        {
            IDataReader dataReader = null;
            List<AttributeDetail> attributesList = null;

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_ATTRIBUTES");

                HCMDatabase.AddInParameter(attributeCommand, "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(attributeCommand, "@ATTRIBUTE_TYPE_ID", DbType.String,
                    (Utility.IsNullOrEmpty(attributeTypeID) ? null : attributeTypeID));

                HCMDatabase.AddInParameter(attributeCommand,
                    "@ORDER_BY",
                    DbType.String,
                    (Utility.IsNullOrEmpty(sortingOrder) ? null : sortingOrder));

                dataReader = HCMDatabase.ExecuteReader(attributeCommand);

                AttributeDetail attributeDetail;
                while (dataReader.Read())
                {
                    // Instantiate the attributes collection.
                    if (attributesList == null)
                        attributesList = new List<AttributeDetail>();

                    // Instantiate the attibute detail.
                    attributeDetail = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                    {
                        attributeDetail.AttributeID =
                            dataReader["ATTRIBUTE_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_TYPE"]))
                    {
                        attributeDetail.AttributeType =
                            dataReader["ATTRIBUTE_TYPE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        attributeDetail.AttributeName =
                            dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_VALUE"]))
                    {
                        attributeDetail.AttributeValue =
                            dataReader["ATTRIBUTE_VALUE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                    {
                        attributeDetail.ModifiedDate =
                            Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                    }

                    // Add the attribute to the collection.
                    attributesList.Add(attributeDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return attributesList;
        }

        /// <summary>
        /// Method that retrieves the list of attribute types.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the attribute 
        /// types.
        /// </returns>
        public List<AttributeDetail> GetAttributeTypes()
        {
            IDataReader dataReader = null;
            List<AttributeDetail> attributesList = null;

            try
            {
                DbCommand atrributeGroupCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ATTRIBUTE_GROUP_TYPES");

                dataReader = HCMDatabase.ExecuteReader(atrributeGroupCommand);

                attributesList = new List<AttributeDetail>();

                AttributeDetail attributeDetail;

                while (dataReader.Read())
                {
                    // Instantiate the attributes collection.
                    if (attributesList == null)
                        attributesList = new List<AttributeDetail>();

                    // Instantiate the attibute detail.
                    attributeDetail = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                    {
                        attributeDetail.AttributeID =
                            dataReader["ATTRIBUTE_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        attributeDetail.AttributeName =
                            dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }

                    // Add the attribute to the collection.
                    attributesList.Add(attributeDetail);
                }
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return attributesList;
        }

        /// <summary>
        /// Method that checks if the given attribute already exist.
        /// </summary>
        /// <param name="attributeName">
        /// A <see cref="string"/> that holds the attribute ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the ATTRIBUTE table.
        /// </remarks>
        public bool IsAttributeExist(int tenantID, string attributeName)
        {
            IDataReader dataReader = null;

            if (Utility.IsNullOrEmpty(attributeName))
                throw new Exception("Attribute name parameter cannot be empty");

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_ATTRIBUTE_BY_NAME");
                HCMDatabase.AddInParameter(attributeCommand, "@TENANT_ID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(attributeCommand,
                    "@ATTRIBUTE_NAME",
                    DbType.String,
                    (Utility.IsNullOrEmpty(attributeName) ? null : attributeName));

                dataReader = HCMDatabase.ExecuteReader(attributeCommand);

                if (dataReader.Read())
                    return true;
                else
                    return false;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that checks if the given test area attribute ID used in 
        /// questions.
        /// </summary>
        /// <param name="testAreaID">
        /// A <see cref="string"/> that holds the test area attribute ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the QUESTION table.
        /// </remarks>
        public bool IsTestAreaUsed(string testAreaID)
        {
            IDataReader dataReader = null;

            if (Utility.IsNullOrEmpty(testAreaID))
                throw new Exception("Test area attribute ID parameter cannot be empty");

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPCHECK_TEST_AREA_USED");

                HCMDatabase.AddInParameter(attributeCommand,
                    "@TEST_AREA_ID",
                    DbType.String,
                    (Utility.IsNullOrEmpty(testAreaID) ? null : testAreaID));

                object count = HCMDatabase.ExecuteScalar(attributeCommand);

                return Convert.ToInt32(count) == 0 ? false : true;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of Reminder attributes.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the 
        /// reminder attributes.
        /// </returns>        
        public List<AttributeDetail> GetReminderAttributes()
        {
            IDataReader dataReader = null;
            List<AttributeDetail> attributesList = null;

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_TEST_REMINDER_TYPES");
                dataReader = HCMDatabase.ExecuteReader(attributeCommand);

                AttributeDetail attributeDetail;
                while (dataReader.Read())
                {
                    // Instantiate the attributes collection.
                    if (attributesList == null)
                        attributesList = new List<AttributeDetail>();

                    // Instantiate the attibute detail.
                    attributeDetail = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                    {
                        attributeDetail.AttributeID =
                            dataReader["ATTRIBUTE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        attributeDetail.AttributeName =
                            dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_VALUE"]))
                        attributeDetail.AttributeValue = dataReader["ATTRIBUTE_VALUE"].ToString().Trim();

                    // Add the attribute to the collection.
                    attributesList.Add(attributeDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return attributesList;
        }

        /// <summary>
        /// Method that retrieves the list of Credit Type attributes.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the 
        /// Credit attributes..
        /// </returns>
        public List<AttributeDetail> GetCreditTypeAttributes()
        {
            IDataReader dataReader = null;
            List<AttributeDetail> attributesList = null;

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_CREDIT_TYPE_ATTRIBUTES");
                dataReader = HCMDatabase.ExecuteReader(attributeCommand);

                AttributeDetail attributeDetail;
                while (dataReader.Read())
                {
                    // Instantiate the attributes collection.
                    if (attributesList == null)
                        attributesList = new List<AttributeDetail>();

                    // Instantiate the attibute detail.
                    attributeDetail = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                    {
                        attributeDetail.AttributeID =
                            dataReader["ATTRIBUTE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        attributeDetail.AttributeName =
                            dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }

                    // Add the attribute to the collection.
                    attributesList.Add(attributeDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return attributesList;
        }

        /// <summary>
        /// Method that retrieves the credit type from the database
        /// </summary>
        /// <returns>
        /// A list of <see cref="AttributeDetail"/> that holds the 
        /// Credit attributes..
        /// </returns>
        public List<AttributeDetail> GetAttributeCreditType()
        {
            IDataReader dataReader = null;
            List<AttributeDetail> attributesList = null;

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_CREDIT_TYPE");
                dataReader = HCMDatabase.ExecuteReader(attributeCommand);

                AttributeDetail attributeDetail;
                while (dataReader.Read())
                {
                    // Instantiate the attributes collection.
                    if (attributesList == null)
                        attributesList = new List<AttributeDetail>();

                    // Instantiate the attibute detail.
                    attributeDetail = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                    {
                        attributeDetail.AttributeID =
                            dataReader["ATTRIBUTE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        attributeDetail.AttributeName =
                            dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }

                    // Add the attribute to the collection.
                    attributesList.Add(attributeDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return attributesList;
        }
        #endregion
    }
}
