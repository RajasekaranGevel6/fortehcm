﻿#region Header                                                                 
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ReportDLManager.cs
// File that represents the data layer for the Test Session module.
// This will talk to the database to perform the operations associated
// with the module.
#endregion

#region Directives                                                             
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the report module.
    /// This includes functionalities for design report , candidate report etc.
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class ReportDLManager : DatabaseConnectionManager
    {
        #region Public Method                                                  
        /// <summary>
        /// this method represent the Test details based on search criteria.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A<see cref="string"/>that holds the test Search Criteria
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="string"/>that holds the page size
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="string"/>that holds the page number
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction Asc/desc
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total Records out parameter.
        /// </param>
        /// <returns>
        /// A list for <see cref="TestDetail"/> that holds the test detail
        /// </returns>
        public List<TestDetail> GetTests(TestSearchCriteria testSearchCriteria,
            int pageSize, int pageNumber, string orderBy,
            SortType orderByDirection, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                string isCertification = null;
                if (testSearchCriteria.IsCertification != null)
                    isCertification = testSearchCriteria.IsCertification == true ? "Y" : "N";

                List<TestDetail> testDetails = null;

                DbCommand getTestDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_TEST_REPORT");

                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_KEY", DbType.String, testSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AUTHOR", DbType.String, testSearchCriteria.TestAuthorName);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AUTHOR_ID", DbType.String, testSearchCriteria.TestAuthorID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CAT_SUB", DbType.String, testSearchCriteria.CategoriesID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CATEGORY", DbType.String, testSearchCriteria.Category);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@SUBJECT", DbType.String, testSearchCriteria.Subject);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AREA", DbType.String, testSearchCriteria.TestAreasID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_NAME", DbType.String, testSearchCriteria.Name);

                if (testSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestDetailCommand, "@COMPLEXITY", DbType.String, testSearchCriteria.Complexity);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CERTIFICATION ", DbType.String, (isCertification));
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBYDIRECTION", DbType.String, orderByDirection == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_COST_START", DbType.Int32, testSearchCriteria.TestCostStart);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_COST_END", DbType.Int32, (testSearchCriteria.TestCostEnd == 0) ? 100 : testSearchCriteria.TestCostEnd);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@SUFFICIENT_CREDIT", DbType.String, testSearchCriteria.isSufficientCredits);

                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        TestDetail testDetail = new TestDetail();
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                        testDetail.Name = dataReader["TEST_NAME"].ToString();
                        testDetail.TestAuthorName = dataReader["USERNAME"].ToString();
                        testDetail.TestAuthorFullName = dataReader["TEST_AUTHOR_FULLNAME"].ToString().Trim();
                        testDetail.NoOfQuestions = int.Parse(dataReader["TOTAL_QUESTION"].ToString());
                        testDetail.CreatedDate = DateTime.Parse(dataReader["CREATED_DATE"].ToString());
                        testDetail.TestCost = decimal.Parse(dataReader["TEST_COST"].ToString());
                        testDetail.IsCertification = (Convert.ToChar(dataReader["CERTIFICATION"]) == 'Y') ? true : false; ;
                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                dataReader.Close();
                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// this method represent the Test details based on search criteria.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A<see cref="string"/>that holds the test Search Criteria
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="string"/>that holds the page size
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="string"/>that holds the page number
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction Asc/desc
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total Records out parameter.
        /// </param>
        /// <returns>
        /// A list for <see cref="TestDetail"/> that holds the test detail
        /// </returns>
        public List<TestDetail> GetTestsByQuestionType(QuestionType questionType, TestSearchCriteria testSearchCriteria,
            int pageSize, int pageNumber, string orderBy,
            SortType orderByDirection, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                string isCertification = null;
                if (testSearchCriteria.IsCertification != null)
                    isCertification = testSearchCriteria.IsCertification == true ? "Y" : "N";

                List<TestDetail> testDetails = null;

                DbCommand getTestDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_TEST_REPORT_WITH_QUESTIONTYPE");

                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_KEY", DbType.String, testSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AUTHOR", DbType.String, testSearchCriteria.TestAuthorName);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AUTHOR_ID", DbType.String, testSearchCriteria.TestAuthorID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CAT_SUB", DbType.String, testSearchCriteria.CategoriesID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CATEGORY", DbType.String, testSearchCriteria.Category);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@SUBJECT", DbType.String, testSearchCriteria.Subject);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AREA", DbType.String, testSearchCriteria.TestAreasID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_NAME", DbType.String, testSearchCriteria.Name);

                if (testSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestDetailCommand, "@COMPLEXITY", DbType.String, testSearchCriteria.Complexity);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CERTIFICATION ", DbType.String, (isCertification));
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBYDIRECTION", DbType.String, orderByDirection == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_COST_START", DbType.Int32, testSearchCriteria.TestCostStart);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_COST_END", DbType.Int32, (testSearchCriteria.TestCostEnd == 0) ? 100 : testSearchCriteria.TestCostEnd);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@SUFFICIENT_CREDIT", DbType.String, testSearchCriteria.isSufficientCredits);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@QUESTION_TYPE", DbType.Int32, questionType);

                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        TestDetail testDetail = new TestDetail();
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                        testDetail.Name = dataReader["TEST_NAME"].ToString();
                        testDetail.TestAuthorName = dataReader["USERNAME"].ToString();
                        testDetail.TestAuthorFullName = dataReader["TEST_AUTHOR_FULLNAME"].ToString().Trim();
                        testDetail.NoOfQuestions = int.Parse(dataReader["TOTAL_QUESTION"].ToString());
                        testDetail.CreatedDate = DateTime.Parse(dataReader["CREATED_DATE"].ToString());
                        testDetail.TestCost = decimal.Parse(dataReader["TEST_COST"].ToString());
                        testDetail.IsCertification = (Convert.ToChar(dataReader["CERTIFICATION"]) == 'Y') ? true : false; ;
                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                dataReader.Close();
                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the question statistics deatils for the question.
        /// In second tab of the test statistics info page
        /// </summary>
        /// <param name="testID">
        /// A<see cref="int"/> that holds the test id 
        /// </param>
        /// <returns>
        /// A list for <see cref="QuestionStatisticsDetail"/>that holds the 
        /// question statistics details 
        /// </returns>
        public List<QuestionStatisticsDetail> GetQuestionStatistics(string testID)
        {
            DbCommand getTestQuestionCategoryCountCommand =
            HCMDatabase.GetStoredProcCommand("SPGET_TEST_QUESTION_STATISTICS");
            IDataReader dataReader = null;
            List<QuestionStatisticsDetail> questionStatisticsDetails = new List<QuestionStatisticsDetail>();
            try
            {
                HCMDatabase.AddInParameter(getTestQuestionCategoryCountCommand,
                    "@TEST_KEY", DbType.String, testID);

                dataReader = HCMDatabase.ExecuteReader(getTestQuestionCategoryCountCommand);
                QuestionStatisticsDetail questionStatisticsDetail;
                while (dataReader.Read())
                {
                    questionStatisticsDetail = new QuestionStatisticsDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"].ToString()))
                    {
                        questionStatisticsDetail.QuestionKey =
                        dataReader["QUESTION_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["GLOBAL_TIME"]))
                    {
                        questionStatisticsDetail.AverageTimeTakenAcrossTest =
                          decimal.Parse(dataReader["GLOBAL_TIME"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECTLY"]))
                    {
                        questionStatisticsDetail.NoOfTimesAnsweredCorrectly =
                            int.Parse(dataReader["ANSWERED_CORRECTLY"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPTED"]))
                    {
                        questionStatisticsDetail.NoOfTimesAdministered =
                             int.Parse(dataReader["ATTEMPTED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["LOCAL_TIME"]))
                    {
                        questionStatisticsDetail.AverageTimeTakenWithinTest
                            = decimal.Parse(dataReader["LOCAL_TIME"].ToString());
                    }
                    questionStatisticsDetail.Ratio =
                    string.Concat(questionStatisticsDetail.
                    NoOfTimesAnsweredCorrectly.ToString(), ":",
                    questionStatisticsDetail.NoOfTimesAdministered.ToString());
                    questionStatisticsDetails.Add(questionStatisticsDetail);
                }
                return questionStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the Question Detail based on the test id and questionId.
        /// </summary>
        /// <param name="testID">
        /// A<see cref="int"/> that holds the test id
        /// </param>
        /// <param name="questionID">
        /// A<see cref="string"/>that holds the question Id
        /// </param>
        /// <returns>
        /// A<see cref="QuestionDetail"/>that holds the Question Detail
        /// </returns>
        public QuestionDetail GetQuestion(string testID, string questionID)
        {
            IDataReader dataReader = null;
            try
            {
                QuestionDetail questionDetail = new QuestionDetail();

                DbCommand getSingleQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_QUESTION_DETAILS");

                // Add parameters.
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@QUESTION_KEY", DbType.String, questionID);

                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@TEST_KEY", DbType.String, testID);

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getSingleQuestionDetailCommand);

                // Retrieve basic question details using SPGET_TEST_QUESTION_DETAILS stored procedure
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                    {
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER"]))
                    {
                        questionDetail.Answer = Convert.ToInt16(dataReader["ANSWER"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        questionDetail.TestAreaName = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY"]))
                    {
                        questionDetail.ComplexityName = dataReader["COMPLEXITY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR"]))
                    {
                        questionDetail.AuthorName = dataReader["AUTHOR"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_FULLNAME"]))
                    {
                        questionDetail.QuestionAuthorFullName = dataReader["AUTHOR_FULLNAME"].ToString().Trim();
                    }

                }
                //Retrieves the answer choices
                dataReader.NextResult();
                questionDetail.AnswerChoices = new List<AnswerChoice>();
                while (dataReader.Read())
                {
                    AnswerChoice answer = new AnswerChoice();
                    if (!Utility.IsNullOrEmpty(dataReader["CHOICE_ID"]))
                    {
                        answer.ChoiceID = int.Parse(dataReader["CHOICE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty("CHOICE_DESC"))
                    {
                        answer.Choice = dataReader["CHOICE_DESC"].ToString();
                    }
                    answer.IsCorrect = answer.ChoiceID == questionDetail.Answer ? true : false;

                    questionDetail.AnswerChoices.Add(answer);
                }

                return questionDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        
       public QuestionDetail GetQuestion(string testID, string questionID,int attemptID,string candidateSessionKey)
        {
            IDataReader dataReader = null;
            try
            {
                QuestionDetail questionDetail = new QuestionDetail();

                DbCommand getSingleQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_CANDIDATE_QUESTION_DETAILS");

                // Add parameters.
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@QUESTION_KEY", DbType.String, questionID);

                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@TEST_KEY", DbType.String, testID);
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@CANDIDATE_SESSION_KEY", DbType.String, candidateSessionKey);

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getSingleQuestionDetailCommand);
                int userAnswerChoice = 0;
                // Retrieve basic question details using SPGET_TEST_QUESTION_DETAILS stored procedure
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                    {
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER"]))
                    {
                        questionDetail.Answer = Convert.ToInt16(dataReader["ANSWER"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        questionDetail.TestAreaName = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY"]))
                    {
                        questionDetail.ComplexityName = dataReader["COMPLEXITY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR"]))
                    {
                        questionDetail.AuthorName = dataReader["AUTHOR"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_FULLNAME"]))
                    {
                        questionDetail.QuestionAuthorFullName = dataReader["AUTHOR_FULLNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CAND_ANSWER_ID"]))
                    {
                        userAnswerChoice = int.Parse(dataReader["CAND_ANSWER_ID"].ToString().Trim());
                    }

                }
                //Retrieves the answer choices
                dataReader.NextResult();
                questionDetail.AnswerChoices = new List<AnswerChoice>();
                while (dataReader.Read())
                {
                    AnswerChoice answer = new AnswerChoice();
                    if (!Utility.IsNullOrEmpty(dataReader["CHOICE_ID"]))
                    {
                        answer.ChoiceID = int.Parse(dataReader["CHOICE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty("CHOICE_DESC"))
                    {
                        answer.Choice = dataReader["CHOICE_DESC"].ToString();
                    }
                    answer.IsCandidateCorrect = userAnswerChoice;
                    answer.IsCorrect = answer.ChoiceID == questionDetail.Answer ? true : false;

                    questionDetail.AnswerChoices.Add(answer);
                }

                return questionDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

       public QuestionDetail GetQuestionOpenText(string testID,string questionID)
       {
           IDataReader dataReader = null;
           try
           {
               QuestionDetail questionDetail = new QuestionDetail();

               DbCommand getSingleQuestionDetailCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_OPENTEST_QUESTION_ANSWERREF");

               // Add parameters.
               HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                   "@QUESTION_KEY", DbType.String, questionID);

               HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                   "@TEST_KEY", DbType.String, testID);

               // Execute command.
               dataReader = HCMDatabase.ExecuteReader(getSingleQuestionDetailCommand);

               QuestionAttribute questionAtrribute = new QuestionAttribute();
               // Retrieve basic question details using SPGET_TEST_QUESTION_DETAILS stored procedure
               while (dataReader.Read())
               {
                   if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                   {
                       questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["ANSWER_REFERENCE"]))
                   {
                       questionAtrribute.AnswerReference = dataReader["ANSWER_REFERENCE"].ToString();
                       questionDetail.QuestionAttribute = questionAtrribute;
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                   {
                       questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                   {
                       questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                   {
                       questionDetail.TestAreaName = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY"]))
                   {
                       questionDetail.ComplexityName = dataReader["COMPLEXITY"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["AUTHOR"]))
                   {
                       questionDetail.AuthorName = dataReader["AUTHOR"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_FULLNAME"]))
                   {
                       questionDetail.QuestionAuthorFullName = dataReader["AUTHOR_FULLNAME"].ToString().Trim();
                   }
               }
               return questionDetail;
           }
           finally
           {
               // Close the data reader, if it is open.
               if (dataReader != null && !dataReader.IsClosed)
               {
                   dataReader.Close();
               }
           }

          
       }

       public QuestionDetail GetQuestionOpenText(string testID, string questionID, int attemptID, string candidateSessionKey)
       {
           IDataReader dataReader = null;
           try
           {
               QuestionDetail questionDetail = new QuestionDetail();

               DbCommand getSingleQuestionDetailCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_TEST_CANDIDATE_QUESTION_DETAILS_OPENTEXT");

               // Add parameters.
               HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                   "@QUESTION_KEY", DbType.String, questionID);

               HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                   "@TEST_KEY", DbType.String, testID);
               HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                   "@ATTEMPT_ID", DbType.Int32, attemptID);
               HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                   "@CANDIDATE_SESSION_KEY", DbType.String, candidateSessionKey);

               // Execute command.
               dataReader = HCMDatabase.ExecuteReader(getSingleQuestionDetailCommand);
               int userAnswerChoice = 0;
               QuestionAttribute questionAttribute = new QuestionAttribute();
               // Retrieve basic question details using SPGET_TEST_QUESTION_DETAILS stored procedure
               while (dataReader.Read())
               {
                   if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                   {
                       questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                   }                   
                   if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                   {
                       questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                   {
                       questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                   {
                       questionDetail.TestAreaName = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY"]))
                   {
                       questionDetail.ComplexityName = dataReader["COMPLEXITY"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["AUTHOR"]))
                   {
                       questionDetail.AuthorName = dataReader["AUTHOR"].ToString().Trim();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_FULLNAME"]))
                   {
                       questionDetail.QuestionAuthorFullName = dataReader["AUTHOR_FULLNAME"].ToString().Trim();
                   }

                   if (!Utility.IsNullOrEmpty(dataReader["ANSWER_REFERENCE"]))
                   {
                       questionAttribute.AnswerReference = dataReader["ANSWER_REFERENCE"].ToString();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["CAND_ANSWER"]))
                   {
                       questionAttribute.Answer = dataReader["CAND_ANSWER"].ToString();
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["MARKS_OBTAINED"]))
                   {
                       questionAttribute.MarksObtained = int.Parse(dataReader["MARKS_OBTAINED"].ToString().Trim());
                   }
                   if (!Utility.IsNullOrEmpty(dataReader["MARKS"]))
                   {
                       questionAttribute.Marks = int.Parse(dataReader["MARKS"].ToString().Trim());
                   }

                   questionDetail.QuestionAttribute = questionAttribute;

               }
              
               return questionDetail;
           }
           finally
           {
               // Close the data reader, if it is open.
               if (dataReader != null && !dataReader.IsClosed)
               {
                   dataReader.Close();
               }
           }
       }

        /// <summary>
        /// Method to get the Candidate Statistics Detail based on the search creteria.
        /// </summary>
        /// <param name="candidateName">
        /// A<see cref="int"/> that holds the candidate name
        /// </param>
        /// <param name="sessionCreatedBy">
        /// A<see cref="int"/> that holds the session creator user id
        /// </param>
        /// <param name="scheduleCreatedBy">
        /// A<see cref="int"/> that holds the scheldule creator user id
        /// </param>
        /// <param name="testDateFrom">
        /// A<see cref="string"/> that holds the test date from
        /// </param>
        /// <param name="testDateTo">
        /// A<see cref="string"/> that holds the test date to
        /// </param>
        /// <param name="absoluteScoreFrom">
        /// A<see cref="string"/> that holds the absolute score from
        /// </param>
        /// <param name="absoluteScoreTo">
        /// A<see cref="string"/> that holds the absolute score to
        /// </param>
        /// <param name="relativeScoreFrom">
        /// A<see cref="string"/> that holds the relative score from
        /// </param>
        /// <param name="relativeScoreTo">
        /// A<see cref="string"/> that holds the relative score to
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/> that holds the test key
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/> that holds the order by
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/> that holds the Order by direction asc/desc 
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/> that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total records
        /// </param>
        /// <returns>
        /// A List for<see cref="CandidateStatisticsDetail"/> 
        /// that holds the Candidate Statistics Detail
        /// </returns>
        public List<CandidateStatisticsDetail> GetReportCandidateStatisticsDetails
            (string candidateName, string sessionCreatedBy, string scheduleCreatedBy,
            string testDateFrom, string testDateTo, string absoluteScoreFrom,
            string absoluteScoreTo, string relativeScoreFrom, string relativeScoreTo,
            string testKey, string orderBy, string orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {

            DbCommand getReportCandidateStatisticsCommand =
                 HCMDatabase.GetStoredProcCommand("SPGET_TEST_CANDIDATE_STATISTICS");

            totalRecords = 0;

            IDataReader dataReader = null;
            try
            {
                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_KEY", DbType.String,
                    testKey);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@CANDIDATE_NAME", DbType.String,
                    candidateName.Length == 0 ? null : candidateName);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SESSION_CREATOR", DbType.String,
                    sessionCreatedBy.Length == 0 ? null : sessionCreatedBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SCHEDULE_CREATOR", DbType.String,
                    scheduleCreatedBy.Length == 0 ? null : scheduleCreatedBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_FROM", DbType.DateTime,
                    testDateFrom.Length == 0 ? Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(testDateFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_TO", DbType.DateTime,
                    testDateTo.Length == 0 ? Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(testDateTo));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_START_VALUE", DbType.Decimal,
                    absoluteScoreFrom.Length == 0 ? 0 : int.Parse(absoluteScoreFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_END_VALUE", DbType.Int32,
                    absoluteScoreTo.Length == 0 || absoluteScoreTo == "0" ? 100 : int.Parse(absoluteScoreTo));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_START_VALUE", DbType.Int32,
                    relativeScoreFrom.Length == 0 ? 0 : int.Parse(relativeScoreFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_END_VALUE", DbType.Decimal,
                    relativeScoreTo.Length == 0 || relativeScoreTo == "0" ? 100 : int.Parse(relativeScoreTo));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBY", DbType.String,
                    orderBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGENUM", DbType.Int32,
                    pageNumber);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGESIZE", DbType.Int32,
                    pageSize);

                dataReader = HCMDatabase.ExecuteReader(getReportCandidateStatisticsCommand);

                List<CandidateStatisticsDetail> candidateStatisticsDetails =
                    new List<CandidateStatisticsDetail>();

                while (dataReader.Read())
                {
                    CandidateStatisticsDetail candidateStatisticsDetail =
                        new CandidateStatisticsDetail();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Candidate Session ID.
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_KEY"]))
                        {
                            candidateStatisticsDetail.CandidateSessionID =
                                dataReader["CANDIDATE_SESSION_KEY"].ToString();
                        }

                        // Cyber Proctoring Status.
                        if (!Utility.IsNullOrEmpty(dataReader["IS_CYBER_PROCTORING"]))
                        {
                            candidateStatisticsDetail.IsCyberProctoring =
                                dataReader["IS_CYBER_PROCTORING"].ToString().Trim().
                                ToUpper() == "Y" ? true : false;
                        }

                        //Get the candidate name
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        {
                            candidateStatisticsDetail.FirstName =
                                dataReader["CANDIDATE_NAME"].ToString();
                        }

                        //Get the candidate full name
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FULL_NAME"]))
                        {
                            candidateStatisticsDetail.CandidateFullName =
                                dataReader["CANDIDATE_FULL_NAME"].ToString();
                        }

                        //Get the candidate Id
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        {
                            candidateStatisticsDetail.CandidateID =
                                dataReader["CANDIDATE_ID"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATOR"]))
                        {
                            candidateStatisticsDetail.SessionCreator =
                                dataReader["SESSION_CREATOR"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATOR_FULLNAME"]))
                        {
                            candidateStatisticsDetail.SessionCreatorFullName =
                                dataReader["SESSION_CREATOR_FULLNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULE_CREATOR"]))
                        {
                            candidateStatisticsDetail.ScheduleCreator =
                                dataReader["SCHEDULE_CREATOR"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULE_CREATOR_FULLNAME"]))
                        {
                            candidateStatisticsDetail.ScheduleCreatorFullName =
                                dataReader["SCHEDULE_CREATOR_FULLNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                        {
                            candidateStatisticsDetail.TestDate =
                           dataReader["DATE_COMPLETED"].ToString().Trim().Length == 0 ? DateTime.MinValue :
                       Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());

                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ATTEMPT_ID"]))
                        {
                            candidateStatisticsDetail.AttemptNumber =
                          int.Parse(dataReader["CANDIDATE_ATTEMPT_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                        {
                            candidateStatisticsDetail.AnsweredCorrectly =
                           Convert.ToDecimal(dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                        {
                            candidateStatisticsDetail.TimeTaken =
                            dataReader["TIME_TAKEN"].ToString().Trim().Length == 0 ? "00:00:00" :
                            Utility.ConvertSecondsToHoursMinutesSeconds(
                            int.Parse(dataReader["TIME_TAKEN"].ToString()));
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                        {
                            decimal avgTimeTaken = Convert.ToDecimal
                            (dataReader["AVERAGE_TIME_TAKEN"].ToString());

                            int avgTimeTakenInt = Convert.ToInt32
                            (avgTimeTaken);

                            candidateStatisticsDetail.AverageTimeTaken =
                            dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim().Length == 0 ? "0" :
                            Utility.ConvertSecondsToHoursMinutesSeconds(avgTimeTakenInt);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE_ANSWER"]))
                        {
                            candidateStatisticsDetail.AbsoluteScore =
                          dataReader["ABSOLUTE_SCORE_ANSWER"].ToString().Trim().Length == 0 ?
                          0 : Convert.ToDecimal(dataReader["ABSOLUTE_SCORE_ANSWER"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                        {

                            candidateStatisticsDetail.RelativeScore =
                                dataReader["RELATIVE_SCORE"].ToString().Trim().Length == 0 ?
                                0 : Convert.ToDecimal(dataReader["RELATIVE_SCORE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                        {
                            candidateStatisticsDetail.Percentile =
                                decimal.Parse(dataReader["PERCENTILE"].ToString());
                        }

                        candidateStatisticsDetail.Recruiter = candidateStatisticsDetail.ScheduleCreator;

                        candidateStatisticsDetails.Add(candidateStatisticsDetail);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }
                return candidateStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the Candidate Statistics Detail based on the search creteria.
        /// </summary>
        /// <param name="testStatisticsSearchCriteria">
        /// A<see cref="TestStatisticsSearchCriteria"/> that holds the test StatisticsSearch Criteria
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/> that holds the order by field 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/> that holds the order by direction asc/desc
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/> that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total records out parameter
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateStatisticsDetail"/> that holds the candidate statistics detail
        /// </returns>
        public List<CandidateStatisticsDetail> GetReportCandidateStatisticsDetails
           (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy, string orderByDirection, int pageNumber,
           int pageSize, out int totalRecords)
        {

            DbCommand getReportCandidateStatisticsCommand =
                 HCMDatabase.GetStoredProcCommand("SPGET_RANKED_TEST_CANDIDATE_STATISTICS");

            totalRecords = 0;

            IDataReader dataReader = null;
            try
            {
                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_KEY", DbType.String,
                    testStatisticsSearchCriteria.TestKey);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@CANDIDATE_NAME", DbType.String,
                    testStatisticsSearchCriteria.CandidateName.Length == 0 ? null : testStatisticsSearchCriteria.CandidateName);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SESSION_CREATOR", DbType.String,
                    testStatisticsSearchCriteria.SessionCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.SessionCreatedBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SCHEDULE_CREATOR", DbType.String,
                    testStatisticsSearchCriteria.ScheduleCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.ScheduleCreatedBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_FROM", DbType.DateTime,
                    testStatisticsSearchCriteria.TestDateFrom.Length == 0 ? Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_TO", DbType.DateTime,
                    testStatisticsSearchCriteria.TestDateTo.Length == 0 ? Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateTo).AddDays(1));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_START_VALUE", DbType.Decimal,
                    testStatisticsSearchCriteria.AbsoluteScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_END_VALUE", DbType.Int32,
                    testStatisticsSearchCriteria.AbsoluteScoreTo.Length == 0 || testStatisticsSearchCriteria.AbsoluteScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreTo));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_START_VALUE", DbType.Int32,
                    testStatisticsSearchCriteria.RelativeScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.RelativeScoreFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_END_VALUE", DbType.Decimal,
                    testStatisticsSearchCriteria.RelativeScoreTo.Length == 0 || testStatisticsSearchCriteria.RelativeScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.RelativeScoreTo));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBY", DbType.String,
                    orderBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGENUM", DbType.Int32,
                    pageNumber);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGESIZE", DbType.Int32,
                    pageSize);

                dataReader = HCMDatabase.ExecuteReader(getReportCandidateStatisticsCommand);

                List<CandidateStatisticsDetail> candidateStatisticsDetails =
                    new List<CandidateStatisticsDetail>();

                while (dataReader.Read())
                {
                    CandidateStatisticsDetail candidateStatisticsDetail =
                        new CandidateStatisticsDetail();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Candidate Session ID.
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_KEY"]))
                        {
                            candidateStatisticsDetail.CandidateSessionID =
                                dataReader["CANDIDATE_SESSION_KEY"].ToString();
                        }

                        // Cyber Proctoring Status.
                        if (!Utility.IsNullOrEmpty(dataReader["IS_CYBER_PROCTORING"]))
                        {
                            candidateStatisticsDetail.IsCyberProctoring =
                                dataReader["IS_CYBER_PROCTORING"].ToString().Trim().
                                ToUpper() == "Y" ? true : false;
                        }

                        //Get the candidate name
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        {
                            candidateStatisticsDetail.FirstName =
                                dataReader["CANDIDATE_NAME"].ToString();
                        }

                        //Get the candidate full name
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FULL_NAME"]))
                        {
                            candidateStatisticsDetail.CandidateFullName =
                                dataReader["CANDIDATE_FULL_NAME"].ToString();
                        }

                        //Get the candidate Id
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        {
                            candidateStatisticsDetail.CandidateID =
                                dataReader["CANDIDATE_ID"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATOR"]))
                        {
                            candidateStatisticsDetail.SessionCreator =
                                dataReader["SESSION_CREATOR"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATOR_FULLNAME"]))
                        {
                            candidateStatisticsDetail.SessionCreatorFullName =
                                dataReader["SESSION_CREATOR_FULLNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULE_CREATOR"]))
                        {
                            candidateStatisticsDetail.ScheduleCreator =
                                dataReader["SCHEDULE_CREATOR"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULE_CREATOR_FULLNAME"]))
                        {
                            candidateStatisticsDetail.ScheduleCreatorFullName =
                                dataReader["SCHEDULE_CREATOR_FULLNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                        {
                            candidateStatisticsDetail.TestDate =
                           dataReader["DATE_COMPLETED"].ToString().Trim().Length == 0 ? DateTime.MinValue :
                       Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());

                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ATTEMPT_ID"]))
                        {
                            candidateStatisticsDetail.AttemptNumber =
                          int.Parse(dataReader["CANDIDATE_ATTEMPT_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                        {
                            candidateStatisticsDetail.AnsweredCorrectly =
                           Convert.ToDecimal(dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RANK_ORDER"]))
                        {
                            candidateStatisticsDetail.RankOrder =
                                Convert.ToInt32(dataReader["RANK_ORDER"].ToString());
                        }
                        
                        if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_RANK_ORDER"]))
                        {
                            candidateStatisticsDetail.RelativeRankOrder =
                                Convert.ToInt32(dataReader["RELATIVE_RANK_ORDER"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                        {
                            candidateStatisticsDetail.TimeTaken =
                            dataReader["TIME_TAKEN"].ToString().Trim().Length == 0 ? "00:00:00" :
                            Utility.ConvertSecondsToHoursMinutesSeconds(
                            int.Parse(dataReader["TIME_TAKEN"].ToString()));
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                        {
                            decimal avgTimeTaken = Convert.ToDecimal
                            (dataReader["AVERAGE_TIME_TAKEN"].ToString());

                            int avgTimeTakenInt = Convert.ToInt32
                            (avgTimeTaken);

                            candidateStatisticsDetail.AverageTimeTaken =
                            dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim().Length == 0 ? "0" :
                            Utility.ConvertSecondsToHoursMinutesSeconds(avgTimeTakenInt);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE_ANSWER"]))
                        {
                            candidateStatisticsDetail.AbsoluteScore =
                          dataReader["ABSOLUTE_SCORE_ANSWER"].ToString().Trim().Length == 0 ?
                          0 : Convert.ToDecimal(dataReader["ABSOLUTE_SCORE_ANSWER"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                        {

                            candidateStatisticsDetail.RelativeScore =
                                dataReader["RELATIVE_SCORE"].ToString().Trim().Length == 0 ?
                                0 : Convert.ToDecimal(dataReader["RELATIVE_SCORE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                        {
                            candidateStatisticsDetail.Percentile =
                                decimal.Parse(dataReader["PERCENTILE"].ToString());
                        }

                        candidateStatisticsDetail.Recruiter = candidateStatisticsDetail.ScheduleCreator;

                        candidateStatisticsDetails.Add(candidateStatisticsDetail);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }

                }

                return candidateStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public List<CandidateStatisticsDetail> GetReportCandidateStatisticsDetailsOpenText
         (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy, string orderByDirection, int pageNumber,
         int pageSize, out int totalRecords)
        {

            DbCommand getReportCandidateStatisticsCommand =
                 HCMDatabase.GetStoredProcCommand("SPGET_RANKED_TEST_OPENTEXT_CANDIDATE_STATISTICS");

            totalRecords = 0;

            IDataReader dataReader = null;
            try
            {
                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_KEY", DbType.String,
                    testStatisticsSearchCriteria.TestKey);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@CANDIDATE_NAME", DbType.String,
                    testStatisticsSearchCriteria.CandidateName.Length == 0 ? null : testStatisticsSearchCriteria.CandidateName);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SESSION_CREATOR", DbType.String,
                    testStatisticsSearchCriteria.SessionCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.SessionCreatedBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SCHEDULE_CREATOR", DbType.String,
                    testStatisticsSearchCriteria.ScheduleCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.ScheduleCreatedBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_FROM", DbType.DateTime,
                    testStatisticsSearchCriteria.TestDateFrom.Length == 0 ? Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_TO", DbType.DateTime,
                    testStatisticsSearchCriteria.TestDateTo.Length == 0 ? Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateTo).AddDays(1));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_START_VALUE", DbType.Decimal,
                    testStatisticsSearchCriteria.AbsoluteScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_END_VALUE", DbType.Int32,
                    testStatisticsSearchCriteria.AbsoluteScoreTo.Length == 0 || testStatisticsSearchCriteria.AbsoluteScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreTo));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_START_VALUE", DbType.Int32,
                    testStatisticsSearchCriteria.RelativeScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.RelativeScoreFrom));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_END_VALUE", DbType.Decimal,
                    testStatisticsSearchCriteria.RelativeScoreTo.Length == 0 || testStatisticsSearchCriteria.RelativeScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.RelativeScoreTo));


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBY", DbType.String,
                    orderBy);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection);


                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGENUM", DbType.Int32,
                    pageNumber);

                HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGESIZE", DbType.Int32,
                    pageSize);

                dataReader = HCMDatabase.ExecuteReader(getReportCandidateStatisticsCommand);

                List<CandidateStatisticsDetail> candidateStatisticsDetails =
                    new List<CandidateStatisticsDetail>();

                while (dataReader.Read())
                {
                    CandidateStatisticsDetail candidateStatisticsDetail =
                        new CandidateStatisticsDetail();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Candidate Session ID.
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_KEY"]))
                        {
                            candidateStatisticsDetail.CandidateSessionID =
                                dataReader["CANDIDATE_SESSION_KEY"].ToString();
                        }

                        // Cyber Proctoring Status.
                        if (!Utility.IsNullOrEmpty(dataReader["IS_CYBER_PROCTORING"]))
                        {
                            candidateStatisticsDetail.IsCyberProctoring =
                                dataReader["IS_CYBER_PROCTORING"].ToString().Trim().
                                ToUpper() == "Y" ? true : false;
                        }

                        //Get the candidate name
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        {
                            candidateStatisticsDetail.FirstName =
                                dataReader["CANDIDATE_NAME"].ToString();
                        }

                        //Get the candidate full name
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FULL_NAME"]))
                        {
                            candidateStatisticsDetail.CandidateFullName =
                                dataReader["CANDIDATE_FULL_NAME"].ToString();
                        }

                        //Get the candidate Id
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        {
                            candidateStatisticsDetail.CandidateID =
                                dataReader["CANDIDATE_ID"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATOR"]))
                        {
                            candidateStatisticsDetail.SessionCreator =
                                dataReader["SESSION_CREATOR"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATOR_FULLNAME"]))
                        {
                            candidateStatisticsDetail.SessionCreatorFullName =
                                dataReader["SESSION_CREATOR_FULLNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULE_CREATOR"]))
                        {
                            candidateStatisticsDetail.ScheduleCreator =
                                dataReader["SCHEDULE_CREATOR"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULE_CREATOR_FULLNAME"]))
                        {
                            candidateStatisticsDetail.ScheduleCreatorFullName =
                                dataReader["SCHEDULE_CREATOR_FULLNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                        {
                            candidateStatisticsDetail.TestDate =
                           dataReader["DATE_COMPLETED"].ToString().Trim().Length == 0 ? DateTime.MinValue :
                       Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());

                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ATTEMPT_ID"]))
                        {
                            candidateStatisticsDetail.AttemptNumber =
                          int.Parse(dataReader["CANDIDATE_ATTEMPT_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                        {
                            candidateStatisticsDetail.AnsweredCorrectly =
                           Convert.ToDecimal(dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RANK_ORDER"]))
                        {
                            candidateStatisticsDetail.RankOrder =
                                Convert.ToInt32(dataReader["RANK_ORDER"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_RANK_ORDER"]))
                        {
                            candidateStatisticsDetail.RelativeRankOrder =
                                Convert.ToInt32(dataReader["RELATIVE_RANK_ORDER"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                        {
                            candidateStatisticsDetail.TimeTaken =
                            dataReader["TIME_TAKEN"].ToString().Trim().Length == 0 ? "00:00:00" :
                            Utility.ConvertSecondsToHoursMinutesSeconds(
                            int.Parse(dataReader["TIME_TAKEN"].ToString()));
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                        {
                            decimal avgTimeTaken = Convert.ToDecimal
                            (dataReader["AVERAGE_TIME_TAKEN"].ToString());

                            int avgTimeTakenInt = Convert.ToInt32
                            (avgTimeTaken);

                            candidateStatisticsDetail.AverageTimeTaken =
                            dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim().Length == 0 ? "0" :
                            Utility.ConvertSecondsToHoursMinutesSeconds(avgTimeTakenInt);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE_ANSWER"]))
                        {
                            candidateStatisticsDetail.AbsoluteScore =
                          dataReader["ABSOLUTE_SCORE_ANSWER"].ToString().Trim().Length == 0 ?
                          0 : Convert.ToDecimal(dataReader["ABSOLUTE_SCORE_ANSWER"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                        {

                            candidateStatisticsDetail.RelativeScore =
                                dataReader["RELATIVE_SCORE"].ToString().Trim().Length == 0 ?
                                0 : Convert.ToDecimal(dataReader["RELATIVE_SCORE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                        {
                            candidateStatisticsDetail.Percentile =
                                decimal.Parse(dataReader["PERCENTILE"].ToString());
                        }

                        candidateStatisticsDetail.Recruiter = candidateStatisticsDetail.ScheduleCreator;

                        candidateStatisticsDetails.Add(candidateStatisticsDetail);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }

                }

                return candidateStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// Method to get the Candidate Statistics Detail based on the search creteria.
        /// </summary>
        /// <param name="testStatisticsSearchCriteria">
        /// A<see cref="TestStatisticsSearchCriteria"/> that holds the test StatisticsSearch Criteria
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/> that holds the order by field 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/> that holds the order by direction asc/desc
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/> that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total records out parameter
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateStatisticsDetail"/> that holds the candidate statistics detail
        /// </returns>
        public DataTable GetReportCandidateStatisticsDetailsTable
           (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy, string orderByDirection, int pageNumber,
           int pageSize, out int totalRecords)
        {

            DbCommand getReportCandidateStatisticsCommand =
                 HCMDatabase.GetStoredProcCommand("SPGET_RANKED_TEST_CANDIDATE_STATISTICS");

            totalRecords = 0;

            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_KEY", DbType.String,
                testStatisticsSearchCriteria.TestKey);

            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@CANDIDATE_NAME", DbType.String,
                testStatisticsSearchCriteria.CandidateName.Length == 0 ? null : testStatisticsSearchCriteria.CandidateName);

            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SESSION_CREATOR", DbType.String,
                testStatisticsSearchCriteria.SessionCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.SessionCreatedBy);


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SCHEDULE_CREATOR", DbType.String,
                testStatisticsSearchCriteria.ScheduleCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.ScheduleCreatedBy);


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_FROM", DbType.DateTime,
                testStatisticsSearchCriteria.TestDateFrom.Length == 0 ? Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateFrom));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_TO", DbType.DateTime,
                testStatisticsSearchCriteria.TestDateTo.Length == 0 ? Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateTo).AddDays(1));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_START_VALUE", DbType.Decimal,
                testStatisticsSearchCriteria.AbsoluteScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreFrom));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_END_VALUE", DbType.Int32,
                testStatisticsSearchCriteria.AbsoluteScoreTo.Length == 0 || testStatisticsSearchCriteria.AbsoluteScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreTo));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_START_VALUE", DbType.Int32,
                testStatisticsSearchCriteria.RelativeScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.RelativeScoreFrom));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_END_VALUE", DbType.Decimal,
                testStatisticsSearchCriteria.RelativeScoreTo.Length == 0 || testStatisticsSearchCriteria.RelativeScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.RelativeScoreTo));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBY", DbType.String,
                orderBy);


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBYDIRECTION",
                DbType.String, orderByDirection);


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGENUM", DbType.Int32,
                pageNumber);

            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGESIZE", DbType.Int32,
                pageSize);

            DataSet dataSet = HCMDatabase.ExecuteDataSet(getReportCandidateStatisticsCommand);

            if (dataSet == null || dataSet.Tables == null ||
                dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
            {
                return null;
            }
            return dataSet.Tables[0];
        }

        public DataTable GetReportCandidateStatisticsDetailsOpenTextTable
          (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy, string orderByDirection, int pageNumber,
          int pageSize, out int totalRecords)
        {

            DbCommand getReportCandidateStatisticsCommand =
                 HCMDatabase.GetStoredProcCommand("SPGET_RANKED_TEST_OPENTEXT_CANDIDATE_STATISTICS");

            totalRecords = 0;

            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_KEY", DbType.String,
                testStatisticsSearchCriteria.TestKey);

            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@CANDIDATE_NAME", DbType.String,
                testStatisticsSearchCriteria.CandidateName.Length == 0 ? null : testStatisticsSearchCriteria.CandidateName);

            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SESSION_CREATOR", DbType.String,
                testStatisticsSearchCriteria.SessionCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.SessionCreatedBy);


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_SCHEDULE_CREATOR", DbType.String,
                testStatisticsSearchCriteria.ScheduleCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.ScheduleCreatedBy);


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_FROM", DbType.DateTime,
                testStatisticsSearchCriteria.TestDateFrom.Length == 0 ? Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateFrom));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@TEST_DATE_TO", DbType.DateTime,
                testStatisticsSearchCriteria.TestDateTo.Length == 0 ? Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateTo).AddDays(1));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_START_VALUE", DbType.Decimal,
                testStatisticsSearchCriteria.AbsoluteScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreFrom));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_END_VALUE", DbType.Int32,
                testStatisticsSearchCriteria.AbsoluteScoreTo.Length == 0 || testStatisticsSearchCriteria.AbsoluteScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreTo));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_START_VALUE", DbType.Int32,
                testStatisticsSearchCriteria.RelativeScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.RelativeScoreFrom));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_END_VALUE", DbType.Decimal,
                testStatisticsSearchCriteria.RelativeScoreTo.Length == 0 || testStatisticsSearchCriteria.RelativeScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.RelativeScoreTo));


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBY", DbType.String,
                orderBy);


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ORDERBYDIRECTION",
                DbType.String, orderByDirection);


            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGENUM", DbType.Int32,
                pageNumber);

            HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@PAGESIZE", DbType.Int32,
                pageSize);

            DataSet dataSet = HCMDatabase.ExecuteDataSet(getReportCandidateStatisticsCommand);

            if (dataSet == null || dataSet.Tables == null ||
                dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
            {
                return null;
            }
            return dataSet.Tables[0];
        }

        /// <summary>
        /// Method to get the test statistics details for a single test.
        /// For the first tab in the test statistics info page
        /// </summary>
        /// <param name="testID">
        /// A<see cref="string"/>that holds the testid
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the 
        /// test statistics inforamtions
        /// </returns>
        public TestStatistics GetTestStatisticsDetails(string testID)
        {
            TestStatistics testStatistics = new TestStatistics();

            IDataReader dataReader = null;

            try
            {
                DbCommand getTestStatisticsDetail = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_STATISTICS_INFORMATION");

                HCMDatabase.AddInParameter(getTestStatisticsDetail,
                    "@TEST_KEY", DbType.String, testID);

                dataReader = HCMDatabase.ExecuteReader(getTestStatisticsDetail);

                //To get the oveerall deatils of the test such as 
                //test name, author name, total question, maximum score,
                //minimum score
                if (dataReader.Read())
                {
                    testStatistics.TestID = testID;

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        testStatistics.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                    {
                        testStatistics.TestAuthor = dataReader["USRFIRSTNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AUTHOR_FULLNAME"]))
                    {
                        testStatistics.TestAuthorFullName =
                            dataReader["TEST_AUTHOR_FULLNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        testStatistics.NoOfQuestions = int.Parse(dataReader["TOTAL_QUESTION"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        testStatistics.AverageTimeTakenByCandidates = Convert.ToDecimal
                       (dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        testStatistics.HighestScore = Convert.ToDecimal(dataReader["MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        testStatistics.LowestScore = Convert.ToDecimal(dataReader["MINIMUM_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MEAN_SCORE"]))
                    {
                        testStatistics.MeanScore = Convert.ToDecimal(dataReader["MEAN_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STANDARD_DEVIATION"]))
                    {
                        testStatistics.StandardDeviation = Convert.ToDecimal(dataReader["STANDARD_DEVIATION"].ToString().Trim());
                    }
                }

                dataReader.NextResult();

                if (dataReader.Read())
                {
                    testStatistics.NoOfCandidates = int.Parse(dataReader["CANDIDATE_COUNT"].ToString());

                }

                //Read next result to take the subject and category statistics
                dataReader.NextResult();

                testStatistics.SubjectStatistics = new List<Subject>();

                while (dataReader.Read())
                {
                    Subject subject = new Subject();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        subject.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        subject.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    testStatistics.SubjectStatistics.Add(subject);
                }

                dataReader.NextResult();

                testStatistics.CategoryStatisticsChartData = new SingleChartData();

                testStatistics.CategoryStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the category statistics
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["CATEGORY_COUNT"].ToString().Trim());
                    }

                    testStatistics.CategoryStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.SubjectStatisticsChartData = new SingleChartData();

                testStatistics.SubjectStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the subject statistics
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["SUBJECT_COUNT"].ToString().Trim());
                    }

                    testStatistics.SubjectStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.TestAreaStatisticsChartData = new SingleChartData();

                testStatistics.TestAreaStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the test area count
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TESTAREA_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["TESTAREA_COUNT"].ToString().Trim());
                    }

                    testStatistics.TestAreaStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.ComplexityStatisticsChartData = new SingleChartData();

                testStatistics.ComplexityStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the complexity count
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["COMPLEXITY_COUNT"].ToString().Trim());
                    }

                    testStatistics.ComplexityStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.Close();

                return testStatistics;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Method to get the test statistics details of the test
        /// fisrt control in first tab of the test statiscs info page
        /// </summary>
        /// <param name="testID"></param>
        /// <returns></returns>
        public TestStatistics GetTestSummaryDetails(string testID)
        {
            TestStatistics testStatistics = new TestStatistics();
            IDataReader dataReader = null;
            try
            {
                DbCommand getTestDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_GENERAL_STATISTICS_TEST_SUMMARY");

                HCMDatabase.AddInParameter(getTestDetailCommand,
                    "@TEST_KEY", DbType.String, testID);

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                // Retrieve basic question details using SPGET_QUESTION_HEADER stored procedure
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["USRUSERNAME"]))
                    {
                        testStatistics.TestAuthor = dataReader["USRUSERNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        testStatistics.NoOfQuestions =
                            int.Parse(dataReader["TOTAL_QUESTION"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        testStatistics.AverageTimeTakenByCandidates = Convert.ToDecimal
                      (dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        testStatistics.HighestScore =
                            Convert.ToDecimal(dataReader["MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        testStatistics.LowestScore =
                            Convert.ToDecimal(dataReader["MINIMUM_SCORE"].ToString().Trim());
                    }
                }

                dataReader.NextResult();

                while (dataReader.Read())
                {
                    testStatistics.NoOfCandidates = int.Parse(dataReader["CANDIDATE_COUNT"].ToString());

                }

                return testStatistics;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Represents the method to get the additional test details from DB
        /// </summary>
        /// <param name="testID">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the test statistics 
        /// information
        /// </returns>
        public TestStatistics GetAdditionalTestDetails(string testID)
        {
            TestStatistics testStatistics = null;
            IDataReader dataReader = null;
            try
            {
                DbCommand getAdditionalTestDetails = HCMDatabase.
                    GetStoredProcCommand("SPGET_ADDITIONAL_TEST_STATISTICS");

                HCMDatabase.AddInParameter(getAdditionalTestDetails,
                    "@TEST_Key", DbType.String, testID);

                dataReader = HCMDatabase.ExecuteReader(getAdditionalTestDetails);

                if (dataReader.Read())
                {
                    testStatistics = new TestStatistics();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        testStatistics.NoOfQuestions =
                            int.Parse(dataReader["TOTAL_QUESTION"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        testStatistics.AverageTimeTakenByCandidates =
                            Convert.ToDecimal(dataReader["AVERAGE_TIME_TAKEN"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        testStatistics.LowestScore =
                            Convert.ToDecimal(dataReader["MINIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        testStatistics.HighestScore =
                            Convert.ToDecimal(dataReader["MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                    {
                        testStatistics.AverageScore =
                            Convert.ToDecimal(dataReader["AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_COST"].ToString()))
                    {
                        testStatistics.TestCost =
                            Convert.ToDecimal(dataReader["TEST_COST"].ToString());
                    }
                }
                return testStatistics;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents a method to get a name of the test
        /// when test id is passed
        /// </summary>
        /// <param name="testID">
        /// A<see cref="string"/>that holds the testid
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the test name
        /// </returns>
        public string GetTestName(string testID)
        {
            IDataReader dataReader = null;
            string testName = null;
            try
            {
                //Assign the stored procedure
                DbCommand getTestNameCommmand = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_NAME");

                //Add parameter
                HCMDatabase.AddInParameter(getTestNameCommmand,
                    "@TEST_KEY", DbType.String, testID);

                dataReader = HCMDatabase.ExecuteReader(getTestNameCommmand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["Test_Name"]))
                    {
                        //Get the test name from DB
                        testName = dataReader["Test_Name"].ToString();
                    }
                }
                //Return the testname
                return testName;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Represents a method to get a completed date of the test
        /// when candidate session and sessionid  passed
        /// </summary>
        /// <param name="candidateSessionId">
        /// A<see cref="string"/>that holds the candidateSessionId
        /// </param>
        /// <param name="attemptId">
        /// A<see cref="string"/>that holds the attemptId
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the completed date
        /// </returns>
        public DateTime GetTestCompletedDateByCandidate(string candidateSessionId, int attemptId)
        {
            IDataReader dataReader = null;
            DateTime testCompletedDate = DateTime.Now;
            try
            {
                //Assign the stored procedure
                DbCommand getTestCompletedCommmand = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_COMPLETED_DATE_BY_CANDIDATE");

                //Add parameter
                HCMDatabase.AddInParameter(getTestCompletedCommmand,
                    "@CAND_SESSION_KEY", DbType.String, candidateSessionId);

                HCMDatabase.AddInParameter(getTestCompletedCommmand,
                    "@ATTEMPT_ID", DbType.Int32, attemptId);


                dataReader = HCMDatabase.ExecuteReader(getTestCompletedCommmand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                    {
                        testCompletedDate =
                          dataReader["DATE_COMPLETED"].ToString().Trim().Length == 0 ? DateTime.MinValue :
                      Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                    }
                }
                //Return the testname
                return testCompletedDate;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents a method to get a completed date of the interview test
        /// when candidate session and sessionid  passed
        /// </summary>
        /// <param name="candidateSessionId">
        /// A<see cref="string"/>that holds the candidateSessionId
        /// </param>
        /// <param name="attemptId">
        /// A<see cref="string"/>that holds the attemptId
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the completed date
        /// </returns>
        public DateTime GetInterviewTestCompletedDateByCandidate(string candidateSessionId, int attemptId)
        {
            IDataReader dataReader = null;
            DateTime testCompletedDate = DateTime.Now;
            try
            {
                //Assign the stored procedure
                DbCommand getTestCompletedCommmand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_TEST_COMPLETED_DATE_BY_CANDIDATE");

                //Add parameter
                HCMDatabase.AddInParameter(getTestCompletedCommmand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionId);

                HCMDatabase.AddInParameter(getTestCompletedCommmand,
                    "@ATTEMPT_ID", DbType.Int32, attemptId);


                dataReader = HCMDatabase.ExecuteReader(getTestCompletedCommmand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                    {
                        testCompletedDate =
                          dataReader["DATE_COMPLETED"].ToString().Trim().Length == 0 ? DateTime.MinValue :
                      Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                    }
                }
                //Return the testname
                return testCompletedDate;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// Represents the method to get the candidate 
        /// test result statistics details from the database
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns></returns>
        public CandidateStatisticsDetail GetCandidateTestResultStatisticsDetails
            (string sessionKey, string testKey, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_TEST_RESULT_STATISTICS");

                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@CANDIDATE_SESSION_ID", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateStatisticsDetailsCommand);

                CandidateStatisticsDetail candidateDetails = new CandidateStatisticsDetail();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        candidateDetails.TotalQuestion = int.Parse
                            (dataReader["TOTAL_QUESTION"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_ANSWERED_CORRECTLY"]))
                    {
                        candidateDetails.QuestionsAnsweredCorrectly = int.Parse
                            (dataReader["TOTAL_ANSWERED_CORRECTLY"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_ANSWERED_INCORRECT"]))
                    {
                        candidateDetails.QuestionsAnsweredWrongly = int.Parse
                            (dataReader["TOTAL_ANSWERED_INCORRECT"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                    {
                        candidateDetails.AnsweredCorrectly = int.Parse
                            (dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                    {
                        candidateDetails.TimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds
                            (int.Parse(dataReader["TIME_TAKEN"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        candidateDetails.AverageTimeTaken =
                           dataReader["AVERAGE_TIME_TAKEN"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE_ANSWER"]))
                    {
                        candidateDetails.AbsoluteScore =
                           decimal.Parse(dataReader["ABSOLUTE_SCORE_ANSWER"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                    {
                        candidateDetails.RelativeScore =
                            decimal.Parse(dataReader["RELATIVE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MY_SCORE"]))
                    {
                        candidateDetails.MyScore =
                            decimal.Parse(dataReader["MY_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                    {
                        candidateDetails.Percentile =
                            decimal.Parse(dataReader["PERCENTILE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_STATUS"]))
                    {
                        candidateDetails.TestStatus =
                            dataReader["TEST_STATUS"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        candidateDetails.TestName =
                            dataReader["TEST_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        candidateDetails.CandidateFullName =
                            dataReader["CANDIDATE_NAME"].ToString();
                    }
                    
                }

                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["SKIPPED"]))
                    {
                        candidateDetails.TotalQuestionSkipped =
                            int.Parse(dataReader["SKIPPED"].ToString());
                    }

                }

                //Calculate the total question attended by finding difference 
                //between total questions and skipped
                //candidateDetails.TotalQuestionAttended =
                //    candidateDetails.QuestionsAnsweredCorrectly + candidateDetails.QuestionsAnsweredWrongly+ candidateDetails.TotalQuestionSkipped;
                //candidateDetails.TotalQuestionSkipped = candidateDetails.TotalQuestion - candidateDetails.TotalQuestionAttended;
                
                //updated by brindha 
                candidateDetails.TotalQuestionAttended = candidateDetails.QuestionsAnsweredCorrectly + candidateDetails.QuestionsAnsweredWrongly;
                   //candidateDetails.TotalQuestion - candidateDetails.TotalQuestionSkipped;                

                return candidateDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }


        public CandidateStatisticsDetail GetCandidateOpenTextTestResultStatisticsDetails
           (string sessionKey, string testKey, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_OPENTEXT_TEST_RESULT_STATISTICS");

                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@CANDIDATE_SESSION_ID", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateStatisticsDetailsCommand);

                CandidateStatisticsDetail candidateDetails = new CandidateStatisticsDetail();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        candidateDetails.TotalQuestion = int.Parse
                            (dataReader["TOTAL_QUESTION"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_ANSWERED_CORRECTLY"]))
                    {
                        candidateDetails.QuestionsAnsweredCorrectly = int.Parse
                            (dataReader["TOTAL_ANSWERED_CORRECTLY"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_ANSWERED_INCORRECT"]))
                    {
                        candidateDetails.QuestionsAnsweredWrongly = int.Parse
                            (dataReader["TOTAL_ANSWERED_INCORRECT"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                    {
                        candidateDetails.AnsweredCorrectly = int.Parse
                            (dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                    {
                        candidateDetails.TimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds
                            (int.Parse(dataReader["TIME_TAKEN"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        candidateDetails.AverageTimeTaken =
                           dataReader["AVERAGE_TIME_TAKEN"].ToString();
                    }                    
                    if (!Utility.IsNullOrEmpty(dataReader["MARKS_OBTAINED"]))
                    {
                        candidateDetails.MarksObtained =
                            decimal.Parse(dataReader["MARKS_OBTAINED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MARKS"]))
                    {
                        candidateDetails.Marks =
                            decimal.Parse(dataReader["MARKS"].ToString());
                    }                    
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_STATUS"]))
                    {
                        candidateDetails.TestStatus =
                            dataReader["TEST_STATUS"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        candidateDetails.TestName =
                            dataReader["TEST_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        candidateDetails.CandidateFullName =
                            dataReader["CANDIDATE_NAME"].ToString();
                    }                    
                }

                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["SKIPPED"]))
                    {
                        candidateDetails.TotalQuestionSkipped =
                            int.Parse(dataReader["SKIPPED"].ToString());
                    }
                }

                candidateDetails.TotalQuestionAttended = candidateDetails.QuestionsAnsweredCorrectly + candidateDetails.QuestionsAnsweredWrongly;
                //candidateDetails.TotalQuestion - candidateDetails.TotalQuestionSkipped;                
                return candidateDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        ///<summary>
        /// Represents the method to get the candidate test result categories 
        /// chart details from Database
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </returns>
        public MultipleSeriesChartData GetCandidateStatisticsCategoriesChartDetails
            (string sessionKey, string testKey, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateCategoryStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_TEST_RESULT_CATEGORY_STATISTICS");

                HCMDatabase.AddInParameter(getCandidateCategoryStatisticsDetailsCommand,
                    "@CANDIDATE_SESSION_KEY", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCandidateCategoryStatisticsDetailsCommand,
                    "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getCandidateCategoryStatisticsDetailsCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateCategoryStatisticsDetailsCommand);

                MultipleSeriesChartData multipleChartDataSource = new MultipleSeriesChartData();

                multipleChartDataSource.MultipleSeriesChartDataSource = new List<MultipleChartData>();

                multipleChartDataSource.MultipleSeriesRelativeChartDataSource = new List<MultipleChartData>();

                MultipleChartData chartData = null;

                MultipleChartData relativeChartData = null;

                while (dataReader.Read())
                {
                    chartData = new MultipleChartData();

                    relativeChartData = new MultipleChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["CAT_KEY"]))
                    {
                        chartData.ID = dataReader["CAT_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        chartData.Name = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SHORT_NAME"]))
                    {
                        chartData.ShortName = dataReader["SHORT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        chartData.MaxScore = decimal.Parse
                            (dataReader["MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        chartData.MinScore = decimal.Parse
                            (dataReader["MINIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                    {
                        chartData.AvgScore = decimal.Parse
                            (dataReader["AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MY_SCORE"]))
                    {
                        chartData.MyScore = decimal.Parse
                            (dataReader["MY_SCORE"].ToString());
                    }

                    multipleChartDataSource.MultipleSeriesChartDataSource.Add(chartData);

                    //Add relative score details

                    if (!Utility.IsNullOrEmpty(dataReader["CAT_KEY"]))
                    {
                        relativeChartData.ID = dataReader["CAT_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        relativeChartData.Name = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SHORT_NAME"]))
                    {
                        relativeChartData.ShortName = dataReader["SHORT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MAXIMUM_SCORE"]))
                    {
                        relativeChartData.MaxScore = decimal.Parse
                            (dataReader["RELATIVE_MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MINIMUM_AVERAGE"]))
                    {
                        relativeChartData.MinScore = decimal.Parse
                            (dataReader["RELATIVE_MINIMUM_AVERAGE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_AVERAGE_SCORE"]))
                    {
                        relativeChartData.AvgScore = decimal.Parse
                            (dataReader["RELATIVE_AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MY_SCORE"]))
                    {
                        relativeChartData.MyScore = decimal.Parse
                            (dataReader["RELATIVE_MY_SCORE"].ToString());
                    }

                    multipleChartDataSource.MultipleSeriesRelativeChartDataSource.Add(relativeChartData);
                }
                return multipleChartDataSource;

            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }
        ///<summary>
        /// Represents the method to get the candidate test result subjects 
        /// chart details from the Database
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </returns>
        public MultipleSeriesChartData GetCandidateStatisticsSubjectsChartDetails(string sessionKey, string testKey, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateSubjectStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_TEST_RESULT_SUBJECT_STATISTICS");

                HCMDatabase.AddInParameter(getCandidateSubjectStatisticsDetailsCommand,
                    "@CANDIDATE_SESSION_KEY", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCandidateSubjectStatisticsDetailsCommand,
                    "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getCandidateSubjectStatisticsDetailsCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateSubjectStatisticsDetailsCommand);

                MultipleSeriesChartData multipleChartDataSource = new MultipleSeriesChartData();

                multipleChartDataSource.MultipleSeriesChartDataSource = new List<MultipleChartData>();

                multipleChartDataSource.MultipleSeriesRelativeChartDataSource = new List<MultipleChartData>();

                MultipleChartData chartData = null;

                MultipleChartData relativeChartData = null;

                while (dataReader.Read())
                {
                    chartData = new MultipleChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        chartData.Name = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_ID"]))
                    {
                        chartData.ID = dataReader["SUBJECT_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SHORT_NAME"]))
                    {
                        chartData.ShortName = dataReader["SHORT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        chartData.MaxScore = decimal.Parse
                            (dataReader["MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        chartData.MinScore = decimal.Parse
                            (dataReader["MINIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                    {
                        chartData.AvgScore = decimal.Parse
                            (dataReader["AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MY_SCORE"]))
                    {
                        chartData.MyScore = decimal.Parse
                            (dataReader["MY_SCORE"].ToString());
                    }

                    multipleChartDataSource.MultipleSeriesChartDataSource.Add(chartData);

                    //Add details for the relative scores 

                    relativeChartData = new MultipleChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        relativeChartData.Name = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_ID"]))
                    {
                        relativeChartData.ID = dataReader["SUBJECT_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SHORT_NAME"]))
                    {
                        relativeChartData.ShortName = dataReader["SHORT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MAXIMUM_SCORE"]))
                    {
                        relativeChartData.MaxScore = decimal.Parse
                            (dataReader["RELATIVE_MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MINIMUM_AVERAGE"]))
                    {
                        relativeChartData.MinScore = decimal.Parse
                            (dataReader["RELATIVE_MINIMUM_AVERAGE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_AVERAGE_SCORE"]))
                    {
                        relativeChartData.AvgScore = decimal.Parse
                            (dataReader["RELATIVE_AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MY_SCORE"]))
                    {
                        relativeChartData.MyScore = decimal.Parse
                            (dataReader["RELATIVE_MY_SCORE"].ToString());
                    }

                    multipleChartDataSource.MultipleSeriesRelativeChartDataSource.Add(relativeChartData);

                }
                return multipleChartDataSource;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        ///<summary>
        /// Represents the method to get the candidate test result test area 
        /// chart details from the database
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </returns>
        public MultipleSeriesChartData GetCandidateStatisticsTestAreaChartDetails
            (string sessionKey, string testKey, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateTestAreaStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_TEST_RESULT_TESTAREA_STATISTICS");

                HCMDatabase.AddInParameter(getCandidateTestAreaStatisticsDetailsCommand,
                    "@CANDIDATE_SESSION_KEY", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCandidateTestAreaStatisticsDetailsCommand,
                    "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getCandidateTestAreaStatisticsDetailsCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateTestAreaStatisticsDetailsCommand);

                MultipleSeriesChartData multipleChartDataSource = new MultipleSeriesChartData();

                multipleChartDataSource.MultipleSeriesChartDataSource = new List<MultipleChartData>();

                multipleChartDataSource.MultipleSeriesRelativeChartDataSource = new List<MultipleChartData>();
                MultipleChartData chartData = null;

                MultipleChartData relativeChartData = null;

                while (dataReader.Read())
                {
                    chartData = new MultipleChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                    {
                        chartData.Name = dataReader["TEST_AREA_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                    {
                        chartData.ID = dataReader["TEST_AREA_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SHORT_NAME"]))
                    {
                        chartData.ShortName = dataReader["SHORT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        chartData.MaxScore = decimal.Parse
                            (dataReader["MAXIMUM_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        chartData.MinScore = decimal.Parse
                            (dataReader["MINIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                    {
                        chartData.AvgScore = decimal.Parse
                            (dataReader["AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MY_SCORE"]))
                    {
                        chartData.MyScore = decimal.Parse
                            (dataReader["MY_SCORE"].ToString());
                    }

                    multipleChartDataSource.MultipleSeriesChartDataSource.Add(chartData);

                    //Add data for relative score
                    relativeChartData = new MultipleChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                    {
                        relativeChartData.Name = dataReader["TEST_AREA_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                    {
                        relativeChartData.ID = dataReader["TEST_AREA_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SHORT_NAME"]))
                    {
                        relativeChartData.ShortName = dataReader["SHORT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MAXIMUM_SCORE"]))
                    {
                        relativeChartData.MaxScore = decimal.Parse
                            (dataReader["RELATIVE_MAXIMUM_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MINIMUM_AVERAGE"]))
                    {
                        relativeChartData.MinScore = decimal.Parse
                            (dataReader["RELATIVE_MINIMUM_AVERAGE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_AVERAGE_SCORE"]))
                    {
                        relativeChartData.AvgScore = decimal.Parse
                            (dataReader["RELATIVE_AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MY_SCORE"]))
                    {
                        relativeChartData.MyScore = decimal.Parse
                            (dataReader["RELATIVE_MY_SCORE"].ToString());
                    }

                    multipleChartDataSource.MultipleSeriesRelativeChartDataSource.Add(relativeChartData);

                }

                return multipleChartDataSource;


            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }


        ///<summary>
        /// Represents the method to get the candidate test result complexity
        /// chart details from the database
        /// </summary>
        /// <param name="sessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <param name="attemptID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <returns>
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </returns>
        public MultipleSeriesChartData GetCandidateStatisticsComplexityChartDetails
            (string sessionKey, string testKey, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateTestAreaStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_TEST_RESULT_COMPLEXITY_STATISTICS");

                HCMDatabase.AddInParameter(getCandidateTestAreaStatisticsDetailsCommand,
                    "@CANDIDATE_SESSION_KEY", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCandidateTestAreaStatisticsDetailsCommand,
                    "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getCandidateTestAreaStatisticsDetailsCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateTestAreaStatisticsDetailsCommand);

                MultipleSeriesChartData multipleChartDataSource = new MultipleSeriesChartData();

                multipleChartDataSource.MultipleSeriesChartDataSource = new List<MultipleChartData>();

                multipleChartDataSource.MultipleSeriesRelativeChartDataSource = new List<MultipleChartData>();
                MultipleChartData chartData = null;

                MultipleChartData relativeChartData = null;

                while (dataReader.Read())
                {
                    chartData = new MultipleChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                    {
                        chartData.Name = dataReader["COMPLEXITY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                    {
                        chartData.ID = dataReader["COMPLEXITY_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SHORT_NAME"]))
                    {
                        chartData.ShortName = dataReader["SHORT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        chartData.MaxScore = decimal.Parse
                            (dataReader["MAXIMUM_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        chartData.MinScore = decimal.Parse
                            (dataReader["MINIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                    {
                        chartData.AvgScore = decimal.Parse
                            (dataReader["AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MY_SCORE"]))
                    {
                        chartData.MyScore = decimal.Parse
                            (dataReader["MY_SCORE"].ToString());
                    }

                    multipleChartDataSource.MultipleSeriesChartDataSource.Add(chartData);

                    //Add data for relative score
                    relativeChartData = new MultipleChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                    {
                        relativeChartData.Name = dataReader["COMPLEXITY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                    {
                        relativeChartData.ID = dataReader["COMPLEXITY_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SHORT_NAME"]))
                    {
                        relativeChartData.ShortName = dataReader["SHORT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MAXIMUM_SCORE"]))
                    {
                        relativeChartData.MaxScore = decimal.Parse
                            (dataReader["RELATIVE_MAXIMUM_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MINIMUM_AVERAGE"]))
                    {
                        relativeChartData.MinScore = decimal.Parse
                            (dataReader["RELATIVE_MINIMUM_AVERAGE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_AVERAGE_SCORE"]))
                    {
                        relativeChartData.AvgScore = decimal.Parse
                            (dataReader["RELATIVE_AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MY_SCORE"]))
                    {
                        relativeChartData.MyScore = decimal.Parse
                            (dataReader["RELATIVE_MY_SCORE"].ToString());
                    }

                    multipleChartDataSource.MultipleSeriesRelativeChartDataSource.Add(relativeChartData);

                }

                return multipleChartDataSource;


            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Method to get the candidate test score 
        /// </summary>
        /// <param name="testKey">
        /// A<see cref="string "/>that holds the test key 
        /// </param>
        /// <returns>
        /// A List for<see cref="ChartData"/>that holds the chart data
        /// </returns>
        public List<ChartData> GetTestCandidateScores(string testKey)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateScreDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATES_TEST_RESULT");

                HCMDatabase.AddInParameter(getCandidateScreDetailsCommand,
                    "@TEST_KEY", DbType.String, testKey);

                dataReader = HCMDatabase.ExecuteReader(getCandidateScreDetailsCommand);

                List<ChartData> chartDataList = new List<ChartData>();

                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["SCORE"]))
                    {
                        chartData.ChartXValue = "0";

                        chartData.ChartYValue = decimal.ToInt32(decimal.Parse
                            (dataReader["SCORE"].ToString()));
                    }
                    chartDataList.Add(chartData);
                }
                return chartDataList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Method to get the test statistics control information 
        /// </summary>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key 
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the test statistics information
        /// </returns>
        public TestStatistics GetGeneralTestStatisticsControlInformation(string testKey)
        {
            TestStatistics testStatistics = new TestStatistics();
            IDataReader dataReader = null;
            try
            {
                //Get the stored procedure
                DbCommand getTestStatisticsDetail = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_STATISTICS_CONTROL_INFORMATION");

                //Add parameters
                HCMDatabase.AddInParameter(getTestStatisticsDetail,
                    "@TEST_KEY", DbType.String, testKey);

                dataReader = HCMDatabase.ExecuteReader(getTestStatisticsDetail);

                //To get the oveerall deatils of the test such as 
                //test name, author name, total question, maximum score,
                //minimum score
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        testStatistics.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                    {
                        testStatistics.TestAuthor = dataReader["USRFIRSTNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        testStatistics.NoOfQuestions = int.Parse(dataReader["TOTAL_QUESTION"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        testStatistics.AverageTimeTakenByCandidates = Convert.ToDecimal(dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        testStatistics.HighestScore = Convert.ToDecimal(dataReader["MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        testStatistics.LowestScore = Convert.ToDecimal(dataReader["MINIMUM_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MEAN_SCORE"]))
                    {
                        testStatistics.MeanScore = Convert.ToDecimal(dataReader["MEAN_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STANDARD_DEVIATION"]))
                    {
                        testStatistics.StandardDeviation = Convert.ToDecimal(dataReader["STANDARD_DEVIATION"].ToString().Trim());
                    }
                }

                //This will return the candidate count.
                //Candidate count is not needed here
                dataReader.NextResult();

                //This will return the list of categories, subjects and its quesiton count
                dataReader.NextResult();

                Subject subject = null;

                testStatistics.SubjectStatistics = new List<Subject>();

                while (dataReader.Read())
                {
                    subject = new Subject();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        subject.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        subject.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_COUNT"]))
                    {
                        subject.QuestionCount = int.Parse(dataReader["QUESTION_COUNT"].ToString());
                    }
                    //Add subjects to the list of data source
                    testStatistics.SubjectStatistics.Add(subject);
                }
                return testStatistics;
            }
            finally
            {
                //If data reader is not null and is not closed. Close it
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that used to get the test statistics chart information 
        /// for the given test
        /// </summary>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the test statistics information
        /// </returns>
        public TestStatistics GetGeneralStatisticsChartInformation(string testKey)
        {
            TestStatistics testStatistics = new TestStatistics();

            IDataReader dataReader = null;

            try
            {
                DbCommand getGeneralStatisticsChartInformationCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_GENERAL_STATISTICS_CHART_CONTROL_INFORMATION");

                HCMDatabase.AddInParameter(getGeneralStatisticsChartInformationCommand,
                    "@TEST_KEY", DbType.String, testKey);

                testStatistics.CategoryStatisticsChartData = new SingleChartData();

                testStatistics.CategoryStatisticsChartData.ChartData = new List<ChartData>();

                dataReader = HCMDatabase.ExecuteReader(getGeneralStatisticsChartInformationCommand);

                //Read next result to take the category statistics
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["CATEGORY_COUNT"].ToString().Trim());
                    }
                    testStatistics.CategoryStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.SubjectStatisticsChartData = new SingleChartData();

                testStatistics.SubjectStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the subject statistics
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["SUBJECT_COUNT"].ToString().Trim());
                    }

                    testStatistics.SubjectStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.TestAreaStatisticsChartData = new SingleChartData();

                testStatistics.TestAreaStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the test area count
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TESTAREA_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["TESTAREA_COUNT"].ToString().Trim());
                    }

                    testStatistics.TestAreaStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.ComplexityStatisticsChartData = new SingleChartData();

                testStatistics.ComplexityStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the complexity count
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["COMPLEXITY_COUNT"].ToString().Trim());
                    }

                    testStatistics.ComplexityStatisticsChartData.ChartData.Add(chartData);
                }

                return testStatistics;
            }
            finally
            {
                //If data reader is not null and is not closed. Close it
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that used to get the candidate statistics detail along with the 
        /// test statistics details for the given candidate 
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <returns>
        /// A<see cref="CandidateStatisticsDetail"/>that holds the candidate 
        /// statistics details 
        /// </returns>
        public List<CandidateStatisticsDetail> GetCandidatesStatistics(CandidateReportDetail candidateReportDetail)
        {
            IDataReader dataReader = null;
            CandidateStatisticsDetail candidateStatisticsDetail = null;
            List<CandidateStatisticsDetail> candidateStatisticsDetails = new List<CandidateStatisticsDetail>();
            try
            {
                DbCommand getCandidateStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_AND_TEST_STATISTICS_INFORMATION");

                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@USER_ID", DbType.Int32, candidateReportDetail.CandidateID);

                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                 "@CANDIDATE_SESSION_KEY", DbType.String, candidateReportDetail.CandidateSessionkey);

                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                 "@ATTEMPT_ID", DbType.Int32, candidateReportDetail.AttemptID);

                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                 "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

                dataReader = HCMDatabase.ExecuteReader(getCandidateStatisticsDetailsCommand);

                if (dataReader.Read())
                {
                    candidateStatisticsDetail = new CandidateStatisticsDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                    {
                        candidateStatisticsDetail.FirstName = dataReader["USER_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                    {
                        candidateStatisticsDetail.FirstName = dataReader["USER_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ADDRESS"]))
                    {
                        candidateStatisticsDetail.Address = dataReader["ADDRESS"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CITY_NAME"]))
                    {
                        candidateStatisticsDetail.Address += " " + dataReader["CITY_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STATE_NAME"]))
                    {
                        candidateStatisticsDetail.Address += " " + dataReader["STATE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COUNTRY_NAME"]))
                    {
                        candidateStatisticsDetail.Address += " " + dataReader["COUNTRY_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["USER_PHONE"]))
                    {
                        candidateStatisticsDetail.Phone = dataReader["USER_PHONE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SYNOPSIS"]))
                    {
                        candidateStatisticsDetail.Synopsis = dataReader["SYNOPSIS"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE"]))
                    {
                        candidateStatisticsDetail.AbsoluteScore = Convert.ToDecimal
                            (dataReader["ABSOLUTE_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                    {
                        candidateStatisticsDetail.RelativeScore = Convert.ToDecimal
                            (dataReader["RELATIVE_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                    {
                        candidateStatisticsDetail.TimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds
                            (int.Parse(dataReader["TIME_TAKEN"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DATE"]))
                    {
                        candidateStatisticsDetail.TestDate = Convert.ToDateTime
                            (dataReader["TEST_DATE"].ToString());

                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                    {
                        candidateStatisticsDetail.AnsweredCorrectly = Convert.ToDecimal
                            (dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        candidateStatisticsDetail.AverageTimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds
                            (decimal.ToInt32(Convert.ToDecimal(dataReader["AVERAGE_TIME_TAKEN"].ToString())));
                    }
                    else
                    {
                        candidateStatisticsDetail.AverageTimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds(0);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                    {
                        candidateStatisticsDetail.Percentile = Convert.ToDecimal
                            (dataReader["PERCENTILE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DATE"]))
                    {
                        candidateStatisticsDetail.InterviewDate = dataReader["INTERVIEW_DATE"].ToString();
                    }

                    candidateStatisticsDetails.Add(candidateStatisticsDetail);
                }
                return candidateStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that used to get the candidate's statistics detail along with the 
        /// test statistics details for the given candidate session key 
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <returns>
        /// A<see cref="CandidateStatisticsDetail"/>that holds the candidate 
        /// statistics details 
        /// </returns>
        public OverAllCandidateScoreDetail GetOverallScoreDetails(CandidateReportDetail candidateReportDetail)
        {
            IDataReader dataReader = null;

            OverAllCandidateScoreDetail overAllScoreDetails = null;

            try
            {
                DbCommand getCandidateScoreDetails =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_OVERALL_TEST_SCORE_DETAILS");

                HCMDatabase.AddInParameter(getCandidateScoreDetails,
                    "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

                HCMDatabase.AddInParameter(getCandidateScoreDetails,
                    "@CAND_SESSION_KEY", DbType.String, candidateReportDetail.CandidateSessionkey);

                HCMDatabase.AddInParameter(getCandidateScoreDetails,
                    "@ATTEMPT_ID", DbType.Int32, candidateReportDetail.AttemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateScoreDetails);

                overAllScoreDetails = new OverAllCandidateScoreDetail();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["MY_SCORE"]))
                    {
                        overAllScoreDetails.AbsoluteMyScore =
                            Convert.ToDecimal(dataReader["MY_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                    {
                        overAllScoreDetails.AbsoluteAvgScore =
                            Convert.ToDecimal(dataReader["AVERAGE_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        overAllScoreDetails.AbsoluteMaxScore =
                            Convert.ToDecimal(dataReader["MAXIMUM_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        overAllScoreDetails.AbsoluteMinScore =
                           Convert.ToDecimal(dataReader["MINIMUM_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MY_SCORE"]))
                    {
                        overAllScoreDetails.RelativeMyScore =
                           Convert.ToDecimal(dataReader["RELATIVE_MY_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_AVERAGE_SCORE"]))
                    {
                        overAllScoreDetails.RelativeAvgScore =
                           Convert.ToDecimal(dataReader["RELATIVE_AVERAGE_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MAXIMUM_SCORE"]))
                    {
                        overAllScoreDetails.RelativeMaxScore =
                            Convert.ToDecimal(dataReader["RELATIVE_MAXIMUM_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MINIMUM_AVERAGE"]))
                    {
                        overAllScoreDetails.RelativeMinScore =
                           Convert.ToDecimal(dataReader["RELATIVE_MINIMUM_AVERAGE"]);
                    }
                }

                //dataReader.NextResult();

                //while (dataReader.Read())
                //{
                //    
                //}
                return overAllScoreDetails;

            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the candidate score 
        /// details for the histogram chart
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <returns>
        /// A<see cref="HistogramData"/>that holds the histogram data
        /// </returns>
        public ReportHistogramData GetReportCandidateScoreDetails(CandidateReportDetail candidateReportDetail)
        {
            IDataReader dataReader = null;

            ReportHistogramData histogramData = null;

            try
            {
                DbCommand getCandidateScorePositionDetails =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATES_SCORE_POSITION");

                histogramData = new ReportHistogramData();

                HCMDatabase.AddInParameter(getCandidateScorePositionDetails,
                    "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

                HCMDatabase.AddInParameter(getCandidateScorePositionDetails,
                    "@CANDIDATE_SESSION_ID", DbType.String, candidateReportDetail.CandidateSessionkey);

                HCMDatabase.AddInParameter(getCandidateScorePositionDetails,
                    "@ATTEMPT_ID", DbType.Int32, candidateReportDetail.AttemptID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateScorePositionDetails);

                // Initilialise the aboslute chart data
                histogramData.AbsoluteChartData = new List<ChartData>();

                // Initialise the relative chart data
                histogramData.RelativeChartData = new List<ChartData>();

                ChartData absoluteChartData = null;
                ChartData relativeChartData = null;

                while (dataReader.Read())
                {
                    absoluteChartData = new ChartData();
                    relativeChartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE"]))
                    {
                        absoluteChartData.ChartXValue = "0";

                        absoluteChartData.ChartYValue = decimal.ToInt32(decimal.Parse
                            (dataReader["ABSOLUTE_SCORE"].ToString()));
                    }
                    histogramData.AbsoluteChartData.Add(absoluteChartData);
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                    {
                        relativeChartData.ChartXValue = "0";

                        relativeChartData.ChartYValue = decimal.ToInt32(decimal.Parse
                            (dataReader["RELATIVE_SCORE"].ToString()));
                    }
                    histogramData.RelativeChartData.Add(relativeChartData);
                }

                dataReader.NextResult();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                    {
                        histogramData.CandidateName = dataReader["USER_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE"]))
                    {
                        histogramData.CandidateScore = decimal.ToInt32(
                            Convert.ToDecimal(dataReader["ABSOLUTE_SCORE"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                    {
                        histogramData.CandidateRelativeScore = decimal.ToInt32(
                            Convert.ToDecimal(dataReader["RELATIVE_SCORE"].ToString()));
                    }
                }
                return histogramData;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the candidate statistics detail
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <param name="totalRecord">
        /// A<see cref="CandidateReportDetail"/>that holds the total records
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="CandidateReportDetail"/>that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="CandidateReportDetail"/>that holds the page size
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateStatisticsDetail"/>that holds the candidate statistics detail
        /// </returns>
        public List<CandidateStatisticsDetail> GetComparisonCandidateStatistics
            (CandidateReportDetail candidateReportDetail, out int totalRecord,
            int pageNumber, int pageSize)
        {
            IDataReader dataReader = null;
            CandidateStatisticsDetail candidateStatisticsDetail = null;
            List<CandidateStatisticsDetail> candidateStatisticsDetails =
                new List<CandidateStatisticsDetail>();
            totalRecord = 0;
            try
            {
                DbCommand getCandidateStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_COMPARISON_CANDIDATE_STATISTICS_INFORMATION_REPORT");
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                 "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@COMBINED_SESSION_KEYS", DbType.String, candidateReportDetail.CombinedSessionKeyAttemtID);
                dataReader = HCMDatabase.ExecuteReader(getCandidateStatisticsDetailsCommand);
                while (dataReader.Read())
                {
                    candidateStatisticsDetail = new CandidateStatisticsDetail();
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                        {
                            candidateStatisticsDetail.FirstName = dataReader["USER_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ADDRESS"]))
                        {
                            candidateStatisticsDetail.Address = dataReader["ADDRESS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CITY_NAME"]))
                        {
                            candidateStatisticsDetail.Address += " " + dataReader["CITY_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["STATE_NAME"]))
                        {
                            candidateStatisticsDetail.Address += " " + dataReader["STATE_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["COUNTRY_NAME"]))
                        {
                            candidateStatisticsDetail.Address += " " + dataReader["COUNTRY_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["USER_PHONE"]))
                        {
                            candidateStatisticsDetail.Phone = dataReader["USER_PHONE"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SYNOPSIS"]))
                        {
                            candidateStatisticsDetail.Synopsis = dataReader["SYNOPSIS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE"]))
                        {
                            candidateStatisticsDetail.AbsoluteScore = Convert.ToDecimal
                                (dataReader["ABSOLUTE_SCORE"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                        {
                            candidateStatisticsDetail.RelativeScore = Convert.ToDecimal
                                (dataReader["RELATIVE_SCORE"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                        {
                            candidateStatisticsDetail.TimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds
                                (int.Parse(dataReader["TIME_TAKEN"].ToString()));
                        }
                        else
                        {
                            candidateStatisticsDetail.TimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds(0);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DATE"]))
                        {
                            candidateStatisticsDetail.TestDate = Convert.ToDateTime
                                (dataReader["TEST_DATE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                        {
                            candidateStatisticsDetail.AnsweredCorrectly = Convert.ToDecimal
                                (dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                        {
                            candidateStatisticsDetail.AverageTimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds
                                (decimal.ToInt32(Convert.ToDecimal(dataReader["AVERAGE_TIME_TAKEN"].ToString())));
                        }
                        else
                            candidateStatisticsDetail.AverageTimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds(0);

                        if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                        {
                            candidateStatisticsDetail.Percentile = Convert.ToDecimal
                                (dataReader["PERCENTILE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DATE"]))
                        {
                            candidateStatisticsDetail.InterviewDate = dataReader["INTERVIEW_DATE"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CAND_SESSION_KEY"]))
                        {
                            candidateStatisticsDetail.CandidateSessionID = dataReader["CAND_SESSION_KEY"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            candidateStatisticsDetail.AttemptNumber = int.Parse(dataReader["ATTEMPT_ID"].ToString());
                        }
                        candidateStatisticsDetails.Add(candidateStatisticsDetail);
                    }
                    else
                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        {
                            totalRecord = int.Parse(dataReader["TOTAL"].ToString());
                        }
                }
                return candidateStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the candidate score 
        /// details for the histogram chart
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail
        /// </param>
        /// <returns>
        /// A<see cref="HistogramData"/>that holds the histogram data
        /// </returns>
        public ReportHistogramData GetComparisonReportHistogramScoreDetails
            (CandidateReportDetail candidateReportDetail)
        {
            IDataReader dataReader = null;

            ReportHistogramData histogramData = null;

            try
            {
                DbCommand getCandidateScorePostionDetails =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATES_TEST_RESULT_COMPARISON_REPORT");

                histogramData = new ReportHistogramData();

                HCMDatabase.AddInParameter(getCandidateScorePostionDetails,
                    "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

                HCMDatabase.AddInParameter(getCandidateScorePostionDetails,
                    "@COMBINED_SESSION_KEY", DbType.String, candidateReportDetail.
                    CombinedSessionKeyAttemtID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateScorePostionDetails);

                histogramData.AbsoluteChartData = new List<ChartData>();

                //Initialise the relative chart data
                histogramData.RelativeChartData = new List<ChartData>();

                histogramData.CandidateRelativeScoreData = new List<HistogramData>();

                ChartData relativeChartData = null;

                ChartData chartData = null;

                while (dataReader.Read())
                {
                    chartData = new ChartData();

                    relativeChartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE"]))
                    {
                        chartData.ChartXValue = "0";

                        chartData.ChartYValue = decimal.ToInt32(decimal.Parse
                            (dataReader["ABSOLUTE_SCORE"].ToString()));
                    }
                    histogramData.AbsoluteChartData.Add(chartData);

                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                    {
                        relativeChartData.ChartXValue = "0";

                        relativeChartData.ChartYValue = decimal.ToInt32(decimal.Parse
                            (dataReader["RELATIVE_SCORE"].ToString()));
                    }
                    histogramData.RelativeChartData.Add(relativeChartData);
                }

                dataReader.NextResult();

                histogramData.CandidateScoreData = new List<HistogramData>();

                histogramData.CandidateRelativeScoreData = new List<HistogramData>();

                HistogramData candidateDetail = null;

                HistogramData relativeCandidateDetail = null;

                while (dataReader.Read())
                {
                    candidateDetail = new HistogramData();

                    relativeCandidateDetail = new HistogramData();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                    {
                        candidateDetail.CandidateName = dataReader["USER_NAME"].ToString();

                        relativeCandidateDetail.CandidateName = dataReader["USER_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE"]))
                    {
                        candidateDetail.CandidateScore = decimal.ToInt32(
                            Convert.ToDecimal(dataReader["ABSOLUTE_SCORE"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                    {
                        histogramData.CandidateRelativeScore = decimal.ToInt32(
                            Convert.ToDecimal(dataReader["RELATIVE_SCORE"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                    {
                        relativeCandidateDetail.CandidateScore = decimal.ToInt32(
                            Convert.ToDecimal(dataReader["RELATIVE_SCORE"].ToString()));
                    }
                    histogramData.CandidateRelativeScoreData.Add(relativeCandidateDetail);
                    histogramData.CandidateScoreData.Add(candidateDetail);
                }
                return histogramData;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the overall candidate score details
        /// for the comparison report
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the 
        /// candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="OverAllCandidateScoreDetail"/>over all candidate score 
        /// details
        /// </returns>
        public OverAllCandidateScoreDetail GetComparisonOverallScoreDetails(CandidateReportDetail
            candidateReportDetail)
        {
            //SPGET_COMPARISON_OVERALL_TEST_SCORE_DETAILS

            IDataReader dataReader = null;

            OverAllCandidateScoreDetail overAllScoreDetails = null;

            try
            {
                DbCommand getCandidateScoreDetails =
                    HCMDatabase.GetStoredProcCommand("SPGET_COMPARISON_OVERALL_TEST_SCORE_DETAILS");

                HCMDatabase.AddInParameter(getCandidateScoreDetails,
                    "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

                HCMDatabase.AddInParameter(getCandidateScoreDetails,
                    "@COMBINED_SESSION_KEY", DbType.String, candidateReportDetail.
                    CombinedSessionKeyAttemtID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateScoreDetails);

                overAllScoreDetails = new OverAllCandidateScoreDetail();

                overAllScoreDetails.AbsoluteScoreDetails = new List<ChartData>();

                overAllScoreDetails.RelativeScoreDetails = new List<ChartData>();

                ChartData chartData = null;
                ChartData relativeChartData = null;

                while (dataReader.Read())
                {
                    if ((!Utility.IsNullOrEmpty(dataReader["MY_SCORE"])) &&
                        (!Utility.IsNullOrEmpty(dataReader["USRUSERNAME"])) &&
                         (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_KEY"]))
                        && !Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                    {
                        chartData = new ChartData();

                        chartData.ReportChartXValue = dataReader["USRUSERNAME"].ToString().Trim() + "-" +
                                dataReader["CANDIDATE_SESSION_KEY"].ToString().Trim() + "-" + dataReader["ATTEMPT_ID"].ToString().Trim();

                        chartData.ReportChartYValue =
                         Convert.ToDecimal(dataReader["MY_SCORE"]);

                        overAllScoreDetails.AbsoluteScoreDetails.Add(chartData);

                        relativeChartData = new ChartData();

                        relativeChartData.ReportChartXValue = dataReader["USRUSERNAME"].ToString().Trim() + "-" +
                             dataReader["CANDIDATE_SESSION_KEY"].ToString().Trim() + "-" + dataReader["ATTEMPT_ID"].ToString().Trim();

                        relativeChartData.ReportChartYValue =
                          Convert.ToDecimal(dataReader["RELATIVE_MY_SCORE"]);

                        overAllScoreDetails.RelativeScoreDetails.Add(relativeChartData);
                    }

                }

                dataReader.NextResult();

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                    {
                        chartData = new ChartData();

                        chartData.ReportChartXValue = "Avg Score";

                        chartData.ReportChartYValue =
                           Convert.ToDecimal(dataReader["AVERAGE_SCORE"]);

                        overAllScoreDetails.AbsoluteScoreDetails.Add(chartData);

                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        chartData = new ChartData();

                        chartData.ReportChartXValue = "Max Score";

                        chartData.ReportChartYValue =
                           Convert.ToDecimal(dataReader["MAXIMUM_SCORE"]);

                        overAllScoreDetails.AbsoluteScoreDetails.Add(chartData);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        chartData = new ChartData();

                        chartData.ReportChartXValue = "Min Score";

                        chartData.ReportChartYValue =
                            Convert.ToDecimal(dataReader["MINIMUM_SCORE"]);

                        overAllScoreDetails.AbsoluteScoreDetails.Add(chartData);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_AVERAGE_SCORE"]))
                    {
                        relativeChartData = new ChartData();

                        relativeChartData.ReportChartXValue = "Avg Score";

                        relativeChartData.ReportChartYValue =
                            Convert.ToDecimal(dataReader["RELATIVE_AVERAGE_SCORE"]);

                        overAllScoreDetails.RelativeScoreDetails.Add(relativeChartData);

                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MAXIMUM_SCORE"]))
                    {
                        relativeChartData = new ChartData();

                        relativeChartData.ReportChartXValue = "Max Score";

                        relativeChartData.ReportChartYValue =
                            Convert.ToDecimal(dataReader["RELATIVE_MAXIMUM_SCORE"]);

                        overAllScoreDetails.RelativeScoreDetails.Add(relativeChartData);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_MINIMUM_AVERAGE"]))
                    {
                        relativeChartData = new ChartData();

                        relativeChartData.ReportChartXValue = "Min Score";

                        relativeChartData.ReportChartYValue =
                           Convert.ToDecimal(dataReader["RELATIVE_MINIMUM_AVERAGE"]);

                        overAllScoreDetails.RelativeScoreDetails.Add(relativeChartData);
                    }
                }

                return overAllScoreDetails;

            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the dataview of the multiseris chart control 
        /// for the comparison category chart 
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>   
        /// <returns>
        /// A<see cref="DataTable"/>that holds the data table of the category details
        /// </returns>
        public DataTable GetComparisonReportCategoryDetails(CandidateReportDetail candidateReportDetail)
        {
            DataSet categoryDetails = new DataSet();
            DbCommand getCandidateScoreDetails =
                HCMDatabase.GetStoredProcCommand("SPCOMPARE_TEST_RESULT_CATEGORY_STATISTICS");
            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);
            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@svalue", DbType.String, candidateReportDetail.
                CombinedSessionKeyAttemtID);
            HCMDatabase.LoadDataSet(getCandidateScoreDetails, categoryDetails, "Category");
            return categoryDetails.Tables[0];
        }

        /// <summary>
        /// Represents the method to get the data table for the comparison 
        /// report subject details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// that holds the comparison's subject chart details
        /// </returns>
        public DataTable GetComparisonReportSubjectDetails(CandidateReportDetail candidateReportDetail)
        {
            DataSet subjectDetails = new DataSet();
            DbCommand getCandidateScoreDetails =
                HCMDatabase.GetStoredProcCommand("SPGETCOMPARE_TEST_RESULT_SUBJECT_STATISTICS");

            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@svalue", DbType.String, candidateReportDetail.
                CombinedSessionKeyAttemtID);

            HCMDatabase.LoadDataSet(getCandidateScoreDetails, subjectDetails, "Subject");
            return subjectDetails.Tables[0];
        }

        /// <summary>
        /// Represents the method to get the data table for the comparison 
        /// report test area details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// that holds the comparison's subject chart details
        /// </returns>
        public DataTable GetComparisonReportTestAreaDetails(CandidateReportDetail candidateReportDetail)
        {
            DataSet testAreaDetails = new DataSet();
            DbCommand getCandidateScoreDetails =
                HCMDatabase.GetStoredProcCommand("SPGETCOMPARE_TEST_RESULT_TESTAREA_STATISTICS");
            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);
            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@svalue", DbType.String, candidateReportDetail.
                CombinedSessionKeyAttemtID);
            HCMDatabase.LoadDataSet(getCandidateScoreDetails,
                testAreaDetails, "TestArea");
            return testAreaDetails.Tables[0];
        }


        /// <summary>
        /// Represents the method to get the over all score details 
        /// for the group analysis report
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="OverAllCandidateScoreDetail"/>that holds the over all score details 
        /// for the group analysis report
        /// </returns>
        public OverAllCandidateScoreDetail GetGroupAnalysisOverallScoreDetails
            (CandidateReportDetail candidateReportDetail)
        {
             IDataReader dataReader = null;

             try
             {
                 OverAllCandidateScoreDetail overAllScoreDetails = null;
                 ChartData chartData = null;
                 DbCommand getCandidateScoreDetails =
                     HCMDatabase.GetStoredProcCommand("SPGETGROUP_ANALYSIS_OVERALL_SCORE_DETAILS");

                 HCMDatabase.AddInParameter(getCandidateScoreDetails,
                     "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

                 HCMDatabase.AddInParameter(getCandidateScoreDetails,
                     "@COMBINED_SESSION_KEY", DbType.String, candidateReportDetail.
                     CombinedSessionKeyAttemtID);

                 dataReader = HCMDatabase.ExecuteReader(getCandidateScoreDetails);

                 overAllScoreDetails = new OverAllCandidateScoreDetail();

                 overAllScoreDetails.AbsoluteScoreDetails = new List<ChartData>();

                 overAllScoreDetails.RelativeScoreDetails = new List<ChartData>();

                 if (dataReader.Read())
                 {
                     if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                     {
                         chartData = new ChartData();

                         chartData.ReportChartXValue = "Avg Score";

                         chartData.ReportChartYValue =
                            Convert.ToDecimal
                             (dataReader["AVERAGE_SCORE"]);

                         overAllScoreDetails.AbsoluteScoreDetails.Add(chartData);

                     }
                     if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                     {
                         chartData = new ChartData();

                         chartData.ReportChartXValue = "Max Score";

                         chartData.ReportChartYValue =
                            Convert.ToDecimal
                             (dataReader["MAXIMUM_SCORE"]);

                         overAllScoreDetails.AbsoluteScoreDetails.Add(chartData);
                     }
                     if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                     {
                         chartData = new ChartData();

                         chartData.ReportChartXValue = "Min Score";

                         chartData.ReportChartYValue =
                            Convert.ToDecimal
                             (dataReader["MINIMUM_SCORE"]);

                         overAllScoreDetails.AbsoluteScoreDetails.Add(chartData);
                     }
                     if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_RELATIVE_SCORE"]))
                     {
                         chartData = new ChartData();

                         chartData.ReportChartXValue = "Avg Score";

                         chartData.ReportChartYValue =
                            Convert.ToDecimal
                             (dataReader["AVERAGE_RELATIVE_SCORE"]);

                         overAllScoreDetails.RelativeScoreDetails.Add(chartData);

                     }
                     if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_RELATIVE_SCORE"]))
                     {
                         chartData = new ChartData();

                         chartData.ReportChartXValue = "Max Score";

                         chartData.ReportChartYValue =
                            Convert.ToDecimal
                             (dataReader["MAXIMUM_RELATIVE_SCORE"]);

                         overAllScoreDetails.RelativeScoreDetails.Add(chartData);
                     }
                     if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_RELATIVE_SCORE"]))
                     {
                         chartData = new ChartData();

                         chartData.ReportChartXValue = "Min Score";

                         chartData.ReportChartYValue =
                            Convert.ToDecimal
                             (dataReader["MINIMUM_RELATIVE_SCORE"]);

                         overAllScoreDetails.RelativeScoreDetails.Add(chartData);
                     }
                 }
                 return overAllScoreDetails;
             }
             finally
             {
                 if (dataReader != null && !dataReader.IsClosed)
                 {
                     dataReader.Close();
                 }
             }
        }

        /// <summary>
        /// Reprsents the method to get the data table
        /// that holds the group analysis category chart details from database
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="DataTable"/>that holds the group analysis chart 
        /// details 
        /// </returns>
        public DataTable GetGroupAnalysisReportCategoryDetails(CandidateReportDetail candidateReportDetail)
        {
            DataSet categoryDetails = new DataSet();
            DbCommand getCandidateScoreDetails =
                HCMDatabase.GetStoredProcCommand("SPGET_GROUP_ANALYSIS_CATEGORY_STATISTICS");

            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@COMBINED_SESSION_KEY", DbType.String, candidateReportDetail.
                CombinedSessionKeyAttemtID);

            HCMDatabase.LoadDataSet(getCandidateScoreDetails,
                categoryDetails, "Category");
            return categoryDetails.Tables[0];
        }

        /// <summary>
        /// Reprsents the method to get the data table
        /// that holds the group analysis subject chart details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report details
        /// </param>
        /// <returns>
        /// A<see cref="DataTable"/>that holds the group analysis chart 
        /// details 
        /// </returns>
        public DataTable GetGroupAnalysisReportSubjectDetails(CandidateReportDetail candidateReportDetail)
        {
            DataSet categoryDetails = new DataSet();
            DbCommand getCandidateScoreDetails =
                HCMDatabase.GetStoredProcCommand("SPGET_GROUP_ANALYSIS_SUBJECT_STATISTICS");

            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);

            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@COMBINED_SESSION_KEY", DbType.String, candidateReportDetail.
                CombinedSessionKeyAttemtID);

            HCMDatabase.LoadDataSet(getCandidateScoreDetails,
                categoryDetails, "Category");
            return categoryDetails.Tables[0];
        }

        /// <summary>
        /// Reprsents the method to get the data table
        /// that holds the group analysis test area chart details
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate 
        /// report details
        /// </param>
        /// <returns>
        /// A<see cref="DataTable"/>that holds the group analysis chart 
        /// details 
        /// </returns>
        public DataTable GetGroupAnalysisReportTestAreaDetails(CandidateReportDetail candidateReportDetail)
        {
            DataSet categoryDetails = new DataSet();
            DbCommand getCandidateScoreDetails =
                HCMDatabase.GetStoredProcCommand("SPGET_GROUP_ANALYSIS_TESTAREA_STATISTICS");
            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@TEST_KEY", DbType.String, candidateReportDetail.TestKey);
            HCMDatabase.AddInParameter(getCandidateScoreDetails,
                "@COMBINED_SESSION_KEY", DbType.String, candidateReportDetail.
                CombinedSessionKeyAttemtID);
            HCMDatabase.LoadDataSet(getCandidateScoreDetails,
                categoryDetails, "Category");
            return categoryDetails.Tables[0];
        }

        /// <summary>
        /// Reprsents the method to get the Candidate Statistics Detail
        /// </summary>
        /// <param name="candidateReportDetail">
        /// A<see cref="CandidateReportDetail"/>that holds the candidate report detail 
        /// </param>
        /// <returns>
        /// A<see cref="CandidateStatisticsDetail"/>that holds the Candidate Statistics Detail 
        /// </returns>
        public List<CandidateStatisticsDetail> GetCandidateReportCandidateStatistics(
            CandidateReportDetail candidateReportDetail)
        {
            IDataReader dataReader = null;
            CandidateStatisticsDetail candidateStatisticsDetail = null;
            List<CandidateStatisticsDetail> candidateStatisticsDetails =
                new List<CandidateStatisticsDetail>();
            try
            {
                DbCommand getCandidateStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_REPORTS");
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                 "@CANDIDATE_IDS", DbType.String, candidateReportDetail.CombinedCandidateIDs);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@SUBJECT_IDS", DbType.String, candidateReportDetail.CombinedSubjectIDs);
                dataReader = HCMDatabase.ExecuteReader(getCandidateStatisticsDetailsCommand);
                while (dataReader.Read())
                {
                    candidateStatisticsDetail = new CandidateStatisticsDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["FIRSTNAME"]))
                    {
                        candidateStatisticsDetail.FirstName = dataReader["FIRSTNAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["USERNAME"]))
                    {
                        candidateStatisticsDetail.UserName = dataReader["USERNAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STREET"]))
                    {
                        candidateStatisticsDetail.Address = dataReader["STREET"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CITY"]))
                    {
                        candidateStatisticsDetail.Address += " " + dataReader["CITY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STATE"]))
                    {
                        candidateStatisticsDetail.Address += " " + dataReader["STATE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COUNTRY"]))
                    {
                        candidateStatisticsDetail.Address += " " + dataReader["COUNTRY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["OFFICE_PHONE"]))
                    {
                        candidateStatisticsDetail.Phone = dataReader["OFFICE_PHONE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUMMARY"]))
                    {
                        candidateStatisticsDetail.Synopsis = dataReader["SUMMARY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE"]))
                    {
                        candidateStatisticsDetail.AbsoluteScore = Convert.ToDecimal
                            (dataReader["ABSOLUTE_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                    {
                        candidateStatisticsDetail.RelativeScore = Convert.ToDecimal
                            (dataReader["RELATIVE_SCORE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["PERCENTAGE"]))
                    {
                        candidateStatisticsDetail.AnsweredCorrectly = Convert.ToDecimal
                            (dataReader["PERCENTAGE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                    {
                        candidateStatisticsDetail.Percentile = Convert.ToDecimal
                            (dataReader["PERCENTILE"].ToString());
                    }
                    candidateStatisticsDetails.Add(candidateStatisticsDetail);
                }
                return candidateStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the completed candidate session key
        /// </summary>
        /// <param name="testKey">
        /// A<see cref="string"/>that holds the test key
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the Candidate key
        /// </returns>
        public string GetCompletedTestSessionKeys(string testKey)
        {
            IDataReader reader = null;
            string candidateKeys = null;
            try
            {
                DbCommand getCandidateKeysCommand = HCMDatabase.GetStoredProcCommand("SPGET_COMPLETED_TESTSESSION_CANDIDATE_KEYS");
                HCMDatabase.AddInParameter(getCandidateKeysCommand, "@TEST_KEY", DbType.String, testKey);
                reader = HCMDatabase.ExecuteReader(getCandidateKeysCommand);
                while (reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["CAND_SESSION_KEY"]))
                    {
                        candidateKeys = reader["CAND_SESSION_KEY"].ToString().Trim();
                    }
                }
                return candidateKeys;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }
        #endregion
    }
}
