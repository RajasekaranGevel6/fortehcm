﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalResumeServiceDLManager.cs
// Class that represents the data layer for the external resume
// service management. This will talk to the database to perform the 
// operations associated with external resume management processes.

#endregion

#region Directives                                                             
using System;
using System.Xml;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the external resume
    /// service management. This will talk to the database to perform the 
    /// operations associated with external resume management processes.
    /// </summary>
    public class ExternalResumeServiceDLManager : DatabaseConnectionManager
    {
        #region Public Method                                                  
        /// <summary>
        /// Gets the career builder resume.
        /// </summary>
        /// <param name="careerBuilderID">The career builder ID.</param>
        /// <returns></returns>
        public CareerBuilderResume GetCareerBuilderResume(int careerBuilderID)
        {
            IDataReader dataReader = null;
            CareerBuilderResume careerBuilderResume = null;
            try
            {
                DbCommand dashBoardCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CAREERBUILDER_RESUME_BY_ID");

                HCMDatabase.AddInParameter(dashBoardCommand,
                    "@CAREERBUILDER_ID", DbType.Int32, careerBuilderID);
                dataReader = HCMDatabase.ExecuteReader(dashBoardCommand);
                if (dataReader.Read())
                {
                    if (careerBuilderResume == null)
                        careerBuilderResume = new CareerBuilderResume();

                    careerBuilderResume.CareerBuilderID = int.Parse(dataReader["CAREERBUILDER_ID"].ToString());
                    careerBuilderResume.ResumeContent = (byte[])dataReader["RESUME_CONTENT"];
                    careerBuilderResume.ResumeDetails = dataReader["RESUME_DETAILS"].ToString().Trim(); ;
                    careerBuilderResume.ResumeID = dataReader["RESUME_ID"].ToString().Trim(); ;
                    careerBuilderResume.ResumeText = dataReader["RESUME_TEXT"].ToString().Trim(); ;
                    careerBuilderResume.ResumeTitle = dataReader["RESUME_TITLE"].ToString().Trim(); ;
                }
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

            return careerBuilderResume;
        }

        /// <summary>
        /// Inserts the career builder resume.
        /// </summary>
        /// <param name="careerBuilderResume">The career builder resume.</param>
        public void InsertCareerBuilderResume(CareerBuilderResume careerBuilderResume)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand insertFormCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_CAREERBUILDER_RESUME");

                // Add input parameters
                HCMDatabase.AddInParameter(insertFormCommand,
                    "@RESUME_ID", DbType.String, careerBuilderResume.ResumeID);

                HCMDatabase.AddInParameter(insertFormCommand,
                 "@RESUME_DETAILS", DbType.Xml,
                 new SqlXml(new XmlTextReader(careerBuilderResume.ResumeDetails.Replace("&lt;", "<").Replace("&gt;", ">"),
                     XmlNodeType.Document, null)));

                HCMDatabase.AddInParameter(insertFormCommand,
                 "@RESUME_TITLE", DbType.String, careerBuilderResume.ResumeTitle);

                HCMDatabase.AddInParameter(insertFormCommand,
                    "@RESUME_CONTENT", DbType.Binary, careerBuilderResume.ResumeContent);
                HCMDatabase.AddInParameter(insertFormCommand,
                   "@RESUME_TEXT", DbType.String, careerBuilderResume.ResumeText);
                // Execute command
                HCMDatabase.ExecuteNonQuery(insertFormCommand);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Inserts the external resume.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="externalResume">The external resume.</param>
        public void InsertExternalResume(IDbTransaction transaction, ExternalResume externalResume)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand insertExternalResumeCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_EXTERNAL_RESUME");

                // Add input parameters
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                    "@RESUME_ID", DbType.String, externalResume.ResumeID);

                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                 "@RESUME_DETAILS", DbType.Xml,
                 new SqlXml(new XmlTextReader(externalResume.ResumeDetails.Replace("&lt;", "<").Replace("&gt;", ">"),
                     XmlNodeType.Document, null)));

                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                 "@RESUME_TITLE", DbType.String, externalResume.ResumeTitle);

                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                    "@RESUME_CONTENT", DbType.Binary, externalResume.ResumeContent);
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@USER_ID", DbType.Int32, externalResume.CreatedBy);

                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@CANDIDATE_ID", DbType.Int32, externalResume.CandidateID);

                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@JOB_BOARD_ID", DbType.String, externalResume.JobBoardID);
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@RESUME_TEXT", DbType.String, externalResume.ResumeText);

                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@CANDIDATE_NAME", DbType.String, externalResume.CandidateName);
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@CANDIDATE_ADDRESS", DbType.String, externalResume.CandidateAddress);
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@RECENT_PAY", DbType.String, externalResume.RecentPay);
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@MOST_RECENT_TITLE", DbType.String, externalResume.MostRecentTitle);
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@EXPERIENCE_YEAR", DbType.String, (int.Parse(externalResume.ExperienceMonths) / 12).ToString());
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@HIGHEST_DEGREE", DbType.String, externalResume.HighestDegree);
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@MOST_RECENT_COMPANYNAME", DbType.String, externalResume.MostRecentCompanyName);
                HCMDatabase.AddInParameter(insertExternalResumeCommand,
                   "@LAST_MODIFIED", DbType.String, externalResume.LastModified);
                // Execute command
                HCMDatabase.ExecuteNonQuery(insertExternalResumeCommand, transaction as DbTransaction);
                

            }
            catch (Exception exp)
            {
                throw exp;
            }

        }

        /// <summary>
        /// Inserts the tenanat candidate.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="candidateID">The candidate ID.</param>
        /// <param name="userID">The user ID.</param>
        public void InsertTenanatCandidate(IDbTransaction transaction, int tenantID, int candidateID, int userID)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand insertTenanatCandidateCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_TENANT_CANDIDATE");

                // Add input parameters
                HCMDatabase.AddInParameter(insertTenanatCandidateCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(insertTenanatCandidateCommand,
                   "@USER_ID", DbType.Int32, userID);

                HCMDatabase.AddInParameter(insertTenanatCandidateCommand,
                   "@CANDIDATE_ID", DbType.Int32, candidateID);
                //SPINSERT_CANDIDATEINFORMATION
                // Execute command
                HCMDatabase.ExecuteNonQuery(insertTenanatCandidateCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Inserts the candidate information.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="candidateID">The candidate ID.</param>
        public int InsertCandidateInformation(IDbTransaction transaction, CandidateInformation 
            candidateInformation, int userID, out int candidateID)
        {
            try
            {
                candidateID = 0;
                // Create a stored procedure command object
                DbCommand insertCandidateInformationCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_CANDIDATEINFORMATION");
                // Add input parameters
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                    "@CAIFIRSTNAME", DbType.String, candidateInformation.caiFirstName);
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                    "@CAILASTNAME", DbType.String, candidateInformation.caiLastName);
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                   "@CAILOCATION", DbType.String, candidateInformation.caiLocation);
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                   "@CAIZIP", DbType.String, candidateInformation.caiZip);
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                  "@CAIEMAIL", DbType.String, candidateInformation.caiEmail);
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                  "@CAIC2CCOST", DbType.Decimal, candidateInformation.caiC2CCost);
                HCMDatabase.AddOutParameter(insertCandidateInformationCommand,
                  "@CAIID", DbType.Int32, candidateID);
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                  "@USER_ID", DbType.Int32, userID);
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                  "@CAITYPE", DbType.String, candidateInformation.caiType);
                
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                  "@CAIEMPLOYEE", DbType.String, candidateInformation.caiEmployee);
                HCMDatabase.AddInParameter(insertCandidateInformationCommand,
                    "@CAILIMITEDACCESS", DbType.String, candidateInformation.caiLimitedAccess == true ? "Y" : "N");
                //SPINSERT_CANDIDATEINFORMATION
                // Execute command
                HCMDatabase.ExecuteNonQuery(insertCandidateInformationCommand, transaction as DbTransaction);
                if (!(Utility.IsNullOrEmpty(insertCandidateInformationCommand.Parameters["@CAIID"].Value)))
                {
                    return int.Parse(insertCandidateInformationCommand.Parameters["@CAIID"].Value.ToString());
                }
                else
                {
                    return candidateID;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Gets the resume count.
        /// </summary>
        /// <param name="resumeID">The resume ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int GetExistingCandidateID(string resumeID, int? tenantID)
        {
            int candidateID = 0;
            IDataReader dataReader = null;
            try
            {
                DbCommand resumeCountCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CHECK_EXTERNAL_RESUME");

                HCMDatabase.AddInParameter(resumeCountCommand,
                "@RESUME_ID", DbType.String, resumeID);
                HCMDatabase.AddInParameter(resumeCountCommand,
                    "@TENANAT_ID", DbType.Int32, tenantID);
                dataReader = HCMDatabase.ExecuteReader(resumeCountCommand);
                if (dataReader.Read())
                {
                    candidateID = int.Parse(dataReader["CANDIDATE_ID"].ToString());
                }
                return candidateID;
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the external resume.
        /// </summary>
        /// <param name="resumeID">The resume ID.</param>
        /// <returns></returns>
        public ExternalResume GetExternalResume(string resumeID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand resumeCountCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_EXTERNAL_RESUME");

                HCMDatabase.AddInParameter(resumeCountCommand,
                "@RESUME_ID", DbType.String, resumeID);
                dataReader = HCMDatabase.ExecuteReader(resumeCountCommand);
                ExternalResume externalResume = null;
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(externalResume))
                        externalResume = new ExternalResume();
                    externalResume.ResumeDetails = dataReader["RESUME_DETAILS"].ToString();
                    externalResume.ResumeText = dataReader["RESUME_TEXT"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["RESUME_CONTENT"]))
                        externalResume.ResumeContent = (byte[])dataReader["RESUME_CONTENT"];
                }
                return externalResume;
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<string> GetResumesID(int pageNo,int pageSize)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand resumeCountCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_EXTERNAL_RESUMES");

                HCMDatabase.AddInParameter(resumeCountCommand,
                "@PAGENUM", DbType.Int32, pageNo);
                HCMDatabase.AddInParameter(resumeCountCommand,
                "@PAGESIZE", DbType.Int32, pageSize);
                dataReader = HCMDatabase.ExecuteReader(resumeCountCommand);
                List<string> resumesId = null;
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(resumesId))
                        resumesId = new List<string>();

                    resumesId.Add(dataReader["RESUME_ID"].ToString());
                }
                return resumesId;
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        #endregion Public Method
    }
}
