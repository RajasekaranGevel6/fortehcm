﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestRecommendationDLManager.cs
// File that represents the data layer for the Test respository Manager.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Test Recommendation management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for Recommendation. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class TestRecommendationDLManager : DatabaseConnectionManager
    {
        #region Public Method

        /// <summary>
        /// Method that inserts the recommended test detail.
        /// </summary>
        /// <param name="testRecommendationDetail">
        /// A <see cref="TestRecommendationDetail"/> that holds the test recommendation detail.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </returns>
        public int InsertTestSegmentDetail(TestRecommendationDetail testRecommendationDetail,
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_TEST_SEGMENT_DETAIL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_TOTAL_QUESTION", DbType.Int16, testRecommendationDetail.NoOfQuestions);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_NAME", DbType.String, testRecommendationDetail.TestName);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_SKILL", DbType.String, testRecommendationDetail.Skill);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_TIME", DbType.Int32, testRecommendationDetail.TimeLimit);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_DESCRIPTION", DbType.String, testRecommendationDetail.TestDescription);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@USER_ID", DbType.Int32, testRecommendationDetail.UserId);
            HCMDatabase.AddOutParameter(insertTestCommand, "@TEST_RECOMMEND_ID",DbType.Int32,0);
            // Execute the stored procedure.
            object returnValue = HCMDatabase.ExecuteNonQuery(insertTestCommand, transaction as DbTransaction);
            returnValue = HCMDatabase.GetParameterValue(insertTestCommand, "@TEST_RECOMMEND_ID");

            if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                return 0;

            return Convert.ToInt32(returnValue.ToString());
        }

        /// <summary>
        /// Method that updates the recommended test detail.
        /// </summary>
        /// <param name="testRecommendationDetail">
        /// A <see cref="TestRecommendationDetail"/> that holds the test recommendation detail.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </returns>
        public void UpdateTestSegmentDetail(TestRecommendationDetail testRecommendationDetail,
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_TEST_SEGMENT_DETAIL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_ID", DbType.Int32, testRecommendationDetail.TestRecommendId);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_TOTAL_QUESTION", DbType.Int16, testRecommendationDetail.NoOfQuestions);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_NAME", DbType.String, testRecommendationDetail.TestName);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_SKILL", DbType.String, testRecommendationDetail.Skill);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_TIME", DbType.Int32, testRecommendationDetail.TimeLimit);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_RECOMMEND_DESCRIPTION", DbType.String, testRecommendationDetail.TestDescription);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@USER_ID", DbType.Int32, testRecommendationDetail.UserId);
            
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method is used to insert Test Recommendation Skill 
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the Creater of the Test.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction.
        /// </param>
        public void InsertTestSegmentSkill(TestRecommendationDetail testRecommendationDetail,
            int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestQuestionsCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_TEST_SEGMENT_SKILL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TEST_RECOMMEND_ID", DbType.Int32, testRecommendationDetail.TestRecommendId);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TEST_SEGMENT_SUB_ID", DbType.Int32, testRecommendationDetail.SubjectId);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TEST_SEGMENT_TESTAREA", DbType.String, testRecommendationDetail.TestArea);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TEST_SEGMENT_COMPLEXITY", DbType.String, testRecommendationDetail.Complexity);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TEST_SEGMENT_KEYWORD", DbType.String, testRecommendationDetail.Keyword);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TEST_SEGMENT_WEIGHTAGE", DbType.Int32, testRecommendationDetail.Weightage);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestQuestionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that deletes the test segment skills for the given test 
        /// recommendation ID.
        /// </summary>
        /// <param name="testRecommendationID">
        /// A <see cref="int"/> that contains the test recommendation ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void DeleteTestSegmentSkills(int testRecommendationID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestQuestionsCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_TEST_SEGMENT_SKILL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TEST_RECOMMEND_ID", DbType.Int32, testRecommendationID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestQuestionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the list of self admin test recommendations for the 
        /// given search parameters.
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="TestRecommendationDetailSearchCriteria"/> that holds the search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestRecommendationDetail"/> that holds the self 
        /// admin test recommendations.
        /// </returns>
        public List<TestRecommendationDetail> GetSelfAdminTestRecommendations
            (TestRecommendationDetailSearchCriteria criteria, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<TestRecommendationDetail> testDetails = null;

            TestRecommendationDetail testDetail = null;

            try
            {
                DbCommand searchCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_SELF_ADMIN_TEST_RECOMMENDATIONS");

                // Add parameters.
                HCMDatabase.AddInParameter(searchCommand,
                    "@TEST_NAME", DbType.String, Utility.IsNullOrEmpty(criteria.TestName) ? null : criteria.TestName.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@TEST_DESCRIPTION", DbType.String, Utility.IsNullOrEmpty(criteria.TestDescription) ? null : criteria.TestDescription.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@SKILL", DbType.String, Utility.IsNullOrEmpty(criteria.Skill) ? null : criteria.Skill.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@ACTIVE", DbType.String, Utility.IsNullOrEmpty(criteria.Status) ? null : criteria.Status.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@PAGE_NUM", DbType.String, pageNumber);

                HCMDatabase.AddInParameter(searchCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY_DIRECTION", DbType.String, sordOrder == SortType.Ascending ? "A" : "D");

                dataReader = HCMDatabase.ExecuteReader(searchCommand);

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestRecommendationDetail>();

                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        testDetail = new TestRecommendationDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_ID"]))
                            testDetail.TestRecommendId = Convert.ToInt32(dataReader["TEST_RECOMMEND_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TOTAL_QUESTION"]))
                            testDetail.NoOfQuestions = Convert.ToInt32(dataReader["TEST_RECOMMEND_TOTAL_QUESTION"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_NAME"]))
                            testDetail.TestName = dataReader["TEST_RECOMMEND_NAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_DESCRIPTION"]))
                            testDetail.TestDescription = dataReader["TEST_RECOMMEND_DESCRIPTION"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_SKILL"]))
                            testDetail.Skill = dataReader["TEST_RECOMMEND_SKILL"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TIME"]))
                            testDetail.TimeLimit = Convert.ToInt32(dataReader["TEST_RECOMMEND_TIME"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_COUNT"]))
                        {
                            if (Convert.ToInt32(dataReader["TEST_COUNT"].ToString().Trim()) != 0)
                                testDetail.IsTestUsed = true;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]) && dataReader["ACTIVE"].ToString().Trim().ToUpper() == "Y")
                            testDetail.IsActive = true;

                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the self admin test recommendation status
        /// (either active or inactive).
        /// </summary>
        /// <param name="testRecommendationID">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateSelfAdminTestRecommendationStatus(int testRecommendationID, 
            string status, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateStatusCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_SELF_ADMIN_TEST_RECOMMENDATION_STATUS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateStatusCommand,
                "@TEST_RECOMMEND_ID", DbType.Int32, testRecommendationID);
            HCMDatabase.AddInParameter(updateStatusCommand,
                "@ACTIVE_STATUS", DbType.String, status);
            HCMDatabase.AddInParameter(updateStatusCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateStatusCommand);
        }

        /// <summary>
        /// Method that retrieves the recommended test and criteria segments for the 
        /// given test recommendation ID.
        /// </summary>
        /// <param name="testRecommendation">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestRecommendationDetail"/> that holds test and criteria segment
        /// details.
        /// </returns>
        public TestRecommendationDetail GetTestSegmentDetail(int testRecommendation)
        {
            IDataReader dataReader = null;

            TestRecommendationDetail testDetail = null;
            TestRecommendationSegment segment = null;

            try
            {
                DbCommand searchCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_SEGMENT_DETAIL");

                // Add parameters.
                HCMDatabase.AddInParameter(searchCommand,
                    "@TEST_RECOMMEND_ID", DbType.Int32, testRecommendation);

                dataReader = HCMDatabase.ExecuteReader(searchCommand);

                // Read test detail.
                if (dataReader.Read())
                {
                    // Instantiate test recommendation object.
                    testDetail = new TestRecommendationDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_ID"]))
                        testDetail.TestRecommendId = Convert.ToInt32(dataReader["TEST_RECOMMEND_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TOTAL_QUESTION"]))
                        testDetail.NoOfQuestions = Convert.ToInt32(dataReader["TEST_RECOMMEND_TOTAL_QUESTION"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_NAME"]))
                        testDetail.TestName = dataReader["TEST_RECOMMEND_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_DESCRIPTION"]))
                        testDetail.TestDescription = dataReader["TEST_RECOMMEND_DESCRIPTION"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_SKILL"]))
                        testDetail.Skill = dataReader["TEST_RECOMMEND_SKILL"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TIME"]))
                        testDetail.TimeLimit = Convert.ToInt32(dataReader["TEST_RECOMMEND_TIME"].ToString().Trim());
                }

                // Read segment details (criteria).
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate test recommendation object.
                    if (testDetail == null)
                        testDetail = new TestRecommendationDetail();

                    // Instantiate test segment list object.
                    if (testDetail.Segments == null)
                        testDetail.Segments = new List<TestRecommendationSegment>();

                    // Instantiate the segment object.
                    segment = new TestRecommendationSegment();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_ID"]))
                        segment.TestSegmentID = Convert.ToInt32(dataReader["TEST_SEGMENT_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_SUB_ID"]))
                        segment.CateogorySubjectID = Convert.ToInt32(dataReader["TEST_SEGMENT_SUB_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_TESTAREA"]))
                        segment.TestArea = dataReader["TEST_SEGMENT_TESTAREA"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_COMPLEXITY"]))
                        segment.Complexity = dataReader["TEST_SEGMENT_COMPLEXITY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_KEYWORD"]))
                        segment.Keyword = dataReader["TEST_SEGMENT_KEYWORD"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SEGMENT_WEIGHTAGE"]))
                        segment.Weightage = Convert.ToInt32(dataReader["TEST_SEGMENT_WEIGHTAGE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                        segment.Subject = dataReader["SUBJECT_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                        segment.Category = dataReader["CATEGORY_NAME"].ToString().Trim();

                    // Add to the segment to the list.
                    testDetail.Segments.Add(segment);
                }

                return testDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of candidates associated with a self admin
		/// test for the given search parameters
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="TestRecommendationDetailSearchCriteria"/> that holds the search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestRecommendationDetail"/> that holds the candidate details.
        /// </returns>
        public List<TestRecommendationDetail> GetSelfAdminTestCandidates
            (TestRecommendationDetailSearchCriteria criteria, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<TestRecommendationDetail> testDetails = null;

            TestRecommendationDetail testDetail = null;

            try
            {
                DbCommand searchCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_SELF_ADMIN_TEST_CANDIDATES");

                // Add parameters.
                HCMDatabase.AddInParameter(searchCommand,
                    "@TEST_RECOMMEND_ID", DbType.Int32, criteria.TestRecommendId);

                HCMDatabase.AddInParameter(searchCommand,
                    "@STATUS", DbType.String, Utility.IsNullOrEmpty(criteria.Status) ? null : criteria.Status.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@CANDIDATE_NAME", DbType.String, Utility.IsNullOrEmpty(criteria.CandidateName) ? null : criteria.CandidateName.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@CANDIDATE_EMAIL", DbType.String, Utility.IsNullOrEmpty(criteria.CandidateEmail) ? null : criteria.CandidateEmail.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@PAGE_NUM", DbType.String, pageNumber);

                HCMDatabase.AddInParameter(searchCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY_DIRECTION", DbType.String, sordOrder == SortType.Ascending ? "A" : "D");

                dataReader = HCMDatabase.ExecuteReader(searchCommand);

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestRecommendationDetail>();

                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        testDetail = new TestRecommendationDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                            testDetail.UserId = Convert.ToInt32(dataReader["USER_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                            testDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                            testDetail.CandidateEmail = dataReader["EMAIL"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                            testDetail.Status = dataReader["STATUS"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["STATUS_NAME"]))
                            testDetail.StatusName = dataReader["STATUS_NAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                            testDetail.TestKey = dataReader["TEST_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                            testDetail.SessionKey = dataReader["SESSION_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["CAND_SESSION_KEY"]))
                            testDetail.CandidateSessionKey = dataReader["CAND_SESSION_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                            testDetail.TestKey = dataReader["TEST_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                            testDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                            testDetail.CompletedDate = Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_STATUS"]))
                            testDetail.SessionStatus = dataReader["SESSION_STATUS"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                        {
                            testDetail.AnsweredCorrectly =
                                Convert.ToDecimal(dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                        {
                            testDetail.TimeTaken =
                                dataReader["TIME_TAKEN"].ToString().Trim().Length == 0 ? "00:00:00" :
                                Utility.ConvertSecondsToHoursMinutesSeconds(
                                int.Parse(dataReader["TIME_TAKEN"].ToString()));
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                        {
                            decimal avgTimeTaken = Convert.ToDecimal
                            (dataReader["AVERAGE_TIME_TAKEN"].ToString());

                            int avgTimeTakenInt = Convert.ToInt32
                            (avgTimeTaken);

                            testDetail.AverageTimeTaken =
                                dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim().Length == 0 ? "0" :
                            Utility.ConvertSecondsToHoursMinutesSeconds(avgTimeTakenInt);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE_ANSWER"]))
                        {
                            testDetail.AbsoluteScore =
                                dataReader["ABSOLUTE_SCORE_ANSWER"].ToString().Trim().Length == 0 ?
                                0 : Convert.ToDecimal(dataReader["ABSOLUTE_SCORE_ANSWER"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                        {
                            testDetail.RelativeScore =
                                dataReader["RELATIVE_SCORE"].ToString().Trim().Length == 0 ?
                                0 : Convert.ToDecimal(dataReader["RELATIVE_SCORE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                        {
                            testDetail.Percentile =
                                decimal.Parse(dataReader["PERCENTILE"].ToString());
                        }

                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        #endregion Public Methods
    }
}
