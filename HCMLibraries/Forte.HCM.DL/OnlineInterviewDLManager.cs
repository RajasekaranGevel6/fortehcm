﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewSessionDLManager.cs
// File that represents the data layer for the Interview Session module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;
using System.Globalization;

#endregion
namespace Forte.HCM.DL
{
    public class OnlineInterviewDLManager : DatabaseConnectionManager
    {
        #region Public Method

        /// <summary>
        /// Inserts Online Interview Test session details in [ONLINE_INTERVIEW_TEST_SESSION] table object
        /// </summary>
        /// <param name="onlineInterviewSessionDetail"></param>
        /// <param name="userID"></param>
        /// <param name="transaction"></param>
        public void InsertOnlineInterviewSession(OnlineInterviewSessionDetail onlineInterviewSessionDetail,
            int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertTestSessionCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_SESSION");

            // Add input parameters
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@INTERVIEW_SESSION_KEY", DbType.String, onlineInterviewSessionDetail.OnlineInterviewSessionKey);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@SESSION_COUNT", DbType.Int32, onlineInterviewSessionDetail.NumberOfCandidateSessions);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                 "@CLIENT_REQUEST_NUMBER", DbType.Int32, onlineInterviewSessionDetail.PositionProfileID);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@SESSION_EXPIRY", DbType.DateTime, onlineInterviewSessionDetail.ExpiryDate == DateTime.MinValue ? null : onlineInterviewSessionDetail.SessionExpiryDate);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@USER_ID", DbType.Int32, onlineInterviewSessionDetail.CreatedBy);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
               "@ASSESSOR_IDS", DbType.String, onlineInterviewSessionDetail.AssessorIDs);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertTestSessionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Inserts online candidate session details into [ONLINE_INTERVIEW_SESSION_CANDIDATE] and 
        /// ONLINE_INTERVIEW_CANDIDATE_SESSION_TRACKING
        /// </summary>
        /// <param name="candidateOnlineInterviewSessionDetail"></param>
        /// <param name="transaction"></param>
        public void InsertCandidateOnlineInterviewSession(CandidateInterviewSessionDetail 
            candidateOnlineInterviewSessionDetail, string assessorIds,IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertCandidateTestSession
                = HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_SESSION_CANDIDATE");
            // Add input parameter
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateOnlineInterviewSessionDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@INTERVIEW_SESSION_KEY", DbType.String, candidateOnlineInterviewSessionDetail.InterviewTestSessionID);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@STATUS", DbType.String, candidateOnlineInterviewSessionDetail.Status);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@CREATED_BY", DbType.Int32, candidateOnlineInterviewSessionDetail.CreatedBy);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@MODIFIED_BY", DbType.Int32, candidateOnlineInterviewSessionDetail.ModifiedBy);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
               "@ASSESSOR_IDS", DbType.String, assessorIds);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertCandidateTestSession, transaction as DbTransaction);
        }

        /// <summary>
        /// Inserts Online Interview session description details in ONLINE_INTERVIEW_TEST_SESSION_DESC table
        /// </summary>
        /// <param name="OnlineInterviewSessionDetail"></param>
        /// <param name="transaction"></param>
        public void InsertOnlineInterviewSessionDescription(OnlineInterviewSessionDetail OnlineInterviewSessionDetail, 
            IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertTestDescriptionCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_TEST_SESSION_DESC");

            // Add input parameter
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@INTERVIEW_SESSION_KEY", DbType.String, OnlineInterviewSessionDetail.OnlineInterviewSessionKey);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@INTERVIEW_SESSION_NAME", DbType.String, OnlineInterviewSessionDetail.OnlineInterviewSessionName);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@INTERVIEW_SESSION_DESC", DbType.String, OnlineInterviewSessionDetail.OnlineInterviewSessionDescription);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@INSTRUCTIONS", DbType.String, OnlineInterviewSessionDetail.OnlineInterviewInstruction);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, OnlineInterviewSessionDetail.PositionProfileID);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@SKILL_DETAIL", DbType.String, OnlineInterviewSessionDetail.Skills);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@USER_ID", DbType.Int32, OnlineInterviewSessionDetail.CreatedBy);

            HCMDatabase.ExecuteNonQuery(insertTestDescriptionCommand, transaction as DbTransaction);
        }

        public TestSessionDetail GetOnlineInterviewCandidateSessions(string interviewSessionID)
        {
            IDataReader dataReader = null;

            try
            {
                // Check if object type is given.
                if (Utility.IsNullOrEmpty(interviewSessionID))
                {
                    throw new Exception
                        ("Online interview session ID cannot be null or empty");
                }
                TestSessionDetail testSessionDetail = null;

                DbCommand getCandidateSessionsCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_CANDIDATE_SESSIONS");

                HCMDatabase.AddInParameter(getCandidateSessionsCommand,
                    "@INTERVIEW_SESSION_ID", DbType.String, interviewSessionID.Trim());

                HCMDatabase.AddOutParameter(getCandidateSessionsCommand,
                  "@INTERVIEW_CANDIDATE_SESSIONS", DbType.String, 1000);

                dataReader = HCMDatabase.ExecuteReader
                    (getCandidateSessionsCommand);
                if (dataReader.Read())
                {
                    testSessionDetail = new TestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                        testSessionDetail.UserName = dataReader["NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        testSessionDetail.Email = dataReader["EMAIL"].ToString();
                }
                if (dataReader != null)
                    dataReader.Close();

                if (testSessionDetail != null)
                {
                    testSessionDetail.CandidateSessions = Convert.ToString
                        (HCMDatabase.GetParameterValue(getCandidateSessionsCommand, "@INTERVIEW_CANDIDATE_SESSIONS"));
                }
                return testSessionDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method searches the online interview session details by passing SessionName,
        /// SessionKey,ClientRequestNumber or SchedulerName,Skills
        /// </summary>
        /// <param name="onlineInterviewSessionSearchCriteria">
        /// A list of<see cref="OnlineInterviewSessionSearchCriteria"/> that holds the online interview session criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>that holds the page Number
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/>that holds the page Size
        /// </param>
        /// <param name="totalRecords">
        ///  A <see cref="int"/>that holds the total Records
        /// </param>
        /// <param name="sortingKey">
        ///  A <see cref="string"/>that holds the sorting Key 
        /// </param>
        /// <param name="sortByDirection">
        ///  A <see cref="SortType"/>that holds the sortBy Direction Like 'A' or 'D'(Asc/dec)
        /// </param>
        /// <returns>
        ///  A list of <see cref="OnlineInterviewSessionDetail"/>that holds the Online Interview Session Detail
        /// </returns>
        public List<OnlineInterviewSessionDetail> SearchOnlineInterviewSessionDetails
            (OnlineInterviewSessionSearchCriteria onlineInterviewSessionCriteria, int pageNumber,
            int pageSize, out int totalRecords,
            string sortingKey, SortType sortByDirection)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SESSION_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSIONNAME", DbType.String, onlineInterviewSessionCriteria.SessionName);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_AUTHOR_ID", DbType.Int32, onlineInterviewSessionCriteria.SessionCreatorID == 0 ? null : onlineInterviewSessionCriteria.SessionCreatorID);
                if (onlineInterviewSessionCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, onlineInterviewSessionCriteria.PositionProfileID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSIONID", DbType.String, onlineInterviewSessionCriteria.SessionKey);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SHEDULER_ID", DbType.String, onlineInterviewSessionCriteria.SchedulerNameID == 0 ? null : onlineInterviewSessionCriteria.SchedulerNameID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                   "@SKILL_DETAIL", DbType.String, onlineInterviewSessionCriteria.Skill);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBY",
                    DbType.String, sortingKey);

                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBYDIRECTION",
                    DbType.String, sortByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                List<OnlineInterviewSessionDetail> onlineInterviewSessionCollection = null;

                OnlineInterviewSessionDetail onlineInterviewSession = null;
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (onlineInterviewSessionCollection == null)
                            onlineInterviewSessionCollection = new List<OnlineInterviewSessionDetail>();

                        onlineInterviewSession = new OnlineInterviewSessionDetail();
                        onlineInterviewSession.OnlineInterviewSessionName = dataReader["SESSION_NAME"].ToString();
                        onlineInterviewSession.OnlineInterviewSessionKey = dataReader["SESSION_KEY"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_ID"]))
                            onlineInterviewSession.SessionAuthorID = Convert.ToInt32(dataReader["AUTHOR_ID"].ToString());

                        onlineInterviewSession.SessionAuthor = dataReader["AUTHOR_NAME"].ToString();
                        onlineInterviewSession.SessionAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        onlineInterviewSession.Skills = dataReader["SKILL_DETAIL"].ToString().Trim();
                        onlineInterviewSession.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());
                        // Add the testSession to the collection.
                        onlineInterviewSessionCollection.Add(onlineInterviewSession);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return onlineInterviewSessionCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will return the online interview session and candidate session details
        /// based on the search criteria.
        /// </summary>
        /// <param name="scheduleCandidateSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search criteria details.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that contains the sort expression.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="SortType"/> that contains the sort type either ASC/DESC.
        /// </param>
        /// <returns>
        /// A <see cref="OnlineInterviewSessionDetail"/> that contains the online interview session details information.
        /// </returns>
        public OnlineInterviewSessionDetail GetOnlineInterviewSessionDetail(TestScheduleSearchCriteria scheduleCandidateSearchCriteria,
            string sortExpression, SortType sortDirection)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_CANDIDATE_SESSION_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_KEY", DbType.String, scheduleCandidateSearchCriteria.TestSessionID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@CAND_SESSION_KEYS", DbType.String, scheduleCandidateSearchCriteria.CandidateSessionIDs);
                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBY", DbType.String, sortExpression);
                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBYDIRECTION",
                    DbType.String, sortDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                OnlineInterviewSessionDetail onlineInterviewSession = null;
                List<CandidateInterviewSessionDetail> candidateTestSessions = null;
                while (dataReader.Read())
                {
                    // Instantiate the testSession instance
                    if (onlineInterviewSession == null)
                        onlineInterviewSession = new OnlineInterviewSessionDetail();

                    // Assign property values to the online interview object.

                    onlineInterviewSession.OnlineInterviewSessionKey = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                    onlineInterviewSession.OnlineInterviewSessionName = dataReader["INTERVIEW_SESSION_NAME"].ToString().Trim();
                    if(dataReader["SESSION_EXPIRY"].ToString()!=string.Empty)
                        onlineInterviewSession.ExpiryDate = Convert.ToDateTime(dataReader["SESSION_EXPIRY"].ToString());

                    onlineInterviewSession.OnlineInterviewInstruction = dataReader["INSTRUCTIONS"].ToString();
                    onlineInterviewSession.OnlineInterviewSessionDescription = dataReader["INTERVIEW_SESSION_DESC"].ToString();
                    onlineInterviewSession.Skills = dataReader["SKILL_DETAIL"].ToString();
                    onlineInterviewSession.SessionAuthor = dataReader["SESSION_AUTHOR_NAME"].ToString().Trim();
                    onlineInterviewSession.SessionAuthorEmail = dataReader["SESSION_AUTHOR_EMAIL"].ToString().Trim();
                    onlineInterviewSession.NumberOfCandidateSessions = Convert.ToInt32(dataReader["SESSION_COUNT"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        onlineInterviewSession.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        onlineInterviewSession.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        onlineInterviewSession.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        onlineInterviewSession.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString());
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the candidate online interview session collection.
                    if (candidateTestSessions == null)
                        candidateTestSessions = new List<CandidateInterviewSessionDetail>();
                    // Create a new candidate online interview session object. 
                    CandidateInterviewSessionDetail candidateTestSession = new CandidateInterviewSessionDetail();
                    candidateTestSession.CandidateTestSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();
                    candidateTestSession.Status = dataReader["STATUS"].ToString();
                    //candidateTestSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());
                    candidateTestSession.RetakeRequest = dataReader["RETAKE_REQUEST"].ToString();
                    //candidateTestSession.CancelReason = dataReader["CANCEL_REASON"].ToString();
                    // If status is 'Not Scheduled', then don't assign the candidate informations
                    if (candidateTestSession.Status != "SESS_NSCHD")
                    {
                        candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                        candidateTestSession.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candidateTestSession.CandidateFirstName = dataReader["CANDIDATE_FIRSTNAME"].ToString().Trim();
                        candidateTestSession.CandidateFullName = dataReader["CANDIDATE_FULLNAME"].ToString().Trim();
                        candidateTestSession.Email = dataReader["EMAIL_ID"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();

                        //if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                        //    candidateTestSession.DateCompleted = Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                        //if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                        //    candidateTestSession.ScheduledDate =
                        //        Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                    }
                    //if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                    //    candidateTestSession.ModifiedDate =
                    //        Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                    candidateTestSessions.Add(candidateTestSession);
                }
                onlineInterviewSession.CandidateOnlineInterviewSessions = new List<CandidateInterviewSessionDetail>();
                onlineInterviewSession.CandidateOnlineInterviewSessions = candidateTestSessions;
                return onlineInterviewSession;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method passes the SessionKey and CandidateSession keys separated by comma 
        /// and return the invalid candidateSession keys.
        /// </summary>
        /// <param name="sessionKey">
        /// A <see cref="string"/>that holds the session Key
        /// </param>
        /// <param name="candidateSessionKeys">
        /// A <see cref="string"/>that holds the candidate Session Keys
        /// </param>
        /// <returns>
        /// A List of <see cref="String"/>that holds mismatched Candidate Ids.
        /// </returns>
        public List<string> ValidateOnlineInterviewSessionInput
            (string sessionKey, string candidateSessionKeys)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCommand = HCMDatabase.
                    GetStoredProcCommand("SP_VALIDATE_ONLINE_INTERVIEW_SESSION_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCommand,
                    "@SESSION_KEY", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCommand,
                    "@CAND_SESSION_KEYS", DbType.String, candidateSessionKeys);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCommand);

                List<string> mismatchedCandIdCollection = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {

                    // Instantiate the testSession instance.
                    if (mismatchedCandIdCollection == null)
                        mismatchedCandIdCollection = new List<string>();

                    if (dataReader["NOT_MATCHED_CAND_SESSION_KEY"].ToString() == "-1")
                    {
                        throw new Exception("Online Interview Session Id not found");
                    }
                    // Add the testSession to the collection.
                    mismatchedCandIdCollection.Add(dataReader["NOT_MATCHED_CAND_SESSION_KEY"].ToString());

                }
                return mismatchedCandIdCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Inserts assessor and skill id into [ONLINE_INTERVIEW_TEST_SESSION_ASSESSOR] 
        /// </summary>
        /// <param name="interviewSessionKey">interviewSessionKey</param>
        /// <param name="assessorId">assessorId</param>
        /// <param name="skillIds">skillIds</param>
        /// <param name="userID">userID</param>
        /// <param name="transaction"></param>
        public void InsertOnlinerInterviewSessionAssessorSkills(string interviewSessionKey,int assessorId, 
            string skillIds, int userID, IDbTransaction transaction)
        {
            AssessorDetail assessorDetails = new AssessorDetail();
            // Create command object
            DbCommand insertAssessorSkill
                = HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_ASSESSOR_AND_SKILL");
            // Add input parameter
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionKey);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@ASSESSOR_ID", DbType.Int32, assessorId);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@SKILL_ID", DbType.String, skillIds);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@CREATED_BY", DbType.Int32, userID);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAssessorSkill, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will update the scheduled candidate details for online interview.
        /// </summary>
        /// <param name="onlineCandidateSessionDetail">
        /// A <see cref="OnlineCandidateSessionDetail"/> that contains the schedule detail.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <remarks>
        public void UpdateOnlineInterviewScheduleCandidate(OnlineCandidateSessionDetail onlineCandidateSessionDetail, 
            int modifiedBy, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand updateOnlineScheduleCandCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_SCHEDULE_CANDIDATE_ONLINE_INTERVIEW");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, onlineCandidateSessionDetail.CandidateSessionID);
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@CANDIDATE_ID", DbType.Int32, Convert.ToInt32(onlineCandidateSessionDetail.CandidateID));
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@EMAILID", DbType.String, onlineCandidateSessionDetail.EmailId);
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateOnlineScheduleCandCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will insert the scheduled time slots to ONLINE_INTERVIEW_CANDIDATE_SESSION_TRACKING for online interview.
        /// </summary>
        /// <param name="ossessorTimeSlotDetail">
        /// A <see cref="AssessorTimeSlotDetail"/> that contains the schedule detail.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <remarks>
        public void InsertOnlineInterviewScheduleCandidateTimeSlots(AssessorTimeSlotDetail assessorTimeSlotDetail,
            string candidateSessionKey,int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand updateOnlineScheduleCandCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_SCHEDULE_CANDIDATE_TIMESLOTS_ONLINE_INTERVIEW");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@ASSESSOR_ID", DbType.String, assessorTimeSlotDetail.AssessorID);
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@SKILL_ID", DbType.Int32, Convert.ToInt32(assessorTimeSlotDetail.SkillID));
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@TIME_SLOT_ID", DbType.Int32, Convert.ToInt32(assessorTimeSlotDetail.TimeSlotID));
            HCMDatabase.AddInParameter(updateOnlineScheduleCandCommand,
                "@CREATED_BY", DbType.Int32, userID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateOnlineScheduleCandCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will get the candidate details.
        /// </summary>
        /// <param name="onlineCandidateSessionDetail">
        /// A <see cref="OnlineCandidateSessionDetail"/> that contains the candidate session detail.
        /// </param>
        public OnlineCandidateSessionDetail GetCandidateDetailsByCandidateSessionKey(string candidateSessionKey)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getContributorQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_DETAILS_BY_CANDIDATE_SESSION_KEY");

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);

                dataReader = HCMDatabase.ExecuteReader(getContributorQuestionCommand);

                OnlineCandidateSessionDetail onlineCandidateSessionDetail = null;

                // Retreive candidate details.
                if (dataReader.Read())
                {
                    // Instantiate candidate detail object.
                    onlineCandidateSessionDetail = new OnlineCandidateSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        onlineCandidateSessionDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        onlineCandidateSessionDetail.CandidateEmail = dataReader["EMAIL_ID"].ToString();
                }
                return onlineCandidateSessionDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that checks if assessor is expertise in the given skill set
        /// parameters.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="string"/> that holds the skill ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the expertise status. If 
        /// available returns true and otherwise false.
        /// </returns>
        public bool IsAssessorExpertise(int assessorID,int skillID)
        {
            IDataReader dataReader = null;
            int total = 0;

            try
            {
                DbCommand getTimeSlots =
                    HCMDatabase.GetStoredProcCommand("SPGET_SKILL_EXPERTISE");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);
                HCMDatabase.AddInParameter(getTimeSlots,
                   "@SKILL_ID", DbType.Int32, skillID);

                dataReader = HCMDatabase.ExecuteReader(getTimeSlots);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        total = Convert.ToInt32(dataReader["TOTAL"].ToString().Trim());
                }

                if (total == 0)
                    return false;
                else
                    return true;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Inserts Online Interview details in [ONLINE_INTERVIEW] table object
        /// </summary>
        /// <param name="onlineInterviewSessionDetail">
        /// A <see cref="OnlineInterviewSessionDetail"/> that holds the online interview detail.
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertOnlineInterview(OnlineInterviewSessionDetail 
            onlineInterviewSessionDetail, int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertOnlineInterviewCommand = 
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW");

            // Add input parameters
            HCMDatabase.AddInParameter(insertOnlineInterviewCommand,
                "@ONLINE_INTERVIEW_KEY", DbType.String, onlineInterviewSessionDetail.Interviewkey);

            HCMDatabase.AddInParameter(insertOnlineInterviewCommand,
                "@ONLINE_INTERVIEW_NAME", DbType.String, onlineInterviewSessionDetail.InterviewName);

            if (onlineInterviewSessionDetail.PositionProfileID == 0)
            {
                HCMDatabase.AddInParameter(insertOnlineInterviewCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertOnlineInterviewCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, onlineInterviewSessionDetail.PositionProfileID);
            }

            HCMDatabase.AddInParameter(insertOnlineInterviewCommand,
                "@INSTRUCTION", DbType.String, onlineInterviewSessionDetail.InterviewInstruction);

            HCMDatabase.AddInParameter(insertOnlineInterviewCommand,
                "@REMARKS", DbType.String, onlineInterviewSessionDetail.InterviewRemarks);

            HCMDatabase.AddInParameter(insertOnlineInterviewCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertOnlineInterviewCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Insert Online Interview Skill details in [ONLINE_INTERVIEW_SKILL] table object
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds interview key
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertOnlineInterviewSkill(SkillDetail skillDetail, string interviewKey,
            int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertOnlineInterviewSkillCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_SKILL");

            // Add input parameters
            HCMDatabase.AddInParameter(insertOnlineInterviewSkillCommand,
                "@ONLINE_INTERVIEW_KEY", DbType.String, interviewKey);

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillCommand,
                "@SKILL_ID", DbType.Int32, skillDetail.SkillID);

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillCommand,
                "@WEIGHTAGE", DbType.Int32, skillDetail.Weightage);

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillCommand,
                "@SKILL_LEVEL", DbType.String, skillDetail.SkillLevel);

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillCommand,
                 "@USER_ID", DbType.Int32, userID);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertOnlineInterviewSkillCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Delete the online interview skill details in
        /// [ONLINE_INTERVIEW_SKILL] table object
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds interview key
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        public void DeleteOnlineInterviewSkill(string interviewKey, int skillID)
        {
            // Create command object 
            DbCommand deleteOnlineInterviewSkillCommand =
                HCMDatabase.GetStoredProcCommand("SPDELETE_ONLINE_INTERVIEW_SKILL");

            // Add input parameters
            HCMDatabase.AddInParameter(deleteOnlineInterviewSkillCommand,
                "@ONLINE_INTERVIEW_KEY", DbType.String, interviewKey);

            HCMDatabase.AddInParameter(deleteOnlineInterviewSkillCommand,
                "@SKILL_ID", DbType.Int32, skillID);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(deleteOnlineInterviewSkillCommand);
        }

        /// <summary>
        /// This method searches the online interview details by passing interview name,
        /// position profile id, author id
        /// </summary>
        /// <param name="onlineInterviewSessionSearchCriteria">
        /// A list of<see cref="OnlineInterviewSessionSearchCriteria"/> that holds the online interview session criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>that holds the page Number
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/>that holds the page Size
        /// </param>
        /// <param name="totalRecords">
        ///  A <see cref="int"/>that holds the total Records
        /// </param>
        /// <param name="sortingKey">
        ///  A <see cref="string"/>that holds the sorting Key 
        /// </param>
        /// <param name="sortByDirection">
        ///  A <see cref="SortType"/>that holds the sortBy Direction Like 'A' or 'D'(Asc/dec)
        /// </param>
        /// <returns>
        ///  A list of <see cref="OnlineInterviewSessionDetail"/>that holds the Online Interview Session Detail
        /// </returns>
        public List<OnlineInterviewSessionDetail> GetOnlineInterviewDetail
            (OnlineInterviewSessionSearchCriteria onlineInterviewSessionCriteria, int pageNumber,
            int pageSize, out int totalRecords, string sortingKey, SortType sortByDirection)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getOnlineInterviewCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@INTERVIEW_KEY", DbType.String, onlineInterviewSessionCriteria.Interviewkey);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@INTERVIEW_NAME", DbType.String, onlineInterviewSessionCriteria.InterviewName);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@TENANT_ID", DbType.Int32, onlineInterviewSessionCriteria.SearchTenantID);

                if (onlineInterviewSessionCriteria.PositionProfileID == 0)
                {
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@POSITION_PROFILE_ID",
                        DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@POSITION_PROFILE_ID",
                        DbType.Int32, onlineInterviewSessionCriteria.PositionProfileID);
                }

                if (onlineInterviewSessionCriteria.InterviewCreatorID == 0)
                {
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@INTERVIEW_AUTHOR_ID",
                        DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@INTERVIEW_AUTHOR_ID", 
                        DbType.Int32, onlineInterviewSessionCriteria.InterviewCreatorID);
                }

                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@ORDERBY",
                    DbType.String, sortingKey);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@ORDERBYDIRECTION",
                    DbType.String, sortByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getOnlineInterviewCommand);

                List<OnlineInterviewSessionDetail> onlineInterviewSessionCollection = null;

                OnlineInterviewSessionDetail onlineInterviewSession = null;
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (onlineInterviewSessionCollection == null)
                            onlineInterviewSessionCollection = new List<OnlineInterviewSessionDetail>();

                        onlineInterviewSession = new OnlineInterviewSessionDetail();
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_KEY"]))
                        {
                            onlineInterviewSession.Interviewkey = dataReader["INTERVIEW_KEY"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                        {
                            onlineInterviewSession.InterviewName = dataReader["INTERVIEW_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_AUTHOR_NAME"]))
                        {
                            onlineInterviewSession.InterviewAuthorName = dataReader["INTERVIEW_AUTHOR_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_DETAIL"]))
                        {
                            onlineInterviewSession.Skills = dataReader["SKILL_DETAIL"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                        {
                            onlineInterviewSession.TotalQuestion = 
                                Convert.ToInt32(dataReader["TOTAL_QUESTION"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            onlineInterviewSession.CreatedDate =
                                Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());
                        }
                        // Add the testSession to the collection.
                        onlineInterviewSessionCollection.Add(onlineInterviewSession);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return onlineInterviewSessionCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of skills for the given 
        /// interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetSkillByOnlineInterviewKey(string interviewKey)
        {
            List<SkillDetail> skills = null;
            IDataReader dataReader = null;

            try
            {
                DbCommand getSkillCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SKILLS_BY_INTERVIEW_KEY");

                HCMDatabase.AddInParameter(getSkillCommand, "@INTERVIEW_KEY", DbType.String,interviewKey);

                dataReader = HCMDatabase.ExecuteReader(getSkillCommand);

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skills == null)
                        skills = new List<SkillDetail>();

                    // Create a skill object.
                    SkillDetail skill = new SkillDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        skill.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skill.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]))
                    {
                        skill.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_LEVEL"]))
                    {
                        skill.SkillLevel = dataReader["SKILL_LEVEL"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_QUESTIONS"]))
                    {
                        skill.SkillQuestionCount = 
                            Convert.ToInt32(dataReader["NO_OF_QUESTIONS"].ToString());
                    }

                    // Add to the list.
                    skills.Add(skill);
                }

                return skills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }


        /// <summary>
        /// Method that will return the online interview details
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that contains the interview key.
        /// </param>
        /// <returns>
        /// A <see cref="OnlineInterviewSessionDetail"/> that contains the online interview details.
        /// </returns>
        public OnlineInterviewSessionDetail GetOnlineInterviewDetailByKey(string interviewKey)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getInterviewDetailCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_DETAIL_INTERVIEW_KEY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getInterviewDetailCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getInterviewDetailCommand);

                OnlineInterviewSessionDetail onlineInterviewDetail = null;
                List<SkillDetail> skills = null;
                if (dataReader.Read())
                {
                    // Instantiate the testSession instance
                    if (onlineInterviewDetail == null)
                        onlineInterviewDetail = new OnlineInterviewSessionDetail();

                    // Assign property values to the online interview object.

                    onlineInterviewDetail.InterviewName = dataReader["ONLINE_INTERVIEW_NAME"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        onlineInterviewDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        onlineInterviewDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    onlineInterviewDetail.InterviewInstruction = dataReader["INSTRUCTION"].ToString().Trim();
                    onlineInterviewDetail.InterviewRemarks = dataReader["REMARKS"].ToString().Trim();
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skills == null)
                        skills = new List<SkillDetail>();

                    // Create a skill object.
                    SkillDetail skill = new SkillDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        skill.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skill.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]))
                    {
                        skill.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_LEVEL"]))
                    {
                        skill.SkillLevel = dataReader["SKILL_LEVEL"].ToString();
                    }


                    skills.Add(skill);
                }
                onlineInterviewDetail.skillDetail = new List<SkillDetail>();
                onlineInterviewDetail.skillDetail = skills;
                
                return onlineInterviewDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }



        /// <summary>
        /// Insert Online Interview Skill details in [ONLINE_INTERVIEW_SKILL] table object
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="QuestionDetail"/> that holds question details
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds interview key
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertOnlineInterviewSkillQuestion(QuestionDetail questionDetail,
            string interviewKey, int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertOnlineInterviewSkillQuestionCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_SKILL_QUESTION");

            // Add input parameters
            HCMDatabase.AddInParameter(insertOnlineInterviewSkillQuestionCommand,
                "@ONLINE_INTERVIEW_KEY", DbType.String, interviewKey);

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillQuestionCommand,
                "@SKILL_ID", DbType.Int32, questionDetail.SkillID);

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillQuestionCommand,
                "@QUESTION_KEY", DbType.String, questionDetail.QuestionKey);

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillQuestionCommand,
                "@QUESTION_RELATION_ID", DbType.Int32, questionDetail.QuestionRelationId);

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillQuestionCommand,
                "@IS_MANDATORY", DbType.String, questionDetail.IsMandatory);

            if (questionDetail.Weightage == 0)
            {
                HCMDatabase.AddInParameter(insertOnlineInterviewSkillQuestionCommand,
                    "@WEIGHTAGE", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertOnlineInterviewSkillQuestionCommand,
                    "@WEIGHTAGE", DbType.Int32, questionDetail.Weightage);
            }

            HCMDatabase.AddInParameter(insertOnlineInterviewSkillQuestionCommand,
                 "@USER_ID", DbType.Int32, userID);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertOnlineInterviewSkillQuestionCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the list of interview skill questios for the given 
        /// interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the skill details.
        /// </returns>
        public List<QuestionDetail> GetOnlineInterviewSkillQuestion(string interviewKey, int skillID)
        {
            List<QuestionDetail> skillQuestions = null;
            IDataReader dataReader = null;

            try
            {
                DbCommand getInterviewSkillQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SKILL_QUESTION");

                HCMDatabase.AddInParameter(getInterviewSkillQuestionCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);

                if (skillID == 0)
                {
                    HCMDatabase.AddInParameter(getInterviewSkillQuestionCommand,
                        "@SKILL_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getInterviewSkillQuestionCommand,
                        "@SKILL_ID", DbType.Int32, skillID);
                }

                dataReader = HCMDatabase.ExecuteReader(getInterviewSkillQuestionCommand);

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skillQuestions == null)
                        skillQuestions = new List<QuestionDetail>();

                    // Create a skill object.
                    QuestionDetail skillQuestion = new QuestionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skillQuestion.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skillQuestion.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]))
                    {
                        skillQuestion.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                    {
                        skillQuestion.Question = dataReader["QUESTION_DESC"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_RELATION_ID"]))
                    {
                        skillQuestion.QuestionRelationId = 
                            Convert.ToInt32(dataReader["QUESTION_RELATION_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]))
                    {
                        skillQuestion.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["IS_MANDATORY"]))
                    {
                        skillQuestion.IsMandatory = dataReader["IS_MANDATORY"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        skillQuestion.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        skillQuestion.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                    {
                        skillQuestion.ComplexityName = dataReader["COMPLEXITY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                    {
                        skillQuestion.Complexity = dataReader["COMPLEXITY_ID"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                    {
                        skillQuestion.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                    {
                        skillQuestion.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                    }

                    // Add to the list.
                    skillQuestions.Add(skillQuestion);
                }

                return skillQuestions;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that deletes a skill question.
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <param name="questionRelationID">
        /// A <see cref="int"/> that holds the question relation id.
        /// </param>
        /// <remarks>
        /// This will do a physical deletion.
        /// </remarks>
        public void DeleteOnlineInterviewSkillQuesionByQuestionKey(string interviewKey, 
            int skillID, int questionRelationID)
        {
            // Create command object.
            DbCommand deleteSkillQuestionCommand = 
                HCMDatabase.GetStoredProcCommand("SPDELETE_ONLINE_INTERVIEW_SKILL_QUESTION_BY_QUESTION_KEY");

            // Add parameters.
            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                "@ONLINE_INTERVIEW_KEY", DbType.String, interviewKey);

            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                "@SKILL_ID", DbType.Int32, skillID);

            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                "@QUESTION_RELATION_ID", DbType.Int32, questionRelationID);

            // Execute statement.
            HCMDatabase.ExecuteNonQuery(deleteSkillQuestionCommand);
        }

        /// <summary>
        /// Method that copies the online interview detail against the interview key
        /// </summary>
        /// <param name="parentInterviewKey">
        /// A <see cref="string"/> that holds the parent interview key.
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="copySkill">
        /// A <see cref="string"/> that holds the key to copy skill or not.
        /// </param>
        /// <param name="copyQuestionSet">
        /// A <see cref="string"/> that holds the key to copy question or not.
        /// </param>
        /// <param name="copyAssessor">
        /// A <see cref="string"/> that holds the key to copy assessor or not.
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <remarks>
        /// This will do a copy interview.
        /// </remarks>
        public void CopyOnlineInterview(string parentInterviewKey, string interviewKey,
            string copySkill, string copyQuestionSet, string copyAssessor, int userID,
            IDbTransaction transaction)
        {
            // Create command object.
            DbCommand deleteSkillQuestionCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_COPY_ONLINE_INTERVIEW");

            // Add parameters.
            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                "@PARENT_INTERVIEW_KEY", DbType.String, parentInterviewKey);

            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                "@INTERVIEW_KEY", DbType.String, interviewKey);

            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                "@SKILL", DbType.String, copySkill);

            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                "@QUESTION_SET", DbType.String, copyQuestionSet);

            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                "@ASSESSOR", DbType.String, copyAssessor);

            HCMDatabase.AddInParameter(deleteSkillQuestionCommand,
                 "@USER_ID", DbType.Int32, userID);

            // Execute statement.
            HCMDatabase.ExecuteNonQuery(deleteSkillQuestionCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// This function inserts the online interview schedule information
        /// </summary>
        /// <param name="onlineSchedule">
        /// A <see cref="onlineSchedule"/> that holds the inline schedule information
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public int InsertOnlineInterviewSchedule(OnlineCandidateSessionDetail onlineSchedule,
            int userID, IDbTransaction transaction)
        {
            // Create command object.
            DbCommand InsertScheduleCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_SCHEDULE");

            // Add parameters.
            HCMDatabase.AddInParameter(InsertScheduleCommand,
                "@INTERVIEW_KEY", DbType.String, onlineSchedule.InterviewKey);

            if (onlineSchedule.InterviewDate == DateTime.MinValue)
            {
                HCMDatabase.AddInParameter(InsertScheduleCommand, 
                    "@INTERVIEW_DATE", DbType.DateTime, null);
            }
            else
            {
                HCMDatabase.AddInParameter(InsertScheduleCommand, "@INTERVIEW_DATE",
                    DbType.DateTime, onlineSchedule.InterviewDate);
            }

            HCMDatabase.AddInParameter(InsertScheduleCommand,
                "@CANDIDATE_ID", DbType.Int32, onlineSchedule.CandidateID);

            HCMDatabase.AddInParameter(InsertScheduleCommand,
                "@INTERVIEW_STATUS", DbType.String, onlineSchedule.SessionStatus);

            HCMDatabase.AddInParameter(InsertScheduleCommand,
                "@CHAT_ROOM", DbType.String, onlineSchedule.ChatRoomName);  

            HCMDatabase.AddInParameter(InsertScheduleCommand,
                "@INTERVIEW_START_TIME", DbType.String, onlineSchedule.TimeSlotFrom); 

            HCMDatabase.AddInParameter(InsertScheduleCommand,
                "@INTERVIEW_END_TIME", DbType.String, onlineSchedule.TimeSlotTo);

            HCMDatabase.AddInParameter(InsertScheduleCommand,
                "@REMINDER", DbType.String, onlineSchedule.ReminderType);

            HCMDatabase.AddInParameter(InsertScheduleCommand,
                "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddOutParameter(InsertScheduleCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, int.MaxValue);

            object returnValue = null;

            // Execute statement.
            if (transaction == null)
            {
                returnValue = HCMDatabase.ExecuteNonQuery(InsertScheduleCommand);
            }
            else
            {
                returnValue = HCMDatabase.ExecuteNonQuery(InsertScheduleCommand,
                    transaction as DbTransaction);
            }

            returnValue = HCMDatabase.GetParameterValue(InsertScheduleCommand, "@CAND_ONLINE_INTERVIEW_ID");

            if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                return 0;

            return int.Parse(returnValue.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="interviewkey"></param>
        /// <returns></returns>
        public OnlineCandidateSessionDetail GetOnlineInterviewDateTime(string interviewkey)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getInterviewDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_DATETIME");

                HCMDatabase.AddInParameter(getInterviewDetailCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewkey);

                // Create a skill object.
                OnlineCandidateSessionDetail interviewDetail = null;

                dataReader = HCMDatabase.ExecuteReader(getInterviewDetailCommand);

                while (dataReader.Read())
                {
                    interviewDetail = new OnlineCandidateSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_DATE"]))
                    {
                        interviewDetail.InterviewDate = Convert.ToDateTime(dataReader["ONLINE_INTERVIEW_DATE"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CHAT_ROOM_NAME"]))
                    {
                        interviewDetail.ChatRoomName = dataReader["CHAT_ROOM_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_START_TIME"]))
                    {
                        interviewDetail.TimeSlotFrom = dataReader["INTERVIEW_START_TIME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_END_TIME"]))
                    {
                        interviewDetail.TimeSlotTo = dataReader["INTERVIEW_END_TIME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        interviewDetail.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());
                    } 
                }

                return interviewDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Return the chat room count for given chat room id
        /// </summary>
        /// <param name="chatRoomName">
        /// A <see cref="System.String"/> that holds the chat room name
        /// </param>
        /// <returns>
        /// Returns <see cref="System.Int32"/> that holds the count of chat room
        /// </returns>
        public int GetChatRoomCount(string chatRoomName)
        { 
             IDataReader dataReader = null;
            int roomCount=0;
             try
             {
                 DbCommand getChatRoomNameCommand =
                     HCMDatabase.GetStoredProcCommand("SPCHECK_CHAT_ROOM");

                 HCMDatabase.AddInParameter(getChatRoomNameCommand,
                     "@CHAT_ROOM_NAME", DbType.String, chatRoomName); 

                 dataReader = HCMDatabase.ExecuteReader(getChatRoomNameCommand);

                 while (dataReader.Read())
                 {
                     if (!Utility.IsNullOrEmpty(dataReader["CHAT_ROOM_COUNT"]))
                     {
                         roomCount =Convert.ToInt32(dataReader["CHAT_ROOM_COUNT"]);
                     } 
                 }

                 return roomCount;
             }
             finally
             {
                 if (dataReader != null && !dataReader.IsClosed)
                     dataReader.Close();
             }
        }

        public List<OnlineCandidateSessionDetail> GetOnlineInterviews(string interviewKey,string status, string searchText,
             int pageNumber, int pageSize, string sortingKey, SortType sortByDirection, out int totalRecords)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getOnlineInterviewCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEWS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@INTERVIEW_KEY", DbType.String,  interviewKey);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@STATUS", DbType.String, status);

                if (!Utility.IsNullOrEmpty(searchText))
                    HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@SEARCH_TEXT", DbType.String, searchText);
                 
                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@ORDER_BY",
                    DbType.String, sortingKey);

                HCMDatabase.AddInParameter(getOnlineInterviewCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sortByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getOnlineInterviewCommand);

                List<OnlineCandidateSessionDetail> onlineInterviewSessionCollection = null;

                OnlineCandidateSessionDetail onlineInterviewSession = null;
                
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (onlineInterviewSessionCollection == null)
                            onlineInterviewSessionCollection = new List<OnlineCandidateSessionDetail>();

                        onlineInterviewSession = new OnlineCandidateSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            onlineInterviewSession.ScheduledBy = dataReader["SCHEDULED_BY"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_STATUS"]))
                        {
                            onlineInterviewSession.SessionStatus = dataReader["ONLINE_INTERVIEW_STATUS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_DATE"]))
                        {
                            onlineInterviewSession.InterviewDate = Convert.ToDateTime(dataReader["ONLINE_INTERVIEW_DATE"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSORS"]))
                        {
                            onlineInterviewSession.AssessorName = dataReader["ASSESSORS"].ToString();
                        }
                        onlineInterviewSession.CandidateSessionID = dataReader["ONLINE_INTERVIEW_KEY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        {
                            onlineInterviewSession.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        {
                            onlineInterviewSession.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        {
                            onlineInterviewSession.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        {
                            onlineInterviewSession.EmailId = dataReader["EMAIL"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SLOT_KEY"]))
                        {
                            onlineInterviewSession.SlotKey = 
                                dataReader["SLOT_KEY"].ToString().Trim();
                        }

                        // Add the testSession to the collection.
                        onlineInterviewSessionCollection.Add(onlineInterviewSession);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return onlineInterviewSessionCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method that validates whether generated key is already exists or not
        /// </summary>
        /// <param name="slotKey">
        /// A <see cref="string"/> that contains slot generated key.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains true or false.
        /// </returns>
        public bool IsSlotyKeyExists(string slotKey)
        {

            // Create a stored procedure command object.
            DbCommand getSlotKeyCommand = HCMDatabase.
                 GetStoredProcCommand("SPGET_CANDIDATE_REQUESTED_KEY");

            HCMDatabase.AddInParameter(getSlotKeyCommand,
               "@SLOT_KEY", System.Data.DbType.String, slotKey);

            HCMDatabase.AddOutParameter(getSlotKeyCommand,
               "@ISEXIST", System.Data.DbType.String, 5);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(getSlotKeyCommand);

            if (!Utility.IsNullOrEmpty(getSlotKeyCommand.Parameters["@SLOT_KEY"].Value) &&
                getSlotKeyCommand.Parameters["@ISEXIST"].Value.ToString().ToUpper() == "Y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Method that validates whether generated key is already exists or not
        /// </summary>
        /// <param name="slotKey">
        /// A <see cref="string"/> that contains slot generated key.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains true or false.
        /// </returns>
        public bool IsChatRoomExists(string chatRoom)
        {

            // Create a stored procedure command object.
            DbCommand getSlotKeyCommand = HCMDatabase.
                 GetStoredProcCommand("SPGET_CHAT_ROOM");

            HCMDatabase.AddInParameter(getSlotKeyCommand,
               "@CHAT_ROOM_NAME", System.Data.DbType.String, chatRoom);

            HCMDatabase.AddOutParameter(getSlotKeyCommand,
               "@ISEXIST", System.Data.DbType.String, 5);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(getSlotKeyCommand);

            if (!Utility.IsNullOrEmpty(getSlotKeyCommand.Parameters["@ISEXIST"].Value) &&
                getSlotKeyCommand.Parameters["@ISEXIST"].Value.ToString().ToUpper() == "Y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Method that search for the assessor against the selected date and time criteria
        /// </summary>
        /// <param name="timeSlotCriteria">
        /// A <see cref="AssessorTimeSlotDetail"/> that holds the time slot search criteria.
        /// </param>
        /// <returns>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortType"/> that holds the sort order.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor time slot details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetAssessorAvailableDates(AssessorTimeSlotSearchCriteria timeSlotCriteria,
            int tenantID, string sortField, SortType sordOrder, int pageNumber, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            try
            {
                DbCommand getAvailabilityDateCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSORS_BY_AVAILABLE_DATE");

                // Add parameters.   
                HCMDatabase.AddInParameter(getAvailabilityDateCommand, "@TENANT_ID", DbType.Int32, tenantID);

                if (timeSlotCriteria.FromDate.ToShortDateString() != "1/1/0001")
                    HCMDatabase.AddInParameter(getAvailabilityDateCommand, "@FROM_DATE", DbType.Date, timeSlotCriteria.FromDate);

                if (timeSlotCriteria.ToDate.ToShortDateString() != "1/1/0001")
                    HCMDatabase.AddInParameter(getAvailabilityDateCommand, "@TO_DATE", DbType.Date, timeSlotCriteria.ToDate);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@FROM_TIME_ID", DbType.Int32, timeSlotCriteria.TimeSlotIDFrom);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@TO_TIME_ID", DbType.Int32, timeSlotCriteria.TimeSlotIDTo);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@ORDER_BY_DIRECTION", DbType.String,
                    sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);


                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getAvailabilityDateCommand);

                List<AssessorTimeSlotDetail> timeSlotDetails = null;
                while (dataReader.Read())
                {
                    // Instantiate the list.
                    if (timeSlotDetails == null)
                        timeSlotDetails = new List<AssessorTimeSlotDetail>();

                    AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]))
                    {
                        timeSlot.Assessor = dataReader["ASSESSOR_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                    {
                        timeSlot.AssessorID = Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVAILABILITY_DATE"]))
                    {
                        timeSlot.AvailabilityDate =
                            Convert.ToDateTime(dataReader["AVAILABILITY_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANY_TIME"]))
                    {
                        timeSlot.IsAvailableFullTime = dataReader["ANY_TIME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ACCEPTED_DATE"]))
                    {
                        timeSlot.AcceptedDate =
                            Convert.ToDateTime(dataReader["ACCEPTED_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                    {
                        timeSlot.TimeSlotIDFrom =
                            Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                    {
                        timeSlot.TimeSlotIDTo =
                            Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_FROM"]))
                    {
                        timeSlot.TimeSlotTextFrom = dataReader["TIME_SLOT_TEXT_FROM"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_TO"]))
                    {
                        timeSlot.TimeSlotTextTo = dataReader["TIME_SLOT_TEXT_TO"].ToString().Trim();
                    }
                    // Add to the list.
                    timeSlotDetails.Add(timeSlot);
                }
                return timeSlotDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that gets the list of assessor details against the candidate interview id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview id.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the assessor details.
        /// </returns>
        public List<AssessorDetail> GetOnlineInterviewScheduledAssessorByCandidateID(
            int candidateInterviewID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getAssessorDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SCHEDULE_ASSESSOR_BY_CANIDATE_ID");

                // Add parameters.   
                HCMDatabase.AddInParameter(getAssessorDetailCommand,
                    "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getAssessorDetailCommand);

                List<AssessorDetail> assessorDetails = null;
                while (dataReader.Read())
                {
                    // Instantiate the list.
                    if (assessorDetails == null)
                        assessorDetails = new List<AssessorDetail>();

                    AssessorDetail assessorDetail = new AssessorDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                    {
                        assessorDetail.AssessorId =
                            Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        assessorDetail.FirstName =
                            dataReader["FIRST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                    {
                        assessorDetail.LastName =
                            dataReader["LAST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                    {
                        assessorDetail.UserEmail =
                            dataReader["EMAIL"].ToString().Trim();
                    }

                    // Add to the list.
                    assessorDetails.Add(assessorDetail);
                }
                return assessorDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method that updates the slot key and schedule 
        /// the candidate which is approved by the client
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        /// <param name="requestSlotID">
        /// A <see cref="int"/> that holds the requested ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public string ApproveSlotIDAndSchedule(int candidateInterviewID, 
            int requestSlotID, int userID)
        {
            // Create a stored procedure command object
            DbCommand updateSlotIDCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_MOVE_ONLINE_INTERVIEW_ASSESSOR_SLOTS");

            // Add input parameters
            HCMDatabase.AddInParameter(updateSlotIDCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.String, candidateInterviewID);

            HCMDatabase.AddInParameter(updateSlotIDCommand,
               "@REQUEST_SLOT_ID", DbType.Int32, requestSlotID);

            HCMDatabase.AddInParameter(updateSlotIDCommand,
               "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddOutParameter(updateSlotIDCommand,
                "@CHAT_ROOM_KEY", DbType.String, 10);

            object returnValue = null;

            // Execute command
            returnValue = HCMDatabase.ExecuteNonQuery(updateSlotIDCommand);

            returnValue = HCMDatabase.GetParameterValue(updateSlotIDCommand, "@CHAT_ROOM_KEY");

            if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                return null;

            return returnValue.ToString();
        }

        /// <summary>
        /// Method that unschedule the online interview candidate against the candidate interview id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        public void UnScheduleOlineInterviewCandidate(int candidateInterviewID)
        {
            // Create a stored procedure command object
            DbCommand updateUnScheduleCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_UNSCHEDULE_ONLINE_INTERVIEW_CANDIDATE");

            // Add input parameters
            HCMDatabase.AddInParameter(updateUnScheduleCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.String, candidateInterviewID);

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateUnScheduleCommand);
        }

        /// <summary>
        /// Method that get the scheduled detail
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <returns>
        /// A list of <see cref="OnlineCandidateSessionDetail"/> that holds the candidate session details.
        /// </returns>
        public OnlineCandidateSessionDetail GetCandidateScheduledDetail(int candidateInterviewID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getScheduledCandidateDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SCHEDULED_BY_CANDIDATE_ID");

                // Add parameters.   
                HCMDatabase.AddInParameter(getScheduledCandidateDetailCommand, "@CAND_ONLINE_INTERVIEW_ID", 
                    DbType.Int32, candidateInterviewID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getScheduledCandidateDetailCommand);

                OnlineCandidateSessionDetail onlineInterviewDetail = null;

                if (dataReader.Read())
                {
                    // Instantiate the online inteview session
                    if (onlineInterviewDetail == null)
                        onlineInterviewDetail = new OnlineCandidateSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        onlineInterviewDetail.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        onlineInterviewDetail.FirstName = dataReader["FIRST_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                    {
                        onlineInterviewDetail.EmailId = dataReader["EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_DATE"]))
                    {
                        onlineInterviewDetail.InterviewDate = Convert.ToDateTime(dataReader["ONLINE_INTERVIEW_DATE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_END_TIME"]))
                    {
                        onlineInterviewDetail.TimeSlotTo = dataReader["INTERVIEW_END_TIME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_START_TIME"]))
                    {
                        onlineInterviewDetail.TimeSlotFrom = dataReader["INTERVIEW_START_TIME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                    {
                        onlineInterviewDetail.TimeSlotIDFrom = Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                    {
                        onlineInterviewDetail.TimeSlotIDTo = Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CHAT_ROOM_KEY"]))
                    {
                        onlineInterviewDetail.ChatRoomName = dataReader["CHAT_ROOM_KEY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SLOT_KEY"]))
                    {
                        onlineInterviewDetail.SlotKey = dataReader["SLOT_KEY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["REMINDER"]))
                    {
                        onlineInterviewDetail.ReminderType = dataReader["REMINDER"].ToString();
                    }

                }
                return onlineInterviewDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This function updates the online interview schedule information
        /// </summary>
        /// <param name="onlineSchedule">
        /// A <see cref="onlineSchedule"/> that holds the online schedule information
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void UpdateOnlineInterviewSchedule(OnlineCandidateSessionDetail onlineSchedule,
            int userID, IDbTransaction transaction)
        {
            // Create command object.
            DbCommand UpdateScheduleCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_ONLINE_INTERVIEW_SCHEDULE");

            // Add parameters.
            HCMDatabase.AddInParameter(UpdateScheduleCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.String, onlineSchedule.CandidateInterviewID);

            if (onlineSchedule.InterviewDate == DateTime.MinValue)
            {
                HCMDatabase.AddInParameter(UpdateScheduleCommand,
                    "@INTERVIEW_DATE", DbType.DateTime, null);
            }
            else
            {
                HCMDatabase.AddInParameter(UpdateScheduleCommand, "@INTERVIEW_DATE",
                    DbType.DateTime, onlineSchedule.InterviewDate);
            }

            HCMDatabase.AddInParameter(UpdateScheduleCommand,
                "@INTERVIEW_START_TIME", DbType.String, onlineSchedule.TimeSlotFrom);

            HCMDatabase.AddInParameter(UpdateScheduleCommand,
                "@INTERVIEW_END_TIME", DbType.String, onlineSchedule.TimeSlotTo);

            HCMDatabase.AddInParameter(UpdateScheduleCommand,
                "@REMINDER", DbType.String, onlineSchedule.ReminderType);

            HCMDatabase.AddInParameter(UpdateScheduleCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute statement.
            if (transaction == null)
            {
                HCMDatabase.ExecuteNonQuery(UpdateScheduleCommand);
            }
            else
            {
                HCMDatabase.ExecuteNonQuery(UpdateScheduleCommand,
                    transaction as DbTransaction);
            }
        }


        /// <summary>
        /// Method that deletes an assessor available time slot.
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        /// <param name="assessorIds">
        /// A <see cref="string"/> that holds the list of assessor id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void DeleteOnlineInterviewScheduledAssessor(int candidateInterviewID,
            string assessorIds, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand deleteScheduledAssessorCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ONLINE_INTERVIEW_SCHEDULED_ASSESSOR");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteScheduledAssessorCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteScheduledAssessorCommand,
                "@ASSESSOR_IDS", DbType.String, assessorIds);

            // Execute the command.
            HCMDatabase.ExecuteNonQuery(deleteScheduledAssessorCommand, 
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method that gets the list of list requested candidated slots
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the slot key.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor time slot details.
        /// </returns>
        public AssessorTimeSlotDetail GetCandidateSlotDetailByCandidateID(int candidateInterviewID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getRequestedSlotCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_REQUESTED_SLOT_BY_CANDIDATE_ID");

                // Add parameters.   
                HCMDatabase.AddInParameter(getRequestedSlotCommand, "@CAND_ONLINE_INTERVIEW_ID", 
                    DbType.Int32, candidateInterviewID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getRequestedSlotCommand);

                AssessorTimeSlotDetail assessorSlotDetail =
                    new AssessorTimeSlotDetail();

                List<AssessorTimeSlotDetail> timeSlotDetails = null;
                List<AssessorDetail> assessorDetails = null;

                OnlineInterviewSessionDetail onlineInterviewDetail = null;

                if (dataReader.Read())
                {
                    // Instantiate the online inteview session
                    if (onlineInterviewDetail == null)
                        onlineInterviewDetail = new OnlineInterviewSessionDetail();

                    // Assign property values to the online interview object.
                    onlineInterviewDetail.InterviewName = dataReader["ONLINE_INTERVIEW_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        onlineInterviewDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        onlineInterviewDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["INSTRUCTION"]))
                    {
                        onlineInterviewDetail.InterviewInstruction =
                            dataReader["INSTRUCTION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["REMARKS"]))
                    {
                        onlineInterviewDetail.InterviewRemarks =
                            dataReader["REMARKS"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_BY"]))
                    {
                        onlineInterviewDetail.RequestedBy =
                            dataReader["REQUESTED_BY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CAND_ONLINE_INTERVIEW_ID"]))
                    {
                        onlineInterviewDetail.CandidateInterviewID =
                            Convert.ToInt32(dataReader["CAND_ONLINE_INTERVIEW_ID"].ToString().Trim());
                    }
                }

                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the list.
                    if (timeSlotDetails == null)
                        timeSlotDetails = new List<AssessorTimeSlotDetail>();

                    AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_SLOT_ID"]))
                    {
                        timeSlot.RequestDateGenID =
                            Convert.ToInt32(dataReader["REQUESTED_SLOT_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ACCEPTED_ID"]))
                    {
                        timeSlot.AcceptedID =
                            Convert.ToInt32(dataReader["ACCEPTED_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_DATE"]))
                    {
                        timeSlot.AvailabilityDate =
                            Convert.ToDateTime(dataReader["REQUESTED_DATE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                    {
                        timeSlot.TimeSlotIDFrom =
                            Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                    {
                        timeSlot.TimeSlotIDTo =
                            Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_FROM"]))
                    {
                        timeSlot.TimeSlotTextFrom = dataReader["TIME_SLOT_TEXT_FROM"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_TO"]))
                    {
                        timeSlot.TimeSlotTextTo = dataReader["TIME_SLOT_TEXT_TO"].ToString().Trim();
                    }

                    // Add to the list.
                    timeSlotDetails.Add(timeSlot);
                }

                //Get the assessor list
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the list.
                    if (assessorDetails == null)
                        assessorDetails = new List<AssessorDetail>();

                    AssessorDetail assessorDetail = new AssessorDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                    {
                        assessorDetail.AssessorId =
                            Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]))
                    {
                        assessorDetail.AssessorName = 
                            dataReader["ASSESSOR_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_SLOT_ID"]))
                    {
                        assessorDetail.RequestedSlotID=
                            Convert.ToInt32(dataReader["REQUESTED_SLOT_ID"].ToString().Trim());
                    }

                    // Add to the list.
                    assessorDetails.Add(assessorDetail);
                }

                if (onlineInterviewDetail != null)
                {
                    assessorSlotDetail.onlineInterviewDetail = onlineInterviewDetail;
                }

                if (timeSlotDetails != null)
                {
                    assessorSlotDetail.assessorTimeSlotDetails = timeSlotDetails;
                }

                if (assessorDetails != null)
                {
                    assessorSlotDetail.assessorDetail = assessorDetails;
                }

                return assessorSlotDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that deletes an assessor available time slot.
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        /// <param name="assessorIds">
        /// A <see cref="string"/> that holds the list of assessor id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void DeleteOnlineInterviewCandidateRequested(int candidateInterviewID,
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand deleteRequestedSlotsCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ONLINE_INTERVIEW_CANDIDATE_REQUESTED_BY_CANDIDATE_ID");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRequestedSlotsCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            // Execute the command.
            HCMDatabase.ExecuteNonQuery(deleteRequestedSlotsCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the list of interview skill questios for the given 
        /// interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the skill details.
        /// </returns>
        public List<QuestionDetail> GetOnlineInterviewConductionSummary(string interviewKey, 
            int candidateInterviewID, int assessorID, int skillID)
        {
            List<QuestionDetail> skillQuestions = null;
            IDataReader dataReader = null;

            try
            {
                DbCommand getInterviewConductionQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_CONDUCTION_SUMMARY");

                HCMDatabase.AddInParameter(getInterviewConductionQuestionCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);

                if (candidateInterviewID == 0)
                {
                    HCMDatabase.AddInParameter(getInterviewConductionQuestionCommand,
                        "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getInterviewConductionQuestionCommand,
                        "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);
                }

                if (assessorID == 0)
                {
                    HCMDatabase.AddInParameter(getInterviewConductionQuestionCommand,
                        "@ASSESSOR_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getInterviewConductionQuestionCommand,
                        "@ASSESSOR_ID", DbType.Int32, assessorID);
                }

                if (skillID == 0)
                {
                    HCMDatabase.AddInParameter(getInterviewConductionQuestionCommand,
                        "@SKILL_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getInterviewConductionQuestionCommand,
                        "@SKILL_ID", DbType.Int32, skillID);
                }

                dataReader = HCMDatabase.ExecuteReader(getInterviewConductionQuestionCommand);

                while (dataReader.Read())
                {
                    // Instantiate question list.
                    if (skillQuestions == null)
                        skillQuestions = new List<QuestionDetail>();

                    // Create a question object.
                    QuestionDetail skillQuestion = new QuestionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skillQuestion.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skillQuestion.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]))
                    {
                        skillQuestion.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CHOICE_DESC"]))
                    {
                        skillQuestion.Choice_Desc = dataReader["CHOICE_DESC"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                    {
                        skillQuestion.Question = dataReader["QUESTION_DESC"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_RELATION_ID"]))
                    {
                        skillQuestion.QuestionRelationId =
                            Convert.ToInt32(dataReader["QUESTION_RELATION_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]))
                    {
                        skillQuestion.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["IS_MANDATORY"]))
                    {
                        skillQuestion.IsMandatory = dataReader["IS_MANDATORY"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        skillQuestion.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        skillQuestion.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                    {
                        skillQuestion.ComplexityName = dataReader["COMPLEXITY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                    {
                        skillQuestion.Complexity = dataReader["COMPLEXITY_ID"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                    {
                        skillQuestion.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                    {
                        skillQuestion.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_COMMENTS"]))
                    {
                        skillQuestion.AssessorComments = dataReader["ASSESSOR_COMMENTS"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_RATING"]))
                    {
                        skillQuestion.AssessorRating = 
                            Convert.ToInt32(dataReader["ASSESSOR_RATING"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_RATING"]))
                    {
                        skillQuestion.SkillRating =
                            Convert.ToInt32(dataReader["SKILL_RATING"].ToString());
                    }

                    
                    // Add to the list.
                    skillQuestions.Add(skillQuestion);
                }

                return skillQuestions;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that inserts the assessor ratings and 
        /// comments against the questions 
        /// </summary>
        /// <param name="questionDetails">
        /// A <see cref="QuestionDetail"/> that holds the question details
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id
        /// </param>
        public void SaveOnlineInterviewQuestionRatingComments(QuestionDetail questionDetails, 
            int candidateInterviewID, int assessorID, int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertAssessorQuestionRatingCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_ASSESSMENT_QUESTION_SCORE");

            // Add input parameters
            HCMDatabase.AddInParameter(insertAssessorQuestionRatingCommand,
                "@QUESTION_ID", DbType.Int32, questionDetails.QuestionID);

            HCMDatabase.AddInParameter(insertAssessorQuestionRatingCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            HCMDatabase.AddInParameter(insertAssessorQuestionRatingCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);

            HCMDatabase.AddInParameter(insertAssessorQuestionRatingCommand,
                "@RATING", DbType.Int32, questionDetails.Rating);

            HCMDatabase.AddInParameter(insertAssessorQuestionRatingCommand,
                "@COMMENTS", DbType.String, questionDetails.Comments);

            HCMDatabase.AddInParameter(insertAssessorQuestionRatingCommand,
                "@USER_ID", DbType.String, userID);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertAssessorQuestionRatingCommand,
                transaction as DbTransaction);
        }


        /// <summary>
        /// Method that inserts the assessor ratings and 
        /// comments against the skill 
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id
        /// </param>
        /// <param name="rating">
        /// A <see cref="int"/> that holds the rating
        /// </param>
        public void SaveOnlineInterviewSkillRating(int candidateInterviewID, int skillID,
            int assessorID, int rating, string comments, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertAssessorSkillRatingCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_ASSESSMENT_SKILL_SCORE");

            // Add input parameters
            HCMDatabase.AddInParameter(insertAssessorSkillRatingCommand,
                "@SKILL_ID", DbType.Int32, skillID);

            HCMDatabase.AddInParameter(insertAssessorSkillRatingCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            HCMDatabase.AddInParameter(insertAssessorSkillRatingCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);

            HCMDatabase.AddInParameter(insertAssessorSkillRatingCommand,
                "@RATING", DbType.Int32, rating);

            HCMDatabase.AddInParameter(insertAssessorSkillRatingCommand,
                "@COMMENTS", DbType.String, comments);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertAssessorSkillRatingCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method that gets the online interview 
        /// candidate and position profile details
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview id
        /// </param>
        public AssessmentSummary GetOnlineInterviewAssessmentCandidate(int candidateInterviewID)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateAssessmentSummaryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSMENT_CANDIDATE");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateAssessmentSummaryCommand,
                    "@CAND_ONLINE_INTERVIEW_ID", DbType.String, candidateInterviewID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateAssessmentSummaryCommand);

                AssessmentSummary assessmentSummary = null;

                if(dataReader.Read())
                {
                    // Instantiate the assessmentsummary 
                    if (assessmentSummary == null)
                        assessmentSummary = new AssessmentSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        assessmentSummary.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        assessmentSummary.CanidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                        assessmentSummary.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        assessmentSummary.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        assessmentSummary.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString().Trim());

                    assessmentSummary.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_DEPARTMENTS"]))
                        assessmentSummary.ClientDepartments = dataReader["CLIENT_DEPARTMENTS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_CONTACTS"]))
                        assessmentSummary.ClientContacts = dataReader["CLIENT_CONTACTS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_NAME"]))
                        assessmentSummary.InterviewName = dataReader["ONLINE_INTERVIEW_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                        assessmentSummary.ScheduledDate = Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString().Trim());

                }
                
                return assessmentSummary;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of skills, and its rating for the given 
        /// interview key, assessor id, candidate interview id
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetOnlineInterviewKeySkillRating(string interviewKey, 
            int candidateInterviewID, int assessorID)
        {
            List<SkillDetail> skills = null;
            IDataReader dataReader = null;

            try
            {
                DbCommand getSkillRatingCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SKILLS_BY_INTERVIEW_KEY_ASSESSOR");

                HCMDatabase.AddInParameter(getSkillRatingCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);

                if (assessorID == 0)
                {
                    HCMDatabase.AddInParameter(getSkillRatingCommand,
                        "@ASSESSOR_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getSkillRatingCommand,
                        "@ASSESSOR_ID", DbType.Int32, assessorID);
                }

                if (candidateInterviewID == 0)
                {
                    HCMDatabase.AddInParameter(getSkillRatingCommand,
                        "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getSkillRatingCommand,
                        "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);
                }

                dataReader = HCMDatabase.ExecuteReader(getSkillRatingCommand);

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skills == null)
                        skills = new List<SkillDetail>();

                    // Create a skill object.
                    SkillDetail skill = new SkillDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        skill.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skill.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]))
                    {
                        skill.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_LEVEL"]))
                    {
                        skill.SkillLevel = dataReader["SKILL_LEVEL"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_QUESTIONS"]))
                    {
                        skill.SkillQuestionCount =
                            Convert.ToInt32(dataReader["NO_OF_QUESTIONS"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_RATING"]))
                    {
                        skill.SkillRating =
                            Convert.ToInt32(dataReader["SKILL_RATING"].ToString());
                    }

                    
                    // Add to the list.
                    skills.Add(skill);
                }

                return skills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }


        /// <summary>
        /// Method that gets the other assessor rating and comments 
        /// against this candidate interview id and question id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="questionID">
        /// A <see cref="int"/> that holds the question id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the assessor details.
        /// </returns>
        public List<AssessorDetail> GetOnlineInterviewOtherAssessorRating(
            int candidateInterviewID, int questionID, int assessorID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getOtherAssessorRatingCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_OTHER_ASSESSOR_RATING");

                // Add parameters.   
                HCMDatabase.AddInParameter(getOtherAssessorRatingCommand,
                    "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

                HCMDatabase.AddInParameter(getOtherAssessorRatingCommand,
                       "@QUESTION_ID", DbType.Int32, questionID);

                if (assessorID == 0)
                {
                    HCMDatabase.AddInParameter(getOtherAssessorRatingCommand,
                        "@ASSESSOR_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getOtherAssessorRatingCommand,
                        "@ASSESSOR_ID", DbType.Int32, assessorID);
                }

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getOtherAssessorRatingCommand);

                List<AssessorDetail> assessorDetails = null;
                while (dataReader.Read())
                {
                    // Instantiate the list.
                    if (assessorDetails == null)
                        assessorDetails = new List<AssessorDetail>();

                    AssessorDetail assessorDetail = new AssessorDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_RATING"]))
                    {
                        assessorDetail.QuestionRating =
                            Convert.ToInt32(dataReader["QUESTION_RATING"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        assessorDetail.FirstName =
                            dataReader["FIRST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                    {
                        assessorDetail.LastName =
                            dataReader["LAST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_COMMENTS"]))
                    {
                        assessorDetail.Comments =
                            dataReader["QUESTION_COMMENTS"].ToString().Trim();
                    }

                    // Add to the list.
                    assessorDetails.Add(assessorDetail);
                }
                return assessorDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts scheduled assessor skills against candidate id.
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <param name="skillids">
        /// A <see cref="string"/> that holds the skill ids.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void InsertOnlineInterviewScheduledAssessorSkill(int candidateInterviewID, 
            int assessorID, string skillids, int userID, IDbTransaction transaction)
        {
            DbCommand insertOnlineInterviewScheduledAssessorSkillCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_SCHEDULED_ASSESSOR_SKILL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledAssessorSkillCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledAssessorSkillCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);

            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledAssessorSkillCommand,
                "@SKILL_IDS", DbType.String, skillids);

            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledAssessorSkillCommand,
                "@USER_ID", DbType.Int32, userID);

            if (transaction == null)
            {
                HCMDatabase.ExecuteNonQuery(insertOnlineInterviewScheduledAssessorSkillCommand);
            }
            else
            {
                HCMDatabase.ExecuteNonQuery(insertOnlineInterviewScheduledAssessorSkillCommand,
                    transaction as DbTransaction);
            }
        }

        /// <summary>
        /// Method that inserts scheduled skills against candidate id.
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void InsertOnlineInterviewScheduledSkill(int candidateInterviewID,
            int skillID, int weightage,  int userID, IDbTransaction transaction)
        {
            DbCommand insertOnlineInterviewScheduledSkillCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_SCHEDULED_SKILL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledSkillCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledSkillCommand,
                "@SKILL_ID", DbType.Int32, skillID);

            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledSkillCommand,
                "@WEIGHTAGE", DbType.Int32, weightage);

            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledSkillCommand,
                "@USER_ID", DbType.Int32, userID);

            if (transaction == null)
            {
                HCMDatabase.ExecuteNonQuery(insertOnlineInterviewScheduledSkillCommand);
            }
            else
            {
                HCMDatabase.ExecuteNonQuery(insertOnlineInterviewScheduledSkillCommand,
                    transaction as DbTransaction);
            }
        }

        /// <summary>
        /// Method that delete the scheduled skill 
        /// against the candidate id, skill id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        public void DeleteOnlineInterviewScheduledSkill(int candidateInterviewID, int skillID)
        {
            // Create a stored procedure command object.
            DbCommand deleteScheduledSkillCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ONLINE_INTERVIEW_SCHEDULED_SKILL");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteScheduledSkillCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteScheduledSkillCommand,
                "@SKILL_ID", DbType.Int32, skillID);

            // Execute the command.
            HCMDatabase.ExecuteNonQuery(deleteScheduledSkillCommand);
        }

        /// <summary>
        /// Method that deletes the scheduled skill 
        /// against the candidate id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void DeleteOnlineInterviewScheduledAssessorSkill(int candidateInterviewID,
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand deleteScheduledAssessorSkillCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ONLINE_INTERVIEW_SCHEDULED_ASSESSOR_SKILL");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteScheduledAssessorSkillCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            // Execute the command.
            HCMDatabase.ExecuteNonQuery(deleteScheduledAssessorSkillCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the list of interview skills against the candidate id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetOnlineInterviewScheduledSkills(int candidateInterviewID)
        {
            List<SkillDetail> skills = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getScheduledSkillCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SCHEDULED_SKILLS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getScheduledSkillCommand, "@CAND_ONLINE_INTERVIEW_ID",
                    DbType.Int32, candidateInterviewID);

                // Execute the command.
                dataReader = HCMDatabase.ExecuteReader(getScheduledSkillCommand);

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skills == null)
                        skills = new List<SkillDetail>();

                    // Create a skill object.
                    SkillDetail skill = new SkillDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skill.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]))
                    {
                        skill.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString());
                    }

                    // Add to the list.
                    skills.Add(skill);
                }

                return skills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that gets the rating and comments
        /// against interview id,skill id,assessor id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the candidate session details.
        /// </returns>
        public AssessorDetail GetOnlineInterviewSKillRating(int candidateInterviewID, 
            int assessorID, int skillID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getSkillScoreCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSMENT_SKILL_SCORE");

                // Add parameters.   
                HCMDatabase.AddInParameter(getSkillScoreCommand, "@CAND_ONLINE_INTERVIEW_ID",
                    DbType.Int32, candidateInterviewID);

                HCMDatabase.AddInParameter(getSkillScoreCommand, "@ASSESSOR_ID",
                    DbType.Int32, assessorID);

                HCMDatabase.AddInParameter(getSkillScoreCommand, "@SKILL_ID",
                    DbType.Int32, skillID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getSkillScoreCommand);

                AssessorDetail assessorDetail = null;

                if (dataReader.Read())
                {
                    // Instantiate the online inteview session
                    if (assessorDetail == null)
                        assessorDetail = new AssessorDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["RATING"]))
                    {
                        assessorDetail.assessorRating = 
                            Convert.ToInt32(dataReader["RATING"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                    {
                        assessorDetail.Comments = dataReader["COMMENTS"].ToString();
                    }
                }

                return assessorDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of interview skills against the candidate id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetOnlineInterviewScheduledAssessorSkills(int candidateInterviewID,
            int assessorID)
        {
            List<SkillDetail> skills = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getScheduledAssessorSkillCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SCHEDULED_ASSESSOR_SKILL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getScheduledAssessorSkillCommand, "@CAND_ONLINE_INTERVIEW_ID",
                    DbType.Int32, candidateInterviewID);

                HCMDatabase.AddInParameter(getScheduledAssessorSkillCommand, "@ASSESSOR_ID",
                    DbType.Int32, assessorID);

                // Execute the command.
                dataReader = HCMDatabase.ExecuteReader(getScheduledAssessorSkillCommand);

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skills == null)
                        skills = new List<SkillDetail>();

                    // Create a skill object.
                    SkillDetail skill = new SkillDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        skill.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skill.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_RATING"]))
                    {
                        skill.SkillRating =
                            Convert.ToInt32(dataReader["SKILL_RATING"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_COMMENTS"]))
                    {
                        skill.SkillComments =
                            dataReader["SKILL_COMMENTS"].ToString();
                    }

                    // Add to the list.
                    skills.Add(skill);
                }

                return skills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the list of interview skills against the candidate id
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <param name="comments">
        /// A <see cref="string"/> that holds the overall comments.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void UpdateOnlineInterviewScheduledStatus(int candidateInterviewID,
            int assessorID, string comments, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand updateOnlineScheduledStatusCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_ONLINE_INTERVIEW_SCHEDULE_STATUS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateOnlineScheduledStatusCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            HCMDatabase.AddInParameter(updateOnlineScheduledStatusCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);

            HCMDatabase.AddInParameter(updateOnlineScheduledStatusCommand,
                "@COMMENTS", DbType.String, comments);

            // Execute the stored procedure.
            if (transaction == null)
            {
                HCMDatabase.ExecuteNonQuery(updateOnlineScheduledStatusCommand);
            }
            else
            {
                HCMDatabase.ExecuteNonQuery(updateOnlineScheduledStatusCommand,
                    transaction as DbTransaction);
            }

        }

        /// <summary>
        /// Method that retrieves the assessment report
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor id.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetOnlineInterviewAssessorSkillReport(int candidateInterviewID,
            int assessorID)
        {
            List<SkillDetail> skills = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getOnlineInterviewAssessorSkillReportCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSOR_SKILL_REPORT");

                // Add input parameters.
                HCMDatabase.AddInParameter(getOnlineInterviewAssessorSkillReportCommand, "@CAND_ONLINE_INTERVIEW_ID",
                    DbType.Int32, candidateInterviewID);

                HCMDatabase.AddInParameter(getOnlineInterviewAssessorSkillReportCommand, "@ASSESSOR_ID",
                    DbType.Int32, assessorID);

                // Execute the command.
                dataReader = HCMDatabase.ExecuteReader(getOnlineInterviewAssessorSkillReportCommand);

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skills == null)
                        skills = new List<SkillDetail>();

                    // Create a skill object.
                    SkillDetail skill = new SkillDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skill.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPUTED"]))
                    {
                        skill.Weightage = Convert.ToInt32(dataReader["COMPUTED"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_RATING"]))
                    {
                        skill.SkillRating = Convert.ToInt32(dataReader["SKILL_RATING"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_COMMENTS"]))
                    {
                        skill.SkillComments = dataReader["SKILL_COMMENTS"].ToString();
                    }

                    // Add to the list.
                    skills.Add(skill);
                }

                return skills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }


        /// <summary>
        /// Method that retrieves interview assessor against the interview key
        /// </summary>
        /// <param name="onlineInterviewKey">
        /// A <see cref="string"/> that holds the interview key
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the assessor details.
        /// </returns>
        public List<AssessorDetail> GetOnlineInterviewAssessorByInterviewKey(string onlineInterviewKey)
        {
            List<AssessorDetail> assessorDetail = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getOnlineInterviewAssessorReportCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSOR_BY_INTERVIEW_KEY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getOnlineInterviewAssessorReportCommand, "@ONLINE_INTERVIEW_KEY",
                    DbType.String, onlineInterviewKey);

                
                // Execute the command.
                dataReader = HCMDatabase.ExecuteReader(getOnlineInterviewAssessorReportCommand);

                while (dataReader.Read())
                {
                    // Instantiate assessor list.
                    if (assessorDetail == null)
                        assessorDetail = new List<AssessorDetail>();

                    // Create a assessor object.
                    AssessorDetail assessor = new AssessorDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                    {
                        assessor.AssessorId = Convert.ToInt32(dataReader["USER_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]))
                    {
                        assessor.AssessorName = dataReader["ASSESSOR_NAME"].ToString();
                    }

                    // Add to the list.
                    assessorDetail.Add(assessor);
                }

                return assessorDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the candidate online interview detail for the given
        /// candidate interview id.
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the candidate
        /// interview detail.
        /// </returns>
        public CandidateInterviewSessionDetail GetCandidateOnlineInterviewDetail(int candidateInterviewID)
        {
            
            CandidateInterviewSessionDetail candidateInterviewDetail = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateInterviewDetailCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_ONLINE_INTERVIEW_EMAIL_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateInterviewDetailCommand,
                    "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

                
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateInterviewDetailCommand);

                if (dataReader.Read())
                {
                    candidateInterviewDetail = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CHAT_ROOM_KEY"]))
                    {
                        candidateInterviewDetail.ChatRoomKey = 
                            dataReader["CHAT_ROOM_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SLOT_KEY"]))
                    {
                        candidateInterviewDetail.SlotKey =
                            dataReader["SLOT_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        candidateInterviewDetail.CandidateID = Convert.ToString(dataReader["CANDIDATE_ID"]).Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        candidateInterviewDetail.CandidateFirstName = dataReader["FIRST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                    {
                        candidateInterviewDetail.CandidateLastName = dataReader["LAST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                    {
                        candidateInterviewDetail.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_OWNER_EMAIL"]))
                    {
                        candidateInterviewDetail.CandidateOwnerEmail = dataReader["CANDIDATE_OWNER_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_EMAIL"]))
                    {
                        candidateInterviewDetail.SchedulerEmail = dataReader["SCHEDULER_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_FIRST_NAME"]))
                    {
                        candidateInterviewDetail.SchedulerFirstName = dataReader["SCHEDULER_FIRST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_LAST_NAME"]))
                    {
                        candidateInterviewDetail.SchedulerLastName = dataReader["SCHEDULER_LAST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_COMPANY"]))
                    {
                        candidateInterviewDetail.SchedulerCompany = 
                            dataReader["SCHEDULER_COMPANY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_EMAIL"]))
                    {
                        candidateInterviewDetail.PositionProfileOwnerEmail = 
                            dataReader["POSITION_PROFILE_OWNER_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                    {
                        candidateInterviewDetail.InterviewTestName = 
                            dataReader["INTERVIEW_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_KEY"]))
                    {
                        candidateInterviewDetail.InterviewTestKey = 
                            dataReader["INTERVIEW_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DATE"]))
                    {
                        candidateInterviewDetail.ScheduledDate = 
                            Convert.ToDateTime(dataReader["INTERVIEW_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_FROM"]))
                    {
                        candidateInterviewDetail.TimeSlotTextFrom = 
                            dataReader["TIME_SLOT_TEXT_FROM"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_TO"]))
                    {
                        candidateInterviewDetail.TimeSlotTextTo = 
                            dataReader["TIME_SLOT_TEXT_TO"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        candidateInterviewDetail.PositionProfileName =
                            dataReader["POSITION_PROFILE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                    {
                        candidateInterviewDetail.ClientName =
                            dataReader["CLIENT_NAME"].ToString().Trim();
                    }
                }

                return candidateInterviewDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the assessor overall comments
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the 
        /// assessor comments
        /// </returns>
        public string GetAssessorOverAllComments
            (int candidateInterviewID, int assessorID)
        {

            string assessorComments = string.Empty;
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getAssessorCommentsCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSMENT_ASSESSOR_COMMENTS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getAssessorCommentsCommand,
                    "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

                HCMDatabase.AddInParameter(getAssessorCommentsCommand,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessorCommentsCommand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                    {
                        assessorComments = dataReader["COMMENTS"].ToString().Trim();
                    }
                }

                return assessorComments;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that gets the list of associated candidates against the interview id
        /// </summary>
        /// <param name="interviewkey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </returns>
        public List<CandidateDetail> GetOnlineInterviewCandidates(
            string interviewkey)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getAssessorDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_CANDIDATES_BY_INTERVIEW_KEY");

                // Add parameters.   
                HCMDatabase.AddInParameter(getAssessorDetailCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewkey);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getAssessorDetailCommand);

                List<CandidateDetail> candidateDetail = null;
                while (dataReader.Read())
                {
                    // Instantiate the list.
                    if (candidateDetail == null)
                        candidateDetail = new List<CandidateDetail>();

                    CandidateDetail candidate = new CandidateDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        candidate.CandidateInfoID =
                            Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        candidate.FirstName =
                            dataReader["FIRST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                    {
                        candidate.LastName =
                            dataReader["LAST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                    {
                        candidate.EMailID =
                            dataReader["EMAIL"].ToString().Trim();
                    }

                    // Add to the list.
                    candidateDetail.Add(candidate);
                }
                return candidateDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the recruiter detail.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterDetail> GetOnlineInterviewRecruiterSkill(int tenantID, string interviewKey,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<RecruiterDetail> recruiterDetails = null;

            try
            {
                DbCommand getRecruiterDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_RECRUITER_DETAILS_BY_SKILL");

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand, "@ORDER_BY",
                   DbType.String, sortField);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getRecruiterDetailsCommand);

                // Retreive recruiter details.
                while (dataReader.Read())
                {
                    // Instantiate recruiter details list.
                    if (recruiterDetails == null)
                        recruiterDetails = new List<RecruiterDetail>();

                    // Create recruiter detail object.
                    RecruiterDetail recruiterDetail = new RecruiterDetail();

                    // Instantiate recruiter detail object.
                    recruiterDetail = new RecruiterDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        recruiterDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_NAME"]))
                        recruiterDetail.RecruiterName = dataReader["RECRUITER_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_EMAIL"]))
                        recruiterDetail.RecruiterEmail = dataReader["RECRUITER_EMAIL"].ToString();

                    recruiterDetails.Add(recruiterDetail);
                }
                return recruiterDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the recruiter 
        /// detail who workin similar clients.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the interview key.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="RecruiterDetail"/> that holds the 
        /// recruiter details.
        /// </returns>
        public List<RecruiterDetail> GetOnlineInterviewRecruiterClient(int tenantID, string interviewKey,
            int pageNumber, int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<RecruiterDetail> recruiterDetails = null;

            try
            {
                DbCommand getRecruiterDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_RECRUITER_DETAILS_BY_CLIENT");

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand, "@ORDER_BY",
                   DbType.String, sortField);

                HCMDatabase.AddInParameter(getRecruiterDetailsCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getRecruiterDetailsCommand);

                // Retreive recruiter details.
                while (dataReader.Read())
                {
                    // Instantiate recruiter details list.
                    if (recruiterDetails == null)
                        recruiterDetails = new List<RecruiterDetail>();

                    // Create recruiter detail object.
                    RecruiterDetail recruiterDetail = new RecruiterDetail();

                    // Instantiate recruiter detail object.
                    recruiterDetail = new RecruiterDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        recruiterDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_NAME"]))
                        recruiterDetail.RecruiterName = dataReader["RECRUITER_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_EMAIL"]))
                        recruiterDetail.RecruiterEmail = dataReader["RECRUITER_EMAIL"].ToString();

                    recruiterDetails.Add(recruiterDetail);
                }
                return recruiterDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the assessor overall comments
        /// </summary>
        /// <param name="interviewDetail">
        /// A <see cref="OnlineCandidateSessionDetail"/> that holds the interview detail
        /// </param>
        /// <param name="interviewDate">
        /// A <see cref="DateTime"/> that holds the inte.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the 
        /// assessor comments
        /// </returns>
        public bool GetScheduleAvailableTimes(OnlineCandidateSessionDetail interviewDetail)
        {

            bool isCandidateAvailable = true;

            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getAvailableTimesCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_AVAILABLE_SCHEDULED_TIMES");

                // Add input parameters.
                HCMDatabase.AddInParameter(getAvailableTimesCommand,
                    "@CANDIDATE_ID", DbType.Int32, interviewDetail.CandidateID);

                HCMDatabase.AddInParameter(getAvailableTimesCommand,
                    "@INTERVIEW_DATE", DbType.DateTime, interviewDetail.InterviewDate);

                HCMDatabase.AddInParameter(getAvailableTimesCommand,
                    "@TIME_SLOT_TEXT_FROM", DbType.String, interviewDetail.TimeSlotFrom);

                HCMDatabase.AddInParameter(getAvailableTimesCommand,
                    "@TIME_SLOT_TEXT_TO", DbType.String, interviewDetail.TimeSlotTo);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAvailableTimesCommand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CANIDATE_AVAILABLE"]) &&
                        dataReader["CANIDATE_AVAILABLE"].ToString().Trim() == "N")
                    {
                        isCandidateAvailable = false;
                    }
                }

                return isCandidateAvailable;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of interview skill questios for the given 
        /// interview key
        /// </summary>
        /// <param name="interviewKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill id.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the skill details.
        /// </returns>
        public List<QuestionDetail> GetOnlineInterviewSearchQuestion(string interviewKey, int skillID,
            int pageNumber, int pageSize, out int totalRecords)
        {
            List<QuestionDetail> skillQuestions = null;
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                DbCommand getInterviewSkillQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_SEARCH_QUESTION");

                HCMDatabase.AddInParameter(getInterviewSkillQuestionCommand,
                    "@INTERVIEW_KEY", DbType.String, interviewKey);

                if (skillID == 0)
                {
                    HCMDatabase.AddInParameter(getInterviewSkillQuestionCommand,
                        "@SKILL_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getInterviewSkillQuestionCommand,
                        "@SKILL_ID", DbType.Int32, skillID);
                }

                HCMDatabase.AddInParameter(getInterviewSkillQuestionCommand,
                 "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getInterviewSkillQuestionCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getInterviewSkillQuestionCommand);

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skillQuestions == null)
                        skillQuestions = new List<QuestionDetail>();

                    // Create a skill object.
                    QuestionDetail skillQuestion = new QuestionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skillQuestion.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skillQuestion.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]))
                    {
                        skillQuestion.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                    {
                        skillQuestion.Question = dataReader["QUESTION_DESC"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_RELATION_ID"]))
                    {
                        skillQuestion.QuestionRelationId =
                            Convert.ToInt32(dataReader["QUESTION_RELATION_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]))
                    {
                        skillQuestion.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["IS_MANDATORY"]))
                    {
                        skillQuestion.IsMandatory = dataReader["IS_MANDATORY"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        skillQuestion.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        skillQuestion.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                    {
                        skillQuestion.ComplexityName = dataReader["COMPLEXITY_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                    {
                        skillQuestion.Complexity = dataReader["COMPLEXITY_ID"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                    {
                        skillQuestion.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                    {
                        skillQuestion.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                    }

                    // Add to the list.
                    skillQuestions.Add(skillQuestion);
                }

                return skillQuestions;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that updates the online interview reminder type
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the interview candidate id.
        /// </param>
        /// <param name="remiderType">
        /// A <see cref="string"/> that holds the requested ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void UpdateOnlineInterviewReminder(int candidateInterviewID,
            string remiderType, int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object
            DbCommand updateOnlineInterviewReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_ONLINE_INTERVIEW_REMINDER");

            // Add input parameters
            HCMDatabase.AddInParameter(updateOnlineInterviewReminderCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            HCMDatabase.AddInParameter(updateOnlineInterviewReminderCommand,
               "@REMINDER", DbType.String, remiderType);

            HCMDatabase.AddInParameter(updateOnlineInterviewReminderCommand,
               "@USER_ID", DbType.Int32, userID);

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateOnlineInterviewReminderCommand, 
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method to insert the online candidate availability
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview key
        /// </param>
        /// <param name="slotKey">
        /// A <see cref="string"/> that holds the slot key.
        /// </param>
        /// <param name="userID"> 
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public int InsertOnlineCandidateRequested(int candidateInterviewID,
            string slotKey, int slotExpTimeInMin, int userID, IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertCandidateRequest
                = HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_CANDIDATE_REQUESTED");
            // Add input parameter
            HCMDatabase.AddInParameter(insertCandidateRequest,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);
            HCMDatabase.AddInParameter(insertCandidateRequest,
                "@SLOT_KEY", DbType.String, slotKey);
            HCMDatabase.AddInParameter(insertCandidateRequest,
                "@SLOT_EXP_TIME_IN_MIN", DbType.Int32, slotExpTimeInMin);
            HCMDatabase.AddInParameter(insertCandidateRequest,
                "@USER_ID", DbType.Int32, userID);
            HCMDatabase.AddOutParameter(insertCandidateRequest,
                "@REQUESTED_GEN_ID", DbType.Int32, int.MaxValue);

            object returnValue = null;

            if (transaction == null)
            {
                returnValue = HCMDatabase.ExecuteNonQuery(insertCandidateRequest);
            }
            else
            {
                returnValue = HCMDatabase.ExecuteNonQuery(insertCandidateRequest, transaction as DbTransaction);
            }

            returnValue = HCMDatabase.GetParameterValue(insertCandidateRequest, "@REQUESTED_GEN_ID");

            if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                return 0;

            return int.Parse(returnValue.ToString());
        }

        /// <summary>
        /// Method that inserts an assessor time slot detail.
        /// </summary>
        /// <param name="timeSlot">
        /// A <seealso cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public int InsertOnlineCandidateRequestedSlots(int requestedID, AssessorTimeSlotDetail timeSlot,
            int userID, IDbTransaction transaction)
        {
            DbCommand insertCandidateRequestedSlotsCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_CANDIDATE_REQUESTED_TIMESLOT");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCandidateRequestedSlotsCommand,
                "@CAND_REQUESTED_ID", DbType.Int32, requestedID);

            HCMDatabase.AddInParameter(insertCandidateRequestedSlotsCommand,
                "@REQUESTED_DATE", DbType.DateTime, timeSlot.AvailabilityDate);

            HCMDatabase.AddInParameter(insertCandidateRequestedSlotsCommand,
                "@TIME_SLOT_ID_FROM", DbType.Int32, timeSlot.TimeSlotIDFrom);

            HCMDatabase.AddInParameter(insertCandidateRequestedSlotsCommand,
                "@TIME_SLOT_ID_TO", DbType.Int32, timeSlot.TimeSlotIDTo);

            HCMDatabase.AddInParameter(insertCandidateRequestedSlotsCommand,
                "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddOutParameter(insertCandidateRequestedSlotsCommand,
                "@REQUESTED_SLOT_ID", DbType.Int32, int.MaxValue);

            object returnValue = null;

            if (transaction == null)
            {
                returnValue = HCMDatabase.ExecuteNonQuery(insertCandidateRequestedSlotsCommand);
            }
            else
            {
                returnValue = HCMDatabase.ExecuteNonQuery(insertCandidateRequestedSlotsCommand, transaction as DbTransaction);
            }

            returnValue = HCMDatabase.GetParameterValue(insertCandidateRequestedSlotsCommand, "@REQUESTED_SLOT_ID");

            if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                return 0;

            return int.Parse(returnValue.ToString());
        }

        /// <summary>
        /// Method that inserts assessor against the requested candidate slot id
        /// </summary>
        /// <param name="candRequestedID">
        /// A <seealso cref="int"/> that holds the requested candidate slot id
        /// </param>
        /// <param name="assessorID">
        /// A <seealso cref="int"/> that holds assessor ID
        /// </param>
        /// <param name="userID">
        /// A <seealso cref="int"/> that holds user ID
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void InsertOnlineCandidateRequestedSlotsAssessor(int requestdSlotID, int assessorID,
            int userID, IDbTransaction transaction)
        {
            DbCommand insertOnlineCandidateRequestedAssessorCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_CANDIDATE_REQUESTED_TIMESLOT_ASSESSOR");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertOnlineCandidateRequestedAssessorCommand,
                "@REQUESTED_SLOT_ID", DbType.Int32, requestdSlotID);

            HCMDatabase.AddInParameter(insertOnlineCandidateRequestedAssessorCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);

            HCMDatabase.AddInParameter(insertOnlineCandidateRequestedAssessorCommand,
                "@USER_ID", DbType.Int32, userID);

            if (transaction == null)
            {
                HCMDatabase.ExecuteNonQuery(insertOnlineCandidateRequestedAssessorCommand);
            }
            else
            {
                HCMDatabase.ExecuteNonQuery(insertOnlineCandidateRequestedAssessorCommand,
                    transaction as DbTransaction);
            }
        }

        /// <summary>
        /// Method that inserts the online interview reminder.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="InterviewReminderDetail"/> that holds the reminder
        /// detail.
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void InsertOnlineInterviewReminder(InterviewReminderDetail reminderDetail,
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertOnlineInterviewReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_REMINDER");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertOnlineInterviewReminderCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, reminderDetail.CandidateInterviewID);
            HCMDatabase.AddInParameter(insertOnlineInterviewReminderCommand,
               "@INTERVAL_ID", DbType.String, reminderDetail.IntervalID.ToString());
            HCMDatabase.AddInParameter(insertOnlineInterviewReminderCommand,
                "@SCHEDULED_DATE", DbType.DateTime, reminderDetail.ReminderDate);
            HCMDatabase.AddInParameter(insertOnlineInterviewReminderCommand,
                "@START_TIME", DbType.String, reminderDetail.InterviewStartTime);
            HCMDatabase.AddInParameter(insertOnlineInterviewReminderCommand,
               "@USER_ID", DbType.Int16, reminderDetail.UserID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertOnlineInterviewReminderCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the list of online interview reminder sender list 
        /// for the given date/time and relative time differences. Mails needs 
        /// to be sent for this list.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<InterviewReminderDetail> GetOnlineInterviewReminderSenderList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getSenderList = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_REMINDER_SENDER_LIST");

                // Add input parameters.
                HCMDatabase.AddInParameter(getSenderList,
                    "@CURRENT_DATE_TIME", DbType.String, currentDateTime);
                HCMDatabase.AddInParameter(getSenderList,
                    "@RELATIVE_TIME_DIFFERENCE_SECONDS", DbType.Int32, relativeTimeDifference);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getSenderList);

                List<InterviewReminderDetail> senderList = null;

                while (dataReader.Read())
                {
                    if (senderList == null)
                        senderList = new List<InterviewReminderDetail>();

                    InterviewReminderDetail senderDetail = new InterviewReminderDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_ONLINE_INTERVIEW_ID"]))
                    {
                        senderDetail.CandidateInterviewID = 
                            Convert.ToInt32(dataReader["CAND_ONLINE_INTERVIEW_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                    {
                        senderDetail.EmailID = dataReader["EMAIL_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVAL_ID"]))
                    {
                        senderDetail.IntervalID = dataReader["INTERVAL_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVAL_DESCRIPTION"]))
                    {
                        senderDetail.IntervalDescription = dataReader["INTERVAL_DESCRIPTION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_KEY"]))
                    {
                        senderDetail.InterviewID = dataReader["INTERVIEW_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                    {
                        senderDetail.InterviewName = dataReader["INTERVIEW_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EXPECTED_DATE"]))
                    {
                        senderDetail.ExpectedDate = Convert.ToDateTime(dataReader["EXPECTED_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        senderDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        senderDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }
                    // Add the detail to the list.
                    senderList.Add(senderDetail);
                }
                return senderList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Method that will update the reminder sent status for the given 
        /// candidate, attempt number and interval ID.
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        /// <param name="intervalID">
        /// A <see cref="string"/> that holds the interval ID.
        /// </param>
        /// <param name="reminderSent">
        /// A <see cref="bool"/> that holds the reminder sent status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateOnlineInterviewReminderStatus(int candidateInterviewID,
            string intervalID, bool reminderSent, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateOnlineInterviewReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_ONLINE_INTERVIEW_REMINDER_SENT_STATUS");
            //Add input parameters.
            HCMDatabase.AddInParameter(updateOnlineInterviewReminderCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);
            HCMDatabase.AddInParameter(updateOnlineInterviewReminderCommand,
                "@INTERVAL_ID", DbType.String, intervalID);
            HCMDatabase.AddInParameter(updateOnlineInterviewReminderCommand,
                "@REMINDER_SENT", DbType.String, reminderSent == true ? "Y" : "N");
            HCMDatabase.AddInParameter(updateOnlineInterviewReminderCommand,
                "@USER_ID", DbType.Int32, userID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateOnlineInterviewReminderCommand);
        }

        /// <summary>
        /// Method that inserts assessor against candidate,interview.
        /// </summary>
        /// <param name="timeSlot">
        /// A <seealso cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void InsertOnlineInterviewScheduledAssessor(int assessorID,
            int candidateInterviewID, int userID, IDbTransaction transaction)
        {
            DbCommand insertOnlineInterviewScheduledAssessorCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ONLINE_INTERVIEW_SCHEDULED_ASSESSOR");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledAssessorCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledAssessorCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);

            HCMDatabase.AddInParameter(insertOnlineInterviewScheduledAssessorCommand,
                "@USER_ID", DbType.Int32, userID);

            if (transaction == null)
            {
                HCMDatabase.ExecuteNonQuery(insertOnlineInterviewScheduledAssessorCommand);
            }
            else
            {
                HCMDatabase.ExecuteNonQuery(insertOnlineInterviewScheduledAssessorCommand,
                    transaction as DbTransaction);
            }
        }

        /// <summary>
        /// Method that retrieves the list of expired slot request list 
        /// for the given date/time and relative time differences.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<CandidateDetail> GetExpiredSlotRequestList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getExpiredSenderList = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_EXPIRED_CANDIDATE_SLOT_REQUEST_LIST");

                // Add input parameters.
                HCMDatabase.AddInParameter(getExpiredSenderList,
                    "@CURRENT_DATE_TIME", DbType.String, currentDateTime);
                HCMDatabase.AddInParameter(getExpiredSenderList,
                    "@RELATIVE_TIME_DIFFERENCE_SECONDS", DbType.Int32, relativeTimeDifference);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getExpiredSenderList);

                List<CandidateDetail> candidateDetails = null;

                while (dataReader.Read())
                {
                    if (candidateDetails == null)
                        candidateDetails = new List<CandidateDetail>();

                    CandidateDetail candidateDetail = new CandidateDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_ONLINE_INTERVIEW_ID"]))
                    {
                        candidateDetail.CandidateInterviewID =
                            Convert.ToInt32(dataReader["CAND_ONLINE_INTERVIEW_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                    {
                        candidateDetail.EMailID = dataReader["EMAIL_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        candidateDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                    {
                        candidateDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                    }
                    
                    // Add the detail to the list.
                    candidateDetails.Add(candidateDetail);
                }
                return candidateDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Method that will update the online interview 
        /// session status for the given candidate interview id 
        /// </summary>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview ID.
        /// </param>
        public void UpdateOnlineInterviewSessionStatus(int candidateInterviewID)
        {
            // Create a stored procedure command object.
            DbCommand updateOnlineInterviewStatusCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_ONLINE_INTERVIEW_SESSION_STATUS");
            
            //Add input parameters.
            HCMDatabase.AddInParameter(updateOnlineInterviewStatusCommand,
                "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, candidateInterviewID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateOnlineInterviewStatusCommand);
        }

        /// <summary>
        /// Method that retrieves the list of expired online interview list 
        /// for the given date/time and relative time differences.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<CandidateDetail> GetExpiredOnlineInterviewList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getExpiredInterviewSenderList = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_EXPIRED_SENDER_LIST");

                // Add input parameters.
                HCMDatabase.AddInParameter(getExpiredInterviewSenderList,
                    "@CURRENT_DATE_TIME", DbType.String, currentDateTime);
                HCMDatabase.AddInParameter(getExpiredInterviewSenderList,
                    "@RELATIVE_TIME_DIFFERENCE_SECONDS", DbType.Int32, relativeTimeDifference);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getExpiredInterviewSenderList);

                List<CandidateDetail> candidateDetails = null;

                while (dataReader.Read())
                {
                    if (candidateDetails == null)
                        candidateDetails = new List<CandidateDetail>();

                    CandidateDetail candidateDetail = new CandidateDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_ONLINE_INTERVIEW_ID"]))
                    {
                        candidateDetail.CandidateInterviewID =
                            Convert.ToInt32(dataReader["CAND_ONLINE_INTERVIEW_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                    {
                        candidateDetail.EMailID = dataReader["EMAIL_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        candidateDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                    {
                        candidateDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                    }

                    // Add the detail to the list.
                    candidateDetails.Add(candidateDetail);
                }
                return candidateDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the assessor overall comments
        /// </summary>
        /// <param name="interviewDetail">
        /// A <see cref="OnlineCandidateSessionDetail"/> that holds the interview detail
        /// </param>
        /// <param name="interviewDate">
        /// A <see cref="DateTime"/> that holds the inte.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the 
        /// assessor comments
        /// </returns>
        public bool IsAssessorAvailableForScheduledTime(int assessorID, DateTime interviewDate, int timeSlotIDFrom, int timeSlotIDTo)
        {

            bool isAssessorAvailable = true;

            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getAvailableTimesCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_ASSESSOR_AVAILABLE_TO_SCHEDULE");

                // Add input parameters.
                HCMDatabase.AddInParameter(getAvailableTimesCommand,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);

                HCMDatabase.AddInParameter(getAvailableTimesCommand,
                    "@INTERVIEW_DATE", DbType.DateTime, interviewDate);

                HCMDatabase.AddInParameter(getAvailableTimesCommand,
                    "@TIME_SLOT_ID_FROM", DbType.Int32, timeSlotIDFrom);

                HCMDatabase.AddInParameter(getAvailableTimesCommand,
                    "@TIME_SLOT_ID_TO", DbType.Int32, timeSlotIDTo);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAvailableTimesCommand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_AVAILABLE"]) &&
                        dataReader["ASSESSOR_AVAILABLE"].ToString().Trim() == "N")
                    {
                        isAssessorAvailable = false;
                    }
                }

                return isAssessorAvailable;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        #endregion
    }
}
