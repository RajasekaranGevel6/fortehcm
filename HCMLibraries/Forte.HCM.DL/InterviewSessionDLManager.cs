﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewSessionDLManager.cs
// File that represents the data layer for the Interview Session module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

#endregion
namespace Forte.HCM.DL
{
    public class InterviewSessionDLManager : DatabaseConnectionManager
    {
        #region Public Method

        /// <summary>
        /// Method that retrieves the list of interview sessions for the given 
        /// search criteria.
        /// </summary>
        /// <param name="interviewSessionSearchCriteria"> 
        /// A <see cref="InterviewSessionSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="total">
        /// A <see cref="int"/> that holds the total records in the list as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewSessionSearch"/> that holds the interview 
        /// sessions.
        /// </returns>
        public List<TestSessionSearch> GetInterviewSessionDetails(TestSessionSearchCriteria
           testSessionSearchCriteria, int pageNumber, int pageSize, string orderBy,
           SortType direction, out int total)
        {
            IDataReader datareader = null;
            total = 0;
            try
            {
                DbCommand getTestSessionDetailsCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEWS_BY_SESSION");
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@CANDIDATE_SESSION_ID", DbType.String, testSessionSearchCriteria.CandidateSessionID);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@INTERVIEW_TEST_SESSION_ID", DbType.String, testSessionSearchCriteria.SessionKey);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@INTERVIEW_TEST_KEY", DbType.String, testSessionSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@INTERVIEW_TEST_NAME", DbType.String, testSessionSearchCriteria.TestName);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@INTERVIEW_TEST_SCHEDULE_AUTHOR_ID", DbType.String, testSessionSearchCriteria.SchedulerNameID == 0 ? null : testSessionSearchCriteria.SchedulerNameID.ToString());
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@INTERVIEW_TEST_SESSION_AUTHOR_ID", DbType.String, testSessionSearchCriteria.TestSessionCreatorID == 0 ? null : testSessionSearchCriteria.TestSessionCreatorID.ToString());
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@CANDIDATE_NAME", DbType.String, testSessionSearchCriteria.CandidateName);

                if (testSessionSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSessionSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                datareader = HCMDatabase.ExecuteReader(getTestSessionDetailsCommand);
                List<TestSessionSearch> testSessionSearch = null;
                TestSessionSearch tstSessionSearch = null;
                while (datareader.Read())
                {
                    if (!Utility.IsNullOrEmpty(datareader["TOTAL"]))
                        total = Convert.ToInt32(datareader["TOTAL"]);
                    else
                    {
                        if (testSessionSearch == null)
                            testSessionSearch = new List<TestSessionSearch>();
                        if (tstSessionSearch == null)
                            tstSessionSearch = new TestSessionSearch();
                        if (!Utility.IsNullOrEmpty(datareader["INTERVIEW_TEST_SESSION_ID"]))
                            tstSessionSearch.TestSessionID = datareader["INTERVIEW_TEST_SESSION_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["INTERVIEWTESTKEY"]))
                            tstSessionSearch.TestKey = datareader["INTERVIEWTESTKEY"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                            tstSessionSearch.CandidateSessionID = datareader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CLIENT_REQUEST_NUMBER"]))
                            tstSessionSearch.ClientRequestNumber = datareader["CLIENT_REQUEST_NUMBER"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["DATE_OF_PURCHASE"]))
                            tstSessionSearch.DateofPurchase = datareader["DATE_OF_PURCHASE"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CREDITS"]))
                            tstSessionSearch.Credits = datareader["CREDITS"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["ADMINISTERED_BY_ID"]))
                            tstSessionSearch.AdministeredID = Convert.ToInt32(datareader["ADMINISTERED_BY_ID"].ToString());
                        if (!Utility.IsNullOrEmpty(datareader["ADMINISTERED_BY"]))
                            tstSessionSearch.AdministeredBy = datareader["ADMINISTERED_BY"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["AUTHOR_FULLNAME"]))
                            tstSessionSearch.TestAuthorFullName = datareader["AUTHOR_FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_ID"]))
                            tstSessionSearch.CandidateID = Convert.ToInt32(datareader["CANDIDATE_ID"]);
                        if (!Utility.IsNullOrEmpty(datareader["CANIDATE_NAME"]))
                            tstSessionSearch.CandidateName = datareader["CANIDATE_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_FULLNAME"]))
                            tstSessionSearch.CandidateFullName = datareader["CANDIDATE_FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(datareader["DATE_OF_INTERVIEW"]))
                            tstSessionSearch.DateofTest = Convert.ToDateTime(datareader["DATE_OF_INTERVIEW"]).ToString("MM/dd/yyyy");
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_CANDIDATE_ICON"]))
                            tstSessionSearch.CandidateVisibleStatus = Convert.ToBoolean(datareader["SHOW_CANDIDATE_ICON"]);
                        else
                            tstSessionSearch.CandidateVisibleStatus = false;
                        if (!Utility.IsNullOrEmpty(datareader["CANCEL_REASON"]))
                            tstSessionSearch.CancelReason = datareader["CANCEL_REASON"].ToString().Replace("\r\n", "<br />");
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_CANCEL_ICON"]))
                            tstSessionSearch.ShowCancelIcon = Convert.ToBoolean(datareader["SHOW_CANCEL_ICON"]);
                        else
                            tstSessionSearch.ShowCancelIcon = false;
                        if (!Utility.IsNullOrEmpty(datareader["ATTEMPT_ID"]))
                            tstSessionSearch.AttemptID = Convert.ToInt32(datareader["ATTEMPT_ID"]);
                        else
                            tstSessionSearch.AttemptID = 0;
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_INTERVIEW_SCORE"]))
                            tstSessionSearch.ShowTestScore = Convert.ToBoolean(datareader["SHOW_INTERVIEW_SCORE"]);
                        else
                            tstSessionSearch.ShowTestScore = false;

                        testSessionSearch.Add(tstSessionSearch);
                        tstSessionSearch = null;
                    }
                }
                return testSessionSearch;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the list of interview sessions for the given 
        /// search criteria.
        /// </summary>
        /// <param name="interviewSessionSearchCriteria"> 
        /// A <see cref="InterviewSessionSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="total">
        /// A <see cref="int"/> that holds the total records in the list as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewSessionSearch"/> that holds the interview 
        /// sessions.
        /// </returns>
        public List<TestSessionSearch> GetOnlineInterviewSessionDetails(TestSessionSearchCriteria
           testSessionSearchCriteria, int pageNumber, int pageSize, string orderBy,
           SortType direction, out int total)
        {
            IDataReader datareader = null;
            total = 0;
            try
            {
                DbCommand getTestSessionDetailsCommand = HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEWS_BY_SESSION");
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@CANDIDATE_SESSION_ID", DbType.String, testSessionSearchCriteria.CandidateSessionID);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@INTERVIEW_TEST_SESSION_ID", DbType.String, testSessionSearchCriteria.SessionKey);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@INTERVIEW_TEST_SCHEDULE_AUTHOR_ID", DbType.String, testSessionSearchCriteria.SchedulerNameID == 0 ? null : testSessionSearchCriteria.SchedulerNameID.ToString());
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@INTERVIEW_TEST_SESSION_AUTHOR_ID", DbType.String, testSessionSearchCriteria.TestSessionCreatorID == 0 ? null : testSessionSearchCriteria.TestSessionCreatorID.ToString());
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@CANDIDATE_NAME", DbType.String, testSessionSearchCriteria.CandidateName);

                if (testSessionSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSessionSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                datareader = HCMDatabase.ExecuteReader(getTestSessionDetailsCommand);
                List<TestSessionSearch> testSessionSearch = null;
                TestSessionSearch tstSessionSearch = null;
                while (datareader.Read())
                {
                    if (!Utility.IsNullOrEmpty(datareader["TOTAL"]))
                        total = Convert.ToInt32(datareader["TOTAL"]);
                    else
                    {
                        if (testSessionSearch == null)
                            testSessionSearch = new List<TestSessionSearch>();
                        if (tstSessionSearch == null)
                            tstSessionSearch = new TestSessionSearch();
                        if (!Utility.IsNullOrEmpty(datareader["INTERVIEW_TEST_SESSION_ID"]))
                            tstSessionSearch.TestSessionID = datareader["INTERVIEW_TEST_SESSION_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                            tstSessionSearch.CandidateSessionID = datareader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CLIENT_REQUEST_NUMBER"]))
                            tstSessionSearch.ClientRequestNumber = datareader["CLIENT_REQUEST_NUMBER"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["DATE_OF_PURCHASE"]))
                            tstSessionSearch.DateofPurchase = datareader["DATE_OF_PURCHASE"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CREDITS"]))
                            tstSessionSearch.Credits = datareader["CREDITS"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["ADMINISTERED_BY_ID"]))
                            tstSessionSearch.AdministeredID = Convert.ToInt32(datareader["ADMINISTERED_BY_ID"].ToString());
                        if (!Utility.IsNullOrEmpty(datareader["ADMINISTERED_BY"]))
                            tstSessionSearch.AdministeredBy = datareader["ADMINISTERED_BY"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["AUTHOR_FULLNAME"]))
                            tstSessionSearch.TestAuthorFullName = datareader["AUTHOR_FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_ID"]))
                            tstSessionSearch.CandidateID = Convert.ToInt32(datareader["CANDIDATE_ID"]);
                        if (!Utility.IsNullOrEmpty(datareader["CANIDATE_NAME"]))
                            tstSessionSearch.CandidateName = datareader["CANIDATE_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_FULLNAME"]))
                            tstSessionSearch.CandidateFullName = datareader["CANDIDATE_FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(datareader["DATE_OF_INTERVIEW"]))
                            tstSessionSearch.DateofTest = Convert.ToDateTime(datareader["DATE_OF_INTERVIEW"]).ToString("MM/dd/yyyy");
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_CANDIDATE_ICON"]))
                            tstSessionSearch.CandidateVisibleStatus = Convert.ToBoolean(datareader["SHOW_CANDIDATE_ICON"]);
                        else
                            tstSessionSearch.CandidateVisibleStatus = false;
                        if (!Utility.IsNullOrEmpty(datareader["CANCEL_REASON"]))
                            tstSessionSearch.CancelReason = datareader["CANCEL_REASON"].ToString().Replace("\r\n", "<br />");
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_CANCEL_ICON"]))
                            tstSessionSearch.ShowCancelIcon = Convert.ToBoolean(datareader["SHOW_CANCEL_ICON"]);
                        else
                            tstSessionSearch.ShowCancelIcon = false;
                        if (!Utility.IsNullOrEmpty(datareader["ATTEMPT_ID"]))
                            tstSessionSearch.AttemptID = Convert.ToInt32(datareader["ATTEMPT_ID"]);
                        else
                            tstSessionSearch.AttemptID = 0;
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_INTERVIEW_SCORE"]))
                            tstSessionSearch.ShowTestScore = Convert.ToBoolean(datareader["SHOW_INTERVIEW_SCORE"]);
                        else
                            tstSessionSearch.ShowTestScore = false;

                        testSessionSearch.Add(tstSessionSearch);
                        tstSessionSearch = null;
                    }
                }
                return testSessionSearch;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }

        /// <summary>
        /// Represents the method to get the bool value to show candidate
        /// test score
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the candidate session id
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds whether to display score or not
        /// </returns>
        public bool GetInterviewShowTestScore(string candidateSessionID)
        {
            try
            {
                // Check if object type is given.
                if (Utility.IsNullOrEmpty(candidateSessionID))
                {
                    throw new Exception
                        ("Candidate session ID cannot be null or empty");
                }

                // This SP helps to generate Question ID 
                DbCommand getCandidateScoreDetails = HCMDatabase
                    .GetStoredProcCommand("SPGET_INTERVIEW_SHOW_TEST_SCORE");

                HCMDatabase.AddInParameter(getCandidateScoreDetails,
                    "@CAND_SESSION_KEYS", DbType.String, candidateSessionID.Trim());

                object value = HCMDatabase.ExecuteScalar(getCandidateScoreDetails);

                if (value != null)
                {
                    return Convert.ToBoolean(value);
                }
                return false;
            }
            finally
            { 
            }
        }

        /// <summary>
        /// Inserts candidate test session details 
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="TestSessionDetail"/> that contains the TestSessionDetail.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>
        public void InsertCandidateTestSession(CandidateTestSessionDetail candidateTestSessionDetail,
          IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertCandidateTestSession
                = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_CANDIDATE_SESSION");
            // Add input parameter
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@CAND_SESSION_KEY", DbType.String, candidateTestSessionDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@SESSION_KEY", DbType.String, candidateTestSessionDetail.TestSessionID);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@STATUS", DbType.String, candidateTestSessionDetail.Status);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@CREATED_BY", DbType.Int32, candidateTestSessionDetail.CreatedBy);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@MODIFIED_BY", DbType.Int32, candidateTestSessionDetail.ModifiedBy);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertCandidateTestSession, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will update the test session details based on the test session id
        /// </summary>
        /// <param name="testSessionDetail">
        ///  A <see cref="TestSessionDetail"/> that contains the test session detail.
        /// </param>
        /// <param name="userID">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction.
        /// </param>
        public void UpdateInterviewTestSession(InterviewTestSessionDetail testSessionDetail, int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand updateTestSessionCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_INTERVIEW_TEST_SESSION");

            // Add input parameters
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@SESSION_KEY", DbType.String, testSessionDetail.TestSessionID);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@SESSION_COUNT", DbType.Int32, testSessionDetail.NumberOfCandidateSessions);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@CLIENT_REQUEST_NUMBER", DbType.Int32, testSessionDetail.PositionProfileID);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@SESSION_EXPIRY", DbType.DateTime, testSessionDetail.ExpiryDate);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
              "@ALLOW_PAUSE_INTERVIEW", DbType.String, testSessionDetail.AllowPauseInterview);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@ASSESSOR_IDS", DbType.String, testSessionDetail.AssessorIDs);
            HCMDatabase.AddInParameter(updateTestSessionCommand,
                "@USER_ID", DbType.Int32, testSessionDetail.ModifiedBy);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(updateTestSessionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will update the test description details against the test session id.
        /// </summary>
        /// <param name="testSessionDetail">
        ///  A <see cref="TestSessionDetail"/> that contains the test session detail.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction.
        /// </param>
        public void UpdateTestDescription(InterviewTestSessionDetail testSessionDetail,
            int userID, IDbTransaction transaction)
        {
            // Create command object
            DbCommand updateTestDescriptionCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_INTERVIEW_TEST_SESSION_DESC");

            // Add input parameter
            HCMDatabase.AddInParameter(updateTestDescriptionCommand,
                "@SESSION_KEY", DbType.String, testSessionDetail.TestSessionID);
            HCMDatabase.AddInParameter(updateTestDescriptionCommand,
                "@TEST_SESSION_DESC", DbType.String, testSessionDetail.TestSessionDesc);
            HCMDatabase.AddInParameter(updateTestDescriptionCommand,
                "@INSTRUCTIONS", DbType.String, testSessionDetail.Instructions);
            HCMDatabase.AddInParameter(updateTestDescriptionCommand,
                "@USER_ID", DbType.Int32, testSessionDetail.ModifiedBy);

            HCMDatabase.ExecuteNonQuery(updateTestDescriptionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Inserts Interview Test session details in the [INTERVIEW_TEST_SESSION] table object
        /// </summary>
        /// <param name="interviewSessionDetail"></param>
        /// <param name="userID"></param>
        /// <param name="transaction"></param>
        public void InsertInterviewTestSession(InterviewSessionDetail interviewSessionDetail,
            int userID, IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertTestSessionCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_SESSION");

            // Add input parameters
            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionDetail.InterviewTestSessionID);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, interviewSessionDetail.InterviewTestID);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@SESSION_COUNT", DbType.Int32, interviewSessionDetail.NumberOfCandidateSessions);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                 "@CLIENT_REQUEST_NUMBER", DbType.Int32, interviewSessionDetail.PositionProfileID);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@SESSION_EXPIRY", DbType.DateTime, interviewSessionDetail.ExpiryDate);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
                "@USER_ID", DbType.Int32, interviewSessionDetail.CreatedBy);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
               "@ASSESSOR_IDS", DbType.String, interviewSessionDetail.AssessorIDs);

            HCMDatabase.AddInParameter(insertTestSessionCommand,
               "@ALLOW_PAUSE_INTERVIEW", DbType.String, interviewSessionDetail.AllowPauseInterview);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertTestSessionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Inserts candidate session details into [INTERVIEW_SESSION_CANDIDATE] and INTERVIEW_CANDIDATE_SESSION_TRACKING
        /// </summary>
        /// <param name="candidateTestSessionDetail"></param>
        /// <param name="transaction"></param>
        public void InsertCandidateInterviewSession(CandidateInterviewSessionDetail candidateInterviewSessionDetail, string assessorIds,
            IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertCandidateTestSession
                = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_SESSION_CANDIDATE");
            // Add input parameter
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateInterviewSessionDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@INTERVIEW_SESSION_KEY", DbType.String, candidateInterviewSessionDetail.InterviewTestSessionID);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@STATUS", DbType.String, candidateInterviewSessionDetail.Status);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@CREATED_BY", DbType.Int32, candidateInterviewSessionDetail.CreatedBy);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
                "@MODIFIED_BY", DbType.Int32, candidateInterviewSessionDetail.ModifiedBy);
            HCMDatabase.AddInParameter(insertCandidateTestSession,
               "@ASSESSOR_IDS", DbType.String, assessorIds);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertCandidateTestSession, transaction as DbTransaction);
        }

        /// <summary>
        /// Inserts Interview session descrption details in the INTERVIEW_TEST_SESSION_DESC table
        /// </summary>
        /// <param name="interviewSessionDetail"></param>
        /// <param name="transaction"></param>
        public void InsertInterviewSessionDescription(InterviewSessionDetail interviewSessionDetail, IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertTestDescriptionCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_TEST_SESSION_DESC");

            // Add input parameter
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionDetail.InterviewTestSessionID);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@INTERVIEW_SESSION_DESC", DbType.String, interviewSessionDetail.InterviewSessionDesc);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@INSTRUCTIONS", DbType.String, interviewSessionDetail.Instructions);
            HCMDatabase.AddInParameter(insertTestDescriptionCommand,
                "@USER_ID", DbType.Int32, interviewSessionDetail.CreatedBy);

            HCMDatabase.ExecuteNonQuery(insertTestDescriptionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testID"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="sortDirection"></param>
        /// <param name="userID"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public List<CandidateInterviewSessionDetail> GetCandidateInterviewSessions
            (string interviewTestkey, int pageNumber, int pageSize, string orderBy,
            SortType sortDirection, int userID, out int totalRecords)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateDetailCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_SESSION_BY_TEST_KEY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@INTERVIEW_TEST_KEY", DbType.String, interviewTestkey);
                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                  "@INTERVIEW_SESSION_AUTHOR", DbType.String, userID);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                        "@ORDERBYDIRECTION",
                         DbType.String, sortDirection == SortType.Ascending ?
                         Constants.SortTypeConstants.ASCENDING :
                         Constants.SortTypeConstants.DESCENDING);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateDetailCommand);

                List<CandidateInterviewSessionDetail> candidateDetailColl = null;

                CandidateInterviewSessionDetail candSessionDetail = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (candidateDetailColl == null)
                            candidateDetailColl = new List<CandidateInterviewSessionDetail>();
                        candSessionDetail = new CandidateInterviewSessionDetail();
                        candSessionDetail.InterviewTestID = dataReader["INTERVIEW_TEST_KEY"].ToString();
                        candSessionDetail.InterviewTestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString();
                        candSessionDetail.CandidateTestSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();
                        candSessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());

                        if (dataReader["POSITION_PROFILE_NAME"] != null)
                            candSessionDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["PURCHASE_DATE"]))
                            candSessionDetail.CreatedDate = Convert.ToDateTime(dataReader["PURCHASE_DATE"].ToString());
                        candSessionDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candSessionDetail.CandidateFullName = dataReader["CANDIDATE_FULLNAME"].ToString().Trim();
                        candSessionDetail.InteriewSessionAuthor = dataReader["ADMINISTERED_BY"].ToString();
                        candSessionDetail.InterviewSessionAuthorFullName = dataReader["TEST_AUTHOR_FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candSessionDetail.ScheduledDate = Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                        candSessionDetail.Status = dataReader["SESSION_STATUS"].ToString();
                        // Add the testSession to the collection.

                        candidateDetailColl.Add(candSessionDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return candidateDetailColl;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public TestSessionDetail GetInterviewCandidateSessions(string interviewSessionID)
        {
            IDataReader dataReader = null;

            try
            {
                // Check if object type is given.
                if (Utility.IsNullOrEmpty(interviewSessionID))
                {
                    throw new Exception
                        ("Test session ID cannot be null or empty");
                }
                TestSessionDetail testSessionDetail = null;

                // This SP helps to generate Question ID 
                DbCommand getCandidateSessionsCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_INTERVIEW_CANDIDATE_SESSIONS");

                HCMDatabase.AddInParameter(getCandidateSessionsCommand,
                    "@INTERVIEW_SESSION_ID", DbType.String, interviewSessionID.Trim());

                HCMDatabase.AddOutParameter(getCandidateSessionsCommand,
                  "@INTERVIEW_CANDIDATE_SESSIONS", DbType.String, 1000);

                dataReader = HCMDatabase.ExecuteReader
                    (getCandidateSessionsCommand);
                if (dataReader.Read())
                {
                    testSessionDetail = new TestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                        testSessionDetail.UserName = dataReader["NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        testSessionDetail.Email = dataReader["EMAIL"].ToString();
                }
                if (dataReader != null)
                    dataReader.Close();

                if (testSessionDetail != null)
                {
                    testSessionDetail.CandidateSessions = Convert.ToString
                        (HCMDatabase.GetParameterValue(getCandidateSessionsCommand, "@INTERVIEW_CANDIDATE_SESSIONS"));
                }
                return testSessionDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Inserts assessor and skill id into [INTERVIEW_TEST_SESSION_ASSESSOR] 
        /// </summary>
        /// <param name="interviewSessionKey">interviewSessionKey</param>
        /// <param name="assessorId">assessorId</param>
        /// <param name="skillIds">skillIds</param>
        /// <param name="userID">userID</param>
        /// <param name="transaction"></param>
        public void InsertInterviewSessionAssessorSkills(string interviewSessionKey, int assessorId,
            string skillIds, int userID, IDbTransaction transaction)
        {
            AssessorDetail assessorDetails = new AssessorDetail();
            // Create command object
            DbCommand insertAssessorSkill
                = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_ASSESSOR_AND_SKILL");
            // Add input parameter
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionKey);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@ASSESSOR_ID", DbType.Int32, assessorId);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@SKILL_ID", DbType.String, skillIds);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@CREATED_BY", DbType.Int32, userID);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAssessorSkill, transaction as DbTransaction);
        }

        /// <summary>
        /// Update assessor and skill id in [INTERVIEW_TEST_SESSION_ASSESSOR] & [INTERVIEW_SESSION_ASSESSOR_SKILLS]
        /// </summary>
        /// <param name="interviewSessionKey">interviewSessionKey</param>
        /// <param name="assessorId">assessorId</param>
        /// <param name="skillIds">skillIds</param>
        /// <param name="userID">userID</param>
        /// <param name="transaction"></param>
        public void UpdateInterviewSessionAssessorSkills(string interviewSessionKey, int assessorId,
            string skillIds, int userID, IDbTransaction transaction)
        {
            AssessorDetail assessorDetails = new AssessorDetail();
            // Create command object
            DbCommand updateAssessorSkill
                = HCMDatabase.GetStoredProcCommand("SPUPDATE_INTERVIEW_ASSESSOR_AND_SKILL");
            // Add input parameter
            HCMDatabase.AddInParameter(updateAssessorSkill,
                "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionKey);
            HCMDatabase.AddInParameter(updateAssessorSkill,
                "@ASSESSOR_ID", DbType.Int32, assessorId);
            HCMDatabase.AddInParameter(updateAssessorSkill,
                "@SKILL_ID", DbType.String, skillIds);
            HCMDatabase.AddInParameter(updateAssessorSkill,
                "@CREATED_BY", DbType.Int32, userID);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(updateAssessorSkill, transaction as DbTransaction);
        }

        /// <summary>
        /// Method to delete the interview session assessor and skill
        /// </summary>
        /// <param name="interviewSessionKey">interviewSessionKey</param>
        /// <param name="assessorIds">assessorIds</param>
        public void DeleteInterviewSessionAssessorSkills(string interviewSessionKey, string assessorIds, IDbTransaction transaction)
        {
            DbCommand deleteInterviewSessionAssessorCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_INTERVIEW_SESSION_ASSESSOR");
            HCMDatabase.AddInParameter(deleteInterviewSessionAssessorCommand,
                "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionKey);
            HCMDatabase.AddInParameter(deleteInterviewSessionAssessorCommand,
                "@ASSESSOR_IDS", DbType.String, assessorIds);
            HCMDatabase.ExecuteNonQuery(deleteInterviewSessionAssessorCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Retrieves subject id list 
        /// </summary>
        /// <param name="interviewTestKey"></param>
        /// <returns></returns>
        public string GetInterviewTestSubjectIdsByKey(string interviewTestKey)
        {
            IDataReader dataReader = null;
            string skillids = string.Empty;
            try
            {
                DbCommand getSubjectIds = HCMDatabase.
                       GetStoredProcCommand("SPGET_INTERVIEW_TEST_SUBJECTS_IDS_BY_INTERVIEW_TEST_KEY");

                HCMDatabase.AddInParameter(getSubjectIds,
                    "@INTERVIEW_TEST_KEY", DbType.String, interviewTestKey);

                dataReader = HCMDatabase.ExecuteReader(getSubjectIds);
                while (dataReader.Read())
                {

                    if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"].ToString()))
                    {
                        skillids = skillids + dataReader["CAT_SUB_ID"].ToString() + ",";
                    }
                }
                return skillids.ToString().TrimEnd(',');
            }
            catch
            {
                return null;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves list of assesor 
        /// (Display only assessors that have that have provided time slots) 
        /// </summary>
        /// <param name="referenceType">
        /// A <see cref="string"/> that holds the reference type.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status ids.
        /// </param>
        /// <returns>
        /// A <see cref="DataTatable"/> that holds the summary data.
        /// </returns>
        public DataTable GetAssessorListByHaveProvidedTimeSlots(string status)
        {
            DataSet assessorDetails = new DataSet();
            DbCommand assessorsTimeSlotsCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_ASSESSOR_BY_HAVE_PROVIDED_TIME_SLOTS");

            HCMDatabase.AddInParameter(assessorsTimeSlotsCommand,
                "@STATUS", DbType.String, status);

            // Execute the stored procedure.
            HCMDatabase.LoadDataSet(assessorsTimeSlotsCommand,
                assessorDetails, "AssessorTimeSlots");
            return assessorDetails.Tables[0];
        }


        /// <summary>
        /// Method that retrieves the assessor and skill id against its interview session key
        /// </summary>
        /// <param name="interviewSessionkey">
        /// A <see cref="string"/> that holds the interviewSessionkey type.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status ids.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetOnlineAssessorAndSkillIdBySessionKey(string interviewSessionkey)
        {
            DbCommand assessorsAndSkillCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_ONLINE_INTERVIEW_SESSION_ASSESSOR_AND_SKILL");

            HCMDatabase.AddInParameter(assessorsAndSkillCommand,
                "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionkey);

            // Execute the stored procedure.
            return HCMDatabase.ExecuteDataSet(assessorsAndSkillCommand);
        }

        /// <summary>
        /// Method that retrieves the assessor and skill id against its interview session key or candidate key
        /// </summary>
        /// <param name="key">
        /// A <see cref="string"/> that holds the key type.
        /// </param>
        /// <param name="type">
        /// A <see cref="type"/> that holds the type.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the positionProfileID.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetAssessorAndSkillBySessionOrCandidateKey(
            string key, string type, int positionProfileID)
        {
            DbCommand assessorsAndSkillCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_INTERVIEW_SESSION_ASSESSOR_AND_SKILL");

            HCMDatabase.AddInParameter(assessorsAndSkillCommand,
                "@KEY", DbType.String, key);

            HCMDatabase.AddInParameter(assessorsAndSkillCommand,
                "@TYPE", DbType.String, type);

            HCMDatabase.AddInParameter(assessorsAndSkillCommand,
              "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            // Execute the stored procedure.
            return HCMDatabase.ExecuteDataSet(assessorsAndSkillCommand);
        }

        /// <summary>
        /// Method that retrieves the assessor and skill id against its interview session key or candidate key
        /// </summary>
        /// <param name="key">
        /// A <see cref="string"/> that holds the key type.
        /// </param>
        /// <param name="type">
        /// A <see cref="type"/> that holds the type.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetOnlineAssessorAndSkillBySessionOrCandidateKey(
            string key, string type)
        {
            DbCommand assessorsAndSkillCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_ONLINE_INTERVIEW_SESSION_ASSESSOR_AND_SKILL");

            HCMDatabase.AddInParameter(assessorsAndSkillCommand,
                "@KEY", DbType.String, key);

            HCMDatabase.AddInParameter(assessorsAndSkillCommand,
                "@TYPE", DbType.String, type);

            // Execute the stored procedure.
            return HCMDatabase.ExecuteDataSet(assessorsAndSkillCommand);
        }

        /// <summary>
        /// This method gets the interview session id details using navigatekey.
        /// </summary>
        /// A <param name="navigateKey"> holds the navigate key.</param>
        /// <returns></returns>
        public InterviewSessionIdDetails GetInterviewSessionIdDetails(string navigateKey)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getInterviewSessionIdDetails = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_CANDIDATE_SESSION_TRACKING_BY_NAVIGATEKEY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getInterviewSessionIdDetails,
                    "@NAVIGATE_KEY", DbType.String, navigateKey);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getInterviewSessionIdDetails);

                InterviewSessionIdDetails interviewSessionIdDetails = null;

                if (dataReader.Read())
                {
                    // Instantiate the candidate test session id details instance
                    if (interviewSessionIdDetails == null)
                        interviewSessionIdDetails = new InterviewSessionIdDetails();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        interviewSessionIdDetails.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        interviewSessionIdDetails.CandidateSessionId =
                            dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        interviewSessionIdDetails.CandidateId =
                            Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());
                }

                return interviewSessionIdDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of skills for the given 
        /// candidate session id
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetSkillsByCandidateSessionKey(string candidateSessionKey)
        {
            List<SkillDetail> skills = null;
            IDataReader dataReader = null;

            try
            {
                DbCommand getSkillCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_SUBJECTS_BY_CANDIDATE_SESSION_KEY");

                HCMDatabase.AddInParameter(getSkillCommand, "@CANDIDATE_INTERVIEW_SESSION_KEY", DbType.String,
                    Utility.IsNullOrEmpty(candidateSessionKey) ? null : candidateSessionKey.Trim());

                dataReader = HCMDatabase.ExecuteReader(getSkillCommand);

                while (dataReader.Read())
                {
                    // Instantiate skill list.
                    if (skills == null)
                        skills = new List<SkillDetail>();

                    // Create a skill object.
                    SkillDetail skill = new SkillDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                    {
                        skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                    {
                        skill.SkillName = dataReader["SKILL_NAME"].ToString();
                    }

                    // Add to the list.
                    skills.Add(skill);
                }

                return skills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that insert record into external assessor table and 
        /// returns the external assessor id.
        /// </summary>
        /// <param name="email">
        /// A <see cref="string"/> that holds the external assessor email id.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the external assessor id.
        /// </returns>
        public int InsertExternalAssessorDetail(string email, int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object
            DbCommand insertExternalAssessorDetailCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_EXTERNAL_ASSESSOR_DETAIL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertExternalAssessorDetailCommand,
                "@EXTERNAL_EMAIL", DbType.String, email);
            HCMDatabase.AddInParameter(insertExternalAssessorDetailCommand,
                "@CREATED_BY", DbType.Int32, userID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(insertExternalAssessorDetailCommand,
                "@EXTERNAL_ASSESSOR_ID", DbType.Int32, 0);

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(insertExternalAssessorDetailCommand, transaction as DbTransaction);

            if (insertExternalAssessorDetailCommand.Parameters["@EXTERNAL_ASSESSOR_ID"].Value != null)
            {
                // Retrieve and assign external assessor ID.
                return int.Parse(insertExternalAssessorDetailCommand.Parameters["@EXTERNAL_ASSESSOR_ID"].Value.ToString());
            }
            else
            {
                return 0;
            }
        }


        /// <summary>
        /// Method that updates external assessor detail
        /// </summary>
        /// <param name="externalAssessorDetail">
        /// A <see cref="externalAssessorDetail"/> that holds the external assessor detail.
        /// </param>
        public void UpdateExternalAssessorDetail(ExternalAssessorDetail externalAssessorDetail)
        {
            // Create a stored procedure command object
            DbCommand updateExternalAssessorDetailCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_EXTERNAL_ASSESSOR_DETAIL");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateExternalAssessorDetailCommand,
                "@EXTERNAL_ASSESSOR_ID", DbType.String, externalAssessorDetail.ExternalAssessorID);

            HCMDatabase.AddInParameter(updateExternalAssessorDetailCommand,
                "@EMAIL", DbType.String, externalAssessorDetail.Email);

            HCMDatabase.AddInParameter(updateExternalAssessorDetailCommand,
                "@FIRST_NAME", DbType.String, externalAssessorDetail.FirstName);

            HCMDatabase.AddInParameter(updateExternalAssessorDetailCommand,
                "@LAST_NAME", DbType.String, externalAssessorDetail.LastName);

            HCMDatabase.AddInParameter(updateExternalAssessorDetailCommand,
                "@CONTACT_NUMBER", DbType.String, externalAssessorDetail.ContactNumber);

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(updateExternalAssessorDetailCommand);

        }

        /// <summary>
        /// Method that insert record into external assessor candidate session table
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that holds the candidate session id.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds attempt ID.
        /// </param>
        /// <param name="externalAssessorID">
        /// A <see cref="int"/> that holds the external assessor ID.
        /// </param>
        /// <param name="externalKey">
        /// A <see cref="string"/> that holds the external unique.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the external key.
        /// </returns>
        public string InsertExternalAssessorCandidateSessionDetail(string candidateSessionKey, int attempID, 
            int externalAssessorID, string externalKey, int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object
            DbCommand insertExternalAssessorCandiateSessionCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_EXTERNAL_CANDIDATE_SESSION_DETAIL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertExternalAssessorCandiateSessionCommand,
                "@EXTERNAL_ASSESSOR_ID", DbType.Int32, externalAssessorID);
            HCMDatabase.AddInParameter(insertExternalAssessorCandiateSessionCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(insertExternalAssessorCandiateSessionCommand,
                "@ATTEMPT_ID", DbType.Int32, attempID);
            HCMDatabase.AddInParameter(insertExternalAssessorCandiateSessionCommand,
                "@EXTERNAL_KEY", DbType.String, externalKey);
            HCMDatabase.AddInParameter(insertExternalAssessorCandiateSessionCommand,
                "@CREATED_BY", DbType.Int32, userID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(insertExternalAssessorCandiateSessionCommand,
                "@EXT_EXTERNAL_KEY", DbType.String, 10);

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(insertExternalAssessorCandiateSessionCommand, transaction as DbTransaction);

            if (!Utility.IsNullOrEmpty(insertExternalAssessorCandiateSessionCommand.Parameters["@EXT_EXTERNAL_KEY"].Value))
            {
                // Retrieve and assign external key against this session.
                return insertExternalAssessorCandiateSessionCommand.Parameters["@EXT_EXTERNAL_KEY"].Value.ToString();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Method that validates whether generated key is already exists or not
        /// </summary>
        /// <param name="externalKey">
        /// A <see cref="string"/> that contains external generated key.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains true or false.
        /// </returns>
        public bool IsExternalKeyExists(string externalKey)
        {
            
            // Create a stored procedure command object.
            DbCommand getExternalKeyCommand = HCMDatabase.
                 GetStoredProcCommand("SPGET_EXTERNAL_ASSESSOR_KEY");

            HCMDatabase.AddInParameter(getExternalKeyCommand,
               "@EXTERNAL_KEY", System.Data.DbType.String, externalKey);

            HCMDatabase.AddOutParameter(getExternalKeyCommand,
               "@ISEXIST", System.Data.DbType.String, 5);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(getExternalKeyCommand);

            if (!Utility.IsNullOrEmpty(getExternalKeyCommand.Parameters["@EXTERNAL_KEY"].Value) &&
                getExternalKeyCommand.Parameters["@ISEXIST"].Value.ToString().ToUpper() == "Y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This method gets the external assessor details against its unique key.
        /// </summary>
        /// <param name="externalKey">
        /// A <see cref="string"/> that contains external generated key.
        /// </param>
        /// <returns>
        /// A <see cref="ExternalAssessorDetail"/> that contains external assessor details.
        /// </returns>
        public ExternalAssessorDetail GetExternalAssessorDetailByKey(string externalKey)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getExternalAssessorDetails = HCMDatabase.
                    GetStoredProcCommand("SPGET_EXTERNAL_ASSESSOR_DETAIL_BY_KEY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getExternalAssessorDetails,
                    "@EXTERNAL_KEY", DbType.String, externalKey);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getExternalAssessorDetails);

                ExternalAssessorDetail externalAssessorDetails = null;

                if (dataReader.Read())
                {
                    // Instantiate the external assessor detail instance
                    if (externalAssessorDetails == null)
                        externalAssessorDetails = new ExternalAssessorDetail();


                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        externalAssessorDetails.InterviewKey =
                            dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                        externalAssessorDetails.InterviewSessionKey =
                            dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        externalAssessorDetails.CandidateInterviewSessionkey =
                            dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        externalAssessorDetails.AttemptID =
                            Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESMENT_STATUS"]))
                        externalAssessorDetails.AssessmentStatus =
                            dataReader["ASSESMENT_STATUS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EXTERNAL_ASSESSOR_ID"]))
                        externalAssessorDetails.ExternalAssessorID = 
                            Convert.ToInt32(dataReader["EXTERNAL_ASSESSOR_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        externalAssessorDetails.Email = dataReader["EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        externalAssessorDetails.FirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        externalAssessorDetails.LastName = dataReader["LAST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CONTACT_NUMBER"]))
                        externalAssessorDetails.ContactNumber = dataReader["CONTACT_NUMBER"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        externalAssessorDetails.ExternalAssessorCreatedByID =
                            Convert.ToInt32(dataReader["CREATED_BY"].ToString());

                }

                return externalAssessorDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method insert/updates newly added assessor and skill 
        /// to scheduled/completed candidate sessions.
        /// </summary>
        /// <param name="interviewSessionKey">
        /// A <see cref="string"/> that contains interview session key.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that contains assessor ID.
        /// </param>
        /// <param name="skillIds">
        /// A <see cref="string"/> that contains selected skill ids.
        /// </param>
        /// <param name="candidateInterviewStatus">
        /// A <see cref="string"/> that contains candidate interview status[SESS_COMP,SESS_SCHD].
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>        
        public void InsertAssessorForScheduledAndCompletedCandidates(string interviewSessionKey, int assessorID,
            string skillIds, string candidateInterviewStatus, int userID, IDbTransaction transaction)
        {
            
            // Create command object
            DbCommand insertAssessorSkillCommand
                = HCMDatabase.GetStoredProcCommand("SPINSERT_ASSESSOR_FOR_SCHEDULED_COMPLETED_CANDIDATES");
            
            // Add input parameter
            HCMDatabase.AddInParameter(insertAssessorSkillCommand,
                "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionKey);
            HCMDatabase.AddInParameter(insertAssessorSkillCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);
            HCMDatabase.AddInParameter(insertAssessorSkillCommand,
                "@SKILL_IDS", DbType.String, skillIds);
            HCMDatabase.AddInParameter(insertAssessorSkillCommand,
                "@CANDIDATE_INTERVIEW_STATUS", DbType.String, candidateInterviewStatus);
            HCMDatabase.AddInParameter(insertAssessorSkillCommand,
                "@CREATED_BY", DbType.Int32, userID);

            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAssessorSkillCommand,
                transaction as DbTransaction);
        }

        #endregion
    }
}
