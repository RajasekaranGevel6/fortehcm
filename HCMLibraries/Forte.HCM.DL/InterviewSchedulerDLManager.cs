﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewSchedulerDLManager.cs
// File that represents the data layer for the Test Scheduler module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.DL
{
    public class InterviewSchedulerDLManager : DatabaseConnectionManager
    {

        public List<InterviewSessionDetail> SearchInterviewTestSessionDetails
          (TestSessionSearchCriteria testSessionCriteria, int pageNumber,
          int pageSize, out int totalRecords,
          string sortingKey, SortType sortByDirection)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_TEST_SESSION_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@INTERVIEW_TESTNAME", DbType.String, testSessionCriteria.TestName);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@INTERVIEW_TESTID", DbType.String, testSessionCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@INTERVIEW_SESSIONID", DbType.String, testSessionCriteria.SessionKey);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@INTERVIEW_SHEDULER_ID", DbType.String, testSessionCriteria.SchedulerNameID == 0 ? null : testSessionCriteria.SchedulerNameID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_AUTHOR_ID", DbType.Int32, testSessionCriteria.TestSessionCreatorID == 0 ? null : testSessionCriteria.TestSessionCreatorID);

                if (testSessionCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSessionCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBY",
                    DbType.String, sortingKey);

                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBYDIRECTION",
                    DbType.String, sortByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                List<InterviewSessionDetail> interviewtestSessionCollection = null;

                InterviewSessionDetail interviewTestSession = null;
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (interviewtestSessionCollection == null)
                            interviewtestSessionCollection = new List<InterviewSessionDetail>();
                        interviewTestSession = new InterviewSessionDetail();
                        interviewTestSession.InterviewTestName = dataReader["TEST_NAME"].ToString();
                        interviewTestSession.InterviewTestID = dataReader["INTERVIEW_TEST_KEY"].ToString();
                        interviewTestSession.InterviewTestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString();
                        interviewTestSession.InteriewSessionAuthor = dataReader["INTERVIEW_AUTHOR_NAME"].ToString();
                        interviewTestSession.InterviewSessionAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        interviewTestSession. NumberOfQuestions = Convert.ToInt32(dataReader["TOTAL_QUESTION"].ToString());
                        interviewTestSession.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());
                        //testSession.TotalCredit = Convert.ToDecimal(dataReader["TEST_COST"].ToString());
                        //testSession.IsCertification = (dataReader["TEST_COST"].ToString() == "Y") ? true : false;
                        interviewTestSession.IsCertification = (dataReader["CERTIFICATION"].ToString() == "Y") ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_DESC"]))
                            interviewTestSession.InterviewSessionDesc = dataReader["INTERVIEW_SESSION_DESC"].ToString().Trim();

                        // Add the testSession to the collection.
                        interviewtestSessionCollection.Add(interviewTestSession);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return interviewtestSessionCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Searches the assessor details.
        /// </summary>
        /// <param name="searchAssessor">The search assessor.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="sortingKey">The sorting key.</param>
        /// <param name="sortByDirection">The sort by direction.</param>
        /// <returns></returns>
        public List<TestSessionSearchCriteria> SearchAssessorDetails(TestSessionSearchCriteria searchAssessor, int pageNumber,
            int pageSize, out int totalRecords, string sortingKey, SortType sortByDirection)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getAssessorCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ASSESSOR_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getAssessorCommand,
                    "@FIRSTNAME", DbType.String, searchAssessor.AssessorFirstName);
                HCMDatabase.AddInParameter(getAssessorCommand,
                    "@LASTNAME", DbType.String, searchAssessor.AssessorLastName);
                HCMDatabase.AddInParameter(getAssessorCommand,
                    "@DATE_FROM", DbType.String, searchAssessor.FromDate);
                HCMDatabase.AddInParameter(getAssessorCommand,
                    "@DATE_TO", DbType.String, searchAssessor.ToDate);
                HCMDatabase.AddInParameter(getAssessorCommand,
                    "@SKILLS", DbType.String, searchAssessor.Skill);
                HCMDatabase.AddInParameter(getAssessorCommand,
                    "@USER_ID", DbType.Int32, searchAssessor.UserID);
                HCMDatabase.AddInParameter(getAssessorCommand, "@ORDER_BY_DIRECTION",
                   DbType.String, sortByDirection == SortType.Ascending ?
                   Constants.SortTypeConstants.ASCENDING :
                   Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getAssessorCommand, "@SORT_EXPRESSION",
               DbType.String, sortingKey);
                HCMDatabase.AddInParameter(getAssessorCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getAssessorCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessorCommand);

                List<TestSessionSearchCriteria> assessorCollection = null;

                TestSessionSearchCriteria assessor = null;
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (assessorCollection == null)
                            assessorCollection = new List<TestSessionSearchCriteria>();
                        assessor = new TestSessionSearchCriteria();
                        assessor.AssessorID = Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString());
                        assessor.Email = dataReader["EMAIL"].ToString();
                        assessor.AssessorFirstName = dataReader["FIRST_NAME"].ToString();
                        assessor.AssessorLastName = dataReader["LAST_NAME"].ToString();
                        assessor.CreatedDate = dataReader["CREATED_DATE"].ToString();
                        assessor.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());
                        // Add the testSession to the collection.
                        assessorCollection.Add(assessor);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return assessorCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public InterviewSessionDetail GetInteviewSessionDetail(string interviewSessionID, string candidateSessionID,
            string sortExpression, SortType sortDirection)
        {

            {
                IDataReader dataReader = null;
                try
                {
                    // Create a stored procedure command object.
                    DbCommand getInterviewSessionCommand = HCMDatabase.
                        GetStoredProcCommand("SPGET_INTERVIEWSESSIONDETAILS");

                    // Add input parameters.
                    HCMDatabase.AddInParameter(getInterviewSessionCommand,
                        "@SESSION_KEY", DbType.String, interviewSessionID);
                    HCMDatabase.AddInParameter(getInterviewSessionCommand,
                        "@CAND_SESSION_KEYS", DbType.String, candidateSessionID);
                    HCMDatabase.AddInParameter(getInterviewSessionCommand, "@ORDERBY", DbType.String, sortExpression);
                    HCMDatabase.AddInParameter(getInterviewSessionCommand, "@ORDERBYDIRECTION",
                        DbType.String, sortDirection == SortType.Ascending ?
                        Constants.SortTypeConstants.ASCENDING :
                        Constants.SortTypeConstants.DESCENDING);

                    // Execute the stored procedure.
                    dataReader = HCMDatabase.ExecuteReader(getInterviewSessionCommand);

                    InterviewSessionDetail  interviewSession = null;
                    List<CandidateInterviewSessionDetail> candidateInterviewSessions = null;
                    while (dataReader.Read())
                    {
                        // Instantiate the testSession instance
                        if (interviewSession == null)
                            interviewSession = new InterviewSessionDetail();

                        // Assign property values to the testSession object.
                        interviewSession.InterviewTestID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                        interviewSession.AllowPauseInterview = Convert.ToChar(dataReader["ALLOW_PAUSE_INTERVIEW"].ToString());

                        interviewSession.InterviewTestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                        interviewSession.InterviewTestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                        interviewSession.ExpiryDate = Convert.ToDateTime(dataReader["SESSION_EXPIRY"].ToString());
                        interviewSession.Instructions = dataReader["INSTRUCTIONS"].ToString();
                        interviewSession.InterviewSessionDesc = dataReader["INTERVIEW_SESSION_DESC"].ToString();
                        interviewSession.InteriewSessionAuthor = dataReader["SESSION_AUTHOR_NAME"].ToString().Trim();
                        interviewSession.InterviewSessionAuthorEmail = dataReader["SESSION_AUTHOR_EMAIL"].ToString().Trim();
                        interviewSession.NumberOfCandidateSessions = Convert.ToInt32(dataReader["SESSION_COUNT"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                            interviewSession.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                            interviewSession.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                            interviewSession.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                            interviewSession.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString());
                    }
                    dataReader.NextResult();

                    while (dataReader.Read())
                    {
                        // Instantiate the candidate test session collection.
                        if (candidateInterviewSessions == null)
                            candidateInterviewSessions = new List<CandidateInterviewSessionDetail>();
                        // Create a new candidate test session object. 
                        CandidateInterviewSessionDetail candidateInterviewSession = new CandidateInterviewSessionDetail();
                        candidateInterviewSession.CandidateTestSessionID = dataReader["CAND_SESSION_KEY"].ToString();
                        candidateInterviewSession.CandidateAssessorCount = Convert.ToInt32(dataReader["CAND_ASSESSOR_COUNT"].ToString());
                        candidateInterviewSession.Status = dataReader["STATUS"].ToString();
                        candidateInterviewSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());
                        candidateInterviewSession.RetakeRequest = dataReader["RETAKE_REQUEST"].ToString();
                        candidateInterviewSession.CancelReason = dataReader["CANCEL_REASON"].ToString();
                        candidateInterviewSession.EmailRemainder = dataReader["DAILY_REMINDER"].ToString();

                        // If status is 'Not Scheduled', then don't assign the candidate informations
                        if (candidateInterviewSession.Status != "SESS_NSCHD")
                        {
                            candidateInterviewSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                            candidateInterviewSession.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                            candidateInterviewSession.CandidateFirstName = dataReader["CANDIDATE_FIRSTNAME"].ToString().Trim();
                            candidateInterviewSession.CandidateFullName = dataReader["CANDIDATE_FULLNAME"].ToString().Trim();
                            candidateInterviewSession.Email = dataReader["EMAIL_ID"].ToString();

                            if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                                candidateInterviewSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();

                            if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                                candidateInterviewSession.DateCompleted = Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                            if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                                candidateInterviewSession.ScheduledDate =
                                    Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                            candidateInterviewSession.ModifiedDate =
                                Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                        candidateInterviewSessions.Add(candidateInterviewSession);
                    }
                    if (interviewSession != null)
                    {
                        interviewSession.CandidateInterviewSessions = new List<CandidateInterviewSessionDetail>();
                        interviewSession.CandidateInterviewSessions = candidateInterviewSessions;
                    }
                    return interviewSession;
                }
                finally
                {
                    // Close the data reader, if it is open.
                    if (dataReader != null && !dataReader.IsClosed)
                    {
                        dataReader.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Method that will update the scheduled candidate details.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the schedule detail.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <remarks>
        /// While scheduling a candidate to the test, this method gets triggered to 
        /// store the values in Session_Candidate and Candidate_Session_Tracking tables.
        /// </remarks>
        public void ScheduleCandidate(TestScheduleDetail testScheduleDetail, int modifiedBy, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand updateScheduleCandCommand = HCMDatabase.
                GetStoredProcCommand("SPSCHEDULE_CANDIDATE_INTERVIEW");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, testScheduleDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@CANDIDATE_ID", DbType.Int32, Convert.ToInt32(testScheduleDetail.CandidateID));
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@ATTEMPT_ID", DbType.Int16, Convert.ToInt32(testScheduleDetail.AttemptID));
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@EMAILID", DbType.String, testScheduleDetail.EmailId);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@SCHEDULED_DATE", DbType.DateTime, testScheduleDetail.ExpiryDate);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@DEFAULT_ASSESSOR", DbType.String, testScheduleDetail.DefaultAssessor);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);

            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@EMAIL_REMAINDER", DbType.String, testScheduleDetail.EmailReminder);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateScheduleCandCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Represents the method to check whether the same candidate
        /// has been assigned for the test session ID
        /// </summary>
        /// <param name="testSessionID">A
        /// <see cref="string"/>that holds the test session ID
        /// </param>
        /// <param name="candidateID">
        /// A<see cref="string"/>that holds the candidate ID
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds  candidate already assigned scheduler name
        /// </returns>
        public string CheckCandidateAlreadyAssigned(string interviewSessionID, string candidateID)
        {
            try
            {

                // Create a stored procedure command object.
                DbCommand CheckcandidateCommand = HCMDatabase.
                    GetStoredProcCommand("SPCHECK_CANDIDATE_EXIST_INTERVIEWSESSION");
                // Add input parameters.
                HCMDatabase.AddInParameter(CheckcandidateCommand,
                    "@INTERVIEW_SESSION_KEY", DbType.String, interviewSessionID);
                HCMDatabase.AddInParameter(CheckcandidateCommand,
                    "@CANDIDATE_ID", DbType.Int32, Convert.ToInt32(candidateID.Trim()));

                object schedulerName = null;
                string result = string.Empty;

                schedulerName = HCMDatabase.ExecuteScalar(CheckcandidateCommand);

                return schedulerName == null ? result : schedulerName.ToString();
            }
            finally
            {

            }
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestSessionDetail"/> that holds the candidate
        /// test session details.
        /// </returns>
        public CandidateInterviewSessionDetail GetCandidateInterviewSession
            (string candidateInterviewSessionID, int attemptID)
        {
            if (Utility.IsNullOrEmpty(candidateInterviewSessionID))
                throw new Exception("Candidate test session ID cannot be empty");

            CandidateInterviewSessionDetail session = null;
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_INTERVIEW_SESSION");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@CANDIDATE_SESSION_ID", DbType.String, candidateInterviewSessionID);

                HCMDatabase.AddInParameter(getCandidateSession,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                if (dataReader.Read())
                {
                    session = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_GENID"]))
                    {
                        session.GenID = Convert.ToInt32(dataReader["CANDIDATE_SESSION_GENID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        session.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                    {
                        session.InterviewTestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        session.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        session.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                    {
                        session.Email = dataReader["EMAIL_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        session.Status = dataReader["STATUS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_ATTEMPT"]))
                    {
                        session.NoOfAttempt = Convert.ToInt16(dataReader["NO_OF_ATTEMPT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        session.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                    {
                        session.InterviewTestID = dataReader["TEST_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        session.InterviewTestName = dataReader["TEST_NAME"].ToString().Trim();
                    }

                    session.AttemptID = attemptID;
                }

                return session;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID and attempt ID.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the candidate
        /// interview session details.
        /// </returns>
        public CandidateInterviewSessionDetail GetCandidateInterviewSessionDetail
            (string candidateTestSessionID, int attemptID)
        {
            if (Utility.IsNullOrEmpty(candidateTestSessionID))
                throw new Exception("Candidate test session ID cannot be empty");

            if (attemptID == 0)
                throw new Exception("Attempt ID cannot be zero");

            CandidateInterviewSessionDetail session = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_INTERVIEW_SESSION");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@CANDIDATE_SESSION_ID", DbType.String, candidateTestSessionID);

                HCMDatabase.AddInParameter(getCandidateSession,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                if (dataReader.Read())
                {
                    session = new CandidateInterviewSessionDetail();
                    session.AttemptID = attemptID;
                    session.CandidateTestSessionID = candidateTestSessionID;

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        session.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INFO_ID"]))
                    {
                        session.CandidateInformationID = Convert.ToInt32(dataReader["CANDIDATE_INFO_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        session.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                    {
                        session.Email = dataReader["EMAIL_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        session.InterviewTestName = dataReader["TEST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                    {
                        session.InterviewTestDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                    {
                        session.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        session.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                    {
                        session.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                    {
                        session.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                    {
                        session.DateCompleted =Convert.ToDateTime(dataReader["COMPLETED_DATE"]) ;
                    }
                }

                return session;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate interview session ID and attempt ID, that helps in 
        /// composing email.
        /// </summary>
        /// <param name="candidateInterviewSessionID">
        /// A <see cref="string"/> that holds the candidate interview session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the candidate
        /// interview session details.
        /// </returns>
        public CandidateInterviewSessionDetail GetCandidateInterviewSessionEmailDetail
            (string candidateInterviewSessionID, int attemptID)
        {
            if (Utility.IsNullOrEmpty(candidateInterviewSessionID))
                throw new Exception("Candidate interview session ID cannot be empty");

            CandidateInterviewSessionDetail session = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_INTERVIEW_SESSION_EMAIL_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@CANDIDATE_INTERVIEW_SESSION_ID", DbType.String, candidateInterviewSessionID);

                HCMDatabase.AddInParameter(getCandidateSession,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                if (dataReader.Read())
                {
                    session = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_ID"]))
                    {
                        session.GenID = Convert.ToInt32(dataReader["CAND_INTERVIEW_SESSION_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                    {
                        session.CandidateTestSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                    {
                        session.InterviewTestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        session.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["AUTH_CODE"]))
                    {
                        session.SecurityCode = dataReader["AUTH_CODE"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        session.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                    {
                        session.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_OWNER_EMAIL"]))
                    {
                        session.CandidateOwnerEmail = dataReader["CANDIDATE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_EMAIL"]))
                    {
                        session.SchedulerEmail = dataReader["SCHEDULER_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_FIRST_NAME"]))
                    {
                        session.SchedulerFirstName = dataReader["SCHEDULER_FIRST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_COMPANY"]))
                    {
                        session.SchedulerCompany = dataReader["SCHEDULER_COMPANY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_LAST_NAME"]))
                    {
                        session.SchedulerLastName = dataReader["SCHEDULER_LAST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_EMAIL"]))
                    {
                        session.PositionProfileOwnerEmail = dataReader["POSITION_PROFILE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_EMAILS"]))
                    {
                        session.AssessorEmails = dataReader["ASSESSOR_EMAILS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        session.Status = dataReader["STATUS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_ATTEMPT"]))
                    {
                        session.NoOfAttempt = Convert.ToInt16(dataReader["NO_OF_ATTEMPT"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["NAVIGATE_KEY"]))
                    {
                        session.NavigateKey = dataReader["NAVIGATE_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        session.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                    {
                        session.InterviewTestID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                    {
                        session.InterviewTestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    }

                    session.AttemptID = attemptID;
                }

                return session;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will call to load the candidate details 
        /// who scheduled for the tests.
        /// </summary>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/>that contains the current page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the candidate Information Like Name...
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column to be sorted.
        /// </param>
        /// <param name="orderDirection">
        /// A <see cref="SortType"/> that contains the sort direction either Asc/Desc.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestScheduleDetail"/> that contains candidate details.
        /// </returns>
        public List<TestScheduleDetail> GetInterviewScheduler
            (int? user, int pageNumber, int pageSize, out int totalRecords,
            TestScheduleSearchCriteria scheduleSearchCriteria, string orderBy, SortType orderDirection)
        {
            totalRecords = 0;
            IDataReader dataReader = null;
            List<TestScheduleDetail> testScheduleDetails = null;

            try
            {
                DbCommand getSchedulerCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_SCHEDULED_DETAILS");
                // Add input parameters
                HCMDatabase.AddInParameter(getSchedulerCommand, "@CANDIDATE_NAME", DbType.String, scheduleSearchCriteria.CandidateName);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@CANDIDATE_EMAIL", DbType.String, scheduleSearchCriteria.EMail);
                //HCMDatabase.AddInParameter(getSchedulerCommand, "@SCHEDULER_NAME", DbType.String, scheduleSearchCriteria.TestScheduleAuthor);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@INTERVIEW_TEST_SESSION_ID", DbType.String, scheduleSearchCriteria.TestSessionID);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@INTERVIEW_TEST_NAME", DbType.String, scheduleSearchCriteria.TestName);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@TEST_COMPLETED_STATUS", DbType.Int32, scheduleSearchCriteria.TestCompletedStatus);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@ORDERBYDIRECTION", DbType.String,
                    orderDirection == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING
                    : Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@TOTAL", DbType.String, 0);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@SCHEDULED_BY", DbType.Int32, user == 0 ? null : user);
                dataReader = HCMDatabase.ExecuteReader(getSchedulerCommand);
                while (dataReader.Read())
                {
                    if (testScheduleDetails == null)
                        testScheduleDetails = new List<TestScheduleDetail>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        TestScheduleDetail testScheduleDetail = new TestScheduleDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_REQUEST_NUMBER"]))
                            testScheduleDetail.ClientRequestID =
                                dataReader["CLIENT_REQUEST_NUMBER"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                            testScheduleDetail.TestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_EXPIRY"]))
                            testScheduleDetail.SessionExpiryDate =
                                Convert.ToDateTime(dataReader["SESSION_EXPIRY"]);

                        if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                            testScheduleDetail.CandidateTestSessionID =
                                dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            testScheduleDetail.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        {
                            string candidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                            string[] candidateFirstName = candidateName.Split(new char[] { '~' });
                            testScheduleDetail.CandidateName = candidateFirstName[0];
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FULLNAME"]))
                            testScheduleDetail.CandidateFullName = dataReader["CANDIDATE_FULLNAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                            testScheduleDetail.EmailId = dataReader["CANDIDATE_EMAIL"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATED_ON"]))
                            testScheduleDetail.CreatedDate =
                                Convert.ToDateTime(dataReader["SESSION_CREATED_ON"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            string sessionAuthorName = dataReader["SCHEDULED_BY"].ToString().Trim();
                            string[] schedulerDetails = sessionAuthorName.Split(new char[] { '~' });
                            testScheduleDetail.SchedulerFirstName = schedulerDetails[0];
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_FULLNAME"]))
                            testScheduleDetail.SchedulerFullName = dataReader["SCHEDULER_FULLNAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            testScheduleDetail.ExpiryDate =
                                Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                            testScheduleDetail.TestStatus = dataReader["STATUS"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                            testScheduleDetail.CompletedDate = Convert.ToDateTime(dataReader["COMPLETED_DATE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                            testScheduleDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                            testScheduleDetail.TestID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();

                        testScheduleDetails.Add(testScheduleDetail);
                    }
                    else
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                }
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
            return testScheduleDetails;
        }

        /// <summary>
        /// Method that will return test session and candidates session details
        /// for the given test session id or candidate session id/s.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the test session id.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate session ids 
        /// separated by comma.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the test session detail 
        /// with the list of CandidateTestSessionDetail instances.
        /// </returns>
        public InterviewTestSessionDetail GetInterviewTestSessionDetail(string testSessionID, string candidateSessionIDs)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_INTERVIEW_SESSION_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_KEY", DbType.String, testSessionID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@CAND_SESSION_KEYS", DbType.String, candidateSessionIDs);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                InterviewTestSessionDetail testSession = null;
                List<CandidateTestSessionDetail> candidateTestSessions = null;
                while (dataReader.Read())
                {
                    // Instantiate the testSession instance
                    if (testSession == null)
                        testSession = new InterviewTestSessionDetail();

                    // Assign property values to the testSession object.
                    testSession.TestID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();

                    testSession.TestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                    testSession.TestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    //testSession.TimeLimit = Convert.ToInt32(dataReader["TIME_LIMIT"].ToString());
                    testSession.ExpiryDate = Convert.ToDateTime(dataReader["SESSION_EXPIRY"].ToString());
                    //testSession.IsRandomizeQuestionsOrdering = (dataReader["RANDOM_ORDER"].ToString() == "Y" ? true : false);
                    //testSession.IsCyberProctoringEnabled = (dataReader["CYBER_PROCTORING"].ToString() == "Y" ? true : false);
                    //testSession.IsDisplayResultsToCandidate = (dataReader["DISPLAY_RESULT"].ToString() == "Y" ? true : false);
                    testSession.Instructions = dataReader["INSTRUCTIONS"].ToString();
                    testSession.TestSessionDesc = dataReader["INTERVIEW_SESSION_DESC"].ToString();
                    testSession.TestSessionAuthor = dataReader["SESSION_AUTHOR_NAME"].ToString().Trim();
                    testSession.TestSessionAuthorEmail = dataReader["SESSION_AUTHOR_EMAIL"].ToString().Trim();
                    //testSession.RecommendedTimeLimit = Convert.ToInt32(dataReader["RECOMMENDED_TIME"].ToString());
                    //testSession.TotalCredit = Convert.ToDecimal(dataReader["CREDIT"].ToString());
                    testSession.NumberOfCandidateSessions = Convert.ToInt32(dataReader["SESSION_COUNT"].ToString());
                    testSession.AllowPauseInterview = Convert.ToChar(dataReader["ALLOW_PAUSE_INTERVIEW"]);
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        testSession.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        testSession.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        testSession.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        testSession.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString());
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the candidate test session collection.
                    if (candidateTestSessions == null)
                        candidateTestSessions = new List<CandidateTestSessionDetail>();

                    // Create a new candidate test session object. 
                    CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();
                    candidateTestSession.CandidateTestSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();
                    candidateTestSession.Status = dataReader["STATUS"].ToString();
                    candidateTestSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());
                    candidateTestSession.RetakeRequest = dataReader["RETAKE_REQUEST"].ToString();
                    candidateTestSession.CancelReason = dataReader["CANCEL_REASON"].ToString();

                    // If status is 'Not Scheduled', then don't assign the candidate informations
                    if (candidateTestSession.Status != "SESS_NSCHD")
                    {
                        candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                        candidateTestSession.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candidateTestSession.CandidateFirstName = dataReader["CANDIDATE_FIRSTNAME"].ToString().Trim();
                        candidateTestSession.Email = dataReader["EMAIL_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                            candidateTestSession.DateCompleted = Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candidateTestSession.ScheduledDate =
                                Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        candidateTestSession.ModifiedDate =
                            Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                    candidateTestSessions.Add(candidateTestSession);

                }
                testSession.CandidateTestSessions = new List<CandidateTestSessionDetail>();
                testSession.CandidateTestSessions = candidateTestSessions;

                return testSession;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will retrieves the interview session and associated 
        /// candidate sessions for the given parameters.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the interview session ID.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate session IDs 
        /// separated by comma.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewSessionDetail"/> that contains the interview 
        /// session and associated candidate sessions.
        /// </returns>
        public InterviewSessionDetail GetInterviewTestSessionDetailScheduler
            (string testSessionID, string candidateSessionIDs)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_INTERVIEW_SESSION_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_KEY", DbType.String, testSessionID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@CAND_SESSION_KEYS", DbType.String, candidateSessionIDs);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                InterviewSessionDetail testSession = null;

                while (dataReader.Read())
                {
                    // Instantiate the testSession instance
                    if (testSession == null)
                        testSession = new InterviewSessionDetail();

                    // Assign property values to the testSession object.
                    testSession.InterviewTestID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();

                    testSession.InterviewTestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                    testSession.InterviewTestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    //testSession.TimeLimit = Convert.ToInt32(dataReader["TIME_LIMIT"].ToString());
                    testSession.ExpiryDate = Convert.ToDateTime(dataReader["SESSION_EXPIRY"].ToString());
                    //testSession.IsRandomizeQuestionsOrdering = (dataReader["RANDOM_ORDER"].ToString() == "Y" ? true : false);
                    //testSession.IsCyberProctoringEnabled = (dataReader["CYBER_PROCTORING"].ToString() == "Y" ? true : false);
                    //testSession.IsDisplayResultsToCandidate = (dataReader["DISPLAY_RESULT"].ToString() == "Y" ? true : false);
                    testSession.Instructions = dataReader["INSTRUCTIONS"].ToString();
                    testSession.InterviewSessionDesc = dataReader["INTERVIEW_SESSION_DESC"].ToString();
                    testSession.InteriewSessionAuthor = dataReader["SESSION_AUTHOR_NAME"].ToString().Trim();
                    testSession.InterviewSessionAuthorEmail = dataReader["SESSION_AUTHOR_EMAIL"].ToString().Trim();
                    //testSession.RecommendedTimeLimit = Convert.ToInt32(dataReader["RECOMMENDED_TIME"].ToString());
                    //testSession.TotalCredit = Convert.ToDecimal(dataReader["CREDIT"].ToString());
                    testSession.NumberOfCandidateSessions = Convert.ToInt32(dataReader["SESSION_COUNT"].ToString());
                    testSession.AllowPauseInterview = Convert.ToChar(dataReader["ALLOW_PAUSE_INTERVIEW"]);
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        testSession.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        testSession.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        testSession.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        testSession.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString());
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the candidate session collection.
                    if (testSession.CandidateInterviewSessions == null)
                        testSession.CandidateInterviewSessions = new List<CandidateInterviewSessionDetail>();

                    // Create a new candidate test session object. 
                    CandidateInterviewSessionDetail candidateTestSession = new CandidateInterviewSessionDetail();
                    candidateTestSession.CandidateTestSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();
                    candidateTestSession.Status = dataReader["STATUS"].ToString();
                    candidateTestSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());
                    candidateTestSession.RetakeRequest = dataReader["RETAKE_REQUEST"].ToString();
                    candidateTestSession.CancelReason = dataReader["CANCEL_REASON"].ToString();

                    // If status is 'Not Scheduled', then don't assign the candidate informations
                    if (candidateTestSession.Status != "SESS_NSCHD")
                    {
                        candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                        candidateTestSession.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candidateTestSession.CandidateFirstName = dataReader["CANDIDATE_FIRSTNAME"].ToString().Trim();
                        candidateTestSession.Email = dataReader["EMAIL_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                            candidateTestSession.DateCompleted = Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candidateTestSession.ScheduledDate =
                                Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        candidateTestSession.ModifiedDate =
                            Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    testSession.CandidateInterviewSessions.Add(candidateTestSession);

                }
                return testSession;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of interview reminder sender list 
        /// for the given date/time and relative time differences. Mails needs 
        /// to be sent for this list.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<InterviewReminderDetail> GetInterviewReminderSenderList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getSenderList = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_REMINDER_SENDER_LIST");

                // Add input parameters.
                HCMDatabase.AddInParameter(getSenderList,
                    "@CURRENT_DATE_TIME", DbType.String, currentDateTime);
                HCMDatabase.AddInParameter(getSenderList,
                    "@RELATIVE_TIME_DIFFERENCE_SECONDS", DbType.Int32, relativeTimeDifference);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getSenderList);

                List<InterviewReminderDetail> senderList = null;

                while (dataReader.Read())
                {
                    if (senderList == null)
                        senderList = new List<InterviewReminderDetail>();

                    InterviewReminderDetail senderDetail = new InterviewReminderDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        senderDetail.CandidateSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        senderDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        senderDetail.EmailID = dataReader["EMAIL_ID"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVAL_ID"]))
                        senderDetail.IntervalID = dataReader["INTERVAL_ID"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVAL_DESCRIPTION"]))
                        senderDetail.IntervalDescription = dataReader["INTERVAL_DESCRIPTION"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        senderDetail.InterviewID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        senderDetail.InterviewName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["EXPECTED_DATE"]))
                        senderDetail.ExpectedDate = Convert.ToDateTime(dataReader["EXPECTED_DATE"].ToString().Trim());
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        senderDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        senderDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    // Add the detail to the list.
                    senderList.Add(senderDetail);
                }
                return senderList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Method that will update the reminder sent status for the given 
        /// candidate, attempt number and interval ID.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="intervalID">
        /// A <see cref="string"/> that holds the interval ID.
        /// </param>
        /// <param name="reminderSent">
        /// A <see cref="bool"/> that holds the reminder sent status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateInterviewReminderStatus(string candidateSessionID,
            int attemptID, string intervalID, bool reminderSent, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_INTERVIEW_REMINDER_SENT_STATUS");
            //Add input parameters.
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@ATTEMPT_ID", DbType.Int32, attemptID);
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@INTERVAL_ID", DbType.String, intervalID);
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@REMINDER_SENT", DbType.String, reminderSent == true ? "Y" : "N");
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@USER_ID", DbType.Int32, userID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateReminderCommand);
        }

        /// <summary>
        /// Method that will update the scheduled information to the respective tables.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the test schedule detail.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>
        public void InterviewScheduleCandidate(TestScheduleDetail testScheduleDetail,
            int modifiedBy, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand updateScheduleCandCommand = HCMDatabase.
                GetStoredProcCommand("SP_INTERVIEW_SCHEDULE_CANDIDATE");
            // Add input parameters.
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, testScheduleDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@CANDIDATE_ID", DbType.Int32, Convert.ToInt32(testScheduleDetail.CandidateID));
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@ATTEMPT_ID", DbType.Int16, Convert.ToInt32(testScheduleDetail.AttemptID));
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@EMAILID", DbType.String, testScheduleDetail.EmailId);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@SCHEDULED_DATE", DbType.DateTime, testScheduleDetail.ExpiryDate);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateScheduleCandCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will update the candidate session status if the candidate is unscheduled an interview.
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate session detail.
        /// </param>
        /// <param name="candidateAttemptStatus">
        /// A <see cref="string"/> that contains the candidate attempt status.
        /// </param>
        /// <param name="candidateSessionStatus">
        /// A <see cref="string"/> that contains the candidate session status.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        public void UpdateSessionStatus(CandidateInterviewSessionDetail candidateInterviewSessionDetail,
            string candidateAttemptStatus, string candidateSessionStatus, int modifiedBy)
        {
            // Create a stored procedure command object.
            DbCommand command = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_UNSCHEDULE_INTERVIEW_BY_CANDIDATE_SESSION_KEY");

            // Add input parameters.
            HCMDatabase.AddInParameter(command,
                "@CAND_INTERVIEW_SESSION_ID", DbType.String, candidateInterviewSessionDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(command,
                "@ATTEMPT_ID", DbType.Int16, candidateInterviewSessionDetail.AttemptID);
            HCMDatabase.AddInParameter(command,
                "@SESSION_STATUS", DbType.String, candidateAttemptStatus);
            HCMDatabase.AddInParameter(command,
                "@STATUS", DbType.String, candidateSessionStatus);
            HCMDatabase.AddInParameter(command,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(command);
        }

        /// <summary>
        /// Method that will update the test status for the given candidate session id
        /// and attempt id.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attemptId">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <param name="sessionTrackingStatus">
        /// A <see cref="string"/> that contains the session tracking status.
        /// </param>
        /// <param name="sessionCandidateStatus">
        /// A <see cref="string"/> that contains the candidate session status.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        public void UpdateSessionStatus(string candidateSessionKey, int attemptId,
            string sessionTrackingStatus, string sessionCandidateStatus, int modifiedBy)
        {
            // Create a stored procedure command object.
            DbCommand command = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_INTERVIEW_SESSION_STATUS");

            // Add input parameters.
            HCMDatabase.AddInParameter(command,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(command,
                "@ATTEMPT_ID", DbType.Int16, attemptId);
            HCMDatabase.AddInParameter(command,
                "@SESSION_TRACKING_STATUS", DbType.String, sessionTrackingStatus);
            HCMDatabase.AddInParameter(command,
                "@SESSION_CANDIDATE_STATUS", DbType.String, sessionCandidateStatus);
            HCMDatabase.AddInParameter(command,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(command);
        }

        /// <summary>
        /// Inserts assessor,candidate sessession key,attempt id and skill id into 
        /// [INTERVIEW_CANDIDATE_ASSESSMENT_SKILL_TRACKING] 
        /// </summary>
        /// <param name="candidateSessionKey">candidateSessionKey</param>
        /// <param name="assessorId">assessorId</param>
        /// <param name="assessorId">attempId</param>
        /// <param name="skillIds">skillIds</param>
        /// <param name="userID">userID</param>
        /// <param name="transaction"></param>
        public void InsertInterviewSessionAssessorSkillForCandidate(string candidateSessionKey, int assessorId, 
            int attempId, string skillIds, int userID, IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertAssessorSkill
                = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_CANDIDATE_ASSESSMENT_SKILL_TRACKING");
            // Add input parameter
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@ASSESSOR_ID", DbType.Int32, assessorId);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@ATTEMPT_ID", DbType.Int32, attempId);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@SKILL_ID", DbType.String, skillIds);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@CREATED_BY", DbType.Int32, userID);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAssessorSkill, transaction as DbTransaction);
        }


        /// <summary>
        /// Update assessor,candidate sessession key,attempt id and skill id into 
        /// [INTERVIEW_CANDIDATE_ASSESSMENT_TRACKING] ,[INTERVIEW_CANDIDATE_ASSESSMENT_SKILL_TRACKING]
        /// </summary>
        /// <param name="candidateSessionKey">candidateSessionKey</param>
        /// <param name="assessorId">assessorId</param>
        /// <param name="assessorId">attempId</param>
        /// <param name="skillIds">skillIds</param>
        /// <param name="userID">userID</param>
        /// <param name="transaction"></param>
        public void UpdateInterviewSessionAssessorAndSkill(string candidateSessionKey, int assessorId,
            int attempId, string skillIds, int userID, IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertAssessorSkill
                = HCMDatabase.GetStoredProcCommand("SPUPDATE_INTERVIEW_CANDIDATE_ASSESSOR_AND_SKILL");
            // Add input parameter
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@ASSESSOR_ID", DbType.Int32, assessorId);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@ATTEMPT_ID", DbType.Int32, attempId);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@SKILL_ID", DbType.String, skillIds);
            HCMDatabase.AddInParameter(insertAssessorSkill,
                "@CREATED_BY", DbType.Int32, userID);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAssessorSkill, transaction as DbTransaction);
        }


        /// <summary>
        /// Method to delete the existing assessor tracking details against candidatekey,attempt and assessor id
        /// </summary>
        /// <param name="candidateSessionKey"></param>
        /// <param name="attemptId"></param>
        public void DeleteInterviewCandidateSessionAssessorSkill(string candidateSessionKey,
            string assessorIds, int attemptId, IDbTransaction transaction)
        {
            DbCommand assessmentSkillTrackingCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_INTERVIEW_CANDIDATE_ASSESSOR_AND_SKILL");
            HCMDatabase.AddInParameter(assessmentSkillTrackingCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(assessmentSkillTrackingCommand,
                "@ASSESSOR_IDS", DbType.String, assessorIds);
            HCMDatabase.AddInParameter(assessmentSkillTrackingCommand,
                "@ATTEMPT_ID", DbType.Int32, attemptId);
            HCMDatabase.ExecuteNonQuery(assessmentSkillTrackingCommand, transaction as DbTransaction);
        }
    }
}