﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientDLManager.cs
// File that represents the data layer for the Client Management module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Data.Common;
using System.Drawing.Imaging;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    public class ClientDLManager : DatabaseConnectionManager
    {
        #region INSERT
        /// <summary>
        /// Inserts the certificate.
        /// </summary>
        /// <param name="clientInformation">The client information.</param>
        public void InsertClient(ClientInformation clientInformation, out int clientID)
        {
            clientID = 0;
            // Create a stored procedure command object
            DbCommand insertClientCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CLIENT");

            // Add input parameters
            HCMDatabase.AddInParameter(insertClientCommand,
                "@TENANT_ID", DbType.Int32, clientInformation.TenantID);

            HCMDatabase.AddInParameter(insertClientCommand,
                "@CLIENT_NAME", DbType.String, clientInformation.ClientName);

            HCMDatabase.AddInParameter(insertClientCommand,
                "@STREET_ADDRESS", DbType.String, clientInformation.ContactInformation.StreetAddress);

            HCMDatabase.AddInParameter(insertClientCommand,
                "@CITY_NAME", DbType.String, clientInformation.ContactInformation.City);
            HCMDatabase.AddInParameter(insertClientCommand,
                "@STATE_NAME", DbType.String, clientInformation.ContactInformation.State);
            HCMDatabase.AddInParameter(insertClientCommand,
                "@ZIPCODE", DbType.String, clientInformation.ContactInformation.PostalCode);
            HCMDatabase.AddInParameter(insertClientCommand,
                "@COUNTRY", DbType.String, clientInformation.ContactInformation.Country);
            HCMDatabase.AddInParameter(insertClientCommand,
                "@FEIN_NO", DbType.String, clientInformation.FeinNo);

            HCMDatabase.AddInParameter(insertClientCommand,
                "@PHONE_NUMBER", DbType.String, clientInformation.ContactInformation.Phone.Mobile);

            HCMDatabase.AddInParameter(insertClientCommand,
                "@FAX_NO", DbType.String, clientInformation.FaxNumber);

            HCMDatabase.AddInParameter(insertClientCommand,
            "@EMAIL_ID", DbType.String, clientInformation.ContactInformation.EmailAddress);

            HCMDatabase.AddInParameter(insertClientCommand,
            "@WEBSITE_URL", DbType.String, clientInformation.ContactInformation.WebSiteAddress.Personal);

            HCMDatabase.AddInParameter(insertClientCommand,
               "@ADDITIONAL_INFO", DbType.String, clientInformation.AdditionalInfo);

            HCMDatabase.AddInParameter(insertClientCommand,
               "@IS_DELETED", DbType.Boolean, clientInformation.IsDeleted);

            HCMDatabase.AddInParameter(insertClientCommand,
               "@USER_ID", DbType.Int32, clientInformation.CreatedBy);
            HCMDatabase.AddOutParameter(insertClientCommand,
               "@CLIENT_ID", DbType.Int32, clientID);
            // Execute command
            clientID = int.Parse(HCMDatabase.ExecuteScalar(insertClientCommand).ToString());
        }
        /// <summary>
        /// Inserts the deparment.
        /// </summary>
        /// <param name="clientInformation">The client information.</param>
        public void InsertDepartment(Department departmentInfo, out int departmentID)
        {
            departmentID = 0;
            // Create a stored procedure command object
            DbCommand insertDepartmentCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_DEPARTMENT");

            // Add input parameters

            HCMDatabase.AddInParameter(insertDepartmentCommand,
               "@CLIENT_ID", DbType.String, departmentInfo.ClientID);
            HCMDatabase.AddInParameter(insertDepartmentCommand,
                "@DEPARTMENT_NAME", DbType.String, departmentInfo.DepartmentName);

            HCMDatabase.AddInParameter(insertDepartmentCommand,
                "@STREET_ADDRESS", DbType.String, departmentInfo.ContactInformation.StreetAddress);

            HCMDatabase.AddInParameter(insertDepartmentCommand,
                "@CITY_NAME", DbType.String, departmentInfo.ContactInformation.City);
            HCMDatabase.AddInParameter(insertDepartmentCommand,
                "@STATE_NAME", DbType.String, departmentInfo.ContactInformation.State);
            HCMDatabase.AddInParameter(insertDepartmentCommand,
                "@ZIPCODE", DbType.String, departmentInfo.ContactInformation.PostalCode);
            HCMDatabase.AddInParameter(insertDepartmentCommand,
                "@COUNTRY", DbType.String, departmentInfo.ContactInformation.Country);

            HCMDatabase.AddInParameter(insertDepartmentCommand,
                "@PHONE_NUMBER", DbType.String, departmentInfo.ContactInformation.Phone.Mobile);

            HCMDatabase.AddInParameter(insertDepartmentCommand,
                "@FAX_NO", DbType.String, departmentInfo.FaxNumber);

            HCMDatabase.AddInParameter(insertDepartmentCommand,
            "@EMAIL_ID", DbType.String, departmentInfo.ContactInformation.EmailAddress);

            HCMDatabase.AddInParameter(insertDepartmentCommand,
               "@DEPARTMENT_DESCRIPTION ", DbType.String, departmentInfo.DepartmentDescription);

            HCMDatabase.AddInParameter(insertDepartmentCommand,
               "@ADDITIONAL_INFO", DbType.String, departmentInfo.AdditionalInfo);

            HCMDatabase.AddInParameter(insertDepartmentCommand,
               "@USER_ID", DbType.Int32, departmentInfo.CreatedBy);

            HCMDatabase.AddOutParameter(insertDepartmentCommand,
              "@DEPARTMENT_ID", DbType.Int32, departmentID);

            // Execute command
            departmentID = int.Parse(HCMDatabase.ExecuteScalar(insertDepartmentCommand).ToString());
        }


        public void InsertContact(ClientContactInformation clientContactInformation, out int contactID)
        {
            contactID = 0;
            // Create a stored procedure command object
            DbCommand insertContactCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CONTACT");

            // Add input parameters

            HCMDatabase.AddInParameter(insertContactCommand,
               "@TENANT_ID", DbType.Int32, clientContactInformation.TenantID);
            HCMDatabase.AddInParameter(insertContactCommand,
               "@FIRST_NAME", DbType.String, clientContactInformation.FirstName);
            HCMDatabase.AddInParameter(insertContactCommand,
                "@MIDDLE_NAME", DbType.String, clientContactInformation.MiddleName);
            HCMDatabase.AddInParameter(insertContactCommand,
                "@LAST_NAME", DbType.String, clientContactInformation.LastName);
            HCMDatabase.AddInParameter(insertContactCommand,
                "@STREET_ADDRESS", DbType.String, clientContactInformation.ContactInformation.StreetAddress);

            HCMDatabase.AddInParameter(insertContactCommand,
                "@CITY_NAME", DbType.String, clientContactInformation.ContactInformation.City);
            HCMDatabase.AddInParameter(insertContactCommand,
                "@STATE_NAME", DbType.String, clientContactInformation.ContactInformation.State);
            HCMDatabase.AddInParameter(insertContactCommand,
                "@ZIPCODE", DbType.String, clientContactInformation.ContactInformation.PostalCode);
            HCMDatabase.AddInParameter(insertContactCommand,
                "@COUNTRY", DbType.String, clientContactInformation.ContactInformation.Country);

            HCMDatabase.AddInParameter(insertContactCommand,
                "@PHONE_NUMBER", DbType.String, clientContactInformation.ContactInformation.Phone.Mobile);

            HCMDatabase.AddInParameter(insertContactCommand,
                "@FAX_NO", DbType.String, clientContactInformation.FaxNo);

            HCMDatabase.AddInParameter(insertContactCommand,
            "@EMAIL_ID", DbType.String, clientContactInformation.ContactInformation.EmailAddress);

            HCMDatabase.AddInParameter(insertContactCommand,
              "@ADDITIONAL_INFO", DbType.String, clientContactInformation.AdditionalInfo);
            HCMDatabase.AddInParameter(insertContactCommand,
               "@USER_ID", DbType.Int32, clientContactInformation.CreatedBy);

            HCMDatabase.AddOutParameter(insertContactCommand,
              "@CONTACT_ID", DbType.Int32, contactID);

            // Execute command
            contactID = int.Parse(HCMDatabase.ExecuteScalar(insertContactCommand).ToString());

        }

        public void InsertDepartmentContact(int tenantID, int contactID, string departmentIDs, int userID)
        {
            // Create a stored procedure command object
            DbCommand insertDepartmentContactCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_DEPARTMENT_CONTACT");
            // Add input parameters
            HCMDatabase.AddInParameter(insertDepartmentContactCommand,
               "@TENANT_ID", DbType.Int32, tenantID);
            HCMDatabase.AddInParameter(insertDepartmentContactCommand,
               "@CONTACT_ID", DbType.Int32, contactID);
            HCMDatabase.AddInParameter(insertDepartmentContactCommand,
                "@DEPARTMENT_IDs", DbType.String, departmentIDs);
            HCMDatabase.AddInParameter(insertDepartmentContactCommand,
               "@USER_ID", DbType.Int32, userID);

            // Execute command
            HCMDatabase.ExecuteNonQuery(insertDepartmentContactCommand);

        }
        public void InsertClientContact(int clientID, int contactID, int userID)
        {
            // Create a stored procedure command object
            DbCommand insertClientContactCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CLIENT_CONTACTS");
            // Add input parameters
            HCMDatabase.AddInParameter(insertClientContactCommand,
               "@CLIENT_ID", DbType.Int32, clientID);
            HCMDatabase.AddInParameter(insertClientContactCommand,
               "@CONTACT_ID", DbType.Int32, contactID);
            HCMDatabase.AddInParameter(insertClientContactCommand,
               "@USER_ID", DbType.Int32, userID);

            // Execute command
            HCMDatabase.ExecuteNonQuery(insertClientContactCommand);

        }


        #endregion INSERT

        #region GET
        /// <summary>
        /// Gets the client information by passing client id.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns></returns>
        public ClientInformation GetClientInformationByClientId(int clientId)
        {
            IDataReader dataReader = null;
            try
            {

                ClientInformation client = null;
                DbCommand getClientCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_DETAILS_CLIENTID");

                // Add parameters.
                HCMDatabase.AddInParameter(getClientCommand, "@CLIENT_ID", DbType.Int32, clientId);

                dataReader = HCMDatabase.ExecuteReader(getClientCommand);

                if (dataReader.Read())
                {
                    client = new ClientInformation();
                    ContactInformation contInfo = new ContactInformation();
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]) && dataReader["CLIENT_ID"].ToString().Trim().Length > 0)
                    {
                        client.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                    {
                        client.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STREET_ADDRESS"]))
                    {
                        contInfo.StreetAddress = dataReader["STREET_ADDRESS"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CITY_NAME"]))
                    {
                        contInfo.City = dataReader["CITY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STATE_NAME"]))
                    {
                        contInfo.State = dataReader["STATE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ZIPCODE"]))
                    {
                        contInfo.PostalCode = dataReader["ZIPCODE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COUNTRY"]))
                    {
                        contInfo.Country = dataReader["COUNTRY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                    {
                        contInfo.EmailAddress = dataReader["EMAIL_ID"].ToString().Trim();
                    }
                    WebSite website = new WebSite();
                    if (!Utility.IsNullOrEmpty(dataReader["WEBSITE_URL"]))
                        website.Personal = dataReader["WEBSITE_URL"].ToString().Trim();
                    else
                        website.Personal = string.Empty;
                    contInfo.WebSiteAddress = website;

                    PhoneNumber phone = new PhoneNumber();
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE_NUMBER"]))
                        phone.Mobile = dataReader["PHONE_NUMBER"].ToString().Trim();
                    else
                        phone.Mobile = string.Empty;

                    contInfo.Phone = phone;

                    if (!Utility.IsNullOrEmpty(dataReader["FAX_NO"]))
                    {
                        client.FaxNumber = dataReader["FAX_NO"].ToString().Trim();
                    }

                    client.ContactInformation = contInfo;
                    if (!Utility.IsNullOrEmpty(dataReader["FEIN_NO"]))
                    {
                        client.FeinNo = dataReader["FEIN_NO"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_DEPARTMENTS"]))
                    {
                        client.NoOfDepartments = Convert.ToInt32(dataReader["NO_OF_DEPARTMENTS"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ADDITIONAL_INFO"]))
                    {
                        client.AdditionalInfo = dataReader["ADDITIONAL_INFO"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_CONTACTS"]))
                    {
                        client.NoOfContacts = Convert.ToInt32(dataReader["NO_OF_CONTACTS"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_POSITION_PROFILES"]))
                    {
                        client.NoOfContacts = Convert.ToInt32(dataReader["NO_OF_POSITION_PROFILES"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                    {
                        client.CreatedByName = dataReader["CREATED_BY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_DEPARTMENTS"]))
                    {
                        client.ClientDepartments = dataReader["CLIENT_DEPARTMENTS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_CONTACTS"]))
                    {
                        client.ClientContacts = dataReader["CLIENT_CONTACTS"].ToString().Trim();
                    }

                    client.Departments = GetDepartments(clientId);
                }
                return client;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the client information.
        /// </summary>
        /// <param name="clientSearchCriteria">The client search criteria.</param>
        /// <returns></returns>
        public List<ClientInformation> GetClientInformation(ClientSearchCriteria clientSearchCriteria, int pageNumber,
            int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            DataSet clientsDataSet = new DataSet();

            totalRecords = 0;
            try
            {
                DbCommand getClientCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_DETAILS");

                // Add parameters.
                HCMDatabase.AddInParameter(getClientCommand,
                   "@CLIENT_ID", DbType.String, clientSearchCriteria.ClientID);
                HCMDatabase.AddInParameter(getClientCommand,
                  "@DEPARTMENT_ID", DbType.String, clientSearchCriteria.DepartmentID);
                HCMDatabase.AddInParameter(getClientCommand,
                  "@CONTACT_ID", DbType.String, clientSearchCriteria.ContactID);
                HCMDatabase.AddInParameter(getClientCommand,
                    "@CLIENT_NAME",
                    DbType.String, clientSearchCriteria.ClientName == null || clientSearchCriteria.ClientName.Trim().Length == 0 ? null : clientSearchCriteria.ClientName.Trim());
                HCMDatabase.AddInParameter(getClientCommand,
                    "@CONTACT_NAME",
                    DbType.String, clientSearchCriteria.ContactName == null || clientSearchCriteria.ContactName.Trim().Length == 0 ? null : clientSearchCriteria.ContactName.Trim());
                HCMDatabase.AddInParameter(getClientCommand,
                    "@LOCATION",
                    DbType.String, clientSearchCriteria.Location == null || clientSearchCriteria.Location.Trim().Length == 0 ? null : clientSearchCriteria.Location.Trim());

                HCMDatabase.AddInParameter(getClientCommand,
                    "@CREATED_BY",
                    DbType.Int32, clientSearchCriteria.CreatedBy);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@TENANT_ID",
                    DbType.Int32, clientSearchCriteria.TenantID);
                HCMDatabase.AddInParameter(getClientCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.LoadDataSet(getClientCommand,
                clientsDataSet, "clientInfo");
                List<ClientInformation> clientList = null;

                foreach (DataRow row in clientsDataSet.Tables[0].Rows)
                {
                    if (row["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (clientList == null)
                            clientList = new List<ClientInformation>();

                        ClientInformation client = new ClientInformation();
                        ContactInformation contInfo = new ContactInformation();
                        if (!Utility.IsNullOrEmpty(row["CLIENT_ID"]) && row["CLIENT_ID"].ToString().Trim().Length > 0)
                        {
                            client.ClientID = Convert.ToInt32(row["CLIENT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(row["CLIENT_NAME"]))
                        {
                            client.ClientName = row["CLIENT_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(row["STREET_ADDRESS"]))
                        {
                            contInfo.StreetAddress = row["STREET_ADDRESS"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["CITY_NAME"]))
                        {
                            contInfo.City = row["CITY_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["STATE_NAME"]))
                        {
                            contInfo.State = row["STATE_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["ZIPCODE"]))
                        {
                            contInfo.PostalCode = row["ZIPCODE"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["COUNTRY"]))
                        {
                            contInfo.Country = row["COUNTRY"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["EMAIL_ID"]))
                        {
                            contInfo.EmailAddress = row["EMAIL_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["WEBSITE_URL"]))
                        {
                            WebSite website = new WebSite();
                            website.Personal = row["WEBSITE_URL"].ToString().Trim();
                            contInfo.WebSiteAddress = website;
                        }
                        PhoneNumber phone = new PhoneNumber();
                        if (!Utility.IsNullOrEmpty(row["PHONE_NUMBER"]))
                            phone.Mobile = row["PHONE_NUMBER"].ToString().Trim();
                        else
                            phone.Mobile = string.Empty;

                        contInfo.Phone = phone;

                        if (!Utility.IsNullOrEmpty(row["FAX_NO"]))
                        {
                            client.FaxNumber = row["FAX_NO"].ToString().Trim();
                        }

                        client.ContactInformation = contInfo;
                        if (!Utility.IsNullOrEmpty(row["FEIN_NO"]))
                        {
                            client.FeinNo = row["FEIN_NO"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["NO_OF_DEPARTMENTS"]))
                        {
                            client.NoOfDepartments = Convert.ToInt32(row["NO_OF_DEPARTMENTS"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(row["ADDITIONAL_INFO"]))
                        {
                            client.AdditionalInfo = row["ADDITIONAL_INFO"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["NO_OF_CONTACTS"]))
                        {
                            client.NoOfContacts = Convert.ToInt32(row["NO_OF_CONTACTS"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(row["NO_OF_POSITION_PROFILES"]))
                        {
                            client.NoOfPositionProfile = Convert.ToInt32(row["NO_OF_POSITION_PROFILES"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(row["CREATED_BY"]))
                        {
                            client.CreatedByName = row["CREATED_BY"].ToString().Trim();
                        }

                        var query =
                        (from DataRow p in clientsDataSet.Tables[1].AsEnumerable()
                         where p.Field<int>("CLIENT_ID") == client.ClientID
                         select new Department
                         {
                             DepartmentID = p.Field<int>("DEPARTMENT_ID"),
                             DepartmentName = p.Field<string>("DEPARTMENT_NAME"),
                             DepartmentDescription = p.Field<string>("DEPARTMENT_DESCRIPTION")

                         });
                        client.Departments = query.ToList();
                        // Add to the list.
                        clientList.Add(client);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(row["TOTAL"]);
                    }
                }
                return clientList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (clientsDataSet != null)
                {
                    clientsDataSet.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets the client detail view.
        /// </summary>
        /// <param name="clientID">The client ID.</param>
        /// <returns></returns>
        //public ClientInformation GetClientDetailView(int clientID)
        //{
        //    IDataReader dataReader = null;
        //    try
        //    {
        //        DbCommand getClientCommand =
        //            HCMDatabase.GetStoredProcCommand("SPGET_CLIENT");

        //        HCMDatabase.AddInParameter(getClientCommand,
        //            "@CLIENT_ID",
        //            DbType.Int32, clientID);

        //        dataReader = HCMDatabase.ExecuteReader(getClientCommand);
        //        ClientInformation client = null;
        //        ContactInformation contactInfo = null;
        //        if (dataReader.Read())
        //        {
        //            client = new ClientInformation();
        //            contactInfo = new ContactInformation();
        //            if (dataReader["CLIENT_ID"] != null && dataReader["CLIENT_ID"].ToString().Trim().Length > 0)
        //            {
        //                client.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString().Trim());
        //            }


        //            if (dataReader["CLIENT_NAME"] != null)
        //            {
        //                client.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();
        //            }
        //            else
        //                client.ClientName = string.Empty;

        //            if (dataReader["STREET_ADDRESS"] != null)
        //            {
        //                contactInfo.StreetAddress = dataReader["STREET_ADDRESS"].ToString().Trim();
        //            }
        //            else
        //                contactInfo.StreetAddress = string.Empty;
        //            if (dataReader["CITY_NAME"] != null)
        //            {
        //                contactInfo.City = dataReader["CITY_NAME"].ToString().Trim();
        //            }
        //            else
        //                contactInfo.City = string.Empty;
        //            if (dataReader["STATE_NAME"] != null)
        //            {
        //                contactInfo.State = dataReader["STATE_NAME"].ToString().Trim();
        //            }
        //            else
        //                contactInfo.State = string.Empty;

        //            if (dataReader["ZIPCODE"] != null)
        //            {
        //                contactInfo.PostalCode = dataReader["ZIPCODE"].ToString().Trim();
        //            }
        //            else
        //                contactInfo.PostalCode = string.Empty;

        //            if (dataReader["COUNTRY"] != null)
        //            {
        //                contactInfo.Country = dataReader["COUNTRY"].ToString().Trim();
        //            }
        //            else
        //                contactInfo.Country = string.Empty;

        //            if (dataReader["FEIN_NO"] != null)
        //            {
        //                client.FeinNo = dataReader["FEIN_NO"].ToString().Trim();
        //            }
        //            else
        //                client.FeinNo = string.Empty;

        //            if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
        //            {
        //                contactInfo.EmailAddress = dataReader["EMAIL_ID"].ToString().Trim();
        //            }
        //            else
        //                contactInfo.EmailAddress = string.Empty;




        //            PhoneNumber phone = new PhoneNumber();
        //            if (!Utility.IsNullOrEmpty(dataReader["PHONE_NUMBER"]))
        //            {
        //                phone.Mobile = dataReader["PHONE_NUMBER"].ToString();
        //            }
        //            else
        //            {
        //                phone.Mobile = string.Empty;
        //            }
        //            contactInfo.Phone = phone;
        //            client.ContactInformation = contactInfo;
        //            WebSite website = new WebSite();
        //            if (dataReader["WEBSITE_URL"] != null)
        //            {
        //                website.Personal = dataReader["WEBSITE_URL"].ToString().Trim();
        //            }
        //            else
        //            {
        //                website.Personal = string.Empty;
        //            }
        //                contactInfo.WebSiteAddress = website;
        //                if (dataReader["ADDITIONAL_INFO"] != null)
        //                {
        //                    client.AdditionalInfo = dataReader["ADDITIONAL_INFO"].ToString().Trim();
        //                }
        //                else
        //                    client.AdditionalInfo = string.Empty;


        //        }

        //        dataReader.NextResult();

        //        while (dataReader.Read())
        //        {
        //            client.Departments = GetDepartments(clientID);
        //        }
        //        return client;
        //    }
        //    finally
        //    {
        //        if (dataReader != null && !dataReader.IsClosed)
        //        {
        //            dataReader.Close();
        //        }
        //    }
        //}
        /// <summary>
        /// Gets the question options.
        /// </summary>
        /// <param name="clientID">The client ID.</param>
        /// <returns></returns>
        public List<Department> GetDepartments(int clientID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getAnswerChoiseCommand = HCMDatabase.GetStoredProcCommand("SPGET_DEPARTMENT_CLIENT_ID");
                HCMDatabase.AddInParameter(getAnswerChoiseCommand, "@CLIENT_ID", DbType.Int32, clientID);
                dataReader = HCMDatabase.ExecuteReader(getAnswerChoiseCommand);

                List<Department> departmentList = null;
                ContactInformation contInfo = null;
                PhoneNumber phone = null;
                while (dataReader.Read())
                {
                    if (departmentList == null)
                        departmentList = new List<Department>();
                    Department department = new Department();
                    contInfo = new ContactInformation();

                    contInfo.StreetAddress = dataReader["STREET_ADDRESS"].ToString();
                    contInfo.City = dataReader["CITY_NAME"].ToString();
                    contInfo.State = dataReader["STATE_NAME"].ToString();
                    contInfo.Country = dataReader["COUNTRY"].ToString();
                    contInfo.PostalCode = dataReader["ZIPCODE"].ToString();
                    contInfo.EmailAddress = dataReader["EMAIL_ID"].ToString();

                    department.DepartmentDescription = dataReader["DEPARTMENT_DESCRIPTION"].ToString();
                    department.DepartmentName = dataReader["DEPARTMENT_NAME"].ToString();
                    department.DepartmentID = int.Parse(dataReader["DEPARTMENT_ID"].ToString());
                    department.FaxNumber = dataReader["FAX_NUMBERS"].ToString();
                    department.ClientID = clientID;
                    phone = new PhoneNumber();
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE_NUMBER"]))
                    {
                        phone.Mobile = dataReader["PHONE_NUMBER"].ToString();
                    }
                    else
                    {
                        phone.Mobile = string.Empty;
                    }
                    contInfo.Phone = phone;
                    department.ContactInformation = contInfo;
                    department.PrimaryAddress = dataReader["PRIMARY_ADDRESS"].ToString();
                    departmentList.Add(department);
                }

                return departmentList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public Department GetDepartment(int departmentID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_DEPARTMENTS");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@DEPARTMENT_ID", DbType.Int32, departmentID);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                Department department = null;
                if (dataReader.Read())
                {
                    department = new Department();
                    ContactInformation contInfo = new ContactInformation();
                    contInfo.StreetAddress = dataReader["STREET_ADDRESS"].ToString();
                    contInfo.City = dataReader["CITY_NAME"].ToString();
                    contInfo.State = dataReader["STATE_NAME"].ToString();
                    contInfo.Country = dataReader["COUNTRY"].ToString();
                    contInfo.PostalCode = dataReader["ZIPCODE"].ToString();
                    contInfo.EmailAddress = dataReader["EMAIL_ID"].ToString();

                    department.DepartmentDescription = dataReader["DEPARTMENT_DESCRIPTION"].ToString();
                    department.DepartmentName = dataReader["DEPARTMENT_NAME"].ToString();
                    department.DepartmentID = departmentID;
                    
                    if(!Utility.IsNullOrEmpty(dataReader["ADDITIONAL_INFO"]))
                        department.AdditionalInfo = dataReader["ADDITIONAL_INFO"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ADDITIONAL_INFO"]))
                        department.DepartmentAdditionalInfo = dataReader["CLIENT_ADDITIONAL_INFO"].ToString();

                    department.FaxNumber = dataReader["FAX_NUMBERS"].ToString();
                    department.ClientID = int.Parse(dataReader["CLIENT_ID"].ToString());
                    PhoneNumber phone = new PhoneNumber();
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE_NUMBER"]))
                    {
                        phone.Mobile = dataReader["PHONE_NUMBER"].ToString();
                    }
                    else
                    {
                        phone.Mobile = string.Empty;
                    }
                    contInfo.Phone = phone;
                    department.ContactInformation = contInfo;
                    department.PrimaryAddress = dataReader["PRIMARY_ADDRESS"].ToString();
                }

                return department;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the client information.
        /// </summary>
        /// <param name="clientSearchCriteria">The client search criteria.</param>
        /// <returns></returns>
        public List<Department> GetClientDepartmentInformation(ClientSearchCriteria clientSearchCriteria, int pageNumber,
               int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            DataSet clientsDataSet = new DataSet();

            totalRecords = 0;
            try
            {
                DbCommand getClientCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_DEPARTMENTS");

                // Add parameters.
                HCMDatabase.AddInParameter(getClientCommand,
                    "@CLIENT_NAME",
                    DbType.String, clientSearchCriteria.ClientName == null || clientSearchCriteria.ClientName.Trim().Length == 0 ? null : clientSearchCriteria.ClientName.Trim());
                HCMDatabase.AddInParameter(getClientCommand,
                    "@CONTACT_NAME",
                    DbType.String, clientSearchCriteria.ContactName == null || clientSearchCriteria.ContactName.Trim().Length == 0 ? null : clientSearchCriteria.ContactName.Trim());
                HCMDatabase.AddInParameter(getClientCommand,
                    "@DEPARTMENT_NAME",
                    DbType.String, clientSearchCriteria.DeparmentName == null || clientSearchCriteria.DeparmentName.Trim().Length == 0 ? null : clientSearchCriteria.DeparmentName.Trim());

                HCMDatabase.AddInParameter(getClientCommand,
                    "@CREATED_BY",
                    DbType.Int32, clientSearchCriteria.CreatedBy);

                HCMDatabase.AddInParameter(getClientCommand,
                   "@DEPARTMENT_ID",
                   DbType.Int32, clientSearchCriteria.DepartmentID);
                HCMDatabase.AddInParameter(getClientCommand,
                    "@TENANT_ID",
                    DbType.Int32, clientSearchCriteria.TenantID);

                HCMDatabase.AddInParameter(getClientCommand,
                   "@CLIENT_ID",
                   DbType.Int32, clientSearchCriteria.ClientID);
                HCMDatabase.AddInParameter(getClientCommand,
                  "@CONTACT_ID",
                  DbType.Int32, clientSearchCriteria.ContactID);
                HCMDatabase.AddInParameter(getClientCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.LoadDataSet(getClientCommand,
                clientsDataSet, "clientInfo");
                List<Department> clientDepartmentList = null;

                foreach (DataRow row in clientsDataSet.Tables[0].Rows)
                {
                    if (row["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (clientDepartmentList == null)
                            clientDepartmentList = new List<Department>();

                        Department clientDepartment = new Department();
                        ContactInformation contInfo = new ContactInformation();
                        if (!Utility.IsNullOrEmpty(row["CLIENT_ID"]) && row["CLIENT_ID"].ToString().Trim().Length > 0)
                        {
                            clientDepartment.ClientID = Convert.ToInt32(row["CLIENT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(row["DEPARTMENT_ID"]) && row["DEPARTMENT_ID"].ToString().Trim().Length > 0)
                        {
                            clientDepartment.DepartmentID = Convert.ToInt32(row["DEPARTMENT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(row["CLIENT_NAME"]))
                        {
                            clientDepartment.ClientName = row["CLIENT_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["DEPARTMENT_NAME"]))
                        {
                            clientDepartment.DepartmentName = row["DEPARTMENT_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["DEPARTMENT_DESCRIPTION"]))
                        {
                            clientDepartment.DepartmentDescription = row["DEPARTMENT_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["ADDITIONAL_INFO"]))
                        {
                            clientDepartment.AdditionalInfo = row["ADDITIONAL_INFO"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["STREET_ADDRESS"]))
                        {
                            contInfo.StreetAddress = row["STREET_ADDRESS"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["CITY_NAME"]))
                        {
                            contInfo.City = row["CITY_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["STATE_NAME"]))
                        {
                            contInfo.State = row["STATE_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["ZIPCODE"]))
                        {
                            contInfo.PostalCode = row["ZIPCODE"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["COUNTRY"]))
                        {
                            contInfo.Country = row["COUNTRY"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["EMAIL_ID"]))
                        {
                            contInfo.EmailAddress = row["EMAIL_ID"].ToString().Trim();
                        }
                        //if (!Utility.IsNullOrEmpty(row["WEBSITE_URL"]))
                        //{
                        //    WebSite website = new WebSite();
                        //    website.Personal = row["WEBSITE_URL"].ToString().Trim();
                        //    contInfo.WebSiteAddress = website;
                        //}
                        PhoneNumber phone = new PhoneNumber();
                        if (!Utility.IsNullOrEmpty(row["PHONE_NUMBER"]))
                            phone.Mobile = row["PHONE_NUMBER"].ToString().Trim();
                        else
                            phone.Mobile = string.Empty;

                        contInfo.Phone = phone;

                        if (!Utility.IsNullOrEmpty(row["FAX_NO"]))
                        {
                            clientDepartment.FaxNumber = row["FAX_NO"].ToString().Trim();
                        }

                        clientDepartment.ContactInformation = contInfo;
                        //if (!Utility.IsNullOrEmpty(row["FEIN_NO"]))
                        //{
                        //    clientDepartment.FeinNo = row["FEIN_NO"].ToString().Trim();
                        //}
                        //if (!Utility.IsNullOrEmpty(row["NO_OF_DEPARTMENTS"]))
                        //{
                        //    client.NoOfDepartments = Convert.ToInt32(row["NO_OF_DEPARTMENTS"].ToString().Trim());
                        //}
                        //if (!Utility.IsNullOrEmpty(row["ADDITIONAL_INFO"]))
                        //{
                        //    clientDepartment.AdditionalInfo = row["ADDITIONAL_INFO"].ToString().Trim();
                        //}
                        if (!Utility.IsNullOrEmpty(row["NO_OF_CONTACTS"]))
                        {
                            clientDepartment.NoOfContacts = Convert.ToInt32(row["NO_OF_CONTACTS"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(row["NO_OF_POSITION_PROFILES"]))
                        {
                            clientDepartment.NoOfPositionProfile = Convert.ToInt32(row["NO_OF_POSITION_PROFILES"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(row["CREATED_BY"]))
                        {
                            clientDepartment.CreatedByName = row["CREATED_BY"].ToString().Trim();
                        }

                        //var query =
                        //(from DataRow p in clientsDataSet.Tables[1].AsEnumerable()
                        // where p.Field<int>("CLIENT_ID") == client.ClientID
                        // select new Department
                        // {
                        //     DepartmentID = p.Field<int>("DEPARTMENT_ID"),
                        //     DepartmentName = p.Field<string>("DEPARTMENT_NAME"),
                        //     DepartmentDescription = p.Field<string>("DEPARTMENT_DESCRIPTION")

                        // });
                        //client.Departments = query.ToList();
                        // Add to the list.
                        clientDepartmentList.Add(clientDepartment);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(row["TOTAL"]);
                    }
                }
                return clientDepartmentList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (clientsDataSet != null)
                {
                    clientsDataSet.Dispose();
                }
            }
        }


        public List<ClientContactInformation> GetClientContactInformation(ClientSearchCriteria clientSearchCriteria, int pageNumber,
             int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            DataSet clientsDataSet = new DataSet();

            totalRecords = 0;
            try
            {
                DbCommand getClientCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CONTACTS");

                // Add parameters.
                HCMDatabase.AddInParameter(getClientCommand,
                    "@CLIENT_NAME",
                    DbType.String, clientSearchCriteria.ClientName == null || clientSearchCriteria.ClientName.Trim().Length == 0 ? null : clientSearchCriteria.ClientName.Trim());
                HCMDatabase.AddInParameter(getClientCommand,
                    "@CONTACT_NAME",
                    DbType.String, clientSearchCriteria.ContactName == null || clientSearchCriteria.ContactName.Trim().Length == 0 ? null : clientSearchCriteria.ContactName.Trim());
                HCMDatabase.AddInParameter(getClientCommand,
                    "@DEPARTMENT_NAME",
                    DbType.String, clientSearchCriteria.DeparmentName == null || clientSearchCriteria.DeparmentName.Trim().Length == 0 ? null : clientSearchCriteria.DeparmentName.Trim());

                HCMDatabase.AddInParameter(getClientCommand,
                    "@CREATED_BY",
                    DbType.Int32, clientSearchCriteria.CreatedBy);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@TENANT_ID",
                    DbType.Int32, clientSearchCriteria.TenantID);

                HCMDatabase.AddInParameter(getClientCommand,
                   "@CLIENT_ID",
                   DbType.Int32, clientSearchCriteria.ClientID);
                HCMDatabase.AddInParameter(getClientCommand,
                  "@DEPARTMENT_ID",
                  DbType.Int32, clientSearchCriteria.DepartmentID);
                HCMDatabase.AddInParameter(getClientCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getClientCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.LoadDataSet(getClientCommand,
                clientsDataSet, "clientInfo");
                List<ClientContactInformation> clientContactList = null;

                foreach (DataRow row in clientsDataSet.Tables[0].Rows)
                {
                    if (row["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (clientContactList == null)
                            clientContactList = new List<ClientContactInformation>();

                        ClientContactInformation clientContact = new ClientContactInformation();
                        ContactInformation contInfo = new ContactInformation();
                        if (!Utility.IsNullOrEmpty(row["CLIENT_ID"]) && row["CLIENT_ID"].ToString().Trim().Length > 0)
                        {
                            clientContact.ClientID = Convert.ToInt32(row["CLIENT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(row["CONTACT_ID"]) && row["CONTACT_ID"].ToString().Trim().Length > 0)
                        {
                            clientContact.ContactID = Convert.ToInt32(row["CONTACT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(row["FIRST_NAME"]) && row["FIRST_NAME"].ToString().Trim().Length > 0)
                        {
                            clientContact.FirstName = row["FIRST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["MIDDLE_NAME"]) && row["MIDDLE_NAME"].ToString().Trim().Length > 0)
                        {
                            clientContact.MiddleName = row["MIDDLE_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["LAST_NAME"]) && row["LAST_NAME"].ToString().Trim().Length > 0)
                        {
                            clientContact.LastName = row["LAST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["CONTACT_NAME"]) && row["CONTACT_NAME"].ToString().Trim().Length > 0)
                        {
                            clientContact.ContactName = row["CONTACT_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(row["CLIENT_NAME"]))
                        {
                            clientContact.ClientName = row["CLIENT_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(row["STREET_ADDRESS"]))
                        {
                            contInfo.StreetAddress = row["STREET_ADDRESS"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["CITY_NAME"]))
                        {
                            contInfo.City = row["CITY_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["STATE_NAME"]))
                        {
                            contInfo.State = row["STATE_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["ZIPCODE"]))
                        {
                            contInfo.PostalCode = row["ZIPCODE"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["COUNTRY"]))
                        {
                            contInfo.Country = row["COUNTRY"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["EMAIL_ID"]))
                        {
                            contInfo.EmailAddress = row["EMAIL_ID"].ToString().Trim();
                        }
                        //if (!Utility.IsNullOrEmpty(row["WEBSITE_URL"]))
                        //{
                        //    WebSite website = new WebSite();
                        //    website.Personal = row["WEBSITE_URL"].ToString().Trim();
                        //    contInfo.WebSiteAddress = website;
                        //}
                        PhoneNumber phone = new PhoneNumber();
                        if (!Utility.IsNullOrEmpty(row["PHONE_NUMBER"]))
                            phone.Mobile = row["PHONE_NUMBER"].ToString().Trim();
                        else
                            phone.Mobile = string.Empty;

                        contInfo.Phone = phone;

                        if (!Utility.IsNullOrEmpty(row["FAX_NO"]))
                        {
                            clientContact.FaxNo = row["FAX_NO"].ToString().Trim();
                        }

                        clientContact.ContactInformation = contInfo;
                        //if (!Utility.IsNullOrEmpty(row["FEIN_NO"]))
                        //{
                        //    clientDepartment.FeinNo = row["FEIN_NO"].ToString().Trim();
                        //}
                        //if (!Utility.IsNullOrEmpty(row["NO_OF_DEPARTMENTS"]))
                        //{
                        //    client.NoOfDepartments = Convert.ToInt32(row["NO_OF_DEPARTMENTS"].ToString().Trim());
                        //}
                        if (!Utility.IsNullOrEmpty(row["ADDITIONAL_INFO"]))
                        {
                            clientContact.AdditionalInfo = row["ADDITIONAL_INFO"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(row["NO_OF_DEPARTMENTS"]))
                        {
                            clientContact.NoOfDepartments = Convert.ToInt32(row["NO_OF_DEPARTMENTS"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(row["NO_OF_POSITION_PROFILES"]))
                        {
                            clientContact.NoOfPositionProfile = Convert.ToInt32(row["NO_OF_POSITION_PROFILES"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(row["CREATED_BY"]))
                        {
                            clientContact.CreatedByName = row["CREATED_BY"].ToString().Trim();
                        }

                        var query =
                        (from DataRow p in clientsDataSet.Tables[1].AsEnumerable()
                         where p.Field<int>("CONTACT_ID") == clientContact.ContactID
                         select new Department
                         {
                             DepartmentID = p.Field<int>("DEPARTMENT_ID"),
                             DepartmentName = p.Field<string>("DEPARTMENT_NAME"),
                             DepartmentDescription = p.Field<string>("DEPARTMENT_DESCRIPTION")

                         });
                        clientContact.Departments = query.ToList();
                        // Add to the list.
                        clientContactList.Add(clientContact);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(row["TOTAL"]);
                    }
                }
                return clientContactList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (clientsDataSet != null)
                {
                    clientsDataSet.Dispose();
                }
            }
        }

        #endregion GET
        #region UPDATE
        /// <summary>
        /// This method edits the client informations
        /// </summary>
        /// <param name="clientInfo">
        /// Contains client details
        /// </param>
        /// <param name="transaction">
        /// Holds the transaction object
        /// </param>
        public void UpdateClient(ClientInformation clientInfo, out int clientCount, IDbTransaction transaction)
        {
            clientCount = 0;
            // Create a stored procedure command object.
            DbCommand updateClientCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CLIENT");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateClientCommand,
                "@CLIENT_ID", DbType.Int32, clientInfo.ClientID);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@CLIENT_NAME", DbType.String, clientInfo.ClientName);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@STREET_ADDRESS", DbType.String, clientInfo.ContactInformation.StreetAddress);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@CITY_NAME", DbType.String, clientInfo.ContactInformation.City);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@STATE_NAME", DbType.String, clientInfo.ContactInformation.State);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@ZIPCODE", DbType.String, clientInfo.ContactInformation.PostalCode);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@COUNTRY", DbType.String, clientInfo.ContactInformation.Country);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@FEIN_NO", DbType.String, clientInfo.FeinNo);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@PHONE_NUMBER", DbType.String, clientInfo.ContactInformation.Phone.Mobile);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@FAX_NO", DbType.String, clientInfo.FaxNumber);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@EMAIL_ID", DbType.String, clientInfo.ContactInformation.EmailAddress);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@WEBSITE_URL", DbType.String, clientInfo.ContactInformation.WebSiteAddress.Personal);
            HCMDatabase.AddInParameter(updateClientCommand,
                "@ADDITIONAL_INFO", DbType.String, clientInfo.AdditionalInfo);
            HCMDatabase.AddInParameter(updateClientCommand,
               "@TENANT_ID", DbType.Int32, clientInfo.TenantID);
            HCMDatabase.AddOutParameter(updateClientCommand,
              "@CLIENT_COUNT", DbType.Int32, clientCount);


            // Execute the stored procedure.
            clientCount = int.Parse(HCMDatabase.ExecuteScalar(updateClientCommand, transaction as DbTransaction).ToString());
        }

        /// <summary>
        /// Updates the department.
        /// </summary>
        /// <param name="departmentInfo">The department info.</param>
        /// <param name="departmentCount">The department count.</param>
        /// <param name="transaction">The transaction.</param>
        public void UpdateDepartment(Department departmentInfo, out int departmentCount, IDbTransaction transaction)
        {
            departmentCount = 0;
            // Create a stored procedure command object.
            DbCommand updateDepartmentCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_DEPARTMENT");

            HCMDatabase.AddInParameter(updateDepartmentCommand,
               "@CLIENT_ID", DbType.String, departmentInfo.ClientID);
            HCMDatabase.AddInParameter(updateDepartmentCommand,
                "@DEPARTMENT_NAME", DbType.String, departmentInfo.DepartmentName);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
                "@STREET_ADDRESS", DbType.String, departmentInfo.ContactInformation.StreetAddress);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
                "@CITY_NAME", DbType.String, departmentInfo.ContactInformation.StreetAddress);
            HCMDatabase.AddInParameter(updateDepartmentCommand,
                "@STATE_NAME", DbType.String, departmentInfo.ContactInformation.State);
            HCMDatabase.AddInParameter(updateDepartmentCommand,
                "@ZIPCODE", DbType.String, departmentInfo.ContactInformation.PostalCode);
            HCMDatabase.AddInParameter(updateDepartmentCommand,
                "@COUNTRY", DbType.String, departmentInfo.ContactInformation.Country);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
                "@PHONE_NUMBER", DbType.String, departmentInfo.ContactInformation.Phone.Mobile);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
                "@FAX_NO", DbType.String, departmentInfo.FaxNumber);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
            "@EMAIL_ID", DbType.String, departmentInfo.ContactInformation.EmailAddress);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
               "@DEPARTMENT_DESCRIPTION ", DbType.String, departmentInfo.DepartmentDescription);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
               "@ADDITIONAL_INFO ", DbType.String, departmentInfo.AdditionalInfo);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
               "@USER_ID", DbType.Int32, departmentInfo.CreatedBy);

            HCMDatabase.AddInParameter(updateDepartmentCommand,
              "@DEPARTMENT_ID", DbType.Int32, departmentInfo.DepartmentID);
            HCMDatabase.AddOutParameter(updateDepartmentCommand,
              "@DEPARTMENT_COUNT", DbType.Int32, departmentCount);

            // Execute the stored procedure.
            departmentCount = int.Parse(HCMDatabase.ExecuteScalar(updateDepartmentCommand, transaction as DbTransaction).ToString());
        }

        #endregion UPDATE

        #region Client Management Settings

        /// <summary>
        /// Method that updates the client management settings
        /// </summary>
        /// <param name="settings">
        /// A <see cref="ClientManagementSettingsDetail"/> that holds the client 
        /// management settings.
        /// </param>
        public void UpdateClientManagementSettings(ClientManagementSettingsDetail settings)
        {
            // Create a stored procedure command object
            DbCommand updateSettingsCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CLIENT_MANAGEMENT_SETTINGS");

            // Add input parameters
            HCMDatabase.AddInParameter(updateSettingsCommand,
                "@TENANT_ID", DbType.Int32, settings.TenantID);

            HCMDatabase.AddInParameter(updateSettingsCommand,
                "@ALLOW_DELETE_CLIENT_FOR_ALL", DbType.String, settings.AllowDeleteClientForAll ? 'Y' : 'N');

            HCMDatabase.AddInParameter(updateSettingsCommand,
                "@USER_ID", DbType.Int32, settings.UserID);

            // Execute the command.
            HCMDatabase.ExecuteNonQuery(updateSettingsCommand);
        }

        /// <summary>
        /// Method that retreives the client management settings.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A <see cref="ClientManagementSettingsDetail"/> that holds the client 
        /// management settings.
        /// </returns>
        public ClientManagementSettingsDetail GetClientManagementSettings(int tenantID)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object
                DbCommand getSettingsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_MANAGEMENT_SETTINGS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getSettingsCommand, "@TENANT_ID", DbType.Int32, tenantID);

                dataReader = HCMDatabase.ExecuteReader(getSettingsCommand);

                // Create a client management settings object.
                ClientManagementSettingsDetail settings = null;

                // Check if the row exist.
                if (dataReader.Read())
                {
                    // Instantiate the client management settings object.
                    settings = new ClientManagementSettingsDetail();

                    ContactInformation contInfo = new ContactInformation();
                    if (!Utility.IsNullOrEmpty(dataReader["ALLOW_DELETE_CLIENT_FOR_ALL"]) &&
                        dataReader["ALLOW_DELETE_CLIENT_FOR_ALL"].ToString().Trim().ToUpper() == "Y")
                    {
                        settings.AllowDeleteClientForAll = true;
                    }
                }
                return settings;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        #endregion Client Management Settings

        public List<ClientInformation> GetClientName(int tenantID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPGET_CLIENTDETAILS");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@TENANT_ID", DbType.Int32, tenantID);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                List<ClientInformation> clientInfo = new List<ClientInformation>();


                while (dataReader.Read())
                {
                    ClientInformation clientInfoDetails = new ClientInformation();
                    clientInfoDetails.ClientID = int.Parse(dataReader["CLIENT_ID"].ToString());
                    clientInfoDetails.ClientName = dataReader["CLIENT_NAME"].ToString();

                    clientInfo.Add(clientInfoDetails);
                }

                return clientInfo;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public ClientContactInformation GetContact(int contactId)
        {
            IDataReader dataReader = null;
            try
            {

                // ClientInformation clientContact = null;
                ClientContactInformation clientContactInfo = null;

                DbCommand getClientCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_CONTACT_EDITDETAILS");

                // Add parameters.
                HCMDatabase.AddInParameter(getClientCommand, "@CONTACT_ID", DbType.Int32, contactId);


                dataReader = HCMDatabase.ExecuteReader(getClientCommand);
                if (dataReader.Read())
                {
                    clientContactInfo = new ClientContactInformation();

                    ContactInformation contInfo = new ContactInformation();
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                    {
                        clientContactInfo.ClientID = int.Parse(dataReader["CLIENT_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        clientContactInfo.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MIDDLE_NAME"]))
                    {
                        clientContactInfo.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                    {
                        clientContactInfo.LastName = dataReader["LAST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STREET_ADDRESS"]))
                    {
                        contInfo.StreetAddress = dataReader["STREET_ADDRESS"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CITY_NAME"]))
                    {
                        contInfo.City = dataReader["CITY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STATE_NAME"]))
                    {
                        contInfo.State = dataReader["STATE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ZIPCODE"]))
                    {
                        contInfo.PostalCode = dataReader["ZIPCODE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COUNTRY"]))
                    {
                        contInfo.Country = dataReader["COUNTRY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                    {
                        contInfo.EmailAddress = dataReader["EMAIL_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ADDITIONAL_INFO"]))
                    {
                        clientContactInfo.AdditionalInfo = dataReader["ADDITIONAL_INFO"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_CONTACT_INFO"]))
                    {
                        clientContactInfo.ClientAdditionalInfo = dataReader["CLIENT_CONTACT_INFO"].ToString().Trim();
                    }

                    PhoneNumber phone = new PhoneNumber();
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE_NUMBER"]))
                        phone.Mobile = dataReader["PHONE_NUMBER"].ToString().Trim();
                    else
                        phone.Mobile = string.Empty;

                    contInfo.Phone = phone;

                    if (!Utility.IsNullOrEmpty(dataReader["FAX_NUMBER"]))
                    {
                        clientContactInfo.FaxNo = dataReader["FAX_NUMBER"].ToString().Trim();
                    }

                    clientContactInfo.ContactInformation = contInfo;
                    clientContactInfo.ContactID = contactId;

                    clientContactInfo.Departments = GetDepartmentNames(contactId);
                }

                return clientContactInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        private List<Department> GetDepartmentNames(int contactId)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPGET_DEPARTMENT_NAMES");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@CONTACT_ID", DbType.Int32, contactId);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                List<Department> departmentInfo = new List<Department>();


                while (dataReader.Read())
                {
                    Department departmentInfoDetails = new Department();

                    departmentInfoDetails.DepartmentID = int.Parse(dataReader["DEPARTMENT_ID"].ToString());
                    departmentInfoDetails.DepartmentName = dataReader["DEPARTMENT_NAME"].ToString();

                    departmentInfoDetails.DepartmentIsSelected = dataReader["IS_SELECTED"].ToString();
                    departmentInfo.Add(departmentInfoDetails);
                }

                return departmentInfo;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        public void UpdateContact(ClientContactInformation clientContactInformation, out int contactCount, IDbTransaction transaction)
        {
            contactCount = 0;
            // Create a stored procedure command object.
            DbCommand updateContactCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CONTACT");

            ClientInformation clientInfo = new ClientInformation();
            HCMDatabase.AddInParameter(updateContactCommand,
               "@CONTACT_ID", DbType.String, clientContactInformation.ContactID);

            HCMDatabase.AddInParameter(updateContactCommand,
               "@TENANT_ID", DbType.String, clientContactInformation.TenantID);
            HCMDatabase.AddInParameter(updateContactCommand,
                "@FIRST_NAME", DbType.String, clientContactInformation.FirstName);

            HCMDatabase.AddInParameter(updateContactCommand,
                "@MIDDLE_NAME", DbType.String, clientContactInformation.MiddleName);

            HCMDatabase.AddInParameter(updateContactCommand,
                "@LAST_NAME", DbType.String, clientContactInformation.LastName);
            HCMDatabase.AddInParameter(updateContactCommand,
                "@STREET_ADDRESS", DbType.String, clientContactInformation.ContactInformation.StreetAddress);
            HCMDatabase.AddInParameter(updateContactCommand,
                "@CITY_NAME", DbType.String, clientContactInformation.ContactInformation.City);
            HCMDatabase.AddInParameter(updateContactCommand,
                "@STATE_NAME", DbType.String, clientContactInformation.ContactInformation.State);

            HCMDatabase.AddInParameter(updateContactCommand,
                "@ZIPCODE", DbType.String, clientContactInformation.ContactInformation.Phone.Mobile);

            HCMDatabase.AddInParameter(updateContactCommand,
                "@COUNTRY", DbType.String, clientContactInformation.ContactInformation.Country);

            HCMDatabase.AddInParameter(updateContactCommand,
            "@PHONE_NUMBER", DbType.String, clientContactInformation.ContactInformation.Phone.Mobile);



            HCMDatabase.AddInParameter(updateContactCommand,
               "@FAX_NUMBER", DbType.String, clientContactInformation.FaxNo);


            HCMDatabase.AddInParameter(updateContactCommand,
               "@EMAIL_ID", DbType.String, clientContactInformation.ContactInformation.EmailAddress);


            HCMDatabase.AddInParameter(updateContactCommand,
               "@ADDITIONAL_INFO", DbType.String, clientContactInformation.AdditionalInfo);

            HCMDatabase.AddInParameter(updateContactCommand,
               "@USER_ID", DbType.Int32, clientContactInformation.CreatedBy);

            HCMDatabase.AddOutParameter(updateContactCommand,
              "@CONTACT_COUNT", DbType.Int32, contactCount);

            // Execute the stored procedure.
            contactCount = int.Parse(HCMDatabase.ExecuteScalar(updateContactCommand, transaction as DbTransaction).ToString());
        }

        public void UpdateDepartments(ClientContactInformation clientContactInformation, IDbTransaction transaction)
        {

            try
            {
                DbCommand insertDepartmentContactsCommand = HCMDatabase.
           GetStoredProcCommand("SPINSERT_DEPARTMENTCONTACTS");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertDepartmentContactsCommand,
                    "@TENANT_ID", DbType.Int16, clientContactInformation.TenantID);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertDepartmentContactsCommand,
                    "@CONTACT_ID", DbType.Int16, clientContactInformation.ContactID);

                //HCMDatabase.AddInParameter(insertDepartmentContactsCommand,
                //   "@DEPARTMENT_ID", DbType.Int16, Convert.ToInt16(clientContactInformation.SellectedDeparments.Split(',')[i]));
                HCMDatabase.AddInParameter(insertDepartmentContactsCommand,
                   "@DEPARTMENT_ID", DbType.String, clientContactInformation.SellectedDeparments);
                // Add input parameters.
                HCMDatabase.AddInParameter(insertDepartmentContactsCommand,
                    "@USER_ID", DbType.Int16, clientContactInformation.CreatedBy);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(insertDepartmentContactsCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                throw exp;
            }



        }

        public int GetClientContactProstionProfileDetails(int contactID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPGETPOSITIONPROFILE_CONTACT");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@CONTACT_ID", DbType.Int32, contactID);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                ClientContactInformation clientInfoDetails = new ClientContactInformation();

                while (dataReader.Read())
                {

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_CONTACTS_COUNT"].ToString()))
                    {
                        clientInfoDetails.NoOfContacts = int.Parse(dataReader["POSITION_PROFILE_CONTACTS_COUNT"].ToString());
                    }

                }
                return clientInfoDetails.NoOfContacts;


            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int DeleteClientContactProstionProfileDetails(short contactID, int deleteOption)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_CONTACT");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@CONTACT_ID", DbType.Int32, contactID);
                HCMDatabase.AddInParameter(getdepartmentCommand, "@DELETE_CONDITION", DbType.Int32, deleteOption);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                ClientContactInformation clientInfoDetails = new ClientContactInformation();

                while (dataReader.Read())
                {

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_CONTACTS_COUNT"].ToString()))
                    {
                        clientInfoDetails.NoOfContacts = int.Parse(dataReader["POSITION_PROFILE_CONTACTS_COUNT"].ToString());
                    }

                }
                return clientInfoDetails.NoOfContacts;


            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int DeleteDepartmentClientContactDetails(short departmentId, int deleteCondition)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_DEPARTMENT");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@DEPARTMENT_ID", DbType.Int32, departmentId);
                HCMDatabase.AddInParameter(getdepartmentCommand, "@DELETE_CONDITION", DbType.Int32, deleteCondition);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                ClientContactInformation clientInfoDetails = new ClientContactInformation();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENTCONTACTS_COUNT"].ToString()))
                    {
                        clientInfoDetails.NoOfDepartments = int.Parse(dataReader["CLIENTCONTACTS_COUNT"].ToString());
                    }
                }
                return clientInfoDetails.NoOfDepartments;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int GetDepartmentCOntactsDetails(short departmentID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPGETCLIENTCONTACT_DEPARTMENT");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@DEPARTMENT_ID", DbType.Int32, departmentID);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                ClientContactInformation clientInfoDetails = new ClientContactInformation();

                while (dataReader.Read())
                {

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENTCONTACTS_COUNT"].ToString()))
                    {
                        clientInfoDetails.NoOfDepartments = int.Parse(dataReader["CLIENTCONTACTS_COUNT"].ToString());
                    }

                }
                return clientInfoDetails.NoOfDepartments;


            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int GetClientContactDetails(short clientID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPGETCONTACT_CLIENT");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@CLIENT_ID", DbType.Int32, clientID);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                ClientContactInformation clientInfoDetails = new ClientContactInformation();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENTCONTACTS_COUNT"].ToString()))
                    {
                        clientInfoDetails.NoOfContacts = int.Parse(dataReader["CLIENTCONTACTS_COUNT"].ToString());
                    }
                }
                return clientInfoDetails.NoOfContacts;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int DeleteClientPositionProfiletDetails(short clientID, int deleteCondition)
        {

            IDataReader dataReader = null;
            try
            {
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_CLIENT");
                HCMDatabase.AddInParameter(getdepartmentCommand, "@CLIENT_ID", DbType.Int32, clientID);
                HCMDatabase.AddInParameter(getdepartmentCommand, "@DELETE_CONDITION", DbType.Int32, deleteCondition);
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                ClientContactInformation clientInfoDetails = new ClientContactInformation();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENTCONTACTS_COUNT"].ToString()))
                    {
                        clientInfoDetails.NoOfContacts = int.Parse(dataReader["CLIENTCONTACTS_COUNT"].ToString());
                    }
                }
                return clientInfoDetails.NoOfContacts;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<ClientContactInformation> GetClientDerpartmentContacts(int clientID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand GetDepartmentClientContactsCommand = HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_DEPARTMENT_CONTACTS");
                HCMDatabase.AddInParameter(GetDepartmentClientContactsCommand, "@CLIENT_ID", DbType.Int32, clientID);
                dataReader = HCMDatabase.ExecuteReader(GetDepartmentClientContactsCommand);
                List<ClientContactInformation> clientInfoDetails = null;
                while (dataReader.Read())
                {
                    if (clientInfoDetails == null)
                        clientInfoDetails = new List<ClientContactInformation>();

                    ClientContactInformation clientInfoDetail = new ClientContactInformation();

                    clientInfoDetail.ContactName = dataReader["DEPARTMENT_CONTACT_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["DEPARTMENT_NAME"].ToString()))
                    {
                        clientInfoDetail.DepartmentName = dataReader["DEPARTMENT_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["DEPARTMENT_ID"].ToString()))
                    {
                        clientInfoDetail.DepartmentID = int.Parse(dataReader["DEPARTMENT_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CONTACT_ID"].ToString()))
                    {
                        clientInfoDetail.ContactID = int.Parse(dataReader["CONTACT_ID"].ToString());
                    }
                    clientInfoDetail.ClientDepartmentContactID = dataReader["CLIENT_DEPARTMENT_CONTACT_IDS"].ToString();
                    

                    clientInfoDetail.ClientID = clientID;

                    clientInfoDetails.Add(clientInfoDetail);
                }
                return clientInfoDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        public void GetClientName(int? clientID, int? departmentID, int? contactID, out int clientNewId, out string clientName)
        {
            IDataReader dataReader = null;
            try
            {
                clientNewId = 0;
                clientName = string.Empty;
                DbCommand getClientNameCommand = HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_NAME");
                HCMDatabase.AddInParameter(getClientNameCommand, "@CLIENT_ID", DbType.Int32, clientID);
                HCMDatabase.AddInParameter(getClientNameCommand, "@DEPARTMENT_ID", DbType.Int32, departmentID);
                HCMDatabase.AddInParameter(getClientNameCommand, "@CONTACT_ID", DbType.Int32, contactID);
                
                dataReader = HCMDatabase.ExecuteReader(getClientNameCommand);
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                    {
                        clientNewId = int.Parse(dataReader["CLIENT_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                    {
                        clientName = dataReader["CLIENT_NAME"].ToString();
                    }
                }
               
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of clients for the given search
        /// parameters.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="clientName">
        /// A <see cref="string"/> that holds the client name.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ClientInformation"/> that holds the client information.
        /// </returns>
        public List<ClientInformation> GetClients(int tenantID,string clientName, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            List<ClientInformation> clientDetails = null;

            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getClientCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_LIST");


                HCMDatabase.AddInParameter(getClientCommand, "@TENANT_ID",
                    DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getClientCommand, "@CLIENT_NAME", DbType.String,
                    Utility.IsNullOrEmpty(clientName) ? null : clientName.Trim());

                HCMDatabase.AddInParameter(getClientCommand, "@PAGE_NUM",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getClientCommand, "@PAGE_SIZE",
                    DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getClientCommand, "@ORDER_BY",
                    DbType.String, sortField);

                HCMDatabase.AddInParameter(getClientCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getClientCommand);

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate skill list.
                        if (clientDetails == null)
                            clientDetails = new List<ClientInformation>();

                        // Create a skill object.
                        ClientInformation clientDetail = new ClientInformation();

                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                        {
                            clientDetail.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        {
                            clientDetail.ClientName = dataReader["CLIENT_NAME"].ToString();
                        }

                        // Add to the list.
                        clientDetails.Add(clientDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return clientDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the list of departments 
        /// info against its contact id
        /// </summary>
        /// <param name="contactID">
        /// A <see cref="int"/> that holds the contact id.
        /// </param>
        /// <returns>
        /// A list of <see cref="Department"/> that holds the department details.
        /// </returns>
        public List<Department> GetDepartmentsByContactID(int contactID)
        {
            List<Department> departments = null;
            IDataReader dataReader = null;

            try
            {
                DbCommand getDepartmentCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CONTACT_DEPARTMENTS_BY_CONTACT_ID");

                HCMDatabase.AddInParameter(getDepartmentCommand, "@CONTACT_ID", DbType.Int32,
                    contactID);

                dataReader = HCMDatabase.ExecuteReader(getDepartmentCommand);

                while (dataReader.Read())
                {
                    // Instantiate department list.
                    if (departments == null)
                        departments = new List<Department>();

                    // Create a department object.
                    Department department = new Department();

                    if (!Utility.IsNullOrEmpty(dataReader["DEPARTMENT_NAME"]))
                    {
                        department.DepartmentName = dataReader["DEPARTMENT_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ADDITIONAL_INFO"]))
                    {
                        department.AdditionalInfo = dataReader["ADDITIONAL_INFO"].ToString();
                    }

                    // Add to the list.
                    departments.Add(department);
                }

                return departments;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }


        /// <summary>
        /// Method that retreives the client details against the position profile id.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the positionProfile ID.
        /// </param>
        /// <returns>
        /// A <see cref="ClientInformation"/> that holds the client details.
        /// </returns>
        public ClientInformation GetClientInfoPositionProfileID(int positionProfileID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getClientInfoCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_BY_POSITION_PROFILE_ID");

                HCMDatabase.AddInParameter(getClientInfoCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                dataReader = HCMDatabase.ExecuteReader(getClientInfoCommand);

                ClientInformation clientDetail = null;

                // Retreive recruiter details.
                if (dataReader.Read())
                {
                    // Instantiate assessor detail object.
                    clientDetail = new ClientInformation();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                        clientDetail.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        clientDetail.ClientName = dataReader["CLIENT_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_DEPARTMENTS"]))
                        clientDetail.ClientDepartments = dataReader["CLIENT_DEPARTMENTS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_CONTACTS"]))
                        clientDetail.ClientContacts = dataReader["CLIENT_CONTACTS"].ToString();
                }
                return clientDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
    }
}
