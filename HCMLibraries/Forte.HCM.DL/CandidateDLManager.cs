﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateDLManager.cs
// File that represents the business layer for the Candidate module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

#region Directives                                                             
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the Candidate module.
    /// This includes functionalities for retrieving the Test information ,
    /// schelduler Details,credit information, create self test session,
    /// resumes details, etc.  
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class CandidateDLManager : DatabaseConnectionManager
    {
        #region Public Methods                                                 

        /// <summary>
        /// Retrieve the test instructions disclaimer message.
        /// </summary>
        /// <returns>
        /// A <see cref="DisclaimerMessage"/> that holds the disclaimer
        /// messages.
        /// </returns>
        public DisclaimerMessage GetTestInstructionsDisclaimerMessage()
        {
            IDataReader dataReader = null;

            try
            {
                DisclaimerMessage disclaimer = null;

                // Create a stored procedure command object.
                DbCommand disclaimerCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_TEST_INSTRUCTIONS_DISCLAIMER_MESSAGES");

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(disclaimerCommand);

                // Read the records from the data reader.
                if (dataReader.Read())
                {
                    // Instantiate the disclaimer message object.
                    disclaimer = new DisclaimerMessage();

                    if (!Utility.IsNullOrEmpty(dataReader["HARDWARE_INSTRUCTIONS"]))
                    {
                        disclaimer.HardwareInstructions = dataReader
                            ["HARDWARE_INSTRUCTIONS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_INSTRUCTIONS"]))
                    {
                        disclaimer.TestInstructions = dataReader
                            ["TEST_INSTRUCTIONS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CYBER_PROCTORING_INSTRUCTIONS"]))
                    {
                        disclaimer.CyberProctoringInstructions = dataReader
                            ["CYBER_PROCTORING_INSTRUCTIONS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WARNING_INSTRUCTIONS"]))
                    {
                        disclaimer.WarningInstructions = dataReader
                            ["WARNING_INSTRUCTIONS"].ToString().Trim();
                    }
                }
                return disclaimer;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Retrieve the interview instructions disclaimer message.
        /// </summary>
        /// <returns>
        /// A <see cref="DisclaimerMessage"/> that holds the disclaimer
        /// messages.
        /// </returns>
        public DisclaimerMessage GetInterviewInstructionsDisclaimerMessage()
        {
            IDataReader dataReader = null;

            try
            {
                DisclaimerMessage disclaimer = null;

                // Create a stored procedure command object.
                DbCommand disclaimerCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_INSTRUCTIONS_DISCLAIMER_MESSAGES");

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(disclaimerCommand);

                // Read the records from the data reader.
                if (dataReader.Read())
                {
                    // Instantiate the disclaimer message object.
                    disclaimer = new DisclaimerMessage();

                    if (!Utility.IsNullOrEmpty(dataReader["HARDWARE_SOFTWARE_REQUIREMENTS"]))
                    {
                        disclaimer.HardwareInstructions = dataReader
                            ["HARDWARE_SOFTWARE_REQUIREMENTS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_INSTRUCTIONS"]))
                    {
                        disclaimer.InterviewInstructions = dataReader
                            ["INTERVIEW_INSTRUCTIONS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WARNING_INSTRUCTIONS"]))
                    {
                        disclaimer.WarningInstructions = dataReader
                            ["WARNING_INSTRUCTIONS"].ToString().Trim();
                    }
                }
                return disclaimer;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Retrive the Candidate Details 
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate id.
        /// </param>
        /// <returns>
        /// Candidate Details
        /// </returns>
        public CandidateSummary GetCandidateActivities(int candidateID)
        {
            if (candidateID <= 0)
                throw new Exception("Candidate ID parameter zero or less than zero");

            IDataReader dataReader = null;
            CandidateSummary summary = null;
            try 
            {/*
                DbCommand getPageControlsCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_CURRENT_ACTIVITIES");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                CandidateTestSessionDetail sessionDetail = null;
                #region PendingTest
                
                while (dataReader.Read())
                {
                    // Instantiate candidate summary object.
                    if (summary == null)
                        summary = new CandidateSummary();

                    // Instantiate pending tests list object.
                    if (summary.PendingTests == null)
                        summary.PendingTests = new List<CandidateTestSessionDetail>();

                    // Instantiate candidate test session detail object.
                    sessionDetail = new CandidateTestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                    {
                        sessionDetail.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        sessionDetail.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                    {
                        sessionDetail.TestID = dataReader["TEST_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        sessionDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                    {
                        sessionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                    }
                   
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                    {
                        sessionDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                    {
                        sessionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                    {
                        sessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        sessionDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }

                    // Add to the list.
                    summary.PendingTests.Add(sessionDetail);
                }
                #endregion

                #region Pending Interviews 

                CandidateInterviewSessionDetail interviewDetail = null;
                if (dataReader.NextResult())
                {     
                    while (dataReader.Read()) 
                    {
                        // Instantiate candidate summary object.
                        if (summary == null)
                            summary = new CandidateSummary();

                        // Instantiate candidate interview test session detail object.
                        if (summary.InterviewTests == null)
                            summary.InterviewTests = new List<CandidateInterviewSessionDetail>();

                        interviewDetail = new CandidateInterviewSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_ID"]))
                        {
                            interviewDetail.InterviewTestID = dataReader["INTERVIEW_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                        {
                            interviewDetail.InterviewTestSessionID = dataReader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_ID"]))
                        {
                            interviewDetail.InterviewTestID = dataReader["INTERVIEW_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                        {
                            interviewDetail.InterviewTestName = dataReader["INTERVIEW_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                        {
                            interviewDetail.InterviewTestDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        {
                            interviewDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            interviewDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            interviewDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            interviewDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            interviewDetail.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        {
                            interviewDetail.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                        {
                            if (dataReader["STATUS"].ToString().Trim().ToUpper() == "ATMPT_PAUS")
                                interviewDetail.IsPaused = true;
                        }

                        // Add activity to the list.
                        summary.InterviewTests.Add(interviewDetail);
                    }
                }
                #endregion

                if (dataReader.NextResult())
                    while (dataReader.Read())
                    { }

                #region MyTests
                CandidateTestSessionDetail myTests = null;
                if (dataReader.NextResult())
                { 
                    while (dataReader.Read()) 
                    {
                        // Instantiate candidate summary object.
                        if (summary == null)
                            summary = new CandidateSummary();

                        // Instantiate pending tests list object.
                        if (summary.MyTests == null)
                            summary.MyTests = new List<CandidateTestSessionDetail>(); 

                        // Instantiate candidate test session detail object.
                        myTests = new CandidateTestSessionDetail();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            myTests.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            myTests.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            myTests.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            myTests.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            myTests.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        } 
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        {
                            myTests.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            myTests.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            myTests.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            myTests.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }

                        // Add to the list.
                        summary.MyTests.Add(myTests);
                    }
                }
                #endregion

                if (dataReader.NextResult())
                    while (dataReader.Read())
                    { }

                #region Completed test
               CandidateTestSessionDetail candidateCompletedTestDetail = null;
                if (dataReader.NextResult())
                {
                    while (dataReader.Read())
                    {
                        // Instantiate candidate summary object.
                        if (summary == null)
                            summary = new CandidateSummary();

                        // Instantiate pending tests list object.
                        if (summary.CompletedTests == null)
                            summary.CompletedTests = new List<CandidateTestSessionDetail>();

                        // Instantiate candidate test session detail object.
                        candidateCompletedTestDetail = new CandidateTestSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            candidateCompletedTestDetail.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            candidateCompletedTestDetail.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            candidateCompletedTestDetail.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            candidateCompletedTestDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            candidateCompletedTestDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            candidateCompletedTestDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_STATUS"]))
                        {
                            candidateCompletedTestDetail.IsCertification = (dataReader["CERTIFICATION_STATUS"].ToString().ToUpper() == "AVAILABLE") ? true : false;
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        {
                            candidateCompletedTestDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            candidateCompletedTestDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            candidateCompletedTestDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                        {
                            candidateCompletedTestDetail.DateCompleted = Convert.ToDateTime(dataReader["COMPLETED_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DISPLAY_RESULT"]))
                        {
                            candidateCompletedTestDetail.ShowResults = dataReader["DISPLAY_RESULT"].ToString().
                                Trim().ToUpper() == "Y" ? true : false;
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            candidateCompletedTestDetail.TestInitiatedBy = dataReader["INITIATED_BY"].ToString().Trim();
                        }

                        // Add to the list.
                        summary.CompletedTests.Add(candidateCompletedTestDetail);
                    }
                }    
                #endregion
                */
                return summary;
            }
            catch { return null; }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        
        /// <summary>
        /// Retrive the Candidate Details 
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate id.
        /// </param>
        /// <returns>
        /// Candidate Details
        /// </returns>
        public CandidateSummary GetCandidateSummary(int candidateID)
        {
            if (candidateID <= 0)
                throw new Exception("Candidate ID parameter zero or less than zero");

            IDataReader dataReader = null;
            CandidateSummary summary = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_SUMMARY");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                // Read candidate details.
                if (dataReader.Read())
                {
                    // Instantiate candidate summary object.
                    if (summary == null)
                        summary = new CandidateSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        summary.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        summary.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    summary.UserSince = DateTime.MinValue;
                    if (!Utility.IsNullOrEmpty(dataReader["USER_SINCE"]))
                    {
                        summary.UserSince = Convert.ToDateTime(dataReader["USER_SINCE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_TEST_TAKEN"]))
                    {
                        summary.LastTestTaken = dataReader["LAST_TEST_TAKEN"].ToString().Trim();
                    }

                    summary.LastTestTakenDate = DateTime.MinValue;
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_TEST_TAKEN_DATE"]))
                    {
                        summary.LastTestTakenDate = Convert.ToDateTime(dataReader["LAST_TEST_TAKEN_DATE"].ToString().Trim());
                    }

                    // Test summary.
                    if (!Utility.IsNullOrEmpty(dataReader["PENDING_TESTS_COUNT"]))
                    {
                        summary.PendingTestsCount = Convert.ToInt32(dataReader["PENDING_TESTS_COUNT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_TESTS_COUNT"]))
                    {
                        summary.CompletedTestsCount = Convert.ToInt32(dataReader["COMPLETED_TESTS_COUNT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRED_TESTS_COUNT"]))
                    {
                        summary.ExpiredTestsCount = Convert.ToInt32(dataReader["EXPIRED_TESTS_COUNT"].ToString().Trim());
                    }

                    // Interview summary.
                    if (!Utility.IsNullOrEmpty(dataReader["PENDING_INTERVIEWS_COUNT"]))
                    {
                        summary.PendingInterviewsCount = Convert.ToInt32(dataReader["PENDING_INTERVIEWS_COUNT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_INTERVIEWS_COUNT"]))
                    {
                        summary.CompletedInterviewsCount = Convert.ToInt32(dataReader["COMPLETED_INTERVIEWS_COUNT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRED_INTERVIEWS_COUNT"]))
                    {
                        summary.ExpiredInterviewsCount = Convert.ToInt32(dataReader["EXPIRED_INTERVIEWS_COUNT"].ToString().Trim());
                    }
                }

                // Read top 3 pending tests.
                if (dataReader.NextResult())
                {
                    CandidateTestSessionDetail sessionDetail = null;

                    while (dataReader.Read())
                    {
                        // Instantiate candidate summary object.
                        if (summary == null)
                            summary = new CandidateSummary();

                        // Instantiate pending tests list object.
                        if (summary.PendingTest == null)
                            summary.PendingTest = new List<CandidateTestSessionDetail>();

                        // Instantiate candidate test session detail object.
                        sessionDetail = new CandidateTestSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            sessionDetail.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            sessionDetail.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            sessionDetail.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            sessionDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            sessionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            sessionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        {
                            sessionDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            sessionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            sessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            sessionDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }

                        // Add to the list.
                        summary.PendingTest.Add(sessionDetail);
                    }
                }

                // Read top 3 suggested tests.
                if (dataReader.NextResult())
                {
                    CandidateTestSessionDetail sessionDetail = null;

                    while (dataReader.Read())
                    {
                        // Instantiate candidate summary object.
                        if (summary == null)
                            summary = new CandidateSummary();

                        // Instantiate pending tests list object.
                        if (summary.SuggestedTests == null)
                            summary.SuggestedTests = new List<CandidateTestSessionDetail>();

                        // Instantiate candidate test session detail object.
                        sessionDetail = new CandidateTestSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            sessionDetail.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            sessionDetail.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            sessionDetail.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            sessionDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            sessionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            sessionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        {
                            sessionDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            sessionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            sessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            sessionDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }

                        // Add to the list.
                        summary.SuggestedTests.Add(sessionDetail);
                    }
                }

                // Read top 3 completed tests.
                if (dataReader.NextResult())
                {
                    CandidateTestSessionDetail sessionDetail = null;

                    while (dataReader.Read())
                    {
                        // Instantiate candidate summary object.
                        if (summary == null)
                            summary = new CandidateSummary();

                        // Instantiate pending tests list object.
                        if (summary.CompletedTest == null)
                            summary.CompletedTest = new List<CandidateTestSessionDetail>();

                        // Instantiate candidate test session detail object.
                        sessionDetail = new CandidateTestSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            sessionDetail.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            sessionDetail.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            sessionDetail.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            sessionDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            sessionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            sessionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_STATUS"]))
                        {
                            sessionDetail.IsCertification = (dataReader["CERTIFICATION_STATUS"].ToString().ToUpper() == "AVAILABLE") ? true : false;
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        {
                            sessionDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            sessionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            sessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                        {
                            sessionDetail.DateCompleted = Convert.ToDateTime(dataReader["COMPLETED_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DISPLAY_RESULT"]))
                        {
                            sessionDetail.ShowResults = dataReader["DISPLAY_RESULT"].ToString().
                                Trim().ToUpper() == "Y" ? true : false;
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            sessionDetail.TestInitiatedBy = dataReader["INITIATED_BY"].ToString().Trim();
                        }

                        // Add to the list.
                        summary.CompletedTest.Add(sessionDetail);
                    }
                }

                return summary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate activity summary.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateActivitySummary"/> that holds the candidate
        /// activity summary.
        /// </returns>
        public CandidateActivitySummary GetCandidateActivitySummary(int candidateID)
        {
            if (candidateID <= 0)
                throw new Exception("Invalid candidate ID");

            IDataReader dataReader = null;
            CandidateActivitySummary summary = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_ACTIVITY_SUMMARY");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                // Read candidate details.
                if (dataReader.Read())
                {
                    // Instantiate candidate summary object.
                    if (summary == null)
                        summary = new CandidateActivitySummary();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        summary.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INFO_ID"]))
                    {
                        summary.CandidateInfoID = Convert.ToInt32(dataReader["CANDIDATE_INFO_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        summary.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_NAME"]))
                    {
                        summary.RecruiterName = dataReader["RECRUITER_NAME"].ToString().Trim();
                    }

                    summary.UserSince = DateTime.MinValue;
                    if (!Utility.IsNullOrEmpty(dataReader["USER_SINCE"]))
                    {
                        summary.UserSince = Convert.ToDateTime(dataReader["USER_SINCE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVITIES_COUNT"]))
                    {
                        summary.ActivitiesCount = Convert.ToInt32(dataReader["ACTIVITIES_COUNT"].ToString().Trim());
                    }
                }

                return summary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate pending test activities
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateTestSessionDetail"/> that holds the 
        /// candidate pending test activities.
        /// </returns>
        public List<CandidateTestSessionDetail> GetPendingTestActivities(
            int candidateID, int pageNumber, int pageSize, out int totalRecords)
        {
            if (candidateID <= 0)
                throw new Exception("Invalid candidate ID");

            // Assign default value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_PENDING_TEST_ACTIVITIES");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                List<CandidateTestSessionDetail> activities = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate candidate summary object.
                        if (activities == null)
                            activities = new List<CandidateTestSessionDetail>();

                        // Instantiate candidate test session detail object.
                        CandidateTestSessionDetail sessionDetail = new CandidateTestSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                        {
                            sessionDetail.RowNumber = Convert.ToInt32(dataReader["ROWNUMBER"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            sessionDetail.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            sessionDetail.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            sessionDetail.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            sessionDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            sessionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        {
                            sessionDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            sessionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            sessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            sessionDetail.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        {
                            sessionDetail.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            sessionDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["IS_SCHEDULED"]))
                        {
                            sessionDetail.IsScheduled = dataReader["IS_SCHEDULED"].
                                ToString().Trim().ToUpper() == "Y" ? true : false;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["IS_SELF_ADMIN"]))
                        {
                            sessionDetail.IsSelfAdmin = dataReader["IS_SELF_ADMIN"].
                                ToString().Trim().ToUpper() == "Y" ? true : false;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_STATUS"]))
                        {
                            sessionDetail.Status = dataReader["TEST_STATUS"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_ID"]))
                        {
                            sessionDetail.TestRecommendationID = dataReader["TEST_RECOMMEND_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTIONS"]))
                        {
                            sessionDetail.TotalQuestions = Convert.ToInt32(dataReader["TOTAL_QUESTIONS"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TIME"]))
                        {
                            sessionDetail.TimeLimit = Convert.ToInt32(dataReader["TEST_RECOMMEND_TIME"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ADMIN_STATUS"]))
                        {
                            sessionDetail.TestAdminStatus = dataReader["TEST_ADMIN_STATUS"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRED"]))
                        {
                            sessionDetail.IsExpired = dataReader["EXPIRED"].ToString().Trim().ToUpper() == "Y" ? true : false;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_MESSAGE"]))
                        {
                            sessionDetail.ScheduledByMessage = dataReader["SCHEDULED_BY_MESSAGE"].ToString().Trim();
                        }

                        // Add activity to the list.
                        activities.Add(sessionDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return activities;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate pending interview activities
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInterviewSessionDetail"/> that holds the 
        /// candidate pending interview activities.
        /// </returns>
        public List<CandidateInterviewSessionDetail> GetPendingInterviewActivities
            (int candidateID)
        {
            if (candidateID <= 0)
                throw new Exception("Invalid candidate ID");

            IDataReader dataReader = null;

            try
            {
                DbCommand getInterviewsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_PENDING_INTERVIEW_ACTIVITIES");

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(getInterviewsCommand);

                List<CandidateInterviewSessionDetail> activities = null;

                while (dataReader.Read())
                {
                    // Instantiate candidate summary object.
                    if (activities == null)
                        activities = new List<CandidateInterviewSessionDetail>();

                    // Instantiate candidate interview test session detail object.
                    CandidateInterviewSessionDetail sessionDetail = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_ID"]))
                    {
                        sessionDetail.InterviewTestSessionID = dataReader["INTERVIEW_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                    {
                        sessionDetail.CandidateTestSessionID = dataReader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_ID"]))
                    {
                        sessionDetail.InterviewTestID = dataReader["INTERVIEW_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                    {
                        sessionDetail.InterviewTestName = dataReader["INTERVIEW_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                    {
                        sessionDetail.InterviewTestDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                    {
                        sessionDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                    {
                        sessionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                    {
                        sessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        sessionDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                    {
                        sessionDetail.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                    {
                        sessionDetail.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        if (dataReader["STATUS"].ToString().Trim().ToUpper() == "ATMPT_PAUS")
                            sessionDetail.IsPaused = true;
                    }

                    // Add activity to the list.
                    activities.Add(sessionDetail);
                }

                return activities;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Retrive the test introduction details.
        /// </summary>
        /// <param name="candidateSessionID">
        ///  A <see cref="string"/> that holds the candidate Session ID.
        /// </param>
        /// <returns>
        /// Test Introduction Detail
        /// </returns>
        public TestIntroductionDetail GetTestIntroduction(string candidateSessionID)
        {
            if (Utility.IsNullOrEmpty(candidateSessionID))
                throw new Exception("Candidate session ID parameter cannot be empty");

            IDataReader dataReader = null;
            TestIntroductionDetail testIntroductionDetail = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_INTRODUCTION");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_SESSION_ID",
                    DbType.String, candidateSessionID);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                if (dataReader.Read())
                {
                    // Instantiate test introduction detail object.
                    testIntroductionDetail = new TestIntroductionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                    {
                        testIntroductionDetail.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        testIntroductionDetail.CandidateSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                    {
                        testIntroductionDetail.TestID = dataReader["TEST_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        testIntroductionDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                    {
                        testIntroductionDetail.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["RECOMMENDED_TIME"]))
                    {
                        testIntroductionDetail.TimeLimit = Convert.ToInt32(dataReader["RECOMMENDED_TIME"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREAS"]))
                    {
                        testIntroductionDetail.TestAreas = dataReader["TEST_AREAS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SUBJECTS"]))
                    {
                        testIntroductionDetail.TestSubjects = dataReader["TEST_SUBJECTS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_INSTRUCTIONS"]))
                    {
                        testIntroductionDetail.TestInstructions = dataReader["TEST_INSTRUCTIONS"].ToString().Trim();
                    }
                }
                return testIntroductionDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Retrieves the interview introduction detail.
        /// </summary>
        /// <param name="candidateSessionID">
        ///  A <see cref="string"/> that holds the candidate Session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewIntroductionDetail"/> that holds the interview
        /// introduction detail.
        /// </returns>
        public InterviewIntroductionDetail GetInterviewIntroduction
            (string candidateSessionID, int attemptID)
        {
            if (Utility.IsNullOrEmpty(candidateSessionID))
                throw new Exception("Candidate session ID parameter cannot be empty");

            IDataReader dataReader = null;
            InterviewIntroductionDetail testIntroductionDetail = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_INTRODUCTION");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_INTERVIEW_SESSION_ID",
                    DbType.String, candidateSessionID);
                HCMDatabase.AddInParameter(getPageControlsCommand,
                   "@ATTEMPT_ID",
                   DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                if (dataReader.Read())
                {
                    // Instantiate test introduction detail object.
                    testIntroductionDetail = new InterviewIntroductionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_ID"]))
                    {
                        testIntroductionDetail.InterviewSessionID = dataReader["INTERVIEW_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        testIntroductionDetail.CandidateSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_ID"]))
                    {
                        testIntroductionDetail.InterviewID = dataReader["INTERVIEW_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_NAME"]))
                    {
                        testIntroductionDetail.InterviewName = dataReader["INTERVIEW_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                    {
                        testIntroductionDetail.InterviewDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_AREAS"]))
                    {
                        testIntroductionDetail.InterviewTestAreas = dataReader["INTERVIEW_AREAS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SUBJECTS"]))
                    {
                        testIntroductionDetail.InterviewSubjects = dataReader["INTERVIEW_SUBJECTS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_INSTRUCTIONS"]))
                    {
                        testIntroductionDetail.InterviewInstructions = dataReader["INTERVIEW_INSTRUCTIONS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        if (dataReader["STATUS"].ToString().Trim().ToUpper() == "ATMPT_PAUS")
                            testIntroductionDetail.IsPaused = true;
                    }
                }
                return testIntroductionDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
       
        /// <summary>
        /// Retrive the Pending test based on the Search Creteria.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="string"/> that holds the candidate ID.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the orderBy.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order By Direction Like 'A' or 'D'.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// List of Candidate pending TestDetails based on the Cadidate Id and search criteria.
        /// </returns>
        public List<CandidateTestDetail> GetPendingTests(int candidateID, string orderBy,
            SortType orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {
            if (Utility.IsNullOrEmpty(orderBy))
                throw new Exception("OrderBy parameter cannot be empty");

            if (Utility.IsNullOrEmpty(orderByDirection))
                throw new Exception("OrderByDirection parameter cannot be empty");

            // Assign default value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_MY_TESTS");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_SESSION_STATUS",
                    DbType.String, Constants.CandidateTestTypes.PENDING);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                List<CandidateTestDetail> tests = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (tests == null)
                            tests = new List<CandidateTestDetail>();

                        CandidateTestDetail test = new CandidateTestDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                        {
                            test.RowNumber = Convert.ToInt32(dataReader["ROWNUMBER"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            test.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            test.CandidateSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            test.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            test.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            test.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY_USER_ID"]))
                        {
                            test.InitiatedByID = Convert.ToInt32(dataReader["INITIATED_BY_USER_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            test.InitiatedBy = dataReader["INITIATED_BY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            test.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_ON"]))
                        {
                            test.InitiatedOn = Convert.ToDateTime(dataReader["INITIATED_ON"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            test.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            test.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            test.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        {
                            test.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                        }

                        if (candidateID == test.SchedulerID)
                        {
                            test.IsSelfAdminTest = true;
                            test.TestAdminStatus = "Self Administered";
                        }
                        else
                            test.TestAdminStatus = "Recruiter Administered";
                        
                        // Add to the list.
                        tests.Add(test);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return tests;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Retrive the Completed test based on the Search Creteria.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="string"/> that holds the candidate ID.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the orderBy.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order By Direction Like 'A' or 'D'.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// List of Candidate Completed  TestDetails based on the Cadidate Id and search criteria.
        /// </returns>
        public List<CandidateTestDetail> GetCompletedTests(int candidateID, string orderBy,
           SortType orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {

            if (Utility.IsNullOrEmpty(orderBy))
                throw new Exception("OrderBy parameter cannot be empty");

            if (Utility.IsNullOrEmpty(orderByDirection))
                throw new Exception("OrderByDirection parameter cannot be empty");

            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_MY_TESTS");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_SESSION_STATUS",
                    DbType.String, Constants.CandidateTestTypes.COMPLETED);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                List<CandidateTestDetail> tests = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (tests == null)
                            tests = new List<CandidateTestDetail>();

                        CandidateTestDetail test = new CandidateTestDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                        {
                            test.RowNumber = Convert.ToInt32(dataReader["ROWNUMBER"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            test.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            test.CandidateSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            test.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            test.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            test.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY_USER_ID"]))
                        {
                            test.InitiatedByID = Convert.ToInt32(dataReader["INITIATED_BY_USER_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            test.InitiatedBy = dataReader["INITIATED_BY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            test.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                        {
                            test.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_ON"]))
                        {
                            test.InitiatedOn = Convert.ToDateTime(dataReader["INITIATED_ON"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            test.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_ON"]))
                        {
                            test.CompletedOn = Convert.ToDateTime(dataReader["COMPLETED_ON"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_STATUS"]))
                        {
                            test.CertificationStatus = dataReader["CERTIFICATION_STATUS"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CYBER_PROCTORING"]))
                        {
                            test.IsCyberProctoring = dataReader["CYBER_PROCTORING"].ToString().
                                Trim().ToUpper() == "Y" ? true : false;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DISPLAY_RESULT"]))
                        {
                            test.ShowResults = dataReader["DISPLAY_RESULT"].ToString().
                                Trim().ToUpper() == "Y" ? true : false;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["IS_SELF_ADMIN"]))
                        {
                            test.IsSelfAdmin = dataReader["IS_SELF_ADMIN"].
                                ToString().Trim().ToUpper() == "Y" ? true : false;
                        }

                        if (candidateID == test.SchedulerID)
                        {
                            test.IsSelfAdminTest = true;
                            test.TestAdminStatus = "Self Administered";
                        }
                        else
                            test.TestAdminStatus = "Recruiter Administered";

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_MESSAGE"]))
                        {
                            test.ScheduledByMessage = dataReader["SCHEDULED_BY_MESSAGE"].ToString().Trim();
                        }


                        // Add to the list.
                        tests.Add(test);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return tests;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Retrive the Expired test based on the Search Creteria.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="string"/> that holds the candidate ID.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the orderBy.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order By Direction Like 'A' or 'D'.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// List of Candidate Expired TestDetails based on the Cadidate Id and search criteria.
        /// </returns>
        public List<CandidateTestDetail> GetExpiredTests(int candidateID, string orderBy,
           SortType orderByDirection, int pageNumber, int pageSize, out int totalRecords)
        {
            if (Utility.IsNullOrEmpty(orderBy))
                throw new Exception("OrderBy parameter cannot be empty");

            if (Utility.IsNullOrEmpty(orderByDirection))
                throw new Exception("OrderByDirection parameter cannot be empty");

            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_MY_TESTS");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_SESSION_STATUS",
                    DbType.String, Constants.CandidateTestTypes.EXPIRED);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                List<CandidateTestDetail> tests = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (tests == null)
                            tests = new List<CandidateTestDetail>();

                        CandidateTestDetail test = new CandidateTestDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                        {
                            test.RowNumber = Convert.ToInt32(dataReader["ROWNUMBER"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            test.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            test.CandidateSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            test.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            test.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY_USER_ID"]))
                        {
                            test.InitiatedByID = Convert.ToInt32(dataReader["INITIATED_BY_USER_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            test.InitiatedBy = dataReader["INITIATED_BY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            test.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_ON"]))
                        {
                            test.InitiatedOn = Convert.ToDateTime(dataReader["INITIATED_ON"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            test.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            test.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }

                        // Add to the list.
                        tests.Add(test);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return tests;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will help to retrieve the scheduler, candidate and test details
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session id.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <returns>
        /// A <see cref="TestScheduleDetail"/> that contains the scheduler information.
        /// </returns>
        public TestScheduleDetail GetSchedulerDetails(string candidateSessionID, int attemptID)
        {
            IDataReader dataReader = null;
            TestScheduleDetail scheduleDetail = null;
            string scheduler = string.Empty;

            try
            {
                DbCommand getSchedulerCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_SCHEDULER_DETAILS");

                // Add input parameter
                HCMDatabase.AddInParameter(getSchedulerCommand,
                    "@CANDIDATE_SESSION_ID", DbType.String, candidateSessionID);
                HCMDatabase.AddInParameter(getSchedulerCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getSchedulerCommand);

                // Retrieve candidate details
                while (dataReader.Read())
                {
                    if (scheduleDetail == null)
                        scheduleDetail = new TestScheduleDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        scheduleDetail.TestID = dataReader["TEST_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        scheduleDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                        scheduleDetail.EmailId = dataReader["CANDIDATE_EMAIL"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        scheduleDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_EMAIL"]))
                        scheduleDetail.SchedulerEmail = dataReader["AUTHOR_EMAIL"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_FIRST_NAME"]))
                        scheduleDetail.SchedulerFirstName = dataReader["AUTHOR_FIRST_NAME"].ToString().Trim();
                }
                return scheduleDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the test scheduer email detail, that allows
        /// candidates to send email to them.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestScheduleDetail"/> that holds the scheduler detail.
        /// </returns>
        public TestScheduleDetail GetTestSchedulerEmailDetail
            (string candidateSessionID, int attemptID)
        {
            IDataReader dataReader = null;
            TestScheduleDetail schedulerDetail = null;
            string scheduler = string.Empty;

            try
            {
                DbCommand getSchedulerCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_SCHEDULER_EMAIL_DETAIL");

                // Add input parameter
                HCMDatabase.AddInParameter(getSchedulerCommand,
                    "@CANDIDATE_SESSION_ID", DbType.String, candidateSessionID);
                HCMDatabase.AddInParameter(getSchedulerCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getSchedulerCommand);

                // Retrieve candidate details
                if (dataReader.Read())
                {
                    if (schedulerDetail == null)
                        schedulerDetail = new TestScheduleDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        schedulerDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                        schedulerDetail.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_NAME"]))
                        schedulerDetail.SchedulerName = dataReader["SCHEDULER_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_EMAIL"]))
                        schedulerDetail.SchedulerEmail = dataReader["SCHEDULER_EMAIL"].ToString().Trim();
                }
                return schedulerDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the interview scheduer email detail, that allows
        /// candidates to send email to them.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestScheduleDetail"/> that holds the scheduler detail.
        /// </returns>
        public TestScheduleDetail GetInterviewSchedulerEmailDetail
            (string candidateSessionID, int attemptID)
        {
            IDataReader dataReader = null;
            TestScheduleDetail schedulerDetail = null;
            string scheduler = string.Empty;

            try
            {
                DbCommand getSchedulerCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_SCHEDULER_EMAIL_DETAIL");

                // Add input parameter
                HCMDatabase.AddInParameter(getSchedulerCommand,
                    "@CANDIDATE_INTERVIEW_SESSION_ID", DbType.String, candidateSessionID);
                HCMDatabase.AddInParameter(getSchedulerCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                dataReader = HCMDatabase.ExecuteReader(getSchedulerCommand);

                // Retrieve candidate details
                if (dataReader.Read())
                {
                    if (schedulerDetail == null)
                        schedulerDetail = new TestScheduleDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        schedulerDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                        schedulerDetail.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_NAME"]))
                        schedulerDetail.SchedulerName = dataReader["SCHEDULER_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_EMAIL"]))
                        schedulerDetail.SchedulerEmail = dataReader["SCHEDULER_EMAIL"].ToString().Trim();
                }
                return schedulerDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method that retrieves the recruiter email detail, that allows
        /// candidates to send email to them.
        /// </summary>
        /// <param name="candidateLoginID">
        /// A <see cref="int"/> that holds the candidate login ID.
        /// </param>
        /// <returns>
        /// A <see cref="RecruiterDetail"/> that holds the recruiter detail.
        /// </returns>
        public RecruiterDetail GetRecruiterEmailDetail
            (int candidateLoginID)
        {
            IDataReader dataReader = null;
            RecruiterDetail recruiterDetail = null;
            string scheduler = string.Empty;

            try
            {
                DbCommand getSchedulerCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_RECRUITER_EMAIL_DETAIL");

                // Add input parameter
                HCMDatabase.AddInParameter(getSchedulerCommand,
                    "@CANDIDATE_LOGIN_ID", DbType.Int32, candidateLoginID);

                dataReader = HCMDatabase.ExecuteReader(getSchedulerCommand);

                // Retrieve candidate details
                if (dataReader.Read())
                {
                    if (recruiterDetail == null)
                        recruiterDetail = new RecruiterDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        recruiterDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                        recruiterDetail.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_NAME"]))
                        recruiterDetail.RecruiterName = dataReader["RECRUITER_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_EMAIL"]))
                        recruiterDetail.RecruiterEmail = dataReader["RECRUITER_EMAIL"].ToString().Trim();
                }
                return recruiterDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will be called when the user wants to retake/reactivate
        /// a test.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="requestType">
        /// A <see cref="string"/> that contains the request Type.
        /// </param>
        public void UpdateTestRequest(string candidateSessionKey, string requestType)
        {
            // Create a stored procedure command object
            DbCommand updateRetakeRequestCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_RETAKE_REQUEST");

            // Add input parameter
            HCMDatabase.AddInParameter(updateRetakeRequestCommand,
                "@CAND_SESS_KEY", DbType.String, candidateSessionKey);

            HCMDatabase.AddInParameter(updateRetakeRequestCommand,
                "@ACTION", DbType.String, requestType.Trim());

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateRetakeRequestCommand);
        }

        /// <summary>
        /// check whether Cyber Proctor Enabled or not 
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains Cyber Proctor Enabled or not.
        /// </returns>
        public bool IsCyberProctorEnabled(string cansessionKey, int attempID)
        {
            bool isProctorEnabled = false;

            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand cyberProctorDesktopImagedetail = HCMDatabase.
                     GetStoredProcCommand("SPGET_CYBERPROCTOR_STATUS");

                cyberProctorDesktopImagedetail.CommandTimeout = 0;

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                    "CAND_SESSION_KEY", System.Data.DbType.String, cansessionKey);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                   "ATTEMPT_ID", System.Data.DbType.Int32, attempID);


                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImagedetail);


                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["CYBER_PROCTORING"].ToString().ToUpper().Trim().Equals("Y"))
                        isProctorEnabled = true;
                }
                dataReader.Close();

                return isProctorEnabled;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// check whether Cyber Proctor Started or not 
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains Cyber Proctor Started or not.
        /// </returns>
        public bool IsCyberProctorStarted(string cansessionKey, int attempID)
        {
            bool isProctorStarted = false;

            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand cyberProctorDesktopImagedetail = HCMDatabase.
                     GetStoredProcCommand("SPGET_CYBERPROCTOR_RUNING_STATUS");

                cyberProctorDesktopImagedetail.CommandTimeout = 0;

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                    "CAND_SESSION_KEY", System.Data.DbType.String, cansessionKey);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                   "ATTEMPT_ID", System.Data.DbType.Int32, attempID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImagedetail);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["SESSION_STATUS"].ToString().ToUpper().Trim().Equals("ATMPT_CPIN"))
                        isProctorStarted = true;
                }
                dataReader.Close();

                return isProctorStarted;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Update the session Status. 
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains the cansessionKey.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains the attempt ID.
        /// </param>
        public void UpdateTestStatus(string cansessionKey, int attempID)
        {
            // Create a stored procedure command object.
            DbCommand cyberProctorDesktopImagedetail = HCMDatabase.
                 GetStoredProcCommand("SPUPDATE_CANDIDATE_SESSION_TRACKING_STATUS_BYCANDSESSIONKEY");

            cyberProctorDesktopImagedetail.CommandTimeout = 0;

            HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                "CAND_SESSION_KEY", System.Data.DbType.String, cansessionKey);

            HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
               "ATTEMPT_ID", System.Data.DbType.Int32, attempID);

            HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
               "@SESSION_STATUS", System.Data.DbType.String, "SESS_INIT");

            HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
            "@MODIFIED_BY", System.Data.DbType.Int32, 1);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(cyberProctorDesktopImagedetail);
        }

        /// <summary>
        /// Update the authetication code.
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains attempt ID.
        /// </param>
        /// <param name="authCode">
        /// A <see cref="string"/> that contains authendication code.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains authendication Code Code is true or false.
        /// </returns>
        public bool UpdateAuthCode(string cansessionKey, int attempID, string authCode)
        {
            // Create a stored procedure command object.
            DbCommand cyberProctoruthCode = HCMDatabase.
                 GetStoredProcCommand("SPUPDATE_CANDIDATE_AUTHCODE");

            cyberProctoruthCode.CommandTimeout = 0;

            HCMDatabase.AddInParameter(cyberProctoruthCode,
                "CAND_SESSION_KEY", System.Data.DbType.String, cansessionKey);

            HCMDatabase.AddInParameter(cyberProctoruthCode,
               "ATTEMPT_ID", System.Data.DbType.Int32, attempID);

            HCMDatabase.AddInParameter(cyberProctoruthCode,
               "@AUTHCODE", System.Data.DbType.String, authCode);

            HCMDatabase.AddOutParameter(cyberProctoruthCode,
               "@ISEXIST", System.Data.DbType.String, 5);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(cyberProctoruthCode);

            if ((!Utility.IsNullOrEmpty(cyberProctoruthCode.Parameters["@ISEXIST"].Value) &&
                cyberProctoruthCode.Parameters["@ISEXIST"].Value.ToString().ToUpper() == "Y"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method that updates the interview security code.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID>
        /// </param>
        /// <param name="securityCode">
        /// A <see cref="string"/> that holds the security code.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status whether security code 
        /// already exist or not. True if already exist and false not exist.
        /// </returns>
        public bool UpdateInterviewSecurityCode(string candidateSessionID, 
            int attemptID, string securityCode)
        {

            // Create a stored procedure command object.
            DbCommand updateSecurityCodeCommand = HCMDatabase.
                 GetStoredProcCommand("SPUPDATE_INTERVIEW_SECURITY_CODE");

            updateSecurityCodeCommand.CommandTimeout = 0;

            HCMDatabase.AddInParameter(updateSecurityCodeCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

            HCMDatabase.AddInParameter(updateSecurityCodeCommand,
               "@ATTEMPT_ID", DbType.Int32, attemptID);

            HCMDatabase.AddInParameter(updateSecurityCodeCommand,
               "@AUTH_CODE", DbType.String, securityCode);

            HCMDatabase.AddOutParameter(updateSecurityCodeCommand,
               "@IS_EXIST", DbType.String, 5);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateSecurityCodeCommand);

            if ((!Utility.IsNullOrEmpty(updateSecurityCodeCommand.Parameters["@IS_EXIST"].Value) &&
                updateSecurityCodeCommand.Parameters["@IS_EXIST"].Value.ToString().ToUpper() == "Y"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method that updates the interview session tracking status.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID>
        /// </param>
        /// <param name="sessionStatus">
        /// A <see cref="string"/> that holds the session status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status whether security code 
        /// already exist or not. True if already exist and false not exist.
        /// </returns>
        public void UpdateInterviewSessionTrackingStatus(string candidateSessionID,
            int attemptID, string sessionStatus, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateSecurityCodeCommand = HCMDatabase.
                 GetStoredProcCommand("SPUPDATE_INTERVIEW_CANDIDATE_SESSION_TRACKING_STATUS_BY_SESSION_KEY");

            updateSecurityCodeCommand.CommandTimeout = 0;

            HCMDatabase.AddInParameter(updateSecurityCodeCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

            HCMDatabase.AddInParameter(updateSecurityCodeCommand,
               "@ATTEMPT_ID", DbType.Int32, attemptID);

            HCMDatabase.AddInParameter(updateSecurityCodeCommand,
                "@SESSION_STATUS", DbType.String, sessionStatus);

            HCMDatabase.AddInParameter(updateSecurityCodeCommand,
                "@MODIFIED_BY", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateSecurityCodeCommand);
        }

        /// <summary>
        /// Method that retrieves the test detail.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <returns>
        ///  A <see cref="TestDetail"/> that holds the test detail.
        /// </returns>
        public TestDetail GetTestDetail(string candidateSessionID)
        {
            IDataReader dataReader = null;

            try
            {
                TestDetail tstDetail = new TestDetail();
                // Create a stored procedure command object.
                DbCommand cyberProctorDesktopImagedetail = HCMDatabase.
                     GetStoredProcCommand("SPGET_TEST_DETAIL_AND_SESSION_KEYS");
                cyberProctorDesktopImagedetail.CommandTimeout = 0;
                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                    "CAND_SESSION_KEY", DbType.String, candidateSessionID);
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImagedetail);
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    tstDetail.Description = dataReader["TEST_DESCRIPTION"].ToString();
                }
                return tstDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the interview detail.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        ///  A <see cref="InterviewDetail"/> that holds the interview detail.
        /// </returns>
        public InterviewDetail GetInterviewDetail(string candidateSessionID, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                InterviewDetail interviewDetail = new InterviewDetail();

                DbCommand getInterviewCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_DETAIL");

                getInterviewCommand.CommandTimeout = 0;

                HCMDatabase.AddInParameter(getInterviewCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getInterviewCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getInterviewCommand);

                // Read the records from the data reader.
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        interviewDetail.InterviewTestKey = dataReader["INTERVIEW_TEST_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                        interviewDetail.InterviewSessionKey = dataReader["INTERVIEW_SESSION_KEY"].ToString();
                    
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        interviewDetail.InterviewName = dataReader["INTERVIEW_TEST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                        interviewDetail.InterviewDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SECURITY_CODE"]))
                        interviewDetail.SecurityCode = dataReader["SECURITY_CODE"].ToString();
                }
                return interviewDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the test and session details.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds attempt ID.
        /// </param>
        /// <returns>
        ///  A <see cref="TestDetail"/> that contains Test Detail.
        /// </returns>
        public TestDetail GetTestSessionDetail(string candidateSessionID, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                TestDetail testDetail = null;

                // Create a stored procedure command object.
                DbCommand cyberProctorDesktopImagedetail = HCMDatabase.
                     GetStoredProcCommand("SPGET_TEST_SESSION_DETAIL");
                cyberProctorDesktopImagedetail.CommandTimeout = 0;
                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                    "@CANDIDATE_SESSION_KEY", System.Data.DbType.String, candidateSessionID);
                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                    "@ATTEMPT_ID", System.Data.DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImagedetail);

                // Read the records from the data reader.
                if (dataReader.Read())
                {
                    if (testDetail == null)
                        testDetail = new TestDetail();

                    // Assign test name.
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        testDetail.Name = dataReader["TEST_NAME"].ToString().Trim();

                    // Assign test description.
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        testDetail.Description = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                }
                return testDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the candidate name for the given candidate id
        /// </summary>
        /// <param name="candidateSessionId">
        /// A<see cref="string"/>that holds the candidate id 
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the candidate name
        /// </returns>
        public string GetCandidateName(string candidateSessionId)
        {
            IDataReader reader = null;

            string candidateName = null;
            try
            {
                DbCommand getCandidateNameCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_NAME");
                HCMDatabase.AddInParameter(getCandidateNameCommand, "@CANDIDATE_SESSION_KEY", DbType.String, candidateSessionId);

                reader = HCMDatabase.ExecuteReader(getCandidateNameCommand);

                while (reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["USER_NAME"]))
                    {
                        candidateName = reader["USER_NAME"].ToString().Trim();
                    }
                }
                return candidateName;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }

            }
        } 

        public CandidateDetail GetCandidateInformation(int candidateID)
        {
            IDataReader reader = null;
            CandidateDetail candidateDetail = null;

            try
            {
                DbCommand getCandidateNameCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_DETAIL");
                HCMDatabase.AddInParameter(getCandidateNameCommand, "@CANDIDATE_ID", DbType.Int32, candidateID);

                reader = HCMDatabase.ExecuteReader(getCandidateNameCommand);

                if (reader.Read())
                {
                    // Instantiate the candidate detail object.
                    candidateDetail = new CandidateDetail();

                    if (!Utility.IsNullOrEmpty(reader["CAIFIRSTNAME"]))
                    {
                        candidateDetail.FirstName = reader["CAIFIRSTNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["CAILASTNAME"]))
                    {
                        candidateDetail.LastName = reader["CAILASTNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["CAIADDRESS"]))
                    {
                        candidateDetail.Address = reader["CAIADDRESS"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["CAILOCATION"]))
                    {
                        candidateDetail.Location = reader["CAILOCATION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["CAIEMAIL"]))
                    {
                        candidateDetail.EMailID = reader["CAIEMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(reader["CAIHOMEPHONE"]))
                    {
                        candidateDetail.Phone = reader["CAIHOMEPHONE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["CAICELL"]))
                    {
                        candidateDetail.Mobile = reader["CAICELL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["LinkedInProfile"]))
                    {
                        candidateDetail.LinkedInProfile = reader["LinkedInProfile"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["Recruiter"]))
                    {
                        candidateDetail.RecruiterName = reader["Recruiter"].ToString().Trim();
                    }
                } 
            } 
            finally
            {
                // Close the data reader, if it is open.
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                } 
            }

            return candidateDetail;
        }

        public List<CandidateTestSessionDetail> GetTestActivityDashboard(int candidateID,
            int pageNumber, int pageSize,string searchText, out int totalRecords)
        {
             if (candidateID <= 0)
                throw new Exception("Candidate ID parameter zero or less than zero");

             totalRecords = 0;

            IDataReader dataReader = null;
            List<CandidateTestSessionDetail> listTestSession = null;
            try
            {
                DbCommand getPageControlsCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_TEST_ACTIVITY_DASHBOARD");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                "@SEARCHTEXT", DbType.String, searchText);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                CandidateTestSessionDetail testSession = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {  
                        // Instantiate pending tests list object.
                        if (listTestSession == null)
                            listTestSession = new List<CandidateTestSessionDetail>();

                        // Instantiate candidate test session detail object.
                        testSession = new CandidateTestSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                        {
                            testSession.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                        {
                            testSession.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                        {
                            testSession.TestID = dataReader["TEST_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        {
                            testSession.TestName = dataReader["TEST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                        {
                            testSession.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            testSession.AttemptID =Convert.ToInt32(dataReader["ATTEMPT_ID"] );
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        {
                            testSession.SchedulerName ="Scheduled by "+ dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                        {
                            testSession.CompletedDate ="Completed on " + Convert.ToDateTime(dataReader["COMPLETED_DATE"].ToString().Trim()).ToShortDateString();
                             
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SCORE"]))
                        {
                            testSession.AbsoluteScore =  Convert.ToInt32(dataReader["SCORE"]) + "%";
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PROFILE_NAME"]))
                        {
                            testSession.ProfileName = dataReader["PROFILE_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        {
                            testSession.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();
                        }
                        listTestSession.Add(testSession);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return listTestSession;
            }
            catch { return null; }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<CandidateInterviewSessionDetail> GetInterviewActivityDashboard(int candidateID,
            int pageNumber, int pageSize,string searchText, out int totalRecords)
        {
            if (candidateID <= 0)
                throw new Exception("Candidate ID parameter zero or less than zero");
            totalRecords = 0;
            IDataReader dataReader = null;
            try
            {
                DbCommand getPageControlsCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_ACTIVITY_DASHBOARD");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getPageControlsCommand,
                "@SEARCHTEXT", DbType.String, searchText);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                List<CandidateInterviewSessionDetail> listInterviewSession = null;

                CandidateInterviewSessionDetail interviewSession = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate pending tests list object.
                        if (listInterviewSession == null)
                            listInterviewSession = new List<CandidateInterviewSessionDetail>();

                        // Instantiate candidate interviewSession session detail object.
                        interviewSession = new CandidateInterviewSessionDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                        {
                            interviewSession.InterviewSessionKey = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                        {
                            interviewSession.CandidateInterviewSessionID = dataReader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        {
                            interviewSession.InterviewTestKey = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        {
                            interviewSession.InterviewTestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                        {
                            interviewSession.InterviewTestDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                        {
                            interviewSession.CompletedDate =" Completed on "+ Convert.ToDateTime(dataReader["COMPLETED_DATE"].ToString().Trim()).ToShortDateString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                        {
                            interviewSession.AssessorID = Convert.ToInt32(dataReader["ASSESSOR_ID"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            interviewSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PROFILE_NAME"]))
                        {
                            interviewSession.PositionProfileName= dataReader["PROFILE_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        {
                            interviewSession.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        {
                            interviewSession.SchedulerName = "Scheduled by " + dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                        }
                        listInterviewSession.Add(interviewSession);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return listInterviewSession;
            }
            catch { return null; }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
 
        /// <summary>
        /// Method to get the candidate name for the given candidate id
        /// </summary>
        /// <param name="candidateSessionId">
        /// A<see cref="string"/>that holds the candidate Session Id
        /// </param>
        /// <returns>
        /// A<see cref="CandidateDetail"/>that holds the candidate detail
        /// </returns>
        public CandidateDetail GetCandidateBasicDetail(int candidateID)
        {
            IDataReader reader = null;
            CandidateDetail candidateDetail = new CandidateDetail();

            try
            {
                DbCommand getCandidateNameCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_BASICDETAIL");
                HCMDatabase.AddInParameter(getCandidateNameCommand, "@CANDIDATE_ID", DbType.Int32, candidateID);

                reader = HCMDatabase.ExecuteReader(getCandidateNameCommand);

                while (reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["FIRSTNAME"]))
                    {
                        candidateDetail.FirstName = reader["FIRSTNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["ADDRESS"]))
                    {
                        candidateDetail.Address = reader["ADDRESS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(reader["LOCATION"]))
                    {
                        candidateDetail.Location = reader["LOCATION"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(reader["EMAIL"]))
                    {
                        candidateDetail.EMailID = reader["EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(reader["PHONE"]))
                    {
                        candidateDetail.Phone = reader["PHONE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["MOBILE"]))
                    {
                        candidateDetail.Mobile = reader["MOBILE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["LINKEDINPROFILE"]))
                    {
                        candidateDetail.LinkedInProfile = reader["LINKEDINPROFILE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["USER_NAME"]))
                    {
                        candidateDetail.UserName = reader["USER_NAME"].ToString().Trim();
                    }
                     
                }
                return candidateDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }

            }
        }

        /// <summary>
        /// Method to get the candidate name for the given candidate id
        /// </summary>
        /// <param name="candidateSessionId">
        /// A<see cref="string"/>that holds the candidate Session Id
        /// </param>
        /// <returns>
        /// A<see cref="CandidateDetail"/>that holds the candidate detail
        /// </returns>
        public CandidateDetail GetCandidateDetail(string candidateSessionId)
        {
            IDataReader reader = null;
            CandidateDetail candidateDetail = new CandidateDetail();

            try
            {
                DbCommand getCandidateNameCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_INFO");
                HCMDatabase.AddInParameter(getCandidateNameCommand, "@CANDIDATE_SESSION_KEY", DbType.String, candidateSessionId);

                reader = HCMDatabase.ExecuteReader(getCandidateNameCommand);

                while (reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["FIRSTNAME"]))
                    {
                        candidateDetail.FirstName = reader["FIRSTNAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(reader["LOCATION"]))
                    {
                        candidateDetail.Location = reader["LOCATION"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(reader["EMAIL"]))
                    {
                        candidateDetail.EMailID = reader["EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(reader["PHONE"]))
                    {
                        candidateDetail.Phone = reader["PHONE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(reader["SYNOPSIS"]))
                    {
                        candidateDetail.CandidateSynopsis = reader["SYNOPSIS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(reader["NOTES"]))
                    {
                        candidateDetail.CandidateNotes = reader["NOTES"].ToString().Trim();
                    }
                }
                return candidateDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }

            }
        }

        /// <summary>
        /// Method that inserts a credit request.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertCreditRequest(int userID)
        {
            // Create a stored procedure command object.
            DbCommand insertCreditRequestCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CREDIT_REQUEST");

            // Add input parameters.
            // Add user id 
            HCMDatabase.AddInParameter(insertCreditRequestCommand,
                "@USER_ID", DbType.Int32, userID);

            // Add credit id credit request type
            HCMDatabase.AddInParameter(insertCreditRequestCommand,
                "@CREDIT_ID", DbType.String, Constants.CreditRequestConstants.CANDIDATE_REQUEST);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertCreditRequestCommand);
        }

        /// <summary>
        /// Method that checks if a credit request is already exists for 
        /// an user.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public bool IsCreditRequestExist(int userID)
        {
            // Create a stored procedure command object.
            DbCommand creditRequestCommand = HCMDatabase.
                GetStoredProcCommand("SPGET_CREDIT_REQUEST_EXITS");

            // Add input parameters.
            HCMDatabase.AddInParameter(creditRequestCommand,
                "@USER_ID", DbType.Int32, userID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(creditRequestCommand,
                "@EXISTS", DbType.Int32, 0);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(creditRequestCommand);

            if (!(Utility.IsNullOrEmpty(creditRequestCommand.Parameters["@EXISTS"].Value)) &&
                Convert.ToInt32(creditRequestCommand.Parameters["@EXISTS"].Value.ToString()) == 1)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to get the candidate resume for the given candidate session id
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="int"/>that holds the candidate session id
        /// </param>
        /// <param name="mimeType">
        /// A <see cref="string"/> that holds the mime type as an output 
        /// parameter.
        /// </param>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the filename as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A<see cref="byte"/>that holds the resume data.
        /// </returns>
        public byte[] GetResumeContents(string candidateSessionID,
            out string mimeType, out string fileName)
        {
            mimeType = string.Empty;
            fileName = string.Empty;
            DbCommand command = HCMDatabase.GetStoredProcCommand
                ("SPGET_RESUME_SOURCE");
            HCMDatabase.AddInParameter(command,
                "@CAND_SESSION_KEY", DbType.String, candidateSessionID);
            IDataReader reader = HCMDatabase.ExecuteReader(command);
            if (reader != null && reader.Read())
            {
                mimeType = reader["MIME_TYPE"] as string;
                fileName = reader["FILE_NAME"] as string;
                return reader["RESUME_SOURCE"] as byte[];
            }
            return null;
        }
            public byte[] GetResumeContentsByCandidiateID(int candidateID,
            out string mimeType, out string fileName)
        {
            mimeType = string.Empty;
            fileName = string.Empty;
            DbCommand command = HCMDatabase.GetStoredProcCommand
                ("SPGET_RESUME_SOURCE_CAIID");
            HCMDatabase.AddInParameter(command,
                "@CANDIDATE_ID", DbType.Int32, candidateID);
        
            IDataReader reader = HCMDatabase.ExecuteReader(command);
            if (reader != null && reader.Read())
            {
                mimeType = reader["MIME_TYPE"] as string;
                fileName = reader["FILE_NAME"] as string;
                return reader["RESUME_SOURCE"] as byte[];
            }
            return null;
        }

         
        /// <summary>
        /// Method to get the candidate hrXml for the given candidate session id. 
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the candidate session id 
        /// </param>        
        /// <returns>
        /// A<see cref="string"/>that holds the hrXml source.
        /// </returns>
        public string GetCandidateHRXml(string candidateSessionID)
        {
            IDataReader dataReader = null;
            string hrXml = null;
            try
            {
                DbCommand getCandidateXMLCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_HRXML_BY_CANDIDATE_SESSION_KEY");
                HCMDatabase.AddInParameter(getCandidateXMLCommand,
                    "@CAND_SESSION_KEY", DbType.String, candidateSessionID);
                dataReader = HCMDatabase.ExecuteReader(getCandidateXMLCommand);
                if (dataReader != null && dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["HR_XML"]))
                        hrXml = (dataReader["HR_XML"].ToString());
                }
                return hrXml;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method to get the candidate hrXml for the given candidate id. 
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="string"/>that holds the candidate id 
        /// </param>        
        /// <returns>
        /// A<see cref="string"/>that holds the hrXml source.
        /// </returns>
        public string GetCandidateHRXLDetail(int candidateID)
        {
            IDataReader dataReader = null;
            string hrXml = null;
            try
            {
                DbCommand getCandidateXMLCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_HRXMLDETAIL");
                HCMDatabase.AddInParameter(getCandidateXMLCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateID);
                dataReader = HCMDatabase.ExecuteReader(getCandidateXMLCommand);
                if (dataReader != null && dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["HR_XML"]))
                        hrXml = (dataReader["HR_XML"].ToString());
                }
                return hrXml;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

         /// <summary>
        /// Method to get the candidate hrXml for the given candidate session id. 
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the candidate session id 
        /// </param>        
        /// <returns>
        /// A<see cref="string"/>that holds the hrXml source.
        /// </returns>
        public string GetInterviewCandidateHRXml(string candidateSessionID)
        {
            IDataReader dataReader = null;
            string hrXml = null;
            try
            {
                DbCommand getCandidateXMLCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_HRXML_BY_CANDIDATE_SESSION_KEY");
                HCMDatabase.AddInParameter(getCandidateXMLCommand,
                    "@CAND_SESSION_KEY", DbType.String, candidateSessionID);
                dataReader = HCMDatabase.ExecuteReader(getCandidateXMLCommand);
                if (dataReader != null && dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["HR_XML"]))
                        hrXml = (dataReader["HR_XML"].ToString());
                }
                return hrXml;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        
        /// <summary>
        /// Method that retrieves the validation objects.
        /// </summary>
        /// <param name="candidateid">
        /// An <see cref="string"/> that holds the specific candidate id.
        /// </param>
        ///  <param name="testID">
        /// An <see cref="string"/> that holds the specific test id.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the pending attempts as an output 
        /// parameter.
        /// </param>
        /// <param name="isCertificateTest">
        /// A <see cref="string"/> that holds is Certificate Test as an output 
        /// parameter.
        /// </param>
        /// <param name="retakeCount">
        /// A <see cref="string"/> that holds retake count as an output 
        /// parameter.
        /// </param>
        /// <param name="dicRetakeDetails">
        /// that holds the retake details as an output 
        /// parameter.
        /// </param>
        public string TestRetakeValidation(string candidateid, string testID, int attemptID,
             out string retakeCount,out string isCertificateTest, out Dictionary<string, string> dicRetakeDetails)
        {
            retakeCount = string.Empty;
            isCertificateTest = string.Empty;
            dicRetakeDetails = new Dictionary<string,string>();            

            DbCommand command = HCMDatabase.GetStoredProcCommand
                ("SPGET_REQUEST_TO_RETAKE_VALIDATION");
            HCMDatabase.AddInParameter(command,
                "@CAND_SESSION_KEY", DbType.String, candidateid);
            HCMDatabase.AddInParameter(command,
              "@TEST_KEY", DbType.String, testID);
            HCMDatabase.AddInParameter(command,
              "@ATTEMPT_ID", DbType.Int32, attemptID);
            IDataReader reader = HCMDatabase.ExecuteReader(command);
            if (reader != null && reader.Read())
            {
                if (!Utility.IsNullOrEmpty(reader["RETAKE_COUNT"]))
                    retakeCount = reader["RETAKE_COUNT"].ToString().Trim();

                if (reader.NextResult() && reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["RETAKE_DAYS_LEFT"])
                        && !Utility.IsNullOrEmpty(reader["COMPLETED_DATE"]))
                        dicRetakeDetails.Add(reader["RETAKE_DAYS_LEFT"].ToString().Trim(),
                           reader["COMPLETED_DATE"].ToString().Trim());
                }
                if (reader.NextResult() && reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["ISCERTIFICATE_TEST"]))
                        isCertificateTest = reader["ISCERTIFICATE_TEST"].ToString().Trim();
                }
                if (reader.NextResult() && reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["PENDING_ATTEMPTS"]))
                        return reader["PENDING_ATTEMPTS"].ToString().Trim();
                }               
            }

            return null;
        }

        /// <summary>
        /// Method that inserts the interview reminder.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="InterviewReminderDetail"/> that holds the reminder
        /// detail.
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void InsertInterviewReminder(InterviewReminderDetail reminderDetail, 
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_INTERVIEW_REMINDER");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertReminderCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, reminderDetail.CandidateSessionID);
            HCMDatabase.AddInParameter(insertReminderCommand,
                "@ATTEMPT_ID", DbType.Int16, reminderDetail.AttemptID);
            HCMDatabase.AddInParameter(insertReminderCommand,
               "@INTERVAL_ID", DbType.String, reminderDetail.IntervalID.ToString());
            HCMDatabase.AddInParameter(insertReminderCommand,
                "@SCHEDULED_DATE", DbType.DateTime, reminderDetail.ReminderDate);
            HCMDatabase.AddInParameter(insertReminderCommand,
               "@USER_ID", DbType.Int16, reminderDetail.UserID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertReminderCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that updates the interview reminder.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="InterviewReminderDetail"/> that holds the reminder
        /// detail.
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void UpdateInterviewReminder(InterviewReminderDetail reminderDetail, 
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand updateReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_INTERVIEW_REMINDER");
            // Add input parameters.
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, reminderDetail.CandidateSessionID);
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@ATTEMPT_ID", DbType.Int16, reminderDetail.AttemptID);
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@INTERVAL_ID", DbType.String, reminderDetail.IntervalID.ToString());
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@SCHEDULED_DATE", DbType.DateTime, reminderDetail.ReminderDate);
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@USER_ID", DbType.Int16, reminderDetail.UserID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateReminderCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that deletes the interview reminder.
        /// </summary>
        /// <param name="reminderDetail">
        /// A <see cref="InterviewReminderDetail"/> that holds the reminder
        /// detail.
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void DeleteInterviewReminder(InterviewReminderDetail reminderDetail, 
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_INTERVIEW_REMINDER");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, reminderDetail.CandidateSessionID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@INTERVAL_ID", DbType.String, reminderDetail.IntervalID.ToString());
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ATTEMPT_ID", DbType.Int16, reminderDetail.AttemptID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the interview reminder detail for the given 
        /// candidate session ID and attempt ID.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID>
        /// </param>
        /// <param name="attemptID"> 
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewReminderDetail"/> that holds the interview
        /// reminder detail.
        /// </returns>
        public InterviewReminderDetail GetInterviewReminder
            (string candidateSessionID, int attemptID)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_REMINDER_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@ATTEMPT_ID", DbType.Int16, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                InterviewReminderDetail interviewReminder = null;

                if (dataReader.Read())
                {
                    // Instantiate the interview reminder object.
                    interviewReminder = new InterviewReminderDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        interviewReminder.InterviewID = dataReader["INTERVIEW_TEST_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        interviewReminder.InterviewName = dataReader["INTERVIEW_TEST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                        interviewReminder.InterviewDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                        interviewReminder.ExpiryDate = Convert.ToDateTime(dataReader["SCHEDULED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["REMINDER_DATE"]))
                        interviewReminder.ReminderDate = Convert.ToDateTime(dataReader["REMINDER_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        interviewReminder.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        interviewReminder.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString();
                }

                dataReader.NextResult();
                while (dataReader.Read())
                {
                    // Instantiate the attributes list.
                    if (interviewReminder.AttributeList == null)
                        interviewReminder.AttributeList = new List<string>();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVAL_ID"]))
                    {
                        interviewReminder.AttributeList.Add(dataReader["INTERVAL_ID"].ToString().Trim());
                    }

                }
                return interviewReminder;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that deletes all interview reminders.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds attempt ID.
        /// </param>
        public void DeleteAllInterviewReminders(string candidateSessionID, int attemptID)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ALL_INTERVIEW_REMINDERS");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@ATTEMPT_ID", DbType.Int32, attemptID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand);
        }

        /// <summary>
        /// Method that retrieves the pending interviews.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="string"/> that holds the candidate ID.
        /// </param>
        /// <param name="orderByColumn">
        /// A <see cref="string"/> that holds the order by column
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInterviewDetail"/> that holds the 
        /// pending interview details.
        /// </returns>
        public List<CandidateInterviewDetail> GetPendingInterviews(int candidateID, 
            string orderByColumn, SortType orderByDirection, int pageNumber, 
            int pageSize, out int totalRecords)
        {
            if (Utility.IsNullOrEmpty(orderByColumn))
                throw new Exception("Order by column parameter cannot be empty");

            if (Utility.IsNullOrEmpty(orderByDirection))
                throw new Exception("Order by direction parameter cannot be empty");

            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getInterviewsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_PENDING_INTERVIEWS");

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@ORDER_BY",
                    DbType.String, orderByColumn);

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@ORDER_BY_DIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getInterviewsCommand);

                List<CandidateInterviewDetail> pendingInterviews = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (pendingInterviews == null)
                            pendingInterviews = new List<CandidateInterviewDetail>();

                        CandidateInterviewDetail pendingInterview = new CandidateInterviewDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                        {
                            pendingInterview.RowNumber = Convert.ToInt32(dataReader["ROWNUMBER"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                        {
                            pendingInterview.InterviewSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                        {
                            pendingInterview.CandidateSessionID = dataReader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        {
                            pendingInterview.InterviewID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        {
                            pendingInterview.InterviewName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                        {
                            pendingInterview.InterviewDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                        }
                        
                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY_USER_ID"]))
                        {
                            pendingInterview.InitiatedByID = Convert.ToInt32(dataReader["INITIATED_BY_USER_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            pendingInterview.InitiatedBy = dataReader["INITIATED_BY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            pendingInterview.InterviewAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_ON"]))
                        {
                            pendingInterview.InitiatedOn = Convert.ToDateTime(dataReader["INITIATED_ON"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            pendingInterview.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            pendingInterview.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                        {
                            if (dataReader["STATUS"].ToString().Trim().ToUpper() == "ATMPT_PAUS")
                                pendingInterview.IsPaused = true;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            pendingInterview.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        {
                            pendingInterview.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRED"]))
                        {
                            pendingInterview.IsExpired = dataReader["EXPIRED"].ToString().Trim().ToUpper() == "Y" ? true : false;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_MESSAGE"]))
                        {
                            pendingInterview.ScheduledByMessage = dataReader["SCHEDULED_BY_MESSAGE"].ToString().Trim();
                        }

                        if (candidateID == pendingInterview.SchedulerID)
                            pendingInterview.IsSelfAdminTest = true;

                        // Add to the list.
                        pendingInterviews.Add(pendingInterview);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return pendingInterviews;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the completed interviews.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="string"/> that holds the candidate ID.
        /// </param>
        /// <param name="orderByColumn">
        /// A <see cref="string"/> that holds the order by column
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page Number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page Size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total Records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInterviewDetail"/> that holds the 
        /// completed interview details.
        /// </returns>
        public List<CandidateInterviewDetail> GetCompletedInterviews(int candidateID,
            string orderByColumn, SortType orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {
            if (Utility.IsNullOrEmpty(orderByColumn))
                throw new Exception("Order by column parameter cannot be empty");

            if (Utility.IsNullOrEmpty(orderByDirection))
                throw new Exception("Order by direction parameter cannot be empty");

            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getInterviewsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_COMPLETED_INTERVIEWS");

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@ORDER_BY",
                    DbType.String, orderByColumn);

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@ORDER_BY_DIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getInterviewsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getInterviewsCommand);

                List<CandidateInterviewDetail> completedInterviews = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (completedInterviews == null)
                            completedInterviews = new List<CandidateInterviewDetail>();

                        CandidateInterviewDetail completedInterview = new CandidateInterviewDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                        {
                            completedInterview.RowNumber = Convert.ToInt32(dataReader["ROWNUMBER"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                        {
                            completedInterview.InterviewSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                        {
                            completedInterview.CandidateSessionID = dataReader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        {
                            completedInterview.InterviewID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        {
                            completedInterview.InterviewName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                        {
                            completedInterview.InterviewDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY_USER_ID"]))
                        {
                            completedInterview.InitiatedByID = Convert.ToInt32(dataReader["INITIATED_BY_USER_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            completedInterview.InitiatedBy = dataReader["INITIATED_BY"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                        {
                            completedInterview.InterviewAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_ON"]))
                        {
                            completedInterview.InitiatedOn = Convert.ToDateTime(dataReader["INITIATED_ON"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        {
                            completedInterview.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        {
                            completedInterview.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                        {
                            completedInterview.CompletedOn = Convert.ToDateTime(dataReader["COMPLETED_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                        {
                            if (dataReader["STATUS"].ToString().Trim().ToUpper() == "ATMPT_PAUS")
                                completedInterview.IsPaused = true;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            completedInterview.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        {
                            completedInterview.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_MESSAGE"]))
                        {
                            completedInterview.ScheduledByMessage = dataReader["SCHEDULED_BY_MESSAGE"].ToString().Trim();
                        }

                        if (candidateID == completedInterview.SchedulerID)
                            completedInterview.IsSelfAdminTest = true;

                        // Add to the list.
                        completedInterviews.Add(completedInterview);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return completedInterviews;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that update the user profile details such as first name, 
        /// last name, email.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </param>
        public void UpdateUserProfile(UserDetail userDetail)
        {
            // Create a stored procedure command object.
            DbCommand updateProfileCommand = HCMDatabase.
                 GetStoredProcCommand("SPUPDATE_USER_PROFILE");

            HCMDatabase.AddInParameter(updateProfileCommand,
                "@USER_ID", DbType.Int32, userDetail.UserID);

            HCMDatabase.AddInParameter(updateProfileCommand,
                "@ALT_EMAIL", DbType.String, userDetail.AltEmail);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateProfileCommand);
        }

        /// <summary>
        /// Method that deletes the candidate resume.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        public void DeleteCandidateResume(int candidateResumeID)
        {
            // Create a stored procedure command object.
            DbCommand updateProfileCommand = HCMDatabase.
                 GetStoredProcCommand("SPDELETE_CANDIDATE_RESUME_DETAIL");

            HCMDatabase.AddInParameter(updateProfileCommand,
                "@CAND_RESUME_ID", DbType.Int32, candidateResumeID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateProfileCommand);
        }

        /// <summary>
        /// Method that retrieves the candidate test recommendation detail.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="testRecommendationID">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestDetail"/> that holds the test recommendation detail.
        /// </returns>
        public TestDetail GetCandidateTestRecommendationDetail(int userID, int testRecommendationID)
        {
            IDataReader dataReader = null;
            TestDetail testDetail = null;

            try
            {
                DbCommand getTestDetail = HCMDatabase.GetStoredProcCommand
                    ("SPGET_CANDIDATE_TEST_RECOMMENDATION_DETAIL");

                HCMDatabase.AddInParameter(getTestDetail, "@USER_ID",
                    DbType.Int32, userID);

                HCMDatabase.AddInParameter(getTestDetail, "@TEST_RECOMMEND_ID",
                    DbType.Int32, testRecommendationID);

                dataReader = HCMDatabase.ExecuteReader(getTestDetail);

                if (dataReader.Read())
                {
                    // Instantiate test introduction detail object.
                    testDetail = new TestDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                    {
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        testDetail.Status = dataReader["STATUS"].ToString().Trim();
                    }
                }
                return testDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the candidate test recommendation status.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="testRecommendationID">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status. The possible statuses are:
        /// SAT_COMP, SAT_SAVD, SAT_SCHD.
        /// </param>
        public void UpdateCandidateTestRecommendationStatus(int userID, int testRecommendationID,
            string testKey, string status)
        {
            // Create a stored procedure command object.
            DbCommand updateStatusCommand = HCMDatabase.
                 GetStoredProcCommand("SPUPDATE_CANDIDATE_TEST_RECOMMENDATION_STATUS");

            updateStatusCommand.CommandTimeout = 0;

            HCMDatabase.AddInParameter(updateStatusCommand, "@USER_ID",
                    DbType.Int32, userID);

            HCMDatabase.AddInParameter(updateStatusCommand, "@TEST_RECOMMEND_ID",
                DbType.Int32, testRecommendationID);

            HCMDatabase.AddInParameter(updateStatusCommand, "@TEST_KEY",
                    DbType.String, testKey);

            HCMDatabase.AddInParameter(updateStatusCommand, "@STATUS",
                DbType.String, status);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateStatusCommand);

        }

        /// <summary>
        /// Method that deletes the candidate recommended test.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="testRecommendationID">
        /// A <see cref="int"/> that holds the test recommendation ID.
        /// </param>
        public void DeleteCandidateRecommendedTest(int userID, int testRecommendationID)
        {
            // Create a stored procedure command object.
            DbCommand deleteTestCommand = HCMDatabase.
                 GetStoredProcCommand("SPDELETE_CANDIDATE_RECOMMENDED_TEST");

            HCMDatabase.AddInParameter(deleteTestCommand,
                "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddInParameter(deleteTestCommand,
                "@TEST_RECOMMEND_ID", DbType.Int32, testRecommendationID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteTestCommand);
        }

        /// <summary>
        /// Method that retrieves the count of saved and completed self admin
        /// test for the given month and year.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the candidate user ID.
        /// </param>
        /// <param name="month">
        /// A <see cref="int"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the count.
        /// </returns>
        public int GetSelfAdminTestCount(int userID, int month, int year)
        {
            int count = 0;

            // Create commad object.
            DbCommand getCount = HCMDatabase.GetStoredProcCommand
                ("SPGET_CANDIDATE_SELF_ADMIN_TEST_COUNT");

            // Add input parameters.
            HCMDatabase.AddInParameter(getCount, "@USER_ID", DbType.Int32, userID);
            HCMDatabase.AddInParameter(getCount, "@MONTH", DbType.Int32, month);
            HCMDatabase.AddInParameter(getCount, "@YEAR", DbType.Int32, year);

            object value = HCMDatabase.ExecuteScalar(getCount);
            if (!Utility.IsNullOrEmpty(value))
                count = int.Parse(value.ToString().Trim());

            return count;
        }

        /// <summary>
        /// Method that inserts the resume reminder detail.
        /// </summary>
        /// <param name="resumeReminderDetail">
        /// A <see cref="ResumeReminderDetail"/> that holds the resume reminder 
        /// detail.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the status. 0 indicates reminder
        /// created successfully and 1 indicates already a reminder available 
        /// for the same reminder date.
        /// </returns>
        public int InsertResumeReminder(ResumeReminderDetail resumeReminderDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_RESUME_REMINDER_SCHEDULER");

            HCMDatabase.AddInParameter(insertReminderCommand,
                "@EMAIL_ID", DbType.String, resumeReminderDetail.EmailID);

            HCMDatabase.AddInParameter(insertReminderCommand,
                "@REMINDER_DATE", DbType.DateTime, resumeReminderDetail.ReminderDate);

            HCMDatabase.AddInParameter(insertReminderCommand,
                "@USER_ID", DbType.Int32, resumeReminderDetail.UserID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(insertReminderCommand, "@STATUS", DbType.Int32, 4);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertReminderCommand);

            // Retrieve the output parameter value.
            if (insertReminderCommand.Parameters["@STATUS"].Value != null &&
                insertReminderCommand.Parameters["@STATUS"].Value != DBNull.Value)
            {
                return int.Parse(insertReminderCommand.Parameters["@STATUS"].Value.ToString());
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Method that retrieves the list of resume reminder sender list 
        /// for the given date/time. Mails needs to be sent for this list.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// <returns>
        /// A list of <see cref="InterviewReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<ResumeReminderDetail> GetResumeReminderSenderList(DateTime currentDateTime)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getSenderList = HCMDatabase.
                    GetStoredProcCommand("SPGET_RESUME_REMINDER_SENDER_LIST");

                // Add input parameters.
                HCMDatabase.AddInParameter(getSenderList,
                    "@CURRENT_DATE_TIME", DbType.String, currentDateTime);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getSenderList);

                List<ResumeReminderDetail> senderList = null;

                while (dataReader.Read())
                {
                    if (senderList == null)
                        senderList = new List<ResumeReminderDetail>();

                    ResumeReminderDetail senderDetail = new ResumeReminderDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        senderDetail.ID = Convert.ToInt32(dataReader["ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        senderDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        senderDetail.EmailID = dataReader["EMAIL_ID"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["REMINDER_DATE"]))
                        senderDetail.ReminderDate = Convert.ToDateTime(dataReader["REMINDER_DATE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                        senderDetail.UserName = dataReader["USER_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["PASSWORD"]))
                        senderDetail.Password = dataReader["PASSWORD"].ToString().Trim();

                    // Add the detail to the list.
                    senderList.Add(senderDetail);
                }
                return senderList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will update the resume reminder sent status for the ID.
        /// </summary>
        /// <param name="genID">
        /// A <see cref="int"/> that holds the gen ID.
        /// </param>
        /// <param name="reminderSent">
        /// A <see cref="bool"/> that holds the reminder sent status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateResumeReminderStatus(int genID, bool reminderSent, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_RESUME_REMINDER_SENT_STATUS");
            //Add input parameters.
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@ID", DbType.Int32, genID);
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@REMINDER_SENT", DbType.String, reminderSent == true ? "Y" : "N");
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateReminderCommand);
        }

        /// <summary>
        /// Method that creates a candidate record in the original table, and 
        /// associate the records from the temporatory table. This will also 
        /// remove the record from the temporary table.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID which refers to the
        /// temporary table.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the candidate ID which refers to the
        /// newly created record in the original table.
        /// </returns>
        public int CreateCandidate(int candidateID)
        { 
            // Construct stored procedure command.
            DbCommand insertCandidate = HCMDatabase.
                GetStoredProcCommand("SPINSERT_TRANSFER_CANDIDATE_EXTERNAL_DATA_STORE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCandidate,
                "@CANDIDATE_ID", DbType.Int32, candidateID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(insertCandidate,
                "@NEW_CANDIDATE_ID", DbType.Int32, 0);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertCandidate);

            // Retrieve the new candidate ID from the output parameter.
            int newCandidateID = 0;
            if (insertCandidate.Parameters["@NEW_CANDIDATE_ID"].Value != null)
            {
                // Retrieve and assign candidate ID.
                newCandidateID = int.Parse(insertCandidate.Parameters["@NEW_CANDIDATE_ID"].Value.ToString());
            }

            return newCandidateID;
        }


        public void CreateCandidateNewResume(int temporaryCandiateID,int candidateID)
        { 
             // Construct stored procedure command.
            DbCommand insertCandidate = HCMDatabase.
                GetStoredProcCommand("SPINSERT_TRANSFER_CANDIDATE_EXTERNAL_RESUME");

            // Add output parameters.
            HCMDatabase.AddInParameter(insertCandidate,
                "@TEMPORARY_CANDIDATE_ID", DbType.Int32, temporaryCandiateID);


            // Add input parameters.
            HCMDatabase.AddInParameter(insertCandidate,
                "@CANDIDATE_ID", DbType.Int32, candidateID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertCandidate); 
        }

        /// <summary>
        /// Retrive the candidateid
        /// </summary>
        /// <param name="prasUserID"></param>
        /// <returns></returns>
        public int GetCandidateID(int prasUserID, string sessionKey)
        {
            IDataReader reader = null;

            int candidateID = 0;
            try
            {
                DbCommand getCandidateIDCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATEID_FOR_DASHBOARD");

                HCMDatabase.AddInParameter(getCandidateIDCommand, "@PRAS_USER_ID", DbType.Int32, prasUserID);

                if (!Utility.IsNullOrEmpty(sessionKey))
                    HCMDatabase.AddInParameter(getCandidateIDCommand, "@CANDSESSIONKEY", DbType.String, sessionKey);

                reader = HCMDatabase.ExecuteReader(getCandidateIDCommand);

                while (reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["CANDIDATE_ID"]))
                    {
                        candidateID = Convert.ToInt32( reader["CANDIDATE_ID"] );
                    }
                }
                return candidateID;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }

            }
        }

        /// <summary>
        /// Method that retrieves the daily test reminder list for mailing to 
        /// candidates.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CandidateTestSessionDetail"/> that holds the mailing 
        /// list.
        /// </returns>
        public List<CandidateTestSessionDetail> GetDailyTestReminders()
        {
            List<CandidateTestSessionDetail> reminders = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_DAILY_TEST_REMINDERS");

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                while (dataReader.Read())
                {
                    // Instantiate the reminder list object.
                    if (reminders == null)
                        reminders = new List<CandidateTestSessionDetail>();
 
                    // Create a reminder object.
                    CandidateTestSessionDetail reminder = new CandidateTestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_GENID"]))
                    {
                        reminder.GenID = Convert.ToInt32(dataReader["CANDIDATE_SESSION_GENID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        reminder.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                    {
                        reminder.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        reminder.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FIRST_NAME"]))
                    {
                        reminder.CandidateFirstName = dataReader["CANDIDATE_FIRST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        reminder.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                    {
                        reminder.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_OWNER_EMAIL"]))
                    {
                        reminder.CandidateOwnerEmail = dataReader["CANDIDATE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                    {
                        reminder.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["NAVIGATE_KEY"]))
                    {
                        reminder.NavigateKey = dataReader["NAVIGATE_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_EMAIL"]))
                    {
                        reminder.SchedulerEmail = dataReader["SCHEDULER_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_FIRST_NAME"]))
                    {
                        reminder.SchedulerFirstName = dataReader["SCHEDULER_FIRST_NAME"].ToString().Trim();
                        reminder.SchedulerName = dataReader["SCHEDULER_FIRST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_LAST_NAME"]))
                    {
                        reminder.SchedulerLastName = dataReader["SCHEDULER_LAST_NAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(reminder.SchedulerName))
                            reminder.SchedulerName = reminder.SchedulerName + ' ' + reminder.SchedulerLastName ;
                        else
                            reminder.SchedulerName = reminder.SchedulerLastName;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_COMPANY"]))
                    {
                        reminder.SchedulerCompany = dataReader["SCHEDULER_COMPANY"].ToString().Trim();
                    }
                    
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_EMAIL"]))
                    {
                        reminder.PositionProfileOwnerEmail = dataReader["POSITION_PROFILE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        reminder.Status = dataReader["STATUS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_ATTEMPT"]))
                    {
                        reminder.NoOfAttempt = Convert.ToInt16(dataReader["NO_OF_ATTEMPT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        reminder.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                    {
                        reminder.TestID = dataReader["TEST_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        reminder.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAVIGATE_KEY"]))
                    {
                        reminder.NavigateKey = dataReader["NAVIGATE_KEY"].ToString().Trim();
                    }

                    // Add to reminder list.
                    reminders.Add(reminder);
                }

                return reminders;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the daily interview reminder list for mailing to 
        /// candidates.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CandidateInterviewSessionDetail"/> that holds the mailing 
        /// list.
        /// </returns>
        public List<CandidateInterviewSessionDetail> GetDailyInterviewReminders()
        {
            List<CandidateInterviewSessionDetail> reminders = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_DAILY_INTERVIEW_REMINDERS");

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                while (dataReader.Read())
                {
                    // Instantiate the reminder list object.
                    if (reminders == null)
                        reminders = new List<CandidateInterviewSessionDetail>();

                    // Create a reminder object.
                    CandidateInterviewSessionDetail reminder = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_ID"]))
                    {
                        reminder.GenID = Convert.ToInt32(dataReader["CAND_INTERVIEW_SESSION_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                    {
                        reminder.CandidateTestSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                    {
                        reminder.InterviewTestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        reminder.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["AUTH_CODE"]))
                    {
                        reminder.SecurityCode = dataReader["AUTH_CODE"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FIRST_NAME"]))
                    {
                        reminder.CandidateFirstName = dataReader["CANDIDATE_FIRST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        reminder.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                    {
                        reminder.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_OWNER_EMAIL"]))
                    {
                        reminder.CandidateOwnerEmail = dataReader["CANDIDATE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_EMAIL"]))
                    {
                        reminder.SchedulerEmail = dataReader["SCHEDULER_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_FIRST_NAME"]))
                    {
                        reminder.SchedulerFirstName = dataReader["SCHEDULER_FIRST_NAME"].ToString().Trim();
                        reminder.SchedulerName = dataReader["SCHEDULER_FIRST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_LAST_NAME"]))
                    {
                        reminder.SchedulerLastName = dataReader["SCHEDULER_LAST_NAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(reminder.SchedulerName))
                            reminder.SchedulerName = reminder.SchedulerName + ' ' + reminder.SchedulerLastName;
                        else
                            reminder.SchedulerName = reminder.SchedulerLastName;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_COMPANY"]))
                    {
                        reminder.SchedulerCompany = dataReader["SCHEDULER_COMPANY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_EMAIL"]))
                    {
                        reminder.PositionProfileOwnerEmail = dataReader["POSITION_PROFILE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_EMAILS"]))
                    {
                        reminder.AssessorEmails = dataReader["ASSESSOR_EMAILS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        reminder.Status = dataReader["STATUS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_ATTEMPT"]))
                    {
                        reminder.NoOfAttempt = Convert.ToInt16(dataReader["NO_OF_ATTEMPT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAVIGATE_KEY"]))
                    {
                        reminder.NavigateKey = dataReader["NAVIGATE_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        reminder.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                    {
                        reminder.InterviewTestID = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                    {
                        reminder.InterviewTestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    }

                    // Add to reminder list.
                    reminders.Add(reminder);
                }

                return reminders;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method that delete a candidate record  
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID  
        /// </param>
        /// <returns>
        /// A <see cref="int"/>transactions record count.
        /// </returns>
        public int DeleteCandidate(int candidateID)
        {
            // Construct stored procedure command.
            DbCommand deleteCandidate = HCMDatabase.
                GetStoredProcCommand("SPDELETE_CANDIATE_INFORMATION");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteCandidate,
                "@CANDIDATE_ID", DbType.Int32, candidateID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(deleteCandidate,
                "@TRANSACTION_COUNT", DbType.Int32, 0);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteCandidate);

            // Retrieve the new candidate ID from the output parameter.
            int transCount = 0;
            if (deleteCandidate.Parameters["@TRANSACTION_COUNT"].Value != null)
            {
                // Retrieve and assign candidate ID.
                transCount = int.Parse(deleteCandidate.Parameters["@TRANSACTION_COUNT"].Value.ToString());
            }

            return transCount;
        }

        /// <summary>
        /// Method that retrives the additional documents details for
        /// the selected candidates associated with position profile ids
        /// </summary>
        /// <param name="candidateIds"></param>
        /// <param name="positionProfileId"></param>
        /// <returns></returns>
        public List<CareerBuilderResume> GetAdditionalDocument(string candidateIds, int positionProfileId)
        {
            IDataReader dataReader = null;

            List<CareerBuilderResume> additionalDocs = null;

            try
            {
                DbCommand additionalDocsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ADDITIONAL_DOCUMENTS");

                HCMDatabase.AddInParameter(additionalDocsCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);

                HCMDatabase.AddInParameter(additionalDocsCommand,
                    "@CANDIDIATE_IDS", DbType.String, candidateIds);

                dataReader = HCMDatabase.ExecuteReader(additionalDocsCommand);

                while (dataReader.Read())
                {
                    if (additionalDocs == null)
                        additionalDocs = new List<CareerBuilderResume>();

                    CareerBuilderResume careerBuilderResume = new CareerBuilderResume();

                    if (!Utility.IsNullOrEmpty(dataReader["DOCUMENT_ID"]))
                        careerBuilderResume.ResumeID = dataReader["DOCUMENT_ID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["FILE_NAME"]))
                        careerBuilderResume.ResumeTitle = dataReader["FILE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["caifirstName"]))
                        careerBuilderResume.CandidateFirstName = dataReader["caifirstName"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["caiLastName"]))
                        careerBuilderResume.CandidateLastName = dataReader["caiLastName"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        careerBuilderResume.CandidateID = dataReader["CANDIDATE_ID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["DOCUMENT_SOURCE"]))
                    {
                        careerBuilderResume.ResumeContent = dataReader["DOCUMENT_SOURCE"] as byte[];
                        careerBuilderResume.ISDocument = true;
                    }
                    else
                    {
                        careerBuilderResume.ISDocument = false;
                    }
                    additionalDocs.Add(careerBuilderResume);
                }
                return additionalDocs;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }


        /// <summary>
        /// Method that retrives the additional document contents
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public byte[] GetDocumentContent(int DocumentId, out string fileName)
        {

            fileName = string.Empty;
            DbCommand command = HCMDatabase.GetStoredProcCommand
                ("SPGET_DOCUMENTCONTENTS");
            HCMDatabase.AddInParameter(command,
                "@DOCUMENT_ID", DbType.Int32, DocumentId);
            IDataReader reader = HCMDatabase.ExecuteReader(command);
            if (reader != null && reader.Read())
            {
                fileName = reader["FILE_NAME"] as string;
                return reader["DOCUMENT_SOURCE"] as byte[];
            }
            return null;
        }

        public OnlineCandidateSessionDetail GetOnlineInterviewDetail(int candidateId,int userId,string chatRoomId)
        {
            IDataReader dataReader = null;

            OnlineCandidateSessionDetail onlineInterviewDetail = null;

            try
            {
                DbCommand onlineInterviewDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_DATETIME");

                if(candidateId>0)
                    HCMDatabase.AddInParameter(onlineInterviewDetailCommand,
                        "@CANDIDATE_ID ", DbType.Int32, candidateId);

                if (userId > 0)
                    HCMDatabase.AddInParameter(onlineInterviewDetailCommand,
                        "@ASSESSOR_ID ", DbType.Int32, userId);

                HCMDatabase.AddInParameter(onlineInterviewDetailCommand,
                        "@CHATROOM_ID ", DbType.String, chatRoomId);

                dataReader = HCMDatabase.ExecuteReader(onlineInterviewDetailCommand);

                while (dataReader.Read())
                {

                    onlineInterviewDetail = new OnlineCandidateSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_KEY"]))
                        onlineInterviewDetail.CandidateSessionID = dataReader["ONLINE_INTERVIEW_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ONLINE_INTERVIEW_DATE"]))
                        onlineInterviewDetail.InterviewDate =Convert.ToDateTime(dataReader["ONLINE_INTERVIEW_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CHAT_ROOM_NAME"]))
                        onlineInterviewDetail.ChatRoomName = dataReader["CHAT_ROOM_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_END_TIME"]))
                        onlineInterviewDetail.TimeSlotTo = dataReader["INTERVIEW_END_TIME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_START_TIME"]))
                        onlineInterviewDetail.TimeSlotFrom= dataReader["INTERVIEW_START_TIME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        onlineInterviewDetail.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString()); 
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                    {
                        onlineInterviewDetail.EmailId = dataReader["USER_NAME"].ToString();
                    } 
                }
                return onlineInterviewDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        #endregion Public Methods

        #region Signup Methods                                                 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="signupCandidate"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int InsertSignupCandidateSkill(
            SignupCandidate signupCandidate, IDbTransaction transaction)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand insertCandidateSkill = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_SIGNUPCANDIDATESKILLS");

                // Add input parameters
                HCMDatabase.AddInParameter(insertCandidateSkill,
                    "@CANDIDATE_ID", DbType.Int32, signupCandidate.CandidateID);

                HCMDatabase.AddInParameter(insertCandidateSkill,
                    "@CANDIDATE_SKILL", DbType.String, signupCandidate.CandidateSkill);

                HCMDatabase.AddInParameter(insertCandidateSkill,
                    "@EXPERIENCEYEARS", DbType.String, signupCandidate.ExperienceYears);

                HCMDatabase.AddInParameter(insertCandidateSkill,
                    "@LASTUSED", DbType.String, signupCandidate.LastUsed);

                HCMDatabase.AddInParameter(insertCandidateSkill,
                    "@SKILLTYPE", DbType.String, signupCandidate.SkillType);

                HCMDatabase.AddInParameter(insertCandidateSkill,
                    "@CREATED_BY", DbType.Int32, signupCandidate.CandidateCreatedBy);

                // Execute command
                int TransID = Convert.ToInt32(HCMDatabase.
                    ExecuteScalar(insertCandidateSkill, transaction as DbTransaction));

                return TransID;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CandidateID"></param>
        /// <param name="SkillID"></param>
        /// <returns></returns>
        public int DeleteCandidateSkill(int CandidateID, int SkillID
            , IDbTransaction transaction)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand insertCandidateSkill = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_SIGNUPCANDIDATESKILLS");

                // Add input parameters
                HCMDatabase.AddInParameter(insertCandidateSkill,
                    "@CANDIDATE_ID", DbType.Int32, CandidateID);

                HCMDatabase.AddInParameter(insertCandidateSkill,
                    "@CAND_SKILL_ID", DbType.Int32, SkillID);

                // Execute command
                int ResultID = Convert.ToInt32(HCMDatabase.
                    ExecuteScalar(insertCandidateSkill, transaction as DbTransaction));
                return ResultID;
            }
            catch { return 0; }
        }

        public List<SignupCandidate> GetCandidateSkill(int candidateID)
        {
            List<SignupCandidate> listCandidateSkill = null;
            IDataReader dataReader = null;
            try
            {
                DbCommand getDegreeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_SIGNUP_CANDIDATE_SKILLS");

                HCMDatabase.AddInParameter(getDegreeCommand, "@CANDIDATE_ID",
                    DbType.Int32, candidateID);
                dataReader = HCMDatabase.ExecuteReader(getDegreeCommand);

                int SlNo = 1;
                while (dataReader.Read())
                {
                    if (listCandidateSkill == null)
                        listCandidateSkill = new List<SignupCandidate>();

                    SignupCandidate ListElements = new SignupCandidate();
                    if (!Utility.IsNullOrEmpty(dataReader["CAND_SKILL_ID"]))
                    {
                        ListElements.SkillID = Convert.ToInt32
                            (dataReader["CAND_SKILL_ID"].ToString().Trim());

                        ListElements.Skill = Convert.ToString
                            (dataReader["SKILL_DETAIL"].ToString().Trim());

                        ListElements.ExperienceYears = Convert.ToString
                            (dataReader["EXP_IN_YEARS"].ToString().Trim());

                        ListElements.LastUsed = Convert.ToString
                            (dataReader["LAST_USED"].ToString().Trim());

                        ListElements.SlNo = SlNo;

                        // Add to the list.
                        listCandidateSkill.Add(ListElements);
                        SlNo += 1;
                    }
                }

                return listCandidateSkill;
            }
            catch { }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

            return listCandidateSkill;
        }

        public bool VerifyOpenEmailID(string userName)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand getOpenEmailIDUser = HCMDatabase.
                    GetStoredProcCommand("SPGET_VERIFYOPENEMAILID");

                // Add input parameters
                HCMDatabase.AddInParameter(getOpenEmailIDUser,
                    "@USERNAME", DbType.String, userName);

                // Execute command
                return Convert.ToBoolean(HCMDatabase.
                    ExecuteScalar(getOpenEmailIDUser));
            }
            catch { return false; }
        }

        /// <summary>
        /// Gets skill for candidate .
        /// </summary>
        /// <param name="prefixKeyword">The prefix keyword.</param>
        /// <returns></returns>
        public string[] GetCandidateSearchSkill(string prefixKeyword)
        {
            IDataReader dataReader = null;
            try
            {
                List<string> candidateSkills = null;
                DbCommand getCandidateSkill = HCMDatabase.GetStoredProcCommand("SPGET_SKILL_KEYWORDS");
                HCMDatabase.AddInParameter(getCandidateSkill, "@PREFIX_TEXT", DbType.String, prefixKeyword);

                dataReader = HCMDatabase.ExecuteReader(getCandidateSkill);
                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        if (candidateSkills == null)
                            candidateSkills = new List<string>();
                        candidateSkills.Add(dataReader["SKILL_NAME"].ToString());
                    }
                    dataReader.Close();
                }
                if (!Support.Utility.IsNullOrEmpty(candidateSkills) && candidateSkills.Count > 0)
                    return candidateSkills.ToArray();
                else
                    return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public CandidateSummary GetCandidateSelfTestDetail(int candidateID)
        {
            if (candidateID <= 0)
                throw new Exception("Candidate ID parameter zero or less than zero");

            IDataReader dataReader = null;
            CandidateSummary summary = null;
            try
            {
                DbCommand getPageControlsCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATETESTKEY");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATEID", DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);
                #region MyTests
                CandidateTestSessionDetail myTests = null;

                while (dataReader.Read())
                {
                    // Instantiate candidate summary object.
                    if (summary == null)
                        summary = new CandidateSummary();

                    // Instantiate pending tests list object.
                    if (summary.MyTests == null)
                        summary.MyTests = new List<CandidateTestSessionDetail>();

                    // Instantiate candidate test session detail object.
                    myTests = new CandidateTestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        myTests.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESC"]))
                    {
                        myTests.TestDescription = dataReader["TEST_DESC"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_STATUS"]))
                    {
                        myTests.Status = dataReader["TEST_STATUS"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                    {
                        myTests.TestID = dataReader["TEST_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_ID"]))
                    {
                        myTests.TestSessionID = dataReader["TEST_RECOMMEND_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TIME"]))
                    {
                        myTests.TimeLimit = Convert.ToInt32(dataReader["TEST_RECOMMEND_TIME"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                    {
                        myTests.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                    }
                    // Add to the list.
                    summary.MyTests.Add(myTests);
                }
                return summary;
                #endregion
            }
            catch { return null; }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate resume status.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="ResumeStatus"/> that holds the resume status.
        /// </returns>
        public ResumeStatus GetCandidateResumeStatus(int userID)
        {
            IDataReader dataReader = null;

            try
            {
                ResumeStatus resumeStatus = null;

                DbCommand getCandidateResumeStatus = HCMDatabase.
                    GetStoredProcCommand("SPGET_UPLOADED_RESUME_STATUS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateResumeStatus, "@USER_ID",
                    DbType.Int32, userID);

                // Execute the query.
                dataReader = HCMDatabase.ExecuteReader(getCandidateResumeStatus);

                // Check if record exist.
                if (dataReader.Read())
                {
                    // Instantiate the resume status object. 
                    resumeStatus = new ResumeStatus();

                    if (!Utility.IsNullOrEmpty(dataReader["DAYS_OLD"]))
                        resumeStatus.DaysOld = Convert.ToInt32(dataReader["DAYS_OLD"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["APPROVED"]))
                    {
                        resumeStatus.Approved = dataReader["APPROVED"].ToString().Trim().
                            ToUpper() == "Y" ? true : false;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["UPLOADED"]))
                    {
                        resumeStatus.Uploaded = dataReader["UPLOADED"].ToString().Trim().
                            ToUpper() == "Y" ? true : false;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["RESUME_NAME"]))
                    {
                        resumeStatus.ResumeName = dataReader["RESUME_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_RESUME_ID"]))
                    {
                        resumeStatus.CandidateResumeID = Convert.ToInt32(dataReader["CAND_RESUME_ID"].ToString().Trim());
                    }

                    dataReader.Close();
                }

                return resumeStatus;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Retrive the Candidate Details 
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate id.
        /// </param>
        /// <returns>
        /// Candidate Details
        /// </returns>
        public CandidateSummary GetCandidateActivity(int candidateID)
        {
            if (candidateID <= 0)
                throw new Exception("Candidate ID parameter zero or less than zero");

            IDataReader dataReader = null;
            CandidateSummary candidateSummary = null;
            try
            {
                DbCommand getPageControlsCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_CURRENT_ACTIVITIES");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);


                #region Completed Test

                CandidateTestSessionDetail completedTestSession = null;

                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(dataReader["ROWNUMBER"])) continue;

                    // Instantiate candidate summary object.
                    if (candidateSummary == null)
                        candidateSummary = new CandidateSummary();

                    // Instantiate pending tests list object.
                    if (candidateSummary.CompletedTest == null)
                        candidateSummary.CompletedTest = new List<CandidateTestSessionDetail>();

                    // Instantiate candidate test session detail object.
                    completedTestSession = new CandidateTestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                    {
                        completedTestSession.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        completedTestSession.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                    {
                        completedTestSession.TestID = dataReader["TEST_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        completedTestSession.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                    {
                        completedTestSession.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_MODE"]))
                    {
                        completedTestSession.TestAdminStatus = dataReader["TEST_MODE"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                    {
                        completedTestSession.TestInitiatedBy = dataReader["INITIATED_BY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INITIATED_ON"]))
                    {
                        completedTestSession.CreatedDate = Convert.ToDateTime(dataReader["INITIATED_ON"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                    {
                        completedTestSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_ON"]))
                    {
                        completedTestSession.DateCompleted = Convert.ToDateTime(dataReader["COMPLETED_ON"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_STATUS"]))
                    {
                        completedTestSession.IsCertification = (dataReader["CERTIFICATION_STATUS"].ToString().ToUpper() == "AVAILABLE") ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["DISPLAY_RESULT"]))
                    {
                        completedTestSession.ShowResults = dataReader["DISPLAY_RESULT"].ToString().
                            Trim().ToUpper() == "Y" ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                    {
                        completedTestSession.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                    {
                        completedTestSession.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["IS_SELF_ADMIN"]))
                    {
                        completedTestSession.IsSelfAdmin = dataReader["IS_SELF_ADMIN"].ToString().
                            Trim().ToUpper() == "Y" ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                    {
                        completedTestSession.CandidateFullName = dataReader["FULLNAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_MESSAGE"]))
                    {
                        completedTestSession.ScheduledByMessage = dataReader["SCHEDULED_BY_MESSAGE"].ToString();
                    }
                    // Add to the list.
                    candidateSummary.CompletedTest.Add(completedTestSession);
                }

                #endregion

                #region Pending Test
                CandidateTestSessionDetail pendingTestSession = null;
                dataReader.NextResult();
                while (dataReader.Read())
                {

                    if (Utility.IsNullOrEmpty(dataReader["ROWNUMBER"])) continue;

                    // Instantiate candidate summary object.
                    if (candidateSummary == null)
                        candidateSummary = new CandidateSummary();

                    // Instantiate pending tests list object.
                    if (candidateSummary.PendingTest == null)
                        candidateSummary.PendingTest = new List<CandidateTestSessionDetail>();

                    // Instantiate candidate test session detail object.
                    pendingTestSession = new CandidateTestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                    {
                        pendingTestSession.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        pendingTestSession.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                    {
                        pendingTestSession.TestID = dataReader["TEST_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        pendingTestSession.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"]))
                    {
                        pendingTestSession.TestDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                    {
                        pendingTestSession.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                    {
                        pendingTestSession.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                    {
                        pendingTestSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                    {
                        pendingTestSession.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                    {
                        pendingTestSession.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        pendingTestSession.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["IS_SCHEDULED"]))
                    {
                        pendingTestSession.IsScheduled = dataReader["IS_SCHEDULED"].ToString().ToUpper() == "Y" ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["IS_SELF_ADMIN"]))
                    {
                        pendingTestSession.IsSelfAdmin = dataReader["IS_SELF_ADMIN"].ToString().ToUpper() == "Y" ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_STATUS"]))
                    {
                        pendingTestSession.TestStatus = dataReader["TEST_STATUS"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_ID"]))
                    {
                        pendingTestSession.TestRecommandID = dataReader["TEST_RECOMMEND_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_RECOMMEND_TIME"]))
                    {
                        pendingTestSession.TestRecommandTime =
                            Convert.ToInt32(dataReader["TEST_RECOMMEND_TIME"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ADMIN_STATUS"]))
                    {
                        pendingTestSession.TestAdminStatus =
                            dataReader["TEST_ADMIN_STATUS"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRED"]))
                    {
                        pendingTestSession.IsExpired =
                            dataReader["EXPIRED"].ToString().ToUpper() == "Y" ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_MESSAGE"]))
                    {
                        pendingTestSession.ScheduledByMessage =
                            dataReader["SCHEDULED_BY_MESSAGE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTIONS"]))
                    {
                        pendingTestSession.TotalQuestions =
                            Convert.ToInt32(dataReader["TOTAL_QUESTIONS"].ToString());
                    }
                    // Add to the list. 
                    candidateSummary.PendingTest.Add(pendingTestSession);
                }

                #endregion

                #region Completed  Interviews
                CandidateInterviewSessionDetail completedInterviewSesstion = null;
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                        continue;

                    // Instantiate candidate summary object.
                    if (candidateSummary == null)
                        candidateSummary = new CandidateSummary();

                    // Instantiate candidate interview test session detail object.
                    if (candidateSummary.CompletedInterview == null)
                        candidateSummary.CompletedInterview = new List<CandidateInterviewSessionDetail>();

                    completedInterviewSesstion = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                    {
                        completedInterviewSesstion.InterviewSessionKey = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                    {
                        completedInterviewSesstion.CandidateInterviewSessionID = dataReader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                    {
                        completedInterviewSesstion.InterviewTestKey = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                    {
                        completedInterviewSesstion.InterviewTestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                    {
                        completedInterviewSesstion.InterviewTestDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY_USER_ID"]))
                    {
                        completedInterviewSesstion.CreatedBy = Convert.ToInt32(dataReader["INITIATED_BY_USER_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INITIATED_ON"]))
                    {
                        completedInterviewSesstion.CreatedDate = Convert.ToDateTime(dataReader["INITIATED_ON"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                    {
                        completedInterviewSesstion.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                    {
                        completedInterviewSesstion.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                    {
                        completedInterviewSesstion.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        completedInterviewSesstion.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                    {
                        completedInterviewSesstion.DateCompleted = Convert.ToDateTime(dataReader["COMPLETED_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                    {
                        completedInterviewSesstion.CandidateFullName = dataReader["FULLNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        completedInterviewSesstion.Status = dataReader["STATUS"].ToString().Trim();
                    }
                    candidateSummary.CompletedInterview.Add(completedInterviewSesstion);
                }
                #endregion

                #region Pending Interviews

                CandidateInterviewSessionDetail pendingInterviewSession = null;
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(dataReader["ROWNUMBER"]))
                        continue;

                    // Instantiate candidate summary object.
                    if (candidateSummary == null)
                        candidateSummary = new CandidateSummary();

                    // Instantiate candidate interview test session detail object.
                    if (candidateSummary.PendingInterview == null)
                        candidateSummary.PendingInterview = new List<CandidateInterviewSessionDetail>();

                    pendingInterviewSession = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                    {
                        pendingInterviewSession.InterviewSessionKey = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INTERVIEW_SESSION_ID"]))
                    {
                        pendingInterviewSession.CandidateInterviewSessionID = dataReader["CANDIDATE_INTERVIEW_SESSION_ID"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                    {
                        pendingInterviewSession.InterviewTestKey = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                    {
                        pendingInterviewSession.InterviewTestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                    {
                        pendingInterviewSession.InterviewTestDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY_USER_ID"]))
                    {
                        pendingInterviewSession.CreatedBy = Convert.ToInt32(dataReader["INITIATED_BY_USER_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INITIATED_ON"]))
                    {
                        pendingInterviewSession.CreatedDate = Convert.ToDateTime(dataReader["INITIATED_ON"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                    {
                        pendingInterviewSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        pendingInterviewSession.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                    {
                        pendingInterviewSession.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                    {
                        pendingInterviewSession.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        if (dataReader["STATUS"].ToString().Trim().ToUpper() == "ATMPT_PAUS")
                            pendingInterviewSession.IsPaused = true;
                    }
                    // Add activity to the list.
                    candidateSummary.PendingInterview.Add(pendingInterviewSession);
                }

                #endregion

                return candidateSummary;
            }
            catch { return null; }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<SignupCandidate> GetUserInfomation(int userID)
        {
            if (userID == 0)
                throw new Exception("User ID is zero");

            IDataReader dataReader = null;
            List<SignupCandidate> listCandidateInfo = null;

            try
            {
                DbCommand getPageControlsCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_USERINFO");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@USERID", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                while (dataReader.Read())
                {
                    if (listCandidateInfo == null)
                        listCandidateInfo = new List<SignupCandidate>();

                    SignupCandidate ListElements = new SignupCandidate();
                    if (!Utility.IsNullOrEmpty(dataReader["usrUserName"]))
                    {
                        ListElements.EmailID = dataReader["usrUserName"].ToString().Trim();

                        ListElements.Password = dataReader["usrPassword"].ToString().Trim();

                        // Add to the list.
                        listCandidateInfo.Add(ListElements);
                    }
                }

                return listCandidateInfo;
            }
            catch { return null; }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int GetUserID(string userName)
        {
            if (userName == "")
                throw new Exception("User name is empty");

            IDataReader dataReader = null;

            int userID = 0;
            try
            {
                DbCommand getPageControlsCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_USERID");

                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@USERNAME", DbType.String, userName);

                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["usrID"]))
                    {
                        userID = Convert.ToInt32(dataReader["usrID"]);
                    }
                }
                return userID;
            }
            catch { return 0; }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the candidate user name
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds the candidate candidate id 
        /// </param>        
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        public UserDetail GetUserNameByCandidateID(int candidateID)
        {
            IDataReader dataReader = null;
            try
            {
                UserDetail userDetail = null;

                DbCommand getUserNameCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_USER_NAME_BY_CANDIDATE_ID");
                HCMDatabase.AddInParameter(getUserNameCommand,
                    "@CANDIDATE_ID", DbType.String, candidateID);
                dataReader = HCMDatabase.ExecuteReader(getUserNameCommand);
                if (dataReader != null && dataReader.Read())
                {
                    // Instantiate the resume status object. 
                    userDetail = new UserDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                      userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                        userDetail.UserName = dataReader["USER_NAME"].ToString();


                    dataReader.Close();
                }
                return userDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method to update the user name
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user id
        /// </param>        
        /// <param name="userName">
        /// A<see cref="string"/>that holds the user name
        /// </param>
        /// <param name="modifiedBy">
        /// A<see cref="int"/>that holds the modified by
        /// </param>        
        public void UpdateUserName(int userID, string userName,
            int modifiedBy)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand updateUserNameCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_USER_NAME");

                // Add input parameters.
                HCMDatabase.AddInParameter(updateUserNameCommand,
                  "@USER_ID", DbType.Int32, userID);

                HCMDatabase.AddInParameter(updateUserNameCommand,
                  "@USER_NAME", DbType.String, userName);

                HCMDatabase.AddInParameter(updateUserNameCommand,
                  "@MODIFIED_BY", DbType.Int32, modifiedBy);

                HCMDatabase.ExecuteNonQuery(updateUserNameCommand);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public void InsertAdditionalDocuments(AdditionalDocuments addiDoc)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand insertDocumentCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_CANDIDATE_ADDITIONAL_DOCUMENTS");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertDocumentCommand,
                  "@CADIDATE_ID", DbType.Int32, addiDoc.CandidateID);

                HCMDatabase.AddInParameter(insertDocumentCommand,
                  "@FILE_NAME", DbType.String, addiDoc.FileName);

                HCMDatabase.AddInParameter(insertDocumentCommand,
                  "@FILE_PATH", DbType.String, addiDoc.FilePath);

                HCMDatabase.AddInParameter(insertDocumentCommand,
                  "@MIME_TYPE", DbType.String, addiDoc.FileExtension);

                HCMDatabase.AddInParameter(insertDocumentCommand,
                  "@UPLOADED_DATE", DbType.DateTime, addiDoc.UploadedDate);

                HCMDatabase.AddInParameter(insertDocumentCommand,
                  "@CREATED_BY", DbType.Int32, addiDoc.CreatedBy);

                HCMDatabase.AddInParameter(insertDocumentCommand,
                  "@CREATED_DATE", DbType.DateTime, addiDoc.CreatedDate);

                HCMDatabase.ExecuteNonQuery(insertDocumentCommand);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public void DeleteAdditionalDocument(int docId)
        { 
            //SPDELETE_CANDIDATE_ADDITIONAL_DOCUMENT
            try
            {
                // Create a stored procedure command object.
                DbCommand delDocumentCommand = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_CANDIDATE_ADDITIONAL_DOCUMENT");

                // Add input parameters.
                HCMDatabase.AddInParameter(delDocumentCommand,
                  "@DOC_ID", DbType.Int32, docId); 

                HCMDatabase.ExecuteNonQuery(delDocumentCommand);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public List<AdditionalDocuments> GetCandidateAdditionalDocuments(int candidateId)
        {
            if (candidateId == 0)
                throw new Exception("User ID is zero");

            IDataReader dataReader = null;

            List<AdditionalDocuments> listAddidtionalDocuments = null;

            try
            {
                DbCommand getAdditionalDocCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_ADDITIONAL_DOCUMENTS");

                HCMDatabase.AddInParameter(getAdditionalDocCommand,
                    "@CADIDATE_ID", DbType.Int32, candidateId);

                dataReader = HCMDatabase.ExecuteReader(getAdditionalDocCommand);

                while (dataReader.Read())
                {
                    if (listAddidtionalDocuments == null)
                        listAddidtionalDocuments = new List<AdditionalDocuments>();

                    AdditionalDocuments additionaldoc = new AdditionalDocuments();

                    if (!Utility.IsNullOrEmpty(dataReader["FILE_NAME"]))
                        additionaldoc.FileName = dataReader["FILE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["FILE_PATH"]))
                        additionaldoc.FilePath = dataReader["FILE_PATH"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MIME_TYPE"]))
                        additionaldoc.FileExtension = dataReader["MIME_TYPE"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["UPLOADED_DATE"]))
                        additionaldoc.UploadedDate =Convert.ToDateTime(dataReader["UPLOADED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        additionaldoc.UploadedBy  = dataReader["CREATED_BY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["DOC_ID"]))
                        additionaldoc.DocumentID = Convert.ToInt32(dataReader["DOC_ID"]);
                    // Add to the list.
                    listAddidtionalDocuments.Add(additionaldoc);
                }

                return listAddidtionalDocuments;
            }
            catch { return null; }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        #endregion Signup Methods
    }
}
