﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TrackingDLManager.cs
// File that represents the data layer for the cyber proctoring and
// offline interview tracking module. This will talk to the database 
// to perform these operations

#endregion

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the cyber proctoring and
    /// offline interview tracking module. This will talk to the database 
    /// to perform these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class TrackingDLManager : DatabaseConnectionManager
    {
        public List<SystemLogDetail> GetCyberProctorCommonLog(string systemName, string macAddress,
           string date, string sortOrder, string sortExpression, int pageNumber, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;

            try
            {
                totalRecords = 0;

                DbCommand getCyberProctorLog = HCMDatabase.GetStoredProcCommand("SPGET_CYBER_PROCTOR_COMMON_LOG");

                HCMDatabase.AddInParameter(getCyberProctorLog, "@SYSTEM_NAME", DbType.String, systemName);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@MAC_ADDRESS", DbType.String, macAddress);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@DATE", DbType.String, date);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@ORDER_BY_DIRECTION", DbType.String,
                    sortOrder == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getCyberProctorLog, "@SORT_EXPRESSION", DbType.String, sortExpression);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@PAGE_SIZE", DbType.Int32, pageSize);

                List<SystemLogDetail> systemLogs = new List<SystemLogDetail>();

                dataReader = HCMDatabase.ExecuteReader(getCyberProctorLog);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalRecords = int.Parse(dataReader["COUNT"].ToString());
                    }
                    else
                    {
                        SystemLogDetail systemLog = new SystemLogDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["LOG_ID"]))
                        {
                            systemLog.LogID = Convert.ToInt16(dataReader["LOG_ID"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SYSTEM_NAME"]))
                        {
                            systemLog.SystemName = dataReader["SYSTEM_NAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MAC_ADDRESS"]))
                        {
                            systemLog.MacAddress = dataReader["MAC_ADDRESS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["IP_ADDRESS"]))
                        {
                            systemLog.IPAddress = dataReader["IP_ADDRESS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["OS_INFO"]))
                        {
                            systemLog.OSInfo = dataReader["OS_INFO"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["DATE"]))
                        {
                            systemLog.CreatedDate = dataReader["DATE"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["LOG_FILE_ID"]))
                        {
                            systemLog.LogFileID = Convert.ToInt32(dataReader["LOG_FILE_ID"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["WEBCAM_IMAGE_AVAILABLE"]) &&
                            dataReader["WEBCAM_IMAGE_AVAILABLE"].ToString().ToUpper() == "Y")
                        {
                            systemLog.IsWebcamImageAvailable = true;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DESKTOP_IMAGE_AVAILABLE"]) &&
                           dataReader["DESKTOP_IMAGE_AVAILABLE"].ToString().ToUpper() == "Y")
                        {
                            systemLog.IsDesktopImageAvailable = true;
                        }

                        systemLogs.Add(systemLog);
                    }
                }
                return systemLogs;
            }

            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        public List<SystemLogDetail> GetCyberProctorLogMesageDetails(string logID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCyberProctorLog = HCMDatabase.GetStoredProcCommand("SPGET_CYBER_PROCTOR_COMMON_MESSAGE_LOG");

                HCMDatabase.AddInParameter(getCyberProctorLog, "@LOG_ID", DbType.Int16, logID);

                List<SystemLogDetail> cyberProctorLogDetails = new List<SystemLogDetail>();

                dataReader = HCMDatabase.ExecuteReader(getCyberProctorLog);

                while (dataReader.Read())
                {

                    SystemLogDetail cyberProctor = new SystemLogDetail();


                    if (!Utility.IsNullOrEmpty(dataReader["LOG_ID"]))
                    {
                        cyberProctor.LogID = Convert.ToInt16(dataReader["LOG_ID"]);
                    }


                    if (!Utility.IsNullOrEmpty(dataReader["MESSAGE_ID"]))
                    {
                        cyberProctor.MessageID = Convert.ToInt16(dataReader["MESSAGE_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["MESSAGE"]))
                    {
                        cyberProctor.Message = dataReader["MESSAGE"].ToString();
                    }


                    if (!Utility.IsNullOrEmpty(dataReader["DATE"]))
                    {
                        cyberProctor.CreatedDate = dataReader["DATE"].ToString();
                    }

                    cyberProctorLogDetails.Add(cyberProctor);
                }
                return cyberProctorLogDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }


        public List<SystemLogDetail> GetCyberProctorLogImages(int logID, string imageType)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getCyberProctorLog = HCMDatabase.GetStoredProcCommand("SPGET_CYBER_PROCTOR_IMAGES");

                HCMDatabase.AddInParameter(getCyberProctorLog, "@LOG_ID", DbType.Int16, logID);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@IMAGE_TYPE", DbType.String, imageType);

                List<SystemLogDetail> cyberProctorLogDetails = new List<SystemLogDetail>();

                dataReader = HCMDatabase.ExecuteReader(getCyberProctorLog);

                while (dataReader.Read())
                {

                    SystemLogDetail cyberProctor = new SystemLogDetail();


                    if (!Utility.IsNullOrEmpty(dataReader["LOG_ID"]))
                    {
                        cyberProctor.LogID = Convert.ToInt16(dataReader["LOG_ID"]);
                    }


                    cyberProctorLogDetails.Add(cyberProctor);
                }
                return cyberProctorLogDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        public List<SystemLogDetail> GetOfflineInterviewLogImages(int logID, string imageType)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getCyberProctorLog = HCMDatabase.GetStoredProcCommand("SPGET_CYBER_PROCTOR_IMAGES");

                HCMDatabase.AddInParameter(getCyberProctorLog, "@LOG_ID", DbType.Int16, logID);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@IMAGE_TYPE", DbType.String, imageType);

                List<SystemLogDetail> cyberProctorLogDetails = new List<SystemLogDetail>();

                dataReader = HCMDatabase.ExecuteReader(getCyberProctorLog);

                while (dataReader.Read())
                {

                    SystemLogDetail cyberProctor = new SystemLogDetail();


                    if (!Utility.IsNullOrEmpty(dataReader["LOG_ID"]))
                    {
                        cyberProctor.LogID = Convert.ToInt16(dataReader["LOG_ID"]);
                    }


                    cyberProctorLogDetails.Add(cyberProctor);
                }
                return cyberProctorLogDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        public void DeleteLogMessage(int logID)
        {
            // Create a stored procedure command object.
            DbCommand deleteRoleCatgeoryTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_CYBER_PROCTOR_COMMON_LOG");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRoleCatgeoryTypeCommand,
                "@LOG_ID", DbType.Int32, logID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteRoleCatgeoryTypeCommand);
            {

            }
        }

        /// <summary>
        /// Method that retreives the log file taken from the candidate machine
        /// during test session.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        /// <returns>
        /// An array of <see cref="byte"/> that holds the file contents.
        /// </returns>
        public byte[] GetCyberProctorLogFile(int logID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand command = HCMDatabase.GetStoredProcCommand
                    ("SPGET_CYBER_PROCTOR_COMMON_FILE_LOG");
                HCMDatabase.AddInParameter(command,
                    "@LOG_ID", DbType.Int32, logID);
                dataReader = HCMDatabase.ExecuteReader(command);

                if (dataReader != null && dataReader.Read())
                {
                    return dataReader["FILE_CONTENTS"] as byte[];
                }

                return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the log file taken from the candidate machine
        /// during offline interview session.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        /// <returns>
        /// An array of <see cref="byte"/> that holds the file contents.
        /// </returns>
        public byte[] GetOfflineInterviewLogFile(int logID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand command = HCMDatabase.GetStoredProcCommand
                    ("SPGET_OFFLINE_INTERVIEW_COMMON_FILE_LOG");
                HCMDatabase.AddInParameter(command,
                    "@LOG_ID", DbType.Int32, logID);
                dataReader = HCMDatabase.ExecuteReader(command);

                if (dataReader != null && dataReader.Read())
                {
                    return dataReader["FILE_CONTENTS"] as byte[];
                }

                return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the offline interview message log.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="SystemMessageLogDetail"/> that holds the 
        /// system message logs.
        /// </returns>
        public List<SystemMessageLogDetail> GetOfflineInterviewMessageLog(int logID)
        {
            IDataReader dataReader = null;

            // Create a system message log detail object list.
            List<SystemMessageLogDetail> systemMessageLogs = null;

            try
            {
                DbCommand getCyberProctorLog = HCMDatabase.GetStoredProcCommand
                    ("SPGET_OFFLINE_INTERVIEW_COMMON_MESSAGE_LOG");

                HCMDatabase.AddInParameter(getCyberProctorLog, "@LOG_ID",
                    DbType.Int16, logID);

                dataReader = HCMDatabase.ExecuteReader(getCyberProctorLog);

                while (dataReader.Read())
                {
                    // Instantiate the system message log detail list.
                    if (systemMessageLogs == null)
                        systemMessageLogs = new List<SystemMessageLogDetail>();

                    // Create a system message log detail object.
                    SystemMessageLogDetail systemMessageLog = new SystemMessageLogDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["LOG_ID"]))
                    {
                        systemMessageLog.LogID = Convert.ToInt16(dataReader["LOG_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["MESSAGE_ID"]))
                    {
                        systemMessageLog.MessageID = Convert.ToInt16(dataReader["MESSAGE_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["MESSAGE"]))
                    {
                        systemMessageLog.Message = dataReader["MESSAGE"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["DATE"]))
                    {
                        systemMessageLog.CreatedDate = dataReader["DATE"].ToString();
                    }

                    // Add the system message log detail object to the list.
                    systemMessageLogs.Add(systemMessageLog);
                }
                return systemMessageLogs;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the offline interview common log image.
        /// </summary>
        /// <param name="imageID">
        /// A <see cref="int"/> that holds the image ID.
        /// </param>
        /// <param name="imageType">
        /// A <see cref="string"/> that holds the image type.
        /// </param>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID.
        /// </param>
        /// <returns>
        /// An array of <see cref="byte"/> that holds the image contents.
        /// </returns>
        public byte[] GetOfflineInterviewCommonLogImage(int imageID, string imageType, int logID)
        {
            byte[] image = null;

            IDataReader dataReader = null;

            try
            {
                DbCommand getImageCommand = HCMDatabase.
                      GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_COMMON_LOG_IMAGE");

                HCMDatabase.AddInParameter(getImageCommand,
                    "@IMAGE_ID", DbType.Int16, imageID);

                HCMDatabase.AddInParameter(getImageCommand,
                    "@LOG_ID", DbType.Int16, logID);

                HCMDatabase.AddInParameter(getImageCommand,
                    "@IMAGE_TYPE", DbType.String, imageType);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getImageCommand);

                while (dataReader.Read())
                {
                    if (dataReader["IMAGE_CONTENTS"] != DBNull.Value)
                        image = (byte[])dataReader["IMAGE_CONTENTS"];
                    else
                        image = null;
                }

                dataReader.Close();
                return image;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that deletes the offline interview log.
        /// </summary>
        /// <param name="logID">
        /// A <see cref="int"/> that holds the log ID to delete.
        /// </param>
        public void DeleteOfflineInterviewLog(int logID)
        {
            // Create a stored procedure command object.
            DbCommand deleteLogCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_OFFLINE_INTERVIEW_COMMON_LOG");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteLogCommand,
                "@LOG_ID", DbType.Int32, logID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteLogCommand);
        }

        /// <summary>
        /// Method that retrieves the common log for the offline interview
        /// session.
        /// </summary>
        /// <param name="systemName">
        /// A <see cref="string"/> that holds the system name to apply like search.
        /// </param>
        /// <param name="macAddress">
        /// A <see cref="string"/> that holds the MAC address to apply like search.
        /// </param>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the log date.
        /// </param>
        /// <param name="sortOrder">
        /// A <see cref="string"/> that holds the sort order.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that holds the sort expression.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="SystemLogDetail"/> that holds the common log.
        /// </returns>
        public List<SystemLogDetail> GetOfflineInterviewCommonLog
            (string systemName, string macAddress, string date, string sortOrder,
            string sortExpression, int pageNumber, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;

            // Create a system log detail object list.
            List<SystemLogDetail> systemLogs = null;

            try
            {
                DbCommand getCyberProctorLog = HCMDatabase.GetStoredProcCommand
                    ("SPGET_OFFLINE_INTERVIEW_COMMON_LOG");

                HCMDatabase.AddInParameter(getCyberProctorLog, "@SYSTEM_NAME",
                    DbType.String, systemName);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@MAC_ADDRESS",
                    DbType.String, macAddress);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@DATE",
                    DbType.String, date);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@ORDER_BY_DIRECTION",
                    DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getCyberProctorLog, "@SORT_EXPRESSION",
                    DbType.String, sortExpression);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@PAGE_NUM",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCyberProctorLog, "@PAGE_SIZE",
                    DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getCyberProctorLog);

                while (dataReader.Read())
                {
                    // Instantiate the system log detail list.
                    if (systemLogs == null)
                        systemLogs = new List<SystemLogDetail>();

                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalRecords = int.Parse(dataReader["COUNT"].ToString());
                    }
                    else
                    {
                        // Create a system log detail object.
                        SystemLogDetail systemLog = new SystemLogDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["LOG_ID"]))
                        {
                            systemLog.LogID = Convert.ToInt16(dataReader["LOG_ID"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SYSTEM_NAME"]))
                        {
                            systemLog.SystemName = dataReader["SYSTEM_NAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MAC_ADDRESS"]))
                        {
                            systemLog.MacAddress = dataReader["MAC_ADDRESS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["IP_ADDRESS"]))
                        {
                            systemLog.IPAddress = dataReader["IP_ADDRESS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["OS_INFO"]))
                        {
                            systemLog.OSInfo = dataReader["OS_INFO"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["DATE"]))
                        {
                            systemLog.CreatedDate = dataReader["DATE"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["LOG_FILE_ID"]))
                        {
                            systemLog.LogFileID = Convert.ToInt32(dataReader["LOG_FILE_ID"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["WEBCAM_IMAGE_AVAILABLE"]) &&
                            dataReader["WEBCAM_IMAGE_AVAILABLE"].ToString().ToUpper() == "Y")
                        {
                            systemLog.IsWebcamImageAvailable = true;
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DESKTOP_IMAGE_AVAILABLE"]) &&
                            dataReader["DESKTOP_IMAGE_AVAILABLE"].ToString().ToUpper() == "Y")
                        {
                            systemLog.IsDesktopImageAvailable = true;
                        }

                        // Add the system log detail object to the list.
                        systemLogs.Add(systemLog);
                    }
                }

                return systemLogs;
            }

            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// to Get CyberProctor Image Details from database.
        /// </summary>
        /// <param name="cansessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="attempID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <param name="imageType">
        /// A<see cref="int"/>that holds the image type
        /// </param>
        /// <param name="pageNo">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <param name="totalRec">
        /// A<see cref="int"/>that holds the total records
        /// </param>
        /// <returns>
        /// A list for<see cref="TrackingImageDetail"/>that holds the Cyber Proctor Image
        /// </returns>
        public List<TrackingImageDetail> GetCyberProctorImageDetail(string cansessionKey,
            int attempID, string imageType, int pageNo, int pageSize, out int totalRec)
        {
            IDataReader dataReader = null;
            int attempidTemp = 0;
            totalRec = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand cyberProctorDesktopImagedetail = HCMDatabase.
                     GetStoredProcCommand("SPGET_CYBERPROCTOR_IMAGE_DETAIL");

                cyberProctorDesktopImagedetail.CommandTimeout = 0;

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                    "CAND_SESSION_KEY", System.Data.DbType.String, cansessionKey);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                   "ATTEMPT_ID", System.Data.DbType.String, attempID);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@IMAGE_TYPE", System.Data.DbType.String, imageType);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@PAGENUM", System.Data.DbType.Int32, pageNo);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@PAGESIZE", System.Data.DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImagedetail);

                List<TrackingImageDetail> CyberProctorDesktopImages = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the CyberProctorDesktopImages collection.
                    if (CyberProctorDesktopImages == null)
                        CyberProctorDesktopImages = new List<TrackingImageDetail>();

                    // Create a new CyberProctorDesktopImages object.
                    TrackingImageDetail CyberProctorDesktopImage =
                        new TrackingImageDetail();

                    // Assign property values to the CyberProctorDesktopImage object.
                    if (dataReader["CAND_SESSION_KEY"] != DBNull.Value)
                    {
                        CyberProctorDesktopImage.CandidateSessionID = dataReader["CAND_SESSION_KEY"].ToString().Trim();
                        int.TryParse(dataReader["ATTEMPT_ID"].ToString().Trim(), out attempidTemp);
                        CyberProctorDesktopImage.AttempID = attempidTemp;
                        CyberProctorDesktopImage.ImageID = dataReader["CP_IMAGE_ID"].ToString().Trim();

                        // Add the category to the collection.
                        CyberProctorDesktopImages.Add(CyberProctorDesktopImage);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRec);
                    }
                }
                dataReader.Close();

                return CyberProctorDesktopImages;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<TrackingImageDetail> GetCyberProctorLogImages(int logID, string imgtype,
            int pageNo, int pageSize, out int TotalRec)
        {
            IDataReader dataReader = null;
            TotalRec = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand cyberProctorDesktopImagedetail = HCMDatabase.
                     GetStoredProcCommand("SPGET_CYBER_PROCTOR_LOG_IMAGES");

                cyberProctorDesktopImagedetail.CommandTimeout = 0;

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                   "@LOG_ID", System.Data.DbType.String, logID);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@IMAGE_TYPE", System.Data.DbType.String, imgtype);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@PAGENUM", System.Data.DbType.Int32, pageNo);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@PAGESIZE", System.Data.DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImagedetail);

                List<TrackingImageDetail> CyberProctorDesktopImages = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the CyberProctorDesktopImages collection.
                    if (CyberProctorDesktopImages == null)
                        CyberProctorDesktopImages = new List<TrackingImageDetail>();

                    // Create a new CyberProctorDesktopImages object.
                    TrackingImageDetail CyberProctorDesktopImage =
                        new TrackingImageDetail();

                    // Assign property values to the CyberProctorDesktopImage object.
                    if (dataReader["LOG_ID"] != DBNull.Value)
                    {
                        CyberProctorDesktopImage.LogID = Convert.ToInt16(dataReader["LOG_ID"]);
                        CyberProctorDesktopImage.ImageID = dataReader["IMAGE_ID"].ToString().Trim();

                        // Add the category to the collection.
                        CyberProctorDesktopImages.Add(CyberProctorDesktopImage);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out TotalRec);
                    }
                }
                dataReader.Close();

                return CyberProctorDesktopImages;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<TrackingImageDetail> GetOfflineInterviewLogImages(int logID, string imgtype,
            int pageNo, int pageSize, out int TotalRec)
        {
            IDataReader dataReader = null;
            TotalRec = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand cyberProctorDesktopImagedetail = HCMDatabase.
                     GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_LOG_IMAGES");

                cyberProctorDesktopImagedetail.CommandTimeout = 0;

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                   "@LOG_ID", System.Data.DbType.String, logID);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@IMAGE_TYPE", System.Data.DbType.String, imgtype);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@PAGENUM", System.Data.DbType.Int32, pageNo);

                HCMDatabase.AddInParameter(cyberProctorDesktopImagedetail,
                  "@PAGESIZE", System.Data.DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImagedetail);

                List<TrackingImageDetail> CyberProctorDesktopImages = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the CyberProctorDesktopImages collection.
                    if (CyberProctorDesktopImages == null)
                        CyberProctorDesktopImages = new List<TrackingImageDetail>();

                    // Create a new CyberProctorDesktopImages object.
                    TrackingImageDetail CyberProctorDesktopImage =
                        new TrackingImageDetail();

                    // Assign property values to the CyberProctorDesktopImage object.
                    if (dataReader["LOG_ID"] != DBNull.Value)
                    {
                        CyberProctorDesktopImage.LogID = Convert.ToInt16(dataReader["LOG_ID"]);
                        CyberProctorDesktopImage.ImageID = dataReader["IMAGE_ID"].ToString().Trim();

                        // Add the category to the collection.
                        CyberProctorDesktopImages.Add(CyberProctorDesktopImage);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out TotalRec);
                    }
                }
                dataReader.Close();

                return CyberProctorDesktopImages;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method that retrieves the offline interview tracking images 
        /// details.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/>that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="imageType">
        /// A <see cref="int"/> that holds the image type.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records
        /// </param>
        /// <returns>
        /// A list of <see cref="TrackingImageDetail"/> that holds 
        /// the tracking images.
        /// </returns>
        public List<TrackingImageDetail> GetOfflineInterviewTrackingImageDetails
            (string candidateSessionID, int attempID, string imageType,
            int pageNumber, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;

            try
            {
                // Create a stored procedure command object.
                DbCommand getImagesCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_TRACKING_IMAGE_DETAIL");

                getImagesCommand.CommandTimeout = 0;

                HCMDatabase.AddInParameter(getImagesCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getImagesCommand,
                    "@ATTEMPT_ID", DbType.String, attempID);

                HCMDatabase.AddInParameter(getImagesCommand,
                    "@IMAGE_TYPE", DbType.String, imageType);

                HCMDatabase.AddInParameter(getImagesCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getImagesCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getImagesCommand);

                List<TrackingImageDetail> trackingImages = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the tracking images collection.
                    if (trackingImages == null)
                        trackingImages = new List<TrackingImageDetail>();

                    // Create a new tracking image detail object.
                    TrackingImageDetail trackingImage =
                        new TrackingImageDetail();

                    // Check if image ID is null or not.
                    if (!Utility.IsNullOrEmpty(dataReader["IMAGE_ID"]) &&
                        dataReader["IMAGE_ID"] != DBNull.Value)
                    {
                        // Assign values to tracking image detail object.
                        trackingImage.CandidateSessionID = candidateSessionID;
                        trackingImage.AttempID = attempID;
                        trackingImage.ImageID = dataReader["IMAGE_ID"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["MESSAGE"]) &&
                            dataReader["MESSAGE"] != DBNull.Value)
                        {
                            trackingImage.Message = dataReader["MESSAGE"].ToString().Trim();
                        }

                        // Add the tracking image detail to the list.
                        trackingImages.Add(trackingImage);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }
                }
                dataReader.Close();

                return trackingImages;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the offline interview tracking image.
        /// </summary>
        /// <param name="imageID">
        /// A <see cref="int"/> that holds the image ID.
        /// </param>
        /// <param name="isThumb">
        /// A <see cref="int"/> that holds the status whether the thumbnail or
        /// original image needs to be retrieved. 1 represents thumbnail and
        /// 0 original image.
        /// </param>
        /// <returns>
        /// A <see cref="byte"/> that holds the image contents.
        /// </returns>
        public byte[] GetOfflineInterviewTrackingImage(int imageID, int isThumb)
        {
            byte[] image = null;

            IDataReader dataReader = null;

            try
            {
                DbCommand getImageCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_TRACKING_IMAGE");

                HCMDatabase.AddInParameter(getImageCommand,
                    "@IMAGE_ID", DbType.String, imageID);
                HCMDatabase.AddInParameter(getImageCommand,
                    "@IS_THUMB", DbType.Int32, isThumb);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getImageCommand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["IMAGE"]) &&
                        dataReader["IMAGE"] != DBNull.Value)
                    {
                        image = (byte[])dataReader["IMAGE"];
                    }
                }

                dataReader.Close();
                return image;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        ///  To Get CyberProctor Desktop Image from database.
        /// </summary>
        /// <param name="imageID">
        /// A<see cref="int"/>that holds the image id
        /// </param>
        /// <param name="isThumb">
        /// A<see cref="int"/>that holds the thumnail 
        /// </param>
        /// <returns>
        /// A<see cref="byte"/> that holds the total records
        /// </returns>
        public byte[] GetCyberProctorDesktopImage(int imageID, int isThumb)
        {
            byte[] image = null;

            IDataReader dataReader = null;
            DbCommand cyberProctorDesktopImageDbCommand = HCMDatabase.
                  GetStoredProcCommand("SPGET_CYBERPROCTOR_IMAGE");

            HCMDatabase.AddInParameter(cyberProctorDesktopImageDbCommand,
                "@IMAGE_ID", System.Data.DbType.String, imageID);
            HCMDatabase.AddInParameter(cyberProctorDesktopImageDbCommand,
                "@ISTHUMB", System.Data.DbType.String, isThumb);

            // Execute the stored procedure.
            dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImageDbCommand);

            while (dataReader.Read())
            {
                if (dataReader["CP_IMAGE"] != DBNull.Value)
                    image = (byte[])dataReader["CP_IMAGE"];
                else
                    image = null;
            }
            dataReader.Close();
            return image;
        }

        public byte[] GetCyberProctorDesktopLogImage(int imageid, string imageType, int logId)
        {
            byte[] image = null;

            IDataReader dataReader = null;
            DbCommand cyberProctorDesktopImageDbCommand = HCMDatabase.
                  GetStoredProcCommand("SPGET_CYBERPROCTOR_LOG_IMAGE");

            HCMDatabase.AddInParameter(cyberProctorDesktopImageDbCommand,
                "@IMAGE_ID", System.Data.DbType.Int16, imageid);


            HCMDatabase.AddInParameter(cyberProctorDesktopImageDbCommand,
                "@LOG_ID", System.Data.DbType.Int16, logId);

            HCMDatabase.AddInParameter(cyberProctorDesktopImageDbCommand,
             "@IMAGE_TYPE", System.Data.DbType.String, imageType);

            // Execute the stored procedure.
            dataReader = HCMDatabase.ExecuteReader(cyberProctorDesktopImageDbCommand);

            while (dataReader.Read())
            {
                if (dataReader["IMAGE_CONTENTS"] != DBNull.Value)
                    image = (byte[])dataReader["IMAGE_CONTENTS"];
                else
                    image = null;
            }
            dataReader.Close();
            return image;
        }

        /// <summary>
        /// Method that retrieves the cyber proctoring application logs.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="SystemLogDetail"/> that holds the cyber proctoring
        /// application logs.
        /// </returns>
        public List<SystemLogDetail> GetCyberProctorApplicationLogs
            (string candidateSessionKey, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            IDataReader dataReader = null;
            SystemLogDetail applicationLog = null;
            List<SystemLogDetail> applicationLogs = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand applicationLogsDbCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_CYBER_PROCTOR_APPLICATION_LOGS");

                applicationLogsDbCommand.CommandTimeout = 0;

                HCMDatabase.AddInParameter(applicationLogsDbCommand,
                    "CAND_SESSION_KEY", System.Data.DbType.String, candidateSessionKey);

                HCMDatabase.AddInParameter(applicationLogsDbCommand,
                   "ATTEMPT_ID", System.Data.DbType.String, attempID);

                HCMDatabase.AddInParameter(applicationLogsDbCommand,
                  "@PAGENUM", System.Data.DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(applicationLogsDbCommand,
                  "@PAGESIZE", System.Data.DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(applicationLogsDbCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the application logs list.
                    if (applicationLogs == null)
                        applicationLogs = new List<SystemLogDetail>();

                    //  nstantiate the application logs object.
                    applicationLog = new SystemLogDetail();

                    if (dataReader["CAND_SESSION_KEY"] != DBNull.Value && dataReader["CAND_SESSION_KEY"] != null)
                    {
                        if (dataReader["MESSAGE"] != DBNull.Value && dataReader["MESSAGE"] != null)
                            applicationLog.Message = dataReader["MESSAGE"].ToString().Trim();

                        if (dataReader["CREATED_DATE"] != DBNull.Value && dataReader["CREATED_DATE"] != null)
                            applicationLog.Date = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());

                        // Add the application logs object to the list.
                        applicationLogs.Add(applicationLog);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }
                }
                dataReader.Close();

                return applicationLogs;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// this method represent get CyberProctor Events from database.
        /// </summary>
        /// <param name="cansessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="attempID">
        /// A<see cref="int"/>that holds the candidate attempt id
        /// </param>
        /// <param name="pageNo">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/></param>that holds the total records
        /// <returns>
        /// A list for <see cref="ApplicationEventDetail"/>that holds the Cyber Proctor Event
        /// </returns>
        public List<ApplicationEventDetail> GetCyberProctorEvents(string cansessionKey,
            int attempID, int pageNo, int pageSize, out int totalRecords)
        {

            totalRecords = 0;
            IDataReader dataReader = null;
            int attempidTemp = 0;
            ApplicationEventDetail cyberProctorEvent = null;
            List<ApplicationEventDetail> cyberProctorEvents = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand cyberProctorEventsDbCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_CYBERPROCTOR_EVENTS");

                cyberProctorEventsDbCommand.CommandTimeout = 0;

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                    "CAND_SESSION_KEY", System.Data.DbType.String, cansessionKey);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                   "ATTEMPT_ID", System.Data.DbType.String, attempID);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                  "@PAGENUM", System.Data.DbType.Int32, pageNo);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                  "@PAGESIZE", System.Data.DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorEventsDbCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the CyberProctorEvents.
                    if (cyberProctorEvents == null)
                        cyberProctorEvents = new List<ApplicationEventDetail>();

                    // Create a new CyberProctorEvents object.
                    cyberProctorEvent = new ApplicationEventDetail();

                    // Assign property values to the CyberProctorEvents object.
                    if (dataReader["CAND_SESSION_KEY"] != DBNull.Value)
                    {
                        cyberProctorEvent.CandidateSessionID = dataReader["CAND_SESSION_KEY"].ToString().Trim();
                        int.TryParse(dataReader["ATTEMPT_ID"].ToString().Trim(), out attempidTemp);
                        cyberProctorEvent.AttempID = attempidTemp;
                        cyberProctorEvent.ApplicationName = dataReader["APPLICATION_NAME"].ToString().Trim();
                        cyberProctorEvent.ApplicationPath = dataReader["APP_FILE_PATH"].ToString().Trim();
                        cyberProctorEvent.Date = dataReader["CREATED_DATE"].ToString().Trim();
                        cyberProctorEvent.EventStatus = dataReader["EVENT_STATUS"].ToString().Trim();
                        // Add the ApplicationEventDetail to the collection.
                        cyberProctorEvents.Add(cyberProctorEvent);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }
                }
                dataReader.Close();

                return cyberProctorEvents;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// to Get CyberProctor Exceptions from database.
        /// </summary>
        /// <param name="cansessionKey">
        /// A<see cref="string"/>that holds the candidate session key
        /// </param>
        /// <param name="attempID">
        /// A<see cref="int"/>that holds the attempt id
        /// </param>
        /// <param name="pageNo">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total records out parameter
        /// </param>
        /// <returns>
        /// A list for<see cref="ApplicationExceptionDetail"/>that holds the Cyber Proctor Exception
        /// </returns>
        public List<ApplicationExceptionDetail> GetCyberProctorExceptions(string cansessionKey,
            int attempID, int pageNo, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;
            int attempidTemp = 0;
            totalRecords = 0;
            ApplicationExceptionDetail cyberProctorException = null;
            List<ApplicationExceptionDetail> cyberProctorExceptions = null;
            try
            {
                // Create a stored procedure command object.

                DbCommand cyberProctorEventsDbCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CYBERPROCTOR_EXCEPTIONS");
                cyberProctorEventsDbCommand.CommandTimeout = 0;
                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                    "CAND_SESSION_KEY", System.Data.DbType.String, cansessionKey);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                   "ATTEMPT_ID", System.Data.DbType.String, attempID);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                "@PAGENUM", System.Data.DbType.Int32, pageNo);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                  "@PAGESIZE", System.Data.DbType.Int32, pageSize);


                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorEventsDbCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the CyberProctorExceptions collection.
                    if (cyberProctorExceptions == null)
                        cyberProctorExceptions = new List<ApplicationExceptionDetail>();
                    // Create a new CyberProctorExceptions object.
                    cyberProctorException = new ApplicationExceptionDetail();

                    // Assign property values to the CyberProctorExceptions object.
                    if (dataReader["CAND_SESSION_KEY"] != DBNull.Value)
                    {
                        cyberProctorException.CandidateSessionID = dataReader["CAND_SESSION_KEY"].ToString().Trim();
                        int.TryParse(dataReader["ATTEMPT_ID"].ToString().Trim(), out attempidTemp);
                        cyberProctorException.AttempID = attempidTemp;
                        cyberProctorException.Exception = dataReader["EXCEPTION"].ToString().Trim();

                        cyberProctorException.Date = dataReader["CREATED_DATE"].ToString().Trim();

                        // Add the CyberProctorExceptions to the collection.
                        cyberProctorExceptions.Add(cyberProctorException);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }
                }
                dataReader.Close();
                return cyberProctorExceptions;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// this method represent Get CyberProctor ExistApplications from database.
        /// </summary>
        /// <param name="cansessionKey">
        /// A<see cref="string"/>that holds candidate session key
        /// </param>
        /// <param name="attempID">
        /// A<see cref="int"/>that holds attepmt id 
        /// </param>
        /// <param name="pageNo">
        /// A<see cref="int"/>that holds page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds total records out parameter 
        /// </param>
        /// <returns>
        /// A list for <see cref="CyberProctorExistApplication"/>that holds cyber proctor exist application
        /// </returns>
        public List<ApplicationDetail> GetCyberProctorExistApplications(string cansessionKey,
           int attempID, int pageNo, int pageSize, out int totalRecords)
        {

            IDataReader dataReader = null;
            int attempidTemp = 0;
            totalRecords = 0;
            ApplicationDetail cyberProctorExistApplication = null;
            List<ApplicationDetail> cyberProctorExistApplications = null;
            try
            {

                // Create a stored procedure command object.
                DbCommand cyberProctorEventsDbCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CYBERPROCTOR_EXIST_APPLICATIONS");
                cyberProctorEventsDbCommand.CommandTimeout = 0;
                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                    "CAND_SESSION_KEY", System.Data.DbType.String, cansessionKey);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                   "ATTEMPT_ID", System.Data.DbType.String, attempID);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                   "@PAGENUM", System.Data.DbType.Int32, pageNo);

                HCMDatabase.AddInParameter(cyberProctorEventsDbCommand,
                  "@PAGESIZE", System.Data.DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(cyberProctorEventsDbCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the ApplicationDetail collection.
                    if (cyberProctorExistApplications == null)
                        cyberProctorExistApplications = new List<ApplicationDetail>();
                    // Create a new CyberProctorDesktopImages object.
                    cyberProctorExistApplication = new ApplicationDetail();

                    // Assign property values to the ApplicationDetail object.
                    if (dataReader["CAND_SESSION_KEY"] != DBNull.Value)
                    {
                        cyberProctorExistApplication.CandidateSessionID = dataReader["CAND_SESSION_KEY"].ToString().Trim();
                        int.TryParse(dataReader["ATTEMPT_ID"].ToString().Trim(), out attempidTemp);
                        cyberProctorExistApplication.AttempID = attempidTemp;
                        cyberProctorExistApplication.Application = dataReader["ProcessName"].ToString().Trim();
                        cyberProctorExistApplication.ExePath = dataReader["EXEPATH"].ToString().Trim();
                        // Add the ApplicationDetail to the collection.
                        cyberProctorExistApplications.Add(cyberProctorExistApplication);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }

                }
                dataReader.Close();

                return cyberProctorExistApplications;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the offline interview application logs.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ApplicationLogDetail"/> that holds the 
        /// application logs.
        /// </returns>
        public List<ApplicationLogDetail> GetApplicationLogs
            (string candidateSessionID, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            IDataReader dataReader = null;

            // Create an application log list.
            List<ApplicationLogDetail> applicationLogs = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getLogsCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_APPLICATION_LOG");

                getLogsCommand.CommandTimeout = 0;

                HCMDatabase.AddInParameter(getLogsCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", System.Data.DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getLogsCommand,
                   "@ATTEMPT_ID", System.Data.DbType.String, attempID);

                HCMDatabase.AddInParameter(getLogsCommand,
                  "@PAGENUM", System.Data.DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getLogsCommand,
                  "@PAGESIZE", System.Data.DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getLogsCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the application logs list.
                    if (applicationLogs == null)
                        applicationLogs = new List<ApplicationLogDetail>();

                    // Create a application log object.
                    ApplicationLogDetail applicationLog = new ApplicationLogDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["MESSAGE"]) &&
                        dataReader["MESSAGE"] != DBNull.Value)
                    {
                        if (dataReader["MESSAGE"] != DBNull.Value && dataReader["MESSAGE"] != null)
                            applicationLog.Message = dataReader["MESSAGE"].ToString().Trim();

                        if (dataReader["CREATED_DATE"] != DBNull.Value && dataReader["CREATED_DATE"] != null)
                            applicationLog.Date = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString().Trim());

                        // Add the application log object to the list.
                        applicationLogs.Add(applicationLog);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }
                }
                dataReader.Close();

                return applicationLogs;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the offline interview application events.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ApplicationEventDetail"/> that holds the 
        /// application events.
        /// </returns>
        public List<ApplicationEventDetail> GetApplicationEvents
            (string candidateSessionID, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            IDataReader dataReader = null;

            // Create an application events list.
            List<ApplicationEventDetail> applicationEvents = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getEventsCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_EVENTS");

                getEventsCommand.CommandTimeout = 0;

                HCMDatabase.AddInParameter(getEventsCommand,
                    "CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getEventsCommand,
                   "ATTEMPT_ID", DbType.String, attempID);

                HCMDatabase.AddInParameter(getEventsCommand,
                  "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getEventsCommand,
                  "@PAGESIZE", DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getEventsCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the application events list.
                    if (applicationEvents == null)
                        applicationEvents = new List<ApplicationEventDetail>();

                    // Create an application event object.
                    ApplicationEventDetail applicationEvent = new ApplicationEventDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["APPLICATION_NAME"]) &&
                        dataReader["APPLICATION_NAME"] != DBNull.Value)
                    {
                        applicationEvent.CandidateSessionID = candidateSessionID;
                        applicationEvent.AttempID = attempID;

                        applicationEvent.ApplicationName = dataReader["APPLICATION_NAME"].ToString().Trim();

                        if (dataReader["APP_FILE_PATH"] != DBNull.Value && dataReader["APP_FILE_PATH"] != null)
                            applicationEvent.ApplicationPath = dataReader["APP_FILE_PATH"].ToString().Trim();

                        if (dataReader["CREATED_DATE"] != DBNull.Value && dataReader["CREATED_DATE"] != null)
                            applicationEvent.Date = dataReader["CREATED_DATE"].ToString().Trim();

                        if (dataReader["EVENT_STATUS"] != DBNull.Value && dataReader["EVENT_STATUS"] != null)
                            applicationEvent.EventStatus = dataReader["EVENT_STATUS"].ToString().Trim();

                        // Add the application event object to the list.
                        applicationEvents.Add(applicationEvent);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }
                }
                dataReader.Close();

                return applicationEvents;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the offline interview application exceptions.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ApplicationExceptionDetail"/> that holds the 
        /// application exception.
        /// </returns>
        public List<ApplicationExceptionDetail> GetApplicationExceptions
            (string candidateSessionID, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;

            // Create an application exception list.
            List<ApplicationExceptionDetail> applicationExceptions = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getExceptionsCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_EXCEPTIONS");

                getExceptionsCommand.CommandTimeout = 0;

                HCMDatabase.AddInParameter(getExceptionsCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getExceptionsCommand,
                    "@ATTEMPT_ID", DbType.String, attempID);

                HCMDatabase.AddInParameter(getExceptionsCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getExceptionsCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getExceptionsCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the application exceptions list.
                    if (applicationExceptions == null)
                        applicationExceptions = new List<ApplicationExceptionDetail>();

                    // Create an application exception object.
                    ApplicationExceptionDetail applicationException = new ApplicationExceptionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["EXCEPTION"]) &&
                        dataReader["EXCEPTION"] != DBNull.Value)
                    {
                        applicationException.CandidateSessionID = candidateSessionID;
                        applicationException.AttempID = attempID;
                        applicationException.Exception = dataReader["EXCEPTION"].ToString().Trim();

                        if (dataReader["CREATED_DATE"] != DBNull.Value && dataReader["CREATED_DATE"] != null)
                            applicationException.Date = dataReader["CREATED_DATE"].ToString().Trim();

                        // Add the application exception object to the list.
                        applicationExceptions.Add(applicationException);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }
                }
                dataReader.Close();
                return applicationExceptions;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the offline interview running application 
        /// details.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="ApplicationDetail"/> that holds the running
        /// application details.
        /// </returns>
        public List<ApplicationDetail> GetRunningApplications
            (string candidateSessionID, int attempID, int pageNumber,
            int pageSize, out int totalRecords)
        {

            IDataReader dataReader = null;
            totalRecords = 0;

            // Create a running application object list.
            List<ApplicationDetail> runningApplications = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getRunningApplicationsCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_RUNNING_APPLICATIONS");

                getRunningApplicationsCommand.CommandTimeout = 0;

                HCMDatabase.AddInParameter(getRunningApplicationsCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getRunningApplicationsCommand,
                    "@ATTEMPT_ID", DbType.String, attempID);

                HCMDatabase.AddInParameter(getRunningApplicationsCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getRunningApplicationsCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getRunningApplicationsCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the running applications list.
                    if (runningApplications == null)
                        runningApplications = new List<ApplicationDetail>();

                    // Create a running application object.
                    ApplicationDetail runningApplication = new ApplicationDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["PROCESS_NAME"]) &&
                         dataReader["PROCESS_NAME"] != DBNull.Value)
                    {
                        runningApplication.CandidateSessionID = candidateSessionID;
                        runningApplication.AttempID = attempID;
                        runningApplication.Application = dataReader["PROCESS_NAME"].ToString().Trim();

                        if (dataReader["EXE_PATH"] != DBNull.Value && dataReader["EXE_PATH"] != null)
                            runningApplication.ExePath = dataReader["EXE_PATH"].ToString().Trim();

                        // Add the running application object to the list.
                        runningApplications.Add(runningApplication);
                    }
                    else
                    {
                        int.TryParse(dataReader["TOTAL"].ToString().Trim(), out totalRecords);
                    }

                }
                dataReader.Close();

                return runningApplications;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
    }
}
