﻿#region Header

// Copyright (C) Forte. All rights reserved.

// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestConductionDLManager.cs
// File that represents the business layer for the Test Conduction module.
// This will talk to the data layer to perform the operations associated
// with the module.

#endregion

#region Directives
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the Test Conduction module.
    /// This includes functionalities for updating the test result , test status 
    ///  This will talk to the data layer for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class TestConductionDLManager : DatabaseConnectionManager
    {
        #region Public Method
        /// <summary>
        /// This method is used update the CANDIDATE_TEST_RESULT_FACT table.
        /// </summary>
        /// <param name="candidateTestResultFact">
        /// This parameter contains all the details like
        /// Start_Time, End_Time, Status etc
        /// </param>
        /// <param name="submittedAnswer">
        /// Submitted answer by candidate is passed to DB to compare with correct answer
        /// </param>
        public void UpdateCandidateTestResultFact(CandidateTestResultFact candidateTestResultFact, int submittedAnswer, string answer)
        {
            // Create a stored procedure command object.
            DbCommand insertTestResultCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CANDIDATE_TEST_RESULT_FACT");

            // Add input parameters.

            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@CAND_SESSION_KEY", DbType.String, candidateTestResultFact.CandidateSessionKey);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@ATTEMPT_ID", DbType.Int16, candidateTestResultFact.AttemptID);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@START_TIME", DbType.DateTime, candidateTestResultFact.StartTime);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@END_TIME", DbType.DateTime, candidateTestResultFact.EndTime);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@QUESTION_KEY", DbType.String, candidateTestResultFact.QuestionKey);

            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@STATUS", DbType.String, candidateTestResultFact.Status);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@SKIPPED", DbType.String, candidateTestResultFact.Skipped);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@SUBMITTED_ANSWER", DbType.Int32, submittedAnswer);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@ANSWER", DbType.String, answer);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@MODIFIED_BY", DbType.Int32, candidateTestResultFact.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestResultCommand);
        }

        /// <summary>
        /// This method is used update the CANDIDATE_TEST_RESULT table.
        /// </summary>
        /// <param name="candidateTestResultFact">
        /// This parameter contains all the details like
        /// Total_Questions_Viewed, Total_Questions_Not_Viewed, Status etc
        /// </param>
        public void UpdateCandidateTestResult(CandidateTestResult candidateTestResult)
        {
            // Create a stored procedure command object.
            DbCommand insertTestResultCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CANDIDATE_TEST_RESULT");

            // Add input parameters.

            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@CAND_SESSION_KEY", DbType.String, candidateTestResult.CandidateSessionKey);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@ATTEMPT_ID", DbType.Int16, candidateTestResult.AttemptID);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@END_TIME", DbType.DateTime, candidateTestResult.EndTime);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@STATUS", DbType.String, candidateTestResult.Status);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                 "@TEST_KEY", DbType.String, candidateTestResult.TestKey);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@TOTAL_QUESTION_VIEWED", DbType.Int16, candidateTestResult.TotalQuestionViewed);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@TOTAL_QUESTION_NOT_VIEWED", DbType.Int16, candidateTestResult.TotalQuestionNotViewed);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@MODIFIED_BY", DbType.Int32, candidateTestResult.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestResultCommand);
        }
        /// <summary>
        /// This method is used to get the Test_Question_Id and QuestionKey details
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate Session Key.
        /// </param>
        /// <returns>
        /// that contains the question Ids and test question id.
        /// </returns>
        public Dictionary<string, string> GetQuestionIDs(string candidateSessionKey)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getQuestionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_QUESTION_IDS");

                HCMDatabase.AddInParameter(getQuestionCommand,
               "@CAND_SESSION_KEY", DbType.String, candidateSessionKey);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getQuestionCommand);

                //List<string> questionIds = null;
                Dictionary<string, string> questionIds = null;


                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the questionId collection.
                    if (questionIds == null)
                        questionIds = new Dictionary<string, string>();

                    // Add the questionId to the collection.
                    questionIds.Add(dataReader["QUESTION_KEY"].ToString(), dataReader["TEST_QUESTION_ID"].ToString());
                }

                return questionIds;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method is used to get the all Question details by passing candSessionKey
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        /// <returns>
        /// A <see cref="QuestionDetail"/> that contains the Question Detail.
        /// </returns>
        public List<QuestionDetail> GetQuestions(string candSessionKey)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getQuestionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_QUESTIONS_BY_CANDSESSIONKEY");

                HCMDatabase.AddInParameter(getQuestionCommand, 
                    "@CANDIDATE_SESSION_KEY", DbType.String, candSessionKey);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getQuestionCommand);

                List<QuestionDetail> questionDetails = new List<QuestionDetail>();
                
                
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    QuestionDetail questionDetail = new QuestionDetail();
                    questionDetail.Question = dataReader["QUESTION"].ToString();
                    questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                    questionDetail.QuestionID = Convert.ToInt32(dataReader["TEST_QUESTION_ID"]);
                    questionDetail.IsAnswered = false;
                    questionDetail.IsSaveForLater = false;
                    QuestionAttribute questionAtrribute = new QuestionAttribute();
                    questionAtrribute.Answer = "";
                    questionDetail.QuestionAttribute = questionAtrribute;
                    questionDetails.Add(questionDetail);
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    // Close the data reader, if it is open.
                    dataReader.Close();
                }
                 
            }
        }

        /// <summary>
        /// This method is used to get the Question details by passing QuestionKey
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        /// <returns>
        /// A <see cref="QuestionDetail"/> that contains the Question Detail.
        /// </returns>
        public QuestionDetail GetQuestionByQuestionKey(string questionKey)
        {
            DataSet dataSet = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getQuestionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_QUESTION_BY_QUESTION_KEY");

                HCMDatabase.AddInParameter(getQuestionCommand,
               "@QUESTION_KEY", DbType.String, questionKey);

                // Execute the stored procedure.
                dataSet = HCMDatabase.ExecuteDataSet(getQuestionCommand);

                QuestionDetail questionDetail = null;
                AnswerChoice answerChoice = null;
                // Read the records from the dataset.
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    // Instantiate the categories collection.
                    if (questionDetail == null)
                        questionDetail = new QuestionDetail();

                    // Assign the question details
                    questionDetail.QuestionID = Convert.ToInt32(dataSet.Tables[0].Rows[0]["QUESTION_ID"].ToString());
                    questionDetail.QuestionType = (QuestionType)Convert.ToInt32(dataSet.Tables[0].Rows[0]["QUESTION_TYPE"].ToString());
                    questionDetail.QuestionKey = questionKey;
                    questionDetail.Question = dataSet.Tables[0].Rows[0]["QUESTION_DESC"].ToString();
                    if (Utility.IsNullOrEmpty(dataSet.Tables[0].Rows[0]["ILLUSTRATION"]))
                        questionDetail.HasImage = false;
                    else
                    {
                        questionDetail.QuestionImage = dataSet.Tables[0].Rows[0]["ILLUSTRATION"] as byte[];
                        questionDetail.HasImage = true;
                    }
                }
                if (dataSet.Tables[1].Rows.Count != 0)
                {
                    string formatAnswerChoice = string.Empty;
                    questionDetail.AnswerChoices = new List<AnswerChoice>();
                    int alpha = 65;
                    for (int i = 0; i < dataSet.Tables[1].Rows.Count; i++)
                    {
                        formatAnswerChoice = dataSet.Tables[1].Rows[i]["CHOICE_DESC"].ToString();
                        formatAnswerChoice = formatAnswerChoice == null ? formatAnswerChoice : formatAnswerChoice.ToString().Replace(Environment.NewLine, "<br />");
                        answerChoice = new AnswerChoice(((char)alpha).ToString() + ". " + formatAnswerChoice, false);
                        alpha++;
                        answerChoice.ChoiceID = Convert.ToInt32(dataSet.Tables[1].Rows[i]["CHOICE_ID"].ToString());
                        answerChoice.QuestionOptionId = Convert.ToInt32(dataSet.Tables[1].Rows[i]["QUESTION_OPTION_ID"].ToString());
                        questionDetail.AnswerChoices.Add(answerChoice);
                    }
                }

                if (dataSet.Tables[2].Rows.Count != 0)
                {
                    questionDetail.QuestionAttribute = new QuestionAttribute();
                    questionDetail.QuestionAttribute.MaxLength = Convert.ToInt32(dataSet.Tables[2].Rows[0]["MAX_LENGTH"]);
                    questionDetail.QuestionAttribute.Marks = Convert.ToInt32(dataSet.Tables[2].Rows[0]["MARKS"]);
                    //questionDetail.QuestionAttribute.AnswerReference = dataSet.Tables[2].Rows[0]["ANSWER_REFERENCE"].ToString().Trim();
                }

                return questionDetail;
            }
            finally
            {
                // Clear the data set, if it is not null.
                if (dataSet != null)
                {
                    dataSet.Clear();
                }
            }
        }

        /// <summary>
        /// Method that will insert the certification image.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attemptID">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <param name="imageData">
        /// A <see cref="byte"/> that contains the image data as byte.
        /// </param>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        public void InsertCertificateImage(string candidateSessionKey,
            int attemptID, byte[] imageData, int user)
        {
            // Create a stored procedure command object.
            DbCommand insertCertificateImageCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_TEST_CERTIFICATE_IMAGE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCertificateImageCommand,
                "@CAND_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(insertCertificateImageCommand,
                "@ATTEMPT_ID", DbType.Int16, attemptID);
            HCMDatabase.AddInParameter(insertCertificateImageCommand,
                "@CERTIFICATE", DbType.Binary, imageData);
            HCMDatabase.AddInParameter(insertCertificateImageCommand,
                "@CREATED_BY", DbType.Int32, user);

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(insertCertificateImageCommand);
        }

        /// <summary>
        /// This method stores the test details in DB
        /// </summary>
        /// <param name="candidateTestResult">
        /// A <see cref="CandidateTestResult"/> that contains the Candidate Test Result.
        /// </param>
        public void InsertCandidateTestResult(CandidateTestResult candidateTestResult)
        {
            // Create a stored procedure command object.
            DbCommand insertTestResultCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CANDIDATE_TEST_RESULT");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@CANDIDATE_RESULT_KEY", DbType.String, candidateTestResult.CandidateResultKey);

            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@CAND_SESSION_KEY", DbType.String, candidateTestResult.CandidateSessionKey);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@ATTEMPT_ID", DbType.Int16, candidateTestResult.AttemptID);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@TEST_KEY", DbType.String, candidateTestResult.TestKey);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@START_TIME", DbType.DateTime, candidateTestResult.StartTime);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                 "@STATUS", DbType.String, candidateTestResult.Status);

            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@CREATED_BY", DbType.Int32, candidateTestResult.CreatedBy);
            HCMDatabase.AddInParameter(insertTestResultCommand,
                "@MODIFIED_BY", DbType.Int32, candidateTestResult.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestResultCommand);
        }

        /// <summary>
        /// This method is called when the test is started. 
        /// It passes the inprogress status to store it in DB
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate Session Key.
        /// </param>
        /// <param name="attemptId">
        /// A <see cref="CandidateTestResult"/> that contains the Candidate Test Result.
        /// </param>
        /// <param name="sessionTrackingStatus">
        /// A <see cref="string"/> that contains the values can be ATMPT_INPR
        /// </param>
        /// <param name="sessionCandidateStatus">
        /// A <see cref="string"/> that contains the values can be SESS_INPR
        /// </param>
        /// <param name="modifiedBy">
        /// A <see cref="int"/> that contains the Logged in user id.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the Test Session Detail.
        /// </returns>
        public TestSessionDetail StartTestConduction(string candidateSessionKey, int attemptId, string sessionTrackingStatus, string sessionCandidateStatus, int modifiedBy)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand command = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_TEST_CONDUCTION_STATUS");

                // Add input parameters.
                HCMDatabase.AddInParameter(command,
                    "@CAND_SESSION_KEY", DbType.String, candidateSessionKey);
                HCMDatabase.AddInParameter(command,
                    "@ATTEMPT_ID", DbType.Int16, attemptId);
                HCMDatabase.AddInParameter(command,
                    "@SESSION_TRACKING_STATUS", DbType.String, sessionTrackingStatus);
                HCMDatabase.AddInParameter(command,
                    "@SESSION_CANDIDATE_STATUS", DbType.String, sessionCandidateStatus);
                HCMDatabase.AddInParameter(command,
                    "@MODIFIED_BY", DbType.Int32, modifiedBy);
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(command);

                TestSessionDetail testSession = null;

                // Read the records from the data reader.
                if (dataReader.Read())
                {
                    if (!(Utility.IsNullOrEmpty(dataReader["IS_TEST_INPROGRESS"])))
                        if (testSession == null)
                            testSession = new TestSessionDetail();

                    testSession.CompletedTests = dataReader["IS_TEST_INPROGRESS"].ToString();
                    //throw new Exception("Test is either in progress or already taken by the candidate");
                    if (dataReader["IS_TEST_INPROGRESS"].ToString() == "N")
                    {
                            dataReader.NextResult();
                            dataReader.Read();

                            // Instantiate the testSession collection.
                            
                            testSession.TestID = dataReader["TEST_KEY"].ToString();
                            if (!(Utility.IsNullOrEmpty(dataReader["TIME_LIMIT"])))
                                testSession.TimeLimit = Convert.ToInt32(dataReader["TIME_LIMIT"].ToString());
                            testSession.IsDisplayResultsToCandidate =
                                (dataReader["DISPLAY_RESULT"].ToString() == "Y" ? true : false);
                            if (!(Utility.IsNullOrEmpty(dataReader["TEST_NAME"])))
                                testSession.TestName = dataReader["TEST_NAME"].ToString();
                        }
                }

                return testSession;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Method that will update the test status for the given candidate session id
        /// and attempt id.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attemptId">
        /// An <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <param name="sessionTrackingStatus">
        /// A <see cref="string"/> that contains the session tracking status.
        /// </param>
        /// <param name="sessionCandidateStatus">
        /// A <see cref="string"/> that contains the candidate session status.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        public void UpdateSessionStatus(string candidateSessionKey, int attemptId,
            string sessionTrackingStatus, string sessionCandidateStatus, int modifiedBy)
        {
            // Create a stored procedure command object.
            DbCommand command = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_SESSION_STATUS");

            // Add input parameters.
            HCMDatabase.AddInParameter(command,
                "@CAND_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(command,
                "@ATTEMPT_ID", DbType.Int16, attemptId);
            HCMDatabase.AddInParameter(command,
                "@SESSION_TRACKING_STATUS", DbType.String, sessionTrackingStatus);
            HCMDatabase.AddInParameter(command,
                "@SESSION_CANDIDATE_STATUS", DbType.String, sessionCandidateStatus);
            HCMDatabase.AddInParameter(command,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(command);
        }

        /// <summary>
        /// Method that will update the candidate session status if he is unscheduled.
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate session detail.
        /// </param>
        /// <param name="candidateAttemptStatus">
        /// A <see cref="string"/> that contains the candidate attempt status.
        /// </param>
        /// <param name="candidateSessionStatus">
        /// A <see cref="string"/> that contains the candidate session status.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        public void UpdateSessionStatus(CandidateTestSessionDetail candidateTestSessionDetail,
            string candidateAttemptStatus, string candidateSessionStatus, int modifiedBy)
        {
            // Create a stored procedure command object.
            DbCommand command = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_UNSCHEDULE_STATUS_BY_CANDIDATE_SESSION_KEY");

            // Add input parameters.
            HCMDatabase.AddInParameter(command,
                "@CAND_SESSION_KEY", DbType.String, candidateTestSessionDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(command,
                "@ATTEMPT_ID", DbType.Int16, candidateTestSessionDetail.AttemptID);
            HCMDatabase.AddInParameter(command,
                "@SESSION_STATUS", DbType.String, candidateAttemptStatus);
            HCMDatabase.AddInParameter(command,
                "@STATUS", DbType.String, candidateSessionStatus);
            HCMDatabase.AddInParameter(command,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(command);
        }

        /// <summary>
        /// Method that updates the candidate rating.
        /// </summary>
        /// <param name="ratingDetail">
        /// A <see cref="CandidateRatingDetail"/> that contains the candidate rating detail.
        /// </param>
        public void UpdateRating(CandidateRatingDetail ratingDetail)
        {
            // Create a stored procedure command object.
            DbCommand command = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CANDIDATE_SESSION_RATING");

            // Add input parameters.
            HCMDatabase.AddInParameter(command,
                "@CAND_SESSION_KEY", DbType.String, ratingDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(command,
                "@RATING", DbType.Int16, ratingDetail.Rating);
            HCMDatabase.AddInParameter(command,
                "@FEEDBACK", DbType.String, ratingDetail.Feedback);
            HCMDatabase.AddInParameter(command,
                "@MODIFIED_BY", DbType.Int32, ratingDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(command);
        }


        public void AddImageDetails(string canSessionKey, int attemptID, string imageType,
           byte[] image, int createdBy, int modifiedBy, out Int64 CP_IMAGE_ID)
        {


            //IDataReader dataReader = null;

            DbCommand InsertCyberProctorImage = HCMDatabase.GetStoredProcCommand("SPINSERT_CYBER_PROCTOR_IMAGE");

            InsertCyberProctorImage.CommandTimeout = 0;
            HCMDatabase.AddInParameter(InsertCyberProctorImage, "@CAND_SESSION_KEY",
                System.Data.DbType.String, canSessionKey);
            HCMDatabase.AddInParameter(InsertCyberProctorImage, "@ATTEMPT_ID",
            System.Data.DbType.Int64, attemptID);
            HCMDatabase.AddInParameter(InsertCyberProctorImage, "@CP_IMAGE",
            System.Data.DbType.Binary, image);

            HCMDatabase.AddInParameter(InsertCyberProctorImage, "@IMAGE_TYPE",
               System.Data.DbType.String, imageType);
            HCMDatabase.AddInParameter(InsertCyberProctorImage, "@CREATED_BY",
                System.Data.DbType.Int32, createdBy);
            HCMDatabase.AddInParameter(InsertCyberProctorImage, "@MODIFIED_BY",
                System.Data.DbType.Int32, modifiedBy);
            HCMDatabase.AddOutParameter(InsertCyberProctorImage, "@CP_IMAGE_ID", System.Data.DbType.Int64, 10);
            //execute the command to insert the image
            HCMDatabase.ExecuteNonQuery(InsertCyberProctorImage);//, transaction as DbTransaction);
            // transaction.Commit();
            // database.ExecuteNonQuery(InsertCyberProctorImage);
            CP_IMAGE_ID = Convert.ToInt64(HCMDatabase.GetParameterValue(InsertCyberProctorImage, "@CP_IMAGE_ID").ToString());


            DbCommand InsertCyberProctorImageDetail = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CYBER_PROCTOR_IMAGE_DETAIL");

            InsertCyberProctorImageDetail.CommandTimeout = 0;
            HCMDatabase.AddInParameter(InsertCyberProctorImageDetail, "@CP_IMAGE_ID",
               System.Data.DbType.Int64, CP_IMAGE_ID);
            HCMDatabase.AddInParameter(InsertCyberProctorImageDetail, "@CP_IMAGE",
                System.Data.DbType.Binary, image);
            HCMDatabase.AddInParameter(InsertCyberProctorImageDetail, "@CREATED_BY",
                System.Data.DbType.String, createdBy);
            HCMDatabase.AddInParameter(InsertCyberProctorImageDetail, "@MODIFIED_BY",
                System.Data.DbType.String, modifiedBy);
            //execute the command to insert the image
            HCMDatabase.ExecuteNonQuery(InsertCyberProctorImageDetail);//, transaction as DbTransaction);
            //transaction.Commit();


        }
        #endregion Public Method
    }
}
