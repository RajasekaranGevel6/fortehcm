﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionDLManager.cs
// File that represents the data layer for the interview question 
// repository module. This includes functionalities for create, retrieve, 
// update, delete questions. This will talk to the database for performing
// these operations.

#endregion

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the interview question 
    /// repository module. This includes functionalities for create, retrieve, 
    /// update, delete questions. This will talk to the database for performing
    /// these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class InterviewQuestionDLManager : DatabaseConnectionManager
    {
        /// <summary>
        /// Method that retrieves the list of test for the given question key. 
        /// criteria.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that holds the TestDetails regarding this question.
        /// </returns>
        public List<TestDetail> GetQuestionUsageSummary(string questionKey,
            int pageNumber, int pageSize, string orderBy, SortType orderByDirection,
            out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {

                DbCommand getQuestionUsageSummaryCommand =
                       HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_QUESTION_SUMMARY");
                // Add parameters.
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@QUESTIONKEY", DbType.String, questionKey.ToString());
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getQuestionUsageSummaryCommand);
                List<TestDetail> testDetails = null;

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();

                    TestDetail testDetail = new TestDetail();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        testDetail.Name = dataReader["INTERVIEW_TEST_NAME"].ToString();
                        testDetail.TestKey = dataReader["INTERVIEW_TEST_KEY"].ToString();
                        testDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());
                        testDetail.NoOfCandidatesAdministered = int.Parse(dataReader["TEST_ADMINSTRED_COUNT"].ToString());
                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }

                }
                dataReader.Close();
                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Get question image by giving a question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        ///  A <see cref="byte[]"/> that holds the question image. 
        /// </returns>
        public byte[] GetInterviewQuestionImage(string questionKey)
        {
            DbCommand getQuestionImageCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_QUESTION_IMAGE");

            // Add parameters.
            HCMDatabase.AddInParameter(getQuestionImageCommand,
                "@QUESTION_KEY", DbType.String, questionKey.ToString());

            // Execute command.
            byte[] questionImage = HCMDatabase.ExecuteScalar(getQuestionImageCommand) as byte[];

            return questionImage;
        }
    }
}
