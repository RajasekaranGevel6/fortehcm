﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.DL
{
    public class AssessmentSummaryDLManager : DatabaseConnectionManager
    {
        public AssessmentSummary GetAssessorSummary(string candidateInterviewKey,
            int attemptID, int assessorID,char orderBy)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getAssessorSummaryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_ASSESSMENT_SUMMARY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getAssessorSummaryCommand,
                    "@CANDIDATE_INTERVIEW_KEY", DbType.String, candidateInterviewKey);
                HCMDatabase.AddInParameter(getAssessorSummaryCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);
                HCMDatabase.AddInParameter(getAssessorSummaryCommand,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);
                HCMDatabase.AddInParameter(getAssessorSummaryCommand,
                   "@SORT_EXPRESSION", DbType.String,orderBy);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessorSummaryCommand);

                AssessmentSummary assessmentSummary = null;
                List<QuestionDetail> assessmentSummaryQuestionDetails = null;
                List<AssessorDetail> assessorDetails = null;
                while (dataReader.Read())
                {
                    // Instantiate the testSession instance
                    if (assessmentSummary == null)
                        assessmentSummary = new AssessmentSummary();
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        assessmentSummary.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INFORMATION_ID"]))
                        assessmentSummary.CandidateInfoID = Convert.ToInt32(dataReader["CANDIDATE_INFORMATION_ID"].ToString().Trim());

                    assessmentSummary.CanidateName = dataReader["CAND_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                        assessmentSummary.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        assessmentSummary.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        assessmentSummary.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString().Trim());

                    assessmentSummary.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_DEPARTMENTS"]))
                        assessmentSummary.ClientDepartments = dataReader["CLIENT_DEPARTMENTS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_CONTACTS"]))
                        assessmentSummary.ClientContacts = dataReader["CLIENT_CONTACTS"].ToString().Trim();

                    assessmentSummary.AssessmentStatus = dataReader["ASSESMENT_STATUS"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["OVERALL_RATING"]))
                        assessmentSummary.OverallRating = Convert.ToDecimal(dataReader["OVERALL_RATING"].ToString().Trim());
                    assessmentSummary.Comments = dataReader["COMMENTS"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_ON"]))
                        assessmentSummary.CompletedOn = Convert.ToDateTime(dataReader["COMPLETED_ON"].ToString().Trim());
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    if (assessorDetails == null)
                        assessorDetails = new List<AssessorDetail>();
                    AssessorDetail assessorDetail = new AssessorDetail();

                    assessorDetail.AssessmentSatus = dataReader["ASSESMENT_STATUS"].ToString().Trim();
                    assessorDetail.Comments = dataReader["COMMENTS"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["OVERALL_RATING"]))
                        assessorDetail.assessorRating = Convert.ToDecimal(dataReader["OVERALL_RATING"].ToString().Trim());
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_ON"]))
                        assessorDetail.CompletedOn = Convert.ToDateTime(dataReader["COMPLETED_ON"].ToString().Trim());
                    assessorDetail.FirstName = dataReader["ASS_NAME"].ToString().Trim();

                    assessorDetails.Add(assessorDetail);
                }
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (assessmentSummaryQuestionDetails == null)
                        assessmentSummaryQuestionDetails = new List<QuestionDetail>();

                    QuestionDetail questionDetail = new QuestionDetail();
                    questionDetail.CandidateInterviewSessionKey = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        questionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    questionDetail.CandidateResultKey = dataReader["CANDIDATE_RESULT_KEY"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_QUESTION_ID"]))
                        questionDetail.TestQuestionID = Convert.ToInt32(dataReader["TEST_QUESTION_ID"].ToString().Trim());
                    questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString().Trim();
                    questionDetail.InterviewQuestionStatus = dataReader["STATUS"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["RATING"]))
                        questionDetail.Rating = Convert.ToDecimal(dataReader["RATING"].ToString().Trim());
                    questionDetail.Skipped = dataReader["SKIPPED"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"]))
                        questionDetail.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"].ToString().Trim());
                    questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                    questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["RATINGEDIT"]))
                        questionDetail.RatingEdit =  dataReader["RATINGEDIT"].ToString().Trim();

                    assessmentSummaryQuestionDetails.Add(questionDetail);
                }

                assessmentSummary.QuestionDetails = new List<QuestionDetail>();
                assessmentSummary.AssessorDetails = new List<AssessorDetail>();
                assessmentSummary.QuestionDetails = assessmentSummaryQuestionDetails;
                assessmentSummary.AssessorDetails = assessorDetails;
                return assessmentSummary;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="candidateInterviewKey"></param>
        /// <param name="attemptID"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public AssessmentSummary GetAssessorSummaryPublish(string candidateInterviewKey,
            int attemptID, char orderBy)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getAssessorSummaryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_ASSESSMENT_SUMMARY_PUBLISH");

                // Add input parameters.
                HCMDatabase.AddInParameter(getAssessorSummaryCommand,
                    "@CANDIDATE_INTERVIEW_KEY", DbType.String, candidateInterviewKey);
                HCMDatabase.AddInParameter(getAssessorSummaryCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);
                HCMDatabase.AddInParameter(getAssessorSummaryCommand,
                   "@SORT_EXPRESSION", DbType.String, orderBy);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getAssessorSummaryCommand);

                AssessmentSummary assessmentSummary = null;
                List<QuestionDetail> assessmentSummaryQuestionDetails = null;
                while (dataReader.Read())
                {
                    // Instantiate the testSession instance
                    if (assessmentSummary == null)
                        assessmentSummary = new AssessmentSummary();
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        assessmentSummary.CandidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INFORMATION_ID"]))
                        assessmentSummary.CandidateInfoID = Convert.ToInt32(dataReader["CANDIDATE_INFORMATION_ID"].ToString().Trim());

                    assessmentSummary.CanidateName = dataReader["CAND_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                        assessmentSummary.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        assessmentSummary.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        assessmentSummary.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString().Trim());

                    assessmentSummary.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();
                }
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (assessmentSummaryQuestionDetails == null)
                        assessmentSummaryQuestionDetails = new List<QuestionDetail>();

                    QuestionDetail questionDetail = new QuestionDetail();
                    questionDetail.CandidateInterviewSessionKey = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        questionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    questionDetail.CandidateResultKey = dataReader["CANDIDATE_RESULT_KEY"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_QUESTION_ID"]))
                        questionDetail.TestQuestionID = Convert.ToInt32(dataReader["TEST_QUESTION_ID"].ToString().Trim());
                    questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString().Trim();
                    questionDetail.InterviewQuestionStatus = dataReader["STATUS"].ToString().Trim();
                    questionDetail.Skipped = dataReader["SKIPPED"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"]))
                        questionDetail.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"].ToString().Trim());
                    questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                    questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();

                    assessmentSummaryQuestionDetails.Add(questionDetail);
                }

                assessmentSummary.QuestionDetails = new List<QuestionDetail>();
                assessmentSummary.QuestionDetails = assessmentSummaryQuestionDetails;
                return assessmentSummary;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Update the candidate interview score  code.
        /// </summary>
        /// <param name="cansessionKey">
        /// A <see cref="string"/> that contains candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains attempt ID.
        /// </param>
        /// <param name="scoreCode">
        /// A <see cref="string"/> that contains scoreCode code.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that contains scoreCode is true or false.
        /// </returns>
        public InterviewScoreParamDetail UpdateCandidateInterviewScoreCode(string cansessionKey, int attempID, string scoreCode)
        {
            InterviewScoreParamDetail interviewScoreParamDetail = null;
            if (interviewScoreParamDetail == null)
                interviewScoreParamDetail = new InterviewScoreParamDetail();
            
            // Create a stored procedure command object.
            DbCommand interviewScoreCode = HCMDatabase.
                 GetStoredProcCommand("SPUPDATE_CANDIDATE_INTERVIEW_PUBLISH_SCORE_CODE");

            interviewScoreCode.CommandTimeout = 0;

            HCMDatabase.AddInParameter(interviewScoreCode,
                "CAND_INTERVIEW_SESSION_KEY", System.Data.DbType.String, cansessionKey);

            HCMDatabase.AddInParameter(interviewScoreCode,
               "ATTEMPT_ID", System.Data.DbType.Int32, attempID);

            HCMDatabase.AddInParameter(interviewScoreCode,
               "@SCORE_CODE", System.Data.DbType.String, scoreCode);

            HCMDatabase.AddOutParameter(interviewScoreCode,
               "@EXT_SCORE_CODE", System.Data.DbType.String, 15);

            HCMDatabase.AddOutParameter(interviewScoreCode,
               "@ISEXIST", System.Data.DbType.String, 5);


            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(interviewScoreCode);
            if (!Utility.IsNullOrEmpty(interviewScoreCode.Parameters["@EXT_SCORE_CODE"].Value))
            {
                interviewScoreParamDetail.ScoreCode = Convert.ToString
                           (HCMDatabase.GetParameterValue(interviewScoreCode, "@EXT_SCORE_CODE"));
            }

            if ((!Utility.IsNullOrEmpty(interviewScoreCode.Parameters["@ISEXIST"].Value) &&
                interviewScoreCode.Parameters["@ISEXIST"].Value.ToString().ToUpper() == "Y"))
            {
                interviewScoreParamDetail.IsCodeExist = true;
            }
            else
            {
                interviewScoreParamDetail.IsCodeExist = false;
            }

            return interviewScoreParamDetail;
        }
        
        /// <summary>
        /// Method to get the candidate session details
        /// </summary>
        /// <param name="scoreCode"></param>
        /// <returns></returns>
        public InterviewScoreParamDetail GetCandidateInterviewPublishDetails(string scoreCode)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateInterviewPublishCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_CANDIDATE_PUBLISH_BY_SCORE_CODE");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateInterviewPublishCommand,
                    "@SCORE_CODE", DbType.String, scoreCode);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateInterviewPublishCommand);

                InterviewScoreParamDetail interviewScoreParamDetail = null;
                
                if(dataReader.Read())
                {
                    // Instantiate the candidate interview details instance
                    if (interviewScoreParamDetail == null)
                        interviewScoreParamDetail = new InterviewScoreParamDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        interviewScoreParamDetail.InterviewKey = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        interviewScoreParamDetail.CandidateInterviewSessionKey = 
                            dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        interviewScoreParamDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                }

                return interviewScoreParamDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public void SaveCandidateAssessmentRatngComments(AssessmentSummary assessmentSummary, int assessorID,
            string candidateInterviewSessionKey, int attemptID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertCandidateStatusCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_INTERVIEWCANDIDATE_ASSESSMENT_TRACKING");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertCandidateStatusCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);
            HCMDatabase.AddInParameter(insertCandidateStatusCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateInterviewSessionKey);
            HCMDatabase.AddInParameter(insertCandidateStatusCommand,
               "@ATTEMPT_ID", DbType.Int32, attemptID);
            HCMDatabase.AddInParameter(insertCandidateStatusCommand,
                "@ASSESMENT_STATUS", DbType.String, assessmentSummary.AssessmentStatus);
            HCMDatabase.AddInParameter(insertCandidateStatusCommand,
               "@COMMENTS", DbType.String, assessmentSummary.Comments);
            HCMDatabase.AddInParameter(insertCandidateStatusCommand,
               "@OVERALL_RATING", DbType.Int16, assessmentSummary.OverallRating);
            HCMDatabase.AddInParameter(insertCandidateStatusCommand,
               "@COMPLETED_ON", DbType.DateTime, assessmentSummary.CompletedOn 
                                == DateTime.MinValue ? null : assessmentSummary.CompletedOn);
            HCMDatabase.AddInParameter(insertCandidateStatusCommand,
                "@USER_ID", DbType.Int32, assessmentSummary.CreatedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertCandidateStatusCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the rating summary for the given candidate 
        /// session ID and attempt ID.
        /// </summary>
        /// <param name="candidateSesssionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="DataSet"/> that holds the summary data.
        /// </returns>
        public DataSet GetRatingSummary(string candidateSesssionID, int attemptID)
        {
            DbCommand skillMatrixCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_INTERVIEW_ASSESSMENT_REPORT");

            HCMDatabase.AddInParameter(skillMatrixCommand,
                "@CANDIDATE_INTERVIEW_KEY", DbType.String, candidateSesssionID);

            HCMDatabase.AddInParameter(skillMatrixCommand,
                "@ATTEMPT_ID", DbType.Int32, attemptID);

            // Execute the stored procedure.
            return HCMDatabase.ExecuteDataSet(skillMatrixCommand);
        }

        /// <summary>
        /// Method that updates the candidate interview score
        /// </summary>
        /// <param name="interviewTestKey"></param>
        /// <param name="candidateInterviewSessionKey"></param>
        /// <param name="attempID"></param>
        /// <param name="totalSubjectScore"></param>
        /// <param name="totalSubjectWeightageScore"></param>
        /// <param name="userID"></param>
        public void UpdateCanidateInterviewScore(string interviewTestKey,string candidateInterviewSessionKey,
            int attempID,decimal totalSubjectScore,decimal totalSubjectWeightageScore,int userID)
        {
            DbCommand updateScoreCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_CANDIDATE_INTERVIEW_SCORE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateScoreCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, interviewTestKey);

            HCMDatabase.AddInParameter(updateScoreCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateInterviewSessionKey);

            HCMDatabase.AddInParameter(updateScoreCommand,
                "@ATTEMPT_ID", DbType.Int32, attempID);

            HCMDatabase.AddInParameter(updateScoreCommand,
                "@TOTAL_SUBJECT_SCORE", DbType.Decimal, totalSubjectScore);

            HCMDatabase.AddInParameter(updateScoreCommand,
                "@TOTAL_SUBJECT_WEIGHTAGE_SCORE", DbType.Decimal, totalSubjectWeightageScore);

            HCMDatabase.AddInParameter(updateScoreCommand,
                "@USER_ID", DbType.Int32, userID);


            HCMDatabase.ExecuteNonQuery(updateScoreCommand);
        }

        /// <summary>
        /// Method that retrieves the rating summary for the given interview id
        /// </summary>
        /// <param name="onlineInterviewId">
        /// A<see cref="int"/> that hold the candidate interview id
        /// </param>
        /// <returns>
        /// A<see cref="Dataset"/> that holds the summary data
        /// </returns>
        public DataSet GetOnlineInterviewAssessorySummary(int onlineInterviewId)
        {
            DbCommand summaryCommand = HCMDatabase.GetStoredProcCommand
               ("SPGET_ONLINE_INTERVIEW_ASSESSMENT_REPORT");

            HCMDatabase.AddInParameter(summaryCommand,
             "@CAND_ONLINE_INTERVIEW_ID", DbType.Int32, onlineInterviewId);

            // Execute the stored procedure.
            return HCMDatabase.ExecuteDataSet(summaryCommand);

        }
    }
}
