﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionDLManager.cs
// File that represents the data layer for the Question respository Manager.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Question respository management.
    /// This includes functionalities for retrieving,updating,delete and add 
    /// values for Question. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class QuestionDLManager : DatabaseConnectionManager
    {
        #region Public method                                                  

        /// <summary>
        /// This method handles the new question entry functions
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="string"/>that holds the question detail collection
        /// </param>
        /// <param name="transaction">
        /// A <see cref="string"/>that holds the transaction object
        /// </param>
        public void InsertQuestion(QuestionDetail questionDetail, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_QUESTION");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@QUESTION_KEY", DbType.String, questionDetail.QuestionKey);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@QUESTION_TYPE", DbType.Int16, (int) questionDetail.QuestionType);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@QUESTION_DESC", DbType.String, questionDetail.Question);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@COMPLEXITY_ID", DbType.String, questionDetail.Complexity);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@TAGS", DbType.String, questionDetail.Tag);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CREDIT_EARNED", DbType.Decimal, questionDetail.CreditsEarned);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@NO_OF_CHOICES", DbType.Int16, questionDetail.NoOfChoices);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ANSWER", DbType.Int16, questionDetail.Answer);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@AUTHOR", DbType.Int32, questionDetail.Author);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@TEST_AREA_ID", DbType.String, questionDetail.TestAreaID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CREATED_BY", DbType.Int32, questionDetail.CreatedBy);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@MODIFIED_BY", DbType.Int32, questionDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method handles the new question entry functions
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="string"/>that holds the question detail collection
        /// </param>
        /// <param name="transaction">
        /// A <see cref="string"/>that holds the transaction object
        /// </param>
        public void InsertQuestionAttribute(QuestionDetail questionDetail, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_QUESTION_ATTRIBUTE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@QUESTION_KEY", DbType.String, questionDetail.QuestionKey);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@MAX_LENGTH", DbType.Int32, questionDetail.QuestionAttribute.MaxLength);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@MARKS", DbType.Int32, questionDetail.QuestionAttribute.Marks);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@ANSWER_REFERENCE", DbType.String, questionDetail.QuestionAttribute.AnswerReference);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method is used to insert answer choices against the given question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question key
        /// </param>
        /// <param name="answerChoice">
        /// A <see cref="AnswerChoice"/>that holds the answer choice
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/>that holds the user id
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/>that holds the transaction object
        /// </param>
        public void InsertAnswerChoice(string questionKey,
            AnswerChoice answerChoice, int user, IDbTransaction transaction)
        {
            DbCommand insertAnswerChoiceCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_OPTIONS");

            // Add input paramater.
            HCMDatabase.AddInParameter(insertAnswerChoiceCommand,
                   "@CHOICE_ID", DbType.Int32, answerChoice.ChoiceID);
            HCMDatabase.AddInParameter(insertAnswerChoiceCommand,
                   "@CHOICE_DESC", DbType.String, answerChoice.Choice);
            HCMDatabase.AddInParameter(insertAnswerChoiceCommand,
                   "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertAnswerChoiceCommand,
                "@USER_ID", DbType.Int32, user);

            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAnswerChoiceCommand, transaction as DbTransaction);
        }

      
        /// <summary>
        /// This method is used to insert the question image
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question key
        /// </param>
        /// <param name="questionImage">
        /// A <see cref="byte"/>that holds the question image
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/>that holds the user id
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/>that holds the transaction object
        /// </param>
        public void InsertQuestionImage(string questionKey,byte[] questionImage,
            int user, IDbTransaction transaction)
        {
            DbCommand insertQuestionImageCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_QUESTION_IMAGE");

            // Add input paramater.
            HCMDatabase.AddInParameter(insertQuestionImageCommand,
                   "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertQuestionImageCommand,
                   "@ILLUSTRATION", DbType.Binary, questionImage);
            HCMDatabase.AddInParameter(insertQuestionImageCommand,
                "@CREATED_BY", DbType.Int32, user);
            HCMDatabase.AddInParameter(insertQuestionImageCommand,
                "@MODIFIED_BY", DbType.Int32, user);

            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertQuestionImageCommand, transaction as DbTransaction);
        }
        /// <summary>
        /// This method helps to insert question relation according to the question and subject ids
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="listOfSubjectIDs">
        /// A <see cref="string"/>that holds the list of subject Id
        /// </param>
        /// <param name="createdBy">
        /// A <see cref="int"/>that holds the creadted by
        /// </param>
        /// <param name="modifiedBy">
        /// A <see cref="int"/> that holds the modified by
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object
        /// </param>
        public void InsertQuestionRelation(string questionKey, string listOfSubjectIDs,
            int createdBy, int modifiedBy, IDbTransaction transaction)
        {
            DbCommand insertQuestionRelationCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_QUESTION_RELATION");

            // Add input paramater.
            HCMDatabase.AddInParameter(insertQuestionRelationCommand,
                   "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertQuestionRelationCommand,
                   "@CAT_SUB_ID", DbType.String, listOfSubjectIDs);
            HCMDatabase.AddInParameter(insertQuestionRelationCommand,
                   "@CREATED_BY", DbType.Int32, createdBy);
            HCMDatabase.AddInParameter(insertQuestionRelationCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);

            HCMDatabase.ExecuteNonQuery(insertQuestionRelationCommand, transaction as DbTransaction);
        }



        /// <summary>
        /// Get question details by giving a question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        ///  A <see cref="QuestionDetail"/> that holds the question details. 
        /// </returns>
        public QuestionDetail GetQuestion(string questionKey)
        {
            IDataReader dataReader = null;
            try
            {
                QuestionDetail questionDetail = new QuestionDetail();

                DbCommand getSingleQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_QUESTION_DETAILS");

                // Add parameters.
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@QUESTION_KEY", DbType.String, questionKey.ToString());

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getSingleQuestionDetailCommand);

                // Retrieve basic question details using SPGET_QUESTION_HEADER stored procedure
                while (dataReader.Read())
                {
                    questionDetail.QuestionKey = questionKey.Trim();
                    questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                    questionDetail.Complexity = dataReader["COMPLEXITY_ID"].ToString().Trim();
                    questionDetail.ComplexityName = dataReader["COMPLEXITY_NAME"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["ILLUSTRATION"]))
                    {
                        questionDetail.QuestionImage = dataReader["ILLUSTRATION"] as byte[];
                        questionDetail.HasImage = true;
                    }
                    else
                        questionDetail.HasImage = false;

                    questionDetail.Tag = dataReader["TAGS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CREDIT_EARNED"]))
                        questionDetail.CreditsEarned = decimal.Parse(dataReader["CREDIT_EARNED"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_CHOICES"]))
                        questionDetail.NoOfChoices = Convert.ToInt16(dataReader["NO_OF_CHOICES"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER"]))
                        questionDetail.Answer = Convert.ToInt16(dataReader["ANSWER"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR"]))
                        questionDetail.Author = int.Parse(dataReader["AUTHOR"].ToString());

                    questionDetail.TestAreaID = dataReader["TEST_AREA_ID"].ToString().Trim();
                    questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        questionDetail.CreatedBy = int.Parse(dataReader["CREATED_BY"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        questionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_BY"]))
                        questionDetail.ModifiedBy = int.Parse(dataReader["MODIFIED_BY"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        questionDetail.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["RATIO_CORRECT_ANSWER_ATTENDED"]))
                        questionDetail.RatioOfCorrectAnswerToAttended =
                            (dataReader["RATIO_CORRECT_ANSWER_ATTENDED"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                        questionDetail.AverageTimeTaken = Convert.ToDecimal(dataReader["AVERAGE_TIME_TAKEN"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_INCLUDED"]))
                        questionDetail.Administered = int.Parse(dataReader["TEST_INCLUDED"].ToString());
                }
                // Retrieve answer choices using SPGET_ANSWER_CHOICES stored procedure
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    questionDetail.AnswerChoices = GetQuestionOptions(questionKey);
                }

                // Retrieve subjects and categories using SPGET_SUBJECTS_CATEGORIES stored procedure
                dataReader.NextResult();
                List<Subject> subjects = new List<Subject>();

                // Loop through to get all subjects and their categories
                while (dataReader.Read())
                {
                    Subject subject = new Subject();
                    subject.CategoryID = Convert.ToInt32(dataReader["CAT_KEY"]);
                    subject.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                    subject.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"]);
                    subject.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                    subject.IsSelected = (dataReader["IS_CORRECT"].ToString().Trim() == "Y") ? true : false;

                    subjects.Add(subject);
                }

                questionDetail.Subjects = subjects;

                // Retrieve question option
                dataReader.NextResult();
                if (dataReader.Read())
                {
                    questionDetail.QuestionAttribute = new QuestionAttribute();
                    questionDetail.QuestionAttribute.MaxLength = Convert.ToInt32(dataReader["MAX_LENGTH"]);
                    questionDetail.QuestionAttribute.Marks = Convert.ToInt32(dataReader["MARKS"]);
                    questionDetail.QuestionAttribute.AnswerReference = dataReader["ANSWER_REFERENCE"].ToString().Trim();
                }

                return questionDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Get question details by giving a question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        ///  A <see cref="QuestionDetail"/> that holds the question details. 
        /// </returns>
        public QuestionDetail GetInterviewQuestion(string questionKey)
        {
            IDataReader dataReader = null;
            try
            {
                QuestionDetail questionDetail = new QuestionDetail();

                DbCommand getSingleQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_QUESTION_DETAILS");

                // Add parameters.
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@QUESTION_KEY", DbType.String, questionKey.ToString());

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getSingleQuestionDetailCommand);

                // Retrieve basic question details using SPGET_QUESTION_HEADER stored procedure
                while (dataReader.Read())
                {
                    questionDetail.QuestionKey = questionKey.Trim();
                    questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                    questionDetail.Complexity = dataReader["COMPLEXITY_ID"].ToString().Trim();
                    questionDetail.ComplexityName = dataReader["COMPLEXITY_NAME"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["ILLUSTRATION"]))
                    {
                        questionDetail.QuestionImage = dataReader["ILLUSTRATION"] as byte[];
                        questionDetail.HasImage = true;
                    }
                    else
                        questionDetail.HasImage = false;

                    questionDetail.Tag = dataReader["TAGS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CREDIT_EARNED"]))
                        questionDetail.CreditsEarned = decimal.Parse(dataReader["CREDIT_EARNED"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_CHOICES"]))
                        questionDetail.NoOfChoices = Convert.ToInt16(dataReader["NO_OF_CHOICES"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER"]))
                        questionDetail.Answer = Convert.ToInt16(dataReader["ANSWER"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR"]))
                        questionDetail.Author = int.Parse(dataReader["AUTHOR"].ToString());

                    questionDetail.TestAreaID = dataReader["TEST_AREA_ID"].ToString().Trim();
                    questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        questionDetail.CreatedBy = int.Parse(dataReader["CREATED_BY"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        questionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_BY"]))
                        questionDetail.ModifiedBy = int.Parse(dataReader["MODIFIED_BY"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        questionDetail.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["RATIO_CORRECT_ANSWER_ATTENDED"]))
                        questionDetail.RatioOfCorrectAnswerToAttended =
                            (dataReader["RATIO_CORRECT_ANSWER_ATTENDED"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                        questionDetail.AverageTimeTaken = Convert.ToDecimal(dataReader["AVERAGE_TIME_TAKEN"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_INCLUDED"]))
                        questionDetail.Administered = int.Parse(dataReader["TEST_INCLUDED"].ToString());
                }
                // Retrieve answer choices using SPGET_ANSWER_CHOICES stored procedure
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    questionDetail.AnswerChoices = GetInterviewQuestionOptions(questionKey);
                }

                // Retrieve subjects and categories using SPGET_SUBJECTS_CATEGORIES stored procedure
                dataReader.NextResult();
                List<Subject> subjects = new List<Subject>();

                // Loop through to get all subjects and their categories
                while (dataReader.Read())
                {
                    Subject subject = new Subject();
                    subject.CategoryID = Convert.ToInt32(dataReader["CAT_KEY"]);
                    subject.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                    subject.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"]);
                    subject.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                    subject.IsSelected = (dataReader["IS_CORRECT"].ToString().Trim() == "Y") ? true : false;

                    subjects.Add(subject);
                }

                questionDetail.Subjects = subjects;

                return questionDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Get question image by giving a question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        ///  A <see cref="byte[]"/> that holds the question image. 
        /// </returns>
        public byte[] GetQuestionImage(string questionKey)
        {
            DbCommand getSingleQuestionDetailCommand =
                   HCMDatabase.GetStoredProcCommand("SPGET_QUESTION_IMAGE");

            // Add parameters.
            HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                "@QUESTION_KEY", DbType.String, questionKey.ToString());

            // Execute command.
            byte[] questionImage = HCMDatabase.ExecuteScalar(getSingleQuestionDetailCommand) as byte[];

            return questionImage;
        }
        /// <summary>
        /// Helps to check if the question key already involved in test question.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        /// A <see cref="bool"/>that holds the question Key involved in test question.
        /// </returns>
        public bool IsQuestionExistsInTest(string questionKey)
        {
            DbCommand getQuestionKeyCountCommand =
                HCMDatabase.GetStoredProcCommand("SPCHECK_QUESTION_KEY");

            // Add parameters.
            HCMDatabase.AddInParameter(getQuestionKeyCountCommand,
                "@QUESTION_KEY", DbType.String, questionKey.ToString());

            // Execute command.
            int rowsAffected = Convert.ToInt32(HCMDatabase.ExecuteScalar(getQuestionKeyCountCommand));

            if (rowsAffected > 0)
                return true;
            else
                return false;
        }
        

       /// <summary>
        /// Helps to check if the question key already involved in Interview test question.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <returns>
        /// A <see cref="bool"/>that holds the question Key involved in Interview test question.
        /// </returns>
        public bool IsInterviewQuestionExistsInTest(string questionKey)
        {
            DbCommand getQuestionKeyCountCommand =
                HCMDatabase.GetStoredProcCommand("SPCHECK_INTERVIEW_QUESTION_KEY");

            // Add parameters.
            HCMDatabase.AddInParameter(getQuestionKeyCountCommand,
                "@QUESTION_KEY", DbType.String, questionKey.ToString());

            // Execute command.
            int rowsAffected = Convert.ToInt32(HCMDatabase.ExecuteScalar(getQuestionKeyCountCommand));

            if (rowsAffected > 0)
                return true;
            else
                return false;
        }


        /// <summary>
        /// Method allows us to update the question information in the question repository.
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="QuestionDetail"/>that holds the question Detail
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/>that holds the transaction
        /// </param>
        public void UpdateQuestion(QuestionDetail questionDetail, string questionKey, IDbTransaction transaction)
        {
            // Update Question Header details
            DbCommand updateQuestionOptionsCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_QUESTION");

            // Add Input Parameters
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@QUESTION_TYPE", DbType.Int16, (int)questionDetail.QuestionType);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@QUESTION_DESC", DbType.String, questionDetail.Question);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@COMPLEXITY_ID", DbType.String, questionDetail.Complexity);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@TAGS", DbType.String, questionDetail.Tag);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@CREDIT_EARNED", DbType.Decimal, questionDetail.CreditsEarned);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@NO_OF_CHOICES", DbType.Int16, questionDetail.NoOfChoices);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@ANSWER", DbType.String, questionDetail.Answer);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@AUTHOR", DbType.Int32, questionDetail.Author);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@TEST_AREA_ID", DbType.String, questionDetail.TestAreaID);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@MODIFIED_BY", DbType.Int32, questionDetail.ModifiedBy);

            // Execute stored procedure
            HCMDatabase.ExecuteNonQuery(updateQuestionOptionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method allows us to update the question information in the question repository.
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="QuestionDetail"/>that holds the question Detail
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/>that holds the transaction
        /// </param>
        public void UpdateQuestionAttribute(QuestionDetail questionDetail, string questionKey, IDbTransaction transaction)
        {
            // Update Question Header details
            DbCommand updateQuestionOptionsCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_QUESTION_ATTRIBUTE");

            // Add Input Parameters
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@MAX_LENGTH", DbType.Int32, questionDetail.QuestionAttribute.MaxLength);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@MARKS", DbType.Int32, questionDetail.QuestionAttribute.Marks);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
               "@ANSWER_REFERENCE", DbType.String, questionDetail.QuestionAttribute.AnswerReference);

            // Execute stored procedure
            HCMDatabase.ExecuteNonQuery(updateQuestionOptionsCommand, transaction as DbTransaction);
        }


        /// <summary>
        /// Method allows us to update the interview question information in the question repository.
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="QuestionDetail"/>that holds the question Detail
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/>that holds the transaction
        /// </param>
        public void UpdateInterviewQuestion(QuestionDetail questionDetail, string questionKey, IDbTransaction transaction)
        {
            // Update Question Header details
            DbCommand updateQuestionOptionsCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_INTERVIEW_QUESTION");

            // Add Input Parameters
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@QUESTION_DESC", DbType.String, questionDetail.Question);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@COMPLEXITY_ID", DbType.String, questionDetail.Complexity);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@TAGS", DbType.String, questionDetail.Tag);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@CREDIT_EARNED", DbType.Decimal, questionDetail.CreditsEarned);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@NO_OF_CHOICES", DbType.Int16, questionDetail.NoOfChoices);
          
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@AUTHOR", DbType.Int32, questionDetail.Author);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@TEST_AREA_ID", DbType.String, questionDetail.TestAreaID);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@MODIFIED_BY", DbType.Int32, questionDetail.ModifiedBy);

            // Execute stored procedure
            HCMDatabase.ExecuteNonQuery(updateQuestionOptionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will updates the correct answer to the question repository.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        /// <param name="correctAnswerID">
        /// An <see cref="int"/> that contains the correct answer id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>
        public void UpdateCorrectAnswer(string questionKey, int correctAnswerID, IDbTransaction transaction)
        {
            try
            {
                DbCommand updateCorrectAnswerCommand =
                    HCMDatabase.GetStoredProcCommand("SPUPDATE_CORRECT_ANSWER");

                // Add input parameters.
                HCMDatabase.AddInParameter(updateCorrectAnswerCommand,
                    "@ANSWER", DbType.Int32, correctAnswerID);
                HCMDatabase.AddInParameter(updateCorrectAnswerCommand,
                    "@QUESTION_KEY", DbType.String, questionKey);

                HCMDatabase.ExecuteNonQuery(updateCorrectAnswerCommand, transaction as DbTransaction);
            }
            catch (Exception exp)
            {
                transaction.Rollback();
                throw exp;
            }
        }

        /// <summary>
        /// Update Answer Choices
        /// </summary>
        /// <param name="answerChoice">
        /// A <see cref="AnswerChoice"/> that holds the Answer Choice
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/>that holds the user id
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object
        /// </param>
        public void UpdateAnswerChoices(AnswerChoice answerChoice, int user, IDbTransaction transaction)
        {
            DbCommand updateQuestionOptionsCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_OPTIONS");

            // Add Input Parameters
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@CHOICE_ID", DbType.Int32, answerChoice.ChoiceID);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@CHOICE_DESC", DbType.String, answerChoice.Choice);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@QUESTION_OPTION_ID", DbType.Int32, answerChoice.QuestionGenId);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@MODIFIED_BY", DbType.Int32, user);

            HCMDatabase.ExecuteNonQuery(updateQuestionOptionsCommand, transaction as DbTransaction);
        }


        /// <summary>
        /// Update Answer Choices
        /// </summary>
        /// <param name="answerChoice">
        /// A <see cref="AnswerChoice"/> that holds the Answer Choice
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/>that holds the user id
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object
        /// </param>
        public void UpdateInterviewAnswerChoices(AnswerChoice answerChoice, int user, IDbTransaction transaction)
        {
            DbCommand updateQuestionOptionsCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_INTERVIEW_OPTIONS");
            // Add Input Parameters
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@CHOICE_ID", DbType.Int32, answerChoice.ChoiceID);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@CHOICE_DESC", DbType.String, answerChoice.Choice);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@QUESTION_OPTION_ID", DbType.Int32, answerChoice.QuestionGenId);
            HCMDatabase.AddInParameter(updateQuestionOptionsCommand,
                "@MODIFIED_BY", DbType.Int32, user);

            HCMDatabase.ExecuteNonQuery(updateQuestionOptionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Update the subject information in the question relation
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the question Key
        /// </param>
        /// <param name="subject">
        /// A <see cref="Subject"/> that holds the Subject List
        /// </param>
        /// <param name="createdBy">
        /// A <see cref="int"/> that holds the created By
        /// </param>
        /// <param name="modifiedBy">
        /// A <see cref="int"/> that holds the modified By
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object
        /// </param>
        public void UpdateCategorySubjects(string questionKey, Subject subject, Int32 createdBy,
            Int32 modifiedBy, IDbTransaction transaction)
        {
            DbCommand updateQuestionRelationCommand
                = HCMDatabase.GetStoredProcCommand("SPUPDATE_QUESTION_RELATION");
            // Add Input Parameters
            HCMDatabase.AddInParameter(updateQuestionRelationCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(updateQuestionRelationCommand,
                "@CAT_SUB_ID", DbType.Int32, subject.SubjectID);
            HCMDatabase.AddInParameter(updateQuestionRelationCommand,
                "@CREATED_BY", DbType.Int32, createdBy);
            HCMDatabase.AddInParameter(updateQuestionRelationCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);
            HCMDatabase.ExecuteNonQuery(updateQuestionRelationCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method helps to delete answer choices. It is used by the UpdateQuestion method
        /// in QuestionBLManager.
        /// </summary>
        /// <param name="choiceIdsToBeDeleted">
        /// A <see cref="string"/> that holds the answer choice Ids.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the Transaction
        /// </param>
        public void DeleteAnswerChoices(string choiceIdsToBeDeleted, IDbTransaction transaction)
        {
            DbCommand deleteQuestionOptionsCommand =
                HCMDatabase.GetStoredProcCommand("SPDELETE_OPTIONS");

            // Add Input Parameters
            HCMDatabase.AddInParameter(deleteQuestionOptionsCommand,
                "@CHOICES_TO_BE_DELETED", DbType.String, choiceIdsToBeDeleted);

            HCMDatabase.ExecuteNonQuery(deleteQuestionOptionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method helps to delete answer choices. It is used by the UpdateQuestion method
        /// in QuestionBLManager.
        /// </summary>
        /// <param name="choiceIdsToBeDeleted">
        /// A <see cref="string"/> that holds the answer choice Ids.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the Transaction
        /// </param>
        public void DeleteInterviewAnswerChoices(string choiceIdsToBeDeleted, IDbTransaction transaction)
        {
            DbCommand deleteQuestionOptionsCommand =
                HCMDatabase.GetStoredProcCommand("SPDELETE_INTERVIEW_OPTIONS");

            // Add Input Parameters
            HCMDatabase.AddInParameter(deleteQuestionOptionsCommand,
                "@CHOICES_TO_BE_DELETED", DbType.String, choiceIdsToBeDeleted);

            HCMDatabase.ExecuteNonQuery(deleteQuestionOptionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Delete subjects from the question relation
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="subject">
        /// A <see cref="Subject"/> that holds the subject.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction.
        /// </param>
        public void DeleteCategorySubjects(string questionKey, Subject subject, IDbTransaction transaction)
        {
            DbCommand deleteQuestionOptionsCommand =
                HCMDatabase.GetStoredProcCommand("SPDELETE_QUESTION_RELATION");

            // Add Input Parameters
            HCMDatabase.AddInParameter(deleteQuestionOptionsCommand,
                "@QUESTION_KEY", DbType.String, questionKey);

            HCMDatabase.AddInParameter(deleteQuestionOptionsCommand,
                "@CAT_SUB_ID", DbType.Int32, subject.SubjectID);

            HCMDatabase.ExecuteNonQuery(deleteQuestionOptionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method is used to delete all the subjects based on the category ids and question key 
        /// from the question relation repository
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the question Key.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction.
        /// </param>
        public void DeleteSubjectsByCategoryID(string questionKey, IDbTransaction transaction)
        {
            DbCommand deleteCategoriesCommand =
                                HCMDatabase.GetStoredProcCommand("SPDELETE_QUESTION_CATEGORIES");
            // Add Input Parameters
            HCMDatabase.AddInParameter(deleteCategoriesCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.ExecuteNonQuery(deleteCategoriesCommand, transaction as DbTransaction);
        }


        /// <summary>
        /// This method is used to delete all the subjects based on the category ids and question key 
        /// from the question relation repository
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the question Key.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction.
        /// </param>
        public void DeleteInterviewSubjectsByCategoryID(string questionKey, IDbTransaction transaction)
        {
            DbCommand deleteCategoriesCommand =
                                HCMDatabase.GetStoredProcCommand("SPDELETE_INTERVIEW_QUESTION_CATEGORIES");
            // Add Input Parameters
            HCMDatabase.AddInParameter(deleteCategoriesCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.ExecuteNonQuery(deleteCategoriesCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method is used to delete all the subjects based on the category ids and question key 
        /// from the question relation repository
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the question Key.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction.
        /// </param>
        public void DeleteInterviewQuestionImage(string questionKey, IDbTransaction transaction)
        {
            DbCommand deleteInterviewQuestionCommand =
                                HCMDatabase.GetStoredProcCommand("SPDELETE_INTERVIEW_QUESTION_IMAGE");
            // Add Input Parameters
            HCMDatabase.AddInParameter(deleteInterviewQuestionCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.ExecuteNonQuery(deleteInterviewQuestionCommand, transaction as DbTransaction);
        }


        /// <summary>
        /// This method is used to delete all the subjects based on the category ids and question key 
        /// from the question relation repository
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the question Key.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction.
        /// </param>
        public void DeleteQuestionImage(string questionKey, IDbTransaction transaction)
        {
            DbCommand deleteInterviewQuestionImageCommand =
                                HCMDatabase.GetStoredProcCommand("SPDELETE_QUESTION_IMAGE");
            // Add Input Parameters
            HCMDatabase.AddInParameter(deleteInterviewQuestionImageCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.ExecuteNonQuery(deleteInterviewQuestionImageCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method is used to Get all the Answer choices based on the question key.
        /// </summary>
        /// <param name="questionKey">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <returns>
        /// A list of <see cref="AnswerChoice"/> that holds the AnswerChoice regarding this question.
        /// </returns>
        /// <remarks>
        /// Supports providing data.
        /// </remarks>
        public List<AnswerChoice> GetQuestionOptions(string questionKey)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getAnswerChoiseCommand = HCMDatabase.GetStoredProcCommand("SPGET_QUESTION_OPTIONS");
                HCMDatabase.AddInParameter(getAnswerChoiseCommand, "@QUESTION_KEY", DbType.String, questionKey);
                dataReader = HCMDatabase.ExecuteReader(getAnswerChoiseCommand);

                List<AnswerChoice> AnswerChoices = null;

                while (dataReader.Read())
                {
                    if (AnswerChoices == null)
                        AnswerChoices = new List<AnswerChoice>();

                    AnswerChoice answerChoice = new AnswerChoice();
                    answerChoice.ChoiceID = int.Parse(dataReader["CHOICE_ID"].ToString());
                    answerChoice.Choice = dataReader["CHOICE_DESC"].ToString();
                    answerChoice.IsCorrect = (dataReader["IS_CORRECT"].ToString().Trim() == "Y") ? true : false;
                    answerChoice.QuestionGenId = int.Parse(dataReader["QUESTION_OPTION_ID"].ToString());
                    AnswerChoices.Add(answerChoice);
                }

                return AnswerChoices;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// This method is used to Get all the Answer choices based on the question key.
        /// </summary>
        /// <param name="questionKey">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <returns>
        /// A list of <see cref="AnswerChoice"/> that holds the AnswerChoice regarding this question.
        /// </returns>
        /// <remarks>
        /// Supports providing data.
        /// </remarks>
        public List<AnswerChoice> GetInterviewQuestionOptions(string questionKey)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getAnswerChoiseCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_QUESTION_OPTIONS");
                HCMDatabase.AddInParameter(getAnswerChoiseCommand, "@QUESTION_KEY", DbType.String, questionKey);
                dataReader = HCMDatabase.ExecuteReader(getAnswerChoiseCommand);

                List<AnswerChoice> AnswerChoices = null;

                while (dataReader.Read())
                {
                    if (AnswerChoices == null)
                        AnswerChoices = new List<AnswerChoice>();

                    AnswerChoice answerChoice = new AnswerChoice();
                    answerChoice.ChoiceID = int.Parse(dataReader["CHOICE_ID"].ToString());
                    answerChoice.Choice = dataReader["CHOICE_DESC"].ToString();
                    answerChoice.IsCorrect = (dataReader["IS_CORRECT"].ToString().Trim() == "Y") ? true : false;
                    answerChoice.QuestionGenId = int.Parse(dataReader["QUESTION_OPTION_ID"].ToString());
                    AnswerChoices.Add(answerChoice);
                }

                return AnswerChoices;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method is used to Delete question based on the question key.
        /// </summary>
        /// <param name="questionKey">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeleteQuestion(string questionKey, int user)
        {
            DbCommand questionUpdateStatusCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_QUESTION");
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@ACTIVE_FLAG", DbType.String, null);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@DELETED", DbType.String, "Y");
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@USERID", DbType.Int32, user);
            HCMDatabase.ExecuteNonQuery(questionUpdateStatusCommand);
        }

        /// <summary>
        /// Represents the method to get the contributor summary
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userId
        /// </param>
        /// <returns>
        /// A<see cref="ContributorSummary"/>that holds the contributor summary details
        /// </returns>
        public ContributorSummary GetContributorSummary(int userID)
        {
            IDataReader dataReader = null;

            ContributorSummary contributorSummary = null;

            try
            {
                DbCommand getQuestionSummaryCommand =
                       HCMDatabase.GetStoredProcCommand("SPGET_CONTRIBUTOR_QUESTION_SUMMARY");
                // Add parameters.
                HCMDatabase.AddInParameter(getQuestionSummaryCommand,
                    "@USER_ID", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(getQuestionSummaryCommand);

                if (dataReader.Read())
                {
                    contributorSummary = new ContributorSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["CONTRIBUTOR_SINCE"]))
                    {
                        contributorSummary.ContributorSince =
                            Convert.ToDateTime(dataReader["CONTRIBUTOR_SINCE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_AUTHORED"]))
                    {
                        contributorSummary.QuestionsAuthored =
                            int.Parse(dataReader["QUESTION_AUTHORED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREDITS_EARNED"]))
                    {
                        contributorSummary.CreditsEarned =
                            Convert.ToDecimal(dataReader["CREDITS_EARNED"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_CONTRIBUTED"]))
                    {
                        contributorSummary.CategoriesContributed =
                            int.Parse(dataReader["CATEGORY_CONTRIBUTED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_CONTRIBUTED"]))
                    {
                        contributorSummary.SubjectsContributed =
                            int.Parse(dataReader["SUBJECT_CONTRIBUTED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_USAGE_COUNT"]))
                    {
                        contributorSummary.QuestionUsageCount =
                            int.Parse(dataReader["QUESTION_USAGE_COUNT"].ToString());
                    }
                }
                return contributorSummary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        
        /// <summary>
        /// Represents the method to get the contributor summary
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userId
        /// </param>
        /// <returns>
        /// A<see cref="ContributorSummary"/>that holds the contributor summary details
        /// </returns>
        public ContributorSummary GetInterviewContributorSummary(int userID)
        {
            IDataReader dataReader = null;

            ContributorSummary contributorSummary = null;

            try
            {
                DbCommand getQuestionSummaryCommand =
                       HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_CONTRIBUTOR_QUESTION_SUMMARY");
                // Add parameters.
                HCMDatabase.AddInParameter(getQuestionSummaryCommand,
                    "@USER_ID", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(getQuestionSummaryCommand);

                if (dataReader.Read())
                {
                    contributorSummary = new ContributorSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["CONTRIBUTOR_SINCE"]))
                    {
                        contributorSummary.ContributorSince =
                            Convert.ToDateTime(dataReader["CONTRIBUTOR_SINCE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_AUTHORED"]))
                    {
                        contributorSummary.QuestionsAuthored =
                            int.Parse(dataReader["QUESTION_AUTHORED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CREDITS_EARNED"]))
                    {
                        contributorSummary.CreditsEarned =
                            Convert.ToDecimal(dataReader["CREDITS_EARNED"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_CONTRIBUTED"]))
                    {
                        contributorSummary.CategoriesContributed =
                            int.Parse(dataReader["CATEGORY_CONTRIBUTED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_CONTRIBUTED"]))
                    {
                        contributorSummary.SubjectsContributed =
                            int.Parse(dataReader["SUBJECT_CONTRIBUTED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_USAGE_COUNT"]))
                    {
                        contributorSummary.QuestionUsageCount =
                            int.Parse(dataReader["QUESTION_USAGE_COUNT"].ToString());
                    }
                }
                return contributorSummary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the category summary details based on the author.
        /// </summary>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that holds the pagenumber.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains total number of pages.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that takes the total number of records returned by the SP.
        /// </param>
        /// <returns>
        /// A list of <see cref="Category"/> that holds the categories.
        /// </returns>
        public List<Category> GetCategoriesContributed
            (int user, int pageNumber, int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            IDataReader dataReader = null;

            try
            {
                List<Category> categories = null;

                DbCommand getCategorySummaryCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CATEGORY_SUMMARY");

                // Add input parameter
                HCMDatabase.AddInParameter(getCategorySummaryCommand,
                    "@AUTHORID", DbType.Int16, user);

                HCMDatabase.AddInParameter(getCategorySummaryCommand, "@PAGENUM",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCategorySummaryCommand, "@PAGESIZE",
                    DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getCategorySummaryCommand);

                while (dataReader.Read())
                {
                    if (categories == null)
                        categories = new List<Category>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        if ((!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                            && (!Utility.IsNullOrEmpty(dataReader["CATEGORY_ID"])))

                            categories.Add(new Category(int.Parse(dataReader["CATEGORY_ID"].ToString()),
                                dataReader["CATEGORY_NAME"].ToString().Trim()));
                    }
                    else
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                }
                return categories;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }


        /// <summary>
        /// Method that retrieves the interview category summary details based on the author.
        /// </summary>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that holds the pagenumber.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains total number of pages.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that takes the total number of records returned by the SP.
        /// </param>
        /// <returns>
        /// A list of <see cref="Category"/> that holds the categories.
        /// </returns>
        public List<Category> GetInterviewCategoriesContributed
            (int user, int pageNumber, int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            IDataReader dataReader = null;

            try
            {
                List<Category> categories = null;

                DbCommand getCategorySummaryCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_CATEGORY_SUMMARY");

                // Add input parameter
                HCMDatabase.AddInParameter(getCategorySummaryCommand,
                    "@AUTHORID", DbType.Int16, user);

                HCMDatabase.AddInParameter(getCategorySummaryCommand, "@PAGENUM",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCategorySummaryCommand, "@PAGESIZE",
                    DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getCategorySummaryCommand);

                while (dataReader.Read())
                {
                    if (categories == null)
                        categories = new List<Category>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        if ((!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                            && (!Utility.IsNullOrEmpty(dataReader["CATEGORY_ID"])))

                            categories.Add(new Category(int.Parse(dataReader["CATEGORY_ID"].ToString()),
                                dataReader["CATEGORY_NAME"].ToString().Trim()));
                    }
                    else
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                }
                return categories;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the subject summary details based on the author.
        /// </summary>
        /// <param name="user">
        /// A <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the current pagenumber.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that contains total number of pages.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that takes the total number of records returned by the SP.
        /// </param>
        /// <returns>
        /// A list of <see cref="Subject"/> that holds the subjects.
        /// </returns>
        public List<Subject> GetSubjectsContributed
            (int user, int pageNumber, int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            IDataReader dataReader = null;

            try
            {
                List<Subject> subjects = null;

                DbCommand getCategorySummaryCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_SUBJECT_SUMMARY");

                // Add input parameter
                HCMDatabase.AddInParameter(getCategorySummaryCommand,
                    "@AUTHORID", DbType.Int16, user);

                HCMDatabase.AddInParameter(getCategorySummaryCommand, "@PAGENUM",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCategorySummaryCommand, "@PAGESIZE",
                    DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getCategorySummaryCommand);

                while (dataReader.Read())
                {
                    if (subjects == null)
                        subjects = new List<Subject>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        if ((!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"])) &&
                            (!Utility.IsNullOrEmpty(dataReader["SUBJECT_ID"])))
                            subjects.Add(new Subject(dataReader["SUBJECT_NAME"].ToString().Trim(),
                                int.Parse(dataReader["SUBJECT_ID"].ToString())));
                    }
                    else
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                }
                return subjects;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the interview subject summary details based on the author.
        /// </summary>
        /// <param name="user">
        /// A <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the current pagenumber.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that contains total number of pages.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that takes the total number of records returned by the SP.
        /// </param>
        /// <returns>
        /// A list of <see cref="Subject"/> that holds the subjects.
        /// </returns>
        public List<Subject> GetInterviewSubjectsContributed
            (int user, int pageNumber, int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            IDataReader dataReader = null;

            try
            {
                List<Subject> subjects = null;

                DbCommand getCategorySummaryCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_SUBJECT_SUMMARY");

                // Add input parameter
                HCMDatabase.AddInParameter(getCategorySummaryCommand,
                    "@AUTHORID", DbType.Int16, user);

                HCMDatabase.AddInParameter(getCategorySummaryCommand, "@PAGENUM",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCategorySummaryCommand, "@PAGESIZE",
                    DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getCategorySummaryCommand);

                while (dataReader.Read())
                {
                    if (subjects == null)
                        subjects = new List<Subject>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        if ((!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"])) &&
                            (!Utility.IsNullOrEmpty(dataReader["SUBJECT_ID"])))
                            subjects.Add(new Subject(dataReader["SUBJECT_NAME"].ToString().Trim(),
                                int.Parse(dataReader["SUBJECT_ID"].ToString())));
                    }
                    else
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                }
                return subjects;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// This method is used to Active/inactive question based on the question key.
        /// </summary>
        /// <param name="questionKey">
        ///  A <see cref="string"/> that holds the question Key.
        /// </param>
        /// <param name="questionStatus">
        ///  A <see cref="string"/> that holds the question Status.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void UpdateStatus(string questionKey, string questionStatus, int user)
        {
            DbCommand questionUpdateStatusCommand = HCMDatabase.
              GetStoredProcCommand("SPDELETE_QUESTION");
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@ACTIVE_FLAG", DbType.String, questionStatus);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@DELETED", DbType.String, null);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
               "@USERID", DbType.Int32, user);
            HCMDatabase.ExecuteNonQuery(questionUpdateStatusCommand);
        }

        /// <summary>
        /// Method that retrieves the list of Questions for the given search 
        /// </summary>
        /// <param name="searchCriteria">
        /// A list of <see cref="QuestionDetailSearchCriteria"/> that holds the questionKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the QuestionDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<QuestionDetail> GetQuestions(QuestionDetailSearchCriteria searchCriteria, QuestionType questionType,
            int pageSize, int pageNumber, string orderBy, SortType direction, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                DbCommand getQuestionDeatilsCommand = HCMDatabase.GetStoredProcCommand("SPGET_QUESTIONS");

                string creditEarned = null;
                if (searchCriteria.CreditsEarned > 0)
                {
                    creditEarned = searchCriteria.CreditsEarned.ToString();
                }
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@QUESTION_KEY", DbType.String, searchCriteria.QuestionKey);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@AUTHOR", DbType.String, searchCriteria.AuthorName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@AUTHOR_ID", DbType.Int32, searchCriteria.Author);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CAT_SUB", DbType.String, searchCriteria.CategorySubjectsKey);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CATEGORY", DbType.String, searchCriteria.CategoryName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@SUBJECT", DbType.String, searchCriteria.SubjectName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@KEYWORD", DbType.String, searchCriteria.Tag);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@TEST_AREA", DbType.String, searchCriteria.TestAreaID);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@COMPLEXITY", DbType.String, searchCriteria.Complexities);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CREDIT_EARNED", DbType.Decimal, creditEarned);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ACTIVE_FLAG", DbType.String, searchCriteria.ActiveFlag);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@QUESTIONTYPE", DbType.Int32,questionType==QuestionType.MultipleChoice?1:2);
             

                dataReader = HCMDatabase.ExecuteReader(getQuestionDeatilsCommand);

                List<QuestionDetail> questionDetails = null;
                while (dataReader.Read())
                {
                    if (questionDetails == null)
                        questionDetails = new List<QuestionDetail>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        QuestionDetail questionDetail = new QuestionDetail();
                        int a = int.Parse(dataReader["CORECT_ANSWER"].ToString()) - 1;
                        questionDetail.QuestionID = int.Parse(dataReader["QUESTION_ID"].ToString());
                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                        questionDetail.QuestionType = (QuestionType)System.Convert.ToInt32(dataReader["QUESTION_TYPE"]);
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString();
                        questionDetail.Complexity = dataReader["COMPLEXITY_NAME"].ToString();
                        questionDetail.Answer = short.Parse(a.ToString());
                        questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                        questionDetail.Status = (QuestionStatus)Convert.ToChar((dataReader["ACTIVE_FLAG"]));
                        questionDetail.ModifiedDate = System.Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                        questionDetail.CreditsEarned = System.Convert.ToDecimal(dataReader["CREDIT_EARNED"]);
                        questionDetail.DataAccessRights = dataReader["DATA_ACCESS_RIGHTS"].ToString();

                        questionDetail.CategoryName = dataReader["CATEGORY"].ToString();
                        questionDetail.SubjectName= dataReader["SUBJECT"].ToString();
                        if (dataReader["TEST_APPEARED"] == DBNull.Value)
                        {
                            questionDetail.TestIncluded = 0;
                        }
                        else
                        {
                            questionDetail.TestIncluded = Convert.ToInt32(dataReader["TEST_APPEARED"]);
                        }
                        questionDetails.Add(questionDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                dataReader.Close();
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of test for the given questionkey Mapped. 
        /// criteria.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that holds the TestDetails regarding this question.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<TestDetail> GetQuestionUsageSummary(string questionKey,
            int pageNumber, int pageSize, string orderBy, SortType orderByDirection,
            out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {

                DbCommand getQuestionUsageSummaryCommand =
                       HCMDatabase.GetStoredProcCommand("SPGET_QUESTION_SUMMARY");
                // Add parameters.
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@QUESTIONKEY", DbType.String, questionKey.ToString());
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getQuestionUsageSummaryCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getQuestionUsageSummaryCommand);
                List<TestDetail> testDetails = null;

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();

                    TestDetail testDetail = new TestDetail();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        testDetail.Name = dataReader["TEST_NAME"].ToString();
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                        testDetail.TestCreationDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());
                        testDetail.NoOfCandidatesAdministered = int.Parse(dataReader["TEST_ADMINSTRED_COUNT"].ToString());
                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }

                }
                dataReader.Close();
                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Method that checks whether the attributes of a question
        /// are valid or not
        /// </summary>
        /// <param name="question">
        /// A<see cref="QuestionDetail"/>QuestionDetail that contains 
        /// the detail of the question.
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>value that contain question status
        /// </returns>
        public QuestionDetail CheckQuestionAttribute(QuestionDetail question, int tenantID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand checkQuestionAttributeCommand = HCMDatabase.
            GetStoredProcCommand("SPGET_CHECK_QUESTION_ATTRIBUTES");

                if (!Utility.IsNullOrEmpty(question.Question))
                {
                    HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@QUESTION", DbType.String, question.Question.Replace("\r\n", "\n").Replace("\n", "\r\n"));
                }
                else
                {
                    HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@QUESTION", DbType.String, question.Question);
                }

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                  "@SUBJECT_NAME", DbType.String, question.SubjectName);

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@CATEGORY_NAME", DbType.String, question.CategoryName);

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@COMPLEXITY", DbType.String, question.ComplexityName);

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@TEST_AREA", DbType.String, question.TestAreaName);

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                   "@AUTHOR_ID", DbType.Int32, question.Author);

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                   "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddOutParameter(checkQuestionAttributeCommand,
                    "@RESULT", DbType.Int32, 32);

                dataReader = HCMDatabase.ExecuteReader(checkQuestionAttributeCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["INVALID_QUESTION_REMARKS"]))
                    {
                        question.InvalidQuestionRemarks += dataReader
                            ["INVALID_QUESTION_REMARKS"].ToString().Trim() + ", ";
                        //if (!Utility.IsNullOrEmpty(question.InvalidQuestionRemarks))
                        //{
                        //    question.InvalidQuestionRemarks = question.InvalidQuestionRemarks + dataReader
                        //    ["INVALID_QUESTION_REMARKS"].ToString().Trim() + ", ";
                        //}
                        //else
                        //{
                        //    question.InvalidQuestionRemarks = dataReader
                        //    ["INVALID_QUESTION_REMARKS"].ToString().Trim() + ", ";
                        //}
                    }
                }
                if (question.InvalidQuestionRemarks == null)
                    question.InvalidQuestionRemarks = "";
                //question.InvalidQuestionRemarks = question.InvalidQuestionRemarks.TrimEnd(' ').TrimEnd(',');
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["RESULT"]))
                    {
                        int value = int.Parse(dataReader["RESULT"].ToString());
                        question.IsValid = value == 0 ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_ID"]))
                    {
                        question.CategoryID = int.Parse(dataReader["CATEGORY_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_ID"]))
                    {
                        question.SubjectID = int.Parse(dataReader["SUBJECT_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                    {
                        question.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                    {
                        question.Complexity = dataReader["COMPLEXITY_ID"].ToString();
                    }
                }

                return question;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the author id and name of the question author
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userId
        /// </param>
        /// <returns>
        /// A<see cref="UserDetail"/>that holds the user details
        /// </returns>    
        public UserDetail GetAuthorIDAndName(int userID)
        {
            IDataReader dataReader = null;
            try
            {
                UserDetail user = null;
                //Define the SP name
                DbCommand getQuestionAuthorCommand =
                       HCMDatabase.GetStoredProcCommand("SPGET_USER");

                // Add parameters.
                HCMDatabase.AddInParameter(getQuestionAuthorCommand,
                    "@USER_ID", DbType.Int32, userID);

                //Execute reader
                dataReader = HCMDatabase.ExecuteReader(getQuestionAuthorCommand);

                if (dataReader.Read())
                {
                    user = new UserDetail();

                    //Add user name
                    if (!Utility.IsNullOrEmpty(dataReader["usrUserName"]))
                    {
                        user.UserName = dataReader["usrUserName"].ToString();
                    }
                    //Add first name
                    if (!Utility.IsNullOrEmpty(dataReader["usrFirstName"]))
                    {
                        user.FirstName = dataReader["usrFirstName"].ToString();
                    }
                }
                return user;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the contributor summary question details
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userID 
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number 
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page Size 
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction like asc/desc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records out parameter
        /// </param>
        /// <returns>
        /// A List for<see cref="QuestionDetail"/>that holds the Question Detail
        /// </returns>
        public List<QuestionDetail> GetContributorSummaryQuestionDetails(int userID,
           int pageNumber, int pageSize, string orderBy, SortType orderByDirection,
            out int totalRecords)
        {
            IDataReader dataReader = null;

            QuestionDetail questionDetail = null;

            totalRecords = 0;

            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            try
            {
                DbCommand getContributorQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CONTRIBUTOR_SUMMARY_QUESTION_DETAILS");

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@USERID", DbType.Int32, userID);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@ORDERBYDIRECTION", DbType.String,
                    orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getContributorQuestionCommand);

                while (dataReader.Read())
                {
                    questionDetail = new QuestionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTIONKEY"]))
                        questionDetail.QuestionKey = dataReader["QUESTIONKEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        questionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ATTEMPTED"]))
                        questionDetail.TestIncluded = int.Parse(dataReader["TEST_ATTEMPTED"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ADMINISTERED"]))
                        questionDetail.Administered = int.Parse(dataReader["TEST_ADMINISTERED"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CREDITS_EARNED"]))
                        questionDetail.CreditsEarned = Convert.ToDecimal(dataReader["CREDITS_EARNED"]);

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                        questionDetail.Status = (QuestionStatus)Convert.ToChar(dataReader["STATUS"].ToString());

                    questionDetails.Add(questionDetail);
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }



        /// <summary>
        /// Represents the method to get the contributor summary question details
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userID 
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number 
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page Size 
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction like asc/desc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records out parameter
        /// </param>
        /// <returns>
        /// A List for<see cref="QuestionDetail"/>that holds the Question Detail
        /// </returns>
        public List<QuestionDetail> GetInterviewContributorSummaryQuestionDetails(int userID,
           int pageNumber, int pageSize, string orderBy, SortType orderByDirection,
            out int totalRecords)
        {
            IDataReader dataReader = null;

            QuestionDetail questionDetail = null;

            totalRecords = 0;

            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            try
            {
                DbCommand getContributorQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_CONTRIBUTOR_SUMMARY_QUESTION_DETAILS");

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@USERID", DbType.Int32, userID);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@ORDERBYDIRECTION", DbType.String,
                    orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getContributorQuestionCommand);

                while (dataReader.Read())
                {
                    questionDetail = new QuestionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTIONKEY"]))
                        questionDetail.QuestionKey = dataReader["QUESTIONKEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        questionDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ATTEMPTED"]))
                        questionDetail.TestIncluded = int.Parse(dataReader["TEST_ATTEMPTED"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ADMINISTERED"]))
                        questionDetail.Administered = int.Parse(dataReader["TEST_ADMINISTERED"].ToString());

                    //if (!Utility.IsNullOrEmpty(dataReader["CREDITS_EARNED"]))
                    //    questionDetail.CreditsEarned = Convert.ToDecimal(dataReader["CREDITS_EARNED"]);

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                        questionDetail.Status = (QuestionStatus)Convert.ToChar(dataReader["STATUS"].ToString());

                    questionDetails.Add(questionDetail);
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the contributor chart details
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user ID 
        /// </param>
        /// <returns>
        /// A<see cref="ContributorSummary"/>that hold the contributor 
        /// summary details
        /// </returns>
        public ContributorSummary GetContributorChartDetails(int userID)
        {
            IDataReader dataReader = null;

            ContributorSummary contributorSummary = new ContributorSummary();

            //Initialise the question authored data
            contributorSummary.QuestionsAuthoredData = new SingleChartData();

            //Initialise the question usage count data
            contributorSummary.QuestionUsageCountData = new SingleChartData();

            //Initialise the credits earned data
            contributorSummary.CreditsEarnedData = new SingleChartData();

            contributorSummary.QuestionsAuthoredData.ChartData = new List<ChartData>();

            contributorSummary.QuestionUsageCountData.ChartData = new List<ChartData>();

            contributorSummary.CreditsEarnedData.ChartData = new List<ChartData>();

            try
            {
                DbCommand totalQuestionAuthoredChartCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CONTRIBUTOR_CHART_DATA");

                HCMDatabase.AddInParameter(totalQuestionAuthoredChartCommand,
                    "@CREATED_BY", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(totalQuestionAuthoredChartCommand);

                while (dataReader.Read())
                {
                    ChartData authoredChartData = new ChartData();
                    ChartData questionUsageChartData = new ChartData();
                    ChartData creditsEarnedChartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["MONTH_NAME"]))
                    {
                        authoredChartData.ChartXValue = dataReader["MONTH_NAME"].ToString();
                        questionUsageChartData.ChartXValue = dataReader["MONTH_NAME"].ToString();
                        creditsEarnedChartData.ChartXValue = dataReader["MONTH_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_COUNT"]))
                    {
                        authoredChartData.ChartYValue = int.Parse(dataReader["QUESTION_COUNT"].ToString());
                    }
                    contributorSummary.QuestionsAuthoredData.ChartData.Add(authoredChartData);

                    //Read the question usage count details
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_USAGE_COUNT"]))
                    {
                        questionUsageChartData.ChartYValue = int.Parse(dataReader["QUESTION_USAGE_COUNT"].ToString());
                    }
                    contributorSummary.QuestionUsageCountData.ChartData.Add(questionUsageChartData);
                    //Read contributor's credits earned details
                    if (!Utility.IsNullOrEmpty(dataReader["CREDITS_EARNED"]))
                    {
                        creditsEarnedChartData.ChartYValue = decimal.ToInt32(Convert.ToDecimal(dataReader["CREDITS_EARNED"].ToString()));
                    }
                    contributorSummary.CreditsEarnedData.ChartData.Add(creditsEarnedChartData);
                }
                return contributorSummary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Represents the method to get the interview contributor chart details
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user ID 
        /// </param>
        /// <returns>
        /// A<see cref="ContributorSummary"/>that hold the contributor 
        /// summary details
        /// </returns>
        public ContributorSummary GetInterviewContributorChartDetails(int userID)
        {
            IDataReader dataReader = null;

            ContributorSummary contributorSummary = new ContributorSummary();

            //Initialise the question authored data
            contributorSummary.QuestionsAuthoredData = new SingleChartData();

            //Initialise the question usage count data
            contributorSummary.QuestionUsageCountData = new SingleChartData();

            //Initialise the credits earned data
            contributorSummary.CreditsEarnedData = new SingleChartData();

            contributorSummary.QuestionsAuthoredData.ChartData = new List<ChartData>();

            contributorSummary.QuestionUsageCountData.ChartData = new List<ChartData>();

            contributorSummary.CreditsEarnedData.ChartData = new List<ChartData>();

            try
            {
                DbCommand totalQuestionAuthoredChartCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_CONTRIBUTOR_CHART_DATA");

                HCMDatabase.AddInParameter(totalQuestionAuthoredChartCommand,
                    "@CREATED_BY", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(totalQuestionAuthoredChartCommand);

                while (dataReader.Read())
                {
                    ChartData authoredChartData = new ChartData();
                    ChartData questionUsageChartData = new ChartData();
                    ChartData creditsEarnedChartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["MONTH_NAME"]))
                    {
                        authoredChartData.ChartXValue = dataReader["MONTH_NAME"].ToString();
                        questionUsageChartData.ChartXValue = dataReader["MONTH_NAME"].ToString();
                        creditsEarnedChartData.ChartXValue = dataReader["MONTH_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_COUNT"]))
                    {
                        authoredChartData.ChartYValue = int.Parse(dataReader["QUESTION_COUNT"].ToString());
                    }
                    contributorSummary.QuestionsAuthoredData.ChartData.Add(authoredChartData);

                    //Read the question usage count details
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_USAGE_COUNT"]))
                    {
                        questionUsageChartData.ChartYValue = int.Parse(dataReader["QUESTION_USAGE_COUNT"].ToString());
                    }
                    contributorSummary.QuestionUsageCountData.ChartData.Add(questionUsageChartData);
                    //Read contributor's credits earned details
                    if (!Utility.IsNullOrEmpty(dataReader["CREDITS_EARNED"]))
                    {
                        creditsEarnedChartData.ChartYValue = decimal.ToInt32(Convert.ToDecimal(dataReader["CREDITS_EARNED"].ToString()));
                    }
                    contributorSummary.CreditsEarnedData.ChartData.Add(creditsEarnedChartData);
                }
                return contributorSummary;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the questions for the Automated test
        /// </summary>
        /// <param name="testSearchCriteria">search criterial list provided by the user</param>
        /// <param name="TotalRecords">total record picked by the data base for the search critera
        /// provided by the user (Note:- this is 'out' parameter column)</param>
        /// <returns>List of question details that satisfies the search 
        /// criteria provided by the user</returns>
        public List<QuestionDetail> GetAutomatedQuestions(QuestionType questionType, TestSearchCriteria testSearchCriteria,int questionAuthor,  out int TotalRecords)
        {
            IDataReader dataReader = null;
            try
            {
                TotalRecords = 0;
                DbCommand getAutomatedQuestions = HCMDatabase.GetStoredProcCommand("SPGET_AUTOMATED_TEST_QUESTIONS");
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@QUESTION_TYPE", DbType.Int16, (int)questionType);
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@STARTINDEX", DbType.Int32, testSearchCriteria.TestStartIndex);
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@ENDINDEX", DbType.Int32, testSearchCriteria.TestEndIndex);
                if (!Utility.IsNullOrEmpty(testSearchCriteria.TestAreasID))
                    HCMDatabase.AddInParameter(getAutomatedQuestions, "@TESTAREA", DbType.String, testSearchCriteria.TestAreasID);
                if (!Utility.IsNullOrEmpty(testSearchCriteria.Complexity))
                    HCMDatabase.AddInParameter(getAutomatedQuestions, "@COMPLEXITY", DbType.String, testSearchCriteria.Complexity);
                if (!Utility.IsNullOrEmpty(testSearchCriteria.Keyword))
                    HCMDatabase.AddInParameter(getAutomatedQuestions, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@CAT_SUB_ID", DbType.Int32, ((testSearchCriteria.Subjects == null) ? 0 :
                    testSearchCriteria.Subjects[0].SubjectID));
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@AUTHOR_ID", DbType.Int32, questionAuthor);
                dataReader = HCMDatabase.ExecuteReader(getAutomatedQuestions);
                List<QuestionDetail> questionDetails = null;
                QuestionDetail questionDetail = null;
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["Total"]))
                        TotalRecords = Convert.ToInt32(dataReader["Total"]);
                    else
                    {
                        if (Utility.IsNullOrEmpty(questionDetail))
                            questionDetail = new QuestionDetail();
                        if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]))
                            questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESCRIPTION"]))
                            questionDetail.Question = dataReader["QUESTION_DESCRIPTION"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                            questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                            questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_ID"]))
                            questionDetail.CategoryID = Convert.ToInt32(dataReader["CATEGORY_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_ID"]))
                            questionDetail.SubjectID = Convert.ToInt32(dataReader["SUBJECT_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY"]))
                            questionDetail.ComplexityName = dataReader["COMPLEXITY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                            questionDetail.Complexity = dataReader["COMPLEXITY_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CORRECT_ANSWER"]))
                            questionDetail.AnswerID = dataReader["CORRECT_ANSWER"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                            questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                            questionDetail.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_VALUE"]))
                            questionDetail.ComplexityValue = Convert.ToDecimal(dataReader["COMPLEXITY_VALUE"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CREDIT_EARNED"]))
                            questionDetail.CreditsEarned = Convert.ToDecimal(dataReader["CREDIT_EARNED"]);
                        if (!Utility.IsNullOrEmpty(dataReader["AVG_TIME_TAKEN"]))
                            questionDetail.AverageTimeTaken = Convert.ToDecimal(dataReader["AVG_TIME_TAKEN"]);
                        if (!Utility.IsNullOrEmpty(dataReader["QUESTION_RELATION_ID"]))
                            questionDetail.QuestionRelationId = Convert.ToInt32(dataReader["QUESTION_RELATION_ID"]);
                        if (Utility.IsNullOrEmpty(questionDetails))
                            questionDetails = new List<QuestionDetail>();
                        questionDetails.Add(questionDetail);
                        questionDetail = null;
                    }
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }

            }
        }
        
        #endregion


        /// <summary>
        /// This method handles the new question entry functions
        /// </summary>
        /// <param name="questionDetail">
        /// A <see cref="string"/>that holds the question detail collection
        /// </param>
        /// <param name="transaction">
        /// A <see cref="string"/>that holds the transaction object
        /// </param>
        public void InsertInterviewQuestion(QuestionDetail questionDetail, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_INTERVIEW_QUESTION");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@QUESTION_KEY", DbType.String, questionDetail.QuestionKey);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@QUESTION_DESC", DbType.String, questionDetail.Question);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@COMPLEXITY_ID", DbType.String, questionDetail.Complexity);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@TAGS", DbType.String, questionDetail.Tag);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CREDIT_EARNED", DbType.Decimal, questionDetail.CreditsEarned);
            if(Utility.IsNullOrEmpty(questionDetail.IsActiveFlag))
            {
                HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ACTIVE_FLAG", DbType.String, "Y");
            }
            else
            {
                HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ACTIVE_FLAG", DbType.String, questionDetail.IsActiveFlag);
            }
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@NO_OF_CHOICES", DbType.Int16, questionDetail.NoOfChoices);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@AUTHOR", DbType.Int32, questionDetail.Author);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@TEST_AREA_ID", DbType.String, questionDetail.TestAreaID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CREATED_BY", DbType.Int32, questionDetail.CreatedBy);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@MODIFIED_BY", DbType.Int32, questionDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method helps to insert question relation according to the question and subject ids
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="listOfSubjectIDs">
        /// A <see cref="string"/>that holds the list of subject Id
        /// </param>
        /// <param name="createdBy">
        /// A <see cref="int"/>that holds the creadted by
        /// </param>
        /// <param name="modifiedBy">
        /// A <see cref="int"/> that holds the modified by
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object
        /// </param>
        public void InsertInterviewQuestionRelation(string questionKey, string listOfSubjectIDs,
            int createdBy, int modifiedBy, IDbTransaction transaction)
        {
            DbCommand insertQuestionRelationCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_QUESTION_RELATION");

            // Add input paramater.
            HCMDatabase.AddInParameter(insertQuestionRelationCommand,
                   "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertQuestionRelationCommand,
                   "@CAT_SUB_ID", DbType.String, listOfSubjectIDs);
            HCMDatabase.AddInParameter(insertQuestionRelationCommand,
                   "@CREATED_BY", DbType.Int32, createdBy);
            HCMDatabase.AddInParameter(insertQuestionRelationCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);

            HCMDatabase.ExecuteNonQuery(insertQuestionRelationCommand, transaction as DbTransaction);
        }
        /// <summary>
        /// This method is used to insert the question image
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question key
        /// </param>
        /// <param name="questionImage">
        /// A <see cref="byte"/>that holds the question image
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/>that holds the user id
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/>that holds the transaction object
        /// </param>
        public void InsertInterviewQuestionImage(string questionKey, byte[] questionImage,
            int user, IDbTransaction transaction)
        {
            DbCommand insertQuestionImageCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_QUESTION_IMAGE");

            // Add input paramater.
            HCMDatabase.AddInParameter(insertQuestionImageCommand,
                   "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertQuestionImageCommand,
                   "@ILLUSTRATION", DbType.Binary, questionImage);
            HCMDatabase.AddInParameter(insertQuestionImageCommand,
                "@CREATED_BY", DbType.Int32, user);
            HCMDatabase.AddInParameter(insertQuestionImageCommand,
                "@MODIFIED_BY", DbType.Int32, user);

            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertQuestionImageCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that checks whether the attributes of a question
        /// are valid or not
        /// </summary>
        /// <param name="question">
        /// A<see cref="QuestionDetail"/>QuestionDetail that contains 
        /// the detail of the question.
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>value that contain question status
        /// </returns>
        public QuestionDetail CheckInterviewQuestionAttribute(QuestionDetail question)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand checkQuestionAttributeCommand = HCMDatabase.
            GetStoredProcCommand("SPGET_CHECK_INTERVIEW_QUESTION_ATTRIBUTES");

                if (!Utility.IsNullOrEmpty(question.Question))
                {
                    HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@QUESTION", DbType.String, question.Question.Replace("\r\n", "\n").Replace("\n", "\r\n"));
                }
                else
                {
                    HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@QUESTION", DbType.String, question.Question);
                }

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                  "@SUBJECT_NAME", DbType.String, question.SubjectName);

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@CATEGORY_NAME", DbType.String, question.CategoryName);

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@COMPLEXITY", DbType.String, question.ComplexityName);

                HCMDatabase.AddInParameter(checkQuestionAttributeCommand,
                    "@TEST_AREA", DbType.String, question.TestAreaName);

                HCMDatabase.AddOutParameter(checkQuestionAttributeCommand,
                    "@RESULT", DbType.Int32, 32);

                dataReader = HCMDatabase.ExecuteReader(checkQuestionAttributeCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["INVALID_QUESTION_REMARKS"]))
                    {
                        question.InvalidQuestionRemarks += dataReader
                            ["INVALID_QUESTION_REMARKS"].ToString().Trim() + ", ";
                        //if (!Utility.IsNullOrEmpty(question.InvalidQuestionRemarks))
                        //{
                        //    question.InvalidQuestionRemarks = question.InvalidQuestionRemarks + dataReader
                        //    ["INVALID_QUESTION_REMARKS"].ToString().Trim() + ", ";
                        //}
                        //else
                        //{
                        //    question.InvalidQuestionRemarks = dataReader
                        //    ["INVALID_QUESTION_REMARKS"].ToString().Trim() + ", ";
                        //}
                    }
                }
                if (question.InvalidQuestionRemarks == null)
                    question.InvalidQuestionRemarks = "";
                //question.InvalidQuestionRemarks = question.InvalidQuestionRemarks.TrimEnd(' ').TrimEnd(',');
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["RESULT"]))
                    {
                        int value = int.Parse(dataReader["RESULT"].ToString());
                        question.IsValid = value == 0 ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_ID"]))
                    {
                        question.CategoryID = int.Parse(dataReader["CATEGORY_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_ID"]))
                    {
                        question.SubjectID = int.Parse(dataReader["SUBJECT_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                    {
                        question.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                    {
                        question.Complexity = dataReader["COMPLEXITY_ID"].ToString();
                    }
                }

                return question;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method is used to insert answer choices against the given question key
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question key
        /// </param>
        /// <param name="answerChoice">
        /// A <see cref="AnswerChoice"/>that holds the answer choice
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/>that holds the user id
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/>that holds the transaction object
        /// </param>
        public void InsertInterviewAnswerChoice(string questionKey,
            AnswerChoice answerChoice, int user, IDbTransaction transaction)
        {
            DbCommand insertAnswerChoiceCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEW_OPTIONS");

            // Add input paramater.
            HCMDatabase.AddInParameter(insertAnswerChoiceCommand,
                   "@CHOICE_ID", DbType.Int32, answerChoice.ChoiceID);
            HCMDatabase.AddInParameter(insertAnswerChoiceCommand,
                   "@CHOICE_DESC", DbType.String, answerChoice.Choice);
            HCMDatabase.AddInParameter(insertAnswerChoiceCommand,
                   "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertAnswerChoiceCommand,
                "@USER_ID", DbType.Int32, user);

            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertAnswerChoiceCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the list of Questions for the given search 
        /// </summary>
        /// <param name="searchCriteria">
        /// A list of <see cref="QuestionDetailSearchCriteria"/> that holds the questionKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the QuestionDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<QuestionDetail> GetInterviewQuestions(QuestionDetailSearchCriteria searchCriteria,
            int pageSize, int pageNumber, string orderBy, SortType direction, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                DbCommand getQuestionDeatilsCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_QUESTIONS");

                string creditEarned = null;
                if (searchCriteria.CreditsEarned > 0)
                {
                    creditEarned = searchCriteria.CreditsEarned.ToString();
                }
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@TENANT_ID", DbType.String, searchCriteria.TenantID);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@QUESTION_KEY", DbType.String, searchCriteria.QuestionKey);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CAT_SUB", DbType.String, searchCriteria.CategorySubjectsKey);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CATEGORY", DbType.String, searchCriteria.CategoryName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@SUBJECT", DbType.String, searchCriteria.SubjectName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@KEYWORD", DbType.String, searchCriteria.Tag);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@TEST_AREA", DbType.String, searchCriteria.TestAreaID);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@COMPLEXITY", DbType.String, searchCriteria.Complexities);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ACTIVE_FLAG", DbType.String, searchCriteria.ActiveFlag);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@AUTHOR", DbType.String, searchCriteria.AuthorName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@AUTHOR_ID", DbType.Int32, searchCriteria.Author);    
                //HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CREDIT_EARNED", DbType.Decimal, creditEarned);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@PAGESIZE", DbType.Int32, pageSize);              
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                dataReader = HCMDatabase.ExecuteReader(getQuestionDeatilsCommand);

                List<QuestionDetail> questionDetails = null;
                while (dataReader.Read())
                {
                    if (questionDetails == null)
                        questionDetails = new List<QuestionDetail>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        QuestionDetail questionDetail = new QuestionDetail();
                        //int a = int.Parse(dataReader["CORECT_ANSWER"].ToString()) - 1;
                        questionDetail.QuestionID = int.Parse(dataReader["QUESTION_ID"].ToString());
                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString();
                        questionDetail.Complexity = dataReader["COMPLEXITY_NAME"].ToString();
                        questionDetail.ComplexityID = dataReader["COMPLEXITY_ID"].ToString();
                        //questionDetail.Answer = short.Parse(a.ToString());
                        questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                        questionDetail.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                        questionDetail.Status = (QuestionStatus)Convert.ToChar((dataReader["ACTIVE_FLAG"]));
                        questionDetail.ModifiedDate = System.Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                        //questionDetail.CreditsEarned = System.Convert.ToDecimal(dataReader["CREDIT_EARNED"]);
                        questionDetail.DataAccessRights = dataReader["DATA_ACCESS_RIGHTS"].ToString();
                        if (dataReader["TEST_APPEARED"] == DBNull.Value)
                        {
                            questionDetail.TestIncluded = 0;
                        }
                        else
                        {
                            questionDetail.TestIncluded = Convert.ToInt32(dataReader["TEST_APPEARED"]);
                        }
                        questionDetails.Add(questionDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                dataReader.Close();
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// This method is used to Active/inactive question based on the question key.
        /// </summary>
        /// <param name="questionKey">
        ///  A <see cref="string"/> that holds the question Key.
        /// </param>
        /// <param name="questionStatus">
        ///  A <see cref="string"/> that holds the question Status.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void UpdateInterviewStatus(string questionKey, string questionStatus, int user)
        {
            DbCommand questionUpdateStatusCommand = HCMDatabase.
              GetStoredProcCommand("SPDELETE_INTERVIEWQUESTION");
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@ACTIVE_FLAG", DbType.String, questionStatus);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@DELETED", DbType.String, null);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
               "@USERID", DbType.Int32, user);
            HCMDatabase.ExecuteNonQuery(questionUpdateStatusCommand);
        }
        /// <summary>
        /// This method is used to Delete question based on the question key.
        /// </summary>
        /// <param name="questionKey">
        ///  A <see cref="string"/> that holds the questionKey.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeleteInterviewQuestion(string questionKey, int user)
        {
            DbCommand questionUpdateStatusCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_INTERVIEWQUESTION");
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@ACTIVE_FLAG", DbType.String, null);
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@DELETED", DbType.String, "Y");
            HCMDatabase.AddInParameter(questionUpdateStatusCommand,
                "@USERID", DbType.Int32, user);
            HCMDatabase.ExecuteNonQuery(questionUpdateStatusCommand);
        }
        /// <summary>
        /// Gets the question relation ID.
        /// </summary>
        /// <param name="QuestionKey">The question key.</param>
        /// <returns></returns>
        public int GetQuestionRelationID(string QuestionKey)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getQuestionRelationCommand = HCMDatabase.GetStoredProcCommand("SPGET_QUESTION_RELATION_ID");
                HCMDatabase.AddInParameter(getQuestionRelationCommand, "@QUESTION_KEY", DbType.String, QuestionKey);
                dataReader = HCMDatabase.ExecuteReader(getQuestionRelationCommand);

                QuestionDetail questionDetail = new QuestionDetail();

                while (dataReader.Read())
                {
                    questionDetail.QuestionRelationId = Convert.ToInt16(dataReader["RELATION_ID"].ToString());
                }

                return questionDetail.QuestionRelationId;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the category.
        /// </summary>
        /// <param name="QuestionKey">The question key.</param>
        /// <returns></returns>
        public string GetCategory(string QuestionKey)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getQuestionRelationCommand = HCMDatabase.GetStoredProcCommand("SPGET_CATEGORY_INTERVIEW");
                HCMDatabase.AddInParameter(getQuestionRelationCommand, "@QUESTION_KEY", DbType.String, QuestionKey);
                dataReader = HCMDatabase.ExecuteReader(getQuestionRelationCommand);

                QuestionDetail questionDetail = new QuestionDetail();

                while (dataReader.Read())
                {
                    questionDetail.CategoryName = dataReader["CATGORY"].ToString();
                }

                return questionDetail.CategoryName;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the subject.
        /// </summary>
        /// <param name="QuestionKey">The question key.</param>
        /// <returns></returns>
        public string GetSubject(string QuestionKey)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getQuestionRelationCommand = HCMDatabase.GetStoredProcCommand("SPGET_SUBJECT_INTERVIEW");
                HCMDatabase.AddInParameter(getQuestionRelationCommand, "@QUESTION_KEY", DbType.String, QuestionKey);
                dataReader = HCMDatabase.ExecuteReader(getQuestionRelationCommand);

                QuestionDetail questionDetail = new QuestionDetail();

                while (dataReader.Read())
                {
                    questionDetail.SubjectName = dataReader["SUBJECT"].ToString();
                }

                return questionDetail.SubjectName;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        
        
        /// <summary>
        /// Gets the questions for the Automated test
        /// </summary>
        /// <param name="testSearchCriteria">search criterial list provided by the user</param>
        /// <param name="TotalRecords">total record picked by the data base for the search critera
        /// provided by the user (Note:- this is 'out' parameter column)</param>
        /// <returns>List of question details that satisfies the search 
        /// criteria provided by the user</returns>
        public List<QuestionDetail> GetAutomatedInterviewQuestions(TestSearchCriteria testSearchCriteria, 
            int questionAuthor, int tenantID, out int TotalRecords)
        {
            IDataReader dataReader = null;
            int displayOrder = 0;
            try
            {
                TotalRecords = 0;
                DbCommand getAutomatedQuestions = HCMDatabase.GetStoredProcCommand("SPGET_AUTOMATED_INTERVIEW_QUESTIONS");
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@TENANT_ID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@STARTINDEX", DbType.Int32, testSearchCriteria.TestStartIndex);
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@ENDINDEX", DbType.Int32, testSearchCriteria.TestEndIndex);
                if (!Utility.IsNullOrEmpty(testSearchCriteria.TestAreasID))
                    HCMDatabase.AddInParameter(getAutomatedQuestions, "@TESTAREA", DbType.String, testSearchCriteria.TestAreasID);
                if (!Utility.IsNullOrEmpty(testSearchCriteria.Complexity))
                    HCMDatabase.AddInParameter(getAutomatedQuestions, "@COMPLEXITY", DbType.String, testSearchCriteria.Complexity);
                if (!Utility.IsNullOrEmpty(testSearchCriteria.Keyword))
                    HCMDatabase.AddInParameter(getAutomatedQuestions, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@CAT_SUB_ID", DbType.Int32, ((testSearchCriteria.Subjects == null) ? 0 :
                    testSearchCriteria.Subjects[0].SubjectID));
                HCMDatabase.AddInParameter(getAutomatedQuestions, "@AUTHOR_ID", DbType.Int32, questionAuthor);
                dataReader = HCMDatabase.ExecuteReader(getAutomatedQuestions);
                List<QuestionDetail> questionDetails = null;
                QuestionDetail questionDetail = null;
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["Total"]))
                        TotalRecords = Convert.ToInt32(dataReader["Total"]);
                    else
                    {
                        if (Utility.IsNullOrEmpty(questionDetail))
                            questionDetail = new QuestionDetail();
                        if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]))
                            questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESCRIPTION"]))
                            questionDetail.Question = dataReader["QUESTION_DESCRIPTION"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                            questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                            questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_ID"]))
                            questionDetail.CategoryID = Convert.ToInt32(dataReader["CATEGORY_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_ID"]))
                            questionDetail.SubjectID = Convert.ToInt32(dataReader["SUBJECT_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY"]))
                            questionDetail.ComplexityName = dataReader["COMPLEXITY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                            questionDetail.Complexity = dataReader["COMPLEXITY_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CORRECT_ANSWER"]))
                            questionDetail.AnswerID = dataReader["CORRECT_ANSWER"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                            questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                            questionDetail.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_VALUE"]))
                            questionDetail.ComplexityValue = Convert.ToDecimal(dataReader["COMPLEXITY_VALUE"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CREDIT_EARNED"]))
                            questionDetail.CreditsEarned = Convert.ToDecimal(dataReader["CREDIT_EARNED"]);
                        if (!Utility.IsNullOrEmpty(dataReader["AVG_TIME_TAKEN"]))
                            questionDetail.AverageTimeTaken = Convert.ToDecimal(dataReader["AVG_TIME_TAKEN"]);
                        if (!Utility.IsNullOrEmpty(dataReader["QUESTION_RELATION_ID"]))
                            questionDetail.QuestionRelationId = Convert.ToInt32(dataReader["QUESTION_RELATION_ID"]);
                        questionDetail.DisplayOrder = displayOrder + 1;
                        if (Utility.IsNullOrEmpty(questionDetails))
                            questionDetails = new List<QuestionDetail>();
                        questionDetails.Add(questionDetail);
                        questionDetail = null;
                        displayOrder = displayOrder + 1;
                    }
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }

            }
        }
       
    }
}