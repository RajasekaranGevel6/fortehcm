﻿using System.Data;
using System.Data.Common;

using Forte.HCM.Common;
using Forte.HCM.Support;
using Forte.HCM.Common.DL; 

namespace Forte.HCM.DL
{
    public class EmailExceptionDLManager : DatabaseConnectionManager
    {

        public int InsertEmailFailureLog(string mailMessage, string fromEmailId,
            string toEmailId, string ccEmailId, string failureEmailId, string exceptionMessage,
            int UserID,string subject,string isHTML )
        {
            try
            {

                string ccId = null;
                string toId = null;

                ccId = FailureCCEmailID(ccEmailId,failureEmailId);
                toId = FailureToEmailID(toEmailId, failureEmailId);

                DbCommand insertEmailFailureLogCommand =
                    HCMDatabase.GetStoredProcCommand("SPINSERT_EMAIL_FAILURE_LOG");

                HCMDatabase.AddInParameter(insertEmailFailureLogCommand,
                   "@MAIL_MESSAGE", DbType.String, mailMessage);

                HCMDatabase.AddInParameter(insertEmailFailureLogCommand,
                    "@FROM_EMAIL_ID", DbType.String, fromEmailId);

                if (!Utility.IsNullOrEmpty(toId))
                {
                    HCMDatabase.AddInParameter(insertEmailFailureLogCommand,
                        "@TO_EMAIL_ID", DbType.String, toId);
                } 
                if (!Utility.IsNullOrEmpty(ccId))
                {
                    HCMDatabase.AddInParameter(insertEmailFailureLogCommand,
                        "@CC_EMAIL_ID", DbType.String, ccId);
                }
                
                HCMDatabase.AddInParameter(insertEmailFailureLogCommand,
                    "@SUBJECT", DbType.String, subject);

                HCMDatabase.AddInParameter(insertEmailFailureLogCommand,
                    "@ISHTML", DbType.String, isHTML); 

                HCMDatabase.AddInParameter(insertEmailFailureLogCommand,
                    "@FAILURE_MESSAGE", DbType.String, exceptionMessage);

                HCMDatabase.AddInParameter(insertEmailFailureLogCommand,
                 "@CREATE_BY", DbType.String, UserID);

                HCMDatabase.ExecuteNonQuery(insertEmailFailureLogCommand); 
                
                return 1;
            }
            catch
            {
                return 0;
            } 
        }


        /// <summary>
        /// This function returns failure CC emails ids 
        /// </summary>
        /// <param name="ccMails"></param>
        /// <param name="failureCCMails"></param>
        /// <returns></returns>
        private string FailureCCEmailID(string ccMails,string failureCCMails)
        {
            
            if (Utility.IsNullOrEmpty(ccMails)) return null;

            if (Utility.IsNullOrEmpty(failureCCMails)) return null;

            string failureCCEmailIDs = null;

            failureCCMails = failureCCMails.Replace(',',';');

            string[] failureCCMailsArray = failureCCMails.Split(';');

            if (failureCCMailsArray.Length == 0) return null;
             
            foreach (string failureCC in failureCCMailsArray)
            {
                if (ccMails.Contains(failureCC))
                {
                    failureCCEmailIDs += failureCC + ","; 
                }
            }

            if (!Utility.IsNullOrEmpty(failureCCEmailIDs))
            {
                failureCCEmailIDs = failureCCEmailIDs.Remove(failureCCEmailIDs.LastIndexOf(','));
            } 

            return failureCCEmailIDs;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toMail"></param>
        /// <param name="failureToMail"></param>
        /// <returns></returns>
        private string FailureToEmailID(string toMail, string failureToMail)
        {
            if (Utility.IsNullOrEmpty(toMail)) return null;

            if (Utility.IsNullOrEmpty(failureToMail)) return null;

            string failureToEmailID = null;

            failureToMail = failureToMail.Replace(',', ';');

            string[] failureToMailArray = failureToMail.Split(';');

            if (failureToMailArray.Length == 0) return null; 
             
            foreach (string failureToAddr in failureToMailArray)
            {
                if (toMail.Contains(failureToAddr))
                {
                    failureToEmailID += failureToAddr + ",";
                }
            }

            if (!Utility.IsNullOrEmpty(failureToEmailID))
            {
                failureToEmailID = failureToEmailID.Remove(failureToEmailID.LastIndexOf(','));
            } 

            return failureToEmailID;
        }
    }

}
