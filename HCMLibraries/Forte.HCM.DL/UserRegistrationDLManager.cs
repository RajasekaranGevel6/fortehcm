﻿
#region Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

#endregion

namespace Forte.HCM.DL
{
    /// <summary>
    /// 
    /// </summary>
    public class UserRegistrationDLManager : DatabaseConnectionManager
    {

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public UserRegistrationDLManager()
        {
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// Method that checks whether the user email id exists in repository or not.
        /// </summary>
        /// <param name="usrEmail">
        /// A <see cref="System.String"/> that holds the user email id
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that contains whether user email 
        /// exists in Registration repository or not.
        /// </returns>
        public bool CheckUserEmailExists(string usrEmail, int tenantID)
        {
            IDataReader dataReader = null;
            DbCommand CheckUserDbCommand = null;
            try
            {
                CheckUserDbCommand = HCMDatabase.GetStoredProcCommand("SPCHECK_USEREMAIL");
                HCMDatabase.AddInParameter(CheckUserDbCommand, "@TENANT_ID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(CheckUserDbCommand, "@USER_NAME", DbType.String, usrEmail);
                dataReader = HCMDatabase.ExecuteReader(CheckUserDbCommand);
                if (!dataReader.Read())
                    return true;
                return false;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed) dataReader.Close();
                if (dataReader != null) dataReader = null;
                if (CheckUserDbCommand != null) CheckUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that inserts a user to the registration repository
        /// </summary>
        /// <param name="TENANT_USER_ID">
        /// A <see cref="System.Int32"/> that holds the tenant user id
        /// </param>
        /// <param name="userRegistrationInfo">
        /// A <see cref="System.String"/> that holds the user email
        /// A <see cref="System.String"/> that holds the user password
        /// A <see cref="System.String"/> that holds the user first name
        /// A <see cref="System.String"/> that holds the user last name
        /// A <see cref="System.String"/> that holds the user Email
        /// A <see cref="System.String"/> that holds the user phone number
        /// A <see cref="System.String"/> that holds the user company name
        /// A <see cref="System.String"/> that holds the user title
        /// A <see cref="System.Int32"/> that holds the user subscription type id
        /// A <see cref="System.Char"/> that holds the Activation Status
        /// A <see cref="System.Int16"/> that holds the number of users
        /// A <see cref="System.DateTime"/> that holds the account activated date
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <param name="registrationConfiration">
        /// A <see cref="System.Int32"/> that holds the user registration id
        /// A <see cref="System.String"/> that holds the user confirmation code
        /// A <see cref="System.Int32"/> that holds the number of attempts
        /// A <see cref="System.DateTime"/> that holds the verification date
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <param name="transaction">
        /// A <see cref="System.Data.Common.DbTransaction"/> that holds the transaction
        /// object
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of effected
        /// records in the registration Repository
        /// </returns>
        public int InsertUser(int? TENANT_USER_ID, UserRegistrationInfo userRegistrationInfo, RegistrationConfirmation registrationConfiration,
            DbTransaction transaction)
        {
            DbCommand InsertUserDbCommand = null;
            try
            {
                if (Utility.IsNullOrEmpty(transaction))
                    throw new NullReferenceException("Transaction should not be null");
                if (transaction.Connection.State == ConnectionState.Closed)
                    throw new InvalidOperationException("Transaction connection state should be in open");
                InsertUserDbCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_TENANT_USERS");
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TENANT_USER_ID", DbType.String, TENANT_USER_ID);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@BUSSINESS_TYPE", DbType.String, userRegistrationInfo.BussinessTypesIds);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@BUSSINESS_OTHERTYPE", DbType.String, userRegistrationInfo.BussinessTypeOther);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@USER_NAME", DbType.String, userRegistrationInfo.UserEmail);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@PASSWORD", DbType.String, userRegistrationInfo.Password);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@FIRST_NAME", DbType.String, userRegistrationInfo.FirstName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@LAST_NAME", DbType.String, userRegistrationInfo.LastName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@PHONE", DbType.String, userRegistrationInfo.Phone);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@COMPANY_NAME", DbType.String, userRegistrationInfo.Company);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TITLE", DbType.String, userRegistrationInfo.Title);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@SUBSCRIPTION_ID", DbType.String, userRegistrationInfo.SubscriptionId);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@SUBSRIPTION_ROLE", DbType.String, userRegistrationInfo.SubscriptionRole);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@NUMBER_OF_USERS", DbType.Int16, userRegistrationInfo.NumberOfUsers);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CONFIRMATION_CODE", DbType.String, registrationConfiration.ConfirmationCode);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@IS_TRIAL", DbType.Boolean, userRegistrationInfo.IsTrial);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TRIAL_DAYS", DbType.Int32, userRegistrationInfo.TrialDays);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@EXPIRY_DATE", DbType.DateTime, userRegistrationInfo.ExpiryDate);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@LEGAL_ACCEPTED", DbType.String, "N");
                return HCMDatabase.ExecuteNonQuery(InsertUserDbCommand, transaction);
            }
            finally
            {
                if (InsertUserDbCommand != null) InsertUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that checks and activates the user account
        /// </summary>
        /// <param name="User_Email">
        /// A <see cref="System.String"/> that holds the user email
        /// </param>
        /// <param name="Confirmation_Code">
        /// A <see cref="System.String"/> that holds the confirmation code
        /// for the user email id
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that holds the user first name
        /// </returns>
        public string CheckForActivationCode(string User_Email, string Confirmation_Code)
        {
            DbCommand CheckAndActivateCodeDbCommand = null;
            try
            {
                CheckAndActivateCodeDbCommand = HCMDatabase.GetStoredProcCommand("SPCHECK_ACTIVATE_USER_EMAIL");
                HCMDatabase.AddInParameter(CheckAndActivateCodeDbCommand, "@USER_NAME", DbType.String, User_Email);
                HCMDatabase.AddInParameter(CheckAndActivateCodeDbCommand, "@CONFIRMATION_CODE", DbType.String, Confirmation_Code);
                return HCMDatabase.ExecuteScalar(CheckAndActivateCodeDbCommand).ToString();
            }
            finally
            {
                if (CheckAndActivateCodeDbCommand != null) CheckAndActivateCodeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that gets the tenant name for the user id
        /// </summary>
        /// <param name="Tenant_user_Id">
        /// A <see cref="System.Int32"/> that holds the user id
        /// </param>
        /// <param name="SortExpression">
        /// A <see cref="System.String"/> that holds the sort expression
        /// </param>
        /// <param name="sortType">
        /// A <see cref="Forte.HCM.DataObjects"/> that holds the sort direction
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        public List<UserRegistrationInfo> GetSelectedCorporateAccountUsers(int Tenant_user_Id, string SortExpression, SortType sortType)
        {
            DbCommand GetUserCompanyNameDbCommand = null;
            IDataReader dataReader = null;
            List<UserRegistrationInfo> UserRegistrationsInfo = null;
            UserRegistrationInfo userInfo = null;
            try
            {
                GetUserCompanyNameDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_USER_TENANT_NAME");
                HCMDatabase.AddInParameter(GetUserCompanyNameDbCommand, "@TENANT_ID", DbType.Int32, Tenant_user_Id);
                HCMDatabase.AddInParameter(GetUserCompanyNameDbCommand, "@SORT_EXPRESSION", DbType.String, SortExpression);
                HCMDatabase.AddInParameter(GetUserCompanyNameDbCommand, "@SORT_ORDER", DbType.String, sortType == SortType.Ascending ?
                            Constants.SortTypeConstants.ASCENDING :
                            Constants.SortTypeConstants.DESCENDING);
                dataReader = HCMDatabase.ExecuteReader(GetUserCompanyNameDbCommand);
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(userInfo))
                        userInfo = new UserRegistrationInfo();
                    if (!Utility.IsNullOrEmpty(dataReader["TENANT_USER_ID"]))
                        userInfo.UserID = Convert.ToInt32(dataReader["TENANT_USER_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TENANT_ID"]))
                        userInfo.TenantID = Convert.ToInt32(dataReader["TENANT_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                        userInfo.UserEmail = dataReader["USER_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        userInfo.FirstName = dataReader["FIRST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        userInfo.LastName = dataReader["LAST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["NUMBER_OF_USERS"]))
                        userInfo.NumberOfUsers = Convert.ToInt16(dataReader["NUMBER_OF_USERS"]);
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE"]))
                        userInfo.Phone = dataReader["PHONE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY_NAME"]))
                        userInfo.Company = dataReader["COMPANY_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TITLE"]))
                        userInfo.Title = dataReader["TITLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVATED"]))
                        userInfo.Activated = Convert.ToChar(dataReader["ACTIVATED"]);
                    if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE"]))
                        userInfo.IsActive = Convert.ToInt16(dataReader["IS_ACTIVE"]);
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ROLE"]))
                        userInfo.SubscriptionRole = dataReader["SUBSCRIPTION_ROLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FORM_ID"]))
                        userInfo.FormId = Convert.ToInt32(dataReader["FORM_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["FORM_NAME"]))
                        userInfo.UserFormName = dataReader["FORM_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE_STATUS"]))
                        userInfo.IsActiveStatus = Convert.ToInt16(dataReader["IS_ACTIVE_STATUS"]);

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR"]))
                        userInfo.isAssessor =  dataReader["ASSESSOR"].ToString(); 
 
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        userInfo.UserEmail =  dataReader["EMAIL"].ToString(); 

                    if (Utility.IsNullOrEmpty(UserRegistrationsInfo))
                        UserRegistrationsInfo = new List<UserRegistrationInfo>();
                    UserRegistrationsInfo.Add(userInfo);
                    userInfo = null;
                }
                return UserRegistrationsInfo;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (GetUserCompanyNameDbCommand != null) GetUserCompanyNameDbCommand = null;
                if (!Utility.IsNullOrEmpty(userInfo)) userInfo = null;
                if (!Utility.IsNullOrEmpty(UserRegistrationsInfo)) UserRegistrationsInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that updates the corporate users
        /// details
        /// </summary>
        /// <param name="userRegistrationInfo">
        /// A <see cref="System.String"/> that holds the user email
        /// A <see cref="System.String"/> that holds the user password
        /// A <see cref="System.String"/> that holds the user first name
        /// A <see cref="System.String"/> that holds the user last name
        /// A <see cref="System.String"/> that holds the user Email
        /// A <see cref="System.String"/> that holds the user phone number
        /// A <see cref="System.String"/> that holds the user company name
        /// A <see cref="System.String"/> that holds the user title
        /// A <see cref="System.Int32"/> that holds the user subscription type id
        /// A <see cref="System.Char"/> that holds the Activation Status
        /// A <see cref="System.Int16"/> that holds the number of users
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of records updated
        /// </returns>
        public int UpdateCorporateUser(UserRegistrationInfo userRegistrationInfo)
        {
            DbCommand UpdateCorporateUserDbCommand = null;
            try
            {
                UpdateCorporateUserDbCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_CORPORATE_TENANT_USER");
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@TENANT_USER_ID", DbType.Int32, userRegistrationInfo.UserID);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@MODIFIED_BY", DbType.Int32, userRegistrationInfo.ModifiedBy);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@FIRST_NAME", DbType.String, userRegistrationInfo.FirstName);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@LAST_NAME", DbType.String, userRegistrationInfo.LastName);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@IS_ACTIVE", DbType.Int16, userRegistrationInfo.IsActive);
                return HCMDatabase.ExecuteNonQuery(UpdateCorporateUserDbCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateCorporateUserDbCommand)) UpdateCorporateUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that deletes the tenant user
        /// </summary>
        /// <param name="Tenant_User_Id">
        /// A <see cref="System.Int32"/> that holds the tenant user id
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the effected records for the
        /// command
        /// </returns>
        public int DeleteTenantUser(int Tenant_User_Id)
        {
            DbCommand DeleteTenantUserDbCommand = null;
            try
            {
                DeleteTenantUserDbCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_TENANT_USER");
                HCMDatabase.AddInParameter(DeleteTenantUserDbCommand, "@TENANT_USER_ID", DbType.Int32, Tenant_User_Id);
                return HCMDatabase.ExecuteNonQuery(DeleteTenantUserDbCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(DeleteTenantUserDbCommand)) DeleteTenantUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="System.Int32"/> that holds the user id of the 
        /// logged in user
        /// </param>
        /// <param name="TenantId">
        /// A <see cref="System.Int32"/> that hold the tenant id of the 
        /// logged in user
        /// </param>
        /// <param name="UserInfo">
        /// A <see cref="System.String"/> that holds the user email
        /// A <see cref="System.String"/> that holds the user password
        /// A <see cref="System.String"/> that holds the user first name
        /// A <see cref="System.String"/> that holds the user last name
        /// A <see cref="System.String"/> that holds the user Email
        /// A <see cref="System.String"/> that holds the user phone number
        /// A <see cref="System.String"/> that holds the user company name
        /// A <see cref="System.String"/> that holds the user title
        /// A <see cref="System.Int32"/> that holds the user subscription type id
        /// A <see cref="System.Char"/> that holds the Activation Status
        /// A <see cref="System.Int16"/> that holds the number of users
        /// A <see cref="System.DateTime"/> that holds the account activated date
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <param name="subscriptionTypes">
        /// A <see cref="System.Int32"/> that holds the Id of the Subscription
        /// A <see cref="System.String"/> that holds the name of the Subscription
        /// A <see cref="System.String"/> that holds the description of the Subscription
        /// A <see cref="System.String"/> that holds the published status of the Subscription
        /// A <see cref="System.Int32"/> that holds the created user id of the Subscription
        /// A <see cref="System.DateTime"/> that holds the created date of the Subscription
        /// A <see cref="System.Int32"/> that holds the modified user id of the Subscription
        /// A <see cref="System.DateTime"/> that holds the modified date of the Subscription
        /// A <see cref="System.Boolean"/> that holds the whether the subscription is deleted or not
        /// A <see cref="System.String"/> that holds the delted user id of the subscription
        /// A <see cref="System.String"/> that holds the concatenated values
        /// of subscription id and published status
        /// </param>
        /// <param name="subscriptionFeatures">
        /// A <see cref="System.Int32"/> that holds the Id of the subscription feature
        /// A <see cref="System.Int32"/> that holds the Id of the subscription
        /// A <see cref="System.Int32"/> that holds the Id of the feature
        /// A <see cref="System.String"/> that holds the feature name of the subscription
        /// A <see cref="System.String"/> that holds the feature value
        /// A <see cref="System.String"/> that holds the unit id 
        /// of the subscription feature
        /// A <see cref="System.String"/> that holds the Unit name 
        /// of the subscription feature
        /// A <see cref="System.Int16"/> that holds the not applicable
        /// value of the subscription feature
        /// A <see cref="System.String"/> that holds the default value
        /// of the subscription feature.
        /// A <see cref="System.Int16"/> that holds whether
        /// the feature is unlimited or not.
        /// A <see cref="System.Int32"/> that holds the Id of the created user
        /// A <see cref="System.DateTime"/> that holds the created date of the subscription feature
        /// A <see cref="System.Int32"/> that holds the id of the modified user 
        /// A <see cref="System.DateTime"/> that holds the modified date of the subscription feature
        /// </param>
        /// <param name="SubscriptionRole"></param>
        public void GetSubscriptionAccountDetailsUser(int UserId, int TenantId, out UserRegistrationInfo UserInfo,
            out SubscriptionTypes subscriptionTypes, out List<SubscriptionFeatures> subscriptionFeatures, string SubscriptionRole)
        {
            DbCommand GetSubscriptionAccountDetailsForCorporateUserDbCommand = null;
            IDataReader dataReader = null;
            SubscriptionFeatures subscriptionFeature = null;
            try
            {
                UserInfo = null;
                subscriptionFeatures = null;
                subscriptionTypes = null;
                GetSubscriptionAccountDetailsForCorporateUserDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_SUBSCRIPTION_ACCOUNT_DETAILS");
                HCMDatabase.AddInParameter(GetSubscriptionAccountDetailsForCorporateUserDbCommand, "@USRID", DbType.Int32, UserId);
                HCMDatabase.AddInParameter(GetSubscriptionAccountDetailsForCorporateUserDbCommand, "@TENANT_ID", DbType.Int32, TenantId);
                HCMDatabase.AddInParameter(GetSubscriptionAccountDetailsForCorporateUserDbCommand, "@SUBSCRIPTION_ROLE", DbType.String, SubscriptionRole);
                dataReader = HCMDatabase.ExecuteReader(GetSubscriptionAccountDetailsForCorporateUserDbCommand);
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(UserInfo))
                        UserInfo = new UserRegistrationInfo();
                    if (Utility.IsNullOrEmpty(subscriptionTypes))
                        subscriptionTypes = new SubscriptionTypes();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ROLE"]))
                        UserInfo.SubscriptionRole = dataReader["SUBSCRIPTION_ROLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_TYPE"]))
                        subscriptionTypes.SubscriptionName = dataReader["SUBSCRIPTION_TYPE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIBED_ON"]))
                        UserInfo.CreatedDate = Convert.ToDateTime(dataReader["SUBSCRIBED_ON"]);
                    if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE"]))
                        UserInfo.IsActive = Convert.ToInt16(dataReader["IS_ACTIVE"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVATED_ON"]))
                        UserInfo.ActivatedOn = Convert.ToDateTime(dataReader["ACTIVATED_ON"]);
                    if (!Utility.IsNullOrEmpty(dataReader["NUMBER_OF_USERS"]))
                        UserInfo.NumberOfUsers = Convert.ToInt16(dataReader["NUMBER_OF_USERS"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TITLE"]))
                        UserInfo.Title = dataReader["TITLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                        UserInfo.Company = dataReader["COMPANY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["IS_TRIAL"]))
                        UserInfo.IsTrial = Convert.ToInt16(dataReader["IS_TRIAL"]);
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE"]))
                        UserInfo.Phone = dataReader["PHONE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        UserInfo.FirstName = dataReader["FIRST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        UserInfo.LastName = dataReader["LAST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ID"]))
                        subscriptionTypes.SubscriptionId = Convert.ToInt32(dataReader["SUBSCRIPTION_ID"]);
                    //
                    //
                    //
                    dataReader.NextResult();
                    while (dataReader.Read())
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                            continue;
                        if (Utility.IsNullOrEmpty(subscriptionFeature))
                            subscriptionFeature = new SubscriptionFeatures();
                        if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_FEATURE_ID"]))
                            subscriptionFeature.SubscriptionFeatureId = Convert.ToInt32(dataReader["SUBSCRIPTION_FEATURE_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["FEATURE_NAME"]))
                            subscriptionFeature.FeatureName = dataReader["FEATURE_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["FEATURE_VALUE"]))
                            subscriptionFeature.FeatureValue = dataReader["FEATURE_VALUE"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ID"]))
                            subscriptionFeature.SubscriptionId = Convert.ToInt32(dataReader["SUBSCRIPTION_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["ISNOTAPPLICABLE"]))
                            subscriptionFeature.IsNotApplicable = Convert.ToInt16(dataReader["ISNOTAPPLICABLE"]);
                        if (!Utility.IsNullOrEmpty(dataReader["ISUNUNLIMITED"]))
                            subscriptionFeature.IsUnlimited = Convert.ToInt16(dataReader["ISUNUNLIMITED"]);
                        if (!Utility.IsNullOrEmpty(dataReader["FEATURE_UNIT"]))
                            subscriptionFeature.FeatureUnit = dataReader["FEATURE_UNIT"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["FEATURE_UNIT_ID"]))
                            subscriptionFeature.FeatureUnitId = dataReader["FEATURE_UNIT_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["DEFAULT_VALUE"]))
                            subscriptionFeature.DefaultValue = dataReader["DEFAULT_VALUE"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["FEATURE_ID"]))
                            subscriptionFeature.FeatureId = Convert.ToInt32(dataReader["FEATURE_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                            subscriptionFeature.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                            subscriptionFeature.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                        if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_BY"]))
                            subscriptionFeature.ModifiedBy = Convert.ToInt32(dataReader["MODIFIED_BY"]);
                        if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                            subscriptionFeature.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                        if (Utility.IsNullOrEmpty(subscriptionFeatures))
                            subscriptionFeatures = new List<SubscriptionFeatures>();
                        subscriptionFeatures.Add(subscriptionFeature);
                        subscriptionFeature = null;
                    }
                    //
                    //
                }
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(subscriptionFeature)) subscriptionFeature = null;
                if (!Utility.IsNullOrEmpty(GetSubscriptionAccountDetailsForCorporateUserDbCommand)) GetSubscriptionAccountDetailsForCorporateUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that updates the corporate user subscription details
        /// to the repository
        /// </summary>
        /// <param name="UserInfo">
        /// A <see cref="System.String"/> that holds the user email
        /// A <see cref="System.String"/> that holds the user password
        /// A <see cref="System.String"/> that holds the user first name
        /// A <see cref="System.String"/> that holds the user last name
        /// A <see cref="System.String"/> that holds the user Email
        /// A <see cref="System.String"/> that holds the user phone number
        /// A <see cref="System.String"/> that holds the user company name
        /// A <see cref="System.String"/> that holds the user title
        /// A <see cref="System.Int32"/> that holds the user subscription type id
        /// A <see cref="System.Char"/> that holds the Activation Status
        /// A <see cref="System.Int16"/> that holds the number of users
        /// A <see cref="System.DateTime"/> that holds the account activated date
        /// A <see cref="System.String"/> that holds the user created by
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// A <see cref="System.String"/> that holds the user modified by
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of effected
        /// records in the repository
        /// </returns>
        public int UpdateCorporateUserSubscriptionDetails(UserRegistrationInfo UserInfo)
        {
            DbCommand UpdateSubscriptionDetailsDBCommand = null;
            try
            {
                UpdateSubscriptionDetailsDBCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_ACCOUNT_DETAILS");
                HCMDatabase.AddInParameter(UpdateSubscriptionDetailsDBCommand, "@TENANT_USER_ID", DbType.Int32, UserInfo.UserID);
                HCMDatabase.AddInParameter(UpdateSubscriptionDetailsDBCommand, "@TITLE", DbType.String, UserInfo.Title);
                HCMDatabase.AddInParameter(UpdateSubscriptionDetailsDBCommand, "@COMPANY", DbType.String, UserInfo.Company);
                HCMDatabase.AddInParameter(UpdateSubscriptionDetailsDBCommand, "@PHONE", DbType.String, UserInfo.Phone);
                HCMDatabase.AddInParameter(UpdateSubscriptionDetailsDBCommand, "@FIRST_NAME", DbType.String, UserInfo.FirstName);
                HCMDatabase.AddInParameter(UpdateSubscriptionDetailsDBCommand, "@LAST_NAME", DbType.String, UserInfo.LastName);
                HCMDatabase.AddInParameter(UpdateSubscriptionDetailsDBCommand, "@MODIFIED_BY", DbType.Int32, UserInfo.ModifiedBy);
                HCMDatabase.AddInParameter(UpdateSubscriptionDetailsDBCommand, "@SUBSCRIPTION_ROLE", DbType.String, UserInfo.SubscriptionRole);
                return HCMDatabase.ExecuteNonQuery(UpdateSubscriptionDetailsDBCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateSubscriptionDetailsDBCommand)) UpdateSubscriptionDetailsDBCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that get the user registration info based on 
        /// the user id
        /// </summary>
        /// <param name="userID">
        /// A <see cref="System.Int32"/> that holds the user id
        /// </param>
        /// <returns>
        /// A <see cref="Forte.HCM.DataObjects.UserRegistrationInfo"/> that holds
        /// the user registration details
        /// </returns>
        public UserRegistrationInfo GetUserRegistrationInfo(int userID)
        {
            DbCommand GetUserRegistrationInfoDbCommand = null;
            UserRegistrationInfo userRegistrationInfo = null;
            IDataReader dataReader = null;
            try
            {
                GetUserRegistrationInfoDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_USER_REGISTRATION_INFO");
                HCMDatabase.AddInParameter(GetUserRegistrationInfoDbCommand, "@USRID", DbType.Int32, userID);
                dataReader = HCMDatabase.ExecuteReader(GetUserRegistrationInfoDbCommand);
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(userRegistrationInfo))
                        userRegistrationInfo = new UserRegistrationInfo();
                    if (!Utility.IsNullOrEmpty(dataReader["USRID"]))
                        userRegistrationInfo.UserID = Convert.ToInt32(dataReader["USRID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["USRUSERNAME"]))
                        userRegistrationInfo.UserEmail = dataReader["USRUSERNAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                        userRegistrationInfo.FirstName = dataReader["USRFIRSTNAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["USRLASTNAME"]))
                        userRegistrationInfo.LastName = dataReader["USRLASTNAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["USRPASSWORD"]))
                        userRegistrationInfo.Password = dataReader["USRPASSWORD"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ID"]))
                        userRegistrationInfo.SubscriptionId = Convert.ToInt32(dataReader["SUBSCRIPTION_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ROLE"]))
                        userRegistrationInfo.SubscriptionRole = dataReader["SUBSCRIPTION_ROLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TENANT_ID"]))
                        userRegistrationInfo.TenantID = Convert.ToInt32(dataReader["TENANT_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVATED"]))
                        userRegistrationInfo.Activated = Convert.ToChar(dataReader["ACTIVATED"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVATED_ON"]))
                        userRegistrationInfo.ActivatedOn = Convert.ToDateTime(dataReader["ACTIVATED_ON"]);
                    if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE"]))
                        userRegistrationInfo.IsActive = Convert.ToInt16(dataReader["IS_ACTIVE"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TITLE"]))
                        userRegistrationInfo.Title = dataReader["TITLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                        userRegistrationInfo.Company = dataReader["COMPANY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE"]))
                        userRegistrationInfo.Phone = dataReader["PHONE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["NUMBER_OF_USERS"]))
                        userRegistrationInfo.NumberOfUsers = Convert.ToInt16(dataReader["NUMBER_OF_USERS"]);
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ID"]))
                        userRegistrationInfo.SubscriptionId = Convert.ToInt32(dataReader["SUBSCRIPTION_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["BUSINESS_TYPE"]))
                        userRegistrationInfo.BussinessTypesIds = dataReader["BUSINESS_TYPE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["BUSINESS_OTHERTYPE"]))
                        userRegistrationInfo.BussinessTypeOther = dataReader["BUSINESS_OTHERTYPE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["CONFIRMATION_CODE"]))
                        userRegistrationInfo.ActivationCode = dataReader["CONFIRMATION_CODE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["USER_CORP_ADMIN_EMAIL"]))
                        userRegistrationInfo.UserCorporateAdminEmail = dataReader["USER_CORP_ADMIN_EMAIL"].ToString();
                }
                return userRegistrationInfo;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(GetUserRegistrationInfoDbCommand)) GetUserRegistrationInfoDbCommand = null;
                if (!Utility.IsNullOrEmpty(userRegistrationInfo)) userRegistrationInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// A Method that updates the subscription for a user
        /// </summary>
        /// <param name="userRegistrationInfo">
        /// A <see cref="Forte.HCM.DataObjects.UserRegistrationInfo"/> that holds
        /// the user registration details
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the effected records in the
        /// repository
        /// </returns>
        public int UpgradeAccountSubscriptionDetails(UserRegistrationInfo userRegistrationInfo)
        {
            DbCommand UpgradeAccountSubscriptionDetailsDbCommand = null;
            try
            {
                UpgradeAccountSubscriptionDetailsDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_UPGRADE_SUBSCRIPTION");
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@USRID", DbType.String, userRegistrationInfo.UserID);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@USRFIRSTNAME", DbType.String, userRegistrationInfo.FirstName);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@USRLASTNAME", DbType.String, userRegistrationInfo.LastName);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@TITLE", DbType.String, userRegistrationInfo.Title);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@COMPANY", DbType.String, userRegistrationInfo.Company);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@PHONE", DbType.String, userRegistrationInfo.Phone);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@NUMBER_OF_USERS", DbType.Int16, userRegistrationInfo.NumberOfUsers);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@SUBSCRIPTION_ID", DbType.Int32, userRegistrationInfo.SubscriptionId);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@BUSINESS_TYPE", DbType.String, userRegistrationInfo.BussinessTypesIds);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@BUSINESS_OTHERTYPE", DbType.String, userRegistrationInfo.BussinessTypeOther);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@IS_TRIAL", DbType.Int16, userRegistrationInfo.IsTrial);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@TRIAL_DAYS", DbType.Int32, userRegistrationInfo.TrialDays);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@SUBSCRIPTION_ROLE", DbType.String, userRegistrationInfo.SubscriptionRole);
                HCMDatabase.AddInParameter(UpgradeAccountSubscriptionDetailsDbCommand, "@MODIFIED_BY", DbType.Int32, userRegistrationInfo.ModifiedBy);
                return HCMDatabase.ExecuteNonQuery(UpgradeAccountSubscriptionDetailsDbCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpgradeAccountSubscriptionDetailsDbCommand)) UpgradeAccountSubscriptionDetailsDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// A Method that gets the system maximum corporate users
        /// </summary>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the maximum corporate users
        /// can created by a user
        /// </returns>
        public int GetMaximumCorporateUsers()
        {
            DbCommand GetMaximumCorporateUsersDbCommand = null;
            try
            {
                GetMaximumCorporateUsersDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_MAXIMUM_CORPORATE_USERS");
                object MaximumUsers = HCMDatabase.ExecuteScalar(GetMaximumCorporateUsersDbCommand);
                if (Utility.IsNullOrEmpty(MaximumUsers))
                    return 0;
                return Convert.ToInt32(MaximumUsers);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(GetMaximumCorporateUsersDbCommand)) GetMaximumCorporateUsersDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <returns></returns>
        public UserDetail GetPassword(int UserID)
        {
            DbCommand GetPasswordCommand = null;
            UserDetail userDetail = null;
            IDataReader dataReader = null;

            try
            {
                GetPasswordCommand = HCMDatabase.GetStoredProcCommand("SPGET_PASSWORD");
                HCMDatabase.AddInParameter(GetPasswordCommand, "@USRID", DbType.Int32, UserID);
                dataReader = HCMDatabase.ExecuteReader(GetPasswordCommand);
                if (dataReader.Read())
                {
                    userDetail = new UserDetail();
                    userDetail.Email = dataReader["USREMAIL"].ToString();
                    userDetail.FirstName = dataReader["USRFIRSTNAME"].ToString();
                    userDetail.LastName = dataReader["USRLASTNAME"].ToString();
                    userDetail.Password = dataReader["USRPASSWORD"].ToString();
                    userDetail.UserName = dataReader["USRUSERNAME"].ToString();
                }

                return userDetail;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(GetPasswordCommand)) GetPasswordCommand = null;
                if (!Utility.IsNullOrEmpty(userDetail)) userDetail = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        public UserDetail GetPassword(string UserName)
        {
            DbCommand GetPasswordCommand = null;
            UserDetail userDetail = null;
            IDataReader dataReader = null;

            try
            {
                GetPasswordCommand = HCMDatabase.GetStoredProcCommand("SPGET_PASSWORDBYUSERNAME");
                HCMDatabase.AddInParameter(GetPasswordCommand, "@USERNAME", DbType.String, UserName);
                dataReader = HCMDatabase.ExecuteReader(GetPasswordCommand);
                if (dataReader.Read())
                {
                    userDetail = new UserDetail();
                    userDetail.Email = dataReader["USREMAIL"].ToString();
                    userDetail.FirstName = dataReader["USRFIRSTNAME"].ToString();
                    userDetail.LastName = dataReader["USRLASTNAME"].ToString();
                    userDetail.Password = dataReader["USRPASSWORD"].ToString();
                    userDetail.UserName = dataReader["USRUSERNAME"].ToString();
                }

                return userDetail;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(GetPasswordCommand)) GetPasswordCommand = null;
                if (!Utility.IsNullOrEmpty(userDetail)) userDetail = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// Checks for activation code with transaction.
        /// </summary>
        /// <param name="User_Email">The user_ email.</param>
        /// <param name="Confirmation_Code">The confirmation_ code.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public string CheckForActivationCodeWithTransaction(string User_Email,
            string Confirmation_Code, IDbTransaction transaction)
        {
            DbCommand CheckAndActivateCodeDbCommand = null;
            try
            {
                CheckAndActivateCodeDbCommand = HCMDatabase.GetStoredProcCommand("SPCHECK_ACTIVATE_USER_EMAIL");
                HCMDatabase.AddInParameter(CheckAndActivateCodeDbCommand, "@USER_NAME", DbType.String, User_Email);
                HCMDatabase.AddInParameter(CheckAndActivateCodeDbCommand, "@CONFIRMATION_CODE", DbType.String, Confirmation_Code);
                return HCMDatabase.ExecuteScalar(CheckAndActivateCodeDbCommand, transaction as DbTransaction).ToString();
            }
            finally
            {
                if (CheckAndActivateCodeDbCommand != null) CheckAndActivateCodeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }


        /// <summary>
        /// Updates the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdatePassword(string password, int userID)
        {
            DbCommand UpdateCorporateUserDbCommand = null;
            try
            {
                UpdateCorporateUserDbCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_PASSWORD");
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@USER_ID", DbType.Int32, userID);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@PASSWORD", DbType.String, password);
                HCMDatabase.ExecuteNonQuery(UpdateCorporateUserDbCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateCorporateUserDbCommand)) UpdateCorporateUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }
        #endregion Public Methods

        public int InsertCorporateUser(int? TENANT_USER_ID, UserRegistrationInfo userRegistrationInfo, RegistrationConfirmation registrationConfiration, DbTransaction transaction)
        {

            DbCommand InsertUserDbCommand = null;
            try
            {
                if (Utility.IsNullOrEmpty(transaction))
                    throw new NullReferenceException("Transaction should not be null");
                if (transaction.Connection.State == ConnectionState.Closed)
                    throw new InvalidOperationException("Transaction connection state should be in open");
                InsertUserDbCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_TENANT_CORPORATE_USERS");
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TENANT_USER_ID", DbType.String, TENANT_USER_ID);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@BUSSINESS_TYPE", DbType.String, userRegistrationInfo.BussinessTypesIds);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@BUSSINESS_OTHERTYPE", DbType.String, userRegistrationInfo.BussinessTypeOther);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@USER_NAME", DbType.String, userRegistrationInfo.UserEmail);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@PASSWORD", DbType.String, userRegistrationInfo.Password);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@FIRST_NAME", DbType.String, userRegistrationInfo.FirstName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@LAST_NAME", DbType.String, userRegistrationInfo.LastName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@PHONE", DbType.String, userRegistrationInfo.Phone);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@COMPANY_NAME", DbType.String, userRegistrationInfo.Company);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TITLE", DbType.String, userRegistrationInfo.Title);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@SUBSCRIPTION_ID", DbType.String, userRegistrationInfo.SubscriptionId);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@SUBSRIPTION_ROLE", DbType.String, userRegistrationInfo.SubscriptionRole);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@NUMBER_OF_USERS", DbType.Int16, userRegistrationInfo.NumberOfUsers);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CONFIRMATION_CODE", DbType.String, registrationConfiration.ConfirmationCode);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@IS_TRIAL", DbType.Boolean, userRegistrationInfo.IsTrial);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TRIAL_DAYS", DbType.Int32, userRegistrationInfo.TrialDays);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@IS_ADMIN", DbType.Boolean, userRegistrationInfo.IsCustomerAdmin);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CORPORATE_ROLEIDS", DbType.String, userRegistrationInfo.CorporateRoles);

                // Add output parameters.
                HCMDatabase.AddOutParameter(InsertUserDbCommand,
                    "@USER_ID", DbType.Int32, 0);

                // Execute the query.
                HCMDatabase.ExecuteNonQuery(InsertUserDbCommand, transaction);

                if (InsertUserDbCommand.Parameters["@USER_ID"].Value != null)
                {
                    // Retrieve and assign user ID.
                    return int.Parse(InsertUserDbCommand.Parameters["@USER_ID"].Value.ToString());
                }
                else
                {
                    return 0;
                }
            }
            finally
            {
                if (InsertUserDbCommand != null) InsertUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }


        public bool GetCustomerAdmin(int userid)
        {
            DbCommand GetCustomerAdminCommand = null;
            IDataReader dataReader = null;
            try
            {

                GetCustomerAdminCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_ADMIN");
                HCMDatabase.AddInParameter(GetCustomerAdminCommand, "@USER_ID", DbType.String, userid);
                dataReader = HCMDatabase.ExecuteReader(GetCustomerAdminCommand);

                UserRegistrationInfo userDetail = null;

                if (dataReader.Read())
                {
                    userDetail = new UserRegistrationInfo();                    
                    userDetail.IsCustomerAdmin = Convert.ToBoolean(Convert.ToInt32(dataReader["COUNT"]));
                }
                return userDetail.IsCustomerAdmin;

            }
           
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(GetCustomerAdminCommand)) GetCustomerAdminCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        public int UpdateCorporateUserAdmin(UserRegistrationInfo userRegistrationInfo)
        {
            DbCommand UpdateCorporateUserDbCommand = null;
            try
            {
                UpdateCorporateUserDbCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_CORPORATE_ADMIN_TENANT_USER");
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@TENANT_USER_ID", DbType.Int32, userRegistrationInfo.UserID);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@MODIFIED_BY", DbType.Int32, userRegistrationInfo.ModifiedBy);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@FIRST_NAME", DbType.String, userRegistrationInfo.FirstName);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@LAST_NAME", DbType.String, userRegistrationInfo.LastName);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@IS_ACTIVE", DbType.Int16, userRegistrationInfo.IsActive);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@IS_ADMIN", DbType.Int16, userRegistrationInfo.IsCustomerAdmin);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@CORPORATE_ROLES", DbType.String, userRegistrationInfo.CorporateRoles);
                return HCMDatabase.ExecuteNonQuery(UpdateCorporateUserDbCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateCorporateUserDbCommand)) UpdateCorporateUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        public string GetcorporateRoles(int userid)
        {
            DbCommand GetCustomerAdminCommand = null;
            IDataReader dataReader = null;
            try
            {

                GetCustomerAdminCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_ROLEIDS");
                HCMDatabase.AddInParameter(GetCustomerAdminCommand, "@USER_ID", DbType.String, userid);
                dataReader = HCMDatabase.ExecuteReader(GetCustomerAdminCommand);

                UserRegistrationInfo userDetail = null;

                if (dataReader.Read())
                {
                    userDetail = new UserRegistrationInfo();
                    userDetail.CorporateRoles = dataReader["ROLID"].ToString();
                }
                return userDetail.CorporateRoles;

            }

            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(GetCustomerAdminCommand)) GetCustomerAdminCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        public int InsertCorporateUserRoles(UserRegistrationInfo userRegistrationInfo, DbTransaction dbTransaction)
        {
            DbCommand InsertUserDbCommand = null;
            try
            {
                if (Utility.IsNullOrEmpty(dbTransaction))
                    throw new NullReferenceException("Transaction should not be null");
                if (dbTransaction.Connection.State == ConnectionState.Closed)
                    throw new InvalidOperationException("Transaction connection state should be in open");

                InsertUserDbCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_COROPORATE_ROLES");

                HCMDatabase.AddInParameter(InsertUserDbCommand, "@USERID", DbType.Int16, userRegistrationInfo.UserID);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TENANTID", DbType.String, userRegistrationInfo.TenantID);

                return HCMDatabase.ExecuteNonQuery(InsertUserDbCommand, dbTransaction);
            }
            finally
            {
                if (InsertUserDbCommand != null) InsertUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        public void UpdateUserInfo(UserRegistrationInfo userEditInfo)
        {
            DbCommand UpdateCorporateUserDbCommand = null;
            try
            {
                UpdateCorporateUserDbCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_USER_DETAILS");
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@USERID", DbType.Int32, userEditInfo.UserID);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@FIRSTNAME", DbType.String, userEditInfo.FirstName);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@LASTNAME", DbType.String, userEditInfo.LastName);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@PHONE", DbType.String, userEditInfo.Phone);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@COMPANY", DbType.String, userEditInfo.Company);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@TITLE", DbType.String, userEditInfo.Title);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@NOOFUSERS", DbType.Int16, userEditInfo.NoOfUsers);
                HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@ROLES", DbType.String, userEditInfo.SelectedRoles);
              //  HCMDatabase.AddInParameter(UpdateCorporateUserDbCommand, "@ISACTIVE", DbType.Int16, userEditInfo.IsActive);               
                HCMDatabase.ExecuteNonQuery(UpdateCorporateUserDbCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateCorporateUserDbCommand)) UpdateCorporateUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        public List<UserRegistrationInfo> GetCorporateChileUsers(short user_ID)
        {
            DbCommand GetUserCompanyNameDbCommand = null;
            IDataReader dataReader = null;
            List<UserRegistrationInfo> UserRegistrationsInfo = null;
            UserRegistrationInfo userInfo = null;
            try
            {
                GetUserCompanyNameDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_CHILD_USERS");
                HCMDatabase.AddInParameter(GetUserCompanyNameDbCommand, "@USERID", DbType.String, user_ID);
                dataReader = HCMDatabase.ExecuteReader(GetUserCompanyNameDbCommand);
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(userInfo))
                        userInfo = new UserRegistrationInfo();
                    if (!Utility.IsNullOrEmpty(dataReader["TENANT_USER_ID"]))
                        userInfo.UserID = Convert.ToInt32(dataReader["TENANT_USER_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TENANT_ID"]))
                        userInfo.TenantID = Convert.ToInt32(dataReader["TENANT_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                        userInfo.UserEmail = dataReader["USER_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        userInfo.FirstName = dataReader["FIRST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        userInfo.LastName = dataReader["LAST_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["NUMBER_OF_USERS"]))
                        userInfo.NumberOfUsers = Convert.ToInt16(dataReader["NUMBER_OF_USERS"]);
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE"]))
                        userInfo.Phone = dataReader["PHONE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY_NAME"]))
                        userInfo.Company = dataReader["COMPANY_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TITLE"]))
                        userInfo.Title = dataReader["TITLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVATED"]))
                        userInfo.Activated = Convert.ToChar(dataReader["ACTIVATED"]);
                    if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE"]))
                        userInfo.ActiveUser = dataReader["IS_ACTIVE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ROLE"]))
                        userInfo.SubscriptionRole = dataReader["SUBSCRIPTION_ROLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FORM_ID"]))
                        userInfo.FormId = Convert.ToInt32(dataReader["FORM_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["FORM_NAME"]))
                        userInfo.UserFormName = dataReader["FORM_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE_STATUS"]))
                        userInfo.IsActiveStatus = Convert.ToInt16(dataReader["IS_ACTIVE_STATUS"]);
                    if (Utility.IsNullOrEmpty(UserRegistrationsInfo))
                        UserRegistrationsInfo = new List<UserRegistrationInfo>();
                    UserRegistrationsInfo.Add(userInfo);
                    userInfo = null;
                }
                return UserRegistrationsInfo;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (GetUserCompanyNameDbCommand != null) GetUserCompanyNameDbCommand = null;
                if (!Utility.IsNullOrEmpty(userInfo)) userInfo = null;
                if (!Utility.IsNullOrEmpty(UserRegistrationsInfo)) UserRegistrationsInfo = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        public double GetExpiryDate(int subscriptionId)
        {
            DbCommand GetExpiryDateDbCommand = null;
            IDataReader dataReader = null;

            UserRegistrationInfo userInfo = new UserRegistrationInfo();
            try
            {
                GetExpiryDateDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_EXPIRYDAYS");
                HCMDatabase.AddInParameter(GetExpiryDateDbCommand, "@SUBSRIPTION_ID", DbType.String, subscriptionId);
                dataReader = HCMDatabase.ExecuteReader(GetExpiryDateDbCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DAYS"]))
                        userInfo.ExpiryDays =Convert.ToDouble(dataReader["EXPIRY_DAYS"].ToString());
                }
                return userInfo.ExpiryDays;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the legal accepted status.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="accepted">
        /// A <see cref="bool"/> that holds the accepted status. True 
        /// represents accepted and false not.
        /// </param>
        public void UpdateLegalAcceptedStatus(int userID, bool accepted)
        {
            DbCommand updateStatusCommand = null;

            // Create command.
            updateStatusCommand = HCMDatabase.GetStoredProcCommand
                ("SPUPDATE_LEGAL_ACCEPTED_STATUS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateStatusCommand,
                "@USER_ID", DbType.Int32, userID);
            HCMDatabase.AddInParameter(updateStatusCommand,
                "@STATUS", DbType.String, accepted ? "Y" : "N");

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(updateStatusCommand);
        }

        /// <summary>
        /// Method that checks and activates the user account along with legal
        /// acceptance.
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="string"/> that holds the confirmation code.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the user first name + last name.
        /// </returns>
        public string CheckForActivationCodeWithLegalAcceptance
            (string userName, string confirmationCode)
        {
            DbCommand checkCommand = null;

            // Create command.
            checkCommand = HCMDatabase.GetStoredProcCommand
                ("SPCHECK_ACTIVATE_USER_WITH_LEGAL_ACCEPTANCE");

            // Add input parameters.
            HCMDatabase.AddInParameter(checkCommand,
                "@USER_NAME", DbType.String, userName);
            HCMDatabase.AddInParameter(checkCommand,
                "@CONFIRMATION_CODE", DbType.String, confirmationCode);

            // Execute the query and return the first name + last name.
            return HCMDatabase.ExecuteScalar(checkCommand).ToString();
        }

        /// <summary>
        /// Method that gets the role id against its role code
        /// </summary>
        /// <param name="roleCode">
        /// A <see cref="string"/> that holds the role code.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds role id
        /// </returns>
        public int GetRoleIDByRoleCode(string roleCode)
        {
            DbCommand getRoleIDCommand = null;

            // Create command.
            getRoleIDCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_ROLE_ID_BY_ROLE_CODE");

            // Add input parameters.
            HCMDatabase.AddInParameter(getRoleIDCommand,
                "@ROLE_CODE", DbType.String, roleCode);

            // Execute the query and get the role ID
            return Convert.ToInt32(HCMDatabase.ExecuteScalar(getRoleIDCommand).ToString());
        }
    }
}
