﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewDLManager.cs
// File that represents the data layer for the Test respository Manager.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives


namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Question respository management.
    /// This includes functionalities for retrieving
    /// values for Question. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class InterviewDLManager : DatabaseConnectionManager
    {
        public void InsertInterviewTest(TestDetail testDetail, int userID, string complexity, IDbTransaction transaction)
        {
            /* if ((bool)testDetail.IsCertification)
                 if ((Utility.IsNullOrEmpty(testDetail.CertificationDetail)) ||
                         (testDetail.CertificationDetail.CertificateID <= 0))
                     throw new Exception("Certification details not included for certification test");*/
            // Create a stored procedure command object.
            DbCommand insertTestCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_INTERVIEW_TEST");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, testDetail.TestKey);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@INTERVIEW_TEST_NAME", DbType.String, testDetail.Name);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@INTERVIEW_DESCRIPTION", DbType.String, testDetail.Description);
            //HCMDatabase.AddInParameter(insertTestCommand,
            //    "@RECOMMENDED_TIME", DbType.Int32, testDetail.RecommendedCompletionTime);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TOTAL_QUESTION", DbType.Int16, testDetail.NoOfQuestions);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@CERTIFICATION", DbType.String, (testDetail.IsCertification) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_AUTHOR", DbType.Int32, testDetail.TestAuthorID);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_COST", DbType.Decimal, testDetail.TestCost);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@DELETED", DbType.String, (testDetail.IsDeleted) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@ACTIVE", DbType.String, (testDetail.IsActive) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_MODE", DbType.String, testDetail.TestMode);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@USER_ID", DbType.Int32, userID);
            if (testDetail.PositionProfileID == 0)
            {
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, testDetail.PositionProfileID);
            }
            HCMDatabase.AddInParameter(insertTestCommand,
               "@COMPLEXITY", DbType.String, complexity);
            /*    if ((bool)testDetail.IsCertification)
                {
                    HCMDatabase.AddInParameter(insertTestCommand, "@QUALIFICATION_SCORE", DbType.Decimal,
                        testDetail.CertificationDetail.MinimumTotalScoreRequired);
                    HCMDatabase.AddInParameter(insertTestCommand, "@MAXIMUM_TIME_PERM", DbType.Int32,
                        testDetail.CertificationDetail.MaximumTimePermissible);
                    HCMDatabase.AddInParameter(insertTestCommand, "@CERTIFICATION_VALID_ID", DbType.String,
                        testDetail.CertificationDetail.CertificateValidity);
                    HCMDatabase.AddInParameter(insertTestCommand, "@NO_OF_RETAKES", DbType.Int16,
                        testDetail.CertificationDetail.PermissibleRetakes);
                    HCMDatabase.AddInParameter(insertTestCommand, "@CERTIFICATE_FORMAT_ID", DbType.Int32,
                        testDetail.CertificationDetail.CertificateID);
                    HCMDatabase.AddInParameter(insertTestCommand, "@TIME_FOR_NO_OF_RETAKES", DbType.Int32,
                        testDetail.CertificationDetail.DaysElapseBetweenRetakes);
                }*/
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method communicates with DB and gets the test session records 
        /// from the DB satisfying the user search criteria
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A list of <see cref="TestSearchCriteria"/> that holds the TestKey,Category,Subject,etc..
        /// </param>
        /// /// <param name="PageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="PageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="OrderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="TotalNoofRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>List of interview test details</returns>
        public List<TestDetail> GetInterviewsForInterviewSession(TestSearchCriteria testSearchCriteria,
            int PageNumber, int PageSize, string OrderBy, SortType direction, out int TotalNoofRecords)
        {
            IDataReader dataReader = null;
            try
            {
                TotalNoofRecords = 0;
                DbCommand getTestforTestSessionCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEWS_BY_INTERVIEW");
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@INTERVIEW_TEST_KEY", DbType.String, testSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@INTERVIEW_TEST_AUTHOR", DbType.String, testSearchCriteria.TestAuthorName);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@INTERVIEW_TEST_AUTHOR_ID", DbType.String, testSearchCriteria.TestAuthorID);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@CAT_SUB", DbType.String, testSearchCriteria.CategoriesID);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@CATEGORY", DbType.String, testSearchCriteria.Category);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@SUBJECT", DbType.String, testSearchCriteria.Subject);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@INTERVIEW_TEST_AREA", DbType.String, testSearchCriteria.TestAreasID);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@INTERVIEW_TEST_NAME", DbType.String, testSearchCriteria.Name);

                if (testSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@SHOW_COPIED_INTERVIEW_TESTS", DbType.String, testSearchCriteria.ShowCopiedTests == true ? "Y" : "N");

                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@INTERVIEW_TEST_COST_START", DbType.Int32, testSearchCriteria.TestCostStart);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@INTERVIEW_TEST_COST_END", DbType.Int32, (testSearchCriteria.TestCostEnd == 0) ? 100 : testSearchCriteria.TestCostEnd);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@QUESTION_COUNT_START ", DbType.Int32, testSearchCriteria.TotalQuestionStart);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@QUESTION_COUNT_END", DbType.Int32, (testSearchCriteria.TotalQuestionEnd == 0) ? 100 : testSearchCriteria.TotalQuestionEnd);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@CERTIFICATION ", DbType.String,
                    Utility.IsNullOrEmpty(testSearchCriteria.IsCertification) ? null : Convert.ToBoolean(testSearchCriteria.IsCertification) ? "Y" : "N");
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@PAGENUM", DbType.Int32, PageNumber);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@PAGESIZE", DbType.Int32, PageSize);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@ORDERBY", DbType.String, OrderBy);
                HCMDatabase.AddInParameter(getTestforTestSessionCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                dataReader = HCMDatabase.ExecuteReader(getTestforTestSessionCommand);
                List<TestDetail> testDetails = null;
                TestDetail testDetail = null;
                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        TotalNoofRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    else
                    {
                        if (testDetail == null)
                            testDetail = new TestDetail();
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                            testDetail.TestKey = dataReader["INTERVIEW_TEST_KEY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                            testDetail.Name = dataReader["INTERVIEW_TEST_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_AUTHOR"]))
                            testDetail.TestAuthorID = Convert.ToInt32(dataReader["INTERVIEW_TEST_AUTHOR"]);
                        if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_AUTHOR_NAME"]))
                            testDetail.TestAuthorName = dataReader["INTERVIEW_TEST_AUTHOR_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                            testDetail.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(dataReader["NO_OF_QUESTIONS"]))
                            testDetail.NoOfQuestions = Convert.ToInt32(dataReader["NO_OF_QUESTIONS"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                            testDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_COST"]))
                            testDetail.TestCost = Convert.ToDecimal(dataReader["TEST_COST"]);
                        if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION"]))
                            testDetail.IsCertification = dataReader["CERTIFICATION"].ToString() == "N" ? false : true;
                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]))
                            testDetail.TestStatus = dataReader["ACTIVE"].ToString() == "Y" ? TestStatus.Active : TestStatus.Inactive;
                        testDetails.Add(testDetail);
                        testDetail = null;
                    }
                }
                return testDetails;
            }
            finally
            {
                if (!((Utility.IsNullOrEmpty(dataReader)) && (dataReader.IsClosed))) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
            }
        }

        /// <summary>
        /// Method that updates the copy interview test status. If questions are same for
        /// both parent and child test, then status and parent test key needs 
        /// to be updated.
        /// </summary>
        /// <param name="parentTestKey">
        /// A <see cref="string"/> that holds the parent interview test key.
        /// </param>
        /// <param name="childTestKey">
        /// A <see cref="string"/> that holds the child interview test key.
        /// </param>
        public void UpdateCopyInterviewTestStatus(string parentTestKey, string childTestKey)
        {
            // Create a stored procedure command object.
            DbCommand updateCopyTestStatusCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_COPY_INTERVIEW_TEST_STATUS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCopyTestStatusCommand,
                "@PARENT_TEST_KEY", DbType.String, parentTestKey);
            HCMDatabase.AddInParameter(updateCopyTestStatusCommand,
                "@CHILD_TEST_KEY", DbType.String, childTestKey);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCopyTestStatusCommand);
        }

        public List<AssessorDetail> GetInterviewTestAssessorDetail(string testSessionID, int userID)
        {
            IDataReader dataReader = null;
            List<AssessorDetail> assessorDetails = null;

            try
            {
                DbCommand getassessorDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_INTERVIEW_TESTSESSION_DETAILS");

                // Add input parameter.
                HCMDatabase.AddInParameter(getassessorDetailsCommand, "@INTERVIEW_SESSION_KEY", DbType.String, testSessionID);
                HCMDatabase.AddInParameter(getassessorDetailsCommand, "@CREATED_BY", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(getassessorDetailsCommand);
                assessorDetails = new List<AssessorDetail>();
                while (dataReader.Read())
                {
                    AssessorDetail searchAssessor = new AssessorDetail();
                    searchAssessor.FirstName = dataReader["FIRST_NAME"].ToString();
                    searchAssessor.LastName = dataReader["LAST_NAME"].ToString();
                    searchAssessor.UserEmail = dataReader["EMAIL"].ToString();
                    searchAssessor.UserID = Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString());
                    assessorDetails.Add(searchAssessor);
                }

                return assessorDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public void InsertInterviewTestQuestions(string testKey, string questionKey, 
            int questionTimeLimit, int questionRelationId,
            int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestQuestionsCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_INTERVIEW_TEST_QUESTION");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, testKey);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@QUESTION_RELATION_ID", DbType.Int32, questionRelationId);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@QUESTION_TIME_LIMIT", DbType.Int32, questionTimeLimit);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestQuestionsCommand, transaction as DbTransaction);
        }

        public void InsertInterviewTestQuestionsAttributes(string testKey, string questionKey,
            int questionTimeLimit, int questionRatings, int weightage, string questionComments,
            int userID, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestQuestionsCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_INTERVIEW_TEST_QUESTION_ATTRIBUTES");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, testKey);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@QUESTION_KEY", DbType.String, questionKey);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@TIME_LIMIT", DbType.Int32, questionTimeLimit);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@RATINGS", DbType.Int32, questionRatings);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@WEIGHTAGE", DbType.Int32, weightage);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@COMMENTS", DbType.String, questionComments);
            HCMDatabase.AddInParameter(insertTestQuestionsCommand,
                "@USER_ID", DbType.Int32, userID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestQuestionsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that will return the list of candidate test session details.
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="string"/> that contains the sort order either ASC/DESC.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateTestSessionDetail"/> that contains information
        /// about the candidate test session details.
        /// </returns>
        public List<CandidateTestSessionDetail> GetCandidateTestSessions
            (string testID, int pageNumber, int pageSize, string orderBy,
            SortType sortDirection, int userID, out int totalRecords)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateDetailCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_TEST_SESSION_BY_TEST_KEY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@TEST_KEY", DbType.String, testID);
                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                  "@TEST_SESSION_AUTHOR", DbType.String, userID);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getCandidateDetailCommand,
                        "@ORDERBYDIRECTION",
                         DbType.String, sortDirection == SortType.Ascending ?
                         Constants.SortTypeConstants.ASCENDING :
                         Constants.SortTypeConstants.DESCENDING);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateDetailCommand);

                List<CandidateTestSessionDetail> candidateDetailColl = null;

                CandidateTestSessionDetail candSessionDetail = null;
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (candidateDetailColl == null)
                            candidateDetailColl = new List<CandidateTestSessionDetail>();
                        candSessionDetail = new CandidateTestSessionDetail();
                        candSessionDetail.TestID = dataReader["TEST_KEY"].ToString();
                        candSessionDetail.TestSessionID = dataReader["SESSION_KEY"].ToString();
                        candSessionDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());

                        if (dataReader["POSITION_PROFILE_NAME"] != null)
                            candSessionDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["PURCHASE_DATE"]))
                            candSessionDetail.CreatedDate = Convert.ToDateTime(dataReader["PURCHASE_DATE"].ToString());
                        // candSessionDetail.TotalCredit = Convert.ToDecimal(dataReader["CREDIT"].ToString());
                        candSessionDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candSessionDetail.CandidateFullName = dataReader["CANDIDATE_FULLNAME"].ToString().Trim();
                        candSessionDetail.TestSessionAuthor = dataReader["ADMINISTERED_BY"].ToString();
                        candSessionDetail.TestSessionAuthorFullName = dataReader["TEST_AUTHOR_FULLNAME"].ToString().Trim();
                        candSessionDetail.CandidateTestSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candSessionDetail.ScheduledDate = Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                        candSessionDetail.Status = dataReader["SESSION_STATUS"].ToString();
                        // Add the testSession to the collection.

                        candidateDetailColl.Add(candSessionDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return candidateDetailColl;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will cancel the test session for a particular interview candidate.
        /// </summary>
        /// <param name="candidateTestSessionDetail">
        /// A <see cref="CandidateTestSessionDetail"/> that contains the candidate 
        /// interview session information.
        /// </param>
        public void CancelInterviewTestSession(CandidateTestSessionDetail candidateTestSessionDetail)
        {
            // Create a stored procedure command object.
            DbCommand cancelTestSession = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CANDIDATE_INTERVIEW_SESSION_STATUS");

            // Add input parameters.

            HCMDatabase.AddInParameter(cancelTestSession,
                "@CAND_SESSION_KEY", DbType.String, candidateTestSessionDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(cancelTestSession,
                "@ATTEMPT_ID", DbType.Int16, candidateTestSessionDetail.AttemptID);

            HCMDatabase.AddInParameter(cancelTestSession,
                "@CANCEL_REASON", DbType.String, candidateTestSessionDetail.CancelReason);

            HCMDatabase.AddInParameter(cancelTestSession,
                "@MODIFIED_BY", DbType.Int32, candidateTestSessionDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(cancelTestSession);
        }

        /// <summary>
        /// Method that will load the interview test inclusion details are 
        /// associated with the question. 
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/> that contains the question key.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/> that contains the page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column expression
        /// to be sorted.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that contains the expression to be
        /// re-arranged either asc/desc.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that contains information based on the 
        /// question key.
        /// </returns>
        public List<TestDetail> GetInterviewTestInclusionDetail(string questionKey, int pageNumber,
            int pageSize, string orderBy, SortType orderByDirection, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            List<TestDetail> testDetails = null;

            try
            {
                DbCommand getTestInclusionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_INCLUSION_DETAILS");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@QUESTIONKEY", DbType.String, questionKey.ToString());

                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getTestInclusionDetailCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getTestInclusionDetailCommand);

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();

                    TestDetail testDetail = new TestDetail();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                            testDetail.Name = dataReader["TEST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                            testDetail.TestKey = dataReader["TEST_KEY"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                            testDetail.TestCreationDate =
                                Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                            testDetail.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString());

                        testDetail.TestAuthorName =
                            new CommonDLManager().GetUserDetail(testDetail.CreatedBy).FirstName;

                        if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                            testDetail.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_ADMINSTRED_COUNT"]))
                            testDetail.NoOfCandidatesAdministered =
                                int.Parse(dataReader["TEST_ADMINSTRED_COUNT"].ToString());

                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets test name 
        /// </summary>
        /// <param name="testID">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="TestDetail"/> that contains the Test Detail.
        /// </returns>
        public TestDetail GetInterviewTestDetail(string testID)
        {
            IDataReader dataReader = null;
            TestDetail testDetail = null;

            try
            {
                // Create command object
                DbCommand getTestNameCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_INTERVIEW_TEST_DETAILS_BYKEY");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestNameCommand, "@INTERVIEW_TEST_KEY", DbType.String, testID);

                dataReader = HCMDatabase.ExecuteReader(getTestNameCommand);

                if (dataReader.Read())
                {
                    testDetail = new TestDetail();
                    testDetail.Name = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    testDetail.Description = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();
                    testDetail.TestKey = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION"]))
                        testDetail.IsCertification =
                            dataReader["CERTIFICATION"].ToString().Trim() == "Y" ? true : false;

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                    {
                        testDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        testDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    }
                }
                return testDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of Interview Tests for the given search 
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A list of <see cref="TestSearchCriteria"/> that holds the TestKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestDetail"/> that holds the TestDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging and Sorting data.
        /// </remarks>
        public List<TestDetail> GetInterviewTests(TestSearchCriteria testSearchCriteria, int pageSize, int pageNumber, string orderBy, SortType direction, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;
            try
            {
                string isCertification = null;
                if (testSearchCriteria.IsCertification != null)
                    isCertification = testSearchCriteria.IsCertification == true ? "Y" : "N";

                DbCommand getTestDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TESTS");
                //HCMDatabase.AddInParameter(getTestDetailCommand, "@TENANT_ID", DbType.String, testSearchCriteria.TenantID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@INTERVIEW_TEST_KEY", DbType.String, testSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AUTHOR", DbType.String, testSearchCriteria.TestAuthorName);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AUTHOR_ID", DbType.Int32, testSearchCriteria.TestAuthorID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CAT_SUB", DbType.String, testSearchCriteria.CategoriesID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CATEGORY", DbType.String, testSearchCriteria.Category);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@SUBJECT", DbType.String, testSearchCriteria.Subject);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_AREA", DbType.String, testSearchCriteria.TestAreasID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_NAME", DbType.String, testSearchCriteria.Name);

                if (testSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.String, null);
                else
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.String, testSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestDetailCommand, "@SHOW_COPIED_TESTS", DbType.String, testSearchCriteria.ShowCopiedTests == true ? "Y" : "N");
                HCMDatabase.AddInParameter(getTestDetailCommand, "@QUESTION_COUNT_START ", DbType.Int32, testSearchCriteria.TotalQuestionStart);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@QUESTION_COUNT_END", DbType.Int32, (testSearchCriteria.TotalQuestionEnd == 0) ? 100 : testSearchCriteria.TotalQuestionEnd);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CERTIFICATION ", DbType.String, isCertification);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);


                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                List<TestDetail> testDetails = null;
                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();


                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        TestDetail testDetail = new TestDetail();
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString();
                        testDetail.Name = dataReader["TEST_NAME"].ToString();
                        testDetail.TestAuthorName = dataReader["USRNAME"].ToString();
                        testDetail.NoOfQuestions = int.Parse(dataReader["TOTAL_QUESTION"].ToString());
                        testDetail.CreatedDate = DateTime.Parse(dataReader["CREATED_DATE"].ToString());
                        testDetail.TestCost = 0;
                        testDetail.IsCertification = (Convert.ToChar(dataReader["CERTIFICATION"]) == 'Y') ? true : false;
                        testDetail.IsActive = (Convert.ToChar(dataReader["ACTIVE_FLAG"]) == 'Y') ? true : false;
                        testDetail.SessionIncluded = (dataReader["SESSION_INCLUDED"].ToString() == "0") ? true : false;
                        testDetail.SessionIncludedActive = (int.Parse(dataReader["SESSION_INCLUDED_ACTIVE"].ToString()) == 0) ? true : false;
                        testDetail.TestAuthorID = int.Parse(dataReader["USRID"].ToString());
                        testDetail.TestAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        testDetail.DataAccessRights = dataReader["DATA_ACCESS_RIGHTS"].ToString();
                        testDetail.TenantID = int.Parse(dataReader["TENANT_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_REQUEST_NUMBER"]))
                            testDetail.PositionProfileID = int.Parse(dataReader["CLIENT_REQUEST_NUMBER"].ToString());

                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                dataReader.Close();
                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<QuestionDetail> GetInterviewTestQuestionDetail(string testKey, string sortExpression)
        {
            IDataReader dataReader = null;
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            try
            {
                // Create command object
                DbCommand getTestQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_QUESTION_DETAILS_BY_TESTKEY");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@ORDERBY", DbType.String, sortExpression);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getTestQuestionDetailCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        QuestionDetail questionDetail = new QuestionDetail();

                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString().Trim();
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                        questionDetail.Complexity = dataReader["COMPLEXITY"].ToString().Trim();

                        questionDetail.TestAreaName = dataReader["TESTAREANAME"].ToString().Trim();
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                        questionDetail.Choice_Desc = dataReader["CHOICE_DESC"].ToString().Trim();
                        questionDetail.QuestionRelationId = int.Parse(dataReader["RELATION_ID"].ToString().Trim());
                        questionDetail.ComplexityValue = dataReader["COMPLEXITY_VALUE"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["COMPLEXITY_VALUE"].ToString());
                        questionDetail.AverageTimeTaken = dataReader["AVERAGE_TIME"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["AVERAGE_TIME"].ToString());
                        if (Convert.ToBoolean(dataReader["HAS_IMAGE"]))
                            questionDetail.HasImage = true;
                        else
                            questionDetail.HasImage = false;
                        if (!Utility.IsNullOrEmpty(dataReader["ILLUSTRATION"]))
                            questionDetail.QuestionImage = dataReader["ILLUSTRATION"] as byte[];

                        if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]) && dataReader["WEIGHTAGE"] != DBNull.Value)
                        {
                            questionDetail.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]) && dataReader["COMMENTS"] != DBNull.Value)
                        {
                            questionDetail.Comments = dataReader["COMMENTS"].ToString().Trim();
                        }
                        questionDetails.Add(questionDetail);
                    }
                    dataReader.Close();
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of Questions for the given search 
        /// </summary>
        /// <param name="searchCriteria">
        /// A list of <see cref="QuestionDetailSearchCriteria"/> that holds the questionKey,Category,Subject,etc..
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="QuestionDetail"/> that holds the QuestionDetail regarding this Search Criteria.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<QuestionDetail> GetInterviewSearchQuestions(QuestionDetailSearchCriteria searchCriteria, int pageSize, int pageNumber, string orderBy, string direction, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                List<QuestionDetail> QuestionDetail = new List<QuestionDetail>();

                DbCommand getQuestionDeatilsCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_QUESTIONS");
                string creditEarned = null;
                if (searchCriteria.CreditsEarned > 0)
                {
                    creditEarned = searchCriteria.CreditsEarned.ToString();
                }

                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@QUESTION_KEY", DbType.String, searchCriteria.QuestionKey);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@AUTHOR", DbType.String, searchCriteria.AuthorName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@AUTHOR_ID", DbType.Int32, searchCriteria.Author);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CAT_SUB", DbType.String, searchCriteria.CategorySubjectsKey);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CATEGORY", DbType.String, searchCriteria.CategoryName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@SUBJECT", DbType.String, searchCriteria.SubjectName);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@KEYWORD", DbType.String, searchCriteria.Tag);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@TEST_AREA", DbType.String, searchCriteria.TestAreaID);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@COMPLEXITY", DbType.String, searchCriteria.Complexities);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ACTIVE_FLAG", DbType.String, searchCriteria.ActiveFlag);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@ORDERBYDIRECTION", DbType.String, direction);
                HCMDatabase.AddInParameter(getQuestionDeatilsCommand, "@CLIENT_REQUEST", DbType.String, searchCriteria.ClientRequest);
                dataReader = HCMDatabase.ExecuteReader(getQuestionDeatilsCommand);

                while (dataReader.Read())
                {

                    QuestionDetail questionDetails = new QuestionDetail();
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        questionDetails.QuestionID = int.Parse(dataReader["QUESTION_ID"].ToString());
                        questionDetails.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                        questionDetails.Question = dataReader["QUESTION_DESC"].ToString();
                        questionDetails.Complexity = dataReader["COMPLEXITY_NAME"].ToString();
                        questionDetails.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                        questionDetails.ComplexityValue = dataReader["COMPLEXITY_VALUE"] == DBNull.Value ? 0.0m : Convert.ToDecimal(dataReader["COMPLEXITY_VALUE"].ToString());
                        questionDetails.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                        questionDetails.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                        questionDetails.Status = (QuestionStatus)Convert.ToChar((dataReader["ACTIVE_FLAG"]));
                        questionDetails.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                        questionDetails.QuestionRelationId = int.Parse(dataReader["QUESTION_RELATION_ID"].ToString());
                        questionDetails.DataAccessRights = dataReader["DATA_ACCESS_RIGHTS"].ToString();
                        QuestionDetail.Add(questionDetails);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }

                }
                dataReader.Close();
                return QuestionDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method helps to retrieve the question attributes which is involved in a interview test
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/>Contains a interview test key which is incorporated with a question attributes
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/>Contains a sort Expression ase/dec.
        /// </param> 
        /// <returns>
        /// A List for <see cref="QuestionDetail"/>
        /// Returns QuestionDetail object
        /// </returns>
        public List<QuestionDetail> GetInterviewTestQuestionAttributes(string interviewTestKey)
        {
            IDataReader dataReader = null;
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            try
            {
                // Create command object
                DbCommand getTestQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_QUESTION_ATTRIBUTES");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@INTERVIEW_TEST_KEY",
                    DbType.String, interviewTestKey);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getTestQuestionDetailCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        QuestionDetail questionDetail = new QuestionDetail();
                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString().Trim();
                        questionDetail.TimeLimit = Convert.ToInt32(dataReader["TIME_LIMIT"].ToString().Trim());
                        questionDetail.Rating = Convert.ToInt32(dataReader["RATINGS"].ToString().Trim());
                        questionDetail.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString().Trim());
                        questionDetail.Comments = dataReader["COMMENTS"].ToString().Trim();
                        questionDetails.Add(questionDetail);
                    }
                    dataReader.Close();
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method is used to delete  interview test question based on the question keys and testKey.
        /// </summary>
        /// <param name="questionsKeys">
        /// that holds the questionKeys comma sepearted values.
        /// </param>
        /// <param name="interviewTestKey">
        /// that holds the interview testKey.
        /// </param>
        /// <param name="transaction">
        /// Maintaing tranaction from the Test.
        /// </param>
        /// <remarks>
        /// Delete the multiple Test question from the Test based on the QuestionKey and TestKey.
        /// </remarks>
        public void DeleteInterviewTestQuestion(string interviewTestKey, IDbTransaction transaction)
        {
            DbCommand testQuestionDeleteStatusCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_INTERVIEW_TEST_QUESTION");
            HCMDatabase.AddInParameter(testQuestionDeleteStatusCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, interviewTestKey);
            HCMDatabase.ExecuteNonQuery(testQuestionDeleteStatusCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method is used to deleteS interview test question attributes based on the question keys.
        /// </summary>
        /// <param name="interviewTestKey">
        /// that holds the interview test key.
        /// </param>
        /// <param name="transaction">
        /// Maintaing tranaction from the Test.
        /// </param>
        /// <remarks>
        /// Delete the question attributes from the interview test key based on the TestKey.
        /// </remarks>
        public void DeleteInterviewTestQuestionAttributes(string interviewTestKey, IDbTransaction transaction)
        {
            DbCommand testQuestionDeleteStatusCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_INTERVIEW_TEST_QUESTION_ATTRIBUTES");
            HCMDatabase.AddInParameter(testQuestionDeleteStatusCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, interviewTestKey);
            HCMDatabase.ExecuteNonQuery(testQuestionDeleteStatusCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// To get interviewTest Details
        /// </summary>
        /// <param name="testKey"></param>
        /// <returns></returns>
        public TestDetail GetInterviewTestDetailView(string testKey)
        {
            IDataReader dataReader = null;
            TestDetail testDetail = new TestDetail();
            try
            {
                // Create command object
                DbCommand getTestDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_DETAILS");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_KEY", DbType.String, testKey);

                // Get data from the database
                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                if (dataReader != null)
                {

                    while (dataReader.Read())
                    {
                        testDetail.TestKey = testKey.ToString().Trim();

                        testDetail.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_INCLUDED"].ToString().Trim()))
                            testDetail.SessionIncluded =
                                (Convert.ToInt32(dataReader["SESSION_INCLUDED"].ToString()) == 1) ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"].ToString()))
                            testDetail.Name = dataReader["TEST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                            testDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                            testDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"].ToString()))
                            testDetail.Description = dataReader["TEST_DESCRIPTION"].ToString().Trim();


                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"].ToString()))
                            testDetail.IsActive = (Convert.ToChar(dataReader["ACTIVE"]) == 'Y') ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["DELETED"].ToString()))
                            testDetail.IsDeleted = (Convert.ToChar(dataReader["DELETED"]) == 'N') ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_INCLUDED_ACTIVE"].ToString()))
                            testDetail.SessionIncludedActive =
                                (int.Parse(dataReader["SESSION_INCLUDED_ACTIVE"].ToString()) == 0) ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AUTHOR"].ToString()))
                            testDetail.TestAuthorID = int.Parse(dataReader["TEST_AUTHOR"].ToString());
                    }
                    dataReader.Close();
                }

                return testDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// To get the adaptive interview questions.
        /// </summary>
        /// <param name="UserId">
        /// A <see cref="int"/> that holds the UserId 
        /// </param>
        /// <param name="UserSelectedQuestions">
        /// A <see cref="string"/> that holds the user selected questions.
        /// (User moved to Test Draft Panel) 
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size 
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number 
        /// </param>
        /// <param name="TotalNoOfRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// </param>
        /// <returns>
        /// A List for<see cref="QuestionDetail"/> List of interview question details 
        /// </returns>
        public List<QuestionDetail> GetAdaptiveInterviewQuestions(int tenantId, int userId, string userSelectedQuestions,
          int pageSize, int pageNumber, int questionAuthor, out int TotalNoOfRecords)
        {
            DbCommand GetAdaptiveQuestionsCommand = null;
            IDataReader dataReader = null;
            List<QuestionDetail> questionDetails = null;
            QuestionDetail questionDetail = null;
            try
            {
                TotalNoOfRecords = 0;
                GetAdaptiveQuestionsCommand = HCMDatabase.GetStoredProcCommand("SPGET_ADAPTIVE_INTERVIEW_QUESTIONS");
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@TENANT_ID", DbType.Int32, tenantId);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@TEST_AUTHOR", DbType.Int32, userId);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@AUTHOR_ID", DbType.Int32, questionAuthor);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@SELECTED_QUESTION_KEYS", DbType.String, userSelectedQuestions);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(GetAdaptiveQuestionsCommand, "@PAGESIZE", DbType.Int32, pageSize);
                dataReader = HCMDatabase.ExecuteReader(GetAdaptiveQuestionsCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        TotalNoOfRecords = Convert.ToInt32(dataReader["TOTAL"]);
                        continue;
                    }
                    if (Utility.IsNullOrEmpty(questionDetail))
                        questionDetail = new QuestionDetail();
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]))
                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_ID"]))
                        questionDetail.ComplexityName = dataReader["COMPLEXITY_ID"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TAGS"]))
                        questionDetail.Tag = dataReader["TAGS"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"]))
                        questionDetail.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_RELATION_ID"]))
                        questionDetail.QuestionRelationId = Convert.ToInt32(dataReader["QUESTION_RELATION_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_NAME"]))
                        questionDetail.Complexity = dataReader["COMPLEXITY_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER_ID"]))
                        questionDetail.Answer = Convert.ToInt16(dataReader["ANSWER_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER_DESCRIPTION"]))
                        questionDetail.AnswerID = dataReader["ANSWER_DESCRIPTION"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["CREDIT_EARNED"]))
                        questionDetail.CreditsEarned = Convert.ToDecimal(dataReader["CREDIT_EARNED"]);
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_ID"]))
                        questionDetail.TestAreaID = dataReader["TEST_AREA_ID"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_NAME"]))
                        questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString();
                    if (Utility.IsNullOrEmpty(questionDetails))
                        questionDetails = new List<QuestionDetail>();
                    questionDetails.Add(questionDetail);
                    questionDetail = null;
                }
                return questionDetails;
            }
            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(GetAdaptiveQuestionsCommand)) GetAdaptiveQuestionsCommand = null;
                if (!Utility.IsNullOrEmpty(questionDetails)) questionDetails = null;
                if (!Utility.IsNullOrEmpty(questionDetail)) questionDetail = null;
            }
        }

        /// <summary>
        /// This method handles the Edit Interview Test functions
        /// </summary>
        /// <param name="testDetail">
        /// Contains Interview Test detail collection
        /// </param>
        /// <param name="userID">
        /// Creater of the Test.
        /// </param>
        /// <param name="complexity">
        /// Holds the complexity Atribute
        /// </param>
        /// <param name="transaction">
        /// Holds the transaction object
        /// </param>
        public void UpdateInterviewTest(TestDetail testDetail, int userID, string complexity, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertTestCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_INTERVIEW_TEST");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertTestCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, testDetail.TestKey);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@INTERVIEW_TEST_NAME", DbType.String, testDetail.Name);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@INTERVIEW_DESCRIPTION", DbType.String, testDetail.Description);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TOTAL_QUESTION", DbType.Int16, testDetail.NoOfQuestions);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@CERTIFICATION", DbType.String, (testDetail.IsCertification) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_AUTHOR", DbType.Int32, testDetail.TestAuthorID);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_COST", DbType.Decimal, testDetail.TestCost);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@DELETED", DbType.String, (testDetail.IsDeleted) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@ACTIVE", DbType.String, (testDetail.IsActive) == true ? 'Y' : 'N');
            HCMDatabase.AddInParameter(insertTestCommand,
                "@TEST_MODE", DbType.String, testDetail.TestMode);
            HCMDatabase.AddInParameter(insertTestCommand,
                "@MODIFIED_BY", DbType.Int32, testDetail.ModifiedBy);
            HCMDatabase.AddInParameter(insertTestCommand,
               "@COMPLEXITY", DbType.String, complexity);

            if (testDetail.PositionProfileID == 0)
            {
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertTestCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, testDetail.PositionProfileID);
            }

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertTestCommand, transaction as DbTransaction);
        }

        
        /// <summary>
        /// This method is used to Active/inactive question based on the interview test key.
        /// </summary>
        /// <param name="interviewTestKey"></param>
        /// <param name="interviewTestStatus"></param>
        /// <param name="user"></param>
        public void UpdateInterviewStatus(string interviewTestKey, string interviewTestStatus,
            int user)
        {
            DbCommand interviewTestUpdateStatusCommand = HCMDatabase.
                          GetStoredProcCommand("SPDELETE_INTERVIEW_TEST");
            HCMDatabase.AddInParameter(interviewTestUpdateStatusCommand,
                            "@INTERVIEW_TEST_KEY", DbType.String, interviewTestKey);
            HCMDatabase.AddInParameter(interviewTestUpdateStatusCommand,
                            "@ACTIVE_FLAG", DbType.String, interviewTestStatus);
            HCMDatabase.AddInParameter(interviewTestUpdateStatusCommand,
                            "@DELETED", DbType.String, null);
            HCMDatabase.AddInParameter(interviewTestUpdateStatusCommand,
                           "@USERID", DbType.Int32, user);
            HCMDatabase.ExecuteNonQuery(interviewTestUpdateStatusCommand);
        }

        /// <summary>
        /// This method is used to Delete Interview Test based on the interview test key.
        /// </summary>
        /// <param name="testKey">
        ///  A <see cref="string"/> that holds the interview test key.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the Logedin auther Id.
        /// </param>
        public void DeleteInterviewTest(string interviewTestKey, int user)
        {
            DbCommand interviewTestDeletedStatusCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_INTERVIEW_TEST");
            HCMDatabase.AddInParameter(interviewTestDeletedStatusCommand,
                "@INTERVIEW_TEST_KEY", DbType.String, interviewTestKey);
            HCMDatabase.AddInParameter(interviewTestDeletedStatusCommand,
                "@ACTIVE_FLAG", DbType.String, null);
            HCMDatabase.AddInParameter(interviewTestDeletedStatusCommand,
                "@DELETED", DbType.String, "Y");
            HCMDatabase.AddInParameter(interviewTestDeletedStatusCommand,
                "@USERID", DbType.Int32, user);
            HCMDatabase.ExecuteNonQuery(interviewTestDeletedStatusCommand);
        }

        public InterviewDetail GetViewInterviewTestDetail(string testKey)
        {
            IDataReader dataReader = null;
            InterviewDetail testDetail = new InterviewDetail();

            try
            {
                // Create command object
                DbCommand getTestDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_DETAILS");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestDetailCommand, "@TEST_KEY", DbType.String, testKey);

                // Get data from the database
                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                if (dataReader != null)
                {

                    while (dataReader.Read())
                    {
                        testDetail.InterviewTestKey = testKey.ToString().Trim();

                        testDetail.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_INCLUDED"].ToString().Trim()))
                            testDetail.SessionIncluded =
                                (Convert.ToInt32(dataReader["SESSION_INCLUDED"].ToString()) == 1) ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"].ToString()))
                            testDetail.InterviewName = dataReader["TEST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                            testDetail.PositionProfileId = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                            testDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_DESCRIPTION"].ToString()))
                            testDetail.InterviewDescription = dataReader["TEST_DESCRIPTION"].ToString().Trim();


                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"].ToString()))
                            testDetail.IsActive = (Convert.ToChar(dataReader["ACTIVE"]) == 'Y') ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["DELETED"].ToString()))
                            testDetail.IsDeleted = (Convert.ToChar(dataReader["DELETED"]) == 'N') ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_INCLUDED_ACTIVE"].ToString()))
                            testDetail.SessionIncludedActive =
                                (int.Parse(dataReader["SESSION_INCLUDED_ACTIVE"].ToString()) == 0) ? true : false;

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_AUTHOR"].ToString()))
                            testDetail.TestAuthorID = int.Parse(dataReader["TEST_AUTHOR"].ToString());
                    }
                    dataReader.Close();
                }

                return testDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// returns subject/category id
        /// </summary>
        /// <param name="skill">
        /// <see cref="string"/> holds the assessor skill
        /// </param>
        /// <returns></returns>
        public DataTable getSubjectCategoryID(string skill)
        {

            DataSet ds_subject = new DataSet();
            DbCommand skillCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_SUBJECTID");

            HCMDatabase.AddInParameter(skillCommand,
                "@SKILL", DbType.String, skill);

            // Execute the stored procedure.
            HCMDatabase.LoadDataSet(skillCommand,
                ds_subject, "Subject");
            return ds_subject.Tables[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="AssessorSkill_Table"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int InsertAssosserSKill(int UserID, DataTable
            AssessorSkill_Table, IDbTransaction transaction) 
        {
            try{

            DbCommand deleteskillCommand =
                HCMDatabase.GetStoredProcCommand("SPDELETE_ASSESSOR_SKILLS");

            HCMDatabase.AddInParameter(deleteskillCommand,
                "@USERID", DbType.Int32, UserID);

             HCMDatabase.ExecuteNonQuery(deleteskillCommand, transaction as DbTransaction);

             foreach (DataRow dr in AssessorSkill_Table.Rows)
             {
                 DbCommand insertAssessorSkill =
                     HCMDatabase.GetStoredProcCommand("SPINSERT_ASSESSOR_SKILL");

                 HCMDatabase.AddInParameter(insertAssessorSkill,
                     "@USERID", DbType.Int32, UserID);

                 HCMDatabase.AddInParameter(insertAssessorSkill,
                     "@SKILLID", DbType.Int32, Convert.ToInt32(dr["SkillID"]));

                 HCMDatabase.ExecuteNonQuery(insertAssessorSkill, transaction as DbTransaction);
             } 
                 return 1;
            }
            catch
            {
                return 0;
            } 
        }

        public DataTable getAssessorSkills(int userID)
        { 
            //
            DataSet ds_subject = new DataSet();
            DbCommand getAssessorSkillCommand = HCMDatabase.GetStoredProcCommand
                ("GETASSESSORSKILL");

            HCMDatabase.AddInParameter(getAssessorSkillCommand,
                "@USERID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.LoadDataSet(getAssessorSkillCommand,
                ds_subject, "AssesssorSkills");
            return ds_subject.Tables[0];
        }


        /// <summary>
        /// Method that retrieves assoiciated position profile details for the 
        /// given interview test key.
        /// </summary>
        /// <param name="interviewTestKey">
        /// A <see cref="string"/> that holds the interview test key.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileDetail"/> that holds the position 
        /// profile and interview detail.
        /// </returns>
        public PositionProfileDetail GetPositionProfileInterviewDetail(string interviewTestKey)
        {
            IDataReader dataReader = null;
            PositionProfileDetail positionProfileDetail = null;

            try
            {
                DbCommand getPositionProfileDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_INTERVIEW");

                // Add input parameter.
                HCMDatabase.AddInParameter(getPositionProfileDetailCommand, "@INTERVIEW_TEST_KEY", DbType.String, interviewTestKey);

                // Execute the reader.
                dataReader = HCMDatabase.ExecuteReader(getPositionProfileDetailCommand);

                if (dataReader.Read())
                {
                    positionProfileDetail = new PositionProfileDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        positionProfileDetail.InterviewName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DESCRIPTION"]))
                        positionProfileDetail.InterviewDescription = dataReader["INTERVIEW_DESCRIPTION"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        positionProfileDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_NAME"]))
                        positionProfileDetail.PositionProfileOwnerName = dataReader["POSITION_PROFILE_OWNER_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_EMAIL"]))
                        positionProfileDetail.PositionProfileOwnerEmail = dataReader["POSITION_PROFILE_OWNER_EMAIL"].ToString().Trim();
                }

                return positionProfileDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
    }
}
