﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.DL
{
    public class ResumeRepositoryDLManager : DatabaseConnectionManager
    {
        /// <summary>
        /// Method to update resume status for the given candidate id
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate candidate id
        /// </param>                 
        public void ApproveResume(int candidateID)
        {
            DbCommand updateCandidateResume = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_APPROVE_RESUME");
            // Add input parameters.
            HCMDatabase.AddInParameter(updateCandidateResume,
               "@CANDIDATE_ID", DbType.Int32, candidateID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCandidateResume);
        }

        /// <summary>
        /// Method to get the candidate resume for the given candidate id. 
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds the candidate resume id 
        /// </param>        
        /// <returns>
        /// A<see cref="List<string>"/>that holds the resume id and resume source.
        /// </returns>
        public List<string> GetCandidateHRXml(int candidateID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getCandidateXMLCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_HRXML_BYCANDIDATEID");
                // Add parameters.
                HCMDatabase.AddInParameter(getCandidateXMLCommand,
                    "@CANDIDATE_ID", DbType.Int16, candidateID);
                dataReader = HCMDatabase.ExecuteReader(getCandidateXMLCommand);
                List<string> oList = null;
                while (dataReader.Read())
                {
                    oList = new List<string>();
                    if (dataReader["CAND_RESUME_ID"] != null)
                    {
                        oList.Add(dataReader["CAND_RESUME_ID"].ToString());
                    }
                    if (dataReader["HR_XML"] != null)
                    {
                        oList.Add(dataReader["HR_XML"].ToString());
                    }
                    if (dataReader["RESUME_SOURCE"] != null)
                    {
                        oList.Add(dataReader["RESUME_SOURCE"].ToString());
                    }
                    if (dataReader["APPROVAL_STATUS"] != null)
                    {
                        oList.Add(dataReader["APPROVAL_STATUS"].ToString());
                    }
                }
                return oList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<string> GetTemporaryResumeHRXML(int candidateID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getCandidateXMLCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEMPORARY_RESUME_HRXML");
                // Add parameters.
                HCMDatabase.AddInParameter(getCandidateXMLCommand,
                    "@CANDIDATE_ID", DbType.Int16, candidateID);
                dataReader = HCMDatabase.ExecuteReader(getCandidateXMLCommand);
                List<string> oList = null;
                while (dataReader.Read())
                {
                    oList = new List<string>();
                    if (dataReader["CAND_RESUME_ID"] != null)
                    {
                        oList.Add(dataReader["CAND_RESUME_ID"].ToString());
                    }
                    if (dataReader["HR_XML"] != null)
                    {
                        oList.Add(dataReader["HR_XML"].ToString());
                    }
                    if (dataReader["RESUME_SOURCE"] != null)
                    {
                        oList.Add(dataReader["RESUME_SOURCE"].ToString());
                    }
                    if (dataReader["APPROVAL_STATUS"] != null)
                    {
                        oList.Add(dataReader["APPROVAL_STATUS"].ToString());
                    }
                }
                return oList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int UpdateTemporaryResumeStatus(int candidateID, string status)
        {
            DbCommand UpdateTemporaryResumeStatusCommand = null;

            try
            {
                UpdateTemporaryResumeStatusCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_TEMPORARY_RESUME");

                HCMDatabase.AddInParameter(UpdateTemporaryResumeStatusCommand, "@CANDIDATE_ID", DbType.Int32, candidateID);

                HCMDatabase.AddInParameter(UpdateTemporaryResumeStatusCommand, "@STATUS", DbType.String, status);

                HCMDatabase.ExecuteScalar(UpdateTemporaryResumeStatusCommand);
                return 1;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public int DeleteTemporaryResume(int candidateID)
        {
            DbCommand UpdateTemporaryResumeStatusCommand = null;

            try
            {
                UpdateTemporaryResumeStatusCommand = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_TEMPORARY_RESUME");
                HCMDatabase.AddInParameter(UpdateTemporaryResumeStatusCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateID);
                HCMDatabase.ExecuteScalar(UpdateTemporaryResumeStatusCommand);
                return 1;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Method to set the candidate resume for the given candidate id
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds the candidate resume id 
        /// </param>
        /// <param name="oFileBytes">
        /// A <see cref="byte"/> that holds the resume data.
        /// </param>
        /// <param name="dblScore">
        /// A <see cref="string"/> that holds the score.
        /// </param>
        /// <param name="strMimeType">
        /// A <see cref="byte"/> that holds the mime type.
        /// </param>
        /// <param name="strFileName">
        /// A <see cref="string"/> that holds the filename name.
        /// </param>
        /// <returns>
        /// A<see cref="int"/>that holds the candidate resume id.
        /// </returns>
        public int InsertResumeSourceByBytes(int candidateID, byte[] oFileBytes,
            decimal dblScore, string mimeType, string strFileName, int userId)
        {
            // Declare a data reader object.
            IDataReader dataReader = null;
            DbCommand insertResumeSourceCommand = HCMDatabase.
                                GetStoredProcCommand("SPINSERT_CANDIDATE_RESUME_DETAIL_FILE_OBJECT");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertResumeSourceCommand,
               "@CANDIDATE_ID", DbType.Int32, candidateID);
            HCMDatabase.AddInParameter(insertResumeSourceCommand,
               "@RESUME_SOURCE", DbType.Binary, oFileBytes);
            HCMDatabase.AddInParameter(insertResumeSourceCommand,
               "@SCORE", DbType.Decimal, dblScore);
            HCMDatabase.AddInParameter(insertResumeSourceCommand,
              "@MIME_TYPE", DbType.String, mimeType);
            HCMDatabase.AddInParameter(insertResumeSourceCommand,
              "@FILE_NAME", DbType.String, strFileName);
            HCMDatabase.AddInParameter(insertResumeSourceCommand,
               "@USER_ID", DbType.Int32, userId);
            // Execute the stored procedure.
            dataReader = HCMDatabase.ExecuteReader(insertResumeSourceCommand);
            int iCandidateResumeID = 0;
            while (dataReader.Read())
            {
                if (dataReader["CAND_RESUME_ID"] != null)
                {
                    iCandidateResumeID = Convert.ToInt32(dataReader["CAND_RESUME_ID"].ToString().Trim());
                }
            }
            return iCandidateResumeID;
        }

        /// <summary>
        /// Method to set the candidate resume for the given candidate id
        /// </summary>
        /// <param name="oXmlDoc">
        /// A<see cref="string"/>that holds the resume source in xml.
        /// </param>
        /// <param name="candidateResumeID">
        /// A <see cref="byte"/> that holds the candidate resume id.
        /// </param>                
        public void InsertCandidateHRXml(String oXmlDoc, int candidateResumeID, int userId)
        {
            DbCommand insertCandidateHRXmlCommand = HCMDatabase.
                                GetStoredProcCommand("SPINSERT_CANDIDATE_HRXML");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertCandidateHRXmlCommand,
               "@CAND_RESUME_ID", DbType.Int32, candidateResumeID);
            HCMDatabase.AddInParameter(insertCandidateHRXmlCommand,
               "@HR_XML", DbType.Xml, oXmlDoc);
            HCMDatabase.AddInParameter(insertCandidateHRXmlCommand,
               "@CREATED_BY", DbType.Int32, userId);
            HCMDatabase.AddInParameter(insertCandidateHRXmlCommand,
               "@MODIFIED_BY", DbType.Int32, userId);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertCandidateHRXmlCommand);
        }

        /// <summary>
        /// Represents the method that is used to get the lneage item 
        /// from the database
        /// </summary>
        /// <returns>
        /// A<see cref="List<LineageItem>"/>that holds the list of lineage item
        /// </returns>
        public List<LineageItem> GetLineageItem()
        {
            DbCommand selectLineageItem = HCMDatabase.
                GetStoredProcCommand("SPGET_LINEAGE_ITEM");

            IDataReader dataReader = null;

            try
            {
                dataReader = HCMDatabase.ExecuteReader(selectLineageItem);
                List<LineageItem> lineageList = new List<LineageItem>();
                while (dataReader.Read())
                {
                    LineageItem item = new LineageItem();

                    if (!Utility.IsNullOrEmpty(dataReader["XML_LINEAGE"]))
                    {
                        item.Lineage = dataReader["XML_LINEAGE"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPONENT_NAME"]))
                    {
                        item.ComponentName = dataReader["COMPONENT_NAME"].ToString().Trim();
                    }
                    lineageList.Add(item);
                }
                return lineageList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of candidates for the given search 
        /// criteria.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="emailLike">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the candidate 
        /// details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<CandidateDetail> GetCandidateInformation(int tenantID, string firstNameLike,
            string middleNameLike, string lastNameLike, string emailLike,
            string orderBy, SortType orderByDirection,
            int pageNumber, int pageSize, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;
            IDataReader dataReader = null;
            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_INFORMATION");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@TENANT_ID",
                    DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@EMAIL",
                    DbType.String, emailLike == null || emailLike.Trim().Length == 0 ? null : emailLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);
                List<CandidateDetail> users = null;
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<CandidateDetail>();
                        CandidateDetail candidateDetail = new CandidateDetail();
                        if (dataReader["USER_ID"] != null)
                        {
                            candidateDetail.CandidateID = dataReader["USER_ID"].ToString().Trim();
                        }
                        if (dataReader["FIRST_NAME"] != null)
                        {
                            candidateDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }
                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            candidateDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }
                        if (dataReader["LAST_NAME"] != null)
                        {
                            candidateDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }
                        if (dataReader["EMAIL"] != null)
                        {
                            candidateDetail.EMailID = dataReader["EMAIL"].ToString().Trim();
                        }
                        if (dataReader["IS_APPROVED"] != null)
                        {
                            candidateDetail.ResumeIsApproved = dataReader["IS_APPROVED"].ToString().Trim();
                        }
                        // Add to the list.
                        users.Add(candidateDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the candidate resume for the given candidate resume id
        /// </summary>
        /// <param name="candidateResumeID">
        /// A<see cref="int"/>that holds the candidate resume id 
        /// </param>
        /// <param name="mimeType">
        /// A <see cref="string"/> that holds the mime type as an output 
        /// parameter.
        /// </param>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the filename as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A<see cref="byte"/>that holds the resume data.
        /// </returns>
        public byte[] GetResumeContents(int candidateResumeID,
            out string mimeType, out string fileName)
        {
            mimeType = string.Empty;
            fileName = string.Empty;
            DbCommand command = HCMDatabase.GetStoredProcCommand
                ("SPGET_RESUME_CONTENTS");
            HCMDatabase.AddInParameter(command,
                "@CAND_RESUME_ID", DbType.Int32, candidateResumeID);
            IDataReader reader = HCMDatabase.ExecuteReader(command);
            if (reader != null && reader.Read())
            {
                mimeType = reader["MIME_TYPE"] as string;
                fileName = reader["FILE_NAME"] as string;
                fileName = fileName.Remove(fileName.LastIndexOf("."));
                return reader["RESUME_SOURCE"] as byte[];
            }
            return null;
        }

        /// <summary>
        /// Method that inserts a log entry into the upload resume log table whenever a
        /// resume got uploaded.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that hold the candidate ID.
        /// </param>
        /// <param name="candidateName">
        /// A <see cref="string"/> that holds the candidate name.
        /// </param>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the file name.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertUploadResumeLog(int candidateID, string candidateName,
            string fileName, int userID, string profileName)
        {
            DbCommand insertUploadResumeLog = HCMDatabase.
                GetStoredProcCommand("SPINSERT_UPLOAD_RESUME_LOG");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@CANDIDATE_ID", DbType.Int32, candidateID);
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@CANDIDATE_NAME", DbType.String, candidateName);
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@FILE_NAME", DbType.String, fileName);
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@USER_ID", DbType.Int32, userID);
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@PROFILE_NAME", DbType.String, profileName);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertUploadResumeLog);
        }

        /// <summary>
        /// Method that updates the candidate's recruiter.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateCandidateRecruiter(int candidateID, int recruiterID, int userID)
        {
            DbCommand updateCandidateRecruiterCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CANDIDATE_RECRUITER");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCandidateRecruiterCommand,
                "@CANDIDATE_ID", DbType.Int32, candidateID);
            HCMDatabase.AddInParameter(updateCandidateRecruiterCommand,
                "@RECRUITER_ID", DbType.Int32, recruiterID);
            HCMDatabase.AddInParameter(updateCandidateRecruiterCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCandidateRecruiterCommand);
        }

        /// <summary>
        /// Inserts the new candidate.
        /// </summary>
        /// <param name="candidateInfo">The candidate info.</param>
        public void InsertNewCandidate(CandidateInformation candidateInfo)
        {
            DbCommand insertNewCandidate = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CANDIDATE_INFORMATION");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiFirstName", DbType.String, candidateInfo.caiFirstName);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiMiddleName", DbType.String, candidateInfo.caiMiddleName);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiLastName", DbType.String, candidateInfo.caiLastName);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiGender", DbType.Int32, candidateInfo.caiGender);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiDOB", DbType.DateTime, candidateInfo.caiDOB);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiAddress", DbType.String, candidateInfo.caiAddress);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiHomePhone", DbType.String, candidateInfo.caiHomePhone);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiCell", DbType.String, candidateInfo.caiCell);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiEmail", DbType.String, candidateInfo.caiEmail);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiWorkAuthorization", DbType.String, candidateInfo.caiWorkAuthorization);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiWorkAuthorizationOther", DbType.String, candidateInfo.caiWorkAuthorizationOther);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@caiCreatedBy", DbType.Int32, candidateInfo.caiCreatedBy);
            HCMDatabase.AddInParameter(insertNewCandidate,
                "@CAITYPE", DbType.String, candidateInfo.caiType);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertNewCandidate);
        }

        /// <summary>
        /// Inserts the new candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int InsertNewCandidate(CandidateInformation candidateInformation,
            int userID, int tenantID)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand insertCandidateCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_CANDIDATE_INFORMATION");

                // Add input parameters
                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiFirstName", DbType.String, candidateInformation.caiFirstName);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiMiddleName", DbType.String, candidateInformation.caiMiddleName == null ? null :
                    candidateInformation.caiMiddleName.Trim().Length == 0 ?
                    null : candidateInformation.caiMiddleName.Trim());

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@caiLastName", DbType.String, candidateInformation.caiLastName);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiGender", DbType.Int32, candidateInformation.caiGender == 0 ?
                    null : candidateInformation.caiGender);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiDOB", DbType.DateTime, candidateInformation.caiDOB == DateTime.MinValue ?
                    null : candidateInformation.caiDOB);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiAddress", DbType.String, candidateInformation.caiAddress);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiHomePhone", DbType.String, candidateInformation.caiHomePhone == null ? null : candidateInformation.caiHomePhone.Trim().Length == 0 ?
                    null : candidateInformation.caiHomePhone);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiCell", DbType.String, candidateInformation.caiCell == null ? null : candidateInformation.caiCell.Trim().Length == 0 ?
                    null : candidateInformation.caiCell);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiEmail", DbType.String, candidateInformation.caiEmail);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiWorkAuthorization", DbType.String, candidateInformation.caiWorkAuthorization);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiWorkAuthorizationOther", DbType.String, candidateInformation.caiWorkAuthorizationOther);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiCreatedBy", DbType.Int32, userID);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@CAITYPE", DbType.String, candidateInformation.caiType);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@caiEmployee", DbType.String, candidateInformation.caiEmployee);

                HCMDatabase.AddInParameter(insertCandidateCommand,
               "@caiLinkedInProfile", DbType.String, candidateInformation.LinkedInProfile);

                if (candidateInformation.caiCity == 0)
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiCity", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiCity", DbType.Int32, candidateInformation.caiCity);

                if (candidateInformation.staID == 0)
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiState", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiState", DbType.Int32, candidateInformation.staID);

                if (candidateInformation.couID == 0)
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiCountry", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiCountry", DbType.Int32, candidateInformation.couID);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@caiIsActive", DbType.String, candidateInformation.caiIsActive);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiLimitedAccess", DbType.String, candidateInformation.caiLimitedAccess == null ? null : candidateInformation.caiLimitedAccess == true ? "Y" : "N");

                //SPINSERT_CANDIDATEINFORMATION
                // Execute command
                int candidateID = Convert.ToInt32(HCMDatabase.ExecuteScalar(insertCandidateCommand));

                return candidateID;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Inserts the new signup candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// <returns></returns>
        public int InsertSignupCandidate(CandidateInformation candidateInformation,
            int userID, int tenantID, IDbTransaction transaction)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand insertCandidateCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_CANDIDATE_INFORMATION");

                // Add input parameters
                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiFirstName", DbType.String, candidateInformation.caiFirstName);

                if (candidateInformation.caiMiddleName != null)
                    HCMDatabase.AddInParameter(insertCandidateCommand,
                        "@caiMiddleName", DbType.String,
                        candidateInformation.caiMiddleName.Trim().Length == 0 ?
                        null : candidateInformation.caiMiddleName.Trim());
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand,
                        "@caiMiddleName", DbType.String, null);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@caiLastName", DbType.String, candidateInformation.caiLastName);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiGender", DbType.Int32, candidateInformation.caiGender == 0 ?
                    null : candidateInformation.caiGender);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiDOB", DbType.DateTime, candidateInformation.caiDOB == DateTime.MinValue ?
                    null : candidateInformation.caiDOB);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiAddress", DbType.String, candidateInformation.caiAddress);

                if (candidateInformation.caiHomePhone != null)
                    HCMDatabase.AddInParameter(insertCandidateCommand,
                        "@caiHomePhone", DbType.String, candidateInformation.caiHomePhone.Trim().Length == 0 ?
                        null : candidateInformation.caiHomePhone);
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiHomePhone", DbType.String, null);

                if (candidateInformation.caiCell != null)
                    HCMDatabase.AddInParameter(insertCandidateCommand,
                        "@caiCell", DbType.String, candidateInformation.caiCell.Trim().Length == 0 ?
                        null : candidateInformation.caiCell);
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand,
                        "@caiCell", DbType.String, null);

                //@caiMaritalStatus

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiEmail", DbType.String, candidateInformation.caiEmail);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiWorkAuthorization", DbType.String, candidateInformation.caiWorkAuthorization);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiWorkAuthorizationOther", DbType.String, candidateInformation.caiWorkAuthorizationOther);

                if (userID == 0)
                    HCMDatabase.AddInParameter(insertCandidateCommand,
                        "@caiCreatedBy", DbType.Int32, 1);

                else
                    HCMDatabase.AddInParameter(insertCandidateCommand,
                        "@caiCreatedBy", DbType.Int32, userID);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@CAITYPE", DbType.String, candidateInformation.caiType);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@caiEmployee", DbType.String, candidateInformation.caiEmployee);


                if (!string.IsNullOrEmpty(candidateInformation.caiOpenEmailID))
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiOpenEmailID",
                        DbType.String, candidateInformation.caiOpenEmailID);

                if (!string.IsNullOrEmpty(candidateInformation.caiSelfSignIn))
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiSelfSignIn",
                       DbType.String, candidateInformation.caiSelfSignIn);

                if (candidateInformation.caiCity == 0)
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiCity", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiCity", DbType.Int32, candidateInformation.caiCity);

                if (candidateInformation.staID == 0)
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiState", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiState", DbType.Int32, candidateInformation.staID);

                if (candidateInformation.couID == 0)
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiCountry", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(insertCandidateCommand, "@caiCountry", DbType.Int32, candidateInformation.couID);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@caiIsActive", DbType.String, candidateInformation.caiIsActive);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                   "@caiMaritalStatus", DbType.Int32, null);

                HCMDatabase.AddInParameter(insertCandidateCommand,
                    "@caiLimitedAccess", DbType.String, candidateInformation.caiLimitedAccess == true ? "Y" : "N");

                //SPINSERT_CANDIDATEINFORMATION
                // Execute command
                int candidateID = Convert.ToInt32(HCMDatabase.ExecuteScalar(insertCandidateCommand, transaction as DbTransaction));

                return candidateID;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Updates the candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdateCandidate(CandidateInformation candidateInformation, int userID)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand updateCandidateCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_CANDIDATE_INFORMATION");

                // Add input parameters
                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiFirstName", DbType.String, candidateInformation.caiFirstName);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiMiddleName", DbType.String,
                    candidateInformation.caiMiddleName.Trim().Length == 0 ?
                    null : candidateInformation.caiMiddleName.Trim());

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiLastName", DbType.String, candidateInformation.caiLastName);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiGender", DbType.Int32, candidateInformation.caiGender == 0 ?
                    null : candidateInformation.caiGender);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiDOB", DbType.DateTime, candidateInformation.caiDOB == DateTime.MinValue ?
                    null : candidateInformation.caiDOB);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiAddress", DbType.String, candidateInformation.caiAddress);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiHomePhone", DbType.String, candidateInformation.caiHomePhone.Trim().Length == 0 ?
                    null : candidateInformation.caiHomePhone);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiCell", DbType.String, candidateInformation.caiCell.Trim().Length == 0 ?
                    null : candidateInformation.caiCell);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiEmail", DbType.String, candidateInformation.caiEmail);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiModifiedBy", DbType.Int32, userID);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiID", DbType.Int32, candidateInformation.caiID);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiEmployee", DbType.String, candidateInformation.caiEmployee);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiLimitedAccess", DbType.String, candidateInformation.caiLimitedAccess == true ? "Y" : "N");

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiWorkAuthorization", DbType.String, candidateInformation.caiWorkAuthorization);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiWorkAuthorizationOther", DbType.String, candidateInformation.caiWorkAuthorizationOther);

                if (candidateInformation.caiCity == 0)
                    HCMDatabase.AddInParameter(updateCandidateCommand, "@caiCity", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(updateCandidateCommand, "@caiCity", DbType.Int32, candidateInformation.caiCity);

                if (candidateInformation.staID == 0)
                    HCMDatabase.AddInParameter(updateCandidateCommand, "@caiState", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(updateCandidateCommand, "@caiState", DbType.Int32, candidateInformation.staID);

                if (candidateInformation.couID == 0)
                    HCMDatabase.AddInParameter(updateCandidateCommand, "@caiCountry", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(updateCandidateCommand, "@caiCountry", DbType.Int32, candidateInformation.couID);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                   "@caiIsActive", DbType.String, candidateInformation.caiIsActive);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                "@caiLinkedInProfile", DbType.String, candidateInformation.LinkedInProfile);

                // Execute command
                HCMDatabase.ExecuteNonQuery(updateCandidateCommand);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }


        /// <summary>
        /// Updates the candidate.
        /// </summary>
        /// <param name="candidateInformation">The candidate information.</param>         
        public void UpdateCandidateNames(CandidateInformation candidateInformation, int userID)
        {
            try
            {
                // Create a stored procedure command object
                DbCommand updateCandidateCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_CANDIDATE_NAMES");

                // Add input parameters
                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiFirstName", DbType.String, candidateInformation.caiFirstName);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiLastName", DbType.String, candidateInformation.caiLastName);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiModifiedBy", DbType.Int32, userID);

                HCMDatabase.AddInParameter(updateCandidateCommand,
                    "@caiID", DbType.Int32, candidateInformation.caiID);

                // Execute command
                HCMDatabase.ExecuteNonQuery(updateCandidateCommand);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Gets the candidate details information.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="firstNameLike">The first name like.</param>
        /// <param name="middleNameLike">The middle name like.</param>
        /// <param name="lastNameLike">The last name like.</param>
        /// <param name="emailLike">The email like.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDirection">The order by direction.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <returns></returns>
        public List<CandidateInformation> GetCandidateDetailsInformation(int tenantID,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string emailLike, string orderBy, SortType orderByDirection,
            int pageNumber, int pageSize, string recruiterID, bool searchType, string resumeType,
            string candidateActive, DateTime fromDate, DateTime toDate, int positionId, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;
            IDataReader dataReader = null;
            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_SEARCH_CANDIDATE_DETAILS");

                HCMDatabase.AddInParameter(getUsersCommand,
                  "@TENANT_ID", DbType.Int32, tenantID);

                // Add parameters.                
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@EMAIL",
                    DbType.String, emailLike == null || emailLike.Trim().Length == 0 ? null : emailLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);


                if (positionId > 0)
                {
                    HCMDatabase.AddInParameter(getUsersCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionId);
                }

                if (Convert.ToInt32(recruiterID) > 0)
                {
                    HCMDatabase.AddInParameter(getUsersCommand,
                    "@RECRUITER_ID", DbType.Int32, Convert.ToInt32(recruiterID));
                }
                if (resumeType != null)
                {
                    HCMDatabase.AddInParameter(getUsersCommand,
                                       "@RESUME_TYPE", DbType.String, resumeType);
                }

                if (candidateActive != null)
                    HCMDatabase.AddInParameter(getUsersCommand,
                        "@CANDIDATE_STATUS", DbType.String, candidateActive);

                if (fromDate.ToShortDateString() != "1/1/0001" && fromDate.ToShortDateString() != "01-01-0001")
                    HCMDatabase.AddInParameter(getUsersCommand,
                        "@FROMDATE", DbType.Date, fromDate);

                if (toDate.ToShortDateString() != "1/1/0001" && fromDate.ToShortDateString() != "01-01-0001")
                    HCMDatabase.AddInParameter(getUsersCommand,
                        "@TODATE", DbType.Date, toDate);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGE_NUMBER", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);
                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);
                List<CandidateInformation> candidates = null;
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (candidates == null)
                            candidates = new List<CandidateInformation>();
                        CandidateInformation candidateInformation = new CandidateInformation();
                        if (dataReader["TENANT_ID"] != null)
                        {
                            candidateInformation.tenantID = int.Parse(dataReader["TENANT_ID"].ToString().Trim());
                        }

                        if (dataReader["caiID"] != null)
                        {
                            candidateInformation.caiID = int.Parse(dataReader["caiID"].ToString().Trim());
                        }
                        if (dataReader["caiFirstName"] != null)
                        {
                            candidateInformation.caiFirstName = dataReader["caiFirstName"].ToString().Trim();
                        }
                        if (dataReader["caiMiddleName"] != null)
                        {
                            candidateInformation.caiMiddleName = dataReader["caiMiddleName"].ToString().Trim();
                        }
                        if (dataReader["caiLastName"] != null)
                        {
                            candidateInformation.caiLastName = dataReader["caiLastName"].ToString().Trim();
                        }
                        if (dataReader["caiEmail"] != null)
                        {
                            candidateInformation.caiEmail = dataReader["caiEmail"].ToString().Trim();
                        }
                        if (dataReader["usrUserName"] != null)
                        {
                            candidateInformation.UserName = dataReader["usrUserName"].ToString().Trim();
                        }
                        if (dataReader["RECRUITED_BY"] != null)
                        {
                            candidateInformation.RecruiterID = Convert.ToInt32(dataReader["RECRUITED_BY"].ToString().Trim());
                        }
                        if (dataReader["RECRUITED_BY_NAME"] != null)
                        {
                            candidateInformation.RecruiterName = dataReader["RECRUITED_BY_NAME"].ToString().Trim();
                        }
                        if (dataReader["CAND_RESUME_ID"] != null && dataReader["CAND_RESUME_ID"] != DBNull.Value)
                        {
                            candidateInformation.CandidateResumeID = Convert.ToInt32(dataReader["CAND_RESUME_ID"].ToString().Trim());

                        }
                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS"]))
                        {
                            candidateInformation.CandidateStatus = Convert.ToString(dataReader["POSITION_PROFILE_STATUS"]);
                        }

                        // Add to the list.
                        candidates.Add(candidateInformation);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return candidates;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that search for candidates by keywords
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="keywords">
        /// A <see cref="string"/> that holds the keyword.
        /// </param>
        /// <param name="fromDate">
        /// A <see cref="DateTime"/> that holds the from date.
        /// </param>
        /// <param name="toDate">
        /// A <see cref="DateTime"/> that holds the to date.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds teh order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that holds the sort type.
        /// </param>
        /// <param name="candidateActive">
        /// A <see cref="string"/> that holds the candidate active status.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="positionProfielID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateInformation"/> that holds the candidate details.
        /// </returns>
        public List<CandidateInformation> GetCandidateDetailsInformation(int tenantID, int userID,
           string keywords, DateTime fromDate, DateTime toDate, string orderBy,
           SortType orderByDirection, string candidateActive,
           int pageNumber, int pageSize, int positionProfielID, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidatesCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_DETAILS_BY_KEYWORDS");

                HCMDatabase.AddInParameter(getCandidatesCommand, "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getCandidatesCommand, "@USER_ID", DbType.Int32, userID);

                // Add parameters.                
                HCMDatabase.AddInParameter(getCandidatesCommand, "@KEYWORDS", DbType.String, keywords);

                if (fromDate.ToShortDateString() != "1/1/0001")
                    HCMDatabase.AddInParameter(getCandidatesCommand, "@RESUME_FROMDATE", DbType.Date, fromDate);

                if (toDate.ToShortDateString() != "1/1/0001")
                    HCMDatabase.AddInParameter(getCandidatesCommand, "@RESUME_TODATE", DbType.Date, toDate);

                HCMDatabase.AddInParameter(getCandidatesCommand, "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(getCandidatesCommand, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                if (positionProfielID > 0)
                {
                    HCMDatabase.AddInParameter(getCandidatesCommand,
                        "@POSITION_PROFILE_ID", DbType.Int32, positionProfielID);
                }

                if (candidateActive != null)
                    HCMDatabase.AddInParameter(getCandidatesCommand,
                        "@CANDIDATE_STATUS", DbType.String, candidateActive);

                HCMDatabase.AddInParameter(getCandidatesCommand,
                    "@PAGE_NUMBER", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getCandidatesCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getCandidatesCommand);
                List<CandidateInformation> candidates = null;
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (candidates == null)
                            candidates = new List<CandidateInformation>();
                        CandidateInformation candidateInformation = new CandidateInformation();
                        if (dataReader["TENANT_ID"] != null)
                        {
                            candidateInformation.tenantID = int.Parse(dataReader["TENANT_ID"].ToString().Trim());
                        }

                        if (dataReader["caiID"] != null)
                        {
                            candidateInformation.caiID = int.Parse(dataReader["caiID"].ToString().Trim());
                        }
                        if (dataReader["caiFirstName"] != null)
                        {
                            candidateInformation.caiFirstName = dataReader["caiFirstName"].ToString().Trim();
                        }
                        if (dataReader["caiMiddleName"] != null)
                        {
                            candidateInformation.caiMiddleName = dataReader["caiMiddleName"].ToString().Trim();
                        }
                        if (dataReader["caiLastName"] != null)
                        {
                            candidateInformation.caiLastName = dataReader["caiLastName"].ToString().Trim();
                        }
                        if (dataReader["caiEmail"] != null)
                        {
                            candidateInformation.caiEmail = dataReader["caiEmail"].ToString().Trim();
                        }
                        if (dataReader["usrUserName"] != null)
                        {
                            candidateInformation.UserName = dataReader["usrUserName"].ToString().Trim();
                        }
                        if (dataReader["RECRUITED_BY"] != null)
                        {
                            candidateInformation.RecruiterID = Convert.ToInt32(dataReader["RECRUITED_BY"].ToString().Trim());
                        }
                        if (dataReader["RECRUITED_BY_NAME"] != null)
                        {
                            candidateInformation.RecruiterName = dataReader["RECRUITED_BY_NAME"].ToString().Trim();
                        }
                        if (dataReader["CAND_RESUME_ID"] != null && dataReader["CAND_RESUME_ID"] != DBNull.Value)
                        {
                            candidateInformation.CandidateResumeID = Convert.ToInt32(dataReader["CAND_RESUME_ID"].ToString().Trim());

                        }
                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_STATUS"]))
                        {
                            candidateInformation.CandidateStatus = Convert.ToString(dataReader["POSITION_PROFILE_STATUS"]);
                        }

                        // Add to the list.
                        candidates.Add(candidateInformation);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return candidates;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Inserts the candidate users.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="createdBy">The created by.</param>
        /// <param name="confirmationCode">The confirmation code.</param>
        public int InsertCandidateUsers(int tenantID, CandidateInformation candidateInformation,
            int createdBy, string confirmationCode)
        {
            DbCommand InsertUserDbCommand = null;
            try
            {
                InsertUserDbCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_CANDIDATE_USERS");
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TENANT_ID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@USER_NAME", DbType.String, candidateInformation.UserName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@PASSWORD", DbType.String, candidateInformation.Password);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@FIRST_NAME", DbType.String, candidateInformation.caiFirstName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@LAST_NAME", DbType.String, candidateInformation.caiLastName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CONFIRMATION_CODE", DbType.String, confirmationCode);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CANDIDATE_ID", DbType.Int32, candidateInformation.caiID);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@ACTIVATED", DbType.String, "Y");
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CREATED_BY", DbType.Int32, createdBy);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@EMAIL", DbType.String, candidateInformation.caiEmail);

                //HCMDatabase.ExecuteNonQuery(InsertUserDbCommand);

                int usrID = Convert.ToInt32(HCMDatabase.ExecuteScalar(InsertUserDbCommand));


                return usrID;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Inserts the candidate users.
        /// </summary>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="candidateInformation">The candidate information.</param>
        /// <param name="createdBy">The created by.</param>
        /// <param name="confirmationCode">The confirmation code.</param>
        public int InsertSignupUsers(int tenantID, CandidateInformation candidateInformation,
            int createdBy, string confirmationCode, IDbTransaction transaction)
        {
            DbCommand InsertUserDbCommand = null;
            try
            {
                InsertUserDbCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_CANDIDATE_USERS");
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TENANT_ID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@USER_NAME", DbType.String, candidateInformation.UserName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@PASSWORD", DbType.String, candidateInformation.Password);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@FIRST_NAME", DbType.String, candidateInformation.caiFirstName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@LAST_NAME", DbType.String, candidateInformation.caiLastName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CONFIRMATION_CODE", DbType.String, confirmationCode);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CANDIDATE_ID", DbType.Int32, candidateInformation.caiID);
                //Modidified by MKN, candidate should be activate when given url using time, so have change as caiIsActive instead of Y
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@EMAIL", DbType.String, candidateInformation.caiEmail);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@ACTIVATED", DbType.String, candidateInformation.caiIsActive);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CREATED_BY", DbType.Int32, createdBy);

                //HCMDatabase.ExecuteNonQuery(InsertUserDbCommand);
                int usrID = Convert.ToInt32(HCMDatabase.ExecuteScalar(InsertUserDbCommand));

                return usrID;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        /// <summary>
        /// Gets the candidate information.
        /// </summary>
        /// <param name="candidateID">The candidate ID.</param>
        /// <returns></returns>
        public CandidateInformation GetCandidateInformation(int candidateID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_DETAIL");

                HCMDatabase.AddInParameter(getUsersCommand,
                  "@CANDIDATE_ID", DbType.Int32, candidateID);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                CandidateInformation candidateInformation = new CandidateInformation();

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["caiID"]))
                    {
                        candidateInformation.caiID = int.Parse(dataReader["caiID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiFirstName"]))
                    {
                        candidateInformation.caiFirstName = dataReader["caiFirstName"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiMiddleName"]))
                    {
                        candidateInformation.caiMiddleName = dataReader["caiMiddleName"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiLastName"]))
                    {
                        candidateInformation.caiLastName = dataReader["caiLastName"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiEmail"]))
                    {
                        candidateInformation.caiEmail = dataReader["caiEmail"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiGender"]))
                    {
                        candidateInformation.caiGender = int.Parse(dataReader["caiGender"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiDOB"]))
                    {
                        candidateInformation.caiDOB = Convert.ToDateTime(dataReader["caiDOB"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiAddress"]))
                    {
                        candidateInformation.caiAddress = dataReader["caiAddress"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiHomePhone"]))
                    {
                        candidateInformation.caiHomePhone = dataReader["caiHomePhone"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiCell"]))
                    {
                        candidateInformation.caiCell = dataReader["caiCell"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiLimitedAccess"]))
                    {
                        candidateInformation.caiLimitedAccess = false;
                        if (dataReader["caiLimitedAccess"].ToString().Trim().ToUpper() == "Y")
                            candidateInformation.caiLimitedAccess = true;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["caiWorkAuthorization"]))
                    {
                        candidateInformation.caiWorkAuthorization =
                            dataReader["caiWorkAuthorization"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["caiWorkAuthorizationOther"]))
                    {
                        candidateInformation.caiWorkAuthorizationOther
                            = dataReader["caiWorkAuthorizationOther"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CAILOCATION"]))
                    {
                        candidateInformation.caiLocation
                            = dataReader["CAILOCATION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["caiIsActive"]))
                    {
                        candidateInformation.caiIsActive = dataReader["caiIsActive"].ToString().Trim()[0];
                    }
                    else
                    {
                        candidateInformation.caiIsActive = 'Y';
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CTYID"]))
                    {
                        candidateInformation.ctyID
                            = Convert.ToInt32(dataReader["CTYID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STAID"]))
                    {
                        candidateInformation.staID
                            = Convert.ToInt32(dataReader["STAID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COUID"]))
                    {
                        candidateInformation.couID
                            = Convert.ToInt32(dataReader["COUID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["LinkedInProfile"]))
                    {
                        candidateInformation.LinkedInProfile
                            = dataReader["LinkedInProfile"].ToString().Trim();
                    }
                }

                return candidateInformation;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Inserts the downloadload resume log.
        /// </summary>
        /// <param name="candidateID">The candidate ID.</param>
        /// <param name="candidateName">Name of the candidate.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="userID">The user ID.</param>
        public void InsertDownloadloadResumeLog(int candidateID, string candidateName, string fileName, int userID)
        {
            DbCommand insertUploadResumeLog = HCMDatabase.
                GetStoredProcCommand("SPINSERT_DOWNLOAD_RESUME_LOG");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@CANDIDATE_ID", DbType.Int32, candidateID);
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@CANDIDATE_NAME", DbType.String, candidateName);
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@FILE_NAME", DbType.String, fileName);
            HCMDatabase.AddInParameter(insertUploadResumeLog,
                "@USER_ID", DbType.Int32, userID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertUploadResumeLog);
        }

        /// <summary>
        /// Represents the method that get the title nodes from the 
        /// database
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="gridPageSize">
        /// A<see cref="int"/>that holds the grid page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total records
        /// </param>
        /// <param name="searchKeyWord">
        /// A<see cref="string"/>that holds the search key word
        /// </param>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/> that holds the list of node details
        /// </returns>
        public List<NodeDetail> GetPositionTitleNodes(int pageNumber, int gridPageSize,
            out int totalRecords, string searchKeyWord)
        {
            IDataReader dataReader = null;
            List<NodeDetail> nodeDetail = null;

            totalRecords = 0;

            try
            {
                DbCommand getPositionTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_POSITION_TITLE_NODE");

                HCMDatabase.AddInParameter(getPositionTitleCommand, "@PAGE_NUMBER", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getPositionTitleCommand, "@PAGE_SIZE", DbType.Int32, gridPageSize);

                HCMDatabase.AddInParameter(getPositionTitleCommand, "@SEARCH_KEYWORD", DbType.String, searchKeyWord.Trim().Length == 0 ? null : searchKeyWord.Trim());

                dataReader = HCMDatabase.ExecuteReader(getPositionTitleCommand);

                while (dataReader.Read())
                {
                    if (nodeDetail == null)
                        nodeDetail = new List<NodeDetail>();

                    NodeDetail nodeElements = new NodeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                    else
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        {
                            nodeElements.ID = Convert.ToInt32
                                (dataReader["ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                        {
                            nodeElements.Name = dataReader["NAME"].
                                ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                        {
                            nodeElements.AliasName = dataReader["ALIAS_NAME"].
                                ToString().Trim();
                        }

                        // Add to the list.
                        nodeDetail.Add(nodeElements);
                    }
                }

                return nodeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts position title.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the position node detail.
        /// </param> 
        public void InsertPositionTitle(NodeDetail nodeDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertPositionTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_POSITION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertPositionTitleCommand,
                "@POSITION_TITLE", DbType.String, nodeDetail.Name);

            HCMDatabase.AddInParameter(insertPositionTitleCommand,
                "@USER_ID", DbType.Int32, nodeDetail.CreatedBy);

            HCMDatabase.AddInParameter(insertPositionTitleCommand,
                "@POSITION_TITLE_ALIAS", DbType.String, nodeDetail.AliasName);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertPositionTitleCommand);
        }

        /// <summary>
        /// Method that inserts negative title.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the position node detail.
        /// </param>
        public void InsertNegativeTitle(NodeDetail nodeDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertPositionTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_NEGATIVE_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertPositionTitleCommand,
                "@NEGATIVE_TITLE", DbType.String, nodeDetail.Name);

            HCMDatabase.AddInParameter(insertPositionTitleCommand,
                "@USER_ID", DbType.Int32, nodeDetail.CreatedBy);

            HCMDatabase.AddInParameter(insertPositionTitleCommand,
              "@NEGATIVE_TITLE_ALIAS", DbType.String, nodeDetail.AliasName);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertPositionTitleCommand);
        }

        /// <summary>
        /// Method that updates position title.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the position node detail.
        /// </param>
        public void UpdatePositionTitle(NodeDetail nodeDetail)
        {
            // Create a stored procedure command object.
            DbCommand updatePositionTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_PARSER_POSITION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updatePositionTitleCommand,
                "@POSITION_ID", DbType.Int32, nodeDetail.ID);

            HCMDatabase.AddInParameter(updatePositionTitleCommand,
                "@POSITION_TITLE", DbType.String, nodeDetail.Name);

            HCMDatabase.AddInParameter(updatePositionTitleCommand,
                "@POSITION_TITLE_ALIAS", DbType.String, nodeDetail.AliasName);

            HCMDatabase.AddInParameter(updatePositionTitleCommand,
                "@USER_ID", DbType.Int32, nodeDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updatePositionTitleCommand);
        }

        /// <summary>
        /// Method that updates negative title.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the position node detail.
        /// </param>
        public void UpdateNegativeTitle(NodeDetail nodeDetail)
        {
            // Create a stored procedure command object.
            DbCommand updatePositionTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_PARSER_NEGATIVE_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updatePositionTitleCommand,
                "@NEGATIVE_ID", DbType.Int32, nodeDetail.ID);

            HCMDatabase.AddInParameter(updatePositionTitleCommand,
                "@NEGATIVE_TITLE", DbType.String, nodeDetail.Name);

            HCMDatabase.AddInParameter(updatePositionTitleCommand,
                "@USER_ID", DbType.Int32, nodeDetail.ModifiedBy);

            HCMDatabase.AddInParameter(updatePositionTitleCommand,
                "@NEGATIVE_TITLE_ALIAS", DbType.String, nodeDetail.AliasName);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updatePositionTitleCommand);
        }

        /// <summary>
        /// Method that deletes position title.
        /// </summary>
        /// <param name="PositionID">
        /// A <see cref="int"/> that holds the position title ID.
        /// </param>
        public void DeletePositionTitle(int PositionID)
        {
            // Create a stored procedure command object.
            DbCommand deletePositionTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_PARSER_POSITION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(deletePositionTitleCommand,
                "@POSITION_ID", DbType.Int32, PositionID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deletePositionTitleCommand);
        }

        /// <summary>
        /// Method that deletes negative title.
        /// </summary>
        /// <param name="negativeTitleID">
        /// A <see cref="int"/> that holds the position title ID.
        /// </param>
        public void DeleteNegativeTitle(int negativeTitleID)
        {
            // Create a stored procedure command object.
            DbCommand deleteNegativeTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_PARSER_NEGATIVE_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteNegativeTitleCommand,
                "@NEGATIVE_ID", DbType.Int32, negativeTitleID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteNegativeTitleCommand);
        }


        /// <summary>
        /// Represents the method that retrieves the degree name.
        /// database
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="gridPageSize">
        /// A<see cref="int"/>that holds the grid page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total records
        /// </param>
        /// <param name="searchKeyWord">
        /// A<see cref="string"/>that holds the search key word
        /// </param>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/> that holds the list of node details
        /// </returns>
        public List<NodeDetail> GetDegree(int pageNumber, int gridPageSize,
            out int totalRecords, string searchKeyWord)
        {
            IDataReader dataReader = null;
            List<NodeDetail> nodeDetail = null;

            totalRecords = 0;

            try
            {
                DbCommand getDegreeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_DEGREE_NAME");


                HCMDatabase.AddInParameter(getDegreeCommand, "@PAGE_NUMBER",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getDegreeCommand,
                    "@PAGE_SIZE", DbType.Int32, gridPageSize);

                HCMDatabase.AddInParameter(getDegreeCommand,
                    "@SEARCH_KEYWORD", DbType.String, searchKeyWord.Trim().Length == 0 ?
                    null : searchKeyWord.Trim());

                dataReader = HCMDatabase.ExecuteReader(getDegreeCommand);

                while (dataReader.Read())
                {
                    if (nodeDetail == null)
                        nodeDetail = new List<NodeDetail>();

                    NodeDetail nodeElements = new NodeDetail();

                    if (DBNull.Value == dataReader["TOTAL"])
                    {

                        if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        {
                            nodeElements.ID = Convert.ToInt32
                                (dataReader["ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                        {
                            nodeElements.Name = dataReader["NAME"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                        {
                            nodeElements.AliasName
                                = dataReader["ALIAS_NAME"].
                                ToString().Trim();
                        }

                        // Add to the list.
                        nodeDetail.Add(nodeElements);

                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return nodeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the negative titles
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="gridPageSize">
        /// A<see cref="int"/>that holds the grid page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total records
        /// </param>
        /// <param name="searchKeyWord">
        /// A<see cref="string"/>that holds the search key word
        /// </param>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/> that holds the list of node details
        /// </returns>
        public List<NodeDetail> GetNegativeTitles(int pageNumber, int gridPageSize,
            out int totalRecords, string searchKeyWord)
        {
            IDataReader dataReader = null;
            List<NodeDetail> nodeDetail = null;
            totalRecords = 0;
            try
            {
                DbCommand getDegreeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_NEGATIVE_TITLE_NODE");

                HCMDatabase.AddInParameter(getDegreeCommand, "@PAGE_NUMBER",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getDegreeCommand,
                    "@PAGE_SIZE", DbType.Int32, gridPageSize);

                HCMDatabase.AddInParameter(getDegreeCommand,
                    "@SEARCH_KEYWORD", DbType.String, searchKeyWord.Trim().Length == 0 ?
                    null : searchKeyWord.Trim());

                dataReader = HCMDatabase.ExecuteReader(getDegreeCommand);

                while (dataReader.Read())
                {
                    if (nodeDetail == null)
                        nodeDetail = new List<NodeDetail>();

                    NodeDetail nodeElements = new NodeDetail();

                    if (DBNull.Value == dataReader["TOTAL"])
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        {
                            nodeElements.ID = Convert.ToInt32
                                (dataReader["ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                        {
                            nodeElements.Name = dataReader["NAME"].
                                ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                        {
                            nodeElements.AliasName = dataReader["ALIAS_NAME"].
                                ToString().Trim();
                        }

                        // Add to the list.
                        nodeDetail.Add(nodeElements);

                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return nodeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts degree name.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the position node detail.
        /// </param>
        public void InsertDegree(NodeDetail nodeDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertDegreeCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_DEGREE_NAME");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertDegreeCommand,
                "@DEGREE_NAME", DbType.String, nodeDetail.Name);

            HCMDatabase.AddInParameter(insertDegreeCommand,
                "@USER_ID", DbType.Int32, nodeDetail.CreatedBy);

            HCMDatabase.AddInParameter(insertDegreeCommand,
                 "@DEGREE_NAME_ALIAS", DbType.String, nodeDetail.AliasName);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertDegreeCommand);
        }

        /// <summary>
        /// Method that updates degree name.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the position node detail.
        /// </param>
        public void UpdateDegree(NodeDetail nodeDetail)
        {
            // Create a stored procedure command object.
            DbCommand updateDegreeCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_PARSER_DEGREE_NAME");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateDegreeCommand,
                "@DEGREE_ID", DbType.Int32, nodeDetail.ID);

            HCMDatabase.AddInParameter(updateDegreeCommand,
                "@DEGREE_NAME", DbType.String, nodeDetail.Name);

            HCMDatabase.AddInParameter(updateDegreeCommand,
                "@USER_ID", DbType.Int32, nodeDetail.ModifiedBy);

            HCMDatabase.AddInParameter(updateDegreeCommand,
                "@DEGREE_NAME_ALIAS", DbType.String, nodeDetail.AliasName);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateDegreeCommand);
        }

        /// <summary>
        /// Method that deletes degree name.
        /// </summary>
        /// <param name="DegreeID">
        /// A <see cref="int"/> that holds the position title ID.
        /// </param>
        public void DeleteDegree(int DegreeID)
        {
            // Create a stored procedure command object.
            DbCommand deleteDegreeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_PARSER_DEGREE_NAME");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteDegreeCommand,
                "@DEGREE_ID", DbType.Int32, DegreeID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteDegreeCommand);
        }

        /// <summary>
        /// Method that retrieves the disciplines titles
        /// </summary>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="gridPageSize">
        /// A<see cref="int"/>that holds the grid page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total records
        /// </param>
        /// <param name="searchKeyWord">
        /// A<see cref="string"/>that holds the search key word
        /// </param>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/> that holds the list of node details
        /// </returns>
        public List<NodeDetail> GetDisciplines(int pageNumber, int gridPageSize,
                        out int totalRecords, string searchKeyWord)
        {
            IDataReader dataReader = null;
            List<NodeDetail> nodeDetail = null;
            totalRecords = 0;

            try
            {
                DbCommand getDegreeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_DEGREE_DISCIPLINE_NODE");

                HCMDatabase.AddInParameter(getDegreeCommand, "@PAGE_NUMBER",
                     DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getDegreeCommand,
                    "@PAGE_SIZE", DbType.Int32, gridPageSize);

                HCMDatabase.AddInParameter(getDegreeCommand,
                    "@SEARCH_KEYWORD", DbType.String, searchKeyWord.Trim().Length == 0 ?
                    null : searchKeyWord.Trim());

                dataReader = HCMDatabase.ExecuteReader(getDegreeCommand);

                while (dataReader.Read())
                {
                    NodeDetail nodeElements = new NodeDetail();

                    if (nodeDetail == null)
                        nodeDetail = new List<NodeDetail>();

                    if (DBNull.Value == dataReader["TOTAL"])
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        {
                            nodeElements.ID = Convert.ToInt32
                                (dataReader["ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                        {
                            nodeElements.Name = dataReader["NAME"].
                                ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                        {
                            nodeElements.AliasName = dataReader["ALIAS_NAME"].
                                ToString().Trim();
                        }

                        // Add to the list.
                        nodeDetail.Add(nodeElements);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return nodeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts disciplines name.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the position node detail.
        /// </param>
        public void InsertDisciplines(NodeDetail nodeDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertDegreeCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_DEGREE_DISCIPLINE_NAME");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertDegreeCommand,
                "@DISCIPLINE_NAME", DbType.String, nodeDetail.Name);

            HCMDatabase.AddInParameter(insertDegreeCommand,
                "@USER_ID", DbType.Int32, nodeDetail.CreatedBy);

            HCMDatabase.AddInParameter(insertDegreeCommand,
                 "@DISCIPLINE_ALIAS", DbType.String, nodeDetail.AliasName);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertDegreeCommand);
        }

        /// <summary>
        /// Method that updates degree name.
        /// </summary>
        /// <param name="nodeDetail">
        /// A <see cref="NodeDetail"/> that holds the position node detail.
        /// </param>
        public void UpdateDiscipline(NodeDetail nodeDetail)
        {
            // Create a stored procedure command object.
            DbCommand updateDegreeCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_DEGREE_DISCIPLINE_NAME");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateDegreeCommand,
                "@DISCIPLINE_ID", DbType.Int32, nodeDetail.ID);

            HCMDatabase.AddInParameter(updateDegreeCommand,
                "@DISCIPLINE_NAME", DbType.String, nodeDetail.Name);

            HCMDatabase.AddInParameter(updateDegreeCommand,
                "@USER_ID", DbType.Int32, nodeDetail.ModifiedBy);

            HCMDatabase.AddInParameter(updateDegreeCommand,
                "@DISCIPLINE_NAME_ALIAS", DbType.String, nodeDetail.AliasName);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateDegreeCommand);
        }

        /// <summary>
        /// Method that deletes degree name.
        /// </summary>
        /// <param name="DegreeID">
        /// A <see cref="int"/> that holds the position title ID.
        /// </param>
        public void DeleteDiscipline(int DisciplineID)
        {
            // Create a stored procedure command object.
            DbCommand deleteDegreeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_PARSER_DISCIPLINE_NAME");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteDegreeCommand,
                "@DISCIPLINE_ID", DbType.Int32, DisciplineID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteDegreeCommand);
        }

        /// <summary>
        /// Represents the method to get the full node list from the database
        /// </summary>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/>that holds the list of nodes 
        /// </returns>
        public List<NodeDetail> GetPositionNodesFullList()
        {
            IDataReader dataReader = null;
            List<NodeDetail> nodeDetail = null;

            try
            {
                DbCommand getPositionTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_POSITION_FULL_NODES");

                dataReader = HCMDatabase.ExecuteReader(getPositionTitleCommand);

                while (dataReader.Read())
                {
                    if (nodeDetail == null)
                        nodeDetail = new List<NodeDetail>();

                    NodeDetail nodeElements = new NodeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                    {
                        nodeElements.ID = Convert.ToInt32
                            (dataReader["ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                    {
                        nodeElements.Name = dataReader["NAME"].
                            ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                    {
                        nodeElements.AliasName = dataReader["ALIAS_NAME"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    nodeDetail.Add(nodeElements);

                }

                return nodeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the full node list from the database
        /// </summary>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/>that holds the list of nodes 
        /// </returns>
        public List<NodeDetail> GetDegreeNodesFullList()
        {
            IDataReader dataReader = null;
            List<NodeDetail> nodeDetail = null;

            try
            {
                DbCommand getDegreeTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_DEGREE_FULL_LIST");

                dataReader = HCMDatabase.ExecuteReader(getDegreeTitleCommand);

                while (dataReader.Read())
                {
                    if (nodeDetail == null)
                        nodeDetail = new List<NodeDetail>();

                    NodeDetail nodeElements = new NodeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                    {
                        nodeElements.ID = Convert.ToInt32
                            (dataReader["ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                    {
                        nodeElements.Name = dataReader["NAME"].
                            ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                    {
                        nodeElements.AliasName = dataReader["ALIAS_NAME"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    nodeDetail.Add(nodeElements);

                }

                return nodeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }


        /// <summary>
        /// Represents the method to get the full node list from the database
        /// </summary>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/>that holds the list of nodes 
        /// </returns>
        public List<NodeDetail> GetNegativeFullList()
        {
            IDataReader dataReader = null;
            List<NodeDetail> nodeDetail = null;

            try
            {
                DbCommand getNegativeTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_NEGATIVE_FULL_LIST");

                dataReader = HCMDatabase.ExecuteReader(getNegativeTitleCommand);

                while (dataReader.Read())
                {
                    if (nodeDetail == null)
                        nodeDetail = new List<NodeDetail>();

                    NodeDetail nodeElements = new NodeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                    {
                        nodeElements.ID = Convert.ToInt32
                            (dataReader["ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                    {
                        nodeElements.Name = dataReader["NAME"].
                            ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                    {
                        nodeElements.AliasName = dataReader["ALIAS_NAME"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    nodeDetail.Add(nodeElements);

                }

                return nodeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Represents the method to get the full node list from the database
        /// </summary>
        /// <returns>
        /// A<see cref="List<NodeDetail>"/>that holds the list of nodes 
        /// </returns>
        public List<NodeDetail> GetDisciplineFullList()
        {
            IDataReader dataReader = null;
            List<NodeDetail> nodeDetail = null;


            try
            {
                DbCommand getDegreeCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_DEGREE_DISCIPLINE_FULL_NODE");

                dataReader = HCMDatabase.ExecuteReader(getDegreeCommand);

                while (dataReader.Read())
                {
                    NodeDetail nodeElements = new NodeDetail();

                    if (nodeDetail == null)
                        nodeDetail = new List<NodeDetail>();


                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                    {
                        nodeElements.ID = Convert.ToInt32
                            (dataReader["ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                    {
                        nodeElements.Name = dataReader["NAME"].
                            ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                    {
                        nodeElements.AliasName = dataReader["ALIAS_NAME"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    nodeDetail.Add(nodeElements);

                }

                return nodeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<TechnicalSkillsGroup> GetTechnicalGroup()
        {
            DbCommand GetCompetencyVectorCommand = null;

            IDataReader dataReader = null;

            List<TechnicalSkillsGroup> techniclSkillGroups = new List<TechnicalSkillsGroup>();

            try
            {
                GetCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_TECHINICAL_SKILL_GROUP");

                dataReader = HCMDatabase.ExecuteReader(GetCompetencyVectorCommand);

                while (dataReader.Read())
                {
                    TechnicalSkillsGroup techSkillGroup = new TechnicalSkillsGroup();

                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_ID"]))
                    {
                        techSkillGroup.GroupID = int.Parse(dataReader["GROUP_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                    {
                        techSkillGroup.GroupName = dataReader["GROUP_NAME"].ToString();
                    }

                    techniclSkillGroups.Add(techSkillGroup);
                }

                return techniclSkillGroups;
            }

            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
            }

        }

        public List<TechnicalSkillsDetails> GetTechSkillsDetails(int techGroupId, string techSkillName,
            string techSkillAliasName, string sortField, string sortOrder,
            int pageNumber, int pageSize, out int total)
        {
            DbCommand GetCompetencyVectorCommand = null;

            IDataReader dataReader = null;

            List<TechnicalSkillsDetails> techSkillDetails = new List<TechnicalSkillsDetails>();

            total = 0;

            try
            {
                GetCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPSEARCH_TECHNICAL_SKILL_DETAILS");

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@GROUP_NAME", DbType.String,
                    techGroupId.ToString() == "0" ? null : techGroupId.ToString());

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@TECH_NAME", DbType.String,
                    techSkillName.Trim().Length == 0 ? null : techSkillName);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@TECH_ALIAS_NAME", DbType.String,
                    techSkillAliasName.Trim().Length == 0 ? null : techSkillAliasName);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@PAGENUM", DbType.Int32,
                 pageNumber);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@PAGESIZE", DbType.Int32,
                 pageSize);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@ORDERBY", DbType.String,
                sortField);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@ORDERBYDIRECTION", DbType.String,
                 sortOrder);

                dataReader = HCMDatabase.ExecuteReader(GetCompetencyVectorCommand);

                while (dataReader.Read())
                {
                    TechnicalSkillsDetails technicalSkillsDetails = new TechnicalSkillsDetails();

                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                        {
                            technicalSkillsDetails.TechSkillID = int.Parse(dataReader["SKILL_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["GROUP_ID"]))
                        {
                            technicalSkillsDetails.TechGroupID = dataReader["GROUP_ID"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                        {
                            technicalSkillsDetails.TechGroup = dataReader["GROUP_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                        {
                            technicalSkillsDetails.TechSkillName = dataReader["SKILL_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                        {
                            technicalSkillsDetails.TechSkillAliasName = dataReader["ALIAS_NAME"].ToString();
                        }

                        techSkillDetails.Add(technicalSkillsDetails);
                    }
                    else
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        {
                            total = int.Parse(dataReader["TOTAL"].ToString());
                        }
                    }
                }

                return techSkillDetails;
            }

            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
            }


        }

        /// <summary>
        /// Represents the method to add a new technical skills 
        /// </summary>
        /// <param name="techGroupID">
        /// A<see cref="int"/>that holds the group ID
        /// </param>
        /// <param name="skillName">
        /// A<see cref="string"/>that holds the skill name
        /// </param>
        /// <param name="aliasName">
        /// A<see cref="string"/>that holds the alias name
        /// </param>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the user Id
        /// </param>
        /// <returns></returns>
        public bool AddNewSkills(int techGroupID, string skillName, string aliasName, int userID)
        {
            DbCommand InsertSkillCommand = null;

            bool value = false;

            try
            {
                InsertSkillCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_SKILL");

                HCMDatabase.AddInParameter(InsertSkillCommand,
                    "@TECH_GROUP_ID", DbType.Int32, techGroupID);

                HCMDatabase.AddInParameter(InsertSkillCommand,
                    "@SKILL_NAME", DbType.String, skillName);

                HCMDatabase.AddInParameter(InsertSkillCommand,
                    "@ALIAS_NAME", DbType.String, aliasName);

                HCMDatabase.AddInParameter(InsertSkillCommand,
                    "@CREATED_BY", DbType.String, userID);

                HCMDatabase.AddOutParameter(InsertSkillCommand,
                    "@EXISTS", DbType.String, 1);

                HCMDatabase.ExecuteNonQuery(InsertSkillCommand);

                string ifExists = HCMDatabase.GetParameterValue
                    (InsertSkillCommand, "@EXISTS").ToString().Trim();

                value = ifExists == "Y" ? false : true;

                return value;
            }
            finally
            {

            }

        }
        /// <summary>
        /// Represents the method to update the skill alias name
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the skill id 
        /// </param>
        /// <param name="vectorAliasName">
        /// A<see cref="string"/>that holds the skill alias name
        /// </param>
        public void UpdateSkillAliasName(int vectorID, string vectorAliasName)
        {
            DbCommand UpdateCompetencyVectorCommand = null;

            try
            {
                UpdateCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_TECHNICAL_SKILLS");

                HCMDatabase.AddInParameter(UpdateCompetencyVectorCommand, "@SKILL_ID", DbType.Int32, vectorID);

                HCMDatabase.AddInParameter(UpdateCompetencyVectorCommand, "@SKILL_ALIAS_NAME", DbType.String,
                    vectorAliasName.Trim());

                HCMDatabase.ExecuteScalar(UpdateCompetencyVectorCommand);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Represents the method to check whether the skill id is involved 
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the vector id
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that returns whether id is invloved or not
        /// </returns>
        public bool CheckSkillIDInvolved(int skillID)
        {
            DbCommand CheckSkillIDCommand = null;

            object existSkillID = null;

            try
            {
                CheckSkillIDCommand = HCMDatabase.
                    GetStoredProcCommand("SPCHECK_SKILL_ID");

                HCMDatabase.AddInParameter(CheckSkillIDCommand, "@SKILL_ID",
                    DbType.Int32, skillID);

                existSkillID = HCMDatabase.ExecuteScalar(CheckSkillIDCommand);

                if (Utility.IsNullOrEmpty(existSkillID))
                    return true;
                else
                    return false;
            }
            finally
            {


            }

        }

        /// <summary>
        /// Represents the method to delete the technical skill 
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the skill id 
        /// </param>
        public void DeleteTechnicalSkill(int skillID)
        {
            DbCommand DeleteSkillCommand = null;

            try
            {
                DeleteSkillCommand = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_SKILL");

                HCMDatabase.AddInParameter(DeleteSkillCommand,
                    "@SKILL_ID", DbType.Int32, skillID);

                HCMDatabase.ExecuteScalar(DeleteSkillCommand);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Method that returns the list of location types from the Database
        /// </summary>
        /// <returns>
        /// A<see cref="List<LocationDetails>"/>that holds the list of location types
        /// </returns>
        public List<LocationDetails> GetLocationType()
        {
            DbCommand GetLocationTypeCommand = null;

            IDataReader dataReader = null;

            List<LocationDetails> locationDetailsGroups = new List<LocationDetails>();

            try
            {
                GetLocationTypeCommand = HCMDatabase.
                   GetStoredProcCommand("SPGET_LOCATION_GROUP");

                dataReader = HCMDatabase.ExecuteReader(GetLocationTypeCommand);

                while (dataReader.Read())
                {
                    LocationDetails locationDetail = new LocationDetails();

                    if (!Utility.IsNullOrEmpty(dataReader["TYPE_ID"]))
                    {
                        locationDetail.LocationTypeID = int.Parse(dataReader["TYPE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TYPE_NAME"]))
                    {
                        locationDetail.LocationType = dataReader["TYPE_NAME"].ToString();
                    }
                    locationDetailsGroups.Add(locationDetail);
                }

                return locationDetailsGroups;
            }

            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
            }

        }

        /// <summary>
        /// Method that returns the list of location details from the database,
        /// based on the search criteria
        /// </summary>
        /// <param name="locationId">
        /// A<see cref="int"/>that holds the location ID
        /// </param>
        /// <param name="locationName">
        /// A<see cref="string"/>that holds the location name
        /// </param>
        /// <param name="locationAliasName">
        /// A<see cref="string"/>that holds the location alias name
        /// </param>
        /// <param name="sortField">
        /// A<see cref="string"/>that holds the sort field
        /// </param>
        /// <param name="sortOrder">
        /// A<see cref="string"/>that holds the sort order
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pagenumber
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <param name="total">
        /// A<see cref="int"/>that holds the total
        /// </param>
        /// <returns>
        /// A<see cref="List<LocationDetails>"/>that holds the 
        /// list of location details
        /// </returns>
        public List<LocationDetails> GetLocationDetails(int locationId, string locationName,
            string locationAliasName, string sortField, string sortOrder, int pageNumber,
            int pageSize, out int total)
        {
            DbCommand GetCompetencyVectorCommand = null;

            IDataReader dataReader = null;

            List<LocationDetails> locationDetails = new List<LocationDetails>();

            total = 0;

            try
            {
                GetCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPSEARCH_LOCATION_DETAILS");

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@LOCATION_GROUP_NAME", DbType.String,
                    locationId.ToString() == "0" ? null : locationId.ToString());

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@LOCATION_NAME", DbType.String,
                    locationName.Trim().Length == 0 ? null : locationName);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@LOCATION_ALIAS_NAME", DbType.String,
                    locationAliasName.Trim().Length == 0 ? null : locationAliasName);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@PAGENUM", DbType.Int32,
                 pageNumber);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@PAGESIZE", DbType.Int32,
                 pageSize);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@ORDERBY", DbType.String,
                sortField);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@ORDERBYDIRECTION", DbType.String,
                 sortOrder);

                dataReader = HCMDatabase.ExecuteReader(GetCompetencyVectorCommand);

                while (dataReader.Read())
                {
                    LocationDetails locationDetail = new LocationDetails();

                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["LOCATION_ID"]))
                        {
                            locationDetail.LocationID = int.Parse(dataReader["LOCATION_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["LOCATION_TYPE_ID"]))
                        {
                            locationDetail.LocationTypeID = int.Parse(dataReader["LOCATION_TYPE_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["LOCATION_TYPE_NAME"]))
                        {
                            locationDetail.LocationType = dataReader["LOCATION_TYPE_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["LOCATION_NAME"]))
                        {
                            locationDetail.LocationName = dataReader["LOCATION_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["LOCATION_ALIAS"]))
                        {
                            locationDetail.LocationAliasName = dataReader["LOCATION_ALIAS"].ToString();
                        }

                        locationDetails.Add(locationDetail);
                    }
                    else
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        {
                            total = int.Parse(dataReader["TOTAL"].ToString());
                        }
                    }
                }

                return locationDetails;
            }

            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
            }
        }

        /// <summary>
        /// Method to add the new location to the database
        /// </summary>
        /// <param name="locationGroupID">
        /// A<see cref="int"/>that holds the locationGroupID
        /// </param>
        /// <param name="locationName">
        /// A<see cref="string"/>that holds the location name
        /// </param>
        /// <param name="aliasName">
        /// A<see cref="string"/>that holds the alias name
        /// </param>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userID
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds bool value whether the 
        /// location already exist or not
        /// </returns>
        public bool AddNewLocation(int locationGroupID, string locationName, string aliasName, int userID)
        {
            DbCommand InsertLocationCommand = null;

            bool value = false;

            try
            {
                InsertLocationCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_LOCATION");

                HCMDatabase.AddInParameter(InsertLocationCommand,
                    "@LOCATION_GROUP_ID", DbType.Int32, locationGroupID);

                HCMDatabase.AddInParameter(InsertLocationCommand,
                    "@LOCATION_NAME", DbType.String, locationName);

                HCMDatabase.AddInParameter(InsertLocationCommand,
                    "@ALIAS_NAME", DbType.String, aliasName);

                HCMDatabase.AddInParameter(InsertLocationCommand,
                    "@CREATED_BY", DbType.String, userID);

                HCMDatabase.AddOutParameter(InsertLocationCommand,
                    "@EXISTS", DbType.String, 1);

                HCMDatabase.ExecuteNonQuery(InsertLocationCommand);

                string ifExists = HCMDatabase.GetParameterValue
                    (InsertLocationCommand, "@EXISTS").ToString().Trim();

                value = ifExists == "Y" ? false : true;

                return value;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Represents the method to update the location alias name
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the skill id 
        /// </param>
        /// <param name="skillAliasName">
        /// A<see cref="string"/>that holds the skill alias name
        /// </param>
        public void UpdateLocationAliasName(int locationID, string locationAliasName)
        {
            DbCommand UpdateLocationAliasNameCommand = null;

            try
            {
                UpdateLocationAliasNameCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_LOCATION_ALIAS");

                HCMDatabase.AddInParameter(UpdateLocationAliasNameCommand, "@LOCATION_ID", DbType.Int32, locationID);

                HCMDatabase.AddInParameter(UpdateLocationAliasNameCommand, "@LOCATION_ALIAS_NAME", DbType.String,
                    locationAliasName.Trim());

                HCMDatabase.ExecuteScalar(UpdateLocationAliasNameCommand);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Represents the method that is used to delete the location
        /// </summary>
        /// <param name="skillID">
        /// A<see cref="int"/>that holds the skill id
        /// </param>
        public void DeleteLocation(int skillID)
        {
            DbCommand DeleteSkillCommand = null;

            try
            {
                DeleteSkillCommand = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_LOCATION");

                HCMDatabase.AddInParameter(DeleteSkillCommand,
                    "@LOCATION_ID", DbType.Int32, skillID);

                HCMDatabase.ExecuteScalar(DeleteSkillCommand);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Method that retrieves the list dictionary details.
        /// </summary>
        /// <param name="dictionaryTypeID">
        /// A <see cref="int"/> that holds the dictionary type ID.
        /// </param>
        /// <param name="headLike">
        /// A <see cref="string"/> that holds the head keywords.
        /// </param>
        /// <param name="aliasLike">
        /// A <see cref="string"/> that holds the alias keywords.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="DictionaryDetail"/> that holds the dictionary 
        /// details.
        /// </returns>
        public List<DictionaryDetail> GetDictionaryDetails(int dictionaryTypeID,
            string headLike, string aliasLike, string orderBy,
            SortType orderByDirection, int pageNumber, int pageSize,
            out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getDictionaryDetails = HCMDatabase
                    .GetStoredProcCommand("SPSEARCH_DICTIONARY");

                // Add parameters.

                if (dictionaryTypeID == 0)
                {
                    HCMDatabase.AddInParameter(getDictionaryDetails,
                        "@DICTIONARY_TYPE_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getDictionaryDetails,
                        "@DICTIONARY_TYPE_ID", DbType.Int32, dictionaryTypeID);
                }

                HCMDatabase.AddInParameter(getDictionaryDetails,
                    "@HEAD", DbType.String, headLike == null || headLike.Trim().
                    Length == 0 ? null : headLike.Trim());

                HCMDatabase.AddInParameter(getDictionaryDetails,
                    "@ALIAS", DbType.String, aliasLike == null || aliasLike.Trim().
                    Length == 0 ? null : aliasLike.Trim());

                HCMDatabase.AddInParameter(getDictionaryDetails,
                    "@ORDER_BY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getDictionaryDetails,
                    "@ORDER_BY_DIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getDictionaryDetails,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getDictionaryDetails,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader
                    (getDictionaryDetails);

                List<DictionaryDetail> dictionaryDetails = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (dictionaryDetails == null)
                            dictionaryDetails = new List<DictionaryDetail>();

                        DictionaryDetail dictionaryDetail = new DictionaryDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_TYPE_ID"]))
                        {
                            dictionaryDetail.ElementTypeID = Convert.ToInt32
                                (dataReader["ELEMENT_TYPE_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_TYPE_NAME"]))
                        {
                            dictionaryDetail.ElementTypeName = dataReader
                                ["ELEMENT_TYPE_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_ID"]))
                        {
                            dictionaryDetail.ElementID = Convert.ToInt32(
                                dataReader["ELEMENT_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_NAME"]))
                        {
                            dictionaryDetail.ElementName = dataReader
                                ["ELEMENT_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_ID"]))
                        {
                            dictionaryDetail.AliasID = Convert.ToInt32
                                (dataReader["ALIAS_ID"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                        {
                            dictionaryDetail.AliasName = dataReader
                                ["ALIAS_NAME"].ToString().Trim();
                        }

                        // Add to the list.
                        dictionaryDetails.Add(dictionaryDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return dictionaryDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of element types.
        /// </summary>
        /// <returns>
        /// A list of <see cref="ElementTypeDetail"/> that holds the element
        /// type details.
        /// </returns>
        public List<ElementTypeDetail> GetElementTypes()
        {
            IDataReader dataReader = null;
            List<ElementTypeDetail> elementTypes = null;

            try
            {
                DbCommand getElementTypesCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_ELEMENT_TYPES");

                dataReader = HCMDatabase.ExecuteReader(getElementTypesCommand);

                while (dataReader.Read())
                {
                    if (elementTypes == null)
                        elementTypes = new List<ElementTypeDetail>();

                    ElementTypeDetail elementType = new ElementTypeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_TYPE_ID"]))
                    {
                        elementType.ElementTypeID = Convert.ToInt32
                            (dataReader["ELEMENT_TYPE_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_TYPE_NAME"]))
                    {
                        elementType.ElementTypeName = dataReader["ELEMENT_TYPE_NAME"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    elementTypes.Add(elementType);
                }

                return elementTypes;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of elements for the given element
        /// type ID.
        /// </summary>
        /// <param name="elementTypeID">
        /// A <see cref="int"/> that holds the element type ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="ElementDetail"/> that holds the element details.
        /// </returns>
        public List<ElementDetail> GetElements(int elementTypeID)
        {
            IDataReader dataReader = null;
            List<ElementDetail> elements = null;

            try
            {
                DbCommand getElementsCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_ELEMENTS_BY_ELEMENT_TYPE_ID");

                HCMDatabase.AddInParameter(getElementsCommand,
                    "@ELEMENT_TYPE_ID", DbType.Int16, elementTypeID);

                dataReader = HCMDatabase.ExecuteReader(getElementsCommand);

                while (dataReader.Read())
                {
                    if (elements == null)
                        elements = new List<ElementDetail>();

                    ElementDetail element = new ElementDetail();

                    // Assign element type ID.
                    element.ElementTypeID = elementTypeID;

                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_ID"]))
                    {
                        element.ElementID = Convert.ToInt32
                            (dataReader["ELEMENT_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_NAME"]))
                    {
                        element.ElementName = dataReader["ELEMENT_NAME"].
                            ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_DESCRIPTION"]))
                    {
                        element.ElementDescription = dataReader["ELEMENT_DESCRIPTION"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    elements.Add(element);
                }

                return elements;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the attribute dictionary list for the given 
        /// element ID.
        /// </summary>
        /// <param name="elementID">
        /// A <see cref="int"/> that holds the element ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="AliasDetail"/> that holds the attribute 
        /// dictionary details.
        /// </returns>
        public List<AliasDetail> GetAttributeDictionary(int elementID)
        {
            IDataReader dataReader = null;
            List<AliasDetail> attributes = null;

            try
            {
                DbCommand getAttributesCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_ATTRIBUTE_DICTIONARY");

                HCMDatabase.AddInParameter(getAttributesCommand,
                    "@ELEMENT_ID", DbType.Int16, elementID);

                dataReader = HCMDatabase.ExecuteReader(getAttributesCommand);

                while (dataReader.Read())
                {
                    if (attributes == null)
                        attributes = new List<AliasDetail>();

                    AliasDetail attribute = new AliasDetail();

                    // Assign element type ID.
                    attribute.ElementID = elementID;

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                    {
                        attribute.ID = Convert.ToInt32
                            (dataReader["ATTRIBUTE_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                    {
                        attribute.Name = dataReader["NAME"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    attributes.Add(attribute);
                }

                return attributes;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the title dictionary list for the given 
        /// element ID.
        /// </summary>
        /// <param name="elementID">
        /// A <see cref="int"/> that holds the element ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="AliasDetail"/> that holds the title 
        /// dictionary details.
        /// </returns>
        public List<AliasDetail> GetTitleDictionary(int elementID)
        {
            IDataReader dataReader = null;
            List<AliasDetail> attributes = null;

            try
            {
                DbCommand getAttributesCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_TITLE_DICTIONARY");

                HCMDatabase.AddInParameter(getAttributesCommand,
                    "@ELEMENT_ID", DbType.Int16, elementID);

                dataReader = HCMDatabase.ExecuteReader(getAttributesCommand);

                while (dataReader.Read())
                {
                    if (attributes == null)
                        attributes = new List<AliasDetail>();

                    AliasDetail attribute = new AliasDetail();

                    // Assign element type ID.
                    attribute.ElementID = elementID;

                    if (!Utility.IsNullOrEmpty(dataReader["TITLE_ID"]))
                    {
                        attribute.ID = Convert.ToInt32
                            (dataReader["TITLE_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                    {
                        attribute.Name = dataReader["NAME"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    attributes.Add(attribute);
                }

                return attributes;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts record into title dictionary.
        /// </summary>
        /// <param name="aliasDetail">
        /// A <see cref="AliasDetail"/> that holds the alias detail.
        /// </param>
        public void InsertTitleDictionary(AliasDetail aliasDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_TITLE_DICTIONARY");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@NAME", DbType.String, aliasDetail.Name);

            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ELEMENT_ID", DbType.Int32, aliasDetail.ElementID);

            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@USER_ID", DbType.Int32, aliasDetail.CreatedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand);
        }

        /// <summary>
        /// Method that inserts record into attribute dictionary.
        /// </summary>
        /// <param name="aliasDetail">
        /// A <see cref="AliasDetail"/> that holds the alias detail.
        /// </param>
        public void InsertAttributeDictionary(AliasDetail aliasDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_ATTRIBUTE_DICTIONARY");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@NAME", DbType.String, aliasDetail.Name);

            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ELEMENT_ID", DbType.Int32, aliasDetail.ElementID);

            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@USER_ID", DbType.Int32, aliasDetail.CreatedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand);
        }

        /// <summary>
        /// Method that updates record into title dictionary.
        /// </summary>
        /// <param name="aliasDetail">
        /// A <see cref="AliasDetail"/> that holds the alias detail.
        /// </param>
        public void UpdateTitleDictionary(AliasDetail aliasDetail)
        {
            // Create a stored procedure command object.
            DbCommand updateTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_PARSER_TITLE_DICTIONARY");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateTitleCommand,
                "@TITLE_ID", DbType.Int32, aliasDetail.ID);

            HCMDatabase.AddInParameter(updateTitleCommand,
                "@NAME", DbType.String, aliasDetail.Name);

            HCMDatabase.AddInParameter(updateTitleCommand,
                "@USER_ID", DbType.Int32, aliasDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateTitleCommand);
        }

        /// <summary>
        /// Method that updates record into attribute dictionary.
        /// </summary>
        /// <param name="aliasDetail">
        /// A <see cref="AliasDetail"/> that holds the alias detail.
        /// </param>
        public void UpdateAttributeDictionary(AliasDetail aliasDetail)
        {
            // Create a stored procedure command object.
            DbCommand updateAttributeCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_PARSER_ATTRIBUTE_DICTIONARY");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateAttributeCommand,
                "@ATTRIBUTE_ID", DbType.Int32, aliasDetail.ID);

            HCMDatabase.AddInParameter(updateAttributeCommand,
                "@NAME", DbType.String, aliasDetail.Name);

            HCMDatabase.AddInParameter(updateAttributeCommand,
                "@USER_ID", DbType.Int32, aliasDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateAttributeCommand);
        }

        /// <summary>
        /// Method that delete record from the title dictionary.
        /// </summary>
        /// <param name="titleID">
        /// A <see cref="int"/> that holds the title ID.
        /// </param>
        public void DeleteTitleDictionary(int titleID)
        {
            // Create a stored procedure command object.
            DbCommand deleteTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_PARSER_TITLE_DICTIONARY");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteTitleCommand,
                "@TITLE_ID", DbType.Int32, titleID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteTitleCommand);
        }

        /// <summary>
        /// Method that delete record from the attribute dictionary.
        /// </summary>
        /// <param name="attributeID">
        /// A <see cref="int"/> that holds the attribute ID.
        /// </param>
        public void DeleteAttributeDictionary(int attributeID)
        {
            // Create a stored procedure command object.
            DbCommand deleteAttributeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_PARSER_ATTRIBUTE_DICTIONARY");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteAttributeCommand,
                "@ATTRIBUTE_ID", DbType.Int32, attributeID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteAttributeCommand);
        }

        /// <summary>
        /// This method gets the candidate vector table with selected
        /// candidate resume id.
        /// </summary>
        /// <param name="CandidateResumeID">Candidate resume id 
        /// for which to get competency vector table.</param>
        /// <returns>Dataset contains the competency vector table.</returns>
        public DataSet GetCompetencyVectorTable(int CandidateResumeID)
        {
            DataSet CompetencyVectorDataSet = null;
            DbCommand CompetencyVectorCommand = null;
            try
            {
                CompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPGET_RESUME_SKILL_MATRIX");
                HCMDatabase.AddInParameter(CompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, CandidateResumeID);
                CompetencyVectorDataSet = HCMDatabase.ExecuteDataSet(CompetencyVectorCommand);
                return CompetencyVectorDataSet;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(CompetencyVectorCommand)) CompetencyVectorCommand = null;
                if (!Utility.IsNullOrEmpty(CompetencyVectorDataSet)) CompetencyVectorDataSet = null;
            }
        }

        /// <summary>
        /// This method inserts candidate competency 
        /// in to the database.
        /// </summary>
        /// <param name="candidateCompetencyVector">
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int InsertCandidateCompetencyVector(CandidateCompetencyVector candidateCompetencyVector, IDbTransaction transaction)
        {
            DbCommand InsertCandidateCompetencyVectorCommand = null;
            try
            {
                InsertCandidateCompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_CANDIDATE_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@PARAMETER_ID", DbType.Int32, candidateCompetencyVector.Parameter_Id);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@VALUE", DbType.String, candidateCompetencyVector.CompetencyValue);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@REMARKS", DbType.String, candidateCompetencyVector.Remarks);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@CREATED_BY", DbType.Int32, candidateCompetencyVector.Created_By);
                return HCMDatabase.ExecuteNonQuery(InsertCandidateCompetencyVectorCommand, transaction as DbTransaction);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(InsertCandidateCompetencyVectorCommand)) InsertCandidateCompetencyVectorCommand = null;
            }
        }

        /// <summary>
        /// This method updates the candidate competency vector table
        /// to the database.
        /// </summary>
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int UpdateCandidateCompetencyVector(CandidateCompetencyVector candidateCompetencyVector, IDbTransaction transaction)
        {
            DbCommand UpdateCandidateCompetencyVectorCommand = null;
            try
            {
                if (candidateCompetencyVector.Old_Vector_Id != 0)
                {
                    UpdateCandidateCompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_CANDIDATE_COMPETENCY_VECTOR");
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@OLD_VECTOR_ID", DbType.Int32, candidateCompetencyVector.Old_Vector_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@PARAMETER_ID", DbType.Int32, candidateCompetencyVector.Parameter_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VALUE", DbType.String, candidateCompetencyVector.CompetencyValue);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@REMARKS", DbType.String, candidateCompetencyVector.Remarks);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@MODIFIED_BY", DbType.Int32, candidateCompetencyVector.Created_By);
                    HCMDatabase.ExecuteScalar(UpdateCandidateCompetencyVectorCommand, transaction as DbTransaction);
                    return 1;
                }
                else
                {
                    UpdateCandidateCompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_CANDIDATE_COMPETENCY_VECTOR");
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@PARAMETER_ID", DbType.Int32, candidateCompetencyVector.Parameter_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VALUE", DbType.String, candidateCompetencyVector.CompetencyValue);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@REMARKS", DbType.String, candidateCompetencyVector.Remarks);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@MODIFIED_BY", DbType.Int32, candidateCompetencyVector.Created_By);
                    return HCMDatabase.ExecuteNonQuery(UpdateCandidateCompetencyVectorCommand, transaction as DbTransaction);
                }
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateCandidateCompetencyVectorCommand)) UpdateCandidateCompetencyVectorCommand = null;
            }
        }


        /// <summary>
        /// This method updates the candidate competency vectorid 
        /// to the database.
        /// </summary>
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int UpdateCandidateCompetencyVectorIdWithTransaction(
            CandidateCompetencyVector candidateCompetencyVector, IDbTransaction transaction)
        {
            DbCommand UpdateCandidateCompetencyVectorCommand = null;
            try
            {
                UpdateCandidateCompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_CANDIDATE_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@OLD_VECTOR_ID", DbType.Int32, candidateCompetencyVector.Old_Vector_Id);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@PARAMETER_ID", DbType.Int32, candidateCompetencyVector.Parameter_Id);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VALUE", DbType.String, candidateCompetencyVector.CompetencyValue);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@REMARKS", DbType.String, candidateCompetencyVector.Remarks);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@MODIFIED_BY", DbType.Int32, candidateCompetencyVector.Created_By);
                return HCMDatabase.ExecuteNonQuery(UpdateCandidateCompetencyVectorCommand, transaction as DbTransaction);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateCandidateCompetencyVectorCommand)) UpdateCandidateCompetencyVectorCommand = null;
            }
        }

        /// <summary>
        /// This method delete the vector for a candidate
        /// from the database.
        /// </summary>
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <returns>No of rows effected</returns>
        public int DeleteCandidateCompetencyVector(CandidateCompetencyVector candidateCompetencyVector)
        {
            DbCommand DeleteCandidateCompetencyCommand = null;
            try
            {
                DeleteCandidateCompetencyCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_CANDIDATE_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(DeleteCandidateCompetencyCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                HCMDatabase.AddInParameter(DeleteCandidateCompetencyCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                return HCMDatabase.ExecuteNonQuery(DeleteCandidateCompetencyCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(DeleteCandidateCompetencyCommand)) DeleteCandidateCompetencyCommand = null;
            }
        }

        /// <summary>
        /// This method delete the vector for a candidate
        /// from the database.
        /// </summary>
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <returns>No of rows effected</returns>
        public int DeleteCandidateCompetencyVectorWithTransaction(
            CandidateCompetencyVector candidateCompetencyVector, IDbTransaction transaction)
        {
            DbCommand DeleteCandidateCompetencyCommand = null;
            try
            {
                DeleteCandidateCompetencyCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_CANDIDATE_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(DeleteCandidateCompetencyCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                HCMDatabase.AddInParameter(DeleteCandidateCompetencyCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                return HCMDatabase.ExecuteNonQuery(DeleteCandidateCompetencyCommand, transaction as DbTransaction);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(DeleteCandidateCompetencyCommand)) DeleteCandidateCompetencyCommand = null;
            }
        }

        /// <summary>
        /// Gets the vector id from the group id and
        /// vector name
        /// </summary>
        /// <param name="Group_Id">Group id for which to get the
        /// vector id</param>
        /// <param name="Vector_Name">vector name for which to get the
        /// vector id</param>
        /// <returns>Vector id for the passed group id and vector name.</returns>
        public int GetVectorIDFromGroupIdAndVectorName(int GroupId, string Vector_Name)
        {
            DbCommand CompetencyVectorCommand = null;
            IDataReader dataReader = null;
            try
            {
                int VectorId = 0;
                CompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPGET_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(CompetencyVectorCommand, "@GROUP_ID", DbType.Int32, GroupId);
                HCMDatabase.AddInParameter(CompetencyVectorCommand, "@VECTOR_NAME", DbType.String, Vector_Name);
                dataReader = HCMDatabase.ExecuteReader(CompetencyVectorCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["VECTOR_ID"]))
                        VectorId = Convert.ToInt32(dataReader["VECTOR_ID"]);
                }
                return VectorId;
            }
            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(CompetencyVectorCommand)) CompetencyVectorCommand = null;
            }
        }

        /// <summary>
        /// This method returns the Competency Vector group table.
        /// </summary>
        /// <returns>Dataset contains the competency vector group table.</returns>
        public DataSet GetCompetencyVectorGroupTable()
        {
            DbCommand CompetencyVectorTableCommand = null;
            try
            {
                CompetencyVectorTableCommand = HCMDatabase.GetStoredProcCommand("SPGET_COMPETENCY_VECTOR_GROUP");
                return HCMDatabase.ExecuteDataSet(CompetencyVectorTableCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(CompetencyVectorTableCommand)) CompetencyVectorTableCommand = null;
            }
        }

        /// <summary>
        /// Represents the method to return the list of competency group 
        /// vector details
        /// </summary>
        /// <returns>
        /// A<see cref="List<CompetencyVectorGroup>"/>that holds the 
        /// list of competencty vector group
        /// </returns>
        public List<CompetencyVectorGroup> GetCompetencyVectorGroup()
        {
            DbCommand GetCompetencyVectorCommand = null;

            IDataReader dataReader = null;

            List<CompetencyVectorGroup> competencyVectorGroups = new List<CompetencyVectorGroup>();

            try
            {
                GetCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_VECTORS_GROUP");

                dataReader = HCMDatabase.ExecuteReader(GetCompetencyVectorCommand);

                while (dataReader.Read())
                {
                    CompetencyVectorGroup competencyVectorGroup = new CompetencyVectorGroup();

                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_ID"]))
                    {
                        competencyVectorGroup.VectorGroupID = dataReader["GROUP_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                    {
                        competencyVectorGroup.VectorGroupName = dataReader["GROUP_NAME"].ToString();
                    }

                    competencyVectorGroups.Add(competencyVectorGroup);
                }

                return competencyVectorGroups;
            }

            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
            }
        }

        /// <summary>
        /// Represents the method to get the competency vector details for 
        /// the given search criteria
        /// </summary>
        /// <param name="vectorGroupId">
        /// A<see cref="string"/>that holds the vector group ID
        /// </param>
        /// <param name="vectorName">
        /// A<see cref="string"/>that holds the vector name
        /// </param>
        /// <param name="vectorAliasName">
        /// A<see cref="string"/>that holds the vector alias name
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order by value
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="pgeSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <returns>
        /// A<see cref="List<CompetencyVectorDetails>"/>that holds the 
        /// list of competency vector 
        /// </returns>
        public List<CompetencyVectorDetails> GetCompetencyVectorDetails(string vectorGroupId,
            string vectorName, string vectorAliasName, string orderBy, string sortDirection,
            int pageNumber, object pageSize, out int total)
        {
            DbCommand GetCompetencyVectorCommand = null;

            IDataReader dataReader = null;

            List<CompetencyVectorDetails> competencyVectorDetails = new List<CompetencyVectorDetails>();

            total = 0;

            try
            {
                GetCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPSEARCH_COMPETENCY_VECTORS");

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@GROUP_NAME", DbType.String,
                    vectorGroupId.Trim() == "0" ? null : vectorGroupId);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@VECTOR_NAME", DbType.String,
                    vectorName.Trim().Length == 0 ? null : vectorName);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@VECTOR_ALIAS_NAME", DbType.String,
                    vectorAliasName.Trim().Length == 0 ? null : vectorAliasName);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@PAGENUM", DbType.Int32,
                 pageNumber);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@PAGESIZE",
                    pageSize == null ? DbType.String : DbType.Int32,
                    pageSize);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@ORDERBY", DbType.String,
                orderBy);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@ORDERBYDIRECTION", DbType.String,
                 sortDirection);

                dataReader = HCMDatabase.ExecuteReader(GetCompetencyVectorCommand);

                while (dataReader.Read())
                {
                    CompetencyVectorDetails competencyVectorDetail = new CompetencyVectorDetails();

                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["VECTOR_ID"]))
                        {
                            competencyVectorDetail.VectorID = int.Parse(dataReader["VECTOR_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["GROUP_ID"]))
                        {
                            competencyVectorDetail.VectorGroupID = dataReader["GROUP_ID"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                        {
                            competencyVectorDetail.VectorGroup = dataReader["GROUP_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["VECTOR_NAME"]))
                        {
                            competencyVectorDetail.VectorName = dataReader["VECTOR_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                        {
                            competencyVectorDetail.AliasName = dataReader["ALIAS_NAME"].ToString();
                        }

                        competencyVectorDetails.Add(competencyVectorDetail);
                    }
                    else
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        {
                            total = int.Parse(dataReader["TOTAL"].ToString());
                        }
                    }
                }

                return competencyVectorDetails;
            }

            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
            }

        }

        /// <summary>
        /// Represents the method to update the vector manager alias name
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id 
        /// </param>
        /// <param name="vectorAliasName">
        /// A<see cref="string"/>that holds the vector alias name
        /// </param>
        public void UpdateCompetencyVectorManagerAliasName(int vectorID, string vectorAliasName)
        {
            DbCommand UpdateCompetencyVectorCommand = null;

            try
            {
                UpdateCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_COMPETENCY_VECTORS");

                HCMDatabase.AddInParameter(UpdateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, vectorID);

                HCMDatabase.AddInParameter(UpdateCompetencyVectorCommand, "@VECTOR_ALIAS_NAME", DbType.String,
                    vectorAliasName.Trim());

                HCMDatabase.ExecuteScalar(UpdateCompetencyVectorCommand);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Represents the method to check whether the vector id is involved 
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that returns whether id is invloved or not
        /// </returns>
        public bool CheckVectorIDInvolved(int vectorID)
        {
            DbCommand CheckVectorIDCommand = null;

            object existVectorID = null;

            try
            {
                CheckVectorIDCommand = HCMDatabase.
                    GetStoredProcCommand("SPCHECK_VECTOR_ID");

                HCMDatabase.AddInParameter(CheckVectorIDCommand, "@VECTOR_ID", DbType.Int32, vectorID);

                existVectorID = HCMDatabase.ExecuteScalar(CheckVectorIDCommand);

                if (Utility.IsNullOrEmpty(existVectorID))
                    return true;
                else
                    return false;
            }
            finally
            {


            }

        }

        /// <summary>
        /// Represents the method to delete a competency vector
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id
        /// </param>
        public void DeleteCompetencyVector(int vectorID)
        {
            DbCommand DeleteVectorCommand = null;

            try
            {
                DeleteVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_VECTOR");

                HCMDatabase.AddInParameter(DeleteVectorCommand, "@VECTOR_ID", DbType.Int32, vectorID);

                HCMDatabase.ExecuteScalar(DeleteVectorCommand);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Represents the method to add a new vector
        /// </summary>
        /// <param name="vectorGroupID">
        /// A<see cref="int"/>that holds the vector Id
        /// </param>
        /// <param name="vectorName">
        /// A<see cref="string"/>that holds the vector name
        /// </param>
        /// <param name="aliasName">
        /// A<see cref="string"/>that holds the alias name
        /// </param>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userid 
        /// </param>
        public bool AddNewVector(int vectorGroupID, string vectorName, string aliasName, int userID)
        {
            DbCommand InsertVectorCommand = null;

            bool value = false;

            try
            {
                InsertVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_VECTOR");

                HCMDatabase.AddInParameter(InsertVectorCommand, "@VECTOR_GROUP_ID", DbType.Int32,
                   vectorGroupID);

                HCMDatabase.AddInParameter(InsertVectorCommand, "@VECTOR_NAME", DbType.String, vectorName);

                HCMDatabase.AddInParameter(InsertVectorCommand, "@ALIAS_NAME", DbType.String, aliasName);

                HCMDatabase.AddInParameter(InsertVectorCommand, "@CREATED_BY", DbType.String, userID);

                HCMDatabase.AddOutParameter(InsertVectorCommand, "@EXISTS", DbType.String, 1);

                HCMDatabase.ExecuteNonQuery(InsertVectorCommand);

                string ifExists = HCMDatabase.GetParameterValue(InsertVectorCommand, "@EXISTS").ToString().Trim();

                value = ifExists == "Y" ? false : true;

                return value;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Method that retrieves the group name and vector name.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CertificationTitle"/> that holds group name
        /// </returns>
        public List<CertificationTitle> GetGroupName(int vectorID)
        {
            IDataReader dataReader = null;
            List<CertificationTitle> certificationDetail = null;

            try
            {
                DbCommand getCertificationTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_COMPETENCY_GROUP_NAME");

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@VECTOR_ID", DbType.Int32, vectorID);

                dataReader = HCMDatabase.ExecuteReader(getCertificationTitleCommand);

                while (dataReader.Read())
                {
                    if (certificationDetail == null)
                        certificationDetail = new List<CertificationTitle>();

                    CertificationTitle certificationObj = new CertificationTitle();

                    if (!Utility.IsNullOrEmpty(dataReader["VECTOR_NAME"]))
                    {
                        certificationObj.VectorName = dataReader["VECTOR_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                    {
                        certificationObj.GroupName = dataReader["GROUP_NAME"].ToString().Trim();
                    }

                    // Add to the list.
                    certificationDetail.Add(certificationObj);
                }

                return certificationDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the certification title.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CertificationTitle"/> that holds certification title details
        /// </returns>
        public List<CertificationTitle> GetCertificationTitle(int vectorID)
        {
            IDataReader dataReader = null;
            List<CertificationTitle> certificationDetail = null;

            try
            {
                DbCommand getCertificationTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_CERTIFICATION_TITLE");

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@VECTOR_ID", DbType.Int32, vectorID);

                dataReader = HCMDatabase.ExecuteReader(getCertificationTitleCommand);

                while (dataReader.Read())
                {
                    if (certificationDetail == null)
                        certificationDetail = new List<CertificationTitle>();

                    CertificationTitle certificationObj = new CertificationTitle();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                    {
                        certificationObj.ID = Convert.ToInt32
                            (dataReader["ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_TITLE"]))
                    {
                        certificationObj.Title = dataReader["CERTIFICATION_TITLE"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    certificationDetail.Add(certificationObj);
                }

                return certificationDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts certification title.
        /// </summary>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationTitle"/> that holds the certification detail.
        /// </param>
        public void InserCertification(CertificationTitle certificationDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertCertificationTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_CERTIFICATION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCertificationTitleCommand,
                "@CERTIFICATION_TITLE", DbType.String, certificationDetail.Title);

            HCMDatabase.AddInParameter(insertCertificationTitleCommand,
                "@USER_ID", DbType.Int32, certificationDetail.CreatedBy);

            HCMDatabase.AddInParameter(insertCertificationTitleCommand,
              "@VECTOR_ID", DbType.Int32, certificationDetail.VectorID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertCertificationTitleCommand);
        }

        /// <summary>
        /// Method that updates certification title.
        /// </summary>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationTitle"/> that holds the certification detail.
        /// </param>
        public void UpdateCertification(CertificationTitle certificationDetail)
        {
            // Create a stored procedure command object.
            DbCommand updateCertificationTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CERTIFICATION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCertificationTitleCommand,
                "@ID", DbType.Int32, certificationDetail.ID);

            HCMDatabase.AddInParameter(updateCertificationTitleCommand,
                "@CERTIFICATION_TITLE", DbType.String, certificationDetail.Title);

            HCMDatabase.AddInParameter(updateCertificationTitleCommand,
                "@USER_ID", DbType.Int32, certificationDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCertificationTitleCommand);
        }

        /// <summary>
        /// Method that deletes certification.
        /// </summary>
        /// <param name="ID">
        /// A <see cref="int"/> that holds the certification ID.
        /// </param>
        public void DeleteCertification(int ID)
        {
            // Create a stored procedure command object.
            DbCommand deletePositionTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_PARSER_CERTIFICATION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(deletePositionTitleCommand,
                "@ID", DbType.Int32, ID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deletePositionTitleCommand);
        }

        /// <summary>
        /// Represents the method to get the parser options
        /// </summary>
        /// <returns>
        /// A<see cref="ParserSettings"/>that holds the options details
        /// </returns>
        public ParserSettings GetParserOptions()
        {
            ParserSettings parserSettings = new ParserSettings();

            try
            {
                // Create a stored procedure command object.
                DbCommand getPraserSettingsCommand = HCMDatabase.
                                        GetStoredProcCommand("SPGET_PARSER_OPTIONS");

                parserSettings.VersionCode =
                    HCMDatabase.ExecuteScalar(getPraserSettingsCommand).ToString();

                return parserSettings;

            }
            finally
            {

            }
        }

        /// <summary>
        /// Represents the method that is used to update the 
        /// parser settings
        /// </summary>
        /// <param name="parserSetting">
        /// A<see cref="ParserSettings"/>that holds the parser settings details
        /// </param>
        public void UpdateParserSettings(ParserSettings parserSetting)
        {
            try
            {
                // Create a stored procedure command object.
                DbCommand updatePraserSettingsCommand = HCMDatabase.
                                        GetStoredProcCommand("SPUPDATE_PARSER_OPTIONS");

                HCMDatabase.AddInParameter(updatePraserSettingsCommand, "@VERSION_CODES",
                    DbType.String, parserSetting.VersionCode);

                HCMDatabase.AddInParameter(updatePraserSettingsCommand, "@MODIFIED_BY",
                  DbType.String, parserSetting.ModifiedBy);

                HCMDatabase.ExecuteNonQuery(updatePraserSettingsCommand);

            }
            finally
            {

            }
        }

        /// <summary>
        /// Method that retrieves the list of candidate activities for the
        /// given search parameters.
        /// </summary>
        /// <param name="searchCriteria">
        /// A <see cref="CandidateActivityLogSearchCriteria"/> that holds the
        /// search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pageNumber
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the pageSize
        /// </param>
        /// <param name="sortField">
        /// A<see cref="string"/>that holds the sort Field
        /// </param>
        /// <param name="sortOrder">
        /// A<see cref="SortType"/>that holds the sordOrder like Asc/Dsc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateActivityLog"/> that holds candidate 
        /// activity log details.
        /// </returns>
        public List<CandidateActivityLog> GetCandidateActivities
            (CandidateActivityLogSearchCriteria searchCriteria, int pageNumber,
            int pageSize, string sortField, SortType sortOrder,
            out int totalRecords)
        {
            IDataReader dataReader = null;
            List<CandidateActivityLog> certificationDetail = null;

            // Assign out parameter default value.
            totalRecords = 0;

            try
            {
                DbCommand getCertificationTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPSEARCH_CANDIDATE_ACTIVITY_LOG");

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@CANDIDATE_ID", DbType.Int32, searchCriteria.CandidateID);

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@ACTIVITY_TYPE", DbType.String,
                    (Utility.IsNullOrEmpty(searchCriteria.ActivityType) ?
                    null : searchCriteria.ActivityType.Trim()));

                if (searchCriteria.ActivityBy == 0)
                    HCMDatabase.AddInParameter(getCertificationTitleCommand,
                        "@ACTIVITY_BY", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getCertificationTitleCommand,
                        "@ACTIVITY_BY", DbType.Int32, searchCriteria.ActivityBy);

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                   "@ACTIVITY_DATE_FROM", DbType.DateTime, searchCriteria.ActivityDateFrom);

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                   "@ACTIVITY_DATE_TO", DbType.DateTime, searchCriteria.ActivityDateTo);

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                   "@COMMENTS", DbType.String, searchCriteria.Comments);

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@PAGE_NUM", DbType.String, pageNumber);

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                   "@SORT_EXPRESSION", DbType.String, sortField);

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending ? "A" : "D");

                dataReader = HCMDatabase.ExecuteReader(getCertificationTitleCommand);

                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {

                        if (certificationDetail == null)
                            certificationDetail = new List<CandidateActivityLog>();

                        CandidateActivityLog certificationObj = new CandidateActivityLog();

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_DATE"]))
                        {
                            certificationObj.ActivityDate = Convert.ToDateTime
                                (dataReader["ACTIVITY_DATE"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_BY"]))
                        {
                            certificationObj.ActivityBy = Convert.ToInt32
                                (dataReader["ACTIVITY_BY"].ToString().Trim());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_BY_NAME"]))
                        {
                            certificationObj.ActivityByName = dataReader["ACTIVITY_BY_NAME"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_TYPE"]))
                        {
                            certificationObj.ActivityType = dataReader["ACTIVITY_TYPE"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVITY_TYPE_NAME"]))
                        {
                            certificationObj.ActivityTypeName = dataReader["ACTIVITY_TYPE_NAME"].
                                ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                        {
                            certificationObj.Comments = dataReader["COMMENTS"].
                                ToString().Trim();
                        }

                        // Add to the list.
                        certificationDetail.Add(certificationObj);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return certificationDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of candidate activities for the
        /// given search parameters as a data table for export to excel.
        /// </summary>
        /// <param name="searchCriteria">
        /// A <see cref="CandidateActivityLogSearchCriteria"/> that holds the
        /// search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the pageNumber
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the pageSize
        /// </param>
        /// <param name="sortField">
        /// A<see cref="string"/>that holds the sort Field
        /// </param>
        /// <param name="sortOrder">
        /// A<see cref="SortType"/>that holds the sordOrder like Asc/Dsc 
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total Records
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateActivityLog"/> that holds candidate 
        /// activity log details.
        /// </returns>
        public DataTable GetCandidateActivitiesTable
            (CandidateActivityLogSearchCriteria searchCriteria, int pageNumber,
            int pageSize, string sortField, SortType sortOrder,
            out int totalRecords)
        {
            // Assign out parameter default value.
            totalRecords = 0;

            DbCommand getActivitiesCommand = HCMDatabase
                .GetStoredProcCommand("SPSEARCH_CANDIDATE_ACTIVITY_LOG");

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@CANDIDATE_ID", DbType.Int32, searchCriteria.CandidateID);

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@ACTIVITY_TYPE", DbType.String,
                (Utility.IsNullOrEmpty(searchCriteria.ActivityType) ?
                null : searchCriteria.ActivityType.Trim()));

            if (searchCriteria.ActivityBy == 0)
                HCMDatabase.AddInParameter(getActivitiesCommand,
                    "@ACTIVITY_BY", DbType.Int32, null);
            else
                HCMDatabase.AddInParameter(getActivitiesCommand,
                    "@ACTIVITY_BY", DbType.Int32, searchCriteria.ActivityBy);

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@ACTIVITY_DATE_FROM", DbType.DateTime, searchCriteria.ActivityDateFrom);

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@ACTIVITY_DATE_TO", DbType.DateTime, searchCriteria.ActivityDateTo);

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@COMMENTS", DbType.String, searchCriteria.Comments);

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@PAGE_NUM", DbType.String, pageNumber);

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@PAGE_SIZE", DbType.Int32, pageSize);

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@SORT_EXPRESSION", DbType.String, sortField);

            HCMDatabase.AddInParameter(getActivitiesCommand,
                "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending ? "A" : "D");

            DataSet dataSet = HCMDatabase.ExecuteDataSet(getActivitiesCommand);

            if (dataSet == null || dataSet.Tables == null ||
                dataSet.Tables.Count == 0 || dataSet.Tables[0].Rows.Count == 0)
            {
                return null;
            }
            return dataSet.Tables[0];
        }

        /// <summary>
        /// Method that retrieves the resume detail for the given candidate.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateResumeDetails"/> that holds the candidate
        /// resume detail.
        /// </returns>
        public CandidateResumeDetails GetResumeSource(int candidateID)
        {
            DbCommand resumeDetailCommand = null;
            IDataReader dataReader = null;
            CandidateResumeDetails resumeDetail = null;

            try
            {
                resumeDetailCommand = HCMDatabase.GetStoredProcCommand("SPTSGET_CANDIDATE_RESUME_SOURCE");
                HCMDatabase.AddInParameter(resumeDetailCommand, "@CANDIDATE_ID", DbType.Int32, candidateID);
                dataReader = HCMDatabase.ExecuteReader(resumeDetailCommand);

                if (dataReader.Read())
                {
                    // Instantiate resume detail object.
                    resumeDetail = new CandidateResumeDetails();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        resumeDetail.CandidateId = Convert.ToInt32(dataReader["CANDIDATE_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["RESUME_SOURCE"]))
                        resumeDetail.ResumeSource = (byte[])dataReader["RESUME_SOURCE"];
                    if (!Utility.IsNullOrEmpty(dataReader["MIME_TYPE"]))
                        resumeDetail.MimeType = dataReader["MIME_TYPE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FILE_NAME"]))
                        resumeDetail.FileName = dataReader["FILE_NAME"].ToString();
                }
                return resumeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the all resume detail for the given candidate.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateResumeDetails"/> that holds the candidate
        /// resume detail.
        /// </returns>
        public List<CandidateResumeDetails> GetCandidateReviewResumes(int candidateID)
        {
            DbCommand resumeDetailCommand = null;
            IDataReader dataReader = null;
            CandidateResumeDetails resumeDetail = null;

            List<CandidateResumeDetails> resumeDetailList = null;

            try
            {
                resumeDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_REVIEWRESUMES");
                HCMDatabase.AddInParameter(resumeDetailCommand, "@CANDIDATE_ID", DbType.Int32, candidateID);
                dataReader = HCMDatabase.ExecuteReader(resumeDetailCommand);

                while (dataReader.Read())
                {
                    if (resumeDetailList == null)
                        resumeDetailList = new List<CandidateResumeDetails>();

                    // Instantiate resume detail object.
                    resumeDetail = new CandidateResumeDetails();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        resumeDetail.CandidateId = Convert.ToInt32(dataReader["CANDIDATE_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CAND_RESUME_ID"]))
                        resumeDetail.CandidateResumeId = Convert.ToInt32(dataReader["CAND_RESUME_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["MIME_TYPE"]))
                        resumeDetail.MimeType = dataReader["MIME_TYPE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FILE_NAME"]))
                        resumeDetail.FileName = dataReader["FILE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["PROCESSED_DATE"]))
                        resumeDetail.ProcessedDate = Convert.ToDateTime(dataReader["PROCESSED_DATE"]);
                    if (!Utility.IsNullOrEmpty(dataReader["SCORE"]))
                        resumeDetail.Score = Convert.ToDecimal(dataReader["SCORE"]);
                    if (!Utility.IsNullOrEmpty(dataReader["APPROVED"]))
                        resumeDetail.Approved = Convert.ToInt16(dataReader["APPROVED"]);
                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER"]))
                        resumeDetail.Recruiter = "Uploaded by " + dataReader["RECRUITER"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_ID"]))
                        resumeDetail.Recruiterid = Convert.ToInt32(dataReader["RECRUITER_ID"]);

                    resumeDetailList.Add(resumeDetail);
                }
                return resumeDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// Method that retrieves the candidate type for the
        /// given search parameters.
        /// </summary> 
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds candidateID
        /// </param> 
        public CandidateSearchCriteria GetCandidateTypeDetail(int candidateID)
        {
            DbCommand candidateTypeCommand = null;
            IDataReader dataReader = null;
            CandidateSearchCriteria candidateSearchCriteria = null;
            CandidateInformation candidateInfo = null;
            try
            {
                candidateTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_TYPEDETAILS");
                HCMDatabase.AddInParameter(candidateTypeCommand, "@CANDIDATE_ID", DbType.Int32, candidateID);
                dataReader = HCMDatabase.ExecuteReader(candidateTypeCommand);

                if (dataReader.Read())
                {
                    if (candidateSearchCriteria == null)
                        candidateSearchCriteria = new CandidateSearchCriteria();

                    if (candidateSearchCriteria.candidateInfo == null)
                        candidateSearchCriteria.candidateInfo = new List<CandidateInformation>();

                    // Instantiate resume detail object.
                    candidateInfo = new CandidateInformation();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ROLE"]))
                        candidateInfo.caiType = dataReader["USER_ROLE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["TENANT_ID"]))
                        candidateInfo.tenantID = Convert.ToInt32(dataReader["TENANT_ID"]);

                    candidateSearchCriteria.candidateInfo.Add(candidateInfo);
                }
                CandidateDetail candidateDetail = null;

                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (candidateSearchCriteria.candidateDetail == null)
                        candidateSearchCriteria.candidateDetail = new List<CandidateDetail>();

                    candidateDetail = new CandidateDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        candidateDetail.CandidateID = Convert.ToString(dataReader["USER_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        candidateDetail.FirstName = Convert.ToString(dataReader["FIRST_NAME"]);

                    candidateSearchCriteria.candidateDetail.Add(candidateDetail);
                }

                AttributeDetail attributeDetail = null;
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (candidateSearchCriteria.attributesList == null)
                        candidateSearchCriteria.attributesList = new List<AttributeDetail>();

                    attributeDetail = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                        attributeDetail.AttributeID = Convert.ToString(dataReader["ATTRIBUTE_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                        attributeDetail.AttributeName = Convert.ToString(dataReader["ATTRIBUTE_NAME"]);

                    candidateSearchCriteria.attributesList.Add(attributeDetail);
                }

                return candidateSearchCriteria;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method that retrieves the all resume detail for the given candidate.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateResumeDetails"/> that holds the candidate
        /// resume detail.
        /// </returns>
        public List<CandidateResumeDetails> GetTemporaryResumes(ReviewResumeDetail reviewResumeDetail, out int totalRecords)
        {
            DbCommand resumeDetailCommand = null;
            IDataReader dataReader = null;
            CandidateResumeDetails resumeDetail = null;
            totalRecords = 0;
            List<CandidateResumeDetails> resumeDetailList = null;

            try
            {
                resumeDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_REVIEW_RESUME");
                HCMDatabase.AddInParameter(resumeDetailCommand, "@TENANT_ID", DbType.Int32, reviewResumeDetail.TenantID);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@STATUS", DbType.String, reviewResumeDetail.Status);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@PROFILENAME ", DbType.String, reviewResumeDetail.ProfileName);

                if (Convert.ToInt32(reviewResumeDetail.RecruiterID) > 0)
                {
                    HCMDatabase.AddInParameter(resumeDetailCommand, "@RECRUITERID", DbType.Int32, reviewResumeDetail.RecruiterID);
                }
                if (!Utility.IsNullOrEmpty(reviewResumeDetail.FromDate)
                    && reviewResumeDetail.FromDate.ToShortDateString() != "1/1/0001")
                {
                    HCMDatabase.AddInParameter(resumeDetailCommand, "@FROMDATE", DbType.DateTime, reviewResumeDetail.FromDate);
                }
                if (!Utility.IsNullOrEmpty(reviewResumeDetail.ToDate) &&
                    reviewResumeDetail.ToDate.ToShortDateString() != "1/1/0001")
                {
                    HCMDatabase.AddInParameter(resumeDetailCommand, "@TODATE", DbType.DateTime, reviewResumeDetail.ToDate);
                }
                HCMDatabase.AddInParameter(resumeDetailCommand, "@PAGE_NUM", DbType.String, reviewResumeDetail.PageNumber);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@PAGE_SIZE", DbType.String, reviewResumeDetail.PageSize);

                dataReader = HCMDatabase.ExecuteReader(resumeDetailCommand);

                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        if (resumeDetailList == null)
                            resumeDetailList = new List<CandidateResumeDetails>();

                        // Instantiate resume detail object.
                        resumeDetail = new CandidateResumeDetails();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            resumeDetail.CandidateId = Convert.ToInt32(dataReader["CANDIDATE_ID"]);

                        if (!Utility.IsNullOrEmpty(dataReader["MIME_TYPE"]))
                            resumeDetail.MimeType = dataReader["MIME_TYPE"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["FILE_NAME"]))
                            resumeDetail.FileName = dataReader["FILE_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                            resumeDetail.Recruiter = dataReader["FIRST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                            resumeDetail.Recruiter += " " + dataReader["LAST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                            resumeDetail.EmailID = dataReader["EMAIL_ID"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]))
                            resumeDetail.Active = dataReader["ACTIVE"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETE"]))
                            resumeDetail.Complete = dataReader["COMPLETE"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["DUPLICATE"]))
                            resumeDetail.Duplicate = dataReader["DUPLICATE"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["REASON"]))
                            resumeDetail.Reason = dataReader["REASON"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                            resumeDetail.ProcessedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]); ;

                        resumeDetailList.Add(resumeDetail);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }

                }
                return resumeDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the candidate temporary resume for the given candidate id
        /// </summary>
        /// <param name="candidateID">
        /// A<see cref="int"/>that holds the candidate id 
        /// </param>
        /// <param name="mimeType">
        /// A <see cref="string"/> that holds the mime type as an output 
        /// parameter.
        /// </param>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the filename as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A<see cref="byte"/>that holds the resume data.
        /// </returns>
        public byte[] GetTemporaryResumeContents(int candidateID,
            out string mimeType, out string fileName)
        {
            mimeType = string.Empty;
            fileName = string.Empty;
            DbCommand command = HCMDatabase.GetStoredProcCommand
                ("SPGET_TEMPORARY_RESUME_CONTENTS");
            HCMDatabase.AddInParameter(command,
                "@CANDIDATE_ID", DbType.Int32, candidateID);
            IDataReader reader = HCMDatabase.ExecuteReader(command);
            if (reader != null && reader.Read())
            {
                mimeType = reader["MIME_TYPE"] as string;
                fileName = reader["FILE_NAME"] as string;
                fileName = fileName.Remove(fileName.LastIndexOf("."));
                return reader["RESUME_SOURCE"] as byte[];
            }
            return null;
        }

        /// <summary>
        /// Method that retrieves the resume node and its elements
        /// </summary>
        /// <returns></returns>
        public List<ResumeElementsDetail> GetResumeElements(int tenantId)
        {
            DbCommand resumeDetailCommand = null;
            IDataReader dataReader = null;
            ResumeElementsDetail resumeDetail = null;

            List<ResumeElementsDetail> resumeDetailList = null;

            try
            {
                resumeDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_RESUME_ELEMENTS");
                HCMDatabase.AddInParameter(resumeDetailCommand,
                "@TENANT_ID", DbType.Int32, tenantId);
                dataReader = HCMDatabase.ExecuteReader(resumeDetailCommand);

                while (dataReader.Read())
                {
                    if (resumeDetailList == null)
                        resumeDetailList = new List<ResumeElementsDetail>();

                    // Instantiate resume detail object.
                    resumeDetail = new ResumeElementsDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["COMPONENT_NAME"]))
                        resumeDetail.Node = dataReader["COMPONENT_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_TYPE_NAME"]))
                        resumeDetail.ElementTypeName = dataReader["ELEMENT_TYPE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["DESCRIPTION"]))
                        resumeDetail.Description = dataReader["DESCRIPTION"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_ID"]))
                        resumeDetail.ElementId = Convert.ToInt32(dataReader["ELEMENT_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_NAME"]))
                        resumeDetail.ElementName = dataReader["ELEMENT_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_ASSIGNED"]))
                        resumeDetail.ElementIdAssigned = Convert.ToInt32(dataReader["ELEMENT_ASSIGNED"].ToString());
                    if (!Utility.IsNullOrEmpty(dataReader["EXPR_TYPE"]))
                        resumeDetail.ValidateExprType = dataReader["EXPR_TYPE"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["EXPR_VALUE"]))
                        resumeDetail.ValidateExprValue = Convert.ToInt32(dataReader["EXPR_VALUE"].ToString());
                    if (!Utility.IsNullOrEmpty(dataReader["REASON"]))
                        resumeDetail.ValidateReason = dataReader["REASON"].ToString();

                    resumeDetailList.Add(resumeDetail);
                }
                return resumeDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method to insert the resume element details based on the tenantId
        /// </summary>
        /// <param name="resumeElementDetail">resumeElementDetail</param>
        /// <param name="tenantId">tenantId</param>
        /// <param name="userID">userID</param>
        /// <param name="transaction"></param>

        public void InsertResumeValidationRule(ResumeElementsDetail resumeElementDetail, int tenantId,
            int userID, IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertResumeElement
                = HCMDatabase.GetStoredProcCommand("SPINSERT_RESUME_VALIDATION_RULES");
            // Add input parameter
            HCMDatabase.AddInParameter(insertResumeElement,
                "@TENANT_ID", DbType.Int32, tenantId);
            HCMDatabase.AddInParameter(insertResumeElement,
                "@ELEMENT_ID", DbType.Int32, resumeElementDetail.ElementId);
            HCMDatabase.AddInParameter(insertResumeElement,
                "@EXPR_TYPE", DbType.String, resumeElementDetail.ValidateExprType);
            HCMDatabase.AddInParameter(insertResumeElement,
                "@EXPR_VALUE", DbType.Int32, resumeElementDetail.ValidateExprValue);
            HCMDatabase.AddInParameter(insertResumeElement,
                "@REASON", DbType.String, resumeElementDetail.ValidateReason);
            HCMDatabase.AddInParameter(insertResumeElement,
                "@CREATED_BY", DbType.Int32, userID);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertResumeElement, transaction as DbTransaction);
        }

        /// <summary>
        /// Method to delete the existing element id against the tenant id
        /// </summary>
        /// <param name="tenantId">tenantId</param>
        /// <param name="elementIds">elementIds</param>
        /// <param name="transaction">transaction</param>
        public void DeleteValidationRules(int tenantId, string elementIds, IDbTransaction transaction)
        {
            DbCommand elementDeleteCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_RESUME_VALIDATION_RULES");
            HCMDatabase.AddInParameter(elementDeleteCommand,
                "@TENANT_ID", DbType.Int32, tenantId);
            HCMDatabase.AddInParameter(elementDeleteCommand,
                "@ELEMENT_IDS", DbType.String, elementIds);
            HCMDatabase.ExecuteNonQuery(elementDeleteCommand, transaction as DbTransaction);
        }


        /// <summary>
        /// Method that retrieves the external data detail for the given candidate.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateResumeDetails"/> that holds the candidate external data source
        /// </returns>
        public CandidateResumeDetails GetCandidateExternalDataStore(int candidateID)
        {
            DbCommand resumeDetailCommand = null;
            IDataReader dataReader = null;
            CandidateResumeDetails resumeDetail = null;

            try
            {
                resumeDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_EXTERNAL_DATA_STORE");
                HCMDatabase.AddInParameter(resumeDetailCommand, "@CANDIDATE_ID", DbType.Int32, candidateID);
                dataReader = HCMDatabase.ExecuteReader(resumeDetailCommand);

                if (dataReader.Read())
                {
                    // Instantiate resume external data source object.
                    resumeDetail = new CandidateResumeDetails();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        resumeDetail.FirstName = dataReader["CANDIDATE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_NAME"]))
                        resumeDetail.Recruiter = dataReader["RECRUITER_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETE_REASON"]))
                        resumeDetail.Reason = dataReader["COMPLETE_REASON"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        resumeDetail.ProcessedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLETE"]))
                        resumeDetail.Complete = dataReader["COMPLETE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["DUPLICATE"]))
                        resumeDetail.Duplicate = dataReader["DUPLICATE"].ToString();
                }
                return resumeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public List<CandidateResumeDetails> GetDuplicateResumeCandidate(int tenantID, string firstName,
            string lastName, string emailId, string phoneNo, int pageNo, int pageSize, out int totalRecords)
        {
            DbCommand resumeDetailCommand = null;
            IDataReader dataReader = null;
            CandidateResumeDetails resumeDetail = null;

            try
            {
                totalRecords = 0;
                resumeDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_DUPLICATE_RESUME_CANDIDATE");
                HCMDatabase.AddInParameter(resumeDetailCommand, "@TENANTID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@FIRST_NAME", DbType.String, firstName);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@LAST_NAME", DbType.String, lastName);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@EMAILID", DbType.String, emailId);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@PHONENO", DbType.String, phoneNo);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@PAGE_NUM ", DbType.Int32, pageNo);
                HCMDatabase.AddInParameter(resumeDetailCommand, "@PAGE_SIZE ", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(resumeDetailCommand);

                List<CandidateResumeDetails> candidateResumeDetailList = null;

                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        if (candidateResumeDetailList == null)
                            candidateResumeDetailList = new List<CandidateResumeDetails>();

                        // Instantiate resume external data source object.
                        resumeDetail = new CandidateResumeDetails();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            resumeDetail.CandidateId = Convert.ToInt32(dataReader["CANDIDATE_ID"]);

                        if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                            resumeDetail.FirstName = dataReader["FIRST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                            resumeDetail.LastName = dataReader["LAST_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["UPLOADED_BY"]))
                            resumeDetail.UploadedByName = dataReader["UPLOADED_BY"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["UPLOADED_DATE"]))
                            resumeDetail.UploadedDate = Convert.ToDateTime(dataReader["UPLOADED_DATE"]);

                        candidateResumeDetailList.Add(resumeDetail);
                    }
                    else
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                            totalRecords = Convert.ToInt32(dataReader["TOTAL"]);

                    }
                }
                return candidateResumeDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public List<CandidateResumeDetails> GetCandidateResumeList(int candidateID)
        {
            DbCommand resumeDetailCommand = null;
            IDataReader dataReader = null;
            CandidateResumeDetails resumeDetail = null;

            try
            {
                resumeDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_RESUME_LIST");
                HCMDatabase.AddInParameter(resumeDetailCommand, "@CANDIDATE_ID", DbType.Int32, candidateID);


                dataReader = HCMDatabase.ExecuteReader(resumeDetailCommand);

                List<CandidateResumeDetails> candidateResumeDetailList = null;

                while (dataReader.Read())
                {

                    if (candidateResumeDetailList == null)
                        candidateResumeDetailList = new List<CandidateResumeDetails>();

                    // Instantiate resume external data source object.
                    resumeDetail = new CandidateResumeDetails();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        resumeDetail.CandidateId = Convert.ToInt32(dataReader["CANDIDATE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["RESUME_ID"]))
                        resumeDetail.CandidateResumeId = Convert.ToInt32(dataReader["RESUME_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["RESUME_TITLE"]))
                        resumeDetail.FileName = dataReader["RESUME_TITLE"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["UPLOADED_BY"]))
                        resumeDetail.Recruiter = dataReader["UPLOADED_BY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["UPLOADED_DATE"]))
                        resumeDetail.ProcessedDate = Convert.ToDateTime(dataReader["UPLOADED_DATE"]);

                    candidateResumeDetailList.Add(resumeDetail);
                }
                return candidateResumeDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the email reminder settings
        /// </summary>
        /// <param name="tenantID"></param>
        /// <returns></returns>
        public List<ResumeEmailReminderDetail> GetResumeEmailReminderSettings(int tenantID)
        {
            DbCommand resumeDetailCommand = null;
            IDataReader dataReader = null;
            ResumeEmailReminderDetail resumeEmailReminderDetail = null;
            List<ResumeEmailReminderDetail> resumeEmailReminderDetails = null;

            try
            {
                resumeDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_RESUME_EMAIL_REMINDER_SETTINGS");
                HCMDatabase.AddInParameter(resumeDetailCommand,
                "@TENANT_ID", DbType.Int32, tenantID);
                dataReader = HCMDatabase.ExecuteReader(resumeDetailCommand);

                while (dataReader.Read())
                {
                    if (resumeEmailReminderDetails == null)
                        resumeEmailReminderDetails = new List<ResumeEmailReminderDetail>();

                    // Instantiate resume detail object.
                    resumeEmailReminderDetail = new ResumeEmailReminderDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USR_TYPE"]))
                        resumeEmailReminderDetail.AttributeID = dataReader["USR_TYPE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_OPTION"]))
                        resumeEmailReminderDetail.AttributeType = dataReader["EMAIL_OPTION"].ToString();

                    resumeEmailReminderDetails.Add(resumeEmailReminderDetail);
                }
                return resumeEmailReminderDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the candidate information with the data taken
        /// from parsed HRXML.
        /// </summary>
        /// <param name="transaction">
        /// A <see cref="DbTransaction"/> that holds the transaction object.
        /// </param>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="resumeUploadType">
        /// A <see cref="string"/> that holds the resume upload type.
        /// </param>
        public void UpdateCandidateInformation(CandidateDetail candidateDetail,
            Int64 candidateID, int userID, string resumeUploadType)
        {
            // Create a stored procedure command object.
            DbCommand updateCandidateCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_PARSER_CANDIDATE_DETAIL");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@CANDIDATE_ID", DbType.Int64, candidateID);
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@USER_ID", DbType.Int32, userID);
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@FIRST_NAME", DbType.String, Utility.IsNullOrEmpty(candidateDetail.FirstName) ? string.Empty : candidateDetail.FirstName.Trim());
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@MIDDLE_NAME", DbType.String, Utility.IsNullOrEmpty(candidateDetail.MiddleName) ? string.Empty : candidateDetail.MiddleName.Trim());
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@LAST_NAME", DbType.String, Utility.IsNullOrEmpty(candidateDetail.LastName) ? string.Empty : candidateDetail.LastName.Trim());
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@EMAIL", DbType.String, Utility.IsNullOrEmpty(candidateDetail.EMailID) ? string.Empty : candidateDetail.EMailID.Trim());
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@ADDRESS", DbType.String, Utility.IsNullOrEmpty(candidateDetail.Address) ? string.Empty : candidateDetail.Address.Trim());
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@CELL_PHONE", DbType.String, Utility.IsNullOrEmpty(candidateDetail.Mobile) ? string.Empty : candidateDetail.Mobile.Trim());
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@HOME_PHONE", DbType.String, Utility.IsNullOrEmpty(candidateDetail.Phone) ? string.Empty : candidateDetail.Phone.Trim());
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@FAX", DbType.String, Utility.IsNullOrEmpty(candidateDetail.Fax) ? string.Empty : candidateDetail.Fax.Trim());
            HCMDatabase.AddInParameter(updateCandidateCommand,
                "@UPLOAD_TYPE", DbType.String, resumeUploadType);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCandidateCommand);
        }

        /// <summary>
        /// Method to insert the resume email reminder settings
        /// </summary>
        /// <param name="resumeEmailReminderDetail">resumeEmailReminderDetail</param>
        /// <param name="tenantID">tenantID</param>
        /// <param name="userID">userID</param>
        /// <param name="transaction"></param>
        public void InsertResumeEmailReminderSettings(ResumeEmailReminderDetail resumeEmailReminderDetail,
                int tenantID, int userID, IDbTransaction transaction)
        {
            // Create command object
            DbCommand insertResumeEmailSettings
                = HCMDatabase.GetStoredProcCommand("SPINSERT_RESUME_EMAIL_REMINDER_SETTINGS");
            // Add input parameter
            HCMDatabase.AddInParameter(insertResumeEmailSettings,
                "@TENANT_ID", DbType.Int32, tenantID);
            HCMDatabase.AddInParameter(insertResumeEmailSettings,
                "@USR_TYPE", DbType.String, resumeEmailReminderDetail.UserType);
            HCMDatabase.AddInParameter(insertResumeEmailSettings,
                "@EMAIL_OPTION", DbType.String, resumeEmailReminderDetail.EmailOption);
            HCMDatabase.AddInParameter(insertResumeEmailSettings,
                "@CREATED_BY", DbType.Int32, userID);
            // Execute the command
            HCMDatabase.ExecuteNonQuery(insertResumeEmailSettings, transaction as DbTransaction);
        }

        /// <summary>
        /// Method to delete the email reminder settings against tenantid
        /// </summary>
        /// <param name="tenantID"></param>
        public void DeleteResumeEmailReminderSettings(int tenantID)
        {
            DbCommand emailReminderDeleteCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_RESUME_EMAIL_REMINDER_SETTINGS");
            HCMDatabase.AddInParameter(emailReminderDeleteCommand,
                "@TENANT_ID", DbType.Int32, tenantID);
            HCMDatabase.ExecuteNonQuery(emailReminderDeleteCommand);
        }

        /// <summary>
        /// Method that retrieves the list of incomplete resume reminders.
        /// </summary>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. 'D' represents daily
        /// and 'W' represents weekly.
        /// </param>
        /// <returns>
        /// A list of <see cref="ResumeReminderDetail"/> that holds the 
        /// reminder email and files list.
        /// </returns>
        public List<ResumeReminderDetail> GetIncompleteResumeReminders(string type)
        {
            IDataReader dataReader = null;
            List<ResumeReminderDetail> reminders = null;

            try
            {
                DbCommand getReminders = HCMDatabase
                    .GetStoredProcCommand("SPGET_INCOMPLETE_RESUMES_REMINDER");

                // Add input parameter
                HCMDatabase.AddInParameter(getReminders,
                    "@TYPE", DbType.String, type);

                dataReader = HCMDatabase.ExecuteReader(getReminders);

                while (dataReader.Read())
                {
                    // Instantiate the reminder list.
                    if (reminders == null)
                        reminders = new List<ResumeReminderDetail>();

                    // Create a reminder detail object.
                    ResumeReminderDetail reminder = new ResumeReminderDetail();

                    // Check if both email IDs and file names present.
                    if (Utility.IsNullOrEmpty(dataReader["EMAIL_IDS"]) ||
                        Utility.IsNullOrEmpty(dataReader["FILE_NAMES"]))
                        continue;

                    reminder.EmailIDs = dataReader["EMAIL_IDS"].ToString().Trim();
                    reminder.EmailIDs = reminder.EmailIDs.Trim(new char[] { ',' });

                    reminder.ProfileNames = dataReader["FILE_NAMES"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["RECRUITER_FIRST_NAME"]))
                        reminder.RecruiterFirstName = dataReader["RECRUITER_FIRST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                        reminder.Company = dataReader["COMPANY"].ToString().Trim();

                    // Add to the list.
                    reminders.Add(reminder);
                }

                return reminders;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of duplicate resume reminders.
        /// </summary>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. 'D' represents daily
        /// and 'W' represents weekly.
        /// </param>
        /// <returns>
        /// A list of <see cref="ResumeReminderDetail"/> that holds the 
        /// reminder email and files list.
        /// </returns>
        public List<ResumeReminderDetail> GetDuplicateResumeReminders(string type)
        {
            IDataReader dataReader = null;
            List<ResumeReminderDetail> reminders = null;

            try
            {
                DbCommand getReminders = HCMDatabase
                    .GetStoredProcCommand("SPGET_DUPLICATE_RESUMES_REMINDER");

                // Add input parameter
                HCMDatabase.AddInParameter(getReminders,
                    "@TYPE", DbType.String, type);

                dataReader = HCMDatabase.ExecuteReader(getReminders);

                while (dataReader.Read())
                {
                    // Instantiate the reminder list.
                    if (reminders == null)
                        reminders = new List<ResumeReminderDetail>();

                    // Create a reminder detail object.
                    ResumeReminderDetail reminder = new ResumeReminderDetail();

                    // Check if both email IDs and file names present.
                    if (Utility.IsNullOrEmpty(dataReader["EMAIL_IDS"]) ||
                        Utility.IsNullOrEmpty(dataReader["FILE_NAMES"]))
                        continue;

                    reminder.EmailIDs = dataReader["EMAIL_IDS"].ToString().Trim();
                    reminder.EmailIDs = reminder.EmailIDs.Trim(new char[] { ',' });

                    reminder.ProfileNames = dataReader["FILE_NAMES"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        reminder.AdminFirstName = dataReader["FIRST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                        reminder.Company = dataReader["COMPANY"].ToString().Trim();

                    // Add to the list.
                    reminders.Add(reminder);
                }

                return reminders;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of duplicate resume reminders for recruiters.
        /// </summary>
        /// <param name="type">
        /// A <see cref="string"/> that holds the type. 'D' represents daily
        /// and 'W' represents weekly.
        /// </param>
        /// <returns>
        /// A list of <see cref="ResumeReminderDetail"/> that holds the 
        /// reminder email and files list.
        /// </returns>
        public List<ResumeReminderDetail> GetRecruiterDuplicateResumeReminders(string type)
        {
            IDataReader dataReader = null;
            List<ResumeReminderDetail> reminders = null;

            try
            {
                DbCommand getReminders = HCMDatabase
                    .GetStoredProcCommand("SPGET_RECRUITER_DUPLICATE_RESUMES_REMINDER");

                // Add input parameter
                HCMDatabase.AddInParameter(getReminders,
                    "@TYPE", DbType.String, type);

                dataReader = HCMDatabase.ExecuteReader(getReminders);

                while (dataReader.Read())
                {
                    // Instantiate the reminder list.
                    if (reminders == null)
                        reminders = new List<ResumeReminderDetail>();

                    // Create a reminder detail object.
                    ResumeReminderDetail reminder = new ResumeReminderDetail();

                    // Check if both email IDs and file names present.
                    if (Utility.IsNullOrEmpty(dataReader["EMAIL_IDS"]) ||
                        Utility.IsNullOrEmpty(dataReader["FILE_NAMES"]))
                        continue;

                    reminder.EmailIDs = dataReader["EMAIL_IDS"].ToString().Trim();
                    reminder.EmailIDs = reminder.EmailIDs.Trim(new char[] { ',' });

                    reminder.ProfileNames = dataReader["FILE_NAMES"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        reminder.AdminFirstName = dataReader["FIRST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                        reminder.Company = dataReader["COMPANY"].ToString().Trim();

                    // Add to the list.
                    reminders.Add(reminder);
                }

                return reminders;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate resume versions.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="ResumeVersion"/> that holds the resume versions.
        /// </returns>
        public List<ResumeVersion> GetResumeVersions(int candidateID)
        {
            DbCommand getVersions = null;

            IDataReader dataReader = null;

            List<ResumeVersion> resumeVersions = null;

            try
            {
                // Construct stored procedure command.
                getVersions = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_RESUME_VERSIONS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getVersions, "@CANDIDATE_ID",
                    DbType.Int32, candidateID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getVersions);

                // Read the records till end of the reader.
                while (dataReader.Read())
                {
                    // Instantiate the resume version list.
                    if (resumeVersions == null)
                        resumeVersions = new List<ResumeVersion>();

                    // Create a resume version object.
                    ResumeVersion resumeVersion = new ResumeVersion();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_RESUME_ID"]))
                    {
                        resumeVersion.CandidateResumeID = Convert.ToInt32(dataReader["CAND_RESUME_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["PROFILE_NAME"]))
                    {
                        resumeVersion.ProfileName = dataReader["PROFILE_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["UPLOADED_DATE"]))
                    {
                        resumeVersion.UploadedDate = Convert.ToDateTime(dataReader["UPLOADED_DATE"].ToString());
                    }

                    // Add to resume versions to the list.
                    resumeVersions.Add(resumeVersion);
                }

                return resumeVersions;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate resume detail such as HRXML, 
        /// approved status, etc.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/>that holds the candidate resume ID.
        /// </param>        
        /// <returns>
        /// A <see cref="List<string>"/>that holds the resume id and resume source.
        /// </returns>
        public ResumeDetail GetCandidateResumeDetail(int candidateResumeID)
        {
            DbCommand getResume = null;

            IDataReader dataReader = null;

            ResumeDetail resumeDetail = null;

            try
            {
                // Construct stored procedure command.
                getResume = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_HRXML_BY_RESUME_ID");

                // Add input parameters.
                HCMDatabase.AddInParameter(getResume, "@CAND_RESUME_ID",
                    DbType.Int32, candidateResumeID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getResume);

                // Read the records till end of the reader.
                if (dataReader.Read())
                {
                    // Instantiate the resume detail object.
                    if (resumeDetail == null)
                        resumeDetail = new ResumeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["APPROVED"]))
                    {
                        resumeDetail.Approved = dataReader["APPROVED"].ToString().Trim().ToUpper() == "Y" ? true : false;
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["HR_XML"]))
                    {
                        resumeDetail.HRXML = dataReader["HR_XML"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["RESUME_SOURCE"]))
                    {
                        resumeDetail.ResumeSource = dataReader["RESUME_SOURCE"] as byte[];
                    }
                }

                return resumeDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method updating the temporary candidate basic information
        /// </summary>
        /// <param name="candidateInfo"><see cref="candidateinformation"/></param>
        /// <returns>
        /// if updated it returns true else false
        /// </returns>
        public bool UpdateTemporaryCandidateContactInfo(CandidateInformation candidateInfo)
        {
            DbCommand UpdateTemporaryCandidateCommand = null;

            try
            {
                UpdateTemporaryCandidateCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPATE_CANDIDATE_TEMPORARY_HRXML");

                HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                    "@CANDIDATE_ID", DbType.Int32, candidateInfo.caiID);

                HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                    "@FIRSTNAME", DbType.String, candidateInfo.caiFirstName);

                HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                    "@LASTNAME", DbType.String, candidateInfo.caiLastName);

                if (!Utility.IsNullOrEmpty(candidateInfo.caiMiddleName))
                    HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                        "@MIDDLENAME", DbType.String, candidateInfo.caiMiddleName);

                if (!Utility.IsNullOrEmpty(candidateInfo.caiCell))
                    HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                        "@MOBILENO", DbType.String, candidateInfo.caiCell);

                if (!Utility.IsNullOrEmpty(candidateInfo.caiHomePhone))
                    HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                        "@HOMEPHONENO", DbType.String, candidateInfo.caiHomePhone);

                if (!Utility.IsNullOrEmpty(candidateInfo.caiEmail))
                    HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                        "@EMAILID", DbType.String, candidateInfo.caiEmail);

                if (!Utility.IsNullOrEmpty(candidateInfo.caiCountryName))
                    HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                        "@COUNTRYCODE", DbType.String, candidateInfo.caiCountryName);

                if (!Utility.IsNullOrEmpty(candidateInfo.caiZip))
                    HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                        "@POSTALCODE", DbType.String, candidateInfo.caiZip);

                if (!Utility.IsNullOrEmpty(candidateInfo.caiStateName))
                    HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                        "@STATE", DbType.String, candidateInfo.caiStateName);

                if (!Utility.IsNullOrEmpty(candidateInfo.caiAddress))
                    HCMDatabase.AddInParameter(UpdateTemporaryCandidateCommand,
                        "@STREET", DbType.String, candidateInfo.caiAddress);

                HCMDatabase.ExecuteScalar(UpdateTemporaryCandidateCommand);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        /// <summary>
        /// Method that insert record into parser candidate information table and 
        /// returns the parser candidate ID.
        /// </summary>
        /// <param name="candidateInfoID">
        /// A <see cref="int"/> that holds the candidate information ID.
        /// </param>
        /// <param name="uploadType">
        /// A <see cref="string"/> that holds the upload type.
        /// </param>
        /// <param name="resumeName">
        /// A <see cref="string"/> that holds the resume name.
        /// </param>
        /// <param name="recruiterID">
        /// A <see cref="int"/> that holds the recruiter ID.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the parser candidate ID.
        /// </returns>
        public int InsertParserCandidateInformation(int candidateInfoID, string uploadType,
            string resumeName, int recruiterID)
        {
            // Create a stored procedure command object
            DbCommand insertCandidateCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_CANDIDATE_INFORMATION");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCandidateCommand,
                "@CANDIDATE_REFERENCE_ID", DbType.Int32, candidateInfoID);
            HCMDatabase.AddInParameter(insertCandidateCommand,
                "@UPLOAD_TYPE", DbType.String, uploadType);
            HCMDatabase.AddInParameter(insertCandidateCommand,
                "@RESUME_NAME", DbType.String, resumeName);
            HCMDatabase.AddInParameter(insertCandidateCommand,
                "@RECRUITER_ID", DbType.Int32, recruiterID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(insertCandidateCommand,
                "@PARSER_CANDIDATE_ID", DbType.Int32, 0);

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(insertCandidateCommand);

            if (insertCandidateCommand.Parameters["@PARSER_CANDIDATE_ID"].Value != null)
            {
                // Retrieve and assign candidate ID.
                return int.Parse(insertCandidateCommand.Parameters["@PARSER_CANDIDATE_ID"].Value.ToString());
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns the list of resume rules aganist the tenant id
        /// </summary>
        /// <param name="resumeRule"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public List<ResumeRuleDetail> GetResumeRulesList(ResumeRuleDetail resumeRule, out int totalRecords)
        {
            DbCommand getRulesList = null;

            IDataReader dataReader = null;

            totalRecords = 0;

            List<ResumeRuleDetail> resumeRuleDetailList = null;

            try
            {
                // Construct stored procedure command.
                getRulesList = HCMDatabase.
                    GetStoredProcCommand("SPGET_DUPLICATE_RESUME_RULES");

                // Add input parameters.
                HCMDatabase.AddInParameter(getRulesList, "@TENANT_ID",
                    DbType.Int32, resumeRule.TenantId);

                if (!Utility.IsNullOrEmpty(resumeRule.RuleName))
                {
                    HCMDatabase.AddInParameter(getRulesList, "@RULE_NAME",
                    DbType.String, resumeRule.RuleName);
                }

                if (!Utility.IsNullOrEmpty(resumeRule.RuleReason))
                {
                    HCMDatabase.AddInParameter(getRulesList, "@REASON",
                      DbType.String, resumeRule.RuleReason);
                }
                if (!Utility.IsNullOrEmpty(resumeRule.RuleStatus))
                {
                    HCMDatabase.AddInParameter(getRulesList, "@STATUS",
                      DbType.String, resumeRule.RuleStatus);
                }

                HCMDatabase.AddInParameter(getRulesList, "@PAGE_NUM",
                  DbType.Int32, resumeRule.PageNumber);

                HCMDatabase.AddInParameter(getRulesList, "@PAGE_SIZE",
                DbType.Int32, resumeRule.PageSize);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getRulesList);


                // Read the records till end of the reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the resume version list.
                        if (resumeRuleDetailList == null)
                            resumeRuleDetailList = new List<ResumeRuleDetail>();

                        // Create a resume version object.
                        ResumeRuleDetail resumeRuleDetail = new ResumeRuleDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["RULE_ID"]))
                        {
                            resumeRuleDetail.RuleId = Convert.ToInt32(dataReader["RULE_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RULE_NAME"]))
                        {
                            resumeRuleDetail.RuleName = dataReader["RULE_NAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["REASON"]))
                        {
                            resumeRuleDetail.RuleReason = dataReader["REASON"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]))
                        {
                            resumeRuleDetail.RuleStatus = dataReader["ACTIVE"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CONDITION_COMMENTS"]))
                        {
                            resumeRuleDetail.RuleComments = dataReader["CONDITION_COMMENTS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ACTION"]))
                        {
                            resumeRuleDetail.RuleAction = dataReader["ACTION"].ToString();
                        }
                        // Add to resume versions to the list.
                        resumeRuleDetailList.Add(resumeRuleDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return resumeRuleDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        public int DeleteResumeRule(int ruleId)
        {
            try
            {
                DbCommand DeleteRuleCommand = null;

                DeleteRuleCommand = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_DUPLICATE_RESUME_RULES");

                HCMDatabase.AddInParameter(DeleteRuleCommand,
                    "@RULE_ID", DbType.Int32, ruleId);

                HCMDatabase.ExecuteScalar(DeleteRuleCommand);

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateResumeRuleStatus(int ruleId, string status)
        {
            DbCommand UpdateResumeRuleStatusCommand = null;

            try
            {
                UpdateResumeRuleStatusCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_DUPLICATE_RESUME_RULES_STATUS");

                HCMDatabase.AddInParameter(UpdateResumeRuleStatusCommand, "@RULE_ID", DbType.Int32, ruleId);

                HCMDatabase.AddInParameter(UpdateResumeRuleStatusCommand, "@STATUS", DbType.String, status);

                HCMDatabase.ExecuteScalar(UpdateResumeRuleStatusCommand);
                return 1;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Method that insert record into resume duplication rule table and 
        /// returns the rule ID.
        /// </summary>
        /// <param name="resumeRuleDetail">
        /// A <see cref="resumeRuleDetail"/> that holds the resume rule detail.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// <returns>
        /// A <see cref="int"/> that holds the parser candidate ID.
        /// </returns>
        public int InsertResumeDuplicationRule(ResumeRuleDetail resumeRuleDetail, int tenantID,
            int userID, IDbTransaction transaction)
        {
            DbCommand insertResumeRule = HCMDatabase.
                GetStoredProcCommand("SPINSERT_RESUME_DUPLICATION_RULES");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertResumeRule,
                "@TENANT_ID", DbType.Int32, tenantID);
            HCMDatabase.AddInParameter(insertResumeRule,
                "@RULE_NAME", DbType.String, resumeRuleDetail.RuleName);
            HCMDatabase.AddInParameter(insertResumeRule,
                "@XML_FIELDS", DbType.String, resumeRuleDetail.XmlFields);
            HCMDatabase.AddInParameter(insertResumeRule,
                "@SQL_FIELDS", DbType.String, resumeRuleDetail.SqlFields);
            HCMDatabase.AddInParameter(insertResumeRule,
                "@XML_CONDITION", DbType.String, resumeRuleDetail.XmlCondition);
            HCMDatabase.AddInParameter(insertResumeRule,
                "@CONDITION_COMMENTS", DbType.String, resumeRuleDetail.RuleComments);
            HCMDatabase.AddInParameter(insertResumeRule,
                "@REASON", DbType.String, resumeRuleDetail.RuleReason);
            HCMDatabase.AddInParameter(insertResumeRule,
                "@USER_ID", DbType.Int32, userID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(insertResumeRule,
                "@RULE_ID", DbType.Int32, 0);


            // Execute the query.
            HCMDatabase.ExecuteNonQuery(insertResumeRule, transaction as DbTransaction);

            if (insertResumeRule.Parameters["@RULE_ID"].Value != null)
            {
                // Retrieve and assign rule ID.
                return int.Parse(insertResumeRule.Parameters["@RULE_ID"].Value.ToString());
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Method that insert record into resume duplication rule entries table 
        /// </summary>
        /// <param name="resumeRuleDetail">
        /// A <see cref="resumeRuleDetail"/> that holds the resume rule detail.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="ruleID">
        /// A <see cref="int"/> that holds the rule ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        public void InsertResumeDuplicationRuleEntries(ResumeRuleDetail resumeRuleDetail,
            int ruleID, int userID, IDbTransaction transaction)
        {
            DbCommand insertResumeRuleEntries = HCMDatabase.
                GetStoredProcCommand("SPINSERT_RESUME_DUPLICATION_RULE_ENTRIES");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertResumeRuleEntries,
                "@RULE_ID", DbType.Int32, ruleID);
            HCMDatabase.AddInParameter(insertResumeRuleEntries,
                "@SOURCE_ELEMENT_ID", DbType.Int32, resumeRuleDetail.SourceElementID);
            HCMDatabase.AddInParameter(insertResumeRuleEntries,
                "@EXPR_TYPE", DbType.String, resumeRuleDetail.ExpressionType);
            HCMDatabase.AddInParameter(insertResumeRuleEntries,
                "@DESTINATION_ELEMENT_ID", DbType.Int32, resumeRuleDetail.DestinationElementID);
            HCMDatabase.AddInParameter(insertResumeRuleEntries,
                "@DESTINATION_VALUE", DbType.String, resumeRuleDetail.DestinationValue);
            HCMDatabase.AddInParameter(insertResumeRuleEntries,
                "@LOGICAL_OPERATOR", DbType.String, resumeRuleDetail.LogicalOperaterId);
            HCMDatabase.AddInParameter(insertResumeRuleEntries,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertResumeRuleEntries, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that insert record into resume duplication rule action table 
        /// </summary>
        /// <param name="ruleID">
        /// A <see cref="int"/> that holds the rule ID.
        /// </param>
        /// <param name="action">
        /// A <see cref="string"/> that holds the action.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        public void InsertResumeDuplicationRuleActions(int ruleID, string action,
            int userID, IDbTransaction transaction)
        {
            DbCommand insertResumeRuleAction = HCMDatabase.
                GetStoredProcCommand("SPINSERT_RESUME_DUPLICATION_RULE_ACTIONS");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertResumeRuleAction,
                "@RULE_ID", DbType.Int32, ruleID);
            HCMDatabase.AddInParameter(insertResumeRuleAction,
                "@ACTION", DbType.String, action);
            HCMDatabase.AddInParameter(insertResumeRuleAction,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertResumeRuleAction,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method that updates record in resume duplication 
        /// rule table against its rule id
        /// </summary>
        /// <param name="resumeRuleDetail">
        /// A <see cref="resumeRuleDetail"/> that holds the resume rule detail.
        /// </param>
        /// <param name="ruleID">
        /// A <see cref="int"/> that holds the rule ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.

        public void UpdateResumeDuplicationRule(ResumeRuleDetail resumeRuleDetail, int ruleID,
            int userID, IDbTransaction transaction)
        {
            DbCommand updateResumeRule = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_RESUME_DUPLICATION_RULES");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateResumeRule,
                "@RULE_ID", DbType.Int32, ruleID);
            HCMDatabase.AddInParameter(updateResumeRule,
                "@RULE_NAME", DbType.String, resumeRuleDetail.RuleName);
            HCMDatabase.AddInParameter(updateResumeRule,
                "@XML_FIELDS", DbType.String, resumeRuleDetail.XmlFields);
            HCMDatabase.AddInParameter(updateResumeRule,
                "@SQL_FIELDS", DbType.String, resumeRuleDetail.SqlFields);
            HCMDatabase.AddInParameter(updateResumeRule,
                "@XML_CONDITION", DbType.String, resumeRuleDetail.XmlCondition);
            HCMDatabase.AddInParameter(updateResumeRule,
                "@CONDITION_COMMENTS", DbType.String, resumeRuleDetail.RuleComments);
            HCMDatabase.AddInParameter(updateResumeRule,
                "@REASON", DbType.String, resumeRuleDetail.RuleReason);
            HCMDatabase.AddInParameter(updateResumeRule,
                "@USER_ID", DbType.Int32, userID);

            // Execute the query.
            HCMDatabase.ExecuteNonQuery(updateResumeRule, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that to delete reume duplication rule entries.
        /// </summary>
        /// <param name="ruleID">
        /// A <see cref="int"/> that holds the rule ID.
        /// </param>
        public void DeleteReumeDuplicationRuleEntries(int ruleID)
        {
            // Create a stored procedure command object.
            DbCommand deleteRuleEntriesCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_RESUME_DUPLICATION_RULE_ENTRIES");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRuleEntriesCommand,
                "@RULE_ID", DbType.Int32, ruleID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteRuleEntriesCommand);
        }

        /// <summary>
        /// Method that to delete reume duplication rule actions.
        /// </summary>
        /// <param name="ruleID">
        /// A <see cref="int"/> that holds the rule ID.
        /// </param>
        public void DeleteReumeDuplicationRuleActions(int ruleID)
        {
            // Create a stored procedure command object.
            DbCommand deleteRuleActionsCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_RESUME_DUPLICATION_RULE_ACTIONS");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRuleActionsCommand,
                "@RULE_ID", DbType.Int32, ruleID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteRuleActionsCommand);
        }

        /// <summary>
        /// Method to set the xml node the given element id
        /// </summary>
        /// <param name="elementID">
        /// A<see cref="int"/>that holds the element ID 
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the xml node and sql field.
        /// </returns>
        public List<string> GetRumeRuleXmlNode(int elementID)
        {
            // Declare a data reader object.
            IDataReader dataReader = null;
            DbCommand getXmlNodeCommand = HCMDatabase.
                                GetStoredProcCommand("SPGET_RESUME_RULE_XML_NODE_BY_ELEMENT_ID");
            // Add input parameters.
            HCMDatabase.AddInParameter(getXmlNodeCommand,
               "@ELEMENT_ID", DbType.Int32, elementID);

            // Execute the stored procedure.
            dataReader = HCMDatabase.ExecuteReader(getXmlNodeCommand);
            List<string> oList = null;
            while (dataReader.Read())
            {
                oList = new List<string>();
                if (dataReader["SQL_FIELD"] != null)
                {
                    oList.Add(dataReader["SQL_FIELD"].ToString().Trim());
                }
                if (dataReader["XML_LINEAGE_NODE"] != null)
                {
                    oList.Add(dataReader["XML_LINEAGE_NODE"].ToString().Trim());
                }
            }
            return oList;
        }

        /// <summary>
        /// Represents the method to get the list of resume rules
        /// </summary>
        /// <returns>
        /// A<see cref="ResumeElementsDetail"/>that holds the resume rule element details
        /// </returns>
        public List<ResumeElementsDetail> GetResumeRuleElements()
        {
            DbCommand resumeRuleDetailCommand = null;
            IDataReader dataReader = null;
            ResumeElementsDetail resumeDetail = null;

            List<ResumeElementsDetail> resumeDetailList = null;

            try
            {
                resumeRuleDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_RESUME_RULE_ELEMENTS");

                dataReader = HCMDatabase.ExecuteReader(resumeRuleDetailCommand);

                while (dataReader.Read())
                {
                    if (resumeDetailList == null)
                        resumeDetailList = new List<ResumeElementsDetail>();

                    // Instantiate resume rule elements.
                    resumeDetail = new ResumeElementsDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_ID"]))
                        resumeDetail.ElementId = Convert.ToInt32(dataReader["ELEMENT_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ELEMENT_NAME"]))
                        resumeDetail.ElementName = dataReader["ELEMENT_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["DESCRIPTION"]))
                        resumeDetail.Description = dataReader["DESCRIPTION"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["XML_LINEAGE_NODE"]))
                        resumeDetail.XmlNodeValue = dataReader["XML_LINEAGE_NODE"].ToString();

                    resumeDetailList.Add(resumeDetail);
                }
                return resumeDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will retrieves the rule detail.
        /// </summary>
        /// <param name="ruleID">
        /// A <see cref="int"/> that contains the rule ID.
        /// </param>
        /// <returns>
        /// A <see cref="ResumeRuleDetail"/> that contains the resume rule detail 
        /// </returns>
        public ResumeRuleDetail GetResumeRuleDetail
            (int ruleID)
        {
            IDataReader dataReader = null;

            try
            {
                string actions = string.Empty;
                // Create a stored procedure command object.
                DbCommand getResumeRuleCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_RESUME_RULE_BY_ID");

                // Add input parameters.
                HCMDatabase.AddInParameter(getResumeRuleCommand,
                    "@RULE_ID", DbType.Int32, ruleID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getResumeRuleCommand);

                ResumeRuleDetail resumeRuleDetail = null;

                if (dataReader.Read())
                {
                    // Instantiate the resume rule detail instance
                    if (resumeRuleDetail == null)
                        resumeRuleDetail = new ResumeRuleDetail();

                    // Assign property values to the resume rule detail object.
                    resumeRuleDetail.RuleName = dataReader["RULE_NAME"].ToString().Trim();
                    resumeRuleDetail.RuleReason = dataReader["REASON"].ToString().Trim();
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the rule entries collection.
                    if (resumeRuleDetail.ResumeRuleEntries == null)
                        resumeRuleDetail.ResumeRuleEntries = new List<ResumeRuleDetail>();

                    // Create a new rule entries object. 
                    ResumeRuleDetail resumeRuleEntries = new ResumeRuleDetail();
                    resumeRuleEntries.SourceElementID =
                        Convert.ToInt32(dataReader["SOURCE_ELEMENT_ID"].ToString());
                    resumeRuleEntries.ExpressionType = dataReader["EXPR_TYPE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["DESTINATION_ELEMENT_ID"].ToString()))
                    {
                        resumeRuleEntries.DestinationElementID =
                            Convert.ToInt32(dataReader["DESTINATION_ELEMENT_ID"].ToString());
                    }
                    resumeRuleEntries.DestinationValue = dataReader["DESTINATION_VALUE"].ToString();
                    resumeRuleEntries.LogicalOperaterId = dataReader["LOGICAL_OPERATOR"].ToString();
                    resumeRuleDetail.XmlNode = dataReader["XML_LINEAGE_NODE"].ToString();

                    resumeRuleDetail.ResumeRuleEntries.Add(resumeRuleEntries);

                }
                dataReader.NextResult();

                //Retrieve actions
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["ACTION"].ToString()))
                        resumeRuleDetail.RuleAction = dataReader["ACTION"].ToString();
                }

                return resumeRuleDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the failed email list.
        /// </summary>
        /// <param name="maxAttempt">
        /// A <see cref="int"/> that holds the max attempt. Failed emails with 
        /// less than this limit only will get retrieved.
        /// </param>
        /// <returns>
        /// A list of <see cref="FailedEmailDetail"/> that holds the failed email.
        /// </returns>
        public List<FailedEmailDetail> GetFailedEmails(int maxAttempt)
        {
            IDataReader dataReader = null;
            List<FailedEmailDetail> failedEmails = null;

            try
            {
                DbCommand getReminders = HCMDatabase
                    .GetStoredProcCommand("SPGET_EMAIL_FAILURE_LOG");

                // Add input parameter
                HCMDatabase.AddInParameter(getReminders,
                    "@MAX_ATTEMPT", DbType.Int32, maxAttempt);

                dataReader = HCMDatabase.ExecuteReader(getReminders);

                while (dataReader.Read())
                {
                    // Instantiate the failed email list.
                    if (failedEmails == null)
                        failedEmails = new List<FailedEmailDetail>();

                    // Create a failed email object.
                    FailedEmailDetail failedEmail = new FailedEmailDetail();

                    // Check if both email IDs are empty.
                    if (Utility.IsNullOrEmpty(dataReader["TO_EMAIL_ID"]) &&
                        Utility.IsNullOrEmpty(dataReader["CC_EMAIL_ID"]))
                        continue;

                    if (!Utility.IsNullOrEmpty(dataReader["FAILURE_ID"]))
                        failedEmail.FailureID = Convert.ToInt32(dataReader["FAILURE_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["FROM_EMAIL_ID"]))
                        failedEmail.FromEmailID = dataReader["FROM_EMAIL_ID"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TO_EMAIL_ID"]))
                    {
                        failedEmail.ToEmailID = dataReader["TO_EMAIL_ID"].ToString().Trim();
                        failedEmail.ToEmailID = failedEmail.ToEmailID.TrimEnd(',');
                        failedEmail.ToEmailID = failedEmail.ToEmailID.Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CC_EMAIL_ID"]))
                    {
                        failedEmail.CCEmailID = dataReader["CC_EMAIL_ID"].ToString().Trim();
                        failedEmail.CCEmailID = failedEmail.CCEmailID.TrimEnd(',');
                        failedEmail.CCEmailID = failedEmail.CCEmailID.Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["REPORTED_DATE"]))
                        failedEmail.ReportedDate = Convert.ToDateTime(dataReader["REPORTED_DATE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["FAILURE_MESSAGE"]))
                        failedEmail.FailureMessage = dataReader["FAILURE_MESSAGE"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["MAIL_SUBJECT"]))
                        failedEmail.MailSubject = dataReader["MAIL_SUBJECT"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["MAIL_MESSAGE"]))
                        failedEmail.MailMessage = dataReader["MAIL_MESSAGE"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["IS_HTML"]))
                        failedEmail.IsHtml = dataReader["IS_HTML"].ToString().Trim().ToUpper() == "Y" ? true : false;

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT"]))
                        failedEmail.Attempt = Convert.ToInt32(dataReader["ATTEMPT"].ToString().Trim());

                    // Add to the list.
                    failedEmails.Add(failedEmail);
                }

                return failedEmails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that deletes the failed email log.
        /// </summary>
        /// <param name="failureLogID">
        /// A <see cref="int"/> that holds the failure log ID.
        /// </param>
        public void DeleteFailedEmail(int failureLogID)
        {
            // Create a stored procedure command object.
            DbCommand deleteFailedEmailCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_EMAIL_FAILURE_LOG");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteFailedEmailCommand,
                "@FAILURE_ID", DbType.Int32, failureLogID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteFailedEmailCommand);
        }

        /// <summary>
        /// Method that updates the failed email log attempt.
        /// </summary>
        /// <param name="failureLogID">
        /// A <see cref="int"/> that holds the failure log ID.
        /// </param>
        /// <param name="attempt">
        /// A <see cref="int"/> that holds the attempt.
        /// </param>
        public void UpdateFailedEmailAttempt(int failureLogID, int attempt)
        {
            // Create a stored procedure command object.
            DbCommand updateAttemptCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_EMAIL_FAILURE_LOG_ATTEMPT");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateAttemptCommand,
                "@FAILURE_ID", DbType.Int32, failureLogID);

            HCMDatabase.AddInParameter(updateAttemptCommand,
                "@ATTEMPT", DbType.Int32, attempt);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateAttemptCommand);
        }

        /// <summary>
        /// Method that checks if user exist in db.
        /// </summary>
        /// <param name="candidateInformation">
        /// A <see cref="CandidateInformation"/> that holds the Information about candidate.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenantID.
        /// </param>
        public int CheckIfUserExist(CandidateInformation candidateInformation, int tenantID)
        {
            int userID = 0;

                // Create a stored procedure command object.
                DbCommand checkUserCommand = HCMDatabase.
                    GetStoredProcCommand("SP_CHECK_USERS");

                // Add input parameters.
                HCMDatabase.AddInParameter(checkUserCommand,
                    "@USER_NAME", DbType.String, candidateInformation.UserName);
                HCMDatabase.AddInParameter(checkUserCommand,
                    "@EMAIL", DbType.String, candidateInformation.caiEmail);
                HCMDatabase.AddInParameter(checkUserCommand,
                    "@TENANT_ID", DbType.Int16, tenantID);
                HCMDatabase.AddOutParameter(checkUserCommand,
                   "@USER_ID", DbType.Int16, 0);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(checkUserCommand);

                //GET output parameter value
                string user_ID=HCMDatabase.GetParameterValue
                    (checkUserCommand, "@USER_ID").ToString().Trim();
                userID = user_ID != null ? Convert.ToInt16(user_ID) : 0;
                return userID;
             
        }
    }

}
