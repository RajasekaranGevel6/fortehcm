﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.DL
{
    public class AssessorDLManager : DatabaseConnectionManager
    {
        public List<AssessorAssessmentDetail>
            GetAssessmentDetailsForAssessor(AssessmentSearchCriteria assessmentSearchCriteria,
            int pageNumber, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;

            AssessorAssessmentDetail assessmentDetail = null;

            totalRecords = 0;

            List<AssessorAssessmentDetail> assessmentDetails = null;

            try
            {
                DbCommand getContributorQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEWASSESSOR_ASSESSMENT_DETAILS");

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@INTERVIEW_NAME", DbType.String, string.IsNullOrEmpty(assessmentSearchCriteria.InterviewName) ? null : assessmentSearchCriteria.InterviewName);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@INTERVIEW_DESCRIPTION", DbType.String, string.IsNullOrEmpty(assessmentSearchCriteria.InterviewDescription) ? null : assessmentSearchCriteria.InterviewDescription);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@ASSESSMENT_STATUS", DbType.String, string.IsNullOrEmpty(assessmentSearchCriteria.AssessmentStatus) ? null : assessmentSearchCriteria.AssessmentStatus);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@CLIENT_REQUEST_NUMBER", DbType.String, string.IsNullOrEmpty(assessmentSearchCriteria.PositionProfileID) ? null :
                    assessmentSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@USER_ID", DbType.Int32,
                    string.IsNullOrEmpty(assessmentSearchCriteria.AssessorID.ToString()) ? 0 :
                    assessmentSearchCriteria.AssessorID);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@ORDERBY", DbType.String, assessmentSearchCriteria.SortExpression);

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@ORDERBYDIRECTION", DbType.String,
                    assessmentSearchCriteria.SortDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getContributorQuestionCommand);

                while (dataReader.Read())
                {
                    if (assessmentDetails == null)
                        assessmentDetails = new List<AssessorAssessmentDetail>();

                    assessmentDetail = new AssessorAssessmentDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                        assessmentDetail.CandidateName = dataReader["USRFIRSTNAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["USREMAIL"]))
                        assessmentDetail.CandidateEamil = dataReader["USREMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                        assessmentDetail.CompletedDate = Convert.ToDateTime(dataReader["DATE_COMPLETED"]);

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        assessmentDetail.InterviewName = dataReader["INTERVIEW_TEST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        assessmentDetail.InterviewTestKey = dataReader["INTERVIEW_TEST_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        assessmentDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSMENT_STATUS"]))
                        assessmentDetail.AssessmentStatus = dataReader["ASSESSMENT_STATUS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_STATUS"]))
                        assessmentDetail.TestStatus = dataReader["TEST_STATUS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                        assessmentDetail.InterviewSessionKey = dataReader["INTERVIEW_SESSION_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        assessmentDetail.CandidateInterviewSessionKey = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                        assessmentDetail.AssessorId = int.Parse(dataReader["ASSESSOR_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        assessmentDetail.AttemptID = int.Parse(dataReader["ATTEMPT_ID"].ToString());

                    assessmentDetails.Add(assessmentDetail);
                }
                return assessmentDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public AssessorDetail GetAssessorByUserId(int userId)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getContributorQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_DETAILS_BY_USERID");

                HCMDatabase.AddInParameter(getContributorQuestionCommand,
                    "@USER_ID", DbType.Int32, userId);

                dataReader = HCMDatabase.ExecuteReader(getContributorQuestionCommand);

                AssessorDetail assessorDetail = null;

                // Retreive assessor details.
                if (dataReader.Read())
                {
                    // Instantiate assessor detail object.
                    assessorDetail = new AssessorDetail();

                    assessorDetail.UserID = userId;

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                        assessorDetail.AssessorId = Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL"]))
                        assessorDetail.Skill = dataReader["SKILL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ALTERNATE_EMAILID"]))
                        assessorDetail.AlternateEmailID = dataReader["ALTERNATE_EMAILID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["NON_AVAILABILITY_DATES"]))
                        assessorDetail.NonAvailabilityDates = dataReader["NON_AVAILABILITY_DATES"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["IMAGE"]))
                        assessorDetail.Image = dataReader["IMAGE"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MOBILE"]))
                        assessorDetail.Mobile = dataReader["MOBILE"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["HOMEPHONE"]))
                        assessorDetail.HomePhone = dataReader["HOMEPHONE"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        assessorDetail.FirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        assessorDetail.LastName = dataReader["LAST_NAME"].ToString();
                     
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        assessorDetail.UserEmail = dataReader["EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ADDITIONAL_INFO"]))
                    {
                        assessorDetail.AdditionalInfo = dataReader["ADDITIONAL_INFO"].ToString().Replace("\n", "<br/>");
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["LINKED_IN_PROFILE"]))
                        assessorDetail.LinkedInProfile = dataReader["LINKED_IN_PROFILE"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        assessorDetail.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    // Move to next record set.
                    dataReader.NextResult();

                    // Retreive skill details.
                    while (dataReader.Read())
                    {
                        // Instantiate skills list.
                        if (assessorDetail.Skills == null)
                            assessorDetail.Skills = new List<SkillDetail>();

                        // Create a skill object.
                        SkillDetail skill = new SkillDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_ID"]))
                            skill.CategoryID = Convert.ToInt32(dataReader["CATEGORY_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                            skill.CategoryName = dataReader["CATEGORY_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                            skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                            skill.SkillName = dataReader["SKILL_NAME"].ToString();

                        // Add to the skills list.
                        assessorDetail.Skills.Add(skill);
                    }
                }
                return assessorDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public string SaveAssessorDetails(AssessorDetail assessorDetails, IDbTransaction transaction)
        {
            // Create a stored procedure command object
            DbCommand insertFormCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_ASSESSOR_DETAILS");

            // Add input parameters
            HCMDatabase.AddInParameter(insertFormCommand,
                "@USER_ID", DbType.Int32, assessorDetails.UserID);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@USER_EMAILID", DbType.String, assessorDetails.UserEmail);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@SKILLS", DbType.String, assessorDetails.Skill);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@ALTERNATE_EMAILID", DbType.String, assessorDetails.AlternateEmailID);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@FIRST_NAME", DbType.String, assessorDetails.FirstName);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@LAST_NAME", DbType.String, assessorDetails.LastName);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@IS_ACTIVE", DbType.String, assessorDetails.Active);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@NON_AVAILABILITY_DATES", DbType.String, assessorDetails.NonAvailabilityDates);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@IMAGE", DbType.String, assessorDetails.Image);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@MOBILE", DbType.String, assessorDetails.Mobile);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@HOMEPHONE", DbType.String, assessorDetails.HomePhone);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@ADDITIONAL_INFO", DbType.String, assessorDetails.AdditionalInfo);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@LINKED_IN_PROFILE", DbType.String, assessorDetails.LinkedInProfile);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@CREATED_BY", DbType.Int32, assessorDetails.CreatedBy);

            HCMDatabase.AddOutParameter(insertFormCommand,"@RETVAL", DbType.String, 18);

            // Execute command
            object returnValue = HCMDatabase.ExecuteNonQuery(insertFormCommand, transaction as DbTransaction);

            returnValue = HCMDatabase.GetParameterValue(insertFormCommand, "@RETVAL");

            return returnValue.ToString().Trim();
        }

        public List<AttributeDetail> GetAssessmentTypes()
        {
            IDataReader dataReader = null;

            AttributeDetail attribute;

            List<AttributeDetail> assessmentDetails = new List<AttributeDetail>();

            try
            {
                DbCommand getContributorQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSMENT_STATUSES");

                dataReader = HCMDatabase.ExecuteReader(getContributorQuestionCommand);

                while (dataReader.Read())
                {
                    attribute = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                        attribute.AttributeID = dataReader["ATTRIBUTE_ID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                        attribute.AttributeName = dataReader["ATTRIBUTE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_VALUE"]))
                        attribute.AttributeValue = dataReader["ATTRIBUTE_VALUE"].ToString();

                    assessmentDetails.Add(attribute);
                }
                return assessmentDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of time slot settings.
        /// </summary>
        /// <returns>
        /// A list of <see cref="TimeSlotDetail"/> that holds the time slot details.
        /// </returns>
        public List<TimeSlotDetail> GetTimeSlotSettings()
        {
            IDataReader dataReader = null;
            List<TimeSlotDetail> timeSlots = null;

            try
            {
                DbCommand getContributorQuestionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TIME_SLOT_SETTINGS");

                dataReader = HCMDatabase.ExecuteReader(getContributorQuestionCommand);

                while (dataReader.Read())
                {
                    TimeSlotDetail timeSlot = new TimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        timeSlot.ID = Convert.ToInt32(dataReader["ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["SLOT_NAME"]))
                        timeSlot.Name = dataReader["SLOT_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SLOT_FROM_TO"]))
                        timeSlot.FromTo = dataReader["SLOT_FROM_TO"].ToString();

                    if (timeSlots == null)
                        timeSlots = new List<TimeSlotDetail>();

                    // Add to time slot list.
                    timeSlots.Add(timeSlot);
                }

                return timeSlots;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts an assessor time slot detail.
        /// </summary>
        /// <param name="timeSlot">
        /// A <seealso cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void InsertAssessorTimeSlot(int requestDataeID, AssessorTimeSlotDetail timeSlot,
            int userID, IDbTransaction transaction)
        {
            DbCommand insertAssessorTimeCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_ASSESSOR_TIME_SLOT");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertAssessorTimeCommand,
                "@ASS_DATES_GEN_ID", DbType.Int32, requestDataeID);

            HCMDatabase.AddInParameter(insertAssessorTimeCommand,
                "@TIME_SLOT_ID_FROM", DbType.Int32, timeSlot.TimeSlotIDFrom);

            HCMDatabase.AddInParameter(insertAssessorTimeCommand,
                "@TIME_SLOT_ID_TO", DbType.Int32, timeSlot.TimeSlotIDTo);

            HCMDatabase.AddInParameter(insertAssessorTimeCommand,
                "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddInParameter(insertAssessorTimeCommand,
                "@REQUSEST_STATUS", DbType.String, timeSlot.RequestStatus);

            if (transaction == null)
                HCMDatabase.ExecuteNonQuery(insertAssessorTimeCommand);
            else
                HCMDatabase.ExecuteNonQuery(insertAssessorTimeCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that updates an assessor time slot detail.
        /// </summary>
        /// <param name="timeSlot">
        /// A <seealso cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        public void UpdateAssessorTimeSlot(AssessorTimeSlotDetail timeSlot)
        {
            DbCommand insertCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_ASSESSOR_TIME_SLOT");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCommand,
                "@TIME_SLOT_ID", DbType.Int32, timeSlot.ID);

            HCMDatabase.AddInParameter(insertCommand,
                "@AVAILABILITY_DATE", DbType.DateTime, timeSlot.AvailabilityDate);

            HCMDatabase.AddInParameter(insertCommand,
                "@TIME_SLOT_ID_FROM", DbType.Int32, timeSlot.TimeSlotIDFrom);

            HCMDatabase.AddInParameter(insertCommand,
                "@TIME_SLOT_ID_TO", DbType.Int32, timeSlot.TimeSlotIDTo);

            HCMDatabase.AddInParameter(insertCommand,
                "@SESSION_KEY", DbType.String, timeSlot.SessionKey.Trim());

            HCMDatabase.AddInParameter(insertCommand,
                "@REQUESTED_BY", DbType.Int32, timeSlot.InitiatedBy);

            HCMDatabase.AddInParameter(insertCommand,
                "@SKILL_ID", DbType.Int32, timeSlot.SkillID);

            HCMDatabase.AddInParameter(insertCommand,
                "@COMMENTS", DbType.String, timeSlot.Comments == null ||
                timeSlot.Comments.Trim().Length == 0 ? null :
                timeSlot.Comments.Trim());

            HCMDatabase.ExecuteNonQuery(insertCommand);
        }

        /// <summary>
        /// Method that retrieves the list of assessor time slots for the
        /// given assessor ID and availability date.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="availabilityDate">
        /// A <see cref="DateTime"/> that holds the availability date.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor time slot details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetAssessorTimeSlots
            (int assessorID, DateTime availabilityDate)
        {
            IDataReader dataReader = null;
            List<AssessorTimeSlotDetail> timeSlots = null;

            try
            {
                DbCommand getTimeSlots =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_TIME_SLOTS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@AVAILABILITY_DATE", DbType.DateTime, availabilityDate);  

                dataReader = HCMDatabase.ExecuteReader(getTimeSlots);

                while (dataReader.Read())
                {
                    AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        timeSlot.ID = Convert.ToInt32(dataReader["ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                        timeSlot.TimeSlotIDFrom = Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                        timeSlot.TimeSlotIDTo = Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM_NAME"]) &&
                       !Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO_NAME"]))
                    {
                        timeSlot.TimeSlot = dataReader["TIME_SLOT_ID_FROM_NAME"].ToString().Trim() + 
                            " - " + dataReader["TIME_SLOT_ID_TO_NAME"].ToString().Trim();                    
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                        timeSlot.SessionKey = dataReader["SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_DESC"]))
                        timeSlot.SessionDesc = dataReader["SESSION_DESC"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_BY"]))
                        timeSlot.InitiatedBy = Convert.ToInt32(dataReader["REQUESTED_BY"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_BY_NAME"]))
                        timeSlot.InitiatedByName = dataReader["REQUESTED_BY_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                        timeSlot.SkillID = Convert.ToInt32(dataReader["SKILL_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                        timeSlot.Skill = dataReader["SKILL_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                        timeSlot.Comments = dataReader["COMMENTS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["INITIATED_DATE"]))
                        timeSlot.InitiatedDate = Convert.ToDateTime(dataReader["INITIATED_DATE"].ToString().Trim());

                    if (timeSlots == null)
                        timeSlots = new List<AssessorTimeSlotDetail>();

                    // Add to time slot list.
                    timeSlots.Add(timeSlot);
                }

                return timeSlots;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that deletes an assessor available time slot.
        /// </summary>
        /// <param name="requestDateGenID">
        /// A <see cref="int"/> that holds the requested date gen id.
        /// </param>
        /// <param name="timeSlotids">
        /// A <see cref="string"/> that holds the requested date times ids.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void DeleteAssessorTimeSlot(int requestDateGenID, 
            string timeSlotids, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand deleteCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ASSESSOR_TIME_SLOT");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteCommand,
                "@ASS_DATES_GEN_ID", DbType.Int32, requestDateGenID);

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteCommand,
                "@TIME_SLOTS_IDS", DbType.String, timeSlotids);

            // Execute the command.
            HCMDatabase.ExecuteNonQuery(deleteCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the assessor time slot setting for the given 
        /// time slot ID.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="availabilityDate">
        /// A <see cref="DateTime"/> that holds the availability date.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor time slot details.
        /// </returns>
        public AssessorTimeSlotDetail GetAssessorTimeSlot(int timeSlotID)
        {
            IDataReader dataReader = null;
            AssessorTimeSlotDetail timeSlot = null;

            try
            {
                DbCommand getTimeSlots =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_TIME_SLOT_BY_ID");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@REQUEST_DATE_ID", DbType.Int32, timeSlotID);

                dataReader = HCMDatabase.ExecuteReader(getTimeSlots);

                if (dataReader.Read())
                {
                    // Instaltiate the time slot object.
                    timeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        timeSlot.ID = Convert.ToInt32(dataReader["ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["AVAILABILITY_DATE"]))
                        timeSlot.AvailabilityDate = Convert.ToDateTime(dataReader["AVAILABILITY_DATE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_BY"]))
                        timeSlot.InitiatedByName = dataReader["REQUESTED_BY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["ANY_TIME"]))
                        timeSlot.IsAvailableFullTime = dataReader["ANY_TIME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        timeSlot.FirstName = dataReader["FIRST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        timeSlot.Email = dataReader["EMAIL"].ToString().Trim();

                }


                dataReader.NextResult();
                List<AssessorTimeSlotDetail> assessorTimeSlots = null;
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(assessorTimeSlots))
                        assessorTimeSlots = new List<AssessorTimeSlotDetail>();

                    AssessorTimeSlotDetail assessorTimeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        assessorTimeSlot.ID = Convert.ToInt32(dataReader["ID"].ToString().Trim());

                    
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                        assessorTimeSlot.TimeSlotIDFrom = Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                        assessorTimeSlot.TimeSlotIDTo = Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_FROM"]))
                        assessorTimeSlot.TimeSlotTextFrom = dataReader["TIME_SLOT_TEXT_FROM"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_TO"]))
                        assessorTimeSlot.TimeSlotTextTo = dataReader["TIME_SLOT_TEXT_TO"].ToString().Trim();

                    assessorTimeSlots.Add(assessorTimeSlot);
                }

                if (!Utility.IsNullOrEmpty(assessorTimeSlots))
                    timeSlot.assessorTimeSlotDetails = assessorTimeSlots;

                return timeSlot;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the assessor time slot setting for the given 
        /// time slot ID.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="availabilityDate">
        /// A <see cref="DateTime"/> that holds the availability date.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor time slot details.
        /// </returns>
        public AssessorTimeSlotDetail GetAssessorTimeSlot(int assessorId,DateTime selectedDate)
        {
            IDataReader dataReader = null;
            AssessorTimeSlotDetail timeSlot = null;

            try
            {
                DbCommand getTimeSlots =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_TIME_SLOTS_BY_REQUESTDATE");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@ASSESSOR_ID", DbType.Int32, assessorId);

                HCMDatabase.AddInParameter(getTimeSlots,
                    "@REQUEST_DATE", DbType.DateTime, selectedDate);

                dataReader = HCMDatabase.ExecuteReader(getTimeSlots);

                if (dataReader.Read())
                {
                    // Instaltiate the time slot object.
                    timeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        timeSlot.ID = Convert.ToInt32(dataReader["ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["AVAILABILITY_DATE"]))
                        timeSlot.AvailabilityDate = Convert.ToDateTime(dataReader["AVAILABILITY_DATE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_BY"]))
                        timeSlot.InitiatedByName = dataReader["REQUESTED_BY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["ANY_TIME"]))
                        timeSlot.IsAvailableFullTime = dataReader["ANY_TIME"].ToString().Trim();

                }

                dataReader.NextResult();
                List<AssessorTimeSlotDetail> assessorTimeSlots = null;
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(assessorTimeSlots))
                        assessorTimeSlots = new List<AssessorTimeSlotDetail>();

                    AssessorTimeSlotDetail assessorTimeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        assessorTimeSlot.ID = Convert.ToInt32(dataReader["ID"].ToString().Trim());


                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                        assessorTimeSlot.TimeSlotIDFrom = Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                        assessorTimeSlot.TimeSlotIDTo = Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_STATUS"]))
                        assessorTimeSlot.RequestStatus = dataReader["REQUESTED_STATUS"].ToString();

                    assessorTimeSlots.Add(assessorTimeSlot);
                }

                if (!Utility.IsNullOrEmpty(assessorTimeSlots))
                    timeSlot.assessorTimeSlotDetails = assessorTimeSlots;

                return timeSlot;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Method that retrieves the list of assessor slot dates occupied with schedule
		/// for the given month and year.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="month">
        /// A <see cref="int"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <param name="includePrevNextMonth">
        /// A <see cref="bool"/> that holds the status whether to include 
        /// previous and next month dates. This helps in highlighting the
        /// other month dates in the calender display
        /// </param>
        /// <returns>
        /// A list of <see cref="DateTime"/> that holds the slot dates.
        /// </returns>
        public List<DateTime> GetAssessorTimeSlotDates(int assessorID, int month,
            int year, bool includePrevNextMonth)
        {
            IDataReader dataReader = null;
            List<DateTime> slotDates = null;

            try
            {
                DbCommand getSlotsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_MONTH_TIME_SLOT_DATES");

                // Add input parameters.
                HCMDatabase.AddInParameter(getSlotsCommand, "@ASSESSOR_ID", DbType.Int32, assessorID);
                HCMDatabase.AddInParameter(getSlotsCommand, "@MONTH", DbType.Int32, month);
                HCMDatabase.AddInParameter(getSlotsCommand, "@YEAR", DbType.Int32, year);

                if (includePrevNextMonth)
                    HCMDatabase.AddInParameter(getSlotsCommand, "@INCLUDE_PREV_NEXT", DbType.String, 'Y');
                else
                    HCMDatabase.AddInParameter(getSlotsCommand, "@YEAR", DbType.String, 'N');

                dataReader = HCMDatabase.ExecuteReader(getSlotsCommand);

                while (dataReader.Read())
                {
                    // Instantiate slot dates list.
                    if (slotDates == null)
                        slotDates = new List<DateTime>();

                    if (!Utility.IsNullOrEmpty(dataReader["AVAILABILITY_DATE"]))
                        slotDates.Add(Convert.ToDateTime(dataReader["AVAILABILITY_DATE"].ToString()));
                }

                return slotDates;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves assesor for the given skills 
        /// </summary>
        /// <param name="skill">
        /// A <see cref="string"/> that holds the skill.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the positionProfileID.
        /// </param>
        /// <returns>
        /// A <see cref="DataTatable"/> that holds the summary data.
        /// </returns>
        public DataTable GetAssessorListBySkill(int tenantID, string skill,
            int positionProfileID)
        {
            DataSet assessorDetails = new DataSet();
            DbCommand skillCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_ASSESSOR_RECOMMEND_BY_SKILLS");

            HCMDatabase.AddInParameter(skillCommand,
                "@TENANT_ID", DbType.Int32, tenantID);

            HCMDatabase.AddInParameter(skillCommand,
                "@SKILL", DbType.String, skill);

            HCMDatabase.AddInParameter(skillCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

            // Execute the stored procedure.
            HCMDatabase.LoadDataSet(skillCommand,
                assessorDetails, "Assessor");
            return assessorDetails.Tables[0];
        }

        /// <summary>
        /// Method that retrieves list of assesor for the given position profile id
        /// (Display only assessors that have interviewed for this client in the past) 
        /// </summary>
        /// <param name="positionProfileId">
        /// A <see cref="string"/> that holds the Position Profile Id.
        /// </param>
        /// <returns>
        /// A <see cref="DataTatable"/> that holds the summary data.
        /// </returns>
        public DataTable GetAssessorListByPositionProfile(int positionProfileId)
        {
            DataSet assessorDetails = new DataSet();
            DbCommand positionProfileCommand = HCMDatabase.GetStoredProcCommand
                ("SPGET_ASSESSOR_BY_POSITION_PROFILE_AND_CLIENT_INVOLVED");

            HCMDatabase.AddInParameter(positionProfileCommand,
                "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);

            // Execute the stored procedure.
            HCMDatabase.LoadDataSet(positionProfileCommand,
                assessorDetails, "AssessorProfile");
            return assessorDetails.Tables[0];
        }

        /// <summary>
        /// Method that retrieves the list of time slot requests for the given 
        /// search parameters.
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="AssessorTimeSlotSearchCriteria"/> that holds the 
        /// search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetTimeSlotRequest
            (AssessorTimeSlotSearchCriteria criteria, int pageNumber, 
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<AssessorTimeSlotDetail> timeSlots = null;

            AssessorTimeSlotDetail timeSlot = null;

            try
            {
                DbCommand searchCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ASSESSOR_AVILABLE_DATES");

                HCMDatabase.AddInParameter(searchCommand,
                    "@ASSESSOR_ID", DbType.Int32, criteria.AssessorID);

                HCMDatabase.AddInParameter(searchCommand,
                    "@REQUEST_STATUS", DbType.String, Utility.IsNullOrEmpty(criteria.RequestStatus) ? string.Empty : criteria.RequestStatus.Trim());

                if (criteria.InitiatedBy == 0)
                    HCMDatabase.AddInParameter(searchCommand, "@INITIATED_BY", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(searchCommand, "@INITIATED_BY", DbType.Int32, criteria.InitiatedBy);

                if (criteria.InitiatedDate == DateTime.MinValue)
                    HCMDatabase.AddInParameter(searchCommand, "@INITIATED_DATE", DbType.DateTime, null);
                else
                    HCMDatabase.AddInParameter(searchCommand, "@INITIATED_DATE", DbType.DateTime, criteria.InitiatedDate);

                if (criteria.RequestedDate == DateTime.MinValue)
                    HCMDatabase.AddInParameter(searchCommand, "@REQUESTED_DATE", DbType.DateTime, null);
                else
                    HCMDatabase.AddInParameter(searchCommand, "@REQUESTED_DATE", DbType.DateTime, criteria.RequestedDate);

                HCMDatabase.AddInParameter(searchCommand,
                    "@PAGE_NUM", DbType.String, pageNumber);

                HCMDatabase.AddInParameter(searchCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY_DIRECTION", DbType.String, sordOrder == SortType.Ascending ? "A" : "D");

                dataReader = HCMDatabase.ExecuteReader(searchCommand);

                while (dataReader.Read())
                {
                    if (timeSlots == null)
                        timeSlots = new List<AssessorTimeSlotDetail>();

                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        timeSlot = new AssessorTimeSlotDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                            timeSlot.ID = int.Parse(dataReader["ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_DATE"]))
                            timeSlot.AvailabilityDate = Convert.ToDateTime(dataReader["REQUESTED_DATE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_BY"]))
                            timeSlot.InitiatedByName = dataReader["INITIATED_BY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_DATE"]))
                            timeSlot.InitiatedDate = Convert.ToDateTime(dataReader["INITIATED_DATE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["REQUEST_STATUS"]))
                            timeSlot.RequestStatus = dataReader["REQUEST_STATUS"].ToString().Trim();

                        timeSlots.Add(timeSlot);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return timeSlots;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of time slot requests made by a 
		/// scheduler/recruiter.
        /// </summary>
        /// <param name="criteria">
        /// A <see cref="AssessorTimeSlotSearchCriteria"/> that holds the 
        /// search criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// assessor details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetSchedulerTimeSlotRequest
            (AssessorTimeSlotSearchCriteria criteria, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<AssessorTimeSlotDetail> timeSlots = null;

            AssessorTimeSlotDetail timeSlot = null;

            try
            {
                DbCommand searchCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_SCHEDULER_TIME_SLOT_REQUEST");

                HCMDatabase.AddInParameter(searchCommand,
                    "@SCHEDULER_ID", DbType.Int32, criteria.SchedulerID);

                HCMDatabase.AddInParameter(searchCommand,
                   "@REQUEST_STATUS", DbType.String, Utility.IsNullOrEmpty(criteria.RequestStatus) ? string.Empty : criteria.RequestStatus.Trim());

                if (criteria.RequestedDate == DateTime.MinValue)
                    HCMDatabase.AddInParameter(searchCommand, "@REQUESTED_DATE", DbType.DateTime, null);
                else
                    HCMDatabase.AddInParameter(searchCommand, "@REQUESTED_DATE", DbType.DateTime, criteria.RequestedDate);

                if (criteria.InitiatedDate == DateTime.MinValue)
                    HCMDatabase.AddInParameter(searchCommand, "@INITIATED_DATE", DbType.DateTime, null);
                else
                    HCMDatabase.AddInParameter(searchCommand, "@INITIATED_DATE", DbType.DateTime, criteria.InitiatedDate);

                HCMDatabase.AddInParameter(searchCommand,
                    "@SESSION_KEY", DbType.String, Utility.IsNullOrEmpty(criteria.SessionKey) ? null : criteria.SessionKey.Trim());

                if (criteria.AssessorID == 0)
                    HCMDatabase.AddInParameter(searchCommand, "@ASSESSOR_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(searchCommand, "@ASSESSOR_ID", DbType.Int32, criteria.AssessorID);
                
                HCMDatabase.AddInParameter(searchCommand,
                    "@SKILL", DbType.String, Utility.IsNullOrEmpty(criteria.Skill) ? null : criteria.Skill.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@COMMENTS", DbType.String, Utility.IsNullOrEmpty(criteria.Comments) ? null : criteria.Comments.Trim());

                HCMDatabase.AddInParameter(searchCommand,
                    "@PAGE_NUM", DbType.String, pageNumber);

                HCMDatabase.AddInParameter(searchCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY_DIRECTION", DbType.String, sordOrder == SortType.Ascending ? "A" : "D");

                dataReader = HCMDatabase.ExecuteReader(searchCommand);

                while (dataReader.Read())
                {
                    if (timeSlots == null)
                        timeSlots = new List<AssessorTimeSlotDetail>();

                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        timeSlot = new AssessorTimeSlotDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                            timeSlot.ID = int.Parse(dataReader["ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_DATE"]))
                            timeSlot.AvailabilityDate = Convert.ToDateTime(dataReader["REQUESTED_DATE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                            timeSlot.TimeSlotIDFrom = Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                            timeSlot.TimeSlotIDTo = Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM_NAME"]) &&
                           !Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO_NAME"]))
                        {
                            timeSlot.TimeSlot = dataReader["TIME_SLOT_ID_FROM_NAME"].ToString().Trim() +
                                " - " + dataReader["TIME_SLOT_ID_TO_NAME"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                            timeSlot.SessionKey = dataReader["SESSION_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_DESC"]))
                            timeSlot.SessionDesc = dataReader["SESSION_DESC"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                            timeSlot.AssessorID = Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]))
                            timeSlot.Assessor = dataReader["ASSESSOR_NAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["INITIATED_DATE"]))
                            timeSlot.InitiatedDate = Convert.ToDateTime(dataReader["INITIATED_DATE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                            timeSlot.SkillID = Convert.ToInt32(dataReader["SKILL_ID"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL"]))
                            timeSlot.Skill = dataReader["SKILL"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                            timeSlot.Comments = dataReader["COMMENTS"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["REQUEST_STATUS"]))
                            timeSlot.RequestStatus = dataReader["REQUEST_STATUS"].ToString().Trim();

                        timeSlots.Add(timeSlot);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return timeSlots;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the assessor availability status.
        /// </summary>
        /// <param name="requestDateID">
        /// A <seealso cref="int"/> that holds the time slot ID.
        /// </param>
        /// <param name="requestStatus">
        /// A <see cref="string"/> that holds the request status. This can be
        /// either 'A' (accepted) or 'R' (rejected').
        /// </param>
        /// <param name="isAvailableFullTime">
        /// A <seealso cref="string"/> that holds the available status.
        /// </param>
        /// <param name="userID">
        /// A <seealso cref="int"/> that holds the user id.
        /// </param>
        public void UpdateAssessorAvailabilityDates(AssessorTimeSlotDetail slotDetail,
            IDbTransaction transaction)
        {
            DbCommand updateRequestCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_ASSESSOR_AVAILABILITY_DATES");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateRequestCommand,
                "@ASS_DATES_GEN_ID", DbType.Int32, slotDetail.RequestDateGenID);

            HCMDatabase.AddInParameter(updateRequestCommand,
                "@REQUEST_STATUS", DbType.String, slotDetail.RequestStatus);

            HCMDatabase.AddInParameter(updateRequestCommand,
                "@IS_AVAILABLE_FULL_TIME", DbType.String, slotDetail.IsAvailableFullTime);

            HCMDatabase.AddInParameter(updateRequestCommand,
                "@USER_ID", DbType.Int32, slotDetail.InitiatedBy);

            if (transaction == null)
                HCMDatabase.ExecuteNonQuery(updateRequestCommand);
            else
                HCMDatabase.ExecuteNonQuery(updateRequestCommand, 
                    transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retrieves the list of skills for the given search
        /// parameters.
        /// </summary>
        /// <param name="skillName">
        /// A <see cref="string"/> that holds the skill name.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetSkills(string skillName, int pageNumber,
            int pageSize, string sortField, SortType sordOrder, out int totalRecords)
        {
            List<SkillDetail> skills = null;

            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getSkillCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_SKILL");

                HCMDatabase.AddInParameter(getSkillCommand, "@SKILL_NAME", DbType.String,
                    Utility.IsNullOrEmpty(skillName) ? null : skillName.Trim());

                HCMDatabase.AddInParameter(getSkillCommand, "@PAGE_NUM",
                    DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getSkillCommand, "@PAGE_SIZE",
                    DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getSkillCommand, "@ORDER_BY",
                    DbType.String, sortField);

                HCMDatabase.AddInParameter(getSkillCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getSkillCommand);

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate skill list.
                        if (skills == null)
                            skills = new List<SkillDetail>();

                        // Create a skill object.
                        SkillDetail skill = new SkillDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                        {
                            skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                        {
                            skill.SkillName = dataReader["SKILL_NAME"].ToString();
                        }

                        // Add to the list.
                        skills.Add(skill);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return skills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the list of processed time slots for the 
        /// given parameters.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="sessionKey">
        /// A <see cref="string"/> that holds the session key.
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="string"/> that holds the skill ID.
        /// </param>
        /// <param name="requestStatus">
        /// A <see cref="string"/> that holds the request status.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the 
        /// processed time slot list.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetProcesssedTimeSlots
            (int assessorID, string sessionKey, int skillID, string requestStatus)
        {
            IDataReader dataReader = null;
            List<AssessorTimeSlotDetail> timeSlots = null;

            try
            {
                DbCommand getTimeSlots =
                    HCMDatabase.GetStoredProcCommand("SPGET_PROCESSED_TIME_SLOTS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@SESSION_KEY", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getTimeSlots,
                   "@SKILL_ID", DbType.Int32, skillID);
                HCMDatabase.AddInParameter(getTimeSlots,
                   "@REQUEST_STATUS", DbType.String, requestStatus);

                dataReader = HCMDatabase.ExecuteReader(getTimeSlots);

                while (dataReader.Read())
                {
                    AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                        timeSlot.ID = Convert.ToInt32(dataReader["ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["AVAILABILITY_DATE"]))
                        timeSlot.AvailabilityDate = Convert.ToDateTime(dataReader["AVAILABILITY_DATE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                        timeSlot.TimeSlotIDFrom = Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                        timeSlot.TimeSlotIDTo = Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM_NAME"]) &&
                       !Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO_NAME"]))
                    {
                        timeSlot.TimeSlot = dataReader["TIME_SLOT_ID_FROM_NAME"].ToString().Trim() +
                            " - " + dataReader["TIME_SLOT_ID_TO_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                        timeSlot.SessionKey = dataReader["SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_DESC"]))
                        timeSlot.SessionDesc = dataReader["SESSION_DESC"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_BY"]))
                        timeSlot.InitiatedBy = Convert.ToInt32(dataReader["REQUESTED_BY"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_BY_NAME"]))
                        timeSlot.InitiatedByName = dataReader["REQUESTED_BY_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                        timeSlot.SkillID = Convert.ToInt32(dataReader["SKILL_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                        timeSlot.Skill = dataReader["SKILL_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                        timeSlot.Comments = dataReader["COMMENTS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["INITIATED_DATE"]))
                        timeSlot.InitiatedDate = Convert.ToDateTime(dataReader["INITIATED_DATE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["REQUEST_STATUS"]))
                        timeSlot.RequestStatus = dataReader["REQUEST_STATUS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        timeSlot.CandidateSessionKey = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();

                    if (Utility.IsNullOrEmpty(timeSlot.CandidateSessionKey))
                        timeSlot.IsAvailable = true;
                    else
                        timeSlot.IsAvailable = false;

                    if (timeSlots == null)
                        timeSlots = new List<AssessorTimeSlotDetail>();

                    // Add to time slot list.
                    timeSlots.Add(timeSlot);
                }

                return timeSlots;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that checks if accepted time slots available for the given
        /// parameters.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="sessionKey">
        /// A <see cref="string"/> that holds the session key.
        /// </param>
        /// <param name="skillID"> 
        /// A <see cref="string"/> that holds the skill ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the availability status. If 
        /// available returns true and otherwise false.
        /// </returns>
        public bool IsAcceptedTimeSlotsAvailable(int assessorID, 
            string sessionKey, int skillID)
        {
            IDataReader dataReader = null;
            int total = 0;

            try
            {
                DbCommand getTimeSlots =
                    HCMDatabase.GetStoredProcCommand("SPGET_ACCEPTED_TIME_SLOTS_COUNT");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);
                HCMDatabase.AddInParameter(getTimeSlots,
                    "@SESSION_KEY", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getTimeSlots,
                   "@SKILL_ID", DbType.Int32, skillID);

                dataReader = HCMDatabase.ExecuteReader(getTimeSlots);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        total = Convert.ToInt32(dataReader["TOTAL"].ToString().Trim());
                }

                if (total == 0)
                    return false;
                else
                    return true;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of assessments done by the given
        /// assessor.
        /// </summary>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorAssessmentDetail"/> that holds the 
        /// assessment details.
        /// </returns>
        /// <remarks>
        /// This will return only the completed and in-progress assessments.
        /// </remarks>
        public List<AssessorAssessmentDetail> GetAssessorAssessments
            (int assessorID, int pageNumber, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<AssessorAssessmentDetail> assessmentDetails = null;

            try
            {
                DbCommand getAssessmentsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_PROFILE_ASSESSMENTS");

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getAssessmentsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getAssessmentsCommand);

                while (dataReader.Read())
                {
                    // Instantiate assessment details list.
                    if (assessmentDetails == null)
                        assessmentDetails = new List<AssessorAssessmentDetail>();

                    // Create assessment detail object.
                    AssessorAssessmentDetail assessmentDetail = new AssessorAssessmentDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                        assessmentDetail.CandidateName = dataReader["USRFIRSTNAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["USREMAIL"]))
                        assessmentDetail.CandidateEamil = dataReader["USREMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                        assessmentDetail.CompletedDate = Convert.ToDateTime(dataReader["DATE_COMPLETED"]);

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                        assessmentDetail.InterviewName = dataReader["INTERVIEW_TEST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                        assessmentDetail.InterviewTestKey = dataReader["INTERVIEW_TEST_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        assessmentDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSMENT_STATUS"]))
                        assessmentDetail.AssessmentStatus = dataReader["ASSESSMENT_STATUS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_STATUS"]))
                        assessmentDetail.TestStatus = dataReader["TEST_STATUS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                        assessmentDetail.InterviewSessionKey = dataReader["INTERVIEW_SESSION_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                        assessmentDetail.CandidateInterviewSessionKey = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                        assessmentDetail.AssessorId = int.Parse(dataReader["ASSESSOR_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        assessmentDetail.AttemptID = int.Parse(dataReader["ATTEMPT_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        assessmentDetail.ClientName = dataReader["CLIENT_NAME"].ToString();

                    assessmentDetails.Add(assessmentDetail);
                }
                return assessmentDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of assessors and corporate users for
        /// the given search criteria.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="name">
        /// A <see cref="string"/> that holds the name.
        /// </param>
        /// <param name="email">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="includeUsers">
        /// A <see cref="bool"/> that holds the status whether to include users
        /// or not. True indicates include users and false do not.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortType"/> that holds the sort order.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorDetail"/> that holds the assessor details.
        /// </returns>
        public List<AssessorDetail>GetAssessorsAndUsers(int tenantID,
            string name, string email, bool includeUsers, string sortField, 
            SortType sordOrder, int pageNumber, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            List<AssessorDetail> assessorDetails = null;

            try
            {
                DbCommand getAssessorsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSORS_AND_USERS");

                HCMDatabase.AddInParameter(getAssessorsCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getAssessorsCommand,
                    "@NAME", DbType.String, Utility.IsNullOrEmpty(name) ? null : name.Trim());

                HCMDatabase.AddInParameter(getAssessorsCommand,
                    "@EMAIL", DbType.String, Utility.IsNullOrEmpty(email) ? null : email.Trim());

                if (includeUsers)
                    HCMDatabase.AddInParameter(getAssessorsCommand, "@INCLUDE_USERS", DbType.String, "Y");
                else
                    HCMDatabase.AddInParameter(getAssessorsCommand, "@INCLUDE_USERS", DbType.String, "N");

                HCMDatabase.AddInParameter(getAssessorsCommand,
                    "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(getAssessorsCommand,
                    "@ORDER_BY_DIRECTION", DbType.String,
                    sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getAssessorsCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getAssessorsCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getAssessorsCommand);

                while (dataReader.Read())
                {
                    // Instantiate assessment details list.
                    if (assessorDetails == null)
                        assessorDetails = new List<AssessorDetail>();

                    // Create an assessment detail object.
                    AssessorDetail assessorDetail = new AssessorDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        assessorDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]))
                        assessorDetail.FirstName = dataReader["ASSESSOR_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        assessorDetail.UserEmail = dataReader["EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ALTERNATE_EMAIL"]))
                        assessorDetail.AlternateEmailID = dataReader["ALTERNATE_EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["IS_ASSESSOR"]))
                    {
                        if (dataReader["IS_ASSESSOR"].ToString().Trim().ToUpper() == "Y")
                            assessorDetail.IsAssessor = true;
                    }
                    assessorDetails.Add(assessorDetail);
                }
                return assessorDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the combined info such as assessor detail, 
        /// skill name for given parameters.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        /// <returns>
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </returns>
        public AssessorDetail GetAssessorCombinedInfo(int userID, int skillID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCombinedInfoCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_COMBINED_INFO");

                HCMDatabase.AddInParameter(getCombinedInfoCommand,
                    "@USER_ID", DbType.Int32, userID);

                HCMDatabase.AddInParameter(getCombinedInfoCommand,
                   "@SKILL_ID", DbType.Int32, skillID);

                dataReader = HCMDatabase.ExecuteReader(getCombinedInfoCommand);

                AssessorDetail assessorDetail = null;

                // Retreive assessor details.
                if (dataReader.Read())
                {
                    // Instantiate assessor detail object.
                    assessorDetail = new AssessorDetail();

                    assessorDetail.UserID = userID;

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        assessorDetail.UserEmail = dataReader["EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ALTERNATE_EMAILID"]))
                        assessorDetail.AlternateEmailID = dataReader["ALTERNATE_EMAILID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        assessorDetail.FirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        assessorDetail.LastName = dataReader["LAST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                        assessorDetail.Skill = dataReader["SKILL_NAME"].ToString();
                }
                return assessorDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the non availability dates for the given 
        /// assessor.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the non availability dates as
        /// comma separated.
        /// </returns>
        public string GetAssessorNonAvailabilityDates(int userID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCombinedInfoCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_NON_AVAILABILITY_DATES");

                HCMDatabase.AddInParameter(getCombinedInfoCommand,
                    "@USER_ID", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(getCombinedInfoCommand);

                // Retreive non availability dates.
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["NON_AVAILABILITY_DATES"]))
                        return dataReader["NON_AVAILABILITY_DATES"].ToString().Trim();
                    else
                        return null;
                }
                return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the non availability dates for the given 
        /// assessor.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="nonAvailabilityDates">
        /// A <see cref="string"/> that holds non availability dates as comma
        /// separated.
        /// </param>
        public void UpdateAssessorNonAvailabilityDates(int userID, 
            string nonAvailabilityDates)
        {
            // Create a stored procedure command object
            DbCommand updateCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_ASSESSOR_NON_AVAILABILITY_DATES");

            // Add input parameters
            HCMDatabase.AddInParameter(updateCommand,
                "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddInParameter(updateCommand,
                "@NON_AVAILABILITY_DATES", DbType.String, nonAvailabilityDates);

            HCMDatabase.ExecuteNonQuery(updateCommand);
        }

        /// <summary>
        /// Method that retrieves the list of candidate schedules for the given
        /// online interview candidate session key.  
        /// </summary>
        /// <param name="candidateInterviewSessionKey">
        /// A <see cref="string"/> that holds the online interview candidate
        /// session key.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortOrder"/> that holds the sort order.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID as an output
        /// parameter.
        /// </param>
        /// <param name="candidateName">
        /// A <see cref="string"/> that holds the candidate name as an output
        /// parameter.
        /// </param>
        /// <param name="candidateEmail">
        /// A <see cref="string"/> that holds the candidate email as an output
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="OnlineCandidateSessionDetail"/> that holds the
        /// online interview candidate schedules.
        /// </returns>
        public List<OnlineCandidateSessionDetail> GetOnlineInterviewCandidateSchedules
            (string candidateInterviewSessionKey, string sortField, 
            SortType sordOrder, out int candidateID, out string candidateName, 
            out string candidateEmail)
        {
            // Assign default values to output parameters.
            candidateID = 0;
            candidateName = string.Empty;
            candidateEmail = string.Empty;

            IDataReader dataReader = null;

            List<OnlineCandidateSessionDetail> timeSlots = null;

            OnlineCandidateSessionDetail timeSlot = null;

            try
            {
                DbCommand searchCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ONLINE_INTERVIEW_CANDIDATE_SCHEDULES");

                HCMDatabase.AddInParameter(searchCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateInterviewSessionKey);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(searchCommand,
                    "@ORDER_BY_DIRECTION", DbType.String, sordOrder == SortType.Ascending ? "A" : "D");

                dataReader = HCMDatabase.ExecuteReader(searchCommand);

                // Read candidate details.
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        candidateID = Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        candidateName = dataReader["CANDIDATE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        candidateEmail = dataReader["EMAIL_ID"].ToString();
                }

                // Read schedules
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    if (timeSlots == null)
                        timeSlots = new List<OnlineCandidateSessionDetail>();

                    timeSlot = new OnlineCandidateSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                        timeSlot.AssessorID = int.Parse(dataReader["ASSESSOR_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]))
                        timeSlot.AssessorName = dataReader["ASSESSOR_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                        timeSlot.SkillID = int.Parse(dataReader["SKILL_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                        timeSlot.SkillName = dataReader["SKILL_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_DATE"]))
                        timeSlot.InterviewDate = Convert.ToDateTime(dataReader["INTERVIEW_DATE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID"]))
                        timeSlot.TimeSlotID = int.Parse(dataReader["TIME_SLOT_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM_NAME"]) &&
                        !Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO_NAME"]))
                    {
                        timeSlot.TimeSlot = dataReader["TIME_SLOT_ID_FROM_NAME"].ToString().Trim() +
                            " - " + dataReader["TIME_SLOT_ID_TO_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_STATUS"]))
                        timeSlot.SessionStatus = dataReader["SESSION_STATUS"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_STATUS_NAME"]))
                        timeSlot.SessionStatusName = dataReader["SESSION_STATUS_NAME"].ToString().Trim();

                    timeSlots.Add(timeSlot);
                }

                return timeSlots;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that deletes an online interview candidate schedule.
        /// assessor.
        /// </summary>
        /// <param name="candidateInterviewSessionKey">
        /// A <see cref="string"/> that holds the online interview candidate
        /// session key.
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <param name="timeSlotID">
        ///  A <see cref="int"/> that holds the time slot ID.
        /// </param>
        public void DeleteOnlineInterviewCandidateSchedule
            (string candidateInterviewSessionKey,
            int skillID, int assessorID, int timeSlotID)
        {
            // Create a stored procedure command object
            DbCommand deleteCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ONLINE_INTERVIEW_SCHEDULE");

            // Add input parameters
            HCMDatabase.AddInParameter(deleteCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateInterviewSessionKey);

            HCMDatabase.AddInParameter(deleteCommand,
                "@SKILL_ID", DbType.Int32, skillID);

            HCMDatabase.AddInParameter(deleteCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);

            HCMDatabase.AddInParameter(deleteCommand,
                "@TIME_SLOT_ID", DbType.Int32, timeSlotID);

            HCMDatabase.ExecuteNonQuery(deleteCommand);
        }

        /// <summary>
        /// Method that retrieves skill detail for the given skill. If the 
        /// skill is not present then it will be created as new and newly 
        /// added skill will be retrieved.
        /// </summary>
        /// <param name="skill">
        /// A <see cref="string"/> that holds the skill.
        /// </param>
        /// <returns>
        /// A <see cref="SkillDetail"/> that holds the skill detail.
        /// </returns>
        public SkillDetail GetSkillDetail(string skill)
        {
            IDataReader dataReader = null;

            SkillDetail skillDetail = null;

            try
            {
                DbCommand getSkillCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_SKILL_DETAIL");

                HCMDatabase.AddInParameter(getSkillCommand,
                    "@SKILL", DbType.String, skill);

                // Execute the reader.
                dataReader = HCMDatabase.ExecuteReader(getSkillCommand);

                // Read candidate details.
                if (dataReader.Read())
                {
                    // Instantiate skill detail object.
                    skillDetail = new SkillDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                        skillDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"]))
                        skillDetail.SkillID = Convert.ToInt32(dataReader["CAT_SUB_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                        skillDetail.SkillName = dataReader["SUBJECT_NAME"].ToString();
                }

                return skillDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts the assessor skills.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="skillDetails">
        /// A list of <see cref="SkillDetail"/> that holds the skills.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        /// <returns>
        public void InsertAssessorSkill(int userID, List<SkillDetail> skillDetails,
            IDbTransaction transaction)
        {
            DbCommand deleteskillCommand =
                HCMDatabase.GetStoredProcCommand("SPDELETE_ASSESSOR_SKILLS");

            HCMDatabase.AddInParameter(deleteskillCommand,
                "@USERID", DbType.Int32, userID);

            HCMDatabase.ExecuteNonQuery(deleteskillCommand, transaction as DbTransaction);

            foreach (SkillDetail skillDetail in skillDetails)
            {
                DbCommand insertAssessorSkill =
                    HCMDatabase.GetStoredProcCommand("SPINSERT_ASSESSOR_SKILL");

                HCMDatabase.AddInParameter(insertAssessorSkill,
                    "@USERID", DbType.Int32, userID);

                HCMDatabase.AddInParameter(insertAssessorSkill,
                    "@SKILLID", DbType.Int32, skillDetail.SkillID);

                HCMDatabase.ExecuteNonQuery(insertAssessorSkill, transaction as DbTransaction);
            }
        }

        /// <summary>
        /// Method to get the skill matched with assessor or not
        /// </summary>
        /// <param name="assessorID">assessorID</param>
        /// <param name="catSubID">catSubID</param>
        /// <returns></returns>
        public int GetSkillMatchedWithAssessor(int assessorID,int catSubID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getSkillMatchedCntCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_IS_SKILL_MATCHED_WITH_ASSESSOR");

                HCMDatabase.AddInParameter(getSkillMatchedCntCommand,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);

                HCMDatabase.AddInParameter(getSkillMatchedCntCommand,
                    "@CAT_SUB_ID", DbType.Int32, catSubID);

                dataReader = HCMDatabase.ExecuteReader(getSkillMatchedCntCommand);

                // Retreive skill matched count
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["REC_CNT"]))
                        return Convert.ToInt32(dataReader["REC_CNT"].ToString().Trim());
                    else
                        return 0;
                }
                return 0;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<AttributeDetail> GetAssessorEventsCount(int assessorID)
        { 
            IDataReader dataReader = null;

            List<AttributeDetail> attributeDetailList = null;

            AttributeDetail attributeDetail = null;

            try
            {
                DbCommand searchCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ASSESSOR_EVENTS_COUNT");

                HCMDatabase.AddInParameter(searchCommand,
                    "@ASSESSOR_ID", DbType.Int32, assessorID); 

                dataReader = HCMDatabase.ExecuteReader(searchCommand); 

                while (dataReader.Read())
                {
                    if (attributeDetailList == null)
                        attributeDetailList = new List<AttributeDetail>();

                    attributeDetail = new AttributeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTEID"]))
                        attributeDetail.AttributeID = dataReader["ATTRIBUTEID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTENAME"]))
                        attributeDetail.AttributeName = dataReader["ATTRIBUTENAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                        attributeDetail.count = int.Parse(dataReader["COUNT"].ToString());

                    attributeDetailList.Add(attributeDetail);
                }

                return attributeDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that gets the external assessor detail & skill.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="AssessorDetail"/> that holds the external assessor details.
        /// </returns>
        public ExternalAssessorDetail GetExternalAssessorByID(int userID)
        {
            IDataReader dataReader = null;
            try
            {
                DbCommand getExternalAssessorDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_EXTERNAL_ASSESSOR_DETAIL_BY_ID");

                HCMDatabase.AddInParameter(getExternalAssessorDetailCommand,
                    "@EXTERNAL_ASSESSOR_ID", DbType.Int64, userID);

                dataReader = HCMDatabase.ExecuteReader(getExternalAssessorDetailCommand);

                ExternalAssessorDetail assessorDetail = null;

                // Retreive assessor details.
                if (dataReader.Read())
                {
                    // Instantiate assessor detail object.
                    assessorDetail = new ExternalAssessorDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["EXTERNAL_ASSESSOR_ID"]))
                        assessorDetail.ExternalAssessorID = Convert.ToInt32(dataReader["EXTERNAL_ASSESSOR_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        assessorDetail.FirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        assessorDetail.LastName = dataReader["LAST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        assessorDetail.Email = dataReader["EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CONTACT_NUMBER"]))
                        assessorDetail.ContactNumber = dataReader["CONTACT_NUMBER"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        assessorDetail.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    // Move to next record set.
                    dataReader.NextResult();

                    // Retreive skill details.
                    while (dataReader.Read())
                    {
                        // Instantiate skills list.
                        if (assessorDetail.Skills == null)
                            assessorDetail.Skills = new List<SkillDetail>();

                        // Create a skill object.
                        SkillDetail skill = new SkillDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_ID"]))
                            skill.CategoryID = Convert.ToInt32(dataReader["CATEGORY_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                            skill.CategoryName = dataReader["CATEGORY_NAME"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                            skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"].ToString());

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                            skill.SkillName = dataReader["SKILL_NAME"].ToString();

                        // Add to the skills list.
                        assessorDetail.Skills.Add(skill);
                    }
                }
                return assessorDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that udpate the external assessor to registered user.
        /// </summary>
        /// <param name="externalAssessorID">
        /// A <see cref="int"/> that holds the external assessor ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void UpdateExternalAssessorToRegisteredUser(int externalAssessorID, int userID, 
            string email, IDbTransaction transaction)
        {
            // Create a stored procedure command object
            DbCommand updateAssessorCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_EXTERNAL_ASSESSOR_TO_REGISTERED_USER");

            // Add input parameters
            HCMDatabase.AddInParameter(updateAssessorCommand,
                "@EXTERNAL_ASSESSOR_ID", DbType.Int64, externalAssessorID);

            HCMDatabase.AddInParameter(updateAssessorCommand,
                "@USER_ID", DbType.Int32, userID);

            HCMDatabase.AddInParameter(updateAssessorCommand,
                "@EMAIL", DbType.String, email);

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateAssessorCommand,
                transaction as DbTransaction);
        }

        /// <summary>
        /// Method that retreives assessor status against 
        /// this candidate session key 
        /// </summary>
        /// <param name="candidateInterviewSessionKey">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="assessorID">
        /// A <see cref="int"/> that holds the assessor ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status true or false
        /// [True- Assessor already involved against this session]
        /// </returns>
        public bool IsAssessorInvolvedAgainstThisCandidateSession(string candidateInterviewSessionKey,
            int attemptID, int assessorID)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object
                DbCommand getAssessorStatusCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_IS_ASSESSOR_INVOLVED_IN_CANDIDATE_SESSION");

                // Add input parameters
                HCMDatabase.AddInParameter(getAssessorStatusCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateInterviewSessionKey);

                HCMDatabase.AddInParameter(getAssessorStatusCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                HCMDatabase.AddInParameter(getAssessorStatusCommand,
                    "@ASSESSOR_ID", DbType.Int32, assessorID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getAssessorStatusCommand);

                // Retreives assessor status
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_EXIST"]) && 
                        dataReader["ASSESSOR_EXIST"].ToString().ToUpper()=="Y")
                        return true;
                    else
                        return false;
                }
                return false;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that search for the assessor against the selected date and time criteria
        /// </summary>
        /// <param name="timeSlotCriteria">
        /// A <see cref="AssessorTimeSlotDetail"/> that holds the time slot search criteria.
        /// </param>
        /// <returns>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="sortField">
        /// A <see cref="string"/> that holds the sort field.
        /// </param>
        /// <param name="sordOrder">
        /// A <see cref="SortType"/> that holds the sort order.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as output 
        /// parameter.
        /// </param>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor time slot details.
        /// </returns>
        public List<AssessorTimeSlotDetail> GetAssessorAvailableDates(AssessorTimeSlotSearchCriteria timeSlotCriteria,
            int tenantID, string sortField, SortType sordOrder, int pageNumber, int pageSize, out int totalRecords)
        {
            IDataReader dataReader = null;

            totalRecords = 0;

            try
            {
                DbCommand getAvailabilityDateCommand = 
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSORS_BY_AVAILABLE_DATE");

                // Add parameters.   
                HCMDatabase.AddInParameter(getAvailabilityDateCommand, "@TENANT_ID", DbType.Int32, tenantID);

                if (timeSlotCriteria.FromDate.ToShortDateString() != "1/1/0001")
                    HCMDatabase.AddInParameter(getAvailabilityDateCommand, "@FROM_DATE", DbType.Date, timeSlotCriteria.FromDate);

                if (timeSlotCriteria.ToDate.ToShortDateString() != "1/1/0001")
                    HCMDatabase.AddInParameter(getAvailabilityDateCommand, "@TO_DATE", DbType.Date, timeSlotCriteria.ToDate);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand, 
                    "@FROM_TIME_ID", DbType.Int32, timeSlotCriteria.TimeSlotIDFrom);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand, 
                    "@TO_TIME_ID", DbType.Int32, timeSlotCriteria.TimeSlotIDTo);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@ORDER_BY", DbType.String, sortField);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@ORDER_BY_DIRECTION", DbType.String,
                    sordOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getAvailabilityDateCommand,
                    "@PAGE_SIZE", DbType.Int32, pageSize);


                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getAvailabilityDateCommand);

                List<AssessorTimeSlotDetail> timeSlotDetails = null;
                while (dataReader.Read())
                {
                    // Instantiate the list.
                    if (timeSlotDetails == null)
                        timeSlotDetails = new List<AssessorTimeSlotDetail>();

                    AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                        continue;
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]))
                    {
                        timeSlot.Assessor = dataReader["ASSESSOR_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]))
                    {
                        timeSlot.AssessorID = Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVAILABILITY_DATE"]))
                    {
                        timeSlot.AvailabilityDate = 
                            Convert.ToDateTime(dataReader["AVAILABILITY_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANY_TIME"]))
                    {
                        timeSlot.IsAvailableFullTime = dataReader["ANY_TIME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ACCEPTED_DATE"]))
                    {
                        timeSlot.AcceptedDate = 
                            Convert.ToDateTime(dataReader["ACCEPTED_DATE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                    {
                        timeSlot.TimeSlotIDFrom =
                            Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                    {
                        timeSlot.TimeSlotIDTo  = 
                            Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_FROM"]))
                    {
                        timeSlot.TimeSlotTextFrom = dataReader["TIME_SLOT_TEXT_FROM"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_TO"]))
                    {
                        timeSlot.TimeSlotTextTo = dataReader["TIME_SLOT_TEXT_TO"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_AVAILABLE"]))
                    {
                        timeSlot.IsAssessorAvailable = dataReader["ASSESSOR_AVAILABLE"].ToString().Trim();
                    }
                    // Add to the list.
                    timeSlotDetails.Add(timeSlot);
                }
                return timeSlotDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that gets the list of list requested candidated slots
        /// </summary>
        /// <param name="slotKey">
        /// A <see cref="string"/> that holds the slot key.
        /// </param>
        /// <returns>
        /// A list of <see cref="AssessorTimeSlotDetail"/> that holds the assessor time slot details.
        /// </returns>
        public AssessorTimeSlotDetail GetCandidateSlotDetail(string slotKey)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getRequestedSlotCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_REQUESTED_SLOT_BY_KEY");

                // Add parameters.   
                HCMDatabase.AddInParameter(getRequestedSlotCommand, "@SLOT_KEY", DbType.String, slotKey);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getRequestedSlotCommand);

                AssessorTimeSlotDetail assessorSlotDetail = 
                    new AssessorTimeSlotDetail();

                List<AssessorTimeSlotDetail> timeSlotDetails = null;

                OnlineInterviewSessionDetail onlineInterviewDetail = null;
                
                if (dataReader.Read())
                {
                    // Instantiate the online inteview session
                    if (onlineInterviewDetail == null)
                        onlineInterviewDetail = new OnlineInterviewSessionDetail();

                    // Assign property values to the online interview object.
                    onlineInterviewDetail.InterviewName = dataReader["ONLINE_INTERVIEW_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        onlineInterviewDetail.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        onlineInterviewDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["INSTRUCTION"]))
                    {
                        onlineInterviewDetail.InterviewInstruction = 
                            dataReader["INSTRUCTION"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["REMARKS"]))
                    {
                        onlineInterviewDetail.InterviewRemarks = 
                            dataReader["REMARKS"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_BY"]))
                    {
                        onlineInterviewDetail.RequestedBy =
                            dataReader["REQUESTED_BY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CAND_ONLINE_INTERVIEW_ID"]))
                    {
                        onlineInterviewDetail.CandidateInterviewID =
                            Convert.ToInt32(dataReader["CAND_ONLINE_INTERVIEW_ID"].ToString().Trim());
                    }
                }

                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the list.
                    if (timeSlotDetails == null)
                        timeSlotDetails = new List<AssessorTimeSlotDetail>();

                    AssessorTimeSlotDetail timeSlot = new AssessorTimeSlotDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_SLOT_ID"]))
                    {
                        timeSlot.RequestDateGenID =
                            Convert.ToInt32(dataReader["REQUESTED_SLOT_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ACCEPTED_ID"]))
                    {
                        timeSlot.AcceptedID =
                            Convert.ToInt32(dataReader["ACCEPTED_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["REQUESTED_DATE"]))
                    {
                        timeSlot.AvailabilityDate =
                            Convert.ToDateTime(dataReader["REQUESTED_DATE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_FROM"]))
                    {
                        timeSlot.TimeSlotIDFrom =
                            Convert.ToInt32(dataReader["TIME_SLOT_ID_FROM"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_ID_TO"]))
                    {
                        timeSlot.TimeSlotIDTo =
                            Convert.ToInt32(dataReader["TIME_SLOT_ID_TO"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_FROM"]))
                    {
                        timeSlot.TimeSlotTextFrom = dataReader["TIME_SLOT_TEXT_FROM"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_SLOT_TEXT_TO"]))
                    {
                        timeSlot.TimeSlotTextTo = dataReader["TIME_SLOT_TEXT_TO"].ToString().Trim();
                    }

                    // Add to the list.
                    timeSlotDetails.Add(timeSlot);
                }

                if(onlineInterviewDetail!=null)
                {
                    assessorSlotDetail.onlineInterviewDetail=onlineInterviewDetail;
                }

                if(timeSlotDetails!=null)
                {
                    assessorSlotDetail.assessorTimeSlotDetails=timeSlotDetails;
                }

                return assessorSlotDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        
        /// <summary>
        /// Method that updates the slot key which is approved by the client
        /// </summary>
        /// <param name="requestSlotID">
        /// A <see cref="int"/> that holds the requested id.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user id.
        /// </param>
        public void ApproveSlotIDByCandidate(int requestSlotID,
            int userID)
        {
            // Create a stored procedure command object
            DbCommand updateSlotIDCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_SLOT_REQUEST_BY_CANDIDATE");

            // Add input parameters
            HCMDatabase.AddInParameter(updateSlotIDCommand,
                "@REQUEST_SLOT_ID", DbType.Int32, requestSlotID);


            HCMDatabase.AddInParameter(updateSlotIDCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateSlotIDCommand);
        }


        /// <summary>
        /// Method that inserts an assessor time slot detail.
        /// </summary>
        /// <param name="timeSlot">
        /// A <seealso cref="AssessorTimeSlotDetail"/> that holds the assessor
        /// time slot detail.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        public void UpdateAssessorAvailableDateTimeSlots(int requestDataeID, int userID ,AssessorTimeSlotDetail timeSlot,
             IDbTransaction transaction)
        {
            DbCommand updateAssessorTimeCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_ASSESSOR_AVAILABILITY_TIME_SLOTS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateAssessorTimeCommand,
                "@ASS_DATES_GEN_ID", DbType.Int32, requestDataeID);

            HCMDatabase.AddInParameter(updateAssessorTimeCommand,
                "@TIME_SLOT_ID_FROM", DbType.Int32, timeSlot.TimeSlotIDFrom);

            HCMDatabase.AddInParameter(updateAssessorTimeCommand,
                "@TIME_SLOT_ID_TO", DbType.Int32, timeSlot.TimeSlotIDTo); 

            HCMDatabase.AddInParameter(updateAssessorTimeCommand,
                "@USER_ID", DbType.Int32, userID); 

            if (transaction == null)
                HCMDatabase.ExecuteNonQuery(updateAssessorTimeCommand);
            else
                HCMDatabase.ExecuteNonQuery(updateAssessorTimeCommand, transaction as DbTransaction);
        }

        public int GetAssessorAvailableDateGenId(int assessorId,DateTime requestDate)
        { 
            IDataReader dataReader = null;
            int genId = 0;

            try
            {
                DbCommand getAvailableDateGenIdCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSESSOR_AVAILABLE_DATE_GEN_ID");

                // Add parameters.   
                HCMDatabase.AddInParameter(getAvailableDateGenIdCommand, "@ASSESSOR_ID", DbType.Int32, assessorId);
                HCMDatabase.AddInParameter(getAvailableDateGenIdCommand, "@REQUEST_DATE", DbType.DateTime, requestDate);
                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getAvailableDateGenIdCommand);
                 
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["ASS_DATES_GEN_ID"]))
                        genId = Convert.ToInt32(dataReader["ASS_DATES_GEN_ID"]); 
                }

                return genId;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

    }
}