﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestSchedulerDLManager.cs
// File that represents the data layer for the Test Scheduler module.
// This includes functionalities such as schedule candidate, un-schedule
// candidate, re-schedule candidate, retrieve session, set test reminder,
// delete test reminder, etc. This will talk to the database to perform 
// these operations.

#endregion

#region Directives
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Test Scheduler module.
    /// This includes functionalities such as schedule candidate, un-schedule
    /// candidate, re-schedule candidate, retrieve session, set test reminder,
    /// delete test reminder, etc. This will talk to the database to perform 
    /// these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class TestSchedulerDLManager : DatabaseConnectionManager
    {
        #region Public Method

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestSessionDetail"/> that holds the candidate
        /// test session details.
        /// </returns>
        public CandidateTestSessionDetail GetCandidateTestSession
            (string candidateTestSessionID, int attemptID)
        {
            if (Utility.IsNullOrEmpty(candidateTestSessionID))
                throw new Exception("Candidate test session ID cannot be empty");

            CandidateTestSessionDetail session = null;
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_SESSION");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@CANDIDATE_SESSION_ID", DbType.String, candidateTestSessionID);

                HCMDatabase.AddInParameter(getCandidateSession,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                if (dataReader.Read())
                {
                    session = new CandidateTestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_GENID"]))
                    {
                        session.GenID = Convert.ToInt32(dataReader["CANDIDATE_SESSION_GENID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        session.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                    {
                        session.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        session.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        session.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                    {
                        session.Email = dataReader["EMAIL_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        session.Status = dataReader["STATUS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_ATTEMPT"]))
                    {
                        session.NoOfAttempt = Convert.ToInt16(dataReader["NO_OF_ATTEMPT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        session.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                    {
                        session.TestID = dataReader["TEST_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        session.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }

                    session.AttemptID = attemptID;
                }

                return session;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID and attempt ID, that helps in composing 
        /// email.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateTestSessionDetail"/> that holds the candidate
        /// test session details.
        /// </returns>
        public CandidateTestSessionDetail GetCandidateTestSessionEmailDetail
            (string candidateTestSessionID, int attemptID)
        {
            if (Utility.IsNullOrEmpty(candidateTestSessionID))
                throw new Exception("Candidate test session ID cannot be empty");

            CandidateTestSessionDetail session = null;
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_SESSION_EMAIL_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@CANDIDATE_SESSION_ID", DbType.String, candidateTestSessionID);

                HCMDatabase.AddInParameter(getCandidateSession,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                if (dataReader.Read())
                {
                    session = new CandidateTestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_GENID"]))
                    {
                        session.GenID = Convert.ToInt32(dataReader["CANDIDATE_SESSION_GENID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_ID"]))
                    {
                        session.CandidateTestSessionID = dataReader["CANDIDATE_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SESSION_ID"]))
                    {
                        session.TestSessionID = dataReader["TEST_SESSION_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        session.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        session.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                    {
                        session.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_MOBILE"]))
                    {
                        session.CandidateMobile = dataReader["CANDIDATE_MOBILE"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_OWNER_EMAIL"]))
                    {
                        session.CandidateOwnerEmail = dataReader["CANDIDATE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                    {
                        session.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["NAVIGATE_KEY"]))
                    {
                        session.NavigateKey = dataReader["NAVIGATE_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_EMAIL"]))
                    {
                        session.SchedulerEmail = dataReader["SCHEDULER_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_FIRST_NAME"]))
                    {
                        session.SchedulerFirstName = dataReader["SCHEDULER_FIRST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_COMPANY"]))
                    {
                        session.SchedulerCompany = dataReader["SCHEDULER_COMPANY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_LAST_NAME"]))
                    {
                        session.SchedulerLastName = dataReader["SCHEDULER_LAST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_EMAIL"]))
                    {
                        session.PositionProfileOwnerEmail = dataReader["POSITION_PROFILE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                    {
                        session.Status = dataReader["STATUS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_ATTEMPT"]))
                    {
                        session.NoOfAttempt = Convert.ToInt16(dataReader["NO_OF_ATTEMPT"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                    {
                        session.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_ID"]))
                    {
                        session.TestID = dataReader["TEST_ID"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                    {
                        session.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    }

                    session.AttemptID = attemptID;
                }

                return session;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }






        /// <summary>
        /// Method that will call to load the candidate details 
        /// who scheduled for the tests.
        /// </summary>
        /// <param name="user">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="pageNumber">
        /// An <see cref="int"/>that contains the current page number.
        /// </param>
        /// <param name="pageSize">
        /// An <see cref="int"/> that contains the page size.
        /// </param>
        /// <param name="totalRecords">
        /// An <see cref="int"/> that contains the total records.
        /// </param>
        /// <param name="scheduleSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the candidate Information Like Name...
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that contains the column to be sorted.
        /// </param>
        /// <param name="orderDirection">
        /// A <see cref="SortType"/> that contains the sort direction either Asc/Desc.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestScheduleDetail"/> that contains candidate details.
        /// </returns>
        public List<TestScheduleDetail> GetScheduler
            (int? user, int pageNumber, int pageSize, out int totalRecords,
            TestScheduleSearchCriteria scheduleSearchCriteria, string orderBy, SortType orderDirection)
        {
            totalRecords = 0;
            IDataReader dataReader = null;
            List<TestScheduleDetail> testScheduleDetails = null;

            try
            {
                DbCommand getSchedulerCommand = HCMDatabase.GetStoredProcCommand("SPGET_SCHEDULED_DETAILS");
                // Add input parameters
                HCMDatabase.AddInParameter(getSchedulerCommand, "@CANDIDATE_NAME", DbType.String, scheduleSearchCriteria.CandidateName);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@CANDIDATE_EMAIL", DbType.String, scheduleSearchCriteria.EMail);
                //HCMDatabase.AddInParameter(getSchedulerCommand, "@SCHEDULER_NAME", DbType.String, scheduleSearchCriteria.TestScheduleAuthor);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@TEST_SESSION_ID", DbType.String, scheduleSearchCriteria.TestSessionID);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@TEST_NAME", DbType.String, scheduleSearchCriteria.TestName);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@TEST_COMPLETED_STATUS", DbType.Int32, scheduleSearchCriteria.TestCompletedStatus);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@ORDERBYDIRECTION", DbType.String,
                    orderDirection == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING
                    : Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@TOTAL", DbType.String, 0);
                HCMDatabase.AddInParameter(getSchedulerCommand, "@SCHEDULED_BY", DbType.Int32, user == 0 ? null : user);
                dataReader = HCMDatabase.ExecuteReader(getSchedulerCommand);
                while (dataReader.Read())
                {
                    if (testScheduleDetails == null)
                        testScheduleDetails = new List<TestScheduleDetail>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        TestScheduleDetail testScheduleDetail = new TestScheduleDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_REQUEST_NUMBER"]))
                            testScheduleDetail.ClientRequestID =
                                dataReader["CLIENT_REQUEST_NUMBER"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                            testScheduleDetail.TestSessionID = dataReader["SESSION_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_EXPIRY"]))
                            testScheduleDetail.SessionExpiryDate =
                                Convert.ToDateTime(dataReader["SESSION_EXPIRY"]);

                        if (!Utility.IsNullOrEmpty(dataReader["CAND_SESSION_KEY"]))
                            testScheduleDetail.CandidateTestSessionID =
                                dataReader["CAND_SESSION_KEY"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            testScheduleDetail.CandidateID = dataReader["CANDIDATE_ID"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        {
                            string candidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                            string[] candidateFirstName = candidateName.Split(new char[] { '~' });
                            testScheduleDetail.CandidateName = candidateFirstName[0];
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FULLNAME"]))
                            testScheduleDetail.CandidateFullName = dataReader["CANDIDATE_FULLNAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                            testScheduleDetail.EmailId = dataReader["CANDIDATE_EMAIL"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATED_ON"]))
                            testScheduleDetail.CreatedDate =
                                Convert.ToDateTime(dataReader["SESSION_CREATED_ON"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        {
                            string sessionAuthorName = dataReader["SCHEDULED_BY"].ToString().Trim();
                            string[] schedulerDetails = sessionAuthorName.Split(new char[] { '~' });
                            testScheduleDetail.SchedulerFirstName = schedulerDetails[0];
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_FULLNAME"]))
                            testScheduleDetail.SchedulerFullName = dataReader["SCHEDULER_FULLNAME"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            testScheduleDetail.ExpiryDate =
                                Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["STATUS"]))
                            testScheduleDetail.TestStatus = dataReader["STATUS"].ToString().Trim();

                        if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                            testScheduleDetail.CompletedDate = Convert.ToDateTime(dataReader["COMPLETED_DATE"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                            testScheduleDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                            testScheduleDetail.TestID = dataReader["TEST_KEY"].ToString().Trim();

                        testScheduleDetails.Add(testScheduleDetail);
                    }
                    else
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                }
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
            return testScheduleDetails;
        }


        /// <summary>
        /// Method that will call to load the candidate test session details 
        /// </summary>
        /// <param name="testSessionID">
        /// A  <see cref="string"/> that contains the TestSession ID.
        /// </param>
        /// <param name="candidateTestSessionIDs">
        /// A list of <see cref="string"/> that contains the candidate TestSession IDs.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestScheduleDetail"/> that contains candidate details.
        /// </returns>
        public List<TestScheduleDetail> GetCandidateTestSessions(string testSessionID, List<string> candidateTestSessionIDs)
        {
            return null;
        }

        /// <summary>
        /// Method that will update the scheduled candidate details.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the schedule detail.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <remarks>
        /// While scheduling a candidate to the test, this method gets triggered to 
        /// store the values in Session_Candidate and Candidate_Session_Tracking tables.
        /// </remarks>
        public void ScheduleCandidate(TestScheduleDetail testScheduleDetail, int modifiedBy)
        {
            // Create a stored procedure command object.
            DbCommand updateScheduleCandCommand = HCMDatabase.
                GetStoredProcCommand("SP_SCHEDULE_CANDIDATE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@CAND_SESSION_KEY", DbType.String, testScheduleDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@CANDIDATE_ID", DbType.Int32, Convert.ToInt32(testScheduleDetail.CandidateID));
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@ATTEMPT_ID", DbType.Int16, Convert.ToInt32(testScheduleDetail.AttemptID));
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@EMAILID", DbType.String, testScheduleDetail.EmailId);

            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@SCHEDULED_DATE", DbType.DateTime, testScheduleDetail.ExpiryDate);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@EMAIL_REMAINDER", DbType.Int32, testScheduleDetail.EmailReminder==null?"N":testScheduleDetail.EmailReminder);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateScheduleCandCommand);
        }

        /// <summary>
        /// Method that will update the scheduled information to the respective tables.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the test schedule detail.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that contains the transaction object.
        /// </param>
        public void ScheduleCandidate(TestScheduleDetail testScheduleDetail,
            int modifiedBy, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand updateScheduleCandCommand = HCMDatabase.
                GetStoredProcCommand("SP_SCHEDULE_CANDIDATE");
            // Add input parameters.
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@CAND_SESSION_KEY", DbType.String, testScheduleDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@CANDIDATE_ID", DbType.Int32, Convert.ToInt32(testScheduleDetail.CandidateID));
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@ATTEMPT_ID", DbType.Int16, Convert.ToInt32(testScheduleDetail.AttemptID));
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@EMAILID", DbType.String, testScheduleDetail.EmailId);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@SCHEDULED_DATE", DbType.DateTime, testScheduleDetail.ExpiryDate);
            HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);

            if (!Utility.IsNullOrEmpty(testScheduleDetail.EmailReminder))
                HCMDatabase.AddInParameter(updateScheduleCandCommand,
                "@EMAIL_REMAINDER", DbType.String, testScheduleDetail.EmailReminder);

            // Execute the stored procedure.
           int count= HCMDatabase.ExecuteNonQuery(updateScheduleCandCommand, transaction as DbTransaction);
        }



        /// <summary>
        /// Method that will update the retake information to the respective table.
        /// </summary>
        /// <param name="testScheduleDetail">
        /// A <see cref="TestScheduleDetail"/> that contains the test schedule instance.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that contains the user id.
        /// </param>
        public void RetakeTest(TestScheduleDetail testScheduleDetail, int modifiedBy)
        {
            // Create a stored procedure command object.
            DbCommand updateRetakeTestCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_RETAKESCHEDULE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateRetakeTestCommand,
                "@CAND_SESSION_KEY", DbType.String, testScheduleDetail.CandidateTestSessionID);
            HCMDatabase.AddInParameter(updateRetakeTestCommand,
                "@ATTEMPT_ID", DbType.Int16, Convert.ToInt32(testScheduleDetail.AttemptID));

            HCMDatabase.AddInParameter(updateRetakeTestCommand,
                "@SCHEDULED_DATE", DbType.DateTime, testScheduleDetail.ExpiryDate);
            HCMDatabase.AddInParameter(updateRetakeTestCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateRetakeTestCommand);
        }

        /// <summary>
        /// Method that will return the test session and candidate session details
        /// based on the search criteria.
        /// </summary>
        /// <param name="scheduleCandidateSearchCriteria">
        /// A <see cref="TestScheduleSearchCriteria"/> that contains the search criteria details.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that contains the sort expression.
        /// </param>
        /// <param name="sortDirection">
        /// A <see cref="SortType"/> that contains the sort type either ASC/DESC.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the test session details information.
        /// </returns>
        public TestSessionDetail GetTestSessionDetail(TestScheduleSearchCriteria scheduleCandidateSearchCriteria,
            string sortExpression, SortType sortDirection, int userID)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_SESSION_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_KEY", DbType.String, scheduleCandidateSearchCriteria.TestSessionID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@CAND_SESSION_KEYS", DbType.String, scheduleCandidateSearchCriteria.CandidateSessionIDs);
                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBY", DbType.String, sortExpression);
                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBYDIRECTION",
                    DbType.String, sortDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                   "@CREATED_BY", DbType.Int32, userID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                TestSessionDetail testSession = null;
                List<CandidateTestSessionDetail> candidateTestSessions = null;
                while (dataReader.Read())
                {
                    // Instantiate the testSession instance
                    if (testSession == null)
                        testSession = new TestSessionDetail();

                    // Assign property values to the testSession object.
                    testSession.TestID = dataReader["TEST_KEY"].ToString().Trim();
                    testSession.TestType = Convert.ToInt32(dataReader["QUESTION_TYPE"].ToString());
                    testSession.TestSessionID = dataReader["SESSION_KEY"].ToString().Trim();
                    testSession.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    testSession.TimeLimit = Convert.ToInt32(dataReader["TIME_LIMIT"].ToString());
                    testSession.ExpiryDate = Convert.ToDateTime(dataReader["SESSION_EXPIRY"].ToString());
                    testSession.IsRandomizeQuestionsOrdering = (dataReader["RANDOM_ORDER"].ToString() == "Y" ? true : false);
                    testSession.IsCyberProctoringEnabled = (dataReader["CYBER_PROCTORING"].ToString() == "Y" ? true : false);
                    testSession.IsDisplayResultsToCandidate = (dataReader["DISPLAY_RESULT"].ToString() == "Y" ? true : false);
                    testSession.Instructions = dataReader["INSTRUCTIONS"].ToString();
                    testSession.TestSessionDesc = dataReader["TEST_SESSION_DESC"].ToString();
                    testSession.TestSessionAuthor = dataReader["SESSION_AUTHOR_NAME"].ToString().Trim();
                    testSession.TestSessionAuthorEmail = dataReader["SESSION_AUTHOR_EMAIL"].ToString().Trim();
                    testSession.RecommendedTimeLimit = Convert.ToInt32(dataReader["RECOMMENDED_TIME"].ToString());
                    testSession.TotalCredit = Convert.ToDecimal(dataReader["CREDIT"].ToString());
                    testSession.NumberOfCandidateSessions = Convert.ToInt32(dataReader["SESSION_COUNT"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        testSession.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        testSession.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        testSession.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        testSession.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString());
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the candidate test session collection.
                    if (candidateTestSessions == null)
                        candidateTestSessions = new List<CandidateTestSessionDetail>();
                    // Create a new candidate test session object. 
                    CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();
                    candidateTestSession.CandidateTestSessionID = dataReader["CAND_SESSION_KEY"].ToString();
                    candidateTestSession.Status = dataReader["STATUS"].ToString();
                    candidateTestSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());
                    candidateTestSession.RetakeRequest = dataReader["RETAKE_REQUEST"].ToString();
                    candidateTestSession.CancelReason = dataReader["CANCEL_REASON"].ToString();
                    candidateTestSession.EmailReminder = dataReader["DAILY_REMINDER"].ToString();
                    // If status is 'Not Scheduled', then don't assign the candidate informations
                    if (candidateTestSession.Status != "SESS_NSCHD")
                    {
                        candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                        candidateTestSession.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candidateTestSession.CandidateFirstName = dataReader["CANDIDATE_FIRSTNAME"].ToString().Trim();
                        candidateTestSession.CandidateFullName = dataReader["CANDIDATE_FULLNAME"].ToString().Trim();
                        candidateTestSession.Email = dataReader["EMAIL_ID"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                            candidateTestSession.DateCompleted = Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candidateTestSession.ScheduledDate =
                                Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        candidateTestSession.ModifiedDate =
                            Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                    candidateTestSessions.Add(candidateTestSession);
                }
                testSession.CandidateTestSessions = new List<CandidateTestSessionDetail>();
                testSession.CandidateTestSessions = candidateTestSessions;
                return testSession;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that will return test session and candidates session details
        /// for the given test session id or candidate session id/s.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that contains the test session id.
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="string"/> that contains the candidate session ids 
        /// separated by comma.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that contains the test session detail 
        /// with the list of CandidateTestSessionDetail instances.
        /// </returns>
        public TestSessionDetail GetTestSessionDetail(string testSessionID, string candidateSessionIDs, int userID)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_SESSION_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_KEY", DbType.String, testSessionID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@CAND_SESSION_KEYS", DbType.String, candidateSessionIDs);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                   "@CREATED_BY", DbType.Int32, userID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                TestSessionDetail testSession = null;
                List<CandidateTestSessionDetail> candidateTestSessions = null;
                while (dataReader.Read())
                {
                    // Instantiate the testSession instance
                    if (testSession == null)
                        testSession = new TestSessionDetail();

                    // Assign property values to the testSession object.
                    testSession.TestID = dataReader["TEST_KEY"].ToString().Trim();

                    testSession.TestSessionID = dataReader["SESSION_KEY"].ToString().Trim();
                    testSession.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    testSession.TimeLimit = Convert.ToInt32(dataReader["TIME_LIMIT"].ToString());
                    testSession.ExpiryDate = Convert.ToDateTime(dataReader["SESSION_EXPIRY"].ToString());
                    testSession.IsRandomizeQuestionsOrdering = (dataReader["RANDOM_ORDER"].ToString() == "Y" ? true : false);
                    testSession.IsCyberProctoringEnabled = (dataReader["CYBER_PROCTORING"].ToString() == "Y" ? true : false);
                    testSession.IsDisplayResultsToCandidate = (dataReader["DISPLAY_RESULT"].ToString() == "Y" ? true : false);
                    testSession.Instructions = dataReader["INSTRUCTIONS"].ToString();
                    testSession.TestSessionDesc = dataReader["TEST_SESSION_DESC"].ToString();
                    testSession.TestSessionAuthor = dataReader["SESSION_AUTHOR_NAME"].ToString().Trim();
                    testSession.TestSessionAuthorEmail = dataReader["SESSION_AUTHOR_EMAIL"].ToString().Trim();
                    testSession.RecommendedTimeLimit = Convert.ToInt32(dataReader["RECOMMENDED_TIME"].ToString());
                    testSession.TotalCredit = Convert.ToDecimal(dataReader["CREDIT"].ToString());
                    testSession.NumberOfCandidateSessions = Convert.ToInt32(dataReader["SESSION_COUNT"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        testSession.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        testSession.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        testSession.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        testSession.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"].ToString());
                }
                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Instantiate the candidate test session collection.
                    if (candidateTestSessions == null)
                        candidateTestSessions = new List<CandidateTestSessionDetail>();

                    // Create a new candidate test session object. 
                    CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();
                    candidateTestSession.CandidateTestSessionID = dataReader["CAND_SESSION_KEY"].ToString();
                    candidateTestSession.Status = dataReader["STATUS"].ToString();
                    candidateTestSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());
                    candidateTestSession.RetakeRequest = dataReader["RETAKE_REQUEST"].ToString();
                    candidateTestSession.CancelReason = dataReader["CANCEL_REASON"].ToString();

                    // If status is 'Not Scheduled', then don't assign the candidate informations
                    if (candidateTestSession.Status != "SESS_NSCHD")
                    {
                        candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                        candidateTestSession.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candidateTestSession.CandidateFirstName = dataReader["CANDIDATE_FIRSTNAME"].ToString().Trim();
                        candidateTestSession.Email = dataReader["EMAIL_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                            candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                            candidateTestSession.DateCompleted = Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candidateTestSession.ScheduledDate =
                                Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        candidateTestSession.ModifiedDate =
                            Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                    candidateTestSessions.Add(candidateTestSession);

                }
                testSession.CandidateTestSessions = new List<CandidateTestSessionDetail>();
                testSession.CandidateTestSessions = candidateTestSessions;

                return testSession;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method gets all the informations about test as well as 
        /// the candidates informations assigned to the test sessions.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="TestSessionDetail"/>Specifies the test session key
        /// </param>
        /// <param name="candidateSessionIDs">
        /// A <see cref="TestSessionDetail"/>Specifies the candidate session keys
        /// separated by comma.
        /// </param>
        /// <param name="sortingKey">
        /// A <see cref="string"/>Specifies the sorting Key
        /// </param>
        /// <param name="sortBy">
        /// A <see cref="SortType"/>Specifies the sort By Asc/Desc
        /// </param>
        /// <returns>
        /// TestSessionDetail class with list of CandidateTestSessionDetail instances
        /// </returns>
        public List<CandidateTestSessionDetail> GetCandidateTestSessions
            (string testSessionID, string candidateSessionIDs, string sortingKey, SortType sortBy)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_TEST_SESSION_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_KEY", DbType.String, testSessionID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@CAND_SESSION_KEYS", DbType.String, candidateSessionIDs);
                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBY",
                   DbType.String, sortingKey);

                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@ORDERBYDIRECTION", DbType.String,
                    sortBy == SortType.Ascending
                    ? Constants.SortTypeConstants.ASCENDING
                    : Constants.SortTypeConstants.DESCENDING);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                List<CandidateTestSessionDetail> candidateTestSessions = null;
                while (dataReader.Read())
                {
                    // Instantiate the candidate test session collection.
                    if (candidateTestSessions == null)
                        candidateTestSessions = new List<CandidateTestSessionDetail>();

                    // Create a new candidate test session object. 
                    CandidateTestSessionDetail candidateTestSession = new CandidateTestSessionDetail();
                    candidateTestSession.CandidateTestSessionID = dataReader["CAND_SESSION_KEY"].ToString();
                    candidateTestSession.Status = dataReader["STATUS"].ToString();
                    candidateTestSession.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());
                    candidateTestSession.RetakeRequest = dataReader["RETAKE_REQUEST"].ToString();
                    candidateTestSession.CancelReason = dataReader["CANCEL_REASON"].ToString();
                    //If status is 'Not Scheduled', then don't assign the candidate informations
                    if (candidateTestSession.Status != "SESS_NSCHD")
                    {
                        candidateTestSession.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                        candidateTestSession.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                        candidateTestSession.Email = dataReader["EMAIL_ID"].ToString();

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                            candidateTestSession.DateCompleted = Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_DATE"]))
                            candidateTestSession.ScheduledDate = Convert.ToDateTime(dataReader["SCHEDULED_DATE"].ToString());
                    }
                    candidateTestSessions.Add(candidateTestSession);
                }
                return candidateTestSessions;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method searches the test session details by passing TestName,
        /// TestKey,SessionKey,ClientRequestNumber or SchedulerName
        /// </summary>
        /// <param name="testSessionCriteria">
        /// A list of<see cref="TestSessionSearchCriteria"/> that holds the testSession Criteria
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>that holds the page Number
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/>that holds the page Size
        /// </param>
        /// <param name="totalRecords">
        ///  A <see cref="int"/>that holds the total Records
        /// </param>
        /// <param name="sortingKey">
        ///  A <see cref="string"/>that holds the sorting Key 
        /// </param>
        /// <param name="sortByDirection">
        ///  A <see cref="SortType"/>that holds the sortBy Direction Like 'A' or 'D'(Asc/dec)
        /// </param>
        /// <returns>
        ///  A list of <see cref="TestSessionDetail"/>that holds the Test Session Detail
        /// </returns>
        public List<TestSessionDetail> SearchTestSessionDetails
            (TestSessionSearchCriteria testSessionCriteria, int pageNumber,
            int pageSize, out int totalRecords,
            string sortingKey, SortType sortByDirection)
        {
            IDataReader dataReader = null;
            // Assing value to out parameter.
            totalRecords = 0;
            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_SESSION_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@TESTNAME", DbType.String, testSessionCriteria.TestName);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@TESTID", DbType.String, testSessionCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSIONID", DbType.String, testSessionCriteria.SessionKey);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SHEDULER_ID", DbType.String, testSessionCriteria.SchedulerNameID == 0 ? null : testSessionCriteria.SchedulerNameID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@SESSION_AUTHOR_ID", DbType.Int32, testSessionCriteria.TestSessionCreatorID == 0 ? null : testSessionCriteria.TestSessionCreatorID);

                if (testSessionCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestSessionCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSessionCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBY",
                    DbType.String, sortingKey);

                HCMDatabase.AddInParameter(getTestSessionCommand, "@ORDERBYDIRECTION",
                    DbType.String, sortByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                List<TestSessionDetail> testSessionCollection = null;

                TestSessionDetail testSession = null;
                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the testSession instance.
                        if (testSessionCollection == null)
                            testSessionCollection = new List<TestSessionDetail>();
                        testSession = new TestSessionDetail();
                        testSession.TestName = dataReader["TEST_NAME"].ToString();
                        testSession.TestID = dataReader["TEST_KEY"].ToString();
                        testSession.TestSessionID = dataReader["SESSION_KEY"].ToString();
                        testSession.TestSessionAuthor = dataReader["AUTHOR_NAME"].ToString();
                        testSession.TestSessionAuthorFullName = dataReader["FULLNAME"].ToString().Trim();
                        testSession.NumberOfQuestions = Convert.ToInt32(dataReader["TOTAL_QUESTION"].ToString());
                        testSession.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString());
                        testSession.TotalCredit = Convert.ToDecimal(dataReader["TEST_COST"].ToString());
                        //testSession.IsCertification = (dataReader["TEST_COST"].ToString() == "Y") ? true : false;
                        testSession.IsCertification = (dataReader["CERTIFICATION"].ToString() == "Y") ? true : false;

                        // Add the testSession to the collection.
                        testSessionCollection.Add(testSession);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return testSessionCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method passes the SessionKey and CandidateSession keys separated by comma 
        /// and return the invalid candidateSession keys.
        /// </summary>
        /// <param name="sessionKey">
        /// A <see cref="string"/>that holds the session Key
        /// </param>
        /// <param name="candidateSessionKeys">
        /// A <see cref="string"/>that holds the candidate Session Keys
        /// </param>
        /// <returns>
        /// A List of <see cref="String"/>that holds mismatched Candidate Ids.
        /// </returns>
        public List<string> ValidateTestSessionInput
            (string sessionKey, string candidateSessionKeys, int createdBy)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCommand = HCMDatabase.
                    GetStoredProcCommand("SPVALIDATE_TESTSESSIONDETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCommand,
                    "@SESSION_KEY", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCommand,
                    "@CAND_SESSION_KEYS", DbType.String, candidateSessionKeys);
                HCMDatabase.AddInParameter(getCommand,
                   "@CREATED_BY", DbType.Int32, createdBy);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCommand);

                List<string> mismatchedCandIdCollection = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {

                    // Instantiate the testSession instance.
                    if (mismatchedCandIdCollection == null)
                        mismatchedCandIdCollection = new List<string>();

                    if (dataReader["NOT_MATCHED_CAND_SESSION_KEY"].ToString() == "-1")
                    {
                        throw new Exception("Test Session Id not found");
                    }
                    // Add the testSession to the collection.
                    mismatchedCandIdCollection.Add(dataReader["NOT_MATCHED_CAND_SESSION_KEY"].ToString());

                }
                return mismatchedCandIdCollection;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method gets informations about Test reminders
        /// </summary>
        /// <param name="candidateSessionID">Specifies the test canditate session ID</param>
        /// <param name="attemptid">Specifies the candidate attemptid ID </param>
        /// <returns>
        /// A <see cref="TestScheduleDetail"/> that holds the candidate
        /// test Reminder details.
        /// </returns>
        public TestReminderDetail GetTestReminder(string candidateSessionID, int attemptid)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_REMINDER_DETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@CANDIDATE_SESSIONID", DbType.String, candidateSessionID);
                HCMDatabase.AddInParameter(getTestSessionCommand,
                    "@ATTEMPT_ID", DbType.Int16, attemptid);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionCommand);

                TestReminderDetail testReminder = new TestReminderDetail();

                if (dataReader.Read())
                {
                    testReminder.TestID = dataReader["TEST_KEY"].ToString();
                    testReminder.TestDescription = dataReader["TEST_DESCRIPTION"].ToString();
                    testReminder.TestName = dataReader["TEST_NAME"].ToString();
                    testReminder.ExpiryDate = Convert.ToDateTime(dataReader["SCHEDULED_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["REMINDER_DATE"]))
                        testReminder.ReminderDate = Convert.ToDateTime(dataReader["REMINDER_DATE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY"]))
                        testReminder.SchedulerID = Convert.ToInt32(dataReader["SCHEDULED_BY"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULED_BY_NAME"]))
                        testReminder.SchedulerName = dataReader["SCHEDULED_BY_NAME"].ToString();
                }
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (testReminder.AttributeList == null)
                        testReminder.AttributeList = new List<string>();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVAL_ID"]))
                    {
                        testReminder.AttributeList.Add(dataReader["INTERVAL_ID"].ToString().Trim());
                    }

                }
                return testReminder;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of test reminder sender list for the
        /// given date/time and relative time differences. Mails needs to be
        /// sent for this list.
        /// </summary>
        /// <param name="currentDateTime">
        /// A <see cref="DateTime"/> that holds the current date and time.
        /// </param>
        /// <param name="relativeTimeDifference">
        /// A <see cref="int"/> that holds the relative time difference. This
        /// will be validated against +/- time difference of reminder time.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestReminderDetail"/> that holds the senders 
        /// list.
        /// </returns>
        public List<TestReminderDetail> GetTestReminderSenderList
            (DateTime currentDateTime, int relativeTimeDifference)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getSenderList = HCMDatabase.
                    GetStoredProcCommand("SPGET_TEST_REMINDER_SENDER_LIST");

                // Add input parameters.
                HCMDatabase.AddInParameter(getSenderList,
                    "@CURRENT_DATE_TIME", DbType.String, currentDateTime);
                HCMDatabase.AddInParameter(getSenderList,
                    "@RELATIVE_TIME_DIFFERENCE_SECONDS", DbType.Int32, relativeTimeDifference);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getSenderList);

                List<TestReminderDetail> senderList = null;

                while (dataReader.Read())
                {
                    if (senderList == null)
                        senderList = new List<TestReminderDetail>();

                    TestReminderDetail senderDetail = new TestReminderDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_SESSION_KEY"]))
                        senderDetail.CandidateSessionID = dataReader["CAND_SESSION_KEY"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        senderDetail.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        senderDetail.EmailID = dataReader["EMAIL_ID"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVAL_ID"]))
                        senderDetail.IntervalID = dataReader["INTERVAL_ID"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVAL_DESCRIPTION"]))
                        senderDetail.IntervalDescription = dataReader["INTERVAL_DESCRIPTION"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        senderDetail.TestID = dataReader["TEST_KEY"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        senderDetail.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["EXPECTED_DATE"]))
                        senderDetail.ExpectedDate = Convert.ToDateTime(dataReader["EXPECTED_DATE"].ToString().Trim());
                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        senderDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        senderDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    // Add the detail to the list.
                    senderList.Add(senderDetail);
                }
                return senderList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }



        /// <summary>
        /// Method that will update the reminder sent status for the given 
        /// candidate, attempt number and interval ID.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="intervalID">
        /// A <see cref="string"/> that holds the interval ID.
        /// </param>
        /// <param name="reminderSent">
        /// A <see cref="bool"/> that holds the reminder sent status.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void UpdateTestReminderStatus(string candidateSessionID,
            int attemptID, string intervalID, bool reminderSent, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateReminderCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_TEST_REMINDER_SENT_STATUS");
            //Add input parameters.
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@CAND_SESSION_KEY", DbType.String, candidateSessionID);
            HCMDatabase.AddInParameter(updateReminderCommand,
               "@ATTEMPT_ID", DbType.Int32, attemptID);
            HCMDatabase.AddInParameter(updateReminderCommand,
               "@INTERVAL_ID", DbType.String, intervalID);
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@REMINDER_SENT", DbType.String, reminderSent == true ? "Y" : "N");
            HCMDatabase.AddInParameter(updateReminderCommand,
                "@USER_ID", DbType.Int32, userID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateReminderCommand);
        }



        /// <summary>
        /// This method handles the new Reminder entry functions
        /// </summary>
        /// <param name="reminderDetail">Contains Reminder detail object</param>
        /// <param name="transaction">Holds the transaction object</param>
        public void InsertReminder(TestReminderDetail reminderDetail, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_TEST_REMINDER");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CAND_SESSION_KEY", DbType.String, reminderDetail.CandidateSessionID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ATTEMPT_ID", DbType.Int16, reminderDetail.AttemptID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@INTERVAL_ID", DbType.String, reminderDetail.IntervalID.ToString());
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@SCHEDULED_DATE", DbType.DateTime, reminderDetail.ReminderDate);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@USER_ID", DbType.Int16, reminderDetail.UserID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method handles the update Reminder entry functions
        /// </summary>
        /// <param name="reminderDetail">Contains Reminder detail object</param>
        /// <param name="transaction">Holds the transaction object</param>
        public void UpdateReminder(TestReminderDetail reminderDetail, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_TEST_REMINDER");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CAND_SESSION_KEY", DbType.String, reminderDetail.CandidateSessionID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ATTEMPT_ID", DbType.Int16, reminderDetail.AttemptID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@INTERVAL_ID", DbType.String, reminderDetail.IntervalID.ToString());
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@SCHEDULED_DATE", DbType.DateTime, reminderDetail.ReminderDate);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@USER_ID", DbType.Int16, reminderDetail.UserID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method handles the Reminder Delete functions
        /// </summary>
        /// <param name="reminderDetail">Contains  reminder properties</param>
        /// <param name="transaction">Contains  reminder properties</param>
        public void DeleteReminder(TestReminderDetail reminderDetail, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_TEST_REMINDER");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CAND_SESSION_KEY", DbType.String, reminderDetail.CandidateSessionID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@INTERVAL_ID", DbType.String, reminderDetail.IntervalID.ToString());
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ATTEMPT_ID", DbType.Int16, reminderDetail.AttemptID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method deletes all reminder details for specific candidate id
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds candidate session id
        /// </param>
        /// <param name="attemptid">
        /// A <see cref="int"/> that holds attempt id
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds transaction obejct
        /// </param>
        public void DeleteReminderAll(string candidateSessionID, int attemptid, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ALL_TEST_REMINDERS");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CAND_SESSION_KEY", DbType.String, candidateSessionID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@ATTEMPT_ID", DbType.Int16, attemptid);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Represents the method to check whether the same candidate
        /// has been assigned for the test session ID
        /// </summary>
        /// <param name="testSessionID">A
        /// <see cref="string"/>that holds the test session ID
        /// </param>
        /// <param name="candidateID">
        /// A<see cref="string"/>that holds the candidate ID
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds  candidate already assigned scheduler name
        /// </returns>
        public string CheckCandidateAlreadyAssigned(string testSessionID, string candidateID)
        {



            try
            {

                // Create a stored procedure command object.
                DbCommand CheckcandidateCommand = HCMDatabase.
                    GetStoredProcCommand("SPCHECK_CANDIDATE_EXIST");
                // Add input parameters.
                HCMDatabase.AddInParameter(CheckcandidateCommand,
                    "@TEST_SESSION_KEY", DbType.String, testSessionID);
                HCMDatabase.AddInParameter(CheckcandidateCommand,
                    "@CANDIDATE_ID", DbType.Int32, Convert.ToInt32(candidateID.Trim()));

                object schedulerName = null;
                string result = string.Empty;

                schedulerName = HCMDatabase.ExecuteScalar(CheckcandidateCommand);

                return schedulerName == null ? result : schedulerName.ToString();
            }
            finally
            {

            }
        }

        /// <summary>
        /// Method that retrieves the candidate open text response detail.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="CandidateOpenTextResponseDetail"/> that holds the candidate
        /// open text response detail.
        /// </returns>
        public List<CandidateOpenTextResponseDetail> GetCandidateOpenTextResponse
            (string candidateTestSessionID, int attemptID)
        {
            if (Utility.IsNullOrEmpty(candidateTestSessionID))
                throw new Exception("Candidate test session ID cannot be empty");

            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_OPEN_TEXT_TEST_RESULT");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@CANDIDATE_SESSION_KEY", DbType.String, candidateTestSessionID);

                HCMDatabase.AddInParameter(getCandidateSession,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);
                List<CandidateOpenTextResponseDetail> details = new List<CandidateOpenTextResponseDetail>();

                int sno = 1;
                while (dataReader.Read())
                {
                    CandidateOpenTextResponseDetail detail = new CandidateOpenTextResponseDetail();
                    detail.QuestionSNo = sno++;

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]))
                    {
                        detail.QuestionKey = dataReader["QUESTION_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                    {
                        detail.QuestionDescription = dataReader["QUESTION_DESC"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER"]))
                    {
                        detail.Answer = dataReader["ANSWER"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER_REFERENCE"]))
                    {
                        detail.AnswerReference = dataReader["ANSWER_REFERENCE"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["MARKS"]) && dataReader["MARKS"] != DBNull.Value)
                    {
                        detail.Marks = Convert.ToInt32(dataReader["MARKS"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["MARKS_OBTAINED"]) && dataReader["MARKS_OBTAINED"] != DBNull.Value)
                    {
                        detail.MarksObtained = Convert.ToInt32(dataReader["MARKS_OBTAINED"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS"]) && dataReader["STATUS"] != DBNull.Value)
                    {
                        detail.Status = dataReader["STATUS"].ToString().Trim();
                    }

                    details.Add(detail);
                }

                return details;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the candidate open text response.
        /// </summary>
        public void UpdateCandidateOpenTextResponseScore(string candidateSessionKey, int attemptID,
            CandidateOpenTextResponseDetail response, IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertQuestionCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CANDIDATE_OPEN_TEXT_TEST_RESULT");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@CANDIDATE_SESSION_KEY", DbType.String, candidateSessionKey);
            HCMDatabase.AddInParameter(insertQuestionCommand,
                "@ATTEMPT_ID", DbType.Int16, attemptID);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@QUESTION_KEY", DbType.String, response.QuestionKey);
            HCMDatabase.AddInParameter(insertQuestionCommand,
               "@MARKS_OBTAINED", DbType.Int16, response.MarksObtained);

            HCMDatabase.ExecuteNonQuery(insertQuestionCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Update the candidate session status.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attempID">
        /// A <see cref="int"/> that contains the attempt ID.
        /// </param>
        public void UpdateCandidateSessionStatus(string candidateSessionKey, int attempID, string sessionStatus, int modifiedBy)
        {
            // Create a stored procedure command object.
            DbCommand updateCommand = HCMDatabase.
                 GetStoredProcCommand("SPUPDATE_CANDIDATE_SESSION_TRACKING_STATUS");

            updateCommand.CommandTimeout = 0;

            HCMDatabase.AddInParameter(updateCommand,
                "@CAND_SESSION_KEY", System.Data.DbType.String, candidateSessionKey);

            HCMDatabase.AddInParameter(updateCommand,
                "@ATTEMPT_ID", System.Data.DbType.Int32, attempID);

            HCMDatabase.AddInParameter(updateCommand,
               "@SESSION_STATUS", System.Data.DbType.String, sessionStatus);

            HCMDatabase.AddInParameter(updateCommand,
                "@MODIFIED_BY", System.Data.DbType.Int32, modifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCommand);
        }


        public DataTable GetCandidateInformationByIDs(string candidateIDs, string testSessionKey)
        {
            DbCommand selectCommand = HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATESBYIDS");

            HCMDatabase.AddInParameter(selectCommand,
                "@CANDIDATE_USER_IDS", DbType.String, candidateIDs);

            HCMDatabase.AddInParameter(selectCommand,
                "@TEST_SESSION_KEY", DbType.String, testSessionKey);

            DataSet dataSet = HCMDatabase.ExecuteDataSet(selectCommand);

            if (dataSet == null || dataSet.Tables == null
                || dataSet.Tables.Count <= 0 || dataSet.Tables[0].Rows.Count <= 0)
            {
                return null;
            }
            else
               return dataSet.Tables[0];
        }

        #endregion


    }
}