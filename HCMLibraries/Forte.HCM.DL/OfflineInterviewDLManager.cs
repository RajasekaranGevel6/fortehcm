﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
 

namespace Forte.HCM.DL
{
    public class OfflineInterviewDLManager : DatabaseConnectionManager
    {
        public bool UpdateInterviewConductionStatus(OfflineInterviewParams interviewParams)
        {
            IDataReader reader = null;

            try
            {
                using (DbCommand command = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_OFFLINE_INTERVIEW_CONDUCTION_STATUS"))
                {
                    command.CommandTimeout = 1500;

                    // Add input parameters.
                    HCMDatabase.AddInParameter(command,
                        "@CAND_INTERVIEW_SESSION_KEY", DbType.String,
                        interviewParams.candidateSessionID);

                    HCMDatabase.AddInParameter(command,
                        "@ATTEMPT_ID", DbType.Int16, interviewParams.attemptID);

                    HCMDatabase.AddInParameter(command,
                        "@INTERVIEW_SESSION_TRACKING_STATUS", DbType.String,
                        interviewParams.sessionTrackingStatus);

                    HCMDatabase.AddInParameter(command,
                        "@INTERVIEW_SESSION_CANDIDATE_STATUS", DbType.String,
                        interviewParams.sessionCandidateStatus);

                    HCMDatabase.AddInParameter(command,
                        "@MODIFIED_BY", DbType.Int32, interviewParams.candidateID);

                    // Execute the stored procedure.
                    reader = HCMDatabase.ExecuteReader(command);

                    if (reader.Read())
                    {
                        if (!DBNull.Value.Equals(reader["IS_TEST_INPROGRESS"]) &&
                            reader["IS_TEST_INPROGRESS"].ToString() == "Y")
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    reader.Close();
                    return false;
                }
            }
            catch (Exception exp)
            {
                 //Logger.ExceptionLog(exp);
                throw exp;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public DataTable GetQuestions(OfflineInterviewParams interviewParams)
        {
            try
            {
                using (DbCommand getQuestions = HCMDatabase.
                    GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_QUESTIONS"))
                {
                    getQuestions.CommandTimeout = 1500;

                    HCMDatabase.AddInParameter(getQuestions,
                        "@CAND_INTERVIEW_SESSION_KEY", DbType.String,
                        interviewParams.candidateSessionID);

                    HCMDatabase.AddInParameter(getQuestions,
                        "@ATTEMPT_ID", DbType.Int32, interviewParams.attemptID);

                    // Execute the dataset.
                    DataSet dataSet = HCMDatabase.ExecuteDataSet(getQuestions);

                    if (dataSet == null || dataSet.Tables == null || dataSet.Tables.Count == 0 ||
                        dataSet.Tables[0].Rows == null || dataSet.Tables[0].Rows.Count == 0)
                    {
                        return null;
                    }

                    return dataSet.Tables[0];
                }
            }
            catch (Exception exp)
            {
                //Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        public bool UpdateInterviewTestResultFact(OfflineInterviewParams interviewParams)
        {
            try
            {
                using (DbCommand command = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_INTERVIEW_CANDIDATE_TEST_RESULT_FACT"))
                {
                    command.CommandTimeout = 1500;

                    // Add input parameters.
                    HCMDatabase.AddInParameter(command,
                        "@CAND_INTERVIEW_SESSION_KEY", DbType.String,
                        interviewParams.candidateSessionID);

                    HCMDatabase.AddInParameter(command,
                        "@ATTEMPT_ID", DbType.Int16, interviewParams.attemptID);

                    HCMDatabase.AddInParameter(command,
                        "@QUESTION_KEY", DbType.String, interviewParams.questionKey);

                    HCMDatabase.AddInParameter(command,
                        "@COMMENTS", DbType.String, interviewParams.comments);

                    HCMDatabase.AddInParameter(command,
                        "@STATUS", DbType.String, interviewParams.status);

                    HCMDatabase.AddInParameter(command,
                        "@START_TIME", DbType.DateTime, interviewParams.startTime);

                    HCMDatabase.AddInParameter(command,
                        "@END_TIME", DbType.String, interviewParams.endTime);

                    HCMDatabase.AddInParameter(command,
                        "@SKIPPED", DbType.String, interviewParams.skipped ? "Y" : "N");

                    HCMDatabase.AddInParameter(command,
                        "@MODIFIED_BY", DbType.Int32, interviewParams.candidateID);

                    // Execute the stored procedure.
                    HCMDatabase.ExecuteNonQuery(command);
                }
                return true;
            }
            catch (Exception exp)
            {
                return false; 
                throw exp; 
            }
        }

        public bool UpdateInterviewTest(OfflineInterviewParams interviewParams)
        {
            try
            {
                string sessionStatus = null;
                string attemptStatus = null; 

                switch (interviewParams.statusType)
                {
                    case "TransmitCompleted":
                        sessionStatus = "SESS_END";
                        attemptStatus = "ATMPT_COMP"; 
                        break;

                    case "AbandonInterview":
                        sessionStatus = "SESS_QUIT";
                        attemptStatus = "ATMPT_QUIT"; 
                        break;

                    case "Completed":
                        sessionStatus = "SESS_COMP";
                        attemptStatus = "ATMPT_COMP"; 
                        break;

                    case "Paused":
                        sessionStatus = "SESS_PAUS";
                        attemptStatus = "ATMPT_PAUS"; 
                        break;

                    default:
                        break;
                }

                UpdateInterviewTestResult(interviewParams.candidateSessionID,
                    interviewParams.attemptID,
                    interviewParams.testKey, interviewParams.startTime,
                    interviewParams.endTime, sessionStatus,
                    interviewParams.questionsViewed, interviewParams.
                    questionNotViewed, interviewParams.candidateID);

                UpdateInterviewSessionStatus(interviewParams.candidateSessionID,
                    interviewParams.attemptID, attemptStatus, sessionStatus, interviewParams.candidateID);

                if (interviewParams.statusType != "AbandonInterview")
                {
                    UpdateInterviewAssessmentTrackingStatus(interviewParams.candidateSessionID,
                        interviewParams.attemptID, "ASS_INIT", interviewParams.candidateID);
                }

                return true;
            }
            catch
            {                
                return false;
            }
        }

        /// <summary>
        /// Method that updates the interview test results.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="startTime">
        /// A <see cref="DateTime"/> that holds the start time.
        /// </param>
        /// <param name="endTime">
        /// A <see cref="DateTime"/> that holds the end time.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the status.
        /// </param>
        /// <param name="totalQuestionViewed">
        /// A <see cref="int"/> that holds the total question viewed count.
        /// </param>
        /// <param name="totalQuestionNotViewed">
        /// A <see cref="int"/> that holds the total question not viewed count.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        public void UpdateInterviewTestResult(string candidateSessionID,
            int attemptID, string testKey, DateTime startTime, DateTime endTime,
            string status, int totalQuestionViewed, int totalQuestionNotViewed,
            int candidateID)
        {
            try
            {
                // Get candidate result key.
                string candidateResultKey = "";//GetNextNumber("CTR", candidateID);

                using (DbCommand command = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_INTERVIEW_CANDIDATE_TEST_RESULT"))
                {
                    command.CommandTimeout = 150;

                    // Add input parameters.
                    HCMDatabase.AddInParameter(command,
                        "@CANDIDATE_RESULT_KEY", DbType.String, candidateResultKey);
                    HCMDatabase.AddInParameter(command,
                        "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);
                    HCMDatabase.AddInParameter(command,
                        "@ATTEMPT_ID", DbType.Int16, attemptID);
                    HCMDatabase.AddInParameter(command,
                        "@INTERVIEW_TEST_KEY", DbType.String, testKey);
                    HCMDatabase.AddInParameter(command,
                       "@START_TIME", DbType.DateTime, startTime);
                    HCMDatabase.AddInParameter(command,
                        "@END_TIME", DbType.DateTime, endTime);
                    HCMDatabase.AddInParameter(command,
                        "@STATUS", DbType.String, status);
                    HCMDatabase.AddInParameter(command,
                        "@TOTAL_QUESTION_VIEWED", DbType.Int32, totalQuestionViewed);
                    HCMDatabase.AddInParameter(command,
                        "@TOTAL_QUESTION_NOT_VIEWED", DbType.Int32, totalQuestionNotViewed);
                    HCMDatabase.AddInParameter(command,
                        "@USER_ID", DbType.Int32, candidateID);

                    // Execute the stored procedure.
                    HCMDatabase.ExecuteNonQuery(command);
                }
            }
            catch (Exception exp)
            {
                //Logger.ExceptionLog(exp);
                throw exp;
            }
        }
      
        /// <summary>
        /// Method that updates the interview session status.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="sessionTrackingStatus">
        /// A <see cref="string"/> that holds the session tracking status.
        /// </param>
        /// <param name="sessionCandidateStatus">
        /// A <see cref="string"/> that holds the session candidate status.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        public void UpdateInterviewSessionStatus(string candidateSessionID,
            int attemptID, string sessionTrackingStatus, string sessionCandidateStatus,
            int candidateID)
        {
            try
            {
                using (DbCommand command = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_OFFLINE_INTERVIEW_SESSION_STATUS"))
                {
                    command.CommandTimeout = 1500;

                    // Add input parameters.
                    HCMDatabase.AddInParameter(command,
                        "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);
                    HCMDatabase.AddInParameter(command,
                        "@ATTEMPT_ID", DbType.Int16, attemptID);
                    HCMDatabase.AddInParameter(command,
                        "@INTERVIEW_SESSION_TRACKING_STATUS", DbType.String, sessionTrackingStatus);
                    HCMDatabase.AddInParameter(command,
                        "@INTERVIEW_SESSION_CANDIDATE_STATUS", DbType.String, sessionCandidateStatus);
                    HCMDatabase.AddInParameter(command,
                        "@MODIFIED_BY", DbType.Int32, candidateID);

                    // Execute the stored procedure.
                    HCMDatabase.ExecuteNonQuery(command);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Method that updates the interview assessment tracking status.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="assessmentStatus">
        /// A <see cref="string"/> that holds the session tracking status.
        /// </param>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        public void UpdateInterviewAssessmentTrackingStatus(string candidateSessionID,
            int attemptID, string assessmentStatus, int candidateID)
        {
            try
            {
                using (DbCommand command = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_INTERVIEW_CANDIDATE_ASSESSMENT_TRACKING_STATUS"))
                {
                    command.CommandTimeout = 1500;

                    // Add input parameters.
                    HCMDatabase.AddInParameter(command,
                        "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);
                    HCMDatabase.AddInParameter(command,
                        "@ATTEMPT_ID", DbType.Int16, attemptID);
                    HCMDatabase.AddInParameter(command,
                        "@ASSESMENT_STATUS", DbType.String, assessmentStatus);
                    HCMDatabase.AddInParameter(command,
                        "@MODIFIED_BY", DbType.Int32, candidateID);

                    // Execute the stored procedure.
                    HCMDatabase.ExecuteNonQuery(command);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Represents the method that calls the stored procedure <b>SPGET_NEXT_NUMBER </b> 
        /// and retrieves the next number for the given object type.
        /// </summary>
        /// <param name="transaction">
        /// A <seealso cref="IDbTransaction"/> that holds the transaction object.
        /// </param>
        /// <param name="objectType">
        /// A <seealso cref="string"/> that holds the object type.
        /// </param>
        /// <param name="userID">
        /// A <seealso cref="int"/> that holds the user ID. 
        /// </param>
        private string GetNextNumber(string objectType, int userID)
        {
            try
            {
                // Check if object type is given.
                if (objectType == null || objectType == "")
                {
                    throw new Exception
                        ("Object type cannot be null or empty");
                }

                // This SP helps to generate Question ID 
                DbCommand nextNumberCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_NEXT_NUMBER_OUTPUT");

                HCMDatabase.AddInParameter(
                    nextNumberCommand,"@OBJ_TYPE",DbType.String,objectType.Trim());

                HCMDatabase.AddInParameter(nextNumberCommand,"@MODIFIED_BY",DbType.Int32, userID);

                HCMDatabase.AddOutParameter(nextNumberCommand,"@NEXT_NUMBER",DbType.String, 18);

                object returnValue
                    = HCMDatabase.ExecuteNonQuery(nextNumberCommand);

                returnValue = HCMDatabase.GetParameterValue(nextNumberCommand, "@NEXT_NUMBER");

                if (returnValue == null || returnValue.ToString().Trim().Length == 0)
                    return null;

                return returnValue.ToString().Trim();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        } 

        public DataTable GetDisclaimerMessages()
        {
            try
            {    
                using (DbCommand getQuestions = HCMDatabase.
                    GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_INSTRUCTIONS_DISCLAIMER_MESSAGES"))
                {
                    getQuestions.CommandTimeout = 1500;

                    // Execute the dataset.
                    DataSet dataSet = HCMDatabase.ExecuteDataSet(getQuestions);

                    if (dataSet == null || dataSet.Tables == null || dataSet.Tables.Count == 0 ||
                        dataSet.Tables[0].Rows == null || dataSet.Tables[0].Rows.Count == 0)
                    {
                        return null;
                    }

                    return dataSet.Tables[0];
                }
            }
            catch (Exception exp)
            { 
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the candidate session detail for the given
        /// candidate test session ID and attempt ID.
        /// </summary>
        /// <param name="candidateTestSessionID">
        /// A <see cref="string"/> that holds the candidate test session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the candidate test attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the candidate
        /// interview session details.
        /// </returns>
        public CandidateInterviewSessionDetail GetCandidateInterviewSessionDetail
            (string candidateTestSessionID, int attemptID)
        {
            if (Utility.IsNullOrEmpty(candidateTestSessionID))
                throw new Exception("Candidate test session ID cannot be empty");

            if (attemptID == 0)
                throw new Exception("Attempt ID cannot be zero");

            CandidateInterviewSessionDetail session = null;
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCandidateSession = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_INTERVIEW_SESSION_EMAIL_DETAIL");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCandidateSession,
                    "@CANDIDATE_INTERVIEW_SESSION_ID", DbType.String, candidateTestSessionID);

                HCMDatabase.AddInParameter(getCandidateSession,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCandidateSession);

                if (dataReader.Read())
                {
                    session = new CandidateInterviewSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                    {
                        session.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]))
                    {
                        session.CandidateTestSessionID = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_SESSION_KEY"]))
                    {
                        session.InterviewTestSessionID = dataReader["INTERVIEW_SESSION_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        session.CandidateID = Convert.ToString(dataReader["CANDIDATE_ID"]).Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["AUTH_CODE"]))
                    {
                        session.SecurityCode = dataReader["AUTH_CODE"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        session.CandidateName = dataReader["CANDIDATE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_EMAIL"]))
                    {
                        session.CandidateEmail = dataReader["CANDIDATE_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_OWNER_EMAIL"]))
                    {
                        session.CandidateOwnerEmail = dataReader["CANDIDATE_OWNER_EMAIL"].ToString().Trim();
                    }
                    session.DateCompleted = DateTime.Now;
                    /*if (!Utility.IsNullOrEmpty(dataReader["COMPLETED_DATE"]))
                    {
                        session.DateCompleted = Convert.ToDateTime(dataReader["COMPLETED_DATE"]);
                    }*/
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_EMAIL"]))
                    {
                        session.SchedulerEmail = dataReader["SCHEDULER_EMAIL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SCHEDULER_COMPANY"]))
                    {
                        session.SchedulerCompany = dataReader["SCHEDULER_COMPANY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_OWNER_EMAIL"]))
                    {
                        session.PositionProfileOwnerEmail = dataReader["POSITION_PROFILE_OWNER_EMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_EMAILS"]))
                    {
                        session.AssessorEmails = dataReader["ASSESSOR_EMAILS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                    {
                        session.InterviewTestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_KEY"]))
                    {
                        session.InterviewTestKey = dataReader["INTERVIEW_TEST_KEY"].ToString().Trim();
                    }
                }

                return session;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the interview details.
        /// <returns>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="DataTable"/> that holds the interview details.
        /// </returns>
        public DataTable GetInterviewDetails(OfflineInterviewParams interviewParams)
        {
            try
            {
                using (DbCommand getInterviewDetails = HCMDatabase.
                    GetStoredProcCommand("SPGET_OFFLINE_INTERVIEW_DETAIL"))
                {
                    getInterviewDetails.CommandTimeout = 1500;

                    HCMDatabase.AddInParameter(getInterviewDetails,
                        "@CAND_INTERVIEW_SESSION_KEY", DbType.String, interviewParams.candidateSessionID);

                    HCMDatabase.AddInParameter(getInterviewDetails,
                        "@ATTEMPT_ID", DbType.Int32, interviewParams.attemptID);

                    // Execute the dataset.
                    DataSet dataSet = HCMDatabase.ExecuteDataSet(getInterviewDetails);

                    if (dataSet == null || dataSet.Tables == null || dataSet.Tables.Count == 0 ||
                        dataSet.Tables[0].Rows == null || dataSet.Tables[0].Rows.Count == 0)
                    {
                        return null;
                    }

                    return dataSet.Tables[0];
                }
            }
            catch (Exception exp)
            {
                //Logger.ExceptionLog(exp);
                throw exp;
            }
        }
    }
}
