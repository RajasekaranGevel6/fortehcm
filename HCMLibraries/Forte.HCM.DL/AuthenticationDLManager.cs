﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AuthenticationDLManager.cs
// File that represents the data layer for the Authentication and 
// Authorization module. This includes functionalities for retrieving 
// the authenticate user, role based authorized user, etc. This will talk
// to the database for performing these operations

#endregion

#region Directives
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using System.Text;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Authentication and 
    /// Authorization module. This includes functionalities for retrieving 
    /// the authenticate user, role based authorized user, etc. This will talk
    /// to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class AuthenticationDLManager : DatabaseConnectionManager
    {
        #region Public Methods

        /// <summary>
        /// Method that validates the user and returns the user type and 
        /// session key.
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the User details
        /// </returns>
        public string GetSiteUserDetail(string userName, string password)
        {
            IDataReader datareader = null;

            try
            {
                // Create command.
                DbCommand getUserCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_AUTHENTICATE_SITE_USER");

                // Add input parameters.
                HCMDatabase.AddInParameter(getUserCommand,
                    "@USER_NAME", DbType.String, userName);
                HCMDatabase.AddInParameter(getUserCommand,
                    "@PASSWORD", DbType.String, password);

                // Execute the query.
                datareader = HCMDatabase.ExecuteReader(getUserCommand);

                if (datareader.Read())
                {
                    string userType = string.Empty;
                    if (datareader["USER_TYPE"] != null && datareader["USER_TYPE"] != DBNull.Value)
                        userType = datareader["USER_TYPE"].ToString().Trim();

                    string sessionKey = string.Empty;
                    if (datareader["SESSION_KEY"] != null && datareader["SESSION_KEY"] != DBNull.Value)
                        sessionKey = datareader["SESSION_KEY"].ToString().Trim();

                    // Close the data reader.
                    datareader.Close();

                    // Compose and return the user type and session key, delimited by
                    // # symbol.
                    return userType + "#" + sessionKey;
                }
                else
                {
                    // Invalid user name or password.
                    return string.Empty;
                }
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }
       
        /// <summary>
        /// Validating the User
        /// </summary>
        /// <param name="authenticate">
        /// A <see cref="string"/> that holds the authenticate user name and password
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the User details
        /// </returns>
        public UserDetail GetAuthenticateUser(Authenticate authenticate)
        {
            IDataReader datareader = null;
            try
            {
                DbCommand getAuthenticateCommand = HCMDatabase.GetStoredProcCommand("SPGET_CHECK_AUTHENTICATEUSER");
                HCMDatabase.AddInParameter(getAuthenticateCommand, "@USER_NAME", DbType.String, authenticate.UserName);
                HCMDatabase.AddInParameter(getAuthenticateCommand, "@PASSWORD", DbType.String, authenticate.Password);
                datareader = HCMDatabase.ExecuteReader(getAuthenticateCommand);
                UserDetail userDetail = new UserDetail();
                while (datareader.Read())
                {
                    userDetail.UserID = Convert.ToInt32(datareader["USERID"]);
                    userDetail.UserName = datareader["USER_NAME"].ToString();
                    userDetail.FirstName = datareader["USER_FIRST_NAME"].ToString();
                    userDetail.MiddleName = datareader["USER_MIDDLE_NAME"].ToString();
                    userDetail.LastName = datareader["USER_LAST_NAME"].ToString();
                    userDetail.Email = datareader["USER_EMAIL"].ToString();
                    userDetail.TenantID = Convert.ToInt32(datareader["TENANT_ID"]);
                    userDetail.LastLogin = Convert.ToDateTime(datareader["USER_LAST_LOGIN_TIME"]);
                    if (!Utility.IsNullOrEmpty(datareader["SUBSCRIPTION_ROLE"]))
                        userDetail.SubscriptionRole = datareader["SUBSCRIPTION_ROLE"].ToString();
                    if (!Utility.IsNullOrEmpty(datareader["SUBSCRIPTION_ID"]))
                        userDetail.SubscriptionId = Convert.ToInt32(datareader["SUBSCRIPTION_ID"]);
                    if (!Utility.IsNullOrEmpty(datareader["USER_TYPE"]))
                    {
                        if (datareader["USER_TYPE"].ToString().ToUpper().Trim() == "UT_ST_ADM")
                            userDetail.UserType = UserType.SiteAdmin;
                    }

                    userDetail.LegalAccepted = false;
                    if (!Utility.IsNullOrEmpty(datareader["LEGAL_ACCEPTED"]) && 
                        datareader["LEGAL_ACCEPTED"].ToString().ToUpper().Trim() == "Y")
                    {
                        userDetail.LegalAccepted = true;
                    }
                    
                    datareader.NextResult();
                    while (datareader.Read())
                    {
                        if (userDetail.Roles == null)
                            userDetail.Roles = new List<UserRole>();

                        if (!Utility.IsNullOrEmpty(datareader["ROLE_CODE"]))
                        {
                            switch (datareader["ROLE_CODE"].ToString())
                            {
                                case Constants.RoleCodeConstants.CONTENT_AUTHOR:
                                    userDetail.Roles.Add(UserRole.ContentAuthor);
                                    break;
                                case Constants.RoleCodeConstants.CONTENT_VALIDATOR:
                                    userDetail.Roles.Add(UserRole.ContentValidator);
                                    break;
                                case Constants.RoleCodeConstants.TEST_AUTHOR:
                                    userDetail.Roles.Add(UserRole.TestAuthor);
                                    break;
                                case Constants.RoleCodeConstants.DELIVERY_MANAGER:
                                    userDetail.Roles.Add(UserRole.DeliveryManager);
                                    break;
                                case Constants.RoleCodeConstants.RECRUITER:
                                    userDetail.Roles.Add(UserRole.Recruiter);
                                    break;
                                case Constants.RoleCodeConstants.CANDIDATE:
                                    userDetail.Roles.Add(UserRole.Candidate);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_USERSETUP:
                                    userDetail.Roles.Add(UserRole.AdminUserSetup);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_OTM_TS:
                                    userDetail.Roles.Add(UserRole.AdminOTMTS);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_CRD_MGMT:
                                    userDetail.Roles.Add(UserRole.AdminCreditsManagement);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_CAND_MGMT:
                                    userDetail.Roles.Add(UserRole.AdminCandidateManagement);
                                    break;
                                case Constants.RoleCodeConstants.ADMINISTRATOR:
                                    userDetail.Roles.Add(UserRole.AdminSystem);
                                    break;
                                case Constants.RoleCodeConstants.FREE_SUBS:
                                    userDetail.Roles.Add(UserRole.FreeSubscription);
                                    break;
                                case Constants.RoleCodeConstants.STANDARD_SUBS:
                                    userDetail.Roles.Add(UserRole.StandardSubscription);
                                    break;
                                case Constants.RoleCodeConstants.CORPORATE_SUBS_ADMIN:
                                    userDetail.Roles.Add(UserRole.CorporateSubscriptionAdmin);
                                    break;
                                case Constants.RoleCodeConstants.CORPORATE_SUBS_USER:
                                    userDetail.Roles.Add(UserRole.CorporateSubscriptionUser);
                                    break;
                            }
                        }
                    }
                }
                return userDetail;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }

        /// <summary>
        /// Method that validates the user based on the session key.
        /// </summary>
        /// <param name="sessionKey">
        /// A <see cref="string"/> that holds the session key.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        /// <remarks>
        /// This method helps to validate a user when logged in from the web
        /// site.
        /// </remarks>
        public UserDetail GetAuthenticateUser(string sessionKey)
        {
            IDataReader datareader = null;
            UserDetail userDetail = null;

            try
            {
                // Create the command.
                DbCommand authenticateCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_AUTHENTICATE_SESSION_KEY_USER");

                // Add input parameters.
                HCMDatabase.AddInParameter(authenticateCommand, "@SESSION_KEY", 
                    DbType.String, sessionKey);

                // Execute the query.
                datareader = HCMDatabase.ExecuteReader(authenticateCommand);
                
                while (datareader.Read())
                {
                    // Instantiate user detail object.
                    userDetail = new UserDetail();

                    userDetail.UserID = Convert.ToInt32(datareader["USERID"]);
                    userDetail.UserName = datareader["USER_NAME"].ToString();
                    userDetail.FirstName = datareader["USER_FIRST_NAME"].ToString();
                    userDetail.MiddleName = datareader["USER_MIDDLE_NAME"].ToString();
                    userDetail.LastName = datareader["USER_LAST_NAME"].ToString();
                    userDetail.Email = datareader["USER_EMAIL"].ToString();
                    userDetail.TenantID = Convert.ToInt32(datareader["TENANT_ID"]);
                    userDetail.LastLogin = Convert.ToDateTime(datareader["USER_LAST_LOGIN_TIME"]);
                    if (!Utility.IsNullOrEmpty(datareader["SUBSCRIPTION_ROLE"]))
                        userDetail.SubscriptionRole = datareader["SUBSCRIPTION_ROLE"].ToString();
                    if (!Utility.IsNullOrEmpty(datareader["SUBSCRIPTION_ID"]))
                        userDetail.SubscriptionId = Convert.ToInt32(datareader["SUBSCRIPTION_ID"]);
                    if (!Utility.IsNullOrEmpty(datareader["USER_TYPE"]))
                    {
                        if (datareader["USER_TYPE"].ToString().ToUpper().Trim() == "UT_ST_ADM")
                            userDetail.UserType = UserType.SiteAdmin;
                    }

                    userDetail.LegalAccepted = false;
                    if (!Utility.IsNullOrEmpty(datareader["LEGAL_ACCEPTED"]) &&
                        datareader["LEGAL_ACCEPTED"].ToString().ToUpper().Trim() == "Y")
                    {
                        userDetail.LegalAccepted = true;
                    }

                    datareader.NextResult();
                    while (datareader.Read())
                    {
                        if (userDetail.Roles == null)
                            userDetail.Roles = new List<UserRole>();

                        if (!Utility.IsNullOrEmpty(datareader["ROLE_CODE"]))
                        {
                            switch (datareader["ROLE_CODE"].ToString())
                            {
                                case Constants.RoleCodeConstants.CONTENT_AUTHOR:
                                    userDetail.Roles.Add(UserRole.ContentAuthor);
                                    break;
                                case Constants.RoleCodeConstants.CONTENT_VALIDATOR:
                                    userDetail.Roles.Add(UserRole.ContentValidator);
                                    break;
                                case Constants.RoleCodeConstants.TEST_AUTHOR:
                                    userDetail.Roles.Add(UserRole.TestAuthor);
                                    break;
                                case Constants.RoleCodeConstants.DELIVERY_MANAGER:
                                    userDetail.Roles.Add(UserRole.DeliveryManager);
                                    break;
                                case Constants.RoleCodeConstants.RECRUITER:
                                    userDetail.Roles.Add(UserRole.Recruiter);
                                    break;
                                case Constants.RoleCodeConstants.CANDIDATE:
                                    userDetail.Roles.Add(UserRole.Candidate);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_USERSETUP:
                                    userDetail.Roles.Add(UserRole.AdminUserSetup);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_OTM_TS:
                                    userDetail.Roles.Add(UserRole.AdminOTMTS);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_CRD_MGMT:
                                    userDetail.Roles.Add(UserRole.AdminCreditsManagement);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_CAND_MGMT:
                                    userDetail.Roles.Add(UserRole.AdminCandidateManagement);
                                    break;
                                case Constants.RoleCodeConstants.ADMINISTRATOR:
                                    userDetail.Roles.Add(UserRole.AdminSystem);
                                    break;
                                case Constants.RoleCodeConstants.FREE_SUBS:
                                    userDetail.Roles.Add(UserRole.FreeSubscription);
                                    break;
                                case Constants.RoleCodeConstants.STANDARD_SUBS:
                                    userDetail.Roles.Add(UserRole.StandardSubscription);
                                    break;
                                case Constants.RoleCodeConstants.CORPORATE_SUBS_ADMIN:
                                    userDetail.Roles.Add(UserRole.CorporateSubscriptionAdmin);
                                    break;
                                case Constants.RoleCodeConstants.CORPORATE_SUBS_USER:
                                    userDetail.Roles.Add(UserRole.CorporateSubscriptionUser);
                                    break;
                                case Constants.RoleCodeConstants.GENERAL_USER:
                                    userDetail.Roles.Add(UserRole.GeneralUser);
                                    break;
                                case Constants.RoleCodeConstants.SALES_PERSON:
                                    userDetail.Roles.Add(UserRole.SalesPerson);
                                    break;
                                case Constants.RoleCodeConstants.TECHNICAL_SPECIALIST:
                                    userDetail.Roles.Add(UserRole.TechnicalSpecialist);
                                    break;
                                case Constants.RoleCodeConstants.RECRUITER_JAVA:
                                    userDetail.Roles.Add(UserRole.RecruiterJava);
                                    break;
                                case Constants.RoleCodeConstants.RECRUITER_SAP:
                                    userDetail.Roles.Add(UserRole.RecruiterSap);
                                    break;
                                case Constants.RoleCodeConstants.ASSESSOR:
                                    userDetail.Roles.Add(UserRole.Assessor);
                                    break;
                                case Constants.RoleCodeConstants.SITE_ADMIN:
                                    userDetail.Roles.Add(UserRole.SiteAdmin);
                                    break;
                            }
                        }
                    }
                }
                return userDetail;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }

        /// <summary>
        /// Method that authenticates a candidate. This will check for 
        /// user name/password, candidate role and activity count.
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        public UserDetail GetAuthenticateCandidate(string userName, string password, int tenantID)
        {
            IDataReader datareader = null;

            try
            {
                DbCommand getAuthenticateCommand = null;
                
                if (tenantID == 0)
                    getAuthenticateCommand = HCMDatabase.GetStoredProcCommand("SPGET_AUTHENTICATE_CANDIDATE");
                else
                    getAuthenticateCommand = HCMDatabase.GetStoredProcCommand("SPGET_AUTHENTICATE_TENANT_CANDIDATE");

                HCMDatabase.AddInParameter(getAuthenticateCommand, "@USER_NAME", DbType.String, userName);
                HCMDatabase.AddInParameter(getAuthenticateCommand, "@PASSWORD", DbType.String, password);
                if (tenantID > 0)
                    HCMDatabase.AddInParameter(getAuthenticateCommand, "@TENANT_ID", DbType.Int32, tenantID);
                datareader = HCMDatabase.ExecuteReader(getAuthenticateCommand);
                UserDetail userDetail = new UserDetail();

                // Read user details.
                if (datareader.Read())
                {
                    userDetail.UserID = Convert.ToInt32(datareader["USERID"]);

                    if (datareader["CANDIDATE_INFO_ID"] != null)
                        userDetail.CandidateInfoID = Convert.ToInt32(datareader["CANDIDATE_INFO_ID"]);

                    userDetail.UserName = datareader["USER_NAME"].ToString();
                    userDetail.FirstName = datareader["USER_FIRST_NAME"].ToString();
                    userDetail.MiddleName = datareader["USER_MIDDLE_NAME"].ToString();
                    userDetail.LastName = datareader["USER_LAST_NAME"].ToString();
                    userDetail.Email = datareader["USER_EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(datareader["ALT_EMAIL"]))
                        userDetail.AltEmail = datareader["ALT_EMAIL"].ToString();

                    userDetail.TenantID = Convert.ToInt32(datareader["TENANT_ID"]);

                    if (datareader["IS_LIMITED"] == null)
                        userDetail.IsLimited = true;
                    else
                        userDetail.IsLimited = (datareader["IS_LIMITED"].ToString().Trim().ToUpper() == "Y" ? true : false);

                    if (!Utility.IsNullOrEmpty(datareader["IS_OPEN_ID"]))
                        userDetail.IsOpenID = (datareader["IS_OPEN_ID"].ToString().Trim().ToUpper() == "Y" ? true : false);

                    userDetail.LastLogin = Convert.ToDateTime(datareader["USER_LAST_LOGIN_TIME"]);

                    // Read list of roles.
                    datareader.NextResult();
                    while (datareader.Read())
                    {
                        if (userDetail.Roles == null)
                            userDetail.Roles = new List<UserRole>();

                        if (!Utility.IsNullOrEmpty(datareader["ROLE_CODE"]))
                        {
                            switch (datareader["ROLE_CODE"].ToString())
                            {
                                case Constants.RoleCodeConstants.CONTENT_AUTHOR:
                                    userDetail.Roles.Add(UserRole.ContentAuthor);
                                    break;
                                case Constants.RoleCodeConstants.CONTENT_VALIDATOR:
                                    userDetail.Roles.Add(UserRole.ContentValidator);
                                    break;
                                case Constants.RoleCodeConstants.TEST_AUTHOR:
                                    userDetail.Roles.Add(UserRole.TestAuthor);
                                    break;
                                case Constants.RoleCodeConstants.DELIVERY_MANAGER:
                                    userDetail.Roles.Add(UserRole.DeliveryManager);
                                    break;
                                case Constants.RoleCodeConstants.RECRUITER:
                                    userDetail.Roles.Add(UserRole.Recruiter);
                                    break;
                                case Constants.RoleCodeConstants.CANDIDATE:
                                    userDetail.Roles.Add(UserRole.Candidate);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_USERSETUP:
                                    userDetail.Roles.Add(UserRole.AdminUserSetup);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_OTM_TS:
                                    userDetail.Roles.Add(UserRole.AdminOTMTS);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_CRD_MGMT:
                                    userDetail.Roles.Add(UserRole.AdminCreditsManagement);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_CAND_MGMT:
                                    userDetail.Roles.Add(UserRole.AdminCandidateManagement);
                                    break;
                                case Constants.RoleCodeConstants.ADMINISTRATOR:
                                    userDetail.Roles.Add(UserRole.AdminSystem);
                                    break;
                                case Constants.RoleCodeConstants.FREE_SUBS:
                                    userDetail.Roles.Add(UserRole.FreeSubscription);
                                    break;
                                case Constants.RoleCodeConstants.STANDARD_SUBS:
                                    userDetail.Roles.Add(UserRole.StandardSubscription);
                                    break;
                                case Constants.RoleCodeConstants.CORPORATE_SUBS_ADMIN:
                                    userDetail.Roles.Add(UserRole.CorporateSubscriptionAdmin);
                                    break;
                                case Constants.RoleCodeConstants.CORPORATE_SUBS_USER:
                                    userDetail.Roles.Add(UserRole.CorporateSubscriptionUser);
                                    break;
                            }
                        }
                    }

                    bool testActivityExist = false;

                    // Read test activities count.
                    datareader.NextResult();
                    if (datareader.Read())
                    {
                        if (!Utility.IsNullOrEmpty(datareader["TEST_ACTIVITY_COUNT"]))
                        {
                            if (Convert.ToInt32(datareader["TEST_ACTIVITY_COUNT"].ToString()) > 0)
                                testActivityExist = true;
                        }
                    }

                    bool interviewActivityExist = false;

                    // Read interview activities count.
                    datareader.NextResult();
                    if (datareader.Read())
                    {
                        if (!Utility.IsNullOrEmpty(datareader["INTERVIEW_ACTIVITY_COUNT"]))
                        {
                            if (Convert.ToInt32(datareader["INTERVIEW_ACTIVITY_COUNT"].ToString()) > 0)
                                interviewActivityExist = true;
                        }
                    }

                    // If either of the test or interview activity exist, then 
                    // set activity exists to true.
                    if (testActivityExist == true || interviewActivityExist == true)
                        userDetail.IsActivityExists = true;
                }
                return userDetail;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }

        /// <summary>
        /// Method that validates the candidate based on the session key.
        /// </summary>
        /// <param name="sessionKey">
        /// A <see cref="string"/> that holds the session key.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        /// <remarks>
        /// This method helps to validate a candidate when logged in from the web
        /// site.
        /// </remarks>
        public UserDetail GetAuthenticateCandidate(string sessionKey)
        {
            IDataReader datareader = null;
            UserDetail userDetail = null;
            try
            {
                // Create the command.
                DbCommand authenticateCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_AUTHENTICATE_SESSION_KEY_CANDIDATE");

                // Add input parameters.
                HCMDatabase.AddInParameter(authenticateCommand, "@SESSION_KEY",
                    DbType.String, sessionKey);

                // Execute the query.
                datareader = HCMDatabase.ExecuteReader(authenticateCommand);

                // Read user details.
                if (datareader.Read())
                {
                    // Instantiate user detail object.
                    userDetail = new UserDetail();

                    userDetail.UserID = Convert.ToInt32(datareader["USERID"]);

                    if (datareader["CANDIDATE_INFO_ID"] != null)
                        userDetail.CandidateInfoID = Convert.ToInt32(datareader["CANDIDATE_INFO_ID"]);

                    userDetail.UserName = datareader["USER_NAME"].ToString();
                    userDetail.FirstName = datareader["USER_FIRST_NAME"].ToString();
                    userDetail.MiddleName = datareader["USER_MIDDLE_NAME"].ToString();
                    userDetail.LastName = datareader["USER_LAST_NAME"].ToString();
                    userDetail.Email = datareader["USER_EMAIL"].ToString();
                    userDetail.TenantID = Convert.ToInt32(datareader["TENANT_ID"]);

                    if (datareader["IS_LIMITED"] == null)
                        userDetail.IsLimited = true;
                    else
                        userDetail.IsLimited = (datareader["IS_LIMITED"].ToString().Trim().ToUpper() == "Y" ? true : false);

                    userDetail.LastLogin = Convert.ToDateTime(datareader["USER_LAST_LOGIN_TIME"]);

                    // Read list of roles.
                    datareader.NextResult();
                    while (datareader.Read())
                    {
                        if (userDetail.Roles == null)
                            userDetail.Roles = new List<UserRole>();

                        if (!Utility.IsNullOrEmpty(datareader["ROLE_CODE"]))
                        {
                            switch (datareader["ROLE_CODE"].ToString())
                            {
                                case Constants.RoleCodeConstants.CONTENT_AUTHOR:
                                    userDetail.Roles.Add(UserRole.ContentAuthor);
                                    break;
                                case Constants.RoleCodeConstants.CONTENT_VALIDATOR:
                                    userDetail.Roles.Add(UserRole.ContentValidator);
                                    break;
                                case Constants.RoleCodeConstants.TEST_AUTHOR:
                                    userDetail.Roles.Add(UserRole.TestAuthor);
                                    break;
                                case Constants.RoleCodeConstants.DELIVERY_MANAGER:
                                    userDetail.Roles.Add(UserRole.DeliveryManager);
                                    break;
                                case Constants.RoleCodeConstants.RECRUITER:
                                    userDetail.Roles.Add(UserRole.Recruiter);
                                    break;
                                case Constants.RoleCodeConstants.CANDIDATE:
                                    userDetail.Roles.Add(UserRole.Candidate);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_USERSETUP:
                                    userDetail.Roles.Add(UserRole.AdminUserSetup);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_OTM_TS:
                                    userDetail.Roles.Add(UserRole.AdminOTMTS);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_CRD_MGMT:
                                    userDetail.Roles.Add(UserRole.AdminCreditsManagement);
                                    break;
                                case Constants.RoleCodeConstants.ADMIN_CAND_MGMT:
                                    userDetail.Roles.Add(UserRole.AdminCandidateManagement);
                                    break;
                                case Constants.RoleCodeConstants.ADMINISTRATOR:
                                    userDetail.Roles.Add(UserRole.AdminSystem);
                                    break;
                                case Constants.RoleCodeConstants.FREE_SUBS:
                                    userDetail.Roles.Add(UserRole.FreeSubscription);
                                    break;
                                case Constants.RoleCodeConstants.STANDARD_SUBS:
                                    userDetail.Roles.Add(UserRole.StandardSubscription);
                                    break;
                                case Constants.RoleCodeConstants.CORPORATE_SUBS_ADMIN:
                                    userDetail.Roles.Add(UserRole.CorporateSubscriptionAdmin);
                                    break;
                                case Constants.RoleCodeConstants.CORPORATE_SUBS_USER:
                                    userDetail.Roles.Add(UserRole.CorporateSubscriptionUser);
                                    break;
                            }
                        }
                    }

                    bool testActivityExist = false;

                    // Read test activities count.
                    datareader.NextResult();
                    if (datareader.Read())
                    {
                        if (!Utility.IsNullOrEmpty(datareader["TEST_ACTIVITY_COUNT"]))
                        {
                            if (Convert.ToInt32(datareader["TEST_ACTIVITY_COUNT"].ToString()) > 0)
                                testActivityExist = true;
                        }
                    }

                    bool interviewActivityExist = false;

                    // Read interview activities count.
                    datareader.NextResult();
                    if (datareader.Read())
                    {
                        if (!Utility.IsNullOrEmpty(datareader["INTERVIEW_ACTIVITY_COUNT"]))
                        {
                            if (Convert.ToInt32(datareader["INTERVIEW_ACTIVITY_COUNT"].ToString()) > 0)
                                interviewActivityExist = true;
                        }
                    }

                    // If either of the test or interview activity exist, then 
                    // set activity exists to true.
                    if (testActivityExist == true || interviewActivityExist == true)
                        userDetail.IsActivityExists = true;
                }
                return userDetail;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }

        /// <summary>
        /// Validating the Role based User
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user Id
        /// </param>
        /// <param name="featureUrl">
        /// A <see cref="string"/> that holds the featureUrl.
        /// </param>
        /// <returns>
        /// return the role information 
        /// </returns>
        public List<RoleRights> GetAuthorization(int userID, string featureUrl)
        {
            IDataReader datareader = null;
            try
            {
                DbCommand getAuthenticationCommand = HCMDatabase.GetStoredProcCommand("SPGET_FEATURE_ROLES");
                HCMDatabase.AddInParameter(getAuthenticationCommand, "@USER_ID", DbType.Int32, userID);
                HCMDatabase.AddInParameter(getAuthenticationCommand, "@FEATURE_URL", DbType.String, featureUrl);
                datareader = HCMDatabase.ExecuteReader(getAuthenticationCommand);

                List<RoleRights> roleRightsList = new List<RoleRights>();
                while (datareader.Read())
                {
                    RoleRights roleRights = new RoleRights();
                    roleRights.RoleRightAdd = Convert.ToBoolean(datareader["ROLE_RIGHT_ADD"]);
                    roleRights.RoleRightApprove = Convert.ToBoolean(datareader["ROLE_RIGHT_APPROVE"]);
                    roleRights.RoleRightUpdate = Convert.ToBoolean(datareader["ROLE_RIGHT_UPDATE"]);
                    roleRights.RoleRightView = Convert.ToBoolean(datareader["ROLE_RIGHT_VIEW"]);
                    roleRights.RoleCode = datareader["ROLE_CODE"].ToString();
                    roleRightsList.Add(roleRights);
                }
                return roleRightsList;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }

        }

        /// <summary>
        /// Gets the role authorization.
        /// </summary>
        /// <param name="roleID">The role ID.</param>
        /// <returns></returns>
        public List<RoleManagementDetail> GetRoleAuthorization(int roleID)
        {
            IDataReader datareader = null;

            try
            {
                DbCommand getAuthenticationCommand = HCMDatabase.GetStoredProcCommand("SPGET_ROLE_MANAGEMENT_DETAILS");

                HCMDatabase.AddInParameter(getAuthenticationCommand, "@ROLE_ID", DbType.Int32, roleID);

                datareader = HCMDatabase.ExecuteReader(getAuthenticationCommand);

                List<RoleManagementDetail> roleDetails = new List<RoleManagementDetail>();

                RoleManagementDetail roleManagement = null;

                while (datareader.Read())
                {
                    roleManagement = new RoleManagementDetail();

                    if (!Utility.IsNullOrEmpty(datareader["ROLE_PERMISSION_ID"]))
                    {
                        roleManagement.RolePermissionID = int.Parse(datareader["ROLE_PERMISSION_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["IS_APPLICABLE"]))
                    {
                        roleManagement.IsApplicable = Convert.ToBoolean(datareader["IS_APPLICABLE"]);
                    }
                    if (!Utility.IsNullOrEmpty(datareader["ROLE_NAME"]))
                    {
                        roleManagement.RoleName = datareader["ROLE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["ROLE_DESCRIPTION"]))
                    {
                        roleManagement.RoleDescription = datareader["ROLE_DESCRIPTION"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["PAGE_NAME"]))
                    {
                        roleManagement.PageName = datareader["PAGE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["PAGE_DESCRIPTION"]))
                    {
                        roleManagement.PageDescription = datareader["PAGE_DESCRIPTION"].ToString();
                    }

                    roleDetails.Add(roleManagement);
                }

                return roleDetails;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                {
                    datareader.Close();
                }
            }




        }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <returns>
        /// the roles details
        /// </returns>
        public List<Roles> GetRoles()
        {
            IDataReader datareader = null;
            try
            {
                DbCommand getRolesCommand = HCMDatabase.GetStoredProcCommand("SPGET_ROLES");

                datareader = HCMDatabase.ExecuteReader(getRolesCommand);

                List<Roles> roleRightsList = new List<Roles>();
                while (datareader.Read())
                {
                    Roles roleRights = new Roles();
                    if (!Utility.IsNullOrEmpty(datareader["ROLE_ID"]))
                    {
                        roleRights.RoleID = int.Parse(datareader["ROLE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["ROLE_NAME"]))
                    {
                        roleRights.RoleName = datareader["ROLE_NAME"].ToString();
                    }

                    roleRightsList.Add(roleRights);
                }
                return roleRightsList;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }

        ///// <summary>
        ///// Gets the page list.
        ///// </summary>
        ///// <returns></returns>
        //public List<RoleManagementDetail> GetPageList()
        //{
        //    IDataReader datareader = null;
        //    try
        //    {
        //        DbCommand getRolesCommand = HCMDatabase.GetStoredProcCommand("SPGET_PAGES");

        //        datareader = HCMDatabase.ExecuteReader(getRolesCommand);

        //        List<RoleManagementDetail> pageDetailList = new List<RoleManagementDetail>();
        //        while (datareader.Read())
        //        {
        //            RoleManagementDetail pageDetail = new RoleManagementDetail();
        //            if (!Utility.IsNullOrEmpty(datareader["PAGE_ID"]))
        //            {
        //                pageDetail.PageID = int.Parse(datareader["PAGE_ID"].ToString());
        //            }
        //            if (!Utility.IsNullOrEmpty(datareader["PAGE_NAME"]))
        //            {
        //                pageDetail.PageName = datareader["PAGE_NAME"].ToString();
        //            }

        //            pageDetailList.Add(pageDetail);
        //        }
        //        return pageDetailList;
        //    }
        //    finally
        //    {
        //        if (datareader != null && !datareader.IsClosed)
        //            datareader.Close();
        //    }
        //}

        /// <summary>
        /// Gets the module list.
        /// </summary>
        /// <returns></returns>
        public List<Module> GetModuleList()
        {
            IDataReader datareader = null;
            try
            {
                DbCommand getModulesCommand = HCMDatabase.GetStoredProcCommand("SPGET_MODULES");

                datareader = HCMDatabase.ExecuteReader(getModulesCommand);

                List<Module> moduleList = new List<Module>();
                while (datareader.Read())
                {
                    Module moduleDetail = new Module();
                    if (!Utility.IsNullOrEmpty(datareader["MODID"]))
                    {
                        moduleDetail.ModuleID = int.Parse(datareader["MODID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["MODNAME"]))
                    {
                        moduleDetail.ModuleName = datareader["MODNAME"].ToString();
                    }

                    moduleList.Add(moduleDetail);
                }
                return moduleList;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }



        /// <summary>
        /// Gets the role category details.
        /// </summary>
        /// <param name="sortExpression">holds the sorting expression</param>
        /// <param name="sortOrder">holds the sorting order</param>
        /// <param name="pageSize">holds the page size</param>
        /// <param name="pageNumber">holds the page number</param>
        /// <param name="totalPages">holds the total pages</param>
        /// <returns></returns>
        public List<RoleManagementDetail> GetRoleCategory(string sortExpression,
            string sortOrder, int pageSize, int? pageNumber, out int totalPages)
        {
            IDataReader dataReader = null;
            try
            {


                totalPages = 0;
                DbCommand getBusinessTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_ROLE_CATEGORY");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_SIZE", DbType.Int32, pageSize);

                List<RoleManagementDetail> roleCategoryDetails = new List<RoleManagementDetail>();

                dataReader = HCMDatabase.ExecuteReader(getBusinessTypeCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalPages = int.Parse(dataReader["COUNT"].ToString());
                    }
                    else
                    {
                        RoleManagementDetail roleCategory = new RoleManagementDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["ROLCATCODE"]))
                        {
                            roleCategory.RoleCategoryCode = dataReader["ROLCATCODE"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ROLCATNAME"]))
                        {
                            roleCategory.RoleName = dataReader["ROLCATNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ROLCATID"]))
                        {
                            roleCategory.RoleID = int.Parse(dataReader["ROLCATID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CODENAME"]))
                        {
                            roleCategory.RoleCodeName = dataReader["CODENAME"].ToString();
                        }


                        roleCategoryDetails.Add(roleCategory);
                    }

                }
                return roleCategoryDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }



        /// <summary>
        /// Gets the role right matrix.
        /// </summary>
        /// <param name="rolesList">The roles list.</param>
        /// <param name="modulesList">The modules list.</param>
        /// <returns></returns>
        public List<RoleRightMatrixDetail> GetRoleRightMatrix(string rolesList, string modulesList)
        {
            IDataReader datareader = null;
            try
            {
                DbCommand getRoleRightMatrixCommand = HCMDatabase.GetStoredProcCommand("SPGET_ROLE_RIGHT_MATRIX");

                HCMDatabase.AddInParameter(getRoleRightMatrixCommand, "@MODULE_ID", DbType.String, modulesList);

                HCMDatabase.AddInParameter(getRoleRightMatrixCommand, "@ROLE_ID", DbType.String, rolesList);

                datareader = HCMDatabase.ExecuteReader(getRoleRightMatrixCommand);

                List<RoleRightMatrixDetail> roleRightMatrix = new List<RoleRightMatrixDetail>();
                while (datareader.Read())
                {
                    RoleRightMatrixDetail roleDetail = new RoleRightMatrixDetail();

                    if (!Utility.IsNullOrEmpty(datareader["ROLE_RIGHT_ID"]))
                    {
                        roleDetail.RoleRightID = int.Parse(datareader["ROLE_RIGHT_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["ROLE_ID"]))
                    {
                        roleDetail.RoleID = int.Parse(datareader["ROLE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["ROLE_NAME"]))
                    {
                        roleDetail.RoleName = datareader["ROLE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["MODULE_ID"]))
                    {
                        roleDetail.ModuleID = int.Parse(datareader["MODULE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["MODULE_NAME"]))
                    {
                        roleDetail.ModuleName = datareader["MODULE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["PAGE_RIGHT"]))
                    {
                        roleDetail.IsApplicable = Convert.ToBoolean(datareader["PAGE_RIGHT"]);
                    }
                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_ID"]))
                    {
                        roleDetail.FeatureID = int.Parse(datareader["FEATURE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SUB_MODULE_ID"]))
                    {
                        roleDetail.SubModuleID = int.Parse(datareader["SUB_MODULE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SUB_MODULE_NAME"]))
                    {
                        roleDetail.SubModuleName = datareader["SUB_MODULE_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_NAME"]))
                    {
                        roleDetail.FeatureName = datareader["FEATURE_NAME"].ToString();
                    }

                    roleRightMatrix.Add(roleDetail);
                }
                return roleRightMatrix;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }


        }

        /// <summary>
        /// Updates the role category details.
        /// </summary>
        /// <param name="roleCategoryCode">holds the role catgory code
        /// </param>
        /// <param name="roleCategoryName">holds the role category name
        /// </param>
        /// <param name="roleID">holds the role ID</param>
        /// <param name="userID">holds the user ID</param>
        public int UpdateRoleCategory(string roleCategoryCode, string roleCategoryName, int roleID, int userID)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectRoleCatagoryTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPSELECTCOUNT_ROLE_CATEGORY");

                HCMDatabase.AddInParameter(selectRoleCatagoryTypeCommand,
                   "@ROLCAT_CODE", DbType.String, roleCategoryCode);

                HCMDatabase.AddInParameter(selectRoleCatagoryTypeCommand,
                   "@ROLCAT_ID", DbType.Int32, roleID);

                datareader = HCMDatabase.ExecuteReader(selectRoleCatagoryTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWSCOUNT"].ToString());
                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
            if (rowsAffected == 0)
            {

                // Create a stored procedure command object.
                DbCommand updateRoleCategoryTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_ROLE_CATEGORY");

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@ROLCAT_ID", DbType.Int32, roleID);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@ROLCAT_CODE", DbType.String, roleCategoryCode);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@ROLCAT_NAME", DbType.String, roleCategoryName);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@ROLCAT_MODIFIEDBY", DbType.Int32, userID);


                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(updateRoleCategoryTypeCommand);
            }
            return rowsAffected;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleID"></param>
        public void DeleteRoleCategory(int roleID)
        {
            // Create a stored procedure command object.
            DbCommand deleteRoleCatgeoryTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ROLE_CATEGORY");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRoleCatgeoryTypeCommand,
                "@ROLCATID", DbType.Int32, roleID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteRoleCatgeoryTypeCommand);
            {

            }
        }

        /// <summary>
        /// Inserts the new role category
        /// </summary>
        /// <param name="roleCategorycode">holds role category code
        /// </param>
        /// <param name="roleCategoryName">holds role category name
        /// </param>
        /// <param name="rolecategoryCreatedBy"></param>
        public int InsertNewCategoryRole(string roleCategorycode, string roleCategoryName,
            int rolecategoryCreatedBy)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectRoleCatagoryTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPSELECTCOUNT_ROLES_CATEGORY");

                HCMDatabase.AddInParameter(selectRoleCatagoryTypeCommand,
                   "@ROLCAT_CODE", DbType.String, roleCategorycode);

                datareader = HCMDatabase.ExecuteReader(selectRoleCatagoryTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWSCOUNT"].ToString());
                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
            if (rowsAffected == 0)
            {
                // Create a stored procedure command object.
                DbCommand insertRoleCatagoryTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_ROLE_CATEGORY");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@ROLCAT_CODE", DbType.String, roleCategorycode);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@ROLCAT_NAME", DbType.String, roleCategoryName);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@ROLCAT_CREATEDBY", DbType.Int32, rolecategoryCreatedBy);


                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@ROLCAT_MODEFIEDBY", DbType.Int32, rolecategoryCreatedBy);


                HCMDatabase.ExecuteNonQuery(insertRoleCatagoryTypeCommand);
            }
            return rowsAffected;
        }



        /// <summary>
        /// Gets the role
        /// </summary>
        /// <param name="sortExpression">holds the expression for
        /// sorting</param>
        /// <param name="sortOrder">holds the order foe
        /// sorting</param>
        /// <param name="pageNumber">holds the page number</param>
        /// <returns></returns>
        public List<RoleManagementDetail> GetRole(string sortExpression, string sortOrder,
            int pageNumber)
        {
            IDataReader dataReader = null;
            try
            {


                //  int totalPage = 0;
                DbCommand getBusinessTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_ROLE");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_NUM", DbType.Int32, pageNumber);


                List<RoleManagementDetail> roleDetails = new List<RoleManagementDetail>();

                dataReader = HCMDatabase.ExecuteReader(getBusinessTypeCommand);

                while (dataReader.Read())
                {
                    //if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    //{
                    //    totalPage = int.Parse(dataReader["COUNT"].ToString());
                    //}
                    //else
                    //{
                    RoleManagementDetail roles = new RoleManagementDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["ROLID"]))
                    {
                        roles.RoleID = int.Parse(dataReader["ROLID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLCATCODE"]))
                    {
                        roles.RoleCatCode = dataReader["ROLCATCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLCATNAME"]))
                    {
                        roles.RoleName = dataReader["ROLCATNAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLCODE"]))
                    {
                        roles.RoleCode = dataReader["ROLCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        roles.RolesName = dataReader["ROLNAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        roles.RoleCodeName = dataReader["CODENAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLJOBDESCRIPTION"]))
                    {
                        roles.RolesJobDesc = dataReader["ROLJOBDESCRIPTION"].ToString();
                    }

                    roleDetails.Add(roles);
                    //  }

                }
                return roleDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Inserts the new role
        /// </summary>
        /// <param name="roleCode">holds the role code</param>
        /// <param name="roleCatID">holds the role catgory
        /// ID</param>
        /// <param name="roleJobDesc">holds the role job
        /// description</param>
        /// <param name="roleName">holds the role name</param>
        /// <param name="createdBy">holds the user ID
        /// </param>
        /// <returns></returns>
        public int InsertNewRole(string roleCode, int roleCatID, string roleName, string roleJobDesc, int createdBy)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectRoleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPSELECTCOUNT_ROLE");

                HCMDatabase.AddInParameter(selectRoleTypeCommand,
                   "@ROL_CODE", DbType.String, roleCode);

                datareader = HCMDatabase.ExecuteReader(selectRoleTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWSCOUNT"].ToString());
                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
            if (rowsAffected == 0)
            {
                // Create a stored procedure command object.
                DbCommand insertRoleTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_ROLE");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@ROL_CODE", DbType.String, roleCode);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@ROLCAT_ID", DbType.Int32, roleCatID);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@ROL_NAME", DbType.String, roleName);
                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@ROL_JOBDESC", DbType.String, roleJobDesc);


                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@ROLCAT_CREATEDBY", DbType.Int32, createdBy);


                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@ROLCAT_MODEFIEDBY", DbType.Int32, createdBy);


                HCMDatabase.ExecuteNonQuery(insertRoleTypeCommand);
            }
            return rowsAffected;
        }

        /// <summary>
        /// Updates the role
        /// </summary>
        /// <param name="roleCode">holds the role code
        /// </param>
        /// <param name="roleName">holds the role name
        /// </param>
        /// <param name="roleID">holds the role ID
        /// </param>
        /// <param name="modifiedBy">holds the user ID
        /// </param>
        public int UpdateRoles(string roleCode, string roleName, int roleID, int modifiedBy)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectRoleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPSELECTCOUNT_ROLE");

                HCMDatabase.AddInParameter(selectRoleTypeCommand,
                   "@ROL_CODE", DbType.String, roleCode);

                datareader = HCMDatabase.ExecuteReader(selectRoleTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWSCOUNT"].ToString());
                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
            if (rowsAffected == 0)
            {
                // Create a stored procedure command object.
                DbCommand updateRolesTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_ROLES");

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_ID", DbType.Int32, roleID);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_CODE", DbType.String, roleCode);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_NAME", DbType.String, roleName);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_MODIFIEDBY", DbType.Int32, modifiedBy);


                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(updateRolesTypeCommand);
            }
            return rowsAffected;
        }

        /// <summary>
        ///  Deletes the role
        /// </summary>
        /// <param name="roleID">holds the role ID</param>
        public void DeleteRoles(int roleID)
        {
            // Create a stored procedure command object.
            DbCommand deleteRoleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ROLES");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRoleTypeCommand,
                "@ROLID", DbType.Int32, roleID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteRoleTypeCommand);
            {

            }
        }

        /// <summary>
        /// updates the role details
        /// </summary>
        /// <param name="roleDetails">holds the list of role details
        /// </param>
        public void UpdateRoleDetails(List<RoleManagementDetail> roleDetails)
        {

            for (int i = 0; i < roleDetails.Count; i++)
            {

                DbCommand updateRolesTypeCommand = HCMDatabase.
           GetStoredProcCommand("SPUPDATE_ROLES");

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_ID", DbType.Int32, roleDetails[i].RoleID);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_CODE", DbType.String, roleDetails[i].RoleCode);

                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                   "@ROL_CAT_ID", DbType.Int32, roleDetails[i].RoleCodeName);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_NAME", DbType.String, roleDetails[i].RolesName);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_JOBDESC", DbType.String, roleDetails[i].RolesJobDesc);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRolesTypeCommand,
                    "@ROL_MODIFIEDBY", DbType.Int32, roleDetails[i].ModifiedBy);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(updateRolesTypeCommand);
            }


        }

        /// <summary>
        /// Updates the role matrix details.
        /// </summary>
        /// <param name="role">The role.</param>
        public void UpdateRoleMatrixDetails(RoleRightMatrixDetail role)
        {
            DbCommand updateCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_ROLE_MATRIX_DETAILS");

            HCMDatabase.AddInParameter(updateCommand, "@ROLE_RIGHT_ID", DbType.Int32, role.RoleRightID);

            HCMDatabase.AddInParameter(updateCommand, "@ROLE_NAME", DbType.String, role.RoleName);

            HCMDatabase.AddInParameter(updateCommand, "@FEATURE_ID", DbType.String, role.FeatureID);

            HCMDatabase.AddInParameter(updateCommand, "@IS_APPLICABLE", DbType.Int16, role.IsApplicable == true ? 1 : 0);

            HCMDatabase.ExecuteScalar(updateCommand);
        }

        /// <summary>
        /// Delete the role category results
        /// </summary>
        /// <param name="roleCategoryID">holds role category ID</param>
        /// <returns></returns>
        public string DeleteRoleCategoryResult(string roleCategoryID)
        {

            IDataReader datareader = null;
            string result = string.Empty;
            try
            {

                DbCommand deleteRoleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ROLE_CATEGORYRESULT");

                HCMDatabase.AddInParameter(deleteRoleTypeCommand,
                   "@ROLCATID", DbType.String, roleCategoryID);

                datareader = HCMDatabase.ExecuteReader(deleteRoleTypeCommand);

                while (datareader.Read())
                {
                    result = datareader["RESULT"].ToString();
                }
                return result;

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }

        /// <summary>
        /// Get all the role rights
        /// </summary>
        /// <param name="sortExpression">holds the expression for sorting
        /// </param>
        /// <param name="sortOrder">holds the order for sorting
        /// </param>
        /// <returns></returns>
        public List<UsersRoleMatrix> GetAllUserRoleRights(string sortExpression, string sortOrder)
        {

            IDataReader dataReader = null;

            try
            {
                DbCommand getUserRoleRightsTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_USERROLES");

                HCMDatabase.AddInParameter(getUserRoleRightsTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getUserRoleRightsTypeCommand, "@ORDERBY", DbType.String, sortExpression);

                List<UsersRoleMatrix> userRoleRightsDetails = new List<UsersRoleMatrix>();

                dataReader = HCMDatabase.ExecuteReader(getUserRoleRightsTypeCommand);

                while (dataReader.Read())
                {

                    UsersRoleMatrix userRights = new UsersRoleMatrix();

                    if (!Utility.IsNullOrEmpty(dataReader["ROLID"]))
                    {
                        userRights.RoleID = int.Parse(dataReader["ROLID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLCODE"]))
                    {
                        userRights.RoleCode = dataReader["ROLCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        userRights.RoleName = dataReader["ROLNAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLJOBDESCRIPTION"]))
                    {
                        userRights.RoleJobDescription = dataReader["ROLJOBDESCRIPTION"].ToString();
                    }

                    userRoleRightsDetails.Add(userRights);

                }
                return userRoleRightsDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="sortOrder"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<UsersRoleMatrix> GetAppliacableUserRoleRights(string sortExpression, string sortOrder, int userID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getUserRoleRightsTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_USERROLE");

                HCMDatabase.AddInParameter(getUserRoleRightsTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getUserRoleRightsTypeCommand, "@ORDERBY", DbType.String, sortExpression);

                HCMDatabase.AddInParameter(getUserRoleRightsTypeCommand, "@USER_ID", DbType.Int32, userID);


                List<UsersRoleMatrix> userRoleRightsDetails = new List<UsersRoleMatrix>();

                dataReader = HCMDatabase.ExecuteReader(getUserRoleRightsTypeCommand);

                while (dataReader.Read())
                {

                    UsersRoleMatrix userRights = new UsersRoleMatrix();


                    if (!Utility.IsNullOrEmpty(dataReader["URLID"]))
                    {
                        userRights.URLID = int.Parse(dataReader["URLID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["USRID"]))
                    {
                        userRights.UserID = int.Parse(dataReader["USRID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLID"]))
                    {
                        userRights.RoleID = int.Parse(dataReader["ROLID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        userRights.RoleName = dataReader["ROLNAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLJOBDESCRIPTION"]))
                    {
                        userRights.RoleJobDescription = dataReader["ROLJOBDESCRIPTION"].ToString();
                    }

                    userRoleRightsDetails.Add(userRights);

                }
                return userRoleRightsDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }


        /// <summary>
        /// Deletes the user role
        /// </summary>
        /// <param name="roleID">holds the role ID</param>
        public void DeleteUserRoles(int roleID, int userID)
        {

            // Create a stored procedure command object.
            DbCommand deleteRoleCatgeoryTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_USERROLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRoleCatgeoryTypeCommand,
                "@USERROLEID", DbType.Int32, roleID);

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRoleCatgeoryTypeCommand,
                "@USERID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteRoleCatgeoryTypeCommand);
            {

            }
        }

        /// <summary>
        /// Insert new user role
        /// </summary>
        /// <param name="userID">holds the User ID</param>
        /// <param name="roleID">holds the role ID</param>
        /// <param name="createdBy">holds the creaded ID</param>
        public void InsertNewUserRoles(int userID, int roleID, int createdBy)
        {
            // Create a stored procedure command object.
            DbCommand insertUserRolesTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_USERROLES");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertUserRolesTypeCommand,
                "@USER_ID", DbType.Int32, userID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertUserRolesTypeCommand,
                "@ROLE_ID", DbType.Int32, roleID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertUserRolesTypeCommand,
                "@USERROL_CREATEDBY", DbType.Int32, createdBy);

            HCMDatabase.AddInParameter(insertUserRolesTypeCommand,
            "@USERROL_MODEFIEDBY", DbType.Int32, createdBy);

            HCMDatabase.ExecuteNonQuery(insertUserRolesTypeCommand);
        }

        public List<UserRightsMatrix> GetUserRightsMatrixDetails(string userID,
            string moduleIDs, int showAllFeature, int pageNumber,
            int gridPageSize, string orderBy, SortType orderByDirection,
            out int totalRecordCount)
        {
            IDataReader datareader = null;

            totalRecordCount = 0;

            List<UserRightsMatrix> userRightMatrix = new List<UserRightsMatrix>();

            try
            {
                DbCommand getUserRightsMatrixCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_NEW_USER_RIGHTS");

                // Add parameters.
                HCMDatabase.AddInParameter(getUserRightsMatrixCommand, 
                    "@USERID", DbType.Int32, int.Parse(userID));
                HCMDatabase.AddInParameter(getUserRightsMatrixCommand, 
                    "@MODULEIDS", DbType.String, moduleIDs);
                HCMDatabase.AddInParameter(getUserRightsMatrixCommand, 
                    "@SHOWALL", DbType.Int16, showAllFeature);

                datareader = HCMDatabase.ExecuteReader(getUserRightsMatrixCommand);

                while (datareader.Read())
                {
                    UserRightsMatrix userRights = new UserRightsMatrix();

                    if (!Utility.IsNullOrEmpty(datareader["MODID"]))
                    {
                        userRights.ModuleID = int.Parse(datareader["MODID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SMOID"]))
                    {
                        userRights.SubModuleID = int.Parse(datareader["SMOID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["FEAID"]))
                    {
                        userRights.FeatureID = int.Parse(datareader["FEAID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SMONAME"]))
                    {
                        userRights.SubModuleName = datareader["SMONAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["FEAFEATURENAME"]))
                    {
                        userRights.FeatureName = datareader["FEAFEATURENAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["FEAURL"]))
                    {
                        userRights.FeatureURL = datareader["FEAURL"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["RRTPAGE"]))
                    {
                        userRights.IsApplicable =
                            Convert.ToBoolean(int.Parse(datareader["RRTPAGE"].ToString()));
                    }
                    else
                    {
                        userRights.IsApplicable = false;
                    }

                    if (!Utility.IsNullOrEmpty(datareader["ALREADY_EXIST"]))
                    {
                        userRights.IsActive =
                            datareader["ALREADY_EXIST"].ToString() == "Y" ? true : false;
                    }

                    userRightMatrix.Add(userRights);
                }
                return userRightMatrix;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                {
                    datareader.Close();
                }
            }
        }

        public void InsertUserRights(UserRightsMatrix userRight)
        {
            DbCommand insertUserRightCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_USER_RIGHTS");

            HCMDatabase.AddInParameter(insertUserRightCommand, "@USRID", DbType.Int32, userRight.UserID);

            HCMDatabase.AddInParameter(insertUserRightCommand, "@MODID", DbType.Int32, userRight.ModuleID);

            HCMDatabase.AddInParameter(insertUserRightCommand, "@SMODID", DbType.Int32, userRight.SubModuleID);
            HCMDatabase.AddInParameter(insertUserRightCommand, "@FEAID", DbType.Int32, userRight.FeatureID);
            HCMDatabase.AddInParameter(insertUserRightCommand, "@RRTCREATEDBY", DbType.Int32, userRight.CreatedBy);

            HCMDatabase.AddInParameter(insertUserRightCommand, "@RRTPAGE", DbType.Int16, userRight.IsApplicable);

            HCMDatabase.AddInParameter(insertUserRightCommand, "@ROLID", DbType.Int16, null);

            HCMDatabase.ExecuteNonQuery(insertUserRightCommand);
        }

        public void DeleteUserRights(UserRightsMatrix userRight)
        {
            DbCommand updateUserRightCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_USER_RIGHTS");

            HCMDatabase.AddInParameter(updateUserRightCommand, "@USERRIGHTID", DbType.Int32, userRight.UserRightsID);

            HCMDatabase.ExecuteNonQuery(updateUserRightCommand);
        }

        public List<RoleRightMatrixDetail> GetFeatureRightsDetails
            (int moduleID, int subModuleID, int roleID)
        {
            IDataReader datareader = null;

            List<RoleRightMatrixDetail> featureRightMatrix = new List<RoleRightMatrixDetail>();

            try
            {
                DbCommand getFeatureRightsMatrixCommand = HCMDatabase.GetStoredProcCommand("SPGET_FEATURE_MATRIX_DETAILS");

                HCMDatabase.AddInParameter(getFeatureRightsMatrixCommand, "@MODID", DbType.Int32,
                    moduleID);
                HCMDatabase.AddInParameter(getFeatureRightsMatrixCommand, "@SMODID", DbType.Int32,
                    subModuleID);
                HCMDatabase.AddInParameter(getFeatureRightsMatrixCommand, "@ROLID", DbType.Int32,
                 roleID);


                datareader = HCMDatabase.ExecuteReader(getFeatureRightsMatrixCommand);

                while (datareader.Read())
                {
                    RoleRightMatrixDetail featureRightDetails = new RoleRightMatrixDetail();

                    if (!Utility.IsNullOrEmpty(datareader["ROLERIGHTS_ID"]))
                    {
                        featureRightDetails.RoleRightID = int.Parse(datareader["ROLERIGHTS_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_ID"]))
                    {
                        featureRightDetails.FeatureID = int.Parse(datareader["FEATURE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["MODULE_ID"]))
                    {
                        featureRightDetails.ModuleID = int.Parse(datareader["MODULE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SUBMODULE_ID"]))
                    {
                        featureRightDetails.SubModuleID = int.Parse(datareader["SUBMODULE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["MODULE_NAME"]))
                    {
                        featureRightDetails.ModuleName = datareader["MODULE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SUBMODULE_NAME"]))
                    {
                        featureRightDetails.SubModuleName = datareader["SUBMODULE_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_NAME"]))
                    {
                        featureRightDetails.FeatureName = datareader["FEATURE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(datareader["IS_CHECKED"]))
                    {
                        featureRightDetails.IsApplicable = Convert.ToBoolean
                            (datareader["IS_CHECKED"].ToString().Trim() == "Y" ? "True" : "False");
                    }

                    featureRightMatrix.Add(featureRightDetails);
                }
                return featureRightMatrix;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                {
                    datareader.Close();
                }
            }


        }



        public List<SubModule> GetSubModuleBasedOnModule(int moduleID)
        {
            DbCommand getSubModuleCommand = null;
            IDataReader dataReader = null;
            try
            {
                getSubModuleCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_SUBMODULE");

                HCMDatabase.AddInParameter(getSubModuleCommand, "@MOD_ID",
                    DbType.Int32, moduleID);

                List<SubModule> subModuleList = new List<SubModule>();

                dataReader = HCMDatabase.ExecuteReader(getSubModuleCommand);

                while (dataReader.Read())
                {
                    SubModule subModule = new SubModule();

                    if (!Utility.IsNullOrEmpty(dataReader["SMOID"]))
                    {
                        subModule.SubModuleID = int.Parse(dataReader["SMOID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SMONAME"]))
                    {
                        subModule.SubModuleName = dataReader["SMONAME"].ToString();
                    }

                    subModuleList.Add(subModule);
                }

                return subModuleList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleRightID"></param>
        public void DeleteRoleRights(int roleRightID)
        {
            // Create a stored procedure command object.
            DbCommand deleteRoleCatgeoryTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ROLE_RIGHTS");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRoleCatgeoryTypeCommand,
                "@RRTID", DbType.Int32, roleRightID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteRoleCatgeoryTypeCommand);
        }

        public void InsertRoleRight(RoleRightMatrixDetail roleRight)
        {
            DbCommand insertRoleRightCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_ROLE_RIGHTS");

            HCMDatabase.AddInParameter(insertRoleRightCommand, "@MODID", DbType.Int32, roleRight.ModuleID);

            HCMDatabase.AddInParameter(insertRoleRightCommand, "@SMODID", DbType.Int32, roleRight.SubModuleID);
            HCMDatabase.AddInParameter(insertRoleRightCommand, "@FEAID", DbType.Int32, roleRight.FeatureID);
            HCMDatabase.AddInParameter(insertRoleRightCommand, "@RRTCREATEDBY", DbType.Int32, roleRight.CreatedBy);

            HCMDatabase.AddInParameter(insertRoleRightCommand, "@RRTPAGE", DbType.Int16, roleRight.IsApplicable);

            HCMDatabase.AddInParameter(insertRoleRightCommand, "@ROLID", DbType.Int16, roleRight.RoleID);

          //  HCMDatabase.AddInParameter(insertRoleRightCommand, "@TENANTID", DbType.Int16, roleRight.TenantID);

            HCMDatabase.ExecuteNonQuery(insertRoleRightCommand);
        }

        public void UpdateInternalUserDetail(UserDetail userDetail, int modifiedBy)
        {
            DbCommand updateInternalUser = HCMDatabase.GetStoredProcCommand("SPUPDATE_INTERNAL_USERS");

            HCMDatabase.AddInParameter(updateInternalUser, "@USER_ID", DbType.Int32, userDetail.UserID);
            HCMDatabase.AddInParameter(updateInternalUser, "@FIRST_NAME", DbType.String, userDetail.FirstName);
            HCMDatabase.AddInParameter(updateInternalUser, "@LAST_NAME", DbType.String, userDetail.LastName);
            HCMDatabase.AddInParameter(updateInternalUser, "@PHONE", DbType.String, userDetail.Phone);
            HCMDatabase.AddInParameter(updateInternalUser, "@USER_TYPE", DbType.String, userDetail.UserType == UserType.SiteAdmin ? "UT_ST_ADM" : null);
         
            if (userDetail.IsActive == true)
                HCMDatabase.AddInParameter(updateInternalUser, "@IS_ACTIVE", DbType.Int32, 1);
            else
                HCMDatabase.AddInParameter(updateInternalUser, "@IS_ACTIVE", DbType.Int32, 0);

            HCMDatabase.AddInParameter(updateInternalUser, "@EXPIRY_DATE", DbType.DateTime, userDetail.ExpiryDate);

            HCMDatabase.AddInParameter(updateInternalUser, "@MODIFIED_BY", DbType.Int32, modifiedBy);
            HCMDatabase.ExecuteNonQuery(updateInternalUser);
        }
        public void UpdateSubscriptionUserDetail(UserDetail userDetail, int modifiedBy)
        {
            DbCommand updateInternalUser = HCMDatabase.GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_USERS");

            HCMDatabase.AddInParameter(updateInternalUser, "@USER_ID", DbType.Int32, userDetail.UserID);
            HCMDatabase.AddInParameter(updateInternalUser, "@FIRST_NAME", DbType.String, userDetail.FirstName);
            HCMDatabase.AddInParameter(updateInternalUser, "@LAST_NAME", DbType.String, userDetail.LastName);
            HCMDatabase.AddInParameter(updateInternalUser, "@PHONE", DbType.String, userDetail.Phone);
            HCMDatabase.AddInParameter(updateInternalUser, "@COMPANY", DbType.String, userDetail.Company);
            HCMDatabase.AddInParameter(updateInternalUser, "@TITLE", DbType.String, userDetail.Title);
            HCMDatabase.AddInParameter(updateInternalUser, "@BUSINESS_TYPE", DbType.String, userDetail.BussinessType);
            HCMDatabase.AddInParameter(updateInternalUser, "@BUSINESS_OTHERTYPE", DbType.String, userDetail.BussinessOtherType);
            if (userDetail.IsActive == true)
                HCMDatabase.AddInParameter(updateInternalUser, "@IS_ACTIVE", DbType.Int32, 1);
            else
                HCMDatabase.AddInParameter(updateInternalUser, "@IS_ACTIVE", DbType.Int32, 0);
            HCMDatabase.AddInParameter(updateInternalUser, "@EXPIRY_DATE", DbType.DateTime, userDetail.ExpiryDate);            
            HCMDatabase.AddInParameter(updateInternalUser, "@MODIFIED_BY", DbType.Int32, modifiedBy);
            HCMDatabase.ExecuteNonQuery(updateInternalUser);
        }

        public void UpdateUserRights(UserRightsMatrix userRight)
        {
            DbCommand updateUserRightCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_USER_RIGHTS");

            HCMDatabase.AddInParameter(updateUserRightCommand, "@USRID", DbType.Int32, userRight.UserID);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@MODIFIEDBY", DbType.Int32, userRight.ModifiedBy);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@ISAPPLICABLE", DbType.Int16,
                userRight.IsApplicable == true ? 1 : 0);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@MODID", DbType.Int32, userRight.ModuleID);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@SMOID", DbType.Int32, userRight.SubModuleID);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@FEAID", DbType.Int32, userRight.FeatureID);

            HCMDatabase.ExecuteNonQuery(updateUserRightCommand);
        }


        public void UpdateCorporateUserRights(UserRightsMatrix userRight)
        {
            DbCommand updateUserRightCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_USER_RIGHTS");

            HCMDatabase.AddInParameter(updateUserRightCommand, "@USRID", DbType.Int32, userRight.UserID);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@MODIFIEDBY", DbType.Int32, userRight.ModifiedBy);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@ISAPPLICABLE", DbType.Int16,
                userRight.IsApplicable == true ? 1 : 0);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@MODID", DbType.Int32, userRight.ModuleID);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@SMOID", DbType.Int32, userRight.SubModuleID);

            HCMDatabase.AddInParameter(updateUserRightCommand, "@FEAID", DbType.Int32, userRight.FeatureID);

            HCMDatabase.ExecuteNonQuery(updateUserRightCommand);
        }

        public string GetRolesForUser(int userID)
        {
            IDataReader dataReader = null;

            StringBuilder roleName = new StringBuilder();

            try
            {
                DbCommand GetUserRoleCommand = HCMDatabase.GetStoredProcCommand("SPGET_USER_ROLES");

                HCMDatabase.AddInParameter(GetUserRoleCommand, "@USERID", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(GetUserRoleCommand);

                while (dataReader.Read())
                {

                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        roleName.Append(dataReader["ROLNAME"].ToString());
                        roleName.Append(", ");
                    }
                }


                return roleName.Length > 2 ? roleName.ToString().TrimEnd(',', ' ') : roleName.ToString();
            }
            finally
            {

            }
        }


      

        #endregion Public Methods


        public List<Roles> GetCorporateRoles()
        {
            DbCommand getSubModuleCommand = null;
            IDataReader dataReader = null;
            try
            {
                getSubModuleCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_ROLES");

                List<Roles> roleList = new List<Roles>();

                dataReader = HCMDatabase.ExecuteReader(getSubModuleCommand);

                while (dataReader.Read())
                {
                    Roles roles = new Roles();

                    if (!Utility.IsNullOrEmpty(dataReader["ROLID"]))
                    {
                        roles.RoleID = int.Parse(dataReader["ROLID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLCODE"]))
                    {
                        roles.RoleCode = dataReader["ROLCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        roles.RoleName = dataReader["ROLNAME"].ToString();
                    }

                    roleList.Add(roles);
                }

                return roleList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<Roles> GetAssignedCorporateRole(int tenantID)
        {
            DbCommand getAssignedCorporateRoleCommand = null;
            IDataReader dataReader = null;
            try
            {
                getAssignedCorporateRoleCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_ASSIGNED_CORPORATE_ROLES");

                HCMDatabase.AddInParameter(getAssignedCorporateRoleCommand, "@TENANTID", DbType.Int32, tenantID);

                List<Roles> roleList = new List<Roles>();

                dataReader = HCMDatabase.ExecuteReader(getAssignedCorporateRoleCommand);

                while (dataReader.Read())
                {
                    Roles roles = new Roles();

                    if (!Utility.IsNullOrEmpty(dataReader["ROLID"]))
                    {
                        roles.RoleID = int.Parse(dataReader["ROLID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLCODE"]))
                    {
                        roles.RoleCode = dataReader["ROLCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        roles.RoleName = dataReader["ROLNAME"].ToString();
                    }

                    roleList.Add(roles);
                }

                return roleList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<RoleRightMatrixDetail> GetCorporateFeatureRightsDetails
           (int roleID,int tenantID)
        {
            IDataReader datareader = null;

            List<RoleRightMatrixDetail> featureRightMatrix = new List<RoleRightMatrixDetail>();

            try
            {
                DbCommand getFeatureRightsMatrixCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_FEATURE_MATRIX_DETAILS");

                
                HCMDatabase.AddInParameter(getFeatureRightsMatrixCommand, "@ROLID", DbType.Int32,
                 roleID);

                HCMDatabase.AddInParameter(getFeatureRightsMatrixCommand, "@TENANTID", DbType.Int32,
               tenantID);


                datareader = HCMDatabase.ExecuteReader(getFeatureRightsMatrixCommand);

                while (datareader.Read())
                {
                    RoleRightMatrixDetail featureRightDetails = new RoleRightMatrixDetail();

                    if (!Utility.IsNullOrEmpty(datareader["ROLERIGHTS_ID"]))
                    {
                        featureRightDetails.RoleRightID = int.Parse(datareader["ROLERIGHTS_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_ID"]))
                    {
                        featureRightDetails.FeatureID = int.Parse(datareader["FEATURE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["MODULE_ID"]))
                    {
                        featureRightDetails.ModuleID = int.Parse(datareader["MODULE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SUBMODULE_ID"]))
                    {
                        featureRightDetails.SubModuleID = int.Parse(datareader["SUBMODULE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["MODULE_NAME"]))
                    {
                        featureRightDetails.ModuleName = datareader["MODULE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SUBMODULE_NAME"]))
                    {
                        featureRightDetails.SubModuleName = datareader["SUBMODULE_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_NAME"]))
                    {
                        featureRightDetails.FeatureName = datareader["FEATURE_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(datareader["IS_CHECKED"]))
                    {
                        featureRightDetails.IsApplicable = Convert.ToBoolean
                            (datareader["IS_CHECKED"].ToString().Trim() == "Y" ? "True" : "False");
                    }
                    if (!Utility.IsNullOrEmpty(datareader["IS_CHECKED"]))
                    {
                        featureRightDetails.IsChecked = Convert.ToBoolean
                            (datareader["IS_CHECKED"].ToString().Trim() == "Y" ? "True" : "False");
                    }
                    if (!Utility.IsNullOrEmpty(datareader["ROLENAME"]))
                    {
                        featureRightDetails.RoleName = datareader["ROLENAME"].ToString().Trim();
                    }

                    featureRightMatrix.Add(featureRightDetails);
                }
                return featureRightMatrix;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                {
                    datareader.Close();
                }
            }


        }

        public DbCommand getAssignedCorporateRoleCommand { get; set; }

        public List<Roles> GetCorporateSelectRole()
        {
             IDataReader datareader = null;

            List<Roles> roles = new List<Roles>();

            try
            {
                DbCommand getFeatureRightsMatrixCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_CORPORATE_SELECTEDROLES");

                datareader = HCMDatabase.ExecuteReader(getFeatureRightsMatrixCommand);

                while (datareader.Read())
                {
                    Roles role = new Roles();

                    if (!Utility.IsNullOrEmpty(datareader["ROLID"]))
                    {
                        role.RoleID = int.Parse(datareader["ROLID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["ROLCODE"]))
                    {
                        role.RoleCode = datareader["ROLCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["ROLNAME"]))
                    {
                        role.RoleName = datareader["ROLNAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["IS_SELECTED"]))
                    {
                        role.IsSelected = datareader["IS_SELECTED"].ToString();
                    }

                    roles.Add(role);
                }
                return roles;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                {
                    datareader.Close();
                }
            }
        }
        public List<UsersRoleMatrix> GetAllCorporateUserRoleRights(string sortExpression, string sortOrder,int tenantID)
        {

            IDataReader dataReader = null;

            try
            {
                DbCommand getUserRoleRightsTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_USERROLES");

                HCMDatabase.AddInParameter(getUserRoleRightsTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getUserRoleRightsTypeCommand, "@ORDERBY", DbType.String, sortExpression);

                HCMDatabase.AddInParameter(getUserRoleRightsTypeCommand, "@TENANT_ID", DbType.Int16, tenantID);

                List<UsersRoleMatrix> userRoleRightsDetails = new List<UsersRoleMatrix>();

                dataReader = HCMDatabase.ExecuteReader(getUserRoleRightsTypeCommand);

                while (dataReader.Read())
                {

                    UsersRoleMatrix userRights = new UsersRoleMatrix();

                    if (!Utility.IsNullOrEmpty(dataReader["ROLID"]))
                    {
                        userRights.RoleID = int.Parse(dataReader["ROLID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLCODE"]))
                    {
                        userRights.RoleCode = dataReader["ROLCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        userRights.RoleName = dataReader["ROLNAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ROLJOBDESCRIPTION"]))
                    {
                        userRights.RoleJobDescription = dataReader["ROLJOBDESCRIPTION"].ToString();
                    }

                    userRoleRightsDetails.Add(userRights);

                }
                return userRoleRightsDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        public void DeleteCorporateRoleRights(int roleRightsID)
        {
       

            // Create a stored procedure command object.
            DbCommand deleteRoleCatgeoryTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_CORPORATE_ROLE_RIGHTS");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteRoleCatgeoryTypeCommand,
                "@RRTID", DbType.Int32, roleRightsID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteRoleCatgeoryTypeCommand);
        }

        public void InsertCorporateRoleRight(RoleRightMatrixDetail roleRight)
        {
            DbCommand insertRoleRightCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_CORPORATE_ROLE_RIGHTS");

            HCMDatabase.AddInParameter(insertRoleRightCommand, "@TENANTID", DbType.Int32, roleRight.TenantID);
            HCMDatabase.AddInParameter(insertRoleRightCommand, "@MODID", DbType.Int32, roleRight.ModuleID);
            HCMDatabase.AddInParameter(insertRoleRightCommand, "@SMODID", DbType.Int32, roleRight.SubModuleID);
            HCMDatabase.AddInParameter(insertRoleRightCommand, "@FEAID", DbType.Int32, roleRight.FeatureID);
            HCMDatabase.AddInParameter(insertRoleRightCommand, "@RRTCREATEDBY", DbType.Int32, roleRight.CreatedBy);
            HCMDatabase.AddInParameter(insertRoleRightCommand, "@RRTPAGE", DbType.Int16, roleRight.IsApplicable);
            HCMDatabase.AddInParameter(insertRoleRightCommand, "@ROLID", DbType.Int16, roleRight.RoleID);
            HCMDatabase.ExecuteNonQuery(insertRoleRightCommand);
        }

        public void InsertCorporateNewUserRoles(int userID, int roleID, int createdBy)
        {
            // Create a stored procedure command object.
            DbCommand insertUserRolesTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CORPORATE_USERROLES");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertUserRolesTypeCommand,
                "@USER_ID", DbType.Int32, userID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertUserRolesTypeCommand,
                "@ROLE_ID", DbType.Int32, roleID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertUserRolesTypeCommand,
                "@USERROL_CREATEDBY", DbType.Int32, createdBy);

            HCMDatabase.AddInParameter(insertUserRolesTypeCommand,
            "@USERROL_MODEFIEDBY", DbType.Int32, createdBy);

            HCMDatabase.ExecuteNonQuery(insertUserRolesTypeCommand);
        }
    }

}
