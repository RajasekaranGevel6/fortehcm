﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestSessionDLManager.cs
// File that represents the data layer for the Test Session module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using Forte.HCM.Support;

#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the Test Session module.
    /// This includes functionalities for retrieving Session details 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class TestSessionDLManager : DatabaseConnectionManager
    {
        #region Public Method

        /// <summary>
        /// Method that retrieves the list of test sessions for the given 
        /// search criteria.
        /// </summary>
        /// <param name="testSessionSearchCriteria"> 
        /// A <see cref="TestSessionSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/>  that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="direction">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="total">
        /// A <see cref="int"/> that holds the total records in the list as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="TestSessionSearch"/> that holds the test 
        /// sessions.
        /// </returns>
        public List<TestSessionSearch> GetTestSessionDetails(TestSessionSearchCriteria
            testSessionSearchCriteria, int pageNumber, int pageSize, string orderBy,
            SortType direction, out int total)
        {
            IDataReader datareader = null;
            total = 0;
            try
            {
                DbCommand getTestSessionDetailsCommand = HCMDatabase.GetStoredProcCommand("SPGET_SEARCH_TEST_SESSION");
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@CANDIDATE_SESSION_ID", DbType.String, testSessionSearchCriteria.CandidateSessionID);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@TEST_SESSION_ID", DbType.String, testSessionSearchCriteria.SessionKey);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@TEST_KEY", DbType.String, testSessionSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@TEST_NAME", DbType.String, testSessionSearchCriteria.TestName);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@TEST_SCHEDULE_AUTHOR_ID", DbType.String, testSessionSearchCriteria.SchedulerNameID == 0 ? null : testSessionSearchCriteria.SchedulerNameID.ToString());
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@TEST_SESSION_AUTHOR_ID", DbType.String, testSessionSearchCriteria.TestSessionCreatorID == 0 ? null : testSessionSearchCriteria.TestSessionCreatorID.ToString());
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@CANDIDATE_NAME", DbType.String, testSessionSearchCriteria.CandidateName);

                if (testSessionSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSessionSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getTestSessionDetailsCommand, "@ORDERBYDIRECTION", DbType.String, direction == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                datareader = HCMDatabase.ExecuteReader(getTestSessionDetailsCommand);
                List<TestSessionSearch> testSessionSearch = null;
                TestSessionSearch tstSessionSearch = null;
                while (datareader.Read())
                {
                    if (!Utility.IsNullOrEmpty(datareader["TOTAL"]))
                        total = Convert.ToInt32(datareader["TOTAL"]);
                    else
                    {
                        if (testSessionSearch == null)
                            testSessionSearch = new List<TestSessionSearch>();
                        if (tstSessionSearch == null)
                            tstSessionSearch = new TestSessionSearch();
                        if (!Utility.IsNullOrEmpty(datareader["TEST_SESSION_ID"]))
                            tstSessionSearch.TestSessionID = datareader["TEST_SESSION_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["TESTKEY"]))
                            tstSessionSearch.TestKey = datareader["TESTKEY"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_SESSION_ID"]))
                            tstSessionSearch.CandidateSessionID = datareader["CANDIDATE_SESSION_ID"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CLIENT_REQUEST_NUMBER"]))
                            tstSessionSearch.ClientRequestNumber = datareader["CLIENT_REQUEST_NUMBER"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["DATE_OF_PURCHASE"]))
                            tstSessionSearch.DateofPurchase = datareader["DATE_OF_PURCHASE"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CREDITS"]))
                            tstSessionSearch.Credits = datareader["CREDITS"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["ADMINISTERED_BY_ID"]))
                            tstSessionSearch.AdministeredID = Convert.ToInt32(datareader["ADMINISTERED_BY_ID"].ToString());
                        if (!Utility.IsNullOrEmpty(datareader["ADMINISTERED_BY"]))
                            tstSessionSearch.AdministeredBy = datareader["ADMINISTERED_BY"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["AUTHOR_FULLNAME"]))
                            tstSessionSearch.TestAuthorFullName = datareader["AUTHOR_FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_ID"]))
                            tstSessionSearch.CandidateID = Convert.ToInt32(datareader["CANDIDATE_ID"]);
                        if (!Utility.IsNullOrEmpty(datareader["CANIDATE_NAME"]))
                            tstSessionSearch.CandidateName = datareader["CANIDATE_NAME"].ToString();
                        if (!Utility.IsNullOrEmpty(datareader["CANDIDATE_FULLNAME"]))
                            tstSessionSearch.CandidateFullName = datareader["CANDIDATE_FULLNAME"].ToString().Trim();
                        if (!Utility.IsNullOrEmpty(datareader["DATE_OF_TEST"]))
                            tstSessionSearch.DateofTest = Convert.ToDateTime(datareader["DATE_OF_TEST"]).ToString("MM/dd/yyyy");
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_CANDIDATE_ICON"]))
                            tstSessionSearch.CandidateVisibleStatus = Convert.ToBoolean(datareader["SHOW_CANDIDATE_ICON"]);
                        else
                            tstSessionSearch.CandidateVisibleStatus = false;
                        if (!Utility.IsNullOrEmpty(datareader["CANCEL_REASON"]))
                            tstSessionSearch.CancelReason = datareader["CANCEL_REASON"].ToString().Replace("\r\n", "<br />");
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_CANCEL_ICON"]))
                            tstSessionSearch.ShowCancelIcon = Convert.ToBoolean(datareader["SHOW_CANCEL_ICON"]);
                        else
                            tstSessionSearch.ShowCancelIcon = false;
                        if (!Utility.IsNullOrEmpty(datareader["ATTEMPT_ID"]))
                            tstSessionSearch.AttemptID = Convert.ToInt32(datareader["ATTEMPT_ID"]);
                        else
                            tstSessionSearch.AttemptID = 0;
                        if (!Utility.IsNullOrEmpty(datareader["SHOW_TEST_SCORE"]))
                            tstSessionSearch.ShowTestScore = Convert.ToBoolean(datareader["SHOW_TEST_SCORE"]);
                        else
                            tstSessionSearch.ShowTestScore = false;

                        testSessionSearch.Add(tstSessionSearch);
                        tstSessionSearch = null;
                    }
                }
                return testSessionSearch;
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }
        
        /// <summary>
        /// Method that retreives the list of candidate session IDs as comma 
        /// separated and other related details for the given test session ID.
        /// </summary>
        /// <param name="testSessionID">
        /// A <see cref="string"/> that holds the test session ID.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that holds the test session detail.
        /// </returns>
        public TestSessionDetail GetCandidateSessions(string testSessionID)
        {
            IDataReader dataReader = null;

            try
            {
                // Check if object type is given.
                if (Utility.IsNullOrEmpty(testSessionID))
                {
                    throw new Exception
                        ("Test session ID cannot be null or empty");
                }
                TestSessionDetail testSessionDetail = null;

                // This SP helps to generate Question ID 
                DbCommand getCandidateSessionsCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_CANDIDATE_SESSIONS");

                HCMDatabase.AddInParameter(getCandidateSessionsCommand,
                    "@TEST_SESSION_ID", DbType.String, testSessionID.Trim());

                HCMDatabase.AddOutParameter(getCandidateSessionsCommand,
                  "@CANDIDATE_SESSIONS", DbType.String, 1000);

                dataReader = HCMDatabase.ExecuteReader
                    (getCandidateSessionsCommand);
                if (dataReader.Read())
                {
                    testSessionDetail = new TestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                        testSessionDetail.UserName = dataReader["NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        testSessionDetail.Email = dataReader["EMAIL"].ToString();
                }
                if (dataReader != null)
                    dataReader.Close();

                if (testSessionDetail != null)
                {
                    testSessionDetail.CandidateSessions = Convert.ToString
                        (HCMDatabase.GetParameterValue(getCandidateSessionsCommand, "@CANDIDATE_SESSIONS"));
                }
                return testSessionDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// Represents the method to get the bool value to show candidate
        /// test score
        /// </summary>
        /// <param name="candidateSessionID">
        /// A<see cref="string"/>that holds the candidate session id
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that holds whether to display score or not
        /// </returns>
        public bool GetShowTestScore(string candidateSessionID)
        {
            try
            {
                // Check if object type is given.
                if (Utility.IsNullOrEmpty(candidateSessionID))
                {
                    throw new Exception
                        ("Candidate session ID cannot be null or empty");
                }

                // This SP helps to generate Question ID 
                DbCommand getCandidateScoreDetails = HCMDatabase
                    .GetStoredProcCommand("SPGET_SHOW_TEST_SCORE");

                HCMDatabase.AddInParameter(getCandidateScoreDetails,
                    "@CAND_SESSION_KEYS", DbType.String, candidateSessionID.Trim());

                object value = HCMDatabase.ExecuteScalar(getCandidateScoreDetails);

                if (value != null)
                {
                    return Convert.ToBoolean(value);
                }
                return false;
            }
            finally
            { 
            }
        }
        /// <summary>
        /// This method gets the test session id details using navigatekey.
        /// </summary>
        /// A <param name="navigateKey"> holds the navigate key.</param>
        /// <returns></returns>
        public TestSessionIdDetails GetTestSessionIdDetails(string navigateKey)
        {
            IDataReader dataReader = null;
            try
            {
                // Create a stored procedure command object.
                DbCommand getTestSessionIdDetails = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_SESSION_TRACKING_BY_NAVIGATEKEY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestSessionIdDetails,
                    "@NAVIGATE_KEY", DbType.String, navigateKey);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestSessionIdDetails);

                TestSessionIdDetails testSessionIdDetails = null;

                if (dataReader.Read())
                {
                    // Instantiate the candidate test session id details instance
                    if (testSessionIdDetails == null)
                        testSessionIdDetails = new TestSessionIdDetails();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPT_ID"]))
                        testSessionIdDetails.AttemptID = Convert.ToInt32(dataReader["ATTEMPT_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_SESSION_KEY"]))
                        testSessionIdDetails.CandidateSessionId =
                            dataReader["CAND_SESSION_KEY"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        testSessionIdDetails.CandidateId =
                            Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString());
                }

                return testSessionIdDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public TestScheduleSearchCriteria GetPositionProfileTests(int positionProfileId, out int testCount, out string createBy)
        {
            IDataReader dataReader = null;
            TestScheduleSearchCriteria testSearchList = null; 
            testCount = 0;
            createBy = null;
            try 
            {
                // Create a stored procedure command object.
                DbCommand getTestDetails = HCMDatabase.
                    GetStoredProcCommand("SPGET_PP_CREATED_TESTS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getTestDetails,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileId);
                 
                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getTestDetails); 

                while (dataReader.Read())
                {
                    // Instantiate the candidate test session id details instance

                    if (testSearchList == null)
                        testSearchList = new TestScheduleSearchCriteria(); 
                     
                    if (testSearchList.ListTestdetail == null)
                        testSearchList.ListTestdetail = new List<TestDetail>();
 
                    TestDetail testDetail = new TestDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        testDetail.TestKey = dataReader["TEST_KEY"].ToString(); 

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_NAME"]))
                        testDetail.Name =
                            dataReader["TEST_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_COUNT"]))
                        testDetail.SessionCount =
                           Convert.ToInt32(dataReader["SESSION_COUNT"]);  

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        createBy = createBy +
                            dataReader["CREATED_BY"].ToString().Trim() + ", ";

                    testSearchList.ListTestdetail.Add(testDetail);
                    
                }

                   
                dataReader.NextResult();
                 
                while (dataReader.Read())
                {
                    if (testSearchList == null)
                        testSearchList = new TestScheduleSearchCriteria();

                    if (testSearchList.ListTestSessiondetail == null)
                        testSearchList.ListTestSessiondetail = new List<TestDetail>(); 

                    TestDetail testSession = new TestDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                        testSession.TestKey = dataReader["TEST_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_KEY"]))
                        testSession.SessionKey = dataReader["SESSION_KEY"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["SESSION_COUNT"]))
                        testSession.SessionCount = Convert.ToInt32(dataReader["SESSION_COUNT"]); 

                    testSearchList.ListTestSessiondetail.Add(testSession);
                }


                return testSearchList;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<TestSessionDetail> GetPositionProfileTestSessionCount(int positionProfileId)
        {
            IDataReader dataReader = null;
            List<TestSessionDetail> testSessionDetailList = null;
            try
            {
                DbCommand positionProfileCommand = HCMDatabase.GetStoredProcCommand("SPGET_PP_TEST_SESSION_COUNT");

                HCMDatabase.AddInParameter(positionProfileCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileId);

                dataReader = HCMDatabase.ExecuteReader(positionProfileCommand);

                while (dataReader.Read())
                {
                    if (testSessionDetailList == null)
                        testSessionDetailList = new List<TestSessionDetail>();

                    TestSessionDetail testSessionDetail = new TestSessionDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_KEY"]))
                    {
                        testSessionDetail.TestID = dataReader["TEST_KEY"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_SCHEDULED"]))
                    {
                        testSessionDetail.ScheduledTests = dataReader["TEST_SCHEDULED"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_COMPLETED"]))
                    {
                        testSessionDetail.CompletedTests = dataReader["TEST_COMPLETED"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_ID"]))
                    {
                        testSessionDetail.ClientRequestID = dataReader["POSITION_ID"].ToString();
                    }
                    testSessionDetailList.Add(testSessionDetail);
                }

                return testSessionDetailList;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<string> GetPositionProfileSubmittalCandidates(int positionProfileId,out int totalCount, out string createdBy)
        {
            IDataReader dataReader = null; 
            List<string> candidateIds = null;
            totalCount = 0;
            createdBy = null;

            try
            {
                candidateIds = new List<string>();

                DbCommand positionProfileCommand = HCMDatabase.GetStoredProcCommand("SPGET_PP_SUBMITTAL_CANDIDATES");

                HCMDatabase.AddInParameter(positionProfileCommand, "@POSITION_PROFILE_ID",
                    DbType.Int32, positionProfileId); 

                dataReader = HCMDatabase.ExecuteReader(positionProfileCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        totalCount = Convert.ToInt32(dataReader["TOTAL"]);
                        candidateIds.Add(totalCount.ToString());
                    }


                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                    {
                        if (createdBy == null)
                            createdBy = dataReader["CREATED_BY"].ToString();
                        else
                        {
                            if (!createdBy.Contains(dataReader["CREATED_BY"].ToString()))
                            {
                                createdBy += dataReader["CREATED_BY"].ToString() + ",";
                            }
                        }
                    }
                } 

                candidateIds.Add(createdBy);

                return candidateIds;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }  
        }
        #endregion
    }
}
