﻿#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    public class CompetencyVectorDLManager : DatabaseConnectionManager
    {
        public CompetencyVectorDLManager()
        {
        }

        /// <summary>
        /// This method gets the candidate vector table with selected
        /// candidate resume id.
        /// </summary>
        /// <param name="CandidateResumeID">Candidate resume id 
        /// for which to get competency vector table.</param>
        /// <returns>Dataset contains the competency vector table.</returns>
        public DataSet GetCompetencyVectorTable(int CandidateResumeID)
        {
            DataSet CompetencyVectorDataSet = null;
            DbCommand CompetencyVectorCommand = null;
            try
            {
                CompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPGET_RESUME_SKILL_MATRIX");
                HCMDatabase.AddInParameter(CompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, CandidateResumeID);
                CompetencyVectorDataSet = HCMDatabase.ExecuteDataSet(CompetencyVectorCommand);
                return CompetencyVectorDataSet;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(CompetencyVectorCommand)) CompetencyVectorCommand = null;
                if (!Utility.IsNullOrEmpty(CompetencyVectorDataSet)) CompetencyVectorDataSet = null;
            }
        }

        /// <summary>
        /// This method inserts candidate competency 
        /// in to the database.
        /// </summary>
        /// <param name="candidateCompetencyVector">
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int InsertCandidateCompetencyVector(CandidateCompetencyVector candidateCompetencyVector, IDbTransaction transaction)
        {
            DbCommand InsertCandidateCompetencyVectorCommand = null;
            try
            {
                InsertCandidateCompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_CANDIDATE_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@PARAMETER_ID", DbType.Int32, candidateCompetencyVector.Parameter_Id);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@VALUE", DbType.String, candidateCompetencyVector.CompetencyValue);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@REMARKS", DbType.String, candidateCompetencyVector.Remarks);
                HCMDatabase.AddInParameter(InsertCandidateCompetencyVectorCommand, "@CREATED_BY", DbType.Int32, candidateCompetencyVector.Created_By);
                return HCMDatabase.ExecuteNonQuery(InsertCandidateCompetencyVectorCommand, transaction as DbTransaction);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(InsertCandidateCompetencyVectorCommand)) InsertCandidateCompetencyVectorCommand = null;
            }
        }

        /// <summary>
        /// This method updates the candidate competency vector table
        /// to the database.
        /// </summary>
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int UpdateCandidateCompetencyVector(CandidateCompetencyVector candidateCompetencyVector, IDbTransaction transaction)
        {
            DbCommand UpdateCandidateCompetencyVectorCommand = null;
            try
            {
                if (candidateCompetencyVector.Old_Vector_Id != 0)
                {
                    UpdateCandidateCompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_CANDIDATE_COMPETENCY_VECTOR");
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@OLD_VECTOR_ID", DbType.Int32, candidateCompetencyVector.Old_Vector_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@PARAMETER_ID", DbType.Int32, candidateCompetencyVector.Parameter_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VALUE", DbType.String, candidateCompetencyVector.CompetencyValue);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@REMARKS", DbType.String, candidateCompetencyVector.Remarks);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@MODIFIED_BY", DbType.Int32, candidateCompetencyVector.Created_By);
                    HCMDatabase.ExecuteScalar(UpdateCandidateCompetencyVectorCommand, transaction as DbTransaction);
                    return 1;
                }
                else
                {
                    UpdateCandidateCompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_CANDIDATE_COMPETENCY_VECTOR");
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@PARAMETER_ID", DbType.Int32, candidateCompetencyVector.Parameter_Id);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VALUE", DbType.String, candidateCompetencyVector.CompetencyValue);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@REMARKS", DbType.String, candidateCompetencyVector.Remarks);
                    HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@MODIFIED_BY", DbType.Int32, candidateCompetencyVector.Created_By);
                    return HCMDatabase.ExecuteNonQuery(UpdateCandidateCompetencyVectorCommand, transaction as DbTransaction);
                }
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateCandidateCompetencyVectorCommand)) UpdateCandidateCompetencyVectorCommand = null;
            }
        }


        /// <summary>
        /// This method updates the candidate competency vectorid 
        /// to the database.
        /// </summary>
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <param name="transaction">DB Transaction</param>
        /// <returns>No of rows effected</returns>
        public int UpdateCandidateCompetencyVectorIdWithTransaction(
            CandidateCompetencyVector candidateCompetencyVector, IDbTransaction transaction)
        {
            DbCommand UpdateCandidateCompetencyVectorCommand = null;
            try
            {
                UpdateCandidateCompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_CANDIDATE_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@OLD_VECTOR_ID", DbType.Int32, candidateCompetencyVector.Old_Vector_Id);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@PARAMETER_ID", DbType.Int32, candidateCompetencyVector.Parameter_Id);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@VALUE", DbType.String, candidateCompetencyVector.CompetencyValue);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@REMARKS", DbType.String, candidateCompetencyVector.Remarks);
                HCMDatabase.AddInParameter(UpdateCandidateCompetencyVectorCommand, "@MODIFIED_BY", DbType.Int32, candidateCompetencyVector.Created_By);
                return HCMDatabase.ExecuteNonQuery(UpdateCandidateCompetencyVectorCommand, transaction as DbTransaction);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateCandidateCompetencyVectorCommand)) UpdateCandidateCompetencyVectorCommand = null;
            }
        }

        /// <summary>
        /// This method delete the vector for a candidate
        /// from the database.
        /// </summary>
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <returns>No of rows effected</returns>
        public int DeleteCandidateCompetencyVector(CandidateCompetencyVector candidateCompetencyVector)
        {
            DbCommand DeleteCandidateCompetencyCommand = null;
            try
            {
                DeleteCandidateCompetencyCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_CANDIDATE_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(DeleteCandidateCompetencyCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                HCMDatabase.AddInParameter(DeleteCandidateCompetencyCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                return HCMDatabase.ExecuteNonQuery(DeleteCandidateCompetencyCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(DeleteCandidateCompetencyCommand)) DeleteCandidateCompetencyCommand = null;
            }
        }

        /// <summary>
        /// This method delete the vector for a candidate
        /// from the database.
        /// </summary>
        /// A<see cref="int"/>that holds the resume id
        /// A<see cref="int"/>that holds the old vector id
        /// Note This variable will be use ful when we are updating
        /// vector id.
        /// A<see cref="int"/>that holds the vector id
        /// A<see cref="int"/>that holds the parameter id
        /// A<see cref="string"/>that holds the competency value
        /// A<see cref="string"/>that holds the remarks value
        /// </param>
        /// <returns>No of rows effected</returns>
        public int DeleteCandidateCompetencyVectorWithTransaction(
            CandidateCompetencyVector candidateCompetencyVector, IDbTransaction transaction)
        {
            DbCommand DeleteCandidateCompetencyCommand = null;
            try
            {
                DeleteCandidateCompetencyCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_CANDIDATE_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(DeleteCandidateCompetencyCommand, "@CAND_RESUME_ID", DbType.Int32, candidateCompetencyVector.Cand_Resume_Id);
                HCMDatabase.AddInParameter(DeleteCandidateCompetencyCommand, "@VECTOR_ID", DbType.Int32, candidateCompetencyVector.Vector_Id);
                return HCMDatabase.ExecuteNonQuery(DeleteCandidateCompetencyCommand, transaction as DbTransaction);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(DeleteCandidateCompetencyCommand)) DeleteCandidateCompetencyCommand = null;
            }
        }

        /// <summary>
        /// Gets the vector id from the group id and
        /// vector name
        /// </summary>
        /// <param name="Group_Id">Group id for which to get the
        /// vector id</param>
        /// <param name="Vector_Name">vector name for which to get the
        /// vector id</param>
        /// <returns>Vector id for the passed group id and vector name.</returns>
        public int GetVectorIDFromGroupIdAndVectorName(int GroupId, string Vector_Name)
        {
            DbCommand CompetencyVectorCommand = null;
            IDataReader dataReader = null;
            try
            {
                int VectorId = 0;
                CompetencyVectorCommand = HCMDatabase.GetStoredProcCommand("SPGET_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(CompetencyVectorCommand, "@GROUP_ID", DbType.Int32, GroupId);
                HCMDatabase.AddInParameter(CompetencyVectorCommand, "@VECTOR_NAME", DbType.String, Vector_Name);
                dataReader = HCMDatabase.ExecuteReader(CompetencyVectorCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["VECTOR_ID"]))
                        VectorId = Convert.ToInt32(dataReader["VECTOR_ID"]);
                }
                return VectorId;
            }
            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(CompetencyVectorCommand)) CompetencyVectorCommand = null;
            }
        }

        /// <summary>
        /// This method returns the Competency Vector group table.
        /// </summary>
        /// <returns>Dataset contains the competency vector group table.</returns>
        public DataSet GetCompetencyVectorGroupTable()
        {
            DbCommand CompetencyVectorTableCommand = null;
            try
            {
                CompetencyVectorTableCommand = HCMDatabase.GetStoredProcCommand("SPGET_COMPETENCY_VECTOR_GROUP");
                return HCMDatabase.ExecuteDataSet(CompetencyVectorTableCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(CompetencyVectorTableCommand)) CompetencyVectorTableCommand = null;
            }
        }

        /// <summary>
        /// Represents the method to return the list of competency group 
        /// vector details
        /// </summary>
        /// <returns>
        /// A<see cref="List<CompetencyVectorGroup>"/>that holds the 
        /// list of competencty vector group
        /// </returns>
        public List<CompetencyVectorGroup> GetCompetencyVectorGroup()
        {
            DbCommand GetCompetencyVectorCommand = null;

            IDataReader dataReader = null;

            List<CompetencyVectorGroup> competencyVectorGroups = new List<CompetencyVectorGroup>();

            try
            {
                GetCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_VECTORS_GROUP");

                dataReader = HCMDatabase.ExecuteReader(GetCompetencyVectorCommand);

                while (dataReader.Read())
                {
                    CompetencyVectorGroup competencyVectorGroup = new CompetencyVectorGroup();

                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_ID"]))
                    {
                        competencyVectorGroup.VectorGroupID = dataReader["GROUP_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                    {
                        competencyVectorGroup.VectorGroupName = dataReader["GROUP_NAME"].ToString();
                    }

                    competencyVectorGroups.Add(competencyVectorGroup);
                }

                return competencyVectorGroups;
            }

            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
            }
        }

        /// <summary>
        /// Represents the method to get the competency vector details for 
        /// the given search criteria
        /// </summary>
        /// <param name="vectorGroupId">
        /// A<see cref="string"/>that holds the vector group ID
        /// </param>
        /// <param name="vectorName">
        /// A<see cref="string"/>that holds the vector name
        /// </param>
        /// <param name="vectorAliasName">
        /// A<see cref="string"/>that holds the vector alias name
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order by value
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/>that holds the page number
        /// </param>
        /// <param name="pgeSize">
        /// A<see cref="int"/>that holds the page size
        /// </param>
        /// <returns>
        /// A<see cref="List<CompetencyVectorDetails>"/>that holds the 
        /// list of competency vector 
        /// </returns>
        public List<CompetencyVectorDetails> GetCompetencyVectorDetails(string vectorGroupId,
            string vectorName, string vectorAliasName, string orderBy, string sortDirection,
            int pageNumber, object pageSize, out int total)
        {
            DbCommand GetCompetencyVectorCommand = null;

            IDataReader dataReader = null;

            List<CompetencyVectorDetails> competencyVectorDetails = new List<CompetencyVectorDetails>();

            total = 0;

            try
            {
                GetCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPSEARCH_COMPETENCY_VECTORS");

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@GROUP_NAME", DbType.String,
                    vectorGroupId.Trim() == "0" ? null : vectorGroupId);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@VECTOR_NAME", DbType.String,
                    vectorName.Trim().Length == 0 ? null : vectorName);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@VECTOR_ALIAS_NAME", DbType.String,
                    vectorAliasName.Trim().Length == 0 ? null : vectorAliasName);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@PAGENUM", DbType.Int32,
                 pageNumber);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@PAGESIZE",
                    pageSize == null ? DbType.String : DbType.Int32,
                    pageSize);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@ORDERBY", DbType.String,
                orderBy);

                HCMDatabase.AddInParameter(GetCompetencyVectorCommand, "@ORDERBYDIRECTION", DbType.String,
                 sortDirection);

                dataReader = HCMDatabase.ExecuteReader(GetCompetencyVectorCommand);

                while (dataReader.Read())
                {
                    CompetencyVectorDetails competencyVectorDetail = new CompetencyVectorDetails();

                    if (Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["VECTOR_ID"]))
                        {
                            competencyVectorDetail.VectorID = int.Parse(dataReader["VECTOR_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["GROUP_ID"]))
                        {
                            competencyVectorDetail.VectorGroupID = dataReader["GROUP_ID"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                        {
                            competencyVectorDetail.VectorGroup = dataReader["GROUP_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["VECTOR_NAME"]))
                        {
                            competencyVectorDetail.VectorName = dataReader["VECTOR_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ALIAS_NAME"]))
                        {
                            competencyVectorDetail.AliasName = dataReader["ALIAS_NAME"].ToString();
                        }

                        competencyVectorDetails.Add(competencyVectorDetail);
                    }
                    else
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                        {
                            total = int.Parse(dataReader["TOTAL"].ToString());
                        }
                    }
                }

                return competencyVectorDetails;
            }

            finally
            {
                if ((!Utility.IsNullOrEmpty(dataReader)) && (!dataReader.IsClosed)) dataReader.Close();
            }

        }

        /// <summary>
        /// Represents the method to update the vector manager alias name
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id 
        /// </param>
        /// <param name="vectorAliasName">
        /// A<see cref="string"/>that holds the vector alias name
        /// </param>
        public void UpdateCompetencyVectorManagerAliasName(int vectorID, string vectorAliasName)
        {
            DbCommand UpdateCompetencyVectorCommand = null;

            try
            {
                UpdateCompetencyVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_COMPETENCY_VECTORS");

                HCMDatabase.AddInParameter(UpdateCompetencyVectorCommand, "@VECTOR_ID", DbType.Int32, vectorID);

                HCMDatabase.AddInParameter(UpdateCompetencyVectorCommand, "@VECTOR_ALIAS_NAME", DbType.String,
                    vectorAliasName.Trim());

                HCMDatabase.ExecuteScalar(UpdateCompetencyVectorCommand);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Represents the method to check whether the vector id is involved 
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id
        /// </param>
        /// <returns>
        /// A<see cref="bool"/>that returns whether id is invloved or not
        /// </returns>
        public bool CheckVectorIDInvolved(int vectorID)
        {
            DbCommand CheckVectorIDCommand = null;

            object existVectorID = null;

            try
            {
                CheckVectorIDCommand = HCMDatabase.
                    GetStoredProcCommand("SPCHECK_VECTOR_ID");

                HCMDatabase.AddInParameter(CheckVectorIDCommand, "@VECTOR_ID", DbType.Int32, vectorID);

                existVectorID = HCMDatabase.ExecuteScalar(CheckVectorIDCommand);

                if (Utility.IsNullOrEmpty(existVectorID))
                    return true;
                else
                    return false;
            }
            finally
            {


            }

        }

        /// <summary>
        /// Represents the method to delete a competency vector
        /// </summary>
        /// <param name="vectorID">
        /// A<see cref="int"/>that holds the vector id
        /// </param>
        public void DeleteCompetencyVector(int vectorID)
        {
            DbCommand DeleteVectorCommand = null;

            try
            {
                DeleteVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPDELETE_VECTOR");

                HCMDatabase.AddInParameter(DeleteVectorCommand, "@VECTOR_ID", DbType.Int32, vectorID);

                HCMDatabase.ExecuteScalar(DeleteVectorCommand);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Represents the method to add a new vector
        /// </summary>
        /// <param name="vectorGroupID">
        /// A<see cref="int"/>that holds the vector Id
        /// </param>
        /// <param name="vectorName">
        /// A<see cref="string"/>that holds the vector name
        /// </param>
        /// <param name="aliasName">
        /// A<see cref="string"/>that holds the alias name
        /// </param>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the userid 
        /// </param>
        public bool AddNewVector(int vectorGroupID, string vectorName, string aliasName, int userID)
        {
            DbCommand InsertVectorCommand = null;

            bool value = false;

            try
            {
                InsertVectorCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_VECTOR");

                HCMDatabase.AddInParameter(InsertVectorCommand, "@VECTOR_GROUP_ID", DbType.Int32,
                   vectorGroupID);

                HCMDatabase.AddInParameter(InsertVectorCommand, "@VECTOR_NAME", DbType.String, vectorName);

                HCMDatabase.AddInParameter(InsertVectorCommand, "@ALIAS_NAME", DbType.String, aliasName);

                HCMDatabase.AddInParameter(InsertVectorCommand, "@CREATED_BY", DbType.String, userID);

                HCMDatabase.AddOutParameter(InsertVectorCommand, "@EXISTS", DbType.String, 1);

                HCMDatabase.ExecuteNonQuery(InsertVectorCommand);

                string ifExists = HCMDatabase.GetParameterValue(InsertVectorCommand, "@EXISTS").ToString().Trim();

                value = ifExists == "Y" ? false : true;

                return value;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Method that retrieves the group name and vector name.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CertificationTitle"/> that holds group name
        /// </returns>
        public List<CertificationTitle> GetGroupName(int vectorID)
        {
            IDataReader dataReader = null;
            List<CertificationTitle> certificationDetail = null;

            try
            {
                DbCommand getCertificationTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_COMPETENCY_GROUP_NAME");

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@VECTOR_ID", DbType.Int32, vectorID);

                dataReader = HCMDatabase.ExecuteReader(getCertificationTitleCommand);

                while (dataReader.Read())
                {
                    if (certificationDetail == null)
                        certificationDetail = new List<CertificationTitle>();

                    CertificationTitle certificationObj = new CertificationTitle();

                    if (!Utility.IsNullOrEmpty(dataReader["VECTOR_NAME"]))
                    {
                        certificationObj.VectorName = dataReader["VECTOR_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                    {
                        certificationObj.GroupName = dataReader["GROUP_NAME"].ToString().Trim();
                    }

                    // Add to the list.
                    certificationDetail.Add(certificationObj);
                }

                return certificationDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the certification title.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CertificationTitle"/> that holds certification title details
        /// </returns>
        public List<CertificationTitle> GetCertificationTitle(int vectorID)
        {
            IDataReader dataReader = null;
            List<CertificationTitle> certificationDetail = null;

            try
            {
                DbCommand getCertificationTitleCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_PARSER_CERTIFICATION_TITLE");

                HCMDatabase.AddInParameter(getCertificationTitleCommand,
                    "@VECTOR_ID", DbType.Int32, vectorID);

                dataReader = HCMDatabase.ExecuteReader(getCertificationTitleCommand);

                while (dataReader.Read())
                {
                    if (certificationDetail == null)
                        certificationDetail = new List<CertificationTitle>();

                    CertificationTitle certificationObj = new CertificationTitle();

                    if (!Utility.IsNullOrEmpty(dataReader["ID"]))
                    {
                        certificationObj.ID = Convert.ToInt32
                            (dataReader["ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_TITLE"]))
                    {
                        certificationObj.Title = dataReader["CERTIFICATION_TITLE"].
                            ToString().Trim();
                    }

                    // Add to the list.
                    certificationDetail.Add(certificationObj);
                }

                return certificationDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that inserts certification title.
        /// </summary>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationTitle"/> that holds the certification detail.
        /// </param>
        public void InserCertification(CertificationTitle certificationDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertCertificationTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_PARSER_CERTIFICATION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCertificationTitleCommand,
                "@CERTIFICATION_TITLE", DbType.String, certificationDetail.Title);

            HCMDatabase.AddInParameter(insertCertificationTitleCommand,
                "@USER_ID", DbType.Int32, certificationDetail.CreatedBy);

            HCMDatabase.AddInParameter(insertCertificationTitleCommand,
              "@VECTOR_ID", DbType.Int32, certificationDetail.VectorID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertCertificationTitleCommand);
        }

        /// <summary>
        /// Method that updates certification title.
        /// </summary>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationTitle"/> that holds the certification detail.
        /// </param>
        public void UpdateCertification(CertificationTitle certificationDetail)
        {
            // Create a stored procedure command object.
            DbCommand updateCertificationTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CERTIFICATION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCertificationTitleCommand,
                "@ID", DbType.Int32, certificationDetail.ID);

            HCMDatabase.AddInParameter(updateCertificationTitleCommand,
                "@CERTIFICATION_TITLE", DbType.String, certificationDetail.Title);

            HCMDatabase.AddInParameter(updateCertificationTitleCommand,
                "@USER_ID", DbType.Int32, certificationDetail.ModifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCertificationTitleCommand);
        }

        /// <summary>
        /// Method that deletes certification.
        /// </summary>
        /// <param name="ID">
        /// A <see cref="int"/> that holds the certification ID.
        /// </param>
        public void DeleteCertification(int ID)
        {
            // Create a stored procedure command object.
            DbCommand deletePositionTitleCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_PARSER_CERTIFICATION_TITLE");

            // Add input parameters.
            HCMDatabase.AddInParameter(deletePositionTitleCommand,
                "@ID", DbType.Int32, ID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deletePositionTitleCommand);
        }
    }
}


