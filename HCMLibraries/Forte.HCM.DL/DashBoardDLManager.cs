﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// DashBoardDLManager.cs
// File that represents the data layer for the DashBoard module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives                                                             
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the dashboard module.
    /// This includes functionalities for retrieving the Dashboard 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class DashBoardDLManager : DatabaseConnectionManager
    {
        #region Public method                                                  

        /// <summary>
        /// Get the Dashboard ID.
        /// </summary>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the User id
        /// </param>
        /// <param name="groupName">
        /// A<see cref="int"/>that holds the group name
        /// </param>
        /// <returns>
        /// A<see cref="string"/>
        /// Get the Dashboard Details based on the userid.
        /// </returns>
        public DashBoard GetDashBoardId(string userID, string groupName)
        {
            IDataReader dataReader = null;
            DashBoard dashBoard = null;
            try
            {
                DbCommand dashBoardCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_DASHBOARD_USERID");

                HCMDatabase.AddInParameter(dashBoardCommand,
                    "@USER_ID", DbType.String, userID);
                HCMDatabase.AddInParameter(dashBoardCommand,
                    "@GROUPNAME", DbType.String, groupName == "" ? null : groupName);

                dataReader = HCMDatabase.ExecuteReader(dashBoardCommand);
                if (dataReader.Read())
                {
                    if (dashBoard == null)
                        dashBoard = new DashBoard();

                    dashBoard.DashBoardID = dataReader["DASHBOARD_ID"].ToString().Trim();
                    dashBoard.DashBoardSectionID = dataReader["DASHBOARDSECTION_ID"].ToString().Trim();
                }
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

            return dashBoard;
        }

        /// <summary>
        /// delete the widget instance based on the dashBoardID
        /// </summary>
        /// <param name="userID">
        /// A<see cref="string"/>that holds the dashBoardID
        /// </param>
        public void DeleteWidgetInstance(string userID)
        {
            DbCommand widgetInstanceDeleteStatusCommand = HCMDatabase.
                            GetStoredProcCommand("SPDELETE_WIDGETINSTANCE");
            HCMDatabase.AddInParameter(widgetInstanceDeleteStatusCommand,
                "@USER_ID", DbType.String, userID);
            HCMDatabase.ExecuteNonQuery(widgetInstanceDeleteStatusCommand);
        }

        /// <summary>
        /// Get the Dashboard ID.
        /// </summary>
        /// <param name="groupName">
        /// A<see cref="string "/>that holds the group name
        /// </param>
        /// <returns>
        /// A List of<see cref="WidgetTypes"/>
        /// Get the Dashboard ID based on the userid.
        /// </returns>
        public List<WidgetTypes> GetWidgetTypes(string groupName)
        {
            IDataReader dataReader = null;
            List<WidgetTypes> widgetTypes = null;
            try
            {
                DbCommand widgetTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_WIDGETTYPES");

                HCMDatabase.AddInParameter(widgetTypeCommand,
                    "@GROUPNAME", DbType.String, groupName);

                dataReader = HCMDatabase.ExecuteReader(widgetTypeCommand);
                while (dataReader.Read())
                {
                    if (widgetTypes == null)
                        widgetTypes = new List<WidgetTypes>();

                    WidgetTypes widgetType = new WidgetTypes();

                    widgetType.ID = new Guid(dataReader["ID"].ToString().Trim());
                    widgetType.Title = dataReader["TITLE"].ToString().Trim();
                    widgetType.Description = dataReader["DESCRIPTION"].ToString().Trim();
                    widgetType.SkinID = dataReader["SKINID"].ToString().Trim();
                    widgetTypes.Add(widgetType);

                }
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

            return widgetTypes;
        }

        ///// <summary>
        ///// Get the HCM dashboard.
        ///// </summary>
        ///// <param name="userID">The user ID.</param>
        ///// <param name="totalCount">The total count.</param>
        ///// <returns>dashboard details </returns>
        public HCMDashboard GetHCMDashboard(int userID, int totalCount)
        {
            IDataReader dataReader = null;
            HCMDashboard hcmDashboard = new HCMDashboard();
            try
            {
                List<HCMDashboardTest> hcmDashboardTestList = null;
                List<HCMDashboardPositionProfile> hcmDashboardPositionProfileList = null;
                List<HCMDashboardTalentScout> hcmDashboardTalentScoutList = null;
                List<HCMDashboardTestReport> hcmDashboardTestReportList = null;
                List<HCMDashboardResumeUpload> hcmDashboardResumeUploadList = null;
                List<HCMDashboardTestSession> hcmDashboardTestSessionList = null;
                List<HCMDashboardResumeUpload> hcmDashboardResumeDownloadList = null;
                DbCommand HCMDashboardCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_DASHBOARD_DETAILS");
                HCMDatabase.AddInParameter(HCMDashboardCommand,
                    "@USER_ID", DbType.Int32, userID);
                HCMDatabase.AddInParameter(HCMDashboardCommand,
                    "@TOTAL_COUNT", DbType.Int32, totalCount);
                dataReader = HCMDatabase.ExecuteReader(HCMDashboardCommand);
                while (dataReader.Read())
                {
                    if (hcmDashboardPositionProfileList == null)
                        hcmDashboardPositionProfileList = new List<HCMDashboardPositionProfile>();
                    HCMDashboardPositionProfile hcmDashboardPositionProfile = new HCMDashboardPositionProfile();
                    hcmDashboardPositionProfile.ClientName = dataReader["CLIENT_NAME"].ToString();
                    hcmDashboardPositionProfile.PositionProfileName = dataReader["POSITION_NAME"].ToString().Trim();
                    hcmDashboardPositionProfile.PositionProfileID = int.Parse(dataReader["POSITION_PROFILE_ID"].ToString());
                    hcmDashboardPositionProfileList.Add(hcmDashboardPositionProfile);
                }
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (hcmDashboardTestList == null)
                        hcmDashboardTestList = new List<HCMDashboardTest>();
                    HCMDashboardTest hcmDashboardTest = new HCMDashboardTest();
                    hcmDashboardTest.TestKey = dataReader["TEST_KEY"].ToString();
                    hcmDashboardTest.TestName = dataReader["TEST_NAME"].ToString().Trim();
                    hcmDashboardTestList.Add(hcmDashboardTest);
                }

                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (hcmDashboardTalentScoutList == null)
                        hcmDashboardTalentScoutList = new List<HCMDashboardTalentScout>();
                    HCMDashboardTalentScout hcmDashboardTalentScout = new HCMDashboardTalentScout();
                    hcmDashboardTalentScout.SearchTearms = dataReader["SEARCH_TERMS"].ToString();
                    hcmDashboardTalentScout.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();
                    hcmDashboardTalentScoutList.Add(hcmDashboardTalentScout);
                }

                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (hcmDashboardTestSessionList == null)
                        hcmDashboardTestSessionList = new List<HCMDashboardTestSession>();
                    HCMDashboardTestSession hcmDashboardTestSession = new HCMDashboardTestSession();
                    hcmDashboardTestSession.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                    hcmDashboardTestSession.SessionName = dataReader["SESSION_NAME"].ToString();
                    hcmDashboardTestSession.TestName = dataReader["TEST_NAME"].ToString();
                    hcmDashboardTestSession.CandidateDetails = dataReader["CANDIDATE_DETAILS"].ToString();
                    
                    hcmDashboardTestSessionList.Add(hcmDashboardTestSession);
                }


                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (hcmDashboardTestReportList == null)
                        hcmDashboardTestReportList = new List<HCMDashboardTestReport>();
                    HCMDashboardTestReport hcmDashboardTestReport = new HCMDashboardTestReport();
                    hcmDashboardTestReport.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                    hcmDashboardTestReport.TestKey = dataReader["TEST_KEY"].ToString().Trim();
                    hcmDashboardTestReport.TestName = dataReader["TEST_NAME"].ToString();
                    hcmDashboardTestReport.CandidateDetails = dataReader["CAND_DETAILS"].ToString();
                    hcmDashboardTestReportList.Add(hcmDashboardTestReport);
                }

                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (hcmDashboardResumeUploadList == null)
                        hcmDashboardResumeUploadList = new List<HCMDashboardResumeUpload>();
                    HCMDashboardResumeUpload hcmDashboardResumeUpload = new HCMDashboardResumeUpload();

                  //  hcmDashboardResumeUpload.UserID = dataReader["USERID"].ToString();
                    hcmDashboardResumeUpload.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                    hcmDashboardResumeUpload.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                    hcmDashboardResumeUpload.LastName = dataReader["LAST_NAME"].ToString();
                    hcmDashboardResumeUploadList.Add(hcmDashboardResumeUpload);
                }
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (hcmDashboardResumeDownloadList == null)
                        hcmDashboardResumeDownloadList = new List<HCMDashboardResumeUpload>();
                    HCMDashboardResumeUpload hcmDashboardResumeUpload = new HCMDashboardResumeUpload();
                    hcmDashboardResumeUpload.CandidateID = dataReader["CANDIDATE_ID"].ToString();
                    hcmDashboardResumeUpload.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                    hcmDashboardResumeUpload.LastName = dataReader["LAST_NAME"].ToString();
                    hcmDashboardResumeDownloadList.Add(hcmDashboardResumeUpload);
                }

                hcmDashboard.PositionProfile = hcmDashboardPositionProfileList;
                hcmDashboard.Test = hcmDashboardTestList;
                hcmDashboard.TestReport = hcmDashboardTestReportList;
                hcmDashboard.TestSession = hcmDashboardTestSessionList;
                hcmDashboard.ResumeUpload = hcmDashboardResumeUploadList;
                hcmDashboard.TalentScout = hcmDashboardTalentScoutList;
                hcmDashboard.ResumeDownload = hcmDashboardResumeDownloadList;

                return hcmDashboard;
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        #endregion Public method
    }
}
