﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalDataDLManager.cs
// File that represents the data layer for the external data manager
// component. This will talk to the database to perform the operations 
// associated with the external data manager component.

#endregion Header

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the external data manager
    /// component. This will talk to the database to perform the operations 
    /// associated with the external data manager component.
    /// </summary>
    public class ExternalDataDLManager : DatabaseConnectionManager
    {
        /// <summary>
        /// Method that represents the constructor.
        /// </summary>
        public ExternalDataDLManager()
        {

        }

        /// <summary>
        /// Method that retreives the external data structure for the given key.
        /// </summary>
        /// <param name="dataStructureKey">
        /// A <see cref="string"/> that holds the data structure key.
        /// </param> 
        /// <returns>
        /// A <see cref="ExternalDataStructure"/> that holds the external data 
        /// structure.
        /// </returns>
        public ExternalDataStructure GetExternalDataStructure(string dataStructureKey)
        {
            IDataReader dataReader = null;

            try
            {
                // Construct command.
                DbCommand getStructure = HCMDatabase.GetStoredProcCommand("SPGET_EXTERNAL_DATA_STRUCTURE");

                // Add parameters.
                HCMDatabase.AddInParameter(getStructure, "@DATA_STRUCTURE_KEY", DbType.String, dataStructureKey);

                // Execute the result set.
                dataReader = HCMDatabase.ExecuteReader(getStructure);

                ExternalDataStructure dataStructure = null;

                if (dataReader.Read())
                {
                    dataStructure = new ExternalDataStructure();

                    if (!Utility.IsNullOrEmpty(dataReader["DATA_STRUCTURE"]))
                        dataStructure.DataStructure = dataReader["DATA_STRUCTURE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]))
                        dataStructure.Comments = dataReader["COMMENTS"].ToString();
                }
                return dataStructure;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the position profile detail.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <returns>
        /// A <see cref="PositionProfileDetail"/> that holds the position profile detail.
        /// </returns>
        public PositionProfileDetail GetPositionProfile(int positionProfileID)
        {
            IDataReader dataReader = null;
            PositionProfileDetail positionProfileDetail = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand positionProfileDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_EXTERNAL_DATA_POSITION_PROFILE");
                HCMDatabase.AddInParameter(positionProfileDetailCommand, "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
                dataReader = HCMDatabase.ExecuteReader(positionProfileDetailCommand);

                if (dataReader.Read())
                {
                    // Construct the admin settings object.
                    positionProfileDetail = new PositionProfileDetail();
                    positionProfileDetail.RegisteredByName = dataReader["REGISTER_NAME"].ToString();
                    positionProfileDetail.PositionName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    positionProfileDetail.PositionProfileKey = dataReader["POSITION_PROFILE_KEY"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_ADDITIONAL_INFORMATION"]))
                        positionProfileDetail.PositionProfileAdditionalInformation =
                            dataReader["POSITION_ADDITIONAL_INFORMATION"].ToString();
                    positionProfileDetail.RegistrationDate = Convert.ToDateTime(dataReader["REGISTRATION_DATE"]);
                    positionProfileDetail.SubmittalDate = Convert.ToDateTime(dataReader["SUBMITTAL_DEADLINE"]);
                    positionProfileDetail.PositionID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"]);
                    positionProfileDetail.RegisteredBy = Convert.ToInt32(dataReader["REGISTER_ID"]);

                    // Construct client information.
                    positionProfileDetail.ClientInformation = new ClientInformation();

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                        positionProfileDetail.ClientInformation.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        positionProfileDetail.ClientInformation.ClientName = dataReader["CLIENT_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["FEIN_NO"]))
                        positionProfileDetail.ClientInformation.FeinNo = dataReader["FEIN_NO"].ToString();

                    // Client contact information.
                    positionProfileDetail.ClientInformation.ContactInformation = new ContactInformation();
                    positionProfileDetail.ClientInformation.ContactInformation.Phone = new PhoneNumber();

                    if (!Utility.IsNullOrEmpty(dataReader["PHONE_NUMBER"]))
                        positionProfileDetail.ClientInformation.ContactInformation.Phone.Office = dataReader["PHONE_NUMBER"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        positionProfileDetail.ClientInformation.ContactInformation.EmailAddress = dataReader["EMAIL_ID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["STREET_ADDRESS"]))
                        positionProfileDetail.ClientInformation.ContactInformation.StreetAddress = dataReader["STREET_ADDRESS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CITY_NAME"]))
                        positionProfileDetail.ClientInformation.ContactInformation.City = dataReader["CITY_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["STATE_NAME"]))
                        positionProfileDetail.ClientInformation.ContactInformation.State = dataReader["STATE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ZIPCODE"]))
                        positionProfileDetail.ClientInformation.ContactInformation.PostalCode = dataReader["ZIPCODE"].ToString();
                }

                dataReader.NextResult();
                List<SegmentDetail> segmentDetailList = null;
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(segmentDetailList))
                        segmentDetailList = new List<SegmentDetail>();
                    SegmentDetail segmentDetail = new SegmentDetail();
                    segmentDetail.DisplayOrder = Convert.ToInt32(dataReader["DISPLAY_ORDER"]);
                    string str = dataReader["SEGMENT_DATA"].ToString();
                    /*XmlDocument xml = new XmlDocument();
                    xml.InnerXml = str;*/
                    segmentDetail.DataSource = dataReader["SEGMENT_DATA"].ToString();
                    segmentDetail.SegmentID = Convert.ToInt32(dataReader["SEGMENT_ID"]);
                    segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString().Trim();
                    segmentDetail.segmentPath = dataReader["SEGMENT_PATH"].ToString();
                    segmentDetailList.Add(segmentDetail);
                }
                if (!Utility.IsNullOrEmpty(segmentDetailList))
                    positionProfileDetail.Segments = segmentDetailList;

                dataReader.NextResult();
                PositionProfileKeyword positionProfileKeywords = null;
                if (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(positionProfileKeywords))
                        positionProfileKeywords = new PositionProfileKeyword();
                    positionProfileKeywords.SegmentKeyword = dataReader["SEGMENT_KEYWORD"].ToString();
                    positionProfileKeywords.UserKeyword = dataReader["USER_KEYWORD"].ToString();
                }

                if (!Utility.IsNullOrEmpty(positionProfileKeywords))
                    positionProfileDetail.PositionProfileKeywords = positionProfileKeywords;

                dataReader.NextResult();
                List<int> sellectedDepartment = null;
                List<string> sellectedContact = null;

                if (positionProfileDetail.ClientInformation == null)
                    positionProfileDetail.ClientInformation = new ClientInformation();

                if (positionProfileDetail.ClientInformation.ClientContactInformations == null)
                    positionProfileDetail.ClientInformation.ClientContactInformations = new List<ClientContactInformation>();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["DEPARTMENT_ID"]))
                    {
                        if (Utility.IsNullOrEmpty(sellectedDepartment))
                            sellectedDepartment = new List<int>();
                        sellectedDepartment.Add(int.Parse(dataReader["DEPARTMENT_ID"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CONTACT_IDS"]))
                    {
                        if (Utility.IsNullOrEmpty(sellectedContact))
                            sellectedContact = new List<string>();
                        sellectedContact.Add(dataReader["CONTACT_IDS"].ToString());
                    }

                    ClientContactInformation contactInformation = new ClientContactInformation();

                    if (!Utility.IsNullOrEmpty(dataReader["CONTACT_ID"]))
                        contactInformation.ContactID = Convert.ToInt32(dataReader["CONTACT_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        contactInformation.FirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MIDDLE_NAME"]))
                        contactInformation.MiddleName = dataReader["MIDDLE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        contactInformation.LastName = dataReader["LAST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        contactInformation.EmailAddress = dataReader["EMAIL_ID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["PHONE_NUMBER"]))
                        contactInformation.PhoneNumber = dataReader["PHONE_NUMBER"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MOBILE_NUMBER"]))
                        contactInformation.MobileNumber = dataReader["MOBILE_NUMBER"].ToString();

                    // Add to the collection.
                    positionProfileDetail.ClientInformation.ClientContactInformations.Add(contactInformation);
                }

                positionProfileDetail.DepartmentIds = sellectedDepartment;
                positionProfileDetail.ContactIds = sellectedContact;

                dataReader.Close();

                return positionProfileDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retreives the candidate detail.
        /// </summary>
        /// <param name="candidateUserID">
        /// A <see cref="int"/> that holds the candidate login ID.
        /// </param> 
        /// <returns>
        /// A <see cref="CandidateDetail"/> that holds the candidate detail.
        /// </returns>
        public CandidateDetail GetCandidate(int candidateLoginID)
        {
            IDataReader dataReader = null;

            try
            {
                // Construct command.
                DbCommand getStructure = HCMDatabase.GetStoredProcCommand("SPGET_EXTERNAL_DATA_CANDIDATE");

                // Add parameters.
                HCMDatabase.AddInParameter(getStructure, "@CANDIDATE_USER_ID", DbType.Int32, candidateLoginID);

                // Execute the result set.
                dataReader = HCMDatabase.ExecuteReader(getStructure);

                CandidateDetail candidateDetail = null;

                if (dataReader.Read())
                {
                    candidateDetail = new CandidateDetail();

                    candidateDetail.CandidateLoginID = candidateLoginID;

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_INFO_ID"]))
                        candidateDetail.CandidateInfoID = Convert.ToInt32(dataReader["CANDIDATE_INFO_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        candidateDetail.FirstName = dataReader["FIRST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["MIDDLE_NAME"]))
                        candidateDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        candidateDetail.LastName = dataReader["LAST_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        candidateDetail.EMailID = dataReader["EMAIL_ID"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["PHONE_NUMBER"]))
                        candidateDetail.Phone = dataReader["PHONE_NUMBER"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["STREET_ADDRESS"]))
                        candidateDetail.Address = dataReader["STREET_ADDRESS"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["CITY_NAME"]))
                        candidateDetail.City = dataReader["CITY_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["STATE_NAME"]))
                        candidateDetail.State = dataReader["STATE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["ZIPCODE"]))
                        candidateDetail.ZipCode = dataReader["ZIPCODE"].ToString();

                    if (!Utility.IsNullOrEmpty(dataReader["RESUME_TEXT"]))
                        candidateDetail.ResumeText = dataReader["RESUME_TEXT"].ToString();
                }
                return candidateDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }
    }
}