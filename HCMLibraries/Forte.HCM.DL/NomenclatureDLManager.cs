﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// NomenclatureDLManager.cs
// File that represents the data layer for the nomenclature customization
// feature. This will talk to the database to perform the operations associated
// with this feature.

#endregion Header

#region Directives                                                             

using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.Common.DL
{
    /// <summary>
    /// Class that represents the data layer for the nomenclature customization
    /// feature. This will talk to the database to perform the operations 
    /// associated with this feature
    /// </summary>
    public class NomenClatureDLManager : DatabaseConnectionManager
    {
        #region Public Methods                                                 

        /// <summary>
        /// Method that inserts the nomenclature details.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="data">
        /// A <see cref="string"/> that holds the data as XML.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        public void InsertNomenclatureDetail(int tenantID, string data, int userID)
        {
            // Create a stored procedure command object.
            DbCommand insertNomenClature = HCMDatabase.
                GetStoredProcCommand("SPINSERT_NOMENCLATURE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertNomenClature,
                "@TENANT_ID", DbType.Int32, tenantID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertNomenClature,
                "@NOMENCLATURE_NAME", DbType.Xml, data.ToString());

            // Add input parameters.
            HCMDatabase.AddInParameter(insertNomenClature,
                "@CREATED_BY", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertNomenClature);
        }

        /// <summary>
        /// Method that retrieves the nomenclature customization attributes for
        /// the given tenant ID.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="NomenclatureCustomize"/> that holds the
        /// nomenclature customization attributes.
        /// </returns>
        public List<NomenclatureCustomize> GetAttributes(int tenantID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getAttributes = HCMDatabase.GetStoredProcCommand("SPGET_NOMENCLATURE_ATTRIBUTES");

                HCMDatabase.AddInParameter(getAttributes, "@TENANT_ID", 
                    DbType.Int16,tenantID);

                List<NomenclatureCustomize> attributes = null;

                dataReader = HCMDatabase.ExecuteReader(getAttributes);

                while (dataReader.Read())
                {
                    // Instantiate the attributes list.
                    if (attributes == null)
                        attributes = new List<NomenclatureCustomize>();

                    // Instantiate the attribute object.
                    NomenclatureCustomize attribute = new NomenclatureCustomize();

                    if (!Utility.IsNullOrEmpty(dataReader["NOMENCLATURE_NAME"]))
                    {
                        attribute.DataSource = dataReader["NOMENCLATURE_NAME"].ToString();
                    }

                    // Add to attributes list.
                    attributes.Add(attribute);
                }
                return attributes;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retrieves the nomenclature customization role 
        /// attributes for the given tenant ID.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A list of <see cref="NomenclatureCustomize"/> that holds the
        /// nomenclature customization role attributes.
        /// </returns>
        public List<NomenclatureCustomize> GetRoleAttributes(int tenantID)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getAttributes = HCMDatabase.GetStoredProcCommand("SPGET_NOMENCLATURE_ROLES_ATTRIBUTES");

                HCMDatabase.AddInParameter(getAttributes, "@TENANT_ID",
                    DbType.Int16, tenantID);

                List<NomenclatureCustomize> attributes = null;

                dataReader = HCMDatabase.ExecuteReader(getAttributes);

                while (dataReader.Read())
                {
                    // Instantiate the attributes list.
                    if (attributes == null)
                        attributes = new List<NomenclatureCustomize>();

                    // Instantiate the attribute object.
                    NomenclatureCustomize attribute = new NomenclatureCustomize();

                    if (!Utility.IsNullOrEmpty(dataReader["ROLE_ID"]))
                    {
                        attribute.RoleID = dataReader["ROLE_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLE_ALIAS_NAME"]))
                    {
                        attribute.RoleAliasName = dataReader["ROLE_ALIAS_NAME"].ToString();
                    }

                    // Add to attributes list.
                    attributes.Add(attribute);
                }

                return attributes;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that inserts the role attributes.
        /// </summary>
        /// <param name="roleArttributes">
        /// A list of <see cref="NomenclatureCustomize"/> that holds the role
        /// attributes.
        /// </param>
        public void InsertRoleAttributes(List<NomenclatureCustomize> roleArttributes)
        {
            for (int i = 0; i < roleArttributes.Count; i++)
            {
                DbCommand insertRoleAttributeCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_NOMENCLATURE_ROLES");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleAttributeCommand,
                    "@ROLE_ID", DbType.String, roleArttributes[i].RoleID);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleAttributeCommand,
                    "@ROLE_NAME", DbType.String, roleArttributes[i].RoleName);

                HCMDatabase.AddInParameter(insertRoleAttributeCommand,
                   "@ROLE_ALIAS_NAME", DbType.String, roleArttributes[i].RoleAliasName);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleAttributeCommand,
                    "@CREATED_BY", DbType.Int16, roleArttributes[i].UserID);

               
                HCMDatabase.AddInParameter(insertRoleAttributeCommand,
                    "@TENANT_ID", DbType.Int32, roleArttributes[i].TenantID);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(insertRoleAttributeCommand);
            }
        }

        #endregion Public Methods
    }
}
