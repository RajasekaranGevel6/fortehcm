﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewReportDLManager.cs
// File that represents the data layer for the interview report module.
// This will talk to the database to perform the operations associated
// with the module.
#endregion

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the interview report module.
    /// This includes functionalities to display various reports against the 
    /// interviews. This will talk to the database for performing these 
    /// operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class InterviewReportDLManager : DatabaseConnectionManager
    {
        #region Public Methods

        /// <summary>
        /// this method represent the Test details based on search criteria.
        /// </summary>
        /// <param name="testSearchCriteria">
        /// A<see cref="string"/>that holds the test Search Criteria
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="string"/>that holds the page size
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="string"/>that holds the page number
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the order By
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/>that holds the order By Direction Asc/desc
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total Records out parameter.
        /// </param>
        /// <returns>
        /// A list for <see cref="TestDetail"/> that holds the test detail
        /// </returns>
        public List<TestDetail> GetInterviewTests(TestSearchCriteria testSearchCriteria,
            int pageSize, int pageNumber, string orderBy,
            SortType orderByDirection, out int totalRecords)
        {
            IDataReader dataReader = null;
            totalRecords = 0;

            try
            {
                string isCertification = null;
                if (testSearchCriteria.IsCertification != null)
                    isCertification = testSearchCriteria.IsCertification == true ? "Y" : "N";

                List<TestDetail> testDetails = null;

                DbCommand getTestDetailCommand = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_REPORT");

                HCMDatabase.AddInParameter(getTestDetailCommand, "@INTERVIEW_TEST_KEY", DbType.String, testSearchCriteria.TestKey);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@INTERVIEW_TEST_AUTHOR", DbType.String, testSearchCriteria.TestAuthorName);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@INTERVIEW_TEST_AUTHOR_ID", DbType.String, testSearchCriteria.TestAuthorID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CAT_SUB", DbType.String, testSearchCriteria.CategoriesID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CATEGORY", DbType.String, testSearchCriteria.Category);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@SUBJECT", DbType.String, testSearchCriteria.Subject);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@KEYWORD", DbType.String, testSearchCriteria.Keyword);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@INTERVIEW_TEST_AREA", DbType.String, testSearchCriteria.TestAreasID);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@INTERVIEW_TEST_NAME", DbType.String, testSearchCriteria.Name);

                if (testSearchCriteria.PositionProfileID == 0)
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getTestDetailCommand, "@POSITION_PROFILE_ID", DbType.Int32, testSearchCriteria.PositionProfileID);

                HCMDatabase.AddInParameter(getTestDetailCommand, "@COMPLEXITY", DbType.String, testSearchCriteria.Complexity);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGENUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@CERTIFICATION ", DbType.String, (isCertification));
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBY", DbType.String, orderBy);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@ORDERBYDIRECTION", DbType.String, orderByDirection == SortType.Ascending ? Constants.SortTypeConstants.ASCENDING : Constants.SortTypeConstants.DESCENDING);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@INTERVIEW_TEST_COST_START", DbType.Int32, testSearchCriteria.TestCostStart);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@INTERVIEW_TEST_COST_END", DbType.Int32, (testSearchCriteria.TestCostEnd == 0) ? 100 : testSearchCriteria.TestCostEnd);
                HCMDatabase.AddInParameter(getTestDetailCommand, "@SUFFICIENT_CREDIT", DbType.String, testSearchCriteria.isSufficientCredits);

                dataReader = HCMDatabase.ExecuteReader(getTestDetailCommand);

                while (dataReader.Read())
                {
                    if (testDetails == null)
                        testDetails = new List<TestDetail>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        TestDetail testDetail = new TestDetail();
                        testDetail.TestKey = dataReader["INTERVIEW_TEST_KEY"].ToString();
                        testDetail.Name = dataReader["INTERVIEW_TEST_NAME"].ToString();
                        testDetail.TestAuthorName = dataReader["USERNAME"].ToString();
                        testDetail.TestAuthorFullName = dataReader["TEST_AUTHOR_FULLNAME"].ToString().Trim();
                        testDetail.NoOfQuestions = int.Parse(dataReader["TOTAL_QUESTION"].ToString());
                        testDetail.CreatedDate = DateTime.Parse(dataReader["CREATED_DATE"].ToString());
                        testDetail.TestCost = decimal.Parse(dataReader["INTERVIEW_TEST_COST"].ToString());
                        testDetail.IsCertification = (Convert.ToChar(dataReader["CERTIFICATION"]) == 'Y') ? true : false; ;
                        testDetails.Add(testDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                dataReader.Close();
                return testDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<QuestionDetail> GetInterviewCandidateTestQuestionDetail(string testKey, string candidateSessionKey, int attemptID)
        {
            IDataReader dataReader = null;
            List<QuestionDetail> questionDetails = new List<QuestionDetail>();

            try
            {
                // Create command object
                DbCommand getInterviewCandidateTestQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_CANDIDATE_TEST_RESULT");

                // Add input parameter
                HCMDatabase.AddInParameter(getInterviewCandidateTestQuestionDetailCommand, "@INTERVIEW_TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getInterviewCandidateTestQuestionDetailCommand, "@INTERVIEW_CANDIDATE_SESSION_KEY", DbType.String, candidateSessionKey);
                HCMDatabase.AddInParameter(getInterviewCandidateTestQuestionDetailCommand, "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getInterviewCandidateTestQuestionDetailCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        QuestionDetail questionDetail = new QuestionDetail();

                        questionDetail.QuestionKey = dataReader["QUESTION_KEY"].ToString().Trim();
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                        questionDetail.Complexity = dataReader["COMPLEXITY"].ToString().Trim();
                        questionDetail.TestAreaName = dataReader["TEST_AREA_NAME"].ToString().Trim();
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                        questionDetail.QuestionID = Convert.ToInt32(dataReader["QUESTION_ID"].ToString().Trim());

                        if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                            questionDetail.TimeTaken = Convert.ToInt32(dataReader["TIME_TAKEN"]);

                        questionDetail.Skipped = dataReader["SKIPPED"].ToString() == "N" ? "No" : "Yes";
                        if (!Utility.IsNullOrEmpty(dataReader["CHOICE_DESC"]))
                        {
                            questionDetail.Choice_Desc = dataReader["CHOICE_DESC"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ILLUSTRATION"]))
                            questionDetail.QuestionImage = dataReader["ILLUSTRATION"] as byte[];

                        if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]) && dataReader["WEIGHTAGE"] != DBNull.Value)
                        {
                            questionDetail.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]) && dataReader["COMMENTS"] != DBNull.Value)
                        {
                            questionDetail.Comments = dataReader["COMMENTS"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["USER_COMMENTS"]) && dataReader["USER_COMMENTS"] != DBNull.Value)
                        {
                            questionDetail.UserComments = dataReader["USER_COMMENTS"].ToString().Trim();
                        }

                        questionDetails.Add(questionDetail);
                    }
                    dataReader.Close();
                }
                return questionDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public CandidateStatisticsDetail GetInterviewCandidateTestResultStatisticsDetails(string candidateSessionKey, string testKey,
            int attemptID, string sessionKey, int assessorID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCandidateStatisticsDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_CANDIDATE_TEST_RESULT_STATISTICS");

                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@INTERVIEW_CANDIDATE_SESSION_ID", DbType.String, candidateSessionKey);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@INTERVIEW_TEST_KEY", DbType.String, testKey);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                  "@INTERVIEW_SESSION_ID", DbType.String, sessionKey);
                HCMDatabase.AddInParameter(getCandidateStatisticsDetailsCommand,
                   "@ASSESSOR_ID", DbType.Int32, assessorID);

                dataReader = HCMDatabase.ExecuteReader(getCandidateStatisticsDetailsCommand);

                CandidateStatisticsDetail candidateDetails = new CandidateStatisticsDetail();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        candidateDetails.TotalQuestion = int.Parse
                            (dataReader["TOTAL_QUESTION"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                    {
                        candidateDetails.TimeTaken = Utility.ConvertSecondsToHoursMinutesSeconds
                            (int.Parse(dataReader["TIME_TAKEN"].ToString()));
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        candidateDetails.AverageTimeTaken =
                           dataReader["AVERAGE_TIME_TAKEN"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_STATUS"]))
                    {
                        candidateDetails.TestStatus =
                            dataReader["TEST_STATUS"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                    {
                        candidateDetails.TestName =
                            dataReader["INTERVIEW_TEST_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        candidateDetails.CandidateFullName =
                            dataReader["CANDIDATE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATEID"]))
                    {
                        candidateDetails.CandidateID =
                            dataReader["CANDIDATEID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_ID"]))
                        candidateDetails.ClientID = Convert.ToInt32(dataReader["CLIENT_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                        candidateDetails.ClientName = dataReader["CLIENT_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_ID"]))
                        candidateDetails.PositionProfileID = Convert.ToInt32(dataReader["POSITION_PROFILE_ID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        candidateDetails.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR"]))
                    {
                        candidateDetails.Assesor =
                            dataReader["ASSESSOR"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_SKILLS"]))
                    {
                        candidateDetails.AssessorSkill =
                            dataReader["ASSESSOR_SKILLS"].ToString();
                    }
                }
                dataReader.NextResult();
               if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["SKIPPED"]))
                    {
                        candidateDetails.TotalQuestionSkipped =
                            int.Parse(dataReader["SKIPPED"].ToString());
                    }
                }

               dataReader.NextResult();
               if (dataReader.Read())
               {
                   if (!Utility.IsNullOrEmpty(dataReader["ATTENDED"]))
                   {
                       candidateDetails.TotalQuestionAttended =
                           int.Parse(dataReader["ATTENDED"].ToString());
                   }
               }
                //Calculate the total question attended by finding difference 
                //between total questions and skipped
                //candidateDetails.TotalQuestionAttended =
                //    candidateDetails.QuestionsAnsweredCorrectly + candidateDetails.QuestionsAnsweredWrongly + candidateDetails.TotalQuestionSkipped;
                //candidateDetails.TotalQuestionSkipped = candidateDetails.TotalQuestion - candidateDetails.TotalQuestionAttended;

               //candidateDetails.TotalQuestionAttended = candidateDetails.TotalQuestion - candidateDetails.TotalQuestionSkipped;
                return candidateDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the interview response detail for the given 
        /// question.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <param name="testQuestionID">
        /// A <see cref="int"/> that holds the test question ID.
        /// </param>
        /// <returns>
        /// A <see cref="InterviewResponseDetail"/> that holds the response 
        /// detail.
        /// </returns>
        public InterviewResponseDetail GetInterviewResponseDetail
            (string candidateSessionID, int attemptID, int testQuestionID,int assessorID)
        {
            // Create a response detail object.
            InterviewResponseDetail responseDetail = null;

            IDataReader dataReader = null;

            try
            {
                DbCommand getImageCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_QUESTION_RESPONSE_DETAIL");

                HCMDatabase.AddInParameter(getImageCommand,
                    "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getImageCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                HCMDatabase.AddInParameter(getImageCommand,
                    "@TEST_QUESTION_ID", DbType.Int32, testQuestionID);

                HCMDatabase.AddInParameter(getImageCommand,
                   "@ASSESSOR_ID", DbType.Int32, assessorID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getImageCommand);

                // Instantiate the response detail object.
                if (responseDetail == null)
                    responseDetail = new InterviewResponseDetail();
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_KEY"]) && 
                        dataReader["QUESTION_KEY"] != DBNull.Value)
                    {
                        responseDetail.QuestionKey = dataReader
                            ["QUESTION_KEY"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]) &&
                        dataReader["QUESTION_DESC"] != DBNull.Value)
                    {
                        responseDetail.Question = dataReader
                            ["QUESTION_DESC"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ILLUSTRATION"]))
                    {
                        responseDetail.QuestionImage = dataReader["ILLUSTRATION"] as byte[];
                        responseDetail.HasImage = true;
                    }
                    else
                        responseDetail.HasImage = false;

                    if (!Utility.IsNullOrEmpty(dataReader["CHOICE_DESC"]) &&
                      dataReader["CHOICE_DESC"] != DBNull.Value)
                    {
                        responseDetail.ChoiceDesc = dataReader
                            ["CHOICE_DESC"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]) &&
                      dataReader["WEIGHTAGE"] != DBNull.Value)
                    {
                        responseDetail.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]) &&
                        dataReader["SUBJECT_NAME"] != DBNull.Value)
                    {
                        responseDetail.SubjectName = dataReader
                            ["SUBJECT_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]) &&
                        dataReader["CATEGORY_NAME"] != DBNull.Value)
                    {
                        responseDetail.CategoryName = dataReader
                            ["CATEGORY_NAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]) &&
                        dataReader["COMMENTS"] != DBNull.Value)
                    {
                        responseDetail.CandidateComments = dataReader
                            ["COMMENTS"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_DESC"]) &&
                        dataReader["COMPLEXITY_DESC"] != DBNull.Value)
                    {
                        responseDetail.Complexity = dataReader
                            ["COMPLEXITY_DESC"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AREA_DESC"]) &&
                        dataReader["TEST_AREA_DESC"] != DBNull.Value)
                    {
                        responseDetail.TestArea = dataReader
                            ["TEST_AREA_DESC"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STATUS_DESC"]) &&
                        dataReader["STATUS_DESC"] != DBNull.Value)
                    {
                        responseDetail.Status = dataReader
                            ["STATUS_DESC"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]) &&
                        dataReader["TIME_TAKEN"] != DBNull.Value)
                    {
                        responseDetail.ResponseTime = Convert.ToDecimal(dataReader
                            ["TIME_TAKEN"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["START_TIME"]) &&
                        dataReader["START_TIME"] != DBNull.Value)
                    {
                        responseDetail.StartTime = Convert.ToDateTime(dataReader
                            ["START_TIME"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["END_TIME"]) &&
                        dataReader["END_TIME"] != DBNull.Value)
                    {
                        responseDetail.StartTime = Convert.ToDateTime(dataReader
                            ["END_TIME"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKIPPED"]) &&
                        dataReader["SKIPPED"] != DBNull.Value)
                    {
                        responseDetail.Skipped = dataReader["SKIPPED"].ToString().
                            Trim().ToUpper() == "Y" ? true : false;
                    }
                }

                dataReader.NextResult();
                List<InterviewResponseDetail> response = null;
                while (dataReader.Read())
                {

                    InterviewResponseDetail responseDetails = new InterviewResponseDetail();
                     if (response==null)
                     response = new List<InterviewResponseDetail>();

                     if (!Utility.IsNullOrEmpty(dataReader["RATING"]) &&
                        dataReader["RATING"] != DBNull.Value)
                    {
                        responseDetails.Rating = Convert.ToInt32(dataReader["RATING"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]) &&
                        dataReader["COMMENTS"] != DBNull.Value)
                    {
                        responseDetails.userComments = dataReader["COMMENTS"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_ID"]) &&
                       dataReader["ASSESSOR_ID"] != DBNull.Value)
                    {
                        responseDetails.assessorID = Convert.ToInt32(dataReader["ASSESSOR_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR_NAME"]) &&
                       dataReader["ASSESSOR_NAME"] != DBNull.Value)
                    {
                        responseDetails.assessorName = dataReader["ASSESSOR_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CAND_INTERVIEW_SESSION_KEY"]) &&
                       dataReader["CAND_INTERVIEW_SESSION_KEY"] != DBNull.Value)
                    {
                        responseDetails.CandidateSessionId = dataReader["CAND_INTERVIEW_SESSION_KEY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["RATING_PERCENT"]) &&
                       dataReader["RATING_PERCENT"] != DBNull.Value)
                    {
                        responseDetails.RatingPercent = Convert.ToDecimal(dataReader["RATING_PERCENT"].ToString());
                    }
                    if(responseDetails.CandidateSessionId==candidateSessionID)
                        response.Add(responseDetails);
                }

                responseDetail.RatingComments = new List<InterviewResponseDetail>();
                responseDetail.RatingComments = response;

                return responseDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        public void SaveQuestionDetailsWithRatingComments(QuestionDetail questionDetails, string candidateSessionKey,
            int attemptID, int assessorID, int userID,IDbTransaction transaction)
        {
            // Create command object 
            DbCommand insertquestionRatingCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERVIEWCANDIDATE_ASSESSMENT_SCORE");

            // Add input parameters
            HCMDatabase.AddInParameter(insertquestionRatingCommand,
                "@QUESTION_KEY", DbType.String, questionDetails.QuestionKey);

            HCMDatabase.AddInParameter(insertquestionRatingCommand,
                "@CAND_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);

            HCMDatabase.AddInParameter(insertquestionRatingCommand,
                "@ATTEMPT_ID", DbType.Int32, attemptID);

            HCMDatabase.AddInParameter(insertquestionRatingCommand,
                "@ASSESSOR_ID", DbType.Int32, assessorID);

            HCMDatabase.AddInParameter(insertquestionRatingCommand,
                "@RATING", DbType.Int32, questionDetails.Rating);

            HCMDatabase.AddInParameter(insertquestionRatingCommand,
                "@COMMENTS", DbType.String, questionDetails.Comments);

            HCMDatabase.AddInParameter(insertquestionRatingCommand,
                "@USER_ID", DbType.String, userID);

            // Execute the procedure
            HCMDatabase.ExecuteNonQuery(insertquestionRatingCommand, transaction as DbTransaction);
        }

        public List<QuestionDetail> GetAssessorSummary(string candidateInterviewKey, int attemptID, int assessorID)
        {
            List<QuestionDetail> questiondetails = new List<QuestionDetail>();

            return questiondetails;
        }

        /// <summary>
        /// Represents a method to get a name of the interview test
        /// when interview test id is passed
        /// </summary>
        /// <param name="interviewTestID">
        /// A<see cref="string"/>that holds the testid
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the interview test name
        /// </returns>
        public string GetInterviewTestName(string interviewTestID)
        {
            IDataReader dataReader = null;
            string interviewTestName = null;
            try
            {
                //Assign the stored procedure
                DbCommand getTestNameCommmand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_TEST_NAME");

                //Add parameter
                HCMDatabase.AddInParameter(getTestNameCommmand,
                    "@INTERVIEW_TEST_KEY", DbType.String, interviewTestID);

                dataReader = HCMDatabase.ExecuteReader(getTestNameCommmand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["Interview_Test_Name"]))
                    {
                        //Get the test name from DB
                        interviewTestName = dataReader["Interview_Test_Name"].ToString();
                    }
                }
                //Return the testname
                return interviewTestName;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the interview question statistics details for the question.
        /// In second tab of the interview test statistics info page
        /// </summary>
        /// <param name="interviewtestId">
        /// A<see cref="int"/> that holds the interview test id 
        /// </param>
        /// <returns>
        /// A list for <see cref="QuestionStatisticsDetail"/>that holds the 
        /// question statistics details 
        /// </returns>
        public List<QuestionStatisticsDetail> GetInterviewQuestionStatistics(string interviewTestId)
        {
            DbCommand getTestQuestionCategoryCountCommand =
            HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_QUESTION_STATISTICS");
            IDataReader dataReader = null;
            List<QuestionStatisticsDetail> questionStatisticsDetails = new List<QuestionStatisticsDetail>();
            try
            {
                HCMDatabase.AddInParameter(getTestQuestionCategoryCountCommand,
                    "@INTERVIEW_TEST_KEY", DbType.String, interviewTestId);

                dataReader = HCMDatabase.ExecuteReader(getTestQuestionCategoryCountCommand);
                QuestionStatisticsDetail questionStatisticsDetail;
                while (dataReader.Read())
                {
                    questionStatisticsDetail = new QuestionStatisticsDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_QUESTION_KEY"].ToString()))
                    {
                        questionStatisticsDetail.QuestionKey =
                        dataReader["INTERVIEW_QUESTION_KEY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["GLOBAL_TIME"]))
                    {
                        questionStatisticsDetail.AverageTimeTakenAcrossTest =
                          decimal.Parse(dataReader["GLOBAL_TIME"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECTLY"]))
                    {
                        questionStatisticsDetail.NoOfTimesAnsweredCorrectly =
                            int.Parse(dataReader["ANSWERED_CORRECTLY"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTEMPTED"]))
                    {
                        questionStatisticsDetail.NoOfTimesAdministered =
                             int.Parse(dataReader["ATTEMPTED"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["LOCAL_TIME"]))
                    {
                        questionStatisticsDetail.AverageTimeTakenWithinTest
                            = decimal.Parse(dataReader["LOCAL_TIME"].ToString());
                    }
                    questionStatisticsDetail.Ratio =
                    string.Concat(questionStatisticsDetail.
                    NoOfTimesAnsweredCorrectly.ToString(), ":",
                    questionStatisticsDetail.NoOfTimesAdministered.ToString());
                    questionStatisticsDetails.Add(questionStatisticsDetail);
                }
                return questionStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the test statistics details for a single interview test.
        /// For the first tab in the interview test statistics info page
        /// </summary>
        /// <param name="interviewTestID">
        /// A<see cref="string"/>that holds the interview testid
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the 
        /// interview test statistics inforamtions
        /// </returns>
        public TestStatistics GetInterviewTestStatisticsDetails(string interviewTestId)
        {
            TestStatistics testStatistics = new TestStatistics();

            IDataReader dataReader = null;

            try
            {
                DbCommand getTestStatisticsDetail = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_TEST_STATISTICS_INFORMATION");

                HCMDatabase.AddInParameter(getTestStatisticsDetail,
                    "@INTERVIEW_TEST_KEY", DbType.String, interviewTestId);

                dataReader = HCMDatabase.ExecuteReader(getTestStatisticsDetail);

                //To get the oveerall deatils of the test such as 
                //interview test name, author name, total question, maximum score,
                //minimum score
                if (dataReader.Read())
                {
                    testStatistics.TestID = interviewTestId;

                    if (!Utility.IsNullOrEmpty(dataReader["INTERVIEW_TEST_NAME"]))
                    {
                        testStatistics.TestName = dataReader["INTERVIEW_TEST_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                    {
                        testStatistics.TestAuthor = dataReader["USRFIRSTNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_AUTHOR_FULLNAME"]))
                    {
                        testStatistics.TestAuthorFullName =
                            dataReader["TEST_AUTHOR_FULLNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        testStatistics.NoOfQuestions = int.Parse(dataReader["TOTAL_QUESTION"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        testStatistics.AverageTimeTakenByCandidates = Convert.ToDecimal
                       (dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        testStatistics.HighestScore = Convert.ToDecimal(dataReader["MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        testStatistics.LowestScore = Convert.ToDecimal(dataReader["MINIMUM_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MEAN_SCORE"]))
                    {
                        testStatistics.MeanScore = Convert.ToDecimal(dataReader["MEAN_SCORE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["STANDARD_DEVIATION"]))
                    {
                        testStatistics.StandardDeviation = Convert.ToDecimal(dataReader["STANDARD_DEVIATION"].ToString().Trim());
                    }
                }

                dataReader.NextResult();

                if (dataReader.Read())
                {
                    testStatistics.NoOfCandidates = int.Parse(dataReader["CANDIDATE_COUNT"].ToString());

                }

                //Read next result to take the subject and category statistics
                dataReader.NextResult();

                testStatistics.SubjectStatistics = new List<Subject>();

                while (dataReader.Read())
                {
                    Subject subject = new Subject();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        subject.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        subject.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }

                    testStatistics.SubjectStatistics.Add(subject);
                }

                dataReader.NextResult();

                testStatistics.CategoryStatisticsChartData = new SingleChartData();

                testStatistics.CategoryStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the category statistics
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["CATEGORY_COUNT"].ToString().Trim());
                    }

                    testStatistics.CategoryStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.SubjectStatisticsChartData = new SingleChartData();

                testStatistics.SubjectStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the subject statistics
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["SUBJECT_COUNT"].ToString().Trim());
                    }

                    testStatistics.SubjectStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.TestAreaStatisticsChartData = new SingleChartData();

                testStatistics.TestAreaStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the test area count
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TESTAREA_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["TESTAREA_COUNT"].ToString().Trim());
                    }

                    testStatistics.TestAreaStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.NextResult();

                testStatistics.ComplexityStatisticsChartData = new SingleChartData();

                testStatistics.ComplexityStatisticsChartData.ChartData = new List<ChartData>();

                //Read next result to take the complexity count
                while (dataReader.Read())
                {
                    ChartData chartData = new ChartData();

                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        chartData.ChartXValue = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_COUNT"]))
                    {
                        chartData.ChartYValue = int.Parse(dataReader["COMPLEXITY_COUNT"].ToString().Trim());
                    }

                    testStatistics.ComplexityStatisticsChartData.ChartData.Add(chartData);
                }

                dataReader.Close();

                return testStatistics;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the Interview Question Detail based on the interview test id and interview questionId.
        /// </summary>
        /// <param name="interviewTestId">
        /// A<see cref="int"/> that holds the interview test id
        /// </param>
        /// <param name="questionID">
        /// A<see cref="string"/>that holds the interview question Id
        /// </param>
        /// <returns>
        /// A<see cref="QuestionDetail"/>that holds the Question Detail
        /// </returns>
        public QuestionDetail GetInterviewQuestion(string interviewTestId, string questionId)
        {
            IDataReader dataReader = null;
            try
            {
                QuestionDetail questionDetail = new QuestionDetail();

                DbCommand getSingleQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_QUESTION_DETAILS");

                // Add parameters.
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@INTERVIEW_QUESTION_KEY", DbType.String, questionId);

                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@INTERVIEW_TEST_KEY", DbType.String, interviewTestId);

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getSingleQuestionDetailCommand);

                // Retrieve basic question details using SPGET_TEST_QUESTION_DETAILS stored procedure
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                    {
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER"]))
                    {
                        questionDetail.Answer = Convert.ToInt16(dataReader["ANSWER"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        questionDetail.TestAreaName = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY"]))
                    {
                        questionDetail.ComplexityName = dataReader["COMPLEXITY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR"]))
                    {
                        questionDetail.AuthorName = dataReader["AUTHOR"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_FULLNAME"]))
                    {
                        questionDetail.QuestionAuthorFullName = dataReader["AUTHOR_FULLNAME"].ToString().Trim();
                    }

                }
                //Retrieves the answer choices
                dataReader.NextResult();
                questionDetail.AnswerChoices = new List<AnswerChoice>();
                while (dataReader.Read())
                {
                    AnswerChoice answer = new AnswerChoice();
                    if (!Utility.IsNullOrEmpty(dataReader["CHOICE_ID"]))
                    {
                        answer.ChoiceID = int.Parse(dataReader["CHOICE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty("CHOICE_DESC"))
                    {
                        answer.Choice = dataReader["CHOICE_DESC"].ToString();
                    }
                    answer.IsCorrect = answer.ChoiceID == questionDetail.Answer ? true : false;

                    questionDetail.AnswerChoices.Add(answer);
                }

                return questionDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public QuestionDetail GetInterviewQuestion(string testID, string questionID, int attemptID, string candidateSessionKey)
        {
            IDataReader dataReader = null;
            try
            {
                QuestionDetail questionDetail = new QuestionDetail();

                DbCommand getSingleQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_CANDIDATE_QUESTION_DETAILS");

                // Add parameters.
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@INTERVIEW_QUESTION_KEY", DbType.String, questionID);

                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@INTERVIEW_TEST_KEY", DbType.String, testID);
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);
                HCMDatabase.AddInParameter(getSingleQuestionDetailCommand,
                    "@CANDIDATE_INTERVIEW_SESSION_KEY", DbType.String, candidateSessionKey);

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getSingleQuestionDetailCommand);
                int userAnswerChoice = 0;
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["QUESTION_DESC"]))
                    {
                        questionDetail.Question = dataReader["QUESTION_DESC"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ANSWER"]))
                    {
                        questionDetail.Answer = Convert.ToInt16(dataReader["ANSWER"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        questionDetail.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        questionDetail.SubjectName = dataReader["SUBJECT_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                    {
                        questionDetail.TestAreaName = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY"]))
                    {
                        questionDetail.ComplexityName = dataReader["COMPLEXITY"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR"]))
                    {
                        questionDetail.AuthorName = dataReader["AUTHOR"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AUTHOR_FULLNAME"]))
                    {
                        questionDetail.QuestionAuthorFullName = dataReader["AUTHOR_FULLNAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CAND_ANSWER_ID"]))
                    {
                        userAnswerChoice = int.Parse(dataReader["CAND_ANSWER_ID"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ILLUSTRATION"]))
                        questionDetail.QuestionImage = dataReader["ILLUSTRATION"] as byte[];
                    if (!Utility.IsNullOrEmpty(dataReader["WEIGHTAGE"]) && dataReader["WEIGHTAGE"] != DBNull.Value)
                    {
                        questionDetail.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString().Trim());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["COMMENTS"]) && dataReader["COMMENTS"] != DBNull.Value)
                    {
                        questionDetail.Comments = dataReader["COMMENTS"].ToString().Trim();
                    }
                }
                //Retrieves the answer choices
                dataReader.NextResult();
                questionDetail.AnswerChoices = new List<AnswerChoice>();
                while (dataReader.Read())
                {
                    AnswerChoice answer = new AnswerChoice();
                    if (!Utility.IsNullOrEmpty(dataReader["CHOICE_ID"]))
                    {
                        answer.ChoiceID = int.Parse(dataReader["CHOICE_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty("CHOICE_DESC"))
                    {
                        answer.Choice = dataReader["CHOICE_DESC"].ToString();
                    }
                    answer.IsCandidateCorrect = userAnswerChoice;
                    answer.IsCorrect = answer.ChoiceID == questionDetail.Answer ? true : false;

                    questionDetail.AnswerChoices.Add(answer);
                }

                return questionDetail;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the Interview Candidate Statistics Detail based on the search creteria.
        /// </summary>
        /// <param name="testStatisticsSearchCriteria">
        /// A<see cref="TestStatisticsSearchCriteria"/> that holds the test StatisticsSearch Criteria
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/> that holds the order by field 
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="string"/> that holds the order by direction asc/desc
        /// </param>
        /// <param name="pageNumber">
        /// A<see cref="int"/> that holds the page number
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/> that holds the page size
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/> that holds the total records out parameter
        /// </param>
        /// <returns>
        /// A list for<see cref="CandidateStatisticsDetail"/> that holds the candidate statistics detail
        /// </returns>
        public List<CandidateStatisticsDetail> GetInterviewReportCandidateStatisticsDetails
            (TestStatisticsSearchCriteria testStatisticsSearchCriteria, string orderBy, string orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {

            DbCommand command = HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_CANDIDATE_STATISTICS");

            totalRecords = 0;

            IDataReader dataReader = null;
            try
            {
                HCMDatabase.AddInParameter(command, "@INTERVIEW_TEST_KEY", DbType.String,
                    testStatisticsSearchCriteria.TestKey);

                HCMDatabase.AddInParameter(command, "@CANDIDATE_NAME", DbType.String,
                    testStatisticsSearchCriteria.CandidateName.Length == 0 ? null : testStatisticsSearchCriteria.CandidateName);

                HCMDatabase.AddInParameter(command, "@INTERVIEW_TEST_SESSION_CREATOR", DbType.String,
                    testStatisticsSearchCriteria.SessionCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.SessionCreatedBy);

                HCMDatabase.AddInParameter(command, "@INTERVIEW_TEST_SCHEDULE_CREATOR", DbType.String,
                    testStatisticsSearchCriteria.ScheduleCreatedBy.Length == 0 ? null : testStatisticsSearchCriteria.ScheduleCreatedBy);

                HCMDatabase.AddInParameter(command, "@INTERVIEW_TEST_DATE_FROM", DbType.DateTime,
                    testStatisticsSearchCriteria.TestDateFrom.Length == 0 ? Convert.ToDateTime("1/1/1753") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateFrom));

                HCMDatabase.AddInParameter(command, "@INTERVIEW_TEST_DATE_TO", DbType.DateTime,
                    testStatisticsSearchCriteria.TestDateTo.Length == 0 ? Convert.ToDateTime("12/31/9999") : Convert.ToDateTime(testStatisticsSearchCriteria.TestDateTo).AddDays(1));

                //HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_START_VALUE", DbType.Decimal,
                //    testStatisticsSearchCriteria.AbsoluteScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreFrom));

                //HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@ABSOLUTE_SCORE_END_VALUE", DbType.Int32,
                //    testStatisticsSearchCriteria.AbsoluteScoreTo.Length == 0 || testStatisticsSearchCriteria.AbsoluteScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.AbsoluteScoreTo));

                //HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_START_VALUE", DbType.Int32,
                //    testStatisticsSearchCriteria.RelativeScoreFrom.Length == 0 ? 0 : int.Parse(testStatisticsSearchCriteria.RelativeScoreFrom));

                //HCMDatabase.AddInParameter(getReportCandidateStatisticsCommand, "@RELATIVE_SCORE_END_VALUE", DbType.Decimal,
                //    testStatisticsSearchCriteria.RelativeScoreTo.Length == 0 || testStatisticsSearchCriteria.RelativeScoreTo == "0" ? 100 : int.Parse(testStatisticsSearchCriteria.RelativeScoreTo));

                HCMDatabase.AddInParameter(command, "@ORDERBY", DbType.String, orderBy);

                HCMDatabase.AddInParameter(command, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection);

                HCMDatabase.AddInParameter(command, "@PAGENUM", DbType.Int32,
                    pageNumber);

                HCMDatabase.AddInParameter(command, "@PAGESIZE", DbType.Int32,
                    pageSize);

                dataReader = HCMDatabase.ExecuteReader(command);

                List<CandidateStatisticsDetail> candidateStatisticsDetails =
                    new List<CandidateStatisticsDetail>();

                while (dataReader.Read())
                {
                    CandidateStatisticsDetail candidateStatisticsDetail =
                        new CandidateStatisticsDetail();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Candidate Session ID.
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSION_KEY"]))
                        {
                            candidateStatisticsDetail.CandidateSessionID =
                                dataReader["CANDIDATE_SESSION_KEY"].ToString();
                        }

                        //Get the candidate name
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                        {
                            candidateStatisticsDetail.FirstName =
                                dataReader["CANDIDATE_NAME"].ToString();
                        }

                        //Get the candidate full name
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_FULL_NAME"]))
                        {
                            candidateStatisticsDetail.CandidateFullName =
                                dataReader["CANDIDATE_FULL_NAME"].ToString();
                        }

                        //Get the candidate Id
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                        {
                            candidateStatisticsDetail.CandidateID =
                                dataReader["CANDIDATE_ID"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATOR"]))
                        {
                            candidateStatisticsDetail.SessionCreator =
                                dataReader["SESSION_CREATOR"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SESSION_CREATOR_FULLNAME"]))
                        {
                            candidateStatisticsDetail.SessionCreatorFullName =
                                dataReader["SESSION_CREATOR_FULLNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULE_CREATOR"]))
                        {
                            candidateStatisticsDetail.ScheduleCreator =
                                dataReader["SCHEDULE_CREATOR"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SCHEDULE_CREATOR_FULLNAME"]))
                        {
                            candidateStatisticsDetail.ScheduleCreatorFullName =
                                dataReader["SCHEDULE_CREATOR_FULLNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                        {
                            candidateStatisticsDetail.TestDate =
                                dataReader["DATE_COMPLETED"].ToString().Trim().Length == 0 ? DateTime.MinValue :
                                Convert.ToDateTime(dataReader["DATE_COMPLETED"].ToString());

                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ATTEMPT_ID"]))
                        {
                            candidateStatisticsDetail.AttemptNumber =
                                int.Parse(dataReader["CANDIDATE_ATTEMPT_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ANSWERED_CORRECT_PERCENTAGE"]))
                        {
                            candidateStatisticsDetail.AnsweredCorrectly =
                                Convert.ToDecimal(dataReader["ANSWERED_CORRECT_PERCENTAGE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TIME_TAKEN"]))
                        {
                            candidateStatisticsDetail.TimeTaken =
                                dataReader["TIME_TAKEN"].ToString().Trim().Length == 0 ? "00:00:00" :
                                Utility.ConvertSecondsToHoursMinutesSeconds(
                                int.Parse(dataReader["TIME_TAKEN"].ToString()));
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                        {
                            decimal avgTimeTaken = Convert.ToDecimal
                            (dataReader["AVERAGE_TIME_TAKEN"].ToString());

                            int avgTimeTakenInt = Convert.ToInt32
                            (avgTimeTaken);

                            candidateStatisticsDetail.AverageTimeTaken =
                                dataReader["AVERAGE_TIME_TAKEN"].ToString().Trim().Length == 0 ? "0" :
                                Utility.ConvertSecondsToHoursMinutesSeconds(avgTimeTakenInt);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ABSOLUTE_SCORE_ANSWER"]))
                        {
                            candidateStatisticsDetail.AbsoluteScore =
                                dataReader["ABSOLUTE_SCORE_ANSWER"].ToString().Trim().Length == 0 ?
                                0 : Convert.ToDecimal(dataReader["ABSOLUTE_SCORE_ANSWER"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["RELATIVE_SCORE"]))
                        {
                            candidateStatisticsDetail.RelativeScore =
                                dataReader["RELATIVE_SCORE"].ToString().Trim().Length == 0 ?
                                0 : Convert.ToDecimal(dataReader["RELATIVE_SCORE"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["PERCENTILE"]))
                        {
                            candidateStatisticsDetail.Percentile =
                                decimal.Parse(dataReader["PERCENTILE"].ToString());
                        }

                        candidateStatisticsDetail.Recruiter = candidateStatisticsDetail.ScheduleCreator;

                        candidateStatisticsDetails.Add(candidateStatisticsDetail);
                    }
                    else
                    {
                        totalRecords = int.Parse(dataReader["TOTAL"].ToString());
                    }
                }

                return candidateStatisticsDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// This method helps to retrieve the question attributes by question key which is involved in a interview test
        /// </summary>
        /// <param name="interviewTestKey">
        /// A <see cref="string"/>Contains a interview test key which is incorporated with a question attributes
        /// </param>
        /// <param name="questionKey">
        /// A <see cref="string"/>Contains a questionKey key which is incorporated with a question attributes
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/>Contains a sort Expression ase/dec.
        /// </param> 
        /// <returns>
        /// A List for <see cref="QuestionDetail"/>
        /// Returns QuestionDetail object
        /// </returns>
        public QuestionDetail GetInterviewTestQuestionAttributesByKeys(string interviewTestKey,string questionKey)
        {
            IDataReader dataReader = null;
           QuestionDetail questionDetail = new QuestionDetail();

            try
            {
                // Create command object
                DbCommand getTestQuestionDetailCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEW_TEST_QUESTION_ATTRIBUTES_BY_KEYS");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@INTERVIEW_TEST_KEY",
                    DbType.String, interviewTestKey);

                // Add input parameter
                HCMDatabase.AddInParameter(getTestQuestionDetailCommand, "@QUESTION_KEY",
                    DbType.String, questionKey);

                // Execute command
                dataReader = HCMDatabase.ExecuteReader(getTestQuestionDetailCommand);

                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        questionDetail.TimeLimit = Convert.ToInt32(dataReader["TIME_LIMIT"].ToString().Trim());
                        questionDetail.Rating = Convert.ToInt32(dataReader["RATINGS"].ToString().Trim());
                        questionDetail.Weightage = Convert.ToInt32(dataReader["WEIGHTAGE"].ToString().Trim());
                        questionDetail.Comments = dataReader["COMMENTS"].ToString().Trim();
                    }
                    dataReader.Close();
                }
                return questionDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the additional interview test details 
        /// for the particular interview test
        /// </summary>
        /// <param name="interviewTestKey">
        /// A<see cref="string"/>that holds the interview Test Key
        /// </param>
        /// <returns>
        /// A<see cref="TestStatistics"/>that holds the interview test statistics 
        /// information
        /// </returns>
        public TestStatistics GetAdditionalInterviewTestDetails(string interviewTestKey)
        {
            TestStatistics testStatistics = null;
            IDataReader dataReader = null;
            try
            {
                DbCommand getAdditionalTestDetails = HCMDatabase.
                    GetStoredProcCommand("SPGET_ADDITIONAL_INTERVIEW_TEST_STATISTICS");

                HCMDatabase.AddInParameter(getAdditionalTestDetails,
                    "@INTERVIEW_TEST_Key", DbType.String, interviewTestKey);

                dataReader = HCMDatabase.ExecuteReader(getAdditionalTestDetails);

                if (dataReader.Read())
                {
                    testStatistics = new TestStatistics();

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_QUESTION"]))
                    {
                        testStatistics.NoOfQuestions =
                            int.Parse(dataReader["TOTAL_QUESTION"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_TIME_TAKEN"]))
                    {
                        testStatistics.AverageTimeTakenByCandidates =
                            Convert.ToDecimal(dataReader["AVERAGE_TIME_TAKEN"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MINIMUM_SCORE"]))
                    {
                        testStatistics.LowestScore =
                            Convert.ToDecimal(dataReader["MINIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_SCORE"]))
                    {
                        testStatistics.HighestScore =
                            Convert.ToDecimal(dataReader["MAXIMUM_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["AVERAGE_SCORE"]))
                    {
                        testStatistics.AverageScore =
                            Convert.ToDecimal(dataReader["AVERAGE_SCORE"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TEST_COST"].ToString()))
                    {
                        testStatistics.TestCost =
                            Convert.ToDecimal(dataReader["TEST_COST"].ToString());
                    }
                }
                return testStatistics;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public int GetAssessorSkills(string ostSessionKey, string testQuestionID, string assessorID)
        { 
             IDataReader dataReader = null;
             int SkillCount = 0;
             try
             {
                 DbCommand getAdditionalTestDetails = HCMDatabase.
                        GetStoredProcCommand("SPGET_ASSESSORSKILLS");

                 HCMDatabase.AddInParameter(getAdditionalTestDetails,
                     "@ostSessionKey", DbType.String, ostSessionKey);

                 HCMDatabase.AddInParameter(getAdditionalTestDetails,
                     "@testQuestionID", DbType.String, testQuestionID);

                 HCMDatabase.AddInParameter(getAdditionalTestDetails,
                     "@assessorID", DbType.String, assessorID);
                /* HCMDatabase.AddInParameter(getAdditionalTestDetails,
                     "@CandidateSessionKey", DbType.String, candidateSessionKey);*/

                 dataReader = HCMDatabase.ExecuteReader(getAdditionalTestDetails);

                 if (dataReader.Read())
                 {
                     if (!Utility.IsNullOrEmpty(dataReader["COUNT"].ToString()))
                     {
                         SkillCount = Convert.ToInt32(dataReader["COUNT"].ToString());
                     }
                 }
                 return SkillCount;
             }
             catch {
                 return 0;
             }
             finally 
             {
                 // Close the data reader, if it is open.
                 if (dataReader != null && !dataReader.IsClosed)
                 {
                     dataReader.Close();
                 }
             }  
        }
        #endregion Public Methods
    }
}