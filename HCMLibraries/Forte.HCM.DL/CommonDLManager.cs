﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CommonBLManager.cs
// File that represents the business layer for the common operations.
// This includes functionalities for retrieving the category, subjects, 
// contacts, users, user option, etc. This will talk to the database 
// for performing these operations

#endregion

#region Directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;
using System.Text;
using System.Text.RegularExpressions;
#endregion

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the business layer for the common operations.
    /// This includes functionalities for retrieving the category, subjects, 
    /// contacts, users, user option, etc. This will talk to the database 
    /// for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class CommonDLManager : DatabaseConnectionManager
    {
        #region Public Method

        /// <summary>
        /// this method helps to check the record modified or not.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="string"/> that holds entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds entity id.
        /// </param>
        /// <param name="modifiedDate">
        /// A <see cref="DateTime"/> that holds modified date.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds check the record modified or not.
        /// </returns>
        public bool IsRecordModified(string entityType, string entityID,
            DateTime modifiedDate)
        {
            IDataReader reader = null;

            try
            {
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_IS_RECORD_MODIFIED");

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@ENTITY_TYPE", DbType.String, entityType);

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@ENTITY_ID", DbType.String, entityID);

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@MODIFIED_DATE", DbType.DateTime, modifiedDate);

                reader = HCMDatabase.ExecuteReader(getCategoryCommand);

                if (reader.Read())
                {
                    return Convert.ToInt32(reader["COUNT"]) == 0 ? true : false;
                }
                return true;
            }
            finally
            {
                // Close datareader
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// this method helps to check the record modified or not.
        /// </summary>
        /// <param name="entityType">
        /// A <see cref="string"/> that holds entity type.
        /// </param>
        /// <param name="entityID">
        /// A <see cref="string"/> that holds entity id.
        /// </param>
        /// <param name="modifiedDate">
        /// A <see cref="DateTime"/> that holds modified date.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds check the record modified or not.
        /// </returns>
        public bool IsInterviewRecordModified(string entityType, string entityID,
            DateTime modifiedDate)
        {
            IDataReader reader = null;

            try
            {
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_INTERVIEW_IS_RECORD_MODIFIED");

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@ENTITY_TYPE", DbType.String, entityType);

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@ENTITY_ID", DbType.String, entityID);

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@MODIFIED_DATE", DbType.DateTime, modifiedDate);

                reader = HCMDatabase.ExecuteReader(getCategoryCommand);

                if (reader.Read())
                {
                    return Convert.ToInt32(reader["COUNT"]) == 0 ? true : false;
                }
                return true;
            }
            finally
            {
                // Close datareader
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }



        /// <summary>
        /// This method helps to retrieve the disclaimer text from DISCLAIMER table based on the 
        /// Disclaimer Type
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that holds Disclaimer Message.
        /// </returns>
        public string GetDisclaimerMessage(string disclaimerType)
        {
            IDataReader dataReader = null;
            string disclamerText = string.Empty;
            try
            {
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_DISCLAIMER_MESSAGE");

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@DISCLAIMER_TYPE", DbType.String, disclaimerType);

                dataReader = HCMDatabase.ExecuteReader(getCategoryCommand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["DISCLAIMER_TEXT"]))
                        disclamerText = dataReader["DISCLAIMER_TEXT"].ToString().Trim();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return (disclamerText);
        }

        /// <summary>
        /// This method helps to retrieve subjects by passing the category ids
        /// </summary>
        /// <param name="CategoryID">
        /// A <see cref="string"/> that holds the category id.
        /// </param>
        /// <returns>
        /// A List for <see cref="Subject"/> that holds the List of Subjects.
        /// </returns>
        public List<Subject> GetSubjects(int CategoryID)
        {
            List<Subject> Subjects = new List<Subject>();

            IDataReader dataReader = null;

            DbCommand getCategoryCommand = HCMDatabase.
                GetStoredProcCommand("SPGET_SUBJECTS_CAT_IDS");

            HCMDatabase.AddInParameter(getCategoryCommand,
                "@CATEGORY_IDS", DbType.Int32, CategoryID);

            dataReader = HCMDatabase.ExecuteReader(getCategoryCommand);

            while (dataReader.Read())
            {
                Subject subject = new Subject();

                subject.SubjectID = int.Parse
                    (dataReader["CAT_SUB_ID"].ToString());

                subject.SubjectName = dataReader["SUBJECT_NAME"]
                    .ToString().Trim();

                subject.CategoryID = Convert.ToInt32(dataReader["CAT_KEY"]);

                subject.CategoryName = dataReader["CATEGORY_NAME"].ToString().Trim();

                Subjects.Add(subject);
            }

            dataReader.Close();
            return Subjects;
        }

        /// <summary>
        /// Helps to get all subjects against the category ids
        /// </summary>
        /// <param name="categories">
        /// A List for<see cref="Category"/>Holds the category id with comma seperator
        /// </param>
        /// <returns>
        /// A List for <see cref="Subject"/> that holds the List of Subjects.
        /// </returns>
        public List<Subject> GetSubjects(List<Category> categories)
        {
            List<Subject> Subjects = new List<Subject>();
            IDataReader dataReader = null;

            try
            {
                string catIds = string.Empty;

                if (categories != null)
                {
                    //Retrieve all category ids from the List and add comma seperator
                    foreach (Category category in categories)
                    {
                        catIds = catIds + category.CategoryID.ToString() + ",";
                    }

                    //Remove the comma at end of the string
                    if (catIds.EndsWith(","))
                    {
                        catIds = catIds.Substring(0, catIds.Length - 1);
                    }
                }

                // Call the stored procedure which helps to pull SubjectID, Subject Name, 
                // and Category Name from the database and assign to the List
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_SUBJECTS_CAT_IDS");

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@CATEGORY_IDS", DbType.String, catIds);

                dataReader = HCMDatabase.ExecuteReader(getCategoryCommand);

                // Loop through the datareader and get the data from reader and add to the list
                while (dataReader.Read())
                {
                    Subject subject = new Subject();

                    subject.SubjectID = int.Parse
                        (dataReader["CAT_SUB_ID"].ToString().Trim());

                    subject.SubjectName = dataReader["SUBJECT_NAME"]
                        .ToString().Trim();

                    subject.CategoryName = dataReader["CATEGORY_NAME"]
                        .ToString().Trim();

                    Subjects.Add(subject);
                }

                // Close the datareader
                dataReader.Close();

                // Return subjects list
                return Subjects;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                // Close datareader
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// This method helps to retrieve subjects by passing the subject ids
        /// </summary>
        /// <param name="subjectIds">
        /// A <see cref="string"/> that holds the subject id.
        /// </param>
        /// <returns>
        /// A List for <see cref="Subject"/> that holds the List of Subjects.
        /// </returns>
        public List<Subject> GetSubjects(string subjectIds)
        {
            List<Subject> Subjects = new List<Subject>();

            IDataReader dataReader = null;

            DbCommand getCategoryCommand = HCMDatabase.
                GetStoredProcCommand("SPGET_SUBJECTS_BY_SUBJECTIDS");

            HCMDatabase.AddInParameter(getCategoryCommand,
                "@SUBJECT_IDS", DbType.String, subjectIds);

            dataReader = HCMDatabase.ExecuteReader(getCategoryCommand);

            while (dataReader.Read())
            {
                Subject subject = new Subject();

                subject.SubjectID = int.Parse
                    (dataReader["CAT_SUB_ID"].ToString());

                subject.SubjectName = dataReader["SUBJECT_NAME"]
                    .ToString().Trim();

                subject.CategoryName = dataReader["CATEGORY_NAME"]
                    .ToString().Trim();

                Subjects.Add(subject);
            }

            dataReader.Close();
            return Subjects;
        }


        /// <summary>
        /// Get the categories list according to the prefix text. 
        /// </summary>
        /// <param name="categoryPrefix">
        /// A <see cref="string"/> that contains the prefix text.
        /// </param>
        /// <returns>
        /// A list of <see cref="string"/> that contains the category id and name.
        /// </returns>
        public string[] GetCategoryList(string categoryPrefix, int userID)
        {
            IDataReader dataReader = null;

            try
            {
                List<string> categories = null;

                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_AUTOFILL_CATEGORIES");

                HCMDatabase.AddInParameter(getCategoryCommand, "@PREFIX", DbType.String, categoryPrefix);
                HCMDatabase.AddInParameter(getCategoryCommand, "@USERID", DbType.Int32, userID);

                dataReader = HCMDatabase.ExecuteReader(getCategoryCommand);

                while (dataReader.Read())
                {
                    if (categories == null)
                        categories = new List<string>();

                    if (dataReader["CATEGORYNAME"] != null)
                        categories.Add(dataReader["CATEGORYNAME"].ToString().Trim());
                }

                if (categories == null)
                    return null;

                return categories.ToArray();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Gets the user names list according to the prefix text.
        /// </summary>
        /// <param name="UserNamePrefix">
        /// A <see cref="string"/> that contains the prefix text.
        /// </param>
        /// <returns>
        /// A list of <see cref="string"/> that contains user firstname lastname and id.
        /// </returns>
        public string[] GetUserNameList(string UserNamePrefix)
        {
            IDataReader dataReader = null;
            List<string> UserNameList = null;
            DbCommand userNameListCommand = null;
            try
            {
                userNameListCommand = HCMDatabase.GetStoredProcCommand("SPGET_AUTOFILL_USER_LIST");
                HCMDatabase.AddInParameter(userNameListCommand, "@PREFIX_TEXT", DbType.String, UserNamePrefix);
                dataReader = HCMDatabase.ExecuteReader(userNameListCommand);
                if (dataReader.RecordsAffected == 0)
                    return null;
                if (Utility.IsNullOrEmpty(UserNameList))
                    UserNameList = new List<string>();
                while (dataReader.Read())
                    UserNameList.Add(dataReader["USER_NAME"].ToString());
                return UserNameList.ToArray();
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(UserNameList)) UserNameList = null;
                if (!Utility.IsNullOrEmpty(userNameListCommand)) userNameListCommand = null;
            }
        }

        /// <summary>
        /// Method that will check the user firstname, lastname and user id exists
        /// in user table.
        /// </summary>
        /// <param name="UserName">
        /// A <see cref="string"/> that contains the userfirstname and userlastname.
        /// </param>
        /// <param name="UserId">
        /// A <see cref="int"/> that contains the user id
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> represents whether the passed paramters are persent
        /// in database or not.
        /// </returns>
        public bool IsValidUserNameWithId(string UserName, int UserId)
        {
            IDataReader dataReader = null;
            DbCommand IsValidUserCommand = null;
            try
            {
                IsValidUserCommand = HCMDatabase.GetStoredProcCommand("SPGET_CHECK_USER");
                HCMDatabase.AddInParameter(IsValidUserCommand, "@USER_NAME", DbType.String, UserName);
                HCMDatabase.AddInParameter(IsValidUserCommand, "@USER_ID", DbType.Int32, UserId);
                dataReader = HCMDatabase.ExecuteReader(IsValidUserCommand);
                if (dataReader.RecordsAffected == 0)
                    return false;
                if (dataReader.Read())
                {
                    if (dataReader["VALID_USER"].ToString() == "1")
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(IsValidUserCommand)) IsValidUserCommand = null;
            }
        }

        /// <summary>
        /// Method that will check if both category id, and category name exists in the 
        /// category repository.
        /// </summary>
        /// <param name="categoryId">
        /// An <see cref="int"/> that contains the category id.
        /// </param>
        /// <param name="categoryName">
        /// A <see cref="string"/> that contains the category name.
        /// </param>
        /// <returns></returns>
        public bool IsValidCategory(int categoryId, string categoryName)
        {
            try
            {
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CHECK_CATEGORY");

                // Add Input parameter
                HCMDatabase.AddInParameter(getCategoryCommand, "@CATEGORY_ID", DbType.Int32, categoryId);
                HCMDatabase.AddInParameter(getCategoryCommand, "@CATEGORY_NAME", DbType.String, categoryName);

                // Retrieve the count of affected rows
                int rowsAffected = Convert.ToInt32(HCMDatabase.ExecuteScalar(getCategoryCommand));

                return rowsAffected == 1 ? true : false;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// Method that retrieves the list of users for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="roleCodes">
        /// A <see cref="string"/> that holds the role Codes, that indicates the 
        /// list of user roles.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<UserDetail> GetUsers(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string orderBy, SortType orderByDirection, int pageNumber,
            int pageSize, string roleCodes, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_USERS");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@ROLE_CODES", DbType.String, roleCodes);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<UserDetail> GetUserInformation(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string orderBy, SortType orderByDirection, int pageNumber,
            int pageSize, string roleCodes, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_USER_INFORMATION");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_TYPE",
                    DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@ROLE_CODES", DbType.String, roleCodes);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["SUBSCRIPTION_ROLE"] != null)
                        {
                            userDetail.MiddleName = dataReader["SUBSCRIPTION_ROLE"].ToString().Trim();
                        }
                        if (dataReader["SUBSCRIPTION_TYPE"] != null)
                        {
                            userDetail.SubscriptionType = dataReader["SUBSCRIPTION_TYPE"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        if (dataReader["USRUSERTYPENAME"] != null)
                        {
                            userDetail.UserTypeName = dataReader["USRUSERTYPENAME"].ToString().Trim();
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of users for the given search
        /// parameters.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name keyword.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name keyword.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name keyword.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name keyword.
        /// </param>
        /// <param name="userType">
        /// A <see cref="string"/> that holds the user type. 
        /// (A - All, S - Subscription, I - Internal).
        /// </param>
        /// <param name="activatedType">
        /// A <see cref="string"/> that holds the user type.
        /// (B - Both, A - Activated, N - Not activated).
        /// </param>
        /// <param name="orderByColumn">
        /// A <see cref="string"/> that holds the order by column
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that holds the order by direction.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        public DataTable GetUsers(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string userType, string activatedType, string orderByColumn,
            SortType orderByDirection, int pageNumber,
            int pageSize)
        {
            // Assing value to out parameter.
            
            DbCommand getUsersCommand =
                HCMDatabase.GetStoredProcCommand("SPGET_ALLUSERS");
            // Add parameters.
            HCMDatabase.AddInParameter(getUsersCommand,
                "@USER_NAME",
                DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());

            HCMDatabase.AddInParameter(getUsersCommand,
                "@FIRST_NAME",
                DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());

            HCMDatabase.AddInParameter(getUsersCommand,
                "@MIDDLE_NAME",
                DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());

            HCMDatabase.AddInParameter(getUsersCommand,
                "@LAST_NAME",
                DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

            HCMDatabase.AddInParameter(getUsersCommand,
                "@USER_TYPE",
                DbType.String, userType == null || userType.Trim().Length == 0 ? null : userType.Trim());

            HCMDatabase.AddInParameter(getUsersCommand,
               "@ACTIVATED",
               DbType.String, activatedType == null || activatedType.Trim().Length == 0 ? null : activatedType.Trim());

            HCMDatabase.AddInParameter(getUsersCommand,
                "@ORDERBY",
                DbType.String, orderByColumn);

            HCMDatabase.AddInParameter(getUsersCommand,
                "@ORDERBYDIRECTION",
                DbType.String, orderByDirection == SortType.Ascending ?
                Constants.SortTypeConstants.ASCENDING :
                Constants.SortTypeConstants.DESCENDING);

            HCMDatabase.AddInParameter(getUsersCommand,
                "@PAGENUM", DbType.Int32, pageNumber);

            HCMDatabase.AddInParameter(getUsersCommand,
                "@PAGESIZE", DbType.Int32, pageSize);

            DataTable dataTable = HCMDatabase.ExecuteDataSet(getUsersCommand).Tables[0];

            return dataTable;
        }


        /// <summary>
        /// Method that retrieves the list of customers for the given search
        /// parameters.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name keyword.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name keyword.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name keyword.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name keyword.
        /// </param>
        /// <param name="userType">
        /// A <see cref="string"/> that holds the user type. 
        /// (A - All, S - Subscription, I - Internal).
        /// </param>
        /// <param name="activatedType">
        /// A <see cref="string"/> that holds the user type.
        /// (B - Both, A - Activated, N - Not activated).
        /// </param>
        /// <param name="orderByColumn">
        /// A <see cref="string"/> that holds the order by column
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that holds the order by direction.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        public List<UserDetail> GetCustomers(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string userType, string activatedType, string orderByColumn,
            SortType orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CUSTOMERS");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_TYPE",
                    DbType.String, userType == null || userType.Trim().Length == 0 ? null : userType.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@ACTIVATED",
                   DbType.String, activatedType == null || activatedType.Trim().Length == 0 ? null : activatedType.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderByColumn);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        if (dataReader["SUBSCRIPTION_ROLE"] != null)
                        {
                            userDetail.SubscriptionRole = dataReader["SUBSCRIPTION_ROLE"].ToString().Trim();
                        }

                        if (dataReader["EMAIL"] != null)
                        {
                            userDetail.Email = dataReader["EMAIL"].ToString().Trim();
                        }

                        if (dataReader["PHONE"] != null)
                        {
                            userDetail.Phone = dataReader["PHONE"].ToString().Trim();
                        }

                        if (dataReader["SUBSCRIPTION_TYPE"] != null)
                        {
                            userDetail.SubscriptionType = dataReader["SUBSCRIPTION_TYPE"].ToString().Trim();
                        }

                        if (dataReader["USRUSERTYPENAME"] != null)
                        {
                            userDetail.UserTypeName = dataReader["USRUSERTYPENAME"].ToString().Trim();
                        }

                        if (dataReader["ACTIVATED"] != null)
                        {
                            if (dataReader["ACTIVATED"].ToString().Trim().ToUpper() == "Y")
                                userDetail.Activated = "Yes";
                            else
                                userDetail.Activated = "No";
                        }
                        else
                        {
                            userDetail.Activated = "Yes";
                        }

                        if (dataReader["LEGAL_ACCEPTED"] != null)
                        {
                            if (dataReader["LEGAL_ACCEPTED"].ToString().Trim().ToUpper() == "Y")
                                userDetail.LegalAcceptedDesc = "Yes";
                            else
                                userDetail.LegalAcceptedDesc = "No";
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE"]) &&
                            Convert.ToInt32(dataReader["IS_ACTIVE"].ToString()) == 1)
                        {
                            userDetail.IsActive = true;
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of users for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<UserDetail> GetUsers(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string orderBy, SortType orderByDirection, int pageNumber,
            int pageSize, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_USERS");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of candidates for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first Name .
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last Name.
        /// </param>
        /// <param name="emailLike">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the candidate 
        /// details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<UserDetail> GetCandidates(string userNameLike, string firstNameLike,
            string lastNameLike, string emailLike, string orderBy, SortType orderByDirection,
            int pageNumber, int pageSize, out int totalRecords, int tenantID)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATES");

                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@EMAIL",
                    DbType.String, emailLike == null || emailLike.Trim().Length == 0 ? null : emailLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        if (dataReader["EMAIL"] != null)
                        {
                            userDetail.Email = dataReader["EMAIL"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                        {
                            userDetail.FullName = dataReader["FULLNAME"].ToString().Trim();
                        }

                        if (dataReader["MOBILE"] != null)
                        {
                            userDetail.Phone = dataReader["MOBILE"].ToString().Trim();
                        }
                        if (dataReader["CITY_NAME"] != null)
                        {
                            userDetail.City = dataReader["CITY_NAME"].ToString().Trim();
                        }
                        if (dataReader["CAND_ID"] != null && dataReader["CAND_ID"].ToString() != "")
                        {
                            userDetail.CandidateID = int.Parse(dataReader["CAND_ID"].ToString().Trim());
                        }
                        /*if (dataReader["RECRUITER_NAME"] != null && dataReader["RECRUITER_NAME"].ToString() != "")
                        {
                            userDetail.RecruitedBy = dataReader["RECRUITER_NAME"].ToString().Trim();
                        }*/

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of candidates for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first Name .
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last Name.
        /// </param>
        /// <param name="emailLike">
        /// A <see cref="string"/> that holds the email.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="status">
        /// A <see cref="string"/> that holds the session status.
        /// </param>
        /// <param name="pickedBy">
        /// A <see cref="int"/> that holds the picked by user. This will be
        /// zero if 'show my candidate' checkbox is unchecked in the use
        /// interface.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the candidate 
        /// details.
        /// </returns>
        public List<UserDetail> GetPositionProfileCandidates
            (string userNameLike, string firstNameLike, string lastNameLike,
            string emailLike, int tenantID, int positionProfileID, string status,
            int pickedBy, string orderBy, SortType orderByDirection,
            int pageNumber, int pageSize, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_CANDIDATES");

                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@EMAIL",
                    DbType.String, emailLike == null || emailLike.Trim().Length == 0 ? null : emailLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(getUsersCommand,
                  "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@STATUS", DbType.String, Utility.IsNullOrEmpty(status) ? null : status.Trim());

                if (pickedBy == 0)
                    HCMDatabase.AddInParameter(getUsersCommand, "@PICKED_BY", DbType.Int32, null);
                else
                    HCMDatabase.AddInParameter(getUsersCommand, "@PICKED_BY", DbType.Int32, pickedBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        if (dataReader["EMAIL"] != null)
                        {
                            userDetail.Email = dataReader["EMAIL"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                        {
                            userDetail.FullName = dataReader["FULLNAME"].ToString().Trim();
                        }
                        if (dataReader["CAND_ID"] != null && dataReader["CAND_ID"].ToString() != "")
                        {
                            userDetail.CandidateID = int.Parse(dataReader["CAND_ID"].ToString().Trim());
                        }
                        if (dataReader["STATUS"] != null)
                        {
                            userDetail.Status = dataReader["STATUS"].ToString().Trim();
                        }
                        if (dataReader["STATUS_DESCRIPTION"] != null)
                        {
                            userDetail.StatusDescription = dataReader["STATUS_DESCRIPTION"].ToString().Trim();
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the user detail for the given user ID.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the user detail.
        /// </returns>
        public UserDetail GetUserDetail(int userID)
        {
            UserDetail userDetail = null;

            IDataReader dataReader = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_USER");

                // Add parameters.
                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@USER_ID", DbType.Int32, userID);

                // Execute the procedure.
                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                if (dataReader.Read())
                {
                    // Instantiate user detail object.
                    userDetail = new UserDetail();

                    if (dataReader["usrID"] != null && dataReader["usrID"].ToString().Trim().Length > 0)
                    {
                        userDetail.UserID = Convert.ToInt32(dataReader["usrID"].ToString().Trim());
                    }

                    if (dataReader["usrUserName"] != null)
                    {
                        userDetail.UserName = dataReader["usrUserName"].ToString().Trim();
                    }

                    if (dataReader["usrFirstName"] != null)
                    {
                        userDetail.FirstName = dataReader["usrFirstName"].ToString().Trim();
                    }

                    if (dataReader["usrMiddleName"] != null)
                    {
                        userDetail.MiddleName = dataReader["usrMiddleName"].ToString().Trim();
                    }

                    if (dataReader["usrLastName"] != null)
                    {
                        userDetail.LastName = dataReader["usrLastName"].ToString().Trim();
                    }

                    if (dataReader["usrEMail"] != null)
                    {
                        userDetail.Email = dataReader["usrEMail"].ToString().Trim();
                    }
                    if (dataReader["usrMobile"] != null)
                    {
                        userDetail.Phone = dataReader["usrMobile"].ToString().Trim();
                    }

                    if (dataReader["USER_TYPE"] != null)
                    {
                        userDetail.UserType = (UserType)(dataReader["USER_TYPE"].ToString().Trim()
                            == "UT_ST_ADM" ? 1 : 0);
                    }
                    if (dataReader["IS_ACTIVE"] != null)
                    {
                        userDetail.IsActive = Convert.ToBoolean(dataReader["IS_ACTIVE"].ToString().Trim());
                    }
                    if (dataReader["EXPIRY_DATE"] != null)
                    {
                        if (!DBNull.Value.Equals(dataReader["EXPIRY_DATE"]))
                            userDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"]);
                    }
                    if (dataReader["SUBSCRIPTION_ROLE"] != null)
                    {
                        userDetail.SubscriptionRole = dataReader["SUBSCRIPTION_ROLE"].ToString();
                    }
                }
                return userDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the list of categories and subjects  from the database
        /// </summary>
        /// <param name="pagenumber">
        /// A<see cref="int"/>that holds the page number of the page
        /// </param>
        /// <param name="categoryID">
        /// A<see cref="string"/>that holds the category ID
        /// </param>
        /// <param name="categoryName">
        /// A<see cref="string"/>that holds the category name
        /// </param>
        /// <param name="subjectID">
        /// A<see cref="string"/>that holds the subject ID
        /// </param>
        /// <param name="subjectName">
        /// A<see cref="string"/>that holds the subject name
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size of the page
        /// </param>
        /// <param name="totalRecords">
        /// A<see cref="int"/>that holds the total records of categories 
        /// </param>
        /// <param name="orderBy">
        /// A<see cref="string"/>that holds the expression to sort
        /// </param>
        /// <param name="orderByDirection">
        /// A<see cref="SortType"/>that holds the sort by direction.
        /// </param>
        /// <param name="keyWord">
        /// A<see cref="string"/>that holds the keyWord.
        /// </param>
        /// <returns>
        /// A List for<see cref="Subject"/>that holds the List of categories and Subjects details 
        /// </returns>
        public List<Subject> GetCategorySubjects(int pagenumber, int tenantID,
            string categoryID, string categoryName, string subjectID,
            string subjectName, int pageSize, out int totalRecords,
            string orderBy, SortType orderByDirection, string keyWord)
        {
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getCategorySubjectCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CATEGORY_SUBJECTS");

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@TENANT_ID", DbType.Int32,
                  tenantID);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@CATEGORY_ID", DbType.Int32,
                      Utility.IsNullOrEmpty(categoryID) ? 0 : int.Parse(categoryID));

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@CATEGORY_NAME", DbType.String,
                    Utility.IsNullOrEmpty(categoryName) ? null : categoryName);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@SUBJECT_ID", DbType.Int32,
                    Utility.IsNullOrEmpty(subjectID) ? 0 : int.Parse(subjectID));

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@SUBJECT_NAME", DbType.String,
                    Utility.IsNullOrEmpty(subjectName) ? null : subjectName);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@PAGENUM",
                    DbType.Int32, pagenumber);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@PAGESIZE",
                    DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@KEYWORD",
                    DbType.String, Utility.IsNullOrEmpty(keyWord) ? null : keyWord);

                dataReader = HCMDatabase.ExecuteReader(getCategorySubjectCommand);

                List<Subject> subjectDetails = null;

                while (dataReader.Read())
                {
                    // Instantiate subject details list.
                    if (subjectDetails == null)
                        subjectDetails = new List<Subject>();

                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        Subject subject = new Subject();

                        if (!Utility.IsNullOrEmpty(dataReader["CAT_KEY"]))
                        {
                            subject.CategoryID = Convert.ToInt32(dataReader["CAT_KEY"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                        {
                            subject.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"]))
                        {
                            subject.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                        {
                            subject.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                        }
                        subjectDetails.Add(subject);

                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return subjectDetails;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method to get the list of categories from the database
        /// </summary>
        /// <param name="pagenumber">
        /// A <see cref="int"/>that holds the page number of the page
        /// </param>
        /// <param name="categoryId">
        /// A <see cref="string"/>that holds the category ID
        /// </param>
        /// <param name="categoryName">
        /// A <see cref="string"/>that holds the category name
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/>that holds the page size of the page
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/>that holds the total records of categories 
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/>that holds the expression to sort
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/>that holds the sort type.
        /// </param>
        /// <returns>
        /// A List for<see cref="Category"/>that holds the List of categories
        /// </returns>
        public List<Category> GetCategorySubjects(int tenantId, int pagenumber, string categoryId,
            string categoryName, int pageSize, out int totalRecords, string orderBy,
            SortType orderByDirection)
        {
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getCategorySubjectCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CATEGORY");

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@TENANT_ID", DbType.Int32, tenantId);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@CATEGORY_ID", DbType.Int32,
                      categoryId == "" ? 0 : int.Parse(categoryId));

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@CATEGORY_NAME", DbType.String,
                    categoryName == "" || categoryName.Trim().Length == 0 ? null : categoryName);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@PAGENUM",
                    DbType.Int32, pagenumber);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@PAGESIZE",
                    DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getCategorySubjectCommand);

                List<Category> categoryDetails = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate categories list.
                        if (categoryDetails == null)
                            categoryDetails = new List<Category>();

                        Category category = new Category();

                        if (!Utility.IsNullOrEmpty(dataReader["CAT_KEY"]))
                        {
                            category.CategoryID = Convert.ToInt32(dataReader["CAT_KEY"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                        {
                            category.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                        }
                        categoryDetails.Add(category);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return categoryDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method to get the list of categories from the database based on search criteria.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="questionRealtionId">
        /// A <see cref="int"/>that holds the question Realtion Id
        /// </param>
        /// <returns>
        /// A List for <see cref="Subject"/>that holds the List of Subjects
        /// </returns>
        public List<Subject> GetCategorySubjects(string questionKey, int questionRealtionId)
        {
            IDataReader dataReader = null;
            int? QuestionRelationID = null;
            if (questionRealtionId != 0)
                QuestionRelationID = questionRealtionId;

            try
            {
                DbCommand getCategorySubjectCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_SUBJECTS_CATEGORIES_RELATIONID");

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@QUESTION_KEY", DbType.String,
                    Utility.IsNullOrEmpty(questionKey) ? null : questionKey);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@RELATION_ID", DbType.Int32,
                   QuestionRelationID);


                dataReader = HCMDatabase.ExecuteReader(getCategorySubjectCommand);

                List<Subject> subjectDetails = null;

                while (dataReader.Read())
                {
                    // Instantiate subject details list.
                    if (subjectDetails == null)
                        subjectDetails = new List<Subject>();

                    Subject subject = new Subject();

                    if (!Utility.IsNullOrEmpty(dataReader["CAT_KEY"]))
                    {
                        subject.CategoryID = Convert.ToInt32(dataReader["CAT_KEY"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        subject.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"]))
                    {
                        subject.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        subject.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    }
                    subjectDetails.Add(subject);

                }

                return subjectDetails;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of position profiles for the given
        /// search criteria.
        /// </summary>
        /// <param name="searchCriteria">
        /// A <see cref="ClientRequestSearchCriteria"/> that holds the search
        /// criteria.
        /// </param>
        /// <param name="pagenumber">
        /// A <see cref="int"/>that holds the page number of the page
        /// </param>
        /// <param name="pageSize">
        /// A<see cref="int"/>that holds the page size of the page
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/>that holds the total records.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/>that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="SortType"/> that holds the sort order, ascending
        /// or descending.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the logged in user ID
        /// </param>
        /// <returns>
        /// A list for <see cref="ClientRequestDetail"/> that holds the 
        /// position profiles.
        /// </returns>
        public List<ClientRequestDetail> GetClientRequestInformation
            (ClientRequestSearchCriteria searchCriteria, int pagenumber, int pageSize,
            out int totalRecords, string orderBy, SortType orderByDirection, int userID)
        {
            totalRecords = 0;
            IDataReader dataReader = null;

            try
            {
                DbCommand getClientRequestCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CLIENT_REQUEST_INFORMATION_SEARCH");
                HCMDatabase.AddInParameter(getClientRequestCommand, "@USRID", DbType.Int32, userID);
                HCMDatabase.AddInParameter(getClientRequestCommand, "@POSITION_PROFILE_NAME", DbType.String, searchCriteria.PositionProfileName);
                //clientRequestNumber == "" || clientRequestNumber.Trim().Length == 0 ? null : clientRequestNumber);
                HCMDatabase.AddInParameter(getClientRequestCommand, "@CLIENT", DbType.String,
                    Utility.IsNullOrEmpty(searchCriteria.ClientName) ? null : searchCriteria.ClientName);
                //client == "" || client.Trim().Length == 0 ? null : client);

                HCMDatabase.AddInParameter(getClientRequestCommand, "@POSITION_NAME", DbType.String,
                    searchCriteria.PositionName == "" || searchCriteria.PositionName.Trim().Length == 0 ? null : searchCriteria.PositionName);
                HCMDatabase.AddInParameter(getClientRequestCommand, "@CREATED_DATE", DbType.String,
                    searchCriteria.CreatedDateStr == "" || searchCriteria.CreatedDateStr.Trim().Length == 0 ? null : searchCriteria.CreatedDateStr);
                HCMDatabase.AddInParameter(getClientRequestCommand, "@DEPARTMENT_NAME", DbType.String,
                    searchCriteria.DepartmentName == "" || searchCriteria.DepartmentName.Trim().Length == 0 ? null : searchCriteria.DepartmentName);
                HCMDatabase.AddInParameter(getClientRequestCommand, "@CONTACT_NAME", DbType.String,
                    searchCriteria.ContactName == "" || searchCriteria.ContactName.Trim().Length == 0 ? null : searchCriteria.ContactName);
                HCMDatabase.AddInParameter(getClientRequestCommand, "@LOCATION", DbType.String,
                    searchCriteria.Location == "" || searchCriteria.Location.Trim().Length == 0 ? null : searchCriteria.Location);
                HCMDatabase.AddInParameter(getClientRequestCommand, "@CREATED_BY", DbType.String,
                    searchCriteria.CreatedBy == "" || searchCriteria.CreatedBy.Trim().Length == 0 ? null : searchCriteria.CreatedBy);

                HCMDatabase.AddInParameter(getClientRequestCommand, "@PAGENUM",
                    DbType.Int32, pagenumber);

                HCMDatabase.AddInParameter(getClientRequestCommand, "@PAGESIZE",
                    DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getClientRequestCommand, "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getClientRequestCommand, "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                dataReader = HCMDatabase.ExecuteReader(getClientRequestCommand);

                List<ClientRequestDetail> clientRequestDetail = null;
                ClientRequestDetail clientRequest = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate categories list.
                        if (clientRequestDetail == null)
                            clientRequestDetail = new List<ClientRequestDetail>();

                        clientRequest = new ClientRequestDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["GEN_ID"]))
                        {
                            clientRequest.genID = Convert.ToInt32(dataReader["GEN_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CLIENT_REQUEST_NUMBER"]))
                        {
                            clientRequest.ClientRequestNumber = dataReader["CLIENT_REQUEST_NUMBER"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CLIENTNAME"]))
                        {
                            clientRequest.ClientName = (dataReader["CLIENTNAME"]).ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_NAME"]))
                        {
                            clientRequest.PositionName = dataReader["POSITION_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                        {
                            clientRequest.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["DEPARTMENT_NAME"]))
                        {
                            clientRequest.DepartmentName = dataReader["DEPARTMENT_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CONTACT_NAME"]))
                        {
                            clientRequest.ContactName = dataReader["CONTACT_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["STREET_NAME"]))
                        {
                            clientRequest.Street = dataReader["STREET_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        {
                            clientRequest.CreatedBy = dataReader["CREATED_BY"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        {
                            clientRequest.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TAGS"]))
                        {
                            clientRequest.Tags = dataReader["TAGS"].ToString().TrimEnd(',').TrimStart(',');
                            clientRequest.TagsWithNoWeightage = GetKeyWordsWithNoWeightage(clientRequest.Tags);
                        }
                        clientRequestDetail.Add(clientRequest);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return clientRequestDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// A Method that get the skill name from the 
        /// keyword by removing weightages
        /// </summary>
        /// <param name="KeyWords">
        /// A <see cref="System.String"/> that holds the skill names with 
        /// weightages
        /// </param>
        /// <returns>
        /// A <see cref="System.String"/> that contains only skill names 
        /// with out their weightages
        /// </returns>
        private string GetKeyWordsWithNoWeightage(string KeyWords)
        {
            StringBuilder sb = new StringBuilder();
            string[] strSplit = KeyWords.Split(',');
            for (int i = 0; i < strSplit.Length; i++)
                try
                {
                    sb.Append(strSplit[i].Substring(0, strSplit[i].IndexOf('[')) + ",");
                }
                catch { }
            return sb.ToString().TrimEnd(',');
        }

        /// <summary>
        /// Method that retrieves the list of users for the given search 
        /// criteria.
        /// </summary>
        /// <param name="userNameLike">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="firstNameLike">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="middleNameLike">
        /// A <see cref="string"/> that holds the middle name.
        /// </param>
        /// <param name="lastNameLike">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <param name="orderBy">
        /// A <see cref="string"/> that holds the order by field.
        /// </param>
        /// <param name="orderByDirection">
        /// A <see cref="string"/> that holds the order by direction. 'A' 
        /// represents ascending and 'D' descending.
        /// </param>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number to which the records
        /// need to be retrieved.
        /// </param>
        /// <param name="pageSize">
        /// A <see cref="int"/> that holds the page size, that indicates the 
        /// total number of records to be displayed in a page.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        /// <remarks>
        /// Supports providing paging data.
        /// </remarks>
        public List<UserDetail> GetUsersAndCandidates(string userNameLike,
            string firstNameLike, string middleNameLike, string lastNameLike,
            string orderBy, SortType orderByDirection, int pageNumber, int pageSize, int tenantID,
            out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {

                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_USERS_AND_CANDIDATES");

                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@TENANTID", DbType.Int32, tenantID);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        if (dataReader["EMAIL"] != null)
                        {
                            userDetail.Email = dataReader["EMAIL"].ToString().Trim();
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }

        /// <summary>
        /// Represets the method to get the category id for the given name
        /// </summary>
        /// <param name="categoryName">
        /// A<see cref="string"/>that holds the category name
        /// </param>
        public int GetCategoryID(string categoryName)
        {
            int categoryId = 0;
            DbCommand getCategoryNameCommand =
                HCMDatabase.GetStoredProcCommand("SPGET_CATEGORY_ID");

            HCMDatabase.AddInParameter(getCategoryNameCommand, "@CATEGORY_NAME", DbType.String, categoryName);

            object id = HCMDatabase.ExecuteScalar(getCategoryNameCommand);

            if (!Utility.IsNullOrEmpty(id))
            {
                categoryId = int.Parse(id.ToString().Trim());
            }
            else
            {
                categoryId = 0;
            }
            return categoryId;
        }

        /// <summary>
        /// Gets the tenant users.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="middleName">Name of the middle.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="sortField">The sort field.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public List<UserDetail> GetTenantUsers(string userName, string firstName, string middleName,
            string lastName, string sortField, SortType sortOrder, int pageNumber, int pageSize, out int totalRecords, int tenantID, int userID)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TENANT_USERS");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userName == null || userName.Trim().Length == 0 ? null : userName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstName == null || firstName.Trim().Length == 0 ? null : firstName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleName == null || middleName.Trim().Length == 0 ? null : middleName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastName == null || lastName.Trim().Length == 0 ? null : lastName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, sortField);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, sortOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@TENANT_ID", DbType.String, tenantID);

                HCMDatabase.AddInParameter(getUsersCommand,
                  "@USER_ID", DbType.String, userID);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return users;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the tenant users for position profile forms.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="middleName">Name of the middle.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="sortField">The sort field.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public List<UserDetail> GetPositionProfileFormTenantUsers(string userName, string firstName, string middleName,
            string lastName, string sortField, SortType sortOrder, int pageNumber, int pageSize, out int totalRecords, int tenantID)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_FORM_TENANT_USERS");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userName == null || userName.Trim().Length == 0 ? null : userName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstName == null || firstName.Trim().Length == 0 ? null : firstName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleName == null || middleName.Trim().Length == 0 ? null : middleName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastName == null || lastName.Trim().Length == 0 ? null : lastName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, sortField);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, sortOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@TENANT_ID", DbType.String, tenantID);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return users;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        public bool CheckFeatureApplicableOrNot(int subscriptionId, int featureID)
        {
            DbCommand checkFeatureApplicableCommand =
                HCMDatabase.GetStoredProcCommand("SPCHECK_APPLICABLE_FEATURE");

            HCMDatabase.AddInParameter(checkFeatureApplicableCommand, "@FEATURE_ID", DbType.Int32, featureID);

            HCMDatabase.AddInParameter(checkFeatureApplicableCommand, "@SUBSCRIPTION_ID", DbType.Int32, subscriptionId);

            object id = HCMDatabase.ExecuteScalar(checkFeatureApplicableCommand);

            if (!Utility.IsNullOrEmpty(id))
            {
                //If the value is 1 the Not applicable is true.
                return Convert.ToBoolean(id);
            }

            return false;
        }

        #endregion




        public List<UserDetail> GetCandidatesForCandidateReport(string userNameLike, string firstNameLike,
            string lastNameLike, string emailLike, string orderBy, SortType orderByDirection,
            int pageNumber, int pageSize, out int totalRecords, int tenantID)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATES_FOR_CANDIDATEREPORT");

                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@EMAIL",
                    DbType.String, emailLike == null || emailLike.Trim().Length == 0 ? null : emailLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        if (dataReader["EMAIL"] != null)
                        {
                            userDetail.Email = dataReader["EMAIL"].ToString().Trim();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["FULLNAME"]))
                        {
                            userDetail.FullName = dataReader["FULLNAME"].ToString().Trim();
                        }

                        if (dataReader["MOBILE"] != null)
                        {
                            userDetail.Phone = dataReader["MOBILE"].ToString().Trim();
                        }
                        if (dataReader["CITY_NAME"] != null)
                        {
                            userDetail.City = dataReader["CITY_NAME"].ToString().Trim();
                        }


                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <param name="userNameLike">The user name like.</param>
        /// <param name="firstNameLike">The first name like.</param>
        /// <param name="middleNameLike">The middle name like.</param>
        /// <param name="lastNameLike">The last name like.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="orderByDirection">The order by direction.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="roleCodes">The role codes.</param>
        /// <param name="totalRecords">The total records.</param>
        /// <returns></returns>
        public List<UserDetail> GetUsers(string userNameLike,
             string firstNameLike, string middleNameLike, string lastNameLike, int userID,
             string orderBy, SortType orderByDirection, int pageNumber,
             int pageSize, string roleCodes, out int totalRecords)
        {
            // Assing value to out parameter.
            totalRecords = 0;

            IDataReader dataReader = null;

            try
            {
                DbCommand getUsersCommand = null;
                if (roleCodes == "CA")
                    getUsersCommand = HCMDatabase.GetStoredProcCommand("SPGET_USERS_TENANT_ID");
                else
                    getUsersCommand = HCMDatabase.GetStoredProcCommand("SPGET_USERS");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userNameLike == null || userNameLike.Trim().Length == 0 ? null : userNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstNameLike == null || firstNameLike.Trim().Length == 0 ? null : firstNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleNameLike == null || middleNameLike.Trim().Length == 0 ? null : middleNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastNameLike == null || lastNameLike.Trim().Length == 0 ? null : lastNameLike.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                  "@USER_ID",
                  DbType.Int16, userID == 0 ? 0 : userID);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, orderBy);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, orderByDirection == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@ROLE_CODES", DbType.String, roleCodes);

                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (dataReader["USER_ID"] != null && dataReader["USER_ID"].ToString().Trim().Length > 0)
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }

                        if (dataReader["USER_NAME"] != null)
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }

                        if (dataReader["FIRST_NAME"] != null)
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }

                        if (dataReader["MIDDLE_NAME"] != null)
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }

                        if (dataReader["LAST_NAME"] != null)
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }

                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }
                return users;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public string[] GetClientList(string prefixText, int count, int tenantID)
        {
            IDataReader dataReader = null;

            try
            {
                List<string> client = null;

                DbCommand getClinetCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_AUTOFILL_CLIENT");

                HCMDatabase.AddInParameter(getClinetCommand, "@PREFIX", DbType.String, prefixText);
                HCMDatabase.AddInParameter(getClinetCommand, "@TENANT_ID", DbType.String, tenantID);

                dataReader = HCMDatabase.ExecuteReader(getClinetCommand);

                while (dataReader.Read())
                {
                    if (client == null)
                        client = new List<string>();

                    if (dataReader["CLIENTNAME"] != null)
                        client.Add(dataReader["CLIENTNAME"].ToString().Trim());
                }

                if (client == null)
                    return null;

                return client.ToArray();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }
        public bool IsValidClient(int clientID, string clientName)
        {
            try
            {
                DbCommand getClientCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CHECK_CLIENT");

                // Add Input parameter
                HCMDatabase.AddInParameter(getClientCommand, "@CLIENT_ID", DbType.Int32, clientID);
                HCMDatabase.AddInParameter(getClientCommand, "@CLIENT_NAME", DbType.String, clientName);

                // Retrieve the count of affected rows
                int rowsAffected = Convert.ToInt32(HCMDatabase.ExecuteScalar(getClientCommand));

                return rowsAffected == 1 ? true : false;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public List<Department> GetDepartments(int clientID)
        {

            List<Department> department = new List<Department>();

            IDataReader dataReader = null;

            DbCommand getDepartmentCommand = HCMDatabase.
                GetStoredProcCommand("SPGET_DEPARTMENT_DEPT_IDS");

            HCMDatabase.AddInParameter(getDepartmentCommand,
                "@CLIENT_ID", DbType.Int32, clientID);

            dataReader = HCMDatabase.ExecuteReader(getDepartmentCommand);

            while (dataReader.Read())
            {
                Department dept = new Department();

                dept.DepartmentID = int.Parse
                    (dataReader["DEPARTMENT_ID"].ToString());

                dept.DepartmentName = dataReader["DEPARTMENT_NAME"]
                    .ToString().Trim();

                department.Add(dept);
            }

            dataReader.Close();
            return department;
        }

        public List<Department> GetDepartmentContacts(string departmentID, string clientID)
        {
            List<Department> department = new List<Department>();

            IDataReader dataReader = null;

            DbCommand getDepartmentCommand = HCMDatabase.
                GetStoredProcCommand("SPGET_DEPARTMENT_CONTACTS");

            HCMDatabase.AddInParameter(getDepartmentCommand,
                "@DEPARTMENT_IDS", DbType.String, departmentID);

            HCMDatabase.AddInParameter(getDepartmentCommand,
              "@CLIENT_ID", DbType.Int32, clientID);

            dataReader = HCMDatabase.ExecuteReader(getDepartmentCommand);

            while (dataReader.Read())
            {
                Department dept = new Department();

                dept.DepartmentContact = dataReader["DEPARTMENT_CONTACTS"]
                    .ToString().Trim();
                dept.ContactID = dataReader["CONTACT_ID"]
                    .ToString().Trim();

                department.Add(dept);
            }

            dataReader.Close();
            return department;
        }
        public List<Department> GetDepartmentClientIds(short contactID, int departmentID, int clientID)
        {
            List<Department> departmentClient = new List<Department>();

            IDataReader dataReader = null;

            DbCommand getDepartmentCommand = HCMDatabase.
                GetStoredProcCommand("SPGET_DEPARTMENT_CLIENT");

            HCMDatabase.AddInParameter(getDepartmentCommand,
                 "@CONTACT_ID", DbType.Int32, contactID);
            HCMDatabase.AddInParameter(getDepartmentCommand,
                "@DEPARTMENT_ID", DbType.Int16, departmentID);
            HCMDatabase.AddInParameter(getDepartmentCommand,
                "@CLIENT_ID", DbType.Int16, clientID);

            dataReader = HCMDatabase.ExecuteReader(getDepartmentCommand);

            while (dataReader.Read())
            {
                Department dept = new Department();

                dept.ClientName = dataReader["CLIENT_ID"].ToString();
                dept.DepartmentID = Convert.ToInt16(dataReader["DEPARTMENT_ID"]);
                departmentClient.Add(dept);
            }

            dataReader.Close();
            return departmentClient;
        }

        /// <summary>
        /// Method that inserts the candidate activity log.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID which refers to
        /// CANDIDATEINFORMATION table.
        /// </param>
        /// <param name="candidateLoginID">
        /// A <see cref="int"/> that holds the candidate login ID which refers
        /// to PRAS_USER table.
        /// </param>
        /// <param name="comments">
        /// A <see cref="string"/> that holds the comments.
        /// </param>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="activityType">
        /// A <see cref="string"/> that holds the activity type.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the candidate activity log ID.
        /// </returns>
        public int InsertCandidateActivityLog(int candidateID, int candidateLoginID,
            string comments, int positionProfileID, int userID, string activityType)
        {
            DbCommand insertActivityLogCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CANDIDATE_ACTIVITY_LOG");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertActivityLogCommand,
                "@ACTIVITY_TYPE", DbType.String, activityType);

            if (candidateID == 0)
            {
                HCMDatabase.AddInParameter(insertActivityLogCommand,
                    "@CANDIDATE_ID", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertActivityLogCommand,
                 "@CANDIDATE_ID", DbType.Int32, candidateID);
            }

            if (candidateLoginID == 0)
            {
                HCMDatabase.AddInParameter(insertActivityLogCommand,
                    "@CANDIDATE_LOGIN_ID", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertActivityLogCommand,
                 "@CANDIDATE_LOGIN_ID", DbType.Int32, candidateLoginID);
            }

            HCMDatabase.AddInParameter(insertActivityLogCommand,
                "@COMMENTS", DbType.String, comments);

            if (positionProfileID == 0)
            {
                HCMDatabase.AddInParameter(insertActivityLogCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, null);
            }
            else
            {
                HCMDatabase.AddInParameter(insertActivityLogCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);
            }

            HCMDatabase.AddInParameter(insertActivityLogCommand,
                "@USER_ID", DbType.Int32, userID);

            // Add output parameters.
            HCMDatabase.AddOutParameter(insertActivityLogCommand,
                "@LOG_ID", DbType.Int32, 4);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertActivityLogCommand);

            // Retrieve the output parameter value.
            if (insertActivityLogCommand.Parameters["@LOG_ID"].Value != null &&
                insertActivityLogCommand.Parameters["@LOG_ID"].Value != DBNull.Value)
            {
                return int.Parse(insertActivityLogCommand.Parameters["@LOG_ID"].Value.ToString());
            }
            else
            {
                return 0;
            }
        }

        public CandidateActivityLog GetCandidateActivityLog(int logID)
        {
            IDataReader reader = null;
            CandidateActivityLog activityLog = null;

            try
            {
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_CANDIDATE_ACTIVITY_LOG");

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@LOG_ID", DbType.Int32, logID);

                reader = HCMDatabase.ExecuteReader(getCategoryCommand);

                if (reader.Read())
                {
                    // Instantiate the activity log object.
                    activityLog = new CandidateActivityLog();

                    if (!Utility.IsNullOrEmpty(reader["CANDIDATE_NAME"]))
                        activityLog.CandidateName = reader["CANDIDATE_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(reader["COMMENTS"]))
                        activityLog.Comments = reader["COMMENTS"].ToString();

                    if (!Utility.IsNullOrEmpty(reader["DATE"]))
                        activityLog.Date = Convert.ToDateTime(reader["DATE"]);

                    if (!Utility.IsNullOrEmpty(reader["CANDIDATE_OWNER_NAME"]))
                        activityLog.CandidateOwnerName = reader["CANDIDATE_OWNER_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(reader["CANDIDATE_OWNER_EMAIL"]))
                        activityLog.CandidateOwnerEmail = reader["CANDIDATE_OWNER_EMAIL"].ToString();

                    if (!Utility.IsNullOrEmpty(reader["NOTES_ADDED_BY_NAME"]))
                        activityLog.NotesAddedByName = reader["NOTES_ADDED_BY_NAME"].ToString();

                    if (!Utility.IsNullOrEmpty(reader["SEND_MAIL"]))
                        activityLog.SendMail = reader["SEND_MAIL"].ToString().ToUpper().Trim() == "Y" ? true : false;
                    else
                        activityLog.SendMail = true;
                }

                return activityLog;
            }
            finally
            {
                // Close datareader
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the user options.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <returns>
        /// A <see cref="UserOptionDetail"/> that holds the user option detail.
        /// </returns>
        public UserOptionDetail GetUserOptions(int userID)
        {
            IDataReader reader = null;
            UserOptionDetail optionDetail = new UserOptionDetail();

            try
            {
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_EMAIL_NOTIFICATION_SETTINGS");

                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@USER_ID", DbType.Int32, userID);

                reader = HCMDatabase.ExecuteReader(getCategoryCommand);

                // Set default settings.
                optionDetail.SendMailOnNotesAdded = true;

                while (reader.Read())
                {
                    if (!Utility.IsNullOrEmpty(reader["NOTIFICATION_TYPE"]))
                    {
                        string notificationType = reader["NOTIFICATION_TYPE"].ToString().Trim();

                        if (notificationType == Constants.CandidateActivityLogType.NOTES_ADDED)
                        {
                            if (!Utility.IsNullOrEmpty(reader["SEND_MAIL"]) &&
                               reader["SEND_MAIL"].ToString().Trim().ToUpper() == "N")
                            {
                                optionDetail.SendMailOnNotesAdded = false;
                            }
                        }
                    }
                }

                return optionDetail;
            }
            finally
            {
                // Close datareader
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the email notification.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="notificationType">
        /// A <see cref="string"/> that holds the notification type.
        /// </param>
        /// <param name="sendMail">
        /// A <see cref="bool"/> that holds the send mail status.
        /// </param>
        public void UpdateEmailNotification
            (int userID, string notificationType, bool sendMail)
        {
            DbCommand updateOption = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_EMAIL_NOTIFICATION_SETTINGS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateOption,
                "@USER_ID", DbType.Int32, userID);
            HCMDatabase.AddInParameter(updateOption,
                "@NOTIFICATION_TYPE", DbType.String, notificationType);
            HCMDatabase.AddInParameter(updateOption,
                "@SEND_MAIL", DbType.String, sendMail == true ? "Y" : "N");

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateOption);
        }

        /// <summary>
        /// Retrieves the list of email details of the given tenant.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        ///  A list of <see cref="GetEmailDetail"/> that holds the email details.
        /// </returns>
        public List<EmailDetail> GetEmailDetail(int tenantID)
        {
            IDataReader dataReader = null;
            List<EmailDetail> emailDetails = null;

            try
            {
                TestDetail tstDetail = new TestDetail();

                // Create a stored procedure command object.
                DbCommand getEmailCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_TENANT_USER_EMAILS");

                HCMDatabase.AddInParameter(getEmailCommand,
                    "@TENANT_ID", System.Data.DbType.Int32, tenantID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getEmailCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (emailDetails == null)
                        emailDetails = new List<EmailDetail>();

                    // Create and instantiate email detail object.
                    EmailDetail emailDetail = new EmailDetail();

                    string name = string.Empty;
                    string email = string.Empty;

                    // Construct name.
                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                        name = dataReader["NAME"].ToString().Trim();

                    // Construct email.
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL"]))
                        email = dataReader["EMAIL"].ToString().Trim();

                    emailDetail.DisplayName = name + "(" + email + ")";
                    emailDetail.Email = email;

                    // Add to the list.
                    emailDetails.Add(emailDetail);
                }
                return emailDetails;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Retrieves the list of email contacts for the given search criteria.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="keyword">
        /// A <see cref="string"/> that holds the keyword.
        /// </param>
        /// <param name="category">
        /// A <see cref="string"/> that holds the category.
        /// </param>
        /// <param name="roleID">
        /// A <see cref="string"/> that holds the role ID.
        /// </param>
        /// <returns>
        ///  A list of <see cref="GetEmailDetail"/> that holds the email details.
        /// </returns>
        /// <remarks>
        /// This is used in email window to select contacts.
        /// </remarks>
        public List<MailContact> GetMailContacts(
            int tenantID, int userID, string keyword, string category, int roleID)
        {
            IDataReader dataReader = null;
            List<MailContact> mailContacts = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getEmailCommand = HCMDatabase.
                     GetStoredProcCommand("SPGET_MAIL_CONTACTS");

                // Add parameters.
                HCMDatabase.AddInParameter(getEmailCommand,
                    "@TENANT_ID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(getEmailCommand,
                    "@USER_ID", DbType.Int32, userID);
                HCMDatabase.AddInParameter(getEmailCommand,
                    "@KEYWORD", DbType.String, Utility.IsNullOrEmpty(keyword) ? null : keyword.Trim());
                HCMDatabase.AddInParameter(getEmailCommand,
                    "@CATEGORY", DbType.String, category);

                if (roleID == 0)
                {
                    HCMDatabase.AddInParameter(getEmailCommand,
                        "@ROLE_ID", DbType.Int32, null);
                }
                else
                {
                    HCMDatabase.AddInParameter(getEmailCommand,
                        "@ROLE_ID", DbType.Int32, roleID);
                }

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getEmailCommand);

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    if (mailContacts == null)
                        mailContacts = new List<MailContact>();

                    // Create and instantiate email detail object.
                    MailContact mailContact = new MailContact();

                    // Full name.
                    if (!Utility.IsNullOrEmpty(dataReader["FULL_NAME"]))
                        mailContact.FullName = dataReader["FULL_NAME"].ToString().Trim();

                    // Company.
                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                        mailContact.Company = dataReader["COMPANY"].ToString().Trim();

                    // Email ID.
                    if (!Utility.IsNullOrEmpty(dataReader["EMAIL_ID"]))
                        mailContact.EmailID = dataReader["EMAIL_ID"].ToString().Trim();

                    // Add to the list.
                    mailContacts.Add(mailContact);
                }
                return mailContacts;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the keyword for mail contacts based on the 
        /// submit type & position profile.
        /// </summary>
        /// <param name="positionProfileID">
        /// A <see cref="int"/> that holds the position profile ID.
        /// </param>
        /// <param name="submitType">
        /// A <see cref="string"/> that holds the submit type. 'C' represents 'client' 
        /// and 'I' represents 'internal'.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the contact keyword.
        /// </returns>
        public string GetPositionProfileContactKeyword(int positionProfileID, string submitType)
        {
            IDataReader reader = null;

            try
            {
                DbCommand getKeywordCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_POSITION_PROFILE_CONTACT_KEYWORD");

                HCMDatabase.AddInParameter(getKeywordCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfileID);

                HCMDatabase.AddInParameter(getKeywordCommand,
                    "@SUBMIT_TYPE", DbType.String, submitType);

                reader = HCMDatabase.ExecuteReader(getKeywordCommand);

                if (!reader.Read())
                    return null;

                if (!Utility.IsNullOrEmpty(reader["DATA"]))
                {
                    string keyword = reader["DATA"].ToString().Trim();
                    keyword = keyword.TrimEnd(new char[] { ',' });
                    return keyword;
                }

                return null;
            }
            finally
            {
                // Close datareader
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the browser detail and instructions that can 
        /// be shown to the candidates that helps in configure the cyber 
        /// proctor browser settings.
        /// </summary>
        /// <param name="browserType">
        /// A <see cref="string"/> that holds the browser type.
        /// </param>
        /// <returns>
        /// A <see cref="BrowserDetail"/> that holds the browser detail and
        /// instructions.
        /// </returns>
        public BrowserDetail GetBrowserInstructions(string browserType)
        {
            IDataReader reader = null;
            UserOptionDetail optionDetail = new UserOptionDetail();
            BrowserDetail browserDetail = null;

            try
            {
                DbCommand getInstructions = HCMDatabase.
                    GetStoredProcCommand("SPGET_CYBER_PROCTOR_BROWSER_INSTRUCTIONS");

                HCMDatabase.AddInParameter(getInstructions,
                    "@BROWSER_TYPE", DbType.String, browserType);

                reader = HCMDatabase.ExecuteReader(getInstructions);

                // Reader the browser name.
                if (reader.Read())
                {
                    // Instantiate the browser detail object.
                    if (browserDetail == null)
                        browserDetail = new BrowserDetail();

                    if (!Utility.IsNullOrEmpty(reader["BROWSER_NAME"]))
                    {
                        browserDetail.Name = reader["BROWSER_NAME"].ToString().Trim();
                    }

                    // Move to read the instructions.
                    reader.NextResult();
                    while (reader.Read())
                    {
                        // Instantiate the browser instructions object.
                        if (browserDetail.Instructions == null)
                            browserDetail.Instructions = new List<BrowserInstruction>();

                        if (!Utility.IsNullOrEmpty(reader["INSTRUCTION"]))
                        {
                            browserDetail.Instructions.Add
                                (new BrowserInstruction(reader["INSTRUCTION"].ToString().Trim()));
                        }
                    }
                }

                return browserDetail;
            }
            finally
            {
                // Close datareader
                if (reader != null && !reader.IsClosed)
                {
                    reader.Close();
                }
            }
        }
        /// <summary>
        /// Method to get the list of categories from the database based on search criteria.
        /// </summary>
        /// <param name="questionKey">
        /// A <see cref="string"/>that holds the question Key
        /// </param>
        /// <param name="questionRealtionId">
        /// A <see cref="int"/>that holds the question Realtion Id
        /// </param>
        /// <returns>
        /// A List for <see cref="Subject"/>that holds the List of Subjects
        /// </returns>
        public List<Subject> GetInterviewCategorySubjects(string questionKey, int questionRealtionId)
        {
            IDataReader dataReader = null;
            int? QuestionRelationID = null;
            if (questionRealtionId != 0)
                QuestionRelationID = questionRealtionId;

            try
            {
                DbCommand getCategorySubjectCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_INTERVIEWSUBJECTS_CATEGORIES_RELATIONID");

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@QUESTION_KEY", DbType.String,
                    Utility.IsNullOrEmpty(questionKey) ? null : questionKey);

                HCMDatabase.AddInParameter(getCategorySubjectCommand, "@RELATION_ID", DbType.Int32,
                   QuestionRelationID);


                dataReader = HCMDatabase.ExecuteReader(getCategorySubjectCommand);

                List<Subject> subjectDetails = null;

                while (dataReader.Read())
                {
                    // Instantiate subject details list.
                    if (subjectDetails == null)
                        subjectDetails = new List<Subject>();

                    Subject subject = new Subject();

                    if (!Utility.IsNullOrEmpty(dataReader["CAT_KEY"]))
                    {
                        subject.CategoryID = Convert.ToInt32(dataReader["CAT_KEY"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CATEGORY_NAME"]))
                    {
                        subject.CategoryName = dataReader["CATEGORY_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CAT_SUB_ID"]))
                    {
                        subject.SubjectID = Convert.ToInt32(dataReader["CAT_SUB_ID"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBJECT_NAME"]))
                    {
                        subject.SubjectName = dataReader["SUBJECT_NAME"].ToString();
                    }
                    subjectDetails.Add(subject);

                }

                return subjectDetails;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        public List<UserDetail> GetCorporateUsers(string userName, string firstName, string middleName,
            string lastName, string sortField, SortType sortOrder, int pageNumber, int pageSize,
            out int totalRecords, int tenantID, int positionProfielID, string ownerType)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                DbCommand getUsersCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_USERS");
                // Add parameters.
                HCMDatabase.AddInParameter(getUsersCommand,
                    "@USER_NAME",
                    DbType.String, userName == null || userName.Trim().Length == 0 ? null : userName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@FIRST_NAME",
                    DbType.String, firstName == null || firstName.Trim().Length == 0 ? null : firstName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@MIDDLE_NAME",
                    DbType.String, middleName == null || middleName.Trim().Length == 0 ? null : middleName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@LAST_NAME",
                    DbType.String, lastName == null || lastName.Trim().Length == 0 ? null : lastName.Trim());

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBY",
                    DbType.String, sortField);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, sortOrder == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                HCMDatabase.AddInParameter(getUsersCommand,
                   "@TENANT_ID", DbType.String, tenantID);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@POSITION_PROFILE_ID", DbType.Int32, positionProfielID);

                HCMDatabase.AddInParameter(getUsersCommand,
                    "@OWNER_TYPE", DbType.String, ownerType);


                dataReader = HCMDatabase.ExecuteReader(getUsersCommand);

                List<UserDetail> users = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (users == null)
                            users = new List<UserDetail>();

                        UserDetail userDetail = new UserDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                        {
                            userDetail.UserID = Convert.ToInt32(dataReader["USER_ID"].ToString().Trim());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                        {
                            userDetail.UserName = dataReader["USER_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        {
                            userDetail.FirstName = dataReader["FIRST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["MIDDLE_NAME"]))
                        {
                            userDetail.MiddleName = dataReader["MIDDLE_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["LAST_NAME"]))
                        {
                            userDetail.LastName = dataReader["LAST_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                        {
                            userDetail.Email = dataReader["USER_NAME"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["ASSESSOR"]))
                        {
                            userDetail.IsAssessor = dataReader["ASSESSOR"].ToString().Trim();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["IS_OWNER"]))
                        {
                            userDetail.IsOwner = dataReader["IS_OWNER"].ToString().Trim();
                        }
                        // Add to the list.
                        users.Add(userDetail);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return users;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<CandidateInformation> GetLocation(CandidateInformation candidateLocation, int pageNumber,
            int pageSize, out int totalRecords, string sortField, SortType sortType)
        {
            IDataReader dataReader = null;
            totalRecords = 0;
            try
            {
                DbCommand getLocationCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_LOCATION");
                // Add parameters.
                HCMDatabase.AddInParameter(getLocationCommand,
                    "@CITY",
                    DbType.String, candidateLocation.caiCityName == null ||
                    candidateLocation.caiCityName.Trim().Length == 0 ? null : candidateLocation.caiCityName.Trim());

                HCMDatabase.AddInParameter(getLocationCommand,
                    "@STATE",
                    DbType.String, candidateLocation.caiStateName == null ||
                    candidateLocation.caiStateName.Trim().Length == 0 ? null : candidateLocation.caiStateName.Trim());

                HCMDatabase.AddInParameter(getLocationCommand,
                    "@COUNTRY",
                    DbType.String, candidateLocation.caiCountryName == null ||
                    candidateLocation.caiCountryName.Trim().Length == 0 ? null : candidateLocation.caiCountryName.Trim());

                HCMDatabase.AddInParameter(getLocationCommand,
                    "@ORDERBY",
                    DbType.String, sortField);

                HCMDatabase.AddInParameter(getLocationCommand,
                   "@TOTAL",
                   DbType.Int32, totalRecords);

                HCMDatabase.AddInParameter(getLocationCommand,
                    "@ORDERBYDIRECTION",
                    DbType.String, sortType == SortType.Ascending ?
                    Constants.SortTypeConstants.ASCENDING :
                    Constants.SortTypeConstants.DESCENDING);

                HCMDatabase.AddInParameter(getLocationCommand,
                    "@PAGENUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getLocationCommand,
                    "@PAGESIZE", DbType.Int32, pageSize);

                dataReader = HCMDatabase.ExecuteReader(getLocationCommand);

                List<CandidateInformation> candidateInfo = null;

                while (dataReader.Read())
                {
                    if (dataReader["TOTAL"] == DBNull.Value)
                    {
                        // Instantiate the list.
                        if (candidateInfo == null)
                            candidateInfo = new List<CandidateInformation>();

                        CandidateInformation candidateInformation = new CandidateInformation();

                        if (dataReader["CITY_ID"] != null && dataReader["CITY_ID"].ToString().Trim().Length > 0)
                        {
                            candidateInformation.caiCity = Convert.ToInt32(dataReader["CITY_ID"].ToString().Trim());
                        }
                        if (dataReader["STATE_ID"] != null && dataReader["STATE_ID"].ToString().Trim().Length > 0)
                        {
                            candidateInformation.staID = Convert.ToInt32(dataReader["STATE_ID"].ToString().Trim());
                        }
                        if (dataReader["COUNTRY_ID"] != null && dataReader["COUNTRY_ID"].ToString().Trim().Length > 0)
                        {
                            candidateInformation.couID = Convert.ToInt32(dataReader["COUNTRY_ID"].ToString().Trim());
                        }

                        if (dataReader["CITY"] != null)
                        {
                            candidateInformation.caiCityName = dataReader["CITY"].ToString().Trim();
                        }

                        if (dataReader["STATE"] != null)
                        {
                            candidateInformation.caiStateName = dataReader["STATE"].ToString().Trim();
                        }

                        if (dataReader["COUNTRY"] != null)
                        {
                            candidateInformation.caiCountryName = dataReader["COUNTRY"].ToString().Trim();
                        }

                        candidateInformation.caiLocation = candidateInformation.caiCityName + "," + candidateInformation.caiStateName + "," +
                            candidateInformation.caiCountryName;
                        // Add to the list.
                        candidateInfo.Add(candidateInformation);
                    }
                    else
                    {
                        totalRecords = Convert.ToInt32(dataReader["TOTAL"]);
                    }
                }

                return candidateInfo;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public UserDetail GetSubscriptionUserDetail(int userID)
        {
            UserDetail userDetail = null;

            IDataReader dataReader = null;

            try
            {
                DbCommand getPageControlsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_SUBSCRIPTION_USERS");

                // Add parameters.
                HCMDatabase.AddInParameter(getPageControlsCommand,
                    "@USER_ID", DbType.Int32, userID);

                // Execute the procedure.
                dataReader = HCMDatabase.ExecuteReader(getPageControlsCommand);

                if (dataReader.Read())
                {
                    // Instantiate user detail object.
                    userDetail = new UserDetail();

                    if (dataReader["USRID"] != null && dataReader["USRID"].ToString().Trim().Length > 0)
                    {
                        userDetail.UserID = Convert.ToInt32(dataReader["USRID"].ToString().Trim());
                    }

                    if (dataReader["USRUSERNAME"] != null)
                    {
                        userDetail.UserName = dataReader["USRUSERNAME"].ToString().Trim();
                    }

                    if (dataReader["USRFIRSTNAME"] != null)
                    {
                        userDetail.FirstName = dataReader["USRFIRSTNAME"].ToString().Trim();
                    }

                    if (dataReader["USRLASTNAME"] != null)
                    {
                        userDetail.LastName = dataReader["USRLASTNAME"].ToString().Trim();
                    }
                    if (dataReader["PHONE"] != null)
                    {
                        userDetail.Phone = dataReader["PHONE"].ToString().Trim();
                    }
                    if (dataReader["COMPANY"] != null)
                    {
                        userDetail.Company = dataReader["COMPANY"].ToString().Trim();
                    }
                    if (dataReader["TITLE"] != null)
                    {
                        userDetail.Title = dataReader["TITLE"].ToString().Trim();
                    }
                    if (dataReader["EXPIRY_DATE"] != null)
                    {
                        if (dataReader["EXPIRY_DATE"].ToString() == "")
                            userDetail.ExpiryDate = Convert.ToDateTime(string.Empty);
                        else
                            userDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());
                    }
                    if (dataReader["LEGAL_ACCEPTED"] != null)
                    {
                        if (dataReader["LEGAL_ACCEPTED"].ToString() == "Y")
                            userDetail.LegalAccepted = true;
                        else
                            userDetail.LegalAccepted = false;
                    }
                    if (dataReader["SUBSCRIPTION_NAME"] != null)
                    {
                        userDetail.SubscriptionType = dataReader["SUBSCRIPTION_NAME"].ToString().Trim() + " subscription";
                    }
                    if (dataReader["BUSINESS_TYPE"] != null)
                    {
                        userDetail.BussinessType = dataReader["BUSINESS_TYPE"].ToString().Trim();
                    }
                    if (dataReader["IS_ACTIVE"] != null)
                    {
                        if (Convert.ToInt32(dataReader["IS_ACTIVE"]) == 1)
                            userDetail.IsActive = true;
                        else
                            userDetail.IsActive = false;
                    }
                }
                return userDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets skill for assessor .
        /// </summary>
        /// <param name="prefixKeyword">The prefix keyword.</param>
        /// <returns></returns>
        public string[] GetSkillSearch(string prefixKeyword)
        {
            IDataReader dataReader = null;
            try
            {
                List<string> skills = null;
                DbCommand getCandidateSkill = HCMDatabase.GetStoredProcCommand("SPGET_SKILL_KEYWORDS");
                HCMDatabase.AddInParameter(getCandidateSkill, "@PREFIX_TEXT", DbType.String, prefixKeyword);

                dataReader = HCMDatabase.ExecuteReader(getCandidateSkill);
                // Check if datareader contains not equal to null
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        if (skills == null)
                            skills = new List<string>();
                        skills.Add(dataReader["SKILL_NAME"].ToString());
                    }
                    dataReader.Close();
                }
                if (!Support.Utility.IsNullOrEmpty(skills) && skills.Count > 0)
                    return skills.ToArray();
                else
                    return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that flush all data.
        /// </summary>
        public void FlushData()
        {
            DbCommand flushDataCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ALL_DATA");

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(flushDataCommand);
        }

        /// <summary>
        /// Method that flush all data by category Id.
        /// </summary>
        public void FlushData(int categoryId)
        {
            DbCommand flushDataCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_ALL_DATA_BY_CATEGORY");

            HCMDatabase.AddInParameter(flushDataCommand,
                "@CAT_ID", DbType.Int32, categoryId);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(flushDataCommand);
        }
    }
}
