﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdminDLManager.cs
// File that represents the data layer for the admin module.
// This will talk to the database to perform the operations associated
// with the module.

#endregion

#region Directives

using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Data.Common;
using System.Drawing.Imaging;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the admin module.
    /// This includes functionalities for retrieving and updating settings 
    /// values for question, competency vector, credit management, options 
    /// etc. This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class AdminDLManager : DatabaseConnectionManager
    {
        #region Public Methods

        /// <summary>
        /// Method that updates the admin settings.
        /// </summary>
        /// <param name="adminSettings">
        /// A <see cref="AdminSettings"/> that holds the admin settings;
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the user.
        /// </param>
        public void UpdateAdminSettings(AdminSettings adminSettings, int user)
        {
            // Create a stored procedure command object
            DbCommand updateAdminSettingsCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_ADMIN_SETTINGS");

            // Add input parameters
            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@COMPUTATION_FACTOR", DbType.Double, adminSettings.ComputationFactor);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@MAX_RECENCY_FACTOR", DbType.Int32, adminSettings.MaximumRecencyFactor);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@GRID_PAGE_SIZE", DbType.Int32, adminSettings.GridPageSize);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@TALENT_SCOUT_SPHERES", DbType.Int32, adminSettings.NoOfSphere);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@MODIFIED_BY", DbType.Int32, user);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@COMPLEXITY_SIMPLE", DbType.Double, adminSettings.ComplexitySimple);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@COMPLEXITY_MEDIUM", DbType.Double, adminSettings.ComplexityMedium);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@COMPLEXITY_COMPLEX", DbType.Double, adminSettings.ComplexityComplex);

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateAdminSettingsCommand);
        }

        /// <summary>
        /// Method that updates the candidate admin settings.
        /// </summary>
        /// <param name="adminSettings">
        /// A <see cref="CandidateAdminSettings"/> that holds the candidate 
        /// admin settings.
        /// </param>
        /// <param name="user">
        /// A <see cref="int"/> that holds the user.
        /// </param>
        public void UpdateCandidateAdminSettings(CandidateAdminSettings adminSettings, int user)
        {
            // Create a stored procedure command object
            DbCommand updateAdminSettingsCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CANDIDATE_ADMIN_SETTINGS");

            // Add input parameters
            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@MAX_SELF_ADMIN_TEST_RETAKES", DbType.Int32, adminSettings.MaxSelfAdminTestRetakes);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@MAX_SELF_ADMIN_TEST_PER_MONTH", DbType.Int32, adminSettings.MaxSelfAdminTestPerMonth);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
               "@CANDIDATE_SESSIONS_EXPIRY_DAYS", DbType.Int32, adminSettings.CandidateSessionExpiryInDays);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@RESUME_EXPIRY_DAYS", DbType.Int32, adminSettings.ResumeExpiryDays);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@ACTIVITIES_GRID_PAGE_SIZE", DbType.Int32, adminSettings.ActivitiesGridPageSize);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@SEARCH_TEST_GRID_PAGE_SIZE", DbType.Int32, adminSettings.SearchTestGridPageSize);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
               "@MIN_QUESTION_PER_SELF_ADMIN_TEST", DbType.Int32, adminSettings.MinQuestionPerSelfAdminTest);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
               "@MAX_QUESTION_PER_SELF_ADMIN_TEST", DbType.Int32, adminSettings.MaxQuestionPerSelfAdminTest);

            HCMDatabase.AddInParameter(updateAdminSettingsCommand,
                "@MODIFIED_BY", DbType.Int32, user);

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateAdminSettingsCommand);
        }

        /// <summary>
        /// Method that will insert the certificate details.
        /// </summary>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationDetail"/> that contains the certification detail instance.
        /// </param>
        /// <param name="userid">
        /// An <see cref="int"/> that contains the userid.
        /// </param>
        public void InsertCertificate(CertificationDetail certificationDetail, int userid)
        {
            // Create a stored procedure command object
            DbCommand insertCertificateCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CERTIFICATE");

            // Add input parameters
            HCMDatabase.AddInParameter(insertCertificateCommand,
                "@CERTIFICATE_NAME", DbType.String, certificationDetail.CertificateName);

            HCMDatabase.AddInParameter(insertCertificateCommand,
                "@CERTIFICATE_DESCRIPTION", DbType.String, certificationDetail.CertificateDesc);

            HCMDatabase.AddInParameter(insertCertificateCommand,
                "@HTML_TEXT", DbType.String, certificationDetail.HtmlText);

            HCMDatabase.AddInParameter(insertCertificateCommand,
                "@CREATED_BY", DbType.Int32, userid);

            HCMDatabase.AddInParameter(insertCertificateCommand,
                "@MODIFIED_BY", DbType.Int32, userid);

            HCMDatabase.AddInParameter(insertCertificateCommand,
                "@IMAGEDATA", DbType.Binary, certificationDetail.ImageData);

            // Execute command
            HCMDatabase.ExecuteNonQuery(insertCertificateCommand);
        }

        /// <summary>
        /// Method that will update the certificate information.
        /// </summary>
        /// <param name="certificationDetail">
        /// A <see cref="CertificationDetail"/> that contains the certificate details instance.
        /// </param>
        /// <param name="userid">
        /// An <see cref="int"/> that contains the userid.
        /// </param>
        /// <param name="certificateID">
        /// An <see cref="int"/> that contains the certificate id.
        /// </param>
        public void UpdateCertificate(CertificationDetail certificationDetail, int userid, int certificateID)
        {
            // Create a stored procedure command object
            DbCommand updateCertificateCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CERTIFICATE");

            // Add input parameters
            HCMDatabase.AddInParameter(updateCertificateCommand,
                "@CERTIFICATE_ID", DbType.String, certificateID);

            HCMDatabase.AddInParameter(updateCertificateCommand,
                "@CERTIFICATE_NAME", DbType.String, certificationDetail.CertificateName);

            HCMDatabase.AddInParameter(updateCertificateCommand,
                "@CERTIFICATE_DESCRIPTION", DbType.String, certificationDetail.CertificateDesc);

            HCMDatabase.AddInParameter(updateCertificateCommand,
                "@HTML_TEXT", DbType.String, certificationDetail.HtmlText);

            HCMDatabase.AddInParameter(updateCertificateCommand,
                "@MODIFIED_BY", DbType.Int32, userid);

            HCMDatabase.AddInParameter(updateCertificateCommand,
                "@IMAGE_DATA", DbType.Binary, certificationDetail.ImageData);

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateCertificateCommand);
        }

        /// <summary>
        /// Method that will return the certitication details.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CertificationDetail"/> that contains the certification details.
        /// </returns>
        public List<CertificationDetail> GetCertificateDetails()
        {
            IDataReader dataReader = null;
            List<CertificationDetail> certificationDetails = null;
            try
            {
                DbCommand getCertificationCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CERTIFICATE_DETAILS");

                dataReader = HCMDatabase.ExecuteReader(getCertificationCommand);

                while (dataReader.Read())
                {
                    if (certificationDetails == null)
                        certificationDetails = new List<CertificationDetail>();

                    CertificationDetail certificationDetail = new CertificationDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_ID"]))
                        certificationDetail.CertificateID =
                            Convert.ToInt32(dataReader["CERTIFICATE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_NAME"]))
                        certificationDetail.CertificateName = dataReader["CERTIFICATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["DESCRIPTION"]))
                        certificationDetail.CertificateDesc = dataReader["DESCRIPTION"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["HTML_DATA"]))
                        certificationDetail.HtmlText = dataReader["HTML_DATA"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["IMAGE_DATA"]))
                        certificationDetail.ImageData = dataReader["IMAGE_DATA"] as byte[];
                    certificationDetails.Add(certificationDetail);
                }
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return certificationDetails;
        }

        /// <summary>
        /// Method that will return the certitication details based on the certificate id
        /// </summary>
        /// <param name="certificateID">
        /// A <see cref="int"/>that contains the certificate id. 
        /// </param>
        /// <returns>
        /// A list of <see cref="CertificationDetail"/> that contains the certification details.
        /// </returns>
        public CertificationDetail GetCertificateDetails(int certificateID)
        {
            IDataReader dataReader = null;
            CertificationDetail certificationDetail = null;

            try
            {
                DbCommand getCertificationCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CERTIFICATE_DETAILS_BY_ID");

                HCMDatabase.AddInParameter(getCertificationCommand,
                    "@CERTIFICATE_ID", DbType.Int32, certificateID);

                dataReader = HCMDatabase.ExecuteReader(getCertificationCommand);

                while (dataReader.Read())
                {
                    if (certificationDetail == null)
                        certificationDetail = new CertificationDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_ID"]))
                        certificationDetail.CertificateID =
                            Convert.ToInt32(dataReader["CERTIFICATE_ID"].ToString());

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_NAME"]))
                        certificationDetail.CertificateName = dataReader["CERTIFICATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["DESCRIPTION"]))
                        certificationDetail.CertificateDesc = dataReader["DESCRIPTION"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["HTML_DATA"]))
                        certificationDetail.HtmlText = dataReader["HTML_DATA"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["IMAGE_DATA"]))
                        certificationDetail.ImageData = dataReader["IMAGE_DATA"] as byte[];
                }
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return certificationDetail;
        }

        /// <summary>
        /// Method that returns the certificate image as bytes.
        /// </summary>
        /// <param name="candidateSessionKey">
        /// A <see cref="string"/> that contains the candidate session key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that contains the attempt id.
        /// </param>
        /// <returns>
        /// Certificate Details in Byte[]
        /// </returns>
        public byte[] GetCertificateImage(string candidateSessionKey, int attemptID)
        {
            IDataReader dataReader = null;
            byte[] imageData = null;
            try
            {
                DbCommand getCertificateImageCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_CERTIFICATE_IMAGE");

                // Add input parameters
                HCMDatabase.AddInParameter(getCertificateImageCommand,
                    "@CAND_SESSION_KEY", DbType.String, candidateSessionKey);
                HCMDatabase.AddInParameter(getCertificateImageCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);
                dataReader = HCMDatabase.ExecuteReader(getCertificateImageCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_IMAGE"]))
                        imageData = dataReader["CERTIFICATE_IMAGE"] as byte[];
                }
                dataReader.Close();

                return imageData;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that returns the test certification details instance.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that contains the test key.
        /// </param>
        /// <returns>
        /// A <see cref="CertificationDetail"/> that contains the certification details.
        /// </returns>
        public CertificationDetail GetTestCertificationDetails(string testKey)
        {
            IDataReader dataReader = null;
            CertificationDetail certificationDetail = null;

            try
            {
                DbCommand getTestCertificationCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_CERTIFICATE_DETAILS_BY_TESTKEY");

                // Add input parameter
                HCMDatabase.AddInParameter(getTestCertificationCommand, "@TEST_KEY", DbType.String, testKey);
                dataReader = HCMDatabase.ExecuteReader(getTestCertificationCommand);

                while (dataReader.Read())
                {
                    certificationDetail = new CertificationDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_TYPE"]))
                        certificationDetail.CertificateType = dataReader["CERTIFICATE_TYPE"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["QUALIFICATION_SCORE"]))
                        certificationDetail.MinimumTotalScoreRequired =
                            Convert.ToDecimal(dataReader["QUALIFICATION_SCORE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_TIME_PERM"]))
                        certificationDetail.MaximumTimePermissible =
                            Convert.ToInt32(dataReader["MAXIMUM_TIME_PERM"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_VALID_ID"]))
                        certificationDetail.CertificateValidity =
                            dataReader["CERTIFICATION_VALID_ID"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_EXPIRED_ON"]))
                        certificationDetail.CertificateExpiredOn =
                            dataReader["CERTIFICATE_EXPIRED_ON"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_RETAKES"]))
                        certificationDetail.PermissibleRetakes = Convert.ToInt32(dataReader["NO_OF_RETAKES"]);

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_FOR_NO_OF_RETAKES"]))
                        certificationDetail.MaximumTimePermissible =
                            Convert.ToInt32(dataReader["TIME_FOR_NO_OF_RETAKES"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_FORMAT_ID"]))
                        certificationDetail.CertificateFormatID =
                            Convert.ToInt32(dataReader["CERTIFICATE_FORMAT_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_ID"]))
                        certificationDetail.CertificateID = Convert.ToInt32(dataReader["CERTIFICATE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_NAME"]))
                        certificationDetail.CertificateName =
                             dataReader["CERTIFICATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_DESCRIPTION"]))
                        certificationDetail.CertificateDesc =
                            dataReader["CERTIFICATE_DESCRIPTION"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["HTML_TEXT"]))
                        certificationDetail.HtmlText = dataReader["HTML_TEXT"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_IMAGE"]))
                        certificationDetail.ImageData = dataReader["CERTIFICATE_IMAGE"] as byte[];
                }

                return certificationDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that returns the certification details for the given 
        /// candidate session ID.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="CertificationDetail"/> that holds the certification details.
        /// </returns>
        public CertificationDetail GetCertificateByCandidateSession
            (string candidateSessionID, int attemptID)
        {
            IDataReader dataReader = null;
            CertificationDetail certificationDetail = null;

            try
            {
                DbCommand getCertificateCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TEST_CERTIFICATE_DETAILS_BY_CANDIDATE_SESSION_KEY");

                // Add input parameter
                HCMDatabase.AddInParameter(getCertificateCommand, 
                    "@CAND_SESSION_KEY", DbType.String, candidateSessionID);

                HCMDatabase.AddInParameter(getCertificateCommand,
                    "@ATTEMPT_ID", DbType.Int32, attemptID);

                // Execute the query.
                dataReader = HCMDatabase.ExecuteReader(getCertificateCommand);

                if (dataReader.Read())
                {
                    certificationDetail = new CertificationDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_TYPE"]))
                        certificationDetail.CertificateType = dataReader["CERTIFICATE_TYPE"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["QUALIFICATION_SCORE"]))
                        certificationDetail.MinimumTotalScoreRequired =
                            Convert.ToDecimal(dataReader["QUALIFICATION_SCORE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["MAXIMUM_TIME_PERM"]))
                        certificationDetail.MaximumTimePermissible =
                            Convert.ToInt32(dataReader["MAXIMUM_TIME_PERM"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATION_VALID_ID"]))
                        certificationDetail.CertificateValidity =
                            dataReader["CERTIFICATION_VALID_ID"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_EXPIRED_ON"]))
                        certificationDetail.CertificateExpiredOn =
                            dataReader["CERTIFICATE_EXPIRED_ON"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["NO_OF_RETAKES"]))
                        certificationDetail.PermissibleRetakes = Convert.ToInt32(dataReader["NO_OF_RETAKES"]);

                    if (!Utility.IsNullOrEmpty(dataReader["TIME_FOR_NO_OF_RETAKES"]))
                        certificationDetail.MaximumTimePermissible =
                            Convert.ToInt32(dataReader["TIME_FOR_NO_OF_RETAKES"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_FORMAT_ID"]))
                        certificationDetail.CertificateFormatID =
                            Convert.ToInt32(dataReader["CERTIFICATE_FORMAT_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_ID"]))
                        certificationDetail.CertificateID = Convert.ToInt32(dataReader["CERTIFICATE_ID"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_NAME"]))
                        certificationDetail.CertificateName =
                             dataReader["CERTIFICATE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_DESCRIPTION"]))
                        certificationDetail.CertificateDesc =
                            dataReader["CERTIFICATE_DESCRIPTION"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["HTML_TEXT"]))
                        certificationDetail.HtmlText = dataReader["HTML_TEXT"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["CERTIFICATE_IMAGE"]))
                        certificationDetail.ImageData = dataReader["CERTIFICATE_IMAGE"] as byte[];

                    if (!Utility.IsNullOrEmpty(dataReader["DATE_COMPLETED"]))
                        certificationDetail.CompletedDate = Convert.ToDateTime(dataReader["DATE_COMPLETED"]);
                }

                return certificationDetail;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the admin settings.
        /// </summary>
        /// <returns>
        /// A <see cref="AdminSettings"/> that holds the admin settings.
        /// </returns>
        public AdminSettings GetAdminSettings()
        {
            IDataReader dataReader = null;
            AdminSettings adminSettings = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getAdminSettingsCommand = HCMDatabase.GetStoredProcCommand("SPGET_ADMIN_SETTINGS");

                dataReader = HCMDatabase.ExecuteReader(getAdminSettingsCommand);

                if (dataReader.Read())
                {
                    // Construct the admin settings object.
                    adminSettings = new AdminSettings();

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_SIMPLE"]))
                        adminSettings.ComplexitySimple = Convert.ToDouble(dataReader["COMPLEXITY_SIMPLE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_MEDIUM"]))
                        adminSettings.ComplexityMedium = Convert.ToDouble(dataReader["COMPLEXITY_MEDIUM"]);

                    if (!Utility.IsNullOrEmpty(dataReader["COMPLEXITY_COMPLEX"]))
                        adminSettings.ComplexityComplex = Convert.ToDouble(dataReader["COMPLEXITY_COMPLEX"]);

                    if (!Utility.IsNullOrEmpty(dataReader["COMPUTATION_FACTOR"]))
                        adminSettings.ComputationFactor = Convert.ToDouble(dataReader["COMPUTATION_FACTOR"]);

                    if (!Utility.IsNullOrEmpty(dataReader["MAX_RECENCY_FACTOR"]))
                        adminSettings.MaximumRecencyFactor = Convert.ToInt32(dataReader["MAX_RECENCY_FACTOR"]);

                    if (!Utility.IsNullOrEmpty(dataReader["GRID_PAGE_SIZE"]))
                        adminSettings.GridPageSize = Convert.ToInt32(dataReader["GRID_PAGE_SIZE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["TALENT_SCOUT_SPHERES"]))
                        adminSettings.NoOfSphere = Convert.ToInt32(dataReader["TALENT_SCOUT_SPHERES"]);
                }

                dataReader.NextResult();

                AttributeDetail AttributeDetail = null;

                while (dataReader.Read())
                {
                    AttributeDetail = new AttributeDetail();
                    if (Utility.IsNullOrEmpty(adminSettings.ProximityFactor))
                        adminSettings.ProximityFactor = new List<AttributeDetail>();
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_ID"]))
                        AttributeDetail.AttributeID = dataReader["ATTRIBUTE_ID"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_NAME"]))
                        AttributeDetail.AttributeName = dataReader["ATTRIBUTE_NAME"].ToString().Trim();
                    if (!Utility.IsNullOrEmpty(dataReader["ATTRIBUTE_VALUE"]))
                        AttributeDetail.AttributeValue = dataReader["ATTRIBUTE_VALUE"].ToString().Trim();
                    adminSettings.ProximityFactor.Add(AttributeDetail);
                }
                dataReader.Close();

                return adminSettings;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the candidate admin settings.
        /// </summary>
        /// <returns>
        /// A <see cref="CandidateAdminSettings"/> that holds the candidate 
        /// admin settings.
        /// </returns>
        public CandidateAdminSettings GetCandidateAdminSettings()
        {
            IDataReader dataReader = null;
            CandidateAdminSettings adminSettings = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getAdminSettingsCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_CANDIDATE_ADMIN_SETTINGS");

                dataReader = HCMDatabase.ExecuteReader(getAdminSettingsCommand);

                if (dataReader.Read())
                {
                    // Construct the candidate admin settings object.
                    adminSettings = new CandidateAdminSettings();

                    if (!Utility.IsNullOrEmpty(dataReader["MAX_SELF_ADMIN_TEST_RETAKES"]))
                        adminSettings.MaxSelfAdminTestRetakes = Convert.ToInt32(dataReader["MAX_SELF_ADMIN_TEST_RETAKES"]);

                    if (!Utility.IsNullOrEmpty(dataReader["MAX_SELF_ADMIN_TEST_PER_MONTH"]))
                        adminSettings.MaxSelfAdminTestPerMonth = Convert.ToInt32(dataReader["MAX_SELF_ADMIN_TEST_PER_MONTH"]);

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_SESSIONS_EXPIRY_DAYS"]))
                        adminSettings.CandidateSessionExpiryInDays = Convert.ToInt32(dataReader["CANDIDATE_SESSIONS_EXPIRY_DAYS"]);

                    if (!Utility.IsNullOrEmpty(dataReader["RESUME_EXPIRY_DAYS"]))
                        adminSettings.ResumeExpiryDays = Convert.ToInt32(dataReader["RESUME_EXPIRY_DAYS"]);

                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVITIES_GRID_PAGE_SIZE"]))
                        adminSettings.ActivitiesGridPageSize = Convert.ToInt32(dataReader["ACTIVITIES_GRID_PAGE_SIZE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["SEARCH_TEST_GRID_PAGE_SIZE"]))
                        adminSettings.SearchTestGridPageSize = Convert.ToInt32(dataReader["SEARCH_TEST_GRID_PAGE_SIZE"]);

                    if (!Utility.IsNullOrEmpty(dataReader["MIN_QUESTION_PER_SELF_ADMIN_TEST"]))
                        adminSettings.MinQuestionPerSelfAdminTest = Convert.ToInt32(dataReader["MIN_QUESTION_PER_SELF_ADMIN_TEST"]);

                    if (!Utility.IsNullOrEmpty(dataReader["MAX_QUESTION_PER_SELF_ADMIN_TEST"]))
                        adminSettings.MaxQuestionPerSelfAdminTest = Convert.ToInt32(dataReader["MAX_QUESTION_PER_SELF_ADMIN_TEST"]);
                }

                dataReader.Close();

                return adminSettings;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of categories.
        /// </summary>
        /// <returns>
        /// A list of <see cref="Category"/> object that holds the categories.
        /// </returns>
        public List<Category> GetCategories(int tenantID)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_ALL_CATEGORIES");

                HCMDatabase.AddInParameter(getCategoryCommand,
                  "@TENANT_ID", DbType.Int32, tenantID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCategoryCommand);

                List<Category> categories = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the categories collection.
                    if (categories == null)
                        categories = new List<Category>();

                    // Create a new category object.
                    Category category = new Category();

                    // Assign property values to the category object.
                    category.CategoryID = int.Parse
                        (dataReader["CAT_KEY"].ToString().Trim());

                    category.CategoryName = dataReader["CATEGORY_NAME"]
                        .ToString().Trim();

                    // Add the category to the collection.
                    categories.Add(category);
                }

                return categories;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that insert a new category into the database.
        /// </summary>
        /// <param name="category">
        /// A <see cref="Category"/> object that holds the category details.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> object that holds the user ID.
        /// </param>
        public void InsertCategory(Category category, int userID, int tenantID)
        {
            // Create a stored procedure command object.
            DbCommand insertCategoryCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_CATEGORY");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCategoryCommand,
                "@CATEGORY_NAME", DbType.String, category.CategoryName);
            HCMDatabase.AddInParameter(insertCategoryCommand,
                "@TENANT_ID", DbType.Int32, tenantID);
            HCMDatabase.AddInParameter(insertCategoryCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertCategoryCommand);
        }

        /// <summary>
        /// Method that updates a category into the database.
        /// </summary>
        /// <param name="category">
        /// A <see cref="Category"/> object that holds the category details.
        /// </param>
        public void UpdateCategory(Category category)
        {
            // Create a stored procedure command object.
            DbCommand updateCategoryCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_CATEGORY");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCategoryCommand,
                "@CAT_KEY", DbType.Int32, category.CategoryID);
            HCMDatabase.AddInParameter(updateCategoryCommand,
                "@CATEGORY_NAME", DbType.String, category.CategoryName);
            HCMDatabase.AddInParameter(updateCategoryCommand,
                "@MODIFIED_BY", DbType.Int32, category.ModifiedBy);
            HCMDatabase.AddInParameter(updateCategoryCommand,
                "@MODIFIED_DATE", DbType.DateTime, category.ModifiedDate);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCategoryCommand);
        }

        /// <summary>
        /// Method that deletes a category from the database.
        /// </summary>
        /// <param name="categoryKey">
        /// An <see cref="int"/> that holds the category ID to be deleted.
        /// </param>
        public void DeleteCategory(int categoryKey)
        {
            // Create a stored procedure command object.
            DbCommand deleteCategoryCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_CATEGORY");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteCategoryCommand,
                "@CATEGORY_KEY", DbType.Int32, categoryKey);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteCategoryCommand);
        }

        /// <summary>
        /// Method that retrieves the list of subject for the given 
        /// category ID.
        /// </summary>
        /// <returns>
        /// A list of <see cref="Subject"/> object that holds the subject.
        /// </returns>
        public List<Subject> GetSubjects(int CategoryID)
        {
            IDataReader dataReader = null;

            try
            {
                // Create a stored procedure command object.
                DbCommand getCategoryCommand = HCMDatabase.
                    GetStoredProcCommand("SPGET_SUBJECTS_CAT_ID");

                // Add input parameters.
                HCMDatabase.AddInParameter(getCategoryCommand,
                    "@CATEGORY_ID", DbType.Int32, CategoryID);

                // Execute the stored procedure.
                dataReader = HCMDatabase.ExecuteReader(getCategoryCommand);

                List<Subject> subjects = null;

                // Read the records from the data reader.
                while (dataReader.Read())
                {
                    // Instantiate the subjects collection.
                    if (subjects == null)
                        subjects = new List<Subject>();

                    // Create a new subject object. 
                    Subject subject = new Subject();

                    // Assign property values to the subject object.
                    subject.SubjectID = int.Parse
                        (dataReader["CAT_SUB_ID"].ToString().Trim());

                    subject.SubjectName = dataReader["SUBJECT_NAME"]
                        .ToString().Trim();

                    // Add the subject to the collection.
                    subjects.Add(subject);
                }
                return subjects;
            }
            finally
            {
                // Close the data reader, if it is open.
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that insert a new subject into the database.
        /// </summary>
        /// <param name="subject">
        /// A <see cref="Subject"/> object that holds the subject details.
        /// </param>
        public void InsertSubject(Subject subject, int tenantID)
        {
            // Create a stored procedure command object.
            DbCommand insertSubjectCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_SUBJECT");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertSubjectCommand,
                "@CATEGORY_ID", DbType.Int32, subject.CategoryID);
            HCMDatabase.AddInParameter(insertSubjectCommand,
                "@TENANT_ID", DbType.Int32, tenantID);
            HCMDatabase.AddInParameter(insertSubjectCommand,
                "@SUBJECT_NAME", DbType.String, subject.SubjectName);
            HCMDatabase.AddInParameter(insertSubjectCommand,
                "@USER_ID", DbType.Int32, subject.CreatedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertSubjectCommand);
        }

        /// <summary>
        /// Method that checks if the given subject ID used in questions or
        /// contributor summary.
        /// </summary>
        /// <param name="subjectID">
        /// A <see cref="string"/> that holds the subject ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the QUESTION_RELATION and 
        /// CONTRIBUTOR_CATEGORY_SUBJECT tables.
        /// </remarks>
        public bool IsSubjectUsed(int subjectID)
        {
            string status = string.Empty;

            // Checks whether the subject ID is associated with
            // question or contributor summary.
            DbCommand checkSubjectAttributeCommand =
                HCMDatabase.GetStoredProcCommand("SPCHECK_SUBJECT");

            HCMDatabase.AddInParameter(checkSubjectAttributeCommand,
                "@CAT_SUB_ID", DbType.Int32, subjectID);
            HCMDatabase.AddOutParameter(checkSubjectAttributeCommand,
                "@RELATION_ID", DbType.Int32, 32);
            HCMDatabase.AddOutParameter(checkSubjectAttributeCommand,
                "@CON_CATSUB_GENID", DbType.Int32, 32);

            HCMDatabase.ExecuteNonQuery(checkSubjectAttributeCommand);

            //Checks if the subject id is related to questions
            if (!(Utility.IsNullOrEmpty(checkSubjectAttributeCommand.
                Parameters["@RELATION_ID"].Value)))
            {
                return true;
            }

            //Checks if the subject is related to the contributor 
            if (!(Utility.IsNullOrEmpty(checkSubjectAttributeCommand.
                Parameters["@CON_CATSUB_GENID"].Value)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method that deletes a subject from the database.
        /// </summary>
        /// <param name="subjectID">
        /// An <see cref="int"/> that holds the subject ID to be deleted.
        /// </param>
        public void DeleteSubject(int subjectID)
        {
            // Create a stored procedure command object.
            DbCommand deleteSubjectCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_SUBJECT");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteSubjectCommand,
                "@CAT_SUB_ID", DbType.Int32, subjectID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteSubjectCommand);
        }

        /// <summary>
        /// Method that checks whether the subjects already exists
        /// </summary>
        /// <param name="categoryID">
        /// A<see cref="int"/>that holds the category id
        /// </param>
        /// <param name="subjectID">
        /// A<see cref="int"/>that holds the subject id
        /// </param>
        /// <param name="subjectName">
        /// A<see cref="string"/>that holds the subject Name
        /// </param>
        /// <returns>
        /// A<see cref="int"/>that represents whether 
        /// the subject exists or not
        /// </returns>
        public int CheckSubject(int categoryID, int subjectID, string subjectName)
        {
            DbCommand checkSubjectCommand = HCMDatabase.
                GetStoredProcCommand("SPCHECK_CATEGORYSUBJECT");

            HCMDatabase.AddInParameter(checkSubjectCommand,
                "@CATEGORY_ID", DbType.Int32, categoryID);

            HCMDatabase.AddInParameter(checkSubjectCommand,
                "@SUBJECT_ID", DbType.Int32, subjectID);

            HCMDatabase.AddInParameter(checkSubjectCommand,
                "@SUBJECT_NAME", DbType.String, subjectName);

            int numberOfRecords = int.Parse
                (HCMDatabase.ExecuteScalar(checkSubjectCommand).ToString());

            return numberOfRecords;
        }

        /// <summary>
        ///  Method that is used to insert the test area 
        /// into the database
        /// </summary>
        /// <param name="testAreaId">
        /// A<see cref="string"/>that holds the test area Id
        /// </param>
        /// <param name="testAreaName">
        /// A<see cref="string"/>that holds the test area name
        /// </param>
        /// <param name="userID">
        /// A<see cref="int"/>that holds the test user id
        /// </param>
        /// <param name="transaction">
        /// A<see cref="IDbTransaction"/>that holds the transaction object
        /// </param>
        public void InsertTestArea(string testAreaId, string testAreaName, int tenantID, int userID,
            IDbTransaction transaction)
        {
            try
            {
                DbCommand insertTestAreaCommand = HCMDatabase.
                        GetStoredProcCommand("SPINSERT_ATTRIBUTE_TESTAREA");

                HCMDatabase.AddInParameter(insertTestAreaCommand, "@ATTRIBUTE_ID", DbType.String, testAreaId);
                HCMDatabase.AddInParameter(insertTestAreaCommand, "@ATTRIBUTE_TYPE", DbType.String, "TEST_AREA");
                HCMDatabase.AddInParameter(insertTestAreaCommand, "@ATTRIBUTE_NAME", DbType.String, testAreaName);
                HCMDatabase.AddInParameter(insertTestAreaCommand, "@TENANT_ID", DbType.Int32, tenantID);
                HCMDatabase.AddInParameter(insertTestAreaCommand, "@USER_ID", DbType.Int32, userID);

                HCMDatabase.ExecuteNonQuery(insertTestAreaCommand, transaction as DbTransaction);

                transaction.Commit();
            }
            catch (Exception exception)
            {
                transaction.Rollback();
                throw exception;
            }
        }

        /// <summary>
        /// Deelete the test Area from DB.
        /// </summary>
        /// <param name="testAreaID">
        ///  A<see cref="string"/>that holds the testAreaID
        /// </param>
        public void DeleteTestArea(string testAreaID)
        {
            DbCommand deleteTestAreaCommand = HCMDatabase.
                       GetStoredProcCommand("SPDELETE_TESTAREA");

            HCMDatabase.AddInParameter(deleteTestAreaCommand,
                "@ATTRIBUTE_ID", DbType.String, testAreaID);

            HCMDatabase.ExecuteNonQuery(deleteTestAreaCommand);
        }

        /// <summary>
        /// Method that retrieves the list of competency vector group.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AdminCompetencyVectors"/> that holds the 
        /// Vector group attributes..
        /// </returns>
        public List<CompetencyVectorGroup> GetCompetencyVectorGroup()
        {
            IDataReader dataReader = null;
            List<CompetencyVectorGroup> competencyVectorList = null;

            try
            {
                DbCommand vectorCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_COMPETENCY_VECTOR_GROUP");
                dataReader = HCMDatabase.ExecuteReader(vectorCommand);

                CompetencyVectorGroup competencyVectorDetail;
                while (dataReader.Read())
                {
                    // Instantiate the competency Vector List collection.
                    if (competencyVectorList == null)
                        competencyVectorList = new List<CompetencyVectorGroup>();

                    // Instantiate the competency Vector detail.
                    competencyVectorDetail = new CompetencyVectorGroup();

                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_ID"]))
                    {
                        competencyVectorDetail.GroupID =
                            Convert.ToInt32(dataReader["GROUP_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["GROUP_NAME"]))
                    {
                        competencyVectorDetail.GroupName =
                            dataReader["GROUP_NAME"].ToString().Trim();
                    }

                    // Add the attribute to the collection.
                    competencyVectorList.Add(competencyVectorDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return competencyVectorList;
        }

        /// <summary>
        /// Method that retrieves the list of competency vector name based on group id.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AdminCompetencyVectors"/> that holds the 
        /// Vector attributes..
        /// </returns>
        public List<CompetencyVector> GetCompetencyVector(int groupID)
        {
            IDataReader dataReader = null;
            List<CompetencyVector> competencyVectorList = null;

            try
            {
                DbCommand vectorCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_COMPETENCY_VECTOR");
                HCMDatabase.AddInParameter(vectorCommand,
                "@GROUP_ID", DbType.String, groupID);
                dataReader = HCMDatabase.ExecuteReader(vectorCommand);

                CompetencyVector competencyVectorDetail;
                while (dataReader.Read())
                {
                    // Instantiate the competency Vector List collection.
                    if (competencyVectorList == null)
                        competencyVectorList = new List<CompetencyVector>();

                    // Instantiate the competency Vector detail.
                    competencyVectorDetail = new CompetencyVector();



                    if (!Utility.IsNullOrEmpty(dataReader["VECTOR_ID"]))
                    {
                        competencyVectorDetail.VectorID =
                            Convert.ToInt32(dataReader["VECTOR_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["VECTOR_NAME"]))
                    {
                        competencyVectorDetail.VectorType =
                            dataReader["VECTOR_NAME"].ToString().Trim();
                    }

                    // Add the attribute to the collection.
                    competencyVectorList.Add(competencyVectorDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return competencyVectorList;
        }

        /// <summary>
        /// Method that retrieves the list of competency vector Parameters.
        /// </summary>
        /// <returns>
        /// A list of <see cref="AdminCompetencyVectors"/> that holds the 
        /// vector paramaters.
        /// </returns>
        public List<CompetencyVectorParameter> GetCompetencyVectorParameter()
        {
            IDataReader dataReader = null;
            List<CompetencyVectorParameter> competencyVectorParameterList = null;

            try
            {
                DbCommand vectorCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_COMPETENCY_VECTOR_PARAMETER");
                dataReader = HCMDatabase.ExecuteReader(vectorCommand);

                CompetencyVectorParameter competencyVectorParameterDetail;
                while (dataReader.Read())
                {
                    // Instantiate the competency Vector List collection.
                    if (competencyVectorParameterList == null)
                        competencyVectorParameterList = new List<CompetencyVectorParameter>();

                    // Instantiate the competency Vector detail.
                    competencyVectorParameterDetail = new CompetencyVectorParameter();

                    if (!Utility.IsNullOrEmpty(dataReader["PARAMETER_ID"]))
                    {
                        competencyVectorParameterDetail.ParameterID =
                            Convert.ToInt32(dataReader["PARAMETER_ID"]);
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["PARAMETER_NAME"]))
                    {
                        competencyVectorParameterDetail.ParameterName =
                            dataReader["PARAMETER_NAME"].ToString().Trim();
                    }

                    // Add the attribute to the collection.
                    competencyVectorParameterList.Add(competencyVectorParameterDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return competencyVectorParameterList;
        }

        /// <summary>
        /// This method Insert Competency vector group details.
        /// </summary>
        /// <param name="adminCompetencyVectorsDetail">
        /// A list of <see cref="CompetencyVectorGroup"/> that holds the admin Competency Vectors Detail
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object
        /// </param>
        public void InsertCompetencyVectorGroup(CompetencyVectorGroup adminCompetencyVectorsDetail,
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertVectorGroupCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_COMPETENCY_VECTOR_GROUP");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertVectorGroupCommand,
                "@GROUP_NAME", DbType.String, adminCompetencyVectorsDetail.GroupName);
            HCMDatabase.AddInParameter(insertVectorGroupCommand,
               "@CREATED_BY", DbType.Int16, adminCompetencyVectorsDetail.UserID);
            HCMDatabase.AddInParameter(insertVectorGroupCommand,
                "@MODIFIED_BY", DbType.Int16, adminCompetencyVectorsDetail.UserID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertVectorGroupCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method Insert Competency vector details.
        /// </summary>
        /// <param name="adminCompetencyVectorsDetail">
        /// A list of <see cref="CompetencyVector"/> that holds the admin Competency Vectors Detail
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object
        /// </param>
        public void InsertCompetencyVector(CompetencyVector adminCompetencyVectorsDetail,
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertVectorCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_COMPETENCY_VECTOR");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertVectorCommand,
                "@VECTOR_NAME", DbType.String, adminCompetencyVectorsDetail.VectorType);
            HCMDatabase.AddInParameter(insertVectorCommand,
               "@GROUP_ID", DbType.String, adminCompetencyVectorsDetail.GroupID);
            HCMDatabase.AddInParameter(insertVectorCommand,
               "@CREATED_BY", DbType.Int16, adminCompetencyVectorsDetail.UserID);
            HCMDatabase.AddInParameter(insertVectorCommand,
                "@MODIFIED_BY", DbType.Int16, adminCompetencyVectorsDetail.UserID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertVectorCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// This method Insert Competency vector parameters details.
        /// </summary>
        /// <param name="adminCompetencyVectorsDetail">
        /// A <see cref="CompetencyVectorParameter"/> that holds the admin Competency Vectors Detail
        /// </param>
        /// <param name="transaction">
        /// A <see cref="IDbTransaction"/> that holds the transaction object
        /// </param>
        public void InsertCompetencyVectorParameter(CompetencyVectorParameter adminCompetencyVectorsDetail,
            IDbTransaction transaction)
        {
            // Create a stored procedure command object.
            DbCommand insertVectorParameterCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_COMPETENCY_VECTOR_PARAMETER");
            // Add input parameters.
            HCMDatabase.AddInParameter(insertVectorParameterCommand,
                "@PARAMETER_NAME", DbType.String, adminCompetencyVectorsDetail.ParameterName);
            HCMDatabase.AddInParameter(insertVectorParameterCommand,
               "@CREATED_BY", DbType.Int16, adminCompetencyVectorsDetail.UserID);
            HCMDatabase.AddInParameter(insertVectorParameterCommand,
                "@MODIFIED_BY", DbType.Int16, adminCompetencyVectorsDetail.UserID);
            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertVectorParameterCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Method that deletes a Competency vector group from the database.
        /// </summary>
        /// <param name="groupID">
        /// An <see cref="int"/> that holds the Group id to be deleted.
        /// </param>
        public void DeleteCompetencyVectorGroup(int groupID)
        {
            DbCommand deleteVectorCommand = HCMDatabase.
                       GetStoredProcCommand("SPDELETE_COMPETENCY_VECTOR_GROUP");

            HCMDatabase.AddInParameter(deleteVectorCommand,
                "@GROUP_ID", DbType.Int16, groupID);

            HCMDatabase.ExecuteNonQuery(deleteVectorCommand);
        }

        /// <summary>
        /// Method that deletes a Competency vectors from the database.
        /// </summary>
        /// <param name="vectorID">
        /// An <see cref="int"/> that holds the vector id to be deleted.
        /// </param>
        public void DeleteCompetencyVector(string vectorID)
        {
            DbCommand deleteVectorCommand = HCMDatabase.
                       GetStoredProcCommand("SPDELETE_COMPETENCY_VECTOR");

            HCMDatabase.AddInParameter(deleteVectorCommand,
                "@VECTOR_ID", DbType.Int16, vectorID);

            HCMDatabase.ExecuteNonQuery(deleteVectorCommand);
        }

        /// <summary>
        /// Method that deletes a Competency parameters from the database.
        /// </summary>
        /// <param name="parameterID">
        /// An <see cref="int"/> that holds the vector parameter id to be deleted.
        /// </param>
        public void DeleteCompetencyVectorParameter(string parameterID)
        {
            DbCommand deleteVectorParameterCommand = HCMDatabase.
                       GetStoredProcCommand("SPDELETE_COMPETENCY_VECTOR_PARAMETER");

            HCMDatabase.AddInParameter(deleteVectorParameterCommand,
                "@PARAMETER_ID", DbType.Int16, parameterID);

            HCMDatabase.ExecuteNonQuery(deleteVectorParameterCommand);
        }

        /// <summary>
        /// Method that retrieves the list of Competency Group name,vectors and parameters for skill matrix.
        /// </summary>
        /// <param name="candidateid">
        /// An <see cref="string"/> that holds the specific candidate id.
        /// </param>
        /// <returns>
        /// A set of <see cref="DataSet"/> that holds the 
        /// Competency Group name,vectors and parameters for skill matrix.
        /// </returns>        
        public DataSet GetCompetencyVectorSkillMatrix(string candidateid)
        {
            DataSet dataSet = null;
            DbCommand skillMatrixCommand = HCMDatabase
                .GetStoredProcCommand("SPGET_CANDIDATE_SKILLS_MATRIX");
            HCMDatabase.AddInParameter(skillMatrixCommand,
            "@CAND_SESSION_KEY", DbType.String, candidateid);
            // Execute the stored procedure.
            dataSet = HCMDatabase.ExecuteDataSet(skillMatrixCommand);
            return dataSet;
        }

        /// <summary>
        /// Method that retrieves the list of Competency Group name,vectors and parameters for skill matrix  .
        /// </summary>
        /// <param name="candidateid">
        /// An <see cref="int"/> that holds the specific candidate id.
        /// </param>
        /// <returns>
        /// A set of <see cref="DataSet"/> that holds the 
        /// Competency Group name,vectors and parameters for skill matrix for interview.
        /// </returns>   
        public DataSet GetCandidateSkillSubstance(int candidateID, int resumeID)
        {
            DataSet dataSet = null;
            DbCommand skillMatrixCommand = HCMDatabase
                .GetStoredProcCommand("SPGET_CANDIDATE_SKILLS_SUBSTANCE");
            HCMDatabase.AddInParameter(skillMatrixCommand,
            "@CANDIDATE_ID", DbType.Int32, candidateID);

            if (resumeID > 0)
            {
                HCMDatabase.AddInParameter(skillMatrixCommand,
                "@RESUME_ID", DbType.Int32, resumeID);
            }

            // Execute the stored procedure.
            dataSet = HCMDatabase.ExecuteDataSet(skillMatrixCommand);
            return dataSet;
        }

        public DataSet GetTemporaryResumeSkills(int candidateID)
        { 
            DataSet dataSet = null;
            DbCommand skillMatrixCommand = HCMDatabase
                .GetStoredProcCommand("SPGET_TEMPORARY_RESUME_SKILLS");
            HCMDatabase.AddInParameter(skillMatrixCommand,
            "@CANDIDATE_ID", DbType.Int32, candidateID); 

            // Execute the stored procedure.
            dataSet = HCMDatabase.ExecuteDataSet(skillMatrixCommand);
            return dataSet;
            
        }

        /// <summary>
        /// Method that retrieves the list of Competency Group name,vectors and parameters for skill matrix for interview.
        /// </summary>
        /// <param name="candidateid">
        /// An <see cref="string"/> that holds the specific candidate id.
        /// </param>
        /// <returns>
        /// A set of <see cref="DataSet"/> that holds the 
        /// Competency Group name,vectors and parameters for skill matrix for interview.
        /// </returns>        
        public DataSet GetInterviewCompetencyVectorSkillMatrix(string candidateid)
        {
            DataSet dataSet = null;
            DbCommand skillMatrixCommand = HCMDatabase
                .GetStoredProcCommand("SPGET_INTERVIEW_CANDIDATE_SKILLS_MATRIX");
            HCMDatabase.AddInParameter(skillMatrixCommand,
            "@CAND_SESSION_KEY", DbType.String, candidateid);
            // Execute the stored procedure.
            dataSet = HCMDatabase.ExecuteDataSet(skillMatrixCommand);
            return dataSet;
        }

        /// <summary>
        /// Method that checks if the given VEctor ID used in 
        /// skill matrix.
        /// </summary>
        /// <param name="vectorID">
        /// A <see cref="string"/> that holds the Vector ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the Candidate competency vector table.
        /// </remarks>
        public bool IsVectorIDUsed(string vectorID)
        {
            IDataReader dataReader = null;

            if (Utility.IsNullOrEmpty(vectorID))
                throw new Exception("Vector ID cannot be empty");

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPCHECK_VECTOR_ID_USED");

                HCMDatabase.AddInParameter(attributeCommand,
                    "@VECTOR_ID",
                    DbType.String,
                    (Utility.IsNullOrEmpty(vectorID) ? null : vectorID));

                object count = HCMDatabase.ExecuteScalar(attributeCommand);

                return Convert.ToInt32(count) == 0 ? false : true;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that checks if the given parameter ID used in 
        /// skill matrix.
        /// </summary>
        /// <param name="vectorparameterID">
        /// A <see cref="string"/> that holds the Parameter ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the exist status. True represents
        /// exists and false not exists.
        /// </returns>
        /// <remarks>
        /// This will check in the Candidate competency vector table.
        /// </remarks>
        public bool IsParameterIDUsed(string vectorparameterID)
        {
            IDataReader dataReader = null;

            if (Utility.IsNullOrEmpty(vectorparameterID))
                throw new Exception("Vector parameter ID cannot be empty");

            try
            {
                DbCommand attributeCommand = HCMDatabase
                    .GetStoredProcCommand("SPCHECK_VECTOR_PARAMETER_ID_USED");

                HCMDatabase.AddInParameter(attributeCommand,
                    "@PARAMETER_ID",
                    DbType.String,
                    (Utility.IsNullOrEmpty(vectorparameterID) ? null : vectorparameterID));

                object count = HCMDatabase.ExecuteScalar(attributeCommand);

                return Convert.ToInt32(count) == 0 ? false : true;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates proximity factor values into the database.
        /// </summary>
        /// <param name="proximityValue">
        /// A <see cref="decimal"/> object that holds the proximity value.
        /// </param>
        ///  /// <param name="attributeID">
        /// A <see cref="string"/> object that holds the attribute ID.
        /// </param>
        /// <param name="userID">
        /// A <see cref="int"/> object that holds the user ID.
        /// </param>
        public void UpdateProximityFactor(decimal proximityValue, string attributeID, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateCreditCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_PROXIMITY_FACTOR");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCreditCommand,
                "@PROXIMITY_FACTOR", DbType.String, proximityValue);
            HCMDatabase.AddInParameter(updateCreditCommand,
                "@ATTRIBUTE_ID", DbType.String, attributeID);
            HCMDatabase.AddInParameter(updateCreditCommand,
                "@MODIFIED_BY", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateCreditCommand);
        }

        /// <summary>
        /// Method that retrieves the available segments.
        /// </summary>
        /// <returns>
        /// A list of <see cref="SegmentDetail"/> that holds the segment details.
        /// </returns>
        public List<Segment> GetAvailableSegments()
        {
            IDataReader dataReader = null;

            try
            {
                List<Segment> segmentDetails = new List<Segment>();

                Segment segmentDetail;

                // Create a stored procedure command object.
                DbCommand segmentCommand = HCMDatabase.GetStoredProcCommand("SPGET_AVAILABLE_SEGMENTS");

                // Execute the command 
                dataReader = HCMDatabase.ExecuteReader(segmentCommand);

                while (dataReader.Read())
                {
                    // Add the segment detail
                    segmentDetail = new Segment();

                    if (!Utility.IsNullOrEmpty(dataReader["SEGMENT_ID"]))
                    {
                        segmentDetail.SegmentID = int.Parse(dataReader["SEGMENT_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SEGMENT_NAME"]))
                    {
                        segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SEGMENT_PATH"]))
                    {
                        segmentDetail.SegmentPath = dataReader["SEGMENT_PATH"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]))
                    {
                        segmentDetail.IsActive = dataReader["ACTIVE"].ToString().
                            ToUpper().Trim() == "Y" ? true : false;
                    }

                    segmentDetails.Add(segmentDetail);
                }

                // Return the list of segment details
                return segmentDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the available active segments.
        /// </summary>
        /// <returns>
        /// A list of <see cref="SegmentDetail"/> that holds the segment details.
        /// </returns>
        public List<Segment> GetAvailableActiveSegments()
        {
            IDataReader dataReader = null;

            try
            {
                List<Segment> segmentDetails = new List<Segment>();

                Segment segmentDetail;

                // Create a stored procedure command object.
                DbCommand segmentCommand = HCMDatabase.GetStoredProcCommand("SPGET_AVAILABLE_SEGMENTS");

                // Execute the command 
                dataReader = HCMDatabase.ExecuteReader(segmentCommand);

                while (dataReader.Read())
                {
                    // Check if segment is active or not.
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE"]) &&
                        dataReader["ACTIVE"].ToString().ToUpper().Trim() == "N")
                    {
                        continue;
                    }

                    // Add the segment detail
                    segmentDetail = new Segment();

                    segmentDetail.IsActive = true;

                    if (!Utility.IsNullOrEmpty(dataReader["SEGMENT_ID"]))
                    {
                        segmentDetail.SegmentID = int.Parse(dataReader["SEGMENT_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SEGMENT_NAME"]))
                    {
                        segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SEGMENT_PATH"]))
                    {
                        segmentDetail.SegmentPath = dataReader["SEGMENT_PATH"].ToString();
                    }

                    segmentDetails.Add(segmentDetail);
                }

                // Return the list of segment details
                return segmentDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Represents the method to get the segment path from the database
        /// </summary>
        /// <param name="segmentID">
        /// A<see cref="int"/>that holds the segment id
        /// </param>
        /// <returns>
        /// A<see cref="string"/>that holds the segment path
        /// </returns>
        public string GetSegmentPath(string segmentID)
        {
            try
            {
                DbCommand selectSegmentPathCommand = HCMDatabase.GetStoredProcCommand("SPGET_SEGMENT_PATH_NAME");

                HCMDatabase.AddInParameter(selectSegmentPathCommand, "@SEGMENT_ID", DbType.Int32, int.Parse(segmentID));

                string path = HCMDatabase.ExecuteScalar(selectSegmentPathCommand).ToString();

                return path;

            }
            finally
            {

            }
        }


        /// <summary>
        /// Inserts the new form.
        /// </summary>
        /// <param name="formDetail">The form detail.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="PredefinedForms"></param>
        /// <returns></returns>
        public int InsertNewForm(FormDetail formDetail, int tenantID, IDbTransaction transaction, out List<FormDetail> PredefinedForms)
        {
            IDataReader dataReader = null;
            DbCommand insertFormCommand = null;
            // Create a stored procedure command object
            try
            {
                insertFormCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_FORM");

                // Add input parameters

                HCMDatabase.AddInParameter(insertFormCommand,
                   "@TENANT_ID", DbType.Int32, tenantID);

                HCMDatabase.AddInParameter(insertFormCommand,
                    "@DESINGED_BY", DbType.Int32, formDetail.DesignedBy);

                HCMDatabase.AddInParameter(insertFormCommand,
                    "@CREATED_BY", DbType.Int32, formDetail.CreatedBy);

                HCMDatabase.AddInParameter(insertFormCommand,
                    "@FORM_NAME", DbType.String, formDetail.FormName);

                HCMDatabase.AddInParameter(insertFormCommand,
                    "@TAG", DbType.String, formDetail.Tags);

                HCMDatabase.AddInParameter(insertFormCommand,
                    "@IS_PRE_DEFINED", DbType.Int32, formDetail.IsGlobalForm == true ? 1 : 0);

                HCMDatabase.AddInParameter(insertFormCommand,
                    "@MODIFIED_BY", DbType.Int32, formDetail.CreatedBy);


                HCMDatabase.AddInParameter(insertFormCommand,
                    "@TOTAL_SEGMENTS_COUNT", DbType.Int32, formDetail.segmentList.Count);

                HCMDatabase.AddOutParameter(insertFormCommand,
                "@FORM_ID", DbType.Int32, 4);
                dataReader = HCMDatabase.ExecuteReader(insertFormCommand, transaction as DbTransaction);
                int FormId = 0;
                PredefinedForms = null;
                FormDetail PredefineFormDetail = null;
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["INSERTED_FORM_ID"]))
                    {
                        FormId = Convert.ToInt32(dataReader["INSERTED_FORM_ID"]);
                    }
                    dataReader.NextResult();
                    dataReader.NextResult();
                    while (dataReader.Read())
                    {
                        if (Utility.IsNullOrEmpty(PredefineFormDetail))
                            PredefineFormDetail = new FormDetail();
                        if (!Utility.IsNullOrEmpty(dataReader["USRID"]))
                            PredefineFormDetail.CreatedBy = Convert.ToInt32(dataReader["USRID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["FORM_ID"]))
                            PredefineFormDetail.FormID = Convert.ToInt32(dataReader["FORM_ID"]);
                        if (!Utility.IsNullOrEmpty(dataReader["TENANT_ID"]))
                            PredefineFormDetail.ModifiedBy = Convert.ToInt32(dataReader["TENANT_ID"]);
                        if (Utility.IsNullOrEmpty(PredefinedForms))
                            PredefinedForms = new List<FormDetail>();
                        PredefinedForms.Add(PredefineFormDetail);
                        PredefineFormDetail = null;
                    }
                }
                // Execute command
                // HCMDatabase.ExecuteNonQuery(insertFormCommand);

                // object formID = HCMDatabase.GetParameterValue(insertFormCommand,
                //"@FORM_ID");

                // int formIDint = int.Parse(formID.ToString());


                return FormId;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader.Close();
                if (!Utility.IsNullOrEmpty(insertFormCommand)) insertFormCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        #endregion Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="segment"></param>
        /// <param name="transaction"></param>
        /// <param name="formID"></param>
        public void InsertFormSegments(Segment segment, IDbTransaction transaction, int formID)
        {
            // Create a stored procedure command object
            DbCommand insertFormCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_FORM_SEGMENTS");

            // Add input parameters
            HCMDatabase.AddInParameter(insertFormCommand,
                "@FORM_ID", DbType.Int32, formID);

            HCMDatabase.AddInParameter(insertFormCommand,
             "@SEGMENT_ID", DbType.Int32, segment.SegmentID);

            HCMDatabase.AddInParameter(insertFormCommand,
             "@DISPLAY_ORDER", DbType.Int32, segment.DisplayOrder);

            HCMDatabase.AddInParameter(insertFormCommand,
                "@CREATED_BY", DbType.Int32, segment.CreatedBy);

            // Execute command
            HCMDatabase.ExecuteNonQuery(insertFormCommand, transaction as DbTransaction);
        }


        /// <summary>
        /// Method that searches and retrieves the position profile forms.
        /// </summary>
        /// <param name="formDetail">
        /// A <see cref="FormDetail"/> that holds the form search parameters.
        /// </param>
        /// <param name="orderDirection">
        /// A <see cref="string"/> that holds the order direction.
        /// </param>
        /// <param name="sortExpression">
        /// A <see cref="string"/> that holds the sort expression.
        /// </param>
        /// <param name="pageNumer">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="gridPageSize">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        /// <param name="totalRecords">
        /// A <see cref="int"/> that holds the total records.
        /// </param>
        /// <returns>
        /// A list of <see cref="FormDetail"/> that holds the searched forms.
        /// </returns>
        public List<FormDetail> GetPositionProfileForms(FormDetail formDetail,
            string orderDirection, string sortExpression,
            int pageNumer, int gridPageSize, out int totalRecords)
        {

            IDataReader dataReader = null;

            try
            {
                totalRecords = 0;

                List<FormDetail> formDetails = null;

                // Construc the command object.
                DbCommand getSearchedForm = HCMDatabase.GetStoredProcCommand("SPGET_FORMS");

                // Add input parameters.
                HCMDatabase.AddInParameter(getSearchedForm, "@TENANT_ID", DbType.Int32, formDetail.TenantID);
                HCMDatabase.AddInParameter(getSearchedForm, "@FORM_NAME", DbType.String, formDetail.FormName);
                HCMDatabase.AddInParameter(getSearchedForm, "@TAG", DbType.String, formDetail.Tags);
                HCMDatabase.AddInParameter(getSearchedForm, "@DESIGNED_BY", DbType.Int32, formDetail.SearchDesignedBy);
                HCMDatabase.AddInParameter(getSearchedForm, "@IS_GLOBAL", DbType.String, formDetail.IsGlobalForm ? 'Y' : 'N');
                HCMDatabase.AddInParameter(getSearchedForm, "@USER_ID", DbType.Int32, formDetail.UserID);
                HCMDatabase.AddInParameter(getSearchedForm, "@ORDER_BY", DbType.String, sortExpression);
                HCMDatabase.AddInParameter(getSearchedForm, "@ORDER_BY_DIRECTION", DbType.String, orderDirection == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getSearchedForm, "@PAGE_NUM", DbType.Int32, pageNumer);
                HCMDatabase.AddInParameter(getSearchedForm, "@PAGE_SIZE", DbType.Int32, gridPageSize);

                // Execute query.
                dataReader = HCMDatabase.ExecuteReader(getSearchedForm);

                while (dataReader.Read())
                {
                    // Instantiate the forms list.
                    if (formDetails == null)
                        formDetails = new List<FormDetail>();

                    // Instantiate forms object.
                    FormDetail form = new FormDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalRecords = int.Parse(dataReader["COUNT"].ToString());
                    }
                    else
                    {
                        if (!Utility.IsNullOrEmpty(dataReader["FORM_NAME"]))
                        {
                            form.FormName = dataReader["FORM_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["FORM_ID"]))
                        {
                            form.FormID = int.Parse(dataReader["FORM_ID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["TAGS"]))
                        {
                            form.Tags = dataReader["TAGS"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["DESIGNED_BY"]))
                        {
                            form.DesignedBy = int.Parse(dataReader["DESIGNED_BY"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["DESIGNED_BY_NAME"]))
                        {
                            form.UserFullName = dataReader["DESIGNED_BY_NAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["IS_GLOBAL"]) &&
                            dataReader["IS_GLOBAL"].ToString().ToUpper() == "Y")
                        {
                            form.GlobalFormText = "Yes";
                        }
                        else
                        {
                            form.GlobalFormText = "No";
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["ALLOW_EDIT"]) &&
                            dataReader["ALLOW_EDIT"].ToString().ToUpper() == "Y")
                        {
                            form.AllowEdit = true;
                        }

                        formDetails.Add(form);
                    }
                }

                return formDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Deletes the form.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        public void DeleteForm(int formID)
        {
            try
            {
                DbCommand deleteFormCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_FORM");

                HCMDatabase.AddInParameter(deleteFormCommand, "@FORM_ID", DbType.Int32, formID);

                HCMDatabase.ExecuteNonQuery(deleteFormCommand);
            }
            finally
            {
            }
        }


        /// <summary>
        /// Gets the existing segments.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <returns></returns>
        public FormDetail GetExistingSegments(int formID)
        {
            IDataReader dataReader = null;
            try
            {
                FormDetail formDetail = new FormDetail();

                List<Segment> segmentDetails = new List<Segment>();

                Segment segmentDetail;

                // Create a stored procedure command object.
                DbCommand segmentCommand = HCMDatabase.GetStoredProcCommand("[SPGET_EXISTING_FORM_DETAIL]");

                HCMDatabase.AddInParameter(segmentCommand, "@FORM_ID", DbType.Int32, formID);

                // Execute the command 
                dataReader = HCMDatabase.ExecuteReader(segmentCommand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["FORM_NAME"]))
                    {
                        formDetail.FormName = dataReader["FORM_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TAGS"]))
                    {
                        formDetail.Tags = dataReader["TAGS"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                    {
                        formDetail.UserFirstName = dataReader["USRFIRSTNAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["DESIGNED_BY"]))
                    {
                        formDetail.DesignedBy = int.Parse(dataReader["DESIGNED_BY"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FORM_ID"]))
                    {
                        formDetail.FormID = int.Parse(dataReader["FORM_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["IS_PRE_DEFINED"]))
                    {
                        formDetail.IsGlobalForm = Convert.ToBoolean(dataReader["IS_PRE_DEFINED"].ToString());
                    }
                }

                dataReader.NextResult();

                while (dataReader.Read())
                {
                    // Add the segment detail
                    segmentDetail = new Segment();

                    if (!Utility.IsNullOrEmpty(dataReader["SEGMENT_ID"]))
                    {
                        segmentDetail.SegmentID = int.Parse(dataReader["SEGMENT_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SEGMENT_NAME"]))
                    {
                        segmentDetail.SegmentName = dataReader["SEGMENT_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["DISPLAY_ORDER"]))
                    {
                        segmentDetail.DisplayOrder = int.Parse(dataReader["DISPLAY_ORDER"].ToString());
                    }

                    segmentDetails.Add(segmentDetail);
                }

                formDetail.segmentList = segmentDetails;

                // Return the list of segment details
                return formDetail;

            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }



        /// <summary>
        /// Updates the form detail.
        /// </summary>
        /// <param name="formDetail">The form detail.</param>
        /// <param name="transaction">The transaction.</param>
        public void UpdateFormDetail(FormDetail formDetail, IDbTransaction transaction)
        {
            // Create a stored procedure command object
            DbCommand updateFormCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_FORM_DETAIL");

            // Add input parameters
            HCMDatabase.AddInParameter(updateFormCommand,
                "@FORM_ID", DbType.Int32, formDetail.FormID);

            HCMDatabase.AddInParameter(updateFormCommand,
             "@FORM_NAME", DbType.String, formDetail.FormName);

            HCMDatabase.AddInParameter(updateFormCommand,
             "@TAGS", DbType.String, formDetail.Tags);

            HCMDatabase.AddInParameter(updateFormCommand,
                "@DESIGNED_BY", DbType.Int32, formDetail.DesignedBy);

            HCMDatabase.AddInParameter(updateFormCommand,
                "@MODIFIED_BY", DbType.Int32, formDetail.ModifiedBy);

            HCMDatabase.AddInParameter(updateFormCommand,
                "@IS_PRE_DEFINED", DbType.Int32, formDetail.IsGlobalForm == true ? 1 : 0);

            // Execute command
            HCMDatabase.ExecuteNonQuery(updateFormCommand, transaction as DbTransaction);
            //SPUPDATE_FORM_DETAIL

        }



        /// <summary>
        /// Deletes the segment for form.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <param name="transaction">The transaction.</param>
        public void DeleteSegmentForForm(int formID, IDbTransaction transaction)
        {
            DbCommand deleteFormSegmentsCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_FORM_SEGMENTS");

            HCMDatabase.AddInParameter(deleteFormSegmentsCommand, "@FORM_ID", DbType.Int32, formID);

            HCMDatabase.ExecuteNonQuery(deleteFormSegmentsCommand, transaction as DbTransaction);
        }

        /// <summary>
        /// Gets the business types.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="totalPages">The total pages.</param>
        /// <returns></returns>
        public List<BusinessTypeDetail> GetBusinessTypes(string businessName, string sortExpression,
            string sortOrder, int pageSize, int? pageNumber, out int totalPages)
        {
            IDataReader dataReader = null;

            totalPages = 0;
            DbCommand getBusinessTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_BUSINESS_TYPES");

            HCMDatabase.AddInParameter(getBusinessTypeCommand, "@TYPE_NAME", DbType.String, businessName);

            HCMDatabase.AddInParameter(getBusinessTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

            HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_NUM", DbType.Int32, pageNumber);

            HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_SIZE", DbType.Int32, pageSize);

            List<BusinessTypeDetail> businessTypeDetails = new List<BusinessTypeDetail>();

            dataReader = HCMDatabase.ExecuteReader(getBusinessTypeCommand);

            while (dataReader.Read())
            {
                if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                {
                    totalPages = int.Parse(dataReader["COUNT"].ToString());
                }
                else
                {
                    BusinessTypeDetail business = new BusinessTypeDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["BUSINESS_TYPE_ID"]))
                    {
                        business.BusinessTypeID = int.Parse(dataReader["BUSINESS_TYPE_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["NAME"]))
                    {
                        business.BusinessTypeName = dataReader["NAME"].ToString();
                    }

                    businessTypeDetails.Add(business);
                }

            }
            return businessTypeDetails;
        }


        /// <summary>
        /// Deletes the type of the business.
        /// </summary>
        /// <param name="businessID">The business ID.</param>
        public void DeleteBusinessType(int businessID)
        {
            // Create a stored procedure command object.
            DbCommand deleteBusinessTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_BUSINESS_TYPE");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteBusinessTypeCommand,
                "@BUSINESS_ID", DbType.Int32, businessID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteBusinessTypeCommand);
        }


        /// <summary>
        /// Updates the name of the business type.
        /// </summary>
        /// <param name="businessTypeName">Name of the business type.</param>
        /// <param name="businessID">The business ID.</param>
        public void UpdateBusinessTypeName(string businessTypeName, int businessID)
        {
            // Create a stored procedure command object.
            DbCommand updateBusinessTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_BUSINESS_TYPE");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateBusinessTypeCommand,
                "@BUSINESS_ID", DbType.Int32, businessID);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateBusinessTypeCommand,
                "@BUSINESS_NAME", DbType.String, businessTypeName);


            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateBusinessTypeCommand);
        }

        /// <summary>
        /// Inserts the new name of the business type.
        /// </summary>
        /// <param name="businessTypeName">Name of the business type.</param>
        /// <param name="userID">The user ID.</param>
        public void InsertNewBusinessTypeName(string businessTypeName, int userID)
        {
            // Create a stored procedure command object.
            DbCommand insertBusinessTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_BUSINESS_TYPE");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertBusinessTypeCommand,
                "@CREATED_BY", DbType.Int32, userID);

            // Add input parameters.
            HCMDatabase.AddInParameter(insertBusinessTypeCommand,
                "@BUSINESS_NAME", DbType.String, businessTypeName);


            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(insertBusinessTypeCommand);
        }

        /// <summary>
        /// Updates the subscription options.
        /// </summary>
        /// <param name="enrollmentDetail">The enrollment detail.</param>
        public void UpdateSubscriptionOptions(CustomerEnrollmentDetail enrollmentDetail)
        {
            // Create a stored procedure command object.
            DbCommand updateSubscriptionOptionsCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_OPTIONS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateSubscriptionOptionsCommand,
                "@FREE_TRIAL_DAYS", DbType.Int32, enrollmentDetail.FreeSubscriptionTrialValidiy);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateSubscriptionOptionsCommand,
                "@STANDARD_TRIAL_DAYS", DbType.Int32, enrollmentDetail.StandardSubscriptionTrialValidity);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateSubscriptionOptionsCommand,
                "@CORPORATE_TRIAL_DAYS", DbType.Int32, enrollmentDetail.CorporateSubscriptionTrialValidity);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateSubscriptionOptionsCommand,
                "@CORPORATE_MAX_USERS", DbType.Int32, enrollmentDetail.CorporateSubscriptionNoOfUser);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateSubscriptionOptionsCommand,
                "@DEFAULT_FORM", DbType.Int32, enrollmentDetail.DefaultForm);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateSubscriptionOptionsCommand,
                "@MODIFIED_BY", DbType.Int32, enrollmentDetail.ModifiedBy);


            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateSubscriptionOptionsCommand);
        }

        /// <summary>
        /// Gets the default subscrption details.
        /// </summary>
        /// <returns></returns>
        public CustomerEnrollmentDetail GetDefaultSubscrptionDetails()
        {
            IDataReader dataReader = null;

            DbCommand getDefaultValuesCommand = HCMDatabase.GetStoredProcCommand("SPGET_SUBSCRIPTION_OPTIONS");

            CustomerEnrollmentDetail customerEnrollmentDetail = new CustomerEnrollmentDetail();

            try
            {
                dataReader = HCMDatabase.ExecuteReader(getDefaultValuesCommand);

                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["FREE_TRIAL_DAYS"]))
                    {
                        customerEnrollmentDetail.FreeSubscriptionTrialValidiy =
                            int.Parse(dataReader["FREE_TRIAL_DAYS"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["STANDARD_TRIAL_DAYS"]))
                    {
                        customerEnrollmentDetail.StandardSubscriptionTrialValidity =
                            int.Parse(dataReader["STANDARD_TRIAL_DAYS"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CORPORATE_TRIAL_DAYS"]))
                    {
                        customerEnrollmentDetail.CorporateSubscriptionTrialValidity =
                            int.Parse(dataReader["CORPORATE_TRIAL_DAYS"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CORPORATE_MAX_USERS"]))
                    {
                        customerEnrollmentDetail.CorporateSubscriptionNoOfUser =
                            int.Parse(dataReader["CORPORATE_MAX_USERS"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["DEFAULT_FORM"]))
                    {
                        customerEnrollmentDetail.DefaultForm =
                            int.Parse(dataReader["DEFAULT_FORM"].ToString());
                    }


                }

                return customerEnrollmentDetail;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }

        }


        /// <summary>
        /// Gets the subscription details.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalPage">The total page.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns></returns>
        public List<UserRegistrationInfo> GetSubscriptionDetails(UserRegistrationInfo user, int
            pageNumber, int pageSize, out int totalPage, string sortExpression, string sortOrder)
        {
            IDataReader dataReader = null;

            try
            {
                totalPage = 0;

                DbCommand getCorporateSubscriptionCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_SUBSCRIPTION_DETAILS");

                List<UserRegistrationInfo> userRegistrationInfo = new List<UserRegistrationInfo>();

                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@USER_NAME", DbType.String, user.FirstName.Trim());
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@TITLE", DbType.String, user.Title.Trim());
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@COMPANY", DbType.String, user.Company.Trim());
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@COUTNRY", DbType.String, user.Country.Trim());
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@BUSINESS_TYPES", DbType.String, user.BussinessTypesIds);

                if (user.IsActive == -1)
                    HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@ACTIVE_STATUS", DbType.Int16, null);
                else
                    HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@ACTIVE_STATUS", DbType.Int16, user.IsActive);

                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@ORDER_BY", DbType.String, sortExpression.Trim());
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@PAGE_NUM", DbType.Int32, pageNumber);
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@PAGE_SIZE", DbType.Int32, pageSize);
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@SUBSCRIPTION_ROLE", DbType.String, "SR_COR_AMN");

              

                dataReader = HCMDatabase.ExecuteReader(getCorporateSubscriptionCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalPage = int.Parse(dataReader["COUNT"].ToString());
                    }

                    else
                    {
                        UserRegistrationInfo userRegistration = new UserRegistrationInfo();

                        if (!Utility.IsNullOrEmpty(dataReader["TENANT_USER_ID"]))
                        {
                            userRegistration.UserID =
                                int.Parse(dataReader["TENANT_USER_ID"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                        {
                            userRegistration.FirstName =
                                dataReader["FIRST_NAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["TITLE"]))
                        {
                            userRegistration.Title =
                                dataReader["TITLE"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                        {
                            userRegistration.Company =
                                dataReader["COMPANY"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["COUNTRY"]))
                        {
                            userRegistration.Country =
                                dataReader["COUNTRY"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE"]))
                        {
                            userRegistration.IsActive = Convert.ToInt16(dataReader["IS_ACTIVE"]);
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["FULL_NAME"]))
                        {
                            userRegistration.LastName =
                                dataReader["FULL_NAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["NUMBER_OF_USERS"]))
                        {
                            userRegistration.NoOfUsers =Convert.ToInt16(
                                dataReader["NUMBER_OF_USERS"].ToString());
                        }
                          

                        userRegistrationInfo.Add(userRegistration);
                    }
                }

                return userRegistrationInfo;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the default form.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public int GetDefaultForm(int userID)
        {
            // Create a stored procedure command object.
            DbCommand getDefaultFormcommand = HCMDatabase.
                GetStoredProcCommand("SPGET_DEFAULT_FORM");

            // Add input parameters.
            HCMDatabase.AddInParameter(getDefaultFormcommand,
                "@USER_ID", DbType.Int32, userID);



            int formID = Convert.ToInt32(HCMDatabase.ExecuteScalar(getDefaultFormcommand));

            return formID;
        }



        /// <summary>
        /// Updates the default form.
        /// </summary>
        /// <param name="formID">The form ID.</param>
        /// <param name="userID">The user ID.</param>
        public void UpdateDefaultForm(int formID, int userID)
        {
            // Create a stored procedure command object.
            DbCommand updateDefaultFormCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_DEFAULT_FORM");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateDefaultFormCommand,
                "@FORM_ID", DbType.Int32, formID);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateDefaultFormCommand,
                "@USER_ID", DbType.Int32, userID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateDefaultFormCommand);
        }

        /// <summary>
        /// Checks the form.
        /// </summary>
        /// <param name="formDetail">The form detail.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int CheckForm(FormDetail formDetail, int tenantID)
        {
            // Create a stored procedure command object.
            DbCommand checkFormCommand = HCMDatabase.
                GetStoredProcCommand("SPCHECK_FORM");

            // Add input parameters.
            HCMDatabase.AddInParameter(checkFormCommand,
                "@FORM_NAME", DbType.String, formDetail.FormName);

            // Add input parameters.
            HCMDatabase.AddInParameter(checkFormCommand,
                "@TENANT_ID", DbType.Int32, tenantID);

            // Execute the stored procedure.
            int formID = Convert.ToInt32(HCMDatabase.ExecuteScalar(checkFormCommand));

            return formID;
        }


        /// <summary>
        /// Checks the form name already exist.
        /// </summary>
        /// <param name="formDetail">The form detail.</param>
        /// <param name="tenantID">The tenant ID.</param>
        /// <returns></returns>
        public int CheckFormNameAlreadyExist(FormDetail formDetail, int tenantID)
        {
            // Create a stored procedure command object.
            DbCommand checkFormCommand = HCMDatabase.
                GetStoredProcCommand("SPCHECK_FORM_ALREADY_EXIST");

            // Add input parameters.
            HCMDatabase.AddInParameter(checkFormCommand,
                "@FORM_NAME", DbType.String, formDetail.FormName);

            // Add input parameters.
            HCMDatabase.AddInParameter(checkFormCommand,
                "@FORM_ID", DbType.Int32, formDetail.FormID);

            // Add input parameters.
            HCMDatabase.AddInParameter(checkFormCommand,
                "@TENANT_ID", DbType.Int32, tenantID);


            // Execute the stored procedure.
            int formID = Convert.ToInt32(HCMDatabase.ExecuteScalar(checkFormCommand));

            return formID;
        }

        /// <summary>
        /// Activates the users.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <param name="activateID">The activate ID.</param>
        /// <param name="modifiedBy">The modified by.</param>
        public void ActivateUsers(int userID, Int16 activateID, int modifiedBy)
        {
            // Create a stored procedure command object.
            DbCommand updateStausCommand = HCMDatabase.
                GetStoredProcCommand("SPUPDATE_TENANT_USER_STATUS");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateStausCommand,
                "@TENANT_USER_ID", DbType.Int32, userID);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateStausCommand,
                "@ACTIVE_STATUS", DbType.Int16, activateID);

            // Add input parameters.
            HCMDatabase.AddInParameter(updateStausCommand,
                "@MODIFIED_BY", DbType.Int32, modifiedBy);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(updateStausCommand);

        }




        /// <summary>
        /// Gets the user subscription details.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public UsageSummary GetUserSubscriptionDetails(int userID)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getCorporateSubscriptionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_USERSUBSCRIPTION_DETAILS");

                UsageSummary userSubscriptionDetail = null;

                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@USER_ID", DbType.Int32,
                   userID);


                dataReader = HCMDatabase.ExecuteReader(getCorporateSubscriptionCommand);
                if (dataReader.Read())
                {
                    if (userSubscriptionDetail == null)
                        userSubscriptionDetail = new UsageSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ID"]))
                    {
                        userSubscriptionDetail.SubscriptionID =
                            int.Parse(dataReader["SUBSCRIPTION_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_NAME"]))
                    {
                        userSubscriptionDetail.SubscriptionName =
                            dataReader["SUBSCRIPTION_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                    {
                        userSubscriptionDetail.SusbscribedOn =
                            dataReader["CREATED_DATE"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                    {
                        userSubscriptionDetail.Company =
                            dataReader["COMPANY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["TITLE"]))
                    {
                        userSubscriptionDetail.Title =
                            dataReader["TITLE"].ToString();
                    }
                }
                return userSubscriptionDetail;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method the retrieves the feature usage details for the given user, month and year.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="month">
        /// A <see cref="string"/> that holds the month.
        /// </param>
        /// <param name="year">
        /// A <see cref="int"/> that holds the year.
        /// </param>
        /// <returns>
        /// A list of <see cref="UsageSummary"/> that holds the usage details.
        /// </returns>
        public List<UsageSummary> GetFeatureUsageDetails(int userID, string month, int year)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getCorporateSubscriptionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_FEATURE_USAGE");

                List<UsageSummary> userFeatures = new List<UsageSummary>();

                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@USER_ID", DbType.Int32, userID);
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@MONTH", DbType.String, month);
                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@YEAR", DbType.Int32, year);

                dataReader = HCMDatabase.ExecuteReader(getCorporateSubscriptionCommand);

                while (dataReader.Read())
                {
                    UsageSummary userFeatureDetails = new UsageSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["FEATUREUSAGEID"]))
                    {
                        userFeatureDetails.FeatureID =
                            int.Parse(dataReader["FEATUREUSAGEID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FEATURENAME"]))
                    {
                        userFeatureDetails.FeatureName =
                            dataReader["FEATURENAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FEATURELIMIT"]))
                    {
                        userFeatureDetails.FeatureLimit =
                            dataReader["FEATURELIMIT"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FEATUREUSED"]))
                    {
                        userFeatureDetails.FeatureUsed =
                            dataReader["FEATUREUSED"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MMMYYYY"]))
                    {
                        userFeatureDetails.MonthYear =
                            dataReader["MMMYYYY"].ToString();
                    }

                    userFeatures.Add(userFeatureDetails);
                }

                return userFeatures;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the year
        /// </summary>
        /// <returns></returns>
        public List<UsageSummary> GetYear()
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getCorporateSubscriptionCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_YEAR");

                List<UsageSummary> year = new List<UsageSummary>();

                dataReader = HCMDatabase.ExecuteReader(getCorporateSubscriptionCommand);

                while (dataReader.Read())
                {
                    UsageSummary years = new UsageSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["YEAR"]))
                    {
                        years.FeatureYear = dataReader["YEAR"].ToString();
                    }

                    year.Add(years);
                }
                return year;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the position profile usage details.
        /// </summary>
        /// <param name="sortOrder"> holds sorting order</param>
        /// <param name="sortExpression">holds sorting expression</param>
        /// <param name="user">holds userID</param>
        /// <param name="mmmyyyy">holds month and year</param>
        /// <returns></returns>
        public List<UsageSummary> GetPositionProfileUsageDetails
            (string sortOrder, string sortExpression, int user, int mmmyyyy)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getFeatureDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_POSITION_PROFILE_USAGE_DETAILS");

                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY", DbType.String, sortExpression);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@USER_ID", DbType.Int32, user);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@MMM_YYYY", DbType.Int32, mmmyyyy);

                List<UsageSummary> featureDetails = new List<UsageSummary>();

                dataReader = HCMDatabase.ExecuteReader(getFeatureDetailsCommand);

                while (dataReader.Read())
                {
                    UsageSummary featureDetail = new UsageSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_LOG_ID"]))
                    {
                        featureDetail.PPID = dataReader["POSITION_PROFILE_LOG_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        featureDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CLIENT_NAME"]))
                    {
                        featureDetail.ClientName = dataReader["CLIENT_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_NAME"]))
                    {
                        featureDetail.PositionName = dataReader["POSITION_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                    {
                        featureDetail.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"].ToString()).ToShortDateString();
                    }


                    featureDetails.Add(featureDetail);
                }
                return featureDetails;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Gets the forms usage details.
        /// </summary>
        /// <param name="sortOrder"> holds sorting order</param>
        /// <param name="sortExpression">holds sorting expression</param>
        /// <param name="user">holds userID</param>
        /// <param name="mmmyyyy">holds month and year</param>
        /// <returns></returns>
        public List<UsageSummary> GetFormUsageDetails(string sortOrder,
            string sortExpression, int user, int mmmyyyy)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getFeatureDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_FORM_USAGE_DETAILS");

                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY_DIRECTION", DbType.String,
                    sortOrder == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY", DbType.String, sortExpression);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@USER_ID", DbType.Int32, user);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@MMM_YYYY", DbType.Int32, mmmyyyy);

                List<UsageSummary> featureDetails = new List<UsageSummary>();


                dataReader = HCMDatabase.ExecuteReader(getFeatureDetailsCommand);

                while (dataReader.Read())
                {
                    UsageSummary featureDetail = new UsageSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["FORM_LOG_ID"]))
                    {
                        featureDetail.FID = dataReader["FORM_LOG_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FORM_NAME"]))
                    {
                        featureDetail.FormName = dataReader["FORM_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TAGS"]))
                    {
                        featureDetail.FormTag = dataReader["TAGS"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_SEGMENTS"]))
                    {
                        featureDetail.FormSegments = dataReader["TOTAL_SEGMENTS"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                    {
                        featureDetail.CreatedDate = Convert.ToDateTime
                            (dataReader["CREATED_DATE"].ToString()).ToShortDateString();
                    }

                    featureDetails.Add(featureDetail);
                }
                return featureDetails;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// Gets the talents scout search usage details.
        /// </summary>
        /// <param name="sortOrder"> holds sorting order</param>
        /// <param name="sortExpression">holds sorting expression</param>
        /// <param name="user">holds userID</param>
        /// <param name="mmmyyyy">holds month and year</param>
        /// <returns></returns>
        public List<UsageSummary> GetTalentSearchUsageDetails(string sortOrder,
            string sortExpression, int user, int mmmyyyy)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getFeatureDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TALENTSCOUT_SEARCH_USAGE_DETAILS");

                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY_DIRECTION",
                        DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY", DbType.String,
                    sortExpression);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@USER_ID", DbType.Int32, user);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@MMM_YYYY", DbType.Int32, mmmyyyy);

                List<UsageSummary> featureDetails = new List<UsageSummary>();


                dataReader = HCMDatabase.ExecuteReader(getFeatureDetailsCommand);

                while (dataReader.Read())
                {
                    UsageSummary featureDetail = new UsageSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["TALENTSCOUT_SEARCH_LOG_ID"]))
                    {
                        featureDetail.FID = dataReader["TALENTSCOUT_SEARCH_LOG_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SEARCH_TERMS"]))
                    {
                        featureDetail.SearchTerms = dataReader["SEARCH_TERMS"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL_CANDIDATES_SEARCHED"]))
                    {
                        featureDetail.TotalCanditates = dataReader["TOTAL_CANDIDATES_SEARCHED"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["POSITION_PROFILE_NAME"]))
                    {
                        featureDetail.PositionProfileName = dataReader["POSITION_PROFILE_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SEARCHED_DATE"]))
                    {
                        featureDetail.CreatedDate = Convert.ToDateTime
                            (dataReader["SEARCHED_DATE"].ToString()).ToShortDateString();
                    }


                    featureDetails.Add(featureDetail);
                }
                return featureDetails;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }
        /// <summary>
        /// Gets the view profile usage details.
        /// </summary>
        /// <param name="sortOrder"> holds sorting order</param>
        /// <param name="sortExpression">holds sorting expression</param>
        /// <param name="user">holds userID</param>
        /// <param name="mmmyyyy">holds month and year</param>
        /// <returns></returns>
        public List<UsageSummary> GetViewProfileImageUsageDetails(string sortOrder,
            string sortExpression, int user, int mmmyyyy)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getFeatureDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_VIEW_PROFILE_IMAGE_USAGE_DETAILS");

                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY", DbType.String,
                    sortExpression);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@USER_ID", DbType.Int32, user);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@MMM_YYYY", DbType.Int32, mmmyyyy);

                List<UsageSummary> featureDetails = new List<UsageSummary>();


                dataReader = HCMDatabase.ExecuteReader(getFeatureDetailsCommand);

                while (dataReader.Read())
                {
                    UsageSummary featureDetail = new UsageSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["VIEW_PROFILE_IMAGE_LOG_ID"]))
                    {
                        featureDetail.FID = dataReader["VIEW_PROFILE_IMAGE_LOG_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        featureDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["VIEWED_DATE"]))
                    {
                        featureDetail.CreatedDate = Convert.ToDateTime
                            (dataReader["VIEWED_DATE"].ToString()).ToShortDateString();
                    }


                    featureDetails.Add(featureDetail);
                }
                return featureDetails;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that checks if the subscription feature usage limit exceeds.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="featureID">
        /// A <see cref="int"/> that holds the feature ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. If limit exceeds return
        /// a value of greater than 0 else returns 0.
        /// </returns>
        public int GetCheckFeatureUsage(int tenantID, int featureID)
        {
            int count = 0;
            DbCommand getCheckFeatureUsageCommand =
                HCMDatabase.GetStoredProcCommand("SPCHECK_FEATURE_USAGE_COUNT");
            HCMDatabase.AddInParameter(getCheckFeatureUsageCommand, "@TENANT_ID", DbType.Int32, tenantID);
            HCMDatabase.AddInParameter(getCheckFeatureUsageCommand, "@FEATURE_ID", DbType.Int32, featureID);
            object id = HCMDatabase.ExecuteScalar(getCheckFeatureUsageCommand);
            if (!Utility.IsNullOrEmpty(id))
                count = int.Parse(id.ToString().Trim());

            return count;
        }

        /// <summary>
        /// Method that checks if the given feature is applicable for the
        /// tenant ID.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="featureID">
        /// A <see cref="int"/> that holds the feature ID.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the status. If applicable returns 
        /// true else returns false.
        /// </returns>
        public bool IsFeatureApplicable(int tenantID, int featureID)
        {
            int count = 0;
            DbCommand getFeatureApplicableCommand =
                HCMDatabase.GetStoredProcCommand("SPGET_FEATURE_APPLICABLE");
            HCMDatabase.AddInParameter(getFeatureApplicableCommand, "@TENANT_ID", DbType.Int32, tenantID);
            HCMDatabase.AddInParameter(getFeatureApplicableCommand, "@FEATURE_ID", DbType.Int32, featureID);
            object id = HCMDatabase.ExecuteScalar(getFeatureApplicableCommand);
            if (!Utility.IsNullOrEmpty(id))
                count = int.Parse(id.ToString().Trim());

            return count > 0 ? true : false ;
        }

        /// <summary>
        /// Inserts the internal user.
        /// </summary>
        /// <param name="userRegistrationInfo">The user registration info.</param>
        /// <param name="registrationConfirmation">The registration confirmation.</param>
        /// <param name="tenantUserID">The tenant user ID.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public int InsertInternalUser(UserRegistrationInfo userRegistrationInfo,
            RegistrationConfirmation registrationConfirmation, int tenantUserID,
            IDbTransaction transaction)
        {
            DbCommand InsertUserDbCommand = null;
            try
            {
                if (Utility.IsNullOrEmpty(transaction))
                    throw new NullReferenceException("Transaction should not be null");
                if (transaction.Connection.State == ConnectionState.Closed)
                    throw new InvalidOperationException("Transaction connection state should be in open");
                InsertUserDbCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_INTERNAL_USERS");
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@TENANT_USER_ID", DbType.String, tenantUserID);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@USER_NAME", DbType.String, userRegistrationInfo.UserEmail);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@PASSWORD", DbType.String, userRegistrationInfo.Password);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@FIRST_NAME", DbType.String, userRegistrationInfo.FirstName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@LAST_NAME", DbType.String, userRegistrationInfo.LastName);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@PHONE", DbType.String, userRegistrationInfo.Phone);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CONFIRMATION_CODE", DbType.String, registrationConfirmation.ConfirmationCode);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@USER_TYPE", DbType.String, userRegistrationInfo.IsSiteAdmin ? "UT_ST_ADM" : null);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@ROLE_IDS", DbType.String, userRegistrationInfo.RoleIDs == null || userRegistrationInfo.RoleIDs.Trim().Length == 0 ? null : userRegistrationInfo.RoleIDs.TrimEnd(new char[] { ',' }));
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@CREATED_BY", DbType.Int32, int.Parse(userRegistrationInfo.CreatedBy));
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@EXPIRY_DATE", DbType.DateTime, userRegistrationInfo.ExpiryDate);
                HCMDatabase.AddInParameter(InsertUserDbCommand, "@LEGAL_ACCEPTED", DbType.String, 'Y');
                

                return HCMDatabase.ExecuteNonQuery(InsertUserDbCommand, transaction as DbTransaction);
            }
            finally
            {
                if (InsertUserDbCommand != null) InsertUserDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Gets the modules.
        /// </summary>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="totalPage">The total page.</param>
        /// <returns></returns>
        public List<ModuleManagementDetails> GetModules(string sortExpression, string sortOrder,
            int pageSize, int pageNumber, out int totalPage)
        {
            IDataReader dataReader = null;
            try
            {
                totalPage = 0;

                DbCommand getBusinessTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_MODULESDETAILS");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@SORT_EXPRESSION", DbType.String, sortExpression);

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_SIZE", DbType.Int32, pageSize);

                List<ModuleManagementDetails> moduleDetails = new List<ModuleManagementDetails>();

                dataReader = HCMDatabase.ExecuteReader(getBusinessTypeCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalPage = int.Parse(dataReader["COUNT"].ToString());
                    }
                    else
                    {
                        ModuleManagementDetails moduleDetail = new ModuleManagementDetails();

                        if (!Utility.IsNullOrEmpty(dataReader["MODID"]))
                        {
                            moduleDetail.ModID = int.Parse(dataReader["MODID"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MODCODE"]))
                        {
                            moduleDetail.ModCode = dataReader["MODCODE"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MODNAME"]))
                        {
                            moduleDetail.ModName = dataReader["MODNAME"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["MODCODENAME"]))
                        {
                            moduleDetail.ModeCodeName = dataReader["MODCODENAME"].ToString();
                        }


                        moduleDetails.Add(moduleDetail);
                    }

                }
                return moduleDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Updates the module.
        /// </summary>
        /// <param name="moduleCode">The module code.</param>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="moduleID">The module ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public int UpdateModule(string moduleCode, string moduleName, int moduleID, int userID)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectmoduleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPSELECTCOUNT_MODULE");

                HCMDatabase.AddInParameter(selectmoduleTypeCommand,
                   "@MOD_ID", DbType.Int32, moduleID);

                HCMDatabase.AddInParameter(selectmoduleTypeCommand,
                   "@MOD_CODE", DbType.String, moduleName);


                datareader = HCMDatabase.ExecuteReader(selectmoduleTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWSCOUNT"].ToString());
                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
            if (rowsAffected == 0)
            {

                // Create a stored procedure command object.
                DbCommand updateRoleCategoryTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_MODULEDETAILS");

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@MOD_ID", DbType.Int32, moduleID);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@MOD_CODE", DbType.String, moduleCode);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@MOD_NAME", DbType.String, moduleName);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@MOD_MODIFIEDBY", DbType.Int32, userID);


                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(updateRoleCategoryTypeCommand);
            }
            return rowsAffected;
        }

        /// <summary>
        /// Deletes the module.
        /// </summary>
        /// <param name="moduleID">The module ID.</param>
        /// <returns></returns>
        public string DeleteModuleResults(string moduleID)
        {
            IDataReader datareader = null;
            string result = string.Empty;
            try
            {

                DbCommand deleteModuleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_MODULE_RESULT");

                HCMDatabase.AddInParameter(deleteModuleTypeCommand,
                   "@MODID", DbType.Int32, moduleID);

                datareader = HCMDatabase.ExecuteReader(deleteModuleTypeCommand);

                while (datareader.Read())
                {
                    result = datareader["RESULT"].ToString();
                }
                return result;

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }


        /// <summary>
        /// Deletes the module.
        /// </summary>
        /// <param name="moduleID">The module ID.</param>
        /// <returns></returns>
        public void DeleteModule(string moduleID)
        {
            IDataReader datareader = null;
            string result = string.Empty;
            try
            {

                DbCommand deleteModuleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_MODULE");

                HCMDatabase.AddInParameter(deleteModuleTypeCommand,
                   "@MODID", DbType.Int32, moduleID);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(deleteModuleTypeCommand);
                {

                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
        }
        /// <summary>
        /// Inserts the new module.
        /// </summary>
        /// <param name="moduleCode">The module code.</param>
        /// <param name="moduleName">Name of the module.</param>
        /// <param name="moduleCreatedBy">The module created by.</param>
        /// <returns></returns>
        public int InsertNewModule(string moduleCode, string moduleName, int moduleCreatedBy)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectModuleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPSELECTCOUNT_MODULEDETAILS");

                HCMDatabase.AddInParameter(selectModuleTypeCommand,
                   "@MOD_CODE", DbType.String, moduleCode);

                datareader = HCMDatabase.ExecuteReader(selectModuleTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWSCOUNT"].ToString());
                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
            if (rowsAffected == 0)
            {
                // Create a stored procedure command object.
                DbCommand insertModuleTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_MODULE");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertModuleTypeCommand,
                    "@MOD_CODE", DbType.String, moduleCode);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertModuleTypeCommand,
                    "@MOD_NAME", DbType.String, moduleName);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertModuleTypeCommand,
                    "@MOD_CREATEDBY", DbType.Int32, moduleCreatedBy);


                // Add input parameters.
                HCMDatabase.AddInParameter(insertModuleTypeCommand,
                    "@MOD_MODEFIEDBY", DbType.Int32, moduleCreatedBy);


                HCMDatabase.ExecuteNonQuery(insertModuleTypeCommand);
            }
            return rowsAffected;
        }

        /// <summary>
        /// Updates the sub module details.
        /// </summary>
        /// <param name="submoduleDetails">The submodule details.</param>
        public void UpdateSubModuleDetails(List<ModuleManagementDetails> submoduleDetails)
        {
            for (int i = 0; i < submoduleDetails.Count; i++)
            {

                DbCommand updateSubModuleTypeCommand = HCMDatabase.
           GetStoredProcCommand("SPUPDATE_SUBMODULE");

                // Add input parameters.
                HCMDatabase.AddInParameter(updateSubModuleTypeCommand,
                    "@SUBMODULE_ID", DbType.Int32, submoduleDetails[i].SubModuleID);



                // Add input parameters.
                HCMDatabase.AddInParameter(updateSubModuleTypeCommand,
                    "@SUBMODULE_CODE", DbType.String, submoduleDetails[i].SubModuleCode);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateSubModuleTypeCommand,
                    "@SUBMODULE_NAME", DbType.String, submoduleDetails[i].SubModuleName);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateSubModuleTypeCommand,
                    "@SUBMODULE_MODIFIEDBY", DbType.Int32, submoduleDetails[i].ModifiedBy);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(updateSubModuleTypeCommand);
            }

        }

        /// <summary>
        /// Gets the module.
        /// </summary>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="totalPage">The total page.</param>
        /// <returns></returns>
        public List<ModuleManagementDetails> GetModule(string sortExpression, string sortOrder,
            int pageSize, int pageNumber, out int totalPage)
        {
            IDataReader dataReader = null;
            try
            {


                totalPage = 0;

                DbCommand getBusinessTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_MODULESDETAILS");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@SORT_EXPRESSION", DbType.String, sortExpression);

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_SIZE", DbType.Int32, pageSize);

                List<ModuleManagementDetails> moduleDetails = new List<ModuleManagementDetails>();

                dataReader = HCMDatabase.ExecuteReader(getBusinessTypeCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalPage = int.Parse(dataReader["COUNT"].ToString());
                    }
                    else
                    {
                        ModuleManagementDetails module = new ModuleManagementDetails();

                        if (!Utility.IsNullOrEmpty(dataReader["MODCODE"]))
                        {
                            module.ModCode = dataReader["MODCODE"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MODNAME"]))
                        {
                            module.ModName = dataReader["MODNAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MODID"]))
                        {
                            module.ModID = int.Parse(dataReader["MODID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["MODCODENAME"]))
                        {
                            module.ModuleCodeName = dataReader["MODCODENAME"].ToString();
                        }


                        moduleDetails.Add(module);
                    }

                }
                return moduleDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Gets the sub modules.
        /// </summary>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <returns></returns>
        public List<ModuleManagementDetails> GetSubModules(string sortExpression, string sortOrder,
            int pageNumber)
        {
            IDataReader dataReader = null;
            try
            {


                //  int totalPage = 0;
                DbCommand getBusinessTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_SUBMODULES");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getBusinessTypeCommand, "@PAGE_NUM", DbType.Int32, pageNumber);


                List<ModuleManagementDetails> subModuleDetails = new List<ModuleManagementDetails>();

                dataReader = HCMDatabase.ExecuteReader(getBusinessTypeCommand);

                while (dataReader.Read())
                {

                    ModuleManagementDetails submodule = new ModuleManagementDetails();

                    if (!Utility.IsNullOrEmpty(dataReader["MODID"]))
                    {
                        submodule.SubModuleID = int.Parse(dataReader["MODID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["MODCODE"]))
                    {
                        submodule.ModCode = dataReader["MODCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MODNAME"]))
                    {
                        submodule.ModName = dataReader["MODNAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SMOCODE"]))
                    {
                        submodule.SubModuleCode = dataReader["SMOCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SMONAME"]))
                    {
                        submodule.SubModuleName = dataReader["SMONAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["MODCODENAME"]))
                    {
                        submodule.ModeCodeName = dataReader["MODCODENAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SUBMODENAMECODE"]))
                    {
                        submodule.SubModNameCode = dataReader["SUBMODENAMECODE"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SMOSEQUENCENO"]))
                    {
                        submodule.SubModSequence = int.Parse(dataReader["SMOSEQUENCENO"].ToString());
                    }

                    subModuleDetails.Add(submodule);

                }
                return subModuleDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Inserts the new module.
        /// </summary>
        /// <param name="subModuleCode">The sub module code.</param>
        /// <param name="moduleID">The module ID.</param>
        /// <param name="subModuleName">Name of the sub module.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public int InsertNewModule(string subModuleCode, int moduleID,
            string subModuleName, int userID)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectSubModuleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPSELECTCOUNT_SUBMODULE");

                HCMDatabase.AddInParameter(selectSubModuleTypeCommand,
                   "@SMO_CODE", DbType.String, subModuleCode);

                datareader = HCMDatabase.ExecuteReader(selectSubModuleTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWSCOUNT"].ToString());
                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
            if (rowsAffected == 0)
            {
                // Create a stored procedure command object.
                DbCommand insertRoleTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_SUBMODULE");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@SMO_CODE", DbType.String, subModuleCode);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@MOD_ID", DbType.Int32, moduleID);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@SMO_NAME", DbType.String, subModuleName);


                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@SMO_CREATEDBY", DbType.Int32, userID);


                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleTypeCommand,
                    "@SMO_MODEFIEDBY", DbType.Int32, userID);


                HCMDatabase.ExecuteNonQuery(insertRoleTypeCommand);
            }
            return rowsAffected;
        }

        /// <summary>
        /// Deletes the sub module.
        /// </summary>
        /// <param name="modID">The mod ID.</param>
        public void DeleteSubModule(int modID)
        {
            // Create a stored procedure command object.
            DbCommand deleteSubModuleTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_SUBMODULE");

            // Add input parameters.
            HCMDatabase.AddInParameter(deleteSubModuleTypeCommand,
                "@MODID", DbType.Int32, modID);

            // Execute the stored procedure.
            HCMDatabase.ExecuteNonQuery(deleteSubModuleTypeCommand);
            {

            }
        }

        /// <summary>
        /// Gets the features.
        /// </summary>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="totalPage">The total page.</param>
        /// <returns></returns>
        public List<FeatureManagementDetails> GetFeatures(string sortExpression,
            string sortOrder, int pageSize, int pageNumber, out int totalPage)
        {
            IDataReader dataReader = null;
            try
            {
                totalPage = 0;

                DbCommand getFeaturesTypeCommand = HCMDatabase.GetStoredProcCommand("SPGET_FEATUREDETAILS");

                HCMDatabase.AddInParameter(getFeaturesTypeCommand, "@ORDER_BY_DIRECTION", DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");

                HCMDatabase.AddInParameter(getFeaturesTypeCommand, "@SORT_EXPRESSION", DbType.String, sortExpression);

                HCMDatabase.AddInParameter(getFeaturesTypeCommand, "@PAGE_NUM", DbType.Int32, pageNumber);

                HCMDatabase.AddInParameter(getFeaturesTypeCommand, "@PAGE_SIZE", DbType.Int32, pageSize);

                List<FeatureManagementDetails> featureDetails = new List<FeatureManagementDetails>();

                dataReader = HCMDatabase.ExecuteReader(getFeaturesTypeCommand);

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["COUNT"]))
                    {
                        totalPage = int.Parse(dataReader["COUNT"].ToString());
                    }
                    else
                    {
                        FeatureManagementDetails featureDetail = new FeatureManagementDetails();

                        if (!Utility.IsNullOrEmpty(dataReader["FEAID"]))
                        {
                            featureDetail.FeatureID = int.Parse(dataReader["FEAID"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MODID"]))
                        {
                            featureDetail.ModuleID = int.Parse(dataReader["MODID"].ToString());
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["MODULE"]))
                        {
                            featureDetail.Module = dataReader["MODULE"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SMOID"]))
                        {
                            featureDetail.SubModuleID = int.Parse(dataReader["SMOID"].ToString());
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["SUBMODULE"]))
                        {
                            featureDetail.SubModule = dataReader["SUBMODULE"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["FEAFEATURENAME"]))
                        {
                            featureDetail.FeatureName = dataReader["FEAFEATURENAME"].ToString();
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["FEAURL"]))
                        {
                            featureDetail.FeatureURL = dataReader["FEAURL"].ToString();
                        }
                        if (!Utility.IsNullOrEmpty(dataReader["FEASEQUENCENO"]))
                        {
                            featureDetail.FeatureSeqNO = int.Parse(dataReader["FEASEQUENCENO"].ToString());
                        }


                        featureDetails.Add(featureDetail);
                    }

                }
                return featureDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Updates the feature.
        /// </summary>
        /// <param name="moduleID">The module ID.</param>
        /// <param name="subModuleID">The sub module ID.</param>
        /// <param name="featureName">Name of the feature.</param>
        /// <param name="featureURL">The feature URL.</param>
        /// <param name="featureID">The feature ID.</param>
        /// <param name="userID">The user ID.</param>
        /// <returns></returns>
        public int UpdateFeature(string featureName,
            string featureURL, int featureID, int userID)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectfeatureTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPSELECTCOUNT_FEATURE");

                HCMDatabase.AddInParameter(selectfeatureTypeCommand,
                   "@FEATURE_ID", DbType.Int32, featureID);

                HCMDatabase.AddInParameter(selectfeatureTypeCommand,
                   "@FEATURE_NAME", DbType.String, featureName);


                datareader = HCMDatabase.ExecuteReader(selectfeatureTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWSCOUNT"].ToString());
                }

            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }
            if (rowsAffected == 0)
            {

                // Create a stored procedure command object.
                DbCommand updateRoleCategoryTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPUPDATE_FEATUREDETAILS");



                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@FEATURE_NAME", DbType.String, featureName);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@FEATURE_URL", DbType.String, featureURL);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@FEATUREID", DbType.Int32, featureID);

                // Add input parameters.
                HCMDatabase.AddInParameter(updateRoleCategoryTypeCommand,
                    "@MOD_MODIFIEDBY", DbType.Int32, userID);


                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(updateRoleCategoryTypeCommand);
            }
            return rowsAffected;
        }

        public int InsertNewFeature(int moduleID, int submoduleID, string featureName, string featureURL, int featueCreatedBy)
        {
            IDataReader datareader = null;
            int rowsAffected = 0;
            try
            {

                DbCommand selectFeatureTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPGET_FEATURE_COUNT");

                HCMDatabase.AddInParameter(selectFeatureTypeCommand,
                   "@FEATURE_NAME", DbType.String, featureName);

                datareader = HCMDatabase.ExecuteReader(selectFeatureTypeCommand);

                while (datareader.Read())
                {
                    rowsAffected = int.Parse(datareader["ROWS_COUNT"].ToString());
                }
            }
            finally
            {
                if (datareader != null && !datareader.IsClosed)
                    datareader.Close();
            }

            if (rowsAffected == 0)
            {
                // Create a stored procedure command object.
                DbCommand insertRoleCatagoryTypeCommand = HCMDatabase.
                    GetStoredProcCommand("SPINSERT_FEATURE");

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@MOD_ID", DbType.Int32, moduleID);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@SUBMOD_ID", DbType.Int32, submoduleID);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@EFATURE_NAME", DbType.String, featureName);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@FEATUREURL", DbType.String, featureURL);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@ROLCAT_CREATEDBY", DbType.Int32, featueCreatedBy);

                // Add input parameters.
                HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                    "@ROLCAT_MODEFIEDBY", DbType.Int32, featueCreatedBy);

                HCMDatabase.ExecuteNonQuery(insertRoleCatagoryTypeCommand);
            }
            return rowsAffected;
        }

        public void DeleteFeature(int featureID)
        {
            try
            {

                DbCommand deleteFeatureTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPDELETE_FEATURE");

                HCMDatabase.AddInParameter(deleteFeatureTypeCommand,
                   "@FEATUREID", DbType.Int32, featureID);

                // Execute the stored procedure.
                HCMDatabase.ExecuteNonQuery(deleteFeatureTypeCommand);
                {

                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Gets the view cloning usage details.
        /// </summary>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="user">The user.</param>
        /// <param name="monthYear">The month year.</param>
        /// <returns></returns>
        public List<UsageSummary> GetViewCloningUsageDetails(string sortOrder, string sortExpression,
            int user, int monthYear)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getFeatureDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_TALENTSCOUT_CLONE_USAGE_DETAILS");

                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY_DIRECTION",
                    DbType.String, sortOrder == SortType.Ascending.ToString() ? "A" : "D");
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@ORDER_BY", DbType.String,
                    sortExpression);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@USER_ID", DbType.Int32, user);
                HCMDatabase.AddInParameter(getFeatureDetailsCommand, "@MMM_YYYY", DbType.Int32, monthYear);

                List<UsageSummary> featureDetails = new List<UsageSummary>();


                dataReader = HCMDatabase.ExecuteReader(getFeatureDetailsCommand);

                while (dataReader.Read())
                {
                    UsageSummary featureDetail = new UsageSummary();

                    if (!Utility.IsNullOrEmpty(dataReader["TALENTSCOUT_CLONE_LOG_ID"]))
                    {
                        featureDetail.FID = dataReader["TALENTSCOUT_CLONE_LOG_ID"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["SEARCH_TERMS"]))
                    {
                        featureDetail.SearchTerm = dataReader["SEARCH_TERMS"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_NAME"]))
                    {
                        featureDetail.CandidateName = dataReader["CANDIDATE_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CLONED_DATE"]))
                    {
                        featureDetail.CreatedDate = Convert.ToDateTime
                            (dataReader["CLONED_DATE"].ToString()).ToShortDateString();
                    }


                    featureDetails.Add(featureDetail);
                }
                return featureDetails;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }


        public void UpdateCorporateRoles(CustomerEnrollmentDetail enrollmentDetail)
        {
            // Create a stored procedure command object.
            DbCommand insertRoleCatagoryTypeCommand = HCMDatabase.
                GetStoredProcCommand("SPINSERT_COROPORATE_ROLES");


            // Add input parameters.
            HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                "@USER_ID", DbType.Int32, enrollmentDetail.UserID);
            // Add input parameters.
            HCMDatabase.AddInParameter(insertRoleCatagoryTypeCommand,
                "@COMBINED_ROLE_IDS", DbType.String, enrollmentDetail.corporateRoleIDs);

            HCMDatabase.ExecuteNonQuery(insertRoleCatagoryTypeCommand);
        }




        public UserRegistrationInfo GetSubscriptionUserDetails(short userID)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getCorporateSubscriptionCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_SUBSCRIPTION_USER_DETAILS");

                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@USER_ID", DbType.Int16, userID);


                dataReader = HCMDatabase.ExecuteReader(getCorporateSubscriptionCommand);

                UserRegistrationInfo userRegistration = new UserRegistrationInfo();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                    {
                        userRegistration.UserID =
                            int.Parse(dataReader["USER_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        userRegistration.FirstName =
                            dataReader["FIRST_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TITLE"]))
                    {
                        userRegistration.Title =
                            dataReader["TITLE"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                    {
                        userRegistration.Company =
                            dataReader["COMPANY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE"]))
                    {
                        userRegistration.Phone =
                            dataReader["PHONE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE"]))
                    {
                        userRegistration.IsActive = Convert.ToInt16(dataReader["IS_ACTIVE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FULL_NAME"]))
                    {
                        userRegistration.LastName =
                            dataReader["FULL_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["NUMBER_OF_USERS"]))
                    {
                        userRegistration.NoOfUsers =Convert.ToInt16(
                            dataReader["NUMBER_OF_USERS"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE_NO_OF_USERS"]))
                    {
                        userRegistration.ActiveNoOfUsers = Convert.ToInt16(
                            dataReader["ACTIVE_NO_OF_USERS"].ToString());
                    }
                }
                return userRegistration;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public List<Roles> GetCorporateAdminAssignedRoles(short userID)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getRoleDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CORPORATEADMIN_SELECTEDROLES");


                HCMDatabase.AddInParameter(getRoleDetailsCommand, "@USERID", DbType.Int32, userID);

                List<Roles> rolesDetails = new List<Roles>();


                dataReader = HCMDatabase.ExecuteReader(getRoleDetailsCommand);

                while (dataReader.Read())
                {
                    Roles roleDetail = new Roles();

                    if (!Utility.IsNullOrEmpty(dataReader["ROLID"]))
                    {
                        roleDetail.RoleID = Convert.ToInt16(dataReader["ROLID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLCODE"]))
                    {
                        roleDetail.RoleCode= dataReader["ROLCODE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ROLNAME"]))
                    {
                        roleDetail.RoleName = dataReader["ROLNAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["IS_SELECTED"]))
                    {
                        roleDetail.IsSelected = dataReader["IS_SELECTED"].ToString();
                    }


                    rolesDetails.Add(roleDetail);
                }
                return rolesDetails;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        public UserRegistrationInfo GetCorporateDetails(int userID)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getCorporateSubscriptionCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATE_SUBSCRIPTION_USER_DETAILS");

                HCMDatabase.AddInParameter(getCorporateSubscriptionCommand, "@USER_ID", DbType.Int16, userID);


                dataReader = HCMDatabase.ExecuteReader(getCorporateSubscriptionCommand);

                UserRegistrationInfo userRegistration = new UserRegistrationInfo();

                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                    {
                        userRegistration.UserID =
                            int.Parse(dataReader["USER_ID"].ToString());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["FIRST_NAME"]))
                    {
                        userRegistration.FirstName =
                            dataReader["FIRST_NAME"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["TITLE"]))
                    {
                        userRegistration.Title =
                            dataReader["TITLE"].ToString();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["COMPANY"]))
                    {
                        userRegistration.Company =
                            dataReader["COMPANY"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["PHONE"]))
                    {
                        userRegistration.Phone =
                            dataReader["PHONE"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["IS_ACTIVE"]))
                    {
                        userRegistration.IsActive = Convert.ToInt16(dataReader["IS_ACTIVE"]);
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["FULL_NAME"]))
                    {
                        userRegistration.LastName =
                            dataReader["FULL_NAME"].ToString();
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["NUMBER_OF_USERS"]))
                    {
                        userRegistration.NoOfUsers = Convert.ToInt16(
                            dataReader["NUMBER_OF_USERS"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["ACTIVE_NO_OF_USERS"]))
                    {
                        userRegistration.ActiveNoOfUsers = Convert.ToInt16(
                            dataReader["ACTIVE_NO_OF_USERS"].ToString());
                    }
                }
                return userRegistration;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of candidate self skill which is added newly.
        /// </summary>
        /// <returns>
        /// A list of <see cref="CandidateInformation"/> that holds the 
        /// Candidate attributes..
        /// </returns>
        public List<CandidateInformation> GetCandidateSelfSkillPending()
        {
            IDataReader dataReader = null;
            List<CandidateInformation> candidateInformation = null;

            try
            {
                DbCommand candidateSelfCommand = HCMDatabase
                    .GetStoredProcCommand("SPGET_CANDIDATE_SELF_SKILL_PENDING");

                dataReader = HCMDatabase.ExecuteReader(candidateSelfCommand);

                CandidateInformation candidateInformationDetail;
                while (dataReader.Read())
                {
                    // Instantiate the CandidateInformation List collection.
                    if (candidateInformation == null)
                        candidateInformation = new List<CandidateInformation>();

                    candidateInformationDetail = new CandidateInformation();

                    if (!Utility.IsNullOrEmpty(dataReader["CANDIDATE_ID"]))
                    {
                        candidateInformationDetail.caiID =
                            Convert.ToInt32(dataReader["CANDIDATE_ID"].ToString().Trim());
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                    {
                        candidateInformationDetail.caiFirstName = dataReader["USRFIRSTNAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USRMIDDLENAME"]))
                    {
                        candidateInformationDetail.caiMiddleName =
                            dataReader["USRMIDDLENAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USRLASTNAME"]))
                    {
                        candidateInformationDetail.caiLastName =
                            dataReader["USRLASTNAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["USREMAIL"]))
                    {
                        candidateInformationDetail.caiEmail =
                            dataReader["USREMAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_DETAIL"]))
                    {
                        candidateInformationDetail.SkillDetail =
                            dataReader["SKILL_DETAIL"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(dataReader["CAND_SKILL_ID"]))
                    {
                        candidateInformationDetail.SkillID =
                            Convert.ToInt32(dataReader["CAND_SKILL_ID"].ToString().Trim());
                    }

                    // Add the attribute to the collection.
                    candidateInformation.Add(candidateInformationDetail);
                }
                dataReader.Close();
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return candidateInformation;
        }


        /// <summary>
        /// Method that retrieves the list of skills for the given search
        /// parameters.
        /// </summary>
        /// <param name="skillName">
        /// A <see cref="string"/> that holds the skill name.
        /// </param>
        /// <returns>
        /// A list of <see cref="SkillDetail"/> that holds the skill details.
        /// </returns>
        public List<SkillDetail> GetRelatedCandidateSelfSkills(string skillName)
        {
            List<SkillDetail> skills = null;
            IDataReader dataReader = null;

            try
            {
                DbCommand getRelatedSkillCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_RELATED_SKILLS_BY_NEW_SKILL");

                HCMDatabase.AddInParameter(getRelatedSkillCommand, "@SKILL_NAME", DbType.String,
                    Utility.IsNullOrEmpty(skillName) ? null : skillName.Trim());

                dataReader = HCMDatabase.ExecuteReader(getRelatedSkillCommand);

                while (dataReader.Read())
                {
                        // Instantiate skill list.
                        if (skills == null)
                            skills = new List<SkillDetail>();

                        // Create a skill object.
                        SkillDetail skill = new SkillDetail();

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_ID"]))
                        {
                            skill.SkillID = Convert.ToInt32(dataReader["SKILL_ID"]);
                        }

                        if (!Utility.IsNullOrEmpty(dataReader["SKILL_NAME"]))
                        {
                            skill.SkillName = dataReader["SKILL_NAME"].ToString();
                        }
                        // Add to the list.
                        skills.Add(skill);
                }
                return skills;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                    dataReader.Close();
            }
        }

        /// <summary>
        /// Method that retreives the candidate self skill name 
        /// </summary>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skill ID.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the skill name
        /// </returns>
        public string GetCandidateSelfSkillBySkillId(int skillID)
        {
            IDataReader dataReader = null;

            try
            {
                DbCommand getSkillNameCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CANDIDATE_SELF_SKILL_BY_SKILL_ID");

                HCMDatabase.AddInParameter(getSkillNameCommand,
                    "@CAND_SKILL_ID", DbType.Int32, skillID);

                dataReader = HCMDatabase.ExecuteReader(getSkillNameCommand);

                // Retreive skill name.
                if (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["SKILL_DETAIL"]))
                        return dataReader["SKILL_DETAIL"].ToString().Trim();
                    else
                        return null;
                }
                return null;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that insert skill.
        /// </summary>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the candidateID
        /// </param>
        /// <param name="skillName">
        /// A <see cref="string"/> that holds the skill Name.
        /// </param>
        public void InsertSkillFromCandidateSelfSkill(int candidateSkillID, string skillName, int userID)
        {
            DbCommand insertCommand =
                HCMDatabase.GetStoredProcCommand("SPINSERT_SKILL_FROM_CANDIDATE_SELF_SKILL");

            // Add input parameters.
            HCMDatabase.AddInParameter(insertCommand,
                "@CAND_SKILL_ID", DbType.Int32, candidateSkillID);

            HCMDatabase.AddInParameter(insertCommand,
                "@SKILL_NAME", DbType.String, skillName);

            HCMDatabase.AddInParameter(insertCommand,
                "@CREATED_BY", DbType.Int32, userID);

           HCMDatabase.ExecuteNonQuery(insertCommand);
        }

        /// <summary>
        /// Method that updates candidate self skill detail.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidateID
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the skillID
        /// </param>
        /// <param name="skillName">
        /// A <see cref="int"/> that holds the skillName
        /// </param>
        /// <param name="skillID">
        /// A <see cref="int"/> that holds the candidateID
        /// </param>
        public void UpdateCandidateSelfSkill(int candidateSkillID, int skillID, string skillName, int userID)
        {
            DbCommand updateCommand =
                HCMDatabase.GetStoredProcCommand("SPUPDATE_CANDIDATE_SELF_SKILL");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateCommand,
                "@CAND_SKILL_ID", DbType.Int32, candidateSkillID);

            HCMDatabase.AddInParameter(updateCommand,
                "@SKILL_ID", DbType.Int32, skillID);

            HCMDatabase.AddInParameter(updateCommand,
                "@SKILL_NAME", DbType.String, skillName);

            HCMDatabase.AddInParameter(updateCommand,
                "@MODIFIED_BY", DbType.Int32, userID);

            HCMDatabase.ExecuteNonQuery(updateCommand);
        }

        #region Customer Admin 

        /// <summary>
        /// Method that retrieves the list of users for the given tenant.
        /// </summary>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <returns>
        /// A <see cref="UserDetail"/> that holds the list of user details.
        /// </returns>
        public List<UserDetail> GetCustomerUser(int tenantID)
        {
            IDataReader dataReader = null;

            try
            {

                DbCommand getCustomerDetailsCommand =
                    HCMDatabase.GetStoredProcCommand("SPGET_CUSTOMERDETAILS");

                HCMDatabase.AddInParameter(getCustomerDetailsCommand, "@TENANT_ID", DbType.Int16, tenantID);

                List<UserDetail> userDetails = new List<UserDetail>();

                dataReader = HCMDatabase.ExecuteReader(getCustomerDetailsCommand);

                while (dataReader.Read())
                {
                    UserDetail userDetail = new UserDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USER_ID"]))
                    {
                        userDetail.UserID = int.Parse(dataReader["USER_ID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(dataReader["USER_NAME"]))
                    {
                        userDetail.UserName = dataReader["USER_NAME"].ToString();
                    }
                    userDetails.Add(userDetail);
                }
                return userDetails;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that retrieves the list of user rights for the given tenant. 
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="showAllFeature">
        /// A <see cref="int"/> that holds the show all feature flag.
        /// </param>
        /// <param name="tenantID">
        /// A <see cref="int"/> that holds the tenant ID.
        /// </param>
        /// <param name="totalRecordCount">
        /// A <see cref="int"/> that holds the total record count as an output 
        /// parameter.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserRightsMatrix"/> that holds the user rights.
        /// </returns>
        public List<UserRightsMatrix> GetCustomerRightsDetails(string userID, 
            int showAllFeature, int tenantID, out int totalRecordCount)
        {
            IDataReader datareader = null;

            totalRecordCount = 0;

            List<UserRightsMatrix> userRightMatrix = new List<UserRightsMatrix>();

            try
            {
                DbCommand getUserRightsMatrixCommand = HCMDatabase.GetStoredProcCommand("SPGET_CORPORATEUSER_RIGHTS");
                HCMDatabase.AddInParameter(getUserRightsMatrixCommand, "@USERID", DbType.Int32,
                    int.Parse(userID));

                HCMDatabase.AddInParameter(getUserRightsMatrixCommand, "@TENANTID", DbType.Int32,
                tenantID);

                HCMDatabase.AddInParameter(getUserRightsMatrixCommand, "@SHOWALL", DbType.Int16,
                showAllFeature);

                datareader = HCMDatabase.ExecuteReader(getUserRightsMatrixCommand);

                while (datareader.Read())
                {
                    UserRightsMatrix userRights = new UserRightsMatrix();

                    if (!Utility.IsNullOrEmpty(datareader["MODID"]))
                    {
                        userRights.ModuleID = int.Parse(datareader["MODID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SMOID"]))
                    {
                        userRights.SubModuleID = int.Parse(datareader["SMOID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["FEAID"]))
                    {
                        userRights.FeatureID = int.Parse(datareader["FEAID"].ToString());
                    }
                    if (!Utility.IsNullOrEmpty(datareader["SMONAME"]))
                    {
                        userRights.SubModuleName = datareader["SMONAME"].ToString().Trim();
                    }

                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_NAME"]))
                    {
                        userRights.FeatureName = datareader["FEATURE_NAME"].ToString().Trim();
                    }
                    if (!Utility.IsNullOrEmpty(datareader["RRTPAGE"]))
                    {
                        userRights.IsApplicable =
                            Convert.ToBoolean(int.Parse(datareader["RRTPAGE"].ToString()));
                    }
                    else
                    {
                        userRights.IsApplicable = false;
                    }

                    if (!Utility.IsNullOrEmpty(datareader["ALREADY_EXIST"]))
                    {
                        userRights.IsActive =
                            datareader["ALREADY_EXIST"].ToString() == "Y" ? true : false;
                    }

                    userRightMatrix.Add(userRights);
                }

                return userRightMatrix;
            }

            finally
            {
                if (datareader != null && !datareader.IsClosed)
                {
                    datareader.Close();
                }
            }
        }

        #endregion Customer Admin
    }
}