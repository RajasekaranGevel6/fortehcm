﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.DataObjects;

namespace Forte.HCM.DL
{
    /// <summary>                                                  
    /// Class that represents the data layer for the subscripiton management module.
    /// This includes functionalities for retrieving, updating, delete and add 
    /// values for subscriptions and features. 
    /// This will talk to the database for performing these operations.
    /// </summary>
    /// <remarks>
    /// This class inherits <see cref="DatabaseConnectionManager"/> object
    /// to obtain database connection.
    /// </remarks>
    public class SubscriptionDLManager : DatabaseConnectionManager
    {
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public SubscriptionDLManager()
        {
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// Method that retrieves the list of features applicable for the given
        /// subscription type.
        /// </summary>
        /// <param name="subscriptionID">
        /// A <see cref="int"/> that holds the subscription type.
        /// </param>
        /// <returns>
        /// A list of <see cref="SubscriptionFeatures"/> that holds the subscription 
        /// features.
        /// </returns>
        public List<SubscriptionFeatures> GetSubscriptionFeatures(int subscriptionID)
        {
            DbCommand getSubscriptionFeatures = null;
            IDataReader dataReader = null;
            List<SubscriptionFeatures> subscriptionFeatures = null;
            SubscriptionFeatures subscriptionFeature = null;
            try
            {
                getSubscriptionFeatures = HCMDatabase.GetStoredProcCommand("SPGET_SUBSCRIPTION_FEATURES");
                HCMDatabase.AddInParameter(getSubscriptionFeatures, "@SUBSCRIPTION_ID", DbType.Int32, subscriptionID);

                dataReader = HCMDatabase.ExecuteReader(getSubscriptionFeatures);

                while (dataReader.Read())
                {
                    subscriptionFeature = new SubscriptionFeatures();

                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_NAME"]))
                        subscriptionFeature.FeatureName = dataReader["FEATURE_NAME"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_VALUE"]))
                        subscriptionFeature.FeatureValue = dataReader["FEATURE_VALUE"].ToString();

                    if (subscriptionFeatures == null)
                        subscriptionFeatures = new List<SubscriptionFeatures>();

                    // Add to the list.
                    subscriptionFeatures.Add(subscriptionFeature);
                    subscriptionFeature = null;
                }
                return subscriptionFeatures;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that picks all the subscription features based on the selected subscription id.
        /// </summary>
        /// <param name="SubscriptionId">
        /// A <see cref="System.Int32"/> that holds user selected subscription id
        /// </param>
        /// <param name="Total_Records">
        /// A <see cref="System.Int32"/> out parameter that holds the total number of records
        /// in the repository
        /// </param>
        /// <param name="PageSize">
        /// A <see cref="System.Int32"/> that holds the page size of the grid
        /// </param>
        /// <param name="PageNumber">
        /// A <see cref="System.Int32"/> that holds the page number of the grid
        /// </param>
        /// <returns></returns>
        public IList<SubscriptionFeatures> GetSelectedSubscriptionFeatures(int SubscriptionId, out int Total_Records, int? PageSize, int PageNumber)
        {
            DbCommand GetSelectedSubscriptionFeatuersDbCommand = null;
            IDataReader dataReader = null;
            List<SubscriptionFeatures> subscriptionFeatures = null;
            SubscriptionFeatures subscriptionFeature = null;
            try
            {
                Total_Records = 0;
                GetSelectedSubscriptionFeatuersDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_SUBSCRIPTION_FEATURE");
                HCMDatabase.AddInParameter(GetSelectedSubscriptionFeatuersDbCommand, "@SUBSCRIPTION_ID", DbType.Int32, SubscriptionId);
                HCMDatabase.AddInParameter(GetSelectedSubscriptionFeatuersDbCommand, "@PAGESIZE", DbType.Int32, PageSize);
                HCMDatabase.AddInParameter(GetSelectedSubscriptionFeatuersDbCommand, "@PAGENUMBER", DbType.Int32, PageNumber);
                dataReader = HCMDatabase.ExecuteReader(GetSelectedSubscriptionFeatuersDbCommand);
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["TOTAL"]))
                    {
                        Total_Records = Convert.ToInt32(dataReader["TOTAL"]);
                        continue;
                    }
                    if (Utility.IsNullOrEmpty(subscriptionFeature))
                        subscriptionFeature = new SubscriptionFeatures();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_FEATURE_ID"]))
                        subscriptionFeature.SubscriptionFeatureId = Convert.ToInt32(dataReader["SUBSCRIPTION_FEATURE_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_NAME"]))
                        subscriptionFeature.FeatureName = dataReader["FEATURE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_VALUE"]))
                        subscriptionFeature.FeatureValue = dataReader["FEATURE_VALUE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ID"]))
                        subscriptionFeature.SubscriptionId = Convert.ToInt32(dataReader["SUBSCRIPTION_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ISNOTAPPLICABLE"]))
                        subscriptionFeature.IsNotApplicable = Convert.ToInt16(dataReader["ISNOTAPPLICABLE"]);
                    if (!Utility.IsNullOrEmpty(dataReader["ISUNUNLIMITED"]))
                        subscriptionFeature.IsUnlimited = Convert.ToInt16(dataReader["ISUNUNLIMITED"]);
                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_UNIT"]))
                        subscriptionFeature.FeatureUnit = dataReader["FEATURE_UNIT"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_UNIT_ID"]))
                        subscriptionFeature.FeatureUnitId = dataReader["FEATURE_UNIT_ID"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["DEFAULT_VALUE"]))
                        subscriptionFeature.DefaultValue = dataReader["DEFAULT_VALUE"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_ID"]))
                        subscriptionFeature.FeatureId = Convert.ToInt32(dataReader["FEATURE_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_BY"]))
                        subscriptionFeature.CreatedBy = Convert.ToInt32(dataReader["CREATED_BY"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CREATED_DATE"]))
                        subscriptionFeature.CreatedDate = Convert.ToDateTime(dataReader["CREATED_DATE"]);
                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_BY"]))
                        subscriptionFeature.ModifiedBy = Convert.ToInt32(dataReader["MODIFIED_BY"]);
                    if (!Utility.IsNullOrEmpty(dataReader["MODIFIED_DATE"]))
                        subscriptionFeature.ModifiedDate = Convert.ToDateTime(dataReader["MODIFIED_DATE"]);
                    if (Utility.IsNullOrEmpty(subscriptionFeatures))
                        subscriptionFeatures = new List<SubscriptionFeatures>();
                    subscriptionFeatures.Add(subscriptionFeature);
                    subscriptionFeature = null;
                }
                return subscriptionFeatures;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(subscriptionFeatures)) subscriptionFeatures = null;
                if (!Utility.IsNullOrEmpty(subscriptionFeature)) subscriptionFeature = null;
                if (!Utility.IsNullOrEmpty(GetSelectedSubscriptionFeatuersDbCommand)) GetSelectedSubscriptionFeatuersDbCommand = null;
                try { GC.Collect(2, GCCollectionMode.Forced); }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that updates the feature value for a subscription
        /// </summary>
        /// <param name="subscriptionFeatures">
        /// A <see cref="Forte.HCM.DataObjects.SubscriptionFeatures"/> that holds the
        /// subscription features
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of records
        /// effected in the repository
        /// </returns>
        public int UpdateSubscriptionFeaturePricing(SubscriptionFeatures subscriptionFeatures)
        {
            DbCommand UpdateSubscriptionFeaturePricingDbcommand = null;
            try
            {
                UpdateSubscriptionFeaturePricingDbcommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_FEATURE");
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@SUBSCRIPTION_FEATURE_ID", DbType.Int32, subscriptionFeatures.SubscriptionFeatureId);
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@FEATURE_VALUE", DbType.String, subscriptionFeatures.FeatureValue);
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@ISNOTAPPLICABLE", DbType.Boolean, subscriptionFeatures.IsNotApplicable);
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@ISUNLIMITED", DbType.Boolean, subscriptionFeatures.IsUnlimited);
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@MODIFIED_BY", DbType.Int32, subscriptionFeatures.ModifiedBy);
                return HCMDatabase.ExecuteNonQuery(UpdateSubscriptionFeaturePricingDbcommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateSubscriptionFeaturePricingDbcommand)) UpdateSubscriptionFeaturePricingDbcommand = null;
                try { GC.Collect(2, GCCollectionMode.Forced); }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that updates the feature value for a subscription
        /// </summary>
        /// <param name="subscriptionFeatures">
        /// A <see cref="Forte.HCM.DataObjects.SubscriptionFeatures"/> that holds the
        /// subscription features
        /// </param>
        /// <param name="Transaction">
        /// A <see cref="System.Data.IDbTransaction"/> that holds the
        /// transaction object
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of records
        /// effected in the repository
        /// </returns>
        /// <remarks>
        /// Though this method accepts transaction as parameter it will not do any transaction related
        /// properties like begin,commit,rollback
        /// </remarks>
        public int UpdateSubscriptionFeaturePricing(SubscriptionFeatures subscriptionFeatures, IDbTransaction Transaction)
        {
            DbCommand UpdateSubscriptionFeaturePricingDbcommand = null;
            try
            {
                UpdateSubscriptionFeaturePricingDbcommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_FEATURE");
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@SUBSCRIPTION_FEATURE_ID", DbType.Int32, subscriptionFeatures.SubscriptionFeatureId);
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@FEATURE_VALUE", DbType.String, subscriptionFeatures.FeatureValue);
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@ISNOTAPPLICABLE", DbType.Boolean, subscriptionFeatures.IsNotApplicable);
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@ISUNLIMITED", DbType.Boolean, subscriptionFeatures.IsUnlimited);
                HCMDatabase.AddInParameter(UpdateSubscriptionFeaturePricingDbcommand, "@MODIFIED_BY", DbType.Int32, subscriptionFeatures.ModifiedBy);
                return HCMDatabase.ExecuteNonQuery(UpdateSubscriptionFeaturePricingDbcommand, Transaction as DbTransaction);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateSubscriptionFeaturePricingDbcommand)) UpdateSubscriptionFeaturePricingDbcommand = null;
                try { GC.Collect(2, GCCollectionMode.Forced); }
                catch { GC.Collect(); }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<SubscriptionFeatureTypes> GetAllFeatureTypes()
        {
            DbCommand GetAllFeatureTypesDbCommand = null;
            IDataReader datareader = null;
            IList<SubscriptionFeatureTypes> subscriptionFeatureTypes = null;
            SubscriptionFeatureTypes subscriptionFeatureType = null;
            try
            {
                GetAllFeatureTypesDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_SUBSCRIPTION_FEATURE_TYPES");
                datareader = HCMDatabase.ExecuteReader(GetAllFeatureTypesDbCommand);
                while (datareader.Read())
                {
                    if (Utility.IsNullOrEmpty(subscriptionFeatureType))
                        subscriptionFeatureType = new SubscriptionFeatureTypes();
                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_ID"]))
                        subscriptionFeatureType.FeatureId = Convert.ToInt32(datareader["FEATURE_ID"]);
                    if (!Utility.IsNullOrEmpty(datareader["FEATURE_NAME"]))
                        subscriptionFeatureType.FeatureName = datareader["FEATURE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(datareader["UNIT"]))
                        subscriptionFeatureType.Unit = datareader["UNIT"].ToString();
                    if (!Utility.IsNullOrEmpty(datareader["UNIT_NAME"]))
                        subscriptionFeatureType.Unitname = datareader["UNIT_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(datareader["DEFAULT_VALUE"]))
                        subscriptionFeatureType.DefaultValue = datareader["DEFAULT_VALUE"].ToString();
                    if (!Utility.IsNullOrEmpty(datareader["ISUNLIMITED"]))
                        subscriptionFeatureType.IsUnlimited = Convert.ToInt16(datareader["ISUNLIMITED"]);
                    if (!Utility.IsNullOrEmpty(datareader["CREATED_BY"]))
                        subscriptionFeatureType.CreatedBy = Convert.ToInt32(datareader["CREATED_BY"]);
                    if (!Utility.IsNullOrEmpty(datareader["CREATED_DATE"]))
                        subscriptionFeatureType.CreatedDate = Convert.ToDateTime(datareader["CREATED_DATE"]);
                    if (!Utility.IsNullOrEmpty(datareader["MODIFIED_BY"]))
                        subscriptionFeatureType.ModifiedBy = Convert.ToInt32(datareader["MODIFIED_BY"]);
                    if (!Utility.IsNullOrEmpty(datareader["MODIFIED_DATE"]))
                        subscriptionFeatureType.ModifiedDate = Convert.ToDateTime(datareader["MODIFIED_DATE"]);
                    if (Utility.IsNullOrEmpty(subscriptionFeatureTypes))
                        subscriptionFeatureTypes = new List<SubscriptionFeatureTypes>();
                    subscriptionFeatureTypes.Add(subscriptionFeatureType);
                    subscriptionFeatureType = null;
                }
                return subscriptionFeatureTypes;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(datareader) && !datareader.IsClosed) datareader.Close();
                if (!Utility.IsNullOrEmpty(datareader)) datareader = null;
                if (!Utility.IsNullOrEmpty(GetAllFeatureTypesDbCommand)) GetAllFeatureTypesDbCommand = null;
                if (!Utility.IsNullOrEmpty(subscriptionFeatureTypes)) subscriptionFeatureTypes = null;
                if (Utility.IsNullOrEmpty(subscriptionFeatureType)) subscriptionFeatureType = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionFeatureTypes"></param>
        /// <returns></returns>
        public int InsertFeatureSubscriptionType(SubscriptionFeatureTypes subscriptionFeatureTypes)
        {
            DbCommand InsertFeatureSubscriptionTypeDbCommand = null;
            try
            {
                InsertFeatureSubscriptionTypeDbCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_SUBSCRIPTION_FEATURE_TYPES");
                HCMDatabase.AddInParameter(InsertFeatureSubscriptionTypeDbCommand, "@FEATURE_NAME", DbType.String, subscriptionFeatureTypes.FeatureName);
                HCMDatabase.AddInParameter(InsertFeatureSubscriptionTypeDbCommand, "@UNIT", DbType.String, subscriptionFeatureTypes.Unit);
                HCMDatabase.AddInParameter(InsertFeatureSubscriptionTypeDbCommand, "@DEFAULT_VALUE", DbType.String, subscriptionFeatureTypes.DefaultValue);
                HCMDatabase.AddInParameter(InsertFeatureSubscriptionTypeDbCommand, "@CREATED_BY", DbType.Int32, subscriptionFeatureTypes.CreatedBy);
                HCMDatabase.AddInParameter(InsertFeatureSubscriptionTypeDbCommand, "@ISUNLIMITED", DbType.Int16, subscriptionFeatureTypes.IsUnlimited);
                return Convert.ToInt32(HCMDatabase.ExecuteScalar(InsertFeatureSubscriptionTypeDbCommand));
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(InsertFeatureSubscriptionTypeDbCommand)) InsertFeatureSubscriptionTypeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionFeatureTypes"></param>
        /// <returns></returns>
        public int UpdateFeatureSubscriptionType(SubscriptionFeatureTypes subscriptionFeatureTypes)
        {
            DbCommand UpdateFeatureSubscriptionTypeDbCommand = null;
            try
            {
                UpdateFeatureSubscriptionTypeDbCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_FEATURE_TYPES");
                HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@FEATURE_ID", DbType.Int32, subscriptionFeatureTypes.FeatureId);
                HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@FEATURE_NAME", DbType.String, subscriptionFeatureTypes.FeatureName);
                HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@UNIT", DbType.String, subscriptionFeatureTypes.Unit);
                HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@DEFAULT_VALUE", DbType.String, subscriptionFeatureTypes.DefaultValue);
                HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@ISUNLIMITED", DbType.Int16, subscriptionFeatureTypes.IsUnlimited);
                HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@MODIFIED_BY", DbType.Int32, subscriptionFeatureTypes.ModifiedBy);
                return HCMDatabase.ExecuteNonQuery(UpdateFeatureSubscriptionTypeDbCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateFeatureSubscriptionTypeDbCommand)) UpdateFeatureSubscriptionTypeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// A Method that updates the list of subscription feature types with transaction
        /// </summary>
        /// <param name="subscriptionFeatureTypes">
        /// A List of <see cref="Forte.HCM.DataObjects.SubscriptionFeatureTypes"/> that contains
        /// the records to be updated to the DB
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/> that holds the number of effected records in the DB
        /// </returns>
        public int UpdateFeatureSubscriptionType(List<SubscriptionFeatureTypes> subscriptionFeatureTypes)
        {
            int AffectedRecords = 0;
            DbCommand UpdateFeatureSubscriptionTypeDbCommand = null;
            IDbTransaction transaction = null;
            try
            {
                transaction = new TransactionManager().Transaction;
                for (int i = 0; i < subscriptionFeatureTypes.Count; i++)
                {
                    UpdateFeatureSubscriptionTypeDbCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_FEATURE_TYPES");
                    HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@FEATURE_ID", DbType.Int32, subscriptionFeatureTypes[i].FeatureId);
                    HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@FEATURE_NAME", DbType.String, subscriptionFeatureTypes[i].FeatureName);
                    HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@UNIT", DbType.String, subscriptionFeatureTypes[i].Unit);
                    HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@DEFAULT_VALUE", DbType.String, subscriptionFeatureTypes[i].DefaultValue);
                    HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@ISUNLIMITED", DbType.Int16, subscriptionFeatureTypes[i].IsUnlimited);
                    HCMDatabase.AddInParameter(UpdateFeatureSubscriptionTypeDbCommand, "@MODIFIED_BY", DbType.Int32, subscriptionFeatureTypes[i].ModifiedBy);
                    AffectedRecords += HCMDatabase.ExecuteNonQuery(UpdateFeatureSubscriptionTypeDbCommand, transaction as DbTransaction);
                }
                transaction.Commit();
                return AffectedRecords;
            }
            catch
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction.Rollback();
                throw;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction = null;
                if (!Utility.IsNullOrEmpty(UpdateFeatureSubscriptionTypeDbCommand)) UpdateFeatureSubscriptionTypeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionFeatureTypes"></param>
        /// <returns></returns>
        public int DeleteFeatureSubscriptionType(SubscriptionFeatureTypes subscriptionFeatureTypes)
        {
            DbCommand DeleteFeatureSubscriptionTypeDbCommand = null;
            try
            {
                DeleteFeatureSubscriptionTypeDbCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_SUBSCRIPTION_FEATURE_TYPES");
                HCMDatabase.AddInParameter(DeleteFeatureSubscriptionTypeDbCommand, "@FEATURE_ID", DbType.Int32, subscriptionFeatureTypes.FeatureId);
                HCMDatabase.AddInParameter(DeleteFeatureSubscriptionTypeDbCommand, "@DELETED_BY", DbType.Int32, subscriptionFeatureTypes.DeletedBy);
                return Convert.ToInt32(HCMDatabase.ExecuteScalar(DeleteFeatureSubscriptionTypeDbCommand));
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(DeleteFeatureSubscriptionTypeDbCommand)) DeleteFeatureSubscriptionTypeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Gets the subscription types with the features
        /// </summary>
        /// <returns></returns>
        public List<SubscriptionFeatures> GetSubscriptionWithFeatures(out int freeSubscriptionTrialDays, out int freeStandardSubscriptionTrialDays, out int freeCorporateSubscriptonTrialDays)
        {
            DbCommand GetSubscriptionWithFeaturesDbCommand = null;
            IDataReader dataReader = null;
            List<SubscriptionFeatures> subscriptionFeatures = new List<SubscriptionFeatures>();
            SubscriptionFeatures subscriptionFeature = null;
            try
            {
                freeCorporateSubscriptonTrialDays = 0;
                freeStandardSubscriptionTrialDays = 0;
                freeSubscriptionTrialDays = 0;
                GetSubscriptionWithFeaturesDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_FEATURES_SUBSCRIPTION_TYPES");
                dataReader = HCMDatabase.ExecuteReader(GetSubscriptionWithFeaturesDbCommand);
                while (dataReader.Read())
                {
                    if (Utility.IsNullOrEmpty(subscriptionFeature))
                        subscriptionFeature = new SubscriptionFeatures();
                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_ID"]))
                        subscriptionFeature.FeatureId = Convert.ToInt32(dataReader["FEATURE_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_ID"]))
                        subscriptionFeature.SubscriptionId = Convert.ToInt32(dataReader["SUBSCRIPTION_ID"]);
                    if (!Utility.IsNullOrEmpty(dataReader["SUBSCRIPTION_NAME"]))
                        subscriptionFeature.SubscriptionName = dataReader["SUBSCRIPTION_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_NAME"]))
                        subscriptionFeature.FeatureName = dataReader["FEATURE_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(dataReader["FEATURE_VALUE"]))
                        subscriptionFeature.FeatureValue = dataReader["FEATURE_VALUE"].ToString();
                    if (Utility.IsNullOrEmpty(subscriptionFeatures))
                        subscriptionFeatures = new List<SubscriptionFeatures>();
                    subscriptionFeatures.Add(subscriptionFeature);
                    subscriptionFeature = null;
                }
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    if (!Utility.IsNullOrEmpty(dataReader["FREE_TRIAL_DAYS"]))
                        freeSubscriptionTrialDays = Convert.ToInt32(dataReader["FREE_TRIAL_DAYS"]);
                    if (!Utility.IsNullOrEmpty(dataReader["STANDARD_TRIAL_DAYS"]))
                        freeStandardSubscriptionTrialDays = Convert.ToInt32(dataReader["STANDARD_TRIAL_DAYS"]);
                    if (!Utility.IsNullOrEmpty(dataReader["CORPORATE_TRIAL_DAYS"]))
                        freeCorporateSubscriptonTrialDays = Convert.ToInt32(dataReader["CORPORATE_TRIAL_DAYS"]);
                }
                return subscriptionFeatures;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(GetSubscriptionWithFeaturesDbCommand)) GetSubscriptionWithFeaturesDbCommand = null;
                if (!Utility.IsNullOrEmpty(dataReader) && !dataReader.IsClosed) dataReader.Close();
                if (!Utility.IsNullOrEmpty(dataReader)) dataReader = null;
                if (!Utility.IsNullOrEmpty(subscriptionFeature)) subscriptionFeature = null;
                if (!Utility.IsNullOrEmpty(subscriptionFeatures)) subscriptionFeatures = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Gets all the subscription Types
        /// </summary>
        /// <returns>
        /// A List of subscription types available in the repository
        /// </returns>
        public IList<SubscriptionTypes> GetAllSubscriptionTypes()
        {
            DbCommand GetAllSubscriptionDbCommand = null;
            IDataReader datareader = null;
            IList<SubscriptionTypes> subscriptionTypes = null;
            SubscriptionTypes subscriptionType = null;
            try
            {
                GetAllSubscriptionDbCommand = HCMDatabase.GetStoredProcCommand("SPGET_SUBSCRIPTION_TYPES");
                datareader = HCMDatabase.ExecuteReader(GetAllSubscriptionDbCommand);
                while (datareader.Read())
                {
                    if (Utility.IsNullOrEmpty(subscriptionType))
                        subscriptionType = new SubscriptionTypes();
                    if (!Utility.IsNullOrEmpty(datareader["SUBSCRIPTION_ID"]))
                        subscriptionType.SubscriptionId = Convert.ToInt32(datareader["SUBSCRIPTION_ID"]);
                    if (!Utility.IsNullOrEmpty(datareader["SUBSCRIPTION_NAME"]))
                        subscriptionType.SubscriptionName = datareader["SUBSCRIPTION_NAME"].ToString();
                    if (!Utility.IsNullOrEmpty(datareader["SUBSCRIPTION_DESCRIPTION"]))
                        subscriptionType.SubscriptionDescription = datareader["SUBSCRIPTION_DESCRIPTION"].ToString();
                    if (!Utility.IsNullOrEmpty(datareader["PUBLISHED"]))
                        subscriptionType.Published = Convert.ToInt16(datareader["PUBLISHED"]) == 0 ? "No" :
                            Convert.ToInt16(datareader["PUBLISHED"]) == 1 ? "Yes" : "RePub";
                    if (!Utility.IsNullOrEmpty(datareader["CREATED_BY"]))
                        subscriptionType.CreatedBy = Convert.ToInt32(datareader["CREATED_BY"]);
                    if (!Utility.IsNullOrEmpty(datareader["CREATED_DATE"]))
                        subscriptionType.CreatedDate = Convert.ToDateTime(datareader["CREATED_DATE"]);
                    if (!Utility.IsNullOrEmpty(datareader["MODIFIED_BY"]))
                        subscriptionType.ModifiedBy = Convert.ToInt32(datareader["MODIFIED_BY"]);
                    if (!Utility.IsNullOrEmpty(datareader["MODIFIED_DATE"]))
                        subscriptionType.ModifiedDate = Convert.ToDateTime(datareader["MODIFIED_DATE"]);

                    if (!Utility.IsNullOrEmpty(datareader["ROLES"]))
                        subscriptionType.RoleIDs = datareader["ROLES"].ToString();
                    if (Utility.IsNullOrEmpty(subscriptionTypes))
                        subscriptionTypes = new List<SubscriptionTypes>();
                    subscriptionTypes.Add(subscriptionType);
                    subscriptionType = null;
                }
                return subscriptionTypes;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(datareader) && !datareader.IsClosed) datareader.Close();
                if (!Utility.IsNullOrEmpty(datareader)) datareader = null;
                if (!Utility.IsNullOrEmpty(GetAllSubscriptionDbCommand)) GetAllSubscriptionDbCommand = null;
                if (!Utility.IsNullOrEmpty(subscriptionTypes)) subscriptionTypes = null;
                if (Utility.IsNullOrEmpty(subscriptionType)) subscriptionType = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionType"></param>
        /// <returns></returns>
        public int InsertSubscriptionType(SubscriptionTypes subscriptionType)
        {
            DbCommand InsertSubscriptionTypeDbCommand = null;
            try
            {
                InsertSubscriptionTypeDbCommand = HCMDatabase.GetStoredProcCommand("SPINSERT_SUBSCRIPTION_TYPES");
                HCMDatabase.AddInParameter(InsertSubscriptionTypeDbCommand, "@SUBSCRIPTION_NAME", DbType.String, subscriptionType.SubscriptionName);
                HCMDatabase.AddInParameter(InsertSubscriptionTypeDbCommand, "@SUBSCRIPTION_DESCRIPTION", DbType.String, subscriptionType.SubscriptionDescription);
                HCMDatabase.AddInParameter(InsertSubscriptionTypeDbCommand, "@CREATED_BY", DbType.Int32, subscriptionType.CreatedBy);
                HCMDatabase.AddInParameter(InsertSubscriptionTypeDbCommand, "@ROLE_IDS", DbType.String, subscriptionType.RoleIDs);

                return Convert.ToInt32(HCMDatabase.ExecuteScalar(InsertSubscriptionTypeDbCommand));
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(InsertSubscriptionTypeDbCommand)) InsertSubscriptionTypeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionType"></param>
        /// <returns></returns>
        public int UpdateSubscriptionTypes(SubscriptionTypes subscriptionType)
        {
            DbCommand UpdateSubscriptionTypeDbCommand = null;
            try
            {
                UpdateSubscriptionTypeDbCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_TYPES");
                HCMDatabase.AddInParameter(UpdateSubscriptionTypeDbCommand, "@SUBSCRIPTION_ID", DbType.Int32, subscriptionType.SubscriptionId);
                HCMDatabase.AddInParameter(UpdateSubscriptionTypeDbCommand, "@SUBSCRIPTION_NAME", DbType.String, subscriptionType.SubscriptionName);
                HCMDatabase.AddInParameter(UpdateSubscriptionTypeDbCommand, "@SUBSCRIPTION_DESCRIPTION", DbType.String, subscriptionType.SubscriptionDescription);
                HCMDatabase.AddInParameter(UpdateSubscriptionTypeDbCommand, "@MODIFIED_BY", DbType.Int32, subscriptionType.ModifiedBy);
                HCMDatabase.AddInParameter(UpdateSubscriptionTypeDbCommand, "@ROLE_IDS", DbType.String, subscriptionType.RoleIDs);

                return Convert.ToInt32(HCMDatabase.ExecuteScalar(UpdateSubscriptionTypeDbCommand));
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateSubscriptionTypeDbCommand)) UpdateSubscriptionTypeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionType"></param>
        /// <returns></returns>
        public int DeleteSubscriptionTypes(SubscriptionTypes subscriptionType)
        {
            DbCommand DeleteSubscriptionTypeDbCommand = null;
            try
            {
                DeleteSubscriptionTypeDbCommand = HCMDatabase.GetStoredProcCommand("SPDELETE_SUBSCRIPTION_TYPES");
                HCMDatabase.AddInParameter(DeleteSubscriptionTypeDbCommand, "@SUBSCRIPTION_ID", DbType.Int32, subscriptionType.SubscriptionId);
                HCMDatabase.AddInParameter(DeleteSubscriptionTypeDbCommand, "@DELETED_BY", DbType.Int32, subscriptionType.DeletedBy);
                return Convert.ToInt32(HCMDatabase.ExecuteScalar(DeleteSubscriptionTypeDbCommand));
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(DeleteSubscriptionTypeDbCommand)) DeleteSubscriptionTypeDbCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Method that updates the publish status of the subscription
        /// </summary>
        /// <param name="subscriptionTypes"></param>
        /// <returns></returns>
        public int UpdateSubscriptionPublishStatus(SubscriptionTypes subscriptionTypes)
        {
            DbCommand UpdateSubscriptionPublishStatuDBCommand = null;
            try
            {
                UpdateSubscriptionPublishStatuDBCommand = HCMDatabase.GetStoredProcCommand("SPUPDATE_SUBSCRIPTION_TYPES_PUBLISH_STATUS");
                HCMDatabase.AddInParameter(UpdateSubscriptionPublishStatuDBCommand, "@SUBSCRIPTION_ID", DbType.Int32, subscriptionTypes.SubscriptionId);
                HCMDatabase.AddInParameter(UpdateSubscriptionPublishStatuDBCommand, "@PUBLISHED", DbType.Int16,
                    subscriptionTypes.Published);
                return HCMDatabase.ExecuteNonQuery(UpdateSubscriptionPublishStatuDBCommand);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(UpdateSubscriptionPublishStatuDBCommand)) UpdateSubscriptionPublishStatuDBCommand = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { GC.Collect(); }
            }
        }

        /// <summary>
        /// Method that retrieves the list of users that are expired or about
        /// to expire in the given period of time.
        /// </summary>
        /// <param name="days">
        /// A <see cref="int"/> that holds the number of days about to expire.
        /// </param>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the date on which the relative
        /// expiry day is calculated.
        /// </param>
        /// <returns>
        /// A list of <see cref="UserDetail"/> that holds the user details.
        /// </returns>
        public List<UserDetail> GetExpiryUsers(int days, DateTime date)
        {
            IDataReader dataReader = null;
            List<UserDetail> userDetails = null;

            try
            {
                // Create command.
                DbCommand getdepartmentCommand = HCMDatabase.GetStoredProcCommand
                    ("SPGET_USER_EXPIRY_SUMMARY");

                // Add input parameters.
                HCMDatabase.AddInParameter(getdepartmentCommand, "@DAYS", DbType.Int32, days);
                HCMDatabase.AddInParameter(getdepartmentCommand, "@DATE", DbType.DateTime, date);

                // Execute command.
                dataReader = HCMDatabase.ExecuteReader(getdepartmentCommand);

                while (dataReader.Read())
                {
                    // Instantiate the user details list
                    if (userDetails == null)
                        userDetails = new List<UserDetail>();

                    // Create a user detail object.
                    UserDetail userDetail = new UserDetail();

                    if (!Utility.IsNullOrEmpty(dataReader["USRID"]))
                        userDetail.UserID = Convert.ToInt32(dataReader["USRID"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["USREMAIL"]))
                        userDetail.Email = dataReader["USREMAIL"].ToString().Trim();

                    if (!Utility.IsNullOrEmpty(dataReader["USRUSERNAME"]))
                        userDetail.UserName = dataReader["USRUSERNAME"].ToString().Trim();
                    else
                        userDetail.UserName = string.Empty;

                    if (!Utility.IsNullOrEmpty(dataReader["USRFIRSTNAME"]))
                        userDetail.FirstName = dataReader["USRFIRSTNAME"].ToString().Trim();
                    else
                        userDetail.FirstName = string.Empty;

                    if (!Utility.IsNullOrEmpty(dataReader["USRLASTNAME"]))
                        userDetail.LastName = dataReader["USRLASTNAME"].ToString().Trim();
                    else
                        userDetail.LastName = string.Empty;

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRY_DATE"]))
                        userDetail.ExpiryDate = Convert.ToDateTime(dataReader["EXPIRY_DATE"].ToString().Trim());

                    if (!Utility.IsNullOrEmpty(dataReader["EXPIRES_IN"]))
                        userDetail.ExpiresInDays = Convert.ToInt32(dataReader["EXPIRES_IN"].ToString().Trim());

                    // Add user detail to the list.
                    userDetails.Add(userDetail);
                }

                return userDetails;
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
        }

        /// <summary>
        /// Method that updates the user to the given subscription.
        /// </summary>
        /// <param name="userID">
        /// A <see cref="int"/> that holds the user ID.
        /// </param>
        /// <param name="subcriptionID">
        /// A <see cref="int"/> that holds the subscription ID.
        /// </param>
        /// <param name="subscriptionCode">
        /// A <see cref="string"/> that holds the suscription code.
        /// </param>
        public void UpdateUserSubscription(int userID, int subcriptionID, string subscriptionCode)
        {
            // Create command.
            DbCommand updateSubcriptionCommand = HCMDatabase.
                GetStoredProcCommand("SPUDATE_USER_SUBSCRIPTION");

            // Add input parameters.
            HCMDatabase.AddInParameter(updateSubcriptionCommand,
                "@USER_ID", DbType.Int32, userID);
            HCMDatabase.AddInParameter(updateSubcriptionCommand,
                "@SUBSCRIPTION_ID", DbType.Int32, subcriptionID);
            HCMDatabase.AddInParameter(updateSubcriptionCommand,
                "@SUBSCRIPTION_ROLE", DbType.String, subscriptionCode);

            // Execute command.
            HCMDatabase.ExecuteNonQuery(updateSubcriptionCommand);
        }

        #endregion Public Methods
    }
}
