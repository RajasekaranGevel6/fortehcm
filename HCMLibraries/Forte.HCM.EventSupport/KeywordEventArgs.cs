﻿using System;
using System.Collections.Generic;

using Forte.HCM.DataObjects;

namespace Forte.HCM.EventSupport
{
    public class KeywordEventArgs : EventArgs
    {
        public List<TechnicalSkillDetails> TechnicalSkills {set ; get ;}
        public List<VerticalDetails> Verticals { set; get; }
        public string PrimaryRole { set; get; }
        public string SecondaryRole { set; get; }
    }
}
