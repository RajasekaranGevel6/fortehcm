﻿using System;

using Forte.HCM.DataObjects;

namespace Forte.HCM.EventSupport
{
    public class QuestionTypeEventArgs : EventArgs
    {
        public QuestionType QuestionType { set; get; }

        public QuestionTypeEventArgs(QuestionType questionType)
        {
            this.QuestionType = questionType;
        }
    }
}