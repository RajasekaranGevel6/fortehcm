﻿#region Header
// Copyright (C) 2010-2011 Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PageNumberEventArgs.cs
// This class provides consist of the event data for the PageNumberClick
// event which is present in the PageNavigator user control.
//
#endregion

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.EventSupport
{
    [Serializable]
    public class PageNumberEventArgs : EventArgs
    {
        /// <summary>
        /// A <see cref="int"/> that holds the page number.
        /// </summary>
        private int pageNumber;

        /// <summary>
        /// A <see cref="int"/> that holds the cancel status.
        /// </summary>
        private bool cancel;

        /// <summary>
        /// Represents the constructor.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        public PageNumberEventArgs(int pageNumber)
        {
            this.pageNumber = pageNumber;
        }

        /// <summary>
        /// Represents the property that retrieves the page number.
        /// </summary>
        public int PageNumber
        {
            get
            {
                return pageNumber;
            }
        }

        /// <summary>
        /// Represents the property that sets or retrieves the cancel status.
        /// </summary>
        /// <remarks>
        /// This cancel status determines whether to cancel the paging event.
        /// </remarks>
        public bool Cancel
        {
            set
            {
                cancel = value;
            }
            get
            {
                return cancel;
            }
        }
    }    
}
