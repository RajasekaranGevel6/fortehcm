﻿using System;

using Forte.HCM.DataObjects;

namespace Forte.HCM.EventSupport
{
    public class ControlMessageEventArgs : EventArgs
    {
        public string Message { set; get; }
        public MessageType MessageType { set; get; }

        public ControlMessageEventArgs(string message, MessageType messageType)
        {
            this.Message = message;
            this.MessageType = messageType;
        }
    }
}
