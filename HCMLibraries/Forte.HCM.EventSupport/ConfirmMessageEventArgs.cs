﻿using System;

using Forte.HCM.DataObjects;

namespace Forte.HCM.EventSupport
{
    public class ConfirmMessageEventArgs : EventArgs
    {
        public ButtonType ButtonType { set; get; }

        public ConfirmMessageEventArgs(ButtonType ButtonType)
        {
            this.ButtonType = ButtonType;
        }
    }
}
