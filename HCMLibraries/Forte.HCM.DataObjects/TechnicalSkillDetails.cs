﻿#region Header                                                       

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TechnicalSkillDetails.cs
// File that represents details for the technical skills

#endregion Header

#region Directives                                                   

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class that holds the technical skill details
    /// </summary>
    public class TechnicalSkillDetails
    {
        public string CategoryName { set; get; }
        public string SkillName { set; get; }
        public string NoOfYearsExp { set; get; }
        public string SkillRequired { set; get; }
        public string IsMandatory { set; get; }
        public string CertificationRequired { set; get; }
        public string IsTestingRequired { set; get; }
        public string IsInterviewRequired { set; get; }
        public string Weightage { set; get; }

        public TechnicalSkillDetails()
        {
        }

        public TechnicalSkillDetails(string categoryName, string skillName)
        {
            this.CategoryName = categoryName;
            this.SkillName = skillName;
        }
    }
}
