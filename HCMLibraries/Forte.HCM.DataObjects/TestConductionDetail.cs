﻿using System;

namespace Forte.HCM.DataObjects
{
    public class TestConductionDetail
    {
        public string CandidateSessionID { set; get; }
        public string TestID { set; get; }
        public DateTime StartTime { set; get; }
        public DateTime EndTime { set; get; }
        public TestCompletedStatus TestCompletionStatus { set; get; }
    }
}
