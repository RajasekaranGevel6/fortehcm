﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfileDetail:TrackingDetail
    {
        public int PositionID { set; get; }
        public string ClientName { set; get; }
        public string PositionName { set; get; }
        public string Date { set; get; }
        public int NoOfPositions { set; get; }
        public int NoOfAssessedCandidates { set; get; }
        public string IsOpened { set; get; }
        public int RegisteredBy { set; get; }
        public string RegisteredByName { set; get; }
        public DateTime RegistrationDate { set; get; }
        public DateTime SubmittalDate { set; get; }
        public List<SegmentDetail> Segments { set; get; }
        public int PositionProfileID { set; get; }
        public string ClientPositionName { set; get; }
        public string PositionProfileName { set; get; }
        public PositionProfileKeyword PositionProfileKeywords { set; get; }
        public string PositionProfileKey { set; get; }
        public List<PositionProfileKeywordDictionary> PositionProfileDictinaryKeywords { set; get; }
        public List<SkillWeightage> SkillWeightageList { set; get; }
        public string PositionProfileAdditionalInformation { set; get; }
        public string PositionProfileStatus { set; get; }
        public string PositionProfileStatusName { set; get; }
        public bool HasOwnerRights { set; get; }
        public string PositionProfileOwnerAction { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the keyword for position profile
        /// </summary>
        public string Keys { set; get; }
       
        public int DepartmentContactID { get; set; }

        public int DepartmentID { get; set; }

        public int ClientContactID { get; set; }

        public int ClientID { get; set; }

        public string Department_Client_Contact_ID { get; set; }

        public string Closed { set; get; }
        public string DepartmentName { set; get; }
        public string ContactName { set; get; }

        // Test details.
        public string TestName { set; get; }
        public string TestDescription { set; get; }
        public string PositionProfileOwnerName { set; get; }
        public string PositionProfileOwnerEmail{ set; get; }
        public List<int> DepartmentIds { set; get; }
        public List<string> ContactIds { set; get; }
        public string CreatedByName { set; get; }

        //Interview details
        public string InterviewName { set; get; }
        public string InterviewDescription { set; get; }

        public ClientInformation ClientInformation { set; get; }

        public char IsEditable{ set; get; }
        public string TaskOwnerName { set; get; }
        public string TaskOwnerEMail { set; get; }
        public string TaskOwnerType { set; get; }
    }
}
