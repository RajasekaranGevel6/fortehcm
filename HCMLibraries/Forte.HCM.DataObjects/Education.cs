﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Education.cs
// File that represents the class which contains candidate educational 
// information details.

#endregion Header                                                              

using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains EducationId, DegreeName, SchoolName, SchoolLocation, GraduationDate, 
    /// Specialization and GPA members.
    /// </summary>
    [Serializable]
    public class Education
    {
        public int EducationId { set; get; }
        public string DegreeName {set; get;}
        public string SchoolName {set; get;}
        public Location SchoolLocation;
        public DateTime GraduationDate {set; get;}
        public string Specialization {set; get;}
        public string GPA {set; get;}
       
    }
}
