﻿using System;
namespace Forte.HCM.DataObjects
{
	[Serializable]
	public class Subject : TrackingDetail
	{
		public int CategoryID { set; get; }
		public int SubjectID { set; get; }
		public string SubjectName { set; get; }
		public string CategoryName { set; get; }
		public bool IsSelected { set; get; }
        // Used in category subject control to pass to getsubjects method
        // inside the rowdatabound 
        public string QuestionKey { set; get; }

        /// <summary>
        /// Question count for a specific subject. Used in General Test
        /// information in design report
        /// </summary>
        public int QuestionCount { set; get; }

		public Subject()
		{ }

		public Subject(int categoryID, string categoryName)
		{
			this.CategoryID = categoryID;
			this.CategoryName = categoryName;
		}

        public Subject(string subjectName,int subjectID)
        {
            this.SubjectName = subjectName;
            this.SubjectID = subjectID;
        }

		public Subject(int categoryID, int subjectID, string SubjectName, int createdBy, DateTime createdDate, int modifiedBy, DateTime modifiedDate)
		{
			this.CategoryID = categoryID;
			this.SubjectID = subjectID;
			this.SubjectName = SubjectName;
			this.CreatedBy = createdBy;
			this.CreatedDate = createdDate;
			this.ModifiedBy = modifiedBy;
			this.ModifiedDate = modifiedDate;
		}
	}
}
