﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfileDetailSearchCriteria
    {
        public string ClientName { set; get; }
        public string PositionID { set; get; }
        public string PositionName { set; get; }
        public string NumberOfPositions { set; get; }
        public string NatureOfPosition { set; get; }
        public string TargetedStartDate { set; get; }
        public string Location { set; get; }
        public string ContractToHire { set; get; }
        public string Duration { set; get; }
        public string PositionProfileName { set; get; }
        public string DepartmentName { set; get; }
        public string ContactName { set; get; }

        public int ClientID { set; get; }
        public int DepartmentID { set; get; }
        public int ContactID { set; get; }
        public int CreatedBy { set; get; }
        public string CreatedByName { set; get; }
        public string ShowType { set; get; }
        public bool MyProfile { set; get; }

        public string searchType { set; get; }
    }
}
