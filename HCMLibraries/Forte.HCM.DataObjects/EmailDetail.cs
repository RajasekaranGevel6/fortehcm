﻿namespace Forte.HCM.DataObjects
{
    public class EmailDetail
    {
        public string DisplayName { set; get; }
        public string Email { set; get; }
    }
}
