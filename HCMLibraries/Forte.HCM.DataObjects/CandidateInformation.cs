﻿using System;
using ReflectionComboItem;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class CandidateInformation
    {
        public int caiID { set; get; }
        public int cscID { set; get; }
        public int ctyID { set; get; }
        public string caiEmployeeNo { set; get; }
        public string caiCandidateNo { set; get; }
        public bool caiIsEmployee { set; get; }
        public int salID { set; get; }
        [DropDownItemAttribute("Candidate Name", "")]
        public string caiFirstName { set; get; }
        public string caiMiddleName { set; get; }
        public string caiLastName { set; get; }
        public string caiSSN { set; get; }
        public int caiTermType { set; get; }
        public string caiLocation { set; get; }
        public int? caiGender { set; get; }
        public DateTime? caiDOB { set; get; }
        public DateTime caiWedAnniversary { set; get; }
        public string caiW4Deductions { set; get; }
        public int caiRecruitedBy { set; get; }
        public int caiReferredBy { set; get; }
        public string caiAddress { set; get; }
        public int staID { set; get; }
        public string caiZip { set; get; }
        public int couID { set; get; }
        public string caiHomePhone { set; get; }
        public string caiCell { set; get; }
        public string caiWorkPhone { set; get; }
        public string caiExtension { set; get; }
        public string caiFax { set; get; }
        public string caiEmail { set; get; }
        public string caiEmergencyContactName { set; get; }
        public string caiEmergencyContactNo { set; get; }
        public string caiAltEmail { set; get; }
        public decimal caiC2CCost { set; get; }
        public string caiPassportNo { set; get; }
        public DateTime caiPassportIssueDate { set; get; }
        public string caiPassportIssuePlace { set; get; }
        public DateTime caiPassportExpDate { set; get; }
        public int caiCurrentVisaStatus { set; get; }
        public string caiVisaNo { set; get; }
        public DateTime caiVisaIssueDate { set; get; }
        public DateTime caiVisaValidTill { set; get; }
        public string caiVisaPlaceOfStamping { set; get; }
        public string caiPortOfImmigration { set; get; }
        public DateTime caiImmigrationDate { set; get; }
        public int caiDesiredVisaStatus { set; get; }
        public DateTime caiProjectAvailabilityDate { set; get; }
        public int caiTotalYearsOfWorkExp { set; get; }
        public int caiTotalYearsOfITExp { set; get; }
        public int caiCreatedBy { set; get; }
        public DateTime caiCreatedDate { set; get; }
        public int caiModifiedBy { set; get; }
        public DateTime caiModifiedDate { set; get; }
        public int  caiCity { set; get; }
        public DateTime caiDateOfEmployment { set; get; }
        public string caiPhoto { set; get; }
        public string caiPhotoFileName { set; get; }
        public decimal caiDefaultRate { set; get; }
        public int caiDefaultRatePer { set; get; }
        
        public char caiEmployee { set; get; }
        public bool caiLimitedAccess { set; get; }

        public string caiWorkAuthorization { set; get; }
        public string caiWorkAuthorizationOther { set; get; }

        public byte[] ResumeContent { set; get; }
        public string UserName { set; get; }
        public int tenantID { set; get; }
        public string Password { set; get; }
        public string SortField { set; get; }

        public SortType SortOrder { set; get; }
        public int PageNumber { set; get; }
        public bool IsMaximized { set; get; }

        public bool caiIsValid { set; get; }
        public string caiInvalidCandidatesRemarks { set; get; }

        public string caiType{ set; get; }

        //Related to Position Profile candidate

        public int PositionProfileStatusID { set; get; }
        public int PositionProfileID { set; get; }
        public string  PositionName { set; get; }

        public int RecruiterID { set; get; }
         [DropDownItemAttribute("Recruiter Name", "")]
        public string RecruiterName { set; get; }
        [DropDownItemAttribute("Test Score", "0.00")]
        public decimal TestScore { set; get; }

        [DropDownItemAttribute("Resume Score", "0.00")]
        public decimal ResumeScore { set; get; }

        [DropDownItemAttribute("Interview Score", "0.00")]
        public decimal InterviewScore { set; get; }

        public DateTime ScheduledDate{ set; get; }

        [DropDownItemAttribute("Scheduler Name", "")]
        public string SchedulerName { set; get; }

        [DropDownItemAttribute("Test Status", "")]
        public string Status { set; get; }

        [DropDownItemAttribute("Source From", "")]
        public string SourceFrom { set; get; }
        public string candidateSessionID { set; get; }
        public string TestKey { set; get; }
        public Int16 AttemptID { set; get; }
        public Int16 InterviewAttemptID { set; get; }
        public string InterviewCandidateSessionID { set; get; }
        public bool IsTestScore { set; get; }
        public char caiIsActive { get; set; }
        public string caiCityName { get; set; }
        public string caiStateName { get; set; }
        public string caiCountryName { get; set; }
        public string caiOpenEmailID { get; set; }
        public string caiSelfSignIn { set; get; }
        public int CandidateResumeID { set; get; }
        public string SkillDetail { set; get; }
        public int SkillID { set; get; }

        public string InterviewTotalWeightedScore { set; get; }
        public string InterviewTotalScore { set; get; }
        public string InterviewStatus { set; get; }

        public string LinkedInProfile { set; get; }

        public DateTime FromDate{ set; get; }
        public DateTime ToDate { set; get; }

        public string CandidateStatus { set; get; }
        public string CandidateStatusComments { set; get; }

        public string SearchType { set; get; }
        public string Keyword { set; get; }
        public int CandidateLoginID { set; get; }

        public DateTime keywordFromDate { set; get; }
        public DateTime keywordToDate { set; get; }
        public int keywordUserID { set; get; }
        public string OnlineInterviewStatus { set; get; }
        public int CandidateOnlineInterviewID { set; get; }
        public string OnlineInterviewTotalScore { set; get; }
        public string ChatRoomKey { set; get; }
        public string OnlineInterviewKey { set; get; }
    }
}
