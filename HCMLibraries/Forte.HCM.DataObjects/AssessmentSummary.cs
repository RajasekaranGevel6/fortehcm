﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    public class AssessmentSummary : TrackingDetail
    {
        public string CanidateName { get; set; }

        public int CandidateID { get; set; }
        public int CandidateInfoID { get; set; }

        public int ClientID { set; get; }
        public string ClientName { get; set; }
        public string ClientDepartments { get; set; }
        public string ClientContacts { get; set; }

        public string PositionProfileName { get; set; }
        public int PositionProfileID { get; set; }

        public string AssessmentStatus { get; set; }

        public decimal OverallRating { get; set; }

        public string Comments { get; set; }

        public DateTime? CompletedOn { get; set; }

        public string AssessorAssessmentStatus { get; set; }

        public string InterviewName { get; set; }

        public DateTime InterviewDate { set; get; }

        public DateTime ScheduledDate { set; get; }

        public List<QuestionDetail> QuestionDetails { get; set; }

        public List<AssessorDetail> AssessorDetails { get; set; }
    }
}
