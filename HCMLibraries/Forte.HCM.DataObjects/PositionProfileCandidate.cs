﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfileCandidate : TrackingDetail
    {
        public int PositionProfileID { set; get; }
        public int CandidateID { set; get; }
        public int? PickedBy { set; get; }
        public DateTime ? PickedDate { set; get; }
        public string Status { set; get; }
        public DateTime ? SchelduleDate { set; get; }
        public int? ResumeScore { set; get; }
        public string CandidateSessionID { set; get; }
        public int CandidateOnlineInterviewID { set; get; }
        public int? AttemptID { set; get; }
        public string SourceFrom { set; get; }
        public string CandidateStatus { set; get; }
        public int RecruiterID{ set; get; }

        public string RecruiterFirstName { set; get; }
        public string RecruiterName { set; get; }
        public string RecruiterEMail { set; get; }
        public string PositionProfileName { set; get; }
        public string ClientName { set; get; }
        public string CandidateStatusName { set; get; }
        public string CandidateName { set; get; }
    }
}
