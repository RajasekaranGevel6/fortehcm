﻿namespace Forte.HCM.DataObjects
{
    public class KeywordSkill
    {
        public string SkillName { set; get; }
        public string SkillCategory { set; get; }
        public string SkillType { set; get; }
    }
}