﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateReportSearchCriteria.cs
// File that represents the Properties for candidate search criteria detail

#endregion Header

#region Directives

using System;
using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// File that represents the Properties for candidate search criteria detail.
    /// </summary>
    [Serializable]
    public class CandidateReportSearchCriteria
    {
        public string CandidateFirstName { set; get; }
        public string CandidateLastName { set; get; }
        public string CandidateNameEmail { set; get; }
        public string CategoryName { set; get; }
        public string SubjectName { set; get; }
        public string Keyword { set; get; }
        public int CandidateCurrentPage { set; get; }
        public int CategoryCurrentPage { set; get; }
        public List<UserDetail> CandidateDetailList { set; get; }
        public List<Subject> SubjectDetailList { set; get; }
    }
}
