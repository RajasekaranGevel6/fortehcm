﻿using System;

namespace Forte.HCM.DataObjects
{
   [Serializable]
   public class ClientPositionDetails
    {
         public string ClientName { set; get; }
         public string PositionID { set; get; }
         public string PositionName { set; get; }
         public string NumberOfPositions { set; get; }
         public string NatureOfPosition { set; get; }
         public string TargetedStartDate { set; get; }
         public string Location { set; get; }
         public string ContractToHire { set; get; }
         public string Duration { set; get; }
         public string PositionProfileName { set; get; }
    }
}
