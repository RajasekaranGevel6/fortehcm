﻿using System;

namespace Forte.HCM.DataObjects
{
    public class CandidateActivityLog
    {
        public int CandidateID { set; get; }
        public string CandidateName { set; get; }
        public string Comments { set; get; }
        public DateTime Date { set; get; }
        public string CandidateOwnerName {set; get;}
        public string CandidateOwnerEmail {set; get;}
        public string NotesAddedByName { set; get; }
        
        // Search criterial & results.
        public DateTime ActivityDate { set; get; }
        public int ActivityBy { set; get; }
        public string ActivityByName { set; get; }
        public string ActivityType { set; get; }
        public string ActivityTypeName { set; get; }

        // Mail settings.
        public bool SendMail { set; get; }
    }
}
