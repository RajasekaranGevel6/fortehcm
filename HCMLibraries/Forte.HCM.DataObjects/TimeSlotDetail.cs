﻿namespace Forte.HCM.DataObjects
{
    public class TimeSlotDetail
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public string FromTo { set; get; }

        public TimeSlotDetail()
        {
        }

        public TimeSlotDetail(int id, string name)
        {
            this.ID = id;
            this.Name = name;
        }
    }
}
