﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TestArea : StatisticsDetail
    {
        public string ID { set; get; }
        public string Name { set; get; }

        public TestArea(string id, string name)
        {
            this.ID = id;
            this.Name = name;
        }
    }
}
