﻿namespace Forte.HCM.DataObjects
{
    public class TestResult
    {
        public string TestID { set; get; }
        public string TestName { set; get; }
        public string TestDescription { set; get; }
        public string CandidateID { set; get; }
        public string CandidateName { set; get; }
        public int TotalQuestions { set; get; }
        public int QuestionsAttended { set; get; }
        public int QuestionsSkipped { set; get; }
        public int AnsweredCorrectly { set; get; }
        public int AnsweredWrongly { set; get; }
        public int CreditsEarned { set; get; }
    }
}