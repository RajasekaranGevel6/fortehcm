﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientRequestSearchCriteria.cs
// File that represents the properties for the client request search criteria.

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the property list for the client request search criteria.This class inherits
    /// client request detail.
    /// </summary>
    public class ClientRequestSearchCriteria : ClientRequestDetail
    {
        public string Keyword { set; get; }
        public string Location { set; get; }
        public string CreatedDateStr { set; get; }
    }
}
