﻿using System;
namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class CertificationTitle : TrackingDetail
    {
        public int ID { set; get; }
        public int VectorID { set; get; }
        public string VectorName { set; get; }
        public string GroupName { set; get; }
        public string Title { set; get; }
        
    }
}
