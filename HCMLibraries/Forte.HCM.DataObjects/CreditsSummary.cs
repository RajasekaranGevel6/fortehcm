﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreditsSummary.cs
// File that represents the properties for credits summary detail.

#endregion Header
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for credits summary detail.
    /// </summary>
    public class CreditsSummary
    {
        /// <summary>
        /// String Property that holds User ID
        /// </summary>
        public string UserID {set; get;}
        /// <summary>
        /// List property that holds credits usage history
        /// </summary>
        public List<CreditUsageDetail> CreditUsageHistory {set; get;}
        /// <summary>
        /// Decimal property that holds available credits
        /// </summary>
        public decimal AvailableCredits {set; get;}
        /// <summary>
        /// Decimal property that holds credits earned
        /// </summary>
        public decimal CreditsEarned { set; get; }
        /// <summary>
        /// Decimal property that holds credits redeemed
        /// </summary>
        public decimal CreditsRedeemed { set; get; }
    }
}
