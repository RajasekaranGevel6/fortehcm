﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class MailContact
    {
        public int RowID { set; get; }
        public string FullName { set; get; }
        public string Company { set; get; }
        public string EmailID { set; get; }
        public bool Selected { set; get; }

        public MailContact()
        {
        }

        public MailContact(string fullName, string company, string emailID)
        {
            this.FullName = fullName;
            this.Company = company;
            this.EmailID = emailID;
        }
    }
}
