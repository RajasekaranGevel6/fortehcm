﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Authenticate.cs
// File that represents the properties for authentication.

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents properties for authentication.
    /// </summary> 
    public class Authenticate
    {
        #region Properties

        public string UserName { get; set; }
        public string Password { get; set; }

        #endregion Properties
    }
}
