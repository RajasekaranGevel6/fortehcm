﻿namespace Forte.HCM.DataObjects
{
    public class TestIntroductionDetail
    {
        public string TestSessionID { set; get; }
        public string CandidateSessionID { set; get; }
        public string TestID { set; get; }
        public string TestName { set; get; }
        public string TestDescription { set; get; }
        public string TestAreas { set; get; }
        public string TestSubjects { set; get; }
        public int TimeLimit  { set; get; }
        public string TestInstructions { set; get; }
    }
}
