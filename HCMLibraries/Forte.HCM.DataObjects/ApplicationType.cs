﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public enum ApplicationType
    {
        None = 0,
        OnlineTestingModule = 1,
        TalentScout = 2,
        PositionProfile = 3,
        CandidateRepository = 4,
        ResumeRepository = 5
    }
}
