﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class InterviewTestSessionDetail : TrackingDetail
    {      
        public string TestID { set; get; }
        public string TestSessionID { set; get; }
        public string TestName { set; get; }
        public string TestDescription { set; get; }
        public int NumberOfQuestions { set; get; }
        public bool IsCertification { set; get; }
        public string Description { set; get; }
        public int NumberOfCandidateSessions { set; get; }
        public decimal TotalCredit { set; get; }
        public string ClientRequestID { set; get; }
        public int TimeLimit { set; get; }
        public int RecommendedTimeLimit { set; get; }
        public DateTime ExpiryDate { set; get; }
        public string ExpiryDateFormatted
        {
            get
            {
                if (ExpiryDate == DateTime.MinValue)
                    return string.Empty;

                return ExpiryDate.ToString("MM/dd/yyyy");

            }
        }
        public bool IsRandomizeQuestionsOrdering { set; get; }
        public bool IsDisplayResultsToCandidate { set; get; }
        public bool IsCyberProctoringEnabled { set; get; }
        public int CandidateAlloted { set; get; }
        public string Instructions { set; get; }
        public string TestSessionDesc { set; get; }
        public string TestSessionAuthor {set; get;}
        public string TestSessionAuthorFullName { set; get; }
        public string TestSessionAuthorEmail { set; get; }
        public List<CandidateTestSessionDetail> CandidateTestSessions {set; get;}
        public string UserName { set; get; }
        public string Email { set; get; }
        public string CandidateSessions { set; get; }
        public char AllowPauseInterview { set; get; }        
        public int PositionProfileID { set; get; }
        public string PositionProfileName { set; get; }
        public string AssessorIDs { get; set; }
    }
}
