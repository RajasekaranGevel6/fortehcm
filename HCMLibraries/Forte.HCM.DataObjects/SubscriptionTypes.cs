﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SubscriptionTypes.cs
// File that represents the properties about the Subscription type details.

#endregion Header

#region Namespace

using System;

#endregion Namespace

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represent the property list for the SUBSCRIPTION_TYPES.
    /// </summary>
    [Serializable]
    public class SubscriptionTypes
    {

        #region public Properties
        /// <summary>
        /// A <see cref="System.Int32"/> that holds the Id of the Subscription
        /// </summary>
        public int SubscriptionId { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the name of the Subscription
        /// </summary>
        public string SubscriptionName { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the description of the Subscription
        /// </summary>
        public string SubscriptionDescription { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the published status of the Subscription
        /// </summary>
        public string Published { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the created user id of the Subscription
        /// </summary>
        public int CreatedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the created date of the Subscription
        /// </summary>
        public DateTime CreatedDate { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the modified user id of the Subscription
        /// </summary>
        public int ModifiedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the modified date of the Subscription
        /// </summary>
        public DateTime ModifiedDate { set; get; }

        /// <summary>
        /// A <see cref="System.Boolean"/> that holds the whether the subscription is deleted or not
        /// </summary>
        public bool IsDeleted { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the delted user id of the subscription
        /// </summary>
        public int DeletedBy { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the concatenated values
        /// of subscription id and published status
        /// </summary>
        public string ConcatenatedSubscriptionIdPublishStatus
        {
            get
            {
                return SubscriptionId + "," + Published;
            }
        }

        public string RoleIDs { set; get; }

        #endregion public Properties

    }
}
