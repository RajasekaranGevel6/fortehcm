﻿using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that holds the failed email detail.
    /// </summary>
    public class FailedEmailDetail
    {
        public int FailureID {set; get;}
        public string FromEmailID {set; get;}
        public string ToEmailID {set; get;}
        public string CCEmailID {set; get;}
        public DateTime ReportedDate {set; get;}
        public string FailureMessage {set; get;}
        public string MailSubject {set; get;}
        public string MailMessage {set; get;}
        public bool IsHtml {set; get;}
        public int Attempt {set; get;}
    }
}
