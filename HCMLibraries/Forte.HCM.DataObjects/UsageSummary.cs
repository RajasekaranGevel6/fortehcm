﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// UsageSummary.cs
// File that represents the properties about the usage summary details.

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represent the property list for the usage summary.
    /// </summary>
    public class UsageSummary
    {
        #region Properties                                                     
        public int SubscriptionID { get; set; }

        public string SubscriptionName { get; set; }

        public string SusbscribedOn { get; set; }

        public string Company { get; set; }

        public string Title { get; set; }

        public int FeatureID { get; set; }

        public string FeatureName { get; set; }

        public string FeatureLimit { get; set; }

        public string FeatureUsed { get; set; } 

        public string FeatureYear { get; set; }

        public string MonthYear { get; set; }

        public string PPID { get; set; }

        public string PositionProfileName { get; set; }

        public string ClientName { get; set; }

        public string PositionName { get; set; }

        public string CreatedDate { get; set; }

        public string FID { get; set; }

        public string FormName { get; set; }

        public string FormTag { get; set; }

        public string FormSegments { get; set; }

        public string SearchTerms { get; set; }

        public string TotalCanditates { get; set; }

        public string CandidateName { get; set; }

        public string SearchTerm { get; set; }
        #endregion Properties                                                  
    }
}
