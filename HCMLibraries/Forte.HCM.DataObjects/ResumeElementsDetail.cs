﻿#region Namespace
using System;
#endregion Namespace

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the resume element details
    /// </summary>
    [Serializable]
    public class ResumeElementsDetail
    {
        #region Public Properties

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume node name
        /// </summary>
        public string Node { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume element name
        /// </summary>
        public string ElementName { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume element type name
        /// </summary>
        public string ElementTypeName { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume validate expression
        /// </summary>
        public string ValidateExprType { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds
        /// the resume validate expression value
        /// </summary>
        public int ValidateExprValue { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume validate reason
        /// </summary>
        public string ValidateReason { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds
        /// the resume element id
        /// </summary>
        public int ElementId { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds
        /// the resume element id which is already assigned
        /// </summary>
        public int ElementIdAssigned { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume field description
        /// </summary>
        public string Description { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the xml node value
        /// </summary>
        public string XmlNodeValue { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds
        /// the resume element id which is already assigned
        /// </summary>
        public int RowId { set; get; }

        #endregion Public Properties
    }
}
