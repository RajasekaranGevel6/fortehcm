﻿using System;
namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class StatisticsDetail
    {
        public int QuestionsCount {set; get;}
        public decimal Percentage {set; get;}
    }
}
