﻿using System;

namespace Forte.HCM.DataObjects
{
    public class QuestionStatisticsDetail : QuestionDetail
    {
        public decimal AverageTimeTakenWithinTest { set; get; }
        public decimal AverageTimeTakenAcrossTest { set; get; }
        public int NoOfTimesAnsweredCorrectly { set; get; }
        public int NoOfTimesAdministered { set; get; }
        public string Ratio { set; get; }
    }
}
