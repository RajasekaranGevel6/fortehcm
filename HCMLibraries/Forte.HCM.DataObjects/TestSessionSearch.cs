﻿namespace Forte.HCM.DataObjects
{
    public class TestSessionSearch
    {
        public string TestSessionID { set; get; }
        public string TestKey { set; get; }
        public string CandidateSessionID { set; get; }
        public string ClientRequestNumber { set; get; }
        public string DateofPurchase { set; get; }
        public string Credits { set; get; }
        public int AdministeredID { set; get; }
        public string AdministeredBy { set; get; }
        public string TestAuthorFullName { set; get; }
        public int CandidateID { set; get; }
        public string CandidateName { set; get; }
        public string CandidateFullName { set; get; }
        public object DateofTest { set; get; }
        public bool CandidateVisibleStatus { set; get; }
        public string CancelReason { set; get; }
        public bool ShowCancelIcon { set; get; }
        public int AttemptID { set; get; }
        public bool ShowTestScore { set; get; }
    }
}
