﻿using System;

namespace Forte.HCM.DataObjects
{
    public class ResumeVersion
    {
        public int CandidateResumeID { set; get; }
        public string ProfileName { set; get; }
        public DateTime UploadedDate { set; get; }
    }
}
