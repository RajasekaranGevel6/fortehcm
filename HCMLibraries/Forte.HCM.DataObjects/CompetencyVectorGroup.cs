﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CompetencyVector.cs
// File that represents the data object which holds the competency vector
// group details.

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class the holds the competency vector group details.
    /// </summary>
    public class CompetencyVectorGroup
    {
        /// <summary>
        /// A <see cref="string"/> that holds the user ID.
        /// </summary>
        public int UserID { set; get; }

        /// <summary>
        /// A <see cref="int"/> that holds competency vector group ID.
        /// </summary>
        public int GroupID { set; get; }

        /// <summary>
        /// A <see cref="string"/> that holds the competency vector group name.
        /// </summary>
        public string GroupName { set; get; }

        public string VectorGroupName { set; get; }

        public string VectorGroupID { set; get; }
    }
}
