﻿using System;
using Forte.HCM.Support;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfieWorkflowDetail
    {
        public List<InterviewDetail> InterviewDetail { set; get; }
        public List<InterviewSessionDetail> InterviewSessionDetail { set; get; }
        public List<TestDetail> TestDetail { set; get; }
        public List<TestSessionDetail> TestSessionDetail { set; get; }
        public List<CandidateInformation> SubmittedCandidateInformation { set; get; }
        public int InterviewAssessedCnt { set; get; }
        public bool IsInterviewRequired { set; get; }
        public bool IsTestRequired { set; get; }
    }
}
