﻿namespace Forte.HCM.DataObjects
{
    public enum SegmentType
    {
        None = 0,
        ClientPositionDetail = 1,
        TechnicalSkills = 2,
        Verticals = 3,
        Roles = 4,
        InterpersonalSkills = 5,
        DecisionMaking = 6,
        CulturalFit = 7,
        Education = 8,
        CommunicationSkills = 9,
        Screening = 10
    }
}
