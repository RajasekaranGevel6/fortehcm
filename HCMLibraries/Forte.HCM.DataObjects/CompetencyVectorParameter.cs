﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CompetencyVectorParameter.cs
// File that represents the data object which holds the competency vector
// parameter details.

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class the holds the competency vector parameter details.
    /// </summary>
    public class CompetencyVectorParameter
    {
        /// <summary>
        /// A <see cref="string"/> that holds the user ID.
        /// </summary>
        public int UserID { set; get; }

        /// <summary>
        /// A <see cref="string"/> that holds the competency vector parameter.
        /// ID.
        /// </summary>
        public int ParameterID { set; get; }

        /// <summary>
        /// A <see cref="string"/> that holds the competency vector parameter 
        /// name.
        /// </summary>
        public string ParameterName { set; get; }
    }
}
