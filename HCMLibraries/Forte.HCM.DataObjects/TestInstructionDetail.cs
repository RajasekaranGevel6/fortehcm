﻿using System;

namespace Forte.HCM.DataObjects
{
    public class TestInstructionDetail
    {
        public string Instructions { set; get; }
        public DateTime TimeLimit { set; get; }
        public bool IsRandomizeQuestionsOrdering { set; get; }
        public bool IsDisplayResultsToCandidate { set; get; }
        public bool IsCyberProctoringEnabled { set; get; }
    }
}
