﻿using System;

namespace Forte.HCM.DataObjects
{
    public class RecruiterAssignmentDetail
    {
        public int PositionProfileID { set; get; }
        public string PositionProfileName { set; get; }
        public string ClientName { set; get; }
        public int ClientID { set; get; }
        public string PositionProfileStatus { set; get; }
        public string PositionProfileStatusName { set; get; }
        public DateTime AssignedDate { set; get; }
        public int CandidatesAssociatedByRecruiter { set; get; }
        public int CandidatesSubmittedToClientByRecruiter { set; get; }
        public string PositionProfileOwner { set; get; }

        public string AssignedDateString
        {
            get
            {
                if (this.AssignedDate == DateTime.MinValue)
                    return string.Empty;

                return this.AssignedDate.ToString("MM/dd/yyyy");
            }
        }
    }
}
