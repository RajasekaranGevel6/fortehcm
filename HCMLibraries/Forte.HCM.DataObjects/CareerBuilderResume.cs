﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CareerBuilderResume.cs
// File that represents the class which contains CareerBuilder Resume details.

#endregion Header                                                              

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for CareerBuilderResume detail.
    /// </summary>
    public class CareerBuilderResume
    {
        public int CareerBuilderID { set; get; }
        public string ResumeID { set; get; }
        public string ResumeDetails { set; get; }
        public string ResumeTitle { set; get; }
        public byte[] ResumeContent { set; get; }
        public string ResumeText { set; get; }
        public string CandidateFirstName { set; get; }
        public string CandidateLastName { set; get; }
        public bool ISDocument { set; get; }
        public string CandidateID { set; get; }
    }
}
