﻿using System.Xml;

namespace Forte.HCM.DataObjects
{
    interface ISegment
    {
        XmlDocument DataSource { set; get; }
    }
}
