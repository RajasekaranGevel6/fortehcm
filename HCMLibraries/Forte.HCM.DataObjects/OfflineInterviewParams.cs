﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class OfflineInterviewParams
    {

        public string candidateSessionID { set; get; }
        public int attemptID { set; get; }
        public string questionKey { set; get; }
        public string testKey { set; get; }
        public bool skipped { set; get; }
        public DateTime startTime { set; get; }
        public DateTime endTime { set; get; }
        public int candidateID { set; get; }
        public string status { set; get; }
        public string statusType { set; get; }
        public int totalQuestions { set; get; }
        public int questionsViewed { set; get; }
        public int questionNotViewed { set; get; }
        public string sessionTrackingStatus { set; get; }
        public string sessionCandidateStatus { set; get; }
        public string assessmentStatus { set; get; }
        public string comments { set; get; }
    }
}
