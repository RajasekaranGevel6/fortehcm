﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewDetail.cs
// File that represents the class which contains Interview TestDetail and derived from TrackingDetail information.

#endregion Header     

#region Directives

using System;
using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class InterviewDetail : TrackingDetail
    {
        public string InterviewTestKey { set; get; }
        public string InterviewName { set; get; }
        public string InterviewDescription { set; get; }  

        public int PositionProfileId { set; get; }
        public string PositionProfileName { set; get; }
        public string TestAuthor { set; get; }

        public bool SessionIncludedActive { set; get; }

        public int TestAuthorID { set; get; }

        public bool SessionIncluded { set; get; }
        public string SecurityCode { set; get; }
        public string ParentInterviewKey { set; get; }

        public int SessionCount { set; get; }
        public int AssessedCount { set; get; }

        public string SessionValue { set; get; }
        public string InterviewSessionKey { set; get; }

        public string TimeSlotTextFrom { set; get; }
        public string TimeSlotTextTo { set; get; }
    }
}
