﻿namespace Forte.HCM.DataObjects
{
    public enum QuestionStatus
    {
        Active = 'Y',
        Inactive = 'N',
        Deleted = 'F'
    }
}
