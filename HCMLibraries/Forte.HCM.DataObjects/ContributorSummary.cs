﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ContributorSummary.cs
// File that holds the properties for the View Contributor summary page

#endregion

#region  Directives

using System;
using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// ContributorSummary.cs
    /// File that holds the properties for the View Contributor summary page
    /// </summary>   
    public class ContributorSummary
    {
        #region Properties

        public DateTime ContributorSince { set; get; }
        public int QuestionsAuthored { set; get; }
        public int CategoriesContributed { set; get; }
        public int SubjectsContributed { set; get; }
        public int QuestionUsageCount { set; get; }
        public decimal CreditsEarned { set; get; }
        public SingleChartData QuestionsAuthoredData { set; get; }
        public SingleChartData QuestionUsageCountData { set; get; }
        public SingleChartData CreditsEarnedData { set; get; }


        #endregion Properties
    }
}
