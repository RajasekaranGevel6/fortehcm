﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfileStatusSearchCriteria
    {
        public string PositionProfileId { set; get; }
        public string SortField { set; get; }
        public string PageNo { set; get; }
        public string SelectedCandiateIDs { set; get; }
        public string FilterBy { set; get; }


    }
}
