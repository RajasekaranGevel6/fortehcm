﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    public class FeatureManagementDetails
    {
        public int FeatureID { get; set; }

        public int ModuleID { get; set; }

        public string Module { get; set; }

        public int SubModuleID { get; set; }

        public string SubModule { get; set; }

        public string FeatureName { get; set; }

        public string FeatureURL { get; set; }

        public int FeatureSeqNO { get; set; }
    }
}
