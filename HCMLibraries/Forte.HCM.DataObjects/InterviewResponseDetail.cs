﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    public class InterviewResponseDetail
    {
        public string QuestionKey { set; get; }
        public string Question { set; get; }
        public string ChoiceDesc { set; get; }
        public int Weightage { set; get; }
        public string CategoryName { set; get; }
        public string SubjectName { set; get; }
        public string CandidateComments { set; get; }
        public DateTime StartTime { set; get; }
        public DateTime EndTime { set; get; }
        public decimal ResponseTime { set; get; }
        public string Status {set; get;}
        public bool Skipped { set; get;}
        public string Complexity {set; get;}
        public string TestArea {set; get;}
        public string userComments { get; set; }
        public int Rating { get; set; }
        public int assessorID { get; set; }
        public string CandidateSessionId { set; get; }
        public List<InterviewResponseDetail> RatingComments { get; set; }
        public string assessorName { get; set; }
        public decimal RatingPercent { set; get; }
        public byte[] QuestionImage { set; get; }
        private bool hasImage = false;
        public bool HasImage 
        {
            set { hasImage = value; }
            get {return hasImage;}
        }

    }
}