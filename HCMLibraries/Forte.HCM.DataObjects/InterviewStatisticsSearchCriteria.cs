﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class InterviewStatisticsSearchCriteria
    {
        public string TestKey { set; get; }
        public string CandidateName { set; get; }
        public string SessionCreatedBy { set; get; }
        public string ScheduleCreatedBy { set; get; }
        public string TestDateFrom { set; get; }
        public string TestDateTo { set; get; }
        public string AbsoluteScoreFrom { set; get; }
        public string AbsoluteScoreTo { set; get; }
        public string RelativeScoreFrom { set; get; }
        public string RelativeScoreTo { set; get; }
        public int CurrentPage { set; get; }
        public string SortExpression { set; get; }
        public string SortDirection { set; get; }
        public bool IsMaximized { set; get; }
        public string SellectedCandidateDetails { set; get; }
    }
}
