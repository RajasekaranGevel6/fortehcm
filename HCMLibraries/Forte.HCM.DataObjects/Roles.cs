﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class Roles
    {
        public int RoleID{ set; get; }
        public string RoleCode{ set; get; }
        public int RolecatID{ set; get; }
        public string RoleName{ set; get; }
        public string RoleJobDescription { set; get; }

        public string IsSelected { get; set; }
    }
}
