﻿#region Namespace
using System;
#endregion Namespace

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the resume email reminder settings
    /// </summary>
    [Serializable]
    public class ResumeEmailReminderDetail
    {
        #region Public Properties

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume email reminder attribute id
        /// </summary>
        public string AttributeID { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume email reminder attribute type
        /// </summary>
        public string AttributeType { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume email reminder attribute name
        /// </summary>
        public string AttributeName { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume email reminder attribute value
        /// </summary>
        public string AttributeValue { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume email reminder user type
        /// </summary>
        public string UserType { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds
        /// the resume email reminder option
        /// </summary>
        public string EmailOption { set; get; }

        #endregion Public Properties
    }
}
