﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTests.cs
// File that represents the properties for admin settings.

#endregion Header

#region Directives

using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for candidate tests.
    /// </summary>                                                              
    public class CandidateTests
    {
        List<TestDetail> PendingTests { set; get; }
        List<TestDetail> CompletedTests { set; get; }
        List<TestDetail> ExpiredTests { set; get; }
    }
}
