﻿namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that holds the resume status details such as message for age of
    /// resume, approved staus.
    /// </summary>
    public class ResumeStatus
    {
        public int DaysOld { set; get; }
        public bool Approved { set; get; }
        public bool Uploaded { set; get; }
        public string ResumeName { set; get; }
        public int CandidateResumeID { set; get; }
        public bool ShowApproveButton { set; get; }
    }
}
