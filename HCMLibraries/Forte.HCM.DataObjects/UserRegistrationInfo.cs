﻿#region Namespace                                                              

using System;

#endregion Namespace

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class UserRegistrationInfo
    {
        #region Public Properties                                              

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the tenant user id
        /// </summary>
        public int UserID { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the tenant id
        /// </summary>
        public int TenantID { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user email
        /// </summary>
        public string UserEmail { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user password
        /// </summary>
        public string Password { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user first name
        /// </summary>
        public string FirstName { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user last name
        /// </summary>
        public string LastName { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user phone number
        /// </summary>
        public string Phone { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user company name
        /// </summary>
        public string Company { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user title
        /// </summary>
        public string Title { set; get; }

        /// <summary>
        /// A <see cref="System.Int3"/> that holds the user subscription type id
        /// </summary>
        public int SubscriptionId { set; get; }

        /// <summary>
        /// A <see cref="System.Char"/> that holds the activated status
        /// </summary>
        public char Activated { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds the isactive status
        /// </summary>
        public Int16 IsActive { set; get; }

        /// <summary>
        /// A <see cref="bool"/> that holds the site admin status.
        /// </summary>
        public bool IsSiteAdmin { set; get; }

        /// <summary>
        /// A <see cref="bool"/> that holds the role IDs in comma separated format.
        /// </summary>
        public string RoleIDs { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds the active status 
        /// of the user
        /// </summary>
        /// <remarks>
        /// This property is used for editing corporate admin user
        /// </remarks>
        public Int16 IsActiveStatus { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds the number of users
        /// </summary>
        public Int16 NumberOfUsers { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the subscription role.
        /// </summary>
        public string SubscriptionRole { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the subscription name
        /// </summary>
        public string SubscriptionName { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds the is trial status
        /// </summary>
        public Int16 IsTrial { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the trial days
        /// of a subscription
        /// </summary>
        public int? TrialDays { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the account 
        /// activated date.
        /// </summary>
        public DateTime ActivatedOn { set; get; }

        /// <summary>
        /// A <see cref="System.String"/>that holds the country name
        /// </summary>
        public string Country { set; get; }

        /// <summary>
        /// A <see cref="System.String"/>that holds the user selected bussiness type id's
        /// </summary>
        public string BussinessTypesIds { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the other bussiness type
        /// </summary>
        public string BussinessTypeOther { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user created by
        /// </summary>
        public string CreatedBy { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the user form id
        /// </summary>
        public int FormId { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user form name
        /// </summary>
        public string UserFormName { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// </summary>
        public DateTime CreatedDate { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the user modified by
        /// </summary>
        public int ModifiedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </summary>
        public DateTime ModifiedDate { set; get; }

        /// <summary>
        /// A <see cref="string"/> that holds the activation code.
        /// </summary>
        public string ActivationCode { set; get; }

        /// <summary>
        /// A <see cref="string"/> that holds the user corporation email code.
        /// </summary>
        public string UserCorporateAdminEmail { set; get; }

        #endregion Public Properties

        public bool IsCustomerAdmin { get; set; }

        public string CorporateRoles { get; set; }

        public short NoOfUsers { get; set; }

        public string SelectedRoles { get; set; }

        public short ActiveNoOfUsers { get; set; }

        public string ActiveUser { get; set; }

        public DateTime ExpiryDate { get; set; }

        public char LegalAccepted { get; set; }

        public double ExpiryDays { get; set; }

        public string  isAssessor { get; set; }
    }
}
