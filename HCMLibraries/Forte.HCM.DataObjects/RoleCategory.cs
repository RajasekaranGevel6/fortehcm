﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Module.cs
// File that represents the properties related the role catetories.

#endregion Header

#region Directives

using System;

#endregion Directives 

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represent the property list regarding the role categories.
    /// </summary>
    [Serializable]
    public class RoleCategory
    {
        public int RoleCategoryID {set; get;}
        public string RoleCategoryCode {set; get;}
        public string RoleCategoryName {set; get;}
        public int CreatedBy {set; get;}
        public DateTime CreatedDate {set; get;}
        public int ModifiedBy {set; get;}
        public DateTime ModifiedDate { set; get; }
    }
}