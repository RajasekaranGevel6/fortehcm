﻿namespace Forte.HCM.DataObjects
{
    public enum ScheduleStatus
    {
        None = 0,
        Scheduled = 1,
        Unscheduled = 2,
        Rescheduled = 3,
        Cancelled = 4,
        Completed = 5
    }
}
