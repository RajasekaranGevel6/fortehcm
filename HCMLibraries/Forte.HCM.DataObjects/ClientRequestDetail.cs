﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ClientRequestDetail.cs
// File that represents the properties for the client request details.

#endregion Header

#region Directives

using System;
using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the property list for the client request detail
    /// </summary>
    [Serializable]
    public class ClientRequestDetail
    {
        public string ClientRequestNumber { set; get; }
        public string ClientName { set; get; }
        public string PositionName { set; get; }
        public string PositionProfileName { set; get; }
        public DateTime CreatedDate { set; get; }
        public string Tags { set; get; }
        public List<string> Skills { set; get; }
        public int genID { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds only keywords
        /// with out any weightage
        /// </summary>
        public string TagsWithNoWeightage { set; get; }

        public string DepartmentName { set; get; }
        public string ContactName { set; get; }
        public string Street { set; get; }
        public string City { set; get; }
        public string State { set; get; }
        public string ZipCode { set; get; }
        public string CreatedBy { set; get; }
    }
}
