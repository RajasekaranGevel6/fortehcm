﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    public class SignupCandidate
    {
        public int CandidateID { get; set; }
        public string CandidateSkill { get; set; }
        public int CandidateCreatedBy { get; set; }
        public string ExperienceYears { get; set; }
        public string LastUsed { get; set; }

        public int SkillID { get; set; }
        public string Skill { get; set; }
        public int SlNo { get; set; }

        public string FristName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string Retype_Password { get; set; }
        public string SkillType { get; set; }
    }
}
