﻿using System;

namespace Forte.HCM.DataObjects
{
    public enum MessageType
    {
        Success = 0,
        Error = 1,
        Warning = 2,
        Unexpected = 3
    }
}
