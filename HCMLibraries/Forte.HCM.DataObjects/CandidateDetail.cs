﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AttributeDetail.cs
// File that represents the properties for candidate detail.

#endregion Header

#region Directives

using System;
using ReflectionComboItem;

#endregion Directivess

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for candidate detail.
    /// </summary> 
    [Serializable]
    public class CandidateDetail
    {
        public int CandidateInfoID { set; get; }
        public int CandidateLoginID { set; get; }

        public string CandidateID { set; get; }
        //public string FirstName { set; get; }
        //public string MiddleName { set; get; }
        //public string LastName { set; get; }
        //public string EMailID { set; get; }
        //public string Address { set; get; }
        //public string Phone { set; get; }
        //public string TestKey { set; get; }
        //public string Location { set; get; }
        public string CandidateSynopsis { set; get; }
        public string CandidateNotes { set; get; }
        [DropDownItemAttribute("Candidate Name", "FIRST_NAME")]
        public string FirstName { set; get; }

        public string MiddleName { set; get; }

        public string LastName { set; get; }

        public string EMailID { set; get; }

        [DropDownItemAttribute("Address", "ADDRESS")]
        public string Address { set; get; }
        [DropDownItemAttribute("Phone", "ADDRESS")]
        public string Phone { set; get; }

        public string Mobile { set; get; }

        public string TestKey { set; get; }

        public string Location { set; get; }
        public string LinkedInProfile { set; get; }

        public string CandidateFullName { set; get; }
        public string ResumeIsApproved { set; get; }
        public string UserName { set; get; }

        public int ClientID { set; get; }
        public string ClientName { get; set; }
        public string PositionProfileName { get; set; }
        public int PositionProfileID { get; set; }
        public string RecruiterName { get; set; }
        public string Fax { set; get; }

        public string City { set; get; }
        public string State { set; get; }
        public string ZipCode { set; get; }
        public string ResumeText { set; get; }
        public string SlotKey { set; get; }
        public string ChatRoomKey { set; get; }
        public int CandidateInterviewID { set; get; }
    }
}
