﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class InterviewSearchCriteria : InterviewDetail
    {
        public int SearchTenantID { set; get; }
        public SearchType SearchType { set; get; }
        public List<InterviewTestArea> InterviewTestAreas { set; get; }
        public List<Category> Categories { set; get; }
        public List<Subject> Subjects { set; get; }
        public string Category { set; get; }
        public string Subject { set; get; }
        public string Keyword { set; get; }
        public string TestAreasID { set; get; }
        public int InterviewTestCostStart { set; get; }
        public int InterviewTestCostEnd { set; get; }
        public int TotalQuestionStart { set; get; }
        public int TotalQuestionEnd { set; get; }
        public string CategoriesID { set; get; }
        public string Complexity { set; get; }
        public int InterviewTestStartIndex { set; get; }
        public int InterviewTestEndIndex { set; get; }
        public int CurrentPage { set; get; }
        public string SortExpression { set; get; }
        public SortType SortDirection { set; get; }
        public bool IsMaximized { set; get; }
        public decimal? isSufficientCredits { set; get; }
        public bool ShowCopiedTests { set; get; }
        public string InterviewSessionStatus { set; get; }
        public int PageSize { set; get; }
        public int CandidateId { set; get; }
    }
}
