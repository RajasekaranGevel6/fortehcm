﻿namespace Forte.HCM.DataObjects
{
    public class PositionProfileKeywordPattern
    {
        public string TechnicalSkillsPattern { set; get; }
        public string VerticalsPattern { set; get; }
        public string RolesPattern { set; get; }
    }
}
