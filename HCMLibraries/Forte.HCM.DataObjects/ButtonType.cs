﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ButtonType.cs
// File that represents the button type for confirm message dialog
// control.

#endregion Header                                                              

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that represents the values for button type. It holds
    /// yes, no, cancel, and Ok statuses.
    /// </summary>
    public enum ButtonType
    {
        /// <summary>
        /// Yes status. Indicates positive approach when YES/NO buttons are
        /// present in the same dialog.
        /// </summary>
        Yes = 1,

        /// <summary>
        /// No status.Indicates negative approach when YES/NO buttons are
        /// present in the same dialog.
        /// </summary>
        No = 2,

        /// <summary>
        /// Cancel status. Indicates to cancel the process.
        /// </summary>
        Cancel = 0,

        /// <summary>
        /// Ok status.
        /// </summary>
        Ok=3
    }
}