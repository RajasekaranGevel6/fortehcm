﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class UserRightsMatrix : TrackingDetail
    {
        public int UserID { set; get; }

        public string SubModuleName { set; get; }

        public string FeatureName { set; get; }

        public string FeatureURL { set; get; }

        public int SubModuleID { set; get; }

        public int FeatureID { set; get; }

        public bool IsApplicable { set; get; }

        public int RoleID { set; get; }

        public int? UserRightsID { set; get; }

        public int RoleRightsID { set; get; }

        public int ModuleID { set; get; }

        public string RoleName { set; get; }

        public bool IsEditable { set; get; }
    }
}
