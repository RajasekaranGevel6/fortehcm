﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class SkillDetail
    {
        public int SkillID { set; get; }
        public string SkillName { set; get; }
        public int CategoryID { set; get; }
        public string CategoryName { set; get; }
        public int Weightage { set; get; }
        public string SkillLevel { set; get; }
        public int SkillQuestionCount { set; get; }
        public int SkillRating { set; get; }
        public string SkillComments { set; get; }

        /// <summary>
        /// Property that holds the comma separated skills used to display in
        /// the assessor profile page.
        /// </summary>
        public string Skills { set; get; }

        public SkillDetail()
        {
        }

        public SkillDetail(int skillID, string skillName)
        {
            this.SkillID = skillID;
            this.SkillName = skillName;
        }

        public SkillDetail(int categoryID, string categoryName, string skills)
        {
            this.CategoryID = categoryID;
            this.CategoryName = categoryName;
            this.Skills = skills;
        }
    }
}