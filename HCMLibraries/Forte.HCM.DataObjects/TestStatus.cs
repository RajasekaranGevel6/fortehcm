﻿namespace Forte.HCM.DataObjects
{
    public enum TestStatus
    {
        Active = 0,
        Inactive = 1,
        Deleted = 2
    }
}
