﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class BusinessTypeDetail
    {
        public int BusinessTypeID { set; get; }

        public string BusinessTypeName { set; get; }


    }
}
