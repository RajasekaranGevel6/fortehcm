﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Category.cs
// File that represents the class which contains the client management 
// settings details.

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains the client management settings details.
    /// </summary>
    public class ClientManagementSettingsDetail
    {
        public int TenantID { set; get; }
        public int UserID { set; get; }
        public bool AllowDeleteClientForAll { get; set; }
    }

    public enum ClientManagementType
    { 
        Client=0,
        Departement=1,
        Contact=2,
        All=3

    }
}
