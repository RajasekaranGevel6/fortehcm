﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class ExternalAssessorDetail
    {
        public string InterviewKey { set; get; }
        public string InterviewSessionKey { set; get; }
        public string CandidateInterviewSessionkey { set; get; }
        public int AttemptID { set; get; }
        public string AssessmentStatus { set; get; }
        public int ExternalAssessorID { set; get; }
        public string ExternalKey { set; get; }
        public string Email { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string ContactNumber { set; get; }
        public int ExternalAssessorCreatedByID { set; get; }
        public DateTime ModifiedDate { get; set; }
        public List<SkillDetail> Skills { get; set; }
    }
}
