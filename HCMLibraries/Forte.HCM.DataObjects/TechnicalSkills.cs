﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TechnicalSkills.cs
// File that represents the class which contains candidate technical skill details.

#endregion Header                                                              

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains Language, Database, UITools    
    /// and OperatingSystem members.
    /// </summary>
    public class TechnicalSkills
    {
        public string Language {set; get;}
        public string Database {set; get;}
        public string UITools {set; get;}
        public string OperatingSystem {set; get;}
    }
}
