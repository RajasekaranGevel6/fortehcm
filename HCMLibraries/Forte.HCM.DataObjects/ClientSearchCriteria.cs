﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientSearchCriteria
    {
        /// <summary>
        /// Gets or sets the name of the client.
        /// </summary>
        /// <value>
        /// The name of the client.
        /// </value>
        public string ClientName { set; get; }

        /// <summary>
        /// Gets or sets the name of the contact.
        /// </summary>
        /// <value>
        /// The name of the contact.
        /// </value>
        public string ContactName { set; get; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public int ? CreatedBy  { set; get; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        public string Location { set; get; }

        /// <summary>
        /// Gets or sets the tenant ID.
        /// </summary>
        /// <value>
        /// The tenant ID.
        /// </value>
        public int TenantID { set; get; }


        /// <summary>
        /// Gets or sets a value indicating whether [search all clients].
        /// </summary>
        /// <value><c>true</c> if [search all clients]; otherwise, <c>false</c>.</value>
        public bool SearchAllClients { set; get; }


        /// <summary>
        /// Gets or sets the name of the deparment.
        /// </summary>
        /// <value>
        /// The name of the deparment.
        /// </value>
        public string DeparmentName { set; get; }

        /// <summary>
        /// Gets or sets the client ID.
        /// </summary>
        /// <value>
        /// The client ID.
        /// </value>
        public int? ClientID { set; get; }

        /// <summary>
        /// Gets or sets the department ID.
        /// </summary>
        /// <value>
        /// The department ID.
        /// </value>
        public int? DepartmentID { set; get; }

        /// <summary>
        /// Gets or sets the contact ID.
        /// </summary>
        /// <value>
        /// The contact ID.
        /// </value>
        public int? ContactID { set; get; }


    }

    
}
