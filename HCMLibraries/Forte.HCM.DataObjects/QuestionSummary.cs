﻿using System;

namespace Forte.HCM.DataObjects
{
    public class QuestionSummary
    {
        public int TotalCandidatesAdministered { set; get; }
        public DateTime AverageTimeTakenByAllCandidates { set; get; }
        public DateTime MaximumTimeTakenByAllCandidates { set; get; }
        public decimal StandardDeviationOfTimeDistribution { set; get; }
        public decimal Complexity { set; get; }
    }
}
