﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TestSearchCriteria : TestDetail
    {
        public int SearchTenantID { set; get; }
        public SearchType SearchType {set; get;}
        public List<TestArea> TestAreas {set; get;}
        public List<Category> Categories {set; get;}
        public List<Subject> Subjects {set; get;}
        public string Category {set; get;}
        public string Subject {set; get;}
        public string Keyword {set; get;}
        public string TestAreasID { set; get; }
        public int TestCostStart { set; get; }
        public int TestCostEnd { set; get; }
        public int TotalQuestionStart { set; get; }
        public int TotalQuestionEnd{ set; get; }
        public string CategoriesID { set; get; }
        public string Complexity { set; get; }
        public int TestStartIndex { set; get; }
        public int TestEndIndex { set; get; }
        public int CurrentPage { set; get; }
        public string SortExpression { set; get; }
        public SortType SortDirection { set; get; }
        public bool IsMaximized { set; get; }
        public decimal? isSufficientCredits { set; get; }
        public bool ShowCopiedTests { set; get; }


        public List<TestSearchCriteria> TestDetails { get; set; }

        public List<TestDetail> Test { get; set; }

        public string Weightage { get; set; }
    }
}
    