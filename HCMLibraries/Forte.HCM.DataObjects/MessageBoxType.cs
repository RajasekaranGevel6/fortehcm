﻿namespace Forte.HCM.DataObjects
{
    public enum MessageBoxType
    {
        YesNo = 0,
        Ok = 1,
        OkCancel = 2,
        YesNoSmall = 3,
        EmailConfirmType = 4,
        OkSmall = 5,
        ClientControl=6,
        DepartmentControl = 7,
        DeleteContact = 8,
        LogoutYesNo = 9,
        InterviewScoreConfirmType = 10
    }
}
