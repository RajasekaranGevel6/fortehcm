﻿namespace Forte.HCM.DataObjects
{
    public enum ComplexityType
    {
        Simple = 0,
        Normal = 1,
        Complex = 2
    }
}
