﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TestRecommendationDetailSearchCriteria : TestRecommendationDetail
    {
        public int PageNumber { set; get; }
        public bool IsMaximized { set; get; }
        public SortType SortType { set; get; }
        public string SortField { set; get; }
    }
}
