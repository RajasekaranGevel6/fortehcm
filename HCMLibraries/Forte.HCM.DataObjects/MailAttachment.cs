﻿namespace Forte.HCM.DataObjects
{
    public class MailAttachment
    {
        public string FileName {set; get;}
        public byte[] FileContent {set; get;}
    }
}