﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class NomenclatureCustomize
    {
        public string DataSource { get; set; }

        public string Client { get; set; }
        public string ClientName { get; set; }

        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string state { get; set; }
        public string Zipcode { get; set; }
        public string Country { get; set; }
        public string FEINNO { get; set; }
        public string FaxNo { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailID { get; set; }
        public string WebsiteURL { get; set; }
        public string AdditionalInfo { get; set; }
        public string MyRecords { set; get; }
        public string Location { set; get; }
        public string ClientID { set; get; }
        public string CreatedBy { set; get; }
        public string NoOfDepartments { set; get; }
        public string NoOfContacts { set; get; }
        public string NoOfPositionProfiles { set; get; }
        public string ListOfDepartments { set; get; }

        public string ContentAuthor { get; set; }

        public bool ClientNameIsMandatory { get; set; }
        public bool AddressIsMandatory { get; set; }
        public bool PhoneNumberIsMandatory { get; set; }
        public bool EmailIDIsMandatory { get; set; }
        public bool WebsiteURLIsMandatory { get; set; }
        public bool AdditionalInfoIsMandatory { get; set; }
        public bool ContentAuthorIsMandatory { get; set; }

        public bool ClientNameFieldIsMandatory { get; set; }
        public bool StreetAddressIsMandatory { get; set; }
        public bool CityIsMandatory { get; set; }
        public bool StateIsMandatory { get; set; }
        public bool ZipCodeIsMandatory { get; set; }
        public bool CountryIsMandatory { get; set; }
        public bool FEINNOIsMandatory { get; set; }
        public bool FaxNoIsMandatory { get; set; }
        public bool PhoneNumberFieldIsMandatory { get; set; }
        public bool EmailIDFieldIsMandatory { get; set; }
        public bool WebsiteURLFieldIsMandatory { get; set; }
        public bool AdditionalFieldInfoIsMandatory { get; set; }

        public bool ClientNameIsVisible { get; set; }
        public bool StreetAddressIsVisible { get; set; }
        public bool CityIsVisible { get; set; }
        public bool StateIsVisible { get; set; }
        public bool ZipCodeIsVisible { get; set; }
        public bool CountryIsVisible { get; set; }
        public bool FEINNOIsVisible { get; set; }
        public bool FaxNoIsVisible { get; set; }
        public bool PhoneNumberIsVisible { get; set; }
        public bool EmailIDIsVisible { get; set; }
        public bool WebsiteURLIsVisible { get; set; }
        public bool AdditionalInfoIsVisible { get; set; }



        public bool ContentAuthorFieldIsMandatory { get; set; }



        public int TenantID { get; set; }

        public string RoleID { get; set; }

        public string RoleAliasName { get; set; }

        public string RoleName { get; set; }
        public int UserID { get; set; }

        public string Description { get; set; }

        public string DepartmentName { get; set; }

        public bool DepartmentNameIsMandatory { get; set; }

        public bool DepartmentNameIsVisible { get; set; }

        public bool DescriptionIsMandatory { get; set; }

        public bool DescriptionIsVisible { get; set; }

        public string ContactName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Middlename { get; set; }

        public string Departments { get; set; }

        public bool FirstNameIsMandatory { get; set; }

        public bool LastNameIsMandatory { get; set; }

        public bool MiddlenameIsMandatory { get; set; }

        public bool DepartmentsIsMandatory { get; set; }

        public bool FirstNameIsVisible { get; set; }

        public bool LastNameIsVisible { get; set; }

        public bool MiddlenameIsVisible { get; set; }

        public bool DepartmentsIsVisible { get; set; }

        public string Contacts { get; set; }

        public string Department { get; set; }

        public bool DepartmentPrimaryAddress { get; set; }

        public bool ContactsPrimaryAddress { get; set; }


    }
}
