﻿using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace Forte.HCM.DataObjects
{
    public class MailDetail
    {
        private bool _IsBodyHTML = true;
        private bool _IsBodyHTMLPositionProfile = true;
        public string From { set; get; }
        public List<string> To { set; get; }
        public List<string> CC { set; get; }
        public List<string> BCC { set; get; }
        public EntityType EntityType { set; get; }
        public string EntityID { set; get; }
        public List<MailAttachment> Attachments { set; get; }
        public string Message { set; get; }
        public string Subject { set; get; }
        /// <summary>
        /// A <see cref="bool"/> indicates whether the body of the 
        /// mail content is HTML or not.
        /// Note:- By default this is true.
        /// </summary>
        public bool isBodyHTML
        {
            set { _IsBodyHTML = value; }
            get { return _IsBodyHTML; }
        }

        public bool isBodyHTMLPositionProfile
        {
            set { _IsBodyHTMLPositionProfile = value; }
            get { return _IsBodyHTMLPositionProfile; }
        }
        public Hashtable MailValues;
        public FileInfo ForteHCMLogo;
    }
}
