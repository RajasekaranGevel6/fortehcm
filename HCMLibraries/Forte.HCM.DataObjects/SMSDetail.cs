﻿
namespace Forte.HCM.DataObjects
{
    public class SMSDetail
    {
        public string To { set; get; }
        public string Message { set; get; }
    }
}
