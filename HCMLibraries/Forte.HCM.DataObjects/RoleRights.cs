﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Module.cs
// File that represents the properties about the role based rights.

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represent the property list for Role Rights.
    /// </summary>
    [Serializable]
    public class RoleRights
    {
        public int RoleRightsID { set; get; }
        public int RoleID { set; get; }
        public int ModuleID { set; get; }
        public int SubModuleID { set; get; }
        public int FeatureID { set; get; }
        public bool RoleRightAdd { set; get; }
        public bool RoleRightUpdate { set; get; }
        public bool RoleRightView { set; get; }
        public bool RoleRightApprove { set; get; }
        public string RoleCode { set; get; }
        public int CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public int ModifiedBy { set; get; }
        public DateTime ModifiedDate { set; get; }

    }
}