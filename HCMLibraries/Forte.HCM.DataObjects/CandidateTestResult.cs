﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestResult.cs
// File that represents the properties for candidate test result.

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for candidate test result.
    /// </summary>   
    [Serializable]
    public class CandidateTestResult : TrackingDetail
    {
        public string CandidateResultKey { set; get; }
        public string CandidateSessionKey { set; get; }
        public int AttemptID { set; get; }
        public string TestKey { set; get; }
        public DateTime StartTime { set; get; }
        public DateTime EndTime { set; get; }

        public int TotalQuestionViewed { set; get; }
        public int TotalQuestionNotViewed { set; get; }
        public int TotalAnsweredCorrectly { set; get; }
        public int TotalAnsweredInCorrect { set; get; }
        public double AbsoluteScoreAnswer { set; get; }
        public double AbsoluteScoreTime { set; get; }
        public double RelativeScore { set; get; }
        public string Status { set; get; }
    }
}

