﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class InterviewSessionSearchCriteria
    {
        public string InterviewName { set; get; }
        public string InterviewKey { set; get; }
        public string SessionKey { set; get; }
        public string SchedulerName { set; get; }
        public string CandidateSessionID { set; get; }
        public string CandidateName { set; get; }
        public string TestSessionCreator { set; get; }
        public int? SchedulerNameID { set; get; }
        public int? InterviewSessionCreatorID { set; get; }
        public int PositionProfileID { set; get; }
        public string PositionProfileName { set; get; }

        public string AssessorFirstName { get; set; }

        public string AssessorLastName { get; set; }

        public string Skill { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public int AssessorID { get; set; }

        public string Email { get; set; }

        public string CreatedDate { get; set; }

        public int UserID { get; set; }

        public string AlternateEmailID { get; set; }

        public string IsActive { get; set; }

        public List<InterviewDetail> ListInterviewDetail { get; set; }
        public List<InterviewDetail> ListInterviewSessionDetail { get; set; }
    }
}
