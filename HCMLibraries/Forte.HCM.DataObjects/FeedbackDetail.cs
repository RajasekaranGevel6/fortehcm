﻿
namespace Forte.HCM.DataObjects
{
    public class FeedbackDetail
    {
        public string FirstName { set; get; }

        public string LastName { set; get; }

        public string Email { set; get; }

        public string Subject { set; get; }

        public string Comments { set; get; }

        public string PhoneNumber { set; get; }
    }
}
