﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateRequestType.cs
// File that represents the enum which contains candidate request type

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that represents the values for candidate request type. It holds
    /// None, Retake and Activate statuses.
    /// </summary>  
    public enum CandidateRequestType
    {
        None = 0,
        Retake = 1,
        Activate = 2
    }
}
