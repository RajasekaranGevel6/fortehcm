﻿#region Header                                                                 

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CertificationDetail.cs
// File that represents the properties for the certificate details.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Drawing;

#endregion Directives                                                          

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the property list for the certificate
    /// details.
    /// </summary>
    [Serializable]
    public class CertificationDetail
    {
        public decimal MinimumTotalScoreRequired {set; get;}
        public int MaximumTimePermissible {set; get;}
        public int PermissibleRetakes {set; get;}
        public int DaysElapseBetweenRetakes {set; get;}
        public string CertificateFormat {set; get;}
        public int CertificateFormatID { set; get; }
        public string CertificateValidity {set; get;}
        public string CertificateValiditiyText { set; get; }
        public string CertificateExpiredOn { set; get; }
        public int CertificateID { set; get; }
        public string CertificateName { set; get; }
        public string CertificateDesc { set; get; }
        public string HtmlText { set; get; }
        public int CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public int ModifiedBy { set; get; }
        public DateTime ModifiedDate { set; get; }
        public string CertificateType { set; get; }
        public byte[] ImageData { set; get; }
        public Image image { set; get; }
        public DateTime CompletedDate { set; get; }
    }
}