﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class Segment : TrackingDetail
    {
        public int SegmentID { set; get; }
        public int DisplayOrder { set; get; }
        public string SegmentName { set; get; }
        public bool IsSelected { set; get; }
        public string SegmentPath { set; get; }
        public string SegmentIDAndPath
        {
            get
            {
                return SegmentID.ToString() + ',' + SegmentPath;
            }
        }
    }
}
