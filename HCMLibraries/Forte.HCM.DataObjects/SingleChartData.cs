﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SingleChartData.cs
// File that holds the data for the single chart control 
//

#endregion

#region Directives                                                             

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Support;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that defines the data for the chart.
    /// Holds the necessary information about the chart. Inherits
    /// the class Chart Data that holds the X and Y values of the chart
    /// </summary>
    [Serializable]
    public class SingleChartData : ChartData
    {
        private int height;

        private int width;

        private bool isDisplayAxisTitle = false;

        private bool isDisplayChartTitle = false;

        private bool isDisplayLegend = true;

        private bool isShowLabel = false;

        private float chartAreaPositionWidth = Constants.DefaultChartConstants.CHART_AREA_POSITION_WIDTH;

        private int legendItemColumnSpacing = Constants.DefaultChartConstants.LEGEND_ITEM_COLUMN_SPACING;

        private int legendMaximumAutoSize = Constants.DefaultChartConstants.LEGEND_MAXIMUM_AUTO_SIZE;

        private int legendTextWrapThreshold = Constants.DefaultChartConstants.LEGEND_TEXT_WRAP_THRESHOLD;

        private bool isChangeSeriesColor = false;

        private ChartColorPalette paletteName;

        private bool isChangePointsColor = false;

        private Color pointColor;

        private int pointNumber;

        private bool isComparisonReport = false;

        public bool IsComparisonReport
        {
            set
            {
                isComparisonReport = value;
            }
            get
            {
                return isComparisonReport;
            }
        }

        public int PointNumber
        {
            set
            {
                pointNumber = value;
            }
            get
            {
                return pointNumber;
            }
        }


        public bool IsChangePointsColor
        {
            set
            {
                isChangePointsColor = value;
            }
            get
            {
                return isChangePointsColor;
            }
        }

        public Color PointColor
        {
            set
            {
                pointColor = value;
            }
            get
            {
                return pointColor;
            }
        }

        public bool IsDisplayAxisTitle
        {
            set
            {
                isDisplayAxisTitle = value;
            }

            get
            {
                return isDisplayAxisTitle;
            }
        }

        public float ChartAreaPositionWidth
        {
            set
            {
                chartAreaPositionWidth = value;
            }

            get
            {
                return chartAreaPositionWidth;
            }
        }

        public string XAxisTitle { set; get; }

        public string YAxisTitle { set; get; }

        public string ChartTitle { set; get; }

        public bool IsDisplayChartTitle
        {
            set
            {
                isDisplayChartTitle = value;
            }
            get
            {
                return isDisplayChartTitle;
            }
        }

        public bool IsDisplayLegend
        {
            set
            { isDisplayLegend = value; }
            get
            {
                return isDisplayLegend;
            }
        }

        public List<ChartData> ChartData { set; get; }

        public bool IsShowLabel
        {
            set
            {
                isShowLabel = value;
            }
            get
            {
                return isShowLabel;
            }
        }

        public string chartSessionId { set; get; }

        public int ChartLength
        {
            set
            {
                height = value;
            }
            get
            {
                if (height == 0)
                {
                    return Constants.DefaultChartConstants.CHART_LENGTH;
                }
                else
                {
                    return height;
                }
            }
        }

        public int ChartWidth
        {
            set
            {
                width = value;
            }
            get
            {
                if (width == 0)
                {
                    return Constants.DefaultChartConstants.CHART_WIDTH;
                }
                else
                {
                    return width;
                }
            }
        }

        public SeriesChartType ChartType { set; get; }

        public int LegendItemColumnSpacing
        {
            set
            {
                legendItemColumnSpacing = value;
            }
            get
            {
                return legendItemColumnSpacing;
            }
        }

        public int LegendMaximumAutoSize
        {
            set
            {
                legendMaximumAutoSize = value;
            }

            get
            {
                return legendMaximumAutoSize;
            }

        }

        public int LegendTextWrapThreshold
        {
            set
            {
                legendTextWrapThreshold = value;
            }
            get
            {
                return legendTextWrapThreshold;
            }
        }

        public bool IsChangeSeriesColor
        {
            set
            {
                isChangeSeriesColor = value;
            }
            get
            {
                return isChangeSeriesColor;
            }
        }

        public ChartColorPalette PaletteName
        {
            set
            {
                paletteName = value;
            }
            get
            {
                return paletteName;
            }
        }

        public string ChartImageName
        {
            set;
            get;
        }
    }
}
