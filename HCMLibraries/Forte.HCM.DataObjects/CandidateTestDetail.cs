﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestDetail.cs
// File that represents properties for candidate test detail.

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the Properties for candidate test detail.
    /// </summary>    
    public class CandidateTestDetail
    {
        public int RowNumber { set; get; }
        public string TestSessionID { set; get; }
        public string CandidateSessionID { set; get; }
        public int AttemptID { set; get; }
        public string TestID { set; get; }
        public string TestName { set; get; }
        public string TestDescription { set; get; }
        public int InitiatedByID { set; get; }
        public string InitiatedBy { set; get; }
        public DateTime InitiatedOn { set; get; }
        public DateTime ExpiryDate { set; get; }
        public DateTime CompletedOn { set; get; }
        public string CertificationStatus { set; get; }
        public bool IsCyberProctoring { set; get; }
        public bool ShowResults { set; get; }
        public string TestAuthorFullName { set; get; }
        public decimal MyScore { set; get; }
        public int CandidateResultID { set; get; }
        public string CandidateName { set; get; }
        public bool ShowTestScore { set; get; }
        public int SchedulerID { set; get; }
        public string SchedulerName { set; get; }
        public bool IsSelfAdminTest { set; get; }
        public string TestAdminStatus { set; get; }
        public string ScheduledByMessage { set; get; }
        public bool IsSelfAdmin { set; get; }
        public int CandidatID { set; get; }
    }
}
