﻿using System;

namespace Forte.HCM.DataObjects
{
    public class TestConductionSummary
    {
        public string CandidateSessionID { set; get; }
        public string TestID { set; get; }
        public DateTime StartTime { set; get; }
        public DateTime EndTime { set; get; }
        public int TotalQuestionsViewed { set; get; }
        public int TotalAnsweredCorrectly { set; get; }
        public decimal AbsoluteScore { set; get; }
        public decimal RelativeScore { set; get; }
    }
}
