﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// NodeDetail.cs
// File that represents the data object for the resume parser node manager.

#endregion

#region Directives

using System;

#endregion

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// NodeDetail class that represents the objects to manage nodes
    /// </summary>
    [Serializable]
    public class NodeDetail
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public int CreatedBy { set; get; }
        public int ModifiedBy { set; get; }
        public string AliasName { set; get; }
    }
}

