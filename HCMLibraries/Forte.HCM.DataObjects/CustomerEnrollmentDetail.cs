﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    public class CustomerEnrollmentDetail : TrackingDetail
    {
        public List<BusinessTypeDetail> BusinessTypeList { set; get; }

        public int FreeSubscriptionTrialValidiy { set; get; }

        public int StandardSubscriptionTrialValidity { set; get; }

        public int CorporateSubscriptionTrialValidity { set; get; }

        public int CorporateSubscriptionNoOfUser { set; get; }

        public int DefaultForm { set; get; }

        public string corporateRoleIDs { get; set; }

        public int UserID { get; set; }
    }
}
