﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateActivitySummary.cs
// File that represents properties for candidate activity summary.

#endregion Header

#region Directives

using System;

#endregion

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the Properties for candidate activity summary.
    /// </summary>    
    public class CandidateActivitySummary
    {
        public int CandidateID { set; get; }
        public int CandidateInfoID { set; get; }
        public string CandidateName { set; get; }
        public string RecruiterName { set; get; }
        public DateTime UserSince { set; get; }
        public int ActivitiesCount { set; get; }
    }
}
