﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SkillLevels.cs
// File that represents the skill levels
// control.

#endregion Header
namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Represents the enum to hold the skill levels
    /// </summary>
    public enum SkillLevels
    {
        /// <summary>
        /// Represents the expert skill level
        /// </summary>
        Expert = 1,

        /// <summary>
        /// Represents the Intermediate skill level
        /// </summary>
        Intermediate = 2,

        /// <summary>
        /// Represents the beginner skill level
        /// </summary>
        Beginner = 3

    }
}
