﻿namespace Forte.HCM.DataObjects
{
    public class PositionProfileOwnerDetail
    {
        public int UserID { set; get; }
        public string OwnerType { set; get; }
        public string FirstName { set; get; }
        public string EMail { set; get; }
    }
}
