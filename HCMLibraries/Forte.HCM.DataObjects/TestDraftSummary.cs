﻿using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    public class TestDraftSummary
    {
      public  List<QuestionDetail> QuestionDetail { set; get; }
      public AttributeDetail AttributeDetail { set; get; }
    }
}
