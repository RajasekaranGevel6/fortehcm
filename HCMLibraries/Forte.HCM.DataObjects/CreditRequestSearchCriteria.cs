﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Represent the class to search the credits
    /// </summary>
    [Serializable]
    public class CreditRequestSearchCriteria
    {
        public string CandidateName { set; get; }

        public string CandidateEmail { set; get; }

        public string ProcessedStatus { set; get; }

        public string ApprovedStatus { set; get; }

        public DateTime RequestDateFrom { set; get; }

        public DateTime RequestDateTo { set; get; }

        public DateTime ProcessedDateFrom { set; get; }

        public DateTime ProcessedDateTo { set; get; }

        public decimal AmountFrom { set; get; }

        public decimal AmountTo { set; get; }

        public string CreditType { set; get; }       
    }
}
