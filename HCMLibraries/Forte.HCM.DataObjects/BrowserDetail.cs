﻿using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    public class BrowserDetail
    {
        public string Type { set; get; }
        public string Version { set; get; }
        public string Name { set; get; }
        public List<BrowserInstruction> Instructions { set; get; }
    }
}
