﻿using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    public class TestTrackingDetail
    {
        public string CandidateSessionID { set; get; }
        public List<WindowTracingDetail> WindowTracingDetails {set; get;}
        public List<SnapDetail> CandidateSnaps { set; get; }
        public List<SnapDetail> DesktopSnaps { set; get; }
    }
}
