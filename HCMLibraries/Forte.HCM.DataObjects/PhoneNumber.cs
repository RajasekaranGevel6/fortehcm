﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PhoneNumber.cs
// File that represents the class which contains candidate phonenumber details.

#endregion Header                                                              

using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains Office, Residence and Mobile members.
    /// </summary>
    [Serializable]
    public class PhoneNumber
    {
        public string Office {set; get;}
        public string Residence {set; get;}
        public string Mobile {set; get;}
    }
}
