﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// RoleManagementDetail.cs
// File that represents the properties about the role management details.

#endregion Header


using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;


namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represent the property list for the role management.
    /// </summary>
    public class RoleManagementDetail
    {
        #region Properties                                                     
        public string RoleName { set; get; }
        public string RolesName { set; get; }
        public string RolesJobDesc { set; get; }
        public string RoleCodeName { set; get; }
        public string RoleDescription{ set; get; }
        public int RoleID { set; get; }
        public int PageID { set; get; }
        public string PageName { set; get; }
        public string PageDescription { set; get; }
        public int RolePermissionID { set; get; }
        public string RoleCategoryCode { set; get; }
        public bool IsApplicable { set; get; }
        public string RoleCatCode { set; get; }
        public string RoleCode { set; get; }
        public int ModifiedBy { set; get; }
        #endregion
    }
}
