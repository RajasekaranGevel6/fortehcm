﻿#region Directives

using System;
using System.Collections.Generic;
using ReflectionComboItem;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TestStatistics
    {

        [DropDownItemAttribute("Number Of Questions", "")]
        public int NoOfQuestions { set; get; }

        [DropDownItemAttribute("Mean Score", "")]
        public decimal MeanScore { set; get; }

        [DropDownItemAttribute("Standard Deviation", "")]
        public decimal StandardDeviation { set; get; }

        [DropDownItemAttribute("Score Range", "")]
        public decimal ScoreRange { set; get; }

        [DropDownItemAttribute("Highest Score", "")]
        public decimal HighestScore { set; get; }

        [DropDownItemAttribute("Lowest Score", "")]
        public decimal LowestScore { set; get; }

        [DropDownItemAttribute("Average Time", "")]
        public decimal AverageTimeTakenByCandidates { set; get; }

        public List<Subject> SubjectStatistics { set; get; }

        public string TestID { set; get; }
        public string TestName { set; get; }
        public string TestAuthor { set; get; }
        public string TestAuthorFullName { set; get; }
        public int NoOfCandidates { set; get; }
        public List<Category> CategoryStatistics { set; get; }
      
        public List<TestArea> TestAreaStatistics { set; get; }
        public List<Complexity> ComplexityStatistics { set; get; }
        public TestSummary TestSummary { set; get; }
        public List<QuestionDetail> Questions { set; get; }
        public List<QuestionStatisticsDetail> QuestionStatistics { set; get; }
        public List<CandidateStatisticsDetail> CandidateStatistics { set; get; }
        public SingleChartData CategoryStatisticsChartData { set; get; }
        public SingleChartData SubjectStatisticsChartData { set; get; }
        public SingleChartData TestAreaStatisticsChartData { set; get; }
        public SingleChartData ComplexityStatisticsChartData { set; get; }
        public decimal AverageScore { set; get; }
        public decimal TestCost { set; get; }
        public List<AutomatedTestSummaryGrid> AutomatedTestSummaryGrid { set; get; }
        public string AutomatedTestAverageComplexity { set; get; }

        public string Name { get; set; }

        public string Description { get; set; }

        public short RecommendedCompletionTime { get; set; }

        public List<TestDetail> Test { get; set; }

        public List<QuestionDetail> TestDetails { get; set; }

        public string Subject { get; set; }

        public string TestAreasID { get; set; }

        public string Complexity { get; set; }
    }
}
