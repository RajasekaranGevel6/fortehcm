﻿using System;

namespace Forte.HCM.DataObjects
{
    public class OnlineCandidateSessionDetail
    {
        public int AssessorID { set; get; }
        public string InterviewKey { set; get; }
        public string AssessorName { set; get; }
        public int SkillID { set; get; }
        public string SkillName { set; get; }
        public DateTime InterviewDate { set; get; }
        public int TimeSlotID { set; get; }
        public string TimeSlot {set; get;}
        public string SessionStatus { set; get; }
        public string SessionStatusName { set; get; }
        public int CandidateID { set; get; }
        public string CandidateName { set; get; }
        public string CandidateEmail { set; get; }
        public string CandidateSessionID { set; get; }
        public string EmailId { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }

        public string ChatRoomName { set; get; }
        public string TimeSlotFrom { set; get; }
        public string TimeSlotTo { set; get; }

        public int TimeSlotIDFrom { set; get; }
        public int TimeSlotIDTo { set; get; }


        public int CreatedBy { set; get; }
        public int RowNo { set; get; }
        public bool IsExpired { set; get; }

        public string ScheduledBy { set; get; }
        public string SlotKey { set; get; }
        public string ReminderType { set; get; }
        public int CandidateInterviewID { set; get; }
        public string InterviewDateString
        {
            get
            {
                return InterviewDate.ToString("MM/dd/yyyy");
            }
        }

        public string InterviewName { set; get; }
        public string InterviewDesc { set; get; }
        public string PositionProfileName { set; get; }
        public int PositionProfileId { set; get; }
    }
}