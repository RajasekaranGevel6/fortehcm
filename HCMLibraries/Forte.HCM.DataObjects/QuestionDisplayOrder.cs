﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{

    [Serializable]
    public class QuestionDisplayOrder : TrackingDetail
    {
        List<Category> Categories { set; get; }
        public List<Subject> Subjects { set; get; }
        public int CategoryID { set; get; }
        public TestArea TestArea { set; get; }
        public string CategoryName { set; get; }
        public int SubjectID { set; get; }
        public string SubjectName { set; get; }
        public string TestAreaName { set; get; }
        public string Complexity { set; get; }
        public string ComplexityName { set; get; }
        public decimal ComplexityValue { set; get; }
        public int QuestionID { set; get; }
        public string QuestionKey { set; get; }
        public string Question { set; get; }
        public List<AnswerChoice> AnswerChoices { set; get; }
        public Int16 Answer { set; get; } //modified string to int
        public string Choice_Desc { set; get; }
        public string AnswerID { set; get; }
        public string Tag { set; get; }
        public int Author { set; get; } //modified string to int
        public int TestIncluded { set; get; }
        public int Administered { set; get; }
        public decimal CreditsEarned { set; get; }
        public QuestionStatus Status { set; get; }
        //public bool IsActive { set; get; }
        public Int16 NoOfChoices { set; get; }
        public string TestAreaID { set; get; }
        public bool IsValid { set; get; }
        public string DeletedCategories { set; get; }
        public decimal AverageTimeTaken { set; get; }
        public int QuestionRelationId { set; get; }
        public string AuthorName { set; get; }
        public string QuestionAuthorFullName { set; get; }
        public string SelectedSubjectIDs { set; get; }
        public string InvalidQuestionRemarks { set; get; }
        public string RatioOfCorrectAnswerToAttended { set; get; }
        public string candidateAnswerCorrectly { set; get; }
        public decimal AbsoluteScore { set; get; }
        public decimal RelativeScore { set; get; }
        public decimal QuestionWeightage { set; get; }
        public int TimeTaken { set; get; }
        public string Skipped { set; get; }
        public byte[] QuestionImage { set; get; }
        public string ImageName { set; get; }
        private bool hasImage = false;
        public bool HasImage
        {
            set { hasImage = value; }
            get { return hasImage; }
        }

        public string CorrectAnswer { get; set; } //For the Interview question upload

        public int DisplayOrder { get; set; }
    }
}
