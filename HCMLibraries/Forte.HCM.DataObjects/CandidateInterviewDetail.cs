﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateInterviewDetail.cs
// File that represents properties for candidate interview detail.

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    public class CandidateInterviewDetail
    {
        public int RowNumber { set; get; }
        public string InterviewSessionID { set; get; }
        public string CandidateSessionID { set; get; }
        public int AttemptID { set; get; }
        public string InterviewID { set; get; }
        public string InterviewName { set; get; }
        public string InterviewDescription { set; get; }
        public int InitiatedByID { set; get; }
        public string InitiatedBy { set; get; }
        public DateTime InitiatedOn { set; get; }
        public DateTime ExpiryDate { set; get; }
        public DateTime CompletedOn { set; get; }
        public string InterviewAuthorFullName { set; get; }
        public decimal MyScore { set; get; }
        public int CandidateResultID { set; get; }
        public string CandidateName { set; get; }
        public bool IsPaused { set; get; }
        public int SchedulerID { set; get; }
        public string SchedulerName { set; get; }
        public string Company { set; get; }
        public string ScheduledByMessage { set; get; }
        public bool IsSelfAdminTest { set; get; }
        public bool IsExpired { set; get; }
    }
}
