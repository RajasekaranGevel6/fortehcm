﻿using System;

namespace Forte.HCM.DataObjects
{
     [Serializable]
    public class TestRecommendationSegment
    {
        public int TestSegmentID { set; get; }
        public int CateogorySubjectID { set; get; }
        public string TestArea { set; get; }
        public string Complexity { set; get; }
        public string Keyword { set; get; }
        public int Weightage { set; get; }
        public string Category { set; get; }
        public string Subject { set; get; }
    }
}
