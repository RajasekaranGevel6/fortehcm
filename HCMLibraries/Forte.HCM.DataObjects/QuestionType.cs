﻿namespace Forte.HCM.DataObjects
{
    public enum QuestionType
    {
        None = 0,
        MultipleChoice = 1,
        OpenText = 2
    }
}
