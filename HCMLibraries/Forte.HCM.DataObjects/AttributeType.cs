﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AttributeType.cs
// File that represents the enum which contains attribute type

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that represents the values for attribute type. It holds
    /// None, CertificateType, Complexity,TestArea and TestReminder statuses.
    /// </summary>    
    public enum AttributeType
    {
        None = 0,
        CertificateType = 1,
        Complexity = 2,
        TestArea = 3,
        TestReminder = 4
    }
}
