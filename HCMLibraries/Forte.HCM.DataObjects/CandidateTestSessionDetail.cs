﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestSessionDetails.cs
// File that represents the class which contains Candidate Test Session
// detail derived from TestSessionDetail.

#endregion Header                                                              

#region Directives                                                             

using System;

#endregion Directives                                                          

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains GenID, CandidateTestSessionID,CandidateID,CandidateName,
    /// CandidateFirstName, Email, Status, CancelReason, NoOfAttempt, AttemptID,
    /// RetakeRequest, DateCompleted, ScheduledDate, ShowResults members and public
    /// properties.
    /// </summary>
    [Serializable]
    public class CandidateTestSessionDetail : TestSessionDetail
    {
        public int RowNumber { set; get; }
        public int GenID { set; get; }
        public string CandidateTestSessionID {set; get;}
        public string CandidateID {set; get;}
        public int CandidateInformationID { set; get; }
        public string CandidateName { set; get; }
        public string CandidateFirstName { set; get; }
        public string CandidateFullName { set; get; }
        public string Status { set; get; }
        public string CancelReason { set; get; }
        public Int16 NoOfAttempt { set; get; }
        public int AttemptID { set; get; }
        public string RetakeRequest { set; get; }
        public DateTime DateCompleted { set; get; }
        public DateTime ScheduledDate { set; get; }
        public bool ShowResults { set; get; }

        public string CandidateEmail { set; get; }
        public string CandidateMobile { set; get; }
        public string CandidateOwnerEmail { set; get; }
        public int SchedulerID { set; get; }
        public string SchedulerEmail { set; get; }
        public string SchedulerName { set; get; }
        public string SchedulerFirstName { set; get; }
        public string SchedulerLastName { set; get; }
        public string SchedulerCompany { set; get; }
        public string PositionProfileOwnerEmail { set; get; }
        public string ClientName { set; get; }
        public string ProfileName { set; get; }
        public int Score { set;get; }
        /// <summary>
        /// Property that contains the user name who is initiated a test for a candidate.
        /// This property will be used in CandidateDLManager's GetCandidateSummary method.
        /// <remarks>It is used for identifying the 'SELF' username in the CandidateHome.</remarks>
        /// </summary>
        public string TestInitiatedBy { set; get; }

        public string TestRecommandID { set; get; }
        public string TestSchduled { set; get; }
        public int TestRecommandTime { set; get; }
        public bool IsExpired { set; get; }
        public string ScheduledByMessage { set; get; } 

        public string CompletedDate { set; get; }
        public string AbsoluteScore { set; get; }
        public string NavigateKey { set; get; }

        public string EmailReminder { set; get; }
        #region Public Properties                                              

        /// <summary>
        /// Property that will return a date if it is not a min date.
        /// </summary>
        public string DateCompletedFormatted 
        {
            get
            {
                if (DateCompleted == DateTime.MinValue)
                    return string.Empty;

                return DateCompleted.ToString("MM/dd/yyyy"); 
            }
        }

        /// <summary>
        /// Property that will return a date if it doesn't have min date.
        /// </summary>
        public string ScheduledDateFormatted
        {
            get
            {
                if (ScheduledDate == DateTime.MinValue)
                    return string.Empty;

                return ScheduledDate.ToString("MM/dd/yyyy");

            }
        }

        #endregion  Public Properties                                          
    }
}