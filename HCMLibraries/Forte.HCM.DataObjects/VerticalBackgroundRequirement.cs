﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// VerticalBackgroundRequirement.cs
// File that represents details for the vertical background details

#endregion Header

#region Directives

using System;
using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class that contains details of the 
    /// vertical back ground requirement
    /// </summary>
    public class VerticalBackgroundRequirement
    {
        #region Properties
        public string AdditionalVerticalRequirement { set; get; }

        public List<VerticalDetails> VerticalDetails { set; get; }
        #endregion Properties
    }
}
