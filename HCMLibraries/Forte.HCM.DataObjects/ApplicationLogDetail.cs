﻿using System;

namespace Forte.HCM.DataObjects
{
    public class ApplicationLogDetail
    {
        public int LogID { set; get; }
        public string Message { set; get; }
        public DateTime Date { set; get; }
        public string SystemName { get; set; }

        public string MacAddress { get; set; }

        public string IpAddress { get; set; }

        public string OSInfo { get; set; }

        public string CreatedDate { get; set; }

        public short MessageID { get; set; }
    }
}
