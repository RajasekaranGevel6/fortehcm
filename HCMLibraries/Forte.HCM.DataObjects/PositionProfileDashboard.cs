﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfileDashboard
    {
        public string PositionProfileName { set; get; }
        public string ClientName { set; get; }
        public string PositionName { set; get; }
        public DateTime ?DateOfRegistration { set; get; }
        public DateTime? SubmittalDeadline { set; get; }
        public string RegisteredBy { set; get; }
        public List<SkillWeightage> Skills { set; get; }
        public string Keywords { set; get; }
        public string PositionStatus { set; get; }

    }
    
}
