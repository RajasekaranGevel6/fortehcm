﻿using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the candidate resume details
    /// </summary>
    [Serializable]
    public class ReviewResumeDetail
    {
        public int TenantID { get; set; }
        public string Status { get; set; }
        public string ProfileName { get; set; }
        public int RecruiterID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string FilterType { get; set; }    

    }
}
