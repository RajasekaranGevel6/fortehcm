﻿namespace Forte.HCM.DataObjects
{
    public class TestSegmentSearchCriteria : TestDetail
    {
        public string Category { set; get; }
        public string Subject { set; get; }
        public string Keyword { set; get; }
        public decimal Weightage { set; get; }
    }
}
