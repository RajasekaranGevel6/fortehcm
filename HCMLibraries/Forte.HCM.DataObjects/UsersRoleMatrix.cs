﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// UsersRoleMatrix.cs
// File that represents the properties about the sub module details.

#endregion Header

#region Directives                                                             

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represent the property list for the user role matrix.
    /// </summary>
    [Serializable]
    public class UsersRoleMatrix
    {
        #region Properties                                                     

        public int RoleID { get; set; }
        public string RoleCode { get; set; }
        public int UserID { get; set; }
        public string RoleName { set; get; }
        public string RoleJobDescription { set; get; }
        public int URLID { get; set; }

        #endregion Properties
    }
}
