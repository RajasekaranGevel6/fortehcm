﻿namespace Forte.HCM.DataObjects
{
    public class ReminderIntervalDetail
    {
        public string IntervalID { set; get; }
        public RecordStatus RecordStatus { set; get; }
    }
}
