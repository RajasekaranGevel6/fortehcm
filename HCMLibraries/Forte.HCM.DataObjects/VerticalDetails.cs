﻿#region Header                                                       

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// VerticalDetails.cs
// File that represents details for the verticals

#endregion Header

#region Directives                                                   

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class that holds the details for the Vertails
    /// </summary>
   public class VerticalDetails
    {
        #region Properties                                           
        public string VerticalSegment { set; get; }

        public string SubVerticalSegment { set; get; }

        public string NoOfYearsExperience { set; get; }

        public string IsMandatory { set; get; }

        public string IsTestingRequired { set; get; }

        public string IsInterviewRequired { set; get; }

        public string Weightage { set; get; }

        #endregion Properties

        public VerticalDetails()
        {
        }
          
        public VerticalDetails(string verticalSegment, string subVerticalSegment)
        {
            this.VerticalSegment = verticalSegment;
            this.SubVerticalSegment = subVerticalSegment;
        }
    }
}
