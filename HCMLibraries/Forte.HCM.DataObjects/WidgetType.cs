﻿using System;

namespace Forte.HCM.DataObjects
{
   public class WidgetTypes
    {
       public Guid ID { set; get; }
       public string Title { set; get; }
       public string GroupName { set; get; }
       public string Description { set; get; }
       public string SkinID { set; get; }
    }
}
