﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReflectionComboItem;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for client information.
    /// </summary> 
    [Serializable]
    public class ClientInformation:TrackingDetail
    {
        
        public int ClientID { set; get; }
        public int TenantID { set; get; }
        public string ClientName { set; get; }
        
        public string FeinNo { set; get; }

        public string FaxNumber { set; get; }
        public ContactInformation ContactInformation { set; get; }
        public ClientContactInformation ClientContactInformation { set; get; }
        public Department Department { set; get; }
        

        public string AdditionalInfo { set; get; }
        public bool IsValid { set; get; }
        public List<ClientContactInformation> ClientContactInformations { set; get; }
        public List<Department> Departments { set; get; }

        public int NoOfDepartments { set; get; }
        public int NoOfContacts { set; get; }
        public int NoOfPositionProfile { set; get; }
        public string CreatedByName { set; get; }

        public int sellectedDepartmentID { set; get; }

        public string DepartmentName { set; get; }
        public string EmailID { set; get; }
        public string ClientDepartments { set; get; }
        public string ClientContacts { set; get; }
    }
}
