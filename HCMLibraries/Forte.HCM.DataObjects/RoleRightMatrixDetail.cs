﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class RoleRightMatrixDetail
    {
        public string RoleName { set; get; }

        public string RoleDescription { set; get; }

        public int RoleID { set; get; }

        public int PageID { set; get; }

        public string PageName { set; get; }

        public string PageDescription { set; get; }

        public int RolePermissionID { set; get; }

        public string RoleCategoryCode { set; get; }

        public bool IsApplicable { set; get; }

        public int RoleRightID { set; get; }

        public int ModuleID { set; get; }

        public string ModuleName { set; get; }

        public int FeatureID { set; get; }

        public string FeatureName { set; get; }

        public int SubModuleID { set; get; }

        public string SubModuleName { set; get; }

        public int CreatedBy { set; get; }

        public int TenantID { get; set; }

        public bool IsChecked { get; set; }
    }
}
