﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    public class CareerBuilderResumeAdvanceSearchCriteria
    {
        public string SessionToken { set; get; }
        public string Keywords { set; get; }
        public string SearchPattern { set; get; }
        public string JobCategories { set; get; }
        public string City { set; get; }
        public string ZipCode { set; get; }
        public string State { set; get; }
        public string Country { set; get; }
        public string SearchRadiusInMiles { set; get; }
        public string RelocationFilter { set; get; }
        public string FreshnessInDays { set; get; }
        public string EmploymentType { set; get; }
        public string MinimumExperience { set; get; }
        public string MinimumTravelRequirement { set; get; }
        public string MinimumDegree { set; get; }
        public string CompensationType { set; get; }
        public string MinimumSalary { set; get; }
        public string MaximumSalary { set; get; }
        public string ExcludeResumesWithNoSalary { set; get; }
        public string LanguagesSpoken { set; get; }
        public string CurrentlyEmployed { set; get; }
        public string ManagementExperience { set; get; }
        public string MinimumEmployeesManaged { set; get; }
        public string MaximumCommute { set; get; }
        public string SecurityClearance { set; get; }
        public string WorkStatus { set; get; }
        public string ExcludeIVRResumes { set; get; }
        public string OrderBy { set; get; }
        public string PageNumber { set; get; }
        public string RowsPerPage { set; get; }
        public string CustAcctCode { set; get; }
        public string CustomXML { set; get; }
        public string MilitaryExperience { set; get; }
    }
}
