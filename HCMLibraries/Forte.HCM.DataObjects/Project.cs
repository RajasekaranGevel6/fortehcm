﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Project.cs
// File that represents the class which contains candidate project details.

#endregion Header                                                              

using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains ProjectId, ProjectName, ProjectLocation, ProjectDescription
    /// Role, Environment, PositionTitle, ClientName, ClientIndustry, StartDate 
    /// and EndDate members.
    /// </summary>
    [Serializable]
    public class Project
    {
        public int ProjectId { set; get; }
        public DateTime StartDate {set; get;}
        public DateTime EndDate {set; get;}
        public string ClientName {set; get;}
        public string ClientIndustry {set; get;}
        public string ProjectName {set; get;}
        public Location ProjectLocation {set; get;}
        public string PositionTitle {set; get;}
        public string ProjectDescription {set; get;}
        public string Role {set; get;}
        public string Environment {set; get;}       

    }
}
