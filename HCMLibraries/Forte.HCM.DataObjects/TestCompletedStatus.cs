﻿namespace Forte.HCM.DataObjects
{
    public enum TestCompletedStatus
    {
        None = 0,
        NotCompleted = 1,
        Completed = 2,
        Quit = 3,
        RetakeRequested = 4,
    }
}
