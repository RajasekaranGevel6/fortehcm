﻿#region Directives                                                             

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Represents the class that holds the details of the 
    /// parser settings
    /// </summary>    
    [Serializable]
    public class ParserSettings : TrackingDetail
    {
        public int MaximumVersionReplication { set; get; }

        public string VersionCode { set; get; }
    }
}
