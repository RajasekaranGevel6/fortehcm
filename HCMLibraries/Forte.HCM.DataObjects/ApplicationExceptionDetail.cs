﻿namespace Forte.HCM.DataObjects
{
    public class ApplicationExceptionDetail
    {
        public string CandidateSessionID { set; get; }
        public int AttempID { set; get; }
        public string Exception { set; get; }
        public string Date { set; get; }
    }
}