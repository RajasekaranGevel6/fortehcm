﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateSummary.cs
// File that represents properties for candidate summary.

#endregion Header

#region Directives

using System;
using System.Collections.Generic;

#endregion

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the Properties for candidate summary.
    /// </summary>    
    public class CandidateSummary
    {
        public int CandidateID { set; get; }
        public string CandidateName { set; get; }
        public DateTime UserSince { set; get; }
        public string LastTestTaken { set; get; }
        public DateTime LastTestTakenDate { set; get; }
        public int PendingTestsCount { set; get; }
        public int CompletedTestsCount { set; get; }
        public int ExpiredTestsCount { set; get; }

        public int PendingInterviewsCount { set; get; }
        public int CompletedInterviewsCount { set; get; }
        public int ExpiredInterviewsCount { set; get; }

        public List<CandidateTestSessionDetail> PendingTest { set; get; }
        public List<CandidateTestSessionDetail> CompletedTest { set; get; }

        public List<CandidateInterviewSessionDetail> PendingInterview { set; get; }
        public List<CandidateInterviewSessionDetail> CompletedInterview { set; get; }

        public List<CandidateTestSessionDetail> SuggestedTests { set; get; }
        
        public List<CandidateInterviewSessionDetail> InterviewTests { set; get; }
        public List<CandidateTestSessionDetail> MyTests { set; get; }

        public List<CandidateTestSessionDetail> TestActivity_Dashboard { set; get; }
        public List<CandidateInterviewSessionDetail> InterviewActivity_Dashboard { set; get; }
    }
}
