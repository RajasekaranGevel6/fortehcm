﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class UserDetail
    {
        public int UserID { set; get; }
        public int CandidateInfoID { set; get; }
        public string UserName { set; get; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public string FullName { set; get; }
        public string Email { set; get; }
        public string AltEmail { set; get; }
        public string Phone { set; get; }
        public string City { set; get; }
        public DateTime LastLogin { set; get; }
        public List<UserRole> Roles { set; get; }
        public int TenantID { set; get; }
        public string Password { set; get; }
        public string UserTypeName { set; get; }
        public int PageNumber { set; get; }
        public string SortExpression { set; get; }
        public SortType SortDirection { set; get; }
        public bool IsLimited { set; get; }
        public bool IsOpenID {set; get; }
        public bool IsActivityExists { set; get; }
        public string OwnerType { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the subscription role
        /// of the user
        /// </summary>
        public string SubscriptionRole { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the subscription id
        /// of the user
        /// </summary>
        public int SubscriptionId { set; get; }
        //public bool IsAdmin { set; get; }
        //public bool IsCandidate { set; get; }

        public UserType UserType { set; get; } 

        public bool IsAdmin
        {
            get
            {
                if (Roles == null)
                    return false;

                 return Roles.Exists(item => item == UserRole.AdminSystem);
            }
        }

        public bool IsCandidate
        {
            get
            {
                if (Roles == null)
                    return false;

                  return Roles.Exists(item => item == UserRole.Candidate);
            }
        }

        public string SubscriptionType { get; set; }

        public int CandidateID { set; get; }
        public string Status { set; get; }
        public string StatusDescription { set; get; }
        public string Activated { set; get; }
        public bool LegalAccepted { set; get; }
        public string LegalAcceptedDesc { set; get; }
        public string Company { get; set; }

        public DateTime ExpiryDate { get; set; }
        public int ExpiresInDays { get; set; }

        public string BussinessType { get; set; }

        public string Title { get; set; }

        public object BussinessOtherType { get; set; }

        public bool IsActive { get; set; }
        public string IsAssessor { get; set; }
        public string IsOwner { get; set; }
        public string RequestedBy { get; set; }
    }
}