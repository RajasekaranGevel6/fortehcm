﻿using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    public class PositionProfileCandidateSkillScoreDetail
    {
        // Header details.
        public string PositionProfileName { set; get; }
        public string CandidateName { set; get; }

        // Skill scores.
        public List<CandidateSkillScoreDetail> SkillScores { set; get; }

        // Overall scores.
        public double ?OverallTestScore { set; get; }
        public double ?OverallInterviewScore { set; get; }
        public double ?OverallResumeScore { set; get; }
    }
}
