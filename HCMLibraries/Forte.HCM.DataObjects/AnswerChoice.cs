﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdminSettings.cs
// File that represents the properties for answer choices.

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for admin choices.
    /// </summary>                                                            
    [Serializable]
    public class AnswerChoice : TrackingDetail
    {
        public int ChoiceID { set; get; }
        public string Choice { set; get; }
        public bool IsCorrect { set; get; }
        public int QuestionGenId { set; get; }
        public int IsCandidateCorrect { set; get; }
        public int QuestionOptionId { set; get; }
        public AnswerChoice(string choice, bool isCorrect)
        {
            this.Choice = choice;
            this.IsCorrect = isCorrect;
        }
        public AnswerChoice()
        {

        }
        public AnswerChoice(string choice, int choiceID, bool isCorrect)
        {
            this.Choice = choice;
            this.ChoiceID = choiceID;
            this.IsCorrect = isCorrect;
        }
    }
}
