﻿namespace Forte.HCM.DataObjects
{
    public class InterviewIntroductionDetail
    {
        public string InterviewSessionID { set; get; }
        public string CandidateSessionID { set; get; }
        public string InterviewID { set; get; }
        public string InterviewName { set; get; }
        public string InterviewDescription { set; get; }
        public string InterviewTestAreas { set; get; }
        public string InterviewSubjects { set; get; }
        public int InterviewTimeLimit { set; get; }
        public string InterviewInstructions { set; get; }
        public bool IsPaused { set; get; }
    }
}
