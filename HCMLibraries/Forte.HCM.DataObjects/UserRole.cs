﻿namespace Forte.HCM.DataObjects
{
    public enum UserRole
    {
        ContentAuthor,
        ContentValidator,
        TestAuthor,
        DeliveryManager,
        Recruiter,
        Candidate,
        AdminUserSetup,
        AdminOTMTS,
        AdminCreditsManagement,
        AdminCandidateManagement,
        AdminSystem,
        FreeSubscription,
        StandardSubscription,
        CorporateSubscriptionAdmin,
        CorporateSubscriptionUser,
        GeneralUser,
        SalesPerson,
        TechnicalSpecialist,
        RecruiterJava,
        RecruiterSap,
        Assessor,
        SiteAdmin
    }
}
