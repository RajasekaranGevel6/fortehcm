﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestStatus.cs
// File that represents the enum which contains candidate test status

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that represents the values for candidate test status. It holds
    /// All, Pending, Completed, and Expired statuses.
    /// </summary> 
    public enum CandidateTestStatus
    {
        All = 0,
        Pending = 1, 
        Completed = 2,
        Expired = 3
    }
}
