﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class OnlineInterviewSessionDetail : TrackingDetail
    {
        public string InterviewName { set; get; }
        public string Interviewkey { set; get; }
        public string InterviewInstruction { set; get; }
        public string InterviewRemarks { set; get; }
        public string InterviewAuthorName { set; get; }
        public int TotalQuestion { set; get; }
        public List<SkillDetail> skillDetail { set; get; }

        public string OnlineInterviewSessionName { set; get; }
        public string OnlineInterviewSessionKey { set; get; }
        public string OnlineInterviewSessionDescription { set; get; }
        public string OnlineInterviewInstruction { set; get; }
        public string Skills { set; get; }
        public int NumberOfQuestions { set; get; }
        public int NumberOfCandidateSessions { set; get; }
        public string ClientRequestID { set; get; }
        public DateTime ExpiryDate { set; get; }
        public DateTime? SessionExpiryDate { set; get; }
        public string ExpiryDateFormatted
        {
            get
            {
                if (ExpiryDate == DateTime.MinValue)
                    return string.Empty;

                return ExpiryDate.ToString("MM/dd/yyyy");

            }
        }
        public int CandidateAlloted { set; get; }
        public List<CandidateInterviewSessionDetail> CandidateOnlineInterviewSessions { set; get; }
        public string UserName { set; get; }
        public string Email { set; get; }
        public int PositionProfileID { set; get; }
        public string PositionProfileName { set; get; }
        public int AssessorID { get; set; }
        public string AssessorIDs { get; set; }
        public string AssessorEmails {get; set;}
        public int SessionAuthorID { get; set; }
        public string SessionAuthor { get; set; }
        public string SessionAuthorFullName { get; set; }
        public string SessionAuthorEmail { get; set; }
        public int CandidateInterviewID { get; set; }
        public string RequestedBy { get; set; }
        
    }
}
