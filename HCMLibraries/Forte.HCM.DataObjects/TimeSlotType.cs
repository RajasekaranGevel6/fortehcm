﻿namespace Forte.HCM.DataObjects
{
    public enum TimeSlotType
    {
        None = 0,
        From = 1,
        To = 2
    }
}
