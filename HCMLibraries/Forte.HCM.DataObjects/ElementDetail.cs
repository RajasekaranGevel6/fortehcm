﻿namespace Forte.HCM.DataObjects
{
    public class ElementDetail : ElementTypeDetail
    {
        public int ElementID { set; get; }
        public string ElementName { set; get; }
        public string ElementDescription { set; get; }
    }
}
