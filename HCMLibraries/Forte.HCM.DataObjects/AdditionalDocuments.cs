﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class AdditionalDocuments
    {
        public int DocumentID { get; set; }
        public int CandidateID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        public DateTime UploadedDate { get; set; }        
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UploadedBy { get; set; }
    } 
}
