﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class SessionDetail
    {
        public string SessionKey { set; get; }
        public string SessionName { set; get; }

        public List<AssessorDetail> Assessors { set; get; }
    }
}
