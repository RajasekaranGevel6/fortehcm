﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class ReportHistogramData : HistogramData
    {    
        public List<ChartData> AbsoluteChartData { set; get; }

        public List<ChartData> RelativeChartData { set; get; }

        public List<HistogramData> CandidateScoreData { set; get; }

        public List<HistogramData> CandidateRelativeScoreData { set; get; }
    }
}
