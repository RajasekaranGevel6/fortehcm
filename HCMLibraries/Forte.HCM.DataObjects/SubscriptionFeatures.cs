﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SubscriptionFeatures.cs
// File that represents the properties about the Subscription features.

#endregion Header

#region Namespace                                                              

using System;

#endregion Namespace

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represent the property list for the SUBSCRIPTION_FEATURES
    /// </summary>
    [Serializable]
    public class SubscriptionFeatures
    {

        #region Public Properties                                              

        /// <summary>
        ///  A <see cref="System.Int32"/> that holds the Id of the subscription feature
        /// </summary>
        public int SubscriptionFeatureId { set; get; }

        /// <summary>
        ///  A <see cref="System.Int32"/> that holds the Id of the subscription
        /// </summary>
        public int SubscriptionId { set; get; }

        /// <summary>
        /// /
        /// </summary>
        public string SubscriptionName { set; get; }

        /// <summary>
        ///  A <see cref="System.Int32"/> that holds the Id of the feature
        /// </summary>
        public int FeatureId { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the feature name of the subscription
        /// </summary>
        public string FeatureName { set; get; }

        /// <summary>
        ///  A <see cref="System.String"/> that holds the feature value
        /// </summary>
        public string FeatureValue { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the unit id 
        /// of the subscription feature
        /// </summary>
        public string FeatureUnitId { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the Unit name 
        /// of the subscription feature
        /// </summary>
        public string FeatureUnit { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds the not applicable
        /// value of the subscription feature
        /// </summary>
        public Int16 IsNotApplicable { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the default value
        /// of the subscription feature.
        /// </summary>
        public string DefaultValue { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds whether
        /// the feature is unlimited or not.
        /// </summary>
        public Int16 IsUnlimited { set; get; }

        /// <summary>
        ///  A <see cref="System.Int32"/> that holds the Id of the created user
        /// </summary>
        public int CreatedBy { set; get; }

        /// <summary>
        ///  A <see cref="System.DateTime"/> that holds the created date of the subscription feature
        /// </summary>
        public DateTime CreatedDate { set; get; }

        /// <summary>
        ///  A <see cref="System.Int32"/> that holds the id of the modified user 
        /// </summary>
        public int ModifiedBy { set; get; }

        /// <summary>
        ///  A <see cref="System.DateTime"/> that holds the modified date of the subscription feature
        /// </summary>
        public DateTime ModifiedDate { set; get; }

        #endregion Public Properties
    }
}
