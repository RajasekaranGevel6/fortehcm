﻿#region Directives                                                            

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Class that holds the data objects for the 
    /// technical skills group
    /// </summary>
    public class TechnicalSkillsGroup:TrackingDetail
    {
        #region Properties                                                     

        public string GroupName { set; get; }

        public int GroupID { set; get; }

        #endregion Properties
    }
}
