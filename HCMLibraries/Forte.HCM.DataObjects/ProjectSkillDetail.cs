﻿namespace Forte.HCM.DataObjects
{
    public class ProjectSkillDetail
    {
        public int VectorID { set; get; }
        public string VectorName { set; get; }
        public string Experience { set; get; }
        public int Recency { set; get; }
        public int Certifications { set; get; }
        public string GroupId { set; get; }
        public string GroupName { set; get; }
    }
}
