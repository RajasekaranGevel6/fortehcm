﻿namespace Forte.HCM.DataObjects
{
    public class ElementTypeDetail
    {
        public int ElementTypeID { set; get; }
        public string ElementTypeName { set; get; }
    }
}
