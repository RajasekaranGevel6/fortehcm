﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class CandidateAdminSettings
    {
        public int MaxSelfAdminTestRetakes { set; get; }
        public int MaxSelfAdminTestPerMonth { set; get; }
        public int CandidateSessionExpiryInDays { set; get; }
        public int ResumeExpiryDays { set; get; }
        public int ActivitiesGridPageSize { set; get; }
        public int SearchTestGridPageSize { set; get; }
        public int MinQuestionPerSelfAdminTest { set; get; }
        public int MaxQuestionPerSelfAdminTest { set; get; }
    }
}