﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    public class ModuleManagementDetails
    {
        public int ModID { get; set; }

        public string ModCode { get; set; }

        public string ModName { get; set; }

        public string ModeCodeName { get; set; }

        public int SubModuleID { get; set; }

        public string SubModuleName { get; set; }

        public string SubModuleCode { get; set; }

        public string ModuleName { get; set; }

        public int ModifiedBy { get; set; }

        public string ModuleCodeName { get; set; }

        public int SubModSequence { get; set; }

        public string SubModNameCode { get; set; }
    }
}
