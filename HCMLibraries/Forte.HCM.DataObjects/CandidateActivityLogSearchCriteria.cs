﻿using System;

namespace Forte.HCM.DataObjects
{
    public class CandidateActivityLogSearchCriteria : CandidateActivityLog
    {
        public DateTime ActivityDateFrom { set; get; }
        public DateTime ActivityDateTo { set; get; }
    }
}
