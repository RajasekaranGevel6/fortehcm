﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TestRecommendationDetail : TrackingDetail
    {
        public int UserId { set; get; }
        public int TestRecommendId { set; get; }
        public int NoOfQuestions { set; get;}
        public string TestName { set; get; }
        public string Skill { set; get; }
        public int TimeLimit { set; get; }
        public int SubjectId { set; get; }
        public string TestArea { set; get; }
        public string Keyword { set; get; }
        public string Complexity { set; get; }
        public string TestDescription { set; get; }
        public int Weightage { set; get; }
        public string Status { set; get; }
        public string StatusName { set; get; }
        public bool IsTestUsed { set; get; }

        // Candidate details.
        public string CandidateName { set; get; }
        public string CandidateEmail { set; get; }

        // Session detail.
        public string TestKey { set; get; }
        public string SessionKey { set; get; }
        public string CandidateSessionKey { set; get; }
        public int AttemptID { set; get; }
        public DateTime CompletedDate { set; get; }
        public string SessionStatus { set; get; }

        public decimal AnsweredCorrectly { set; get; }
        public string TimeTaken { set; get; }
        public string AverageTimeTaken { set; get; }
        public decimal AbsoluteScore { set; get; }
        public decimal RelativeScore { set; get; }
        public decimal Percentile { set; get; }

        public string ActiveStatusName
        {
            get
            {
                return this.IsActive == true ? "Yes" : "No";
            }
        }

        public List<TestRecommendationSegment> Segments { set; get; }
    }
}
