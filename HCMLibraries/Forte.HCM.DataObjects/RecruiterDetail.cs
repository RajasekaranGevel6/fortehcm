﻿using System;

namespace Forte.HCM.DataObjects
{
    public class RecruiterDetail
    {
        public string CandidateName { set; get; }
        public string CandidateEmail { set; get; }
        public string RecruiterName { set; get; }
        public string RecruiterFirstName { set; get; }
        public string RecruiterEmail { set; get; }

        public int UserID { set; get; }
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        public string Email { set; get; }
        public int ActivePositionAssignments { set; get; }
        public DateTime RecentPositionAssignment { set; get; }

        public string RecentPositionAssignmentString
        {
            get
            {
                if (this.RecentPositionAssignment == DateTime.MinValue)
                    return string.Empty;

                return this.RecentPositionAssignment.ToString("MM/dd/yyyy");
            }
        }
    }
}
