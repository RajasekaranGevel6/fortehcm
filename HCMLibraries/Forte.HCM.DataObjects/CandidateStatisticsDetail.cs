﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateStatisticsDetail.cs
// File that represents the Properties for candidate statistics detail.

#endregion Header

#region Directives

using System;
using ReflectionComboItem;
using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the Properties for candidate statistics detail.This class inherits candidate detail.
    /// </summary>
    [Serializable]
    public class CandidateStatisticsDetail : CandidateDetail
    {
        public string CandidateSessionID { set; get; }
        [DropDownItemAttribute("Test Date", "")]
        public DateTime TestDate { set; get; }

        public string TotalTimeTaken { set; get; }
        [DropDownItemAttribute("Average Time Taken", "")]
        public string AverageTimeTaken { set; get; }
        [DropDownItemAttribute("Interview Date", "TBD")]
        public string InterviewDate { set; get; }
        public int AttemptNumber { set; get; }
        public int RankOrder { set; get; }
        public int RelativeRankOrder { set; get; }
        [DropDownItemAttribute("Answered Correctly", "")]
        public decimal AnsweredCorrectly { set; get; }
        [DropDownItemAttribute("Absolute Score", "")]
        public decimal AbsoluteScore { set; get; }
        [DropDownItemAttribute("Relative Score", "")]
        public decimal RelativeScore { set; get; }
        [DropDownItemAttribute("Percentile", "")]
        public decimal Percentile { set; get; }
        [DropDownItemAttribute("Synopsis", "")]
        public string Synopsis { set; get; }
        [DropDownItemAttribute("Total Time", "")]
        public string TimeTaken { set; get; }
        public string Recruiter { set; get; }
        public string SessionCreator { set; get; }
        public string SessionCreatorFullName { set; get; }
        public string ScheduleCreator { set; get; }
        public string ScheduleCreatorFullName { set; get; }
        public int TotalQuestion { set; get; }
        public int TotalQuestionAttended { set; get; }
        public int TotalQuestionSkipped { set; get; }
        public int QuestionsAnsweredWrongly { set; get; }
        public int QuestionsAnsweredCorrectly { set; get; }
        public bool IsCyberProctoring { set; get; }
        public decimal MyScore { set; get; }
        public string InterviewComments { set; get; }
        public string TestStatus { set; get; }
        public List<DropDownItem> TestDetails { set; get; }
        public string TestName { set; get; }
        public decimal Marks { set; get; }
        public decimal MarksObtained { set; get; } 


        public string Assesor { get; set; }

        public string AssessorSkill { get; set; }
    }
}
