﻿using System;

namespace Forte.HCM.DataObjects
{
    public class AssessorTimeSlotSearchCriteria
    {
        public int ID { set; get; }
        public int AssessorID { set; get; }
        public int SchedulerID { set; get; }
        public int TimeSlotIDFrom { set; get; }
        public int TimeSlotIDTo { set; get; }
        public string TimeSlot { set; get; }
        public string ReferenceType { set; get; }
        public int InitiatedBy { set; get; }
        public DateTime InitiatedDate { set; get; }
        public DateTime RequestedDate { set; get; }
        public DateTime FromDate { set; get; }
        public DateTime ToDate { set; get; }
        public string SkillID { set; get; }
        public string Skill { set; get; }
        public string Comments { set; get; }
        public string RequestStatus { set; get; }
        public string SessionKey { set; get; }
    }
}
