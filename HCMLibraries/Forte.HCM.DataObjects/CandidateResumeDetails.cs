﻿
#region Namespace                                                              

using System;

#endregion Namespace

namespace Forte.HCM.DataObjects
{

    /// <summary>
    /// Class that represents the candidate resume details
    /// </summary>
    [Serializable]
    public class CandidateResumeDetails
    {

        #region Public Properties                                              

        /// <summary>
        /// A <see cref="System.Int32"/> that holds
        /// the candidate resume id
        /// </summary>
        public int CandidateResumeId { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds
        /// the candidate id
        /// </summary>
        public int CandidateId { set; get; }


        /// <summary>
        /// A <see cref="System.Byte"/> array that holds
        /// the binary format resume
        /// </summary>
        public byte[] ResumeSource { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds
        /// the processed date of the resume
        /// </summary>
        public DateTime ProcessedDate { set; get; }


        /// <summary>
        /// A <see cref="System.Decimal"/> that holds the
        /// score
        /// </summary>
        public decimal Score { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the mime type
        /// of the resume
        /// </summary>
        public string MimeType { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the file name 
        /// of the resume
        /// </summary>
        public string FileName { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds the 
        /// approved status of the resume
        /// </summary>
        public Int16 Approved { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the created
        /// user id of the resume
        /// </summary>
        public int CreatedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the created
        /// date of the resume
        /// </summary>
        public DateTime CreatedDate { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the modified user id
        /// of the resume
        /// </summary>
        public int ModifiedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the modified 
        /// date of the resume
        /// </summary>
        public DateTime ModifiedDate { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the Recruiter name  
        /// </summary>
        public string Recruiter { set; get; }
        
        /// <summary>
        /// A <see cref="System.String"/> that holds the resume status   
        /// </summary>
        public string Status { set; get; }
        
        /// <summary>
        /// A <see cref="System.String"/> that holds the status
        /// </summary>
        public string Active{ set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the Recruiter emailid 
        /// </summary>
        public string EmailID { set; get; }
        
        /// <summary>
        /// A <see cref="System.String"/> that holds the reason
        /// </summary>
        public string Reason { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the Complete status
        /// </summary>
        public string Complete { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the candidate name
        /// </summary>
        public string CandidateName { set; get; } 

        /// <summary>
        /// A <see cref="System.String"/> that holds the Duplicate status
        /// </summary>
        public string Duplicate { set; get; } 

        public string FirstName { get; set; }
        public string LastName { get; set; }
         
        public int UploadedBy { get; set; }
        public string UploadedByName { get; set; }
        public DateTime UploadedDate { get; set; }
        public int PageNumber { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the Recruiter name  
        /// </summary>
        public int Recruiterid { set; get; }
        #endregion Public Properties

    }
}
