﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class InterviewTestArea : StatisticsDetail
    {
        public string ID { set; get; }
        public string Name { set; get; }

        public InterviewTestArea(string id, string name)
        {
            this.ID = id;
            this.Name = name;
        }
    }
}
