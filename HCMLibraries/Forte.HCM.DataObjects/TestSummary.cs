﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TestSummary
    {
        public string TestID { set; get; }
        public string TestName { set; get; }
        public QuestionType QuestionType { set; get; }
        public int TimesAdministered { set; get; }
        public decimal MinimumScoreObtained { set; get; }
        public decimal MaximumScoreObtained { set; get; }
        public decimal AverageScore { set; get; }
        public int NumberOfQuestions { set; get; }
        public decimal TestCost { set; get; }
        public DateTime AverageTimeTaken { set; get; }
        public Complexity AverageComplexity { set; get; }
    }
}
