﻿using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Represents the class that holds the data for the 
    /// multiple series chart 
    /// </summary>
    [Serializable]
    public class MultipleChartData
    {
        /// <summary>
        /// Represents the name 
        /// </summary>
        public string Name
        {
            set;
            get;
        }

        /// <summary>
        /// Represents the short name
        /// </summary>
        public string ShortName
        {
            set;
            get;
        }

        /// <summary>
        /// Represents the score value of a specified candidate.
        /// </summary>
        public decimal MyScore
        {
            set;
            get;
        }

        /// <summary>
        /// Represents the minimum score of the test.
        /// </summary>
        public decimal MinScore
        {
            set;
            get;
        }
        /// <summary>
        /// Represents the maximum score of the test.
        /// </summary>
        public decimal MaxScore
        {
            set;
            get;
        }

        /// <summary>
        /// Represents the average score of the test.
        /// </summary>
        public decimal AvgScore
        {
            set;
            get;
        }

        public string ID
        {
            set;
            get;
        }

    }
}
