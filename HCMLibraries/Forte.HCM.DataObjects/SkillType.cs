﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SkillType.cs
// File that represents the skill types
// control.

#endregion Header
namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Represents the enum to hold the skill types
    /// </summary>
    public enum SkillType
    {
        /// <summary>
        /// Represents the Technical skill 
        /// </summary>
        Technical = 'T',
        /// <summary>
        /// Represents the Vertical skill 
        /// </summary>
        Vertical = 'V',
        /// <summary>
        /// Represents the Unknown skill 
        /// </summary>
        Unknown = 'U',
    }
}
