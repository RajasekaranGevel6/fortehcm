﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TrackingImageDetail
    {
        public string CandidateSessionID { set; get; }
        public int AttempID { set; get; }
        public string ImageID { set; get; }
        public int LogID { set; get; }
        public string Message { set; get; }
        public string ImagePath { set; get; }
    }
}
