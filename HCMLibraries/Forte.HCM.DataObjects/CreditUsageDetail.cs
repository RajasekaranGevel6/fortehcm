﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreditUsageDetail.cs
// File that represents the properties for credits usage detail.

#endregion Header
using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for credits usage detail.
    /// </summary>
    public class CreditUsageDetail
    {
        /// <summary>
        /// DateTime property that holds credit processed datetime
        /// </summary>
        public DateTime Date {set; get;}
        /// <summary>
        /// Decimal property that holds credit amount
        /// </summary>
        public decimal Credit {set; get;}
        /// <summary>
        /// Enum property that holds credit type
        /// </summary>
        public CreditType CreditType {set; get;}
        /// <summary>
        /// String property that holds credit description
        /// </summary>
        public string Description {set; get;}
        /// <summary>
        /// String property that holds credit type
        /// </summary>
        public string UsageHistoryCreditType { set; get; }
    }
}
