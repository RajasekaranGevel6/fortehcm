﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Module.cs
// File that represents the properties for user roles.

#endregion Header                                                              

#region Directives                                                             

using System;

#endregion Directives                                                          

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the user roles.
    /// </summary>
    [Serializable]
    public class UserRoles
    {
        public int UserRoleID { set; get; }
        public int UserSubRoleID { set; get; }
        public int RoleID { set; get; }
        public int CreatedBy { set; get; }
        public DateTime CreatedDate { set; get; }
        public int ModifiedBy { set; get; }
        public DateTime ModifiedDate { set; get; }
    }
}