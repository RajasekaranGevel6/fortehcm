﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class QuestionAttribute
    {
        public int MaxLength { get; set; }
        public int Marks { get; set; }
        public string AnswerReference { set; get; }
        public string Answer { set; get; }
        public int MarksObtained { set; get; }
    }
}