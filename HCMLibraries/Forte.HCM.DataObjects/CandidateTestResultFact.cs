﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestResultFact .cs
// File that represents the properties for candidate test results facts.

#endregion Header

#region Diretives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for candidate test results facts.
    /// </summary>                                                              
    [Serializable]
    public class CandidateTestResultFact : TrackingDetail
    {
        public string CandidateSessionKey { set; get; }
        public int AttemptID { set; get; }
        public string CandidateResultKey { set; get; }
        public int TestQuestionID { set; get; }
        public string QuestionKey { set; get; }
        public string Status { set; get; }
        public DateTime TimeTaken { set; get; }
        public char AnsweredCorrectly { set; get; }
        public DateTime StartTime { set; get; }
        public DateTime EndTime { set; get; }
        public char Skipped { set; get; }
    }
}
