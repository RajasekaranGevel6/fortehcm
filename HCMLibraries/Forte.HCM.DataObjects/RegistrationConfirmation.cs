﻿
#region Namespace                                                              

using System;

#endregion Namespace

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// 
    /// </summary>
    public class RegistrationConfirmation
    {

        #region Public Properties                                              

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the user registration id
        /// </summary>
        public int UserRegistrationId { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user confirmation code
        /// </summary>
        public string ConfirmationCode { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the number of attempts
        /// </summary>
        public int Attempts { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the verification date
        /// </summary>
        public DateTime? VerifiedOn { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user created by
        /// </summary>
        public string CreatedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the user created date
        /// </summary>
        public DateTime CreatedDate { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the user modified by
        /// </summary>
        public string ModifiedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the user modified date
        /// </summary>
        public DateTime ModifiedDate { set; get; }

        #endregion Public Properties

        public DateTime ExpiryDate { get; set; }

        public char LegalAccepted { get; set; }
    }
}
