﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ButtonType.cs
// File that represents the Credit Type for credit usage history
// control.

#endregion Header                                                              
namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that represents the values for credit type. It holds
    /// Both-0, Earned-1 and Used-2 status.
    /// </summary>
    public enum CreditType
    {
        /// <summary>
        /// Both status. Indicates both earned and used credits
        /// </summary>
        Both = 0,
        /// <summary>
        /// Earned status. Indicates only earned credits
        /// </summary>
        Earned = 1,
        /// <summary>
        /// Used status. Indicates only used credits
        /// </summary>
        Used = 2
    }
}
