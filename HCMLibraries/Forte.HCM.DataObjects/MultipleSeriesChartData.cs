﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MultipleSeriesChartData.cs
// File that has the properties for the chart such as 
// Chart X values , Chart Y values, height and width 

#endregion

#region Directives                                                             

using System;
using System.Data;
using System.Collections.Generic; 

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class MultipleSeriesChartData : SingleChartData
    {
        private bool isFullNumberYAxis = false;

        public List<MultipleChartData> MultipleSeriesChartDataSource { set; get; }

        public List<MultipleChartData> MultipleSeriesRelativeChartDataSource { set; get; }

        public bool IsHideComparativeScores { set; get; }

        //Data table to get and store data for comparison report 
        public DataTable ComparisonReportDataTable { set; get; }

        //this is to give the whole number for the y axis title
        //for displaying number of candidates
        public bool IsFullNumberYAxis
        {
            set
            {
                isFullNumberYAxis = value;
            }
            get
            {
                return isFullNumberYAxis;
            }
        }        

    }
}
