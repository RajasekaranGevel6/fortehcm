﻿namespace Forte.HCM.DataObjects
{
    public class ExternalDataStructure
    {
        public string DataKey { set; get; }
        public string DataStructure { set; get; }
        public string Comments { set; get; }
    }
}
