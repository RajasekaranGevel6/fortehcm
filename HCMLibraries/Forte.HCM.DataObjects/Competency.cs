﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class Competency
    {
        public string SkillName {set; get;}
        public string Context {set; get;}
        public DateTime DateLastUsed {set; get;}
        public string DurationOfUse {set; get;}
        public string ProjectsUsed {set; get;}
        public bool Certification {set; get;}
    }
}
