﻿namespace Forte.HCM.DataObjects
{
    public class CandidateCompetencyVectorDetail : CompetencyVectorDetails
    {
        public int CandidateResumeID { set; get; }
        public int ParameterID { set; get; }
        public string Value { set; get; }
        public string Remarks { set; get; }
        public int UserID { set; get; }
    }
}
