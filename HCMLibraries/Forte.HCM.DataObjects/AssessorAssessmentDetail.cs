﻿using System;

namespace Forte.HCM.DataObjects
{
    public class AssessorAssessmentDetail : TrackingDetail
    {
        public string CandidateName { set; get; }

        public string CandidateEamil { set; get; }

        public DateTime CompletedDate { get; set; }

        public string InterviewName { get; set; }

        public string PositionProfileName { get; set; }

        public string AssessmentStatus { get; set; }

        public string TestStatus { get; set; }

        public string CandidateInterviewSessionKey { set; get; }

        public int AssessorId { set; get; }

        public string InterviewSessionKey { set; get; }

        public string UsrLastName { set; get; }

        public int AttemptID { get; set; }

        public string InterviewTestKey { get; set; }
        public string ClientName { set; get; }

        public string CompletedDateString 
        { 
            get
            {
                return CompletedDate.ToString("MM/dd/yyyy");
            }
        }
    }
}
