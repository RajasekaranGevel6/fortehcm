﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class NomenclatureAttributes
    {
        public string Client { get; set; }
        public string ClientName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailID { get; set; }
        public string WebsiteURL { get; set; }
        public string AdditionalInfo { get; set; }
        public string ContentAuthor { get; set; }

    }
    [Serializable]
    public class CommonNomenclatureAttributes
    {
        public string Client { get; set; }
        public string ClientName { get; set; }
        public string ClientDepartment { get; set; }
        public string ClientContact { get; set; }
    }
    [Serializable]
    public class NomenclatureAllAttributes
    {
        public CommonNomenclatureAttributes CommonNomenclatureAttributes { get; set; }
        public NomenclatureCustomize ClientNomenClatureAttrubutes { get; set; }
        public NomenclatureCustomize DepartmentNomenClatureAttrubutes { get; set; }
        public NomenclatureCustomize ContactNomenClatureAttrubutes { get; set; }
    }
}
