﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TestScheduleSearchCriteria
    {
        public string CandidateName {set; get;}
        public string EMail {set; get;}
        public string TestSessionID {set; get;}
        public string CandidateTestSessionID {set; get;}
        public string TestName {set; get;}
        public string TestScheduleAuthor {set; get;}
        public TestCompletedStatus TestCompletedStatus {set; get;}
        public bool IsMaximized { set; get; }
        public string SortExpression { set; get; }
        public SortType SortDirection { set; get; }
        public int CurrentPage { set; get; }
        public string CandidateSessionIDs { set; get; } 

        public List<TestDetail> ListTestdetail { get; set; }
        public List<TestDetail> ListTestSessiondetail { get; set; }
    }
}
