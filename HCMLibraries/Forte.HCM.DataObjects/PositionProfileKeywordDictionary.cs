﻿
using System;
namespace Forte.HCM.DataObjects
{
    [Serializable]
   public class PositionProfileKeywordDictionary:TrackingDetail
    {
       public int DictionaryID { set; get; }
       public string Keyword { set; get; }
       public string SkillCategory { set; get; }
       public SkillType skillTypes { set; get; }
    }
}
