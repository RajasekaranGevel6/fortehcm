﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateReportDetail.cs
// File that represents the Properties for candidate report detail

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the Properties for candidate report detail.
    /// </summary>
    [Serializable]
    public class CandidateReportDetail
    {
        public int CandidateID { set; get; }
        public string CandidateName { set; get; }
        public string CandidatesID { set; get; }
        public string TestKey { set; get; }
        public string TestName { set; get; }
        public string LoginUser { set; get; }
        public string CandidateSessionkey { set; get; }
        public int AttemptID { set; get; }
        public string AttemptsID { set; get; }
        public string CombinedSessionKeyAttemtID { set; get; }
        public string CombinedSubjectIDs { set; get; }
        public string CombinedCandidateIDs { set; get; }
        public string CandidateNames { set; get; }
    }
}
