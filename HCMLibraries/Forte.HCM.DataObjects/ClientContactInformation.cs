﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for client contact information.
    /// </summary> 
    [Serializable]
    public class ClientContactInformation:TrackingDetail
    {
        public int ClientID { set; get; }
        public int? ContactID { set; get; }
        public int? DepartmentID { set; get; }
        public string ClientDepartmentContactID { set; get; }
        public int TenantID { set; get; }

        public string ContactName { set; get; }
        public string ClientName { set; get; }
        
        public string FirstName { set; get; }
        public string MiddleName { set; get; }

        public string LastName { set; get; }
        public string AdditionalInfo { set; get; }
        public string ClientAdditionalInfo { set; get; }

        public string FaxNo { set; get; }

        public List<Department> Departments { set; get; }
        public ContactInformation ContactInformation { set; get; }
        public string EmailAddress { set; get; }
        public string PhoneNumber { set; get; }
        public string MobileNumber { set; get; }

        public string SellectedDeparments { set; get; }

        public int NoOfDepartments { set; get; }
        public int NoOfContacts { set; get; }
        public int NoOfPositionProfile { set; get; }
        public string CreatedByName { set; get; }
        public string DepartmentName { set; get; }
        public bool IsContactRequired { set; get; }


        public string IsSelected { get; set; }
    }
}
