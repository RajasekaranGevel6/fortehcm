﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    public class CareerBuilderResumeSearchCriteria
    {
        public string SessionToken { set; get; }
        public string Keywords{ set; get; } 
        public string SearchType{ set; get; } 
        public string City{ set; get; } 
        public string ZipCode{ set; get; } 
        public string RadiusMiles{ set; get; } 
        public string State{ set; get; } 
        public string Country{ set; get; } 
        public string EmploymentType{ set; get; } 
        public string WorkStatus{ set; get; } 
        public string MinimumEducation{ set; get; } 
        public string FreshnessDays{ set; get; } 
        public string PageNumber{ set; get; } 
        public string RowsPerPage{ set; get; }
        public string CustAcctCode { set; get; } 
    }
}
