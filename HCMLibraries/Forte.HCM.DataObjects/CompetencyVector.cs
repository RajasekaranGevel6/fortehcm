﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class CompetencyVector
    {
        public int VectorID { set; get; }
        public string VectorType { set; get; }
        public string Skill { set; get; }
        public int NoOfYears { set; get; }
        public int Recency { set; get; }
        public int Certification { set; get; }
        public int GroupID { set; get; }
        public int UserID { set; get; }
    }
}
