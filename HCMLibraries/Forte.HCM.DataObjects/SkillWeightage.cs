﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class SkillWeightage
    {
       public string SkillName { set; get; }
       public decimal Weightage { set; get; }
       public bool UserSegments { set; get; }
       public SkillType SkillTypes { set; get; }
       public string SkillCategory { set; get; }

       public bool IsTestRequired { set; get; }
       public bool IsInterviewRequired { set; get; }
    }
}
