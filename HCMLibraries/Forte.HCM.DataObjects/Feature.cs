﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    public class Feature
    {
        public int feaID { set; get; }
        public int modID { set; get; }
        public int smoID { set; get; }
        public string feaFeatureName { set; get; }
        public string feaURL { set; get; }
        public int feaSequenceNo { set; get; }
        public string feaHelpURL { set; get; }

    }
}
