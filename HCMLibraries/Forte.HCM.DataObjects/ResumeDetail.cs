﻿namespace Forte.HCM.DataObjects
{
    public class ResumeDetail
    {
        public string HRXML { set; get; }
        public bool Approved { set; get; }
        public byte[] ResumeSource { set; get; }
    }
}
