﻿namespace Forte.HCM.DataObjects
{
    public class DictionaryDetail : ElementDetail
    {
        public int AliasID { set; get; }
        public string AliasName { set; get; }
    }
}
