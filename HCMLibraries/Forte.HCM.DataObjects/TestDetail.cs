﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Category.cs
// File that represents the class which contains TestDetail and derived from TrackingDetail information.

#endregion Header                                                              

#region Directives

using System;
using System.Collections.Generic;

#endregion Directives  

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains TestKey id, name,Description,RecommendedCompletionTime,SystemRecommendedTime,
    /// NoOfQuestions,Certification,CertificationId,TestAuthorID,TestAuthorName,TestCost,TestType,TestCreationDate,
    /// NoOfCandidatesAdministered, Questions Details,,Certification Details,TestStatus,TestMode,SessionIncluded,
    /// SessionIncludedActive,createdby, created date.modifiedby, modified date members and constructor methods.
    /// </summary>
    [Serializable]
    public class TestDetail:TrackingDetail
    {
        public string TestKey {set; get;}
        public string Name {set; get;}
        public string Description {set; get;}
        public int RecommendedCompletionTime {set; get;}
        public int SystemRecommendedTime {set; get;}
        public int NoOfQuestions { set; get; }
        public bool ? IsCertification { set; get; }
        public int CertificationId { set; get; }
        public int TestAuthorID { set; get; }
        public string TestAuthorName { set; get; }
        public string TestAuthorFullName { set; get; }
        public decimal TestCost { set; get; }
        public TestType TestType {set; get;}
        public QuestionType QuestionType { set; get; }
        public DateTime TestCreationDate { set; get; }
        public int NoOfCandidatesAdministered { set; get; }
        public List<QuestionDetail>     Questions {set; get;}
        public CertificationDetail CertificationDetail {set; get;}
        public TestStatus TestStatus {set; get;}
        public string TestMode { set; get; }
        public bool SessionIncluded { set; get; }
        public bool SessionIncludedActive { set; get; }
        public string Question_Key { set; get; }
        public int ? PositionProfileID { set; get; }
        public string PositionProfileName { set; get; }
        public int TenantID { set; get; }


        public short TestRecommendedID { get; set; }

        public int CandidateID { get; set; }

        public string Status { get; set; }

        public int UserID { get; set; }

        public int SessionCount { get; set; }
        public string SessionKey { get; set; }

        public string SessionValue { get; set; }
    }
}

