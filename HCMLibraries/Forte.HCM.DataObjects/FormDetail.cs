﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class FormDetail : TrackingDetail
    {
        public int FormID { set; get; }

        public string FormName { set; get; }

        public int DesignedBy { set; get; }

        public string Tags { set; get; }

        public List<Segment> segmentList { set; get; }

        public string UserFirstName { set; get; }

        public string UserFullName { set; get; }

        public int ?SearchDesignedBy { set; get; }

        public bool IsGlobalForm { set; get; }
        public string GlobalFormText { set; get; }
        public bool AllowEdit { set; get; }

        public int CurrentPage { set; get; }
        public string SortExpression { set; get; }
        public SortType SortDirection { set; get; }
        public bool IsMaximized { set; get; }

        public string CreatedByName { set; get; }
        public int TenantID { set; get; }
        public int UserID { set; get; }
    }
}
