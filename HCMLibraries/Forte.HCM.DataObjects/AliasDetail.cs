﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class AliasDetail : TrackingDetail
    {
        public int ElementID { set; get; }
        public int ID { set; get; }
        public string Name { set; get; }
    }
}