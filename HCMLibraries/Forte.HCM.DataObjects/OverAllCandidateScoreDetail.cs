﻿#region Directives                                                             
using System;
using System.Collections.Generic;
#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class that holds the over all candidate score details
    /// </summary>
    public class OverAllCandidateScoreDetail
    {
        #region Properties                                                     
        public decimal AbsoluteMyScore { set; get; }
        public decimal AbsoluteMinScore { set; get; }
        public decimal AbsoluteMaxScore { set; get; }
        public decimal AbsoluteAvgScore { set; get; }
        public decimal RelativeMyScore { set; get; }
        public decimal RelativeMinScore { set; get; }
        public decimal RelativeMaxScore { set; get; }
        public decimal RelativeAvgScore { set; get; }
        public SingleChartData ChartScoreDetails { set; get; }
        public List<ChartData> AbsoluteScoreDetails { set; get; }
        public List<ChartData> RelativeScoreDetails { set; get; }
        public List<ChartData> AbsoluteCandidateScoreDetails { set; get; }
        public List<ChartData> RelativeCandidateScoreDetails { set; get; }
        #endregion Properties
    }
}
