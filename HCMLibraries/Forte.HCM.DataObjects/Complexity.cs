﻿namespace Forte.HCM.DataObjects
{
    public class Complexity : StatisticsDetail
    {
        public string ID { set; get; }
        public string Name { set; get; }
        public decimal value { set; get; }

        public Complexity()
        { 

        }

        public Complexity(string id, string name)
        {
            this.ID = id;
            this.Name = name;
        }
    }
}
