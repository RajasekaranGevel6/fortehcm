﻿using System;

namespace Forte.HCM.DataObjects
{
    public class TestScheduleDetail
    {
        public string TestID { set; get; }
        public string TestName { set; get; }
        public string TestSessionID { set; get; }
        public int AttemptID { set; get; }
        public string CandidateTestSessionID { set; get; }
        public string ClientRequestID { set; get; }
        public string CandidateID { set; get; }
        public string CandidateName { set; get; }
        public string CandidateFullName { set; get; }
        public string CandidateEmail { set; get; }
        public string EmailId { set; get; }
        public DateTime CreatedDate { set; get; }
        public string TestSessionAuthor { set; get; }
        public DateTime ExpiryDate { set; get; }
        public DateTime SessionExpiryDate { set; get; }
        public ScheduleStatus ScheduleStatus { set; get; }
        public DateTime CompletedDate { set; get; }
        public string SchedulerFirstName { set; get; }
        public string SchedulerName { set; get; }
        public string SchedulerFullName { set; get; }
        public string SchedulerEmail { set; get; }
        public string TestStatus { set; get; }
        public bool IsRescheduled { set; get; }
        public bool IsRequestToRetake { set; get; }
        public DateTime ? ScheduledDate { set; get; }
        public string DefaultAssessor { set; get; }
        public string EmailReminder { get; set; }
        
    }
}
