﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class AssessorDetail : TrackingDetail
    {
        public int UserID { get; set; }

        public string UserEmail { get; set; }

        public string Skill { get; set; }

        public string AlternateEmailID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public char Active { get; set; }

        public string NonAvailabilityDates { get; set; }

        public string Image { get; set; }
        
        public string Mobile { get; set; }

        public string HomePhone { get; set; }

        public string AdditionalInfo { get; set; }

        public int AssessorId { get; set; }
        
        public string AssessorName { get; set; }

        public int skillId { get; set; }

        public string AssessmentSatus { get; set; }

        public string Comments { get; set; }

        public decimal OverallRating { get; set; }

        public DateTime CompletedOn { get; set; }

        public decimal assessorRating { get; set; }

        public List<SkillDetail> Skills { get; set; }

        public string LinkedInProfile { set; get; }

        public string ChatRoomKey { set; get; }

        public int RequestedSlotID { set; get; }

        public bool IsAssessor { set; get; }

        public int QuestionRating { set; get; }

        public int CandidateInterviewID { set; get; }
    }
}
