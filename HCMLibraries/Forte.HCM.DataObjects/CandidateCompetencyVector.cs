﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class CandidateCompetencyVector
    {

        public int Cand_Resume_Id { set; get; }
        /// <summary>
        /// This field is used when we are updating vector id.
        /// </summary>
        public int Old_Vector_Id { set; get; }
        public int Vector_Id { set; get; }
        public int Parameter_Id { set; get; }
        public string CompetencyValue { set; get; }
        public string Remarks { set; get; }
        public int Created_By { set; get; }
    }
}
