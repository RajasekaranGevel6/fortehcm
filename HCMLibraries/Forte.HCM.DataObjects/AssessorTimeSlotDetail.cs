﻿using System;
using Forte.HCM.Support;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class AssessorTimeSlotDetail 
    {
        public int ID { set; get; }
        public int TimeSlotID { set; get; }
        public int AssessorID { set; get; }
        public string Assessor { set; get; }
        public DateTime AvailabilityDate { set; get; }
        public DateTime AcceptedDate { set; get; }
        public int TimeSlotIDFrom { set; get; }
        public int TimeSlotIDTo { set; get; }
        public string TimeSlotTextFrom { set; get; }
        public string TimeSlotTextTo { set; get; }
        public string TimeSlot { set; get; }
        public string SessionDesc { set; get; }
        public string SessionKey { set; get; }
        public string CandidateSessionKey { set; get; }
        public int InitiatedBy { set; get; }
        public string InitiatedByName { set; get; }
        public DateTime InitiatedDate { set; get; }
        public string Skill { set; get; }
        public int SkillID { set; get; }
        public string Comments { set; get; }
        public string RequestStatus { set; get; }
        public bool IsAvailable { set; get; }
        public string IsAvailableFullTime { set; get; }
        public string VacationDates { set; get; }
        public int RequestDateGenID { set; get; }
        public int AcceptedID { set; get; }
        public string InterviewName { set; get; }
        public string InterviewKey { set; get; }
        public string FirstName { set; get; }
        public string Email { set; get; }
        public string IsAssessorAvailable { set; get; }

        public string Scheduled { set; get; }

        public string AvailabilityDateString
        {
            get
            {
                return AvailabilityDate.ToString("ddd, MMM dd yyyy");
            }
        }

        public string InitiatedDateString
        {
            get
            {
                return InitiatedDate.ToString("ddd, MMM dd yyyy");
            }
        }

        public string RequestStatusName
        {
            get
            {
                if (RequestStatus == "I")
                    return Constants.TimeSlotRequestStatus.PENDING_NAME;
                else if (RequestStatus == "A")
                    return Constants.TimeSlotRequestStatus.ACCEPTED_NAME;
                else if (RequestStatus == "R")
                    return Constants.TimeSlotRequestStatus.REJECTED_NAME;
                else
                    return string.Empty;
            }
        }
        public List<AssessorTimeSlotDetail> assessorTimeSlotDetails { get; set; }
        public List<AssessorDetail> assessorDetail { get; set; }

        public OnlineInterviewSessionDetail onlineInterviewDetail { get; set; }
        
    }
}