﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateInterviewSessionDetail.cs
// File that represents the class which contains Candidate Test Session
// detail derived from CandidateInterviewSessionDetail.

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains GenID, CandidateTestSessionID,CandidateID,CandidateName,
    /// CandidateFirstName, Email, Status, CancelReason, NoOfAttempt, AttemptID,
    /// RetakeRequest, DateCompleted, ScheduledDate, ShowResults members and public
    /// properties.
    /// </summary>
    [Serializable]
    public class CandidateInterviewSessionDetail : InterviewSessionDetail
    {
        public int GenID { set; get; }
        public string CandidateTestSessionID { set; get; }
        public string CandidateID { set; get; }
        public int CandidateUserID { set; get; }
        public int CandidateInformationID { set; get; }
        public string CandidateName { set; get; }
        public string CandidateFirstName { set; get; }
        public string CandidateLastName { set; get; }
        public string CandidateFullName { set; get; }
        public int CandidateAssessorCount { set; get; }
        public string Status { set; get; }
        public string SecurityCode { set; get; }
        public string CancelReason { set; get; }
        public Int16 NoOfAttempt { set; get; }
        public int AttemptID { set; get; }
        public string RetakeRequest { set; get; }
        public DateTime DateCompleted { set; get; }
        public DateTime ScheduledDate { set; get; }
        public bool ShowResults { set; get; }

        public string CandidateEmail { set; get; }
        public string CandidateOwnerEmail { set; get; }
        public int SchedulerID { set; get; }
        public string SchedulerEmail { set; get; }
        public string SchedulerName { set; get; }
        public string SchedulerFirstName{ set; get; }
        public string SchedulerLastName { set; get; }
        public string SchedulerCompany { set; get; }
        public string PositionProfileOwnerEmail { set; get; }
        public int ClientID { set; get; }
        public string ClientName { set; get; }
        public string InterviewPublishCode { set; get; }

        /// <summary>
        /// Property that contains the user name who is initiated a test for a candidate.
        /// This property will be used in CandidateDLManager's GetCandidateSummary method.
        /// <remarks>It is used for identifying the 'SELF' username in the CandidateHome.</remarks>
        /// </summary>
        public string TestInitiatedBy { set; get; }
        public bool IsPaused { set; get; }

        public string InterviewSessionKey { set; get; }
        public string CandidateInterviewSessionID { set; get; }
        public string InterviewTestKey { set; get; }

        public decimal totalScore { set; get; }
        public decimal totalWeightedScore { set; get; }

        public string CompletedDate{ set; get; }
        public string AbsoluteScore { set; get; }
        public string NavigateKey { set; get; }
        public string EmailRemainder { set; get; }
        public string ChatRoomKey { set; get; }
        public int CandidateInterviewID { set; get; }
        public string SlotKey { set; get; }
        public string TimeSlotTextFrom { set; get; }
        public string TimeSlotTextTo { set; get; }
        
        #region Public Properties

        /// <summary>
        /// Property that will return a date if it is not a min date.
        /// </summary>
        public string DateCompletedFormatted
        {
            get
            {
                if (DateCompleted == DateTime.MinValue)
                    return string.Empty;

                return DateCompleted.ToString("MM/dd/yyyy");
            }
        }

        /// <summary>
        /// Property that will return a date if it doesn't have min date.
        /// </summary>
        public string ScheduledDateFormatted
        {
            get
            {
                if (ScheduledDate == DateTime.MinValue)
                    return string.Empty;

                return ScheduledDate.ToString("MM/dd/yyyy");

            }
        }

        #endregion  Public Properties
    }
}