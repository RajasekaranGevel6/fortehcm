﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class ClientManagementState
    {
        public string ClientClientName { set; get; }
        public string ClientContactName { set; get; }
        public bool ClientUserCheckbox { set; get; }
        public string ClientLocation { set; get; }
        public string ClientCreatedBy { set; get; }
        public string ClientCreatedHiddenField { set; get; }
        public string ClientOrderBy { set; get; }
        public SortType ClientOrderDirection { set; get; }
        public string ClientPageNo { set; get; }
        public string ClientDeparmentId { set; get; }
        public string ClientContactId { set; get; }



        public string DepartmentClientName { set; get; }
        public string DepartmentContactName { set; get; }
        public bool DepartmentUserCheckbox { set; get; }
        public string DepartmentName { set; get; }
        public string DepartmentCreatedBy { set; get; }
        public string DepartmentCreatedHiddenField { set; get; }
        public string DepartmentOrderBy { set; get; }
        public SortType DepartmentDirection { set; get; }
        public string DepartmentPageNo { set; get; }
        public string DeparmentClientId { set; get; }
        public string DepartmentContactId { set; get; }

        public string ContactClientName { set; get; }
        public string ContactContactName { set; get; }
        public bool ContactUserCheckbox { set; get; }
        public string ContactDepartment { set; get; }
        public string ContactCreatedBy { set; get; }
        public string ContactCreatedHiddenField { set; get; }
        public string ContactOrderBy { set; get; }
        public SortType ContactOrderDirection { set; get; }
        public string ContactPageNo { set; get; }

        public string ContactClientId { set; get; }
        public string ContactDepartmentId { set; get; }

    }
}
