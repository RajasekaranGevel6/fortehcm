﻿#region Directives

using System;

#endregion  Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class LineageItem
    {
        public string Lineage { set; get; }

        public string ComponentName { set; get; }

    }
}
