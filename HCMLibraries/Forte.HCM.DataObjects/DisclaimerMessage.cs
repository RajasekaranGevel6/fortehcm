﻿namespace Forte.HCM.DataObjects
{
    public class DisclaimerMessage
    {
        public string SaveQuestion { set; get; }
        public string TestIntroduction { set; get; }
        public string HardwareInstructions { set; get; }
        public string TestInstructions { set; get; }
        public string InterviewInstructions { set; get; }
        public string CyberProctoringInstructions { set; get; }
        public string WarningInstructions { set; get; }
    }
}
