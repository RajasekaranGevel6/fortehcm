﻿#region Directives                                                             
using System; 
#endregion

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class that holds the technical skills details
    /// </summary>
    public class LocationDetails
    {
        public string LocationType { set; get; }
        public int LocationTypeID { set; get; }
        public string LocationName { set; get; }
        public int LocationID { set; get; }
        public string LocationAliasName { set; get; }
    }
}
