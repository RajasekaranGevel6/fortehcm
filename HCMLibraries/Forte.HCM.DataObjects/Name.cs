﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Name.cs
// File that represents the class which contains candidate name
// details.

#endregion Header                                                              

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains FirstName, MiddleName and LastName members.
    /// </summary>
    public class Name
    {
        public string FirstName {set; get;}
        public string MiddleName {set; get;}
        public string LastName {set; get;}
    }
}
