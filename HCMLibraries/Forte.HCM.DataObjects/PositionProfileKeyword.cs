﻿

using System;
namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfileKeyword:TrackingDetail
    {
        public int KeywordID{ set; get; }
        public int PositionProfileID { set; get; }
        public string SegmentKeyword { set; get; }
        public string UserKeyword { set; get; }
        public string Keyword { set; get; }
    }
}
