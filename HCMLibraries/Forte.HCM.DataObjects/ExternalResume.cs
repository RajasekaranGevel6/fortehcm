﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ExternalResume.cs
// File that represents the class which contains External Resume details.

#endregion Header                                                              

#region Directives                                                             
using System;
using System.Collections.Generic;
#endregion Directives                                                          

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for ExternalResume detail.
    /// </summary>
    [Serializable]
    public class ExternalResume : TrackingDetail
    {
        public int ExternalResumeID { set; get; }
        public string ResumeID { set; get; }
        public string ResumeDetails { set; get; }
        public string ResumeTitle { set; get; }
        public byte[] ResumeContent { set; get; }
        public string ResumeText { set; get; }
        public int CandidateID { set; get; }
        public string JobBoardID { set; get; }
        public string CandidateName { set; get; }
        public string CandidateAddress { set; get; }
        public string RecentPay { set; get; }
        public string MostRecentTitle { set; get; }
        public string ExperienceMonths { set; get; }
        public string HighestDegree { set; get; }
        public string MostRecentCompanyName { set; get; }
        public string LastModified { set; get; }
        public string ResumeType { set; get; }
        // Personel Preview information
        public string CandidateEmail { set; get; }
        public string CandidatePhone { set; get; }
        public string ZipCode { set; get; }
        public string DesiredPay { set; get; }
        public string DesiredTravel{ set; get; }
        public string DesiredReLocations { set; get; }
        public string DesiredCommute { set; get; }
        public string DesiredJobTypes { set; get; }



        public List<Experience> ExperienceHistory { set; get; }

        public List<EducationHistory> EducationHistory { set; get; }

        public string MilitaryExperience { set; get; }
        public string ManagedOthers { set; get; }
        public string FelonyConviction { set; get; }
        public string SecurityClearance { set; get; }
        public string Languages { set; get; }

    }

    [Serializable]
    public class Experience
    {
        public string Company { set; get; }
        public string Title { set; get; }
        public string Period { set; get; }
    }
    [Serializable]
    public class EducationHistory
    {
        public string School { set; get; }
        public string Major { set; get; }
        public string Degree { set; get; }
    }

     [Serializable]
    public class CareerBuilderSearchResults
    { 
        public string HomeLocation { set; get; }
        public string ContactName { set; get; }
        public string RecentPay { set; get; }
        public string LastUpdate { set; get; }
        public string ContactEmail { set; get; }
        public string ResumeID { set; get; }
    }
}
