﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ChartData.cs
// File that has the properties for the chart such as 
// Chart X values and Chart Y values

#endregion

#region Directives                                                             
using System;
#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that defines a data for the chart. 
    /// Holds the X value and Y value for the data.
    /// </summary>
    [Serializable]
    public class ChartData
    {
        #region Properties                                                     

        /// <summary>
        /// Represents the X value for the chart
        /// </summary>
        public string ChartXValue { set; get; }

        /// <summary>
        /// Represents the Y value for the chart
        /// </summary>
        public int ChartYValue { set; get; }

        #endregion Properties

        #region Constructor                                                    

        /// <summary>
        /// Represents the constructor with x and y value
        /// </summary>
        /// <param name="xValue">
        /// A<see cref="string"/>that holds the xValue
        /// </param>
        /// <param name="yValue">
        /// A<see cref="string"/>that holds the yValue
        /// </param>
        public ChartData(string xValue, int yValue)
        {
            this.ChartXValue = xValue;
            this.ChartYValue = yValue;
        }

        /// <summary>
        /// Represents the default constructor
        /// </summary>
        public ChartData()
        {

        }
        /// <summary>
        /// Represents the constructor with 
        /// string x value and decimal y value
        /// </summary>
        /// <param name="xValue">
        /// A<see cref="string"/>that holds the string value
        /// </param>
        /// <param name="yValue">
        /// A<see cref="decimal"/>that holds the decimal 
        /// y value
        /// </param>
        public ChartData(string xValue, decimal yValue)
        {
            this.ReportChartXValue = xValue;
            this.ReportChartYValue = yValue; 
        }

        #endregion  Constructor

        #region Properties                                                     

        /// <summary>
        /// Represents the X value for the chart
        /// </summary>
        public string ReportChartXValue { set; get; }

        /// <summary>
        /// Represents the Y value for the chart
        /// </summary>
        public decimal ReportChartYValue { set; get; }

        #endregion Properties      
    }
}
