﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EducationLevel.cs
// File that represents the Education Level 
// control.

#endregion Header                                                              

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Represents the enum that holds the value of the 
    /// various education level.
    /// </summary>
    public enum EducationLevel
    {
        /// <summary>
        /// Masters level. Indicates the masters education level        
        /// </summary>
        Masters =1,

        /// <summary>
        /// Bachelors level.Indicates the bachelors education level        
        /// </summary>
        Bachelors = 2,

        /// <summary>
        /// Associate level. Indicates the associate education level
        /// </summary>
        Associate = 3,

        /// <summary>
        /// Diploma level. Indicates the diploma education level
        /// </summary>
        Diploma = 4

    }
}
