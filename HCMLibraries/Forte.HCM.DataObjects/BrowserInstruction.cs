﻿namespace Forte.HCM.DataObjects
{
    public class BrowserInstruction
    {
        public string Instruction { set; get; }

        public BrowserInstruction()
        {

        }

        public BrowserInstruction(string instruction)
        {
            this.Instruction = instruction;
        }
    }
}
