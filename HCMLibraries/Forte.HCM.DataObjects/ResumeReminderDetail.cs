﻿using System;

namespace Forte.HCM.DataObjects
{
    public class ResumeReminderDetail
    {
        public int ID { set; get; }
        public string CandidateName { set; get; }
        public string EmailID { set; get; }
        public DateTime ReminderDate { set; get; }
        public int UserID { set; get; }
        public string UserName { set; get; }
        public string Password { set; get; }

        // Incomplete resume reminders.
        public string EmailIDs { set; get; }
        public string RecruiterFirstName { set; get; }
        public string Company { set; get; }
        public string ProfileNames { set; get; }
        public string AdminFirstName { set; get; }
    }
}
