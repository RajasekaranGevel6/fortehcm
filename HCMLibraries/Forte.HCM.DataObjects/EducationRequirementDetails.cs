﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EducationRequirementDetails.cs
// File that represents details for the education requirement details

#endregion Header

#region Directives
using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class that holds the education requirement details
    /// </summary>
    public class EducationRequirementDetails
    {
        #region Properties
        public string Specialization { set; get; }
        public string EducationLevel { set; get; } 
        #endregion
    }
}
