﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TestSessionIdDetails
    {
        public int AttemptID { set; get; }
        public string CandidateSessionId { set; get; }
        public int CandidateId { set; get; }
    }
}
