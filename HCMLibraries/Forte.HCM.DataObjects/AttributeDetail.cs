﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AttributeDetail.cs
// File that represents the properties for admin choices.

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for admin choices.This class inherits tracking detail class.
    /// </summary> 
    [Serializable]
    public class AttributeDetail : TrackingDetail
    {
        public string AttributeID { set; get; }
        public string AttributeType { set; get; }
        public string AttributeName { set; get; }
        public string AttributeValue { set; get; }

        public AttributeDetail()
        {
        }

        public AttributeDetail(string attributeID, string attributeName)
        {
            this.AttributeID = attributeID;
            this.AttributeName = attributeName;
        }
        public int count { set; get; }
    }
}
