﻿#region Directives                                                             

using System; 

#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class that holds the technical skills details
    /// </summary>
    public class TechnicalSkillsDetails
    {
        public string TechGroup { set; get; }
        public string TechGroupID { set; get; }
        public string TechSkillName { set; get; }
        public int TechSkillID { set; get; }
        public string TechSkillAliasName { set; get; }
    }
}
