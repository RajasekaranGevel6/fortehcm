﻿namespace Forte.HCM.DataObjects
{
    public class InterviewSessionSearch
    {
        public string InterviewSessionID { set; get; }
        public string InterviewKey { set; get; }
        public string CandidateSessionID { set; get; }
        public string ClientRequestNumber { set; get; }
        public string DateofPurchase { set; get; }
        public string Credits { set; get; }
        public int AdministeredID { set; get; }
        public string AdministeredBy { set; get; }
        public string InterviewAuthorFullName { set; get; }
        public int CandidateID { set; get; }
        public string CandidateName { set; get; }
        public string CandidateFullName { set; get; }
        public object DateofInterview { set; get; }
        public bool CandidateVisibleStatus { set; get; }
        public string CancelReason { set; get; }
        public bool ShowCancelIcon { set; get; }
        public int AttemptID { set; get; }
        public bool ShowInterviewScore { set; get; }
    }
}
