﻿namespace Forte.HCM.DataObjects
{
    public class CandidateSkillScoreDetail
    {
        public string Skill { set; get; }
        public double ?TestScore { set; get; }
        public double ?InterviewScore { set; get; }
        public double ?ResumeScore { set; get; }
    }
}
