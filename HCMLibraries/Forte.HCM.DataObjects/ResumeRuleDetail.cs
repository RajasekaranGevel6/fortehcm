﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the  resume rules details
    /// </summary>
    [Serializable]
    public class ResumeRuleDetail
    {
        public int TenantId { get; set; }
        public int RuleId{get;set;}
        public string RuleName { get; set; }
        public string RuleReason { get; set; }
        public string RuleStatus { get; set; }
        public string RuleComments { get; set; }
        public string RuleAction { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public string SourceElementNode { set; get; }
        public string DestinationElementNode { set; get; }
        public string ExpressionType { set; get; }
        public string ExpressionOption { set; get; }
        public int SourceElementID { set; get; }
        public int DestinationElementID { set; get; }
        public string DestinationValue { set; get; }
        public string ExpressionTypeId { set; get; }
        public string LogicalOperaterId { set; get; }
        public string XmlFields { set; get; }
        public string SqlFields { set; get; }
        public string XmlCondition { set; get; }
        public string XmlNode { set; get; }
        public int RowID { get; set; }

        public List<ResumeRuleDetail> ResumeRuleEntries { set; get; }

    }
}
