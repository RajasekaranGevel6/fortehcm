﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for client department information.
    /// </summary> 
    [Serializable]
    public class Department:TrackingDetail
    {
        public int DepartmentID { set; get; }
        public int ClientID { set; get; }
       
        public string DepartmentName { set; get; }
        public string DepartmentDescription { set; get; }
        public string AdditionalInfo { set; get; }
        public string DepartmentAdditionalInfo { set; get; }

        public string ClientName { set; get; }
        public string PrimaryAddress { set; get; }

        
        public string FaxNumber { set; get; }
        public bool IsDepartmentRequired { set; get; }

        public ContactInformation ContactInformation { set; get; }
        public List<ClientContactInformation> ClientContactInformations { set; get; }

        public Department()
		{ }

		public Department(int clientID, string clientName)
		{
            this.ClientID = clientID;
            this.ClientName = clientName;
		}

        public Department(string departmentName,int departmentID)
        {
            this.DepartmentName = departmentName;
            this.DepartmentID = departmentID;
        }

        public Department(int clientID, int departmentID, string departmentName, int createdBy, DateTime createdDate, int modifiedBy, DateTime modifiedDate)
		{
            this.ClientID = clientID;
            this.DepartmentID = departmentID;
            this.DepartmentName = departmentName;
			this.CreatedBy = createdBy;
			this.CreatedDate = createdDate;
			this.ModifiedBy = modifiedBy;
			this.ModifiedDate = modifiedDate;
		}

        public string DepartmentContact { get; set; }

        public string ContactID { get; set; }

        public string DepartmentIDs { get; set; }


        public int ClientContactID { get; set; }

        public string DepartmentClientContactID { get; set; }

        public int NoOfContacts { set; get; }
        public int NoOfPositionProfile { set; get; }
        public string CreatedByName { set; get; }
        public string DepartmentIsSelected { get; set; }
        public int DepartmentContactID { get; set; }
        public List<Department> DepartmentList { set; get; }
    }
}
