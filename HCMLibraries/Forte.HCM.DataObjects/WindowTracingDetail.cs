﻿using System;

namespace Forte.HCM.DataObjects
{
    public class WindowTracingDetail
    {
        public string ProcessName { set; get; }
        public DateTime Time { set; get; }
        public string UserName { set; get; }
    }
}
