﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    public class PositionProfileActivityLog
    {
        public string ActivityType { get; set; }

        public int PositionProfileID { get; set; }

        public int ActivityBy { get; set; }

        public DateTime ActivityDateFrom { get; set; }

        public DateTime ActivityDateTo { get; set; }

        public string Comments { get; set; }

        public DateTime ActivityDate { get; set; }

        public string ActivityByName { get; set; }

        public string ActivityTypeName { get; set; }

        public string PositionProfileName { get; set; }
    }
}
