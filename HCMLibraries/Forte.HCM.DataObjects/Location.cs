﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Location.cs
// File that represents the class which contains candidate location details.

#endregion Header                                                              

using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains City, State, Country, and ZipCode members.
    /// </summary>
    [Serializable]
    public class Location
    {
        public string City {set; get;}
        public string State {set; get;}
        public string Country {set; get;}
        public string ZipCode { set; get; }
    }
}