﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class that holds the competency vector details
    /// </summary>
    public class CompetencyVectorDetails
    {
        public string VectorGroup { set; get; }
        public string VectorGroupID { set; get; }
        public string VectorName { set; get; }
        public string AliasName { set; get; }
        public int VectorID { set; get; }
    }
}
