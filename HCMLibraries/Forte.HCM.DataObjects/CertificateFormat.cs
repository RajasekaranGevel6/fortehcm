﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CertificateFormat.cs
// File that represents the enum which contains certificate format

#endregion Header
namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that represents the values for certificate format. It holds
    /// All, Standard, Classic, and Elegant styles.
    /// </summary> 
    public enum CertificateFormat
    {
        None = 0,
        Standard = 1,
        Classic = 2,
        Elegant = 3
    }
}
