﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class HCMDashboardPositionProfile
    {
        public int PositionProfileID { set; get; }
        public string PositionProfileName { set; get; }
        public string ClientName { set; get; }
        

    }

    [Serializable]
    public class HCMDashboardTalentScout
    {
        public string SearchTearms { set; get; }
        public string PositionProfileName { set; get; }
    }

    [Serializable]
    public class HCMDashboardTest
    {
        public string TestKey { set; get; }
        public string TestName { set; get; }
    }
    [Serializable]
    public class HCMDashboardTestSession
    {
        public string CandidateSessionKey { set; get; }
        public string CandidateName { set; get; }
        public string TestName { set; get; }
        public string SessionName { set; get; }
        public string TestSessionKey { set; get; }

        public string CandidateDetails { get; set; }
    }

    [Serializable]
    public class HCMDashboardTestReport
    {
        public string TestKey { set; get; }
        public string TestName { set; get; }
        public string CandidateName { set; get; }
        public string CandidateDetails { set; get; }
    }

    [Serializable]
    public class HCMDashboardResumeUpload
    {
        public string UserID { set; get; }
        public string CandidateID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        
    }

    [Serializable]
    public class HCMDashboard
    {
        public List<HCMDashboardPositionProfile> PositionProfile { set; get; }
        public List<HCMDashboardTalentScout> TalentScout { set; get; }
        public List<HCMDashboardTest> Test { set; get; }
        public List<HCMDashboardTestReport> TestReport { set; get; }
        public List<HCMDashboardResumeUpload> ResumeUpload { set; get; }
        public List<HCMDashboardTestSession> TestSession { set; get; }
        public List<HCMDashboardResumeUpload> ResumeDownload { set; get; }
        
    }
}
