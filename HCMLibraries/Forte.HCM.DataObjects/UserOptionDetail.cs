﻿namespace Forte.HCM.DataObjects
{
    public class UserOptionDetail
    {
        public int UserID { set; get; }
        public bool SendMailOnNotesAdded { set; get; }
    }
}
