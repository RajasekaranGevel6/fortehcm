﻿namespace Forte.HCM.DataObjects
{
    public class ApplicationDetail
    {
        public string CandidateSessionID { set; get; }
        public int AttempID { set; get; }
        public string Application { set; get; }
        public string ExePath { set; get; }
    }
}