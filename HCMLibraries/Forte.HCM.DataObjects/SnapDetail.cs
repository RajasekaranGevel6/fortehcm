﻿using System;

namespace Forte.HCM.DataObjects
{
    public class SnapDetail
    {
        public byte[] Image { set; get; }
        public DateTime Time { set; get; }
    }
}
