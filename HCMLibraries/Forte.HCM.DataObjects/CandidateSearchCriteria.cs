﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateSearchCriteria.cs
// File that represents the Properties for candidate search criteria.   

#endregion Header

using System.Collections.Generic;
namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// File that represents the Properties for candidate report detail.This class inherits candidate detail
    /// </summary>
    public class CandidateSearchCriteria : CandidateDetail
    {
        public string TestID { set; get; }
        public string Recruiter { set; get; }

        public List<CandidateInformation> candidateInfo { get; set; }
        public List<CandidateDetail> candidateDetail{ get; set; }
        public List<AttributeDetail> attributesList { get; set; }
    }
}