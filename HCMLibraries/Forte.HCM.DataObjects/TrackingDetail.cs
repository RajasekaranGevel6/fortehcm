﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class TrackingDetail
    {
        public DateTime CreatedDate { set; get; }
        public string CreatedDateFormatted
        {
            get
            {
                if (CreatedDate == DateTime.MinValue)
                    return string.Empty;

                return CreatedDate.ToString("MM/dd/yyyy");

            }
        }
        public int CreatedBy { set; get; }
        public DateTime ModifiedDate { set; get; }
        public int ModifiedBy { set; get; }
        public string ModifiedByName { set; get; }
        public bool IsDeleted { set; get; }
        public bool IsActive { set; get; }
        public RecordStatus RecordStatus { set; get; }


        /// <summary>
        /// Gets or sets the data access rights.
        /// 'E' Edit View and Edit
        /// 'V' VIEW cant Edit
        /// </summary>
        /// <value>
        /// The data access rights.
        /// </value>
        public string DataAccessRights { set; get; }
    }
}
