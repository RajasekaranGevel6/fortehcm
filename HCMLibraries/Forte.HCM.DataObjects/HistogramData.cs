﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// HistogramData.cs
// File that holds the properties for the histogram chart control

#endregion

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that defines the properties
    /// the HistogramData. 
    /// </summary>
    [Serializable]
    public class HistogramData : SingleChartData
    {
        /// <summary>
        /// Represents the score of the candidate
        /// </summary>
        public double CandidateScore
        {
            set;
            get;
        }
        /// <summary>
        /// Represents the 
        /// A<see cref="bool"/>that holds the bool value
        /// whether to  candidate display
        /// </summary>
        public bool IsDisplayCandidateScore
        {
            set;
            get;
        }

        /// <summary>
        /// Represents the 
        /// A<see cref="int"/>that holds the segment interval
        /// </summary>
        public int SegmentInterval
        {
            set;
            get;
        }

        public bool IsDisplayCandidateName
        {
            set;
            get;
        }
        public string CandidateName
        {
            set;
            get;
        }

        public int CandidateRelativeScore { set; get; }

    }
}
