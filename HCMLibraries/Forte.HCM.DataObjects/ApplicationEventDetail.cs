﻿namespace Forte.HCM.DataObjects
{
    public class ApplicationEventDetail
    {
        public string CandidateSessionID { set; get; }
        public int AttempID { set; get; }
        public string ApplicationName { set; get; }
        public string ApplicationPath { set; get; }
        public string EventStatus { set; get; }
        public string Date { set; get; }
    }
}
