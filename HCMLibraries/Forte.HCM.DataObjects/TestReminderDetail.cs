﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestReminderDetail.cs
// File that represents the Properties for HCM Test Reminder.

#endregion Header

#region Directives

using System;
using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    // TestReminderDetail.cs
    // Class that represents the Properties for HCM Test Reminder.
    /// </summary>
    [Serializable]
    public class TestReminderDetail
    {
        public string TestID { set; get; }
        public string TestName { set; get; }
        public string TestDescription { set; get; }
        public int AttemptID { set; get; }
        public DateTime ExpiryDate { set; get; }
        public string CandidateSessionID { set; get; }
        public DateTime ExpectedDate {set; get;}
        public string IntervalID { set; get; }
        public string IntervalDescription { set; get; }
        public DateTime ReminderDate { set; get; }
        public int ReminderTime { set; get; }
        public int UserID { set; get; }
        public int CandidateUserID { set; get; }
        public string CandidateName { set; get; }
        public string EmailID{ set; get; }
        public char ReminderMailStatus { set; get; }
        public List<string> AttributeList { set; get; }
        public int SchedulerID { set; get; }
        public string SchedulerName { set; get; }
        public List<ReminderIntervalDetail> Intervals { set; get; }
        
    }
}
