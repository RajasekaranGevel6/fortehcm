﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AutomatedTestSummaryGrid.cs
// File that represents the properties for automated test summary.

#endregion Header

#region Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents properties for automated test summary.
    /// </summary> 
    [Serializable]
    public class AutomatedTestSummaryGrid
    {
        public string CategoryName { set; get; }
        public string SubjectName { set; get; }
        public string TestAreaName { set; get; }
        public string Complexity { set; get; }
        public int NoofQuestionsInCategory { set; get; }
    }
}
