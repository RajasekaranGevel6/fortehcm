﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Category.cs
// File that represents the class which contains category information.

#endregion Header                                                              

#region Directives                                                             

using System;

#endregion Directives                                                          

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains category id, name, createdby, created date
    /// modifiedby, modified date members and constructor methods.
    /// </summary>
    [Serializable]
    public class Category : TrackingDetail
    {
        #region Public Properties                                              

        /// <summary>
        /// Property that will set/get the category id.
        /// </summary>
        public int CategoryID { set; get; }

        /// <summary>
        /// Property that will set/get a category name.
        /// </summary>
        public string CategoryName { set; get; }

        #endregion Public Properties                                           

        #region Public Methods                                                 

        /// <summary>
        /// Method which will call when an object is initialized.
        /// </summary>
        public Category(){}

        /// <summary>
        /// Overloaded constructor that assigns category name to the class member.
        /// </summary>
        /// <param name="categoryName">
        /// A <see cref="string"/> that contains the category name.
        /// </param>
        public Category(string categoryName)
        {
            this.CategoryName = categoryName;
        }

        /// <summary>
        /// Overloaded constructor that assigns category id and category name
        /// to its class members.
        /// </summary>
        /// <param name="categoryID">
        /// An <see cref="int"/> that holds the category id.
        /// </param>
        /// <param name="categoryName">
        /// A <see cref="string"/> that represents the category name.
        /// </param>
        public Category(int categoryID, string categoryName)
        {
            this.CategoryID = categoryID;
            this.CategoryName = categoryName;
        }

        /// <summary>
        /// An overloaded constructor which takes the following parameters.
        /// </summary>
        /// <param name="categoryID">
        /// An <see cref="int"/> that contains category id.
        /// </param>
        /// <param name="categoryName">
        /// A <see cref="string"/> that contains category name.
        /// </param>
        /// <param name="createdBy">
        /// An <see cref="int"/> that contains created by i.e.user id.
        /// </param>
        /// <param name="createdDate">
        /// A <see cref="DateTime"/> that represents the created date.
        /// </param>
        /// <param name="modifiedBy">
        /// An <see cref="int"/> that cotnains modified by i.e. user id.
        /// </param>
        /// <param name="modifiedDate">
        /// A <see cref="DateTime"/> that contains modified date.
        /// </param>
        public Category(int categoryID, string categoryName, int createdBy, 
            DateTime createdDate, int modifiedBy, DateTime modifiedDate)
        {
            this.CategoryID = categoryID;
            this.CategoryName = categoryName;
            this.CreatedBy = createdBy;
            this.CreatedDate = createdDate;
            this.ModifiedBy = modifiedBy;
            this.ModifiedDate = modifiedDate;
        }

        #endregion Public Methods                                              
    }
}
