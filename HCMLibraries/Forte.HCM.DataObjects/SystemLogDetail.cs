﻿using System;

namespace Forte.HCM.DataObjects
{
    public class SystemLogDetail
    {
        public int LogID { set; get; }
        public string Message { set; get; }
        public DateTime Date { set; get; }
        public string SystemName { get; set; }
        public string MacAddress { get; set; }
        public string IPAddress { get; set; }
        public string OSInfo { get; set; }
        public string CreatedDate { get; set; }
        public short MessageID { get; set; }
        public int LogFileID { set; get; }
        public bool IsWebcamImageAvailable { set; get; }
        public bool IsDesktopImageAvailable { set; get; }
    }
}
