﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Reference.cs
// File that represents the class which contains candidate reference details.

#endregion Header                                                              

using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains ReferenceId, Name, Organization, Relation    
    /// and ContactInformation members.
    /// </summary>
    [Serializable]
    public class Reference
    {
        public int ReferenceId { set; get; }
        public string Name {set; get;}
        public string Organization {set; get;}
        public string Relation {set; get;}
        public ContactInformation ContactInformation {set; get;}
        public string ContactInfo { set; get; }

    }
}
