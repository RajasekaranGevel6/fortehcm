﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class InterviewReminderDetail
    {
        public string InterviewID { set; get; }
        public string InterviewName { set; get; }
        public string InterviewDescription { set; get; }
        public int AttemptID { set; get; }
        public DateTime ExpiryDate { set; get; }
        public string CandidateSessionID { set; get; }
        public DateTime ExpectedDate { set; get; }
        public string IntervalID { set; get; }
        public string IntervalDescription { set; get; }
        public DateTime ReminderDate { set; get; }
        public int ReminderTime { set; get; }
        public int UserID { set; get; }
        public int CandidateUserID { set; get; }
        public string CandidateName { set; get; }
        public string EmailID { set; get; }
        public char ReminderMailStatus { set; get; }
        public int SchedulerID { set; get; }
        public string SchedulerName { set; get; }
        public List<string> AttributeList { set; get; }
        public List<ReminderIntervalDetail> Intervals { set; get; }
        public int CandidateInterviewID { set; get; }
        public string InterviewStartTime { set; get; }
    }
}
