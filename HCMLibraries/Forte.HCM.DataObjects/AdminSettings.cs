﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdminSettings.cs
// File that represents the properties for admin settings.

#endregion Header

#region Directives

using System.Collections.Generic;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the properties for admin settings.
    /// </summary>                                                              
    public class AdminSettings
    {
        public double ComplexitySimple { set; get; }
        public double ComplexityMedium { set; get; }
        public double ComplexityComplex { set; get; }
        public double ComputationFactor { set; get; }
        public int MaximumRecencyFactor { set; get; }
        public int GridPageSize { set; get; }
        public int NoOfSphere { set; get; }
        public List<AttributeDetail> ProximityFactor { set; get; }
    }
}
