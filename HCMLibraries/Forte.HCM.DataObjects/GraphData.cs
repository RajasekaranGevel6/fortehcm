﻿
namespace Forte.HCM.DataObjects
{
    public class GraphData
    {
        public string Month {set; get;}
        public int Value {set; get;}
    }
}
