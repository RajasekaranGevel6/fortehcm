﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdaptiveRecommendationType.cs
// File that represents the enum which contains adaptive recommendation types

#endregion Header

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that represents the values for adaptive recommendation type. It holds
    /// All, BasedOnProfile, BasedOnAssessmentHistory, and BasedOnTestTakenBySimilarCandidates statuses.
    /// </summary>                                                          
    public enum AdaptiveRecommendationType
    {
        All = 0,
        BasedOnProfile = 1,
        BasedOnAssessmentHistory = 2,
        BasedOnTestTakenBySimilarCandidates = 3
    }
}
