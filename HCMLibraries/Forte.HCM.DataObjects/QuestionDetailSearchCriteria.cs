﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class QuestionDetailSearchCriteria : QuestionDetail
    {
       // public SearchType SearchType { set; get; }
       // public List<TestArea> TestAreas { set; get; }
        //public List<Category> Categories { set; get; }
        //public string Category { set; get; }
        //public string Subject { set; get; }
       // public string Keyword { set; get; }
        
        public string CategorySubjectsKey { set; get; }
        public string TestAreas { set; get; }
        public string Complexities { set; get; }
        public string ActiveFlag { set; get; }
        public bool IsSimple { set; get; }
        public bool IsAdvanced { set; get; }
        public QuestionStatus QuestionStatus { set; get; }
        public int CurrentPage { set; get; }
        public string SortExpression { set; get; }
        public SortType SortDirection { set; get; }
        public bool IsMaximized { set; get; }
        public string ClientRequest { set; get; }

        public int TenantID { get; set; }
    }
}
