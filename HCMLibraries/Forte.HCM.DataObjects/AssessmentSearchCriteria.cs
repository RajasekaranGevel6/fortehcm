﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class AssessmentSearchCriteria
    {
        public string InterviewName { set; get; }

        public string InterviewDescription { set; get; }

        public string AssessmentStatus { set; get; }

        public string PositionProfileID { set; get; }

        public SortType SortDirection { set; get; }

        public string SortExpression { set; get; }

        public int AssessorID { set; get; }

        public string PositionProfileName { get; set; }

        public bool IsMaximized { get; set; }

        public int CurrentPage { get; set; }
    }
}
