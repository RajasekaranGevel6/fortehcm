﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class InterviewSessionDetail : TrackingDetail
    {
        public string InterviewTestID { set; get; }
        public string InterviewTestSessionID { set; get; }
        public string InterviewTestName { set; get; }
        public string InterviewTestDescription { set; get; }
        public int NumberOfQuestions { set; get; }
        public bool IsCertification { set; get; }
        public string Description { set; get; }
        public int NumberOfCandidateSessions { set; get; }
        public string ClientRequestID { set; get; }
        public DateTime ExpiryDate { set; get; }
        public string ExpiryDateFormatted
        {
            get
            {
                if (ExpiryDate == DateTime.MinValue)
                    return string.Empty;

                return ExpiryDate.ToString("MM/dd/yyyy");

            }
        }
        public int CandidateAlloted { set; get; }
        public string Instructions { set; get; }
        public string InterviewSessionDesc { set; get; }
        public string InteriewSessionAuthor { set; get; }
        public string InterviewSessionAuthorFullName { set; get; }
        public string InterviewSessionAuthorEmail { set; get; }
        public List<CandidateInterviewSessionDetail> CandidateInterviewSessions { set; get; }
        public string UserName { set; get; }
        public string Email { set; get; }
       // public string CandidateInterviewSessions { set; get; }
        public int PositionProfileID { set; get; }
        public string PositionProfileName { set; get; }

        public int AssessorID { get; set; }

        public string AssessorIDs { get; set; }
        public string AssessorEmails {get; set;}

        public string InterviewStatus { get; set; }
        public string ScheduledInterviews { get; set; }
        public string CompletedInterviews { get; set; }

        public char AllowPauseInterview { get; set; }
        public int InterviewSessionCount { set; get; }
        public int AssessedCandidates { set; get; }
    }
}
