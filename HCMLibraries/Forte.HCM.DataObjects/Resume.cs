﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Resume.cs
// File that represents the class which contains candidate resume details.

#endregion Header                                                              

using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains Name, ContactInformation, ExecutiveSummary    
    /// TechnicalSkills, Project, Education, Competencies, References 
    /// and Document members.
    /// </summary>
    public class Resume
    {
        public Name Name { set; get; }
        public ContactInformation ContactInformation { set; get; }
        public string ExecutiveSummary { set; get; }
        public TechnicalSkills TechnicalSkills { set; get; }
        public List<Project> Project { set; get; }
        public List<Education> Education { set; get; }
        public List<Competency> Competencies { set; get; }
        public List<Reference> References { set; get; }
        public string Document { set; get; }
    }
}
