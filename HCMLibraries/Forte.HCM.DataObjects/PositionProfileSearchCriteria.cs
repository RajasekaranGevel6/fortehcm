﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfileSearchCriteria
    {
        public PositionProfileDetailSearchCriteria clientPositionDetails { set; get; }
        public int CurrentPage { set; get; }
        public string SortExpression { set; get; }
        public SortType SortDirection { set; get; }
        public bool IsMaximized { set; get; }
        public SearchType SearchType { set; get; }
    }
}
