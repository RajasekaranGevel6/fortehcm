﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// RecordStatus.cs
// File that represents the enumeration values for record status. It holds
// new, modified and deleted statuses.

#endregion

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that represents the values for record status. It holds
    /// new, modified and deleted statuses. 
    /// </summary>
    public enum RecordStatus
    {
        /// <summary>
        /// None status.
        /// </summary>
        None = 0,

        /// <summary>
        /// New status. Indicates an insert operation.
        /// </summary>
        New = 1,

        /// <summary>
        /// Modified status. Indicates a update operation.
        /// </summary>
        Modified = 2,

        /// <summary>
        /// Deleted status. Indicates a delete operation.
        /// </summary>
        Deleted = 3
    }
}
