﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class OnlineInterviewSessionSearchCriteria
    {
        public string InterviewName { set; get; }
        public string Interviewkey { set; get; }
        public string InterviewCreator { set; get; }
        public int InterviewCreatorID { set; get; }
        public string SessionName { set; get; }
        public string SessionKey { set; get; }
        public string SchedulerName { set; get; }
        public string CandidateSessionID { set; get; }
        public string CandidateName { set; get; }
        public int? SessionCreatorID { set; get; }
        public string OnlineInterviewSessionCreator { set; get; }
        public int ?SchedulerNameID { set; get; }
        public int? OnlineInterSessionCreatorID { set; get; }
        public int PositionProfileID { set; get; }
        public string PositionProfileName { set; get; }
        public string AssessorFirstName { get; set; }
        public string AssessorLastName { get; set; }
        public string Skill { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int AssessorID { get; set; }
        public string Email { get; set; }
        public string CreatedDate { get; set; }
        public int UserID { get; set; }
        public string AlternateEmailID { get; set; }
        public string IsActive { get; set; }
        public int SearchTenantID { get; set; }
        public bool IsMaximized { set; get; }
        public int CurrentPage { set; get; }
        public string SortExpression { set; get; }
        public SortType SortDirection { set; get; }
    }
}
