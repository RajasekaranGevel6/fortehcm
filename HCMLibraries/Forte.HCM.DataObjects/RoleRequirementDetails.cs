﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// RoleRequirementDetails.cs
// File that represents details for the technical skills requirements

#endregion Header

#region Directives
using System;
#endregion Directives

namespace Forte.HCM.DataObjects
{
    [Serializable]
    /// <summary>
    /// Represents the class to holds the role requirement details
    /// </summary>
    public class RoleRequirementDetails
    {
        public string PrimaryRole { set; get; }

        public string PrimaryRoleExperience { set; get; }

        public string SecondaryRole { set; get; }

        public string SecondaryRoleExperience { set; get; }

        public string AdditionalRole { set; get; }

        public string IsAdditionalRole { set; get; }
    }
}
