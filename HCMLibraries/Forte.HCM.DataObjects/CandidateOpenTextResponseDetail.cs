﻿using System;
using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class CandidateOpenTextResponseDetail : TrackingDetail
    {
        public int QuestionID { set; get; }
        public int QuestionSNo { set; get; }
        public string QuestionKey { set; get; }
        public string QuestionDescription { set; get; }
        public int MaxLength { get; set; }
        public int Marks { get; set; }
        public string AnswerReference { set; get; }
        public string Answer { set; get; }
        public int MarksObtained { set; get; }
        public string Status { set; get; }
    }
}
