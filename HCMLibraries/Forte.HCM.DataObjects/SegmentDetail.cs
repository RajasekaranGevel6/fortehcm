﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ReflectionComboItem;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class SegmentDetail : TrackingDetail
    {
        public int SegmentID { set; get; }
        public SegmentType SegmentType { set; get; }
         [DropDownItemAttribute("Data Souce", "")]
        public string DataSource { set; get; }
        [DropDownItemAttribute("Display Order", "")]
        public int DisplayOrder { set; get; }
        public int PositionProfileID { set; get; }
        public string SegmentName { set; get; }
        public string segmentPath { set; get; }
        public string SegmentIDAndPath
        {
            get
            {
                return SegmentID.ToString() + ',' + segmentPath;
            }
        }

    }
}
