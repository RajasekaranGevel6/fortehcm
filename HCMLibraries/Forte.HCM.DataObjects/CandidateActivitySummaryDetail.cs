﻿using System.Collections.Generic;

namespace Forte.HCM.DataObjects
{
    public class CandidateActivitySummaryDetail
    {
        public string PositionProfileName { set; get; }
        public string CandidateName { set; get; }

        public List<CandidateActivityLog> Activities { set; get; }
    }
}
