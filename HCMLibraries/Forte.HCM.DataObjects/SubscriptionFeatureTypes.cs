﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SubscriptionFeatureTypes.cs
// File that represents the properties about the Subscription feature type details.

#endregion Header

#region Namespace                                                              

using System;

#endregion Namespace

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represent the property list for the SUBSCRIPTION_FEATURE_TYPES.
    /// </summary>
    [Serializable]
    public class SubscriptionFeatureTypes
    {

        #region Public Properties                                              
        /// <summary>
        ///  A <see cref="System.Int32"/> that holds the Id of the subscription feature
        /// </summary>
        public int FeatureId { set; get; }

        /// <summary>
        ///  A <see cref="System.String"/> that holds the name of the subscription feature
        /// </summary>
        public string FeatureName { set; get; }

        /// <summary>
        ///  A <see cref="System.String"/> that holds the unit of the subscription feature
        /// </summary>
        public string Unit { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the unit name of the subscription feature
        /// </summary>
        public string Unitname { set; get; }

        /// <summary>
        ///  A <see cref="System.String"/> that holds the default value of the subscription feature
        /// </summary>
        public string DefaultValue { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the created user id of the Subscription feature
        /// </summary>
        public int CreatedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the created date of the Subscription feature
        /// </summary>
        public DateTime CreatedDate { set; get; }

        /// <summary>
        /// A <see cref="System.Int32"/> that holds the modified user id of the Subscription feature
        /// </summary>
        public int ModifiedBy { set; get; }

        /// <summary>
        /// A <see cref="System.DateTime"/> that holds the modified date of the Subscription feature
        /// </summary>
        public DateTime ModifiedDate { set; get; }

        /// <summary>
        /// A <see cref="System.Boolean"/> that holds the whether the subscription is deleted or not
        /// </summary>
        public bool IsDeleted { set; get; }

        /// <summary>
        /// A <see cref="System.String"/> that holds the deleted user id of the subscription feature
        /// </summary>
        public int DeletedBy { set; get; }

        /// <summary>
        /// A <see cref="System.Int16"/> that holds the isunlimited value fro subscription feature type
        /// </summary>
        public Int16 IsUnlimited { set; get; }

        #endregion Public Properties

    }
}
