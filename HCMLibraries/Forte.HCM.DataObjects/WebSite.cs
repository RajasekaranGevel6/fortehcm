﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// WebSite.cs
// File that represents the class which contains candidate website details.

#endregion Header                                                              

using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains Personal, LinkedIn and FaceBook members.
    /// </summary>
    [Serializable]
    public class WebSite
    {
        public string Personal {set; get;}
        public string LinkedIn {set; get;}
        public string FaceBook {set; get;}
    }
}
