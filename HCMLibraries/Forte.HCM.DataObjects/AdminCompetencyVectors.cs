﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdminCompetencyVectors.cs
// File that represents the properties for the admin competency vectors details.

#endregion Header

#region Directives

using System;

#endregion Directives
                                                         
namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the property list for the admin competency vectors.
    /// details.
    /// </summary>    
    [Serializable]
    public class AdminCompetencyVectors
    { 
        public int GroupID { set; get; }
        public string GroupName { set; get; }
        public int VectorID { set; get; }
        public string VectorName { set; get; }
        public int ParameterID { set; get; }
        public string ParameterName { set; get; }
        public int UserID { set; get; }
        public string VectorType { set; get; }
        public string CompetencyValue { set; get; }
   
        
    }
}
