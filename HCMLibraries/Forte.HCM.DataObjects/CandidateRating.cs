﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateRating.cs
// File that represents the class which contains candidate rating details.

#endregion Header                                                              

#region Directives                                                             

using System;

#endregion Directives                                                          

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains candidate rating details
    /// </summary>
    [Serializable]
    public class CandidateRatingDetail
    {
        public string CandidateTestSessionID {set; get;}
        public int Rating { set; get; }
        public string Feedback { set; get; }
        public int ModifiedBy { set; get; }
    }
}