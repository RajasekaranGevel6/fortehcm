﻿namespace Forte.HCM.DataObjects
{
    public enum InterviewType
    {
        Offline = 0,
        Online = 1
    }
}
