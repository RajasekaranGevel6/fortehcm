﻿using System;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class CreditRequestDetail:TrackingDetail
    {
        public string CandidateFirstName { set; get; }
        public string CandidateFullName { set; get; }
        public string CandidateEMail { set; get; }

        public DateTime RequestDate { set; get; }

        public bool IsProcessed { set; get; }

        public DateTime ProcessedDate { set; get; }

        public bool IsApproved { set; get; }

        public decimal Amount { set; get; }

        public int CandidateId { set; get; }

        public string Remarks { set; get; }

        public int GenId { set; get; }

        public CreditType CreditType { set; get; }

        public string CreditRequestID { set; get; }

        public string CreditRequestDescription { set; get; }

        public decimal CreditValue { set; get; }

        public string EntityID { set; get; }
    }
}
