﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestDetail.cs
// File that represents properties for candidate test detail.

#endregion Header

#region Directives

using System;

#endregion Directives

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that represents the Properties for candidate test detail.
    /// </summary>    
    [Serializable]
    public class InterviewScoreParamDetail
    {
        public string InterviewKey { set; get; }
        public string CandidateInterviewSessionKey { set; get; }
        public int AssessorID { set; get; }
        public int AttemptID { set; get; }
        public bool IsCodeExist { set; get; }
        public string ScoreCode { set; get; }
        public string CandidateName { set; get; }

    }
}
