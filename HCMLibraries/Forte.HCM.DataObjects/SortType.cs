﻿using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Enumeration that holds the sort types.
    /// </summary>
    [Serializable]
    public enum SortType
    {
        /// <summary>
        /// Ascending order.
        /// </summary>
        Ascending = 0,

        /// <summary>
        /// Descending order.
        /// </summary>
        Descending = 1
    }
}
