﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forte.HCM.DataObjects
{
    [Serializable]
    public class PositionProfileFormSegments
    {
        public int formID { set; get; }
        public string formName { set; get; }
        public int displayOrder { set; get; }
        public int segmentId { set; get; }
        public string segmentName { set; get; }
        public string segmentPath { set; get; }
    }
}
