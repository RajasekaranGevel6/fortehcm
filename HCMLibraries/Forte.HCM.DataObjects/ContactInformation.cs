﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ContactInformation.cs
// File that represents the class which contains candidate contact information
// details.

#endregion Header                                                              

using System;

namespace Forte.HCM.DataObjects
{
    /// <summary>
    /// Class that contains StreetAddress, City, Country, PostalCode, 
    /// State, PhoneNumber, EmailAddress and  WebSiteAddress members.
    /// </summary>
    [Serializable]
    public class ContactInformation
    {
        public string StreetAddress {set; get;}
        public string City {set; get;}
        public string Country {set; get;}
        public string PostalCode {set; get;}
        public string State { set; get; }
        public PhoneNumber Phone;
        public string EmailAddress {set; get;}
        public WebSite WebSiteAddress {set; get;}

        public string AdditionalInfo { get; set; }

        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
    }
}
