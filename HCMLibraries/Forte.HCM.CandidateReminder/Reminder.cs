﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Reminder.cs
// File that represents the user interface layout and functionalities
// for the Reminder form. This form helps in sending the daily reminders
// to candidates on pending test and interview alerts.

#endregion Header

#region Directives

using System;
using System.Configuration;
using System.Windows.Forms;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.CandidateReminder
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the Reminder form. This form helps in sending the daily reminders
    /// to candidates on pending test and interview alerts.
    /// </summary>
    /// <remarks>
    /// This form will not be shown to the users. This executable is assigned
    /// to a scheduler and invokes at specicied time.
    /// </remarks>
    public partial class Reminder : Form
    {
        /// <summary>
        /// Method that represents the constructor.
        /// </summary>
        public Reminder()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Method that is called when the form is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        private void Reminder_Load(object sender, EventArgs e)
        {
            try
            {
                // Hide the window.
                this.Visible = false;

                Logger.TraceLog("Started candidate reminder @" + DateTime.Now);

                // Create an email handler object.
                EmailHandler emailHandler = new EmailHandler();

                // Create a BL manager object.
                CandidateBLManager blManager = new CandidateBLManager();

                #region Send Test Reminders                                    

                // Retrieve the list of test reminders.
                List<CandidateTestSessionDetail> testReminders = blManager.GetDailyTestReminders();

                if (testReminders == null || testReminders.Count == 0)
                {
                    Logger.TraceLog("No test reminders found");
                }
                else
                {
                    Logger.TraceLog(string.Format("{0} test reminders found", testReminders.Count));

                    // Loop through the list and send the reminder.
                    foreach (CandidateTestSessionDetail testReminder in testReminders)
                    {
                        try
                        {
                            emailHandler.SendMail(EntityType.DailyTestReminder, testReminder);

                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                        }
                    }
                }

                #endregion Send Test Reminders

                #region Send Interview Reminders                               

                // Retrieve the list of interview reminders.
                List<CandidateInterviewSessionDetail> interviewReminders = blManager.GetDailyInterviewReminders();

                if (interviewReminders == null || interviewReminders.Count == 0)
                {
                    Logger.TraceLog("No interview reminders found");
                }
                else
                {
                    Logger.TraceLog(string.Format("{0} interview reminders found", testReminders.Count));

                    // Loop through the list and send the reminder.
                    foreach (CandidateInterviewSessionDetail interviewReminder in interviewReminders)
                    {
                        try
                        {
                            emailHandler.SendMail(EntityType.DailyInterviewReminder, interviewReminder);

                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                        }
                    }
                }

                #endregion Send Interview Reminders

                Logger.TraceLog("Stopped candidate reminder @" + DateTime.Now);

                // Close the application.
                this.Close();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                Logger.TraceLog("Stopped candidate reminder @" + DateTime.Now);

                // Close the application.
                this.Close();
            }
        }
    }
}
