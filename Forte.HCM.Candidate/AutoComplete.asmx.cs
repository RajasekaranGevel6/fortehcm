﻿using System;
using System.Web.Services;

using Forte.HCM.BL;
using Forte.HCM.Trace;

namespace Forte.HCM.Candidate
{
    /// <summary>
    /// Summary description for AutoComplete
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AutoComplete : System.Web.Services.WebService
    {

        /// <summary>
        /// Gets the skill category keywords.
        /// </summary>
        /// <param name="prefixText">The prefix text.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        [WebMethod(Description = "Service method to get the skill Keyword(s) for auto complete recommendation")]
        public string[] GetSkillKeywords(string prefixText, int count)
        {
            try
            {
                return new CandidateBLManager().GetCandidateSearchSkill(prefixText); 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw;
            }
        }
    }
}
