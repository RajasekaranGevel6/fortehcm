<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HCMCandidateMaster.Master"
    CodeBehind="MyInterviews.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.MyInterviews" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/HCMCandidateMaster.Master" %>
<asp:Content ID="MyInterviews_bodyContent" runat="server" ContentPlaceHolderID="HCMCandidateMaster_contentPlaceHolder">

    <script type="text/javascript">
        function ShowRetakeWarningMessage() {
            //displays email confirmation pop up extender
            $find("<%= MyInterviews_RetakeValidation_ModalPopupExtender.ClientID %>").show();
            return false;
        }        
    </script>

    <script type="text/javascript" language="javascript">

        // Method that helps to expand or shrink the specific section such
        // as pending, completed and expired.
        function MultipleExpandorCompress(ctrlExpandTR, ctrlExpandDiv, expandImage,
            compressImage, resultDiv) {
            var pendingInterviewsTR = "<%= MyInterviews_pendingInterviewsTR.ClientID %>";
            var completedInterviewsTR = "<%= MyInterviews_completedInterviewsTR.ClientID %>";
            var expiredInterviewsTR = "<%= MyInterviews_expiredInterviewsTR.ClientID %>";
            if (document.getElementById(ctrlExpandTR) != null) {
                document.getElementById(expandImage).style.display = "none";
                document.getElementById(compressImage).style.display = "none";
                document.getElementById(resultDiv).style.display = "block";
                if (document.getElementById(ctrlExpandDiv).style.height.toString() == "150px") {
                    document.getElementById(pendingInterviewsTR).style.display = "none";
                    document.getElementById(completedInterviewsTR).style.display = "none";
                    document.getElementById(expiredInterviewsTR).style.display = "none";
                    document.getElementById(ctrlExpandTR).style.display = "";

                    document.getElementById(ctrlExpandDiv).style.height = "100%";
                    document.getElementById(expandImage).style.display = "block";
                }
                else {
                    document.getElementById(pendingInterviewsTR).style.display = "";
                    document.getElementById(completedInterviewsTR).style.display = "";
                    document.getElementById(expiredInterviewsTR).style.display = "";
                    document.getElementById(ctrlExpandDiv).style.height = "150px";
                    document.getElementById(compressImage).style.display = "block";
                }
            }
            return false;
        }

        // Method that will show the start interview confirmation window.
        function StartInterview(candidateSessionID, attemptID) 
        {
            document.getElementById("<%=MyInterviews_candidateSessionID.ClientID%>").value = candidateSessionID;
            document.getElementById("<%=MyInterviews_attemptID.ClientID%>").value = attemptID;

            $find("<%= MyInterviews_StartInterviewPopupExtender.ClientID  %>").show();
            return false;
        }

        // Method that will show the continue interview confirmation window.
        function ContinueInterview(candidateSessionID, attemptID) 
        {
            document.getElementById("<%=MyInterviews_candidateSessionID.ClientID%>").value = candidateSessionID;
            document.getElementById("<%=MyInterviews_attemptID.ClientID%>").value = attemptID;

            $find("<%= MyInterviews_ContinueInterviewPopupExtender.ClientID  %>").show();
            return false;
        }
    </script>

    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="MyInterviews_headerLiteral" runat="server" Text="My Interviews"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="3" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="MyInterviews_topRefreshButton" runat="server" Text="Refresh" SkinID="sknButtonId"
                                            OnClick="MyInterviews_refreshButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="MyInterviews_topCancelButton" runat="server" Text="Cancel" PostBackUrl="~/Dashboard.aspx"
                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyInterviews_topMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="MyInterviews_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="MyInterviews_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_pendingInterviewsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_pendingInterviewsGridView" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_completedInterviewsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_completedInterviewsGridView" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_expiredInterviewsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_expiredInterviewsGridView" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr runat="server">
            <td>
                <div id="MyInterviews_pendingInterviewsTR" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="header_bg" align="center">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="width: 93%" align="left" class="header_text_bold">
                                            <asp:Literal ID="MyInterviews_pendingInterviewsLiteral" runat="server" Text="Pending Interviews"></asp:Literal>
                                        </td>
                                        <td style="width: 2%" align="right">
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td style="width: 2%" align="right" id="MyInterviews_pendingInterviewsHeaderTR" runat="server">
                                            <span id="MyInterviews_pendingInterviewsUpSpan" runat="server" style="display: none;">
                                                <asp:Image ID="MyInterviews_pendingInterviewsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                    id="MyInterviews_pendingInterviewsDownSpan" runat="server" style="display: block;"><asp:Image ID="MyInterviews_pendingInterviewsDownImage"
                                                        runat="server" SkinID="sknMaximizeImage" /></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;" class="grid_body_bg">
                                <asp:UpdatePanel ID="MyInterviews_pendingInterviewsUpdatePanel" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="display: block;" runat="server" id="MyInterviews_pendingInterviewsPlusDiv">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="right">
                                                        <uc3:PageNavigator ID="MyInterviews_pendingInterviewsTopPagingNavigator" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left">
                                                                    <div style="height: 150px; overflow: auto; display: block;" runat="server" id="MyInterviews_pendingInterviewsDiv">
                                                                        <asp:GridView ID="MyInterviews_pendingInterviewsGridView" runat="server" AllowSorting="True"
                                                                            AutoGenerateColumns="False" Width="100%" OnSorting="MyInterviews_pendingInterviewsGridView_Sorting"
                                                                            OnRowCreated="MyInterviews_pendingInterviewsGridView_RowCreated" OnRowDataBound="MyInterviews_pendingInterviewsGridView_RowDataBound"
                                                                            OnRowCommand="MyInterviews_pendingInterviewsGridView_RowCommand">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="MyInterviews_pendingInterviewsViewImageButton" runat="server" SkinID="sknTestIntroImageButton"
                                                                                            ToolTip="Interview Introduction" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="InterviewIntroduction" CssClass="showCursor" />
                                                                                        <asp:ImageButton ID="MyInterviews_pendingInterviewsStartImageButton" runat="server" SkinID="sknStartTestImageButton"
                                                                                            ToolTip="Start Interview" CssClass="showCursor" Visible='<%# !IsPaused(Eval("IsPaused").ToString())%>'/>
                                                                                        <asp:ImageButton ID="MyInterviews_pendingInterviewsContinueImageButton" runat="server" SkinID="sknContinueTestImageButton"
                                                                                            ToolTip="Continue Interview" CssClass="showCursor" Visible='<%# IsPaused(Eval("IsPaused").ToString())%>'/>
                                                                                        <asp:ImageButton ID="MyInterviews_pendingInterviewsReminderImageButton" runat="server" SkinID="sknReminderImageButton"
                                                                                            ToolTip="Interview Reminder" CssClass="showCursor" />
                                                                                        <asp:HiddenField ID="MyInterviews_pendingInterviewsCandidateSessionIDHiddenField" Value='<%# Eval("CandidateSessionID") %>'
                                                                                            runat="server" />
                                                                                        <asp:HiddenField ID="MyInterviews_pendingInterviewsAttemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                                                            runat="server" />
                                                                                        <asp:HiddenField ID="MyInterviews_pendingInterviewIsPausedHiddenField" Value='<%# Eval("IsPaused") %>'
                                                                                            runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField HeaderText="Interview&nbsp;Name" DataField="InterviewName" SortExpression="INTERVIEW_TEST_NAME" />
                                                                                <asp:BoundField HeaderText="Interview&nbsp;ID" DataField="InterviewID" SortExpression="INTERVIEW_TEST_KEY" />
                                                                                <asp:TemplateField HeaderText="Initiated&nbsp;By" SortExpression="INITIATEDBY">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyInterviews_pendingInterviewsInitiatedByFullNameLabel" runat="server" ToolTip='<%# Eval("InterviewAuthorFullName") %>'
                                                                                            Text='<%# Eval("InitiatedBy") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Initiated&nbsp;On" SortExpression="INITIATEDON">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyInterviews_pendingInterviewsInitiatedOnLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewDetail)Container.DataItem).InitiatedOn) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Expiry Date" SortExpression="EXPIRYDATE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyInterviews_pendingInterviewsExpiryDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewDetail)Container.DataItem).ExpiryDate) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr id="MyInterviews_completedInterviewsTR" runat="server" visible="false">
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 93%" align="left" class="header_text_bold">
                                        <asp:Literal ID="MyInterviews_completedInterviewsLiteral" runat="server" Text="Completed Interviews"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">
                                    </td>
                                    <td style="width: 1%">
                                    </td>
                                    <td style="width: 2%" align="right" id="MyInterviews_completedInterviewsHeaderTR" runat="server">
                                        <span id="MyInterviews_completedInterviewsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="MyInterviews_completedInterviewsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                id="MyInterviews_completedInterviewsDownSpan" runat="server"><asp:Image ID="MyInterviews_completedInterviewsDownImage"
                                                    runat="server" SkinID="sknMaximizeImage" /></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;" class="grid_body_bg">
                            <asp:UpdatePanel ID="MyInterviews_completedInterviewsUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="display: block;" runat="server" id="MyInterviews_completedInterviewsPlusDiv">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="right">
                                                    <uc3:PageNavigator ID="MyInterviews_completedInterviewsTopPagingNavigator" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left">
                                                                <div style="height: 150px; overflow: auto; display: block;" runat="server" id="MyInterviews_completedInterviewsDiv">
                                                                    <asp:GridView ID="MyInterviews_completedInterviewsGridView" runat="server" AllowSorting="true"
                                                                        AutoGenerateColumns="false" Width="100%" OnSorting="MyInterviews_completedInterviewsGridView_Sorting"
                                                                        OnRowCreated="MyInterviews_completedInterviewsGridView_RowCreated" OnRowDataBound="MyInterviews_completedInterviewsGridView_RowDataBound"
                                                                        OnRowCommand="MyInterviews_completedInterviewsGridView_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="MyInterviews_completedInterviewsActivateImageButton" runat="server" SkinID="sknActivationRequestImageButton"
                                                                                        ToolTip="Request to Retake" CommandName="RequestToRetake" CssClass="showCursor" />
                                                                                    <asp:ImageButton ID="MyInterviews_InterviewsResultsImageButton" runat="server" SkinID="sknTestResultImageButton"
                                                                                        ToolTip="Interview Results" CommandArgument="<%# Container.DataItemIndex %>" CommandName="TestResults"
                                                                                        Visible='<%# IsShowResults(Eval("ShowResults").ToString())%>' CssClass="showCursor" />
                                                                                    <asp:ImageButton ID="MyInterviews_certificationImageButton" runat="server" SkinID="sknViewCertificateImageButton"
                                                                                        ToolTip="View Certificate" CommandArgument='<%# Container.DataItemIndex %>' CommandName="viewcertificate"
                                                                                        Visible='<%# IsQualified(Eval("CertificationStatus").ToString())%>' CssClass="showCursor" />
                                                                                    <asp:HiddenField ID="MyInterviews_completedInterviewsCandidateSessionIDHiddenField" Value='<%# Eval("CandidateSessionID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_completedInterviewsAttemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_completedInterviewsTestIDHiddenField" Value='<%# Eval("TestID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_completedInterviewsTestNameHiddenField" Value='<%# Eval("TestName") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_initiatedByHiddenField" Value='<%# Eval("InitiatedBy") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_completedInterviewsCompletedOnHiddenField" Value='<%# Eval("CompletedOn") %>'
                                                                                        runat="server" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Interview&nbsp;Name" DataField="TestName" SortExpression="TESTNAME" />
                                                                            <asp:BoundField HeaderText="Interview&nbsp;ID" DataField="TestID" SortExpression="TESTID" />
                                                                            <%--<asp:BoundField HeaderText="Initiated&nbsp;By" DataField="InitiatedBy" SortExpression="INITIATEDBY" />--%>
                                                                            <asp:TemplateField HeaderText="Initiated&nbsp;By" SortExpression="INITIATEDBY">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyInterviews_completedInterviewsInitiatedByFullNameLabel" runat="server" ToolTip='<%# Eval("TestAuthorFullName") %>'
                                                                                        Text='<%# Eval("InitiatedBy") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Initiated&nbsp;On" SortExpression="INITIATEDON">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyInterviews_completedInterviewsInitiatedOnLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewDetail)Container.DataItem).InitiatedOn) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Completed&nbsp;On" SortExpression="COMPLETEDON">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyInterviews_completedInterviewsCompletedOnLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewDetail)Container.DataItem).CompletedOn) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Certification" DataField="CertificationStatus" SortExpression="CERTIFICATIONSTATUS" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="MyInterviews_expiredInterviewsTR" runat="server" visible="false">
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 93%" align="left" class="header_text_bold">
                                        <asp:Literal ID="MyInterviews_expiredInterviewsLiteral" runat="server" Text="Expired Interviews"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">
                                    </td>
                                    <td style="width: 1%">
                                    </td>
                                    <td style="width: 2%" align="right" id="MyInterviews_expiredInterviewsHeaderTR" runat="server">
                                        <span id="MyInterviews_expiredInterviewsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="MyInterviews_expiredInterviewsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                id="MyInterviews_expiredInterviewsDownSpan" runat="server" style="display: block;"><asp:Image ID="MyInterviews_expiredInterviewsDownImage"
                                                    runat="server" SkinID="sknMaximizeImage" /></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;" class="grid_body_bg">
                            <asp:UpdatePanel ID="ExpiredTest_updatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="display: block;" runat="server" id="MyInterviews_expiredInterviewsPlusDiv">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="right">
                                                    <uc3:PageNavigator ID="MyInterviews_expiredInterviewsTopPagingNavigator" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left">
                                                                <div style="height: 150px; overflow: auto; display: block;" runat="server" id="MyInterviews_expiredInterviewsDiv">
                                                                    <asp:GridView ID="MyInterviews_expiredInterviewsGridView" runat="server" AllowSorting="true"
                                                                        AutoGenerateColumns="false" Width="100%" OnSorting="MyInterviews_expiredInterviewsGridView_Sorting"
                                                                        OnRowDataBound="MyInterviews_expiredInterviewsGridView_RowDataBound" OnRowCreated="MyInterviews_expiredInterviewsGridView_RowCreated">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="MyInterviews_expiredInterviewsActivateImageButton" runat="server" SkinID="sknActivationRequestImageButton"
                                                                                        ToolTip="Request to Activate" CssClass="showCursor" />
                                                                                    <asp:HiddenField ID="MyInterviews_expiredInterviewsCandidateSessionIDHiddenField" Value='<%# Eval("CandidateSessionID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_expiredInterviewsAttemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_expiredInterviewsTestIDHiddenField" Value='<%# Eval("TestID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_expiredInterviewsTestNameHiddenField" Value='<%# Eval("TestName") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyInterviews_expiredInterviewsInitiatedBydHiddenField" Value='<%# Eval("InitiatedBy") %>'
                                                                                        runat="server" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Interview&nbsp;Name" DataField="TestName" SortExpression="TESTNAME" />
                                                                            <asp:BoundField HeaderText="Interview&nbsp;ID" DataField="TestID" SortExpression="TESTID" />
                                                                            <%--<asp:BoundField HeaderText="Initiated&nbsp;By" DataField="InitiatedBy" SortExpression="INITIATEDBY" />--%>
                                                                            <asp:TemplateField HeaderText="Initiated&nbsp;By" SortExpression="INITIATEDBY">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyInterviews_expiredInterviewsInitiatedByFullNameLabel" runat="server" ToolTip='<%# Eval("TestAuthorFullName") %>'
                                                                                        Text='<%# Eval("InitiatedBy") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Initiated&nbsp;On" SortExpression="INITIATEDON">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyInterviews_expiredInterviewsInitiatedOnLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewDetail)Container.DataItem).InitiatedOn) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Expiry&nbsp;Date" SortExpression="EXPIRYDATE">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyInterviews_expiredInterviewsExpiryDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateInterviewDetail)Container.DataItem).ExpiryDate) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyInterviews_bottomMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="MyInterviews_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="MyInterviews_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_pendingInterviewsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_pendingInterviewsGridView" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_completedInterviewsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_completedInterviewsGridView" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_expiredInterviewsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyInterviews_expiredInterviewsGridView" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="right" class="header_bg">
                <table border="0" cellpadding="0" cellspacing="3" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="MyInterviews_bottomRefreshButton" runat="server" Text="Refresh" SkinID="sknButtonId"
                                OnClick="MyInterviews_refreshButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="MyInterviews_bottomCancelButton" runat="server" Text="Cancel" PostBackUrl="~/Dashboard.aspx"
                                SkinID="sknActionLinkButton"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="MyInterviews_StartInterviewPopupPanel" runat="server" Style="display: none;
                    height: 206px" CssClass="popupcontrol_confirm">
                    <div id="MyInterviews_startInterviewHiddenDIV" style="display: none">
                        <asp:Button ID="MyInterviews_startInterviewHiddenButton" runat="server" />
                    </div>
                    <uc1:ConfirmMsgControl ID="MyInterviews_StartInterviewPopupExtenderControl" runat="server"
                        OnOkClick="MyInterviews_startInterviewButton_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="MyInterviews_StartInterviewPopupExtender" runat="server"
                    PopupControlID="MyInterviews_StartInterviewPopupPanel" TargetControlID="MyInterviews_startInterviewHiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>

                <asp:Panel ID="MyInterviews_ContinueInterviewPopupPanel" runat="server" Style="display: none;
                    height: 206px" CssClass="popupcontrol_confirm">
                    <div id="MyInterviews_continueInterviewHiddenDIV" style="display: none">
                        <asp:Button ID="MyInterviews_continueInterviewHiddenButton" runat="server" />
                    </div>
                    <uc1:ConfirmMsgControl ID="MyInterviews_ContinueInterviewPopupExtenderControl" runat="server"
                        OnOkClick="MyInterviews_continueInterviewButton_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="MyInterviews_ContinueInterviewPopupExtender" runat="server"
                    PopupControlID="MyInterviews_ContinueInterviewPopupPanel" TargetControlID="MyInterviews_continueInterviewHiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>

                <asp:UpdatePanel ID="MyInterviews_RetakeValidationUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="MyInterviews_RetakeValidationPopupPanel" runat="server" Style="display: none;
                            height: 202px;" CssClass="popupcontrol_confirm">
                            <div id="MyInterviews_RetakeValidationDiv" style="display: none">
                                <asp:Button ID="MyInterviews_RetakeValidation_hiddenButton" runat="server" />
                            </div>
                            <uc1:ConfirmMsgControl ID="MyInterviews_RetakeValidation_ConfirmMsgControl" runat="server"
                                Title="Request to Retake" Type="OkSmall" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="MyInterviews_RetakeValidation_ModalPopupExtender"
                            runat="server" PopupControlID="MyInterviews_RetakeValidationPopupPanel" TargetControlID="MyInterviews_RetakeValidation_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <asp:HiddenField ID="MyInterviews_candidateSessionID" runat="server" />
            <asp:HiddenField ID="MyInterviews_attemptID" runat="server" />
        </tr>
    </table>
</asp:Content>
