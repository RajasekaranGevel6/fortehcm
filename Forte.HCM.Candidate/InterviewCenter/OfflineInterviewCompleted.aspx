﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="OfflineInterviewCompleted.aspx.cs" Inherits="Forte.HCM.Candidate.InterviewCenter.OfflineInterviewCompleted" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 980px; float: left; padding-left: 10px">
                    <div style="float: left; width: 970px">
                        <%--<asp:Label ID="InterviewConductionCompleted_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>--%>
                    </div>
                    <div style="float: left; width: 970px;text-align:left">
                        <div style="width:290px;float: left"><br />
                            <asp:Label runat="server" ID="InterviewConductionCompleted_statusLabel" SkinID="sknLabelInterviewCompletionTitle"
                                Text="Offline Interview Status :" Font-Names="Tahoma" Font-Size="18pt"></asp:Label>
                        </div>
                        <div style="float: left"><br />
                            <asp:Label runat="server" ID="InterviewConductionCompleted_statusMessageLabel" 
                                SkinID="sknLabelInterviewCompletion" Font-Names="Tahoma" Font-Size="18pt" 
                                ForeColor="#3366FF"></asp:Label>
                        </div>
                    </div>
                   <%-- <div style="float: left; width: 970px">
                        <div style="width: 140px; float: left" class="cand_test_detail_icon">
                        </div>
                        <div style="width: 700px; float: left; padding-top: 30px"  >
                            <div style="width: 790px; float: left" class="candidate_test_header">
                              <asp:Label ID="InterviewConductionCompleted_InterviewNameLabel" runat="server" Text=""></asp:Label> 
                            </div>
                            <div style="width: 790px; float: left" class="candidate_label_text">
                                 <asp:Label ID="InterviewConductionCompleted_InterviewDescriptionLabel" runat="server" Text=""></asp:Label> 
                            </div>
                        </div>
                    </div>
                    <div style="float: left; width: 970px" class="test_conduction_completed_message_link_message">
                     Click here to send an
                        <asp:LinkButton ID="InterviewConductionCompleted_emailRecruiterLinkButton" runat="server"
                            Text="Email" CssClass="test_conduction_completed_message_link_btn" ToolTip="Click here to send an email to recruiter" 
                            OnClick="InterviewConductionCompleted_emailRecruiterLinkButton_Click"> </asp:LinkButton>
                        &nbsp;to the recruiter 
                    </div>
                    <div style="float: left; width: 970px; height: 8px">
                    </div>
                    <div style="float: left; width: 970px" class="test_conduction_completed_message_link_message">
                       Click here to go to
                        <asp:LinkButton ID="InterviewConductionCompleted_feedbackLinkButton" runat="server" Text="Feedback"
                            CssClass="test_conduction_completed_message_link_btn" PostBackUrl="~/General/Feedback.aspx?parentpage=C_DASHBOARD"
                            ToolTip="Click here to send a feedback"> </asp:LinkButton>
                        &nbsp;page
                    </div>--%>
                    <div style="float: left; width: 970px; height: 8px">
                    </div>
                    <div style="float: left; width: 970px" class="test_conduction_completed_message_link_message"><br /><br /><br /><br />
                       Click here to go to
                        <asp:LinkButton ID="InterviewConductionCompleted_homeLinkButton" runat="server" Text="Home"
                            CssClass="test_conduction_completed_message_link_btn" ToolTip="Click here to go to home page"
                            PostBackUrl="~/Dashboard.aspx?parentpage=C_DASHBOARD"> </asp:LinkButton>
                        &nbsp;page 
                        <br /><br /><br /><br />
                    </div>
                    <div style="float: left; width: 970px; height: 8px">
                    </div> 
                </div>
            </div>
        </div>
    </div>
</asp:Content>
