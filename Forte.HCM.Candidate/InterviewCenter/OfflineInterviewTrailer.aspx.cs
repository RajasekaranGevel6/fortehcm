﻿using System;
using System.Configuration;
using Forte.HCM.Trace;

namespace Forte.HCM.Candidate.InterviewCenter
{
    public partial class OfflineInterviewTrailer : System.Web.UI.Page
    {
        private string candidateSessionID;
        private int attemptID; 

        public int height = 750; 
        public string rtmp = "";
        public string selfIntroFileName = "";
        public int questionTimeDelay = 31;
        public int questionTimeOut = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                rtmp = ConfigurationManager.AppSettings["RTMP_SERVER"].ToString();
                candidateSessionID = Request.QueryString["candidateSessionID"].ToString();
                attemptID = Convert.ToInt32(Request.QueryString["attemptID"]);
                rtmp = rtmp + candidateSessionID + "_" + attemptID.ToString();

                if (ConfigurationManager.AppSettings["QUESTION_TIME_DELAY"] != null) 
                    questionTimeDelay = Convert.ToInt32(ConfigurationManager.AppSettings["QUESTION_TIME_DELAY"]);

                questionTimeOut = 120; 
                selfIntroFileName = candidateSessionID + "_" + attemptID.ToString();
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }
    }
}