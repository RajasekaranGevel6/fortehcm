﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ContinueInterviewInstructions.cs
// File that represents the user interface for interview instructions 
// page. This will display the interview instructions such as minimum 
// hardware and OS requirements, instructions, description etc. From 
// here, candidate can initiate their interview. This class inherits 
// Forte.HCM.UI.Common.PageBase. 

#endregion Header 

#region Directives                                                             

using System;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface for interview instructions 
    /// page. This will display the interview instructions such as minimum 
    /// hardware and OS requirements, instructions, description etc. From 
    /// here, candidate can initiate their interview. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class ContinueInterviewInstructions : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </summary>
        private string candidateSessionID = string.Empty;

        /// <summary>
        /// A <see cref="int"/> that holds the attempt ID.
        /// </summary>
        private int attemptID;

        #endregion Private Variables

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Interview Instructions");

                // Clear message and hide labels.
                ClearMessages();

                ContinueInterviewInstructions_launchOfflineInterviewRecorder.Attributes.Add("onClick", "LaunchOfflineInterview('" + ContinueInterviewInstructions_launchOfflineInterviewRecorder.ClientID + "')");
                ContinueInterviewInstructions_launchOfflineInterviewRecorder.NavigateUrl = ConfigurationManager.AppSettings["OFFLINE_INTERVIEW_RECORDER_URL"];

                if (Request.QueryString["candidatesessionid"] != null)
                    candidateSessionID = Request.QueryString["candidatesessionid"];

                if (Request.QueryString["attemptid"] != null)
                    int.TryParse(Request.QueryString["attemptid"], out attemptID);

                if (!Page.IsPostBack)
                {
                    // Load values.
                    LoadValues();

                    // Display browser instructions.
                    DisplayBrowserInstructions();
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(ContinueInterviewInstructions_topErrorMessageLabel,
                    ContinueInterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void GenerateSecurityCode()
        {
            // Generate and show an authentication code image.
            bool isExist = true;

            while (isExist)
            {
                // Generate a 8 lettered authentication code.
                Session["INTERVIEW_CAPTCHA_VALUE"] = RandomString(8);

                // Check if the authentication code already exist. If 
                // already exist, loop will continue to generate another
                // authentication code.
                isExist = new CandidateBLManager().UpdateInterviewSecurityCode
                    (candidateSessionID, attemptID, Session["INTERVIEW_CAPTCHA_VALUE"].ToString());
            }

            // Generate a captcha image.
            CaptchaImage ci = new CaptchaImage(Session["INTERVIEW_CAPTCHA_VALUE"].ToString(),
                200, 50, "Century Schoolbook");

            // Keep image in session.
            Session["INTERVIEW_CAPTCHA_IMAGE"] = ci.Image;
        }

        /// <summary>
        /// Handler method that will be called when the initiate interview button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void ContinueInterviewInstructions_proceedButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Generate and show security code.
                GenerateSecurityCode();

                // Update the interview initiated status.
                new CandidateBLManager().UpdateInterviewSessionTrackingStatus
                    (candidateSessionID, attemptID, "ATMPT_OIIN", base.userID);

                // Show the launch offline interview recorder link.
                ContinueInterviewInstructions_launchOfflineInterviewRecorder.Visible = true;


                // Show success message.
                ShowMessage(ContinueInterviewInstructions_topSuccessMessageLabel,
                    ContinueInterviewInstructions_bottomSuccessMessageLabel,
                    "Your security code has been generated. Click on 'Launch Offline Interview Recorder' and enter the security code shown below into the downloaded offline interview application and click on 'Start Interview' button to start the interview");

                try
                {
                    // Send a mail to the candidate along with the security code, indicating
                    // that the interview session was initiated and security code generated.
                    new EmailHandler().SendMail(EntityType.InterviewInitiated, 
                        candidateSessionID, attemptID.ToString());
                }
                catch (Exception mailException)
                {
                    Logger.ExceptionLog(mailException);
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(ContinueInterviewInstructions_topErrorMessageLabel,
                    ContinueInterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the regenerate link is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void ContinueInterviewInstructions_regenerateKeyLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                // Generate and show security code.
                GenerateSecurityCode();

                // Show success message.
                ShowMessage(ContinueInterviewInstructions_topSuccessMessageLabel,
                    ContinueInterviewInstructions_bottomSuccessMessageLabel,
                    "Your security code has been regenerated. Enter the security code shown below into the downloaded offline interview application and click on 'Start Interview' button to start the interview");

                try
                {
                    // Send a mail to the candidate along with the security code, indicating
                    // that the interview session was initiated and security code regenerated.
                    new EmailHandler().SendMail(EntityType.InterviewInitiatedRegenerated,
                        candidateSessionID, attemptID.ToString());
                }
                catch (Exception mailException)
                {
                    Logger.ExceptionLog(mailException);
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(ContinueInterviewInstructions_topErrorMessageLabel,
                    ContinueInterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the download offline 
        /// interview recorder document is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will download the offline interview recorder troubleshooting
        /// document.
        /// </remarks>
        protected void ContinueInterviewInstructions_downloadOfflineInterviewRecorderDocumentLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Common/Download.aspx?type=OFFLINE_INTERVIEW_RECORDER_DOCUMENT", false);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(ContinueInterviewInstructions_topErrorMessageLabel,
                    ContinueInterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Retrieve test instructions disclaimer message.
            DisclaimerMessage disclaimer = new CandidateBLManager().
                GetInterviewInstructionsDisclaimerMessage();

            if (disclaimer != null)
            {
                // Minimum hardware & OS requirements.
                ContinueInterviewInstructions_minimumHardwareValueLiteral.Text = disclaimer.HardwareInstructions;

                // Test instructions.
                ContinueInterviewInstructions_interviewInstructionsValueLiteral.Text = disclaimer.InterviewInstructions;

                // Warning.
                ContinueInterviewInstructions_warningLiteral.Text = disclaimer.WarningInstructions;
            }

            // Get interview detail
            InterviewDetail testDetail = new CandidateBLManager().GetInterviewDetail
                (candidateSessionID, attemptID);

            if (testDetail != null)
            {
                ContinueInterviewInstructions_interviewDescriptionValueLiteral.Text = testDetail.InterviewDescription;

                if (Utility.IsNullOrEmpty(testDetail.SecurityCode))
                {
                    // Generate a new security code.
                    GenerateSecurityCode();
                }
                else
                {
                    Session["INTERVIEW_CAPTCHA_VALUE"] = testDetail.SecurityCode;

                    // Generate a captcha image.
                    CaptchaImage ci = new CaptchaImage(Session["INTERVIEW_CAPTCHA_VALUE"].ToString(),
                        200, 50, "Century Schoolbook");

                    // Keep image in session.
                    Session["INTERVIEW_CAPTCHA_IMAGE"] = ci.Image;
                }

                // Show success message.
                ShowMessage(ContinueInterviewInstructions_topSuccessMessageLabel,
                    ContinueInterviewInstructions_bottomSuccessMessageLabel,
                    "Click on 'Launch Offline Interview Recorder' and enter the security code shown below into the downloaded offline interview application and click on 'Start Interview' button to start the interview");
            }
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            ContinueInterviewInstructions_topErrorMessageLabel.Text = string.Empty;
            ContinueInterviewInstructions_bottomErrorMessageLabel.Text = string.Empty;
            ContinueInterviewInstructions_topSuccessMessageLabel.Text = string.Empty;
            ContinueInterviewInstructions_bottomSuccessMessageLabel.Text = string.Empty;

            ContinueInterviewInstructions_topErrorMessageLabel.Visible = false;
            ContinueInterviewInstructions_bottomErrorMessageLabel.Visible = false;
            ContinueInterviewInstructions_topSuccessMessageLabel.Visible = false;
            ContinueInterviewInstructions_bottomSuccessMessageLabel.Visible = false;
        }

        /// <summary>
        /// Method that displays the browser instructions. This is applicable only
        /// for cyber proctoring tests.
        /// </summary>
        private void DisplayBrowserInstructions()
        {
            // Set browser specific information.
            string browserType = Request.Browser.Browser;

            // Check if browser type is present.
            if (Utility.IsNullOrEmpty(browserType))
                return;
            browserType = browserType.Trim().ToUpper();

            // Get browser detail and instructions.
            BrowserDetail browserDetail = new CommonBLManager().
                GetBrowserInstructions(browserType);

            // Check if browser instructions are present in the database. If
            // not send a mail to the admin.
            if (browserDetail == null)
            {
                // Send a mail to the admin, indicating that the browser 
                // instructions are not present.
                try
                {
                    // Compose browser detail.
                    browserDetail = new BrowserDetail();
                    browserDetail.Type = browserType;
                    browserDetail.Version = Request.Browser.Version;

                    // Log the detail.
                    Logger.ExceptionLog(string.Format
                        ("Browser instructions missing for type {0}", browserType));

                    // Send email to the admin.
                    new EmailHandler().SendMail(EntityType.BrowserInstructionsMissing, browserDetail);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                return;
            }

            // Set the title label.
            ContinueInterviewInstructions_browserInstructionsDiv_titleLabel.Text =
                string.Format("Browser Requirements ({0})", browserDetail.Name);

            // Set the header label.
            ContinueInterviewInstructions_browserInstructionsDiv_headerLabel.Text = 
                "Please follow the instructions and fix the browser settings. For further details click on the link to download the trouble-shooting document";

            // Assign the data source to the instructions list.
            ContinueInterviewInstructions_browserInstructionsDiv_instructionsListView.DataSource = 
                browserDetail.Instructions;
            ContinueInterviewInstructions_browserInstructionsDiv_instructionsListView.DataBind();

            // Show the browser instructions popup window.
            ScriptManager.RegisterStartupScript(this, this.GetType(),
                "ShowBrowserInstructions",
                "javascript: ShowBrowserInstructions('Y');", true);
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        #endregion Private Methods
    }
}
