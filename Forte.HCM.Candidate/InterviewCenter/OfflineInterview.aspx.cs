﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Configuration;
using System.Security.AccessControl;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects; 


namespace Forte.HCM.Candidate.InterviewCenter
{
    public partial class OfflineInterview : System.Web.UI.Page
    {
        public static string candidateSessionID; 
        public static int attemptID; 
        public static int candidateID; 

        public static bool isPaused = false;
        public static bool canPause = false;

        public int height = 750;
        public int questionTimeDelay = 31;
        public string URL = "";
        public string rtmp = "";
        public string siteURL = "";
        public string currentURL = "";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                URL = ConfigurationManager.AppSettings["APPLICATION_DATA_URL"].ToString();
                rtmp = ConfigurationManager.AppSettings["RTMP_SERVER"].ToString();
                string videoPath = ConfigurationManager.AppSettings["VIDEO_FOLDER"].ToString();
                siteURL = ConfigurationManager.AppSettings["OFFLINE_INTERVIEW_RESULT_URL"].ToString();

                candidateSessionID = Request.QueryString["candidateSessionID"].ToString();
                attemptID = Convert.ToInt32(Request.QueryString["attemptID"]);
                candidateID = Convert.ToInt32(Request.QueryString["candidateID"]);

                rtmp = rtmp + candidateSessionID + "_" + attemptID;
                string tempURL = HttpContext.Current.Request.Url.AbsoluteUri.ToString().Replace("OfflineInterview.aspx", "InterviewInstructions.aspx");
                tempURL = tempURL.Remove(tempURL.IndexOf("?"));
                tempURL = tempURL + "?candidatesessionid=" + candidateSessionID + ",attemptid=" + attemptID + ",parentpage=MY_INT";
                currentURL = tempURL;

                if (ConfigurationManager.AppSettings["QUESTION_TIME_DELAY"] != null) 
                {
                    questionTimeDelay =Convert.ToInt32(ConfigurationManager.AppSettings["QUESTION_TIME_DELAY"]);
                } 
                 
                isPaused = false;
                canPause = false;

                 /*if (!InterviewStatus())
                 {
                     Response.Redirect(ConfigurationManager.AppSettings["DASHBOARD_HOME"], false);
                 } */
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }

        /// <summary>
        /// This returns the interview status. If the status is in process it will return instruction page Check
        /// </summary>
        /// <returns></returns>
        private bool InterviewStatus()
        {
            OfflineInterviewParams interviewParams = 
                new OfflineInterviewParams(); ;

              interviewParams.candidateSessionID = candidateSessionID;
              interviewParams.attemptID = attemptID;
              DataTable dtStatus = new OfflineInterviewBLManager().GetInterviewDetails(interviewParams);

              if (dtStatus == null) return true;
              if (dtStatus.Rows.Count == 0) return true;
              if (Convert.ToString(dtStatus.Rows[0]["INTERVIEW_STATUS"]).Trim() == "SESS_INPR" )
              {
                  DateTime dbTime=Convert.ToDateTime(dtStatus.Rows[0]["MODIFIED_DATE"]);
                  DateTime dtCurrent = DateTime.Now;
                  int _hour = (dtCurrent - dbTime).Hours;
                  if (_hour < 1)
                      return false;
                  else
                      return true;
              }
             return true;
        }
    }
}