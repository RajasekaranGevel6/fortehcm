﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// InterviewInstructions.cs
// File that represents the user interface for interview instructions 
// page. This will display the interview instructions such as minimum 
// hardware and OS requirements, instructions, description etc. From 
// here, candidate can initiate their interview. This class inherits 
// Forte.HCM.UI.Common.PageBase. 

#endregion Header 

#region Directives                                                             

using System;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface for interview instructions 
    /// page. This will display the interview instructions such as minimum 
    /// hardware and OS requirements, instructions, description etc. From 
    /// here, candidate can initiate their interview. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class InterviewInstructions : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </summary>
        private string candidateSessionID = string.Empty;

        /// <summary>
        /// A <see cref="int"/> that holds the attempt ID.
        /// </summary>
        private int attemptID;

        #endregion Private Variables

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("Interview Instructions");
                
                // Clear message and hide labels.
                ClearMessages();

                InterviewInstructions_launchOfflineInterviewRecorder.Attributes.Add("onClick", "LaunchOfflineInterview('" + InterviewInstructions_launchOfflineInterviewRecorder.ClientID + "')");
                InterviewInstructions_launchOfflineInterviewRecorder.NavigateUrl = ConfigurationManager.AppSettings["OFFLINE_INTERVIEW_RECORDER_URL"];

                // Check the querystring 'n'. This querystring is passed in mail while scheduling the candidate for the interview.
                string queryString = string.Empty;
                InterviewSessionIdDetails interviewSessionIds = null;
                if (!Utility.IsNullOrEmpty(Request.QueryString["n"]))
                {
                    queryString = Request.QueryString["n"].ToString();
                    interviewSessionIds = new InterviewSessionBLManager().GetInterviewSessionIdDetails(queryString);
                    if (interviewSessionIds.CandidateId == ((UserDetail)Session["USER_DETAIL"]).UserID)
                    {
                        candidateSessionID = interviewSessionIds.CandidateSessionId;
                        attemptID = interviewSessionIds.AttemptID;
                    }
                    else
                    {
                        Response.Redirect("~/Dashboard.aspx", false);
                    }

                }
                else if (Request.QueryString["candidatesessionid"] != null && Request.QueryString["attemptid"] != null)
                {
                    candidateSessionID = Request.QueryString["candidatesessionid"];
                    int.TryParse(Request.QueryString["attemptid"], out attemptID);
                }
                else
                    Response.Redirect("~/Dashboard.aspx", false);
                InterviewInstructions_linkButton.Attributes.Add("onclick", "return PopupWindow('" + candidateSessionID + "'," + attemptID + ");");
                if (!Page.IsPostBack)
                {
                    // Load values.
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(InterviewInstructions_topErrorMessageLabel,
                    InterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        private void GenerateSecurityCode()
        {
            // Generate and show an authentication code image.
            bool isExist = true;

            while (isExist)
            {
                // Generate a 8 lettered authentication code.
                Session["INTERVIEW_CAPTCHA_VALUE"] = RandomString(8);

                // Check if the authentication code already exist. If 
                // already exist, loop will continue to generate another
                // authentication code.
                isExist = new CandidateBLManager().UpdateInterviewSecurityCode
                    (candidateSessionID, attemptID, Session["INTERVIEW_CAPTCHA_VALUE"].ToString());
            }

            // Generate a captcha image.
            CaptchaImage ci = new CaptchaImage(Session["INTERVIEW_CAPTCHA_VALUE"].ToString(),
                200, 50, "Century Schoolbook");

            // Keep image in session.
            Session["INTERVIEW_CAPTCHA_IMAGE"] = ci.Image;
        }

        /// <summary>
        /// Handler method that will be called when the initiate interview button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void InterviewInstructions_proceedButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("~/InterviewCenter/OfflineInterview.aspx");
               // Clear messages.
                ClearMessages();

                // Check if candidate agree to the terms and conditions.
                if (!InterviewInstructions_agreeCheckbox.Checked)
                {
                    ShowMessage(InterviewInstructions_topErrorMessageLabel,
                        InterviewInstructions_bottomErrorMessageLabel, 
                        "You must agreed to the terms and conditions to initiate the interview");

                    return;
                }

                // Display browser instructions.
                DisplayBrowserInstructions();

                // Generate and show security code.
                GenerateSecurityCode();

                // Update the interview initiated status.
                 new CandidateBLManager().UpdateInterviewSessionTrackingStatus
                    (candidateSessionID, attemptID, "ATMPT_OIIN", base.userID);

                 if (attemptID == 0) attemptID = 1;
                /*Added by MKN for testing only*/
                Response.Redirect("~/InterviewCenter/OfflineInterview.aspx?candidateSessionID=" + candidateSessionID + "&attemptID=" + attemptID + "&candidateID=" + base.userID);

                // Show the security code.
                InterviewInstructions_securityCodeDiv.Visible = true;

                // Show the launch offline interview recorder link.
                InterviewInstructions_launchOfflineInterviewRecorder.Visible = true;

                // Show the offline interview recorder trouble shooting document.
                InterviewInstructions_downloadOfflineInterviewRecorderDocumentTD.Visible = true;

                // Hide the agree check box and proceed button.
                InterviewInstructions_actionTD.Visible = false;

                // Show success message.
                ShowMessage(InterviewInstructions_topSuccessMessageLabel,
                    InterviewInstructions_bottomSuccessMessageLabel,
                    "Your security code has been generated. Click on 'Launch Offline Interview Recorder' and enter the security code shown below into the downloaded offline interview application and click on 'Start Interview' button to start the interview");

                try
                {
                    // Send a mail to the candidate along with the security code, indicating
                    // that the interview session was initiated and security code generated.
                    new EmailHandler().SendMail(EntityType.InterviewInitiated, 
                        candidateSessionID, attemptID.ToString());

                   
                }
                catch (Exception mailException)
                {
                    Logger.ExceptionLog(mailException);
                } 
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(InterviewInstructions_topErrorMessageLabel,
                    InterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the regenerate link is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void InterviewInstructions_regenerateKeyLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                // Generate and show security code.
                GenerateSecurityCode();

                // Show success message.
                ShowMessage(InterviewInstructions_topSuccessMessageLabel,
                    InterviewInstructions_bottomSuccessMessageLabel,
                    "Your security code has been regenerated. Enter the security code shown below into the downloaded offline interview application and click on 'Start Interview' button to start the interview");

                try
                {
                    // Send a mail to the candidate along with the security code, indicating
                    // that the interview session was initiated and security code regenerated.
                    new EmailHandler().SendMail(EntityType.InterviewInitiatedRegenerated,
                        candidateSessionID, attemptID.ToString());
                }
                catch (Exception mailException)
                {
                    Logger.ExceptionLog(mailException);
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(InterviewInstructions_topErrorMessageLabel,
                    InterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the download offline 
        /// interview recorder document is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will download the offline interview recorder troubleshooting
        /// document.
        /// </remarks>
        protected void InterviewInstructions_downloadOfflineInterviewRecorderDocumentLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Common/Download.aspx?type=OFFLINE_INTERVIEW_RECORDER_DOCUMENT", false);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);

                ShowMessage(InterviewInstructions_topErrorMessageLabel,
                    InterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Retrieve test instructions disclaimer message.
            DisclaimerMessage disclaimer = new CandidateBLManager().
                GetInterviewInstructionsDisclaimerMessage();

            if (disclaimer != null)
            {
                // Minimum hardware & OS requirements.
                InterviewInstructions_minimumHardwareValueLiteral.Text = disclaimer.HardwareInstructions;

                // Test instructions.
                InterviewInstructions_interviewInstructionsValueLiteral.Text = disclaimer.InterviewInstructions;

                // Warning.
                InterviewInstructions_warningLiteral.Text = disclaimer.WarningInstructions;
            }

            // Get interview detail
            InterviewDetail testDetail = new CandidateBLManager().GetInterviewDetail
                (candidateSessionID, attemptID);

            if (testDetail != null)
            {
                InterviewInstructions_interviewDescriptionValueLiteral.Text = testDetail.InterviewDescription;
                InterviewInstructions_intervieNameLabel.Text = testDetail.InterviewName.ToString().Replace(Environment.NewLine, "<br />"); 
                InterviewInstructions_interviewDescLabel.Text = testDetail.InterviewDescription.ToString().Replace(Environment.NewLine, "<br />"); 
            }
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            InterviewInstructions_topErrorMessageLabel.Text = string.Empty;
            InterviewInstructions_bottomErrorMessageLabel.Text = string.Empty;
            InterviewInstructions_topSuccessMessageLabel.Text = string.Empty;
            InterviewInstructions_bottomSuccessMessageLabel.Text = string.Empty;

            InterviewInstructions_topErrorMessageLabel.Visible = false;
            InterviewInstructions_bottomErrorMessageLabel.Visible = false;
            InterviewInstructions_topSuccessMessageLabel.Visible = false;
            InterviewInstructions_bottomSuccessMessageLabel.Visible = false;
        }

        /// <summary>
        /// Method that displays the browser instructions. This is applicable only
        /// for cyber proctoring tests.
        /// </summary>
        private void DisplayBrowserInstructions()
        {
            // Set browser specific information.
            string browserType = Request.Browser.Browser;

            // Check if browser type is present.
            if (Utility.IsNullOrEmpty(browserType))
                return;
            browserType = browserType.Trim().ToUpper();

            // Get browser detail and instructions.
            BrowserDetail browserDetail = new CommonBLManager().
                GetBrowserInstructions(browserType);

            // Check if browser instructions are present in the database. If
            // not send a mail to the admin.
            if (browserDetail == null)
            {
                // Send a mail to the admin, indicating that the browser 
                // instructions are not present.
                try
                {
                    // Compose browser detail.
                    browserDetail = new BrowserDetail();
                    browserDetail.Type = browserType;
                    browserDetail.Version = Request.Browser.Version;

                    // Log the detail.
                    Logger.ExceptionLog(string.Format
                        ("Browser instructions missing for type {0}", browserType));

                    // Send email to the admin.
                    new EmailHandler().SendMail(EntityType.BrowserInstructionsMissing, browserDetail);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                return;
            }

            // Set the title label.
            InterviewInstructions_browserInstructionsDiv_titleLabel.Text =
                string.Format("Browser Requirements ({0})", browserDetail.Name);

            // Set the header label.
            InterviewInstructions_browserInstructionsDiv_headerLabel.Text = 
                "Please follow the instructions and fix the browser settings. For further details click on the link to download the trouble-shooting document";

            // Assign the data source to the instructions list.
            InterviewInstructions_browserInstructionsDiv_instructionsListView.DataSource = 
                browserDetail.Instructions;
            InterviewInstructions_browserInstructionsDiv_instructionsListView.DataBind();

            // Show the browser instructions popup window.
            ScriptManager.RegisterStartupScript(this, this.GetType(),
                "ShowBrowserInstructions",
                "javascript: ShowBrowserInstructions('Y');", true);
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        #endregion Private Methods
 
    }
}
