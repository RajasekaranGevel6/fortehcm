﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestIntroduction.cs
// File that represents the user interface for test introduction page.
// This will display the test details and instructions before start the
// test. From here, candidate can start their test as well as set reminder.

#endregion Header                                                              

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface for test introduction page.
    /// This will display the test details and instructions before start the
    /// test. From here, candidate can start their test as well as set reminder.
    /// This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class InterviewIntroduction : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when the page gets loaded. This handler
        /// loads the default values and set javascript functions to the 
        /// buttons.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                // Set browser title.
                Master.SetPageCaption("Interview Introduction");

                if (!IsPostBack)
                {
                    if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
                    {
                        // Clear status.
                        Session["IS_PAUSED"] = null;

                        // Load interview details.
                        LoadValues();
                    }
                }

                if (Session["IS_PAUSED"] != null && Convert.ToBoolean(Session["IS_PAUSED"]) == true)
                {
                    // Set button title on interview status.
                    InterviewIntroduction_startButton.Text = "Continue Interview";
                }
                else
                {
                    // Set button title on interview status.
                    InterviewIntroduction_startButton.Text = "Start Interview";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(InterviewIntroduction_topErrorMessageLabel,
                    InterviewIntroduction_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the start/continue interview 
        /// button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void InterviewIntroduction_startInterviewButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["IS_PAUSED"] != null && Convert.ToBoolean(Session["IS_PAUSED"]) == true)
                {
                    Response.Redirect("~/InterviewCenter/ContinueInterviewInstructions.aspx" +
                        "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                        "&attemptid=" + Request.QueryString["attemptid"] +
                        "&displaytype=" + Request.QueryString["displaytype"] +
                        "&parentpage=" + Constants.ParentPage.INTERVIEW_INTRODUCTION +
                        "&sourcepage=" + Request.QueryString["parentpage"], false);
                }
                else
                {
                    Response.Redirect("~/InterviewCenter/InterviewInstructions.aspx" +
                        "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                        "&attemptid=" + Request.QueryString["attemptid"] +
                        "&displaytype=" + Request.QueryString["displaytype"] +
                        "&parentpage=" + Constants.ParentPage.INTERVIEW_INTRODUCTION +
                        "&sourcepage=" + Request.QueryString["parentpage"], false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(InterviewIntroduction_topErrorMessageLabel,
                   InterviewIntroduction_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the remind me link button 
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test reminder page.
        /// </remarks>
        protected void InterviewIntroduction_remindMeLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Popup/TestReminder.aspx" +
                    "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                    "&attemptid=" + Request.QueryString["attemptid"] +
                    "&displaytype=" + Request.QueryString["displaytype"] +
                    "&parentpage=" + Constants.ParentPage.INTERVIEW_INTRODUCTION +
                    "&activitytype=INT" +
                    "&sourcepage=" + Request.QueryString["parentpage"], false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(InterviewIntroduction_topErrorMessageLabel,
                   InterviewIntroduction_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            InterviewIntroductionDetail interviewIntro = new CandidateBLManager().
                GetInterviewIntroduction(Request.QueryString["candidatesessionid"], 
                Convert.ToInt32(Request.QueryString["attemptid"]));

            if (interviewIntro == null)
                return;

            InterviewIntroduction_interviewNameValueLabel.Text = interviewIntro.InterviewName;
            InterviewIntroduction_interviewAreasValueLabel.Text = interviewIntro.InterviewTestAreas;
            InterviewIntroduction_interviewSubjectsValueLabel.Text = interviewIntro.InterviewSubjects;
            InterviewIntroduction_interviewDescriptionValueLabel.Text = interviewIntro.InterviewDescription == null ? 
                interviewIntro.InterviewDescription : interviewIntro.InterviewDescription.
                    ToString().Replace(Environment.NewLine, "<br />");
            InterviewIntroduction_interviewInstructionsLiteral.Text = interviewIntro.InterviewInstructions == null ?
                interviewIntro.InterviewInstructions : interviewIntro.InterviewInstructions.
                    ToString().Replace(Environment.NewLine, "<br />");
            Session["IS_PAUSED"] = interviewIntro.IsPaused;
        }

        #endregion Protected Overridden Methods     

        #region Private Methods                                                

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            InterviewIntroduction_topErrorMessageLabel.Text = string.Empty;
            InterviewIntroduction_bottomErrorMessageLabel.Text = string.Empty;
            InterviewIntroduction_topSuccessMessageLabel.Text = string.Empty;
            InterviewIntroduction_bottomSuccessMessageLabel.Text = string.Empty;

            InterviewIntroduction_topErrorMessageLabel.Visible = false;
            InterviewIntroduction_bottomErrorMessageLabel.Visible = false;
            InterviewIntroduction_topSuccessMessageLabel.Visible = false;
            InterviewIntroduction_bottomSuccessMessageLabel.Visible = false;
        }

        #endregion Private Methods

    }
}