﻿using System;
using System.IO;
using System.Configuration;

using Forte.HCM.Support;
using Forte.HCM.Trace; 

namespace Forte.HCM.Candidate.InterviewCenter
{
    public partial class OnlineInterview : System.Web.UI.Page
    {
        #region Public Variables
        public string rtmp = string.Empty;
        public string userRole = string.Empty;
        public string userID = string.Empty;
        public string candidateID = string.Empty;
        public string URL = "http://172.16.3.112/fortehcmcandidate/InterviewCenter/OnlineInterviewData.ashx";
        public string chatRoomName = string.Empty;
        public string chatRoomId = string.Empty;
        public string sessionId = string.Empty;
        #endregion
          
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["sessionid"] != null)
                    sessionId = Request.QueryString["sessionid"].ToString();

                if (Request.QueryString["chatroom"] != null)
                    chatRoomId = Request.QueryString["chatroom"].ToString();

                if (Request.QueryString["userid"] != null)
                    userID = Request.QueryString["userid"].ToString();

                if (Request.QueryString["candidateid"] != null)
                    candidateID = Request.QueryString["candidateid"].ToString();

                if (Request.QueryString["userrole"] != null)
                    userRole = Request.QueryString["userrole"].ToString();

                if (Utility.IsNullOrEmpty(sessionId)) return;
                if (Utility.IsNullOrEmpty(chatRoomId)) return;

                chatRoomName = CreatingFolder(sessionId, chatRoomId);

                if (Utility.IsNullOrEmpty(chatRoomName)) return;

                rtmp = ConfigurationManager.AppSettings["RTMP_ONLINE_CONNECTION"].ToString() + "/" + sessionId + "/" + chatRoomId;
            }
            catch(Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        public string CreatingFolder(string folderName, string ChatRoomName)
        {
            try
            {
                string folderPath = ConfigurationManager.AppSettings["ONLINE_INTERVIEWS_PATH"].ToString();
                folderPath += folderName;

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                string mainFolderPath = ConfigurationManager.AppSettings["ONLINE_INTERVIEWS_PATH"].ToString();

                CreateChatRoom(folderPath, "\\" + ChatRoomName);

                CopyFiles(mainFolderPath, folderPath + "\\" + ChatRoomName + "\\");

                return folderPath + "\\" + ChatRoomName;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                return null;
            }
        }

        private void CreateChatRoom(string folderPath, string chatRoomName)
        {
            if (!Directory.Exists(folderPath + chatRoomName))
                Directory.CreateDirectory(folderPath + chatRoomName);
        }

        private void CopyFiles(string mainFolderPath, string folderPath)
        {
            try
            {
                File.Copy(mainFolderPath + "main.asc", folderPath + "main.asc", true);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
    }
}