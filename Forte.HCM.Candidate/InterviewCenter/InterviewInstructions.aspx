<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="InterviewInstructions.aspx.cs" Title="Interview Instructions" Inherits="Forte.HCM.UI.CandidateCenter.InterviewInstructions" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="InterviewInstructions_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">

        // Method that will show/hide the captcha image help panel.
        function ShowBrowserInstructions(show) {
            if (show == 'Y')
                document.getElementById('InterviewInstructions_browserInstructionsDiv').style.display = "block";
            else
                document.getElementById('InterviewInstructions_browserInstructionsDiv').style.display = "none";

            return false;
        }

        // Method that will show/hide the captcha image help panel.
        function ShowWhatsThis(show) {
            if (show == 'Y')
                document.getElementById('InterviewInstructions_whatsThisDiv').style.display = "block";
            else
                document.getElementById('InterviewInstructions_whatsThisDiv').style.display = "none";
            return false;
        }

        function LaunchOfflineInterview(a) {
            document.getElementById(a).style.display = "none";
            return false;
        }

        function PopupWindow(candidateSessionID, attemptID) {
            var height = 735;
            var width = 1075;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:no";

            var queryStringValue = "../InterviewCenter/OfflineInterviewTrailer.aspx?candidateSessionID=" + candidateSessionID + "&attemptID=" + attemptID;
            window.open(queryStringValue); //, "", sModalFeature, false); 
            return false;
        }

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 960px; float: left">
                    <div style="text-align: center">
                        <asp:Label ID="InterviewInstructions_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewInstructions_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div> 
                    
                    <div class="activities_outer_border">
                        <div class="activities_inner_cont_left" style="float:left">
                                <input type="image" name="ctl00$MainContent$Activities_pendingInterviewsImageButton" id="ctl00_MainContent_Activities_pendingInterviewsImageButton" src="../App_Themes/DefaultTheme/Images/pending_interview_icon.png" style="border-width:0px;" />
                         </div>
                        <div style="float:left;padding-top:30px">
                            <asp:Label ID="InterviewInstructions_intervieNameLabel" runat="server"  SkinID="sknActivityNameLabel"></asp:Label>  
                        </div>
                          <div class="activities_border_top">
                          </div>

                    <%--    <div class="test_body_bg_div" style="float:right">
                             <div class="interview_test_content"> 
                               </div>  
                        </div> --%>
                        <div class="test_body_bg_div">
                            <div class="interview_test_content"> 
                                <asp:Label ID="InterviewInstructions_interviewDescLabel" runat="server" SkinID="sknActivityValueLabel" ></asp:Label>
                            </div>
                        </div>
                    </div>


                    <div class="test_body_bg_div">
                        <div class="interview_test_stop_icon">
                        </div>
                        <div id="TestInstructions_title" class="interview_test_header">
                            <asp:Literal ID="InterviewInstructions_headerLiteral" runat="server" Text="Please read this section completely before starting your interview"></asp:Literal>
                        </div>
                    </div>
                   
                    <div class="test_body_bg_div">
                        <div class="interview_test_content">
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">1</div>
                                <div style=" width: 890px;float: right;padding-left:3px">This is a video interview that requires you to face your webcam and speak your
                                answers. Please ensure therefore that you have a working webcam and microphone attached
                                to your computer, and that you are seated in a well-lit room. Further, your system
                                must have at least 512MB of RAM for optimal video capture, and be running a Windows
                                operating system that is Windows XP or newer. </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">2</div>
                                <div style=" width: 890px;float: right;padding-left:3px">You will be presented with one question at a time on your screen. Please click
                                on the following link to view a 
                                <a href="../App_Themes/DefaultTheme/Images/intelliVIEW candidate sample screenshot.jpg" target="_blank"> sample screenshot</a>.</div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">3</div><div style=" width: 890px;float: right;padding-left:3px"> As indicated by a countdown timer that you will see on the screen as soon as
                                a question comes up, you will have 30 seconds to read the question and prepare your
                                answer before the video recording for the question begins.</div>
                            </div>
                            <div class="interview_test_div_padding_left" style="padding-left:40px;">
                                <div class="interview_test_instruction_01" >
                                </div>
                            </div>
                            <div class="interview_test_content_link" style="padding-left:40px;">
                                Please wait until the 30-second countdown has ended before you start speaking your
                                response.
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">4</div><div style=" width: 890px;float: right;padding-left:3px"> Each question may have a different response time limit. The amount of time that
                                you have to speak your answer before the next question comes up will be indicated
                                below each question in the following manner �  [<a href="../App_Themes/DefaultTheme/Images/intelliVIEW candidate sample screenshot.jpg" target="_blank">sample screenshot</a>]</div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">5</div><div style=" width: 890px;float: right;padding-left:3px"> A timer located at the top right corner of your screen will indicate to you at
                                all times, the amount of time that you have left to complete your response for that
                                question before the next question comes up</div>
                            </div>
                            <div class="interview_test_div_padding_left" style="padding-left:40px;">
                                <div class="interview_test_instruction_02">
                                </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">6</div><div style=" width: 890px;float: right;padding-left:3px">You will notice a text box located below the question. Please use this box to
                                enter a text response, if and only if specifically requested to do so in the question
                                being posed. Else, please limit your answers to video responses alone.</div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">7</div><div style=" width: 890px;float: right;padding-left:3px">If you have completed speaking your response for a question, you may click on
                                the orange-colored �Next� button located at the bottom of the screen to load the
                                next question, rather than wait until the allotted time runs out.</div>
                            </div>
                            <div class="interview_test_div_padding_left" style="padding-left:40px;">
                                <div class="interview_test_instruction_03">
                                </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">8</div><div style=" width: 890px;float: right;padding-left:3px">You may take a pass on a question by clicking on the blue-colored �Skip� button.
                                However, please remember that you have only one opportunity to answer a question.
                                Once a question has been skipped, you will not have another opportunity to answer
                                the question. We therefore encourage you to answer each question to the best of
                                your ability.</div>
                            </div>
                            <div class="interview_test_content_link" style="padding-left:40px;">
                                Please do not click on the �back� or �refresh� button on your browser in an attempt
                                to reload a question at any time during the interview. This will invalidate your
                                interview.
                            </div>
                             <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">9</div><div style=" width: 890px;float: right;padding-left:3px">
                                    Please click on the following link to answer a sample question and review
                                 the video recording, before starting your interview. Your response to this question will not be submitted to the client 
                                 and is for your testing only. The question will open in a new window. Once you have completed reviewing your 
                                 recorded response, you may close the window and return to this screen.
                                 </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div style=" width: 890px;float: right;padding-left:35px">
                                    <asp:LinkButton ID="InterviewInstructions_linkButton" runat="server" 
                                        >Sample 1-question interview</asp:LinkButton>
                                 </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float:left">10</div><div style=" width: 890px;float: right;padding-left:3px">You may now start your interview by clicking on the �Start Interview� button
                                below. When presented with the following prompt, please click on the �Allow� button</div>
                            </div>
                            <div class="interview_test_div_padding_left" style="padding-left:40px;">
                                <div class="interview_test_instruction_04">
                                </div>
                            </div>
                            <div class="interview_test_wish">
                                Wish you all the best!
                            </div>
                            <div class="interview_btn"> 
                               <asp:Button ID="InterviewInstructions_proceedButton" Height="24px" runat="server" Text="Start Interview"
                                        SkinID="sknButtonOrange" OnClick="InterviewInstructions_proceedButton_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="test_body_bg_div" style="display:none">
                        <div class="test_body_bg_div">
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="InterviewInstructions_minimumHardwareLiteral" runat="server" Text="Minimum Hardware & OS Requirements"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="InterviewInstructions_minimumHardwareValueLiteral" runat="server"
                                    Text=""></asp:Literal>
                            </div>
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="InterviewInstructions_interviewInstructionsLiteral" runat="server"
                                    Text="Interview Instructions"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="InterviewInstructions_interviewInstructionsValueLiteral" runat="server"></asp:Literal>
                            </div>
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="InterviewInstructions_interviewDescriptionLiteral" runat="server"
                                    Text="Interview Description"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="InterviewInstructions_interviewDescriptionValueLiteral" runat="server"
                                    Text=""></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="InterviewInstructions_warningLiteral" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div id="InterviewInstructions_actionTD" runat="server" class="test_body_bg_div" style="display:none">
                        <div style="width: 190px; height: 60px; float: left">
                        </div>
                        <div style="width: 580px; text-align: center; float: left;">
                            <div class="test_text_div">
                                <asp:CheckBox ID="InterviewInstructions_agreeCheckbox" runat="server" Text=" I agree with the terms and conditions" Checked="true" />
                            </div>
                            <div style="width: 570px">
                                <div class="test_resume_header_bg_left" style="width: 280px; text-align: right">
                                   
                                </div>
                                <div class="test_resume_header_bg_right" style="width: 280px; text-align: left">
                                    <asp:LinkButton runat="server" ID="InterviewInstructions_cancelLinkButton" ToolTip="Click here to go back to the parent"
                                        Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknDivActionLinkButton"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="test_body_bg_div" id="InterviewInstructions_securityCodeDiv" runat="server"
                        visible="false">
                        <div style="width: 190px; height: 100px; float: left">
                        </div>
                        <div style="width: 580px; text-align: center; float: left; background-color: #e8e9ec">
                            <div>
                                <div style="float: left; width: 580px" class="click_once_launch_instructions_label">
                                    <asp:Literal ID="InterviewInstructions_captchaDescriptionLiteral" Text="Click on 'Launch Offline Interview Recorder' to launch the offline interview recorder application and enter the security code shown below into it and click on 'Start Interview' button to start the interview"
                                        runat="server">
                                    </asp:Literal>
                                </div>
                                <div style="float: left; width: 580px; height: 8px">
                                </div>
                                <div style="float: left; width: 580px">
                                    <div style="float: left; width: 300px; text-align: right">
                                        <img id="InterviewInstructions_capchaImage" src="~/Common/CaptchaViewer.aspx?type=OI"
                                            runat="server" alt="Captcha Image" />
                                    </div>
                                    <div style="float: left; width: 220px; text-align: left; padding-left: 4px">
                                        <asp:LinkButton ID="InterviewInstructions_regenerateKeyLinkButton" runat="server"
                                            ToolTip="Click here to regenerate the security code" Text="Regenerate Security Code"
                                            OnClick="InterviewInstructions_regenerateKeyLinkButton_Click" SkinID="sknActionLinkButton" />
                                    </div>
                                    <div style="float: left; width: 220px; height: 20px; text-align: left">
                                    </div>
                                    <div style="float: left; width: 220px; text-align: left; padding-left: 4px">
                                        <asp:LinkButton ID="InterviewInstructions_whatsThisLinkButton" runat="server" Text="What's This"
                                            ToolTip="Click here to show the help on security code" SkinID="sknActionLinkButton"
                                            OnClientClick="javascript:return ShowWhatsThis('Y')" />
                                        <div>
                                            <div id="InterviewInstructions_whatsThisDiv" style="display: none; height: 150px;
                                                width: 270px; left: 390px; top: 340px; z-index: 0; position: absolute" class="popupcontrol_confirm">
                                                <div style="width: 268px; float: left">
                                                    <div class="popup_header_text" style="width: 100px; padding-left: 10px; padding-top: 10px;
                                                        float: left">
                                                        <asp:Literal ID="InterviewInstructions_whatsThisDiv_titleLiteral" runat="server"
                                                            Text="What's This"></asp:Literal>
                                                    </div>
                                                    <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                                                        <asp:ImageButton ID="InterviewInstructions_whatsThisDiv_topCancelImageButton" runat="server"
                                                            SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowWhatsThis('N')" />
                                                    </div>
                                                </div>
                                                <div style="width: 268px; padding-left: 10px; float: left">
                                                    <div style="width: 248px; float: left; height: 100px" class="interview_instructions_captcha_inner_bg">
                                                        <div style="width: 228px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                                                            <asp:Literal ID="InterviewInstructions_whatsThisDiv_messageLiteral" runat="server"
                                                                Text="This is the security code for starting the offline interview recording. Enter the security code into the downloaded offline interview recording application and start">
                                                            </asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <asp:HyperLink ID="InterviewInstructions_launchOfflineInterviewRecorder" Text="Launch Offline Interview Recorder"
                                    runat="server" Visible="false" ToolTip="Click here to launch the offline interview recorder application"></asp:HyperLink>
                            </div>
                            <div runat="server" id="InterviewInstructions_downloadOfflineInterviewRecorderDocumentTD"
                                visible="false" class="click_once_launch_instructions_label">
                                If you have any issues in launching the offline interview recorder, click
                                <asp:LinkButton ID="InterviewInstructions_downloadOfflineInterviewRecorderDocumentLinkButton"
                                    runat="server" Text="here" OnClick="InterviewInstructions_downloadOfflineInterviewRecorderDocumentLinkButton_Click"></asp:LinkButton>&nbsp;to
                                download the trouble-shooting document
                            </div>
                        </div>
                    </div> 
                </div>
                <div style="width: 960px; float: left">
                    <div style="text-align: center">
                        <asp:Label ID="InterviewInstructions_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewInstructions_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
    <div id="InterviewInstructions_browserInstructionsDiv" style="display: none; height: 222px;
        width: 580px; left: 338px; top: 160px; z-index: 1; position: absolute" class="popupcontrol_confirm">
        <div style="width: 578px; float: left">
            <div class="popup_header_text" style="width: 540px; padding-left: 10px; padding-top: 10px;
                float: left">
                <asp:Label ID="InterviewInstructions_browserInstructionsDiv_titleLabel" runat="server"
                    Text=""></asp:Label>
            </div>
            <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                <asp:ImageButton ID="InterviewInstructions_browserInstructionsDiv_closeImageButton"
                    runat="server" ToolTip="Click here to close" SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowBrowserInstructions('N')" />
            </div>
        </div>
        <div style="width: 578px; padding-left: 10px; float: left">
            <div style="width: 558px; float: left; height: 160px" class="interview_instructions_captcha_inner_bg">
                <div style="width: 548px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                    <asp:Label ID="InterviewInstructions_browserInstructionsDiv_headerLabel" runat="server"
                        Text="" SkinID="sknBrowserInstructionsHeader">
                    </asp:Label>
                </div>
                <div style="width: 10px; float: left; padding-left: 4px; padding-right: 4px; height: 60px">
                </div>
                <div style="width: 508px; float: left; padding-left: 4px; padding-right: 4px" class="browser_instructions_list_view">
                    <div id="InterviewInstructions_browserInstructionsListViewDiv" runat="server" style="height: 96px;
                        overflow: auto;">
                        <asp:ListView ID="InterviewInstructions_browserInstructionsDiv_instructionsListView"
                            runat="server" SkinID="sknBrowserInstructionsListView">
                            <LayoutTemplate>
                                <ul class="browser_instructions_list_place_holder">
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                </ul>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div style="width: 480px">
                                    <asp:Label ID="InterviewInstructions_browserInstructionsDiv_instructionsListView_instructionsLabel"
                                        runat="server" SkinID="sknBrowserInstructionsRow" Text='<%# Eval("Instruction") %>' />
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
            <div style="width: 568px; float: left; padding-left: 4px;">
                <asp:LinkButton runat="server" ID="InterviewInstructions_browserInstructionsDiv_cancelLinkButton"
                    ToolTip="Click here to close" Text="Cancel" OnClientClick="javascript:return ShowBrowserInstructions('N')"
                    SkinID="sknPopupLinkButton"></asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
