﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MyInterviews.aspx.cs
// Class that represents the user interface layout and functionalities
// for the MyInterview page. This page helps in viewing the pending, 
// completed and expired interview details. This also provided links 
// for starting a interview, setting reminder, view interview introduction
// etc. This class inherits the Forte.HCM.UI.Common.PageBase class..

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the MyInterview page. This page helps in viewing the pending, 
    /// completed and expired interview details. This also provided links 
    /// for starting a interview, setting reminder, view interview introduction
    /// etc. This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class MyInterviews : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Do not allow limited user.
                if (base.isLimited)
                    Response.Redirect("~/ActivityHome.aspx", false);

                // Subscribe to paging control events.
                SubscribePagingEvents();

                // Set page title
                Master.SetPageCaption(Resources.HCMResource.MyInterviews_Title);

                // Show/hide menu based on user type (limited or not).
                Master.ShowMenu = !base.isLimited;

                if (!IsPostBack)
                {
                    // Add handlers for expand and collapse buttons in pending,
                    // completed and expired tests section.
                    //MyInterviews_pendingInterviewsHeaderTR.Attributes.Add("onclick", "MultipleExpandorCompress('" +
                    //    MyInterviews_pendingInterviewsTR.ClientID + "','" +
                    //    MyInterviews_pendingInterviewsDiv.ClientID + "','" + MyInterviews_pendingInterviewsUpSpan.ClientID + "','" +
                    //    MyInterviews_pendingInterviewsDownSpan.ClientID + "','" + MyInterviews_pendingInterviewsPlusDiv.ClientID + "')");
                    //MyInterviews_completedInterviewsHeaderTR.Attributes.Add("onclick", "MultipleExpandorCompress('" +
                    //    MyInterviews_completedInterviewsTR.ClientID + "','" +
                    //    MyInterviews_completedInterviewsDiv.ClientID + "','" + MyInterviews_completedInterviewsUpSpan.ClientID + "','" +
                    //    MyInterviews_completedInterviewsDownSpan.ClientID + "','" + MyInterviews_completedInterviewsPlusDiv.ClientID + "')");
                    //MyInterviews_expiredInterviewsHeaderTR.Attributes.Add("onclick", "MultipleExpandorCompress('" +
                    //    MyInterviews_expiredInterviewsTR.ClientID + "','" +
                    //    MyInterviews_expiredInterviewsDiv.ClientID + "','" + MyInterviews_expiredInterviewsUpSpan.ClientID + "','" +
                    //    MyInterviews_expiredInterviewsDownSpan.ClientID + "','" + MyInterviews_expiredInterviewsPlusDiv.ClientID + "')");
                    // Set message, title and type for the confirmation control
                    // for start interview.
                    MyInterviews_StartInterviewPopupExtenderControl.Message = "Are you ready to start the interview?";
                    MyInterviews_StartInterviewPopupExtenderControl.Title = "Start Interview";
                    MyInterviews_StartInterviewPopupExtenderControl.Type = MessageBoxType.YesNo;

                    MyInterviews_ContinueInterviewPopupExtenderControl.Message = "Are you ready to continue the interview?";
                    MyInterviews_ContinueInterviewPopupExtenderControl.Title = "Continue Interview";
                    MyInterviews_ContinueInterviewPopupExtenderControl.Type = MessageBoxType.YesNo;

                    // Assign default sort field and order for the pending, 
                    // completed and expired tests section.
                    if (Utility.IsNullOrEmpty(ViewState["PENDING_INTERVIEWS_SORT_ORDER"]))
                        ViewState["PENDING_INTERVIEWS_SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["PENDING_INTERVIEWS_SORT_FIELD"]))
                        ViewState["PENDING_INTERVIEWS_SORT_FIELD"] = "TESTNAME";

                    if (Utility.IsNullOrEmpty(ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"]))
                        ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"]))
                        ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"] = "TESTNAME";

                    if (Utility.IsNullOrEmpty(ViewState["EXPIRED_INTERVIEWS_SORT_ORDER"]))
                        ViewState["EXPIRED_INTERVIEWS_SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["EXPIRED_INTERVIEWS_SORT_FIELD"]))
                        ViewState["EXPIRED_INTERVIEWS_SORT_FIELD"] = "TESTNAME";

                    // Load pending, completed and expired tests. 
                    LoadValues();

                    if (Request.QueryString["ex"] == null)
                        return;
                    int key = 0;
                    int.TryParse(Request.QueryString["ex"], out key);
                    if (key == 0 || key > 3)
                        return;
                    SwitchPanels(key);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of pending tests section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void MyInterviews_pendingInterviewsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowPendingInterviews(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of completed tests section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void MyInterviews_completedInterviewsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Show completed interviews.
                ShowCompletedInterviews(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of expired tests section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void MyInterviews_expiredInterviewsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Show expired interviews.
                ShowExpiredInterviews(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void MyInterviews_pendingInterviewsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "InterviewIntroduction")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (MyInterviews_pendingInterviewsGridView.Rows
                        [index].FindControl("MyInterviews_pendingInterviewsCandidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((MyInterviews_pendingInterviewsGridView.Rows
                        [index].FindControl("MyInterviews_pendingInterviewsAttemptIDHiddenField") as HiddenField).Value);

                    Response.Redirect("~/InterviewCenter/InterviewIntroduction.aspx" +
                        "?candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=MY_INT", false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void MyInterviews_completedInterviewsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TestResults")
                {
                    int index = Convert.ToInt32(e.CommandArgument);

                    string candidateSessionID = (MyInterviews_completedInterviewsGridView.Rows
                        [index].FindControl("MyInterviews_completedInterviewsCandidateSessionIDHiddenField") as HiddenField).Value;

                    int attemptID = Convert.ToInt32((MyInterviews_completedInterviewsGridView.Rows
                        [index].FindControl("MyInterviews_completedInterviewsAttemptIDHiddenField") as HiddenField).Value);

                    string testID = (MyInterviews_completedInterviewsGridView.Rows
                       [index].FindControl("MyInterviews_completedInterviewsTestIDHiddenField") as HiddenField).Value;

                    Response.Redirect("~/TestCenter/CandidateTestResult.aspx" +
                        "?candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=MY_TST" +
                        "&testkey=" + testID, false);
                }
                else if (e.CommandName == "viewcertificate")
                {
                    int rowIndex = Convert.ToInt32(e.CommandArgument);

                    // Find imagebutton in the gridview.
                    ImageButton viewCertificateImageButton = (MyInterviews_completedInterviewsGridView.Rows
                        [rowIndex].FindControl("MyInterviews_certificationImageButton") as ImageButton);

                    string candidateSessionID = (MyInterviews_completedInterviewsGridView.Rows
                        [rowIndex].FindControl("MyInterviews_completedInterviewsCandidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((MyInterviews_completedInterviewsGridView.Rows
                        [rowIndex].FindControl("MyInterviews_completedInterviewsAttemptIDHiddenField") as HiddenField).Value);
                    string testKey = (MyInterviews_completedInterviewsGridView.Rows
                       [rowIndex].FindControl("MyInterviews_completedInterviewsTestIDHiddenField") as HiddenField).Value;
                    string testName = (MyInterviews_completedInterviewsGridView.Rows
                       [rowIndex].FindControl("MyInterviews_completedInterviewsTestNameHiddenField") as HiddenField).Value;
                    string testCompletedOn = (MyInterviews_completedInterviewsGridView.Rows
                       [rowIndex].FindControl("MyInterviews_completedInterviewsCompletedOnHiddenField") as HiddenField).Value;

                    viewCertificateImageButton.Attributes.Add("OnClick", "javascript:return OpenCertificateWindow('"
                        + candidateSessionID + "','" + testKey + "','"
                        + attemptID + "','" + testCompletedOn + "','CERTIFICATE')");
                }
                else if (e.CommandName == "RequestToRetake")
                {
                    string candidateSessionID = ((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("MyInterviews_completedInterviewsCandidateSessionIDHiddenField")).Value;

                    string testID = ((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("MyInterviews_completedInterviewsTestIDHiddenField")).Value;

                    int attemptID = int.Parse(((HiddenField)((ImageButton)e.CommandSource).
                         FindControl("MyInterviews_completedInterviewsAttemptIDHiddenField")).Value);

                    if (IsValidRetake(candidateSessionID, testID, attemptID))
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "RetakeTest",
                         "javascript: OpenCandidateRequest('"
                         + candidateSessionID + "','"
                         + attemptID + "','retake')", true);
                    else
                        MyInterviews_RetakeValidation_ModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                MyInterviews_RetakeValidationUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void MyInterviews_pendingInterviewsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (MyInterviews_pendingInterviewsGridView,
                        (string)ViewState["PENDING_INTERVIEWS_SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["PENDING_INTERVIEWS_SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void MyInterviews_completedInterviewsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (MyInterviews_completedInterviewsGridView,
                        (string)ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the expired tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void MyInterviews_expiredInterviewsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (MyInterviews_expiredInterviewsGridView,
                        (string)ViewState["EXPIRED_INTERVIEWS_SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["EXPIRED_INTERVIEWS_SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void MyInterviews_pendingInterviewsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["PENDING_INTERVIEWS_SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["PENDING_INTERVIEWS_SORT_ORDER"] =
                        ((SortType)ViewState["PENDING_INTERVIEWS_SORT_ORDER"]) ==
                            SortType.Ascending ? SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["PENDING_INTERVIEWS_SORT_ORDER"] = SortType.Ascending;

                ViewState["PENDING_INTERVIEWS_SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                MyInterviews_pendingInterviewsTopPagingNavigator.Reset();

                // Show pending interviews for default page number.
                ShowPendingInterviews(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void MyInterviews_completedInterviewsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"] =
                        ((SortType)ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"]) ==
                        SortType.Ascending ? SortType.Descending : SortType.Ascending;
                }
                else
                {
                    ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"] = SortType.Ascending;
                }
                ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                MyInterviews_completedInterviewsTopPagingNavigator.Reset();

                // Show completed interviews.
                ShowCompletedInterviews(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the expired tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void MyInterviews_expiredInterviewsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["EXPIRED_INTERVIEWS_SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["EXPIRED_INTERVIEWS_SORT_ORDER"] =
                        ((SortType)ViewState["EXPIRED_INTERVIEWS_SORT_ORDER"]) ==
                            SortType.Ascending ? SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["EXPIRED_INTERVIEWS_SORT_ORDER"] = SortType.Ascending;

                ViewState["EXPIRED_INTERVIEWS_SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                MyInterviews_expiredInterviewsTopPagingNavigator.Reset();

                // Show expired interviews.
                ShowExpiredInterviews(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void MyInterviews_pendingInterviewsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    string candidateSessionID = (e.Row.FindControl("MyInterviews_pendingInterviewsCandidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((e.Row.FindControl("MyInterviews_pendingInterviewsAttemptIDHiddenField") as HiddenField).Value);
                    bool isPaused = Convert.ToBoolean((e.Row.FindControl("MyInterviews_pendingInterviewIsPausedHiddenField") as HiddenField).Value);

                    if (isPaused == false)
                    {
                        // Assign events to start interview button.
                        ImageButton startInterview = e.Row.FindControl("MyInterviews_pendingInterviewsStartImageButton")
                            as ImageButton;
                        startInterview.Attributes.Add("OnClick", "javascript:return StartInterview('" +
                            candidateSessionID + "','" + attemptID + "')");
                    }
                    else
                    {
                        // Assign events to continue interview button.
                        ImageButton continueInterview = e.Row.FindControl("MyInterviews_pendingInterviewsContinueImageButton")
                            as ImageButton;
                        continueInterview.Attributes.Add("OnClick", "javascript:return ContinueInterview('" +
                            candidateSessionID + "','" + attemptID + "')");
                    }

                    // Assign events to interview reminder popup launcher button.
                    ImageButton testReminder = e.Row.FindControl("MyInterviews_pendingInterviewsReminderImageButton")
                        as ImageButton;
                    testReminder.Attributes.Add("OnClick", "javascript:return OpenTestReminder('INT','" +
                        candidateSessionID + "','" + attemptID + "')");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void MyInterviews_completedInterviewsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton MyInterviews_completedTestsActivateImageButton = (ImageButton)
                    e.Row.FindControl("MyInterviews_completedTestsActivateImageButton");
                ImageButton MyInterviews_certificationImageButton = (ImageButton)
                    e.Row.FindControl("MyInterviews_certificationImageButton");

                string candidateSessionID = (e.Row.FindControl("MyInterviews_completedTestsCandidateSessionIDHiddenField")
                    as HiddenField).Value;
                int attemptID = Convert.ToInt32((e.Row.FindControl("MyInterviews_completedTestsAttemptIDHiddenField")
                    as HiddenField).Value);

                string testKey = (e.Row.FindControl("MyInterviews_completedTestsTestIDHiddenField") as HiddenField).Value;
                string initiatedBy = (e.Row.FindControl("MyInterviews_initiatedByHiddenField") as HiddenField).Value;
                string testName = (e.Row.FindControl("MyInterviews_completedTestsTestNameHiddenField") as HiddenField).Value;
                string testCompletedOn = (e.Row.FindControl("MyInterviews_completedTestsCompletedOnLabel") as Label).Text;

                if (!Utility.IsNullOrEmpty(initiatedBy) && initiatedBy.ToUpper() == "SELF")
                {
                    MyInterviews_completedTestsActivateImageButton.Visible = false;
                    //MyInterviews_certificationImageButton.Visible = false;
                }
                // Assign events to retake request button.

                //ImageButton retakeRequest = e.Row.FindControl("MyInterviews_completedTestsActivateImageButton")
                //    as ImageButton;
                //retakeRequest.Attributes.Add("OnClick", "javascript:return OpenCandidateRequest('" +
                //    candidateSessionID + "','" + attemptID + "','retake')");

                MyInterviews_certificationImageButton.Attributes.Add("OnClick", "javascript:return OpenCertificateWindow('"
                    + candidateSessionID + "','" + testKey + "','"
                    + attemptID + "','" + testCompletedOn + "','CERTIFICATE')");

                //ImageButton retakeRequest = e.Row.FindControl("MyInterviews_completedTestsActivateImageButton")
                //    as ImageButton;

                //if (IsValidRetake(candidateSessionID, testKey, attemptID))
                //    retakeRequest.Attributes.Add("OnClick", "javascript:return OpenCandidateRequest('" +
                //            candidateSessionID + "','" + attemptID + "','retake')");
                //else
                //    retakeRequest.Attributes.Add("OnClick", "javascript:return ShowRetakeWarningMessage()");
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the expired tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void MyInterviews_expiredInterviewsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                // Assign events to activate request button.
                ImageButton MyInterviews_expiredTestsActivateImageButton = e.Row.FindControl("MyInterviews_expiredTestsActivateImageButton")
                    as ImageButton;

                string candidateSessionID = (e.Row.FindControl("MyInterviews_expiredTestsCandidateSessionIDHiddenField") as HiddenField).Value;
                int attemptID = Convert.ToInt32((e.Row.FindControl("MyInterviews_expiredTestsAttemptIDHiddenField") as HiddenField).Value);

                // Check initiated by is 'SELF'. If it is, hide request to activate image button.
                string initiatedBy = (e.Row.FindControl("MyInterviews_expiredTestsInitiatedBydHiddenField") as HiddenField).Value;
                if (!Utility.IsNullOrEmpty(initiatedBy) && initiatedBy.ToUpper() == "SELF")
                    MyInterviews_expiredTestsActivateImageButton.Visible = false;
                else
                {
                    MyInterviews_expiredTestsActivateImageButton.Visible = true;
                    MyInterviews_expiredTestsActivateImageButton.Attributes.Add("OnClick",
                        "javascript:return OpenCandidateRequest('" + candidateSessionID
                        + "','" + attemptID + "','expired')");
                }
            }
        }

        /// <summary>
        /// Handler method that will be called when the refresh button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void MyInterviews_refreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl.Contains("ex") ? 
                    Request.RawUrl.Substring(0, Request.RawUrl.IndexOf("ex") - 1) : Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyInterviews_topErrorMessageLabel,
                    MyInterviews_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start interview confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the interview instructions page.
        /// </remarks>
        protected void MyInterviews_startInterviewButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/InterviewCenter/InterviewInstructions.aspx" +
                "?candidatesessionid=" + MyInterviews_candidateSessionID.Value +
                "&attemptid=" + MyInterviews_attemptID.Value +
                "&parentpage=MY_INT", false);
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the continue interview confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the interview instructions page.
        /// </remarks>
        protected void MyInterviews_continueInterviewButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/InterviewCenter/ContinueInterviewInstructions.aspx" +
                "?candidatesessionid=" + MyInterviews_candidateSessionID.Value +
                "&attemptid=" + MyInterviews_attemptID.Value +
                "&parentpage=MY_INT", false);
        }

        #endregion Events Hadlers                                              

        #region Private Methods                                                

        /// <summary>
        /// This method visibles the appropriate panel based on key.
        /// </summary>
        /// <param name="Key">
        /// A <see cref="string"/> that holds the key for the panel.
        /// 1. Pending interviews
        /// 2. Completed interviews
        /// 3. Expired interviews</param>
        private void SwitchPanels(int key)
        {
            switch (key)
            {
                case 1:
                    MyInterviews_completedInterviewsTR.Style.Add("display", "none");
                    MyInterviews_expiredInterviewsTR.Style.Add("display", "none");
                    MyInterviews_pendingInterviewsUpSpan.Style.Add("display", "block");
                    MyInterviews_pendingInterviewsDownSpan.Style.Add("display", "none");
                    MyInterviews_pendingInterviewsDiv.Style.Add("height", "100%");
                    break;
                //case 2:
                //    MyInterviews_pendingInterviewsTR.Style.Add("display", "none");
                //    MyInterviews_expiredInterviewsTR.Style.Add("display", "none");
                //    MyInterviews_completedInterviewsUpSpan.Style.Add("display", "block");
                //    MyInterviews_completedInterviewsDownSpan.Style.Add("display", "none");
                //    MyInterviews_completedInterviewsDiv.Style.Add("height", "100%");
                //    break;
                //case 3:
                //    MyInterviews_pendingInterviewsTR.Style.Add("display", "none");
                //    MyInterviews_completedInterviewsTR.Style.Add("display", "none");
                //    MyInterviews_expiredInterviewsUpSpan.Style.Add("display", "block");
                //    MyInterviews_expiredInterviewsDownSpan.Style.Add("display", "none");
                //    MyInterviews_expiredInterviewsDiv.Style.Add("height", "100%");
                //    break;
            }
        }

        /// <summary>
        /// Method that subscribes the paging control events.
        /// </summary>
        private void SubscribePagingEvents()
        {
            MyInterviews_pendingInterviewsTopPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                   (MyInterviews_pendingInterviewsPagingNavigator_PageNumberClick);

            MyInterviews_completedInterviewsTopPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                   (MyInterviews_completedInterviewsPagingNavigator_PageNumberClick);

            MyInterviews_expiredInterviewsTopPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                   (MyInterviews_expiredInterviewsPagingNavigator_PageNumberClick);
        }

        /// <summary>
        /// Method that shows the list of pending interview for the given page 
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowPendingInterviews(int pageNumber)
        {
            int totalRecords;

            List<CandidateInterviewDetail> pendingInterview = new CandidateBLManager().
                GetPendingInterviews(base.userID,
                ViewState["PENDING_INTERVIEWS_SORT_FIELD"].ToString(),
                ((SortType)ViewState["PENDING_INTERVIEWS_SORT_ORDER"]),
                pageNumber, base.GridPageSize, out totalRecords);

            MyInterviews_pendingInterviewsGridView.DataSource = pendingInterview;
            MyInterviews_pendingInterviewsGridView.DataBind();

            MyInterviews_pendingInterviewsTopPagingNavigator.PageSize = base.GridPageSize;
            MyInterviews_pendingInterviewsTopPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that shows the list of completed tests for the given page
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowCompletedInterviews(int pageNumber)
        {
            //int totalRecords;

            //List<CandidateInterviewDetail> completedTests = new CandidateBLManager().GetTests
            //    (CandidateTestStatus.Completed, base.userID,
            //    ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"].ToString(),
            //    ((SortType)ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"]),
            //    pageNumber, base.GridPageSize, out totalRecords);

            //MyInterviews_completedInterviewsGridView.DataSource = completedTests;
            //MyInterviews_completedInterviewsGridView.DataBind();

            //MyInterviews_completedInterviewsTopPagingNavigator.PageSize = base.GridPageSize;
            //MyInterviews_completedInterviewsTopPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that shows the list of expired tests for the given page 
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowExpiredInterviews(int pageNumber)
        {
            //int totalRecords;

            //List<CandidateInterviewDetail> expiredTests = new CandidateBLManager().GetTests
            //    (CandidateTestStatus.Expired, base.userID,
            //    ViewState["EXPIRED_INTERVIEWS_SORT_FIELD"].ToString(),
            //    ((SortType)ViewState["EXPIRED_INTERVIEWS_SORT_ORDER"]),
            //    pageNumber, base.GridPageSize, out totalRecords);

            //MyInterviews_expiredInterviewsGridView.DataSource = expiredTests;
            //MyInterviews_expiredInterviewsGridView.DataBind();

            //MyInterviews_expiredInterviewsTopPagingNavigator.PageSize = base.GridPageSize;
            //MyInterviews_expiredInterviewsTopPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that validate requests to retake a interview. 
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session id.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt id.
        /// </param>
        private bool IsValidRetake(string candidateSessionID, string testKey, int attemptID)
        {
            string retakeCount = string.Empty;
            Dictionary<string, string> dicRetakeDetails = new
                Dictionary<string, string>();
            string isCertificateTest = string.Empty;
            bool isValidRetake = true;
            string pendingAttempt = new CandidateBLManager().TestRetakeValidation(candidateSessionID, testKey,
                attemptID, out retakeCount, out isCertificateTest, out dicRetakeDetails);

            if (pendingAttempt != null && Convert.ToInt32(pendingAttempt) > 0)
            {
                MyInterviews_RetakeValidation_ConfirmMsgControl.Message =
                    "Already an attempt is pending for this interview session.<br>Check with your pending tests";
                return isValidRetake = false;
            }
            if (Convert.ToInt32(isCertificateTest) > 0)
            {
                if (retakeCount != null && Convert.ToInt32(retakeCount) <= 0)
                {
                    MyInterviews_RetakeValidation_ConfirmMsgControl.Message =
                        "You have utilized maximum number of attempts.<br>You are not allowed to retake this interview";
                    return isValidRetake = false;
                }

                //if (dicRetakeDetails != null && dicRetakeDetails.Count > 0)
                //{
                //    foreach (KeyValuePair<string, string> retakeDetail in dicRetakeDetails)//Temporary-will be removed//
                //    {
                //        if (Convert.ToInt32(retakeDetail.Key) > 0)
                //        {
                //            MyInterviews_RetakeValidation_ConfirmMsgControl.Message =
                //                "You are allowed to retake your test only after " + retakeDetail.Key + " days from "
                //                + retakeDetail.Value;
                //            return isValidRetake = false;
                //        }
                //    }
                //}
            }

            return isValidRetake;
        }

        #endregion Private Methods                                             

        #region Protected Methods                                              

        /// <summary>
        /// Method that retrieves the qualified status for certification. This
        /// helps to show or hide the view certification link icon in the 
        /// completed test grid section.
        /// </summary>
        /// <param name="certificationStatus">
        /// A <see cref="string"/> that holds the certification status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the certification available status.
        /// True represents available and false not available.
        /// </returns>
        /// <remarks>
        /// The applicable status for certification status are:
        /// 1. 'N/A' if not applicable.
        /// 2. 'Unavailable' if not qualified for certification.
        /// 3. 'Available' if qualified for certification.
        /// </remarks>
        protected bool IsQualified(string certificationStatus)
        {
            return (certificationStatus == "Available" ? true : false);
        }

        /// <summary>
        /// Method that retrieves the interview paused status. This helps to 
        /// show or hide the icon in the grid.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the show results to candidate status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of show results
        /// to candidate status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for show results to candidates are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsShowResults(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        /// <summary>
        /// Method that retrieves the interview paused status. This helps to 
        /// show or hide the icon in the grid.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the interview paused status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of interview 
        /// paused status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for interview paused status are:
        /// 1. True - Paused.
        /// 2. False - Not Paused.
        /// </remarks>
        protected bool IsPaused(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        #endregion Protected Methods                                           

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Method that will load the pending, completed and expired interviews
        /// into the grid.
        /// </summary>
        /// <remarks>
        /// This is a overridden method.
        /// </remarks>
        protected override void LoadValues()
        {
            ShowPendingInterviews(1);
            //ShowCompletedTests(1);
            //ShowExpiredTests(1);
        }

        #endregion Protected Overridden Methods                                
    }
}