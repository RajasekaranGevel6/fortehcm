﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    Title="Interview Introduction" CodeBehind="InterviewIntroduction.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.InterviewIntroduction" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="InterviewIntroduction_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script language="javascript" type="text/javascript">

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div>
                    <div class="msg_align">
                        <asp:Label ID="InterviewIntroduction_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewIntroduction_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div id="InterviewIntroduction_mainDiv" class="test_right_panel">
                    <div class="test_body_bg_div">
                        <div class="test_header_text_bold_div">
                            <asp:Literal ID="InterviewIntroduction_headerLiteral" runat="server" Text="Interview Introduction"></asp:Literal>
                        </div>
                    </div>
                    <div class="test_body_bg_div" style="height: 20px">
                    </div>
                    <div>
                        <div class="test_body_bg_div">
                            <div style="float: left; width: 940px" class="test_subheader_text_bold_div">
                                <asp:Label ID="InterviewIntroduction_interviewAreasLabel" runat="server" Text="Interview Areas"></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div">
                            <div style="float: left; width: 940px" class="test_text_div">
                                <asp:Label ID="InterviewIntroduction_interviewAreasValueLabel" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div" style="height: 10px">
                        </div>
                        <div class="test_body_bg_div">
                            <div style="float: left; width: 940px" class="test_subheader_text_bold_div">
                                <asp:Label ID="InterviewIntroduction_interviewSubjectsLabel" runat="server" Text="Interview Subjects"></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div">
                            <div style="float: left; width: 940px" class="test_text_div">
                                <asp:Label ID="InterviewIntroduction_interviewSubjectsValueLabel" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div" style="height: 10px">
                        </div>
                        <div class="test_body_bg_div">
                            <div class="test_subheader_text_bold_div">
                                <asp:Label ID="InterviewIntroduction_interviewNameLabel" runat="server" Text="Interview Name"></asp:Label>
                            </div>
                            <div class="test_text_div">
                                <asp:Label ID="InterviewIntroduction_interviewNameValueLabel" runat="server" ReadOnly="true"></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div" style="height: 10px">
                        </div>
                        <div class="test_body_bg_div">
                            <div class="test_subheader_text_bold_div">
                                <asp:Label ID="InterviewIntroduction_interviewDescriptionLabel" runat="server" Text="Interview Description"></asp:Label>
                                <div class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                    <asp:Label ID="InterviewIntroduction_interviewDescriptionValueLabel" runat="server"
                                        Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="test_body_bg_div" style="height: 10px">
                            </div>
                            <div class="test_body_bg_div">
                                <div>
                                    <div class="test_subheader_text_bold_div">
                                        <asp:Label ID="InterviewIntroduction_interviewInstructionsLabel" runat="server" Text="Interview Instructions"></asp:Label>
                                    </div>
                                    <div class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                        <asp:Literal ID="InterviewIntroduction_interviewInstructionsLiteral" runat="server"
                                            Text=""></asp:Literal>
                                    </div>
                                </div>
                            </div>
                            <div class="test_body_bg_div">
                                <div style="width: 330px; height: 40px; float: left">
                                </div>
                                <div style="width: 400px; text-align: center; float: left;">
                                    <div style="float: left; width: 110px; text-align: left">
                                        <asp:Button ID="InterviewIntroduction_startButton" runat="server" Text="Start Interview"
                                            OnClick="InterviewIntroduction_startInterviewButton_Click" SkinID="sknButtonId" />
                                    </div>
                                    <div style="float: left; width: 70px; text-align: left">
                                        <asp:LinkButton ID="InterviewIntroduction_remindMelinkButton" runat="server" Text="Remind Me"
                                            OnClick="InterviewIntroduction_remindMeLinkButton_Click" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </div>
                                    <div style="float: left; width: 80px; text-align: left">
                                        <asp:LinkButton ID="InterviewIntroduction_cancelButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
                <div>
                    <div class="msg_align">
                        <asp:Label ID="InterviewIntroduction_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="InterviewIntroduction_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
