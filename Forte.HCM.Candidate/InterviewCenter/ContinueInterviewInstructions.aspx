<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="ContinueInterviewInstructions.aspx.cs" Title="Interview Instructions"
    Inherits="Forte.HCM.UI.CandidateCenter.ContinueInterviewInstructions" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="ContinueInterviewInstructions_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">

        // Method that will show/hide the captcha image help panel.
        function ShowBrowserInstructions(show)
        {
            if (show == 'Y')
                document.getElementById('ContinueInterviewInstructions_browserInstructionsDiv').style.display = "block";
            else
                document.getElementById('ContinueInterviewInstructions_browserInstructionsDiv').style.display = "none";

            return false;
        }

        // Method that will show/hide the captcha image help panel.
        function ShowWhatsThis(show)
        {
            if (show == 'Y')
                document.getElementById('ContinueInterviewInstructions_whatsThisDiv').style.display = "block";
            else
                document.getElementById('ContinueInterviewInstructions_whatsThisDiv').style.display = "none";
            return false;
        }

        function LaunchOfflineInterview(a)
        {
            document.getElementById(a).style.display = "none";
            return false;
        }

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 960px; float: left">
                    <div style="text-align: center">
                        <asp:Label ID="ContinueInterviewInstructions_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ContinueInterviewInstructions_topErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div class="test_right_panel">
                    <div class="test_body_bg_div">
                        <div id="TestInstructions_title" class="test_header_text_bold_div">
                            <asp:Literal ID="ContinueInterviewInstructions_headerLiteral" runat="server" Text="Interview Instructions"></asp:Literal>
                        </div>
                    </div>
                    <div class="test_body_bg_div">
                        <div class="test_body_bg_div">
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="ContinueInterviewInstructions_minimumHardwareLiteral" runat="server"
                                    Text="Minimum Hardware & OS Requirements"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="ContinueInterviewInstructions_minimumHardwareValueLiteral" runat="server"
                                    Text=""></asp:Literal>
                            </div>
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="ContinueInterviewInstructions_interviewInstructionsLiteral" runat="server"
                                    Text="Interview Instructions"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="ContinueInterviewInstructions_interviewInstructionsValueLiteral"
                                    runat="server"></asp:Literal>
                            </div>
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="ContinueInterviewInstructions_interviewDescriptionLiteral" runat="server"
                                    Text="Interview Description"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="ContinueInterviewInstructions_interviewDescriptionValueLiteral"
                                    runat="server" Text=""></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="ContinueInterviewInstructions_warningLiteral" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="test_body_bg_div" id="ContinueInterviewInstructions_securityCodeDiv"
                        runat="server" visible="true">
                        <div style="width: 190px; height: 100px; float: left">
                        </div>
                        <div style="width: 580px; text-align: center; float: left; background-color: #e8e9ec">
                            <div>
                                <div style="float: left; width: 580px" class="click_once_launch_instructions_label">
                                    <asp:Literal ID="ContinueInterviewInstructions_captchaDescriptionLiteral" Text="Click on 'Launch Offline Interview Recorder' to launch the offline interview recorder application and enter the security code shown below into it and click on 'Start Interview' button to start the interview"
                                        runat="server">
                                    </asp:Literal>
                                </div>
                                <div style="float: left; width: 580px; height: 8px">
                                </div>
                                <div style="float: left; width: 580px">
                                    <div style="float: left; width: 300px; text-align: right">
                                        <img id="ContinueInterviewInstructions_capchaImage" src="~/Common/CaptchaViewer.aspx?type=OI"
                                            runat="server" alt="Captcha Image" />
                                    </div>
                                    <div style="float: left; width: 220px; text-align: left; padding-left: 4px">
                                        <asp:LinkButton ID="ContinueInterviewInstructions_regenerateKeyLinkButton" runat="server"
                                            ToolTip="Click here to regenerate the security code" Text="Regenerate Security Code"
                                            OnClick="ContinueInterviewInstructions_regenerateKeyLinkButton_Click" SkinID="sknActionLinkButton" />
                                    </div>
                                    <div style="float: left; width: 220px; height: 20px; text-align: left">
                                    </div>
                                    <div style="float: left; width: 220px; text-align: left; padding-left: 4px">
                                        <asp:LinkButton ID="ContinueInterviewInstructions_whatsThisLinkButton" runat="server"
                                            Text="What's This" ToolTip="Click here to show the help on security code" SkinID="sknActionLinkButton"
                                            OnClientClick="javascript:return ShowWhatsThis('Y')" />
                                        <div>
                                            <div id="ContinueInterviewInstructions_whatsThisDiv" style="display: none; height: 150px;
                                                width: 270px; left: 390px; top: 340px; z-index: 0; position: absolute" class="popupcontrol_confirm">
                                                <div style="width: 268px; float: left">
                                                    <div class="popup_header_text" style="width: 100px; padding-left: 10px; padding-top: 10px;
                                                        float: left">
                                                        <asp:Literal ID="ContinueInterviewInstructions_whatsThisDiv_titleLiteral" runat="server"
                                                            Text="What's This"></asp:Literal>
                                                    </div>
                                                    <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                                                        <asp:ImageButton ID="ContinueInterviewInstructions_whatsThisDiv_topCancelImageButton"
                                                            runat="server" SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowWhatsThis('N')" />
                                                    </div>
                                                </div>
                                                <div style="width: 268px; padding-left: 10px; float: left">
                                                    <div style="width: 248px; float: left; height: 100px" class="interview_instructions_captcha_inner_bg">
                                                        <div style="width: 228px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                                                            <asp:Literal ID="ContinueInterviewInstructions_whatsThisDiv_messageLiteral" runat="server"
                                                                Text="This is the security code for starting the offline interview recording. Enter the security code into the downloaded offline interview recording application and start">
                                                            </asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <asp:HyperLink ID="ContinueInterviewInstructions_launchOfflineInterviewRecorder"
                                    Text="Launch Offline Interview Recorder" runat="server" Visible="true" ToolTip="Click here to launch the offline interview recorder application"></asp:HyperLink>
                            </div>
                            <div runat="server" id="ContinueInterviewInstructions_downloadOfflineInterviewRecorderDocumentTD"
                                visible="true" class="click_once_launch_instructions_label">
                                If you have any issues in launching the offline interview recorder, click
                                <asp:LinkButton ID="ContinueInterviewInstructions_downloadOfflineInterviewRecorderDocumentLinkButton"
                                    runat="server" Text="here" OnClick="ContinueInterviewInstructions_downloadOfflineInterviewRecorderDocumentLinkButton_Click"></asp:LinkButton>&nbsp;to
                                download the trouble-shooting document
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 960px; float: left">
                    <div style="text-align: center">
                        <asp:Label ID="ContinueInterviewInstructions_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="ContinueInterviewInstructions_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ContinueInterviewInstructions_browserInstructionsDiv" style="display: none;
        height: 222px; width: 580px; left: 338px; top: 160px; z-index: 1; position: absolute"
        class="popupcontrol_confirm">
        <div style="width: 578px; float: left">
            <div class="popup_header_text" style="width: 540px; padding-left: 10px; padding-top: 10px;
                float: left">
                <asp:Label ID="ContinueInterviewInstructions_browserInstructionsDiv_titleLabel" runat="server"
                    Text=""></asp:Label>
            </div>
            <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                <asp:ImageButton ID="ContinueInterviewInstructions_browserInstructionsDiv_closeImageButton"
                    runat="server" ToolTip="Click here to close" SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowBrowserInstructions('N')" />
            </div>
        </div>
        <div style="width: 578px; padding-left: 10px; float: left">
            <div style="width: 558px; float: left; height: 160px" class="interview_instructions_captcha_inner_bg">
                <div style="width: 548px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                    <asp:Label ID="ContinueInterviewInstructions_browserInstructionsDiv_headerLabel"
                        runat="server" Text="" SkinID="sknBrowserInstructionsHeader">
                    </asp:Label>
                </div>
                <div style="width: 10px; float: left; padding-left: 4px; padding-right: 4px; height: 60px">
                </div>
                <div style="width: 508px; float: left; padding-left: 4px; padding-right: 4px" class="browser_instructions_list_view">
                    <div id="ContinueInterviewInstructions_browserInstructionsListViewDiv" runat="server"
                        style="height: 96px; overflow: auto;">
                        <asp:ListView ID="ContinueInterviewInstructions_browserInstructionsDiv_instructionsListView"
                            runat="server" SkinID="sknBrowserInstructionsListView">
                            <LayoutTemplate>
                                <ul class="browser_instructions_list_place_holder">
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                </ul>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div style="width: 480px">
                                    <asp:Label ID="ContinueInterviewInstructions_browserInstructionsDiv_instructionsListView_instructionsLabel"
                                        runat="server" SkinID="sknBrowserInstructionsRow" Text='<%# Eval("Instruction") %>' />
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
            <div style="width: 568px; float: left; padding-left: 4px;">
                <asp:LinkButton runat="server" ID="ContinueInterviewInstructions_browserInstructionsDiv_cancelLinkButton"
                    ToolTip="Click here to close" Text="Cancel" OnClientClick="javascript:return ShowBrowserInstructions('N')"
                    SkinID="sknPopupLinkButton"></asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
