﻿using System; 
using System.Web;
using System.Data;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.DataObjects; 

namespace Forte.HCM.Candidate.InterviewCenter
{
    /// <summary>
    /// Summary description for OfflineInterviewData
    /// </summary>
    public class OfflineInterviewData : IHttpHandler
    {

        /*SqlConnection dbConn = new SqlConnection(
          ConfigurationManager.AppSettings["DBConnection"]); */


        DataSet tempDataset = null;
        DataTable tempTable = null;

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string questionKey = null;
                string comments = "";
                string resultType = "";
                string testKey = "";
                string requestType = null;
                string candidateSessionID = "";
                int attemptID = 0;
                int candidateID = 0;

                DateTime startTime = DateTime.Now;
                DateTime endTime = DateTime.Now;

                int interviewTestQuestionID = 0;
                int questionViewed = 0;
                int questionNotViewed = 0;

                bool skipped = false; 

                OfflineInterviewParams interviewParams = null;
                OfflineInterviewBLManager offlineInterviewMgr = null;

                if (context.Request["call"] != null)
                    requestType = context.Request["call"].ToString();

                if (context.Request["candidateSessionID"] != null)
                    candidateSessionID = context.Request["candidateSessionID"].ToString();

                if (context.Request["attemptID"] != null)
                    attemptID = Convert.ToInt32(context.Request["attemptID"]);

                if (context.Request["candidateID"] != null)
                    candidateID = Convert.ToInt32(context.Request["candidateID"]);

                if (context.Request["questionKey"] != null)
                    questionKey = context.Request["questionKey"].ToString();

                if (context.Request["testQuestionID"] != null)
                    interviewTestQuestionID = Convert.ToInt32(context.Request["testQuestionID"]);

                if (context.Request["comments"] != null)
                    comments = Convert.ToString(context.Request["comments"]);

                if (context.Request["skipped"] != null)
                    skipped = Convert.ToBoolean(context.Request["skipped"]);

                if (context.Request["startTime"] != null)
                    startTime = Convert.ToDateTime(context.Request["startTime"]);

                if (context.Request["endTime"] != null)
                    endTime = Convert.ToDateTime(context.Request["endTime"]);

                if (context.Request["resultType"] != null)
                    resultType = Convert.ToString(context.Request["resultType"]);

                if (context.Request["testKey"] != null)
                    testKey = Convert.ToString(context.Request["testKey"]);

                if (context.Request["questionsViewed"] != null)
                    questionViewed = Convert.ToInt32(context.Request["questionsViewed"]);

                if (context.Request["questionsNotViewed"] != null)
                    questionNotViewed = Convert.ToInt32(context.Request["questionsNotViewed"]);

                if (requestType != null && requestType != "")
                {
                    if (offlineInterviewMgr == null)
                        offlineInterviewMgr = new OfflineInterviewBLManager();

                    interviewParams = new OfflineInterviewParams();

                    tempDataset = new DataSet();
                    tempTable = new DataTable(); 
                  
                    switch (requestType)
                    {
                        case "InterviewDetail": 

                            interviewParams.candidateSessionID = candidateSessionID; 
                            interviewParams.attemptID = attemptID;

                            /*Tracing input data from flex component*/
                            DataTrace("InterviewDetail", interviewParams);

                            tempTable = offlineInterviewMgr.GetInterviewDetails(interviewParams);

                            tempTable.TableName = "InterviewDetail";
                            tempTable.AcceptChanges();

                            tempDataset.Merge(tempTable);
                              
                            WriteXML(tempDataset, context);
                            break;

                        case "Questions":  
                            interviewParams.candidateSessionID = candidateSessionID;

                            interviewParams.attemptID = attemptID;

                            /*Tracing input data from flex component*/
                            DataTrace("Questions", interviewParams);

                            tempTable = offlineInterviewMgr.
                                GetQuestions(interviewParams);

                            tempTable.TableName = "Questions";
                            tempTable.AcceptChanges();

                            tempDataset.Merge(tempTable);

                            WriteXML(tempDataset, context);
                            break;

                        case "UpdateInterviewStatus":
                            interviewParams.candidateSessionID = candidateSessionID;
                            interviewParams.attemptID = attemptID;
                            interviewParams.sessionTrackingStatus = "ATMPT_INPR";
                            interviewParams.sessionCandidateStatus = "SESS_INPR";
                            interviewParams.candidateID = candidateID;

                            /*Tracing input data from flex component*/
                            DataTrace("UpdateInterviewStatus", interviewParams);
                            bool interviewStatus = false;

                            if (!OfflineInterview.isPaused)
                            {
                                interviewStatus = offlineInterviewMgr.UpdateInterviewConductionStatus(interviewParams); 
                            }
                            WriteXML(TableArrange(interviewStatus, "UpdateInterviewStatus"), context);
                            break;

                        case "UpdateTestResultFact":

                            interviewParams.candidateSessionID = candidateSessionID;

                            interviewParams.attemptID = attemptID;

                            interviewParams.comments = comments;

                            interviewParams.status = "COMPLETED";

                            interviewParams.startTime = startTime;
                            interviewParams.endTime = endTime;
                            interviewParams.skipped = skipped;
                            interviewParams.candidateID = candidateID;
                            interviewParams.questionKey = questionKey;


                            /*Tracing input data from flex component*/
                            DataTrace("UpdateTestResultFact", interviewParams);

                            bool resultFact =offlineInterviewMgr.
                                UpdateInterviewTestResultFact(interviewParams) ;

                            WriteXML(TableArrange(resultFact, "UpdateTestResultFact"), context);

                            break;

                        case "UpdateInterviewTest":
                            interviewParams.candidateSessionID = candidateSessionID;

                            interviewParams.attemptID = attemptID;
                            interviewParams.statusType = resultType;
                            interviewParams.comments = comments;

                            interviewParams.status = "COMPLETED";
                            interviewParams.testKey = testKey;
                            interviewParams.startTime = startTime;
                            interviewParams.endTime = endTime;
                            interviewParams.skipped = skipped;
                            interviewParams.candidateID = candidateID;
                            interviewParams.questionKey = questionKey;
                            interviewParams.questionsViewed = questionViewed;
                            interviewParams.questionNotViewed = questionNotViewed;
                            /*Tracing input data from flex component*/
                            DataTrace("UpdateInterviewTest", interviewParams);

                            bool testResult=offlineInterviewMgr.UpdateInterviewTest(interviewParams) ;

                            WriteXML(TableArrange(testResult, "UpdateInterviewTest"), context);
                            break;
                        case "Instructions": 

                            /*Tracing input data from flex component*/
                            DataTrace("Instructions", interviewParams);

                            tempTable = offlineInterviewMgr.GetDisclaimerMessages();

                            tempTable.TableName = "Instructions";
                            tempTable.AcceptChanges();

                            tempDataset.Merge(tempTable);

                            WriteXML(tempDataset, context);

                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }

        private DataSet TableArrange(bool flag,string tableName)
        {
            tempDataset = new DataSet();
            tempTable = new DataTable();

            tempTable.Columns.Add("Result");
            DataRow dr = tempTable.NewRow();
            dr["Result"] = flag.ToString();
            tempTable.Rows.Add(dr);
            tempTable.TableName =tableName;
            tempTable.AcceptChanges();

            tempDataset.Merge(tempTable);
            return tempDataset;
        }

        private void WriteXML(DataSet ds, HttpContext context)
        {
            context.Response.Write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            context.Response.Write(ds.GetXml());
        }

        private void WriteXML(bool flag, HttpContext context)
        {
            DataTable dtTemp = new DataTable();
            dtTemp.TableName = "Interview";
            dtTemp.Columns.Add("Result");
            dtTemp.AcceptChanges();
            DataRow dr = dtTemp.NewRow();
            dr["Result"] = flag.ToString();
            dtTemp.Rows.Add(dr);
            dtTemp.AcceptChanges();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtTemp);
            context.Response.Write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            context.Response.Write(ds.GetXml());
        }

        private void DataTrace(string type,OfflineInterviewParams param)
        {
            switch (type)
            {
                case "InterviewDetail":
                    Logger.TraceLog("InterviewDetail - candidate session id = " 
                        + param.candidateSessionID 
                        + " Attemptid=" + param.attemptID );  
                    break;

                case "Questions":
                    Logger.TraceLog("Questions - candidate session id = "
                        + param.candidateSessionID 
                        + " Attemptid=" + param.attemptID);
                    break;

                case "UpdateInterviewStatus":  
                    Logger.TraceLog("UpdateInterviewStatus - candidate session id = "
                        + param.candidateSessionID + " Attemptid=" 
                        + param.attemptID + " Session Traxking Status ="
                        + param.sessionTrackingStatus + " Session Candidate Status ="
                        + param.sessionCandidateStatus+ " Candidate id "
                        + param.candidateID.ToString());
                    break;

                case "UpdateTestResultFact": 
                    Logger.TraceLog("UpdateTestResultFact - candidate session id = "
                            + param.candidateSessionID + " Attemptid=" 
                            + param.attemptID + " Comments ="
                            + param.comments + " Status =" 
                            + param.status + " Start time :" 
                            + param.startTime.ToString() + " End time : " 
                            + param.endTime.ToString() + " Skipped : "
                            + param.skipped.ToString() + " Candidate id " 
                            + param.candidateID.ToString() + "Question Key : "
                            + param.questionKey);
                    break; 
                case "UpdateInterviewTest": 
                    Logger.TraceLog("UpdateInterviewTest - candidate session id = "
                            + param.candidateSessionID + " Attemptid="
                            + param.attemptID + " Comments ="
                            + param.comments + " Status ="
                            + param.status + "Test key : "  
                            +  param.testKey +  " Start time :"
                            + param.startTime.ToString() + " End time : "
                            + param.endTime.ToString() + " Skipped : "
                            + param.skipped.ToString() + " Candidate id "
                            + param.candidateID.ToString() + "Question Key : "
                            + param.questionKey+ " Questions viewed : "
                            + param.questionsViewed.ToString() + " Questions not viewed : "  
                            + param.questionNotViewed.ToString() );
                    break;
                case "Instructions":
                    Logger.TraceLog("Called instructions function");  
                    break;
                default :
                    break; 
            }

        }
        private void GetDBData(int Qid,HttpContext context)
        {
           /* Logger.TraceLog("QID=" + Qid.ToString(),"FORHCMTEST");
            DataSet ds = new DataSet();
            string sql = null;

            sql = "Select * from (Select Row_Number() Over (order by question_id) as RowNo, question_id as QID,Question_desc as QName from INTERVIEW_TEST_QUESTION inner join Interview_question on "+
                  " INTERVIEW_TEST_QUESTION.Question_Key=Interview_question.Question_Key where Interview_test_key='TST_00000001') tempTable " +
                   " where RowNo= " + Qid;
            var da = new SqlDataAdapter(sql, dbConn);
            da.Fill(ds, "Question");

            Logger.TraceLog("RecordCount=" + ds.Tables[0].Rows.Count.ToString(), "ROWSCOUNT");

            context.Response.Write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            context.Response.Write(ds.GetXml());*/
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}