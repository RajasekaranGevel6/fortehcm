﻿using System;
using System.IO;
using System.Web;
using System.Configuration;

using Forte.HCM.Trace;


namespace Forte.HCM.Candidate.InterviewCenter
{
    /// <summary>
    /// Summary description for OnlineInterviewData
    /// </summary>
    public class OnlineInterviewData : IHttpHandler
    {
        string requestType = null;
        string folderPath = null;
        string sessionId = null;
        string chatRoomId = null;
        string chatText = null;

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (context.Request["call"] != null)
                {
                    requestType = context.Request["call"].ToString();
                    sessionId = context.Request["sessionId"].ToString();
                    chatRoomId = context.Request["chatRoomId"].ToString();
                    chatText = context.Request["chatText"].ToString();

                    SelectingTextChatType(requestType, sessionId, chatRoomId, chatText);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        private void SelectingTextChatType(string paramType, string sessionid, string chatRoomId, string chatText)
        { 
            switch (paramType.ToLower())
            {
                case "chattext":
                    WriteTextChat(sessionId, chatRoomId, chatText);
                    break;
                default:
                    break;
            }            
        }

        private void WriteTextChat(string sessionId, string chatRoomId, string chatText)
        {
            FileStream fs = null;
            StreamWriter m_streamWriter = null;
            try
            {
                folderPath = ConfigurationManager.AppSettings["ONLINE_INTERVIEWS_PATH"].ToString();
                folderPath = folderPath + "\\" + sessionId + "\\" + chatRoomId + "\\";

                if (!File.Exists(folderPath + "chat.txt"))
                    File.Create(folderPath + "chat.txt");

                fs = new FileStream(folderPath + "chat.txt", FileMode.OpenOrCreate, FileAccess.Write);
                m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(chatText);
                m_streamWriter.Flush();
            }
            catch
            {
            }
            finally
            {
                m_streamWriter.Close();
                fs.Close();
                m_streamWriter = null;
                fs = null;
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}