﻿using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;
using Forte.HCM.SmptClientService;

namespace Forte.HCM.Candidate.InterviewCenter
{
    public partial class OfflineInterviewCompleted :PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set browser title.
            Master.SetPageCaption("Interview Completion Summary");

            if (Request.QueryString["status"] != null)
            {
                if (Request.QueryString["candidateSessionID"] != null && Request.QueryString["attemptID"] != null)
                {
                    string candidateSessionID = Convert.ToString(Request.QueryString["candidateSessionID"]);
                    int attemptID = Convert.ToInt32(Request.QueryString["attemptID"]);

                    string entityType = "0";

                    CandidateInterviewSessionDetail sessionDetail = new OfflineInterviewBLManager().GetCandidateInterviewSessionDetail
                                        (candidateSessionID, attemptID);

                    string status = Convert.ToString(Request.QueryString["status"]);
                    switch (status)
                    {
                        case "TransmitCompleted":
                            entityType = "31";
                            break;
                        case "AbandonInterview":
                            entityType = "32";
                            break;

                        case "Completed":
                            entityType = "33";
                            break;

                        case "Paused":
                            entityType = "34";
                            break;
                        default:
                            break;
                    }

                    // Convert entity type string to object.
                    EntityType entity = (EntityType)Enum.Parse(typeof(EntityType), entityType, true);

                    // Get Candidate interview publish url
                    if (!Utility.IsNullOrEmpty(candidateSessionID))
                    {
                        bool isExist = true;
                        string interviewPublishCode = string.Empty;
                        InterviewScoreParamDetail interviewScoreCodeDetail = null;
                        while (isExist)
                        {
                            interviewPublishCode = Utility.RandomString(9);
                            interviewScoreCodeDetail = new AssessmentSummaryBLManager().
                                UpdateCandidateInterviewScoreCode(candidateSessionID, attemptID, interviewPublishCode);
                            if (interviewScoreCodeDetail != null)
                            {
                                isExist = interviewScoreCodeDetail.IsCodeExist;
                                interviewPublishCode = interviewScoreCodeDetail.ScoreCode;
                            }
                        }
                        sessionDetail.InterviewPublishCode = interviewPublishCode;
                    }
                    
                    // Send the email.
                    if(entity==EntityType.Completed)
                        new SmptEmailHandler().OfflineInterviewSendMail(entity, sessionDetail);
                    else
                        new EmailHandler().OfflineInterviewSendMail(entity, sessionDetail);
                    
                }

                InterviewConductionCompleted_statusMessageLabel.Text = Request.QueryString["status"].ToString();
            }

        }

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
    }
}