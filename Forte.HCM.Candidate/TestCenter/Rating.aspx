﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Rating.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.Rating" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="TestConductionCompleted_content" ContentPlaceHolderID="MainContent"
    runat="server">
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 980px; float: left; padding-left: 10px">
                    <div style="float: left; width: 970px">
                        <asp:Label ID="Rating_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage" Visible="false"></asp:Label>
                    </div>
                    <div style="float: left; width: 970px">
                        <asp:Label ID="Rating_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage" Visible="false"></asp:Label>
                    </div>
                     <div style="float: left; width: 970px">
                        <div style="width: 970px; float: left">
                            <asp:Label runat="server" ID="Rating_statusLabel" SkinID="sknLabelTestCompletionTitle"
                                Text="Status"></asp:Label>
                        </div>
                        <%--<div style="width: 800px; float: left">
                            <asp:Label runat="server" ID="Rating_statusMessageLabel" SkinID="sknLabelTestCompletion"
                                Text=""> </asp:Label>
                        </div>--%>
                    </div>
                    <div style="float: left; width: 970px">
                        <div style="width: 140px; float: left" class="cand_rating_feedback_icon">
                        </div>
                        <div style="width: 700px; float: left; padding-top: 30px"  >
                            <div style="width: 120px; padding-left: 10px; padding-top: 10px">
                                <asp:Label ID="Rating_ratingLabel" runat="server" Text="Rating" CssClass="rating_label"></asp:Label>
                            </div>
                             <div style="width: 300px; padding-left: 10px; padding-top: 10px">
                             <ajaxToolKit:Rating ID="ratingControl" AutoPostBack="true" runat="server"
                              StarCssClass="Star" WaitingStarCssClass="WaitingStar" EmptyStarCssClass="Star"
                                 FilledStarCssClass="FilledStar">
                            </ajaxToolKit:Rating>
                             </div>
                             <div style="height: 20px; width: 120px; padding-left: 10px; padding-top: 10px" >
                            </div>
                            <div style="width: 120px; padding-left: 10px; padding-top: 10px">
                                <asp:Label ID="Rating_FeedbackLabel" runat="server" Text="Feedback" CssClass="rating_label"></asp:Label>
                            </div>
                            <div style="width: 800px; padding-left: 10px; padding-top: 10px">
                                <asp:TextBox ID="Rating_FeedbackTextBox" runat="server" Columns="300" TextMode="MultiLine"
                                    Height="100" Width="600" MaxLength="250" onkeyup="CommentsCount(250,this)" onchange="CommentsCount(250,this)"></asp:TextBox>
                            </div>
                             <div style="width: 204px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Button ID="Rating_submitButton" SkinID="sknButtonId" runat="server" Text="Submit"
                                    Width="60px" OnClick="Rating_submitButton_Click" />
                                <asp:Button ID="Rating_skipButton" SkinID="sknButtonBlue" runat="server" Text="Skip"
                                    Width="60px" OnClick="Rating_skipButton_Click" />
                            </div>
                        </div>
                    </div>
                    <div style="float: left; width: 970px; height: 8px">
                    </div>
                    <div style="float: left; width: 970px" class="test_conduction_completed_message_link_message">
                        Click here to go to
                        <asp:LinkButton ID="TestConductionCompleted_homeLinkButton" runat="server" Text="Home"
                            CssClass="test_conduction_completed_message_link_btn" ToolTip="Click here to go to home page"
                            PostBackUrl="~/Dashboard.aspx?parentpage=C_DASHBOARD"> </asp:LinkButton>
                        &nbsp;page
                    </div>
                    <div style="float: left; width: 970px; height: 8px">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
