<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="TestInstructions.aspx.cs" Title="Test Instructions" Inherits="Forte.HCM.UI.CandidateCenter.TestInstructions" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="TestInstructions_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
<meta charset="utf-8">
    <meta name="description" content="WebRTC code samples">
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, maximum-scale=1">
    <meta itemprop="description" content="Client-side WebRTC code samples">
    <meta itemprop="image" content="../../../images/webrtc-icon-192x192.png">
    <meta itemprop="name" content="WebRTC code samples">
    <meta name="mobile-web-app-capable" content="yes">
  <%--  <meta id="theme-color" name="theme-color" content="#ffffff">--%>
    <base target="_blank">
   
    <link rel="icon" sizes="192x192" href="../../../images/webrtc-icon-192x192.png">
    <link href="../Styles/cb-fonts-googleapis.css" rel="stylesheet"
        type="text/css">
  <%--  <link rel="stylesheet" href="../../../css/main.css">--%>
    <script src="../JS/jquery-1.11.0.min.js"></script>
    <script src="../JS/WebRTC-WebcamScreenCapture-latest.js"></script>
    <style type="text/css">
        #TestInstructions_Webcam_video, #TestInstructions_Webcam_canvas, #TestIntsructions_screen_canvas
        {
            display: none;
        }
        
        #TestIntsructions_screen_video
        {
            visibility:hidden;
        }
    </style>
    <script type = "text/javascript" >
    $(document).ready(function () {
            'use strict';
//            try {
          function getUrlQueryString(name) {
                    var query = window.location.search.substring(1);
                    var parms = query.split('&');
                    for (var i = 0; i < parms.length; i++) {              
                        var pos = parms[i].indexOf('=');               
                        if (pos > 0) {
                            var key = parms[i].substring(0, pos);
                            var val = parms[i].substring(pos + 1);
                            if (key === name) {                        
                                return val;
                            }
                        }
                    }
                }
                 $('#TestInstructions_videoCanvasDiv').hide();
           // var canSessionKey=getUrlQueryString('candidatesessionid');
           // var attemptID=getUrlQueryString('attemptid');

            var canSessionKey='<%=Session["TEST_INSTRUCTIONS_CAND_SESSION_KEY"] %>';
            var attemptID='<%=Session["TEST_INSTRUCTIONS_ATTEMPT_ID"] %>';

            if(!navigator.getDisplayMedia && !navigator.mediaDevices.getDisplayMedia) {
                var error = 'Your browser does NOT supports getDisplayMedia API.';          
                throw new Error(error);
            }
           var upload_url = '../Common/TestPerformer_ImageHandler.ashx';
           var Count=0; var TestPerformerWinodw=null;

            //Webcam capture
             const TestInstructions_webcam_video = document.querySelector('#TestInstructions_Webcam_video');
            
             const TestInstructions_Webcam_canvas = window.canvas = document.querySelector('#TestInstructions_Webcam_canvas');
              TestInstructions_Webcam_canvas.width = 480;
                TestInstructions_Webcam_canvas.height = 360;

                //Display capture
             const TestIntsructions_screen_video = document.querySelector('#TestIntsructions_screen_video');
           
             const TestIntsructions_screen_canvas = window.canvas = document.querySelector('#TestIntsructions_screen_canvas');

             TestIntsructions_screen_canvas.width = 600;
                TestIntsructions_screen_canvas.height = 500;

                const constraints = {
                  video: true
                };

                const displayMediaStreamConstraints = {
                    video: {
                    width: 480,
                    height: 360,
                    displaySurface: 'monitor', // monitor or window or application or browser
                    logicalSurface: true,
                    frameRate: 30,
                    aspectRatio: 1.77,
                    cursor: 'always' // always or never or motion
                    }
            }
            var popUpURL="../TestConduction/TestPerformer.aspx?csk="+canSessionKey +"&atmpt="+ attemptID;
            var popUpFeature="width="+screen.width+",height="+screen.height+",scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no";
             $('#<%=TestInstructions_proceedButton1.ClientID%>').click(function ()               
               { 
              
               var chkTermsndConditions=$("#<%= TestInstructions_agreeCheckbox.ClientID %>");                 
                   if(chkTermsndConditions[0].checked)
                   {
                        $('#TestInstructions_instructions').hide();
                       $('#TestInstructions_mainDiv').hide();
                       $('#TestInstructions_videoCanvasDiv').show();

                       if(('<%= Session["IS_CYBER_PROCTORING"] %>')==='True')    
                       {  
                      
                       //$('#TestIntsructions_screen_video').css("display", "block");

                                $("#TestInstructions_proceedButton1").attr("disabled", true);              
                                CheckForCyberProctoring();
                         }
                        else
                        {
                           // $('#TestInstructions_buttonDiv').hide();
                            //$("#TestInstructions_proceedButton1").attr("disabled", true);
                             TestPerformerWinodw= window.open(popUpURL,window.self,popUpFeature);                              
                             MonitorTestPerformerWindow();
                         }                                                     
                   }
                   else
                   {
                        alert("You must agreed to the terms and conditions to proceed the test");
                   }
               });
 
                var interval = null;
               $('#<%=TestInstructions_startTestButton1.ClientID%>').click(function ()               
               {
               
                    var chkTermsndConditions=$("#<%= TestInstructions_agreeCheckbox.ClientID %>");
                   
                   if(chkTermsndConditions[0].checked)
                   {
                   
                      if(('<%= Session["IS_CYBER_PROCTORING"] %>')==='True')     
                       {  
                      // $('#TestIntsructions_screen_video').css("display", "block");
//                           var enumeratorPromise = navigator.mediaDevices.enumerateDevices();
//                            if(enumeratorPromise)
//                            {
                                $("#TestInstructions_startTestButton1").attr("disabled", true);              
                                CheckForCyberProctoring();
//                            }else
//                            alert('your device doesn"t support camera');
                         }
                        else
                        {
                            $("#TestInstructions_startTestButton1").attr("disabled", true);
                             TestPerformerWinodw=window.open(popUpURL,window.self,popUpFeature);// window.open("../TestConduction/TestPerformer.aspx?csk="+canSessionKey +"&atmpt="+ attemptID,"TestPerformer","width="+screen.width+",height="+screen.height+",scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no");                              
                             MonitorTestPerformerWindow();
                         }
                             
                        
                   }
                   else
                   {
                        alert("You must agreed to the terms and conditions to proceed the test");
                   }
               });             

               function CheckForCyberProctoring()
                {                
                    Count=0;
                    CheckForMediaDevice(function(){
                    if(Count==1){
                            CheckForDisplayDevice(function(){
                                if(Count>=2){                                  
                               TestPerformerWinodw=window.open(popUpURL,window.self,popUpFeature);// window.open("../TestConduction/TestPerformer.aspx?csk="+canSessionKey +"&atmpt="+ attemptID,"TestPerformer","width="+screen.width+",height="+screen.height+",scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no");                              
                               MonitorTestPerformerWindow();
                                interval=setInterval(function () {
                                    DisplayCapture();
                                    WemCamCapture();
                                   
                                },10000);
                                //return true;
                                }
                            });
                        }
                    });
                    //return false;                                               
                }

//                $('#btnStop').click(function() {
//                    clearVideoTracks();
//                });

                function clearVideoTracks()
                {
                //if (window.stream) {
                    clearInterval(interval);
                        //video.srcObject.getTracks().forEach(function(track) {
                          //track.stop();

                    const webcam_Media_tracks = TestInstructions_webcam_video.srcObject.getTracks();

                     webcam_Media_tracks.forEach(track => track.stop());
                    TestInstructions_webcam_video.srcObject = null;

                    const screen_Display_tracks = TestIntsructions_screen_video.srcObject.getTracks();

                     screen_Display_tracks.forEach(track => track.stop());
                    TestIntsructions_screen_video.srcObject = null;
                }

               async function CheckForMediaDevice(callback)
                {
                try{
                    var stream=null;
                    if(navigator.mediaDevices.getUserMedia) {
                           stream= await navigator.mediaDevices.getUserMedia(constraints);//.then(handleUserMediaSuccess).catch(handleUserMediaError);
                     }
                     else {
                          stream= await navigator.getUserMedia(constraints);
                          //.then(handleUserMediaSuccess).catch(handleUserMediaError);
                     }
                     if(stream!=null)
                     {
                         window.stream = stream; // make stream available to browser console
                          TestInstructions_webcam_video.srcObject = stream;
                          Count++;
                           callback();
                        }                    
                     }
                     catch(Error)
                     {
                     //alert(Error);
                         if(Error.message==='Permission denied'){
                            alert("Permissions have not been granted to use your camera. you need to allow the page access to your devices in order for the demo to work.");
                            checkforMediaWithIntervals(callback);
                          }
                          else{ 
                                alert('CAMERA1 '+Error);
                          } 
                     }
                }

               async  function CheckForDisplayDevice(callback)
                {
                    try
                    {
                    var stream=null;
                     if(navigator.mediaDevices.getDisplayMedia) {
                         stream= await navigator.mediaDevices.getDisplayMedia(displayMediaStreamConstraints);//.then(handleDisplayCaptureSuccess).catch(handleDisplayCaptureError);
                      }
                     else {
                            stream= await navigator.getDisplayMedia(displayMediaStreamConstraints);
                            //.then(handleDisplayCaptureSuccess).catch(handleDisplayCaptureError);
                        }
                        if(stream!=null)
                        {                       
                            const videoTrack = stream.getVideoTracks()[0];
                           var settings=videoTrack.getSettings();
                           if(videoTrack.getConstraints() && videoTrack.getConstraints().mediaSource==="window")
                           {
                                window.stream = stream;
                                TestIntsructions_screen_video.srcObject = stream;                                     
                                Count++;
                           }
                           else if(settings.displaySurface==='monitor'){
                                window.stream = stream;
                                TestIntsructions_screen_video.srcObject = stream;                                     
                                Count++;
                           }    
                           else{
                                alert('Share your entire screen in order to take test!');
                                CheckForDisplayDevice(callback);
                           }
                           handleDisplayCaptureSuccess(stream);
                           callback();
                       }                         
                    }
                    catch (error)
                    {
                        //handleDisplayCaptureError(error);
                        if(error.name==='NotAllowedError')
                        {
                            alert("Unable to capture your screen. Screen share has been declined. you must share your screen in order to proceed the test.");
                            CheckForDisplayDevice(callback);
                        }
                        else
                         alert('Error: '+error);
                    }
                }

                async function checkforMediaWithIntervals(callback)
                {
                    UserMediaInterval=setInterval(function()
                    {
                           
                           var stream=null;
                            if(navigator.mediaDevices.getUserMedia) {
                                   stream=  navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
                                   clearInterval(UserMediaInterval);
                                     window.stream = stream; // make stream available to browser console
                                      TestInstructions_webcam_video.srcObject = stream;
                                      Count++;
                                       callback();
                                   }).catch(function(error)
                                   {
                                   
                                   });
                             }
                             else {
                                  stream=  navigator.getUserMedia(constraints);
                                  //.then(handleUserMediaSuccess).catch(handleUserMediaError);
                             }                          
                    },1000);
                }

                function handleUserMediaSuccess(stream) {
                      window.stream = stream; // make stream available to browser console
                      TestInstructions_webcam_video.srcObject = stream;
                      Count++;
                }

                 function handleUserMediaError(error) {
                      //console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
                      if(error.message==='Permission denied'){
                        alert("Permissions have not been granted to use your camera. you must to allow the page access to your devices in order to proceed the test.");
                      }
                      else{ 
                        alert('Error: '+error);
                      };                      
                }

                function handleDisplayCaptureError(error) {
                        if(error.name === 'PermissionDeniedError') {
                            if(location.protocol !== 'https:') {
                                error.message = 'Please use HTTPs.';
                                error.stack   = 'HTTPs is required.';
                            }
                        }
                     // alert('Unable to capture your screen.\n\n' + error.name + '\n\n' + error.message + '\n\n' + error.stack);
                      if(error.name==='NotAllowedError')
                      {
                        alert("Unable to capture your screen. Screen share has been declined. you must to share your screen in order to proceed the test.");
                      }
                }

                function handleDisplayCaptureSuccess(stream) {
                  //window.stream = stream; // make stream available to browser console
                  //video.srcObject = stream;
                  addStreamStopListener(stream, function() {
                   
                    TestPerformerWinodw.close();
                    clearVideoTracks();
                    alert('Screen Share has ended!!');  
                    //location.reload();             
                    var RedirectURL="../DashBoard.aspx"; 
                      window.location.replace(RedirectURL);
                    });                      
               }

       function addStreamStopListener(stream, callback) {
            stream.addEventListener('ended', function() {
                callback();
                callback = function() {};
            }, false);
            stream.addEventListener('inactive', function() {
                callback();
                callback = function() {};
            }, false);
            stream.getTracks().forEach(function(track) {
                track.addEventListener('ended', function() {
                    callback();
                    callback = function() {};
                }, false);
                track.addEventListener('inactive', function() {
                    callback();
                    callback = function() {};
                }, false);
            });
        }
        
                    function MonitorTestPerformerWindow()
                    {
                        if(TestPerformerWinodw!=null)
                             {
                                var timer = setInterval(function() {   
                                if(TestPerformerWinodw.closed) {
                                 
                                if(('<%= Session["IS_CYBER_PROCTORING"] %>')==='True')    
                                   {  
                                        clearVideoTracks();
                                   }  
                                    clearInterval(timer);                                     
                                   var RedirectURL='<%=Session["Redirect_URL"]%>'; 
                                   //alert(RedirectURL);                 
                                   if(RedirectURL==="") 
                                        RedirectURL="../DashBoard.aspx"; 
                                                        
                                    window.location.replace(RedirectURL);
                                }  
                            }, 1000);
                            }
                    }

                function WemCamCapture()
                {                    
                    TestInstructions_Webcam_canvas.width = TestInstructions_webcam_video.videoWidth;
                    TestInstructions_Webcam_canvas.height = TestInstructions_webcam_video.videoHeight;
                    TestInstructions_Webcam_canvas.getContext('2d').drawImage(TestInstructions_webcam_video, 0, 0, TestInstructions_Webcam_canvas.width, TestInstructions_Webcam_canvas.height);
                    var ImageURL=TestInstructions_Webcam_canvas.toDataURL("image/jpeg");
                    var block = ImageURL.split(";");
                    // Get the content type
                    var contentType = block[0].split(":")[1];// In this case "image/jpeg"
                    // get the real base64 content of the file
                    var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."

                    // Convert to blob
                    var blob = b64toBlob(realData, contentType);

                    // Create a FormData and append the file
                    var formData = new FormData();
                    formData.append("image", blob);

                    var ImageUploadURL=upload_url+"?type=Webcam&csk="+canSessionKey+"&atmpt="+attemptID;
                    $.ajax({
                            url: ImageUploadURL, 
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'POST',
                            success: function (response) {
                            //alert(response);                               
                            }
                        });
                }

                function DisplayCapture()
                {
                    TestIntsructions_screen_canvas.width = TestIntsructions_screen_video.videoWidth;
                    TestIntsructions_screen_canvas.height = TestIntsructions_screen_video.videoHeight;
                    TestIntsructions_screen_canvas.getContext('2d').drawImage(TestIntsructions_screen_video, 0, 0, TestIntsructions_screen_canvas.width, TestIntsructions_screen_canvas.height);
                    var ImageURL=TestIntsructions_screen_canvas.toDataURL("image/jpeg");
                    var block = ImageURL.split(";");
                    // Get the content type
                    var contentType = block[0].split(":")[1];// In this case "image/jpeg"
                    // get the real base64 content of the file
                    var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."

                    // Convert to blob
                    var blob = b64toBlob(realData, contentType);

                    // Create a FormData and append the file
                    var formData = new FormData();
                    formData.append("image", blob);

                    //var upload_url = 'RTC_ScreenCapture.aspx';
                    var ImageUploadURL=upload_url+"?type=Screen&csk="+canSessionKey+"&atmpt="+attemptID;
                    $.ajax({
                            url: ImageUploadURL, // replace with your own server URL
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'POST',
                            success: function (response) {
                                if (response === 'success') {
                                    //alert('successfully uploaded recorded blob');                                     
                                } else {
                                    //alert(response); // error/failure
                                }
                            }
                        });
                }

                // convert captured base64 string to blob
                function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

              var blob = new Blob(byteArrays, {type: contentType});
              return blob;
    }
//    } catch (e) {
//    alert('123 '+e);
//    
//    }
 });
</script>
    <script type="text/javascript" language="javascript">

        // Method that will show/hide the captcha image help panel.
        function ShowBrowserInstructions(show)
        {
            if (show == 'Y')
                document.getElementById('TestInstructions_browserInstructionsDiv').style.display = "block";
            else
                document.getElementById('TestInstructions_browserInstructionsDiv').style.display = "none";

            return false;
        }

        // Method that will show/hide the captcha image help panel.
        function ShowWhatsThis(show)
        {
            if (show == 'Y')
                document.getElementById('TestInstructions_whatsThisDiv').style.display = "block";
            else
                document.getElementById('TestInstructions_whatsThisDiv').style.display = "none";

            return false;
        }

        function LaunchCyberProtering(a, b)
        {
            document.getElementById(a).style.display = "none";
            document.getElementById(b).style.display = "block";

            return false;
        }

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div id="TestInstructions_errorControls">
                    <div style="text-align: center">
                        <asp:Label ID="TestInstructions_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestInstructions_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div id="TestInstructions_mainDiv" class="test_right_panel">
                    <div class="test_body_bg_div">
                        <div id="TestInstructions_cancel" class="test_resume_header_bg_right">
                            <asp:LinkButton ID="TestInstructions_topCancelButton" runat="server" Text="" OnClick="ParentPageRedirect"
                                SkinID="sknDivActionLinkButton"></asp:LinkButton>
                        </div>
                    </div>
                    <div id="TestInstructions_instructions">
                        <div>
                            <div id="TestInstructions_hardwareTitle" class="test_subheader_text_bold_div">
                                <asp:Literal ID="TestInstructions_minimumHardwareLiteral" runat="server" Text="Minimum Hardware & OS Requirements"></asp:Literal>
                            </div>
                            <div id="TestInstructions_hardwareInstruction" class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Literal ID="TestInstructions_minimumHardwareValueLiteral" runat="server" Text="&lt To Be Filled &gt"></asp:Literal>
                            </div>
                            <div id="TestInstructions_testInstructionTitle" class="test_subheader_text_bold_div">
                                <asp:Literal ID="TestInstructions_testInstructionsLiteral" runat="server" Text="Test Instructions"></asp:Literal>
                            </div>
                            <div id="TestInstructions_testInstructionDesc" class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Literal ID="TestInstructions_testInstructionsValueLiteral" runat="server"></asp:Literal>
                            </div>
                            <!-- CyberProctoring Information -->
                            <div id="TestInstructions_cyberProctoringGuidelinesRow" runat="server">
                                <div id="TestInstructions_cyberProctoringGuidelinesTitle" class="test_subheader_text_bold_div">
                                    <asp:Literal ID="TestInstructions_cyberProctoringGuidelinesLiteral" runat="server"
                                        Text="Cyber Proctoring Guidelines"></asp:Literal>
                                </div>
                                <div id="TestInstructions_cyberProctoringGuidelinesDesc" class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                    <asp:Literal ID="TestInstructions_cyberProctoringGuidelinesValueLiteral" runat="server"
                                        Text="&lt To Be Filled &gt"></asp:Literal>
                                </div>
                            </div>
                            <!-- End CyberProctoring Information -->
                            <!-- Test Information-->
                            <div id="TestInstructions_testDescTitle" class="test_subheader_text_bold_div">
                                <asp:Literal ID="TestInstructions_testDescriptionLiteral" runat="server" Text="Test Description"></asp:Literal>
                            </div>
                            <div id="TestInstructions_testDesc" class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Literal ID="TestInstructions_testDescriptionValueLiteral" runat="server" Text=""></asp:Literal>
                            </div>
                            <!-- End Test Information-->
                            <!-- Warning Message -->
                            <div class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Literal ID="TestInstructions_warningLiteral" runat="server"></asp:Literal>
                            </div>
                            <!-- End Warning Message -->
                            <!-- Terms and conditions -->
                            <div class="test_body_bg_div" id="TestInstructions_buttonDiv">
                                <div style="width: 190px; height: 40px; float: left">
                                </div>
                                <div style="width: 580px; text-align: center; float: left;">
                                    <div class="test_text_div">
                                        <asp:CheckBox ID="TestInstructions_agreeCheckbox" runat="server" Text=" I agree with the terms and conditions" />
                                    </div>
                                    <div style="width: 570px; display:block;" id="TestInstructions_proceedButtonDiv" runat="server">
                                        <div class="test_resume_header_bg_left" style="width: 280px; text-align: right">
                                            <%--<asp:Button ID="TestInstructions_proceedButton" runat="server" Text="Proceed" SkinID="sknButtonId" ClientIDMode="Static"
                                                ToolTip="Click here to take the test" OnClick="TestInstructions_proceedButton_Click" />--%>
                                                <input type="button" value="Proceed" runat="server" id="TestInstructions_proceedButton1" />
                                        </div>
                                        <div class="test_resume_header_bg_right" style="width: 280px; text-align: left">
                                            <asp:LinkButton runat="server" ID="TestInstructions_cancelLinkButton" ToolTip="Click here to go back to the parent"
                                                Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknDivActionLinkButton"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="TestInstructions_startTestButtonDiv" runat="server" style="width: 570px; display:none;">
                                       <%-- <asp:Button ID="TestInstructions_startTestButton" runat="server" Text="Start Test"
                                                SkinID="sknButtonId" Width="100px" ClientIDMode="Static"   />--%>
                                                <input type="button" value="Proceed" runat="server" id="TestInstructions_startTestButton1" />
                                    </div>
                                </div>
                            </div>
                            <div class="test_body_bg_div" id="TestInstructions_generateKeyDiv" runat="server"
                                style="display: none">
                                <div style="width: 190px; height: 100px; float: left">
                                </div>
                                <div style="width: 580px; text-align: center; float: left; background-color: #e8e9ec">
                                    <div>
                                        <div style="float: left; width: 580px" class="click_once_launch_instructions_label">
                                            <asp:Literal ID="TestInstructions_captchaDescriptionLiteral" 
                                                Text="Click on 'Launch Cyber Proctoring' to launch the cyber proctoring application and enter the security code shown below into it and click on 'Start Test' button to start the test"
                                                runat="server">
                                            </asp:Literal>
                                        </div>
                                        <div style="float: left; width: 580px; height: 8px">
                                        </div>
                                        <div style="float: left; width: 580px">
                                            <div style="float: left; width: 300px; text-align: right">
                                                <img id="TestInstructions_capchaImage" src="~/Common/CaptchaViewer.aspx?type=CP"
                                                    runat="server" alt="Captcha Image" />
                                                    <asp:Label ID="lblCaptchaText" runat="server"></asp:Label>
                                            </div>
                                            <div style="float: left; width: 220px; text-align: left; padding-left: 4px">
                                                <asp:LinkButton ID="TestInstructions_regenerateKeyLinkButton" runat="server" Text="Regenerate Key"
                                                    OnClick="TestInstructions_regenerateKeyLinkButton_Click" SkinID="sknActionLinkButton" />
                                            </div>
                                            <div style="float: left; width: 220px; height: 20px; text-align: left">
                                            </div>
                                            <div style="float: left; width: 220px; text-align: left; padding-left: 4px">
                                                <asp:LinkButton ID="TestInstructions_whatsThisLinkButton" runat="server" Text="What's This"
                                                    SkinID="sknActionLinkButton" OnClientClick="javascript:return ShowWhatsThis('Y')"
                                                    ToolTip="Click here to show the help on security code" />
                                                <div>
                                                    <div id="TestInstructions_whatsThisDiv" style="display: none; height: 150px; width: 270px;
                                                        left: 390px; top: 340px; z-index: 0; position: absolute" class="popupcontrol_confirm">
                                                        <div style="width: 268px; float: left">
                                                            <div class="popup_header_text" style="width: 100px; padding-left: 10px; padding-top: 10px;
                                                                float: left">
                                                                <asp:Literal ID="TestInstructions_whatsThisDiv_titleLiteral" runat="server" Text="What's This"></asp:Literal>
                                                            </div>
                                                            <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                                                                <asp:ImageButton ID="TestInstructions_whatsThisDiv_topCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowWhatsThis('N')"
                                                                    ToolTip="Click here to close the window" />
                                                            </div>
                                                        </div>
                                                        <div style="width: 268px; padding-left: 10px; float: left">
                                                            <div style="width: 248px; float: left; height: 100px" class="interview_instructions_captcha_inner_bg">
                                                                <div style="width: 228px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                                                                    <asp:Literal ID="TestInstructions_whatsThisDiv_messageLiteral" runat="server" Text="This is the security code for activating cyber proctoring. Enter the authentication code in the cyber proctoring application and start">
                                                                    </asp:Literal>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <asp:HyperLink ID="TestInstructions_launchCyperProcterLinkButton" Text="Launch Cyber Proctoring"
                                            runat="server" Visible="false" ToolTip="Click here to launch the cyber proctoring application" SkinID="sknLaunchCyberProctoringHyperLink">
                                           </asp:HyperLink>
                                    </div>
                                    <div runat="server" id="TestInstructions_downloadCyberProctorDocumentTD" visible="false" class="click_once_launch_instructions_label">
                                            If you have any issues in launching the cyber proctoring, click <asp:LinkButton ID="TestInstructions_downloadCyberProctorDocumentLinkButton" runat="server"
                                                Text="here" ToolTip="Click here to launch the cyber proctoring application" OnClick="TestInstructions_downloadCyberProctorDocumentLinkButton_Click">
                                            </asp:LinkButton>&nbsp;to download the trouble-shooting document. If you are still facing issues, click <asp:LinkButton ID="TestInstructions_downloadCyberProctorExecutable" runat="server"
                                                Text="here" ToolTip="Click here to download the cyber proctoring application" OnClick="TestInstructions_downloadCyberProctorExecutable_Click"></asp:LinkButton>&nbsp;to download the cyber protoring application, unzip and run the executable manually.
                                    </div>
                                </div>
                            </div>
                            <!-- End terms and conditions -->
                        </div>
                    </div>
                    <!-- Bottom Error Message -->
                    <!-- End Bottom Error Message -->
                    <div style="display: none">
                        <div class="resume_header_bg_right">
                            <asp:LinkButton ID="TestInstructions_bottomCancelButton" runat="server" Text="Cancel"
                                OnClick="ParentPageRedirect" SkinID="sknActionLinkButton">
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
            </div>
            <div id="TestInstructions_videoCanvasDiv" class="container_hiding_video_div">
            
                                <video id="TestInstructions_Webcam_video" playsinline autoplay></video>
                                <canvas id="TestInstructions_Webcam_canvas"></canvas>
                                <video id="TestIntsructions_screen_video" autoplay playsinline muted="false" volume="0"></video>
                               
                                <canvas id="TestIntsructions_screen_canvas"></canvas>
                                 
                                <div class="hiding_video_div">
                                <p>Do not close this browser tab until the test is completed</p>
                                </div>
                            </div>
            <div id="TestInstructions_bottomErrorMessage">
                 <div style="text-align: center">
                <asp:Label ID="TestInstructions_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="TestInstructions_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                </div>
            </div>
        </div>
        <div id="TestInstructions_browserInstructionsDiv" style="display: none; height: 222px;
            width: 580px; left: 520px; top: 260px; z-index: 1; position: absolute" class="popupcontrol_confirm">
            <div style="width: 578px; float: left">
                <div class="popup_header_text" style="width: 540px; padding-left: 10px; padding-top: 10px;
                    float: left">
                    <asp:Label ID="TestInstructions_browserInstructionsDiv_titleLabel" runat="server"
                        Text=""></asp:Label>
                </div>
                <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                    <asp:ImageButton ID="TestInstructions_browserInstructionsDiv_closeImageButton" runat="server"
                        ToolTip="Click here to close" SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowBrowserInstructions('N')" />
                </div>
            </div>
            <div style="width: 578px; padding-left: 10px; float: left">
                <div style="width: 558px; float: left; height: 160px" class="interview_instructions_captcha_inner_bg">
                    <div style="width: 548px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                        <asp:Label ID="TestInstructions_browserInstructionsDiv_headerLabel" runat="server"
                            Text="" SkinID="sknBrowserInstructionsHeader">
                        </asp:Label>
                    </div>
                    <div style="width: 10px; float: left; padding-left: 4px; padding-right: 4px; height: 60px">
                    </div>
                    <div style="width: 508px; float: left; padding-left: 4px; padding-right: 4px" class="browser_instructions_list_view">
                        <div id="TestInstructions_browserInstructionsListViewDiv" runat="server" style="height: 96px;
                            overflow: auto;">
                            <asp:ListView ID="TestInstructions_browserInstructionsDiv_instructionsListView" runat="server"
                                SkinID="sknBrowserInstructionsListView">
                                <LayoutTemplate>
                                    <ul class="browser_instructions_list_place_holder">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                    </ul>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <div style="width: 480px">
                                        <asp:Label ID="TestInstructions_browserInstructionsDiv_instructionsListView_instructionsLabel"
                                            runat="server" SkinID="sknBrowserInstructionsRow" Text='<%# Eval("Instruction") %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                <div style="width: 568px; float: left; padding-left: 4px;">
                    <asp:LinkButton runat="server" ID="TestInstructions_browserInstructionsDiv_cancelLinkButton"
                        ToolTip="Click here to close" Text="Cancel" OnClientClick="javascript:return ShowBrowserInstructions('N')"
                        SkinID="sknPopupLinkButton"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
