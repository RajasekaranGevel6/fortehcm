<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="TestInstructions.aspx.cs" Title="Test Instructions" Inherits="Forte.HCM.UI.CandidateCenter.TestInstructions" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="TestInstructions_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript" language="javascript">

        // Method that will show/hide the captcha image help panel.
        function ShowBrowserInstructions(show)
        {
            if (show == 'Y')
                document.getElementById('TestInstructions_browserInstructionsDiv').style.display = "block";
            else
                document.getElementById('TestInstructions_browserInstructionsDiv').style.display = "none";

            return false;
        }

        // Method that will show/hide the captcha image help panel.
        function ShowWhatsThis(show)
        {
            if (show == 'Y')
                document.getElementById('TestInstructions_whatsThisDiv').style.display = "block";
            else
                document.getElementById('TestInstructions_whatsThisDiv').style.display = "none";

            return false;
        }

        function LaunchCyberProtering(a, b)
        {
            document.getElementById(a).style.display = "none";
            document.getElementById(b).style.display = "block";

            return false;
        }

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div id="TestInstructions_errorControls">
                    <div style="text-align: center">
                        <asp:Label ID="TestInstructions_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestInstructions_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div id="TestInstructions_mainDiv" class="test_right_panel">
                    <div class="test_body_bg_div">
                        <div id="TestInstructions_cancel" class="test_resume_header_bg_right">
                            <asp:LinkButton ID="TestInstructions_topCancelButton" runat="server" Text="" OnClick="ParentPageRedirect"
                                SkinID="sknDivActionLinkButton"></asp:LinkButton>
                        </div>
                    </div>
                    <div id="TestInstructions_instructions">
                        <div>
                            <div id="TestInstructions_hardwareTitle" class="test_subheader_text_bold_div">
                                <asp:Literal ID="TestInstructions_minimumHardwareLiteral" runat="server" Text="Minimum Hardware & OS Requirements"></asp:Literal>
                            </div>
                            <div id="TestInstructions_hardwareInstruction" class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Literal ID="TestInstructions_minimumHardwareValueLiteral" runat="server" Text="&lt To Be Filled &gt"></asp:Literal>
                            </div>
                            <div id="TestInstructions_testInstructionTitle" class="test_subheader_text_bold_div">
                                <asp:Literal ID="TestInstructions_testInstructionsLiteral" runat="server" Text="Test Instructions"></asp:Literal>
                            </div>
                            <div id="TestInstructions_testInstructionDesc" class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Literal ID="TestInstructions_testInstructionsValueLiteral" runat="server"></asp:Literal>
                            </div>
                            <!-- CyberProctoring Information -->
                            <div id="TestInstructions_cyberProctoringGuidelinesRow" runat="server">
                                <div id="TestInstructions_cyberProctoringGuidelinesTitle" class="test_subheader_text_bold_div">
                                    <asp:Literal ID="TestInstructions_cyberProctoringGuidelinesLiteral" runat="server"
                                        Text="Cyber Proctoring Guidelines"></asp:Literal>
                                </div>
                                <div id="TestInstructions_cyberProctoringGuidelinesDesc" class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                    <asp:Literal ID="TestInstructions_cyberProctoringGuidelinesValueLiteral" runat="server"
                                        Text="&lt To Be Filled &gt"></asp:Literal>
                                </div>
                            </div>
                            <!-- End CyberProctoring Information -->
                            <!-- Test Information-->
                            <div id="TestInstructions_testDescTitle" class="test_subheader_text_bold_div">
                                <asp:Literal ID="TestInstructions_testDescriptionLiteral" runat="server" Text="Test Description"></asp:Literal>
                            </div>
                            <div id="TestInstructions_testDesc" class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Literal ID="TestInstructions_testDescriptionValueLiteral" runat="server" Text=""></asp:Literal>
                            </div>
                            <!-- End Test Information-->
                            <!-- Warning Message -->
                            <div class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Literal ID="TestInstructions_warningLiteral" runat="server"></asp:Literal>
                            </div>
                            <!-- End Warning Message -->
                            <!-- Terms and conditions -->
                            <div class="test_body_bg_div">
                                <div style="width: 190px; height: 40px; float: left">
                                </div>
                                <div style="width: 580px; text-align: center; float: left;">
                                    <div class="test_text_div">
                                        <asp:CheckBox ID="TestInstructions_agreeCheckbox" runat="server" Text=" I agree with the terms and conditions" />
                                    </div>
                                    <div style="width: 570px; display:block;" id="TestInstructions_proceedButtonDiv" runat="server">
                                        <div class="test_resume_header_bg_left" style="width: 280px; text-align: right">
                                            <asp:Button ID="TestInstructions_proceedButton" runat="server" Text="Proceed" SkinID="sknButtonId"
                                                ToolTip="Click here to take the test" OnClick="TestInstructions_proceedButton_Click" />
                                        </div>
                                        <div class="test_resume_header_bg_right" style="width: 280px; text-align: left">
                                            <asp:LinkButton runat="server" ID="TestInstructions_cancelLinkButton" ToolTip="Click here to go back to the parent"
                                                Text="Cancel" OnClick="ParentPageRedirect" SkinID="sknDivActionLinkButton"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="TestInstructions_startTestButtonDiv" runat="server" style="width: 570px; display:none;">
                                        <asp:Button ID="TestInstructions_startTestButton" runat="server" Text="Start Test"
                                                SkinID="sknButtonId" Width="100px" OnClick="TestInstructions_startTestButton_Click" />
                                    </div>
                                </div>
                            </div>
                            <div class="test_body_bg_div" id="TestInstructions_generateKeyDiv" runat="server"
                                style="display: none">
                                <div style="width: 190px; height: 100px; float: left">
                                </div>
                                <div style="width: 580px; text-align: center; float: left; background-color: #e8e9ec">
                                    <div>
                                        <div style="float: left; width: 580px" class="click_once_launch_instructions_label">
                                            <asp:Literal ID="TestInstructions_captchaDescriptionLiteral" 
                                                Text="Click on 'Launch Cyber Proctoring' to launch the cyber proctoring application and enter the security code shown below into it and click on 'Start Test' button to start the test"
                                                runat="server">
                                            </asp:Literal>
                                        </div>
                                        <div style="float: left; width: 580px; height: 8px">
                                        </div>
                                        <div style="float: left; width: 580px">
                                            <div style="float: left; width: 300px; text-align: right">
                                                <img id="TestInstructions_capchaImage" src="~/Common/CaptchaViewer.aspx?type=CP"
                                                    runat="server" alt="Captcha Image" />
                                                    <asp:Label ID="lblCaptchaText" runat="server"></asp:Label>
                                            </div>
                                            <div style="float: left; width: 220px; text-align: left; padding-left: 4px">
                                                <asp:LinkButton ID="TestInstructions_regenerateKeyLinkButton" runat="server" Text="Regenerate Key"
                                                    OnClick="TestInstructions_regenerateKeyLinkButton_Click" SkinID="sknActionLinkButton" />
                                            </div>
                                            <div style="float: left; width: 220px; height: 20px; text-align: left">
                                            </div>
                                            <div style="float: left; width: 220px; text-align: left; padding-left: 4px">
                                                <asp:LinkButton ID="TestInstructions_whatsThisLinkButton" runat="server" Text="What's This"
                                                    SkinID="sknActionLinkButton" OnClientClick="javascript:return ShowWhatsThis('Y')"
                                                    ToolTip="Click here to show the help on security code" />
                                                <div>
                                                    <div id="TestInstructions_whatsThisDiv" style="display: none; height: 150px; width: 270px;
                                                        left: 390px; top: 340px; z-index: 0; position: absolute" class="popupcontrol_confirm">
                                                        <div style="width: 268px; float: left">
                                                            <div class="popup_header_text" style="width: 100px; padding-left: 10px; padding-top: 10px;
                                                                float: left">
                                                                <asp:Literal ID="TestInstructions_whatsThisDiv_titleLiteral" runat="server" Text="What's This"></asp:Literal>
                                                            </div>
                                                            <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                                                                <asp:ImageButton ID="TestInstructions_whatsThisDiv_topCancelImageButton" runat="server"
                                                                    SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowWhatsThis('N')"
                                                                    ToolTip="Click here to close the window" />
                                                            </div>
                                                        </div>
                                                        <div style="width: 268px; padding-left: 10px; float: left">
                                                            <div style="width: 248px; float: left; height: 100px" class="interview_instructions_captcha_inner_bg">
                                                                <div style="width: 228px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                                                                    <asp:Literal ID="TestInstructions_whatsThisDiv_messageLiteral" runat="server" Text="This is the security code for activating cyber proctoring. Enter the authentication code in the cyber proctoring application and start">
                                                                    </asp:Literal>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <asp:HyperLink ID="TestInstructions_launchCyperProcterLinkButton" Text="Launch Cyber Proctoring"
                                            runat="server" Visible="false" ToolTip="Click here to launch the cyber proctoring application" SkinID="sknLaunchCyberProctoringHyperLink">
                                           </asp:HyperLink>
                                    </div>
                                    <div runat="server" id="TestInstructions_downloadCyberProctorDocumentTD" visible="false" class="click_once_launch_instructions_label">
                                            If you have any issues in launching the cyber proctoring, click <asp:LinkButton ID="TestInstructions_downloadCyberProctorDocumentLinkButton" runat="server"
                                                Text="here" ToolTip="Click here to launch the cyber proctoring application" OnClick="TestInstructions_downloadCyberProctorDocumentLinkButton_Click">
                                            </asp:LinkButton>&nbsp;to download the trouble-shooting document. If you are still facing issues, click <asp:LinkButton ID="TestInstructions_downloadCyberProctorExecutable" runat="server"
                                                Text="here" ToolTip="Click here to download the cyber proctoring application" OnClick="TestInstructions_downloadCyberProctorExecutable_Click"></asp:LinkButton>&nbsp;to download the cyber protoring application, unzip and run the executable manually.
                                    </div>
                                </div>
                            </div>
                            <!-- End terms and conditions -->
                        </div>
                    </div>
                    <!-- Bottom Error Message -->
                    <!-- End Bottom Error Message -->
                    <div style="display: none">
                        <div class="resume_header_bg_right">
                            <asp:LinkButton ID="TestInstructions_bottomCancelButton" runat="server" Text="Cancel"
                                OnClick="ParentPageRedirect" SkinID="sknActionLinkButton">
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
            </div>
            <div id="TestInstructions_bottomErrorMessage">
                 <div style="text-align: center">
                <asp:Label ID="TestInstructions_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="TestInstructions_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                </div>
            </div>
        </div>
        <div id="TestInstructions_browserInstructionsDiv" style="display: none; height: 222px;
            width: 580px; left: 520px; top: 260px; z-index: 1; position: absolute" class="popupcontrol_confirm">
            <div style="width: 578px; float: left">
                <div class="popup_header_text" style="width: 540px; padding-left: 10px; padding-top: 10px;
                    float: left">
                    <asp:Label ID="TestInstructions_browserInstructionsDiv_titleLabel" runat="server"
                        Text=""></asp:Label>
                </div>
                <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                    <asp:ImageButton ID="TestInstructions_browserInstructionsDiv_closeImageButton" runat="server"
                        ToolTip="Click here to close" SkinID="sknCloseImageButton" OnClientClick="javascript:return ShowBrowserInstructions('N')" />
                </div>
            </div>
            <div style="width: 578px; padding-left: 10px; float: left">
                <div style="width: 558px; float: left; height: 160px" class="interview_instructions_captcha_inner_bg">
                    <div style="width: 548px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                        <asp:Label ID="TestInstructions_browserInstructionsDiv_headerLabel" runat="server"
                            Text="" SkinID="sknBrowserInstructionsHeader">
                        </asp:Label>
                    </div>
                    <div style="width: 10px; float: left; padding-left: 4px; padding-right: 4px; height: 60px">
                    </div>
                    <div style="width: 508px; float: left; padding-left: 4px; padding-right: 4px" class="browser_instructions_list_view">
                        <div id="TestInstructions_browserInstructionsListViewDiv" runat="server" style="height: 96px;
                            overflow: auto;">
                            <asp:ListView ID="TestInstructions_browserInstructionsDiv_instructionsListView" runat="server"
                                SkinID="sknBrowserInstructionsListView">
                                <LayoutTemplate>
                                    <ul class="browser_instructions_list_place_holder">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                    </ul>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <div style="width: 480px">
                                        <asp:Label ID="TestInstructions_browserInstructionsDiv_instructionsListView_instructionsLabel"
                                            runat="server" SkinID="sknBrowserInstructionsRow" Text='<%# Eval("Instruction") %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                <div style="width: 568px; float: left; padding-left: 4px;">
                    <asp:LinkButton runat="server" ID="TestInstructions_browserInstructionsDiv_cancelLinkButton"
                        ToolTip="Click here to close" Text="Cancel" OnClientClick="javascript:return ShowBrowserInstructions('N')"
                        SkinID="sknPopupLinkButton"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
