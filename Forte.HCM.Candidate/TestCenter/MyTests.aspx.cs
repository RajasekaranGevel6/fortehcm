﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MyTests.aspx.cs
// File that represents the MyTests class that defines the user interface
// layout and functionalities for the MyTests page. This page helps in viewing
// the pending, completed and expired test details. This class inherits 
// Forte.HCM.UI.Common.PageBase class.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the MyTests page. This page helps in viewing the pending, completed
    /// and expired test details. This also provided links for starting a test,
    /// viewing results, setting reminder, view test introduction etc. This 
    /// class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class MyTests : PageBase
    {
        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Redirect to dashboard page.
                Response.Redirect("~/Dashboard.aspx", false);

                // Do not allow limited user.
                if (base.isLimited)
                    Response.Redirect("~/ActivityHome.aspx", false);

                // Subscribe to paging control events.
                SubscribePagingEvents();

                // Set page title
                Master.SetPageCaption(Resources.HCMResource.MyTests_Title);

                // Show/hide menu based on user type (limited or not).
                Master.ShowMenu = !base.isLimited;

                if (!IsPostBack)
                {
                    // Add handlers for expand and collapse buttons in pending,
                    // completed and expired tests section.
                    MyTests_pendingTestsHeaderTR.Attributes.Add("onclick", "MultipleExpandorCompress('" +
                        MyTests_pendingTestsTR.ClientID + "','" +
                        MyTests_pendingTestsDiv.ClientID + "','" + MyTests_pendingTestsUpSpan.ClientID + "','" +
                        MyTests_pendingTestsDownSpan.ClientID + "','" + MyTests_pendingTestPlusDiv.ClientID + "')");
                    MyTests_completedTestsHeaderTR.Attributes.Add("onclick", "MultipleExpandorCompress('" +
                        MyTests_completedTestsTR.ClientID + "','" +
                        MyTests_completedTestsDiv.ClientID + "','" + MyTests_completedTestsUpSpan.ClientID + "','" +
                        MyTests_completedTestsDownSpan.ClientID + "','" + MyTests_completedTestsPlusDiv.ClientID + "')");
                    MyTests_expiredTestsHeaderTR.Attributes.Add("onclick", "MultipleExpandorCompress('" +
                        MyTests_expiredTestsTR.ClientID + "','" +
                        MyTests_expiredTestsDiv.ClientID + "','" + MyTests_expiredTestsUpSpan.ClientID + "','" +
                        MyTests_expiredTestsDownSpan.ClientID + "','" + MyTests_expiredTestsPlusDiv.ClientID + "')");
                    // Set message, title and type for the confirmation control
                    // for start test.
                    MyTests_StartTestPopupExtenderControl.Message = "Are you ready to start the test?";
                    MyTests_StartTestPopupExtenderControl.Title = "Start Test";
                    MyTests_StartTestPopupExtenderControl.Type = MessageBoxType.YesNo;

                    // Assign default sort field and order for the pending, 
                    // completed and expired tests section.
                    if (Utility.IsNullOrEmpty(ViewState["PENDING_TESTS_SORT_ORDER"]))
                        ViewState["PENDING_TESTS_SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["PENDING_TESTS_SORT_FIELD"]))
                        ViewState["PENDING_TESTS_SORT_FIELD"] = "TESTNAME";

                    if (Utility.IsNullOrEmpty(ViewState["COMPLETED_TESTS_SORT_ORDER"]))
                        ViewState["COMPLETED_TESTS_SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["COMPLETED_TESTS_SORT_FIELD"]))
                        ViewState["COMPLETED_TESTS_SORT_FIELD"] = "TESTNAME";

                    if (Utility.IsNullOrEmpty(ViewState["EXPIRED_TESTS_SORT_ORDER"]))
                        ViewState["EXPIRED_TESTS_SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["EXPIRED_TESTS_SORT_FIELD"]))
                        ViewState["EXPIRED_TESTS_SORT_FIELD"] = "TESTNAME";
                    // Load pending, completed and expired tests. 
                    LoadValues();
                    if (Request.QueryString["ex"] == null)
                        return;
                    int Key = 0;
                    int.TryParse(Request.QueryString["ex"], out Key);
                    if (Key == 0 || Key > 3)
                        return;
                    SwithTests(Key);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of pending tests section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void MyTests_pendingTestsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowPendingTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of completed tests section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void MyTests_completedTestsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowCompletedTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of expired tests section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void MyTests_expiredTestsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowExpiredTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void MyTests_pendingTestsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TestIntroduction")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (MyTests_pendingTestsGridView.Rows
                        [index].FindControl("MyTests_pendingTestsCandidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((MyTests_pendingTestsGridView.Rows
                        [index].FindControl("MyTests_pendingTestsAttemptIDHiddenField") as HiddenField).Value);

                    Response.Redirect("~/TestCenter/TestIntroduction.aspx" +
                        "?candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=MY_TST", false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void MyTests_completedTestsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TestResults")
                {
                    int index = Convert.ToInt32(e.CommandArgument);

                    string candidateSessionID = (MyTests_completedTestsGridView.Rows
                        [index].FindControl("MyTests_completedTestsCandidateSessionIDHiddenField") as HiddenField).Value;

                    int attemptID = Convert.ToInt32((MyTests_completedTestsGridView.Rows
                        [index].FindControl("MyTests_completedTestsAttemptIDHiddenField") as HiddenField).Value);

                    string testID = (MyTests_completedTestsGridView.Rows
                       [index].FindControl("MyTests_completedTestsTestIDHiddenField") as HiddenField).Value;

                    Response.Redirect("~/TestCenter/CandidateTestResult.aspx" +
                        "?candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=MY_TST" +
                        "&mode=SHARE" +
                        "&testkey=" + testID, false);
                }
                else if (e.CommandName == "viewcertificate")
                {
                    int rowIndex = Convert.ToInt32(e.CommandArgument);

                    // Find imagebutton in the gridview.
                    ImageButton viewCertificateImageButton = (MyTests_completedTestsGridView.Rows
                        [rowIndex].FindControl("MyTests_certificationImageButton") as ImageButton);

                    string candidateSessionID = (MyTests_completedTestsGridView.Rows
                        [rowIndex].FindControl("MyTests_completedTestsCandidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((MyTests_completedTestsGridView.Rows
                        [rowIndex].FindControl("MyTests_completedTestsAttemptIDHiddenField") as HiddenField).Value);
                    string testKey = (MyTests_completedTestsGridView.Rows
                       [rowIndex].FindControl("MyTests_completedTestsTestIDHiddenField") as HiddenField).Value;
                    string testName = (MyTests_completedTestsGridView.Rows
                       [rowIndex].FindControl("MyTests_completedTestsTestNameHiddenField") as HiddenField).Value;
                    string testCompletedOn = (MyTests_completedTestsGridView.Rows
                       [rowIndex].FindControl("MyTests_completedTestsCompletedOnHiddenField") as HiddenField).Value;

                    viewCertificateImageButton.Attributes.Add("OnClick", "javascript:return OpenCertificateWindow('"
                        + candidateSessionID + "','" + testKey + "','"
                        + attemptID + "','" + testCompletedOn + "','CERTIFICATE')");
                }
                else if (e.CommandName == "RequestToRetake")
                {
                    string candidateSessionID = ((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("MyTests_completedTestsCandidateSessionIDHiddenField")).Value;

                    string testID = ((HiddenField)((ImageButton)e.CommandSource).
                        FindControl("MyTests_completedTestsTestIDHiddenField")).Value;

                    int attemptID = int.Parse(((HiddenField)((ImageButton)e.CommandSource).
                         FindControl("MyTests_completedTestsAttemptIDHiddenField")).Value);

                    if (IsValidRetake(candidateSessionID, testID, attemptID))
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "RetakeTest",
                         "javascript: OpenCandidateRequest('"
                         + candidateSessionID + "','"
                         + attemptID + "','retake')", true);
                    else
                        MyTests_RetakeValidation_ModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                MyTests_RetakeValidationUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void MyTests_pendingTestsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (MyTests_pendingTestsGridView,
                        (string)ViewState["PENDING_TESTS_SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["PENDING_TESTS_SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void MyTests_completedTestsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (MyTests_completedTestsGridView,
                        (string)ViewState["COMPLETED_TESTS_SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["COMPLETED_TESTS_SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the expired tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void MyTests_expiredTestsGridView_RowCreated
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (MyTests_expiredTestsGridView,
                        (string)ViewState["EXPIRED_TESTS_SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["EXPIRED_TESTS_SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void MyTests_pendingTestsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["PENDING_TESTS_SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["PENDING_TESTS_SORT_ORDER"] =
                        ((SortType)ViewState["PENDING_TESTS_SORT_ORDER"]) ==
                            SortType.Ascending ? SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["PENDING_TESTS_SORT_ORDER"] = SortType.Ascending;

                ViewState["PENDING_TESTS_SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                MyTests_pendingTestsTopPagingNavigator.Reset();
                ShowPendingTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void MyTests_completedTestsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["COMPLETED_TESTS_SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["COMPLETED_TESTS_SORT_ORDER"] =
                        ((SortType)ViewState["COMPLETED_TESTS_SORT_ORDER"]) ==
                        SortType.Ascending ? SortType.Descending : SortType.Ascending;
                }
                else
                {
                    ViewState["COMPLETED_TESTS_SORT_ORDER"] = SortType.Ascending;
                }
                ViewState["COMPLETED_TESTS_SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                MyTests_completedTestsTopPagingNavigator.Reset();
                ShowCompletedTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the expired tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void MyTests_expiredTestsGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["EXPIRED_TESTS_SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["EXPIRED_TESTS_SORT_ORDER"] =
                        ((SortType)ViewState["EXPIRED_TESTS_SORT_ORDER"]) ==
                            SortType.Ascending ? SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["EXPIRED_TESTS_SORT_ORDER"] = SortType.Ascending;

                ViewState["EXPIRED_TESTS_SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                MyTests_expiredTestsTopPagingNavigator.Reset();
                ShowExpiredTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void MyTests_pendingTestsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    string candidateSessionID = (e.Row.FindControl("MyTests_pendingTestsCandidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((e.Row.FindControl("MyTests_pendingTestsAttemptIDHiddenField") as HiddenField).Value);

                    // Assign events to start test button.
                    ImageButton startTest = e.Row.FindControl("MyTests_pendingTestStartImageButton")
                        as ImageButton;
                    startTest.Attributes.Add("OnClick", "javascript:return StartTest('" +
                        candidateSessionID + "','" + attemptID + "')");

                    // Assign events to test reminder popup launcher button.
                    ImageButton testReminder = e.Row.FindControl("MyTests_pendingTestReminderImageButton")
                        as ImageButton;
                    testReminder.Attributes.Add("OnClick", "javascript:return OpenTestReminder('TST','" +
                        candidateSessionID + "','" + attemptID + "')");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void MyTests_completedTestsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                ImageButton MyTests_completedTestsActivateImageButton = (ImageButton)
                    e.Row.FindControl("MyTests_completedTestsActivateImageButton");
                ImageButton MyTests_certificationImageButton = (ImageButton)
                    e.Row.FindControl("MyTests_certificationImageButton");

                string candidateSessionID = (e.Row.FindControl("MyTests_completedTestsCandidateSessionIDHiddenField")
                    as HiddenField).Value;
                int attemptID = Convert.ToInt32((e.Row.FindControl("MyTests_completedTestsAttemptIDHiddenField")
                    as HiddenField).Value);

                string testKey = (e.Row.FindControl("MyTests_completedTestsTestIDHiddenField") as HiddenField).Value;
                string initiatedBy = (e.Row.FindControl("MyTests_initiatedByHiddenField") as HiddenField).Value;
                string testName = (e.Row.FindControl("MyTests_completedTestsTestNameHiddenField") as HiddenField).Value;
                string testCompletedOn = (e.Row.FindControl("MyTests_completedTestsCompletedOnLabel") as Label).Text;

                if (!Utility.IsNullOrEmpty(initiatedBy) && initiatedBy.ToUpper() == "SELF")
                {
                    MyTests_completedTestsActivateImageButton.Visible = false;
                    //MyTests_certificationImageButton.Visible = false;
                }
                // Assign events to retake request button.

                //ImageButton retakeRequest = e.Row.FindControl("MyTests_completedTestsActivateImageButton")
                //    as ImageButton;
                //retakeRequest.Attributes.Add("OnClick", "javascript:return OpenCandidateRequest('" +
                //    candidateSessionID + "','" + attemptID + "','retake')");

                MyTests_certificationImageButton.Attributes.Add("OnClick", "javascript:return OpenCertificateWindow('"
                    + candidateSessionID + "','" + testKey + "','"
                    + attemptID + "','" + testCompletedOn + "','CERTIFICATE')");

                //ImageButton retakeRequest = e.Row.FindControl("MyTests_completedTestsActivateImageButton")
                //    as ImageButton;

                //if (IsValidRetake(candidateSessionID, testKey, attemptID))
                //    retakeRequest.Attributes.Add("OnClick", "javascript:return OpenCandidateRequest('" +
                //            candidateSessionID + "','" + attemptID + "','retake')");
                //else
                //    retakeRequest.Attributes.Add("OnClick", "javascript:return ShowRetakeWarningMessage()");
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the expired tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void MyTests_expiredTestsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Set row styles.
                e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                // Assign events to activate request button.
                ImageButton MyTests_expiredTestsActivateImageButton = e.Row.FindControl("MyTests_expiredTestsActivateImageButton")
                    as ImageButton;

                string candidateSessionID = (e.Row.FindControl("MyTests_expiredTestsCandidateSessionIDHiddenField") as HiddenField).Value;
                int attemptID = Convert.ToInt32((e.Row.FindControl("MyTests_expiredTestsAttemptIDHiddenField") as HiddenField).Value);

                // Check initiated by is 'SELF'. If it is, hide request to activate image button.
                string initiatedBy = (e.Row.FindControl("MyTests_expiredTestsInitiatedBydHiddenField") as HiddenField).Value;
                if (!Utility.IsNullOrEmpty(initiatedBy) && initiatedBy.ToUpper() == "SELF")
                    MyTests_expiredTestsActivateImageButton.Visible = false;
                else
                {
                    MyTests_expiredTestsActivateImageButton.Visible = true;
                    MyTests_expiredTestsActivateImageButton.Attributes.Add("OnClick",
                        "javascript:return OpenCandidateRequest('" + candidateSessionID
                        + "','" + attemptID + "','expired')");
                }
            }
        }

        /// <summary>
        /// Handler method that will be called when the refresh button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void MyTests_refreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Request.RawUrl.Contains("ex") ? 
                    Request.RawUrl.Substring(0, Request.RawUrl.IndexOf("ex") - 1) : Request.RawUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyTests_topErrorMessageLabel,
                    MyTests_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void MyTests_startTestButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                "?candidatesessionid=" + MyTests_candidateSessionID.Value +
                "&attemptid=" + MyTests_attemptID.Value +
                "&parentpage=MY_TST", false);
        }

        #endregion Events Hadlers                                              

        #region Private Methods                                                

        /// <summary>
        /// This method visibles the appropriate test based on key
        /// </summary>
        /// <param name="Key">Test to display to the user
        /// 1. Pending test
        /// 2. Completed test
        /// 3. Expired test</param>
        private void SwithTests(int Key)
        {
            switch (Key)
            {
                case 1:
                    MyTests_completedTestsTR.Style.Add("display", "none");
                    MyTests_expiredTestsTR.Style.Add("display", "none");
                    MyTests_pendingTestsUpSpan.Style.Add("display", "block");
                    MyTests_pendingTestsDownSpan.Style.Add("display", "none");
                    MyTests_pendingTestsDiv.Style.Add("height", "100%");
                    break;
                case 2:
                    MyTests_pendingTestsTR.Style.Add("display", "none");
                    MyTests_expiredTestsTR.Style.Add("display", "none");
                    MyTests_completedTestsUpSpan.Style.Add("display", "block");
                    MyTests_completedTestsDownSpan.Style.Add("display", "none");
                    MyTests_completedTestsDiv.Style.Add("height", "100%");
                    break;
                case 3:
                    MyTests_pendingTestsTR.Style.Add("display", "none");
                    MyTests_completedTestsTR.Style.Add("display", "none");
                    MyTests_expiredTestsUpSpan.Style.Add("display", "block");
                    MyTests_expiredTestsDownSpan.Style.Add("display", "none");
                    MyTests_expiredTestsDiv.Style.Add("height", "100%");
                    break;
            }
        }

        /// <summary>
        /// Method that subscribes the paging control events.
        /// </summary>
        private void SubscribePagingEvents()
        {
            MyTests_pendingTestsTopPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                   (MyTests_pendingTestsPagingNavigator_PageNumberClick);

            MyTests_completedTestsTopPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                   (MyTests_completedTestsPagingNavigator_PageNumberClick);

            MyTests_expiredTestsTopPagingNavigator.PageNumberClick += new
                PageNavigator.PageNumberClickEventHandler
                   (MyTests_expiredTestsPagingNavigator_PageNumberClick);
        }

        /// <summary>
        /// Method that shows the list of pending tests for the given page 
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowPendingTests(int pageNumber)
        {
            int totalRecords;

            List<CandidateTestDetail> pendingTests = new CandidateBLManager().GetTests
                (CandidateTestStatus.Pending, base.userID,
                ViewState["PENDING_TESTS_SORT_FIELD"].ToString(),
                ((SortType)ViewState["PENDING_TESTS_SORT_ORDER"]),
                pageNumber, base.GridPageSize, out totalRecords);

            MyTests_pendingTestsGridView.DataSource = pendingTests;
            MyTests_pendingTestsGridView.DataBind();

            MyTests_pendingTestsTopPagingNavigator.PageSize = base.GridPageSize;
            MyTests_pendingTestsTopPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that shows the list of completed tests for the given page
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowCompletedTests(int pageNumber)
        {
            int totalRecords;

            List<CandidateTestDetail> completedTests = new CandidateBLManager().GetTests
                (CandidateTestStatus.Completed, base.userID,
                ViewState["COMPLETED_TESTS_SORT_FIELD"].ToString(),
                ((SortType)ViewState["COMPLETED_TESTS_SORT_ORDER"]),
                pageNumber, base.GridPageSize, out totalRecords);

            MyTests_completedTestsGridView.DataSource = completedTests;
            MyTests_completedTestsGridView.DataBind();

            MyTests_completedTestsTopPagingNavigator.PageSize = base.GridPageSize;
            MyTests_completedTestsTopPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that shows the list of expired tests for the given page 
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowExpiredTests(int pageNumber)
        {
            int totalRecords;

            List<CandidateTestDetail> expiredTests = new CandidateBLManager().GetTests
                (CandidateTestStatus.Expired, base.userID,
                ViewState["EXPIRED_TESTS_SORT_FIELD"].ToString(),
                ((SortType)ViewState["EXPIRED_TESTS_SORT_ORDER"]),
                pageNumber, base.GridPageSize, out totalRecords);

            MyTests_expiredTestsGridView.DataSource = expiredTests;
            MyTests_expiredTestsGridView.DataBind();

            MyTests_expiredTestsTopPagingNavigator.PageSize = base.GridPageSize;
            MyTests_expiredTestsTopPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that validate requests to retake a test. 
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session id.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt id.
        /// </param>
        private bool IsValidRetake(string candidateSessionID, string testKey, int attemptID)
        {
            string retakeCount = string.Empty;
            Dictionary<string, string> dicRetakeDetails = new
                Dictionary<string, string>();
            string isCertificateTest = string.Empty;
            bool isValidRetake = true;
            string pendingAttempt = new CandidateBLManager().TestRetakeValidation(candidateSessionID, testKey,
                attemptID, out retakeCount, out isCertificateTest, out dicRetakeDetails);

            if (pendingAttempt != null && Convert.ToInt32(pendingAttempt) > 0)
            {
                MyTests_RetakeValidation_ConfirmMsgControl.Message =
                    "Already an attempt is pending for this test session.<br>Check with your pending tests";
                return isValidRetake = false;
            }
            if (Convert.ToInt32(isCertificateTest) > 0)
            {
                if (retakeCount != null && Convert.ToInt32(retakeCount) <= 0)
                {
                    MyTests_RetakeValidation_ConfirmMsgControl.Message =
                        "You have utilized maximum number of attempts.<br>You are not allowed to retake this test";
                    return isValidRetake = false;
                }

                //if (dicRetakeDetails != null && dicRetakeDetails.Count > 0)
                //{
                //    foreach (KeyValuePair<string, string> retakeDetail in dicRetakeDetails)//Temporary-will be removed//
                //    {
                //        if (Convert.ToInt32(retakeDetail.Key) > 0)
                //        {
                //            MyTests_RetakeValidation_ConfirmMsgControl.Message =
                //                "You are allowed to retake your test only after " + retakeDetail.Key + " days from "
                //                + retakeDetail.Value;
                //            return isValidRetake = false;
                //        }
                //    }
                //}
            }

            return isValidRetake;
        }

        #endregion Private Methods                                             

        #region Protected Methods                                              

        /// <summary>
        /// Method that retrieves the qualified status for certification. This
        /// helps to show or hide the view certification link icon in the 
        /// completed test grid section.
        /// </summary>
        /// <param name="certificationStatus">
        /// A <see cref="string"/> that holds the certification status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the certification available status.
        /// True represents available and false not available.
        /// </returns>
        /// <remarks>
        /// The applicable status for certification status are:
        /// 1. 'N/A' if not applicable.
        /// 2. 'Unavailable' if not qualified for certification.
        /// 3. 'Available' if qualified for certification.
        /// </remarks>
        protected bool IsQualified(string certificationStatus)
        {
            return (certificationStatus == "Available" ? true : false);
        }

        /// <summary>
        /// Method that retrieves the show results to candidate status. This
        /// helps to show or hide the show results link icon in the completed 
        /// test grid section.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the show results to candidate status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of show results
        /// to candidate status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for show results to candidates are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsShowResults(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        /// <summary>
        /// Method that retrieves the qualified status for certification. This
        /// helps to show or hide the view certification link icon in the 
        /// completed test grid section.
        /// </summary>
        /// <param name="cyberProctoringStatus">
        /// A <see cref="string"/> that holds the cyber proctoring status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the cyber proctoring status.
        /// </returns>
        protected bool IsCyberProctoring(string cyberProctoringStatus)
        {
            return cyberProctoringStatus.ToUpper() == "TRUE" ? true : false;
        }

        #endregion Protected Methods                                           

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Method that will load the pending, completed and expired tests into
        /// the grid.
        /// </summary>
        /// <remarks>
        /// This is a overridden method.
        /// </remarks>
        protected override void LoadValues()
        {
            ShowPendingTests(1);
            ShowCompletedTests(1);
            ShowExpiredTests(1);
        }

        #endregion Protected Overridden Methods                                
    }
}