﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestConductionCompleted.aspx.cs
// File that represents the user interface layout and functionalities
// for the TestConductionCompleted page. This page helps in displaying the
// test conduction summary and provide various links such as sending
// feedbacks to admin, emailing recruiter etc.

#endregion Header 

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the TestConductionCompleted page. This page helps in displaying the
    /// test conduction summary and provide various links such as sending
    /// feedbacks to admin, emailing recruiter etc. This class inherits the
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class TestConductionCompleted : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                // Set browser title.
                Master.SetPageCaption("Test Completion Summary");

                if (!IsPostBack)
                {
                    if (Request.QueryString["status"] == null)
                        return;

                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestConductionCompleted_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the email recruiter link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will navigates the email page.
        /// </remarks>
        protected void TestConductionCompleted_emailRecruiterLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("../Popup/SendEmail.aspx" +
                    "?type=REC" +
                    "&candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                    "&attemptid=" + Request.QueryString["attemptid"] +
                    "&displaytype=" + Request.QueryString["displaytype"] +
                    "&parentpage=" + Constants.ParentPage.CANDIDATE_DASHBOARD, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestConductionCompleted_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Set status.
            if (Request.QueryString["status"].ToUpper() == Constants.CandidateSessionStatus.COMPLETED)
                TestConductionCompleted_statusMessageLabel.Text = "Test session was completed successfully";
            else if (Request.QueryString["status"].ToUpper() == Constants.CandidateSessionStatus.ELAPSED)
                TestConductionCompleted_statusMessageLabel.Text = "Test session was elapsed";
            else if (Request.QueryString["status"].ToUpper() == Constants.CandidateSessionStatus.QUIT)
                TestConductionCompleted_statusMessageLabel.Text = "Test session was terminated";

            if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]) &&
                !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
            {
                int attemptID = 0;
                if (int.TryParse(Request.QueryString["attemptid"], out attemptID) == true)
                {
                    // Load test detail.
                    TestDetail testDetail =  new CandidateBLManager().GetTestSessionDetail
                        (Request.QueryString["candidatesessionid"], attemptID);

                    if (testDetail != null)
                    {
                        TestConductionCompleted_testNameLabel.Text = testDetail.Name;
                        TestConductionCompleted_testDescriptionLabel.Text = testDetail.Description == null ? testDetail.Description :
                            testDetail.Description.ToString().Replace(Environment.NewLine, "<br />");
                    }
                }
            }
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            TestConductionCompleted_topErrorMessageLabel.Text = string.Empty;
            TestConductionCompleted_topErrorMessageLabel.Visible = false;
        }

        #endregion Private Methods

    }
}