﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestResult.aspx.cs
// Class that represents the user interface layout and functionalities
// for the CandidateTestResult page. This page helps in viewing the
// candidates test result that includes test summary, chart statistics
// (for category, subject, test area & complexity), answered statistics,
// test score, score distribution amongst categories etc. Also has the 
// facility to publish the results in Facebook & LinkedIn. 

#endregion Header

#region Directives                                                             

using System;
using System.IO;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

using iTextSharp.text;
using iTextSharp.text.pdf;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the CandidateTestResult page. This page helps in viewing the
    /// candidates test result that includes test summary, chart statistics
    /// (for category, subject, test area & complexity), answered statistics,
    /// test score, score distribution amongst categories etc. Also has the 
    /// facility to publish the results in Facebook & LinkedIn. This 
    /// class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CandidateTestResult : PageBase
    {
        #region Protected Variables                                            

        /// <summary>
        /// A <see cref="string"/> that holds the result Url for publishing
        /// into Facebook & LinkedIn.
        /// </summary>
        protected string resultUrl;

        /// <summary>
        /// That holds facebook summary details
        /// </summary>
        protected string facebookSummary;
        #endregion Protected Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Master.SetPageCaption("Test Result");
                string baseURL = ConfigurationManager.AppSettings["ATTACHMENT_URL"];
                
                // Show/hide menu based on user type (limited or not).
                //Master.ShowMenu = !base.isLimited;

                // Clear message and hide labels.
                ClearMessages();
                //Set browser title
                Master.SetPageCaption("Test Results");

                if (IsPostBack)
                    return;

                Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF] = null;
                //Method to load the values in test statistics tab
                LoadValues();
                //Method to load the values in candidate statistics tab
                LoadCandidateStatistics();
                if (!Request.QueryString.HasKeys())
                    return;
                if ((Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"])) ||
                        (Utility.IsNullOrEmpty(Request.QueryString["attemptid"])) ||
                        (Utility.IsNullOrEmpty(Request.QueryString["testkey"])) ||
                        (Utility.IsNullOrEmpty(Request.QueryString["parentpage"])))
                    return;
                ((Button)CandidateTestResult_emailConfirmation_ConfirmMsgControl.
                    FindControl("ConfirmMsgControl_emailAttachmentButton")).
                    Attributes.Add("onclick", "javascript:return RedirectEmailAttachment('"
                    + Request.QueryString["candidatesessionid"].ToString().Trim() + "','"
                    + Request.QueryString["attemptid"].ToString().Trim() + "','"
                    + Request.QueryString["testkey"].ToString().Trim() + "','"
                    + Request.QueryString["parentpage"].ToString().Trim() + "','Y','" +
                    CandidateTestResult_emailConfirmation_ModalPopupExtender.ID + "');");

                ((Button)CandidateTestResult_emailConfirmation_ConfirmMsgControl.
                    FindControl("ConfirmMsgControl_emailContentButton")).
                    Attributes.Add("onclick", "javascript:return RedirectEmailAttachment('"
                    + Request.QueryString["candidatesessionid"].ToString().Trim() + "','"
                    + Request.QueryString["attemptid"].ToString().Trim() + "','"
                    + Request.QueryString["testkey"].ToString().Trim() + "','"
                    + Request.QueryString["parentpage"].ToString().Trim() + "','N','" +
                    CandidateTestResult_emailConfirmation_ModalPopupExtender.ID + "');");

                //int score = Convert.ToInt32("100.00");
               // CandidateTestResult_words.Text = changeToWords(score.ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateTestResult_bottomErrorMessageLabel,
                    CandidateTestResult_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the download button is clicked.
        /// This will perform to download a PDF which contains the candidate
        /// results.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CandidateTestResult_downloadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("../PrintPages/CandidateTestResultTranscript.aspx?candidatesessionid="
                    + Request.QueryString["candidatesessionid"]
                    + "&attemptid=" + Request.QueryString["attemptid"]
                    + "&testkey=" + Request.QueryString["testkey"]
                    + "&downloadoremail=D", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateTestResult_bottomErrorMessageLabel,
                    CandidateTestResult_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that will call when the upload resume button is clicked.
        /// This navigates to the desired page where the candidate can upload
        /// his/her resume.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void CandidateTestResult_uploadResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Signup.aspx?&activetag=updateresume&parentpage=CTR", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateTestResult_bottomErrorMessageLabel,
                    CandidateTestResult_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Represents the method to load the candidate statistics details
        /// </summary>
        private void LoadCandidateStatistics()
        {
            string sessionKey = string.Empty;
            string testKey = string.Empty;
            int attemptID = 0;
            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesessionid"].ToString();

            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
            {
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
                CandidateTestResult_attemptIDHiddenField.Value = attemptID.ToString();
            }
            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            CandidateStatisticsDetail CandidateStatisticsDataSource =
                new ReportBLManager().GetCandidateTestResultStatisticsDetails(sessionKey, testKey, attemptID);

            facebookSummary = string.Concat("My Score is 100% on `", CandidateStatisticsDataSource.TestName,
                "(", testKey, ")`", " Test");
            if (CandidateStatisticsDataSource != null)
            {
                // Assign parameters for linked in share.
                if (Request.QueryString["mode"] != null && 
                    Request.QueryString["mode"].ToString().Trim().ToUpper() == "SHARE")
                {
                    CandidateTestResult_shareLinksRow.Visible = true;

                    //resultUrl = ConfigurationManager.AppSettings["CANDIDATE_TEST_RESULT_URL"];
                    //resultUrl += "?testkey=" + Request.QueryString["testkey"];
                    //resultUrl += "&parentpage=" + Request.QueryString["parentpage"];
                    //resultUrl += "&candidatesessionid=" + Request.QueryString["candidatesessionid"];
                    //resultUrl += "&attemptid=" + Request.QueryString["attemptid"];

                    resultUrl = ConfigurationManager.AppSettings["CANDIDATE_TEST_RESULT_URL"];
                    resultUrl += "%3Ftestkey%3D" + Request.QueryString["testkey"];
                    resultUrl += "%26parentpage%3D" + Request.QueryString["parentpage"];
                    resultUrl += "%26candidatesessionid%3D" + Request.QueryString["candidatesessionid"];
                    resultUrl += "%26attemptid%3D" + Request.QueryString["attemptid"];

                    string url = string.Format("http://www.linkedin.com/shareArticle?mini=true&url={0}&title={1}&summary={2}&source=www.fortehcm.com", 
                        resultUrl, "Test results of " + CandidateStatisticsDataSource.CandidateFullName, CandidateStatisticsDataSource.TestName);
                    CandidateTestResult_shareInLinkedInHyperLink.NavigateUrl = url;

                    //Set the candidate result path in email
                    CandidateTestResult_emailCopyLinkHyperLink.NavigateUrl = "mailto:?subject=My Test Result&body=" + resultUrl;
                }

                Session["CANDIDATE_STATISTICS"] = CandidateStatisticsDataSource;

                //Load the chart details
                CandidateTestResult_candidateTestResultControl.CandidateStatisticsDataSource = CandidateStatisticsDataSource;
            }
            //Load the candidate and question statistics details
            CandidateTestResult_candidateTestResultControl.LoadCandidateStatisticsDetails();

            //Get the categories details for the test 
            MultipleSeriesChartData categoryChartDataSource = new ReportBLManager().
                GetCandidateStatisticsCategoriesChartDetails(sessionKey, testKey, attemptID);

            if (categoryChartDataSource.MultipleSeriesChartDataSource.Count == 0)
            {
                CandidateTestResult_candidateTestResultControl.
                    DisplayErrorMessage = "No chart statistics";

                return;
            }

            categoryChartDataSource.ChartType = SeriesChartType.Column;
            categoryChartDataSource.ChartLength = 170;
            categoryChartDataSource.ChartWidth = 450;
            categoryChartDataSource.IsDisplayAxisTitle = true;
            categoryChartDataSource.IsDisplayChartTitle = false;
            categoryChartDataSource.XAxisTitle = "Categories";
            categoryChartDataSource.YAxisTitle = "Test Scores";
            categoryChartDataSource.IsShowLabel = true;
            categoryChartDataSource.ChartImageName = Constants.ChartConstants.CHART_CATEGORY +
                "-" + testKey + "-" + sessionKey + "-" + attemptID;
            categoryChartDataSource.ChartTitle = Resources.HCMResource.TestResult_CategoryChartTitle;

            //CandidateTestResult_candidateTestResultControl.LoadCategoryChartControl(categoryChartDataSource);

            //Load the subject chart
            MultipleSeriesChartData subjectChartDataSource = new ReportBLManager().
                GetCandidateStatisticsSubjectsChartDetails(sessionKey, testKey, attemptID);

            if (subjectChartDataSource.MultipleSeriesChartDataSource.Count == 0)
            {
                CandidateTestResult_candidateTestResultControl.
                    DisplayErrorMessage = "No chart statistics";

                return;
            }

            subjectChartDataSource.ChartType = SeriesChartType.Column;
            subjectChartDataSource.ChartLength = 170;
            subjectChartDataSource.ChartWidth = 450;
            subjectChartDataSource.IsDisplayAxisTitle = true;
            subjectChartDataSource.IsDisplayChartTitle = false;
            subjectChartDataSource.XAxisTitle = "Subjects";
            subjectChartDataSource.YAxisTitle = "Test Scores";
            subjectChartDataSource.IsShowLabel = true;
            subjectChartDataSource.ChartTitle = Resources.HCMResource.TestResult_SubjectChartTitle;
            subjectChartDataSource.ChartImageName =
                Constants.ChartConstants.CHART_SUBJECT + "-" + testKey + "-" + sessionKey + "-" + attemptID;
            //  CandidateTestResult_candidateTestResultControl.LoadSubjectChartControl(subjectChartDataSource);

            //get the test area details for the test 
            MultipleSeriesChartData testAreaChartDataSource = new ReportBLManager().
               GetCandidateStatisticsTestAreaChartDetails(sessionKey, testKey, attemptID);

            if (testAreaChartDataSource.MultipleSeriesChartDataSource.Count == 0)
            {
                CandidateTestResult_candidateTestResultControl.
                    DisplayErrorMessage = "No chart statistics";
                return;
            }

            testAreaChartDataSource.ChartType = SeriesChartType.Column;
            testAreaChartDataSource.ChartLength = 170;
            testAreaChartDataSource.ChartWidth = 450;
            testAreaChartDataSource.IsDisplayAxisTitle = true;
            testAreaChartDataSource.IsDisplayChartTitle = false;
            testAreaChartDataSource.XAxisTitle = "Test Areas";
            testAreaChartDataSource.YAxisTitle = "Test Scores";
            testAreaChartDataSource.ChartTitle = Resources.HCMResource.TestResult_TestAreaChartTitle;
            testAreaChartDataSource.IsShowLabel = true;
            
            // CandidateTestResult_candidateTestResultControl.LoadTestAreaChartControl(testAreaChartDataSource);

            testAreaChartDataSource.ChartImageName =
                Constants.ChartConstants.CHART_TESTAREA + "-" + testKey + "-" + sessionKey + "-" + attemptID;

            #region get complexity data
            //get the test area details for the test 
            MultipleSeriesChartData complexityChartDataSource = new ReportBLManager().
               GetCandidateStatisticsComplexityChartDetails(sessionKey, testKey, attemptID);

            if (complexityChartDataSource.MultipleSeriesChartDataSource.Count == 0)
            {
                CandidateTestResult_candidateTestResultControl.
                    DisplayErrorMessage = "No chart statistics";
                return;
            }

            complexityChartDataSource.ChartType = SeriesChartType.Column;
            complexityChartDataSource.ChartLength = 170;
            complexityChartDataSource.ChartWidth = 450;
            complexityChartDataSource.IsDisplayAxisTitle = true;
            complexityChartDataSource.IsDisplayChartTitle = false;
            complexityChartDataSource.XAxisTitle = "Complexity";
            complexityChartDataSource.YAxisTitle = "Complexity Scores";
            complexityChartDataSource.ChartTitle = Resources.HCMResource.TestResult_TestAreaChartTitle;
            complexityChartDataSource.IsShowLabel = true;
            complexityChartDataSource.ChartImageName =
                Constants.ChartConstants.CHART_COMPLEXITY + "-" + testKey + "-" + sessionKey + "-" + attemptID;
            #endregion


            CandidateTestResult_candidateTestResultControl.LoadChartData
                (categoryChartDataSource, subjectChartDataSource, testAreaChartDataSource,complexityChartDataSource);

            //Method to load the test Scores histogram chart
            LoadHistogramChart(CandidateTestResult_candidateTestResultControl
                .CandidateStatisticsDataSource.MyScore);
        }

        /// <summary>
        /// Represents the method to load the histogram chart details
        /// </summary>
        private void LoadHistogramChart(decimal myScore)
        {
            string sessionKey = string.Empty;
            int attemptID = 0;
            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesessionid"].ToString();

            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
            {
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
            }

            //Method to load the test Scores histogram chart
            ReportHistogramData histogramData = new ReportHistogramData();

            histogramData.ChartLength = 170;
            histogramData.ChartWidth = 450;
            histogramData.IsDisplayLegend = false;
            histogramData.IsShowLabel = true;
            histogramData.SegmentInterval = 10;
            histogramData.ChartTitle = Resources.HCMResource.TestResult_HistogramChartTitle;
            histogramData.IsDisplayAxisTitle = true;
            histogramData.XAxisTitle = "Scores";
            histogramData.YAxisTitle = "No Of Candidates";
            histogramData.IsDisplayCandidateScore = true;
            histogramData.CandidateScore = Convert.ToDouble(myScore);

            string testKey = string.Empty;

            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            histogramData.ChartData = new ReportBLManager().GetTestCandidateScores(testKey);

            if (histogramData.ChartData.Count == 0)
            {
                CandidateTestResult_candidateTestResultControl.
                    DisplayErrorMessage = "No chart statistics";
                return;
            }
            histogramData.ChartImageName = Constants.ChartConstants.CHART_HISTOGRAM
                    + "-" + testKey + "-" + sessionKey + "-" + attemptID;
            //HistogramChart Control is commented
            //CandidateTestResult_candidateTestResultControl.LoadHistogramChartControl(histogramData);
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            CandidateTestResult_topErrorMessageLabel.Text = string.Empty;
            CandidateTestResult_bottomErrorMessageLabel.Text = string.Empty;
            CandidateTestResult_topSuccessMessageLabel.Text = string.Empty;
            CandidateTestResult_bottomSuccessMessageLabel.Text = string.Empty;

            CandidateTestResult_topErrorMessageLabel.Visible = false;
            CandidateTestResult_bottomErrorMessageLabel.Visible = false;
            CandidateTestResult_topSuccessMessageLabel.Visible = false;
            CandidateTestResult_bottomSuccessMessageLabel.Visible = false;
        }

        #endregion Private Methods

        #region Protected Overriden Methods                                    

        /// <summary>
        /// Method to check the data is valid
        /// </summary>
        /// <returns>
        /// A<see cref="bool"/>that return whether valid or not
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method used to load the values
        /// </summary>
        protected override void LoadValues()
        {
           

            string testID = string.Empty;

            //Get the test id
            if (!Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
            {
                testID = Request.QueryString["testkey"];
                CandidateTestResult_testKeyHiddenField.Value = testID;
            }

            //Get the test name   
            string testName;
            testName = new ReportBLManager().GetTestName(Request.QueryString["testkey"]);
            CandidateTestResult_testNameHiddenField.Value = testName;

            string candidateSessionId = null;
            string candidateName = null;
            DateTime testCompletedDate = DateTime.Now;
            if (Request.QueryString["candidatesessionid"] != null &&
                Request.QueryString["candidatesessionid"].ToString().Length != 0)
            {
                candidateSessionId = Request.QueryString["candidatesessionid"].ToString().Trim();
                CandidateTestResult_candidateSessionHiddenField.Value = candidateSessionId;

                int attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
                //Get testCompletedDate
                testCompletedDate = new ReportBLManager().GetTestCompletedDateByCandidate(candidateSessionId, attemptID);
                //Get the CandidateName
                candidateName = new CandidateBLManager().GetCandidateName(candidateSessionId);
                CandidateTestResult_candidateNameHiddenField.Value = candidateName;
                CandidateTestResult_creditsEarnedHiddenField.Value = "0.00";

                CandidateTestResult_candidateTestResultControl.LoadCandidateTestDetails
                    (testID, testName, candidateName, testCompletedDate);
            }

            TestStatistics testStatistics = new ReportBLManager().GetTestSummaryDetails(testID);

            if (testStatistics != null)
                Session["TEST_STATISTICS"] = testStatistics;

            //Get the test details from database
            CandidateTestResult_candidateTestResultControl.TestStatisticsDataSource = testStatistics;

            CandidateTestResult_candidateTestResultControl.TestStatisticsDataSource.TestID = testID;

            //Get the general test statistics and load the 
            //test summary details
            CandidateTestResult_candidateTestResultControl.LoadTestSummaryDetails();
        }

        #endregion Protected Overriden Methods
    }
}