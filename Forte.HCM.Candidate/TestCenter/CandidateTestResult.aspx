﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    Title="Test Result" CodeBehind="CandidateTestResult.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.CandidateTestResult" %>

<%@ Register Src="~/CommonControls/TestResultsControl.ascx" TagName="TestResultsControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="CandidateTestResult_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script src="../JS/ChartScript.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ShowEmailConfirm() {
            //displays email confirmation pop up extender
            $find("<%= CandidateTestResult_emailConfirmation_ModalPopupExtender.ID %>").show();
            return false;
        }
        // Function that will launch the email attachment popup.
        function RedirectEmailAttachment(CandidateSessionId, AttemptID, TestKey, ParentPage, EmailType, ConfirmPopUpId) {
            var queryStringValue = "../Popup/EmailAttachment.aspx?type=CAND_RSLT_PDF" + "&candidatesessionid="
            + CandidateSessionId + "&attemptid=" + AttemptID + "&testkey="
             + TestKey + "&parentpage=" + ParentPage + "&emailtype=" + EmailType +"&displaytype=CT";
            window.location = queryStringValue;
            $find(ConfirmPopUpId).hide();
            return false;
        }       
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div id="CandidateTestResult_mainDiv">
                    <!-- Header starts here -->
                    <div id="CandidateTestResult_headerDiv" class="test_results_header_bg_div">
                        <div class="header_text_bold" style="float: left; margin-top: 5px;margin-left:5px;">
                            <asp:HiddenField ID="CandidateTestResult_candidateSessionHiddenField" runat="server" />
                            <asp:HiddenField ID="CandidateTestResult_attemptIDHiddenField" runat="server" />
                            <asp:HiddenField ID="CandidateTestResult_testKeyHiddenField" runat="server" />
                            <asp:HiddenField ID="CandidateTestResult_testNameHiddenField" runat="server" />
                            <asp:HiddenField ID="CandidateTestResult_candidateNameHiddenField" runat="server" />
                            <asp:HiddenField ID="CandidateTestResult_creditsEarnedHiddenField" runat="server" />
                            <asp:Label ID="CandidateTestResult_headerLiteral" runat="server" Text="Test Results"></asp:Label>
                        </div>
                        <div id="CandidateTestResult_rightSideControl" style="float: right; margin-top: 5px;margin-right:5px;">
                            
                            <div style="float: left; margin-left: 5px;">
                                <asp:Button ID="CandidateTestResult_topDownloadButton" runat="server" Text="Download Transcript"
                                    SkinID="sknButtonId" OnClick="CandidateTestResult_downloadButton_Click" />
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <asp:Button ID="CandidateTestResult_topEmailButton1" OnClientClick="javascript:return ShowEmailConfirm();"
                                    runat="server" Text="Email" SkinID="sknButtonId" />
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <asp:LinkButton ID="CandidateTestResult_topCancelButton" runat="server" Text="Cancel"
                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div id="CandidateTestResult_messageControl">
                        <div class="msg_align" style="text-align: center;">
                            <asp:Label ID="CandidateTestResult_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="CandidateTestResult_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </div>
                    </div>
                    <!-- Ends here -->
                    <!-- Facebook,in & upload control starts here -->
                    <div class="test_results_header_bg_div" style="height:40px;margin-top:5px;" runat="server" id="CandidateTestResult_shareLinksRow" visible="false">
                        <div style="float:right;padding-top:5px;padding-right:10px;">
                            <div style="float:left;margin-left:5px;">
                                <asp:HyperLink ID="CandidateTestResult_emailCopyLinkHyperLink" runat="server"
                                ToolTip="Email/Publish/Copy Link" Target="_blank" ImageUrl="~/App_Themes/DefaultTheme/Images/publish.png" 
                                Visible="true"></asp:HyperLink>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <%--<a name="fb_share" title="Click here to share your results link into Facebook" share_url="<%=resultUrl %>"></a>
                                <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>--%>
                                <a type="button" title="Click here to share your results link into Facebook"
                                onclick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=ForteHCM - My Test Result&amp;p[summary]=<%=facebookSummary%>&amp;p[url]=<%=resultUrl%>&amp;&p[images][0]=http://www.fortehcm.com/Images/logo.png', 'sharer', 'toolbar=0,status=0,width=650,height=400');" href="javascript: void(0)">
                                <img src="../App_Themes/DefaultTheme/Images/facebook.gif" alt="Click here to share your results link into Facebook"/>
                             </a>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <asp:HyperLink ID="CandidateTestResult_shareInLinkedInHyperLink" runat="server" ImageUrl="../Images/linkedin.png"
                                    Target="_blank" ToolTip="Click here to share your results link into LinkedIn">
                                </asp:HyperLink>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <asp:Button ID="CandidateTestResult_uploadResumeButton" CssClass="self_upload_btn_bg"
                                    runat="server" Text="Update Your Resume" OnClick="CandidateTestResult_uploadResumeButton_Click" />
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <asp:Label ID="CandidateTestResult_words" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Ends here -->
                    <div id="CandidateTestResult_testResultControl">
                        <div>
                            <uc1:TestResultsControl ID="CandidateTestResult_candidateTestResultControl" runat="server">
                            </uc1:TestResultsControl>
                        </div>
                    </div>
                    <!-- Bottom error message starts here -->
                    <div class="msg_align">
                        <asp:Label ID="CandidateTestResult_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CandidateTestResult_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                    <!-- Ends here -->
                    <!--Bottom download,email and cancel starts here -->
                    <div id="CandidateTestResult_bottomControls" style="clear: both;margin-top:5px;" class="test_results_header_bg_div">
                        <div style="float: right; padding-top: 5px; padding-right: 5px;">
                            <div style="float: left; margin-left: 5px;">
                                <asp:Button ID="CandidateTestResult_bottomDownloadButton" runat="server" Text="Download Transcript"
                                    SkinID="sknButtonId" OnClick="CandidateTestResult_downloadButton_Click" /></div>
                            <div style="float: left; margin-left: 5px;">
                                <asp:Button ID="CandidateTestResult_bottomEmailButton" OnClientClick="javascript:return ShowEmailConfirm();"
                                    runat="server" Text="Email" SkinID="sknButtonId" />
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <asp:LinkButton ID="CandidateTestResult_bottomCancelButton" runat="server" Text="Cancel"
                                    SkinID="sknActionLinkButton" OnClick="ParentPageRedirect"></asp:LinkButton>
                            </div>
                            <div style="float: left;">
                                <asp:Panel ID="CandidateTestResult_emailConfirmationPanel" runat="server" Style="display: none;
                                    height: 205px;" CssClass="popupcontrol_confirm">
                                    <div id="CandidateTestResult_emailConfirmationDiv" style="display: none">
                                        <asp:Button ID="CandidateTestResult_emailConfirmation_hiddenButton" runat="server" />
                                    </div>
                                    <uc3:ConfirmMsgControl ID="CandidateTestResult_emailConfirmation_ConfirmMsgControl"
                                        runat="server" Title="Email Confirmation" Type="EmailConfirmType" Message="How do you want to email your results?" />
                                </asp:Panel>
                                <ajaxToolKit:ModalPopupExtender ID="CandidateTestResult_emailConfirmation_ModalPopupExtender"
                                    BehaviorID="CandidateTestResult_emailConfirmation_ModalPopupExtender" runat="server"
                                    PopupControlID="CandidateTestResult_emailConfirmationPanel" TargetControlID="CandidateTestResult_emailConfirmation_hiddenButton"
                                    BackgroundCssClass="modalBackground">
                                </ajaxToolKit:ModalPopupExtender>
                            </div>
                        </div>
                    </div>
                    <!-- Ends here -->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
