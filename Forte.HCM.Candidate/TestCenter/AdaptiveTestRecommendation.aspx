﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HCMCandidateMaster.Master"
    CodeBehind="AdaptiveTestRecommendation.aspx.cs" Inherits=Forte.HCM.UI.CandidateCenter.AdaptiveTestRecommendation" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc4" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/HCMCandidateMaster.Master" %>
<asp:Content ID="AdaptiveTestRecommendation_bodyContent" runat="server" ContentPlaceHolderID="HCMCandidateMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">
        // This will invoke the start test modalpopupextender
        function StartTest() {
            $find("<%= AdaptiveTestRecommendation_startTestPopupExtender.ClientID  %>").show();
            return false;
        }

        function OpenTestSession(testName, testDesc, TestId, Recommended, Cost) {
            document.getElementById("<%= AdaptiveTestRecommendation_testNameLabel.ClientID %>").innerHTML = testName;
            document.getElementById("<%= AdaptiveTestRecommendation_testDescriptionDiv.ClientID %>").innerHTML = testDesc;
            document.getElementById("<%= AdaptiveTestRecommendation_testIDLabel.ClientID %>").innerHTML = TestId;
            document.getElementById("<%= AdaptiveTestRecommendation_timeLimitLabel.ClientID %>").innerHTML = Recommended;
            document.getElementById("<%= AdaptiveTestRecommendation_testCostLabel.ClientID %>").innerHTML = Cost;
            $find("<%= AdaptiveTestRecommendation_questionModalPopupExtender.ClientID  %>").show();
            return false;
        }
        function ShowTestSession() {
            $find("<%= AdaptiveTestRecommendation_questionModalPopupExtender.ClientID  %>").show();
            return false;
        }
    </script>

    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="AdaptiveTestRecommendation_headerLiteral" runat="server" Text="Adaptive Test Recommendation"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <asp:LinkButton ID="AdaptiveTestRecommendation_topCancelButton" runat="server" SkinID="sknActionLinkButton"
                                Text="Cancel" PostBackUrl="~/TestCenter/AdaptiveTestSummary.aspx" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="AdaptiveTestRecommendation_topMessageLabelUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="AdaptiveTestRecommendation_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage" EnableViewState="false"></asp:Label>
                        <asp:Label ID="AdaptiveTestRecommendation_topErrorMessageLabel" EnableViewState="false"
                            runat="server" SkinID="sknErrorMessage">
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="AdaptiveTestRecommendation_updatePanel" runat="server">
                                <%--<Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="AdaptiveTestRecommendation_searchButton" />
                                </Triggers>--%>
                                <ContentTemplate>
                                    <asp:HiddenField ID="AdaptiveTestRecommendation_isMaximizesStateHiddenField" runat="server" />
                                    <div id="AdaptiveTestRecommendation_recommendTypeTR" runat="server" style="display: block;">
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 8%">
                                                    <asp:Label ID="AdaptiveTestRecommendation_userNameHeadLabel" runat="server" Text="User Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 15%">
                                                    <asp:Label ID="AdaptiveTestRecommendation_userNameLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 20%">
                                                    <asp:Label ID="AdaptiveTestRecommendation_selectTypeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                        Text="Select Recommendation Type"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 27%">
                                                    <asp:DropDownList ID="AdaptiveTestRecommendation_selectTypeDropDownList" runat="server">
                                                        <%--<asp:ListItem Value="0" Text="--Select--"></asp:ListItem>--%>
                                                        <asp:ListItem Value="1" Text="Based On Profile"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Based On Assessment History"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Based On Test Taken By Similar Candidate"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="AdaptiveTestRecommendation_selectedIndexHiddenField" runat="server" />
                                                </td>
                                                <td style="width: 30%" align="left">
                                                    <asp:Button ID="AdaptiveTestRecommendation_searchButton" runat="server" Text="Submit"
                                                        SkinID="sknButtonId" OnClick="AdaptiveTestRecommendation_searchButton_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_2">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="AdaptiveTestRecommendation_testDetailsTR" runat="server">
                        <td>
                            <div id="AdaptiveTestRecommendation_profileHeaderDIV" runat="server" style="display: block">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="header_bg" align="left">
                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr>
                                                    <td style="width: 93%" align="left">
                                                        <asp:Literal ID="AdaptiveTestRecommendation_profileHeaderLiteral" runat="server"
                                                            Text="Recommended Test Details"></asp:Literal>
                                                    </td>
                                                    <td style="width: 2%" align="right" id="AdaptiveTestRecommendation_testDetailsHeaderTR"
                                                        runat="server">
                                                        <span id="AdaptiveTestRecommendation_testDetailsUpSpan" style="display: none;" runat="server">
                                                            <asp:Image ID="AdaptiveTestRecommendation_testDetailsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                runat="server" id="AdaptiveTestRecommendation_testDetailsDownSpan" style="display: block;"><asp:Image
                                                                    ID="AdaptiveTestRecommendation_testDetailsDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grid_body_bg">
                                            <asp:UpdatePanel ID="AdaptiveTestRecommendation_testDetailsUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <div id="AdaptiveTestRecommendation_testDetailsDiv" style="height: 200px; overflow: auto;"
                                                        runat="server">
                                                        <asp:UpdatePanel ID="AdaptiveTestRecommendation_searchedGridViewUpdatePanel" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="AdaptiveTestRecommendation_searchedTestGridView" runat="server"
                                                                    AutoGenerateColumns="False" Width="100%" AllowSorting="true" OnSorting="AdaptiveTestRecommendation_searchedTestGridView_Sorting"
                                                                    OnRowCreated="AdaptiveTestRecommendation_searchedTestGridView_RowCreated" OnRowCommand="AdaptiveTestRecommendation_searchedTestGridView_RowCommand">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="7%">
                                                                            <ItemTemplate>
                                                                                &nbsp;
                                                                                <asp:ImageButton ID="AdaptiveTestRecommendation_AdditionalDetailsImage" runat="server"
                                                                                    SkinID="sknAdditionalDetailImageButton" ToolTip="Test Statistics" OnClick='<%#String.Format(" return OpenAdditionalTestDetail(&#39;{0}&#39;)", Eval("TestKey")) %>' />
                                                                                &nbsp;<asp:ImageButton ID="AdaptiveTestRecommendation_CreateSessionImage" runat="server"
                                                                                    SkinID="sknCreateNewSessionImageButton" ToolTip="Create Test Session" CommandName="CreateSession" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Test ID" SortExpression="TESTID">
                                                                            <ItemStyle Width="12%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AdaptiveTestRecommendation_testIdLabel" runat="server" Text='<%# Eval("TestKey") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Test Name" SortExpression="TESTName">
                                                                            <ItemStyle Width="25%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AdaptiveTestRecommendation_testNameLabel" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Number Of Questions" SortExpression="NOQUES">
                                                                            <HeaderStyle CssClass="td_padding_right_20" />
                                                                            <ItemStyle Width="15px" CssClass="td_padding_right_20" HorizontalAlign="Right" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AdaptiveTestRecommendation_testNoOfQuestionsLabel" runat="server"
                                                                                    Text='<%# Eval("NoOfQuestions") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Created Date" SortExpression="CRDATE">
                                                                            <ItemStyle Width="12%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AdaptiveTestRecommendation_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate"))) %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Test Cost (in $)" SortExpression="TESTCOST">
                                                                            <HeaderStyle CssClass="td_padding_right_20" />
                                                                            <ItemStyle Width="15px" CssClass="td_padding_right_20" HorizontalAlign="Right" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AdaptiveTestRecommendation_testCostLabel" runat="server" Text='<%# Eval("TestCost") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Certification Test" SortExpression="ISCERTI">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AdaptiveTestRecommendation_testCertificaionLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsCertification"))) %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Description" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AdaptiveTestRecommendation_testDescriptionLabel" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                                                                <asp:Label ID="AdaptiveTestRecommendation_testRecommendationTimeLimitLabel" runat="server"
                                                                                    Text='<%# Eval("RecommendedCompletionTime") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:UpdatePanel ID="AdaptiveTestRecommendation_pagingUpdatePanel" runat="server">
                                                <ContentTemplate>
                                                    <uc3:PageNavigator ID="AdaptiveTestRecommendation_bottomPagingNavigator" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="AdaptiveTestRecommendation_bottomMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="AdaptiveTestRecommendation_bottomSuccessLabel" EnableViewState="false"
                            runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="AdaptiveTestRecommendation_bottomErrorLabel" runat="server" EnableViewState="false"
                            SkinID="sknErrorMessage"></asp:Label>
                        <asp:HiddenField ID="AdaptiveTestRecommendation_pageNumberHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="right" class="header_bg">
                <table width="100%" cellpadding="0" cellspacing="0" align="right" border="0">
                    <tr>
                        <td style="width: 50%;">
                        </td>
                        <td style="width: 50%;" align="right">
                            <asp:LinkButton ID="AdaptiveTestRecommendation_bottomCancelButton" runat="server"
                                SkinID="sknActionLinkButton" Text="Cancel" PostBackUrl="~/TestCenter/AdaptiveTestSummary.aspx" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="AdaptiveTestRecommendation_popUpUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Panel ID="AdaptiveTestRecommendation_questionPanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_candidate_search_test_session" Width="660px">
                                        <div style="display: none">
                                            <asp:Button ID="AdaptiveTestRecommendation_hiddenButton" runat="server" Text="Hidden" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="AdaptiveTestRecommendation_headerCreateTestSessionLiteral" runat="server"
                                                                    Text="Create Test Session"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="AdaptiveTestRecommendation_topCancelImageButton" runat="server"
                                                                                SkinID="sknCloseImageButton" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="4">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_bottomSuccessMessageLabel" runat="server"
                                                                                EnableViewState="false" SkinID="sknSuccessMessage"></asp:Label>
                                                                            <asp:Label ID="AdaptiveTestRecommendation_bottomErrorMessageLabel" runat="server"
                                                                                EnableViewState="false" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_testIDHeadLabel" runat="server" Text="Test ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_testIDLabel" runat="server" Text="TST_00000763"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_testNameHeadLabel" runat="server" Text="Test Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_testNameLabel" runat="server" Text="SQL Test"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_testCostHeadLabel" runat="server" Text="Test Cost (in $)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_testCostLabel" runat="server" Text="5.60"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_timeLimitHeadLabel" runat="server" Text="Time Limit"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_timeLimitLabel" runat="server" Text="01:30:00"
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" colspan="4">
                                                                            <asp:Label ID="AdaptiveTestRecommendation_testDescriptionLabel" runat="server" Text="Test Description"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <div style="width: 100%; height:30px; overflow:auto;" class="label_multi_field_text" id="AdaptiveTestRecommendation_testDescriptionDiv"
                                                                                runat="server">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Button ID="AdaptiveTestRecommendation_topCreateButton" runat="server" Text="Create"
                                                                    SkinID="sknButtonId" OnClick="AdaptiveTestRecommendation_CreateButton_Click" />
                                                                <asp:Button ID="AdaptiveTestRecommendation_topStartTestButton" runat="server" Text="Start Test"
                                                                    SkinID="sknButtonId" Visible="false" OnClientClick="javascript:return StartTest()" />
                                                                &nbsp;&nbsp;<asp:LinkButton ID="AdaptiveTestRecommendation_bottomCloseLinkButton"
                                                                    SkinID="sknPopupLinkButton" runat="server" Text="Cancel" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="AdaptiveTestRecommendation_questionModalPopupExtender"
                                        runat="server" PopupControlID="AdaptiveTestRecommendation_questionPanel" TargetControlID="AdaptiveTestRecommendation_hiddenButton"
                                        BackgroundCssClass="modalBackground" CancelControlID="AdaptiveTestRecommendation_topCancelImageButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="AdaptiveTestRecommendation_startTestPopupPanel" runat="server" Style="display: none;
                                        height: 206px" CssClass="popupcontrol_confirm">
                                        <div id="AdaptiveTestRecommendation_startTestHiddenDIV" style="display: none">
                                            <asp:Button ID="AdaptiveTestRecommendation_startTestHiddenButton" runat="server" />
                                        </div>
                                        <uc4:ConfirmMsgControl ID="AdaptiveTestRecommendation_startTestPopupExtenderControl"
                                            runat="server" OnOkClick="AdaptiveTestRecommendation_startTestButton_OkClick"
                                            OnCancelClick="AdaptiveTestRecommendation_startTestButton_CancelClick" />
                                        <asp:HiddenField ID="AdaptiveTestRecommendation_candidateSessionIDHiddenField" runat="server" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="AdaptiveTestRecommendation_startTestPopupExtender"
                                        runat="server" PopupControlID="AdaptiveTestRecommendation_startTestPopupPanel"
                                        TargetControlID="AdaptiveTestRecommendation_startTestHiddenButton" BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
