﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateSearchTest.aspx.cs
// File that represents the user interface for searching test page.
// This will provide some set of links are create test session,
// activate/deactivate test, edit test, delete a test in test results. 

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using AjaxControlToolkit;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// This provides set of methods and event handler for searching a test
    /// with some links. They are, CreateTestSession, EditTest, DeleteTest,
    /// Activate/Deactivate Test, ViewTest
    /// </summary>
    public partial class CandidateSearchTest : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        public const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        public const string EXPANDED_HEIGHT = "300px";

        #endregion Private Variables                                           

        /// <summary>
        /// Handler that helps to load some default values and settings
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contain the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Redirect to dashboard page.
                Response.Redirect("~/Dashboard.aspx", false);

                // Do not allow limited user.
                if (base.isLimited)
                    Response.Redirect("~/ActivityHome.aspx", false);

                // Set default button.
                Page.Form.DefaultButton = CandidateSearchTest_topSearchButton.UniqueID;

                // Show/hide menu based on user type (limited or not).
                Master.ShowMenu = !base.isLimited;

                if (!IsPostBack)
                {
                    // Set default focus field.
                    Page.Form.DefaultFocus = CandidateSearchTest_categoryTextBox.UniqueID;
                    CandidateSearchTest_categoryTextBox.Focus();

                    LoadValues();
                    CandidateSearchTest_testDiv.Visible = false;

                    // Set message, title and type for the confirmation control
                    // for start test.
                    CandidateSearchTest_startTestPopupExtenderControl.Message = "Are you ready to start the test ?";
                    CandidateSearchTest_startTestPopupExtenderControl.Title = "Start Test";
                    CandidateSearchTest_startTestPopupExtenderControl.Type = MessageBoxType.YesNo;

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                        ViewState["SORT_ORDER"] = SortType.Ascending;

                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                        ViewState["SORT_FIELD"] = "TESTKEY";
                }
                CandidateSearchTests_topErrorMessageLabel.Text = string.Empty;
                CandidateSearchTests_topSuccessMessageLabel.Text = string.Empty;
                CandidateSearchTest_topSuccessMessageLabel.Text = string.Empty;
                CandidateSearchTest_bottomSuccessMessageLabel.Text = string.Empty;
                CandidateSearchTest_topErrorMessageLabel.Text = string.Empty;
                CandidateSearchTest_bottomErrorMessageLabel.Text = string.Empty;

                CandidateSearchTest_searchCategorySubjectControl.ControlMessageThrown += new CategorySubjectControl.ControlMessageThrownDelegate(CandidateSearchTest_searchCategorySubjectControl_ControlMessageThrown);

                
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(ViewState["PAGENUMBER"]))
                    ViewState["PAGENUMBER"] = "1";

                CandidateSearchTest_bottomPagingNavigator.PageNumberClick += new
                   PageNavigator.PageNumberClickEventHandler
                      (CandidateSearchTest_pagingNavigator_PageNumberClick);

                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CandidateSearchTest_restoreHiddenField.Value) &&
                   CandidateSearchTest_restoreHiddenField.Value == "Y")
                {
                    CandidateSearchTest_searchByTestDiv.Style["display"] = "none";
                    CandidateSearchTest_searchResultsUpSpan.Style["display"] = "block";
                    CandidateSearchTest_searchResultsDownSpan.Style["display"] = "none";
                    CandidateSearchTest_testDiv.Style["height"] =EXPANDED_HEIGHT;
                }
                else
                {
                    CandidateSearchTest_searchByTestDiv.Style["display"] = "block";
                    CandidateSearchTest_searchResultsUpSpan.Style["display"] = "none";
                    CandidateSearchTest_searchResultsDownSpan.Style["display"] = "block";
                    CandidateSearchTest_testDiv.Style["height"] =RESTORED_HEIGHT;
                }
                CandidateSearchTest_searchTestResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    CandidateSearchTest_testDiv.ClientID + "','" +
                    CandidateSearchTest_searchByTestDiv.ClientID + "','" +
                    CandidateSearchTest_searchResultsUpSpan.ClientID + "','" +
                    CandidateSearchTest_searchResultsDownSpan.ClientID + "','" +
                    CandidateSearchTest_restoreHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
                foreach (MultiHandleSliderTarget target in CandidateSearchTest_testCostMultiHandleSliderExtender.MultiHandleSliderTargets)
                    target.ControlID = CandidateSearchTest_testCostMultiHandleSliderExtender.Parent.FindControl(target.ControlID).ClientID;
                CandidateSearchTest_testCostMultiHandleSliderExtender.ClientState = "0";
            }
            catch(Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Event handler that fires based on the button type.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="c">
        /// An <see cref="EventArgs"/> that contain event data.
        /// </param>
        void CandidateSearchTest_searchCategorySubjectControl_ControlMessageThrown
            (object sender, ControlMessageEventArgs c)
        {
            try
            {
                // Show message on the label.
                if (c.MessageType == MessageType.Error)
                {
                    base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                        CandidateSearchTest_bottomErrorMessageLabel, c.Message);
                }
                else if (c.MessageType == MessageType.Success)
                {
                    base.ShowMessage(CandidateSearchTest_topSuccessMessageLabel,
                        CandidateSearchTest_bottomSuccessMessageLabel, c.Message);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler that calls when a gridview column is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewSortEventArgs"/> that takes the event data.
        /// </param>
        protected void CandidateSearchTest_testGridView_Sorting
            (object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;

                // Reset and show records for first page.
                CandidateSearchTest_bottomPagingNavigator.Reset();
                LoadTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler will call when the search button is clicked to load 
        /// test details in the grid.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that takes the event data.
        /// </param>
        protected void CandidateSearchTest_topSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "TESTKEY";

                // Reset the paging control.
                CandidateSearchTest_bottomPagingNavigator.Reset();

                // By default search button click retrieves data for
                // page number 1.
                LoadTests(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handles the PageNumberClick event of the 
        /// SearchQuote_pagingNavigator control.
        /// This binds the data into the grid for the pagenumber.
        /// </summary>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e">The instance containing the event data.</param>
        protected void CandidateSearchTest_pagingNavigator_PageNumberClick(object sender,
                     PageNumberEventArgs e)
        {
            try
            {
                ViewState["PAGENUMBER"] = e.PageNumber;
                LoadTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler will fire for every mouse over on the rows and
        /// assign a javascript function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateSearchTest_testGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);

                    Label CandidateSearchTest_testIdLinkButton = (Label)e.Row.FindControl(
                      "CandidateSearchTest_testIdLinkButton");
                    
                    ImageButton CandidateSearchTest_additionalDetailsImageButton = 
                        (ImageButton)e.Row.FindControl("CandidateSearchTest_additionalDetailsImageButton");

                    CandidateSearchTest_additionalDetailsImageButton.Attributes.Add
                        ("onclick", "OpenAdditionalTestDetail('" 
                        + CandidateSearchTest_testIdLinkButton.Text + "')");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler will call when a grid column is clicked to sort
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="GridViewRowEventArgs"/> that contains the event data.
        /// </param>
        protected void CandidateSearchTest_testGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (CandidateSearchTest_testGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }

        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void CandidateSearchTest_startTestButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                "?candidatesessionid=" + CandidateSearchTest_candidateSessionIDHiddenField.Value +
                "&attemptid=1&parentpage=CS_TST",false);
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void CandidateSearchTest_startTestButton_CancelClick(object sender, EventArgs e)
        {
            try
            {
                // Reshow the window with the default values already set.
                CandidateSearchTests_topSuccessMessageLabel.Text = "Session created successfully";
                CandidateSearchTests_questionModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                    CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Interchange Advanced and Simple Search Link
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateSearchTest_simpleLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CandidateSearchTest_simpleLinkButton.Text.ToLower() == "simple")
                {
                    CandidateSearchTest_simpleLinkButton.Text = "Advanced";
                    CandidateSearchTest_simpleSearchDiv.Visible = true;
                    CandidateSearchTest_advanceSearchDiv.Visible = false;
                }
                else
                {
                    CandidateSearchTest_simpleLinkButton.Text = "Simple";
                    CandidateSearchTest_simpleSearchDiv.Visible = false;
                    CandidateSearchTest_advanceSearchDiv.Visible = true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, exp.Message);
            }
            finally { }
        }

        private void LoadTests(int pageNumber)
        {
            int totalRecords = 0;
            TestSearchCriteria testSearchCriteria = new TestSearchCriteria();
            testSearchCriteria.IsCertification = true;

            if (CandidateSearchTest_simpleLinkButton.Text == "Advanced")
            {
                testSearchCriteria.Category = CandidateSearchTest_categoryTextBox.Text.Trim() == "" ?
                    null : CandidateSearchTest_categoryTextBox.Text.Trim();
                testSearchCriteria.Subject = CandidateSearchTest_subjectTextBox.Text.Trim() == "" ?
                    null : CandidateSearchTest_subjectTextBox.Text.Trim();
                testSearchCriteria.Keyword = CandidateSearchTest_simpleKeywordTextBox.Text.Trim() == "" ?
                    null : CandidateSearchTest_simpleKeywordTextBox.Text.Trim();
            }
            else
            {
                testSearchCriteria.isSufficientCredits = null;
                string SelectedSubjectIDs = "";
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(CandidateSearchTest_searchCategorySubjectControl.SubjectDataSource))
                {
                    List<Category> categoryList = new List<Category>();
                    foreach (Subject subject in CandidateSearchTest_searchCategorySubjectControl.SubjectDataSource)
                    {

                        if (subject.IsSelected == true)
                        {
                            Category category = new Category();
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                            category.CategoryID = subject.CategoryID;
                            categoryList.Add(category);
                        }

                    }
                    ///Get all Subject ids in  Un selected Subject List.
                    Category categoryID = new Category();
                    foreach (Subject subject in CandidateSearchTest_searchCategorySubjectControl.SubjectDataSource)
                    {
                        categoryID = categoryList.Find(p => p.CategoryID == subject.CategoryID);
                        if (categoryID == null)
                            SelectedSubjectIDs = SelectedSubjectIDs + subject.SubjectID.ToString() + ",";
                    }
                }
                testSearchCriteria.CategoriesID = SelectedSubjectIDs == "" ?
                    null : SelectedSubjectIDs.TrimEnd(',');
                testSearchCriteria.Keyword = CandidateSearchTest_advancedKeywordTextBox.Text.Trim() == "" ?
                    null : CandidateSearchTest_advancedKeywordTextBox.Text.Trim();
                testSearchCriteria.TestCostStart = int.Parse(CandidateSearchTest_testCostMinValueTextBox.Text.Trim());
                testSearchCriteria.TestCostEnd = int.Parse(CandidateSearchTest_testCostMaxValueTextBox.Text.Trim());
                if (CandidateSearchTest_sufficientCreditsCheckBox.Checked)
                    testSearchCriteria.isSufficientCredits = new CreditBLManager().GetCreditSummary(userID).AvailableCredits;
            }
            List<TestDetail> testDetail = new BL.ReportBLManager().GetTests
                (testSearchCriteria, base.GridPageSize, pageNumber, 
                 ViewState["SORT_FIELD"].ToString(),
                ((SortType) ViewState["SORT_ORDER"]), out totalRecords);

            if (testDetail == null || testDetail.Count == 0)
            {
                base.ShowMessage(CandidateSearchTest_topErrorMessageLabel,
                CandidateSearchTest_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
                CandidateSearchTest_testGridView.DataSource = null;
                CandidateSearchTest_testGridView.DataBind();
                CandidateSearchTest_bottomPagingNavigator.TotalRecords = 0;
                CandidateSearchTest_testDiv.Visible = false;
            }
            else
            {
                CandidateSearchTest_testGridView.DataSource = testDetail;
                CandidateSearchTest_testGridView.DataBind();
                CandidateSearchTest_bottomPagingNavigator.PageSize = base.GridPageSize;
                CandidateSearchTest_bottomPagingNavigator.TotalRecords = totalRecords;
                CandidateSearchTest_testDiv.Visible = true;
            }
        }

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {

            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            Master.SetPageCaption(Resources.HCMResource.CandidateSearchTest_Title);
        }

        #endregion Protected Overridden Methods

        protected void CandidateSearchTest_ResetLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, false);
        }
        protected void CandidateSearchTest_testGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "createSession")
            {
                // Clear the candidate session ID.
                CandidateSearchTest_candidateSessionIDHiddenField.Value = string.Empty;
                TestDetail testDetail = new TestDetail();
                testDetail=new TestBLManager().GetTestAndCertificateDetail(e.CommandArgument.ToString());
                CandidateSearchTests_testIDTextBox.Text = testDetail.TestKey;
                CandidateSearchTests_testNameTextBox.Text = testDetail.Name;
                CandidateSearchTests_testNameTextBox.ToolTip = testDetail.Name;
                CandidateSearchTests_testCostTextBox.Text = testDetail.TestCost.ToString();
                CandidateSearchTests_timeLimitTextBox.Text = Forte.HCM.Support.Utility.
                    ConvertSecondsToHoursMinutesSeconds(testDetail.RecommendedCompletionTime).ToString();
                CandidateSearchTests_authorRecommendedCompletionTimeHiddenField.Value = testDetail.RecommendedCompletionTime.ToString();
                CandidateSearchTests_testDescriptionLiteral.Text = testDetail.Description;
                CandidateSearchTests_topStartTestButton.Visible = false;
                CandidateSearchTests_topCreateButton.Visible = true;
                CandidateSearchTests_questionModalPopupExtender.Show();
            }
        }

        private TestSessionDetail SaveTestSessionDetails()
        {
            // Initialize test session detail object
            TestSessionDetail testSessionDetail = new TestSessionDetail();
            // Set test name
            testSessionDetail.TestID = CandidateSearchTests_testIDTextBox.Text;
            testSessionDetail.TestName = CandidateSearchTests_testIDTextBox.Text;

            // Set number of candidate session (session count)
            testSessionDetail.NumberOfCandidateSessions = 1;

            // Set total credits limit
            testSessionDetail.TotalCredit =
                Convert.ToDecimal(CandidateSearchTests_testCostTextBox.Text);

            // Set time limit
            testSessionDetail.TimeLimit = Convert.ToInt32
                (CandidateSearchTests_authorRecommendedCompletionTimeHiddenField.Value);

            testSessionDetail.ClientRequestID = "0";

            // Set expiry date
            testSessionDetail.ExpiryDate = DateTime.Now;

            // Set random question order status
            testSessionDetail.IsRandomizeQuestionsOrdering = false;

            // Set display result status 
            testSessionDetail.IsDisplayResultsToCandidate = true;

            // Set cyber proctoring status
            testSessionDetail.IsCyberProctoringEnabled = false;

            // Set created by
            testSessionDetail.CreatedBy = base.userID;

            // Set modified by
            testSessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            testSessionDetail.Instructions = Resources.HCMResource.CandidateSearchTest_Instructions.ToString();

            // Set session descriptions
            testSessionDetail.TestSessionDesc = Resources.HCMResource.CandidateSearchTest_TestSessionDesc.ToString();

            return testSessionDetail;
        }

        protected void CandidateSearchTests_topCreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                TestSessionDetail testSessionDetail = SaveTestSessionDetails();
                TestScheduleDetail testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = base.userID.ToString();
                //  testSchedule.CandidateTestSessionID = candidateSessionID.ToString();
                testSchedule.AttemptID = 1;
                //testSchedule.EmailId = new CommonBLManager().GetUserDetail(base.userID).Email;
                testSchedule.EmailId = ((UserDetail)Session["USER_DETAIL"]).Email;
                testSchedule.ExpiryDate = DateTime.Now.AddDays(base.candidateExpiryDays);
                //   new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID);
                //string testSessionID = null;
                string candidateSessionID = null;
                //// Call save test session BL method.
                new TestBLManager().SaveTestSessionScheduleCandidate(testSessionDetail, testSchedule, base.userID, out candidateSessionID);

                CandidateSearchTests_topStartTestButton.Visible = true;
                CandidateSearchTests_topCreateButton.Visible = false;
                CandidateSearchTests_topSuccessMessageLabel.Text = "Session created successfully";
                CandidateSearchTest_candidateSessionIDHiddenField.Value = candidateSessionID;
                CandidateSearchTests_questionModalPopupExtender.Show();
            }
            catch(Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearchTests_topErrorMessageLabel.Text = exp.Message;
                CandidateSearchTests_questionModalPopupExtender.Show();
            }
        }
    }
}
