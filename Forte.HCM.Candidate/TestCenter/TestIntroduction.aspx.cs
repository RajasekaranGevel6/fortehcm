﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestIntroduction.cs
// File that represents the user interface for test introduction page.
// This will display the test details and instructions before start the
// test. From here, candidate can start their test as well as set reminder.

#endregion Header                                                              

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface for test introduction page.
    /// This will display the test details and instructions before start the
    /// test. From here, candidate can start their test as well as set reminder.
    /// This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class TestIntroduction : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when the page gets loaded. This handler
        /// loads the default values and set javascript functions to the 
        /// buttons.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                // Set browser title.
                Master.SetPageCaption("Test Introduction");

                if (!IsPostBack)
                {
                    if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
                    {
                        // Load test details.
                        LoadValues();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(TestIntroduction_topErrorMessageLabel,
                    TestIntroduction_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the start test button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void TestIntroduction_startTestButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                    "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                    "&attemptid=" + Request.QueryString["attemptid"] +
                    "&displaytype=" + Request.QueryString["displaytype"] +
                    "&parentpage=" + Constants.ParentPage.TEST_INTRODUCTION +
                    "&sourcepage=" + Request.QueryString["parentpage"], false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(TestIntroduction_topErrorMessageLabel,
                    TestIntroduction_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the remind me link button 
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test reminder page.
        /// </remarks>
        protected void TestIntroduction_remindMeLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Popup/TestReminder.aspx" +
                    "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                    "&attemptid=" + Request.QueryString["attemptid"] +
                    "&displaytype=" + Request.QueryString["displaytype"] +
                    "&parentpage=" + Constants.ParentPage.TEST_INTRODUCTION +
                    "&activitytype=TST" +
                    "&sourcepage=" + Request.QueryString["parentpage"], false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(TestIntroduction_topErrorMessageLabel,
                    TestIntroduction_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        #endregion Event Handlers                                              

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            TestIntroductionDetail testIntro = new CandidateBLManager().
                GetTestIntroduction(Request.QueryString["candidatesessionid"]);

            if (testIntro == null)
                return;

            TestIntroduction_testNameValueLabel.Text = testIntro.TestName;
            TestIntroduction_testDescriptionValueLabel.Text = 
                testIntro.TestDescription == null ? testIntro.TestDescription : 
                    testIntro.TestDescription.ToString().Replace(Environment.NewLine, "<br />");

            TestIntroduction_testAreasValueLabel.Text = testIntro.TestAreas;
            TestIntroduction_testSubjectsValueLabel.Text = testIntro.TestSubjects;
            TestIntroduction_durationValueLabel.Text = Utility.
                ConvertSecondsToHoursMinutesSeconds(testIntro.TimeLimit);

            TestIntroduction_testInstructionsLiteral.Text =
                testIntro.TestInstructions == null ? testIntro.TestInstructions : 
                testIntro.TestInstructions.ToString().Replace(Environment.NewLine, "<br />");

                
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            TestIntroduction_topErrorMessageLabel.Text = string.Empty;
            TestIntroduction_bottomErrorMessageLabel.Text = string.Empty;
            TestIntroduction_topSuccessMessageLabel.Text = string.Empty;
            TestIntroduction_bottomSuccessMessageLabel.Text = string.Empty;

            TestIntroduction_topErrorMessageLabel.Visible = false;
            TestIntroduction_bottomErrorMessageLabel.Visible = false;
            TestIntroduction_topSuccessMessageLabel.Visible = false;
            TestIntroduction_bottomSuccessMessageLabel.Visible = false;
        }

        #endregion Protected Overridden Methods                                
    }
}