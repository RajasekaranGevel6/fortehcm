<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HCMCandidateMaster.Master"
    CodeBehind="CandidateSearchTest.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.CandidateSearchTest" %>

<%@ Register Src="~/CommonControls/CategorySubjectControl.ascx" TagName="CategorySubjectControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/HCMCandidateMaster.Master" %>
<asp:Content ID="SearchTestContent_bodyContent" runat="server" ContentPlaceHolderID="HCMCandidateMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

        // Method that will show the start test confirmation window.
        function StartTest() {
            $find("<%= CandidateSearchTest_startTestPopupExtender.ClientID  %>").show();
            return false;
        }
    
    </script>

    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="CandidateSearchTest_headerLiteral" runat="server" Text="Search Test"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="CandidateSearchTest_topResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CandidateSearchTest_ResetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="CandidateSearchTest_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="CandidateSearchTest_topMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="CandidateSearchTest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="CandidateSearchTest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div id="CandidateSearchTest_searchByTestDiv" runat="server" style="display: block;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="right" class="td_padding_bottom_5">
                                            <asp:UpdatePanel runat="server" ID="CandidateSearchTest_simpleLinkUpdatePanel">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="CandidateSearchTest_simpleLinkButton" runat="server" Text="Advanced"
                                                        SkinID="sknActionLinkButton" OnClick="CandidateSearchTest_simpleLinkButton_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="panel_bg">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:UpdatePanel runat="server" ID="CandidateSearchTest_SearchDivUpdatePanel">
                                                            <ContentTemplate>
                                                                <div id="CandidateSearchTest_simpleSearchDiv" runat="server" style="width: 100%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="CandidateSearchTest_categoryHeadLabel" runat="server" Text="Category"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="CandidateSearchTest_categoryTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="CandidateSearchTest_subjectHeadLabel" runat="server" Text="Subject"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="CandidateSearchTest_subjectTextBox" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="CandidateSearchTest_keywordsHeadLabel" runat="server" Text="Keyword"
                                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <div style="float: left; padding-right: 5px;">
                                                                                    <asp:TextBox ID="CandidateSearchTest_simpleKeywordTextBox" runat="server"></asp:TextBox></div>
                                                                                <div style="float: left;">
                                                                                    <asp:ImageButton ID="CandidateSearchTest_keywordsImageButton" SkinID="sknHelpImageButton"
                                                                                        runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip="<%$ Resources:HCMResource, Search_Test_KeywordHelp %>" /></div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="CandidateSearchTest_advanceSearchDiv" runat="server" visible="false" style="width: 100%">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <uc1:CategorySubjectControl ID="CandidateSearchTest_searchCategorySubjectControl"
                                                                                    runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_height_8">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="panel_inner_body_bg">
                                                                                    <tr>
                                                                                        <td style="width: 2%;">
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="CandidateSearchTest_keywordHeadLabel" runat="server" Text="Keyword"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:TextBox ID="CandidateSearchTest_advancedKeywordTextBox" runat="server"></asp:TextBox>
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="CandidateSearchTest_keywordImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" ToolTip='<%$ Resources:HCMResource, Search_Test_KeywordHelp %>' />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 8%">
                                                                                            <asp:Label ID="CandidateSearchTest_testCostHeadLabel" runat="server" Text="Test Cost"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="CandidateSearchTest_testCostMinValueTextBox" runat="server" MaxLength="5"
                                                                                                            Width="25"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td style="width: 120px;" valign="middle">
                                                                                                        <asp:TextBox ID="CandidateSearchTest_testCostTextBox" runat="server" MaxLength="250"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td valign="middle">
                                                                                                        <div style="float: left; padding-right: 5px;">
                                                                                                            <asp:TextBox ID="CandidateSearchTest_testCostMaxValueTextBox" runat="server" MaxLength="5"
                                                                                                                Width="25"></asp:TextBox></div>
                                                                                                        <div style="float: left;">
                                                                                                            <asp:ImageButton ID="CandidateSearchTest_helpCostImageButton" SkinID="sknHelpImageButton"
                                                                                                                runat="server" ImageAlign="AbsMiddle" OnClientClick="javascript:return false;"
                                                                                                                ToolTip="Helps to provide the cost of test for searching" />
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <ajaxToolKit:MultiHandleSliderExtender ID="CandidateSearchTest_testCostMultiHandleSliderExtender"
                                                                                                runat="server" Enabled="True" HandleAnimationDuration="0.1" Maximum="100" Minimum="0"
                                                                                                RailCssClass="slider_rail" HandleCssClass="slider_handler" TargetControlID="CandidateSearchTest_testCostTextBox"
                                                                                                ShowHandleDragStyle="true" ShowHandleHoverStyle="true">
                                                                                                <MultiHandleSliderTargets>
                                                                                                    <ajaxToolKit:MultiHandleSliderTarget ControlID="CandidateSearchTest_testCostMinValueTextBox"
                                                                                                        HandleCssClass="MultiHandleMinSliderCss" />
                                                                                                    <ajaxToolKit:MultiHandleSliderTarget ControlID="CandidateSearchTest_testCostMaxValueTextBox"
                                                                                                        HandleCssClass="MultiHandleMaxSliderCss" />
                                                                                                </MultiHandleSliderTargets>
                                                                                            </ajaxToolKit:MultiHandleSliderExtender>
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <asp:Label ID="CandidateSearchTest_sufficientCreditsLabel" runat="server" Text="Check for sufficient credits"
                                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                        </td>
                                                                                        <td align="left" style="width: 8%;">
                                                                                            <div style="float: left; padding-right: 5px;">
                                                                                                <asp:CheckBox ID="CandidateSearchTest_sufficientCreditsCheckBox" runat="server" />
                                                                                            </div>
                                                                                            <div style="float: left;">
                                                                                                <asp:ImageButton ID="CandidateSearchTest_helpCreditsImageButton" SkinID="sknHelpImageButton"
                                                                                                    runat="server" ImageAlign="Top" OnClientClick="javascript:return false;" ToolTip="Helps to find the tests for which you have sufficient credits." />
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 2%;">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="td_padding_top_5">
                                                        <asp:Button ID="CandidateSearchTest_topSearchButton" runat="server" Text="Search"
                                                            SkinID="sknButtonId" OnClick="CandidateSearchTest_topSearchButton_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_8">
                        </td>
                    </tr>
                    <tr id="CandidateSearchTest_searchTestResultsTR" runat="server">
                        <td class="header_bg">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 98%" align="left" class="header_text_bold">
                                        <asp:Literal ID="CandidateSearchTest_searchResultsLiteral" runat="server" Text="Search Results"></asp:Literal>&nbsp;<asp:Label
                                            ID="CandidateSearchTest_sortHelpLabel" runat="server" SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                    </td>
                                    <td style="width: 2%" align="right">
                                        <span id="CandidateSearchTest_searchResultsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="CandidateSearchTest_searchResultsUpImage" runat="server" SkinID="sknMinimizeImage" />
                                        </span><span id="CandidateSearchTest_searchResultsDownSpan" runat="server" style="display: block;">
                                            <asp:Image ID="CandidateSearchTest_searchResultsDownImage" runat="server" SkinID="sknMaximizeImage" />
                                        </span>
                                        <asp:HiddenField ID="CandidateSearchTest_restoreHiddenField" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid_body_bg">
                            <asp:UpdatePanel runat="server" ID="CandidateSearchTest_testGridViewUpdatePanel">
                                <ContentTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left">
                                                <div style="height: 200px; overflow: auto;" runat="server" id="CandidateSearchTest_testDiv"
                                                    visible="false">
                                                    <asp:GridView ID="CandidateSearchTest_testGridView" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" GridLines="None" Width="100%" OnSorting="CandidateSearchTest_testGridView_Sorting"
                                                        OnRowDataBound="CandidateSearchTest_testGridView_RowDataBound" OnRowCreated="CandidateSearchTest_testGridView_RowCreated"
                                                        OnRowCommand="CandidateSearchTest_testGridView_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="7%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="CandidateSearchTest_additionalDetailsImageButton" runat="server"
                                                                        SkinID="sknAdditionalDetailImageButton" ToolTip="Test Statistics" />
                                                                    &nbsp;<asp:ImageButton ID="CandidateSearchTests_questionModalPopupExtender" CommandArgument='<%# Eval("TestKey") %>'
                                                                        CommandName="createSession" runat="server" SkinID="sknCreateNewSessionImageButton"
                                                                        ToolTip="Create Test Session" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test&nbsp;ID" SortExpression="TESTKEY">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CandidateSearchTest_testIdLinkButton" runat="server" Text='<%# Eval("TestKey") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="9%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Name" SortExpression="TESTNAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CandidateSearchTest_testNameLabel" runat="server" Text='<%# TrimContent(Eval("Name").ToString(),35) %>'
                                                                        ToolTip='<%# Eval("Name") %>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20%" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderStyle-CssClass="td_padding_right_20" HeaderText="No of Questions"
                                                                DataField="NoOfQuestions" SortExpression="TOTALQUESTION">
                                                                <ItemStyle Width="1%" CssClass="td_padding_right_20" HorizontalAlign="right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Created Date" SortExpression="CREATEDDATE" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TestReport_testCreatedDateLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Cost (in&nbsp;$)" HeaderStyle-CssClass="td_padding_right_20"
                                                                DataField="TestCost" SortExpression="TESTCOST">
                                                                <ItemStyle Width="1%" CssClass="td_padding_right_20" HorizontalAlign="right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Certification Test" SortExpression="CERTIFICATION">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CandidateSearchTest_testCertificationLabel" runat="server" Text='<%# GetExpansion(Convert.ToBoolean(Eval("IsCertification")))%>'>
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="10%" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <uc2:PageNavigator ID="CandidateSearchTest_bottomPagingNavigator" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="CandidateSearchTest_topSearchButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel runat="server" ID="CandidateSearchTest_bottomMessageUpdatePanel">
                    <ContentTemplate>
                        <asp:Label ID="CandidateSearchTest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label><asp:Label
                            ID="CandidateSearchTest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                                ID="CandidateSearchTest_authorIdHiddenField" runat="server" />
                        <asp:HiddenField ID="CandidateSearchTest_clientRequestHiddenField" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%">
                            &nbsp;
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:LinkButton ID="CandidateSearchTest_bottomResetLinkButton" runat="server" Text="Reset"
                                            SkinID="sknActionLinkButton" OnClick="CandidateSearchTest_ResetLinkButton_Click" />
                                    </td>
                                    <td width="4%" align="center">
                                        |
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="CandidateSearchTest_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknActionLinkButton" OnClick="ParentPageRedirect" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CandidateSearchTests_questionPanel_UpdatePaenl" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="CandidateSearchTests_questionPanel" runat="server" Style="display: none"
                            CssClass="popupcontrol_candidate_search_test_session" Width="660px">
                            <div style="display: none;">
                                <asp:Button ID="CandidateSearchTests_hiddenButton" runat="server" Text="Hidden" />
                            </div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td_height_20">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                    <asp:Literal ID="CandidateSearchTests_headerLiteral" runat="server" Text="Create Test Session"></asp:Literal>
                                                </td>
                                                <td style="width: 50%" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="TestScheduleControl_topCancelImageButton" runat="server" SkinID="sknCloseImageButton" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                    <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg">
                                                        <tr>
                                                            <td class="msg_align" colspan="4">
                                                                <asp:Label ID="CandidateSearchTests_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                                <asp:Label ID="CandidateSearchTests_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 17%">
                                                                <asp:Label ID="CandidateSearchTests_testIDLabel" runat="server" Text="Test ID" SkinID="sknLabelFieldHeaderText"> </asp:Label>
                                                            </td>
                                                            <td align="left" style="width: 17%">
                                                                <asp:Label ID="CandidateSearchTests_testIDTextBox" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                            <td align="left" style="width: 17%">
                                                                <asp:Label ID="CandidateSearchTests_testNameLabel" runat="server" Text="Test Name"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td align="left" style="width: 49%">
                                                                <asp:Label ID="CandidateSearchTests_testNameTextBox" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 17%">
                                                                <asp:Label ID="CandidateSearchTests_testCostLabel" runat="server" Text="Test Cost (in $)"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td align="left" style="width: 17%">
                                                                <asp:Label ID="CandidateSearchTests_testCostTextBox" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                            </td>
                                                            <td align="left" style="width: 17%">
                                                                <asp:Label ID="CandidateSearchTests_timeLimitLabel" runat="server" Text="Time Limit"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                            <td align="left" style="width: 49%">
                                                                <asp:Label ID="CandidateSearchTests_timeLimitTextBox" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                <asp:HiddenField ID="CandidateSearchTests_authorRecommendedCompletionTimeHiddenField"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="4">
                                                                <asp:Label ID="CandidateSearchTests_testDescriptionLabel" runat="server" Text="Test Description"
                                                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <div style="width: 100%; height: 25px; overflow: auto" class="label_multi_field_text">
                                                                    <asp:Literal ID="CandidateSearchTests_testDescriptionLiteral" runat="server" Text="Screening candidates for Yes Banking project. The candidates must have minimum 3 years experience in the field of ASP.Net UI design, Java Script, C# Programming & OOPS."></asp:Literal>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Button ID="CandidateSearchTests_topCreateButton" runat="server" Text="Create"
                                                        SkinID="sknButtonId" OnClick="CandidateSearchTests_topCreateButton_Click" />
                                                    <asp:Button ID="CandidateSearchTests_topStartTestButton" runat="server" Text="Start Test"
                                                        SkinID="sknButtonId" Visible="false" OnClientClick="javascript:return StartTest()" />
                                                    &nbsp;&nbsp;<asp:LinkButton ID="TestScheduleControl_bottomCloseLinkButton" SkinID="sknPopupLinkButton"
                                                        runat="server" Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CandidateSearchTests_questionModalPopupExtender"
                            runat="server" PopupControlID="CandidateSearchTests_questionPanel" TargetControlID="CandidateSearchTests_hiddenButton"
                            BackgroundCssClass="modalBackground" CancelControlID="TestScheduleControl_topCancelImageButton">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:Panel ID="CandidateSearchTest_startTestPopupPanel" runat="server" Style="display: none;
                            height: 206px" CssClass="popupcontrol_confirm">
                            <div id="CandidateSearchTest_startTestHiddenDIV" style="display: none">
                                <asp:Button ID="CandidateSearchTest_startTestHiddenButton" runat="server" />
                            </div>
                            <uc3:ConfirmMsgControl ID="CandidateSearchTest_startTestPopupExtenderControl" runat="server"
                                OnOkClick="CandidateSearchTest_startTestButton_OkClick" OnCancelClick="CandidateSearchTest_startTestButton_CancelClick" />
                            <asp:HiddenField ID="CandidateSearchTest_candidateSessionIDHiddenField" runat="server" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CandidateSearchTest_startTestPopupExtender" runat="server"
                            PopupControlID="CandidateSearchTest_startTestPopupPanel" TargetControlID="CandidateSearchTest_startTestHiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
