﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyCredits.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.MyCredits"
    MasterPageFile="~/MasterPages/HCMCandidateMaster.Master" %>

<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/HCMCandidateMaster.Master" %>
<asp:Content ID="MyCredits_bodyContent" runat="server" ContentPlaceHolderID="HCMCandidateMaster_contentPlaceHolder">
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%" class="header_text_bold">
                            <asp:Literal ID="MyCredits_headerLiteral" runat="server" Text="My Credits"></asp:Literal>
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:Button ID="MyCredits_topPurchaseCreditsButton" runat="server" SkinID="sknButtonId"
                                            Text="Purchase Credits" />
                                    </td>
                                    <td width="4%" align="center" class="link_button">
                                        &nbsp;
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="MyCredits_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" PostBackUrl="~/Dashboard.aspx" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyCredits_messageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="MyCredits_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="MyCredits_creditsTypeDropDownList" />
                        <asp:AsyncPostBackTrigger ControlID="MyCredits_bottomPageNavigationControl" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            <div id="MyCredits_creditsDiv" runat="server" style="display: block;">
                                <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
                                    <tr>
                                        <td style="width: 7%">
                                            <asp:Label ID="MyCredits_userIdLabel" runat="server" Text="User Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </td>
                                        <td style="width: 12%">
                                            <asp:Label ID="MyCredits_userIdValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                        </td>
                                        <td style="width: 14%">
                                            <asp:Label ID="MyCredits_creditsLabel" runat="server" Text="Available Credits (in $)"
                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </td>
                                        <td style="width: 7%">
                                            <asp:Label ID="MyCredits_availableCreditsValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                        </td>
                                        <td style="width: 14%">
                                            <asp:Label ID="MyCredits_creditsEarnedLabel" runat="server" Text="Credits Earned (in $)"
                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </td>
                                        <td style="width: 7%">
                                            <asp:Label ID="MyCredits_creditsEarnedValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                        </td>
                                        <td style="width: 15%">
                                            <asp:Label ID="MyCredits_creditsRedeemedLabel" runat="server" Text="Credits Redeemed (in $)"
                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </td>
                                        <td style="width: 24%">
                                            <asp:Label ID="MyCredits_creditsRedeemedValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_height_8" colspan="10">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_2">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="MyCredits_updatePanel" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr id="MyCredits_creditsUsageHistoryResultsTR" runat="server">
                                            <td class="header_bg">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="width: 93%" align="left">
                                                            <asp:Literal ID="MyCredits_creditsHistoryLiteral" runat="server" Text="Credits Usage History"></asp:Literal>
                                                            &nbsp;<asp:Label ID="MyCredits_sortHelpLabel" runat="server" SkinID="sknLabelText"
                                                                Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>
                                                        </td>
                                                        <td align="right" id="MyCredits_creditsHistoryHeaderTD" runat="server">
                                                            <span id="MyCredits_creditsHistoryUpSpan" style="display: none;" runat="server">
                                                                <asp:Image ID="MyCredits_creditsHistoryUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                                    runat="server" id="MyCredits_creditsHistoryDownSpan" style="display: block;"><asp:Image
                                                                        ID="MyCredits_creditsHistoryDownImage" runat="server" SkinID="sknMaximizeImage" /></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="grid_body_bg">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Label ID="MyCredits_creditstypeLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                Text="Credit Type"></asp:Label>
                                                            <asp:DropDownList ID="MyCredits_creditsTypeDropDownList" runat="server" AutoPostBack="true"
                                                                OnSelectedIndexChanged="MyCredits_creditsTypeDropDownList_SelectedIndexChanged"
                                                                Width="8%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="height: 200px; overflow: auto" id="MyCredits_creditsHistoryDiv" runat="server">
                                                                <asp:GridView ID="MyCredits_creditsGridView" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    OnSorting="MyCredits_creditsGridView_Sorting" OnRowDataBound="MyCredits_creditsGridView_RowDataBound"
                                                                    OnRowCreated="MyCredits_creditsGridView_RowCreated">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="DATE" DataFormatString="{0:MM/dd/yyyy}" />
                                                                        <asp:BoundField DataField="Credit" HeaderText="Credits (in $)" SortExpression="CREDIT"
                                                                            HeaderStyle-CssClass="td_padding_right_20" ItemStyle-CssClass="td_padding_right_20"
                                                                            ItemStyle-Width="25px" ItemStyle-HorizontalAlign="right" />
                                                                        <asp:BoundField DataField="UsageHistoryCreditType" HeaderText="Credit Type" SortExpression="USAGEHISTORYCREDITTYPE" />
                                                                        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="DESCRIPTION">
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <uc3:PageNavigator ID="MyCredits_bottomPageNavigationControl" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="MyCredits_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="72%">
                            &nbsp;
                        </td>
                        <td width="28%" align="right">
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td style="width: 62%">
                                        &nbsp;
                                    </td>
                                    <td width="16%" align="right">
                                        <asp:Button ID="MyCredits_bottomPurchaseCreditsButton" runat="server" SkinID="sknButtonId"
                                            Text="Purchase Credits" />
                                    </td>
                                    <td width="4%" align="center">
                                        &nbsp;
                                    </td>
                                    <td width="18%" align="left">
                                        <asp:LinkButton ID="MyCredits_bottomCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                            Text="Cancel" PostBackUrl="~/Dashboard.aspx" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="MyCredits_isMaximizedHiddenField" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
