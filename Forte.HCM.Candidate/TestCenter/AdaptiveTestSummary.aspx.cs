﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdaptiveTestSummary.aspx.cs
// File that represents the user interface for adaptive test summary page.

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface for adaptive test summary page.
    /// </summary>
    public partial class AdaptiveTestSummary : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler that helps to load some default values and settings
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contain the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Redirect to dashboard page.
                Response.Redirect("~/Dashboard.aspx", false);

                // Do not allow limited user.
                if (base.isLimited)
                    Response.Redirect("~/ActivityHome.aspx", false);

                Master.SetPageCaption(Resources.HCMResource.CandidateCenter_AdaptiveTests_Title);

                // Show/hide menu based on user type (limited or not).
                Master.ShowMenu = !base.isLimited;

                if (!IsPostBack)
                    LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                    AdaptiveTestSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListCommandEventArgs"/> that holds the event data.
        /// </param>
        protected void AdaptiveTestSummary_similiarCandidateTestDataList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "CreateSession")
                    return;
                AdaptiveTestSummary_testPreviewTestNameLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarCandidateTestNameHiddenField")).Value;
                AdaptiveTestSummary_testDescriptionDiv.InnerHtml = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarCandidateTestDescriptionHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewTestIDLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarCandidateTestKeyHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewTimeLimitLabel.Text = Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSeconds
                    (Convert.ToInt32(((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarCandidateTimeLimitHiddenField")).Value));
                AdaptiveTestSummary_testPreviewTestCostLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarCandidateTestCostHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewStartTestButton.Visible = false;
                AdaptiveTestSummary_testPreviewCreateButton.Visible = true;
                AdaptiveTestSummary_testPreviewModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                    AdaptiveTestSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the create test from pop up window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AdaptiveTestSummary_testPreviewCreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateTestSession();
                LoadAdaptiveLists(false);
                ResetPagingHiddenControls();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                    AdaptiveTestSummary_bottomErrorMessageLabel, exp.Message);
            }
            finally
            {
                AdaptiveTestSummary_updatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void AdaptiveTestSummary_startTestButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                "?candidatesessionid=" + AdaptiveTestSummary_testCandidateSessionIdHiddenField.Value +
                "&attemptid=1&parentpage=" + Forte.HCM.Support.Constants.ParentPage.CANDIDATE_ADAPTIVE_SUMMARY,
                false);
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void AdaptiveTestSummary_startTestButton_CancelClick(object sender, EventArgs e)
        {
            try
            {
                // Reshow the window with the default values already set.
                AdaptiveTestSummary_testPreviewTopSuccessMessageLabel.Text = "Session created successfully";
                AdaptiveTestSummary_testPreviewModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                     AdaptiveTestSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that holds the next button(s) are clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AdaptiveTestSummary_nextImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                switch (((ImageButton)sender).CommandName)
                {
                    case "BasedOnProfile":
                        AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value =
                            (Convert.ToInt32(AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value) + 1).ToString();
                        LoadCandidateProfileTest(Convert.ToInt32(AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value), false);
                        break;
                    case "BasedOnAssessment":
                        AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value =
                            (Convert.ToInt32(AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value) + 1).ToString();
                        LoadCandidateAssessmentHistoryTests(Convert.ToInt32(AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value), false);
                        break;
                    case "BasedOnTests":
                        AdaptiveTestSummary_similarCandidatePagingHiddenField.Value =
                            (Convert.ToInt32(AdaptiveTestSummary_similarCandidatePagingHiddenField.Value) + 1).ToString();
                        LoadCandidateSimiliarTest(Convert.ToInt32(AdaptiveTestSummary_similarCandidatePagingHiddenField.Value), false);
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                     AdaptiveTestSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that holds the previous image button(s) are clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AdaptiveTestSummary_previousButtonClick(object sender, ImageClickEventArgs e)
        {
            try
            {
                switch (((ImageButton)sender).CommandName)
                {
                    case "BasedOnProfile":
                        if (AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value == "1")
                        {
                            base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                                    AdaptiveTestSummary_bottomErrorMessageLabel,
                                    string.Format(Resources.HCMResource.
                                    AdaptiveTestSummary_PreviousNavigationSummaryMessage, "based on profile"));
                            break;
                        }
                        AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value =
                            (Convert.ToInt32(AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value) - 1).ToString();
                        LoadCandidateProfileTest(Convert.ToInt32(AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value), false);
                        break;
                    case "BasedOnAssessment":
                        if (AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value == "1")
                        {
                            base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                                    AdaptiveTestSummary_bottomErrorMessageLabel,
                                    string.Format(Resources.HCMResource.
                                    AdaptiveTestSummary_PreviousNavigationSummaryMessage, "based on assessment history"));
                            break;
                        }
                        AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value =
                            (Convert.ToInt32(AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value) - 1).ToString();
                        LoadCandidateAssessmentHistoryTests(Convert.ToInt32(AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value), false);
                        break;
                    case "BasedOnTests":
                        if (AdaptiveTestSummary_similarCandidatePagingHiddenField.Value == "1")
                        {
                            base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                                   AdaptiveTestSummary_bottomErrorMessageLabel,
                                   string.Format(Resources.HCMResource.
                                   AdaptiveTestSummary_PreviousNavigationSummaryMessage, "based on similar candidate"));
                            break;
                        }
                        AdaptiveTestSummary_similarCandidatePagingHiddenField.Value =
                            (Convert.ToInt32(AdaptiveTestSummary_similarCandidatePagingHiddenField.Value) - 1).ToString();
                        LoadCandidateSimiliarTest(Convert.ToInt32(AdaptiveTestSummary_similarCandidatePagingHiddenField.Value), false);
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                     AdaptiveTestSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void AdaptiveTestSummary_basedOnProfileDatalist_ItemCommand(object sender,
            DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "CreateSession")
                    return;
                AdaptiveTestSummary_testPreviewTestNameLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarProfileTestNameHiddenField")).Value;
                AdaptiveTestSummary_testDescriptionDiv.InnerHtml = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarProfileTestDescriptionHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewTestIDLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarProfileTestKeyHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewTimeLimitLabel.Text = Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSeconds
                    (Convert.ToInt32(((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarProfileTimeLimitHiddenField")).Value));
                AdaptiveTestSummary_testPreviewTestCostLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_similarProfileTestCostHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewStartTestButton.Visible = false;
                AdaptiveTestSummary_testPreviewCreateButton.Visible = true;
                AdaptiveTestSummary_testPreviewModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                    AdaptiveTestSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void AdaptiveTestSummary_assessmentHistoryDataList_ItemCommand(object sender,
            DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "CreateSession")
                    return;
                AdaptiveTestSummary_testPreviewTestNameLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_assessmentHistoryTestNameHiddenField")).Value;
                AdaptiveTestSummary_testDescriptionDiv.InnerHtml = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_assessmentHistoryTestDescriptionHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewTestIDLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_assessmentHistoryTestKeyHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewTimeLimitLabel.Text = Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSeconds
                    (Convert.ToInt32(((HiddenField)e.Item.FindControl("AdaptiveTestSummary_assessmentHistoryTimeLimitHiddenField")).Value));
                AdaptiveTestSummary_testPreviewTestCostLabel.Text = ((HiddenField)e.Item.FindControl("AdaptiveTestSummary_assessmentHistoryTestCostHiddenField")).Value; ;
                AdaptiveTestSummary_testPreviewStartTestButton.Visible = false;
                AdaptiveTestSummary_testPreviewCreateButton.Visible = true;
                AdaptiveTestSummary_testPreviewModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                    AdaptiveTestSummary_bottomErrorMessageLabel, exp.Message);
            }
        }

        protected void AdaptiveTestSummary_topCancelButton_Click(object sender,
            EventArgs e)
        {
            Response.Redirect("~/Dashboard.aspx", false);
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// This method reset the paging hidden field controls for all the
        /// recommendation tests.
        /// </summary>
        private void ResetPagingHiddenControls()
        {
            AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value = "1";
            AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value = "1";
            AdaptiveTestSummary_similarCandidatePagingHiddenField.Value = "1";
        }

        private void SetVisibleStatusForRecommendations()
        {
            if (AdaptiveTestSummary_basedOnProfileDatalist.Items.Count == 0)
            {
                AdaptiveTestSummary_basedOnProfileTable.Visible = false;
                AdaptiveTestSummary_basedOnProfileDatalist.Visible = false;
                AdaptiveTestSummary_basedOnProfileDatalist.Attributes.Add("width", "50%");
                AdaptiveTestSummary_basedOnProfilePrevImageButton.Visible = false;
                AdaptiveTestSummary_basedOnProfileNextImageButton.Visible = false;
                AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value = "1";
                AdaptiveTestSummary_basedOnProfileMoreLinkButton.Visible = false;
            }
            if (AdaptiveTestSummary_assessmentHistoryDataList.Items.Count == 0)
            {
                AdaptiveTestSummary_historyBasedTable.Visible = false;
                AdaptiveTestSummary_assessmentHistoryDataList.Visible = false;
                AdaptiveTestSummary_assessmentHistoryDataList.Width = 1;
                AdaptiveTestSummary_basedOnHistoryPrevImageButton.Visible = false;
                AdaptiveTestSummary_historyBasedPrevImageButton.Visible = false;
                AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value = "1";
                AdaptiveTestSummary_historyBasedMoreLinkButton.Visible = false;
            }
            if (AdaptiveTestSummary_similiarCandidateTestDataList.Items.Count > 0)
                return;
            AdaptiveTestSummery_similarCandidateTestTable.Visible = false;
            AdaptiveTestSummary_similiarCandidateTestDataList.Visible = false;
            AdaptiveTestSummary_similiarCandidateTestDataList.Width = 1;
            AdaptiveTestSummary_similiarCandidatePrevTestImageButton.Visible = false;
            AdaptiveTestSummary_similiarCandidateTestNextImageButton.Visible = false;
            AdaptiveTestSummary_similarCandidatePagingHiddenField.Value = "1";
            AdaptiveTestSummary_similiarCandidateTestMoreLinkButton.Visible = false;
        }

        /// <summary>
        /// This method calls all the three adaptive tests.
        /// </summary>
        private void LoadAdaptiveLists(bool FromPageLoad)
        {
            LoadCandidateProfileTest(1, FromPageLoad);
            LoadCandidateAssessmentHistoryTests(1, FromPageLoad);
            LoadCandidateSimiliarTest(1, FromPageLoad);
        }

        /// <summary>
        /// This method loads the candidate similar profile tests
        /// datalist
        /// </summary>
        private void LoadCandidateProfileTest(int PageNumber,bool FromPageLoad)
        {
            int TotalNoOfRecords = 0;
            AdaptiveTestSummary_basedOnProfileDatalist.DataSource =
                new TestBLManager().GetAdaptiveTestBySimilarProfile(base.userID, 4, PageNumber, 'A', "TESTID",
                out TotalNoOfRecords);
            AdaptiveTestSummary_basedOnProfileDatalist.DataBind();
            if (AdaptiveTestSummary_basedOnProfileDatalist.DataSource != null &&
                AdaptiveTestSummary_basedOnProfileDatalist.Items.Count != 0)
                return;
            AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value = (PageNumber - 1).ToString();
            AdaptiveTestSummary_basedOnProfileDatalist.DataSource =
                new TestBLManager().GetAdaptiveTestBySimilarProfile(base.userID, 4, PageNumber - 1, 'A', "TESTID",
                out TotalNoOfRecords);
            AdaptiveTestSummary_basedOnProfileDatalist.DataBind();
            //AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value = AdaptiveTestSummary_basedOnProfilePagingHiddenField.Value;
            if (FromPageLoad)
                base.ShowMessage(AdaptiveTestSummary_basedOnProfileErrorLabel,
                        string.Format(Resources.HCMResource.AdaptiveTestSummary_NextNavigationSummaryMessage, "based on profile"));
            else
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                        AdaptiveTestSummary_bottomErrorMessageLabel,
                            string.Format(Resources.HCMResource.AdaptiveTestSummary_NextNavigationSummaryMessage, "based on profile"));
        }

        private void LoadCandidateAssessmentHistoryTests(int PageNumber, bool FromPageLoad)
        {
            int TotalNoOfRecords = 0;
            AdaptiveTestSummary_assessmentHistoryDataList.DataSource =
                new TestBLManager().GetAdaptiveTestByAssessmentHistory(base.userID, 4, PageNumber, 'A', "TESTID123",
                out TotalNoOfRecords);
            AdaptiveTestSummary_assessmentHistoryDataList.DataBind();
            if (AdaptiveTestSummary_assessmentHistoryDataList.DataSource != null &&
                AdaptiveTestSummary_assessmentHistoryDataList.Items.Count != 0)
                return;
            AdaptiveTestSummary_assessmentHistoryPagingHiddenField.Value = (PageNumber - 1).ToString();
            AdaptiveTestSummary_assessmentHistoryDataList.DataSource =
                new TestBLManager().GetAdaptiveTestByAssessmentHistory(base.userID, 4, PageNumber - 1, 'A',
                "TESTID2342", out TotalNoOfRecords);
            AdaptiveTestSummary_assessmentHistoryDataList.DataBind();
            if (FromPageLoad)
                base.ShowMessage(AdaptiveTestSummary_historyBasedErrorMessageLabel,
                        string.Format(Resources.HCMResource.AdaptiveTestSummary_NextNavigationSummaryMessage, "based on assessment history"));
            else
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                        AdaptiveTestSummary_bottomErrorMessageLabel,
                        string.Format(Resources.HCMResource.AdaptiveTestSummary_NextNavigationSummaryMessage, "based on assessment history"));
        }

        /// <summary>
        /// This method loads the candidate similar tests
        /// datalist
        /// </summary>
        private void LoadCandidateSimiliarTest(int PageNumber,bool FromPageLoad)
        {
            int TotalNoOfRecords = 0;
            AdaptiveTestSummary_similiarCandidateTestDataList.DataSource =
                new TestBLManager().GetAdaptiveTestBySimilarCandidate(base.userID, 4, PageNumber, 'A', "TESTID132",
                out TotalNoOfRecords);
            AdaptiveTestSummary_similiarCandidateTestDataList.DataBind();
            if (AdaptiveTestSummary_similiarCandidateTestDataList.DataSource != null &&
                            AdaptiveTestSummary_similiarCandidateTestDataList.Items.Count != 0)
                return;
            AdaptiveTestSummary_similarCandidatePagingHiddenField.Value = (PageNumber - 1).ToString();
            AdaptiveTestSummary_similiarCandidateTestDataList.DataSource =
                new TestBLManager().GetAdaptiveTestBySimilarCandidate(base.userID, 4, PageNumber - 1, 'A', "TESTID2322",
                out TotalNoOfRecords);
            AdaptiveTestSummary_similiarCandidateTestDataList.DataBind();
            if (FromPageLoad)
                base.ShowMessage(AdaptiveTestSummary_similarCandidateTestErrorLabel,
                        string.Format(Resources.HCMResource.AdaptiveTestSummary_NextNavigationSummaryMessage, "based on similar candidate"));
            else
                base.ShowMessage(AdaptiveTestSummary_topErrorMessageLabel,
                        AdaptiveTestSummary_bottomErrorMessageLabel,
                        string.Format(Resources.HCMResource.AdaptiveTestSummary_NextNavigationSummaryMessage, "based on similar candidate"));
        }

        /// <summary>
        /// This method creates the session for the test.
        /// </summary>
        private void CreateTestSession()
        {
            TestScheduleDetail testSchedule = null;
            try
            {
                testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = base.userID.ToString();
                //  testSchedule.CandidateTestSessionID = candidateSessionID.ToString();
                testSchedule.AttemptID = 1;
                testSchedule.EmailId = ((UserDetail)Session["USER_DETAIL"]).Email;
                testSchedule.ExpiryDate = DateTime.Now.AddDays(base.candidateExpiryDays);
                //   new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID);
                //string testSessionID = null;
                string candidateSessionID = null;
                //// Call save test session BL method.
                new TestBLManager().SaveTestSessionScheduleCandidate(GetTestSessionDetails(), testSchedule,
                    base.userID, out candidateSessionID);
                AdaptiveTestSummary_testPreviewStartTestButton.Visible = true;
                AdaptiveTestSummary_testPreviewCreateButton.Visible = false;
                AdaptiveTestSummary_testPreviewTopSuccessMessageLabel.Text = "Session created successfully";
                AdaptiveTestSummary_testCandidateSessionIdHiddenField.Value = candidateSessionID;
                AdaptiveTestSummary_testPreviewModalPopupExtender.Show();
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSchedule)) testSchedule = null;
            }
        }

        /// <summary>
        /// This method loads the test details in to test session details 
        /// data object.
        /// </summary>
        /// <returns>Test session detail data object that contains the test details.</returns>
        private TestSessionDetail GetTestSessionDetails()
        {
            // Initialize test session detail object
            TestSessionDetail testSessionDetail = null;
            try
            {
                testSessionDetail = new TestSessionDetail();
                // Set test name
                testSessionDetail.TestID = AdaptiveTestSummary_testPreviewTestIDLabel.Text;
                testSessionDetail.TestName = AdaptiveTestSummary_testPreviewTestNameLabel.Text;
                // Set number of candidate session (session count)
                testSessionDetail.NumberOfCandidateSessions = 1;
                // Set total credits limit
                testSessionDetail.TotalCredit =
                    Convert.ToDecimal(AdaptiveTestSummary_testPreviewTestCostLabel.Text);
                // Set time limit
                testSessionDetail.TimeLimit = Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(
                    AdaptiveTestSummary_testPreviewTimeLimitLabel.Text);
                testSessionDetail.ClientRequestID = "0";
                // Set expiry date
                testSessionDetail.ExpiryDate = DateTime.Now;
                // Set random question order status
                testSessionDetail.IsRandomizeQuestionsOrdering = false;
                // Set display result status 
                testSessionDetail.IsDisplayResultsToCandidate = true;
                // Set cyber proctoring status
                testSessionDetail.IsCyberProctoringEnabled = false;
                // Set created by
                testSessionDetail.CreatedBy = base.userID;
                // Set modified by
                testSessionDetail.ModifiedBy = base.userID;
                // Set test instructions
                testSessionDetail.Instructions = Resources.HCMResource.CandidateSearchTest_Instructions.ToString();
                // Set session descriptions
                testSessionDetail.TestSessionDesc = Resources.HCMResource.CandidateSearchTest_TestSessionDesc.ToString();
                return testSessionDetail;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionDetail)) testSessionDetail = null;
            }
        }

        
        #endregion Private Methods

        #region Protected Methods                                              

        protected string TrimQuestion(string Question)
        {
            return string.Empty;
        }
        /// <summary>
        /// Convert seconds to hh:mm:ss format 
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        protected static string ConvertSecondsToHoursMinutesSeconds(int seconds)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            string hoursMinutesSeconds = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    timeSpan.Hours,
                                    timeSpan.Minutes,
                                    timeSpan.Seconds);

            return hoursMinutesSeconds;
        }
        #endregion Protected Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            AdaptiveTestSummary_startTestPopupExtenderControl.Message = "Are you ready to start the test ?";
            AdaptiveTestSummary_startTestPopupExtenderControl.Title = "Start Test";
            AdaptiveTestSummary_startTestPopupExtenderControl.Type = MessageBoxType.YesNo;
            LoadAdaptiveLists(true);
            SetVisibleStatusForRecommendations();
        }

        #endregion Protected Overridden Methods
    }
}