﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forte.HCM.UI.CandidateCenter
{
    public partial class CertificateImageGenerator : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["HTML_TEXT"] != null)
                {
                    CertificateImageGenerator1.InnerHtml = Session["HTML_TEXT"].ToString().Trim();
                }
            }
        }
    }
}
