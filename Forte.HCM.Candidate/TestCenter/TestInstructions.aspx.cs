﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestInstructions.cs
// File that represents the user interface for test instructions page.
// This will display the test instructions such as minimum hardware and
// OS requirements, test instructions, cyber proctoring guidelines etc.
// From here, candidate can start their test. 

#endregion Header 

#region Directives                                                             

using System;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface for test instructions page.
    /// This will display the test instructions such as minimum hardware and
    /// OS requirements, test instructions, cyber proctoring guidelines etc.
    /// From here, candidate can start their test. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class TestInstructions : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the candidate session key.
        /// </summary>
        public string canSessionKey
        {
            set
            {
                Session["TEST_INSTRUCTIONS_CAND_SESSION_KEY"] = value;
            }
            get
            {
                if (Session["TEST_INSTRUCTIONS_CAND_SESSION_KEY"] == null)
                    return string.Empty;
                else
                    return Session["TEST_INSTRUCTIONS_CAND_SESSION_KEY"].ToString();
            }
        }


        /// <summary>
        /// A <see cref="int"/> that holds the attempt ID.
        /// </summary>
        public int attemptID
        {
            set
            {
                Session["TEST_INSTRUCTIONS_ATTEMPT_ID"] = value;
            }
            get
            {
                if (Session["TEST_INSTRUCTIONS_ATTEMPT_ID"] == null)
                    return 0;
                else
                    return Convert.ToInt32(Session["TEST_INSTRUCTIONS_ATTEMPT_ID"].ToString());
            }
        }

        #endregion Private Variables

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               
                // Set browser title.
                Master.SetPageCaption("Test Instructions");

                TestInstructions_launchCyperProcterLinkButton.Attributes.Add("onClick", "LaunchCyberProtering('" + TestInstructions_launchCyperProcterLinkButton.ClientID + "','" + TestInstructions_startTestButtonDiv.ClientID + "')");
                TestInstructions_launchCyperProcterLinkButton.NavigateUrl = ConfigurationManager.AppSettings["CYBER_PROCTORING_URL"];
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "script", "window.history.forward(); window.onunload = " +
                    " function () { };", true);
                // Clear message and hide labels.
                ClearMessages();

                // Show/hide menu based on user type (limited or not).
                // Master.ShowMenu = !base.isLimited;

                // Check the querystring 'n'. This querystring is passed in mail while scheduling the candidate for the test.
                string queryString = string.Empty;
                TestSessionIdDetails testSessionIds=null;
                if (!Utility.IsNullOrEmpty(Request.QueryString["n"]))
                {
                    queryString = Request.QueryString["n"].ToString();
                    testSessionIds= new TestSessionBLManager().GetTestSessionIdDetails(queryString);
                    if (testSessionIds != null)
                    {
                        if (testSessionIds.CandidateId == ((UserDetail)Session["USER_DETAIL"]).UserID)
                        {
                            canSessionKey = testSessionIds.CandidateSessionId;
                            attemptID = testSessionIds.AttemptID;
                        }
                        else
                        {
                            Response.Redirect("~/Dashboard.aspx", false);
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Dashboard.aspx", false);
                    }

                }
                else if (Request.QueryString["candidatesessionid"] != null && Request.QueryString["attemptid"] != null)
                {
                    canSessionKey = Request.QueryString["candidatesessionid"];
                    int attID = 0;
                    int.TryParse(Request.QueryString["attemptid"], out attID);
                    attemptID = attID;
                }
                else
                    Response.Redirect("~/Dashboard.aspx", false);
                
                if (!Page.IsPostBack)
                {
                    TestInstructions_startTestButtonDiv.Attributes.Add("style", "display:none");

                    Session["IS_CYBER_PROCTORING"] = new CandidateBLManager().
                        IsCyberProctorEnabled(canSessionKey, attemptID);

                    // Load values.
                    LoadValues();

                    if (Request.QueryString["proceed"] != null &&
                        Request.QueryString["proceed"] == "Y")
                    {
                        bool isExist = true;
                        TestInstructions_startTestButtonDiv.Attributes.Add("style", "display:block");
                        TestInstructions_proceedButtonDiv.Attributes.Add("style", "display:none");
                        TestInstructions_cancelLinkButton.Attributes.Add("style", "display:none");
                        TestInstructions_generateKeyDiv.Attributes.Add("style", "display:block");
                        TestInstructions_launchCyperProcterLinkButton.Visible = true;

                        if (Convert.ToBoolean(ConfigurationManager.AppSettings
                            ["SHOW_CYBER_PROCTORING_DOCUMENT_DOWNLOAD_LINK"]))
                        {
                            TestInstructions_downloadCyberProctorDocumentTD.Visible = true;
                        }
                        while (isExist)
                        {
                            Session["CAPTCHAVALUE"] = RandomString(8);
                            lblCaptchaText.Text = Session["CAPTCHAVALUE"].ToString();
                            isExist = new CandidateBLManager().UpdateAuthCode(canSessionKey, attemptID, Session["CAPTCHAVALUE"].ToString());
                        }

                        // Generate a captcha image.
                        CaptchaImage ci = new CaptchaImage(Session["CAPTCHAVALUE"].ToString(),
                            200, 50, "Century Schoolbook");

                        // Keep image in session.
                        Session["TEST_CAPTCHA_IMAGE"] = ci.Image;

                        TestInstructions_agreeCheckbox.Attributes.Add("style", "display:none");

                        // Display browser instructions (for cyber proctoring).
                        // TODO: This is commented out now. Since it the query string paramters are lost with 
                        // firefox browser.
                        //DisplayBrowserInstructions();

                        // Show success message.
                        ShowMessage(TestInstructions_topSuccessMessageLabel,
                            TestInstructions_bottomSuccessMessageLabel,
                            "Your security code has been generated. Click on 'Launch Cyber Proctoring' to launch the cyber proctoring application and enter the security code shown below into it and click on 'Start Test' button to start the test");
                    }
                }

                // Show cyber proctoring guidelines only for cyber proctoring 
                // tests.
                if (IsCyberProctoring)
                {
                    TestInstructions_cyberProctoringGuidelinesRow.Visible = true;
                }
                else
                {
                    TestInstructions_cyberProctoringGuidelinesRow.Visible = false;
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);
                ShowMessage(TestInstructions_topErrorMessageLabel,
                    TestInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the start test button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void TestInstructions_startTestButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                TestInstructions_startTestButtonDiv.Attributes.Add("style", "display:block");
                //TestInstructions_capchaImage.Visible = false;
                bool isCyberProctorStarted = new CandidateBLManager().IsCyberProctorStarted
                    (canSessionKey, attemptID);

                bool isCyberProctorEnabled = new CandidateBLManager().IsCyberProctorEnabled
                   (canSessionKey, attemptID);

                if (isCyberProctorStarted)
                {
                    new CandidateBLManager().UpdateTestStatus(canSessionKey, attemptID);
                    
                    /*Page.ClientScript.RegisterStartupScript(this.GetType(), "test",
                        "<script language='javascript' type='text/javascript'>OpenTestConduction('" + 
                        canSessionKey + "','" + attemptID + "')</script>");*/

                    Response.Redirect("~/TestConduction/TestPerformer.aspx?csk="+canSessionKey +"&atmpt="+ attemptID,false);  
                }
                else
                {
                    base.ShowMessage(TestInstructions_topErrorMessageLabel, 
                        TestInstructions_bottomErrorMessageLabel, 
                        "Cyber proctoring not started. Enter the security code in the cyber proctoring application and start");
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);
                ShowMessage(TestInstructions_topErrorMessageLabel,
                    TestInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the proceed button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void TestInstructions_proceedButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                // Check if candidate agree to the terms and conditions.
                if (!TestInstructions_agreeCheckbox.Checked)
                {
                    ShowMessage(TestInstructions_topErrorMessageLabel,
                        TestInstructions_bottomErrorMessageLabel, 
                        "You must agreed to the terms and conditions to proceed the test");

                    return;
                }

                // If not a cyber proctoring test, proceed to the test conductor
                // page, else start cyber proctoring.
                if (!this.IsCyberProctoring)
                {
                    new CandidateBLManager().UpdateTestStatus(canSessionKey, attemptID);
                    /*Page.ClientScript.RegisterStartupScript(this.GetType(), "test",
                        "<script language='javascript' type='text/javascript'>OpenTestConduction('" + canSessionKey + "','" + attemptID + "')</script>");*/

                    Response.Redirect("~/TestConduction/TestPerformer.aspx?csk=" + canSessionKey + "&atmpt=" + attemptID, false);  
                    return;
                }

                // Generate and show an authentication code image.
                bool isExist = true;

                while (isExist)
                {
                    // Generate a 8 lettered authentication code.
                    Session["CAPTCHAVALUE"] = RandomString(8);
                    lblCaptchaText.Text = Session["CAPTCHAVALUE"].ToString();

                    // Check if the authentication code already exist. If 
                    // already exist, loop will continue to generate another
                    // authentication code.
                    isExist = new CandidateBLManager().UpdateAuthCode
                        (canSessionKey, attemptID, Session["CAPTCHAVALUE"].ToString());
                }

                // Generate a captcha image.
                CaptchaImage ci = new CaptchaImage(Session["CAPTCHAVALUE"].ToString(),
                    200, 50, "Century Schoolbook");

                // Keep image in session.
                Session["TEST_CAPTCHA_IMAGE"] = ci.Image;

                // Show the start test button, captcha image and hide the proceed button.
                TestInstructions_startTestButtonDiv.Attributes.Add("style", "display:block");
                TestInstructions_proceedButtonDiv.Attributes.Add("style", "display:none");
                TestInstructions_generateKeyDiv.Attributes.Add("style", "display:block");

                // Redirect to the same page with cyber proctor status as started.
                Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                    "?m=" + Request.QueryString["m"] +
                    "&candidatesessionid=" + canSessionKey +
                    "&attemptid=" + attemptID +
                    "&parentpage=" + Request.QueryString["parentpage"] +
                    "&proceed=Y", false);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);
                ShowMessage(TestInstructions_topErrorMessageLabel,
                    TestInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the regenerate link is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void TestInstructions_regenerateKeyLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ClearMessages();

                bool isExist = true;
                bool isCyberProctorEnabled = new CandidateBLManager().IsCyberProctorEnabled
                    (canSessionKey, attemptID);
                if (isCyberProctorEnabled)
                {
                    while (isExist)
                    {
                        Session["CAPTCHAVALUE"] = RandomString(8);
                        lblCaptchaText.Text = Session["CAPTCHAVALUE"].ToString();
                        isExist = new CandidateBLManager().UpdateAuthCode
                            (canSessionKey, attemptID, Session["CAPTCHAVALUE"].ToString());
                    }

                    // Generate a captcha image.
                    CaptchaImage ci = new CaptchaImage(Session["CAPTCHAVALUE"].ToString(),
                        200, 50, "Century Schoolbook");

                    // Keep image in session.
                    Session["TEST_CAPTCHA_IMAGE"] = ci.Image;
                }
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);
                ShowMessage(TestInstructions_topErrorMessageLabel,
                    TestInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the download cyber proctoring
        /// document is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will download the cyber proctoring troubleshooting document.
        /// </remarks>
        protected void TestInstructions_downloadCyberProctorDocumentLinkButton_Click
            (object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Common/Download.aspx?type=CYBER_PROCTORING_DOCUMENT", false);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);
                ShowMessage(TestInstructions_topErrorMessageLabel,
                    TestInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the download cyber proctoring
        /// document is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will download the cyber proctoring troubleshooting document.
        /// </remarks>
        protected void TestInstructions_downloadCyberProctorExecutable_Click
            (object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Common/Download.aspx?type=CYBER_PROCTORING_EXECUTABLE", false);
            }
            catch (Exception exp)
            {
                Logger.TraceLog(exp.Message);
                ShowMessage(TestInstructions_topErrorMessageLabel,
                    TestInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Retrieve test instructions disclaimer message.
            DisclaimerMessage disclaimer = new CandidateBLManager().
                GetTestInstructionsDisclaimerMessage();

            if (disclaimer != null)
            {
                // Minimum hardware & OS requirements.
                TestInstructions_minimumHardwareValueLiteral.Text = 
                    disclaimer.HardwareInstructions == null ? disclaimer.HardwareInstructions : 
                        disclaimer.HardwareInstructions.ToString().Replace(Environment.NewLine, "<br />");

                // Test instructions.
                TestInstructions_testInstructionsValueLiteral.Text =
                    disclaimer.TestInstructions == null ? disclaimer.TestInstructions :
                        disclaimer.TestInstructions.ToString().Replace(Environment.NewLine, "<br />");

                // Cyber proctoring guidelines.
                TestInstructions_cyberProctoringGuidelinesValueLiteral.Text =
                    disclaimer.CyberProctoringInstructions == null ? disclaimer.CyberProctoringInstructions :
                        disclaimer.CyberProctoringInstructions.ToString().Replace(Environment.NewLine, "<br />");

                // Warning.
                TestInstructions_warningLiteral.Text =
                    disclaimer.WarningInstructions == null ? disclaimer.WarningInstructions :
                        disclaimer.WarningInstructions.ToString().Replace(Environment.NewLine, "<br />");
            }

            // Test description.
            TestDetail testDetail = new CandidateBLManager().GetTestDetail
                (canSessionKey);

            if (testDetail != null)
            {
                TestInstructions_testDescriptionValueLiteral.Text =
                    testDetail.Description == null ? testDetail.Description :
                        testDetail.Description.ToString().Replace(Environment.NewLine, "<br />");
            }
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that displays the browser instructions. This is applicable 
        /// only for cyber proctoring tests.
        /// </summary>
        private void DisplayBrowserInstructions()
        {
            // Set browser specific information.
            string browserType = Request.Browser.Browser;

            // Check if browser type is present.
            if (Utility.IsNullOrEmpty(browserType))
                return;
            browserType = browserType.Trim().ToUpper();

            // Get browser detail and instructions.
            BrowserDetail browserDetail = new CommonBLManager().
                GetBrowserInstructions(browserType);

            // Check if browser instructions are present in the database. If
            // not send a mail to the admin.
            if (browserDetail == null)
            {
                // Send a mail to the admin, indicating that the browser 
                // instructions are not present.
                try
                {
                    // Compose browser detail.
                    browserDetail = new BrowserDetail();
                    browserDetail.Type = browserType;
                    browserDetail.Version = Request.Browser.Version;

                    // Log the detail.
                    Logger.ExceptionLog(string.Format
                        ("Browser instructions missing for type {0}", browserType));

                    // Send email to the admin.
                    new EmailHandler().SendMail(EntityType.BrowserInstructionsMissing, browserDetail);
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                return;
            }

            // Set the title label.
            TestInstructions_browserInstructionsDiv_titleLabel.Text =
                string.Format("Browser Requirements ({0})", browserDetail.Name);

            // Set the header label.
            TestInstructions_browserInstructionsDiv_headerLabel.Text = 
                "Please follow the instructions and fix the browser settings. For further details click on the link to download the trouble-shooting document";

            // Assign the data source to the instructions list.
            TestInstructions_browserInstructionsDiv_instructionsListView.DataSource = 
                browserDetail.Instructions;
            TestInstructions_browserInstructionsDiv_instructionsListView.DataBind();

            // Show the browser instructions popup window.
            ScriptManager.RegisterStartupScript(this, this.GetType(),
                "ShowBrowserInstructionsKey",
                "<script>ShowBrowserInstructions('Y')</script>", false);
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            TestInstructions_topErrorMessageLabel.Text = string.Empty;
            TestInstructions_bottomErrorMessageLabel.Text = string.Empty;
            TestInstructions_topSuccessMessageLabel.Text = string.Empty;
            TestInstructions_bottomSuccessMessageLabel.Text = string.Empty;

            TestInstructions_topErrorMessageLabel.Visible = false;
            TestInstructions_bottomErrorMessageLabel.Visible = false;
            TestInstructions_topSuccessMessageLabel.Visible = false;
            TestInstructions_bottomSuccessMessageLabel.Visible = false;
        }

        #endregion Private Methods

        #region Private Properties                                             

        /// <summary>
        /// Property that retreives the status whether the test is cyber 
        /// proctored or not.
        /// </summary>
        private bool IsCyberProctoring
        {
            get
            {
                return (bool)Session["IS_CYBER_PROCTORING"];
            }
        }

        #endregion Private Properties
    }
}
