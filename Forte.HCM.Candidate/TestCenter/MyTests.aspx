<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HCMCandidateMaster.Master"
    CodeBehind="MyTests.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.MyTests" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/HCMCandidateMaster.Master" %>
<asp:Content ID="MyTests_bodyContent" runat="server" ContentPlaceHolderID="HCMCandidateMaster_contentPlaceHolder">

    <script type="text/javascript">
        function ShowRetakeWarningMessage() {
            //displays email confirmation pop up extender
            $find("<%= MyTests_RetakeValidation_ModalPopupExtender.ClientID %>").show();
            return false;
        }        
    </script>

    <script type="text/javascript" language="javascript">

        // Method that helps to expand or shrink the specific section such
        // as pending, completed and expired.
        function MultipleExpandorCompress(ctrlExpandTR, ctrlExpandDiv, expandImage,
            compressImage, resultDiv) {
            var pendingTestsTR = "<%= MyTests_pendingTestsTR.ClientID %>";
            var completedTestsTR = "<%= MyTests_completedTestsTR.ClientID %>";
            var expiredTestsTR = "<%= MyTests_expiredTestsTR.ClientID %>";
            if (document.getElementById(ctrlExpandTR) != null) {
                document.getElementById(expandImage).style.display = "none";
                document.getElementById(compressImage).style.display = "none";
                document.getElementById(resultDiv).style.display = "block";
                if (document.getElementById(ctrlExpandDiv).style.height.toString() == "150px") {
                    document.getElementById(pendingTestsTR).style.display = "none";
                    document.getElementById(completedTestsTR).style.display = "none";
                    document.getElementById(expiredTestsTR).style.display = "none";
                    document.getElementById(ctrlExpandTR).style.display = "";

                    document.getElementById(ctrlExpandDiv).style.height = "100%";
                    document.getElementById(expandImage).style.display = "block";
                }
                else {
                    document.getElementById(pendingTestsTR).style.display = "";
                    document.getElementById(completedTestsTR).style.display = "";
                    document.getElementById(expiredTestsTR).style.display = "";
                    document.getElementById(ctrlExpandDiv).style.height = "150px";
                    document.getElementById(compressImage).style.display = "block";
                }
            }
            return false;
        }

        // Method that will show the start test confirmation window.
        function StartTest(candidateSessionID, attemptID) {
            document.getElementById("<%=MyTests_candidateSessionID.ClientID%>").value = candidateSessionID;
            document.getElementById("<%=MyTests_attemptID.ClientID%>").value = attemptID;

            $find("<%= MyTests_StartTestPopupExtender.ClientID  %>").show();
            return false;
        }
    </script>

    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="MyTests_headerLiteral" runat="server" Text="My Tests"></asp:Literal>
                        </td>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="3" align="right">
                                <tr>
                                    <td>
                                        <asp:Button ID="MyTests_topRefreshButton" runat="server" Text="Refresh" SkinID="sknButtonId"
                                            OnClick="MyTests_refreshButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="MyTests_topCancelButton" runat="server" Text="Cancel" PostBackUrl="~/Dashboard.aspx"
                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyTests_topMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="MyTests_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="MyTests_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="MyTests_pendingTestsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_pendingTestsGridView" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_completedTestsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_completedTestsGridView" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_expiredTestsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_expiredTestsGridView" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr runat="server">
            <td>
                <div id="MyTests_pendingTestsTR" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="header_bg" align="center">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="width: 93%" align="left" class="header_text_bold">
                                            <asp:Literal ID="MyTests_pendingTestsLiteral" runat="server" Text="Pending Tests"></asp:Literal>
                                        </td>
                                        <td style="width: 2%" align="right">
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td style="width: 2%" align="right" id="MyTests_pendingTestsHeaderTR" runat="server">
                                            <span id="MyTests_pendingTestsUpSpan" runat="server" style="display: none;">
                                                <asp:Image ID="MyTests_pendingTestsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                    id="MyTests_pendingTestsDownSpan" runat="server" style="display: block;"><asp:Image ID="MyTests_pendingTestsDownImage"
                                                        runat="server" SkinID="sknMaximizeImage" /></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;" class="grid_body_bg">
                                <asp:UpdatePanel ID="MyTests_pendingTestsUpdatePanel" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="display: block;" runat="server" id="MyTests_pendingTestPlusDiv">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="right">
                                                        <uc3:PageNavigator ID="MyTests_pendingTestsTopPagingNavigator" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left">
                                                                    <div style="height: 150px; overflow: auto; display: block;" runat="server" id="MyTests_pendingTestsDiv">
                                                                        <asp:GridView ID="MyTests_pendingTestsGridView" runat="server" AllowSorting="True"
                                                                            AutoGenerateColumns="False" Width="100%" OnSorting="MyTests_pendingTestsGridView_Sorting"
                                                                            OnRowCreated="MyTests_pendingTestsGridView_RowCreated" OnRowDataBound="MyTests_pendingTestsGridView_RowDataBound"
                                                                            OnRowCommand="MyTests_pendingTestsGridView_RowCommand">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="MyTests_pendingTestsViewImageButton" runat="server" SkinID="sknTestIntroImageButton"
                                                                                            ToolTip="Test Introduction" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                            CommandName="TestIntroduction" CssClass="showCursor" />&nbsp;
                                                                                        <asp:ImageButton ID="MyTests_pendingTestStartImageButton" runat="server" SkinID="sknStartTestImageButton"
                                                                                            ToolTip="Start Test" CssClass="showCursor" />&nbsp;
                                                                                        <asp:ImageButton ID="MyTests_pendingTestReminderImageButton" runat="server" SkinID="sknReminderImageButton"
                                                                                            ToolTip="Test Reminder" CssClass="showCursor" />
                                                                                        <asp:HiddenField ID="MyTests_pendingTestsCandidateSessionIDHiddenField" Value='<%# Eval("CandidateSessionID") %>'
                                                                                            runat="server" />
                                                                                        <asp:HiddenField ID="MyTests_pendingTestsAttemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                                                            runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField HeaderText="Test&nbsp;Name" DataField="TestName" SortExpression="TESTNAME" />
                                                                                <asp:BoundField HeaderText="Test&nbsp;ID" DataField="TestID" SortExpression="TESTID" />
                                                                                <asp:TemplateField HeaderText="Initiated&nbsp;By" SortExpression="INITIATEDBY">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyTests_pendingTestsInitiatedByFullNameLabel" runat="server" ToolTip='<%# Eval("TestAuthorFullName") %>'
                                                                                            Text='<%# Eval("InitiatedBy") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Initiated&nbsp;On" SortExpression="INITIATEDON">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyTests_pendingTestsInitiatedOnLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestDetail)Container.DataItem).InitiatedOn) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Expiry Date" SortExpression="EXPIRYDATE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="MyTests_pendingTestsExpiryDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestDetail)Container.DataItem).ExpiryDate) %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr id="MyTests_completedTestsTR" runat="server">
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 93%" align="left" class="header_text_bold">
                                        <asp:Literal ID="MyTests_completedTestsLiteral" runat="server" Text="Completed Tests"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">
                                    </td>
                                    <td style="width: 1%">
                                    </td>
                                    <td style="width: 2%" align="right" id="MyTests_completedTestsHeaderTR" runat="server">
                                        <span id="MyTests_completedTestsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="MyTests_completedTestsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                id="MyTests_completedTestsDownSpan" runat="server"><asp:Image ID="MyTests_completedTestsDownImage"
                                                    runat="server" SkinID="sknMaximizeImage" /></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;" class="grid_body_bg">
                            <asp:UpdatePanel ID="MyTests_completedTestUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="display: block;" runat="server" id="MyTests_completedTestsPlusDiv">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="right">
                                                    <uc3:PageNavigator ID="MyTests_completedTestsTopPagingNavigator" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left">
                                                                <div style="height: 150px; overflow: auto; display: block;" runat="server" id="MyTests_completedTestsDiv">
                                                                    <asp:GridView ID="MyTests_completedTestsGridView" runat="server" AllowSorting="true"
                                                                        AutoGenerateColumns="false" Width="100%" OnSorting="MyTests_completedTestsGridView_Sorting"
                                                                        OnRowCreated="MyTests_completedTestsGridView_RowCreated" OnRowDataBound="MyTests_completedTestsGridView_RowDataBound"
                                                                        OnRowCommand="MyTests_completedTestsGridView_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="MyTests_completedTestsActivateImageButton" runat="server" SkinID="sknActivationRequestImageButton"
                                                                                        ToolTip="Request to Retake" CommandName="RequestToRetake" CssClass="showCursor" />
                                                                                    <asp:ImageButton ID="MyTests_testsResultsImageButton" runat="server" SkinID="sknTestResultImageButton"
                                                                                        ToolTip="Test Results" CommandArgument="<%# Container.DataItemIndex %>" CommandName="TestResults"
                                                                                        Visible='<%# IsShowResults(Eval("ShowResults").ToString())%>' CssClass="showCursor" />
                                                                                    <asp:ImageButton ID="MyTests_certificationImageButton" runat="server" SkinID="sknViewCertificateImageButton"
                                                                                        ToolTip="View Certificate" CommandArgument='<%# Container.DataItemIndex %>' CommandName="viewcertificate"
                                                                                        Visible='<%# IsQualified(Eval("CertificationStatus").ToString())%>' CssClass="showCursor" />
                                                                                    <asp:HiddenField ID="MyTests_completedTestsCandidateSessionIDHiddenField" Value='<%# Eval("CandidateSessionID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_completedTestsAttemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_completedTestsTestIDHiddenField" Value='<%# Eval("TestID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_completedTestsTestNameHiddenField" Value='<%# Eval("TestName") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_initiatedByHiddenField" Value='<%# Eval("InitiatedBy") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_completedTestsCompletedOnHiddenField" Value='<%# Eval("CompletedOn") %>'
                                                                                        runat="server" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Test&nbsp;Name" DataField="TestName" SortExpression="TESTNAME" />
                                                                            <asp:BoundField HeaderText="Test&nbsp;ID" DataField="TestID" SortExpression="TESTID" />
                                                                            <%--<asp:BoundField HeaderText="Initiated&nbsp;By" DataField="InitiatedBy" SortExpression="INITIATEDBY" />--%>
                                                                            <asp:TemplateField HeaderText="Initiated&nbsp;By" SortExpression="INITIATEDBY">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyTests_completedTestsInitiatedByFullNameLabel" runat="server" ToolTip='<%# Eval("TestAuthorFullName") %>'
                                                                                        Text='<%# Eval("InitiatedBy") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Initiated&nbsp;On" SortExpression="INITIATEDON">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyTests_completedTestsInitiatedOnLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestDetail)Container.DataItem).InitiatedOn) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Completed&nbsp;On" SortExpression="COMPLETEDON">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyTests_completedTestsCompletedOnLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestDetail)Container.DataItem).CompletedOn) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Certification" DataField="CertificationStatus" SortExpression="CERTIFICATIONSTATUS" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="MyTests_expiredTestsTR" runat="server">
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="header_bg" align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td style="width: 93%" align="left" class="header_text_bold">
                                        <asp:Literal ID="MyTests_expiredTestsLiteral" runat="server" Text="Expired Tests"></asp:Literal>
                                    </td>
                                    <td style="width: 2%" align="right">
                                    </td>
                                    <td style="width: 1%">
                                    </td>
                                    <td style="width: 2%" align="right" id="MyTests_expiredTestsHeaderTR" runat="server">
                                        <span id="MyTests_expiredTestsUpSpan" runat="server" style="display: none;">
                                            <asp:Image ID="MyTests_expiredTestsUpImage" runat="server" SkinID="sknMinimizeImage" /></span><span
                                                id="MyTests_expiredTestsDownSpan" runat="server" style="display: block;"><asp:Image ID="MyTests_expiredTestsDownImage"
                                                    runat="server" SkinID="sknMaximizeImage" /></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;" class="grid_body_bg">
                            <asp:UpdatePanel ID="ExpiredTest_updatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="display: block;" runat="server" id="MyTests_expiredTestsPlusDiv">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="right">
                                                    <uc3:PageNavigator ID="MyTests_expiredTestsTopPagingNavigator" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left">
                                                                <div style="height: 150px; overflow: auto; display: block;" runat="server" id="MyTests_expiredTestsDiv">
                                                                    <asp:GridView ID="MyTests_expiredTestsGridView" runat="server" AllowSorting="true"
                                                                        AutoGenerateColumns="false" Width="100%" OnSorting="MyTests_expiredTestsGridView_Sorting"
                                                                        OnRowDataBound="MyTests_expiredTestsGridView_RowDataBound" OnRowCreated="MyTests_expiredTestsGridView_RowCreated">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="MyTests_expiredTestsActivateImageButton" runat="server" SkinID="sknActivationRequestImageButton"
                                                                                        ToolTip="Request to Activate" CssClass="showCursor" />
                                                                                    <asp:HiddenField ID="MyTests_expiredTestsCandidateSessionIDHiddenField" Value='<%# Eval("CandidateSessionID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_expiredTestsAttemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_expiredTestsTestIDHiddenField" Value='<%# Eval("TestID") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_expiredTestsTestNameHiddenField" Value='<%# Eval("TestName") %>'
                                                                                        runat="server" />
                                                                                    <asp:HiddenField ID="MyTests_expiredTestsInitiatedBydHiddenField" Value='<%# Eval("InitiatedBy") %>'
                                                                                        runat="server" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Test&nbsp;Name" DataField="TestName" SortExpression="TESTNAME" />
                                                                            <asp:BoundField HeaderText="Test&nbsp;ID" DataField="TestID" SortExpression="TESTID" />
                                                                            <%--<asp:BoundField HeaderText="Initiated&nbsp;By" DataField="InitiatedBy" SortExpression="INITIATEDBY" />--%>
                                                                            <asp:TemplateField HeaderText="Initiated&nbsp;By" SortExpression="INITIATEDBY">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyTests_expiredTestsInitiatedByFullNameLabel" runat="server" ToolTip='<%# Eval("TestAuthorFullName") %>'
                                                                                        Text='<%# Eval("InitiatedBy") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Initiated&nbsp;On" SortExpression="INITIATEDON">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyTests_expiredTestsInitiatedOnLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestDetail)Container.DataItem).InitiatedOn) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Expiry&nbsp;Date" SortExpression="EXPIRYDATE">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="MyTests_expiredTestsExpiryDateLabel" runat="server" Text='<%# GetDateFormat(((Forte.HCM.DataObjects.CandidateTestDetail)Container.DataItem).ExpiryDate) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:UpdatePanel ID="MyTests_bottomMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="MyTests_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="MyTests_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="MyTests_pendingTestsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_pendingTestsGridView" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_completedTestsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_completedTestsGridView" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_expiredTestsTopPagingNavigator" />
                        <asp:AsyncPostBackTrigger ControlID="MyTests_expiredTestsGridView" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="right" class="header_bg">
                <table border="0" cellpadding="0" cellspacing="3" align="right">
                    <tr>
                        <td>
                            <asp:Button ID="MyTests_bottomRefreshButton" runat="server" Text="Refresh" SkinID="sknButtonId"
                                OnClick="MyTests_refreshButton_Click" />
                        </td>
                        <td>
                            <asp:LinkButton ID="MyTests_bottomCancelButton" runat="server" Text="Cancel" PostBackUrl="~/Dashboard.aspx"
                                SkinID="sknActionLinkButton"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="MyTests_StartTestPopupPanel" runat="server" Style="display: none;
                    height: 206px" CssClass="popupcontrol_confirm">
                    <div id="MyTests_hiddenDIV" style="display: none">
                        <asp:Button ID="MyTests_hiddenButton" runat="server" />
                    </div>
                    <uc1:ConfirmMsgControl ID="MyTests_StartTestPopupExtenderControl" runat="server"
                        OnOkClick="MyTests_startTestButton_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="MyTests_StartTestPopupExtender" runat="server"
                    PopupControlID="MyTests_StartTestPopupPanel" TargetControlID="MyTests_hiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:UpdatePanel ID="MyTests_RetakeValidationUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="MyTests_RetakeValidationPopupPanel" runat="server" Style="display: none;
                            height: 202px;" CssClass="popupcontrol_confirm">
                            <div id="MyTests_RetakeValidationDiv" style="display: none">
                                <asp:Button ID="MyTests_RetakeValidation_hiddenButton" runat="server" />
                            </div>
                            <uc1:ConfirmMsgControl ID="MyTests_RetakeValidation_ConfirmMsgControl" runat="server"
                                Title="Request to Retake" Type="OkSmall" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="MyTests_RetakeValidation_ModalPopupExtender"
                            runat="server" PopupControlID="MyTests_RetakeValidationPopupPanel" TargetControlID="MyTests_RetakeValidation_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <asp:HiddenField ID="MyTests_candidateSessionID" runat="server" />
            <asp:HiddenField ID="MyTests_attemptID" runat="server" />
        </tr>
    </table>
</asp:Content>
