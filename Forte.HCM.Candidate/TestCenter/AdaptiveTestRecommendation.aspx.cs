﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// AdaptiveTestRecommendation.aspx.cs
// File that represents the user interface for adaptive test recommendation page.

#endregion Header

#region Directives                                                             

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface for adaptive 
    /// test recommendation page for candidate.
    /// </summary>
    public partial class AdaptiveTestRecommendation : PageBase
    {
        #region Declarations                                                   

        /// <summary>
        /// A <see cref="string"/> constant that holds the value of the
        /// sort expression view state.
        /// </summary>
        private const string ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE = "ADAPTIVETESTRECOMMENDATION_SORTEXPRESSION";
        /// <summary>
        /// A <see cref="string"/> constant that holds the value of the 
        /// sort order view state.
        /// </summary>
        private const string ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE = "ADAPTIVETESTRECOMMENDATION_SORTORDER";
        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "225px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "300px";

        #endregion Declarations

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loading.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Redirect to dashboard page.
                Response.Redirect("~/Dashboard.aspx", false);

                // Do not allow limited user.
                if (base.isLimited)
                    Response.Redirect("~/ActivityHome.aspx", false);

                Master.SetPageCaption(Resources.HCMResource.CandidateCenter_AdaptiveTestRecommendationsTitle);

                // Show/hide menu based on user type (limited or not).
                Master.ShowMenu = !base.isLimited;

                CheckAndSetExpandorRestore();
                AdaptiveTestRecommendation_bottomPagingNavigator.PageNumberClick += 
                        new Forte.HCM.UI.CommonControls.PageNavigator.PageNumberClickEventHandler
                            (AdaptiveTestRecommendation_bottomPagingNavigator_PageNumberClick);
                if (!IsPostBack)
                    LoadValues();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                    AdaptiveTestRecommendation_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the PageNumberClick event of the 
        /// testsession_pagingNavigator control.
        /// This binds the data into the grid for the pagenumber.
        /// </summary>
        /// <param name="sender">Source of the event.</param>
        /// <param name="e">The 
        /// <see cref="Adm.IT.eAdm.eLogistics.Common.BL.PageNumberEventArgs"/> 
        /// instance containing the event data.</param>
        void AdaptiveTestRecommendation_bottomPagingNavigator_PageNumberClick(object sender, PageNumberEventArgs e)
        {
            try
            {
                AdaptiveTestRecommendation_pageNumberHiddenField.Value = e.PageNumber.ToString();
                LoadAdaptiveGridBasedOnIndex(
                    Convert.ToInt32(AdaptiveTestRecommendation_selectedIndexHiddenField.Value) , e.PageNumber,
                    //Convert.ToInt32(AdaptiveTestRecommendation_pageNumberHiddenField.Value),
                    Convert.ToChar(ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE]),
                    ViewState[ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE].ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                    AdaptiveTestRecommendation_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void AdaptiveTestRecommendation_startTestButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                "?candidatesessionid=" + AdaptiveTestRecommendation_candidateSessionIDHiddenField.Value +
                "&id=" + Request.QueryString["id"] +
                "&attemptid=1&parentpage=" + Constants.ParentPage.CANDIDATE_ADAPTIVE_TEST_RECOMMENDATION, false);
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AdaptiveTestRecommendation_startTestButton_CancelClick(object sender, EventArgs e)
        {
            try
            {
                // Reshow the window with the default values already set.
                AdaptiveTestRecommendation_bottomSuccessMessageLabel.Text = "Session created successfully";
                AdaptiveTestRecommendation_questionModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                    AdaptiveTestRecommendation_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the create test from pop up window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AdaptiveTestRecommendation_CreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateTestSession();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                    AdaptiveTestRecommendation_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the search button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void AdaptiveTestRecommendation_searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                InitializeSortVariables();
                AdaptiveTestRecommendation_pageNumberHiddenField.Value = "1";
                //if (AdaptiveTestRecommendation_selectTypeDropDownList.SelectedIndex == 0)
                //    return;
                AdaptiveTestRecommendation_selectedIndexHiddenField.Value = AdaptiveTestRecommendation_selectTypeDropDownList.SelectedValue;
                LoadAdaptiveGridBasedOnIndex(
                    Convert.ToInt32(AdaptiveTestRecommendation_selectTypeDropDownList.SelectedValue), 1,
                    Convert.ToChar(ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE]),
                    ViewState[ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE].ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                    AdaptiveTestRecommendation_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method event is raised whenever any button associated with a row in 
        /// the GridView is clicked. 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        protected void AdaptiveTestRecommendation_searchedTestGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "CreateSession")
                    return;
                AdaptiveTestRecommendation_testNameLabel.Text =
                    ((Label)((ImageButton)e.CommandSource).FindControl("AdaptiveTestRecommendation_testNameLabel")).Text;
                AdaptiveTestRecommendation_testDescriptionDiv.InnerHtml =
                    ((Label)((ImageButton)e.CommandSource).FindControl("AdaptiveTestRecommendation_testDescriptionLabel")).Text; 
                AdaptiveTestRecommendation_testIDLabel.Text =
                    ((Label)((ImageButton)e.CommandSource).FindControl("AdaptiveTestRecommendation_testIdLabel")).Text; ;
                AdaptiveTestRecommendation_timeLimitLabel.Text = Utility.ConvertSecondsToHoursMinutesSeconds(
                    Convert.ToInt32(
                    ((Label)((ImageButton)e.CommandSource).FindControl("AdaptiveTestRecommendation_testRecommendationTimeLimitLabel")).Text));
                AdaptiveTestRecommendation_testCostLabel.Text =
                    ((Label)((ImageButton)e.CommandSource).FindControl("AdaptiveTestRecommendation_testCostLabel")).Text; ;
                AdaptiveTestRecommendation_topCreateButton.Visible = true;
                AdaptiveTestRecommendation_topStartTestButton.Visible = false;
                AdaptiveTestRecommendation_bottomSuccessMessageLabel.Text = "";
                AdaptiveTestRecommendation_bottomErrorMessageLabel.Text = "";
                AdaptiveTestRecommendation_questionModalPopupExtender.Show();
            }
            catch(Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                    AdaptiveTestRecommendation_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the test grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void AdaptiveTestRecommendation_searchedTestGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState[ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE].ToString() == e.SortExpression)
                    ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE] =
                        (ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE].ToString() == "A") ? "D" : "A";
                else
                    ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE] = "A";
                ViewState[ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE] = e.SortExpression;
                AdaptiveTestRecommendation_pageNumberHiddenField.Value = "1";
                LoadAdaptiveGridBasedOnIndex(
                       Convert.ToInt32(AdaptiveTestRecommendation_selectedIndexHiddenField.Value), 1,
                    Convert.ToChar(ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE]),
                    ViewState[ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE].ToString());
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                    AdaptiveTestRecommendation_bottomErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the test grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void AdaptiveTestRecommendation_searchedTestGridView_RowCreated(object sender,
            GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType != DataControlRowType.Header)
                    return;
                AddSortImage(
                    GetSortColumnIndex(AdaptiveTestRecommendation_searchedTestGridView,
                    ViewState[ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE].ToString()), e.Row,
                    (ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE].ToString() == "A") ?
                    Forte.HCM.DataObjects.SortType.Ascending : Forte.HCM.DataObjects.SortType.Descending);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                    AdaptiveTestRecommendation_bottomErrorLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// This Method loads appropriate grid view based on the
        /// user selected drop down value.
        /// </summary>
        /// <param name="AdaptiveIndexToLoad">
        /// A<see cref="int"/>User selected adaptive index from drop down list.</param>
        /// <param name="PageNumber">
        /// A<see cref="int"/>holds the page number to be load in to the grid view.
        /// </param>
        /// <param name="OrderByExpression">
        /// A<see cref="char"/>holds a character that tells whether to get data either in
        /// ascending order or descending order.
        /// </param>
        /// <param name="SortExpression">
        /// A<see cref="string"/>holds the sort expression value of the gird view.
        /// </param>
        private void LoadAdaptiveGridBasedOnIndex(int AdaptiveIndexToLoad, int PageNumber, 
            char OrderByExpression, string SortExpression)
        {
            int TotalNoOfRecords = 0;
            switch (AdaptiveIndexToLoad)
            {
                case 1:
                    LoadAdaptiveGridBasedOnSimilarCandidate(
                         new TestBLManager().GetAdaptiveTestBySimilarProfile(base.userID, base.GridPageSize, PageNumber,
                                OrderByExpression, SortExpression, out TotalNoOfRecords),
                        PageNumber, OrderByExpression, SortExpression, TotalNoOfRecords);
                    break;
                case 2:
                    LoadAdaptiveGridBasedOnSimilarCandidate(
                         new TestBLManager().GetAdaptiveTestByAssessmentHistory(base.userID, base.GridPageSize, PageNumber,
                                OrderByExpression, SortExpression, out TotalNoOfRecords),
                        PageNumber, OrderByExpression, SortExpression, TotalNoOfRecords);
                    break;
                case 3:
                    LoadAdaptiveGridBasedOnSimilarCandidate(
                         new TestBLManager().GetAdaptiveTestBySimilarCandidate(base.userID, base.GridPageSize, PageNumber,
                                OrderByExpression, SortExpression, out TotalNoOfRecords),
                        PageNumber, OrderByExpression, SortExpression, TotalNoOfRecords);
                    break;
                default:
                    AdaptiveTestRecommendation_searchedTestGridView.DataSource = null;
                    AdaptiveTestRecommendation_searchedTestGridView.DataBind();
                    break;
            }
        }

        /// <summary>
        /// This method communicates with the BL Manager to get
        /// the similar tests of the candidate
        /// </summary>
        /// <param name="PageNumber">
        /// A<see cref="int"/>holds the page number to be load in to the grid view.
        /// </param>
        /// <param name="OrderByExpression">
        /// A<see cref="char"/>holds a character that tells whether to get data either in
        /// ascending order or descending order.
        /// </param>
        /// <param name="SortExpression">
        /// A<see cref="string"/>holds the sort expression value of the gird view.
        /// </param>
        private void LoadAdaptiveGridBasedOnSimilarCandidate(List<TestDetail> testDetails, int PageNumber,
            char OrderByExpression, string SortExpression, int TotalNoOfRecords)
        {
            AdaptiveTestRecommendation_searchedTestGridView.DataSource = testDetails;
            AdaptiveTestRecommendation_searchedTestGridView.DataBind();
            if (AdaptiveTestRecommendation_searchedTestGridView.Rows.Count == 0)
            {
                AdaptiveTestRecommendation_bottomPagingNavigator.Visible = false;
                AdaptiveTestRecommendation_testDetailsDiv.Visible = false;
                base.ShowMessage(AdaptiveTestRecommendation_topErrorMessageLabel,
                        AdaptiveTestRecommendation_bottomErrorLabel, Resources.HCMResource.Common_Empty_Grid);
            }
            else
            {
                AdaptiveTestRecommendation_testDetailsDiv.Visible = true;
                if (PageNumber == 1)
                    AdaptiveTestRecommendation_bottomPagingNavigator.Reset();
                AdaptiveTestRecommendation_bottomPagingNavigator.Visible = true;
                AdaptiveTestRecommendation_bottomPagingNavigator.TotalRecords = TotalNoOfRecords;
                AdaptiveTestRecommendation_bottomPagingNavigator.PageSize = base.GridPageSize;
            }
        }

        /// <summary>
        /// Checks and sets whether the div is expanded or restored.
        /// </summary>
        private void CheckAndSetExpandorRestore()
        {
            if (!Forte.HCM.Support.Utility.IsNullOrEmpty(AdaptiveTestRecommendation_isMaximizesStateHiddenField.Value) &&
                         AdaptiveTestRecommendation_isMaximizesStateHiddenField.Value == "Y")
            {
                AdaptiveTestRecommendation_recommendTypeTR.Style["display"] = "none";
                AdaptiveTestRecommendation_testDetailsUpSpan.Style["display"] = "block";
                AdaptiveTestRecommendation_testDetailsDownSpan.Style["display"] = "none";
                AdaptiveTestRecommendation_testDetailsDiv.Style["height"] = EXPANDED_HEIGHT;
            }
            else
            {
                AdaptiveTestRecommendation_recommendTypeTR.Style["display"] = "block";
                AdaptiveTestRecommendation_testDetailsUpSpan.Style["display"] = "none";
                AdaptiveTestRecommendation_testDetailsDownSpan.Style["display"] = "block";
                AdaptiveTestRecommendation_testDetailsDiv.Style["height"] = RESTORED_HEIGHT;
            }
        }

        /// <summary>
        /// This method subscribes the javascript handlers
        /// </summary>
        private void SubscribeClientScriptHandlers()
        {
            AdaptiveTestRecommendation_testDetailsUpSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                AdaptiveTestRecommendation_testDetailsDiv.ClientID + "','" +
                AdaptiveTestRecommendation_recommendTypeTR.ClientID + "','" +
                AdaptiveTestRecommendation_testDetailsUpSpan.ClientID + "','" +
                AdaptiveTestRecommendation_testDetailsDownSpan.ClientID + "','" +
                AdaptiveTestRecommendation_isMaximizesStateHiddenField.ClientID + "','" +
                 RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
            AdaptiveTestRecommendation_testDetailsDownSpan.Attributes.Add("onclick", "ExpandOrRestore('" +
                AdaptiveTestRecommendation_testDetailsDiv.ClientID + "','" +
                AdaptiveTestRecommendation_recommendTypeTR.ClientID + "','" +
                AdaptiveTestRecommendation_testDetailsUpSpan.ClientID + "','" +
                AdaptiveTestRecommendation_testDetailsDownSpan.ClientID + "','" +
                AdaptiveTestRecommendation_isMaximizesStateHiddenField.ClientID + "','" +
                RESTORED_HEIGHT + "','" + EXPANDED_HEIGHT + "');");
        }

        /// <summary>
        /// This method checks whether the page load occured correct or not.
        /// </summary>
        /// <param name="IndexToLoad">
        /// A<see cref="int"/>is out type of paramter
        /// which returns the tests that to be loaded in the grid view.
        /// </param>
        /// <returns>true if has exact query string else false.</returns>
        private bool CheckForValidRequest(out int IndexToLoad)
        {
            bool isValid = false;
            bool KeyFound = false;
            IndexToLoad = 0;
            if (!Request.QueryString.HasKeys())
                return isValid;
            for (int i = 0; i < Request.QueryString.Keys.Count; i++)
                if (Request.QueryString.Keys[i] == "id")
                {
                    KeyFound = true;
                    break;
                }

            if (!KeyFound)
                return isValid;
            int.TryParse(Request.QueryString["id"], out IndexToLoad);
            if ((IndexToLoad == 0) || (IndexToLoad > 3))
                return isValid;
            isValid = true;
            return isValid;
        }

        /// <summary>
        /// This method initializes the view state of sort expression and sort order.
        /// </summary>
        private void InitializeSortVariables()
        {
            ViewState[ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE] = "TESTID";
            ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE] = "A";
        }

        /// <summary>
        /// This method creates the session for the test.
        /// </summary>
        private void CreateTestSession()
        {
            TestScheduleDetail testSchedule = null;
            try
            {
                testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = base.userID.ToString();
                //  testSchedule.CandidateTestSessionID = candidateSessionID.ToString();
                testSchedule.AttemptID = 1;
                testSchedule.EmailId = ((UserDetail)Session["USER_DETAIL"]).Email;
                testSchedule.ExpiryDate = DateTime.Now.AddDays(base.candidateExpiryDays);
                //   new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID);
                //string testSessionID = null;
                string candidateSessionID = null;
                //// Call save test session BL method.
                new TestBLManager().SaveTestSessionScheduleCandidate(GetTestSessionDetails(), testSchedule,
                    base.userID, out candidateSessionID);
                AdaptiveTestRecommendation_topStartTestButton.Visible = true;
                AdaptiveTestRecommendation_topCreateButton.Visible = false;
                AdaptiveTestRecommendation_bottomSuccessMessageLabel.Text = "Session created successfully";
                AdaptiveTestRecommendation_candidateSessionIDHiddenField.Value = candidateSessionID;
                AdaptiveTestRecommendation_questionModalPopupExtender.Show();
                LoadAdaptiveGridBasedOnIndex(
                    Convert.ToInt32(AdaptiveTestRecommendation_selectedIndexHiddenField.Value),
                    Convert.ToInt32(AdaptiveTestRecommendation_pageNumberHiddenField.Value),
                    Convert.ToChar(ViewState[ADAPTIVETESTRECOMMENDATION_SORT_ORDER_VIEWSTATE]),
                    ViewState[ADAPTIVETESTRECOMMENDATION_SORT_EXPRESSION_VIEWSTATE].ToString());
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSchedule)) testSchedule = null;
            }
        }

        /// <summary>
        /// This method loads the test details in to test session details 
        /// data object.
        /// </summary>
        /// <returns>Test session detail data object that contains the test details.</returns>
        private TestSessionDetail GetTestSessionDetails()
        {
            // Initialize test session detail object
            TestSessionDetail testSessionDetail = null;
            try
            {
                testSessionDetail = new TestSessionDetail();
                // Set test name
                testSessionDetail.TestID = AdaptiveTestRecommendation_testIDLabel.Text;
                testSessionDetail.TestName = AdaptiveTestRecommendation_testNameLabel.Text;
                // Set number of candidate session (session count)
                testSessionDetail.NumberOfCandidateSessions = 1;
                // Set total credits limit
                testSessionDetail.TotalCredit =
                    Convert.ToDecimal(AdaptiveTestRecommendation_testCostLabel.Text);
                // Set time limit
                testSessionDetail.TimeLimit = Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(
                    AdaptiveTestRecommendation_timeLimitLabel.Text);
                testSessionDetail.ClientRequestID = "0";
                // Set expiry date
                testSessionDetail.ExpiryDate = DateTime.Now;
                // Set random question order status
                testSessionDetail.IsRandomizeQuestionsOrdering = false;
                // Set display result status 
                testSessionDetail.IsDisplayResultsToCandidate = true;
                // Set cyber proctoring status
                testSessionDetail.IsCyberProctoringEnabled = false;
                // Set created by
                testSessionDetail.CreatedBy = base.userID;
                // Set modified by
                testSessionDetail.ModifiedBy = base.userID;
                // Set test instructions
                testSessionDetail.Instructions = Resources.HCMResource.CandidateSearchTest_Instructions.ToString();
                // Set session descriptions
                testSessionDetail.TestSessionDesc = Resources.HCMResource.CandidateSearchTest_TestSessionDesc.ToString();
                return testSessionDetail;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionDetail)) testSessionDetail = null;
            }
        }

        #endregion Private Methods

        #region Protected Methods                                              

        /// <summary>
        /// Convert seconds to hh:mm:ss format 
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        protected static string ConvertSecondsToHoursMinutesSeconds(int seconds)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(seconds);

            string hoursMinutesSeconds = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    timeSpan.Hours,
                                    timeSpan.Minutes,
                                    timeSpan.Seconds);

            return hoursMinutesSeconds;
        }
        #endregion Protected Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            AdaptiveTestRecommendation_testDetailsDiv.Visible = false;
            AdaptiveTestRecommendation_startTestPopupExtenderControl.Message = "Are you ready to start the test ?";
            AdaptiveTestRecommendation_startTestPopupExtenderControl.Title = "Start Test";
            AdaptiveTestRecommendation_startTestPopupExtenderControl.Type = MessageBoxType.YesNo;
            AdaptiveTestRecommendation_userNameLabel.Text =
                ((Forte.HCM.DataObjects.UserDetail)Session["USER_DETAIL"]).FirstName;
            AdaptiveTestRecommendation_pageNumberHiddenField.Value = "1";
            int IndexToLoad = 0;
            if (!CheckForValidRequest(out IndexToLoad))
                Response.Redirect("~/TestCenter/AdaptiveTestSummary.aspx", false);
            AdaptiveTestRecommendation_selectTypeDropDownList.Items.FindByValue(IndexToLoad.ToString()).Selected = true;
            AdaptiveTestRecommendation_selectedIndexHiddenField.Value = AdaptiveTestRecommendation_selectTypeDropDownList.SelectedValue;
            InitializeSortVariables();
            LoadAdaptiveGridBasedOnIndex(IndexToLoad, 1, 'A', "TESTID");
            SubscribeClientScriptHandlers();
        }

        #endregion Protected Overridden Methods 
    }
}