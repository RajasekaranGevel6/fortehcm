<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdaptiveTestSummary.aspx.cs"
    Inherits="Forte.HCM.UI.CandidateCenter.AdaptiveTestSummary" MasterPageFile="~/MasterPages/HCMCandidateMaster.Master"
    Title="Adaptive Test Recommentation" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/HCMCandidateMaster.Master" %>
<asp:Content ID="AdaptiveTestSummary_content" runat="server" ContentPlaceHolderID="HCMCandidateMaster_contentPlaceHolder">

    <script type="text/javascript" language="javascript">

        // This will invoke the start test modalpopupextender
        function StartTest() {
            $find("<%= AdaptiveTestSummary_startTestPopupExtender.ClientID  %>").show();
            return false;
        }

        // This method assigns the value to the appropriate test details label controls
        // and invokes the test details modalpopupextender
        function showTestDetailsPopup(testName, testDesc, NoOfQuestions, Recommended, Cost, Certification) {
            document.getElementById("<%= AdaptiveTestSummary_testLabel.ClientID %>").innerHTML = testName;
            document.getElementById("<%= AdaptievTestSummary_testDescriptionDiv.ClientID %>").innerHTML = testDesc;
            document.getElementById("<%= AdaptiveTestSummary_noOfquestionLabel.ClientID %>").innerHTML = NoOfQuestions;
            document.getElementById("<%= AdaptiveTestSummary_timeLimitLabel.ClientID %>").innerHTML = Recommended;
            document.getElementById("<%= AdaptiveTestSummary_testCostLabel.ClientID %>").innerHTML = Cost;
            document.getElementById("<%= AdaptiveTestSummary_certificationLabel.ClientID %>").innerHTML = (Certification == 'True') ? "Yes" : "No";
            $find("<%= AdaptiveTestSummary_testDetailsSummaryModalPopUpExtender.ClientID %>").show();
            return false;
        }
    </script>

    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="AdaptiveTestSummary_updatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel Width="100%" ID="AdaptiveTestSummary_dragHandlePanel" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="header_bg">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="72%" class="header_text_bold">
                                                    <asp:Literal ID="AdaptiveTestSummary_headerLiteral" runat="server" Text="Adaptive Test Summary"></asp:Literal>
                                                </td>
                                                <td width="28%" align="right">
                                                    <asp:LinkButton ID="AdaptiveTestSummary_topCancelButton" runat="server" SkinID="sknActionLinkButton"
                                                        Text="Cancel" OnClick="AdaptiveTestSummary_topCancelButton_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="msg_align" align="center">
                                        <asp:UpdatePanel ID="AdaptiveTestSummary_topMessagesUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="AdaptiveTestSummary_topSuccessMessageLabel" EnableViewState="false"
                                                    runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="AdaptiveTestSummary_topErrorMessageLabel" runat="server" EnableViewState="false"
                                                    SkinID="sknErrorMessage"></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="adap_bg" style="width: 100%; height: 143px; table-layout: fixed" align="center"
                                        valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td colspan="3" class="adap_top_line">
                                                    <table cellpadding="0" cellspacing="3" width="100%" border="0">
                                                        <tr>
                                                            <td align="left" class="adap_header_text" valign="top">
                                                                <asp:Literal ID="AdaptiveTestSummary_basedOnProfileLiteral" runat="server" Text="Based On Your Profile">
                                                                </asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px">
                                                    <asp:Image ID="AdaptiveTestSummary_profileBasedImage" runat="server" SkinID="sknProfileBasedImage" />
                                                </td>
                                                <td style="width: 1%;">
                                                </td>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="AdaptiveTestSummary_basedOnProfileErrorLabel" SkinID="sknErrorMessage"
                                                                    runat="server" EnableViewState="false"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table id="AdaptiveTestSummary_basedOnProfileTable" cellpadding="0" cellspacing="0"
                                                                    runat="server" width="99%" border="0">
                                                                    <tr>
                                                                        <td class="adap_arrow_bg" align="center">
                                                                            <asp:ImageButton ID="AdaptiveTestSummary_basedOnProfilePrevImageButton" runat="server"
                                                                                SkinID="sknAdapPrevImageButton" CommandName="BasedOnProfile" CommandArgument="-1"
                                                                                ToolTip="Previous" OnClick="AdaptiveTestSummary_previousButtonClick" />
                                                                        </td>
                                                                        <td class="adap_inside_bg">
                                                                            <asp:HiddenField ID="AdaptiveTestSummary_basedOnProfilePagingHiddenField" runat="server"
                                                                                Value="1" />
                                                                            <asp:DataList ID="AdaptiveTestSummary_basedOnProfileDatalist" runat="server" RepeatColumns="4"
                                                                                RepeatDirection="Vertical" Width="100%" OnItemCommand="AdaptiveTestSummary_basedOnProfileDatalist_ItemCommand">
                                                                                <ItemTemplate>
                                                                                    <div style="float: left; height: 60px; width: 92%;" id="AdaptiveTestSummary_similiarCandidateTestDataList_innerDIV">
                                                                                        <table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <center>
                                                                                                        <table width="95%" cellpadding="2" cellspacing="2" border="0" align="center">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="AdaptiveTestSummary_similiarProfileTestQuestionsHeadingLabel" runat="server"
                                                                                                                        SkinID="sknLabelFieldHeaderText" Text='<%# TrimContent(Eval("Name").ToString(),20) %>'>
                                                                                                                    </asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_similiarProfileTestDescriptionLinkButton"
                                                                                                                        runat="server" SkinID="sknActionLinkButton" Text='<%# TrimContent(Eval("Description").ToString(),20) %>'
                                                                                                                        OnClick='<%#String.Format(" return showTestDetailsPopup(&#39;{4}&#39;,&#39;{5}&#39;,&#39;{0}&#39;,&#39;{1}&#39;,&#39;{2}&#39;,&#39;{3}&#39;)", Eval("NoOfQuestions"),ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(Eval("RecommendedCompletionTime"))),Eval("TestCost"),Eval("IsCertification"),Eval("Name"),Eval("Description")) %>'></asp:LinkButton>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_similiarProfileTestViewLinkButton" runat="server"
                                                                                                                        OnClick='<%#String.Format(" return OpenAdditionalTestDetail(&#39;{0}&#39;)", Eval("TestKey")) %>'
                                                                                                                        Text="View" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_similiarProfileTestCreateSessionLinkButton"
                                                                                                                        runat="server" Text="Create Session" SkinID="sknActionLinkButton" CommandName="CreateSession">
                                                                                                                    </asp:LinkButton>
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarProfileTestKeyHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("TestKey") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarProfileTestDescriptionHiddenField"
                                                                                                                        runat="server" Value='<%# Eval("Description") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarProfileTestNameHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("Name") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarProfileTestCostHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("TestCost") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarProfileTimeLimitHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("RecommendedCompletionTime") %>' />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </center>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div style="float: right; height: 60px;" id="AdaptiveTestSummary_basedOnCandidateTestLineDiv"
                                                                                        runat="server">
                                                                                        <table width="100%" style="height: 100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td class="adap_inside_line">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:DataList>
                                                                        </td>
                                                                        <td class="adap_arrow_bg_001" align="center">
                                                                            <asp:ImageButton ID="AdaptiveTestSummary_basedOnProfileNextImageButton" runat="server"
                                                                                SkinID="sknAdapNextImageButton" ToolTip="Next" CommandName="BasedOnProfile" CommandArgument="1"
                                                                                OnClick="AdaptiveTestSummary_nextImageButton_Click" />
                                                                        </td>
                                                                        <td class="more_link" valign="bottom">
                                                                            <asp:LinkButton ID="AdaptiveTestSummary_basedOnProfileMoreLinkButton" runat="server"
                                                                                PostBackUrl="~/TestCenter/AdaptiveTestRecommendation.aspx?id=1" Text="More >>"
                                                                                SkinID="sknActionLinkButton" CommandArgument="1"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="adap_bg" style="width: 100%; height: 143px; table-layout: fixed" align="center"
                                        valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="adap_top_line" colspan="3">
                                                    <table cellpadding="0" cellspacing="3" width="100%" border="0">
                                                        <tr>
                                                            <td align="left" class="adap_header_text" valign="top">
                                                                <asp:Literal ID="AdaptiveTestSummary_assessmentHistoryHeaderLiteral" runat="server"
                                                                    Text="Based On Your Past Assessments">
                                                                </asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px">
                                                    <asp:Image ID="AdaptiveTestSummary_historyBasedImage" runat="server" SkinID="sknHistoryBasedImage" />
                                                </td>
                                                <td style="width: 1%;">
                                                </td>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="AdaptiveTestSummary_historyBasedErrorMessageLabel" SkinID="sknErrorMessage"
                                                                    runat="server" EnableViewState="false"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table id="AdaptiveTestSummary_historyBasedTable" runat="server" cellpadding="0"
                                                                    cellspacing="0" width="99%" border="0">
                                                                    <tr>
                                                                        <td class="adap_arrow_bg" align="center">
                                                                            <asp:ImageButton ID="AdaptiveTestSummary_basedOnHistoryPrevImageButton" runat="server"
                                                                                SkinID="sknAdapPrevImageButton" CommandArgument="-1" CommandName="BasedOnAssessment"
                                                                                ToolTip="Previous" OnClick="AdaptiveTestSummary_previousButtonClick" />
                                                                        </td>
                                                                        <td class="adap_inside_bg">
                                                                            <asp:HiddenField ID="AdaptiveTestSummary_assessmentHistoryPagingHiddenField" runat="server"
                                                                                Value="1" />
                                                                            <asp:DataList ID="AdaptiveTestSummary_assessmentHistoryDataList" runat="server" RepeatColumns="4"
                                                                                RepeatDirection="Vertical" Width="100%" OnItemCommand="AdaptiveTestSummary_assessmentHistoryDataList_ItemCommand">
                                                                                <ItemTemplate>
                                                                                    <div style="float: left; height: 60px; width: 92%;" id="AdaptiveTestSummary_similiarCandidateTestDataList_innerDIV">
                                                                                        <table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <center>
                                                                                                        <table cellpadding="2" cellspacing="2" border="0" width="95%" align="left">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="AdaptiveTestSummary_assessmentHistoryTestQuestionsHeadingLabel" runat="server"
                                                                                                                        SkinID="sknLabelFieldHeaderText" Text='<%# TrimContent(Eval("Name").ToString(),20) %>'>
                                                                                                                    </asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_assessmentHistoryTestDescriptionLinkButton"
                                                                                                                        runat="server" SkinID="sknActionLinkButton" Text='<%# TrimContent(Eval("Description").ToString(),20) %>'
                                                                                                                        OnClick='<%#String.Format(" return showTestDetailsPopup(&#39;{4}&#39;,&#39;{5}&#39;,&#39;{0}&#39;,&#39;{1}&#39;,&#39;{2}&#39;,&#39;{3}&#39;)", Eval("NoOfQuestions"),ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(Eval("RecommendedCompletionTime"))),Eval("TestCost"),Eval("IsCertification"),Eval("Name"),Eval("Description")) %>'></asp:LinkButton>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_assessmentHistoryTestViewLinkButton" runat="server"
                                                                                                                        OnClick='<%#String.Format(" return OpenAdditionalTestDetail(&#39;{0}&#39;)", Eval("TestKey")) %>'
                                                                                                                        Text="View" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_assessmentHistoryTestCreateSessionLinkButton"
                                                                                                                        runat="server" Text="Create Session" SkinID="sknActionLinkButton" CommandName="CreateSession">
                                                                                                                    </asp:LinkButton>
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_assessmentHistoryTestKeyHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("TestKey") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_assessmentHistoryTestDescriptionHiddenField"
                                                                                                                        runat="server" Value='<%# Eval("Description") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_assessmentHistoryTestNameHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("Name") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_assessmentHistoryTestCostHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("TestCost") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_assessmentHistoryTimeLimitHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("RecommendedCompletionTime") %>' />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </center>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div style="float: right; height: 60px;" id="AdaptiveTestSummary_basedOnCandidateTestLineDiv"
                                                                                        runat="server">
                                                                                        <table width="100%" style="height: 100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td class="adap_inside_line">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:DataList>
                                                                        </td>
                                                                        <td class="adap_arrow_bg_001" align="center">
                                                                            <asp:ImageButton ID="AdaptiveTestSummary_historyBasedPrevImageButton" runat="server"
                                                                                SkinID="sknAdapNextImageButton" CommandName="BasedOnAssessment" CommandArgument="1"
                                                                                ToolTip="Next" OnClick="AdaptiveTestSummary_nextImageButton_Click" />
                                                                        </td>
                                                                        <td class="more_link" valign="bottom">
                                                                            <asp:LinkButton ID="AdaptiveTestSummary_historyBasedMoreLinkButton" runat="server"
                                                                                PostBackUrl="~/TestCenter/AdaptiveTestRecommendation.aspx?id=2" Text="More >>"
                                                                                SkinID="sknActionLinkButton" CommandArgument="2"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="adap_bg" style="width: 100%" valign="top">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0" id="AdaptiveTestSummary_similiarCandidateTestDiv">
                                            <tr>
                                                <td colspan="3" class="adap_top_line">
                                                    <table cellpadding="0" cellspacing="3" width="100%" border="0">
                                                        <tr>
                                                            <td align="left" class="adap_header_text" valign="top">
                                                                <asp:Literal ID="AdaptiveTestSummary_similiarCandidateTestHeaderLiteral" runat="server"
                                                                    Text="Based On Tests Taken By Candidates With Profiles Similar To Yours">
                                                                </asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px">
                                                    <asp:Image ID="AdaptiveTestSummary_similiarCandidateTestImage" runat="server" SkinID="sknCandidateBasedImage" />
                                                </td>
                                                <td style="width: 1%;">
                                                </td>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="AdaptiveTestSummary_similarCandidateTestErrorLabel" runat="server"
                                                                    SkinID="sknErrorMessage" EnableViewState="false"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table id="AdaptiveTestSummery_similarCandidateTestTable" cellpadding="0" cellspacing="0"
                                                                    runat="server" width="99%">
                                                                    <tr>
                                                                        <td class="adap_arrow_bg" align="center">
                                                                            <asp:ImageButton ID="AdaptiveTestSummary_similiarCandidatePrevTestImageButton" runat="server"
                                                                                SkinID="sknAdapPrevImageButton" CommandName="BasedOnTests" CommandArgument="-1"
                                                                                ToolTip="Previous" OnClick="AdaptiveTestSummary_previousButtonClick" />
                                                                        </td>
                                                                        <td class="adap_inside_bg">
                                                                            <asp:HiddenField ID="AdaptiveTestSummary_similarCandidatePagingHiddenField" runat="server"
                                                                                Value="1" />
                                                                            <asp:DataList ID="AdaptiveTestSummary_similiarCandidateTestDataList" runat="server"
                                                                                RepeatColumns="4" RepeatDirection="Vertical" Width="100%" OnItemCommand="AdaptiveTestSummary_similiarCandidateTestDataList_ItemCommand">
                                                                                <ItemTemplate>
                                                                                    <div style="float: left; height: 60px; width: 92%;" id="AdaptiveTestSummary_similiarCandidateTestDataList_innerDIV">
                                                                                        <table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <center>
                                                                                                        <table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Label ID="AdaptiveTestSummary_similiarCandidateTestQuestionsHeadingLabel" runat="server"
                                                                                                                        SkinID="sknLabelFieldHeaderText" Text='<%# TrimContent(Eval("Name").ToString(),20) %>'>
                                                                                                                    </asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_similiarCandidateTestDescriptionLinkButton"
                                                                                                                        runat="server" SkinID="sknActionLinkButton" Text='<%# TrimContent(Eval("Description").ToString(),20) %>'
                                                                                                                        OnClick='<%#String.Format(" return showTestDetailsPopup(&#39;{4}&#39;,&#39;{5}&#39;,&#39;{0}&#39;,&#39;{1}&#39;,&#39;{2}&#39;,&#39;{3}&#39;)", Eval("NoOfQuestions"),ConvertSecondsToHoursMinutesSeconds(Convert.ToInt32(Eval("RecommendedCompletionTime"))),Eval("TestCost"),Eval("IsCertification"),Eval("Name"),Eval("Description")) %>'></asp:LinkButton>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_similiarCandidateTestViewLinkButton" runat="server"
                                                                                                                        OnClick='<%#String.Format(" return OpenAdditionalTestDetail(&#39;{0}&#39;)", Eval("TestKey")) %>'
                                                                                                                        Text="View" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:LinkButton ID="AdaptiveTestSummary_similiarCandidateTestCreateSessionLinkButton"
                                                                                                                        runat="server" Text="Create Session" SkinID="sknActionLinkButton" CommandName="CreateSession">
                                                                                                                    </asp:LinkButton>
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarCandidateTestKeyHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("TestKey") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarCandidateTestDescriptionHiddenField"
                                                                                                                        runat="server" Value='<%# Eval("Description") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarCandidateTestNameHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("Name") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarCandidateTestCostHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("TestCost") %>' />
                                                                                                                    <asp:HiddenField ID="AdaptiveTestSummary_similarCandidateTimeLimitHiddenField" runat="server"
                                                                                                                        Value='<%# Eval("RecommendedCompletionTime") %>' />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </center>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div style="float: right; height: 60px;" id="AdaptiveTestSummary_basedOnCandidateTestLineDiv"
                                                                                        runat="server">
                                                                                        <table width="100%" style="height: 100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td class="adap_inside_line">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:DataList>
                                                                        </td>
                                                                        <td class="adap_arrow_bg_001" align="center">
                                                                            <asp:ImageButton ID="AdaptiveTestSummary_similiarCandidateTestNextImageButton" runat="server"
                                                                                SkinID="sknAdapNextImageButton" CommandName="BasedOnTests" CommandArgument="1"
                                                                                ToolTip="Next" OnClick="AdaptiveTestSummary_nextImageButton_Click" />
                                                                        </td>
                                                                        <td class="more_link" valign="bottom">
                                                                            <asp:LinkButton ID="AdaptiveTestSummary_similiarCandidateTestMoreLinkButton" runat="server"
                                                                                PostBackUrl="~/TestCenter/AdaptiveTestRecommendation.aspx?id=3" Text="More &gt;&gt;"
                                                                                SkinID="sknActionLinkButton" CommandArgument="3"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="msg_align" align="center">
                <asp:UpdatePanel ID="AdaptiveTestSummary_bottomMessagesUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="AdaptiveTestSummary_bottomSuccessMessageLabel" runat="server" EnableViewState="false"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="AdaptiveTestSummary_bottomErrorMessageLabel" EnableViewState="false"
                            runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="AdaptiveTestSummary_popUpUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <asp:Panel ID="AdaptiveTestSummary_testPreviewPanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_candidate_search_test_session" Width="660px">
                                        <div style="display: none">
                                            <asp:Button ID="AdaptiveTestSummary_testPreviewhiddenButton" runat="server" Text="Hidden"
                                                SkinID="sknCloseImageButton" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="AdaptiveTestSummary_testPreviewHeaderLiteral" runat="server" Text="Create Test Session"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="AdaptiveTestSummary_testPreviewTopCancelImageButton" SkinID="sknCloseImageButton"
                                                                                runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="4">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTopSuccessMessageLabel" runat="server"
                                                                                EnableViewState="false" SkinID="sknSuccessMessage"></asp:Label><asp:Label ID="AdaptiveTestSummary_testPreviewTopErrorMessageLabel"
                                                                                    runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTestIDHeadLabel" runat="server" Text="Test ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTestIDLabel" runat="server" Text=""
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTestNameHeadLabel" runat="server" Text="Test Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTestNameLabel" runat="server" Text=""
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTestCostHeadLabel" runat="server" Text="Test Cost (in $)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTestCostLabel" runat="server" Text=""
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTimeLimitHeadLabel" runat="server"
                                                                                Text="Time Limit" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTimeLimitLabel" runat="server" Text=""
                                                                                SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" colspan="4">
                                                                            <asp:Label ID="AdaptiveTestSummary_testPreviewTestDescriptionLabel" runat="server"
                                                                                Text="Test Description" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <div style="width: 100%; height: 30px; overflow: auto;" class="label_field_text_div_tag"
                                                                                id="AdaptiveTestSummary_testDescriptionDiv" runat="server">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Button ID="AdaptiveTestSummary_testPreviewCreateButton" runat="server" Text="Create"
                                                                    SkinID="sknButtonId" OnClick="AdaptiveTestSummary_testPreviewCreateButton_Click" />
                                                                <asp:Button ID="AdaptiveTestSummary_testPreviewStartTestButton" runat="server" Text="Start Test"
                                                                    SkinID="sknButtonId" Visible="false" OnClientClick="javascript:return StartTest()" />
                                                                &nbsp;&nbsp;<asp:LinkButton ID="AdaptiveTestSummary_testPreviewCloseLinkButton" SkinID="sknPopupLinkButton"
                                                                    runat="server" Text="Cancel" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="AdaptiveTestSummary_testPreviewModalPopupExtender"
                                        runat="server" PopupControlID="AdaptiveTestSummary_testPreviewPanel" TargetControlID="AdaptiveTestSummary_testPreviewhiddenButton"
                                        BackgroundCssClass="modalBackground" CancelControlID="AdaptiveTestSummary_testPreviewCloseLinkButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="AdaptiveTestSummary_testDetailsSummaryPanel" runat="server" Style="display: none;"
                                        CssClass="small_popup_control">
                                        <div style="display: none">
                                            <asp:Button ID="AdaptiveTestSummary_testDetailsSummaryButton" runat="server" Text="Hidden" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 80%;">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="AdaptiveTestSummary_testDeatilSummaryPanelHeadLiteral" runat="server"
                                                                    Text="Test Details"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="AdaptiveTestSummary_testDetailsTopCancelButon" SkinID="sknCloseImageButton"
                                                                                runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="height: 10px;">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table border="0" cellpadding="0" cellspacing="3" class="table_outline_bg_preview"
                                                                    width="100%">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div class="label_multi_field_text">
                                                                                <asp:Label ID="AdaptiveTestSummary_testLabel" runat="server" SkinID="sknLabelFieldHeaderText"></asp:Label></div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div id="AdaptievTestSummary_testDescriptionDiv" runat="server" style="width: 100%;
                                                                                height: 28px; overflow: auto;">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%" align="left" colspan="2">
                                                                            <table border="0" cellpadding="0" cellspacing="3" width="100%" border="0">
                                                                                <tr>
                                                                                    <td style="width: 13%">
                                                                                        <asp:Label ID="AdaptiveTestSummary_noOfquestionHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Questions "></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="AdaptiveTestSummary_noOfquestionLabel" runat="server" ReadOnly="true"
                                                                                            SkinID="sknLabelFieldText" Text="35"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 10%">
                                                                                        <asp:Label ID="AdaptiveTestSummary_timeLimitHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Duration"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="AdaptiveTestSummary_timeLimitLabel" runat="server" ReadOnly="true"
                                                                                            SkinID="sknLabelFieldText" Text="00:30:00"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 14%">
                                                                                        <asp:Label ID="AdaptiveTestSummary_testCostHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Cost (in $)"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="AdaptiveTestSummary_testCostLabel" runat="server" ReadOnly="true"
                                                                                            SkinID="sknLabelFieldText" Text="5.6"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label ID="AdaptiveTestSummary_certificationHeadLabel" runat="server" SkinID="sknLabelFieldHeaderText"
                                                                                            Text="Certification"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="AdaptiveTestSummary_certificationLabel" runat="server" ReadOnly="true"
                                                                                            SkinID="sknLabelFieldText" Text="Yes"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table style="float: left;" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:LinkButton ID="AdaptiveTestSummary_closeLinkButton" SkinID="sknPopupLinkButton"
                                                                                runat="server" Text="Cancel"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="AdaptiveTestSummary_testDetailsSummaryModalPopUpExtender"
                                        runat="server" PopupControlID="AdaptiveTestSummary_testDetailsSummaryPanel" TargetControlID="AdaptiveTestSummary_testDetailsSummaryButton"
                                        BackgroundCssClass="modalBackground" CancelControlID="AdaptiveTestSummary_closeLinkButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="AdaptiveTestSummary_startTestUpdatePanel" runat="server">
                                        <ContentTemplate>
                                            <asp:Panel ID="AdaptiveTestSummary_startTestPopupPanel" runat="server" Style="display: none;
                                                height: 206px" CssClass="popupcontrol_confirm">
                                                <div id="AdaptiveTestSummary_startTestHiddenDIV" style="display: none">
                                                    <asp:Button ID="AdaptiveTestSummary_startTestHiddenButton" runat="server" />
                                                </div>
                                                <uc3:ConfirmMsgControl ID="AdaptiveTestSummary_startTestPopupExtenderControl" runat="server"
                                                    OnOkClick="AdaptiveTestSummary_startTestButton_OkClick" OnCancelClick="AdaptiveTestSummary_startTestButton_CancelClick" />
                                                <asp:HiddenField ID="AdaptiveTestSummary_testCandidateSessionIdHiddenField" runat="server" />
                                            </asp:Panel>
                                            <ajaxToolKit:ModalPopupExtender ID="AdaptiveTestSummary_startTestPopupExtender" runat="server"
                                                PopupControlID="AdaptiveTestSummary_startTestPopupPanel" TargetControlID="AdaptiveTestSummary_startTestHiddenButton"
                                                BackgroundCssClass="modalBackground">
                                            </ajaxToolKit:ModalPopupExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
