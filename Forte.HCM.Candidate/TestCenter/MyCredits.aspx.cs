﻿#region Header

// Copyright (C) Forte. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MyCredits.aspx.cs
// File that that represents the user interface layout and functionalities 
// for the MyCredits page. This page helps to view user credits.
// This class inherits Forte.HCM.UI.Common.PageBase class.

#endregion

#region Directives

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the MyCredits class that defines the user interface
    /// layout and functionalities for the My Credits page. This page helps to 
    /// view user credits.This class inherits Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class MyCredits : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> constant that holds the restored height of
        /// search results display panel.
        /// </summary>
        private const string RESTORED_HEIGHT = "203px";

        /// <summary>
        /// A <see cref="string"/> constant that holds the expanded height of
        /// search results display panel.
        /// </summary>
        private const string EXPANDED_HEIGHT = "226px";

        #endregion Private Constants                                           

        #region Events Handlers                                                

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Redirect to dashboard page.
                Response.Redirect("~/Dashboard.aspx", false);

                // Do not allow limited user.
                if (base.isLimited)
                    Response.Redirect("~/ActivityHome.aspx", false);

                // Set page caption
                Master.SetPageCaption(Resources.HCMResource.MyCredits_Title);

                // Show/hide menu based on user type (limited or not).
                Master.ShowMenu = !base.isLimited;

                // Subscribes to the page number click event.
                MyCredits_bottomPageNavigationControl.PageNumberClick += new
                    PageNavigator.PageNumberClickEventHandler
                    (MyCredits_bottomPageNavigationControl_PageNumberClick);
                // Set Expanded or restored height
                if (!Utility.IsNullOrEmpty(MyCredits_isMaximizedHiddenField.Value) &&
                   MyCredits_isMaximizedHiddenField.Value == "Y")
                {
                    MyCredits_creditsDiv.Style["display"] = "none";
                    MyCredits_creditsHistoryUpSpan.Style["display"] = "block";
                    MyCredits_creditsHistoryDownSpan.Style["display"] = "none";
                    MyCredits_creditsHistoryDiv.Style["height"] = EXPANDED_HEIGHT;
                }
                else
                {
                    MyCredits_creditsDiv.Style["display"] = "block";
                    MyCredits_creditsHistoryUpSpan.Style["display"] = "none";
                    MyCredits_creditsHistoryDownSpan.Style["display"] = "block";
                    MyCredits_creditsHistoryDiv.Style["height"] = RESTORED_HEIGHT;
                }
                //Set attribute to purchase credit buttons
                MyCredits_bottomPurchaseCreditsButton.Attributes.Add("OnClick",
                    "Javascript:return ShowCreditRequest();");
                MyCredits_topPurchaseCreditsButton.Attributes.Add("OnClick",
                    "Javascript:return ShowCreditRequest();");
                //Load values
                if (!IsPostBack)
                {
                    MyCredits_creditsHistoryDiv.Visible = false;
                    LoadValues();
                }
                else
                    MyCredits_creditsHistoryDiv.Visible = true;

                //Set attribute to expand/reset TR
                MyCredits_creditsUsageHistoryResultsTR.Attributes.Add("onclick",
                    "ExpandOrRestore('" +
                    MyCredits_creditsHistoryDiv.ClientID + "','" +
                    MyCredits_creditsDiv.ClientID + "','" +
                    MyCredits_creditsHistoryUpSpan.ClientID + "','" +
                    MyCredits_creditsHistoryDownSpan.ClientID + "','" +
                    MyCredits_isMaximizedHiddenField.ClientID + "','" +
                    RESTORED_HEIGHT + "','" +
                    EXPANDED_HEIGHT + "')");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyCredits_topErrorMessageLabel,
                    MyCredits_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the drop down selected index changed 
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will display the results in the
        /// grid for appropriate selected value.
        /// </remarks>
        protected void MyCredits_creditsTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                MyCredits_topErrorMessageLabel.Text = string.Empty;
                MyCredits_bottomErrorMessageLabel.Text = string.Empty;
                // Reset default sort field and order keys.
                ViewState["SORT_ORDER"] = SortType.Ascending;
                ViewState["SORT_FIELD"] = "DATE";
                // Reset the paging control.
                MyCredits_bottomPageNavigationControl.Reset();
                // Reset to default datas in grid view
                LoadMyCreditsGrid(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyCredits_topErrorMessageLabel,
                    MyCredits_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of grid.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void MyCredits_bottomPageNavigationControl_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                LoadMyCreditsGrid(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyCredits_topErrorMessageLabel,
                    MyCredits_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sorting event is fired
        /// in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Sorting event will be fired when the user clicks on the row header.
        /// </remarks>
        protected void MyCredits_creditsGridView_Sorting(object sender,
            GridViewSortEventArgs e)
        {
            try
            {
                if (ViewState["SORT_FIELD"].ToString() == e.SortExpression)
                {
                    ViewState["SORT_ORDER"] =
                        ((SortType)ViewState["SORT_ORDER"]) == SortType.Ascending ?
                        SortType.Descending : SortType.Ascending;
                }
                else
                    ViewState["SORT_ORDER"] = SortType.Ascending;

                ViewState["SORT_FIELD"] = e.SortExpression;
                // Reset and show records for first page.
                MyCredits_bottomPageNavigationControl.Reset();
                LoadMyCreditsGrid(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyCredits_topErrorMessageLabel,
                    MyCredits_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void MyCredits_creditsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Set row styles.
                    e.Row.Attributes.Add("onmouseover", MOUSE_OVER_STYLE);
                    e.Row.Attributes.Add("onmouseout", MOUSE_OUT_STYLE);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyCredits_topErrorMessageLabel,
                    MyCredits_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row created event is 
        /// fired in the results grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row created event will be fired when the row is being created.
        /// </remarks>
        protected void MyCredits_creditsGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = GetSortColumnIndex
                        (MyCredits_creditsGridView,
                        (string)ViewState["SORT_FIELD"]);

                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row,
                            ((SortType)ViewState["SORT_ORDER"]));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(MyCredits_topErrorMessageLabel,
                    MyCredits_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Events Handlers                                             

        #region Private Methods                                                

        /// <summary>
        /// Represents the method that applies the default or selected item from dropdownlist and
        /// load the data into the grid.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadMyCreditsGrid(int pageNumber)
        {
            int totalRecords = 0;
            List<CreditUsageDetail> creditUsageHistory = new CreditBLManager().GetCreditsUsageHistory
                (base.userID,MyCredits_creditsTypeDropDownList.SelectedValue,pageNumber,base.GridPageSize,
                out totalRecords,ViewState["SORT_FIELD"].ToString(),((SortType)ViewState["SORT_ORDER"]));
            
            if (creditUsageHistory == null || creditUsageHistory.Count == 0)
            {
                MyCredits_creditsHistoryDiv.Visible = false;
                base.ShowMessage(MyCredits_topErrorMessageLabel,
                MyCredits_bottomErrorMessageLabel, Resources.HCMResource.Common_Empty_Grid);
            }
            else
                MyCredits_creditsHistoryDiv.Visible = true;

            MyCredits_creditsGridView.DataSource = creditUsageHistory;
            MyCredits_creditsGridView.DataBind();
            MyCredits_bottomPageNavigationControl.PageSize = base.GridPageSize;
            MyCredits_bottomPageNavigationControl.TotalRecords = totalRecords;
            MyCredits_creditsTypeDropDownList.Focus();
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            CreditsSummary creditSummary = new CreditBLManager().GetCreditSummary(base.userID);
            if (creditSummary != null)
            {
                MyCredits_userIdValueLabel.Text = creditSummary.UserID.ToString();
                MyCredits_availableCreditsValueLabel.Text = creditSummary.AvailableCredits.ToString();
                MyCredits_creditsEarnedValueLabel.Text = creditSummary.CreditsEarned.ToString();
                MyCredits_creditsRedeemedValueLabel.Text = creditSummary.CreditsRedeemed.ToString();
            }
            List<AttributeDetail> attributeDetail = new AttributeBLManager().GetCreditTypeAttributes();
            if (attributeDetail != null && attributeDetail.Count > 0)
            {
                MyCredits_creditsTypeDropDownList.DataSource = attributeDetail;
                MyCredits_creditsTypeDropDownList.DataTextField = "AttributeName";
                MyCredits_creditsTypeDropDownList.DataValueField = "AttributeID";
                MyCredits_creditsTypeDropDownList.DataBind();
                MyCredits_creditsTypeDropDownList.Items.Insert(0, new ListItem("Both", "0"));
                MyCredits_creditsTypeDropDownList.Items[0].Selected = true;
            }
            // Clear messages.
            MyCredits_topErrorMessageLabel.Text = string.Empty;
            MyCredits_bottomErrorMessageLabel.Text = string.Empty;

            // Reset default sort field and order keys.
            if (Utility.IsNullOrEmpty(ViewState["SORT_ORDER"]))
                ViewState["SORT_ORDER"] = SortType.Ascending;
            if (Utility.IsNullOrEmpty(ViewState["SORT_FIELD"]))
                ViewState["SORT_FIELD"] = "DATE";

            // Reset the paging control.
            MyCredits_bottomPageNavigationControl.Reset();

            // By default on load retrieves data for  page number 1.           
            LoadMyCreditsGrid(1);
        }

        #endregion Protected Overridden Methods                                
    }
}
