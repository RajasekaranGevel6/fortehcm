﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestConductionCompleted.aspx.cs
// File that represents the user interface layout and functionalities
// for the TestConductionCompleted page. This page helps in displaying the
// test conduction summary and provide various links such as sending
// feedbacks to admin, emailing recruiter etc.

#endregion Header 

#region Directives                                                             

using System;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the Rating page. This page helps to enter rating and feedback.
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class Rating : PageBase
    {
        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                // Set browser title.
                Master.SetPageCaption("Rating & Feedback");

                if (!IsPostBack)
                {
                    Rating_statusLabel.Text = "Please select your rating and enter feedback";

                    //if (Request.QueryString["status"] == null)
                    //    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Rating_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the submit button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will submit the feedback as a mail to the administrator.
        /// </remarks>
        protected void Rating_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["CANDIDATE_SESSIONKEY"] != null)
                {
                    CandidateRatingDetail ratingDetail = new CandidateRatingDetail();
                    ratingDetail.CandidateTestSessionID = Session["CANDIDATE_SESSIONKEY"].ToString();
                    ratingDetail.ModifiedBy = base.userID;
                    ratingDetail.Rating = ratingControl.CurrentRating;
                    ratingDetail.Feedback = Rating_FeedbackTextBox.Text;

                    new TestConductionBLManager().UpdateRating(ratingDetail);
                    //Rating_topSuccessMessageLabel.Text = string.Empty;
                    //base.ShowMessage(Rating_topSuccessMessageLabel, "Rating submitted successfully");

                    GotoNextPage();
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Rating_topErrorMessageLabel, exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the skip button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Rating_skipButton_Click(object sender, EventArgs e)
        {
            try
            {
                GotoNextPage();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Rating_topErrorMessageLabel,
                       Resources.HCMResource.Feedback_MailCouldNotSend);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        private void GotoNextPage()
        {
            TestSessionDetail testSession = null;
            if (Session["TEST_SESSION"] != null)
            {
                testSession = Session["TEST_SESSION"] as TestSessionDetail;
            }

            if (testSession == null)
            {
                Response.Redirect("~/Dashboard.aspx");
            }

            if (testSession != null && testSession.IsDisplayResultsToCandidate)
            {
                Response.Redirect("~/TestCenter/CandidateTestResult.aspx?testkey="
                    + testSession.TestID + "&parentpage=MY_TST&mode=SHARE&candidatesessionid=" + Session["CANDIDATE_SESSIONKEY"].ToString()
                    + "&attemptid=" + Session["ATTEMPT_ID"].ToString());
            }
            else
            {
                Response.Redirect("~/TestCenter/TestConductionCompleted.aspx?testkey="
                    + "?candidatesessionid=" + Session["CANDIDATE_SESSIONKEY"].ToString()
                    + "&attemptid=" + Session["ATTEMPT_ID"].ToString()
                    + "&status=" + Request.QueryString["status"]);
            }
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            Rating_topErrorMessageLabel.Text = string.Empty;
            Rating_topErrorMessageLabel.Visible = false;
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}