﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    Title="Test Introduction" CodeBehind="TestIntroduction.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.TestIntroduction" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="TestIntroduction_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script language="javascript" type="text/javascript">

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div>
                    <div class="msg_align">
                        <asp:Label ID="TestIntroduction_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestIntroduction_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div id="TestIntroduction_mainDiv" class="test_right_panel">
                    <div class="test_body_bg_div">
                        <div class="test_header_text_bold_div">
                            <asp:Literal ID="TestIntroduction_headerLiteral" runat="server" Text="Test Introduction">
                            </asp:Literal>
                        </div>
                    </div>
                    <div class="test_body_bg_div" style="height: 20px">
                    </div>
                    <div>
                        <div class="test_body_bg_div">
                            <div style="float: left; width: 820px" class="test_subheader_text_bold_div">
                                <asp:Label ID="TestIntroduction_testAreasLabel" runat="server" Text="Test Areas"></asp:Label>
                            </div>
                            <div style="float: left; width: 120px" class="test_subheader_text_bold_div">
                                <asp:Label ID="TestIntroduction_durationLabel" runat="server" Text="Duration"></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div">
                            <div style="float: left; width: 820px" class="test_text_div">
                                <asp:Label ID="TestIntroduction_testAreasValueLabel" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: left; width: 120px" class="test_text_div">
                                <asp:Label ID="TestIntroduction_durationValueLabel" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                         <div class="test_body_bg_div" style="height: 10px">
                        </div>
                         <div class="test_body_bg_div">
                            <div class="test_subheader_text_bold_div">
                                <asp:Label ID="TestIntroduction_testSubjectsLabel" runat="server" Text="Test Subjects"></asp:Label>
                            </div>
                            <div class="test_text_div">
                                <asp:Label ID="TestIntroduction_testSubjectsValueLabel" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div" style="height: 10px">
                        </div>
                        <div class="test_body_bg_div">
                            <div class="test_subheader_text_bold_div">
                                <asp:Label ID="TestIntroduction_testNameLabel" runat="server" Text="Test Name"></asp:Label>
                            </div>
                            <div class="test_text_div">
                                <asp:Label ID="TestIntroduction_testNameValueLabel" runat="server" ReadOnly="true"
                                    Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div" style="height: 10px">
                        </div>
                        <div class="test_body_bg_div">
                            <div class="test_subheader_text_bold_div">
                                <asp:Label ID="TestIntroduction_testDescriptionLabel" runat="server" Text="Test Description"></asp:Label></div>
                            <div class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                <asp:Label ID="TestIntroduction_testDescriptionValueLabel" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="test_body_bg_div" style="height: 10px">
                        </div>
                        <div class="test_body_bg_div">
                            <div>
                                <div class="test_subheader_text_bold_div">
                                    <asp:Label ID="TestIntroduction_testInstructionsLabel" runat="server" Text="Test Instructions"></asp:Label>
                                </div>
                                <div class="test_text_div" style="word-wrap: break-word;white-space:normal;">
                                    <asp:Literal ID="TestIntroduction_testInstructionsLiteral" runat="server" Text=""></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="test_body_bg_div">
                            <div style="width: 390px; height: 40px; float: left">
                            </div>
                            <div style="width: 380px; text-align: center; float: left;">
                                <div style="float: left; width: 80px; text-align: left">
                                    <asp:Button ID="TestIntroduction_startButton" runat="server" Text="Start Test" OnClick="TestIntroduction_startTestButton_Click"
                                        SkinID="sknButtonId" />
                                </div>
                                <div style="float: left; width: 70px; text-align: left">
                                    <asp:LinkButton ID="TestIntroduction_remindMeLinkButton" runat="server" Text="Remind Me"
                                        SkinID="sknActionLinkButton" OnClick="TestIntroduction_remindMeLinkButton_Click" />
                                </div>
                                <div style="float: left; width: 80px; text-align: left">
                                    <asp:LinkButton ID="TestIntroduction_cancelButton" runat="server" Text="Cancel" SkinID="sknActionLinkButton"
                                        OnClick="ParentPageRedirect"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
                <div>
                    <div class="msg_align">
                        <asp:Label ID="TestIntroduction_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestIntroduction_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
