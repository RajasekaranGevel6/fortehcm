﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="TestConductionCompleted.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.TestConductionCompleted" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="TestConductionCompleted_content" ContentPlaceHolderID="MainContent"
    runat="server">
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 980px; float: left; padding-left: 10px">
                    <div style="float: left; width: 970px">
                        <asp:Label ID="TestConductionCompleted_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                    <div style="float: left; width: 970px">
                        <div style="width: 100px; float: left">
                            <asp:Label runat="server" ID="TestConductionCompleted_statusLabel" SkinID="sknLabelTestCompletionTitle"
                                Text="Status"></asp:Label>
                        </div>
                        <div style="width: 800px; float: left">
                            <asp:Label runat="server" ID="TestConductionCompleted_statusMessageLabel" SkinID="sknLabelTestCompletion"
                                Text=""> </asp:Label>
                        </div>
                    </div>
                    <div style="float: left; width: 970px">
                        <div style="width: 140px; float: left" class="cand_test_detail_icon">
                        </div>
                        <div style="width: 700px; float: left; padding-top: 30px"  >
                            <div style="width: 790px; float: left" class="candidate_test_header">
                                <asp:Label ID="TestConductionCompleted_testNameLabel" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="width: 790px; float: left;word-wrap: break-word;white-space:normal;" class="candidate_label_text">
                                <asp:Label ID="TestConductionCompleted_testDescriptionLabel" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div style="float: left; width: 970px" class="test_conduction_completed_message_link_message">
                        Click here to send an
                        <asp:LinkButton ID="TestConductionCompleted_emailRecruiterLinkButton" runat="server"
                            Text="Email" CssClass="test_conduction_completed_message_link_btn" ToolTip="Click here to send an email to recruiter" 
                            OnClick="TestConductionCompleted_emailRecruiterLinkButton_Click"> </asp:LinkButton>
                        &nbsp;to the recruiter
                    </div>
                    <div style="float: left; width: 970px; height: 8px">
                    </div>
                    <div style="float: left; width: 970px" class="test_conduction_completed_message_link_message">
                        Click here to go to
                        <asp:LinkButton ID="TestConductionCompleted_feedbackLinkButton" runat="server" Text="Feedback"
                            CssClass="test_conduction_completed_message_link_btn" PostBackUrl="~/General/Feedback.aspx?parentpage=C_DASHBOARD"
                            ToolTip="Click here to send a feedback"> </asp:LinkButton>
                        &nbsp;page
                    </div>
                    <div style="float: left; width: 970px; height: 8px">
                    </div>
                    <div style="float: left; width: 970px" class="test_conduction_completed_message_link_message">
                        Click here to go to
                        <asp:LinkButton ID="TestConductionCompleted_homeLinkButton" runat="server" Text="Home"
                            CssClass="test_conduction_completed_message_link_btn" ToolTip="Click here to go to home page"
                            PostBackUrl="~/Dashboard.aspx?parentpage=C_DASHBOARD"> </asp:LinkButton>
                        &nbsp;page
                    </div>
                    <div style="float: left; width: 970px; height: 8px">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
