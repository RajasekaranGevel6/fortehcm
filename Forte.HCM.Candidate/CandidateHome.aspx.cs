﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateHome.aspx.cs
// This page allows the candidate to know the details of their tests.
// i.e. Pending, Completed, and Expired test details. Also, they can 
// send the request to the creator to retake a test, activate a tests.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using System.Collections.Generic;

#endregion Directives                                                          

namespace Forte.HCM.UI
{
    public partial class CandidateHome : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when the user role is candidate. Then,
        /// it will show the default values by calling 'LoadValues' method.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["USER_DETAIL"] == null)
                {
                    Response.Redirect("~/SignIn.aspx", false);
                }
                else
                {
                    Response.Redirect("~/Dashboard.aspx", false);
                }

                // Set page caption
                Master.SetPageCaption(Resources.HCMResource.CandidateHome_Title);

                // Show/hide menu based on user type (limited or not).
                Master.ShowMenu = !base.isLimited;

                CandidateHome_purchaseCreditsButton.Attributes.Add("OnClick",
                    "Javascript:return ShowCreditRequestFromHome();");

                if (!IsPostBack)
                {
                    // Load values.
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateHome_topErrorMessageLabel,
                    CandidateHome_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="DataListItemEventArgs"/> that contains the event data.
        /// </param>
        protected void CandidateHome_pendingTestsDataList_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                LinkButton testReminder =
                    e.Item.FindControl("CandidateHome_pendingTestsTestReminderLinkButton") as LinkButton;
                HiddenField candidateSessionIDField =
                    e.Item.FindControl("CandidateHome_pendingTestsCandidateSessionID") as HiddenField;
                HiddenField attemptIDField =
                    e.Item.FindControl("CandidateHome_pendingTestsAttemptID") as HiddenField;

                // Add attributes to test reminder link button.
                testReminder.Attributes.Add("OnClick", "javascript:return OpenTestReminderFromHome('TST','" +
                    candidateSessionIDField.Value + "','" + attemptIDField.Value + "')");

                HtmlTableCell cell = (HtmlTableCell)e.Item.FindControl("CandidateHome_pendingTestsTD");
                if (e.Item.ItemIndex % 2 == 0)
                    cell.Attributes.Add("Class", "cand_bg_dark");
                else
                    cell.Attributes.Add("Class", "cand_bg_light");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateHome_topErrorMessageLabel,
                    CandidateHome_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CandidateHome_pendingTestsDataList_ItemCommand
            (object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TestIntroduction")
                {
                    HiddenField candidateSessionIDField =
                        e.Item.FindControl("CandidateHome_pendingTestsCandidateSessionID") as HiddenField;
                    HiddenField attemptIDField =
                        e.Item.FindControl("CandidateHome_pendingTestsAttemptID") as HiddenField;

                    // Redirect to test introduction page.
                    Response.Redirect("~/TestCenter/TestIntroduction.aspx" +
                       "?candidatesessionid=" + candidateSessionIDField.Value +
                       "&attemptid=" + attemptIDField.Value +
                       "&parentpage=CAND_HOME", false);
                }
                else if (e.CommandName == "StartTest")
                {
                    HiddenField candidateSessionIDField =
                        e.Item.FindControl("CandidateHome_pendingTestsCandidateSessionID") as HiddenField;
                    HiddenField attemptIDField =
                        e.Item.FindControl("CandidateHome_pendingTestsAttemptID") as HiddenField;

                    // Assign values to the common hidden fields.
                    CandidateHome_selectedCandidateSessionIDHiddenField.Value = candidateSessionIDField.Value;
                    CandidateHome_selectedAttemptIDHiddenField.Value = attemptIDField.Value;

                    // Show confirmation message.
                    CandidateHome_confirmMsgControl.Message = "Are you ready to start the test?";
                    CandidateHome_confirmMsgControl.Title = "Start Test";
                    CandidateHome_confirmMsgControl.Type = MessageBoxType.YesNo;
                    CandidateHome_startTestPopupExtenderControl.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateHome_topErrorMessageLabel,
                    CandidateHome_topErrorMessageLabel, exp.Message);
            }
        }

        protected void CandidateHome_interestedDataList_ItemCommand(object sender, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName != "CreateSession")
                    return;
                CandidateHome_testPreviewTestNameLabel.Text = ((HiddenField)e.Item.FindControl("CandidateHome_interestedTestNameHiddenField")).Value;
                CandidateHome_testDescriptionDiv.InnerHtml = ((HiddenField)e.Item.FindControl("CandidateHome_interestedTestDescriptionHiddenField")).Value; ;
                CandidateHome_testPreviewTestIDLabel.Text = ((HiddenField)e.Item.FindControl("CandidateHome_interestedTestKeyHiddenField")).Value; ;
                CandidateHome_testPreviewTimeLimitLabel.Text = Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSeconds
                    (Convert.ToInt32(((HiddenField)e.Item.FindControl("CandidateHome_interestedTimeLimitHiddenField")).Value));
                CandidateHome_testPreviewTestCostLabel.Text = ((HiddenField)e.Item.FindControl("CandidateHome_interestedTestCostHiddenField")).Value; ;
                CandidateHome_testPreviewStartTestButton.Visible = false;
                CandidateHome_testPreviewCreateButton.Visible = true;
                CandidateHome_testPreviewModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateHome_topErrorMessageLabel,
                    CandidateHome_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the create test modal pop up.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void CandidateHome_testPreviewCreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateTestSession();
                LoadAdaptiveTests();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateHome_topErrorMessageLabel,
                    CandidateHome_topErrorMessageLabel, exp.Message);
            }
            finally
            {
                CandidateHome_interestedUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void CandidateHome_startTestButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                "?candidatesessionid=" + CandidateHome_selectedCandidateSessionIDHiddenField.Value +
                "&attemptid=" + CandidateHome_selectedAttemptIDHiddenField.Value +
                "&parentpage=CAND_HOME", false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CandidateHome_completedTestsDataList_ItemDataBound
            (object sender, DataListItemEventArgs e)
        {
            try
            {
                LinkButton retakeRequest =
                    e.Item.FindControl("CandidateHome_completedTestsRequestToRetakeLinkButton") as LinkButton;
                HiddenField CandidateHome_initiatedByHiddenField =
                    e.Item.FindControl("CandidateHome_initiatedByHiddenField") as HiddenField;

                string initiatedBy = string.Empty;

                if (!Utility.IsNullOrEmpty(CandidateHome_initiatedByHiddenField.Value) &&
                    CandidateHome_initiatedByHiddenField.Value.ToUpper() == "SELF")
                    retakeRequest.Visible = false;

                HiddenField candidateSessionIDField =
                    e.Item.FindControl("CandidateHome_completedTestsCandidateSessionID") as HiddenField;
                HiddenField attemptIDField =
                    e.Item.FindControl("CandidateHome_completedTestsAttemptID") as HiddenField;
                LinkButton CandidateHome_viewCertificateLinkbutton = (LinkButton)
                    e.Item.FindControl("CandidateHome_viewCertificateLinkbutton");
                HiddenField CandidateHome_testKeyHiddenField = (HiddenField)
                    e.Item.FindControl("CandidateHome_testKeyHiddenField");
                HiddenField CandidateHome_testCompletedOnHiddenField = (HiddenField)
                    e.Item.FindControl("CandidateHome_testCompletedOnHiddenField");

                CandidateHome_viewCertificateLinkbutton.Attributes.Add("onclick","return OpenViewCertificate('" 
                    + candidateSessionIDField.Value + "','" 
                    + attemptIDField.Value + "','" 
                    + CandidateHome_testKeyHiddenField.Value + "','" 
                    + CandidateHome_testCompletedOnHiddenField.Value + "','CERTIFICATE')");

                AjaxControlToolkit.ToolkitScriptManager tsm = (AjaxControlToolkit.ToolkitScriptManager)
                    Master.FindControl("HCMCandidateMaster_scriptManager");
                tsm.RegisterAsyncPostBackControl(retakeRequest);

                HtmlTableCell cell = (HtmlTableCell)e.Item.FindControl("CandidateHome_completedTestsTD");

                if (e.Item.ItemIndex % 2 == 0)
                    cell.Attributes.Add("Class", "cand_bg_dark");
                else
                    cell.Attributes.Add("Class", "cand_bg_light");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateHome_topErrorMessageLabel,
                    CandidateHome_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void CandidateHome_completedTestsDataList_ItemCommand
            (object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TestResults")
                {
                    HiddenField candidateSessionIDField =
                        e.Item.FindControl("CandidateHome_completedTestsCandidateSessionID") as HiddenField;
                    HiddenField attemptIDField =
                        e.Item.FindControl("CandidateHome_completedTestsAttemptID") as HiddenField;
                    HiddenField testIdField =
                        e.Item.FindControl("CandidateHome_completedTestID") as HiddenField;

                    // Redirect to test results page.
                    Response.Redirect("~/TestCenter/CandidateTestResult.aspx" +
                        "?candidatesessionid=" + candidateSessionIDField.Value +
                        "&attemptid=" + attemptIDField.Value +
                        "&parentpage=CAND_HOME" +
                        "&mode=SHARE" + 
                        "&testkey=" + testIdField.Value, false);
                }

                else if (e.CommandName == "RequestToRetake")
                {
                    string candidateSessionID = ((HiddenField)((LinkButton)e.CommandSource).
                        FindControl("CandidateHome_completedTestsCandidateSessionID")).Value;

                    string testID = ((HiddenField)((LinkButton)e.CommandSource).
                        FindControl("CandidateHome_completedTestID")).Value;

                    int attemptID = int.Parse(((HiddenField)((LinkButton)e.CommandSource).
                         FindControl("CandidateHome_completedTestsAttemptID")).Value);

                    if (IsValidRetake(candidateSessionID, testID, attemptID))
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "RetakeTest",
                         "javascript: OpenCandidateRequestFromHome('"
                         + candidateSessionID + "','"
                         + attemptID + "','retake')", true);
                    else
                        CandidateHome_RetakeValidation_ModalPopupExtender.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateHome_topErrorMessageLabel,
                    CandidateHome_topErrorMessageLabel, exp.Message);
            }
            finally
            {
                CandidateHome_confirmMsgUpdatePanel.Update();
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void CandidateHome_startTestForSessionButton_CancelClick(object sender, EventArgs e)
        {
            try
            {
                // Reshow the window with the default values already set.
                CandidateHome_testPreviewTopSuccessMessageLabel.Text = "Session created successfully";
                CandidateHome_testPreviewModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateHome_topErrorMessageLabel,
                    CandidateHome_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void CandidateHome_startTestForSessionButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                "?candidatesessionid=" + CandidateHome_testCandidateSessionIdHiddenField.Value +
                "&attemptid=1&parentpage=CS_TST", false);
        }

        /// <summary>
        /// Method that retrieves the show results to candidate status. This
        /// helps to show or hide the show results link icon in the completed 
        /// test grid section.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the show results to candidate status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of show results
        /// to candidate status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for show results to candidates are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsShowResults(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that validate requests to retake a test. 
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session id.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt id.
        /// </param>
        private bool IsValidRetake(string candidateSessionID, string testKey, int attemptID)
        {
            string retakeCount = string.Empty;
            Dictionary<string, string> dicRetakeDetails = new
                Dictionary<string, string>();
            string isCertificateTest = string.Empty;
            bool isValidRetake = true;
            string pendingAttempt = new CandidateBLManager().TestRetakeValidation(candidateSessionID, testKey,
                attemptID, out retakeCount, out isCertificateTest, out dicRetakeDetails);

            if (pendingAttempt != null && Convert.ToInt32(pendingAttempt) > 0)
            {
                CandidateHome_RetakeValidation_ConfirmMsgControl.Message =
                    "Already an attempt is pending for this test session.<br>Check with your pending tests";
                return isValidRetake = false;
            }
            if (Convert.ToInt32(isCertificateTest) > 0)
            {
                if (retakeCount != null && Convert.ToInt32(retakeCount) <= 0)
                {
                    CandidateHome_RetakeValidation_ConfirmMsgControl.Message =
                        "You have utilized maximum number of attempts.<br>You are not allowed to retake this test";
                    return isValidRetake = false;
                }

                if (dicRetakeDetails != null && dicRetakeDetails.Count > 0)
                {
                    foreach (KeyValuePair<string, string> retakeDetail in dicRetakeDetails)//Temporary-will be removed//
                    {
                        if (Convert.ToInt32(retakeDetail.Key) > 0)
                        {
                            CandidateHome_RetakeValidation_ConfirmMsgControl.Message =
                                "You are allowed to retake your test only after " + retakeDetail.Key + " days from "
                                + retakeDetail.Value;
                            return isValidRetake = false;
                        }
                    }
                }
            }

            return isValidRetake;
        }

        /// <summary>
        /// Loads the adaptive recommendations for the 'you may interested' tab.
        /// </summary>
        private void LoadAdaptiveTests()
        {
            int TotalNoOfRecords = 0;
            List<TestDetail> testDetail = null;
            try
            {
                try
                {
                    if (testDetail == null)
                        testDetail = new List<TestDetail>();
                    testDetail.Add(new TestBLManager().GetAdaptiveTestBySimilarProfile(base.userID, 1, 1, 'A', "TESTID",
                        out TotalNoOfRecords)[0]);
                }
                catch (NullReferenceException) { }
                catch (IndexOutOfRangeException) { }
                try
                {
                    if (testDetail == null)
                        testDetail = new List<TestDetail>();
                    testDetail.Add(new TestBLManager().GetAdaptiveTestByAssessmentHistory(base.userID, 1, 1, 'A', "TESTID",
                    out TotalNoOfRecords)[0]);
                }
                catch (NullReferenceException) { }
                catch (IndexOutOfRangeException) { }
                try
                {
                    if (testDetail == null)
                        testDetail = new List<TestDetail>();
                    testDetail.Add(new TestBLManager().GetAdaptiveTestBySimilarCandidate(base.userID, 1, 1, 'A', "TESTID",
                    out TotalNoOfRecords)[0]);
                }
                catch (NullReferenceException) { }
                catch (IndexOutOfRangeException) { }
                CandidateHome_interestedDataList.DataSource = testDetail;
                CandidateHome_interestedDataList.DataBind();
            }
            catch (Exception exp)
            {
                Response.Write(exp.Message);
            }
            finally
            {
                if (testDetail != null) testDetail = null;
            }
        }

        /// <summary>
        /// This method creates the session for the test.
        /// </summary>
        private void CreateTestSession()
        {
            TestScheduleDetail testSchedule = null;
            try
            {
                testSchedule = new TestScheduleDetail();
                testSchedule.CandidateID = base.userID.ToString();
                //  testSchedule.CandidateTestSessionID = candidateSessionID.ToString();
                testSchedule.AttemptID = 1;
                testSchedule.EmailId = ((UserDetail)Session["USER_DETAIL"]).Email;
                testSchedule.ExpiryDate = DateTime.Now.AddDays(base.candidateExpiryDays);
                //   new TestSchedulerBLManager().ScheduleCandidate(testSchedule, base.userID);
                //string testSessionID = null;
                string candidateSessionID = null;
                //// Call save test session BL method.
                new TestBLManager().SaveTestSessionScheduleCandidate(GetTestSessionDetails(), testSchedule,
                    base.userID, out candidateSessionID);
                CandidateHome_testPreviewStartTestButton.Visible = true;
                CandidateHome_testPreviewCreateButton.Visible = false;
                CandidateHome_testPreviewTopSuccessMessageLabel.Text = "Session created successfully";
                CandidateHome_testCandidateSessionIdHiddenField.Value = candidateSessionID;
                CandidateHome_testPreviewModalPopupExtender.Show();
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSchedule)) testSchedule = null;
            }
        }

        /// <summary>
        /// This method loads the test details in to test session details 
        /// data object.
        /// </summary>
        /// <returns>Test session detail data object that contains the test details.</returns>
        private TestSessionDetail GetTestSessionDetails()
        {
            // Initialize test session detail object
            TestSessionDetail testSessionDetail = null;
            try
            {
                testSessionDetail = new TestSessionDetail();
                // Set test name
                testSessionDetail.TestID = CandidateHome_testPreviewTestIDLabel.Text;
                testSessionDetail.TestName = CandidateHome_testPreviewTestNameLabel.Text;
                // Set number of candidate session (session count)
                testSessionDetail.NumberOfCandidateSessions = 1;
                // Set total credits limit
                testSessionDetail.TotalCredit =
                    Convert.ToDecimal(CandidateHome_testPreviewTestCostLabel.Text);
                // Set time limit
                testSessionDetail.TimeLimit = Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(
                    CandidateHome_testPreviewTimeLimitLabel.Text);
                testSessionDetail.ClientRequestID = "0";
                // Set expiry date
                testSessionDetail.ExpiryDate = DateTime.Now;
                // Set random question order status
                testSessionDetail.IsRandomizeQuestionsOrdering = false;
                // Set display result status 
                testSessionDetail.IsDisplayResultsToCandidate = true;
                // Set cyber proctoring status
                testSessionDetail.IsCyberProctoringEnabled = false;
                // Set created by
                testSessionDetail.CreatedBy = base.userID;
                // Set modified by
                testSessionDetail.ModifiedBy = base.userID;
                // Set test instructions
                testSessionDetail.Instructions = Resources.HCMResource.CandidateSearchTest_Instructions.ToString();
                // Set session descriptions
                testSessionDetail.TestSessionDesc = Resources.HCMResource.CandidateSearchTest_TestSessionDesc.ToString();
                return testSessionDetail;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSessionDetail)) testSessionDetail = null;
            }
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            CandidateHome_startTestForSessionPopupExtenderControl.Message = "Are you ready to start the test ?";
            CandidateHome_startTestForSessionPopupExtenderControl.Title = "Start Test";
            CandidateHome_startTestForSessionPopupExtenderControl.Type = MessageBoxType.YesNo;
            CandidateSummary summary = new CandidateBLManager().
                GetCandidateSummary(base.userID);

            CandidateHome_fullNameValueLabel.Text = summary.CandidateName;

            if (summary.UserSince != DateTime.MinValue)
                CandidateHome_userSinceValueLabel.Text = summary.UserSince.ToString("MMM dd yyyy");

            CandidateHome_lastTestTakenValueLabel.Text = summary.LastTestTaken;

            if (summary.LastTestTakenDate != DateTime.MinValue)
                CandidateHome_lastTestTakenDateValueLabel.Text = summary.LastTestTakenDate.ToString("MMM dd yyyy");

            // Test summary.
            CandidateHome_pendingTestsValueLabel.Text = summary.PendingTestsCount.ToString();
            CandidateHome_completedTestsValueLabel.Text = summary.CompletedTestsCount.ToString();
            CandidateHome_expiredTestsValueLabel.Text = summary.ExpiredTestsCount.ToString();

            // Interview summary.
            CandidateHome_pendingInterviewsValueLabel.Text = summary.PendingInterviewsCount.ToString();
            CandidateHome_completedInterviewsValueLabel.Text = summary.CompletedInterviewsCount.ToString();
            CandidateHome_expiredInterviewsValueLabel.Text = summary.ExpiredInterviewsCount.ToString();

            // Assign pending tests.
            CandidateHome_pendingTestsDataList.DataSource = summary.PendingTest;
            CandidateHome_pendingTestsDataList.DataBind();
            // Assign completed tests.
            CandidateHome_completedTestsDataList.DataSource = summary.CompletedTest;
            CandidateHome_completedTestsDataList.DataBind();
            // Assign Interested Test
            LoadAdaptiveTests();
        }

        #endregion Protected Overridden Methods                                
    }
}