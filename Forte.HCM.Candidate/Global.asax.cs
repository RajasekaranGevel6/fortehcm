﻿using System;
using System.Web;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;

namespace Forte.HCM.UI
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            try
            {
                // Keep page size in session.
                Application[Constants.SessionConstants.GRID_PAGE_SIZE] = new
                    AdminBLManager().GetAdminSettings().GridPageSize;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                Session["UNHANDLED_ERROR"] = exp.Message;
                Response.Redirect("~/Common/CandidateUnhandledError.aspx", true);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            Uri ReqUrl = HttpContext.Current.Request.Url;
            string[] ReqSegment = ReqUrl.Segments;
            string RequestPage = ReqSegment[ReqSegment.Length - 1].ToLower();

            if (RequestPage == "captchaviewer.aspx" ||
                RequestPage == "forgotpassword.aspx" ||
                RequestPage == "signup.aspx" ||
                RequestPage == "candidatehelp.aspx" ||
                RequestPage == "offerings.aspx" ||
                RequestPage == "selfadmindemo.aspx" ||
                RequestPage == "candidatetestresult.aspx" ||
                RequestPage == "candidatetestresultprint.aspx" ||
                RequestPage == "candidatetestresulttranscript.aspx" ||
                RequestPage == "imagehandler.ashx" ||
                RequestPage == "candidatetestdetails.aspx" ||
                RequestPage == "candidatetestdetailsopentext.aspx" ||
                RequestPage == "captchaimagehandler.ashx" ||
                RequestPage == "candidateaccessdenied.aspx" ||
                RequestPage == "candidateunhandlederror.aspx" ||
                RequestPage == "candidatepageunderconstruction.aspx" ||
                RequestPage == "popupaccessdenied.aspx" ||
                RequestPage == "popupunhandlederror.aspx" ||
                RequestPage == "popupunderconstruction.aspx" ||
                RequestPage == "feedback.aspx" ||
                RequestPage == "aboutus.aspx" ||
                RequestPage == "overview.aspx" ||
                RequestPage == "agreement.aspx" ||
                RequestPage == "signin.aspx" || 
                RequestPage == "help.aspx")
            {
                HttpContext.Current.SkipAuthorization = true;
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception currentException = Server.GetLastError().GetBaseException();

            Logger.ExceptionLog(currentException);

            if (currentException.Message.ToUpper().Contains(".ASPX' DOES NOT EXIST."))
            {
                if (currentException.Message.ToUpper().Contains("/POPUP/"))
                    Response.Redirect("~/Popup/PopupUnderConstruction.aspx", true);
                else
                    Response.Redirect("~/Common/CandidatePageUnderConstruction.aspx", true);
            }
            else if (currentException.StackTrace != null &&
                currentException.StackTrace.ToUpper().Contains("FORTE.HCM.UI.POPUP"))
            {
                Session["UNHANDLED_ERROR"] = currentException.Message;
                Response.Redirect("~/Popup/PopupUnhandledError.aspx", true);
            }
            else
            {
                Response.Redirect("~/Common/CandidateUnhandledError.aspx", true);
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
