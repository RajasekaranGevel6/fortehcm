﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyResume.aspx.cs" MasterPageFile="~/SiteMaster.Master"
    Inherits="Forte.HCM.Candidate.ResumeRepository.MyResume" Title="My Resume" %>

<%@ Register Src="~/CommonControls/NameControl.ascx" TagName="NameControl" TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ContactInformationControl.ascx" TagName="ContactInformationControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ExecutiveSummaryControl.ascx" TagName="ExecutiveSummaryControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/TechnicalSkillsControl.ascx" TagName="TechnicalSkillsControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/ProjectsControl.ascx" TagName="ProjectsControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/EducationControl.ascx" TagName="EducationControl"
    TagPrefix="uc6" %>
<%@ Register Src="~/CommonControls/ReferencesControl.ascx" TagName="ReferencesControl"
    TagPrefix="uc7" %>
<%@ Register Src="~/CommonControls/RolesVectorControl.ascx" TagName="RolesVectorControl"
    TagPrefix="uc8" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="MyResume_contentID" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" language="javascript">
        function OnResumeMenuClick(menuType) {
            // Hide all div.
            document.getElementById('<%=MyResume_selectedMenuHiddenField.ClientID %>').value = menuType;
            document.getElementById('<%=MyResume_contactInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=MyResume_executiveInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=MyResume_technicalInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=MyResume_projectsInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=MyResume_educationInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=MyResume_referencesInfoDiv.ClientID %>').style.display = 'none';
            document.getElementById('<%=MyResume_competencyVectoInfoDiv.ClientID %>').style.display = 'none';

            // Reset color.
            document.getElementById('<%=MyResume_contactInfoLinkButton.ClientID %>').className = "my_resume_link_button";
            document.getElementById('<%=MyResume_executiveInfoLinkButton.ClientID %>').className = "my_resume_link_button";
            document.getElementById('<%=MyResume_technicalInfoLinkButton.ClientID %>').className = "my_resume_link_button";
            document.getElementById('<%=MyResume_projectsInfoLinkButton.ClientID %>').className = "my_resume_link_button";
            document.getElementById('<%=MyResume_educationInfoLinkButton.ClientID %>').className = "my_resume_link_button";
            document.getElementById('<%=MyResume_referencesInfoLinkButton.ClientID %>').className = "my_resume_link_button";
            document.getElementById('<%=MyResume_competencyVectorLinkButton.ClientID %>').className = "my_resume_link_button";

            if (menuType == 'CI') {
                document.getElementById('<%=MyResume_contactInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=MyResume_contactInfoLinkButton.ClientID %>').className = "my_resume_link_button_selected";
            }
            else if (menuType == 'SI') {
                document.getElementById('<%=MyResume_executiveInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=MyResume_executiveInfoLinkButton.ClientID %>').className = "my_resume_link_button_selected";
            }
            else if (menuType == 'TI') {
                document.getElementById('<%=MyResume_technicalInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=MyResume_technicalInfoLinkButton.ClientID %>').className = "my_resume_link_button_selected";
            }
            else if (menuType == 'PI') {
                document.getElementById('<%=MyResume_projectsInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=MyResume_projectsInfoLinkButton.ClientID %>').className = "my_resume_link_button_selected";
            }
            else if (menuType == 'EI') {
                document.getElementById('<%=MyResume_educationInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=MyResume_educationInfoLinkButton.ClientID %>').className = "my_resume_link_button_selected";
            }
            else if (menuType == 'RI') {
                document.getElementById('<%=MyResume_referencesInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=MyResume_referencesInfoLinkButton.ClientID %>').className = "my_resume_link_button_selected";
            }
            else if (menuType == 'CV') {
                document.getElementById('<%=MyResume_competencyVectoInfoDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%=MyResume_competencyVectorLinkButton.ClientID %>').className = "my_resume_link_button_selected";
            }
        }
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <!-- Save,Approve,Reset and Cancel Buttion-->
                <asp:UpdatePanel ID="MyResume_saveUpdatePanel" runat="server">
                    <ContentTemplate>
                        <div class="resume_header_bg_div">
                            <div class="resume_header_bg_left">
                                <div id="MyResume_Title" class="resume_header_text_bold_div">
                                    <asp:Literal ID="MyResume_headerLiteral" runat="server" Text="My Resume"></asp:Literal>
                                </div>
                            </div>
                            <div class="resume_header_bg_right">
                                <asp:Button ID="MyResume_topSaveButton" runat="server" CommandName="cmdSave" SkinID="sknButtonId"
                                    Text="Save" OnClick="MyResume_topSaveButton_Click" />&nbsp;
                                <asp:Button ID="MyResume_topApproveButton" runat="server" SkinID="sknButtonId" Text="Approve"
                                    OnClick="MyResume_topApproveButton_Click" />&nbsp;
                                <asp:LinkButton ID="MyResume_topResetButton" runat="server" Text="Reset" SkinID="sknActionLinkButton"
                                    OnClick="MyResume_topResetButton_Click"></asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="MyResume_topCancelButton" runat="server" Text="Cancel" SkinID="sknActionLinkButton"
                                    OnClick="ParentPageRedirect"></asp:LinkButton>&nbsp;
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- End buttons-->
                <!-- Success & error sessage -->
                <div id="MyResume_messagesDiv" class="msg_align">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="MyResume_topErrorMessageLabel" SkinID="sknErrorMessage" runat="server"></asp:Label>
                            <asp:Label ID="MyResume_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <!-- Hidden fields -->
                    <asp:HiddenField runat="server" ID="MyResume_candidateIDHiddenField"></asp:HiddenField>
                    <asp:HiddenField runat="server" ID="MyResume_candidateFirstNameHiddenField" />
                    <asp:HiddenField runat="server" ID="MyResume_candidateNameHiddenField"></asp:HiddenField>
                    <asp:HiddenField runat="server" ID="MyResume_isLoadClickHiddenField"></asp:HiddenField>
                    <asp:HiddenField runat="server" ID="MyResume_candidateResumeFileNameHiddenField">
                    </asp:HiddenField>
                    <asp:HiddenField runat="server" ID="MyResume_candidateResumeFileMimeTypeHiddenField">
                    </asp:HiddenField>
                    <!-- End hidden fields -->
                </div>
                <!-- End error message-->
                <div class="resume_header_bg_div">
                    <div class="resume_header_bg_left">
                        <!--Candidate Name-->
                        <div id="MyResume_nameDiv" class="resume_header_text">
                            <asp:Label ID="MyResume_nameTextBox" runat="server"></asp:Label>
                        </div>
                        <!-- End Candidate Name-->
                    </div>
                    <div runat="server" class="resume_header_bg_right">
                        <asp:ImageButton ID="MyResume_downloadImageButton" runat="server" Text="Download"
                            SkinID="sknDownloadImageButton" OnClick="MyResume_downloadImageButtonClick" ToolTip="Download Resume"
                            AlternateText="Download" />
                        <asp:HiddenField runat="server" ID="MyResume_candidateResumeIDHiddenField" />
                    </div>
                </div>
                <div runat="server" id="MyResume_perviewHtmlDiv" style="overflow: auto; width: 950px;
                    height: 100px" class="resume_ListViewNormalStyle">
                </div>
                <div>
                    &nbsp;
                </div>
                <!-- Resume menu links -->
                <div id="MyResume_Panel_Links">
                    <div id="MyResume_Links">
                        <asp:UpdatePanel ID="MyResume_menuUpdatePanel" runat="server">
                            <ContentTemplate>
                                <div id="MyResume_contactInfoLinkButtionDiv" class="resume_float_left">
                                    <asp:HiddenField ID="MyResume_selectedMenuHiddenField" runat="server" Value="CI" />
                                    <asp:LinkButton ID="MyResume_contactInfoLinkButton" runat="server" Text="Contact Information"
                                        ToolTip="Contact Information" CssClass="my_resume_link_button_selected" OnClientClick="javascript: return OnResumeMenuClick('CI')"></asp:LinkButton>
                                </div>
                                <div id="MyResume_executiveSummaryLinkButtionDiv" class="resume_float_left">
                                    <asp:LinkButton ID="MyResume_executiveInfoLinkButton" runat="server" Text="Executive Summary"
                                        ToolTip="Executive Summary" CssClass="my_resume_link_button" OnClientClick="javascript: return OnResumeMenuClick('SI')">
                                    </asp:LinkButton>
                                </div>
                                <div id="MyResume_technicalSkillsLinkButtionDiv" class="resume_float_left">
                                    <asp:LinkButton ID="MyResume_technicalInfoLinkButton" runat="server" ToolTip="Technical Skills"
                                        Text="Technical Skills" CssClass="my_resume_link_button" OnClientClick="javascript: return OnResumeMenuClick('TI')">
                                    </asp:LinkButton>
                                </div>
                                <div id="MyResume_projectsInfoLinkButtonDiv" class="resume_float_left">
                                    <asp:LinkButton ID="MyResume_projectsInfoLinkButton" runat="server" ToolTip="Projects"
                                        Text="Projects" CssClass="my_resume_link_button" OnClientClick="javascript: return OnResumeMenuClick('PI')">
                                    </asp:LinkButton>
                                </div>
                                <div id="MyResume_educationInfoLinkButtonDiv" class="resume_float_left">
                                    <asp:LinkButton ID="MyResume_educationInfoLinkButton" runat="server" ToolTip="Education"
                                        Text="Education" CssClass="my_resume_link_button" OnClientClick="javascript: return OnResumeMenuClick('EI')">
                                    </asp:LinkButton>
                                </div>
                                <div id="MyResume_referencesInfoLinkButtonDiv" class="resume_float_left">
                                    <asp:LinkButton ID="MyResume_referencesInfoLinkButton" runat="server" ToolTip="References"
                                        Text="References" CssClass="my_resume_link_button" OnClientClick="javascript: return OnResumeMenuClick('RI')">
                                    </asp:LinkButton>
                                </div>
                                <div id="MyResume_competencyVectorLinkButtonDiv" class="resume_float_left">
                                    <asp:LinkButton ID="MyResume_competencyVectorLinkButton" runat="server" ToolTip="Competency Vector"
                                        Text="Competency Vector" CssClass="my_resume_link_button" OnClientClick="javascript: return OnResumeMenuClick('CV')">
                                    </asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div id="MyResume_Link_Source">
                        <asp:UpdatePanel ID="MyResume_menuControlsUpdatePanel" runat="server">
                            <ContentTemplate>
                                <div>
                                    <div class="empty_height">
                                        &nbsp;</div>
                                    <div id="MyResume_contactInfoDiv" runat="server" style="display: block;">
                                        <uc2:ContactInformationControl ID="MainResumeControl_contactInfoControl" runat="server" />
                                    </div>
                                    <div id="MyResume_executiveInfoDiv" runat="server" style="display: block">
                                        <uc3:ExecutiveSummaryControl ID="MainResumeControl_executiveSummaryControl" runat="server" />
                                    </div>
                                    <div id="MyResume_technicalInfoDiv" runat="server" style="display: block">
                                        <uc4:TechnicalSkillsControl ID="MainResumeControl_technicalSkillsControl" runat="server" />
                                    </div>
                                    <div id="MyResume_projectsInfoDiv" runat="server" style="overflow: auto; height: 300px;">
                                        <uc5:ProjectsControl ID="MainResumeControl_projectsControl" runat="server" />
                                    </div>
                                    <div id="MyResume_educationInfoDiv" runat="server" style="display: block">
                                        <uc6:EducationControl ID="MainResumeControl_educationControl" runat="server" />
                                    </div>
                                    <div class="empty_height">
                                        &nbsp;</div>
                                    <div id="MyResume_referencesInfoDiv" runat="server" style="display: block">
                                        <uc7:ReferencesControl ID="MainResumeControl_referencesControl" runat="server" />
                                    </div>
                                    <div id="MyResume_competencyVectoInfoDiv" runat="server" style="display: block">
                                        <uc8:RolesVectorControl ID="MyResume_rolesVectorControl" runat="server" />
                                    </div>
                                    <div class="empty_height">
                                        &nbsp;</div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- Resume menu ends here -->
            </div>
        </div>
    </div>
</asp:Content>
