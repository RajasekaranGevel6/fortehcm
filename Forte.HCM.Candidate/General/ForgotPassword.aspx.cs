﻿#region Header                                                                 

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ForgotPassword.aspx.cs
// Class that represents the user interface layout and functionalities for
// the ForgotPassword page. This page helps in mailing the password to the
// candidate email by entering their user name. 

#endregion Header  

#region Directives                                                             

using System;
using System.Text;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.Candidate
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the ForgotPassword page. This page helps in mailing the password to the
    /// candidate email by entering their user name. This class inherits the
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class ForgotPassword : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try 
            {
                // Set browser title.
                Master.SetPageCaption("Forgot Password");

                // Clear message and hide labels.
                ClearMessages();
                
                // Set default button to 'go' button.
                Page.Form.DefaultButton = ForgotPassword_goImageButton.UniqueID;
            
                if (!IsPostBack)
                {
                    // Set focus to the user name text box.
                    ForgotPassword_userNameTextBox.Focus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ForgotPassword_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the go button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will valid the user name and send to the password to the 
        /// email.
        /// </remarks>
        protected void ForgotPassword_goImageButton_Click(object sender, EventArgs e)
        {
            try 
            {
                if (Utility.IsNullOrEmpty(ForgotPassword_userNameTextBox.Text))
                {
                    base.ShowMessage(ForgotPassword_errorMessageLabel,
                        "Please enter your user name !");
                    ForgotPassword_userNameTextBox.Focus();
                    return;
                }

                // Get user detail along with password.
                UserDetail userDetail = new UserDetail();
                    userDetail = new UserRegistrationBLManager().GetPassword
                        (ForgotPassword_userNameTextBox.Text.Trim());

                if (userDetail == null)
                {
                    base.ShowMessage(ForgotPassword_errorMessageLabel,
                        "No such user name exist !");
                    ForgotPassword_userNameTextBox.Focus();
                    return;
                }
              
                // Decrypt the password.
                userDetail.Password = new EncryptAndDecrypt().DecryptString(userDetail.Password);
                
                // Send the password through mail.
                new EmailHandler().SendMail(ForgotPassword_userNameTextBox.Text.Trim(), 
                    "ForteHCM: Your account password", GetMailBody(userDetail));

                // Show a message to the user.
                base.ShowMessage(ForgotPassword_successMessageLabel,
                    "Your password has been sent your email");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ForgotPassword_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the sign in button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will navigates to the sign in page.
        /// </remarks>
        protected void ForgotPassword_signInButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Signin.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ForgotPassword_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that compose and returns the mail message.
        /// </summary>
        /// <param name="userDetail">
        /// A <see cref="UserDetail"/> that holds the user user detail.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the mail message.
        /// </returns>
        private string GetMailBody(UserDetail userDetail)
        {
            StringBuilder mailBody = new StringBuilder();

            mailBody.Append("Dear " + userDetail.FirstName + " " + userDetail.LastName);
            mailBody.Append(",");
            mailBody.Append(Environment.NewLine);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("This mail is sent to you for the forgot password request");
            mailBody.Append(Environment.NewLine);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Your user: " + userDetail.UserName);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Your password: " + userDetail.Password);
            mailBody.Append(Environment.NewLine);
            mailBody.Append(Environment.NewLine);
            mailBody.Append(Environment.NewLine);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("This is a system generated message");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Do not reply to this message");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("============================== Disclaimer ==============================");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("otherwise private information. If you have received it in error, please notify the sender");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("=========================== End Of Disclaimer ============================");
            return mailBody.ToString();
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            ForgotPassword_errorMessageLabel.Text = string.Empty;
            ForgotPassword_successMessageLabel.Text = string.Empty;

            ForgotPassword_errorMessageLabel.Visible = false;
            ForgotPassword_successMessageLabel.Visible = false;
        }

        #endregion Private Methods
    }
}