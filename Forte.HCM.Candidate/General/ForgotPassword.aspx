﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="ForgotPassword.aspx.cs" Inherits="Forte.HCM.Candidate.ForgotPassword" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="ForgotPassword_headContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="ForgotPassword_mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div class="signin_outer_div">
                    <div class="signin_left_bg">
                        &nbsp;
                    </div>
                    <div class="signin_middle_bg">
                        <div>
                            <div style="text-align: left; padding-top: 6px" >
                                <asp:Label ID="ForgotPassword_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </div>
                            <div style="text-align: left; padding-top: 6px">
                                <asp:Label ID="ForgotPassword_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            </div>
                            <div class="signin_left_panel">
                                <div class="signin_signup_ex_user_line_bottom">
                                    <asp:Label runat="server" ID="ForgotPassword_titleLabel" CssClass="signin_ex_user_head"
                                        Text="Forgot Password"> </asp:Label>
                                </div>
                                <div class="signin_form_container">
                                    <div class="signin_form_container_left">
                                        <asp:Label runat="server" ID="ForgotPassword_userNameLabel" CssClass="signin_ex_user">User Name<span>*</span></asp:Label>
                                    </div>
                                    <div class="signin_form_container_right">
                                        <asp:TextBox ID="ForgotPassword_userNameTextBox" SkinID="sknCanTextBox" runat="server"
                                            ToolTip="Enter user name" onFocus="javascript:this.select()"></asp:TextBox>
                                    </div>
                                    <div class="signin_form_container_right_cust">
                                        <asp:ImageButton ID="ForgotPassword_goImageButton" runat="server" SkinID="sknLoginGoImageButton"
                                            ToolTip="Click here to send password to your email" OnClick="ForgotPassword_goImageButton_Click" />
                                    </div>
                                </div>
                                <div class="form_container">
                                    <div class="form_container_left">
                                    </div>
                                </div>
                            </div>
                            <div class="signin_divider">
                                &nbsp;
                            </div>
                            <div id="title" class="signin_right_panel">
                                <div class="signin_signup_ex_user_line_bottom">
                                    <asp:Label runat="server" ID="ForgotPassword_knowYourPasswordLabel" CssClass="signin_openuser_head">Know your password? Sign in now!</asp:Label>
                                </div>
                                <div>
                                    <div class="form_container">
                                        <div class="signin_container_left_cust_w">
                                            <div style="display: block">
                                                <asp:Button ID="ForgotPassword_signInButton" runat="server" Text="Sign In" CssClass="upload_btn_bg"
                                                    ToolTip="Click here to sign in" OnClick="ForgotPassword_signInButton_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="signin_right_bg">
                        &nbsp;
                    </div>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</asp:Content>
