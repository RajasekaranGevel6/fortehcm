﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="CandidateHelp.aspx.cs" Inherits="Forte.HCM.UI.General.CandidateHelp" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="CandidateHelp_bodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 980px">
                    <div style="width: 978px; float: left">
                        <div style="width: 880px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateHelp_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        </div>
                        <div style="width: 880px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateHelp_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 978px; float: left">
                        <div style="width: 940px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateHelp_titleLabel" runat="server" Text="Help" CssClass="candidate_help_title"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 978px; float: left; height: 20px">
                    </div>
                    <div style="width: 978px; float: left">
                        <div style="width: 956px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:LinkButton ID="CandidateHelp_candidateHomeTitleLabel" runat="server" Text="Candidate Home"
                                ToolTip="Click here to navigate to home page" CssClass="candidate_help_topic_link_button"
                                PostBackUrl="~/Dashboard.aspx"></asp:LinkButton>
                        </div>
                        <div style="width: 956px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateHelp_candidateHomeContentsLabel" runat="server" Text="Acts as a dashboard for candidates. It will list the candidate information along with recent activities. The list of tests created, completed tests, pending tests & pending interviews can be viewed here and various links provided to perform related operations"
                                CssClass="candidate_help_contents"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 978px; float: left; height: 20px">
                    </div>
                    <div style="width: 978px; float: left">
                        <div style="width: 956px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:LinkButton ID="CandidateHelp_searchTestTitleLabel" runat="server" Text="Search Test"
                                ToolTip="Click here to navigate to search test page" CssClass="candidate_help_topic_link_button"
                                PostBackUrl="~/CandidateAdmin/CandidateSearchTest.aspx"></asp:LinkButton>
                        </div>
                        <div style="width: 956px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateHelp_searchTestContentsLabel" runat="server" Text="Helps to search for self admin test by providing skills as keyword. The candidate can take the test immediately or save it for later use"
                                CssClass="candidate_help_contents"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 978px; float: left; height: 20px">
                    </div>
                    <div style="width: 978px; float: left">
                        <div style="width: 956px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:LinkButton ID="CandidateHelp_myActivitiesTitleLabel" runat="server" Text="My Activities"
                                ToolTip="Click here to navigate to my activities page" CssClass="candidate_help_topic_link_button"
                                PostBackUrl="~/CandidateAdmin/Activities.aspx?displaytype=All"></asp:LinkButton>
                        </div>
                        <div style="width: 956px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateHelp_myActivitiesContentsLabel" runat="server" Text="Helps to view the list of activities such as pending tests, pending interviews, etc. Also has the links to perform various operations against tests & interviews"
                                CssClass="candidate_help_contents"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 978px; float: left; height: 20px">
                    </div>
                    <div style="width: 978px; float: left">
                        <div style="width: 956px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:LinkButton ID="CandidateHelp_myResumeTitleLabel" runat="server" Text="My Resume"
                                ToolTip="Click here to navigate to my resume page" CssClass="candidate_help_topic_link_button"
                                PostBackUrl="~/ResumeRepository/MyResume.aspx"></asp:LinkButton>
                        </div>
                        <div style="width: 956px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateHelp_myResumeContentsLabel" runat="server" Text="Helps to review, edit and approve parsed resume details"
                                CssClass="candidate_help_contents"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 978px; float: left; height: 40px">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
