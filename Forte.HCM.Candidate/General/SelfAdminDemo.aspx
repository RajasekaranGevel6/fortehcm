﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelfAdminDemo.aspx.cs" Inherits="Forte.HCM.Candidate.SelfAdminDemo"
    MasterPageFile="~/SiteMaster.Master" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="SelfAdminDemo_headContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="SelfAdminDemo_mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 960px; float: left">
                    <div style="width: 470px; padding-left: 10px; padding-top: 10px; float: left;">
                        <asp:Label ID="SelfAdminDemo_stepsLabel" runat="server" Text="Steps" CssClass="self_admin_demo_title"></asp:Label>
                        <div class="self_admin_demo_list">
                            <ul>
                                <li>Search for tests using keywords</li>
                                <li>Save or schedule the test immediately</li>
                                <li>Attend the test</li>
                                <li>View/download/email results</li>
                                <li>Publish/share results into social web sites</li>
                            </ul>
                        </div>
                    </div>
                    <div style="width: 470px; padding-left: 10px; padding-top: 10px; float: left;">
                        <asp:Label ID="SelfAdminDemo_selfSignUpDemoLabel" runat="server" Text="Self Admin Test Administration Demo"
                            CssClass="self_admin_demo_title"></asp:Label>
                        <object width="420" height="315">
                            <param name="movie" value="http://www.youtube.com/v/Yb63qduvxWU?version=3&amp;hl=en_US">
                            </param>
                            <param name="allowFullScreen" value="true"></param>
                            <param name="allowscriptaccess" value="always"></param>
                            <embed src="http://www.youtube.com/v/Yb63qduvxWU?version=3&amp;hl=en_US" type="application/x-shockwave-flash"
                                width="420" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>
                    </div>
                </div>
                <div style="width: 960px; height: 20px; float: left">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
