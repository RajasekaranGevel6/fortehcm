﻿<%@ Page Title="Feedback" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Feedback.aspx.cs" Inherits="Forte.HCM.UI.General.Feedback" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Feedback_bodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" language="javascript">

        // Method that will show/hide the captcha image help panel.
        function ShowWhatsThis(show)
        {
            if (show == 'Y')
            {
                $find('Feedback_captchaHelpTextModalPopUpExtender').show();
            }

            else
            {
                $find('Feedback_captchaHelpTextModalPopUpExtender').hide();
            }
            return false;
        }
        
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 980px; float: left">
                    <div style="width: 800px; padding-left: 2px; padding-top: 10px; float: left">
                        <asp:Label ID="Feedback_headerLiteral" runat="server" Text="Feedback" CssClass="feedback_title"></asp:Label>
                    </div>
                    <div style="width: 40px; padding-left: 2px; padding-top: 15px; float: right">
                        <asp:LinkButton ID="Feedback_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                            Text="Cancel" OnClick="Feedback_cancelLinkButton_Click" />
                    </div>
                    <div style="width: 4px; padding-left: 2px; padding-top: 15px; float: right">
                        |
                    </div>
                    <div style="width: 40px; padding-left: 2px; padding-top: 15px; float: right">
                        <asp:LinkButton ID="Feedback_topResetLinkButton" runat="server" SkinID="sknActionLinkButton"
                            Text="Reset" OnClick="Feedback_resetLinkButton_Click" />
                    </div>
                </div>
                <div style="width: 980px">
                    <asp:Label ID="Feedback_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                </div>
                <div style="width: 980px">
                    <asp:Label ID="Feedback_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                </div>
                <div style="width: 980px">
                    <div style="width: 488px; float: left">
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left" class="feedback_instructions_title">
                            Instructions:
                        </div>
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left" class="feedback_instructions">
                            1. Items marked as <span class="mandatory">*</span> are mandatory
                        </div>
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left" class="feedback_instructions">
                            2. Security image ensures that the form is being submitted by a human being
                        </div>
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left">
                            <div style="width: 120px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Label ID="Feedback_nameLabel" runat="server" Text="First Name" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                    class="mandatory">*</span>
                            </div>
                            <div style="width: 300px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:TextBox ID="Feedback_nameTextBox" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                            </div>
                            <div style="width: 120px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Label ID="Feedback_lastNameLabel" runat="server" Text="Last Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </div>
                            <div style="width: 300px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:TextBox ID="Feedback_lastNameTextBox" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                            </div>
                            <div style="width: 120px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Label ID="Feedback_emailLabel" runat="server" Text="Email" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                    class="mandatory">*</span>
                            </div>
                            <div style="width: 300px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:TextBox ID="Feedback_emailTextBox" runat="server" Width="60%" MaxLength="50"></asp:TextBox>
                            </div>
                            <div style="width: 120px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Label ID="Feedback_phoneNumberLabel" runat="server" Text="Phone Number" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                    class="mandatory">*</span>
                            </div>
                            <div style="width: 300px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:TextBox ID="Feedback_phoneNumberTextBox" runat="server" Width="30%" MaxLength="15"></asp:TextBox>
                            </div>
                            <div style="width: 120px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Label ID="Feedback_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                    class="mandatory">*</span>
                            </div>
                            <div style="width: 300px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:DropDownList ID="Feedback_subjectDropDownList" runat="server" Width="32%">
                                    <asp:ListItem Text="--Select--" Value="0">
                                    </asp:ListItem>
                                    <asp:ListItem Text="General Comment" Value="1">
                                    </asp:ListItem>
                                    <asp:ListItem Text="Feed back on a feature" Value="2">
                                    </asp:ListItem>
                                    <asp:ListItem Text="Suggest a feature" Value="3">
                                    </asp:ListItem>
                                    <asp:ListItem Text="Report a bug" Value="4">
                                    </asp:ListItem>
                                    <asp:ListItem Text="Others" Value="5">
                                    </asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div style="width: 120px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Label ID="Feedback_commentLabel" runat="server" Text="Comments" SkinID="sknLabelFieldHeaderText"></asp:Label><span
                                    class="mandatory">*</span>
                            </div>
                            <div style="width: 300px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:TextBox ID="Feedback_commentsTextBox" runat="server" Columns="65" TextMode="MultiLine"
                                    Height="100" MaxLength="1000" onkeyup="CommentsCount(1000,this)" onchange="CommentsCount(1000,this)"></asp:TextBox>
                            </div>
                            <div style="width: 204px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:UpdatePanel ID="Feedback_updatePanel" runat="server">
                                    <ContentTemplate>
                                        <input type="hidden" runat="server" id="Feedback_countHidden" value="0" />
                                        <img id="Feedback_capchaImage" src="" runat="server" alt="Captcha Image" />
                                        &nbsp;
                                        <asp:LinkButton ID="Feedback_regenerateCaptchaImageLinkButton" runat="server" Text="Regenerate Key"
                                            CssClass="link_button_hcm" OnClick="Feedback_regenerateCaptchaImageLinkButton_Click"></asp:LinkButton>
                                        &nbsp;&nbsp;<asp:LinkButton ID="Feedback_whatsThisLinkButton" runat="server" Text="What's This"
                                            SkinID="sknActionLinkButton" OnClientClick="javascript:return ShowWhatsThis('Y')" />
                                        <ajaxToolKit:ModalPopupExtender ID="Feedback_captchaHelpTextModalPopUpExtender" runat="server"
                                            BehaviorID="Feedback_captchaHelpTextModalPopUpExtender" TargetControlID="Feedback_whatsThisLinkButton"
                                            PopupControlID="Feedback_whatsThisDiv" BackgroundCssClass="modalBackground">
                                        </ajaxToolKit:ModalPopupExtender>
                                        <div id="Feedback_whatsThisDiv" style="display: none; height: 190px; width: 270px;
                                            left: 390px; top: 340px; z-index: 0; position: absolute" class="popupcontrol_confirm">
                                            <div style="width: 268px; float: left">
                                                <div class="popup_header_text" style="width: 100px; padding-left: 10px; padding-top: 10px;
                                                    float: left">
                                                    <asp:Literal ID="Feedback_whatsThisDiv_titleLiteral" runat="server" Text="What's This"></asp:Literal>
                                                </div>
                                                <div style="width: 20px; float: right; padding-top: 4px; padding-right: 6px">
                                                    <asp:ImageButton ID="Feedback_whatsThisDiv_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                                        OnClientClick="javascript:return ShowWhatsThis('N')" />
                                                </div>
                                            </div>
                                            <div style="width: 268px; padding-left: 10px; float: left">
                                                <div style="width: 248px; float: left; height: 150px" class="feedback_captcha_inner_bg">
                                                    <div style="width: 228px; float: left; padding-left: 4px; padding-right: 4px" class="label_field_text">
                                                        <asp:Label ID="Feedback_whatsThisDiv_messageLiteral" runat="server" Text="The image you are looking at is known as a captcha. This is a security test to determine whether or not the user is human. In order to submit the form, you must enter the text seen in the image into the text box. The letters are case sensitive. If you can't make out the text, click the Regenerate Key link seen below the image">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div style="width: 204px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Label ID="Feedback_captchaImageHeaderLabel" runat="server" Text="Security Code"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div style="width: 204px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:UpdatePanel ID="Feedback_captchaImageTextBoxUpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="Feedback_captchaImageTextBox" runat="server" AutoCompleteType="None"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div style="width: 204px; padding-left: 10px; padding-top: 10px; float: left">
                                <asp:Button ID="Feedback_submitButton" SkinID="sknButtonId" runat="server" Text="Submit"
                                    Width="60px" OnClick="Feedback_submitButton_Click" />
                            </div>
                        </div>
                    </div>
                    <div style="width: 488px; float: left" class="feedback_background">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
