﻿#region Header                                                                 

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Overview.aspx.cs
// File that represents the user interface layout and functionalities for the
// Overview page. This page helps in showing an overview on the application
// features.

#endregion Header 

#region Directives                                                             

using System;
using System.Web.UI;

using Forte.HCM.Trace;
using Forte.HCM.UI.Common;

#endregion Directives

namespace Forte.HCM.UI
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the Overview page. This page helps in showing an overview on the 
    /// application features. This class inherits Forte.HCM.UI.Common.PageBase 
    /// class.
    /// </summary>
    public partial class Overview : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Method that is called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Title = "ForteHCM - Overview";

                // Clear message and hide labels.
                ClearMessages();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Overview_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that is called when the cancel link button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Overview_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["USER_DETAIL"] == null)
                    Response.Redirect("~/SignIn.aspx", false);
                else
                    Response.Redirect("~/Dashboard.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Overview_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            Overview_topErrorMessageLabel.Text = string.Empty;
            Overview_topErrorMessageLabel.Visible = false;
        }

        #endregion Private Methods
    }
}