﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="Overview.aspx.cs" Inherits="Forte.HCM.UI.Overview" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Overview_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div style="width: 980px; float: left">
        <div style="width: 800px; padding-left: 2px; padding-top: 10px; float: left">
            <asp:Label ID="Overview_headerLiteral" runat="server" Text="Overview" CssClass="overview_page_title"></asp:Label>
        </div>
        <div style="width: 40px; padding-left: 2px; padding-top: 15px; float: right">
            <asp:LinkButton ID="Overview_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                Text="Cancel" OnClick="Overview_cancelLinkButton_Click" />
        </div>
    </div>
    <div style="width: 980px; height: 24px">
    </div>
    <div style="width: 980px">
        <asp:Label ID="Overview_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
    </div>
    <div style="width: 980px; height: 60px">
    </div>
    <div style="width: 980px" class="overview_title">
        Online Testing
    </div>
    <div style="width: 980px" class="overview_content">
        Candidates are provided with the facility to take test at online. The application
        provides an user friendly interface to take the test and perform related operations.
    </div>
    <div style="width: 980px; height: 24px">
    </div>
    <div style="width: 980px" class="overview_title">
        Online Interview
    </div>
    <div style="width: 980px" class="overview_content">
        Candidates are provided with the facility to take interview at online. The application
        provides an user friendly interface to take the interview and perform related operations.
        A webcam and mic is necessary to take an online interview online. The candidate
        can communication with more than one interviewer at a time.
    </div>
    <div style="width: 980px; height: 24px">
    </div>
    <div style="width: 980px" class="overview_title">
        Offline Interview
    </div>
    <div style="width: 980px" class="overview_content">
        Candidates are provided with the facility to take interview offline. The application
        provides an user friendly interface to take the offline interview and perform related
        operations. A webcam and mic is necessary to take an offline interview online. A
        windows utility will be downloaded to the candidates machine and the candidate can
        take the interview using that. At the end of the session the recorded videos are
        transfered to the server machine.
    </div>
</asp:Content>
