﻿using System;
using System.Web.UI.WebControls;

using Forte.HCM.Trace;
using Forte.HCM.UI.Common;

namespace Forte.HCM.UI.General
{
    public partial class CandidateHelp : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Help");

                // Clear message and hide labels.
                ClearMessages();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(CandidateHelp_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the method that shows the user message.
        /// </summary>
        /// <param name="topLabel">
        /// A <see cref="Label"/> that holds the top label control.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        private new void ShowMessage(Label label, string message)
        {
            // Set the message to the label.
            if (label.Text.Length == 0)
            {
                label.Text = message;
            }
            else
            {
                label.Text += "<br>" + message;
            }
        }

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods   

        #region Private Methods

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            CandidateHelp_topErrorMessageLabel.Text = string.Empty;
            CandidateHelp_topSuccessMessageLabel.Text = string.Empty;

            CandidateHelp_topErrorMessageLabel.Visible = false;
            CandidateHelp_topSuccessMessageLabel.Visible = false;
        }

        #endregion Private Methods
    }
}