﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Offerings.aspx.cs" Inherits="Forte.HCM.Candidate.Offerings"
    MasterPageFile="~/SiteMaster.Master" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Offerings_headContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Offerings_mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 960px">
                    <div style="width: 470px; padding-left: 10px; padding-top: 10px; float: left;">
                        <asp:Label ID="Offerings_offeringsTitle" runat="server" Text="Offerings" CssClass="offerings_title"></asp:Label>
                        <div class="offerings_list">
                            <ul>
                                <li>Self Signup</li>
                                <li>Self Admin Test</li>
                                <li>View Results</li>
                                <li>Publish/Share Results</li>
                            </ul>
                        </div>
                    </div>
                    <div style="width: 470px; padding-left: 10px; padding-top: 10px; float: left;">
                        <asp:Label ID="Offerings_selfSignUpDemoTitle" runat="server" Text="Self Signup Demo"
                            CssClass="offerings_title"></asp:Label>
                        <object width="420" height="315">
                            <param name="movie" value="http://www.youtube.com/v/Yb63qduvxWU?version=3&amp;hl=en_US">
                            </param>
                            <param name="allowFullScreen" value="true"></param>
                            <param name="allowscriptaccess" value="always"></param>
                            <embed src="http://www.youtube.com/v/Yb63qduvxWU?version=3&amp;hl=en_US" type="application/x-shockwave-flash"
                                width="420" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>
                    </div>
                </div>
                 <div style="width: 960px; height: 20px; float: left">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
