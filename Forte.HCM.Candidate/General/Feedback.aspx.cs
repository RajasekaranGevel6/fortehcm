﻿#region Header                                                                 

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Feedback.cs
// Class that represents the user interface layout and functionalities
// for the Feedback page. This page helps in collecting the feedback and
// send it as a mail to the administrator.

#endregion Header 

#region Directives                                                             

using System;
using System.Text;
using System.Text.RegularExpressions;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using Resources;

#endregion Directives

namespace Forte.HCM.UI.General
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the Feedback page. This page helps in collecting the feedback and
    /// send it as a mail to the administrator. This class inherits the 
    /// Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class Feedback : PageBase
    {
        #region Private Constants                                              

        /// <summary>
        /// A <see cref="string"/> that holds the key for the view state object
        /// that holds the string value used to store the capcha value.
        /// </summary>
        private const string CAPTCHA_KEY = "CAPTCHA_TEXT";

        #endregion Private Constants

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Feedback");

                // Clear message and hide labels.
                ClearMessages();

                if (!IsPostBack)
                {
                    // Load logged in user details.
                    LoadLoggedInUserdDetails();

                    // Generate captcha image.
                    GenerateCaptchaImage();

                    if (Request.QueryString["from"] == "success")
                    {
                        base.ShowMessage(Feedback_topSuccessMessageLabel,
                            Resources.HCMResource.Feedback_FeedbackMailSentSuccessfully);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       exception.Message);
            }
        }
        
        /// <summary>
        /// Handler method that will be called when the reset link button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Feedback_resetLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/General/Feedback.aspx", false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel link button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Feedback_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["USER_DETAIL"] == null)
                    Response.Redirect("~/SignIn.aspx", false);
                else
                    Response.Redirect("~/Dashboard.aspx", false);
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       exception.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the submit button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will submit the feedback as a mail to the administrator.
        /// </remarks>
        protected void Feedback_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    if (!IsValidEmailAddress(Feedback_emailTextBox.Text.Trim()))
                    {
                        base.ShowMessage(Feedback_topErrorMessageLabel,
                            HCMResource.Feedback_EnterValidEmailID);
                        return;
                    }

                    SendMail();

                    Response.Redirect("~/General/Feedback.aspx?&from=success", false);
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       Resources.HCMResource.Feedback_MailCouldNotSend);
            }
        }

        /// <summary>
        /// Handler method that will be called when the regenerate captcha link
        /// button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will generate a new captcha image.
        /// </remarks>
        protected void Feedback_regenerateCaptchaImageLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateCaptchaImage();
                Feedback_captchaImageTextBox.Text = "";
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                base.ShowMessage(Feedback_topErrorMessageLabel,
                       exception.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that validates whether the user input username is valid
        /// email or not using regular expression
        /// </summary>
        /// <param name="strUserEmailId">
        /// A <see cref="System.String"/> that holds the email to validate
        /// </param>
        /// <returns>
        /// A <see cref="System.Boolean"/> that holds the validity status
        /// </returns>
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$", RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            Feedback_topSuccessMessageLabel.Text = string.Empty;
            Feedback_topErrorMessageLabel.Text = string.Empty;
            Feedback_topSuccessMessageLabel.Visible = false;
            Feedback_topErrorMessageLabel.Visible = false;
        }

        /// <summary>
        /// Method that loads the logged in user details.
        /// </summary>
        private void LoadLoggedInUserdDetails()
        {
            if (Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                return;
            UserDetail userDetail = (Session["USER_DETAIL"] as UserDetail);
            Feedback_nameTextBox.Text = userDetail.FirstName.Length == 0 ? string.Empty : userDetail.FirstName;
            Feedback_lastNameTextBox.Text = userDetail.LastName.Length == 0 ? string.Empty : userDetail.LastName.Trim();
            Feedback_emailTextBox.Text = userDetail.Email.Length == 0 ? string.Empty : userDetail.Email.Trim(); ;
            Feedback_phoneNumberTextBox.Text = userDetail.Phone == null ? string.Empty : userDetail.Phone.Trim();
        }

        /// <summary>
        /// Method that generates the captcha image.
        /// </summary>
        private void GenerateCaptchaImage()
        {
            CaptchaImage ci = null;

            try
            {
                Feedback_capchaImage.Src = "";
                ci = new CaptchaImage(
                   RandomString(5), 100, 60, "Century Schoolbook");
                Session["CAPTCHA_IMAGE"] = ci.Image;
                ViewState[CAPTCHA_KEY] = ci.Text;
                Feedback_countHidden.Value = (Convert.ToInt32(Feedback_countHidden.Value) + 1).ToString();
                Feedback_capchaImage.Src = "~/Common/CaptchaImageHandler.ashx?id=" +
                     Feedback_countHidden.Value;
            }
            finally
            {
                if (ci != null) ci = null;
            }
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// Method that compose and sends the feedback mail.
        /// </summary>
        private void SendMail()
        {
            //Collect the details from the UI
            FeedbackDetail feedbackDetail = new FeedbackDetail();

            feedbackDetail.FirstName = Feedback_nameTextBox.Text.Trim();
            feedbackDetail.LastName = Feedback_lastNameTextBox.Text.Trim();
            feedbackDetail.Email = Feedback_emailTextBox.Text.Trim();
            feedbackDetail.PhoneNumber = Feedback_phoneNumberTextBox.Text.Trim();
            feedbackDetail.Subject = Feedback_subjectDropDownList.SelectedItem.Text;
            feedbackDetail.Comments = Feedback_commentsTextBox.Text.Trim();

            // Send email
            new EmailHandler().SendMail(EntityType.Feedback, feedbackDetail);
        }

        /// <summary>
        /// Method that applies the default style to the control. This will
        /// helps to reset the styles overridden by the error message.
        /// </summary>
        private void MakeDefaultStyle()
        {
            Feedback_nameTextBox.CssClass = "text_box";
            Feedback_emailTextBox.CssClass = "text_box";
            Feedback_phoneNumberTextBox.CssClass = "text_box";
            Feedback_commentsTextBox.CssClass = "text_box";
            Feedback_captchaImageTextBox.CssClass = "text_box";
            Feedback_subjectDropDownList.CssClass = "dropdown";
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            MakeDefaultStyle();

            bool value = true;

            if (Utility.IsNullOrEmpty(Feedback_nameTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(Feedback_emailTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(Feedback_phoneNumberTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(Feedback_commentsTextBox.Text.Trim()) ||
                Utility.IsNullOrEmpty(Feedback_captchaImageTextBox.Text.Trim()))
            {
                base.ShowMessage(Feedback_topErrorMessageLabel, HCMResource.Feedback_EnterMandatoryFields);
                value = false;
            }

            if (Utility.IsNullOrEmpty(Feedback_nameTextBox.Text.Trim()))
            {
                Feedback_nameTextBox.CssClass = "mandatory_text_box";
            }
            if (Utility.IsNullOrEmpty(Feedback_emailTextBox.Text.Trim()))
            {
                Feedback_emailTextBox.CssClass = "mandatory_text_box";
            }
            if (Utility.IsNullOrEmpty(Feedback_phoneNumberTextBox.Text.Trim()))
            {
                Feedback_phoneNumberTextBox.CssClass = "mandatory_text_box";
            }
            if (Utility.IsNullOrEmpty(Feedback_commentsTextBox.Text.Trim()))
            {
                Feedback_commentsTextBox.CssClass = "mandatory_text_box";
            }
            if (Utility.IsNullOrEmpty(Feedback_captchaImageTextBox.Text.Trim()))
            {
                Feedback_captchaImageTextBox.CssClass = "mandatory_text_box";
            }

            if (Feedback_subjectDropDownList.SelectedIndex == 0)
            {
                Feedback_subjectDropDownList.CssClass = "mandatory_dropdown";
                value = false;
            }

            if (Feedback_captchaImageTextBox.Text.Trim().Length > 0 &&
                    Feedback_captchaImageTextBox.Text.Trim() != ViewState[CAPTCHA_KEY].ToString())
            {
                base.ShowMessage(Feedback_topErrorMessageLabel, HCMResource.Feedback_SecurityCode);
                value = false;
            }
            return value;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Overridden Methods
    }
}