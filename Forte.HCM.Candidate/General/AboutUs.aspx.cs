﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Forte.HCM.Trace;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.General
{
    public partial class AboutUs : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.SetPageCaption("About Us");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AboutUs_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the AboutUs_cancelLinkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AboutUs_cancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["USER_DETAIL"] == null)
                    Response.Redirect("~/SignIn.aspx", false);
                else
                    Response.Redirect("~/Dashboard.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                ShowMessage(AboutUs_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Represents the method that shows the user message.
        /// </summary>
        /// <param name="topLabel">
        /// A <see cref="Label"/> that holds the top label control.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        private void ShowMessage(Label label, string message)
        {
            // Set the message to the label.
            if (label.Text.Length == 0)
            {
                label.Text = message;
            }
            else
            {
                label.Text += "<br>" + message;
            }
        }
    }
}