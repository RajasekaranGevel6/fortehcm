﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HCMCandidateMaster.Master"
    CodeBehind="ActivityHome.aspx.cs" Inherits="Forte.HCM.UI.ActivityHome" %>
<%@ MasterType VirtualPath="~/MasterPages/HCMCandidateMaster.Master" %>
<%@ Register Src="~/CommonControls/ActivityDetailControl.ascx" TagName="ActivityDetailControl" TagPrefix="uc4" %>
<%@ Register src="~/CommonControls/ActivityPageNavigator.ascx" tagname="ActivityPageNavigator" TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/InterviewDetailControl.ascx" TagName="InterviewDetailControl" TagPrefix="uc6" %>

<asp:Content ID="ActivityHome_headContent" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ActivityHome_content" ContentPlaceHolderID="HCMCandidateMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript">

    </script>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:Label ID="ActivityHome_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ActivityHome_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td class="candidate_header" style="width: 40%">
                            <asp:Literal ID="ActivityHome_myDetailsLiteral" runat="server" Text="MY DETAILS"></asp:Literal>
                        </td>
                        <td class="candidate_header" style="width: 30%">
                            <asp:Literal ID="ActivityHome_quickInfoLiteral" runat="server" Text="QUICK INFO"></asp:Literal>
                        </td>
                        <td class="candidate_header" style="width: 20%">
                            <asp:Literal ID="ActivityHome_quickLinksLiteral" runat="server" Text="QUICK LINKS"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="candidate_td_h_line">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 40%">
                            <table width="100%" cellpadding="0" cellspacing="5">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="ActivityHome_fullNameLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Full Name"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="ActivityHome_fullNameValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="ActivityHome_userSinceLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="User Since"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="ActivityHome_userSinceValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="ActivityHome_recruiterLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Recruiter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="ActivityHome_recruiterValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="td_height_10">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 30%">
                            <table width="100%" cellpadding="0" cellspacing="5">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="ActivityHome_pendingActivitiesLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Pending Activities"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="ActivityHome_pendingActivitiesValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 20%">
                            <table width="100%" cellpadding="0" cellspacing="5">
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="ActivityHome_emailRecruiterButton" Width="100%" Text="Email Recruiter"
                                            ToolTip="Click here to send an email to recruiter" SkinID="sknButtonCandidate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="ActivityHome_feedbackButton" Width="100%" Text="Feedback"
                                            ToolTip="Click here to send a feedback" SkinID="sknButtonCandidate" PostBackUrl="~/General/Feedback.aspx" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="candidate_td_h_line">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 10px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 20%" class="activity_home_pending_tests_icon">
                        </td>
                        <td style="width: 80%">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 100%; height: 2%" align="left">
                                        <uc5:ActivityPageNavigator ID="ActivityHome_testActivityPageNavigator" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%; height: 98%">
                                        <uc4:ActivityDetailControl ID="ActivityHome_testActivityDetailControl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 20%" class="activity_home_pending_interviews_icon">
                        </td>
                        <td style="width: 80%">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 100%; height: 2%" align="left">
                                        <uc5:ActivityPageNavigator ID="ActivityHome_interviewActivityPageNavigator" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%; height: 98%">
                                        <uc6:InterviewDetailControl ID="ActivityHome_interviewActivityDetailControl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="ActivityHome_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ActivityHome_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
