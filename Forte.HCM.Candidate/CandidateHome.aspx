﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/HCMCandidateMaster.Master"
    CodeBehind="CandidateHome.aspx.cs" Inherits="Forte.HCM.UI.CandidateHome" %>

<%@ Register Src="~/CommonControls/AnnouncementsControl.ascx" TagName="AnnouncementsControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/MasterPages/HCMCandidateMaster.Master" %>
<asp:Content ID="CandidateHome_headContent" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="CandidateHome_content" ContentPlaceHolderID="HCMCandidateMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript">
        function StartTest() {
            $find("<%= CandidateHome_startTestPopupExtender.ClientID  %>").show();
            return false;
        }
    </script>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:Label ID="CandidateHome_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="CandidateHome_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td class="candidate_header">
                            <asp:Literal ID="CandidateHome_myDetailsLiteral" runat="server" Text="MY DETAILS"></asp:Literal>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="candidate_header">
                            <asp:Literal ID="CandidateHome_quickInfoLiteral" runat="server" Text="QUICK INFO"></asp:Literal>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="candidate_header">
                            <asp:Literal ID="CandidateHome_quickLinksLiteral" runat="server" Text="QUICK LINKS"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="candidate_td_h_line">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table width="100%" cellpadding="0" cellspacing="5">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_fullNameLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Full Name"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_fullNameValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_userSinceLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="User Since"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_userSinceValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_lastTestTakenLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Last Test Taken"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_lastTestTakenValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_lastTestTakenDateLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Last Test Taken Date"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_lastTestTakenDateValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 10%">
                            &nbsp;
                        </td>
                        <td valign="top">
                            <table width="100%" cellpadding="0" cellspacing="5">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_pendingTestsLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Pending Tests"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label runat="server" ID="CandidateHome_pendingTestsValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_completedTestsLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Completed Tests"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label runat="server" ID="CandidateHome_completedTestsValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_expiredTestsLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Expired Tests"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label runat="server" ID="CandidateHome_expiredTestsValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_pendingInterviewsLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Pending Interviews"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label runat="server" ID="CandidateHome_pendingInterviewsValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_completedInterviewsLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Completed Interviews"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label runat="server" ID="CandidateHome_completedInterviewsValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="CandidateHome_expiredInterviewsLabel" SkinID="sknLabelFieldHeaderText"
                                            Text="Expired Interviews"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label runat="server" ID="CandidateHome_expiredInterviewsValueLabel" SkinID="sknLabelFieldText"
                                            Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="candidate_td_h_line">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 10%">
                            &nbsp;
                        </td>
                        <td valign="top">
                            <table width="100%" cellpadding="0" cellspacing="5">
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="CandidateHome_pendingTestsButton" Width="100%" Text="Pending Tests"
                                            SkinID="sknButtonCandidate" PostBackUrl="~/TestCenter/MyTests.aspx?m=2&ex=1"
                                            ToolTip="Click here to view the pending tests" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="CandidateHome_completedTestsButton" Width="100%" Text="Completed Tests"
                                            SkinID="sknButtonCandidate" PostBackUrl="~/TestCenter/MyTests.aspx?m=2&ex=2"
                                            ToolTip="Click here to view the completed tests" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="CandidateHome_expiredTestsButton" Width="100%" Text="Expired Tests"
                                            SkinID="sknButtonCandidate" PostBackUrl="~/TestCenter/MyTests.aspx?m=2&ex=3"
                                            ToolTip="Click here to view the expired tests" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="CandidateHome_pendingInterviewsButton" Width="100%"
                                            Text="Pending Interviews" SkinID="sknButtonCandidate" PostBackUrl="~/InterviewCenter/MyInterviews.aspx?m=3&ex=1"
                                            ToolTip="Click here to view the pending interviews" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="CandidateHome_creditsButton" Width="100%" Text="Credits"
                                            SkinID="sknButtonCandidate" PostBackUrl="~/TestCenter/MyCredits.aspx?m=4" ToolTip="Click here to view the credits" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="CandidateHome_purchaseCreditsButton" Width="100%"
                                            Text="Purchase Credits" SkinID="sknButtonCandidate" ToolTip="Click here to purchase credits" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="candidate_td_h_line">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table width="100%" cellpadding="5" cellspacing="5" border="0">
                                <tr>
                                    <td class="cand_pending_tests_icon" style="width: 19%; height: 140px" valign="top">
                                    </td>
                                    <td valign="top" style="width: 81%">
                                        <asp:DataList ID="CandidateHome_pendingTestsDataList" runat="server" RepeatColumns="3"
                                            RepeatLayout="Table" GridLines="None" RepeatDirection="Vertical" Width="100%"
                                            OnItemDataBound="CandidateHome_pendingTestsDataList_ItemDataBound" OnItemCommand="CandidateHome_pendingTestsDataList_ItemCommand">
                                            <ItemTemplate>
                                                <div style="float: left; width: 268px; height: 180px">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="cand_bg_dark" runat="server" id="CandidateHome_pendingTestsTD" valign="top">
                                                                <table style="width: 100%">
                                                                    <tr style="height: 40px">
                                                                        <td class="candidate_test_header" style="width: 100%" valign="top">
                                                                            <asp:Label ID="CandidateHome_pendingTestsTestNameLabel" runat="server" Text='<%# Eval("TestName") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="candidate_label_text" style="width: 100%">
                                                                            <table style="width: 100%">
                                                                                <tr style="height: 40px">
                                                                                    <td colspan="2" valign="top" style="width: 100%">
                                                                                        <asp:Label ID="CandidateHome_pendingTestsTestDescriptionLabel" runat="server" ToolTip='<%# Eval("TestDescription")%>'
                                                                                            Text='<%# TrimContent(Convert.ToString(Eval("TestDescription")),20) %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 50%">
                                                                                        <asp:LinkButton ID="CandidateHome_pendingTestsTestIntroductionLinkButton" runat="server"
                                                                                            Text="Test Introduction" ToolTip="Click here to show the test introduction" SkinID="sknActionLinkButton"
                                                                                            CommandName="TestIntroduction"></asp:LinkButton>
                                                                                    </td>
                                                                                    <td style="width: 50%">
                                                                                        <asp:LinkButton ID="CandidateHome_pendingTestsTestReminderLinkButton" runat="server"
                                                                                            Text="Test Reminder" ToolTip="Click here to set the test reminder" SkinID="sknActionLinkButton">
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 50%">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateHome_pendingTestsCreatedDateLabel" runat="server" Text="Created :"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateHome_pendingTestsCreatedDateValueLabel" runat="server" Text='<%# Eval("CreatedDateFormatted") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="width: 50%" rowspan="2">
                                                                                        <asp:Button runat="server" ID="CandidateHome_pendingTestsStartTestButton" Text="Start Test"
                                                                                            SkinID="sknButtonId" CommandName="StartTest" ToolTip="Click here to start the test" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 50%">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateHome_pendingTestsExpiryDateLabel" runat="server" Text="Expiry : "></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateHome_pendingTestsExpiryDateValueLabel" runat="server" Text='<%# Eval("ExpiryDateFormatted") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <asp:HiddenField ID="CandidateHome_pendingTestsCandidateSessionID" runat="server"
                                                                                            Value='<%# Eval("CandidateTestSessionID") %>' />
                                                                                        <asp:HiddenField ID="CandidateHome_pendingTestsAttemptID" runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="cand_interested_tests_icon" style="width: 19%; height: 140px" valign="top">
                                    </td>
                                    <td valign="top" style="width: 81%">
                                        <asp:UpdatePanel ID="CandidateHome_interestedUpdatePanel" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:DataList ID="CandidateHome_interestedDataList" runat="server" RepeatColumns="3"
                                                    RepeatLayout="Table" GridLines="None" RepeatDirection="Vertical" Width="100%"
                                                    OnItemCommand="CandidateHome_interestedDataList_ItemCommand">
                                                    <ItemTemplate>
                                                        <div style="float: left; width: 268px; height: 180px">
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="cand_bg_dark" runat="server" id="CandidateHome_interestedTestsTD">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr style="height: 40px">
                                                                                <td class="candidate_test_header" style="width: 100%" valign="top">
                                                                                    <asp:Label ID="CandidateHome_interestedTestsTestNameLabel" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="candidate_label_text" style="width: 100%">
                                                                                    <table style="width: 100%">
                                                                                        <tr style="height: 40px">
                                                                                            <td colspan="2" valign="top" style="width: 100%">
                                                                                                <asp:Label ID="CandidateHome_interestedTestsTestDescriptionLabel" runat="server"
                                                                                                    ToolTip='<%# Eval("Description") %>' Text='<%# TrimContent(Convert.ToString(Eval("Description")),20) %>'></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 50%">
                                                                                                <asp:LinkButton ID="CandidateHome_interestedViewLinkButton" runat="server" OnClick='<%#String.Format(" return OpenAdditionalTestDetail(&#39;{0}&#39;)", Eval("TestKey")) %>'
                                                                                                    Text="View" ToolTip="Click here to view test statistics" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                            </td>
                                                                                            <td style="width: 50%">
                                                                                                <asp:LinkButton ID="CandidateHome_interestedCreateTestSessionLinkButton" runat="server"
                                                                                                    Text="Create Session" SkinID="sknActionLinkButton" ToolTip="Click here to create session"
                                                                                                    CommandName="CreateSession">
                                                                                                </asp:LinkButton>
                                                                                                <asp:HiddenField ID="CandidateHome_interestedTestKeyHiddenField" runat="server" Value='<%# Eval("TestKey") %>' />
                                                                                                <asp:HiddenField ID="CandidateHome_interestedTestDescriptionHiddenField" runat="server"
                                                                                                    Value='<%# Eval("Description") %>' />
                                                                                                <asp:HiddenField ID="CandidateHome_interestedTestNameHiddenField" runat="server"
                                                                                                    Value='<%# Eval("Name") %>' />
                                                                                                <asp:HiddenField ID="CandidateHome_interestedTestCostHiddenField" runat="server"
                                                                                                    Value='<%# Eval("TestCost") %>' />
                                                                                                <asp:HiddenField ID="CandidateHome_interestedTimeLimitHiddenField" runat="server"
                                                                                                    Value='<%# Eval("RecommendedCompletionTime") %>' />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 53%">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="CandidateHome_interestedTestsCreatedDateLabel" runat="server" Text="Created :"></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="CandidateHome_interestedTestsCreatedDateValueLabel" runat="server"
                                                                                                                Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate"))) %>'></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td style="width: 48%" rowspan="2">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="cand_recently_completed_tests_icon" style="width: 19%;">
                                    </td>
                                    <td valign="top" style="width: 81%">
                                        <asp:DataList ID="CandidateHome_completedTestsDataList" runat="server" RepeatColumns="3"
                                            RepeatLayout="Table" GridLines="None" RepeatDirection="Vertical" Width="100%"
                                            OnItemDataBound="CandidateHome_completedTestsDataList_ItemDataBound" OnItemCommand="CandidateHome_completedTestsDataList_ItemCommand">
                                            <ItemTemplate>
                                                <div style="float: left; width: 268px; height: 180px">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="cand_bg_dark" runat="server" id="CandidateHome_completedTestsTD">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr style="height: 40px">
                                                                        <td class="candidate_test_header" style="width: 100%" valign="top">
                                                                            <asp:Label ID="CandidateHome_completedTestsTestNameLabel" runat="server" Text='<%# Eval("TestName") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="candidate_label_text" style="width: 100%">
                                                                            <table style="width: 100%">
                                                                                <tr style="height: 40px">
                                                                                    <td colspan="2" valign="top" style="width: 100%">
                                                                                        <asp:Label ID="CandidateHome_completedTestsTestDescriptionLabel" runat="server" ToolTip='<%# Eval("TestDescription") %>'
                                                                                            Text='<%# TrimContent(Convert.ToString(Eval("TestDescription")),20) %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 50%">
                                                                                        <asp:LinkButton ID="CandidateHome_completedTestsRequestToRetakeLinkButton" runat="server"
                                                                                            ToolTip="Click here to request for retake" CommandName="RequestToRetake" Text="Request To Retake"
                                                                                            SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                    </td>
                                                                                    <td style="width: 50%">
                                                                                        <asp:LinkButton ID="CandidateHome_completedTestsTestResultsLinkButton" runat="server"
                                                                                            Text="Test Results" ToolTip="Click here to view test results" SkinID="sknActionLinkButton"
                                                                                            Visible='<%# IsShowResults(Eval("ShowResults").ToString())%>' CommandName="TestResults">
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 53%">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateHome_completedTestsCreatedDateLabel" runat="server" Text="Created :"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateHome_completedTestsCreatedDateValueLabel" runat="server"
                                                                                                        Text='<%# Eval("CreatedDateFormatted") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="width: 48%" rowspan="2">
                                                                                        <asp:LinkButton ID="CandidateHome_viewCertificateLinkbutton" Visible='<%# Eval("IsCertification") %>'
                                                                                            runat="server" Text="View Certificate" SkinID="sknActionLinkButton" ToolTip="View Certificate"></asp:LinkButton>
                                                                                        <asp:HiddenField ID="CandidateHome_testKeyHiddenField" runat="server" Value='<%# Eval("TestID") %>' />
                                                                                        <asp:HiddenField ID="CandidateHome_testCompletedOnHiddenField" runat="server" Value='<%# Eval("DateCompleted") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 53%">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateHome_completedTestsCompletedDateLabel" runat="server" Text="Completed : "></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateHome_completedTestsCompletedDateValueLabel" runat="server"
                                                                                                        Text='<%# Eval("DateCompletedFormatted") %>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <asp:HiddenField ID="CandidateHome_completedTestsCandidateSessionID" runat="server"
                                                                                            Value='<%# Eval("CandidateTestSessionID") %>' />
                                                                                        <asp:HiddenField ID="CandidateHome_completedTestsAttemptID" runat="server" Value='<%# Eval("AttemptID") %>' />
                                                                                        <asp:HiddenField ID="CandidateHome_completedTestID" runat="server" Value='<%# Eval("TestID") %>' />
                                                                                        <asp:HiddenField ID="CandidateHome_initiatedByHiddenField" Value='<%# Eval("TestInitiatedBy") %>'
                                                                                            runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="candidate_td_h_line">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="candidate_header">
                            MORE INFORMATION
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <ajaxToolKit:TabContainer ID="CandidateHome_mainTabContainer" runat="server" ActiveTabIndex="0">
                                            <ajaxToolKit:TabPanel ID="CandidateHome_announcementsTabPanel" HeaderText="Announcements & Updates"
                                                runat="server">
                                                <HeaderTemplate>
                                                    Announcements &amp; Updates</HeaderTemplate>
                                                <ContentTemplate>
                                                    <div class="cand_moreinfo_div">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td>
                                                                    <uc1:AnnouncementsControl ID="CandidateHome_announcementsControl" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ContentTemplate>
                                            </ajaxToolKit:TabPanel>
                                            <ajaxToolKit:TabPanel ID="CandidateHome_FAQsTabPanel" HeaderText="FAQs" runat="server">
                                                <HeaderTemplate>
                                                    FAQs</HeaderTemplate>
                                                <ContentTemplate>
                                                    <div class="cand_moreinfo_div">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="candidate_header" align="center">
                                                                    Under Construction
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ContentTemplate>
                                            </ajaxToolKit:TabPanel>
                                            <ajaxToolKit:TabPanel ID="CandidateHome_creditsTabPanel" HeaderText="Credits" runat="server">
                                                <HeaderTemplate>
                                                    Credits</HeaderTemplate>
                                                <ContentTemplate>
                                                    <div class="cand_moreinfo_div">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="candidate_header" align="center">
                                                                    Under Construction
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ContentTemplate>
                                            </ajaxToolKit:TabPanel>
                                            <ajaxToolKit:TabPanel ID="CandidateHome_helpTabPanel" HeaderText="Help" runat="server">
                                                <HeaderTemplate>
                                                    Help</HeaderTemplate>
                                                <ContentTemplate>
                                                    <div class="cand_moreinfo_div">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="candidate_header" align="center">
                                                                    Under Construction
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ContentTemplate>
                                            </ajaxToolKit:TabPanel>
                                        </ajaxToolKit:TabContainer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="CandidateHome_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="CandidateHome_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="CandidateHome_startTestPopupPanel" runat="server" Style="display: none;
                    height: 206px" CssClass="popupcontrol_confirm">
                    <div id="CandidateHome_hiddenDIV" style="display: none">
                        <asp:Button ID="CandidateHome_hiddenButton" runat="server" />
                    </div>
                    <uc2:ConfirmMsgControl ID="CandidateHome_confirmMsgControl" runat="server" OnOkClick="CandidateHome_startTestButton_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="CandidateHome_startTestPopupExtenderControl"
                    runat="server" PopupControlID="CandidateHome_startTestPopupPanel" TargetControlID="CandidateHome_hiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:UpdatePanel ID="CandidateHome_confirmMsgUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="CandidateHome_RetakeValidationPopupPanel" runat="server" Style="display: none;
                            height: 202px;" CssClass="popupcontrol_confirm">
                            <div id="CandidateHome_RetakeValidationDiv" style="display: none">
                                <asp:Button ID="CandidateHome_RetakeValidation_hiddenButton" runat="server" />
                            </div>
                            <uc2:ConfirmMsgControl ID="CandidateHome_RetakeValidation_ConfirmMsgControl" runat="server"
                                Title="Request to Retake" Type="OkSmall" />
                        </asp:Panel>
                        <ajaxToolKit:ModalPopupExtender ID="CandidateHome_RetakeValidation_ModalPopupExtender"
                            runat="server" PopupControlID="CandidateHome_RetakeValidationPopupPanel" TargetControlID="CandidateHome_RetakeValidation_hiddenButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <asp:HiddenField ID="CandidateHome_selectedCandidateSessionIDHiddenField" runat="server" />
            <asp:HiddenField ID="CandidateHome_selectedAttemptIDHiddenField" runat="server" />
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="CandidateHome_popUpUpdatePanel" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Panel ID="CandidateHome_testPreviewPanel" runat="server" Style="display: none"
                                        CssClass="popupcontrol_test_session">
                                        <div style="display: none">
                                            <asp:Button ID="CandidateHome_testPreviewhiddenButton" runat="server" Text="Hidden"
                                                SkinID="sknCloseImageButton" /></div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="td_height_20">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="90%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="width: 50%" class="popup_header_text" valign="middle" align="left">
                                                                <asp:Literal ID="CandidateHome_testPreviewHeaderLiteral" runat="server" Text="Create Test Session"></asp:Literal>
                                                            </td>
                                                            <td style="width: 50%" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0" align="right">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="CandidateHome_testPreviewTopCancelImageButton" SkinID="sknCloseImageButton"
                                                                                runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="popupcontrol_question_inner_bg" colspan="2" align="left">
                                                                <table border="0" cellpadding="2" cellspacing="5" width="100%" class="tab_body_bg">
                                                                    <tr>
                                                                        <td class="msg_align" colspan="4">
                                                                            <asp:Label ID="CandidateHome_testPreviewTopSuccessMessageLabel" runat="server" EnableViewState="false"
                                                                                SkinID="sknSuccessMessage"></asp:Label><asp:Label ID="CandidateHome_testPreviewTopErrorMessageLabel"
                                                                                    runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="CandidateHome_testPreviewTestIDHeadLabel" runat="server" Text="Test ID"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="CandidateHome_testPreviewTestIDLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="CandidateHome_testPreviewTestNameHeadLabel" runat="server" Text="Test Name"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="CandidateHome_testPreviewTestNameLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="CandidateHome_testPreviewTestCostHeadLabel" runat="server" Text="Test Cost (in $)"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="CandidateHome_testPreviewTestCostLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="CandidateHome_testPreviewTimeLimitHeadLabel" runat="server" Text="Time Limit"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:Label ID="CandidateHome_testPreviewTimeLimitLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" colspan="4">
                                                                            <asp:Label ID="CandidateHome_testPreviewTestDescriptionLabel" runat="server" Text="Test Description"
                                                                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <div style="width: 100%; height: 30px; overflow: auto;" class="label_field_text_div_tag"
                                                                                id="CandidateHome_testDescriptionDiv" runat="server">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Button ID="CandidateHome_testPreviewCreateButton" runat="server" Text="Create"
                                                                    SkinID="sknButtonId" OnClick="CandidateHome_testPreviewCreateButton_Click" />
                                                                <asp:Button ID="CandidateHome_testPreviewStartTestButton" runat="server" Text="Start Test"
                                                                    SkinID="sknButtonId" Visible="false" OnClientClick="javascript:return StartTest()" />
                                                                &nbsp;&nbsp;<asp:LinkButton ID="CandidateHome_testPreviewCloseLinkButton" SkinID="sknPopupLinkButton"
                                                                    runat="server" Text="Cancel" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="CandidateHome_testPreviewModalPopupExtender"
                                        runat="server" PopupControlID="CandidateHome_testPreviewPanel" TargetControlID="CandidateHome_testPreviewhiddenButton"
                                        BackgroundCssClass="modalBackground" CancelControlID="CandidateHome_testPreviewCloseLinkButton">
                                    </ajaxToolKit:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="CandidateHome_startTestUpdatePanel" runat="server">
                                        <ContentTemplate>
                                            <asp:Panel ID="CandidateHome_startTestForSessionPopupPanel" runat="server" Style="display: none;
                                                height: 206px" CssClass="popupcontrol_confirm">
                                                <div id="CandidateHome_startTestHiddenDIV" style="display: none">
                                                    <asp:Button ID="CandidateHome_startTestHiddenButton" runat="server" />
                                                </div>
                                                <uc3:ConfirmMsgControl ID="CandidateHome_startTestForSessionPopupExtenderControl"
                                                    runat="server" OnOkClick="CandidateHome_startTestForSessionButton_OkClick" OnCancelClick="CandidateHome_startTestForSessionButton_CancelClick" />
                                                <asp:HiddenField ID="CandidateHome_testCandidateSessionIdHiddenField" runat="server" />
                                            </asp:Panel>
                                            <ajaxToolKit:ModalPopupExtender ID="CandidateHome_startTestPopupExtender" runat="server"
                                                PopupControlID="CandidateHome_startTestForSessionPopupPanel" TargetControlID="CandidateHome_startTestHiddenButton"
                                                BackgroundCssClass="modalBackground">
                                            </ajaxToolKit:ModalPopupExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
