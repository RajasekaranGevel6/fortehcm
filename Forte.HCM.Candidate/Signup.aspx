﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Signup.aspx.cs" Inherits="Forte.HCM.Candidate.Signup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="SignupCandidate_contentID" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function ShowResumeParserMessagePopup()
        {
            if ('<%=parseImmediately %>' == false || '<%=parseImmediately %>' == 'False')
            {
                // Do not show parsing dialog. Just return to server code execution.
                return true;
            }

            // Check if profile name is entered.
            if (document.getElementById('<%= SignupCandidate_profileNameTextBox.ClientID %>').value == null ||
                TrimAll(document.getElementById('<%= SignupCandidate_profileNameTextBox.ClientID %>').value) == '')
            {
                return true;
            }

            if ($get("<%=Signup_fileUploadedHiddenField.ClientID%>").value == 'Y')
            {
                // Reset parsing completed status.
                document.getElementById('<%= Signup_parsingCompletedHiddenField.ClientID %>').value = 'N';

                // Reset title, message & button visibility.
                document.getElementById('<%= Signup_resumeParserMessageTitleLabel.ClientID %>').innerHTML = "Parsing On Process";
                document.getElementById('<%= Signup_resumeParserMessageLabel.ClientID %>').innerHTML = "Your resume is under parsing and will take few seconds to complete. Please wait...";

                if (document.getElementById('<%= Signup_resumeParserRotatingImageDiv.ClientID %>') != null)
                    document.getElementById('<%= Signup_resumeParserRotatingImageDiv.ClientID %>').style.display = "block";

                if (document.getElementById('<%= Signup_resumeParserCloseButton.ClientID %>') != null)
                    document.getElementById('<%= Signup_resumeParserCloseButton.ClientID %>').style.display = "none";

                if (document.getElementById('<%= Signup_resumeParserApproveButton.ClientID %>') != null)
                    document.getElementById('<%= Signup_resumeParserApproveButton.ClientID %>').style.display = "none";

                if (document.getElementById('<%= Signup_resumeParserDashboardButton.ClientID %>') != null)
                    document.getElementById('<%= Signup_resumeParserDashboardButton.ClientID %>').style.display = "none";

                $find("<%= Signup_resumeParserMessageModalPopupExtender.ClientID %>").show();

                ShowResumeParserRotatingImage();
                return true;
            }
        }

        // Trim left and right side white space
        function TrimAll(str)
        {
            str = str.replace(/^\s+/, '');
            return str.replace(/\s+$/, '');
        }

        function ShowResumeParserRotatingImage()
        {
            if ($get("<%=Signup_parsingCompletedHiddenField.ClientID%>").value == 'Y')
                return;

            document.getElementById('<%= Signup_resumeParserRotatingImage.ClientID %>').src = "App_Themes/DefaultTheme/Images/resume_parsing_process.gif";
            setTimeout("ShowResumeParserRotatingImage()", 500);
        }

        function OpenAgreementWindow()
        {
            window.open("./General/Agreement.aspx", "Agreement", "")
        }

        // Function that will call when select resume browse button is clicked
        // on the resumeeditor page and also start the upload process.
        function StartUpload(sender, args)
        {
            var filename = args.get_fileName();
            var contentType = args.get_contentType();
            document.getElementById('<%= ResumeEditor_candidateResumeFileNameHiddenField.ClientID %>').value = filename;
            document.getElementById('<%= ResumeEditor_candidateResumeFileMimeTypeHiddenField.ClientID %>').value = contentType;
        }

        // Function that will call when select resume browse button is clicked
        // on the resumeeditor page and also complete the complete.
        function UploadComplete(sender, args)
        {
            var filename = args.get_fileName();
            var contentType = args.get_contentType();
            $get("<%=SignIn_errorMessageLabel.ClientID%>").innerHTML = "";
            if ((filename.indexOf('.doc') == -1) && (filename.indexOf('.pdf') == -1) && (filename.indexOf('.docx') == -1) && (filename.indexOf('.DOC') == -1) && (filename.indexOf('.DOCX') == -1) && (filename.indexOf('.PDF') == -1))
            {
                $get("<%=SignIn_errorMessageLabel.ClientID%>").innerHTML = "Please provide a word or pdf file";
                return;
            }
            else
            {
                $get("<%= ResumeEditor_candidateResumeFileNameHiddenField.ClientID %>").value = filename;
                $get("<%= ResumeEditor_candidateResumeFileMimeTypeHiddenField.ClientID %>").value = contentType;
            }
        }

        // To validate resume upload control

        function OnClientAsyncResumeFileUploadComplete(sender, args)
        {
            // Reset parsing completed status.
            document.getElementById('<%= Signup_parsingCompletedHiddenField.ClientID %>').value = 'N';

            $get("<%=Signup_fileUploadedHiddenField.ClientID%>").value = 'N';
            var fileName = args.get_fileName();
            var fileExtn = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
            if (fileExtn != "doc" && fileExtn != "DOC" && fileExtn != "pdf" && fileExtn != "PDF" && fileExtn != "docx" && fileExtn != "DOCX")
            {
                var errMsg = "Please select valid file format.(.doc,.docx and .pdf)";
                $get("<%=SignIn_errorMessageLabel.ClientID%>").innerHTML = errMsg;
                args.set_cancel(true);
                var who1 = document.getElementsByName('<%=ResumeUploader_fileUpload.UniqueID %>')[0];
                who1.value = "";
                var who3 = who1.cloneNode(false);
                who3.onchange = who1.onchange;
                who1.parentNode.replaceChild(who3, who1);

                return false;
                alert(errMsg);
            }
            else
            {
                $get("<%=Signup_fileUploadedHiddenField.ClientID%>").value = 'Y';
            }
        }

        function uploadError(sender, args)
        {
            __doPostBack("<%= SignupCandidate_uploadResumeButton.ClientID %>", "");
            return false;
        }

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div>
                    <div class="common_padding">
                        <asp:UpdatePanel ID="SignIn_ErrorMessage_UpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Label runat="server" ID="Signin_CandidateNameLabel" CssClass="can_signup_ex_user"></asp:Label>
                                <br />
                                <asp:Label runat="server" ID="SignIn_errorMessageLabel" CssClass="Mandatory" Text=""></asp:Label>
                                <asp:HiddenField runat="server" ID="Signup_fileUploadedHiddenField"/>
                                <asp:HiddenField runat="server" ID="Signup_parsingCompletedHiddenField"/>

                                <div style="display: none">
                                    <asp:Button ID="Signup_defaultButton" runat="server" /></div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_AddSkillButton" />
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_LoginButton" />
                                <asp:PostBackTrigger ControlID="SignupCandidate_uploadResumeButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div id="title">
                        <asp:UpdatePanel ID="Signup_titleUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="can_tab_bg_left" runat="server" id="Signup_leftCorner">
                                    &nbsp;
                                </div>
                                <div id="NewCandidate_SignUpTag" class="can_tab_active_bg" runat="server">
                                    <asp:LinkButton ID="Candidate_SignupLinkButton" runat="server" Text="Create an account"
                                        CssClass="can_maintab_link"></asp:LinkButton>
                                </div>
                                <div id="NewCandidate_SkillsTag" class="can_tab_bg" runat="server">
                                    <asp:LinkButton ID="Candidate_SkillsLinkButton" runat="server" Text="Skills" CssClass="can_maintab_link"></asp:LinkButton>
                                </div>
                                <div id="NewCandidate_ResumeUploadTag" class="can_tab_bg" runat="server">
                                    <asp:LinkButton ID="Candidate_ResumeUploadLinkButton" runat="server" Text="Resume Upload"
                                        CssClass="can_maintab_link"></asp:LinkButton>
                                </div>
                                <div class="can_tab_bg_right" runat="server" id="Signup_rightCorner">
                                    &nbsp;
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_LoginButton" />
                                <asp:AsyncPostBackTrigger ControlID="NewCandidate_WelcomeContinueButton" />
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_ResumeUploadYesButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="Signup_userDetailUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="clear: both;" id="NewCandidate_SignUpDiv" runat="server" class="signup_outer_div_w">
                                    <div class="signup_left_div_w">
                                        <div class="signup_form_container" style="display: none">
                                            <div class="signup_form_container_left" id="ErrorMessage">
                                            </div>
                                        </div>
                                        <div class="signup_form_container">
                                            <div class="signup_form_container_left">
                                                <asp:Label runat="server" ID="SignupCandidate_FirstNameLabel">First Name<span>*</span></asp:Label>
                                            </div>
                                            <div class="form_container_right">
                                                <asp:TextBox ID="SignupCandidate_FirstNameText" runat="server" SkinID="sknCanTextBox"
                                                    ToolTip="Enter first name"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="signup_form_container">
                                            <div class="signup_form_container_left">
                                                <asp:Label ID="SignupCandidate_LastNameLabel" runat="server">Last Name<span>*</span></asp:Label>
                                            </div>
                                            <div class="signup_form_container_right">
                                                <asp:TextBox ID="SignupCandidate_LastNameText" SkinID="sknCanTextBox" runat="server"
                                                    ToolTip="Enter last name"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="signup_form_container">
                                            <div class="signup_form_container_left">
                                                <asp:Label ID="SignupCandidate_EmailIDLabel" runat="server" Text="">Email ID<span>*</span></asp:Label>
                                            </div>
                                            <div class="signup_form_container_right">
                                                <asp:TextBox ID="SignupCandidate_EmailIDText" SkinID="sknCanTextBox" runat="server"
                                                    ToolTip="Enter emailID"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="signup_form_container">
                                            <div class="signup_form_container_left">
                                                <asp:Label ID="SignupCandidate_PasswordLabel" runat="server" Text="">Password<span>*</span></asp:Label>
                                            </div>
                                            <div class="signup_form_container_right">
                                                <asp:TextBox ID="SignupCandidate_PasswordText" SkinID="sknCanTextBox" TextMode="Password"
                                                    runat="server" ToolTip="Enter password"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="signup_form_container">
                                            <div class="signup_form_container_left">
                                                <asp:Label ID="SignupCandidate_ReTypePasswordLabel" runat="server" Text="">Retype Password <span>*</span></asp:Label>
                                            </div>
                                            <div class="signup_form_container_right">
                                                <asp:TextBox ID="SignupCandidate_ReTypePasswordText" TextMode="Password" SkinID="sknCanTextBox"
                                                    runat="server" ToolTip="Enter confirm password"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="signup_form_container">
                                            <div class="form_container_left_cust">
                                                <asp:CheckBox ID="SignupCandidate_agreeCheckBox" Text="I agree to ForteHCM's" runat="server"
                                                    TextAlign="Right" />
                                                <asp:LinkButton ID="SignupCandidate_TermsofServicesLinkButton" runat="server" CssClass="can_forgotpassword_link"
                                                    ToolTip="Click here to view terms of service" Enabled="true" Style="padding-left: 2px"
                                                    OnClientClick="javascript:OpenAgreementWindow();">Terms of Service</asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="signup_form_container">
                                            <div class="form_container_left">
                                                <asp:Button ID="SignupCandidate_LoginButton" SkinID="sknButtonOrange" runat="server"
                                                    Text="Sign Up" OnClick="SignupCandidate_LoginButton_Click" ToolTip="Click here to signup"
                                                    Height="25px" Width="100px" />
                                                <asp:Label ID="Label2" Style="padding-left: 20px; font-size: 12px; font-weight: bold"
                                                    runat="server" Text="OR"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="signup_form_container">
                                            <div class="form_container_left_cust_w">
                                                <div id="Div1">
                                                    <asp:Label ID="SignupOpenID_Label" runat="server" CssClass="can_signup_link" Text="Sign up using OpenID"></asp:Label>
                                                </div>
                                                <div id="SigninCandidate" class="can_signup_ex_user_line" style="display: block;
                                                    width: 160px">
                                                    <div class="form_container">
                                                        <div class="icons_padding" style="padding-left: 10px">
                                                            <asp:Button ID="OpenID_YahooButton" runat="server" CssClass="yahoo_icon" CommandArgument="https://me.yahoo.com/"
                                                                OnCommand="OpenLogin_Click" ToolTip="Sign up yahoo ID" Style="cursor: pointer" />
                                                        </div>
                                                        <div class="icons_padding" style="padding-left: 10px">
                                                            <asp:Button ID="OpenID_GmailButton" runat="server" CssClass="gmail_icon" CommandArgument="https://www.google.com/accounts/o8/id"
                                                                OnCommand="OpenLogin_Click" ToolTip="Sign up gmail ID" Style="cursor: pointer" />
                                                        </div>
                                                        <div class="icons_padding" style="display: none">
                                                            <asp:Button ID="OpenID_LinkedINButton" runat="server" CssClass="linkedin_icon" CommandArgument="https://www.linkedin.com/uas/oauth/authorize?oauth_token=494dfcaf-7a88-4c34-aa7f-bc8c96172e3c"
                                                                OnCommand="OpenLogin_Click" ToolTip="Sign up linkedin ID" />
                                                        </div>
                                                        <div class="icons_padding" style="display: none">
                                                            <asp:Button ID="OpenID_FaceBookButton" runat="server" CssClass="facebook_icon" ToolTip="Sign up Facebook ID" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="signup_right_div_w">
                                        <div class="signup_left_bg">
                                            &nbsp;
                                        </div>
                                        <div class="signup_middle_bg">
                                            <div class="signup_box_content_head_text">
                                                Why Create an Account?</div>
                                            <div class="box_content_text">
                                                <ul>
                                                    <li>Track your tests/interviews</li>
                                                    <li>Store resumes and cover letters to use with test/interview applications </li>
                                                    <li>Set up alerts</li>
                                                    <li>Please <a class="can_forgotpassword_link" href="SignIn.aspx">sign in</a> here, if
                                                        you have an account with us</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="signup_right_bg">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="Signup_skillsUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="NewCandidate_SkillsDiv" class="signup_form_container_full_cus" runat="server">
                                    <div class="signup_outer_div_w_skill">
                                        <div class="signup_left_div_w_skill">
                                            <div class="form_container_tab">
                                                <div class="label_text">
                                                    <asp:Label runat="server" ID="Skill_labelID" SkinID="sknCanLabelText"> Skills</asp:Label>
                                                    <asp:Label runat="server" ID="Candidate_ExperienceYearLabel" Style="padding-left: 136px;"
                                                        SkinID="sknCanLabelText"> Number of years exp</asp:Label>
                                                    <asp:Label runat="server" ID="Candidate_Skill_LastUsedLabel" Style="padding-left: 22px;"
                                                        SkinID="sknCanLabelText"> Most recently used</asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="SignupCandidate_SkillText" runat="server" SkinID="sknCanTextBox"
                                                        MaxLength="20"> </asp:TextBox>
                                                    <ajaxToolkit:AutoCompleteExtender ID="SearchSubjectControl_SkillAutoCompleteExtender"
                                                        runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetSkillKeywords"
                                                        TargetControlID="SignupCandidate_SkillText" MinimumPrefixLength="1" CompletionListElementID="pnl"
                                                        EnableCaching="true" CompletionSetCount="12">
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                    <asp:DropDownList ID="Candidate_ExperienceYearDropDownList" runat="server" SkinID="sknDropdown"
                                                        Height="26px" Width="140px">
                                                        <asp:ListItem>&lt;1</asp:ListItem>
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                        <asp:ListItem>&gt;10</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="Candidate_Skill_LastUsedDropDownList" runat="server" SkinID="sknDropdown"
                                                        Height="26px" Width="140px">
                                                    </asp:DropDownList>
                                                    <asp:Button ID="SignupCandidate_AddSkillButton" CssClass="add_btn" runat="server"
                                                        OnClick="SignupCandidate_AddSkillButton_Click" ToolTip="Click here to add skill" />
                                                    <asp:Panel ID="pnl" runat="server">
                                                    </asp:Panel>
                                                </div>
                                                <div class="form_container_tab_right_cust">
                                                </div>
                                                <div>
                                                    <asp:Label ID="NewSkill_titleLabel" runat="server" CssClass="edit_profile_delete_resume_warning_label"
                                                        Text="You have entered a skill not present in our dictionary. Do you wish to add this skill?"
                                                        Visible="true" />
                                                    <asp:ImageButton ID="Signup_newSkillYesImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_activation_request.gif"
                                                        Style="padding-left: 15px;" ToolTip="Click here to add the new skill" OnClick="Signup_newSkillYesImageButton_Click" />
                                                    <asp:ImageButton ID="Signup_newSkillNolImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_delete.gif"
                                                        Style="padding-left: 15px; padding-right: 15px" ToolTip="Click here to clear the new skill"
                                                        OnClick="Signup_newSkillNolImageButton_Click" />
                                                </div>
                                            </div>
                                            <div class="form_container_grid">
                                                <div class="form_container_grid" style="overflow: auto; height: 140px">
                                                    <asp:UpdatePanel ID="Signup_CandidateSkillUpdatePanel" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="SignupCandidate_GridViewCandidateSkills" runat="server" AutoGenerateColumns="False"
                                                                EnableModelValidation="True" Width="340px" OnRowDataBound="SignupCandidate_GridViewCandidateSkills_RowDataBound">
                                                                <HeaderStyle Font-Bold="true" Font-Size="12px" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="SlNo" HeaderText="SNo" />
                                                                    <asp:BoundField DataField="Skill" HeaderText="Skill" />
                                                                    <asp:BoundField DataField="ExperienceYears" HeaderText="Exp (In Years)" />
                                                                    <asp:BoundField DataField="LastUsed" HeaderText="Last Used (Year)" />
                                                                    <%--<asp:CommandField ButtonType="Image" DeleteImageUrl="~/Images/delete.png" ShowDeleteButton="True" />--%>
                                                                    <asp:TemplateField HeaderStyle-Width="130px">
                                                                        <ItemTemplate>
                                                                            <div>
                                                                                <div style="float: left">
                                                                                    <asp:ImageButton ID="SignupCandidate_deleteSkillImageButton" runat="server" ImageUrl="~/Images/delete.png"
                                                                                        ToolTip="Click here to delete the skill" OnCommand="SignupCandidate_GridViewCandidateSkills_Command" />
                                                                                </div>
                                                                                <div style="float: right">
                                                                                    <%--<asp:ImageButton ID="SignupCandidate_searchTestImageButton" runat="server"  ImageUrl ="~/App_Themes/DefaultTheme/Images/talent_search_icon.png"
                                                                                        ToolTip="Search for tests" OnCommand="SignupCandidate_GridViewCandidateSkills_Command" />--%>
                                                                                    <asp:Button ID="SignupCandidate_searchTestButton" Text="Search For Tests" Width="110px"
                                                                                        runat="server" ToolTip="Search for tests" SkinID="sknButtonBlue" OnCommand="SignupCandidate_GridViewCandidateSkills_Command" />
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="55px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <%-- <asp:HiddenField ID="SignupCandidate_SkillIDHiddenField" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SKILLID")%>' />--%>
                                                                            <asp:Label ID="SingupCandidate_testCountLabel" runat="server" Text="" Style="color: Red;
                                                                                font-style: italic; font-size: 10px"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="SignupCandidate_AddSkillButton" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <div class="form_container_tab_cust">
                                                <div class="form_container_left_cust_w">
                                                    <asp:Label runat="server" ID="Label1">Would you like to upload your resume now?</asp:Label>
                                                </div>
                                            </div>
                                            <div class="form_container_tab_cust">
                                                <div class="form_container_left_signin">
                                                    <asp:Button ID="SignupCandidate_ResumeUploadYesButton" SkinID="sknButtonOrange" runat="server"
                                                        Text="Yes" OnClick="Signupcandidate_tabChange_Click" ToolTip="Click here to upload resume"
                                                        Height="28px" Width="75px" />
                                                </div>
                                                <div class="form_container_left_signin">
                                                    <asp:Button ID="SignupCandidate_ResumeUploadNoButton" CssClass="can_large_btn_bg"
                                                        runat="server" Text="Remind me in 2 weeks" OnClick="SignupCandidate_toLandingPage_Click"
                                                        ToolTip="Click here to home page" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="signup_right_div_w_skill">
                                            <%--   <div class="signup_left_bg_skill">
                                                &nbsp;
                                            </div>
                                            <div class="signup_middle_bg_skill">
                                                <div class="box_content_text_skil">
                                                    An ability and capacity acquired through deliberate, systematic, and sustained effort
                                                    to smoothly and adaptively carryout complex activities or job functions involving
                                                    ideas (cognitive skills), things (technical skills), and/or people (interpersonal
                                                    skills)
                                                </div>
                                            </div>
                                            <div class="signup_right_bg_skill">
                                                &nbsp;
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="empty_height">
                                        &nbsp;
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="NewCandidate_WelcomeContinueButton" />
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_ResumeUploadNoButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="Signup_resumeUploadUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="NewCandidate_ResumeDiv" class="signup_form_container_full_cus" runat="server">
                                    <div class="signup_outer_div_w_resume">
                                        <div class="signup_left_div_w_resume">
                                            <div class="signup_resume_message">
                                                <asp:Label runat="server" ID="Signup_resumeMessageLabel" Text="Uploading a resume will help your profile get noticed better"></asp:Label>
                                            </div>
                                            <div class="form_container_tab_cust">
                                                <div class="form_container_left" style="padding-top: 15px">
                                                    <asp:Label ID="SignupCandidate_profileNameLabel" runat="server">Profile Name<span>*</span></asp:Label>
                                                </div>
                                                <div class="form_container_left">
                                                    <asp:TextBox ID="SignupCandidate_profileNameTextBox" runat="server" SkinID="sknCanTextBox"
                                                        MaxLength="100" Style="width: 180px"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form_container_tab">
                                                <div class="form_container_left_WID">
                                                    <asp:Label runat="server" ID="SignupCandidate_ResumeUploadLabel" CssClass="cand_upload_resume"
                                                        Text="Select resume"> </asp:Label>
                                                </div>
                                            </div>
                                            <div class="form_container_tab_cust">
                                                <div class="form_container_left">
                                                    <ajaxToolkit:AsyncFileUpload ID="ResumeUploader_fileUpload" runat="server" Width="300"
                                                        Height="24px" ThrobberID="Throbber" FailedValidation="False" PersistFile="false"
                                                        SkinID="sknCanTextBox" OnUploadedComplete="ResumeEditor_fileUpload_UploadedComplete"
                                                        ClientIDMode="AutoID" OnClientUploadError="uploadError" OnClientUploadComplete="OnClientAsyncResumeFileUploadComplete" />
                                                </div>
                                            </div>
                                            <div class="form_container_tab_cust">
                                                <div class="form_container_left">
                                                    <asp:Button ID="SignupCandidate_uploadResumeButton" SkinID="sknButtonOrange" runat="server"
                                                        Text="Upload now" OnClientClick="javascript:return ShowResumeParserMessagePopup()" OnClick="SignupCandidate_uploadResumeButton_Click" ToolTip="Click here to upload resume"
                                                        Width="135px" Height="28px" />
                                                    <asp:HiddenField runat="server" ID="ResumeEditor_candidateResumeFileNameHiddenField">
                                                    </asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="ResumeEditor_candidateResumeFileMimeTypeHiddenField">
                                                    </asp:HiddenField>
                                                </div>
                                                <div class="form_container_mid">
                                                </div>
                                                <div class="form_container_left">
                                                    <asp:Button ID="SignupCandidate_ResumeUploadLaterButton" CssClass="can_large_btn_bg"
                                                        runat="server" Text="Remind me in 2 weeks" OnClick="SignupCandidate_toLandingPage_Click"
                                                        ToolTip="Click here to home page" />
                                                </div>
                                            </div>
                                        </div>
                                        <%-- <div class="signup_right_div_w_resume">
                                            <div class="signup_right_div_w_skill">
                                                <div class="signup_left_bg_skill">
                                                    &nbsp;
                                                </div>
                                                <div class="signup_middle_bg_skill">
                                                </div>
                                                <div class="signup_right_bg_skill">
                                                    &nbsp;
                                                </div>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="empty_height">
                                        &nbsp;
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="SignupCandidate_uploadResumeButton" />
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_ResumeUploadYesButton" />
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_ResumeUploadLaterButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="Signup_intimationScreenUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="Signup_intimationScreen" runat="server" class="signup_form_container_full">
                                    <%--<div class="signup_form_container_full_cus">
                                        <asp:Label ID="Signup_intimationScreenRegisterLabel" runat="server" CssClass="signup_box_register_head_text"
                                            Text="Register for ForteHCM"></asp:Label>
                                    </div>--%>
                                    <div class="signup_form_container_full_cus">
                                        <asp:Label ID="Signup_intimationCandidateLabel" runat="server" CssClass="signup_box_welcome_head_text"
                                            Text="Welcome to ForteHCM! "></asp:Label>
                                    </div>
                                    <div class="signup_form_container_full_cus">
                                        <div class="signup_box_normal_head_text">
                                            You are just a few quick steps away from using your ForteHCM account.<br />
                                            Please check your email account for an email from us, and follow the instructions
                                            to activate your account.
                                        </div>
                                    </div>
                                    <div class="signup_form_container_full_cus">
                                        <asp:Button ID="Signup_intimationScreenCloseButton" CssClass="can_large_btn_bg" runat="server"
                                            Text="Close" ToolTip="Click here to close the window" PostBackUrl="~/SignIn.aspx" />
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_LoginButton" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="Signup_welcomeUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="NewCandidate_WelcomeTag" class="signup_form_container_full" runat="server">
                                    <div class="signup_form_container_full_cus" style="display: none">
                                        <asp:Label ID="NewCandidate_WelcomeLabel" runat="server" CssClass="signup_box_register_head_text"
                                            Text="Register for ForteHCM"></asp:Label>
                                    </div>
                                    <div class="signup_form_container_full_cus">
                                        <asp:Label ID="NewCandidate_WelcomeCandidateNameLabel" runat="server" CssClass="signup_box_welcome_head_text"
                                            Text="Welcome to ForteHCM, "></asp:Label>
                                    </div>
                                    <div class="signup_form_container_full_cus">
                                        <div class="signup_box_normal_head_text">
                                            <br />
                                            On the following screen, we welcome you to enter your skills to help us recommend
                                            tests that are best suited for you.<br />
                                            <br />
                                        </div>
                                    </div>
                                    <%-- <div class="signup_form_container_full_cus">
                                        <div class="signup_box_normal_head_text">
                                            Your ForteHCM account is all set!<br />
                                            Any Time you can manage your account.
                                        </div>
                                    </div>
                                    <div class="signup_form_container_full_cus">
                                        <div class="signup_box_normal_head_text">
                                            Now that you're here,
                                        </div>
                                        <div class="signup_box_green_head_text">
                                            get your resume up!
                                        </div>
                                        <div class="signup_box_normal_head_text">
                                            it's easy.</div>
                                    </div>
                                    <div class="signup_form_container_full_cus">
                                        <div class="signup_box_normal_text">
                                            Our automated resume builder takes your existing resume and pre-populates your profile
                                            to get you started
                                        </div>
                                        <div class="signup_box_normal_text">
                                            <ul>
                                                <li>Once your resume is posted, you can join the ForeHCM Talent Network and network
                                                    with recruiting decision makers</li>
                                                <li>Worried about privacy?Dont' be. You can post your resume confidentially. Would you
                                                    like to add resume and skills?</li>
                                            </ul>
                                        </div>
                                    </div>--%>
                                    <div class="signup_form_container_full_cus">
                                        <asp:Button ID="NewCandidate_WelcomeContinueButton" CssClass="can_large_btn_bg" runat="server"
                                            Text="Continue" ToolTip="Click here to go skill page" OnClick="NewCandidate_WelcomeContinueButton_Click" />
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="NewCandidate_WelcomeLaterLinkButton" runat="server" Text="Later"
                                            OnClick="NewCandidate_WelcomeLaterLinkButton_Click" Style="font-size: 14px"></asp:LinkButton>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="SignupCandidate_LoginButton" />
                                <asp:AsyncPostBackTrigger ControlID="OpenID_YahooButton" EventName="Command" />
                                <asp:AsyncPostBackTrigger ControlID="OpenID_GmailButton" EventName="Command" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <asp:UpdatePanel ID="Signup_resumeParserMessageUpdatePanel" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div style="display: none">
                        <asp:Button ID="Signup_resumeParserMessageHiddenButton" runat="server" />
                    </div>
                    <asp:Panel ID="Signup_resumeParserMessagePanel" runat="server" Style="display: none"
                        CssClass="popupcontrol_test_recommendation_change_test_status">
                        <div id="Signup_resumeParserMessageDiv" style="display: block; height: 190px;
                            width: 580px;" class="popupcontrol_confirm">
                            <div style="width: 548px; padding-left: 14px; float: left; padding-top: 14px">
                                <div style="width: 524px; float: left; height: 134px; padding-left: 14px; padding-top: 14px; padding-right: 14px; padding-bottom: 14px;" 
                                    class="resume_parser_status_inner_bg" >
                                     <div style="text-align: center">
                                        <asp:Label ID="Signup_resumeParserMessageTitleLabel" runat="server" Text="Parsing On Process" CssClass="resume_parser_status_message_title_label"></asp:Label>
                                    </div>
                                     <div style="height: 10px">
                                     </div>
                                    <div style="text-align: center">
                                        <asp:Label ID="Signup_resumeParserMessageLabel" runat="server" Text="Your resume is under parsing and will take few seconds to complete. Please wait..." CssClass="resume_parser_status_message_label"></asp:Label>
                                    </div>
                                    <div style="height: 10px">
                                    </div>
                                    <div style="text-align: center;display:block" runat="server" id="Signup_resumeParserRotatingImageDiv">
                                         <asp:Image ID="Signup_resumeParserRotatingImage" ImageUrl="~/App_Themes/DefaultTheme/Images/resume_parsing_process.gif" runat="server"
                                            ToolTip="Please wait ..." />
                                    </div>
                                    <div style="text-align: center">
                                        <asp:Button ID="Signup_resumeParserCloseButton" runat="server" Text="Close" SkinID="sknButtonBlue" Visible="false" Height="26px" Width="120px" ToolTip="Click here to close"/>
                                        <asp:Button ID="Signup_resumeParserApproveButton" runat="server" Text="Review & Approve" SkinID="sknButtonOrange" Visible="false" Height="26px" Width="120px" PostBackUrl="~/ResumeRepository/MyResume.aspx" ToolTip="Click here to review & approve your resume"/>&nbsp;
                                        <asp:Button ID="Signup_resumeParserDashboardButton" runat="server" Text="Later" SkinID="sknButtonBlue" Visible="false" Height="26px" Width="120px" PostBackUrl="~/Dashboard.aspx" ToolTip="Click here to go to dashboard"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="Signup_resumeParserMessageModalPopupExtender"
                        runat="server" PopupControlID="Signup_resumeParserMessagePanel" TargetControlID="Signup_resumeParserMessageHiddenButton"
                        BackgroundCssClass="modalBackground">
                    </ajaxToolkit:ModalPopupExtender>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
