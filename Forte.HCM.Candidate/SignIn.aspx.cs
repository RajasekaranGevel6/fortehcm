﻿using System;
using System.Web;
using System.Data;
using System.Text;
using System.Web.Security;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Common.DL;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
 
using Resources;

#region OpenID Library

using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;


#endregion

namespace Forte.HCM.Candidate
{
    public partial class  SignIn : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try 
            {
                // Set browser title.
                Master.SetPageCaption("Sign In");

                // Clear message and hide labels.
                ClearMessages();
                
                if (!IsPostBack)
                {  
                    ClearControls_ExistingCandidate();
                    if (Session["SignupOPENID"] == null)
                        GetOpenIDInformation(); 
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
            }
        } 

        private bool IsValidDataExistingCandidate(TextBox TextBoxControl)
        {
            bool value = true;

            if (TextBoxControl.Text.Trim().Length == 0)
            {
                value = false;
            }

            if (TextBoxControl.Text.Trim().Length == 0)
            {
                value = false;
            }  

            return value;
        }
        /// <summary>
        /// Set empty values for existing candidate controls
        /// </summary>
        private void ClearControls_ExistingCandidate()
        {
            SignIn_userNameTextBox.Text = "";
            SignIn_passwordTextBox.Text = "";
            SignIn_userNameTextBox.Focus();
        }
        protected void SignIn_goImageButton_Click(object sender, EventArgs e)
        {
            try 
            {
                if (!IsValidDataExistingCandidate(SignIn_userNameTextBox))
                {
                    base.ShowMessage(SignIn_errorMessageLabel,
                     "Enter username. !");
                    SignIn_userNameTextBox.Focus();
                    return;
                }

                if (!IsValidDataExistingCandidate(SignIn_passwordTextBox))
                {
                    base.ShowMessage(SignIn_errorMessageLabel,
                     "Enter password. !");
                    SignIn_passwordTextBox.Focus();
                    return;
                }

                TryLogin(SignIn_userNameTextBox.Text.Trim(),
                    SignIn_passwordTextBox.Text.Trim(),"NORMAL");

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
            }
        }
        protected override bool IsValidData()
        {
            bool value = true;  
            return value;
        }
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        private void TryLogin(string userName,string password, string userType)
        { 
            try
            {
                int tenantID = 0;

                // Get user details along with roles and activity status.
                UserDetail userDetail = null;
                if (userType == "OPENIDUSER")
                {
                    userDetail = new AuthenticationBLManager().GetAuthenticateCandidate
                        (userName, null, 0);
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["tkey"]))
                        tenantID = Convert.ToInt32(Request.QueryString["tkey"]);

                    userDetail = new AuthenticationBLManager().GetAuthenticateCandidate
                        (userName, new EncryptAndDecrypt().EncryptString(password), tenantID);
                }
                
                // Check if user is valid or not.
                if (Utility.IsNullOrEmpty(userDetail) || userDetail.UserID == 0)
                {
                    base.ShowMessage(SignIn_errorMessageLabel,
                   HCMResource.Login_InvalidUserLogin);                    
                    return; 
                }

                /*
                //Check with url redirection
                if (!Utility.IsNullOrEmpty(ConfigurationManager.AppSettings["SITE_URL"]))
                {
                    //User Requested URL
                    Uri requestedURL = HttpContext.Current.Request.Url;

                    //Candidate URL from config file
                    string siteURL = ConfigurationManager.AppSettings["SITE_URL"].ToString();
                    Uri redirectURL = new Uri(siteURL);

                    if (redirectURL.Host.ToString().ToLower() != requestedURL.Host.ToString().ToLower())
                    {
                        // Do not allow non-candidate users to access the application. 
                        base.ShowMessage(SignIn_errorMessageLabel,
                            "Invalid username or password or you dont have privileges to access the application");

                        // Nullify the user detail.
                        Session["USER_DETAIL"] = null;

                        return;
                    }
                }
                */
                if (!Utility.IsNullOrEmpty(Request.QueryString["c"]))
                {
                    //Checking candidate have online interview for current date & time
                    OnlineCandidateSessionDetail onlineInterviewDetail =
                        new CandidateBLManager().GetOnlineInterviewDetail(userDetail.UserID, 0, Request.QueryString["c"].ToString());

                    if (onlineInterviewDetail != null)
                    {
                        if (new Utility().GetOnlineInterviewAvailable(onlineInterviewDetail.InterviewDate,
                            onlineInterviewDetail.TimeSlotFrom, onlineInterviewDetail.TimeSlotTo))
                        {
                            /*Response.Redirect(ConfigurationManager.AppSettings["ONLINE_INTERVIEW_URL"].ToString() + "?candidateid=" + new EncryptAndDecrypt().EncryptString(onlineInterviewDetail.EmailId.Trim())
                                + "&userrole=C" + "&userid=" + new EncryptAndDecrypt().EncryptString(onlineInterviewDetail.EmailId.Trim()) + "&interviewkey=" + onlineInterviewDetail.CandidateSessionID
                                + "&chatroom=" + onlineInterviewDetail.ChatRoomName, false);*/
                            Response.Redirect(ConfigurationManager.AppSettings["ONLINE_INTERVIEW_URL"].ToString() + "?candidateid=" + onlineInterviewDetail.CandidateID.ToString().Trim()
                                + "&userrole=C" + "&userid=" + onlineInterviewDetail.CandidateID.ToString().Trim() + "&interviewkey=" + onlineInterviewDetail.CandidateSessionID
                                + "&chatroom=" + onlineInterviewDetail.ChatRoomName, false);
                            return;
                        }
                    }
                } 

                // Check if roles available and having a candidate role.
                if (userDetail.Roles != null && userDetail.Roles.Exists(item => item == UserRole.Candidate))
                {
                    // Keep the user detail in session.
                    Session["USER_DETAIL"] = userDetail;
                    Session["TENANT_ID"] = tenantID;

                    // Set authentication cookie.
                    FormsAuthentication.SetAuthCookie(userName, true);

                    // Try to add the cookie information.
                    if (SignIn_rememberMeCheckBox.Checked)
                    {
                        try
                        {
                            HttpCookie cookie = Request.Cookies["FORTEHCM_CANDIDATE_DETAILS"];
                            if (cookie == null)
                            {
                                cookie = new HttpCookie("FORTEHCM_CANDIDATE_DETAILS");
                            }

                            cookie["FHCM_USER_NAME"] = userName;
                            cookie["FHCM_PASSWORD"] = password;

                            cookie.Expires = DateTime.Now.AddMonths
                                (Convert.ToInt32(ConfigurationManager.AppSettings["REMEMBER_ME_MONTHS"]));
                            Response.AppendCookie(cookie);
                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                        }
                    }

                    if (Request.Params["ReturnUrl"] != null && userDetail.IsActivityExists==true)
                    {
                        FormsAuthentication.RedirectFromLoginPage(userName, false);
                    }
                    else
                    {
                        // Redirect to candidate home page.
                        Response.Redirect("Dashboard.aspx", false);
                    }
                    
                }
                else
                {
                    // Do not allow non-candidate users to access the application. 
                    base.ShowMessage(SignIn_errorMessageLabel,
                        "Invalid username or password or you dont have privileges to access the application");

                    // Nullify the user detail.
                    Session["USER_DETAIL"] = null;

                    return;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
            }
        }

        protected void SignIn_openID_Click(object src, CommandEventArgs e)
        {
            try
            {
                string commandArg = e.CommandArgument.ToString();
                OpenIdRelyingParty openIDURL = new OpenIdRelyingParty();

                var uriBuilder = new UriBuilder(Request.Url) { Query = "" };
                var request = openIDURL.CreateRequest(commandArg, uriBuilder.Uri, uriBuilder.Uri); 

                if (request.ClaimedIdentifier == null)
                {
                    Identifier id;
                    if (Identifier.TryParse(commandArg, out id))
                    {
                        try
                        {
                            var fetchUserDetail = new FetchRequest();
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Name.FullName);
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Name.First);
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Name.Middle);
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Name.Last);
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Person.Gender);
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Contact.Email);
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Contact.Phone.Home);
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Contact.Phone.Mobile);
                            fetchUserDetail.Attributes.AddRequired(WellKnownAttributes.Contact.Phone.Fax);

                            request.AddExtension(fetchUserDetail);
                        }
                        catch { }
                    }
                }

                request.RedirectToProvider();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
            }
        }

        protected void GetOpenIDInformation()
        {
            OpenIdRelyingParty openIDReplyParty = new OpenIdRelyingParty();
            try
            { 
                var responseFromOpenID = openIDReplyParty.GetResponse();
                if (responseFromOpenID != null)
                {

                    IDbTransaction transaction =
                      new TransactionManager().Transaction;

                    switch (responseFromOpenID.Status)
                    {
                        case AuthenticationStatus.Authenticated:
                            var fetchResponse = responseFromOpenID.GetExtension<FetchResponse>();

                            CandidateInformation candidateInfo = null;

                            string fullName = null;

                            if (fetchResponse != null)
                            {
                                candidateInfo = new CandidateInformation();
                                candidateInfo.caiFirstName = fetchResponse.GetAttributeValue(WellKnownAttributes.Name.First);
                                candidateInfo.caiLastName = fetchResponse.GetAttributeValue(WellKnownAttributes.Name.Last);
                                candidateInfo.caiEmail = fetchResponse.GetAttributeValue(WellKnownAttributes.Contact.Email);
                                candidateInfo.UserName = fetchResponse.GetAttributeValue(WellKnownAttributes.Contact.Email);

                                if (candidateInfo.caiFirstName == null)
                                    fullName = fetchResponse.GetAttributeValue(WellKnownAttributes.Name.FullName);

                                if (fetchResponse.GetAttributeValue(WellKnownAttributes.Person.Gender) == "Male"
                                    || fetchResponse.GetAttributeValue(WellKnownAttributes.Person.Gender) == "M")
                                    candidateInfo.caiGender = 1;
                                else if (fetchResponse.GetAttributeValue(WellKnownAttributes.Person.Gender) == "Female"
                                    || fetchResponse.GetAttributeValue(WellKnownAttributes.Person.Gender) == "F")
                                    candidateInfo.caiGender = 2;

                                candidateInfo.caiHomePhone = fetchResponse.GetAttributeValue(WellKnownAttributes.Contact.Phone.Home);
                                candidateInfo.caiEmergencyContactNo = fetchResponse.GetAttributeValue(WellKnownAttributes.Contact.Phone.Mobile);
                                candidateInfo.caiFax = fetchResponse.GetAttributeValue(WellKnownAttributes.Contact.Phone.Fax);

                                if (fullName != null)
                                {
                                    string[] candidateNameSplit = fullName.Split(' ');
                                    candidateInfo.caiFirstName = candidateNameSplit[0].ToString();
                                    candidateInfo.caiLastName = candidateNameSplit[1].ToString();
                                }

                                candidateInfo.caiIsActive = 'Y';
                                candidateInfo.caiOpenEmailID = "Y";

                                candidateInfo.caiType = "CT_MANUAL";
                                candidateInfo.caiEmployee = 'N';
                                candidateInfo.caiLimitedAccess = false;
                                candidateInfo.caiCreatedBy = 1;
                            }

                            if (candidateInfo == null) return;

                            if (!new CandidateBLManager().VerifyOpenEmailID(candidateInfo.caiEmail.Trim()))
                            {
                                int candidateID = new ResumeRepositoryBLManager().
                                    InsertSignupCandidate(candidateInfo, 1, 1, transaction);

                                if (candidateID > 0)
                                {
                                    candidateInfo.caiID = candidateID;
                                    SaveUserName(candidateInfo, transaction);
                                }
                                transaction.Commit();
                            }
                            TryLogin(candidateInfo.UserName, null, "OPENIDUSER");
                            break;
                        case AuthenticationStatus.Canceled:
                            base.ShowMessage(SignIn_errorMessageLabel,
                                 "Authentication is cancelled");
                            break;
                        case AuthenticationStatus.Failed:
                            base.ShowMessage(SignIn_errorMessageLabel,
                                 "Authentication is failed");
                            break;
                    }
                }
                 
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel, ex.Message);
            }
            finally { openIDReplyParty = null; }
        }

        /// <summary>
        /// Saves the name of the user.
        /// </summary>
        private void SaveUserName(CandidateInformation candidateInformation,
            IDbTransaction transaction)
        {   
            string confirmationCode = GetConfirmationCode();

            int result = new ResumeRepositoryBLManager().
                InsertSignupUsers(1, candidateInformation,
               1, confirmationCode, transaction); 
        }

        /// <summary>
        /// A Method that generates a random string and used 
        /// the encrypted string as confirmation code for a user
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// Direct signup user information storing in session
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        private void SetDirectSignupUserInfo(string userName, string password)
        {
            UserDetail userDetail = null;
            userDetail = new AuthenticationBLManager().GetAuthenticateCandidate
                          (userName, password, 0);
            Session["USER_DETAIL"] = userDetail;

            FormsAuthentication.SetAuthCookie(userName, true);

            try
            {
                HttpCookie cookie = Request.Cookies["FORTEHCM_CANDIDATE_DETAILS"];
                if (cookie == null)
                {
                    cookie = new HttpCookie("FORTEHCM_CANDIDATE_DETAILS");
                }

                cookie["FHCM_USER_NAME"] = userName;
                cookie["FHCM_PASSWORD"] = password;

                cookie.Expires = DateTime.Now.AddMonths
                    (Convert.ToInt32(ConfigurationManager.AppSettings["REMEMBER_ME_MONTHS"]));
                Response.AppendCookie(cookie);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
            }
        }
       
        private bool IsValidEmailAddress(string strUserEmailId)
        {
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$",
                RegexOptions.Compiled);
            return regex.IsMatch(strUserEmailId);
        }
       
        private string CheckObjectValue(object candidateinfo)
        {
            if (candidateinfo == null) return " ";
            else
                return Convert.ToString(candidateinfo);
        }

        protected void NewCandidate_SignUpLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/signup.aspx", false);
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            SignIn_errorMessageLabel.Text = string.Empty;
            SignIn_errorMessageLabel.Visible = false;             
        }
         
    }
}