﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ActivityHome.aspx.cs
// This page allows the candidate to know the details of their tests.
// i.e. Pending, Completed, and Expired test details. Also, they can 
// send the request to the creator to retake a test, activate a tests.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;

#endregion Directives                                                          

namespace Forte.HCM.UI
{
    public partial class ActivityHome : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["USER_DETAIL"] == null)
                {
                    Response.Redirect("~/SignIn.aspx", false);
                }
                else
                {
                    Response.Redirect("~/Dashboard.aspx", false);
                }

                // Set page caption
                Master.SetPageCaption(Resources.HCMResource.ActivityHome_Title);

                // Assign test activity page navigator event.
                ActivityHome_testActivityPageNavigator.PageNumberClick += new 
                    ActivityPageNavigator.PageNumberClickEventHandler
                    (ActivityHome_testActivityPageNavigator_PageNumberClick);

                // Assign interview activity page navigator event.
                ActivityHome_interviewActivityPageNavigator.PageNumberClick += new
                    ActivityPageNavigator.PageNumberClickEventHandler
                    (ActivityHome_interviewActivityPageNavigator_PageNumberClick);

                // Show/hide menu based on user type (limited or not).
                Master.ShowMenu = !base.isLimited;

                if (!IsPostBack)
                {
                    // Clear session values.
                    Session["PENDING_TEST_ACTIVITIES"] = null;
                    Session["PENDING_INTERVEIW_ACTIVITIES"] = null;

                    // Add hander for 'email recruiter' image and link button.
                    ActivityHome_emailRecruiterButton.Attributes.Add("OnClick",
                        "javascript:return EmailRecruiterFromHome('" + base.userID + "','REC')");

                    // Load values.
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ActivityHome_topErrorMessageLabel,
                    ActivityHome_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the activity number is 
        /// clicked in the test activity paging control.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void ActivityHome_testActivityPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Check if session holds data.
                if (Session["PENDING_TEST_ACTIVITIES"] == null)
                    return;

                // Get pending activities from session.
                List<CandidateTestSessionDetail> activities = 
                    Session["PENDING_TEST_ACTIVITIES"] as List<CandidateTestSessionDetail>;

                // Show the n'th activity as the default activity.
                if (activities != null && (e.PageNumber - 1) < activities.Count)
                    ActivityHome_testActivityDetailControl.DataSource = activities[e.PageNumber - 1];
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ActivityHome_topErrorMessageLabel,
                    ActivityHome_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the activity number is 
        /// clicked in the interview activity paging control.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        private void ActivityHome_interviewActivityPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                // Check if session holds data.
                if (Session["PENDING_INTERVIEW_ACTIVITIES"] == null)
                    return;

                // Get pending activities from session.
                List<CandidateInterviewSessionDetail> activities =
                    Session["PENDING_INTERVIEW_ACTIVITIES"] as List<CandidateInterviewSessionDetail>;

                // Show the n'th activity as the default activity.
                if (activities != null && (e.PageNumber - 1) < activities.Count)
                    ActivityHome_interviewActivityDetailControl.DataSource = activities[e.PageNumber - 1];
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ActivityHome_topErrorMessageLabel,
                    ActivityHome_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void ActivityHome_startTestButton_OkClick(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Method that retrieves the show results to candidate status. This
        /// helps to show or hide the show results link icon in the completed 
        /// test grid section.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the show results to candidate status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of show results
        /// to candidate status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for show results to candidates are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsShowResults(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        private void LoadPendingActivities()
        {
            // Load pending tests.
            LoadPendingTests();

            // Load pending interviews.
            LoadPendingInterviews();
        }

        /// <summary>
        /// Method that loads the pending tests.
        /// </summary>
        private void LoadPendingTests()
        {
            // Get pending activities.
            int totalRecords = 0;
            List<CandidateTestSessionDetail> activities = new CandidateBLManager().
                GetPendingTestActivities(base.userID, 1, 1, out totalRecords);

            // Keep the pending activities in session.
            Session["PENDING_TEST_ACTIVITIES"] = activities;

            if (activities == null || activities.Count == 0)
                return;

            // Set paging.
            ActivityHome_testActivityPageNavigator.TotalRecords = activities.Count;
            ActivityHome_testActivityPageNavigator.PageSize = 1;

            // Show the 0'th activity as the default activity.
            ActivityHome_testActivityDetailControl.DataSource = activities[0];
        }

        /// <summary>
        /// Method that loads the pending interviews.
        /// </summary>
        private void LoadPendingInterviews()
        {
            // Get pending activities.
            List<CandidateInterviewSessionDetail> activities = new CandidateBLManager().
                GetPendingInterviewActivities(base.userID);

            // Keep the pending activities in session.
            Session["PENDING_INTERVIEW_ACTIVITIES"] = activities;

            if (activities == null || activities.Count == 0)
                return;

            // Set paging.
            ActivityHome_interviewActivityPageNavigator.TotalRecords = activities.Count;
            ActivityHome_interviewActivityPageNavigator.PageSize = 1;

            // Show the 0'th activity as the default activity.
            ActivityHome_interviewActivityDetailControl.DataSource = activities[0];
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            CandidateActivitySummary summary = new CandidateBLManager().
                GetCandidateActivitySummary(base.userID);

            ActivityHome_fullNameValueLabel.Text = summary.CandidateName;

            if (summary.UserSince != DateTime.MinValue)
                ActivityHome_userSinceValueLabel.Text = summary.UserSince.ToString("MMM dd yyyy");

            ActivityHome_recruiterValueLabel.Text = summary.RecruiterName;
            ActivityHome_pendingActivitiesValueLabel.Text = summary.ActivitiesCount.ToString();

            // Load pending activities.
            LoadPendingActivities();
        }

        #endregion Protected Overridden Methods                                
    }
}