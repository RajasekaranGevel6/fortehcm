﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="SignIn.aspx.cs" Inherits="Forte.HCM.Candidate.SignIn" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="SignIn_headContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="SignIn_mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript"> 
     
        function OpenAgreementWindow() {
            window.open("./General/Agreement.aspx", "Agreement", "")
        }

        function OpenOfferingsWindow() 
        {
            window.open("./General/Offerings.aspx?parentpage=C_SIGNIN", "Offerings", "")
        } 

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div class="common_padding">
                    <asp:UpdatePanel ID="SignIn_errorMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                                <asp:Label ID="SignIn_errorMessageLabel" runat="server" CssClass="Mandatory"></asp:Label>
                        </ContentTemplate>
                        <Triggers > 
                            <asp:AsyncPostBackTrigger ControlID ="SignIn_goImageButton"/>
                        </Triggers>
                    </asp:UpdatePanel> 
                </div>
                <div class="signin_outer_div">
                    <div class="signin_left_bg">
                        &nbsp;
                    </div>
                    <div class="signin_middle_bg">
                        <div>
                            <div class="signin_left_panel">
                                <div class="signin_signup_ex_user_line_bottom">
                                    <asp:Label runat="server" ID="SignIn_titleLabel" CssClass="signin_ex_user_head" Text="Sign In"> </asp:Label>
                                </div>
                                <div class="signin_form_container">
                                    <div class="signin_form_container_left">
                                        <asp:Label runat="server" ID="SignIn_userNameLabel" CssClass="signin_ex_user">User Name<span>*</span></asp:Label>
                                    </div>
                                    <div class="signin_form_container_right">
                                        <asp:TextBox ID="SignIn_userNameTextBox" SkinID="sknCanTextBox" runat="server"
                                            ToolTip="Enter user name"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="signin_form_container">
                                    <div class="signin_form_container_left">
                                        <asp:Label runat="server" ID="SignIn_passwordLabel" CssClass="signin_ex_user">Password<span>*</span></asp:Label>
                                    </div>
                                    <div class="signin_form_container_right">
                                        <asp:TextBox ID="SignIn_passwordTextBox" TextMode="Password" SkinID="sknCanTextBox"
                                            runat="server" ToolTip="Enter password"></asp:TextBox>
                                    </div>
                                    <div class="signin_form_container_right_cust">
                                        <asp:ImageButton ID="SignIn_goImageButton" runat="server" SkinID="sknLoginGoImageButton"
                                            ToolTip="Click here to login" OnClick="SignIn_goImageButton_Click" />
                                    </div>
                                </div>
                                <div class="signin_form_container">
                                    <div class="form_container_left_cust">
                                        <asp:CheckBox ID="SignIn_rememberMeCheckBox" runat="server" CssClass="signin_check_box_text"
                                            TextAlign="Right" Text="Remember me" />
                                        <asp:LinkButton ID="SignIn_forgotPasswordLinkButton" runat="server" CssClass="signin_forgotpassword_link"
                                            Style="padding-left: 40px" PostBackUrl="~/General/ForgotPassword.aspx">Forgot Password?</asp:LinkButton>
                                    </div>
                                </div> 
                            </div>
                            <div class="signin_divider" style="display:none">
                                &nbsp;
                            </div>
                            <div id="title" class="signin_right_panel" style="display:none">
                                <div id="NewCandidate_SignUpTag" class="signin_signup_ex_user_line_bottom">
                                    <asp:Label runat="server" ID="SignIn_openIDLabel" CssClass="signin_openuser_head" Text="Sign In using OpenID"> </asp:Label>
                                </div>
                                <div id="NewCandidate_SignUp">
                                    <div class="form_container" style="display: none">
                                        <div class="form_container_left" id="ErrorMessage">
                                        </div>
                                    </div>
                                    <div class="form_container">
                                        <div class="signin_container_left_cust_w">
                                            <div id="Div1" class="signin_form_lbl">
                                                Its fast and easy to login using your Yahoo, GMail, LinkedIn & Facebook accounts.<br />
                                                All test and interview search activity and personal information is kept private.<br />
                                                <br />
                                            </div>
                                            <div id="SigninCandidate" class="signin_open_user_id" style="display: block;"> 
                                                <div class="form_container">
                                                    <asp:UpdatePanel ID="SignIn_openIDUpdatePanel" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate >  
                                                            <div class="icons_padding" style="padding-left:10px">
                                                                <asp:Button ID="SignIn_yahooButton" runat="server" CssClass="yahoo_icon" CommandArgument="https://me.yahoo.com/"
                                                                    OnCommand="SignIn_openID_Click" ToolTip="Sign in yahoo ID" style="cursor:pointer;" />
                                                            </div>
                                                            <div class="icons_padding" style="padding-left:10px">
                                                                <asp:Button ID="SignIn_gmailButton" runat="server" CssClass="gmail_icon" CommandArgument="https://www.google.com/accounts/o8/id"
                                                                    OnCommand="SignIn_openID_Click" ToolTip="Sign in gmail ID"  style="cursor:pointer;"/>
                                                            </div>
                                                            <div class="icons_padding" style="display:none">
                                                                <asp:Button ID="SignIn_linkedINButton" runat="server" CssClass="linkedin_icon" CommandArgument="https://www.linkedin.com/uas/oauth/authorize?oauth_token=494dfcaf-7a88-4c34-aa7f-bc8c96172e3c"
                                                                    OnCommand="SignIn_openID_Click" ToolTip="Sign in linkedin ID" style="cursor:pointer;"  />
                                                            </div>
                                                            <div class="icons_padding" style="display:none">
                                                                <asp:Button ID="SignIn_faceBookButton" runat="server" CssClass="facebook_icon" ToolTip="Sign in Facebook ID" style="cursor:pointer;"/>
                                                            </div>
                                                      </ContentTemplate>
                                                     </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="signin_right_bg" style="display:none">
                        &nbsp;
                    </div>
                </div>
                <div class="signin_outer_empty_div" style="display:none">
                    <asp:UpdatePanel ID="SignIn_SignupUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>  
                            <div style="width:320px"> 
                                  <div style="float:left;padding-top:2px">
                                   <asp:Label ID="NewCandidate_SignupTitleLabel" runat="server" Text="" CssClass="signin_header_text" style="padding-left:5px">Not a member? <span style="display:none">Sign up today and get on ForteHCM!</span></asp:Label>
                                </div>
                                 <div style="width:150px;float:left">
                                    <asp:Button ID="NewCandidate_SignUpLinkButton" runat="server" Text="Sign Up Today" SkinID="sknButtonOrange"
                                   ToolTip="Click here to sign up" OnClick="NewCandidate_SignUpLinkButton_Click" Height="25px"  Width="120px"/>
                                </div>
                            </div>    
                        </ContentTemplate> 
                        <Triggers >
                            <asp:PostBackTrigger ControlID="NewCandidate_SignUpLinkButton" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
                <div class="signup_offerings_panel" style="display:none">
                    <asp:LinkButton ID="CandidateSignIn_demoLinkButton" runat="server" CssClass="signup_offerings_link"
                        OnClientClick="javascript:OpenOfferingsWindow();" Style="padding-left: 2px" Text="Offerings & Demo"
                        ToolTip="Click here to know more about offerings and demo on self admin test"></asp:LinkButton>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</asp:Content>
