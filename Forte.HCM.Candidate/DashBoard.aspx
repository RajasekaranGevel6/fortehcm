﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="DashBoard.aspx.cs" Inherits="Forte.HCM.Candidate.DashBoard" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="CandidateHome_ContentID" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function StartTest() {
            $find("<%= CandidateHome_startTestPopupExtender.ClientID  %>").show();
            return false;
        }
        function StartInterview() {
            $find("<%= CandidateHome_startInterviewPopupExtender.ClientID  %>").show();
            return false;
        }

        function SelfAdminDemo() {
            window.open("./General/SelfAdminDemo.aspx?parentpage=C_DASHBOARD", "SelfAdminDemo", "");
        }

        function ShowHideDeleteConfirmation() {
            if (document.getElementById('DashBoard_deleteConfirmationDiv').style.display == "block") {
                document.getElementById('DashBoard_deleteConfirmationDiv').style.display = "none";
            }
            else {
                document.getElementById('DashBoard_deleteConfirmationDiv').style.display = "block";
            }

            return false;
        }

        function showInterview(divid) {

            if (divid == 'OFFLINE') {
                document.getElementById("<%= PendingInterview_Div.ClientID %>").style.display = 'block';
                document.getElementById("<%= onlineInterviewSubDiv.ClientID %>").style.display = 'none';
            }
            else {
                document.getElementById("<%= PendingInterview_Div.ClientID %>").style.display = 'none';
                document.getElementById("<%= onlineInterviewSubDiv.ClientID %>").style.display = 'block';
            }
            return false;
        }

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <asp:HiddenField ID="DashBoard_candidateResumeID" runat="server" />
                <div>
                    <asp:Label ID="DashBoard_successMessageLabel" runat="server" Text="" SkinID="sknSuccessMessage"></asp:Label></div>
                <asp:Label ID="DashBoard_errorMessageLabel" runat="server" Text="" CssClass="Mandatory"></asp:Label></div>
            <div id="Main_divID" class="selfsign_main_outer">
                <div class="DashBoard_topSearchPanel" style=" display:none">
                    <div id="SearchAndAdminitrator_divID">
                        <div id="SearchAdministration_DivID" >
                            <asp:Label ID="DashBoard_SearchAdministration_LabelID" SkinID="sknSelfSearchLabelText"
                                runat="server" Text="Search for a test"></asp:Label>
                        </div>
                        <div style="display:none">
                            <asp:TextBox ID="DashBoard_searchTestTextBox" runat="server" TabIndex="0" SkinID="sknSelfSearchTextBox"
                                ToolTip="Enter search skill"></asp:TextBox>
                            <asp:ImageButton ID="DashBoard_searchTestImageButton" runat="server" SkinID="sknbtnSearchIconNew"
                                OnClick="DashBoard_searchTestImageButton_Click" ToolTip="Click here to search" />
                            <asp:ImageButton ID="DashBoard_searchVideoImageButton" runat="server" SkinID="sknbtnVideoIconNew"
                                ToolTip="Click here to view a demo video on search & self administration of tests"
                                Visible="false" OnClientClick="javascript:SelfAdminDemo();" />
                        </div>
                    </div>
                </div>
                <div class="selfsign_mid_panel" style="display: none; ">
                    &nbsp;
                </div>
                <div class="selfsign_right_panel" style="display: none; ">
                    <div id="SkillDetails_divID">
                        <div class="leftside_W" style="display: none">
                            <div class="leftside">
                                <asp:ImageButton ID="DashBoard_SkillsImageButton" runat="server" SkinID="sknSkillistImageButton" />
                            </div>
                            <div class="leftside">
                                <asp:Label ID="DashBoard_SkillDetail_LabelID" BackColor="Transparent" runat="server"
                                    SkinID="sknSelfHeadLabelText" Text="Skills"></asp:Label>
                            </div>
                            <div class="rightside">
                                <asp:Button ID="DashBoard_AddSkillButton" runat="server" Text="Add Skill" CssClass="self_skil_blue_btn_bg"
                                    OnClick="DashBoard_AddSkillButton_Click" ToolTip="Click here to add skills" />
                            </div>
                        </div>
                        <div class="leftside_W" style="overflow: auto; height: 84px; display: none">
                            <asp:DataList ID="DashBoard_SkillsDataList" runat="server" CellPadding="2" CellSpacing="2"
                                RepeatColumns="2" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <asp:Image ID="DashBoard_ArrowImage" runat="server" SkinID="sknTickImage" />
                                    <asp:Label ID="DashBoard_SkillLabel" runat="server" CssClass="self_label_skiltext"
                                        Text='<%# Eval("Skill") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="leftside_W">
                            <div class="leftside">
                            </div>
                        </div>
                        <div class="self_right_panel_divider" style="display: none">
                            &nbsp;
                        </div>
                        <div class="leftside_W" style="padding-top: 70px;display: none">
                            <div class="leftside">
                                <asp:Image ID="DashBoard_PreviewImage" runat="server" SkinID="sknAlertImage" />
                            </div>
                            <div class="leftside">
                                <asp:Label ID="DashBoard_ResumeStatuLabel" runat="server" Width="160px" Text="Upload your resume"
                                    CssClass="self_label_text_wrap"></asp:Label>
                            </div>
                            <div class="rightside">
                                <asp:Button ID="DashBoard_PreviewButton" runat="server" Text="Preview" CssClass="self_skil_blue_btn_bg"
                                    OnClick="DashBoard_PreviewButton_Click" ToolTip="Click here to resume preview" />
                            </div>
                        </div>
                        <div class="empty_height">
                            &nbsp;
                        </div>
                        <div class="leftside_W" style="padding-top: 4px;display: none">
                            <asp:Button ID="DashBoard_UploadResumeButton" SkinID="sknButtonOrange" runat="server"
                                Text="Upload Your Resume" OnClick="DashBoard_UploadResumeButton_Click" ToolTip="Click here to upload resume" />
                        </div>
                    </div>
                </div>
                <div class="empty_height_width">
                        &nbsp;
                    </div> 
               <%-- <div style="width: 960px; float: left; height: 4px">
                </div>--%>
                <div style="width: 960px; float: left; display:none">
                    <div class="can_tab_bg_left">
                        &nbsp;
                    </div>
                    <div class="can_tab_bg" style="width: 915px;">
                        <asp:Label ID="DashBoard_skillsLabel" runat="server" Text="Skills" CssClass="edit_profile_section_title_label"></asp:Label>
                    </div>
                    <div class="can_tab_bg_right">
                        &nbsp;
                    </div>
                </div>
                <div style="width: 960px; float: left;  display:none">
                    <div class="edit_profile_section_panel" style="padding-top: 8px; padding-bottom: 8px;
                        width: 935px">
                        <div class="edit_profile_control_row">
                            <div style="width: 560px; height: 134px; overflow: auto; float: left;">
                                <asp:GridView ID="DashBoard_skillsGridView" runat="server" AutoGenerateColumns="False"
                                    Width="560px" SkinID="sknEditProfileSkillGridView">
                                    <Columns>
                                        <asp:BoundField DataField="SlNo" HeaderText="SNo" />
                                        <asp:BoundField DataField="Skill" HeaderText="Skill" />
                                        <asp:BoundField DataField="ExperienceYears" HeaderText="Exp (In Years)" />
                                        <asp:BoundField DataField="LastUsed" HeaderText="Last Used (Year)" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div style="width: 100px; float: left; padding-left: 14px">
                                <asp:Button ID="DashBoard_editSkillsButton" runat="server" Text="Edit Skills" SkinID="sknButtonOrange"
                                    ToolTip="Click here to edit your skills" OnClick="DashBoard_AddSkillButton_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 960px; float: left; height: 4px;display:none">
                </div>
                <div style="width: 960px; float: left; display:none">
                    <div class="can_tab_bg_left">
                        &nbsp;
                    </div>
                    <div class="can_tab_bg" style="width: 915px">
                        <asp:Label ID="DashBoard_resumeLabel" runat="server" Text="Resume" CssClass="edit_profile_section_title_label"></asp:Label>
                    </div>
                    <div class="can_tab_bg_right">
                        &nbsp;
                    </div>
                </div>
                <div style="width: 960px; float: left; display:none">
                    <div class="edit_profile_section_panel" style="padding-top: 8px; padding-bottom: 8px;
                        width: 935px">
                        <div class="edit_profile_control_row">
                            <div style="width: 915px; float: left">
                                <asp:LinkButton ID="DashBoard_resumeNameLink" runat="server" OnClick="DashBoard_resumeNameLink_Click"
                                    ToolTip="Click here to download and view your profile" CssClass="edit_profile_resume_name_link"
                                    Visible="false"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="edit_profile_control_row">
                            <div style="width: 915px; float: left">
                                <div style="width: 15px; float: left; padding-top: 5px;">
                                    <asp:Image ID="DashBoard_resumeAlertImage" runat="server" SkinID="sknResumeAlertImage"
                                        ToolTip="Upload your resume" />
                                </div>
                                <div style="float: left; padding-top: 10px">
                                    <asp:Label ID="DashBoard_resumeStatusLabel" runat="server" Text="Upload your resume"
                                        CssClass="edit_profile_resume_status_alert_label"></asp:Label>
                                </div>
                                <div style="float: left; padding-top: 8px; padding-left: 4px;">
                                    <asp:Button ID="DashBoard_previewResumeButton" runat="server" Text="Review & Approve"
                                        Visible="false" SkinID="sknButtonBlue" OnClick="DashBoard_PreviewButton_Click"
                                        ToolTip="Click here to review and approve your resume" />
                                </div>
                                <div style="float: left; padding-top: 8px; padding-left: 4px;">
                                    <asp:Button ID="DashBoard_uploadResumeButton1" SkinID="sknButtonOrange" runat="server"
                                        Text="Update Your Resume" OnClick="DashBoard_UploadResumeButton_Click" ToolTip="Click here to update your resume" />
                                </div>
                                <div id="DashBoard_deleteLinkDiv" style="float: right; padding-top: 12px; padding-left: 4px;
                                    padding-right: 270px; text-align: right">
                                    <asp:LinkButton ID="DashBoard_deleteResumeLink" Text="Delete Resume" runat="server"
                                        OnClientClick="javascript:return ShowHideDeleteConfirmation()" ToolTip="Click here to delete your profile"
                                        CssClass="self_label_text_link" Visible="false">
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="edit_profile_control_row">
                            <%-- <div style="width: 915px; float: left">--%>
                            <div id="DashBoard_deleteConfirmationDiv" style="width: 290px; float: left; padding-right: 4px;
                                padding-left: 565px; display: none;">
                                <div class="edit_profile_delete_resume_warning_panel">
                                    <div style="float: left; padding-top: 3px">
                                        <asp:Label ID="DashBoard_deleteConfirmationLabel" Text="Are you sure that you want to delete this resume?"
                                            runat="server" CssClass="edit_profile_delete_resume_warning_label"></asp:Label>
                                    </div>
                                    <div style="float: left; padding-left: 4px">
                                        <asp:Button ID="DashBoard_deleteConfirmationYesButton" runat="server" Text="Yes"
                                            SkinID="sknButtonBlue" OnClick="DashBoard_deleteConfirmationYesButton_Click" />
                                    </div>
                                    <div style="float: left; padding-left: 4px">
                                        <asp:Button ID="DashBoard_deleteConfirmationNoButton" runat="server" Text="No" SkinID="sknButtonBlue"
                                            OnClientClick="javascript:return ShowHideDeleteConfirmation()" />
                                    </div>
                                </div>
                            </div>
                            <%--</div>--%>
                        </div>
                    </div>
                </div>
                <div class="leftside_main" style="height: 280px">
                <div id="PendingTest_divID" class="leftside_main_width">
                        <div style="height: 280px">
                            <div class="leftside_main_width">
                                <div class="leftside_main_w">
                                    <asp:ImageButton ID="DashBoard_PendingTestButtonImageButton" runat="server" SkinID="sknPendingTestImage" />
                                    <asp:HiddenField ID="DashBoard_pendingTests_testRecommendID" runat="server" />
                                </div>
                                <div class="rightside_main_w">
                                    <asp:Label ID="DashBoard_PendingTest_TitleLabel" runat="server" CssClass="self_label_maintext_head"
                                        Text="Test To Be Taken"></asp:Label>
                                </div>
                            </div>
                            <div id="PendingTest_Div" runat="server" class="leftside_main_width">
                                <div class="leftside_main_width">
                                    <asp:Label ID="DashBoard_PendingTest_NameLabel" CssClass="self_label_subtext_head"
                                        runat="server" Text="Pending Test Name"></asp:Label>
                                </div>
                                <div class="leftside_main_width" style="width: 370px; word-wrap: break-word; white-space: normal;">
                                    <asp:Label ID="DashBoard_PendingTest_DescLabel" runat="server" CssClass="self_label_text"
                                        Text=""></asp:Label>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:ImageButton ID="PendingTest_IntroductionImageButton" runat="server" CssClass="self_label_text_link"
                                            CommandArgument="PendingTest_Introduction" OnCommand="Introduction_Command" ToolTip="Test Introduction"
                                            ImageUrl="~/Images/test_intro.gif" />
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:ImageButton ID="DashBoard_PendingTest_ReminderImageButton" runat="server" CssClass="self_label_text_link"
                                            CommandArgument="PendingTest_TestReminder" ToolTip="Test Reminder" ImageUrl="~/Images/test_reminder.gif"
                                            OnCommand="ReminderImageButton_Command" />
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_PendingTest_CreatedLabel" CssClass="self_label_text" runat="server"
                                            Text="Scheduled On :"></asp:Label>
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:Label ID="DashBoard_PendingTest_CreatedDateLabel" CssClass="self_label_text_blue"
                                            runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_PendingTest_ExpiryLabel" runat="server" CssClass="self_label_text"
                                            Text="Expired On &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;"></asp:Label>
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:Label ID="DashBoard_PendingTest_ExpiryDateLabel" runat="server" CssClass="self_label_text_blue"
                                            Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:LinkButton ID="DashBoard_PendingTest_MoreLinkButton" runat="server" Style="font-family: Verdana,Arial;
                                            font-size: 11px; color: #ff7200; text-decoration: none" CommandArgument="PendingTest_More"
                                            OnCommand="DashBoard_MoreLinkButton_Command">more...</asp:LinkButton>
                                    </div>
                                    <div class="leftside_main_width_float">
                                        <asp:Button ID="DashBoard_pendingScheduledTestStartButton" SkinID="sknButtonOrange"
                                            runat="server" Text="Start Test" OnClick="CandidateHome_startTestButton_OkClick"
                                            ToolTip="Click here to start test" Visible="false" />
                                        <asp:Button ID="DashBoard_pendingSelfTestStartButton" SkinID="sknButtonOrange" runat="server"
                                            Text="Start Test" OnClick="DashBoard_PendingTestStart_Click" ToolTip="Click here to start test"
                                            Visible="false" />
                                    </div>
                                </div>
                                <div>
                                    <asp:Panel ID="CandidateHome_startTestPopupPanel" runat="server" Style="display: none;
                                        height: 206px" CssClass="popupcontrol_confirm">
                                        <div id="CandidateHome_hiddenDIV" style="display: none">
                                            <asp:Button ID="CandidateHome_hiddenButton" runat="server" />
                                        </div>
                                        <uc2:ConfirmMsgControl ID="CandidateHome_confirmMsgControl" runat="server" OnOkClick="CandidateHome_startTestButton_OkClick" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="CandidateHome_startTestPopupExtender" runat="server"
                                        PopupControlID="CandidateHome_startTestPopupPanel" TargetControlID="CandidateHome_hiddenButton"
                                        BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                    <asp:UpdatePanel ID="CandidateHome_confirmMsgUpdatePanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel ID="CandidateHome_RetakeValidationPopupPanel" runat="server" Style="display: none;
                                                height: 202px;" CssClass="popupcontrol_confirm">
                                                <div id="CandidateHome_RetakeValidationDiv" style="display: none">
                                                    <asp:Button ID="CandidateHome_RetakeValidation_hiddenButton" runat="server" />
                                                </div>
                                                <uc2:ConfirmMsgControl ID="CandidateHome_RetakeValidation_ConfirmMsgControl" runat="server"
                                                    Title="Request to Retake" Type="OkSmall" />
                                            </asp:Panel>
                                            <ajaxToolKit:ModalPopupExtender ID="CandidateHome_RetakeValidation_ModalPopupExtender"
                                                runat="server" PopupControlID="CandidateHome_RetakeValidationPopupPanel" TargetControlID="CandidateHome_RetakeValidation_hiddenButton"
                                                BackgroundCssClass="modalBackground">
                                            </ajaxToolKit:ModalPopupExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div id="PendingTest_emptyDiv" runat="server" class="leftside_main_width" style="height: 160px">
                                <asp:Label ID="DashBoard_PendingTestErrorLabel" CssClass="self_label_not_available"
                                    runat="server" Text="Tests not available"></asp:Label>
                            </div>
                        </div>
                       <%-- <div class="self_blue_strip">
                            &nbsp;
                        </div>--%>
                        <asp:HiddenField ID="DashBoard_PendingTest_RecommandIDHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_PendingTest_totalQuestionsHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_PendingTest_RecommandTimeHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_PendingTest_TestIDHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_PendingTest_CandidateSessionKeyHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_PendingTest_TestSessionIDHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_PendingTest_StatusHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_PendingTest_AttemptIDHiddenField" runat="server" />
                    </div>
                    <div class="leftside_main_width" id="CommpletedTest_divID">
                        <div style="height: 280px">
                            <div>
                                <div class="leftside_main_w">
                                    <asp:ImageButton ID="DashBoard_CompletedTestImageButton" runat="server" SkinID="sknCompletedTestImage" />
                                </div>
                                <div class="rightside_main_w">
                                    <asp:Label ID="DashBoard_CompletedTest_LabelID" runat="server" CssClass="self_label_maintext_head"
                                        Text=" Completed Tests"></asp:Label>
                                </div>
                            </div>
                            <div id="CompletedTest_Div" runat="server" class="leftside_main_width">
                                <div class="leftside_main_width" style="width: 370px">
                                    <asp:Label ID="DashBoard_CompletedTest_NameLabel" runat="server" Text="Completed Test"
                                        CssClass="self_label_subtext_head"></asp:Label>
                                </div>
                                <div class="leftside_main_width" style="width: 370px">
                                    <asp:Label ID="DashBoard_CompletedTest_DescLabel" runat="server" CssClass="self_label_text"
                                        Style="word-wrap: break-word; white-space: normal;" Text="Completed Test Description"></asp:Label>
                                </div>
                                <div class="leftside_main_width">
                                    <asp:ImageButton ID="DashBoard_CompletedTest_ViewResultsImageButton" runat="server"
                                        CssClass="self_label_text_link" CommandArgument="CompletedTest_ViewResults" OnCommand="Introduction_Command"
                                        ToolTip="View My Report" ImageUrl="~/Images/test_results.gif" />
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_CompletedTest_CreatedLabel" runat="server" Text="Created&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:"
                                            CssClass="self_label_text"></asp:Label>
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:Label ID="DashBoard_CompletedTest_CreateDateLabel" runat="server" Text="" CssClass="self_label_text_blue"></asp:Label>
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:LinkButton ID="DashBoard_CompletedTest_MoreLinkButton" runat="server" Style="font-family: Verdana,Arial;
                                            font-size: 11px; color: #ff7200; text-decoration: none" CommandArgument="CompletedTest_More"
                                            OnCommand="DashBoard_MoreLinkButton_Command">more...</asp:LinkButton>
                                        <div style="float: right; margin-left: 0px; display: none;">
                                            <asp:ImageButton ID="DashBoard_YahooLoginImageButton" runat="server" ImageUrl="~/Images/yahoo_icon.png"
                                                ToolTip="Click here to share with yahoo" />
                                        </div>
                                        <div style="float: right; margin-left: 5px; display: none; ">
                                            <asp:ImageButton ID="DashBoard_TwitterLoginImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_twitter.png"
                                                ToolTip="Click here to share your results link into LinkedIn" />
                                        </div>
                                        <div style="float: right; margin-left: 5px;display: none;">
                                            <asp:HyperLink ID="CandidateTestResult_shareInLinkedInHyperLink" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/linkedin.png"
                                                Target="_blank" ToolTip="Click here to share your results link into LinkedIn">
                                            </asp:HyperLink>
                                        </div>
                                        <div style="float: right; margin-left: 120px; display:none" runat="server" id="DashBoard_resultsShareDiv" >
                                            <asp:Label ID="DashBoard_CompletedTest_ResultShareLoginLabel" runat="server" CssClass="self_label_text_blue"
                                                Text="Share : " ></asp:Label>
                                            <%--       <a name="fb_share" title="Click here to share your results link into Facebook" share_url="<%=resultUrl %>">
                                            </a>
                                            <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>--%>
                                        </div>
                                    </div>
                                    <div class="leftside_main_width_float">
                                        <div style="float: left">
                                            <%--<asp:ImageButton ID="DashBoard_FacebookLoginImageButton" runat="server" 
                                            ImageUrl="~/App_Themes/DefaultTheme/Images/icon_facebook.png" ToolTip="Click here to share your results link into FaceBook" />--%>
                                            <%--<asp:ImageButton ID="v_LinkedInLoginImageButton" runat="server" 
                                            ImageUrl="~/App_Themes/DefaultTheme/Images/linkedin.png" ToolTip="Click here to share with linkedin"/>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="CompletedTest_emptyDiv" runat="server" class="leftside_main_width" style="height: 160px">
                                <asp:Label ID="DashBoard_Completed_TestErrorLabel" CssClass="self_label_not_available"
                                    runat="server" Text="Completed tests not available"></asp:Label>
                            </div>
                        </div>
                       <%-- <div class="self_green_strip">
                            &nbsp;
                        </div>--%>
                        <asp:HiddenField ID="DashBoard_CompletedTest_SessionIDHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_CompletedTest_AttemptIDHiddenField" runat="server" />
                        <asp:HiddenField ID="DashBoard_CompletedTest_TestKeyHiddenField" runat="server" />
                    </div>
                    
                </div>
                <%--<div class="empty_height_width"  style="background-color: Fuchsia">
                        &nbsp;
                    </div>--%>
                <div class="leftside_main" style="height: 280px; display:none" >
                    <div id="CompletedInterview_divID" class="leftside_main_width">
                        <div style="height: 280px;">
                            <div>
                                <div class="leftside_main_w">
                                    <asp:ImageButton ID="DashBoard_CompletedInterviews_ImageButton" runat="server" SkinID="sknCompletedInterviewImage" />
                                </div>
                                <div class="rightside_main_w">
                                    <asp:Label ID="DashBoard_CompletedInterviews_TitleLabel" runat="server" Text="Completed Interviews"
                                        CssClass="self_label_maintext_head"></asp:Label>
                                </div>
                            </div>
                            <div id="CompletedInterview_Div" runat="server" class="leftside_main_width">
                                <div class="leftside_main_width" style="width: 370px">
                                    <asp:Label ID="DashBoard_CompletedInterview_NameLabel" runat="server" Text="Interview Name"
                                        CssClass="self_label_subtext_head"></asp:Label>
                                </div>
                                <div class="leftside_main_width" style="width: 370px">
                                    <asp:Label ID="DashBoard_CompletedInterview_DescLabel" runat="server" CssClass="self_label_text"
                                        Style="word-wrap: break-word; white-space: normal" Text="Interview Name Description"></asp:Label>
                                </div>
                                <div class="leftside_main_width" style="display: none">
                                    <div class="leftside_main_w">
                                        <asp:ImageButton ID="DashBoard_InterviewIntroImageButton" runat="server" CssClass="self_label_text_link"
                                            CommandArgument="PendingTest_Introduction" OnCommand="Introduction_Command" ToolTip="Interview Introduction"
                                            ImageUrl="~/Images/test_intro.gif" />
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:ImageButton ID="DashBoard_CompletedInterview_ReminderImageButton" runat="server"
                                            CssClass="self_label_text_link" CommandArgument="PendingTest_TestReminder" ToolTip="Completed Interview Reminder"
                                            ImageUrl="~/Images/test_reminder.gif" OnCommand="ReminderImageButton_Command" />
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_CompletedInterview_ExpiryLabel" runat="server" Text="Expiry&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:"
                                            CssClass="self_label_text"></asp:Label>
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:Label ID="DashBoard_CompletedInterview_ExpiryDateLabel" runat="server" Text=""
                                            CssClass="self_label_text_blue"></asp:Label>
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:LinkButton ID="DashBoard_CompletedInterview_MoreLinkButton" runat="server" Style="font-family: Verdana,Arial;
                                            font-size: 11px; color: #ff7200; text-decoration: none" CommandArgument="CompletedInterview_More"
                                            OnCommand="DashBoard_MoreLinkButton_Command">more...</asp:LinkButton>
                                    </div>
                                    <div class="leftside_main_width_float">
                                        <%--<asp:Button ID="DashBoard_CompletedInterview_StartButton" CssClass="self_common_btn_bg" runat="server"
                                            Text="Start Test" OnClientClick="javascript:return StartTest()" ToolTip="Click here to start Interview" />--%>
                                    </div>
                                </div>
                            </div>
                            <div id="CompletedInterview_emptyDiv" runat="server" class="leftside_main_width"
                                style="height: 160px">
                                <asp:Label ID="DashBoard_CompletedInterview_ErrorLabel" CssClass="self_label_not_available"
                                    runat="server" Text="Completed interviews not available"></asp:Label>
                            </div>
                        </div>
                        <div class="self_yellow_strip">
                            &nbsp;
                        </div>
                        <asp:HiddenField ID="DashBoard_CompletedInterview_CandidateSessionIDHiddenField"
                            runat="server" />
                        <asp:HiddenField ID="DashBoard_CompletedInterview_AttemptIDHiddenField" runat="server" />
                    </div>
                    <div class="leftside_main_width">
                        <div style="height: 280px">
                            <div>
                                <div class="leftside_main_w">
                                    <asp:ImageButton ID="Candidate_PendingInterviewImageButton" runat="server" SkinID="sknPendingInterviewImage" />
                                </div>
                                <div class="rightside_main_w">
                                    <div id="Dashboard_pendingOfflineInterviewTitleDiv" style="display: block"> 
                                        <a href="#" style="text-decoration: none" onclick="return showInterview('OFFLINE')"
                                            title="Click here to show the pending offline interviews">
                                            <asp:Label ID="DashBoard_PendingInterview_TitleLabel" runat="server" Text="Pending Offline Interviews"
                                                CssClass="self_label_maintext_head"></asp:Label> 
                                        </a>  
                                         <a href="#" style="text-decoration: none" onclick="return showInterview('ONLINE')"
                                            title="Click here to show the pending interviews">
                                            <asp:Label ID="Label1" runat="server" Text="Pending Interviews" CssClass="self_label_maintext_below_head"></asp:Label>
                                        </a>
                                        
                                    </div>
                                  
                                </div>
                            </div>
                            <div id="PendingInterview_Div" runat="server" class="leftside_main_width" style="display:block" >
                                <div class="leftside_main_width">
                                    <asp:Label ID="DashBoard_PendingInterview_NameLabel" runat="server" Text="Pending Interview Name"
                                        CssClass="self_label_subtext_head"></asp:Label>
                                </div>
                                <div class="leftside_main_width" style="word-wrap: break-word; white-space: normal;">
                                    <asp:Label ID="DashBoard_PendingInterview_DescLabel" runat="server" Text="Pending Interview Description"
                                        CssClass="self_label_text"></asp:Label>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:ImageButton ID="DashBoard_PendingInterview_IntroImageButton" runat="server"
                                            CssClass="self_label_text_link" CommandArgument="PendingInterview_Introduction"
                                            OnCommand="Introduction_Command" ToolTip="Interview Introduction" ImageUrl="~/Images/test_intro.gif" />
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:ImageButton ID="DashBoard_PendingInterview_RemiderImageButton" runat="server"
                                            CssClass="self_label_text_link" ToolTip="Interview Reminder" ImageUrl="~/Images/test_reminder.gif"
                                            CommandArgument="PendingInterview_IntReminder" OnCommand="ReminderImageButton_Command" />
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_PendingInterview_CreatedLabel" runat="server" Text="Scheduled On : &nbsp;&nbsp;&nbsp;"
                                            CssClass="self_label_text"></asp:Label>
                                    </div>
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_PendingInterview_CreatedDateLabel" runat="server" Text=""
                                            CssClass="self_label_text_blue"></asp:Label>
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_PendingInterview_ExpiryLabel" runat="server" Text="Expired On &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
                                            CssClass="self_label_text"></asp:Label>
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:Label ID="DashBoard_PendingInterview_ExpiryDateLabel" runat="server" Text=""
                                            CssClass="self_label_text_blue"></asp:Label>
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:LinkButton ID="DashBoard_PendingInterview_MoreLinkButton" runat="server" Style="font-family: Verdana,Arial;
                                            font-size: 11px; color: #ff7200; text-decoration: none" CommandArgument="PendingInterview_More"
                                            OnCommand="DashBoard_MoreLinkButton_Command">more...</asp:LinkButton>
                                    </div>
                                    <div class="leftside_main_width_float">
                                        <asp:Button ID="DashBoard_PendingInterview_StartButton" SkinID="sknButtonOrange"
                                            runat="server" Text="Start Interview" OnClick="PendingInterviews_startInterviewButton_OkClick"
                                            ToolTip="Click here to start interview" />
                                    </div>
                                </div>
                            </div>
                            
                             <div id="onlineInterviewSubDiv" runat="server" class="leftside_main_width" style="display:none">
                                <div class="leftside_main_width">
                                    <asp:Label ID="DashBoard_pendingOnlineInterview_NameLabel" runat="server" Text="Pending Interview Name"
                                        CssClass="self_label_subtext_head"></asp:Label>
                                </div>
                                <div class="leftside_main_width" style="word-wrap: break-word; white-space: normal;">
                                    <asp:Label ID="DashBoard_pendingOnlineInterview_DescLabel" runat="server" Text="Pending Interview Description"
                                        CssClass="self_label_text"></asp:Label>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:ImageButton ID="DashBoard_pendingOnlineInterview_IndroImageButton" runat="server"
                                            CssClass="self_label_text_link" CommandArgument="PendingOnlineInterview_Introduction"
                                            OnCommand="Introduction_Command" ToolTip="Interview Introduction" ImageUrl="~/Images/test_intro.gif" />
                                    </div>
                                    <div class="rightside_main_w">
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_pendingOnlineInterview_SchduledDateTitleLabel" runat="server"
                                            Text="Scheduled On : &nbsp;&nbsp;&nbsp;" CssClass="self_label_text"></asp:Label>
                                    </div>
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_pendingOnlineInterview_ScheduledValueLabel" runat="server"
                                            Text="" CssClass="self_label_text_blue"></asp:Label>
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:Label ID="DashBoard_pendingOnlineInterview_ExpiredTitleLabel" runat="server"
                                            Text="Expired On &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " CssClass="self_label_text"></asp:Label>
                                    </div>
                                    <div class="rightside_main_w">
                                        <asp:Label ID="DashBoard_pendingOnlineInterview_ExpiredValueLabel" runat="server"
                                            Text="" CssClass="self_label_text_blue"></asp:Label>
                                    </div>
                                </div>
                                <div class="leftside_main_width">
                                    <div class="leftside_main_w">
                                        <asp:LinkButton ID="DashBoard_pendingOnlineInterview_MoreLinkButton" runat="server"
                                            Style="font-family: Verdana,Arial; font-size: 11px; color: #ff7200; text-decoration: none"
                                            CommandArgument="PendingOnlineInterview_More" OnCommand="DashBoard_MoreLinkButton_Command">more...</asp:LinkButton>
                                    </div>
                                    <div class="leftside_main_width_float">
                                        <asp:Button ID="DashBoard_pendingOnlineInterview_StartButton" SkinID="sknButtonOrange"
                                            runat="server" Text="Start Interview" ToolTip="Click here to start interview"
                                            OnClick="DashBoard_pendingOnlineInterview_StartButton_Click" />
                                    </div>
                                </div>
                            </div>

                            <div id="PendingInterview_emptyDiv" runat="server" class="leftside_main_width" style="height: 160px">
                                <asp:Label ID="DashBoard_PendingInterview_ErrorLabel" CssClass="self_label_not_available"
                                    runat="server" Text="Pending interviews not available"></asp:Label>
                            </div>
                        </div>
                        <div class="self_pink_strip">
                            &nbsp;
                        </div>
                         <asp:HiddenField ID="DashBoard_pendingOnlineInterview_chatRoomIdHiddenField" runat="server" />
                        <asp:HiddenField ID="PendingInterview_CandidateSessionIDHiddenField" runat="server" />
                        <asp:HiddenField ID="PendingInterview_AttemptIDHiddenField" runat="server" />
                        <div>
                            <asp:Panel ID="CandidateHome_startInterviewPopupPanel" runat="server" Style="display: none;
                                height: 206px" CssClass="popupcontrol_confirm">
                                <div id="InterviewCandidateHome_hiddenDIV" style="display: none">
                                    <asp:Button ID="InterviewCandidateHome_hiddenButton" runat="server" />
                                </div>
                                <uc2:ConfirmMsgControl ID="InterviewCandidateHome_confirmMsgControl" runat="server"
                                    OnOkClick="PendingInterviews_startInterviewButton_OkClick" />
                            </asp:Panel>
                            <ajaxToolKit:ModalPopupExtender ID="CandidateHome_startInterviewPopupExtender" runat="server"
                                PopupControlID="CandidateHome_startInterviewPopupPanel" TargetControlID="InterviewCandidateHome_hiddenButton"
                                BackgroundCssClass="modalBackground">
                            </ajaxToolKit:ModalPopupExtender>
                            <asp:UpdatePanel ID="InterviewCandidateHome_confirmMsgUpdatePanel" runat="server"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel ID="InterviewCandidateHome_RetakeValidationPopupPanel" runat="server"
                                        Style="display: none; height: 202px;" CssClass="popupcontrol_confirm">
                                        <div id="InterviewCandidateHome_RetakeValidationDiv" style="display: none">
                                            <asp:Button ID="InterviewCandidateHome_RetakeValidation_hiddenButton" runat="server" />
                                        </div>
                                        <uc2:ConfirmMsgControl ID="InterviewCandidateHome_RetakeValidation_ConfirmMsgControl"
                                            runat="server" Title="Request to Retake" Type="OkSmall" />
                                    </asp:Panel>
                                    <ajaxToolKit:ModalPopupExtender ID="InterviewCandidateHome_RetakeValidation_ModalPopupExtender"
                                        runat="server" PopupControlID="InterviewCandidateHome_RetakeValidationPopupPanel"
                                        TargetControlID="InterviewCandidateHome_RetakeValidation_hiddenButton" BackgroundCssClass="modalBackground">
                                    </ajaxToolKit:ModalPopupExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="empty_height">
                &nbsp;
            </div>
        </div>
    </div>
</asp:Content>
