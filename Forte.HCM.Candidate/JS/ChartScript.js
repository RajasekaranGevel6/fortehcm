﻿
//Method that is used to show the zoomed chart for the 
//chart controls
function ShowZoomedChart(sessionName, type) {

    var height = 620;
    var width = 950;
    var top = (screen.availHeight - parseInt(height)) / 2;
    var left = (screen.availWidth - parseInt(width)) / 2;
    var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
    var queryStringValue = null;

    switch (type) {
        //check whether the to load the single series chart control 
        case "SingleSeries":
            queryStringValue = "../Popup/ZoomedChart.aspx?sessionName=" + sessionName;
            break;
        //check whether the to load the single series report chart control  
        case "SingleSeriesReport":
            queryStringValue = "../Popup/SingleSeriesReportZoomedChart.aspx?sessionName=" + sessionName;
            break;
        //check whether the to load the multi series chart control  
        case "MultiSeries":
            queryStringValue = "../Popup/MultiSeriesZoomedChart.aspx?sessionName=" + sessionName;
            break;
        //check whether the to load the multi series report chart control  
        case "MultiSeriesReport":
            queryStringValue = "../Popup/MultiSeriesReportZoomedChart.aspx?sessionName=" + sessionName;
            break;
        //check whether the to load the histogram chart control  
        case "Histogram":
            queryStringValue = "../Popup/HistogramZoomedChart.aspx?sessionName=" + sessionName;
            break;
    }

    window.open(queryStringValue, window.self, sModalFeature);
    return false;
}
