﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateSearchTest.aspx.cs
// File that represents the user interface layout and functionalities
// for the CandidateSearchTest page. This page helps in searching for test
// segments based on skills and allows to save or take the test.

#endregion Header

using System;
using System.Text;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.ExternalService;
using Forte.HCM.UI.CommonControls;

namespace Forte.HCM.Candidate.CandidateAdmin
{
    /// <summary>
    /// File that represents the user interface layout and functionalities
    /// for the CandidateSearchTest page. This page helps in searching for test
    /// segments based on skills and allows to save or take the test. This 
    /// class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CandidateSearchTest : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Search Test");

                // Clear message and hide labels.
                ClearMessages();

                // Set default button to 'go' button.
                Page.Form.DefaultButton = CandidateSearch_goImageButton.UniqueID;

                // Set focus to keywords text box.
                CandidateSearch_keywordsTextBox.Focus();

                CandidateSearch_recommendedTestsPageNavigator.PageNumberClick += new 
                    RecommendedTestPageNavigator.PageNumberClickEventHandler
                    (CandidateSearch_recommendedTestsPageNavigator_PageNumberClick);
                CandidateSearch_recommendedTestsPageNavigator.Visible = true;

                if (!IsPostBack)
                { 
                    CandidateSearch_searchContentDiv.Visible = false;
                    // Assign message, title and type to start test 
                    // confirmation window.
                    CandidateSearch_startTestConfirmMsgControl.Message = "Are you ready to take the test now ?";
                    CandidateSearch_startTestConfirmMsgControl.Title = "Take Test";
                    CandidateSearch_startTestConfirmMsgControl.Type = MessageBoxType.YesNo;

                    CandidateSearch_recommendedTestsListView.Visible = false;

                    ViewState["SORT_ORDER"] = SortType.Ascending;
                    ViewState["SORT_FIELD"] = "TESTNAME";


                    if (Session["CANDIDATE_TEST_DELETED"] != null && Convert.ToBoolean(Session["CANDIDATE_TEST_DELETED"]) == true)
                    {
                        // Show message.
                        base.ShowMessage(CandidateSearch_searchTestSuccessLabel, "Test deleted successfully");

                        // Clear delete status from session.
                        Session["CANDIDATE_TEST_DELETED"] = null;
                    }

                    if (Session["SEARCH_TEST"] != null)
                    {
                        CandidateSearch_keywordsTextBox.Text = Convert.ToString(Session["SEARCH_TEST"]);
                        CandidateSearch_goImageButton_Click(sender, null);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of recommended tests list view.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void CandidateSearch_recommendedTestsPageNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                CandidateSearch_recommendedTestsListView.EditIndex = -1;
                LoadAutomatedQuestions(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the go image button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will search recommended tests for the given keyword.
        /// </remarks>
        protected void CandidateSearch_goImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                // Clear fields and data.
                ViewState["RECOMMENDED_QUESTIONS"] = null;
                ViewState["NO_OF_QUESTIONS"] = null;
                ViewState["TEST_RECOMMENDED_ID"] = null;
                ViewState["RECOMMENDED_TIME"] = null;
                ViewState["CANDIDATE_SESSION_ID"] = null;
                Session["SEARCH_TEST"] = null;
                CandidateSearch_recommendedTestsPageNavigator.Reset();
                CandidateSearch_recommendedTestsListView.Visible = true;

                // Load automated questions.
                LoadAutomatedQuestions(1);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the save button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will save the test segments.
        /// </remarks>
        protected void CandidateSearch_saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if max limit reached for saving/taking tests.
                int count = new CandidateBLManager().GetSelfAdminTestCount
                    (base.userID, DateTime.Now.Month, DateTime.Now.Year);

                if (count >= base.maxSelfAdminTestPerMonth)
                {
                    base.ShowMessage(CandidateSearch_searchTestErrorLabel,
                      string.Format("You have reached the maximum limit. You can process only {0} tests per month", 
                      base.maxSelfAdminTestPerMonth));

                    return;
                }

                // Check if test recommended ID is present in the view state.
                if (Utility.IsNullOrEmpty(ViewState["TEST_RECOMMENDED_ID"]))
                {
                    base.ShowMessage(CandidateSearch_searchTestErrorLabel, 
                        "No test was searched.");
                    return;
                }

                // Save segment.
                bool isNew;
                int genID = SaveSegment(out isNew);

                if (isNew)
                {
                    base.ShowMessage(CandidateSearch_searchTestSuccessLabel,
                        "This test has been added to your list of pending tests.");

                    CandidateSearch_saveButton.Visible = false;
                    CandidateSearchTest_deleteTestLinkButton.Visible = true;
                }
                else
                {
                    base.ShowMessage(CandidateSearch_searchTestSuccessLabel,
                        "This recommended test is already saved by you.");
                }

                try
                {
                    if (genID != 0)
                    {
                        new EmailHandler().SendMail
                            (EntityType.SelfAdminTestRecommendationSaved, genID.ToString());
                    }
                }
                catch 
                {
                    base.ShowMessage(CandidateSearch_searchTestErrorLabel,
                        "Unable to send the test details through mail.");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the take test button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will leads to the test instructions page.
        /// </remarks>
        protected void CandidateSearch_takeTestButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if questions present to save.
                if (Utility.IsNullOrEmpty(ViewState["TEST_STATUS"]))
                {
                    // Check if max limit reached for saving/taking tests.
                    int count = new CandidateBLManager().GetSelfAdminTestCount
                        (base.userID, DateTime.Now.Month, DateTime.Now.Year);

                    if (count >= base.maxSelfAdminTestPerMonth)
                    {
                        base.ShowMessage(CandidateSearch_searchTestErrorLabel,
                          string.Format("You have reached the maximum limit. You can process only {0} tests per month",
                          base.maxSelfAdminTestPerMonth));

                        return;
                    }

                    bool isNew;
                    // Save segment.
                    SaveSegment(out isNew);
                }

                // Check if questions present to save.
                if (Utility.IsNullOrEmpty(ViewState["RECOMMENDED_QUESTIONS"]))
                {
                    base.ShowMessage(CandidateSearch_searchTestErrorLabel,
                        "No test/questions generated to proceed.");
                    return;
                }

                // Get questions from view state.
                List<QuestionDetail> questions = ViewState["RECOMMENDED_QUESTIONS"] 
                    as List<QuestionDetail>;

                // Check if questions found.
                if (questions == null || questions.Count == 0)
                {
                    base.ShowMessage(CandidateSearch_searchTestErrorLabel,
                        "No test/questions generated to proceed.");
                    return;
                }

                // Save test for the list of questions.
                string testKey = string.Empty;
                bool saved = SaveTest(questions, out testKey);

                if (saved == false)
                {
                    base.ShowMessage(CandidateSearch_searchTestErrorLabel,
                      "Unable to save the test and proceed.");

                    return;
                }
                
                // Create test session, schedule candidate and proceed 
                // to test instructions page.
                string candidateSessionID = string.Empty;
                bool scheduled = ScheduleCandidate(testKey, out candidateSessionID);

                // Update the test key & scheduled status.
                new CandidateBLManager().UpdateCandidateTestRecommendationStatus
                    (base.userID, Convert.ToInt16(ViewState["TEST_RECOMMENDED_ID"]), testKey, 
                    Constants.CandidateSelfAdminTestStatus.SCHEDULED);

                if (scheduled == false)
                {
                    base.ShowMessage(CandidateSearch_searchTestErrorLabel,
                        "Unable to schedule the test and proceed.");

                    return;
                }

                // Check if start test confirm message needs to be shown. If
                // configureds as true then show the confirmation message, else
                // navigates to test instructions page.
                if (ConfigurationManager.AppSettings["CONFIRM_SELF_ADMIN_START_TEST"] != null
                    && Convert.ToBoolean(ConfigurationManager.AppSettings["CONFIRM_SELF_ADMIN_START_TEST"]) == false)
                {
                    // Construct the url for the test instructions page.
                    string url = "~/TestCenter/TestInstructions.aspx" +
                        "?candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + Constants.CandidateSelfAdminTestConstants.ATTEMPT_ID +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_SELF_ADMIN_TEST;

                    // Navigates to the test instructions page to proceed to take
                    // the generated test.
                    Response.Redirect(url, false);
                }
                else
                {
                    // Keep the candidate session ID in view state.
                    ViewState["CANDIDATE_SESSION_ID"] = candidateSessionID;

                    // Show the start test confirmation window.
                    CandidateSearch_startTestPopupExtenderControl.Show();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that is called when the yes button is clicked in the delete 
        /// test confirmation panel
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="ImageClickEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This delete the test and reapply the search.
        /// </remarks>
        protected void CandidateSearchTest_deleteTestConfirmationYesButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(ViewState["TEST_RECOMMENDED_ID"]))
                {
                    base.ShowMessage(CandidateSearch_searchTestErrorLabel, "No test found to delete");
                    return;
                }

                // Delete the recommended test.
                new CandidateBLManager().DeleteCandidateRecommendedTest(base.userID, Convert.ToInt32(ViewState["TEST_RECOMMENDED_ID"]));

                // Keep delete status in session.
                Session["CANDIDATE_TEST_DELETED"] = true;

                // Reload the page with same search parameters.
                Response.Redirect("~/CandidateAdmin/CandidateSearchTest.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the item data bound event is 
        /// fired in the recommended tests list view.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void CandidateSearch_recommendedTestsListView_ItemDataBound
            (object sender, ListViewItemEventArgs e)
        {
            try
            {
                CandidateSearch_recommendedTestsListView.Visible = true;

                Label CandidateSearch_recommendedTestRecommendedTimeLabel =
                    (Label)e.Item.FindControl("CandidateSearch_recommendedTestsListView_recommendedTimeValueLabel");

                CandidateSearch_recommendedTestRecommendedTimeLabel.Text = Utility.ConvertSecondsToHoursMinutes
                    (int.Parse(ViewState["RECOMMENDED_TIME"].ToString()));

                Chart CandidateSearch_recommendedTestChart =
                    (Chart)e.Item.FindControl("CandidateSearch_recommendedTestsChart");

                List<AutomatedTestSummaryGrid> testval = ViewState["QuestionDetails"]
                    as List<AutomatedTestSummaryGrid>;

                if (testval == null)
                    return;
                CandidateSearch_recommendedTestChart.DataSource = testval;

                CandidateSearch_recommendedTestChart.Series.Add("Test");

                CandidateSearch_recommendedTestChart.Series["Test"].ChartType = SeriesChartType.Pie;

                CandidateSearch_recommendedTestChart.Series["Test"].IsValueShownAsLabel = false;
                CandidateSearch_recommendedTestChart.Series["Test"].ToolTip = "#VALX (#PERCENT{0%})";

                string[] xValues = new string[testval.Count];
                int[] yValues = new int[testval.Count];

                for (int pointIndex = 0; pointIndex < testval.Count; pointIndex++)
                {
                    yValues[pointIndex] = Convert.ToInt16(testval[pointIndex].NoofQuestionsInCategory);

                    //xValues[pointIndex] = testval[pointIndex].CategoryName.ToString();
                    xValues[pointIndex] = testval[pointIndex].SubjectName.ToString();
                }

                CandidateSearch_recommendedTestChart.Series["Test"].Points.DataBindXY(xValues, yValues);

                CandidateSearch_recommendedTestChart.Series["Test"]["PieLabelStyle"] = "InSide";
            }
            catch (Exception exp)
            {

                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void CandidateSearch_startTest_OkClick(object sender, EventArgs e)
        {
            try
            {
                // Construct the url for the test instructions page.
                string url = "~/TestCenter/TestInstructions.aspx" +
                    "?candidatesessionid=" + ViewState["CANDIDATE_SESSION_ID"] +
                    "&attemptid=" + Constants.CandidateSelfAdminTestConstants.ATTEMPT_ID +
                    "&parentpage=" + Constants.ParentPage.CANDIDATE_SELF_ADMIN_TEST;

                // Navigates to the test instructions page to proceed to take
                // the generated test.
                Response.Redirect(url, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the cancel button is 
        /// clicked in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This retain the current settings (show the results div).
        /// </remarks>
        protected void CandidateSearch_startTest_CancelClick(object sender, EventArgs e)
        {
            try
            {
                CandidateSearch_searchContentDiv.Visible = true;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Method that will load the pending, completed and expired interviews
        /// into the grid.
        /// </summary>
        /// <remarks>
        /// This is a overridden method.
        /// </remarks>
        protected override void LoadValues()
        {


        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that saves the segment.
        /// </summary>
        /// <param name="isNew">
        /// A <see cref="bool"/> that holds the output parameter which indicates 
        /// whether the record is newly saved or already exist.
        /// </param>
        /// <returns>
        /// A <see cref="int"/> that holds the generated ID.
        /// </returns>
        private int SaveSegment(out bool isNew)
        {
            // Construct test detail.
            TestDetail testDetail = new TestDetail();
            testDetail.UserID = base.userID;
            testDetail.TestRecommendedID = Convert.ToInt16(ViewState["TEST_RECOMMENDED_ID"]);
            testDetail.Status = Constants.CandidateSelfAdminTestStatus.SAVED;

            // Insert recommended test.
            return new TestBLManager().InsertCandidateRecommendedTest(testDetail, out isNew);
        }

        /// <summary>
        /// Method that saves the question set as a new test.
        /// </summary>
        /// <param name="questions">
        /// A list of <see cref="QuestionDetail"/> that holds the questions.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key as an output
        /// parameter.
        /// </param>
        /// <remarks>
        /// A <see cref="bool"/> that holds the status. True represents success
        /// and false represents failure.
        /// </remarks>
        private bool SaveTest(List<QuestionDetail> questions, out string testKey)
        {
            // Assign default value to test key.
            testKey = string.Empty;

            List<decimal> creditEarned = new List<decimal>();
            List<decimal> timeTaken = new List<decimal>();

            // Construct sum of credits earned and average time.
            foreach (QuestionDetail question in questions)
            {
                creditEarned.Add(question.CreditsEarned);
                timeTaken.Add(question.AverageTimeTaken);
            }

            // Construct test detail object.
            TestDetail testDetail = new TestDetail();

            string testName = CandidateSearch_recommendedTestNameTitleLabel.Text.Trim();
            testDetail.Name = testName;
            testDetail.Description = testName;
            testDetail.Questions = questions;
            testDetail.IsActive = true;
            testDetail.IsCertification = false;
            testDetail.CertificationId = 0;
            testDetail.CreatedBy = userID;
            testDetail.IsDeleted = false;
            testDetail.ModifiedBy = userID;
            testDetail.NoOfQuestions = questions.Count;
            testDetail.SystemRecommendedTime =
                Convert.ToInt32(timeTaken.Average()) * questions.Count;
            testDetail.RecommendedCompletionTime = testDetail.SystemRecommendedTime;

            testDetail.TestAuthorID = userID;
            testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
            testDetail.TestCreationDate = DateTime.Now;
            testDetail.TestStatus = TestStatus.Active;
            testDetail.TestType = TestType.Genenal;
            testDetail.TestMode = "SELF_ADMIN";
            testDetail.TestKey = "";

            // Get complexity.
            string complexity = new ControlUtility().GetComplexity
                (Constants.AttributeTypes.COMPLEXITY,
                questions).AttributeID.ToString();

            // Save the test and retrieve the test key.
            testKey = new TestBLManager().SaveTest(testDetail, userID, complexity);

            return true;
        }

        /// <summary>
        /// Method that creates the test session and schedules the candidate
        /// for the test.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID as an
        /// output parameter.
        /// </param>
        /// <remarks>
        /// A <see cref="bool"/> that holds the status. True represents success
        /// and false represents failure.
        /// </remarks>
        private bool ScheduleCandidate(string testKey, out string candidateSessionID)
        {
            // Assign default value to test key.
            candidateSessionID = string.Empty;

            // Construct test session detail.
            TestSessionDetail sessionDetail = GetTestSessionDetail(testKey);

            if (sessionDetail == null)
                return false;

            // Construct test schedule detail.
            TestScheduleDetail scheduleDetail = new TestScheduleDetail();
            scheduleDetail.CandidateID = base.userID.ToString();
            scheduleDetail.AttemptID = Constants.CandidateSelfAdminTestConstants.ATTEMPT_ID;
            scheduleDetail.EmailId = ((UserDetail)Session["USER_DETAIL"]).Email;
            scheduleDetail.ExpiryDate = DateTime.Now.AddDays(base.candidateExpiryDays);

            // Schedule the candidate.
            new TestBLManager().SaveTestSessionScheduleCandidate(sessionDetail, scheduleDetail,
                base.userID, out candidateSessionID);

            return true;
        }

        /// <summary>
        /// Method that constructs and returns the test session detail.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that holds the test session detail.
        /// </returns>
        private TestSessionDetail GetTestSessionDetail(string testKey)
        {
            // Get test detail.
            TestDetail testDetail = new TestBLManager().GetTestAndCertificateDetail(testKey);

            if (testDetail == null)
                return null;

            // Construct test session detail object.
            TestSessionDetail sessionDetail = new TestSessionDetail();

            sessionDetail.TestID = testDetail.TestKey;
            sessionDetail.TestName = testDetail.Name;

            // Set number of candidate session (session count)
            sessionDetail.NumberOfCandidateSessions = 1;

            // Set total credits limit
            sessionDetail.TotalCredit = testDetail.TestCost;

            // Set time limit
            sessionDetail.TimeLimit = Convert.ToInt32(ViewState["RECOMMENDED_TIME"]);

            sessionDetail.ClientRequestID = "0";

            // Set expiry date
            sessionDetail.ExpiryDate = DateTime.Now;

            // Set random question order status
            sessionDetail.IsRandomizeQuestionsOrdering = false;

            // Set display result status 
            sessionDetail.IsDisplayResultsToCandidate = true;

            // Set cyber proctoring status
            sessionDetail.IsCyberProctoringEnabled = false;

            // Set created by
            sessionDetail.CreatedBy = base.userID;

            // Set modified by
            sessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            sessionDetail.Instructions = Constants.CandidateSelfAdminTestConstants.TEST_INSTRUCTIONS;
            // Set session descriptions
            sessionDetail.TestSessionDesc = Constants.CandidateSelfAdminTestConstants.SESSION_DESCRIPTION;

            return sessionDetail;
        }

        /// <summary>
        /// Method that loads the automated questions for the given page
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void LoadAutomatedQuestions(int pageNumber)
        {               
            // Clear alert message.
            CandidateSearch_testStatusAlertMessage.Text = string.Empty;
            CandidateSearch_testStatusAlertImage.Visible = false;
            DataTable criteriaTable = null;
            CandidateSearch_recommendedTestsListView.Visible = true;
            string sortOrder = ViewState["SORT_ORDER"].ToString();
            string sortExpression = ViewState["SORT_FIELD"].ToString();
            List<TestDetail> recommendedTestDetails = null;

            int totalPage = 0;

            // Check if test keyword is entered.
            if (CandidateSearch_keywordsTextBox.Text == string.Empty)
            {
                CandidateSearch_searchTestErrorLabel.Visible = true;
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, "Enter the search keywords.");
                CandidateSearch_recommendedTestsListView.DataSource = null;
                CandidateSearch_recommendedTestsListView.DataBind();
                CandidateSearch_recommendedTestsPageNavigator.Visible = false;
                CandidateSearch_saveButton.Visible = false;
                CandidateSearchTest_deleteTestLinkButton.Visible = false;
                CandidateSearch_searchContentDiv.Visible = false;
                CandidateSearch_takeTestButton.Visible = false;
                return;
            }

            // Retrieve recomended test details.
            recommendedTestDetails = new TestBLManager().GetRecommendedTestDetails
                (CandidateSearch_keywordsTextBox.Text.Trim(), sortExpression,
                sortOrder, 1, pageNumber, out totalPage);

            if (recommendedTestDetails.Count == 0)
            {
                base.ShowMessage(CandidateSearch_searchTestErrorLabel, 
                    "No recommended test found for the given keyword(s). Try with different keyword(s).");
                CandidateSearch_recommendedTestsListView.DataSource = null;
                CandidateSearch_recommendedTestsListView.DataBind();
                CandidateSearch_recommendedTestsPageNavigator.Visible = false;
                CandidateSearch_saveButton.Visible = false;
                CandidateSearchTest_deleteTestLinkButton.Visible = false;
                CandidateSearch_searchContentDiv.Visible = false;
                CandidateSearch_takeTestButton.Visible = false;
                CandidateSearch_keywordsTextBox.Focus();
                return;
            }

            criteriaTable = GetCriteriaTable(recommendedTestDetails[0].TestRecommendedID);

            // Load questions.
            LoadQuestions(recommendedTestDetails[0].NoOfQuestions,
                ref criteriaTable);

            // Retrieve candidate test recommendation detail.
            TestDetail testRecommendationDetail = new CandidateBLManager().GetCandidateTestRecommendationDetail
                (base.userID, recommendedTestDetails[0].TestRecommendedID);

            if (testRecommendationDetail == null)
            {
                ViewState["TEST_STATUS"] = null;
                ViewState["TEST_KEY"] = null;
            }
            else
            {
                ViewState["TEST_STATUS"] = testRecommendationDetail.Status; 
                ViewState["TEST_KEY"] = testRecommendationDetail.TestKey;
            }

            ViewState["TEST_RECOMMENDED_ID"] = recommendedTestDetails[0].TestRecommendedID;
            ViewState["NO_OF_QUESTIONS"] = recommendedTestDetails[0].NoOfQuestions;
            ViewState["RECOMMENDED_TIME"] = recommendedTestDetails[0].RecommendedCompletionTime;
                
            CandidateSearch_recommendedTestsListView.DataSource = recommendedTestDetails;
            CandidateSearch_recommendedTestsListView.DataBind();
            CandidateSearch_saveButton.Visible = true;
            CandidateSearchTest_deleteTestLinkButton.Visible = false;
            CandidateSearch_recommendedTestsPageNavigator.Visible = true;
            CandidateSearch_takeTestButton.Visible = true;
            CandidateSearch_recommendedTestsPageNavigator.TotalRecords = totalPage;
            CandidateSearch_recommendedTestsPageNavigator.PageSize = 1;
            CandidateSearch_recommendedTestNameTitleLabel.Text = recommendedTestDetails[0].Name;
            CandidateSearch_searchContentDiv.Visible = true;

            // Show/hide buttons & status.
            if (ViewState["TEST_STATUS"] != null)
            {
                if (ViewState["TEST_STATUS"].ToString().Trim() == Forte.HCM.Support.Constants.CandidateSelfAdminTestStatus.SCHEDULED)
                {
                    CandidateSearch_testStatusAlertMessage.Text = "You have already scheduled this test. Check with your pending tests";
                    CandidateSearch_takeTestButton.Visible = false;
                    CandidateSearch_saveButton.Visible = false;
                    CandidateSearchTest_deleteTestLinkButton.Visible = false;
                    CandidateSearch_testStatusAlertImage.Visible = true;
                }
                else if (ViewState["TEST_STATUS"].ToString().Trim() == Forte.HCM.Support.Constants.CandidateSelfAdminTestStatus.COMPLETED)
                {
                    CandidateSearch_testStatusAlertMessage.Text = "You have already completed this test. Check with your completed tests";
                    CandidateSearch_takeTestButton.Visible = false;
                    CandidateSearch_saveButton.Visible = false;
                    CandidateSearchTest_deleteTestLinkButton.Visible = false;
                    CandidateSearch_testStatusAlertImage.Visible = true;
                }
                else if (ViewState["TEST_STATUS"].ToString().Trim() == Forte.HCM.Support.Constants.CandidateSelfAdminTestStatus.SAVED)
                {
                    CandidateSearch_testStatusAlertMessage.Text = "You have already saved this test";
                    CandidateSearch_saveButton.Visible = false;
                    CandidateSearchTest_deleteTestLinkButton.Visible = true;
                    CandidateSearch_testStatusAlertImage.Visible = true;
                }
            }
        }

        /// <summary>
        /// Method that constructs and returns the criteria table for the given
        /// recommended test ID.
        /// </summary>
        /// <param name="recommendedTestID">
        /// A <see cref="int"/> that holds the recommended test ID.
        /// </param>
        /// <returns>
        /// A <see cref="DataTable"/> that holds the criteria table.
        /// </returns>
        private DataTable GetCriteriaTable(int recommendedTestID)
        {
            // Get search criteria for the given recommended test ID.
            List<TestSearchCriteria> testSearchCriterias = new TestBLManager().
                GetSearchCriteria(recommendedTestID);

            // Check if criteria present.
            if (testSearchCriterias == null || testSearchCriterias.Count == 0)
                return null;

            // Construct test segment criteria table.
            DataTable criteriaTable = GetTestSegmentCriteriaTable();

            DataRow drNewRow = null;

            int SNo = 1;
                
            // Fill the segment criteria rows.
            foreach (TestSearchCriteria criteria in testSearchCriterias)
            {
                drNewRow = criteriaTable.NewRow();
                drNewRow["SNO"] = SNo++.ToString();
                drNewRow["Cat_sub_id"] = criteria.CategoriesID;
                drNewRow["Weightage"] = criteria.Weightage; ;
                drNewRow["Keyword"] = criteria.Keyword;
                drNewRow["TestArea"] = criteria.TestAreasID;
                drNewRow["Complexity"] = criteria.Complexity;
                criteriaTable.Rows.Add(drNewRow);
                drNewRow = null;
            }
            return criteriaTable;
        }

        /// <summary>
        /// Method that construct and returns the test segment criteria table.
        /// </summary>
        /// <param name="criteriaTable">
        /// A <see cref="DataTable"> that holds the criteria table.
        /// </param>
        private DataTable GetTestSegmentCriteriaTable()
        {
            DataTable criteriaTable = new DataTable();
            criteriaTable.Columns.Add("SNO", typeof(int));
            criteriaTable.Columns.Add("Cat_sub_id", typeof(string));
            criteriaTable.Columns.Add("Complexity", typeof(string));
            criteriaTable.Columns.Add("TestArea", typeof(string));
            criteriaTable.Columns.Add("Category", typeof(string));
            criteriaTable.Columns.Add("Subject", typeof(string));
            criteriaTable.Columns.Add("Weightage", typeof(int));
            criteriaTable.Columns.Add("Keyword", typeof(string));
            criteriaTable.Columns.Add("ExpectedQuestions", typeof(int));
            criteriaTable.Columns.Add("PickedQuestions", typeof(int));
            criteriaTable.Columns.Add("TotalRecordsinDB", typeof(int));
            criteriaTable.Columns.Add("QuestionsDifference", typeof(int));
            criteriaTable.Columns.Add("Remarks", typeof(string));

            return criteriaTable;
        }

        /// <summary>
        /// Method that construct and returns the test statistics.
        /// </summary>
        /// <param name="questions">
        /// A list of <see cref="QuestionDetail"/> that holds the question 
        /// details.
        /// </param>
        /// <returns>
        /// A <see cref="TestStatistics"/> that holds the test statistics.
        /// </returns>
        private TestStatistics GetTestStatistics(List<QuestionDetail> questions)
        {
            StringBuilder categoryNames = new StringBuilder();
            AutomatedTestSummaryGrid automatedTestSummaryGrid = null;
            TestStatistics testStatistics = null;
            List<AutomatedTestSummaryGrid> automatedTestSummaryGridOrderdByQuestion = null;
            int totalCategory = 0;

            foreach (QuestionDetail question in questions)
            {
                if (categoryNames.ToString().IndexOf("CAT: " + question.SubjectName.Trim()) >= 0)
                    continue;

                totalCategory = questions.FindAll(
                    p => " CAT: " + p.SubjectName.Trim() == " CAT: " + question.SubjectName.Trim()).Count;

                categoryNames.Append(" CAT: " + question.SubjectName.Trim());

                if (Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGrid))
                    automatedTestSummaryGrid = new AutomatedTestSummaryGrid();

                automatedTestSummaryGrid.CategoryName = question.CategoryName;
                automatedTestSummaryGrid.TestAreaName = question.TestAreaName;
                automatedTestSummaryGrid.Complexity = question.Complexity;
                automatedTestSummaryGrid.NoofQuestionsInCategory = totalCategory;
                /*Added by MKN 14-Feb-2012*/
                automatedTestSummaryGrid.SubjectName = question.SubjectName;

                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics))
                    testStatistics = new TestStatistics();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
                    testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                testStatistics.AutomatedTestSummaryGrid.Add(automatedTestSummaryGrid);
                automatedTestSummaryGrid = null;
                totalCategory = 0;
            }

            if (!Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
            {
                automatedTestSummaryGridOrderdByQuestion = testStatistics.AutomatedTestSummaryGrid.OrderByDescending(p => p.NoofQuestionsInCategory).ToList();
                testStatistics.AutomatedTestSummaryGrid = null;
                testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                testStatistics.AutomatedTestSummaryGrid = automatedTestSummaryGridOrderdByQuestion;

                ViewState["QuestionDetails"] = automatedTestSummaryGridOrderdByQuestion;
            }

            testStatistics.NoOfQuestions = questions.Count;

            return testStatistics;
        }

        private void LoadExpectedQuestions(ref DataTable dtSearchCriteria, int NoofQuestions, out bool ZeroExpectedQuestion)
        {
            ZeroExpectedQuestion = false;
            int SearchSegements = dtSearchCriteria.Rows.Count;
            int QuestionsAdded = 0;
            int ExpectedQuestions = 0;
            int LoopI = 0;
            for (LoopI = 0; LoopI < SearchSegements; LoopI++)
            {
                QuestionsAdded = Convert.ToInt32((Convert.ToDecimal(dtSearchCriteria.Rows[LoopI]["Weightage"])
                    / 100) * NoofQuestions);
                dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] = QuestionsAdded;
                ExpectedQuestions += QuestionsAdded;
            }
            if (ExpectedQuestions > NoofQuestions)
            {
                int QuestionsToAdd = ExpectedQuestions - NoofQuestions;
                for (LoopI = SearchSegements - 1; LoopI >= 0; LoopI--)
                {
                    if (Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd <= 0)
                        continue;
                    dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd;
                    break;
                }
            }
            else if (ExpectedQuestions < NoofQuestions)
                dtSearchCriteria.Rows[LoopI - 1]["ExpectedQuestions"] = QuestionsAdded +
                    (NoofQuestions - ExpectedQuestions);
            if (dtSearchCriteria.Select("ExpectedQuestions <= 0").Length > 0)
            {

                ZeroExpectedQuestion = true;
            }
        }
        private List<TestSearchCriteria> GetQuestionSearchCriteria(ref DataTable dtSearchCriteria)
        {
            List<TestSearchCriteria> testSearchCriterias = null;
            TestSearchCriteria testSearchCriteria = null;
            Subject subject = null;
            try
            {
                for (int i = 0; i < dtSearchCriteria.Rows.Count; i++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria))
                        testSearchCriteria = new TestSearchCriteria();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Cat_sub_id"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(subject))
                            subject = new Subject();
                        subject.SubjectID = Convert.ToInt32(dtSearchCriteria.Rows[i]["Cat_sub_id"]);
                        testSearchCriteria.Subjects = new List<Subject>();
                        testSearchCriteria.Subjects.Add(subject);
                    }
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["TestArea"]))
                        testSearchCriteria.TestAreasID = dtSearchCriteria.Rows[i]["TestArea"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Complexity"]))
                        testSearchCriteria.Complexity = dtSearchCriteria.Rows[i]["Complexity"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Keyword"]))
                        testSearchCriteria.Keyword = dtSearchCriteria.Rows[i]["Keyword"].ToString();
                    testSearchCriteria.NoOfQuestions = Convert.ToInt32(dtSearchCriteria.Rows[i]["expectedquestions"]);
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias))
                        testSearchCriterias = new List<TestSearchCriteria>();
                    testSearchCriterias.Add(testSearchCriteria);
                    testSearchCriteria = null;
                    subject = null;
                }
                return testSearchCriterias;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria)) testSearchCriteria = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias)) testSearchCriterias = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(subject)) subject = null;

            }
        }

        /// <summary>
        /// Method that loads the questions for the given criteria.
        /// </summary>
        /// <param name="noOfQuestions">
        /// A <see cref="int"/> that holds the number of questions.
        /// </param>
        /// <param name="criteriaTable">
        /// A <see cref="DataTable"/> that holds the search criteria.
        /// </param>
        private void LoadQuestions(int noOfQuestions, ref DataTable criteriaTable)
        {
            int TotalWeightage = 0;
            bool ZeroExpectedQuestion = false;
            LoadWeightages(ref criteriaTable, noOfQuestions, out TotalWeightage);
            if (TotalWeightage > 100)
                return;

            LoadExpectedQuestions(ref criteriaTable, noOfQuestions, out ZeroExpectedQuestion);
            if (ZeroExpectedQuestion)
                return;

            List<QuestionDetail> questionDetails =
                new QuestionBLManager().
                GetRecommendedAutomatedQuestions(QuestionType.MultipleChoice,
                GetQuestionSearchCriteria(ref criteriaTable),
                noOfQuestions, ref criteriaTable, 0, base.userID);

            ViewState["RECOMMENDED_QUESTIONS"] = questionDetails;

            if (questionDetails == null || questionDetails.Count == 0)
                return;

            TestStatistics testStatistics = GetTestStatistics(questionDetails);
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);
                
                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                    dtSearchCriteria.Rows[LoopI]["PickedQuestions"] = 0;
                    dtSearchCriteria.Rows[LoopI]["TotalRecordsinDB"] = 0;
                    dtSearchCriteria.Rows[LoopI]["QuestionsDifference"] = 0;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// Method that constructs the test statistics.
        /// </summary>
        /// <param name="questionDetails">
        /// A list of <see cref="QuestionDetail"/> that holds the questions. 
        /// </param>
        /// <param name="testStatistics">
        /// A <see cref="TestStatistics"/> that holds the test statistics.
        /// </param>
        private void ConstructTestStatistics(ref List<QuestionDetail> questionDetails, 
            ref TestStatistics testStatistics)
        {
            testStatistics.NoOfQuestions = questionDetails.Count;
            testStatistics.TestCost = 0;
            for (int i = 0; i < questionDetails.Count; i++)
                testStatistics.TestCost += questionDetails[i].CreditsEarned;
            if (testStatistics.TestCost == 0)
                testStatistics.TestCost = 0.00M;
            testStatistics.AverageTimeTakenByCandidates =
                Convert.ToInt32(questionDetails.Average(p => p.AverageTimeTaken)) * questionDetails.Count;
            testStatistics.AutomatedTestAverageComplexity = ((AttributeDetail)
                (new ControlUtility().GetComplexity(Constants.AttributeTypes.COMPLEXITY, questionDetails))).
                AttributeName;
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            CandidateSearch_searchTestErrorLabel.Text = string.Empty;
            CandidateSearch_searchTestErrorLabel.Visible = false;
            CandidateSearch_searchTestSuccessLabel.Text = string.Empty;
            CandidateSearch_searchTestSuccessLabel.Visible = false;
        }

        #endregion Private Methods
    }
}