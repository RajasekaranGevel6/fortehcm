<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    Title="Activities" CodeBehind="Activities.aspx.cs" Inherits="Forte.HCM.UI.CandidateCenter.Activities" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<%@ Register Src="../CommonControls/RecommendedTestPageNavigator.ascx" TagName="PageNavigator"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Activities_bodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">

        function ShowDeleteConfirmationPanel(ctrlId) {
            if (document.getElementById(ctrlId).style.display == "none") {
                CollapseAllRows(ctrlId);
                document.getElementById(ctrlId).style.display = "block";
            }
            else {
                CollapseAllRows(ctrlId);
                document.getElementById(ctrlId).style.display = "none";
            }
            return false;
        }

        //Hide all the question's Option panel
        function CollapseAllRows(targetControl) {
            var gridCtrl = document.getElementById("<%= Activities_pendingTestsGridDiv.ClientID %>");
            if (gridCtrl != null) {
                var rowItems = gridCtrl.getElementsByTagName("div");
                for (indexRow = 0; indexRow < rowItems.length; indexRow++) {
                    if (rowItems[indexRow].id.indexOf("Activities_pendingTestsGridView_deleteConfirmationDiv") != "-1") {
                        rowItems[indexRow].style.display = "none";
                    }
                }
            }
            return false;
        } 

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div class="activities_main_div">
                    <div class="activities_inner_div">
                        <asp:UpdatePanel ID="Activities_topMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="Activities_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="Activities_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingTestsPagingNavigator" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingTestsGridView" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_completedTestsPagingNavigator" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_completedTestsGridView" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingInterviewsPagingNavigator" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingInterviewsGridView" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_completedInterviewsPagingNavigator" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_completedInterviewsGridView" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingOnlineInterviewsGridView" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="activities_main_div">
                        <div class="activities_outer_border" runat="server" id="Activities_pendingTestsDiv"
                            visible="false">
                            <asp:UpdatePanel ID="Activities_pendingTestsUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="activities_inner_cont">
                                        <asp:HiddenField ID="Activities_pendingTests_testName" runat="server" />
                                        <div class="activities_inner_cont_left">
                                            <asp:ImageButton ID="Activities_pendingTestsImageButton" runat="server" SkinID="sknPendingTestImage" />
                                        </div>
                                        <div class="activities_inner_cont_left">
                                            <asp:Label ID="Activities_pendingTestsTitleLabel" runat="server" Text="Pending Tests"
                                                CssClass="self_label_maintext_head"></asp:Label>
                                        </div>
                                        <div class="activities_inner_cont_right">
                                            <uc3:PageNavigator ID="Activities_pendingTestsPagingNavigator" runat="server" />
                                        </div>
                                    </div>
                                    <div class="activities_border_top">
                                    </div>
                                    <div style="clear: both" runat="server" id="Activities_pendingTestsGridDiv">
                                        <asp:GridView ID="Activities_pendingTestsGridView" runat="server" AllowSorting="True"
                                            ShowHeader="false" AutoGenerateColumns="False" Width="100%" OnRowDataBound="Activities_pendingTestsGridView_RowDataBound"
                                            OnRowCommand="Activities_pendingTestsGridView_RowCommand" SkinID="sknPendingActivities">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="Activities_pendingTestsGridView_candidateSessionIDHiddenField"
                                                            Value='<%# Eval("CandidateTestSessionID") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingTestsGridView_attemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingTestsGridView_isSelfAdminTestHiddenField"
                                                            Value='<%# Eval("IsSelfAdmin") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingTestsGridView_isScheduledHiddenField" Value='<%# Eval("IsScheduled") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingTestsGridView_testRecommendationIDHiddenField"
                                                            Value='<%# Eval("TestRecommendationID") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingTestsGridView_timeLimitHiddenField" Value='<%# Eval("TimeLimit") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingTestsGridView_totalQuestionsHiddenField" Value='<%# Eval("TotalQuestions") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingTestsGridView_testNameHiddenField" Value='<%# Eval("TestName") %>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div class="activities_inner_grid_head">
                                                            <div class="activities_inner__grid_left_head">
                                                                <div style="float: left; width: 20px; padding-right: 10px">
                                                                    <asp:Label ID="Activities_pendingTestsGridView_rowNumberLabel" runat="server" Text='<%# Eval("RowNumber") %>'
                                                                        SkinID="sknActivityNumberLabel"></asp:Label>
                                                                </div>
                                                                <div>
                                                                    <asp:Label ID="Activities_pendingTestsGridView_testNameLabel" runat="server" Text='<%# Eval("TestName") %>'
                                                                        SkinID="sknActivityNameLabel"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="activities_inner__grid_right_head" style="padding-top: 4px; text-align: right">
                                                                <asp:Label ID="Activities_pendingTestsSelfAdministeredLabel" runat="server" Text="Self Administered"
                                                                    SkinID="sknSelfAdministeredLabel" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "TRUE"%>'></asp:Label>
                                                                <asp:Label ID="Activities_pendingTestsRecruiterAdministeredLabel" runat="server"
                                                                    Text="Recruiter Administered" SkinID="sknRecruiterAdministeredLabel" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'></asp:Label>
                                                                <asp:Label ID="Activities_pendingTestsExpiredLabel" runat="server" Text="Expired"
                                                                    SkinID="sknExpiredLabel" Visible='<%# Eval("IsExpired").ToString().ToUpper() == "TRUE"%>'></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="activities_inner__grid_left_head" style="word-wrap: break-word; white-space: normal;">
                                                            <asp:Label ID="Activities_pendingTestsGridView_testDescriptionLabel" runat="server"
                                                                Text='<%# Eval("TestDescription")==null ? Eval("TestDescription") : Eval("TestDescription").ToString().Replace("\n", "<br />") %>'
                                                                SkinID="sknActivityDescriptionLabel"></asp:Label>
                                                        </div>
                                                        <div class="activities_outer_grid">
                                                            <div class="activities_outer_grid_left">
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingTestsGridView_testIntroductionLinkButton" runat="server"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="TestIntroduction"
                                                                            CssClass="self_label_text_link" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE" && Eval("IsExpired").ToString().ToUpper() == "FALSE" %>'>Test Introduction</asp:LinkButton>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingTestsGridView_testReminderLinkButton" runat="server"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="TestReminder" CssClass="self_label_text_link"
                                                                            Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE" && Eval("IsExpired").ToString().ToUpper() == "FALSE" %>'>Test Reminder</asp:LinkButton>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingTestsGridView_emailSchedulerLinkButton" runat="server"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="EmailScheduler"
                                                                            CssClass="self_label_text_link" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'>Email Scheduler</asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <asp:Label ID="Activities_pendingTestsGridView_scheduledByValueLabel" SkinID="sknActivityValueLabel"
                                                                        runat="server" Text='<%# Eval("ScheduledByMessage") %>' Visible='<%# Eval("IsScheduled").ToString().ToUpper() == "TRUE" && Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'></asp:Label>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingTestsGridView_testCreatedDateLabel" runat="server"
                                                                            Text="Scheduled On" SkinID="sknActivityCaptionLabel" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'></asp:Label>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingTestsGridView_testCreatedDateValueLabel" SkinID="sknActivityValueLabel"
                                                                            runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("CreatedDate"))) %>'
                                                                            Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingTestsGridView_testExpiryDateLabel" runat="server"
                                                                            Text="Expired On" SkinID="sknActivityCaptionLabel" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'></asp:Label>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingTestsGridView_testExpiryDateValueLabel" SkinID="sknActivityValueLabel"
                                                                            runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("ExpiryDate"))) %>'
                                                                            Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="activities_outer_grid_right">
                                                                <asp:Button ID="Activities_pendingTestsGridView_startTestButton" SkinID="sknButtonOrange"
                                                                    runat="server" Text="Start Test" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    Visible='<%# Eval("IsExpired").ToString().ToUpper() == "FALSE"%>' CommandName="StartTest" />
                                                                <asp:LinkButton ID="Activities_pendingTestsGridView_deleteTestLinkButton" Text="Delete"
                                                                    runat="server" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "TRUE" && Eval("IsScheduled").ToString().ToUpper() == "FALSE" %>'></asp:LinkButton>
                                                            </div>
                                                            <div id="Activities_pendingTestsGridView_deleteConfirmationDiv" runat="server" style="display: none;
                                                                padding-left: 684px; padding-top: 50px">
                                                                <div class="delete_self_admin_test_warning_panel">
                                                                    <div style="float: left; padding-top: 3px">
                                                                        <asp:Label ID="Activities_pendingTestsGridView_deleteConfirmationLabel" Text="Are you sure to delete the test ?"
                                                                            runat="server" CssClass="delete_self_admin_test_warning_label"></asp:Label>
                                                                    </div>
                                                                    <div style="float: left; padding-left: 4px">
                                                                        <asp:Button ID="Activities_pendingTestsGridView_deleteConfirmationYesButton" runat="server"
                                                                            Text="Yes" CommandArgument="<%# Container.DataItemIndex %>" CommandName="DeleteTest"
                                                                            SkinID="sknButtonBlue" />
                                                                    </div>
                                                                    <div style="float: left; padding-left: 4px">
                                                                        <asp:Button ID="Activities_pendingTestsGridView_deleteConfirmationNoButton" runat="server"
                                                                            Text="No" SkinID="sknButtonBlue" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="activities_border_bottom">
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="activities_outer_grid">
                                                    <asp:Label ID="Activities_pendingTestsNoDataLabel" runat="server" Text="No pending tests found to display"
                                                        SkinID="sknNoActivityLabel"></asp:Label>
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="activities_empty_height">
                            &nbsp;
                        </div>
                        <div class="activities_outer_border" runat="server" id="Activities_completedTestsDiv"
                            visible="false">
                            <asp:UpdatePanel ID="Activities_completedTestsUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="activities_inner_cont">
                                        <asp:HiddenField ID="Activities_completedTests_testName" runat="server" />
                                        <div class="activities_inner_cont_left">
                                            <asp:ImageButton ID="Activities_completedTestsImageButton" runat="server" SkinID="sknCompletedTestImage" />
                                        </div>
                                        <div class="activities_inner_cont_left">
                                            <asp:Label ID="Activities_completedTestsTitleLabel" runat="server" Text="Completed Tests"
                                                CssClass="self_label_maintext_head"></asp:Label>
                                        </div>
                                        <div class="activities_inner_cont_right">
                                            <uc3:PageNavigator ID="Activities_completedTestsPagingNavigator" runat="server" />
                                        </div>
                                    </div>
                                    <div class="activities_border_top">
                                    </div>
                                    <div style="clear: both">
                                        <asp:GridView ID="Activities_completedTestsGridView" runat="server" AllowSorting="True"
                                            ShowHeader="false" AutoGenerateColumns="False" Width="100%" OnRowDataBound="Activities_completedTestsGridView_RowDataBound"
                                            OnRowCommand="Activities_completedTestsGridView_RowCommand" SkinID="sknPendingActivities">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="Activities_completedTestsGridView_candidateSessionIDHiddenField"
                                                            Value='<%# Eval("CandidateSessionID") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_completedTestsGridView_attemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_completedTestsGridView_testIDHiddenField" Value='<%# Eval("TestID") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_completedTestsGridView_testNameHiddenField" Value='<%# Eval("TestName") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_completedTestsGridView_initiatedByHiddenField" Value='<%# Eval("InitiatedBy") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_completedTestsGridView_completedOnHiddenField" Value='<%# Eval("CompletedOn") %>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div class="activities_inner_grid_head">
                                                            <div class="activities_inner__grid_left_head">
                                                                <div style="float: left; width: 20px; padding-right: 10px">
                                                                    <asp:Label ID="Activities_completedTestsGridView_rowNumberLabel" runat="server" Text='<%# Eval("RowNumber") %>'
                                                                        SkinID="sknActivityNumberLabel"></asp:Label>
                                                                </div>
                                                                <div>
                                                                    <asp:Label ID="Activities_completedTestsGridView_testNameLabel" runat="server" Text='<%# Eval("TestName") %>'
                                                                        SkinID="sknActivityNameLabel"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="activities_inner__grid_right_head" style="padding-top: 4px; text-align: right">
                                                                <asp:Label ID="Activities_completedTestsSelfAdministeredLabel" runat="server" Text='Self Administered'
                                                                    SkinID="sknSelfAdministeredLabel" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "TRUE"%>'></asp:Label>
                                                                <asp:Label ID="Activities_completedTestsRecruiterAdministeredLabel" runat="server"
                                                                    Text='Recruiter Administered' SkinID="sknRecruiterAdministeredLabel" Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="activities_inner__grid_left_head" style="word-wrap: break-word; white-space: normal;">
                                                            <asp:Label ID="Activities_completedTestsGridView_testDescriptionLabel" runat="server"
                                                                Text='<%# Eval("TestDescription")==null ? Eval("TestDescription") : Eval("TestDescription").ToString().Replace("\n", "<br />") %>'
                                                                SkinID="sknActivityDescriptionLabel"></asp:Label>
                                                        </div>
                                                        <div class="activities_outer_grid">
                                                            <div class="activities_outer_grid_left">
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_completedTestsGridView_retakeRequestLinkButton" runat="server"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="RequestToRetake"
                                                                            Visible='<%# IsRetakeApplicable(Eval("InitiatedBy").ToString())%>' ToolTip="Retake Request"
                                                                            CssClass="self_label_text_link" Text="Retake Request"></asp:LinkButton>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_completedTestsGridView_viewCertificateLinkButton"
                                                                            runat="server" Text="View Certificate" ToolTip="View Certificate" CommandArgument='<%# Container.DataItemIndex %>'
                                                                            CommandName="viewcertificate" CssClass="self_label_text_link" Visible='<%# IsQualified(Eval("CertificationStatus").ToString())%>'></asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <asp:Label ID="Activities_completedTestsGridView_scheduledByValueLabel" SkinID="sknActivityValueLabel"
                                                                        runat="server" Text='<%# Eval("ScheduledByMessage") %>' Visible='<%# Eval("IsSelfAdmin").ToString().ToUpper() == "FALSE"%>'></asp:Label>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <asp:Label ID="Activities_completedTestsGridView_completedOnValueLabel" CssClass="activities_completed_orange_message"
                                                                        runat="server" Text='<%# GetCompletedTestDateFormat(Convert.ToDateTime(Eval("CompletedOn"))) %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="activities_outer_grid_right" style="padding-top: 50px">
                                                                <div style="float: left">
                                                                    <asp:Button ID="Activities_completedTestsGridView_viewResultsButton" runat="server"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" CommandName="TestResults" SkinID="sknButtonOrange"
                                                                        Text="View My Report" Visible='<%# IsShowResults(Eval("ShowResults").ToString())%>'>
                                                                    </asp:Button>
                                                                </div>
                                                                <div style="float: left" runat="server" id="Activities_completedTestsGridView_shareResultsDiv"
                                                                    visible='<%# IsShowResults(Eval("ShowResults").ToString())%>'>
                                                                    <div style="float: left; margin-left: 2px;">
                                                                        <asp:Label ID="Activities_completedTestsGridView_shareLabel" runat="server" CssClass="self_label_text_blue"
                                                                            Text="Share : "></asp:Label>
                                                                        <a name="fb_share" title="Click here to share your results link into Facebook" share_url='<%# GetFacebookShareURL(Eval("TestID").ToString(), Eval("CandidateSessionID").ToString(), Eval("AttemptID").ToString())%>'>
                                                                        </a>
                                                                        <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
                                                                    </div>
                                                                    <div style="float: left; margin-left: 2px;">
                                                                        <asp:HyperLink ID="Activities_completedTestsGridView_shareInLinkedInHyperLink" runat="server"
                                                                            ImageUrl="~/App_Themes/DefaultTheme/Images/linkedin.png" Target="_blank" ToolTip="Click here to share your results link into LinkedIn">
                                                                        </asp:HyperLink>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="activities_border_bottom">
                                                            </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="activities_outer_grid">
                                                    <asp:Label ID="Activities_completedTestsNoDataLabel" runat="server" Text="No completed tests found to display"
                                                        SkinID="sknNoActivityLabel"></asp:Label>
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="activities_empty_height">
                            &nbsp;
                        </div>
                        <div class="activities_outer_border" runat="server" id="Activities_pendingInterviewsDiv"
                            visible="false">
                            <asp:UpdatePanel ID="Activities_pendingInterviewsUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="activities_inner_cont">
                                        <div class="activities_inner_cont_left">
                                            <asp:ImageButton ID="Activities_pendingInterviewsImageButton" runat="server" SkinID="sknPendingInterviewImage" />
                                        </div>
                                        <div class="activities_inner_cont_left">
                                            <asp:Label ID="Activities_pendingInterviewsTitleLabel" runat="server" Text="Pending offline Interviews"
                                                CssClass="self_label_maintext_head"></asp:Label>
                                        </div>
                                        <div class="activities_inner_cont_right">
                                            <uc3:PageNavigator ID="Activities_pendingInterviewsPagingNavigator" runat="server" />
                                        </div>
                                    </div>
                                    <div class="activities_border_top">
                                    </div>
                                    <div style="clear: both" id="OfflineInterviewDiv">
                                        <asp:GridView ID="Activities_pendingInterviewsGridView" runat="server" AllowSorting="True"
                                            ShowHeader="false" AutoGenerateColumns="False" Width="100%" OnRowDataBound="Activities_pendingInterviewsGridView_RowDataBound"
                                            OnRowCommand="Activities_pendingInterviewsGridView_RowCommand" SkinID="sknPendingActivities">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="Activities_pendingInterviewsGridView_candidateSessionIDHiddenField"
                                                            Value='<%# Eval("CandidateSessionID") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingInterviewsGridView_attemptIDHiddenField" Value='<%# Eval("AttemptID") %>'
                                                            runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingInterviewsGridView_isPausedHiddenField" Value='<%# Eval("IsPaused") %>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div class="activities_inner_grid_head">
                                                            <div class="activities_inner__grid_left_head">
                                                                <div style="float: left; width: 20px; padding-right: 10px">
                                                                    <asp:Label ID="Activities_pendingInterviewsGridView_rowNumberLabel" runat="server"
                                                                        Text='<%# Eval("RowNumber") %>' SkinID="sknActivityNumberLabel"></asp:Label>
                                                                </div>
                                                                <div>
                                                                    <asp:Label ID="Activities_pendingInterviewsGridView_testNameLabel" runat="server"
                                                                        Text='<%# Eval("InterviewName") %>' SkinID="sknActivityNameLabel"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="activities_inner__grid_right_head" style="padding-top: 4px; text-align: right">
                                                                <asp:Label ID="Activities_pendingInterviewsGridViewExpiredLabel" runat="server" Text="Expired"
                                                                    SkinID="sknExpiredLabel" Visible='<%# Eval("IsExpired").ToString().ToUpper() == "TRUE"%>'></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="activities_inner__grid_left_head" style="word-wrap: break-word; white-space: normal;">
                                                            <asp:Label ID="Activities_pendingInterviewsGridView_testDescriptionLabel" runat="server"
                                                                Text='<%# Eval("InterviewDescription")==null ? Eval("InterviewDescription") : Eval("InterviewDescription").ToString().Replace("\n", "<br />") %>'
                                                                SkinID="sknActivityDescriptionLabel"></asp:Label>
                                                        </div>
                                                        <div class="activities_outer_grid">
                                                            <div class="activities_outer_grid_left">
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingInterviewsGridView_testIntroductionLinkButton"
                                                                            runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="InterviewIntroduction"
                                                                            Visible='<%# Eval("IsExpired").ToString().ToUpper() == "FALSE"%>' CssClass="self_label_text_link">Interview Introduction</asp:LinkButton>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingInterviewsGridView_testReminderLinkButton"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="InterviewReminder"
                                                                            Visible='<%# Eval("IsExpired").ToString().ToUpper() == "FALSE"%>' runat="server"
                                                                            CssClass="self_label_text_link">Interview Reminder</asp:LinkButton>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingInterviewsGridView_emailSchedulerLinkButton"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="EmailScheduler"
                                                                            runat="server" CssClass="self_label_text_link">Email Scheduler</asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <asp:Label ID="Activities_pendingInterviewsGridView_scheduledByValueLabel" SkinID="sknActivityValueLabel"
                                                                        runat="server" Text='<%# Eval("ScheduledByMessage") %>'></asp:Label>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingInterviewsGridView_testCreatedDateLabel" runat="server"
                                                                            Text="Scheduled On" SkinID="sknActivityCaptionLabel"></asp:Label>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingInterviewsGridView_testCreatedDateValueLabel" SkinID="sknActivityValueLabel"
                                                                            runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("InitiatedOn"))) %>'></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingInterviewsGridView_testExpiryDateLabel" runat="server"
                                                                            Text="Expired On" SkinID="sknActivityCaptionLabel"></asp:Label>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingInterviewsGridView_testExpiryDateValueLabel" SkinID="sknActivityValueLabel"
                                                                            runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("ExpiryDate"))) %>'></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="activities_outer_grid_right">
                                                                <asp:Button ID="Activities_pendingInterviewsGridView_startInterviewButton" SkinID="sknButtonOrange"
                                                                    runat="server" Text="Start Interview" Visible='<%# !IsPaused(Eval("IsPaused").ToString()) && Eval("IsExpired").ToString().ToUpper() == "FALSE"%>'
                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="StartInterview" />
                                                                <asp:Button ID="Activities_pendingInterviewsGridView_continueInterviewButton" SkinID="sknButtonOrange"
                                                                    runat="server" Text="Continue Interview" Visible='<%# IsPaused(Eval("IsPaused").ToString()) && Eval("IsExpired").ToString().ToUpper() == "FALSE"%>'
                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="ContinueInterview" />
                                                            </div>
                                                        </div>
                                                        <div class="activities_border_bottom">
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="activities_outer_grid">
                                                    <asp:Label ID="Activities_pendingInterviewNoDataLabel" runat="server" Text="No pending interviews found to display"
                                                        SkinID="sknNoActivityLabel"></asp:Label>
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="activities_empty_height">
                            &nbsp;
                        </div>
                        <div class="activities_outer_border" runat="server" id="Activities_pendingOnlineInterviewsDiv">
                            <asp:UpdatePanel ID="Activities_pendingOnlineInterviewsUpdatePanel" runat="server"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="activities_inner_cont">
                                        <div class="activities_inner_cont_left">
                                            <asp:ImageButton ID="Activities_pendingOnlineInterviews_TitleImageButton" runat="server" SkinID="sknPendingInterviewImage" />
                                        </div>
                                        <div class="activities_inner_cont_left">
                                            <asp:Label ID="Activities_pendingOnlineInterviews_TitleLabel" runat="server" Text="Pending Interviews" CssClass="self_label_maintext_head"></asp:Label>
                                        </div>
                                        <div class="activities_inner_cont_right">
                                            <uc3:PageNavigator ID="Activities_pendingOnlineInterviewsPagingNavigator" runat="server" />
                                        </div>
                                    </div>
                                    <div class="activities_border_top">
                                    </div>
                                    <div style="clear: both" id="OnlineInterviewDiv">
                                        <asp:GridView ID="Activities_pendingOnlineInterviewsGridView" runat="server" ShowFooter="false"
                                            ShowHeader="false" SkinID="sknPendingActivities" AutoGenerateColumns="False"
                                            OnRowCommand="Activities_pendingOnlineInterviewsGridView_RowCommand">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="Activities_pendingOnlineInterviewsGridView_candidateSessionIDHiddenField"
                                                            Value='<%# Eval("CandidateSessionID") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingOnlineInterviewsGridView_interviewIDHiddenField"
                                                            Value='<%# Eval("CandidateInterviewID") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_pendingOnlineInterviewsGridView_chatRoomIDHiddenField"
                                                            Value='<%# Eval("ChatRoomName") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div class="activities_inner_grid_head">
                                                            <div class="activities_inner__grid_left_head">
                                                                <div style="float: left; width: 20px; padding-right: 10px">
                                                                    <asp:Label ID="Activities_pendingOnlineInterviewsGridView_rowNumberLabel" runat="server"
                                                                        SkinID="sknActivityNumberLabel" Text='<%# Eval("RowNo") %>'></asp:Label>
                                                                </div>
                                                                <div>
                                                                    <asp:Label ID="Activities_pendingOnlineInterviewsGridView_testNameLabel" runat="server"
                                                                        Text='<%# Eval("InterviewName") %>' SkinID="sknActivityNameLabel"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="activities_inner__grid_right_head" style="padding-top: 4px; text-align: right">
                                                                <asp:Label ID="Activities_pendingOnlineInterviewsGridViewExpiredLabel" runat="server"
                                                                    Text="Expired" SkinID="sknExpiredLabel" Visible='<%# Eval("IsExpired").ToString().ToUpper() == "TRUE"%>'></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="activities_inner__grid_left_head" style="word-wrap: break-word; white-space: normal;">
                                                            <asp:Label ID="Activities_pendingOnlineInterviewsGridView_testDescriptionLabel" runat="server"
                                                                Text="" SkinID="sknActivityDescriptionLabel"></asp:Label>
                                                        </div>
                                                        <div class="activities_outer_grid">
                                                            <div class="activities_outer_grid_left">
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingOnlineInterviewsGridView_testIntroductionLinkButton"
                                                                            runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="InterviewIntroduction"
                                                                            CssClass="self_label_text_link" Visible='<%# Eval("IsExpired").ToString().ToUpper() == "FALSE"%>'>Interview Introduction</asp:LinkButton>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingOnlineInterviewsGridView_testReminderLinkButton"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="InterviewReminder"
                                                                            runat="server" CssClass="self_label_text_link" Visible="false">Interview Reminder</asp:LinkButton>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:LinkButton ID="Activities_pendingOnlineInterviewsGridView_emailSchedulerLinkButton"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="EmailScheduler"
                                                                            runat="server" CssClass="self_label_text_link" Visible="false">Email Scheduler</asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <asp:Label ID="Activities_pendingOnlineInterviewsGridView_scheduledByValueLabel"
                                                                        SkinID="sknActivityValueLabel" runat="server"></asp:Label>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingOnlineInterviewsGridView_testCreatedDateLabel" runat="server"
                                                                            Text="Scheduled On" SkinID="sknActivityCaptionLabel"></asp:Label>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingOnlineInterviewsGridView_testCreatedDateValueLabel"
                                                                            SkinID="sknActivityValueLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("InterviewDate"))) %>'></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingOnlineInterviewsGridView_testExpiryDateLabel" runat="server"
                                                                            Text="Expired On" SkinID="sknActivityCaptionLabel"></asp:Label>
                                                                    </div>
                                                                    <div class="activities_inner__grid_left">
                                                                        <asp:Label ID="Activities_pendingOnlineInterviewsGridView_testExpiryDateValueLabel"
                                                                            SkinID="sknActivityValueLabel" runat="server" Text='<%# GetDateFormat(Convert.ToDateTime(Eval("InterviewDate"))) %>'></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="activities_outer_grid_right">
                                                                <asp:Button ID="Activities_pendingOnlineInterviewsGridView_startInterviewButton"
                                                                    SkinID="sknButtonOrange" runat="server" Text="Start Interview" Visible='<%# Eval("IsExpired").ToString().ToUpper() == "FALSE"%>'
                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="StartInterview" />
                                                            </div>
                                                        </div>
                                                        <div class="activities_border_bottom">
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="activities_outer_grid">
                                                    <asp:Label ID="Activities_pendingOnlineInterviewNoDataLabel" runat="server" Text="No pending interviews found to display"
                                                        SkinID="sknNoActivityLabel"></asp:Label>
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="activities_empty_height">
                            &nbsp;
                        </div>
                        <div class="activities_outer_border" runat="server" id="Activities_completedInterviewsDiv"
                            visible="false">
                            <asp:UpdatePanel ID="Activities_completedInterviewsUpdatePanel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="activities_inner_cont">
                                        <div class="activities_inner_cont_left">
                                            <asp:ImageButton ID="Activities_completedInterviewsImageButton" runat="server" SkinID="sknCompletedInterviewImage" />
                                        </div>
                                        <div class="activities_inner_cont_left">
                                            <asp:Label ID="Activities_completedInterviewsTitleLabel" runat="server" Text="Completed Interviews"
                                                CssClass="self_label_maintext_head"></asp:Label>
                                        </div>
                                        <div class="activities_inner_cont_right">
                                            <uc3:PageNavigator ID="Activities_completedInterviewsPagingNavigator" runat="server" />
                                        </div>
                                    </div>
                                    <div class="activities_border_top">
                                    </div>
                                    <div style="clear: both">
                                        <asp:GridView ID="Activities_completedInterviewsGridView" runat="server" AllowSorting="True"
                                            ShowHeader="false" AutoGenerateColumns="False" Width="100%" OnRowDataBound="Activities_completedInterviewsGridView_RowDataBound"
                                            OnRowCommand="Activities_completedInterviewsGridView_RowCommand" SkinID="sknPendingActivities">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="Activities_completedInterviewsGridView_candidateSessionIDHiddenField"
                                                            Value='<%# Eval("CandidateSessionID") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_completedInterviewsGridView_attemptIDHiddenField"
                                                            Value='<%# Eval("AttemptID") %>' runat="server" />
                                                        <asp:HiddenField ID="Activities_completedInterviewsGridView_isPausedHiddenField"
                                                            Value='<%# Eval("IsPaused") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div class="activities_inner_grid_head">
                                                            <div style="float: left; width: 20px; padding-right: 10px">
                                                                <asp:Label ID="Activities_completedInterviewsGridView_rowNumberLabel" runat="server"
                                                                    Text='<%# Eval("RowNumber") %>' SkinID="sknActivityNumberLabel"></asp:Label>
                                                            </div>
                                                            <div>
                                                                <asp:Label ID="Activities_completedInterviewsGridView_testNameLabel" runat="server"
                                                                    Text='<%# Eval("InterviewName") %>' SkinID="sknActivityNameLabel"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="activities_inner__grid_left_head" style="word-wrap: break-word; white-space: normal;">
                                                            <asp:Label ID="Activities_completedInterviewsGridView_testDescriptionLabel" runat="server"
                                                                Text='<%# Eval("InterviewDescription")==null ? Eval("InterviewDescription") : Eval("InterviewDescription").ToString().Replace("\n", "<br />") %>'
                                                                SkinID="sknActivityDescriptionLabel"></asp:Label>
                                                        </div>
                                                        <div class="activities_outer_grid">
                                                            <div class="activities_outer_grid_left">
                                                                <div class="activities_inner_grid">
                                                                    <asp:Label ID="Activities_completedInterviewsGridView_scheduledByValueLabel" SkinID="sknActivityValueLabel"
                                                                        runat="server" Text='<%# Eval("ScheduledByMessage") %>'></asp:Label>
                                                                </div>
                                                                <div class="activities_inner_grid">
                                                                    <asp:Label ID="Activities_completedInterviewsGridView_completedDateValueLabel" CssClass="activities_completed_orange_message"
                                                                        runat="server" Text='<%# GetCompletedInterviewDateFormat(Convert.ToDateTime(Eval("CompletedOn"))) %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="activities_border_bottom">
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="activities_outer_grid">
                                                    <asp:Label ID="Activities_completedInterviewNoDataLabel" runat="server" Text="No completed interviews found to display"
                                                        SkinID="sknNoActivityLabel"></asp:Label>
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="activities_empty_height">
                            &nbsp;
                        </div>
                    </div>
                    <div class="activities_inner_div">
                        <asp:UpdatePanel ID="Activities_bottomMessageUpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="Activities_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="Activities_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingTestsPagingNavigator" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingTestsGridView" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_completedTestsPagingNavigator" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_completedTestsGridView" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingInterviewsPagingNavigator" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingInterviewsGridView" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_completedInterviewsPagingNavigator" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_completedInterviewsGridView" />
                                <asp:AsyncPostBackTrigger ControlID="Activities_pendingOnlineInterviewsGridView" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
