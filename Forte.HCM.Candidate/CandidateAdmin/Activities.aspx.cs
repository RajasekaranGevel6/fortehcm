﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Activities.aspx.cs
// File that represents the user interface layout and functionalities for the
// Activities page. This page helps in viewing the pending and completed 
// activities (tests & interviews) for the logged in candidate and also 
// provides the facility to take test/interview, set reminder, etc.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using Forte.HCM.UI.CommonControls;
using System.Globalization;

#endregion Directives                                                          

namespace Forte.HCM.UI.CandidateCenter
{
    /// <summary>
    /// File that represents the user interface layout and functionalities for 
    /// the Activities page. This page helps in viewing the pending and 
    /// completed activities (tests & interviews) for the logged in candidate 
    /// and also provides the facility to take test/interview, set reminder, 
    /// etc. This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class Activities : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                // Set browser title.
                Master.SetPageCaption("Activities");

                // Subscribe to paging control events.
                SubscribePagingEvents();

                if (!IsPostBack)
                {
                    if (Utility.IsNullOrEmpty(ViewState["PENDING_INTERVIEWS_SORT_ORDER"]))
                        ViewState["PENDING_INTERVIEWS_SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["PENDING_INTERVIEWS_SORT_FIELD"]))
                        ViewState["PENDING_INTERVIEWS_SORT_FIELD"] = "EXPIRYDATE";

                    if (Utility.IsNullOrEmpty(ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"]))
                        ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"] = SortType.Descending;

                    if (Utility.IsNullOrEmpty(ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"]))
                        ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"] = "COMPLETEDDATE";

                    if (Utility.IsNullOrEmpty(ViewState["COMPLETED_TESTS_SORT_ORDER"]))
                        ViewState["COMPLETED_TESTS_SORT_ORDER"] = SortType.Ascending;

                    if (Utility.IsNullOrEmpty(ViewState["COMPLETED_TESTS_SORT_FIELD"]))
                        ViewState["COMPLETED_TESTS_SORT_FIELD"] = "TESTNAME";

                    // Load pending, completed and expired tests. 
                    LoadValues();

                    if (Session["ACTIVITY_DELETED"] != null && Convert.ToBoolean(Session["ACTIVITY_DELETED"]) == true)
                    {
                        // Show message.
                        base.ShowMessage(Activities_topSuccessMessageLabel,
                            Activities_bottomSuccessMessageLabel, "Test deleted successfully");

                        // Clear delete status from session.
                        Session["ACTIVITY_DELETED"] = null;
                    }
                }

                // Show activity.
                ShowActivity();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of pending tests section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void Activities_pendingTestsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowPendingTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of completed tests section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void Activities_completedTestsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowCompletedTests(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of pending interviews section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void Activities_pendingInterviewsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowPendingInterviews(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of pending interviews section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void Activities_pendingOnlineInterviewsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                SetSearchInterviewParam(e.PageNumber, "ONLINE_INTERVIEW_DATE", SortType.Descending);
                GetOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
                Activities_pendingOnlineInterviewsPagingNavigator.MoveToPage(e.PageNumber); 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the page number is clicked
        /// in the paging control of completed interviews section.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="PageNumberEventArgs"/>that holds the event data.
        /// </param>
        protected void Activities_completedInterviewsPagingNavigator_PageNumberClick
            (object sender, PageNumberEventArgs e)
        {
            try
            {
                ShowCompletedInterviews(e.PageNumber);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void Activities_pendingTestsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TestIntroduction")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_attemptIDHiddenField") as HiddenField).Value);

                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    Response.Redirect("~/TestCenter/TestIntroduction.aspx?" +
                        "&candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES +
                        "&displaytype=" + Request.QueryString["displaytype"], false);
                }
                else if (e.CommandName == "TestReminder")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_attemptIDHiddenField") as HiddenField).Value);

                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    Response.Redirect("~/Popup/TestReminder.aspx" +
                        "?candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&displaytype=" + Request.QueryString["displaytype"] +
                        "&activitytype=TST" +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES, false);
                }
                else if (e.CommandName == "EmailScheduler")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_attemptIDHiddenField") as HiddenField).Value);

                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    Response.Redirect("../Popup/SendEmail.aspx" +
                        "?type=SCH" +
                        "&candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&activitytype=TST" +
                        "&displaytype=" + Request.QueryString["displaytype"] +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES, false);
                }
                else if (e.CommandName == "StartTest")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_attemptIDHiddenField") as HiddenField).Value);
                    bool isScheduled = Convert.ToBoolean((Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_isScheduledHiddenField") as HiddenField).Value);
                    int testRecommendationID = Convert.ToInt32((Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_testRecommendationIDHiddenField") as HiddenField).Value);
                    int totalQuestions = Convert.ToInt32((Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_totalQuestionsHiddenField") as HiddenField).Value);

                    ViewState["RECOMMENDED_TIME"] = (Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_timeLimitHiddenField") as HiddenField).Value;

                    // Keep the test name in hidden field.
                    Activities_pendingTests_testName.Value = (Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_testNameHiddenField") as HiddenField).Value;

                    if (isScheduled)
                    {
                        // Keep page number in session.
                        Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                        Response.Redirect("~/TestCenter/TestInstructions.aspx?" +
                            "&candidatesessionid=" + candidateSessionID +
                            "&attemptid=" + attemptID +
                            "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES +
                            "&displaytype=" + Request.QueryString["displaytype"], false);
                    }
                    else
                    {
                        List<QuestionDetail> questions = null;

                        try
                        {
                            DataTable criteriaTable = GetCriteriaTable(testRecommendationID);

                            questions = GetQuestions(totalQuestions, ref criteriaTable);
                        }
                        catch (Exception exp)
                        {
                            Logger.ExceptionLog(exp);
                        }

                        // Check if questions found.
                        if (questions == null || questions.Count == 0)
                        {
                            base.ShowMessage(Activities_topErrorMessageLabel,
                                Activities_bottomErrorMessageLabel, "No test/questions generated to proceed.");
                            return;
                        }

                        // Save test for the list of questions.
                        string testKey = string.Empty;
                        bool saved = SaveTest(questions, out testKey);

                        if (saved == false)
                        {
                            base.ShowMessage(Activities_topErrorMessageLabel,
                                Activities_bottomErrorMessageLabel, "Unable to save the test and proceed.");

                            return;
                        }

                        // Create test session, schedule candidate and proceed 
                        // to test instructions page.
                        string newCandidateSessionID = string.Empty;
                        bool scheduled = ScheduleCandidate(testKey, out newCandidateSessionID);

                        if (testRecommendationID != 0)
                        {
                            // Update the test key & scheduled status.
                            new CandidateBLManager().UpdateCandidateTestRecommendationStatus
                                (base.userID, testRecommendationID, testKey,
                                Constants.CandidateSelfAdminTestStatus.SCHEDULED);
                        }

                        if (scheduled == false)
                        {
                            base.ShowMessage(Activities_topErrorMessageLabel,
                                Activities_bottomErrorMessageLabel, "Unable to schedule the test and proceed.");

                            return;
                        }

                        // Keep page number in session.
                        Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                        // Construct the url for the test instructions page.
                        string url = "~/TestCenter/TestInstructions.aspx?" +
                            "&candidatesessionid=" + newCandidateSessionID +
                            "&attemptid=" + Constants.CandidateSelfAdminTestConstants.ATTEMPT_ID +
                            "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES +
                            "&displaytype=" + Request.QueryString["displaytype"];

                        // Navigates to the test instructions page to proceed to take
                        // the generated test.
                        Response.Redirect(url, false);
                    }
                }
                else if (e.CommandName == "DeleteTest")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    int testRecommendationID = Convert.ToInt32((Activities_pendingTestsGridView.Rows
                        [index].FindControl("Activities_pendingTestsGridView_testRecommendationIDHiddenField") as HiddenField).Value);
                    
                    // Delete the recommended test.
                    new CandidateBLManager().DeleteCandidateRecommendedTest(base.userID, testRecommendationID);
                    
                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    // Keep delete status in session.
                    Session["ACTIVITY_DELETED"] = true;

                    // Reload the page.
                    Response.Redirect(Request.RawUrl);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the pending tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void Activities_pendingTestsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton Activities_pendingTestsGridView_deleteTestLinkButton =
                        (LinkButton)e.Row.FindControl("Activities_pendingTestsGridView_deleteTestLinkButton");

                    Button Activities_pendingTestsGridView_deleteConfirmationNoButton =
                        (Button)e.Row.FindControl("Activities_pendingTestsGridView_deleteConfirmationNoButton");
                    HtmlContainerControl Activities_pendingTestsGridView_deleteConfirmationDiv =
                        (HtmlContainerControl)e.Row.FindControl("Activities_pendingTestsGridView_deleteConfirmationDiv");
                    
                    Activities_pendingTestsGridView_deleteTestLinkButton.Attributes.Add("onclick", "return ShowDeleteConfirmationPanel('" +
                        Activities_pendingTestsGridView_deleteConfirmationDiv.ClientID + "')");
                    Activities_pendingTestsGridView_deleteConfirmationNoButton.Attributes.Add("onclick", "return ShowDeleteConfirmationPanel('" +
                        Activities_pendingTestsGridView_deleteConfirmationDiv.ClientID + "')");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void Activities_completedTestsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TestResults")
                {
                    int index = Convert.ToInt32(e.CommandArgument);

                    string candidateSessionID = (Activities_completedTestsGridView.Rows
                        [index].FindControl("Activities_completedTestsGridView_candidateSessionIDHiddenField") as HiddenField).Value;

                    int attemptID = Convert.ToInt32((Activities_completedTestsGridView.Rows
                        [index].FindControl("Activities_completedTestsGridView_attemptIDHiddenField") as HiddenField).Value);

                    string testID = (Activities_completedTestsGridView.Rows
                       [index].FindControl("Activities_completedTestsGridView_testIDHiddenField") as HiddenField).Value;

                    Response.Redirect("~/TestCenter/CandidateTestResult.aspx" +
                        "?candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES + 
                        "&displaytype=" + Request.QueryString["displaytype"] + 
                        "&mode=SHARE" +
                        "&testkey=" + testID, false);
                }
                else if (e.CommandName == "viewcertificate")
                {
                    int rowIndex = Convert.ToInt32(e.CommandArgument);

                    // Find link button in the gridview.
                    LinkButton viewCertificateImageButton = (Activities_completedTestsGridView.Rows
                        [rowIndex].FindControl("Activities_completedTestsGridView_viewCertificateLinkButton") as LinkButton);

                    string candidateSessionID = (Activities_completedTestsGridView.Rows
                        [rowIndex].FindControl("Activities_completedTestsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_completedTestsGridView.Rows
                        [rowIndex].FindControl("Activities_completedTestsGridView_attemptIDHiddenField") as HiddenField).Value);
                    string testKey = (Activities_completedTestsGridView.Rows
                       [rowIndex].FindControl("Activities_completedTestsGridView_testIDHiddenField") as HiddenField).Value;
                    string testName = (Activities_completedTestsGridView.Rows
                       [rowIndex].FindControl("Activities_completedTestsGridView_testNameHiddenField") as HiddenField).Value;
                    string testCompletedOn = (Activities_completedTestsGridView.Rows
                       [rowIndex].FindControl("Activities_completedTestsGridView_completedOnHiddenField") as HiddenField).Value;

                    Response.Redirect("~/Popup/ViewCertificate.aspx" +
                       "?candidatesessionid=" + candidateSessionID +
                       "&attemptid=" + attemptID +
                       "&certificatetype=CERTIFICATE" +
                       "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES +
                       "&displaytype=" + Request.QueryString["displaytype"], false);
                }
                else if (e.CommandName == "RequestToRetake")
                {
                    int rowIndex = Convert.ToInt32(e.CommandArgument);

                    string candidateSessionID = (Activities_completedTestsGridView.Rows
                       [rowIndex].FindControl("Activities_completedTestsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_completedTestsGridView.Rows
                        [rowIndex].FindControl("Activities_completedTestsGridView_attemptIDHiddenField") as HiddenField).Value);
                    string testID = (Activities_completedTestsGridView.Rows
                        [rowIndex].FindControl("Activities_completedTestsGridView_testIDHiddenField") as HiddenField).Value;


                    if (IsValidRetake(candidateSessionID, testID, attemptID))
                    {
                        Response.Redirect("~/Popup/CandidateRequest.aspx" +
                            "?candidatesessionid=" + candidateSessionID +
                            "&attemptid=" + attemptID +
                            "&requesttype=" + "retake" + 
                            "&displaytype=" + Request.QueryString["displaytype"] +
                            "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES, false);
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the completed tests section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void Activities_completedTestsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string testKey = (e.Row.FindControl("Activities_completedTestsGridView_testIDHiddenField") as HiddenField).Value;
                    string candidateSessionID = (e.Row.FindControl("Activities_completedTestsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    string attemptID = (e.Row.FindControl("Activities_completedTestsGridView_attemptIDHiddenField") as HiddenField).Value;
                    string testName = (e.Row.FindControl("Activities_completedTestsGridView_testNameHiddenField") as HiddenField).Value;
                        
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                    string resultUrl = ConfigurationManager.AppSettings["CANDIDATE_TEST_RESULT_URL"];
                    resultUrl += "?testkey=" + testKey;
                    resultUrl += "&parentpage=CAND_HOME";
                    resultUrl += "&candidatesessionid=" + candidateSessionID;
                    resultUrl += "&attemptid=" + attemptID;

                    // Assign linked in profile.
                    HyperLink Activities_completedTestsGridView_shareInLinkedInHyperLink = e.Row.FindControl("Activities_completedTestsGridView_shareInLinkedInHyperLink") as HyperLink;

                    string url = string.Format("http://www.linkedin.com/shareArticle?mini=true&url={0}&title={1}&summary={2}&source=www.fortehcm.com",
                        resultUrl, "Test results of " + userDetail.FullName, testName);
                    Activities_completedTestsGridView_shareInLinkedInHyperLink.NavigateUrl = url; 

                    // Assign facebook share.


                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the pending interviews section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void Activities_pendingInterviewsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "InterviewIntroduction")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_attemptIDHiddenField") as HiddenField).Value);

                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    Response.Redirect("~/InterviewCenter/InterviewIntroduction.aspx?" +
                        "&candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES + 
                        "&displaytype=" + Request.QueryString["displaytype"], false);
                }
                else if (e.CommandName == "InterviewReminder")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_attemptIDHiddenField") as HiddenField).Value);

                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    Response.Redirect("~/Popup/TestReminder.aspx" +
                        "?candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&activitytype=INT" +
                        "&displaytype=" + Request.QueryString["displaytype"] +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES, false);
                }
                else if (e.CommandName == "EmailScheduler")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_attemptIDHiddenField") as HiddenField).Value);

                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    Response.Redirect("../Popup/SendEmail.aspx" +
                        "?type=SCH" +
                        "&candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&activitytype=INT" +
                        "&displaytype=" + Request.QueryString["displaytype"] +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES, false);
                }
                else if (e.CommandName == "StartInterview")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_attemptIDHiddenField") as HiddenField).Value);

                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    Response.Redirect("~/InterviewCenter/InterviewInstructions.aspx?" +
                        "&candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES +
                        "&displaytype=" + Request.QueryString["displaytype"], false);
                }
                else if (e.CommandName == "ContinueInterview")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int attemptID = Convert.ToInt32((Activities_pendingInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingInterviewsGridView_attemptIDHiddenField") as HiddenField).Value);

                    // Keep page number in session.
                    Session["ACTIVITIES_PAGE_NUMBER"] = ViewState["PAGE_NUMBER"];

                    Response.Redirect("~/InterviewCenter/ContinueInterviewInstructions.aspx?" +
                        "&candidatesessionid=" + candidateSessionID +
                        "&attemptid=" + attemptID +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES +
                        "&displaytype=" + Request.QueryString["displaytype"], false);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the pending interviews section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void Activities_pendingInterviewsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row command event is 
        /// fired in the completed interviews section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewCommandEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row command event will be fired when an event is triggered in the 
        /// row.
        /// </remarks>
        protected void Activities_completedInterviewsGridView_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            try
            {
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the row data bound event is 
        /// fired in the completed interviews section grid.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewRowEventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Row data bound event will be fired when the data is being bind to
        /// the row.
        /// </remarks>
        protected void Activities_completedInterviewsGridView_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that shows the activity based on the display type.
        /// </summary>
        private void ShowActivity()
        {
            // Hide all divs.
            Activities_pendingTestsDiv.Visible = false;
            Activities_completedTestsDiv.Visible = false;
            Activities_pendingInterviewsDiv.Visible = false;
            Activities_completedInterviewsDiv.Visible = false;
            Activities_pendingOnlineInterviewsDiv.Visible = false;

            // Check if display type query string is passed or not.
            if (Utility.IsNullOrEmpty(Request.QueryString["displaytype"]))
            {
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, "The display type parameter is missing");

                return;
            }

            // Retrieve display type.
            string displayType = Request.QueryString["displaytype"].ToUpper();

            if (displayType == "PT")
                Activities_pendingTestsDiv.Visible = true;
            else if (displayType == "CT")
                Activities_completedTestsDiv.Visible = true;
            else if (displayType == "PI")
                Activities_pendingInterviewsDiv.Visible = true;
            else if (displayType == "CI")
                Activities_completedInterviewsDiv.Visible = true;
            else if (displayType == "OI")
                Activities_pendingOnlineInterviewsDiv.Visible = true;
            else if (displayType == "ALL")
            {
                Activities_pendingTestsDiv.Visible = true;
                Activities_completedTestsDiv.Visible = true;
                Activities_pendingInterviewsDiv.Visible = true;
                Activities_completedInterviewsDiv.Visible = true;
                Activities_pendingOnlineInterviewsDiv.Visible = true;
            }
            else
            {
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, "The display type parameter is invalid");
            }
        }

        /// <summary>
        /// Method that loads the questions for the given criteria.
        /// </summary>
        /// <param name="noOfQuestions">
        /// A <see cref="int"/> that holds the number of questions.
        /// </param>
        /// <param name="criteriaTable">
        /// A <see cref="DataTable"/> that holds the search criteria.
        /// </param>
        private List<QuestionDetail> GetQuestions(int noOfQuestions, ref DataTable criteriaTable)
        {
            int TotalWeightage = 0;
            bool ZeroExpectedQuestion = false;
            LoadWeightages(ref criteriaTable, noOfQuestions, out TotalWeightage);
            if (TotalWeightage > 100)
                return null;

            LoadExpectedQuestions(ref criteriaTable, noOfQuestions, out ZeroExpectedQuestion);
            if (ZeroExpectedQuestion)
                return null;

            return new QuestionBLManager().
                GetRecommendedAutomatedQuestions(QuestionType.MultipleChoice,
                GetQuestionSearchCriteria(ref criteriaTable),
                noOfQuestions, ref criteriaTable, 0, base.userID);
        }

        private void LoadExpectedQuestions(ref DataTable dtSearchCriteria, int NoofQuestions, out bool ZeroExpectedQuestion)
        {
            ZeroExpectedQuestion = false;
            int SearchSegements = dtSearchCriteria.Rows.Count;
            int QuestionsAdded = 0;
            int ExpectedQuestions = 0;
            int LoopI = 0;
            for (LoopI = 0; LoopI < SearchSegements; LoopI++)
            {
                QuestionsAdded = Convert.ToInt32((Convert.ToDecimal(dtSearchCriteria.Rows[LoopI]["Weightage"])
                    / 100) * NoofQuestions);
                dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] = QuestionsAdded;
                ExpectedQuestions += QuestionsAdded;
            }
            if (ExpectedQuestions > NoofQuestions)
            {
                int QuestionsToAdd = ExpectedQuestions - NoofQuestions;
                for (LoopI = SearchSegements - 1; LoopI >= 0; LoopI--)
                {
                    if (Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd <= 0)
                        continue;
                    dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd;
                    break;
                }
            }
            else if (ExpectedQuestions < NoofQuestions)
                dtSearchCriteria.Rows[LoopI - 1]["ExpectedQuestions"] = QuestionsAdded +
                    (NoofQuestions - ExpectedQuestions);
            if (dtSearchCriteria.Select("ExpectedQuestions <= 0").Length > 0)
            {
                ZeroExpectedQuestion = true;
            }
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);

                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                    dtSearchCriteria.Rows[LoopI]["PickedQuestions"] = 0;
                    dtSearchCriteria.Rows[LoopI]["TotalRecordsinDB"] = 0;
                    dtSearchCriteria.Rows[LoopI]["QuestionsDifference"] = 0;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// Method that constructs and returns the question search criteria list object.
        /// </summary>
        /// <param name="dtSearchCriteria"></param>
        /// <returns></returns>
        private List<TestSearchCriteria> GetQuestionSearchCriteria(ref DataTable dtSearchCriteria)
        {
            List<TestSearchCriteria> testSearchCriterias = null;
            TestSearchCriteria testSearchCriteria = null;
            Subject subject = null;

            for (int i = 0; i < dtSearchCriteria.Rows.Count; i++)
            {
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria))
                    testSearchCriteria = new TestSearchCriteria();
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Cat_sub_id"]))
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(subject))
                        subject = new Subject();
                    subject.SubjectID = Convert.ToInt32(dtSearchCriteria.Rows[i]["Cat_sub_id"]);
                    testSearchCriteria.Subjects = new List<Subject>();
                    testSearchCriteria.Subjects.Add(subject);
                }
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["TestArea"]))
                    testSearchCriteria.TestAreasID = dtSearchCriteria.Rows[i]["TestArea"].ToString();
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Complexity"]))
                    testSearchCriteria.Complexity = dtSearchCriteria.Rows[i]["Complexity"].ToString();
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Keyword"]))
                    testSearchCriteria.Keyword = dtSearchCriteria.Rows[i]["Keyword"].ToString();
                testSearchCriteria.NoOfQuestions = Convert.ToInt32(dtSearchCriteria.Rows[i]["expectedquestions"]);
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias))
                    testSearchCriterias = new List<TestSearchCriteria>();
                testSearchCriterias.Add(testSearchCriteria);
                testSearchCriteria = null;
                subject = null;
            }
            return testSearchCriterias;
        }

        /// <summary>
        /// Method that constructs and returns the criteria table for the given
        /// recommended test ID.
        /// </summary>
        /// <param name="recommendedTestID">
        /// A <see cref="int"/> that holds the recommended test ID.
        /// </param>
        /// <returns>
        /// A <see cref="DataTable"/> that holds the criteria table.
        /// </returns>
        private DataTable GetCriteriaTable(int recommendedTestID)
        {
            // Get search criteria for the given recommended test ID.
            List<TestSearchCriteria> testSearchCriterias = new TestBLManager().
                GetSearchCriteria(recommendedTestID);

            // Check if criteria present.
            if (testSearchCriterias == null || testSearchCriterias.Count == 0)
                return null;

            // Construct test segment criteria table.
            DataTable criteriaTable = GetTestSegmentCriteriaTable();

            DataRow drNewRow = null;

            int SNo = 1;

            // Fill the segment criteria rows.
            foreach (TestSearchCriteria criteria in testSearchCriterias)
            {
                drNewRow = criteriaTable.NewRow();
                drNewRow["SNO"] = SNo++.ToString();
                drNewRow["Cat_sub_id"] = criteria.CategoriesID;
                drNewRow["Weightage"] = criteria.Weightage; ;
                drNewRow["Keyword"] = criteria.Keyword;
                drNewRow["TestArea"] = criteria.TestAreasID;
                drNewRow["Complexity"] = criteria.Complexity;
                criteriaTable.Rows.Add(drNewRow);
                drNewRow = null;
            }
            return criteriaTable;
        }

        /// <summary>
        /// Method that construct and returns the test segment criteria table.
        /// </summary>
        /// <param name="criteriaTable">
        /// A <see cref="DataTable"> that holds the criteria table.
        /// </param>
        private DataTable GetTestSegmentCriteriaTable()
        {
            DataTable criteriaTable = new DataTable();
            criteriaTable.Columns.Add("SNO", typeof(int));
            criteriaTable.Columns.Add("Cat_sub_id", typeof(string));
            criteriaTable.Columns.Add("Complexity", typeof(string));
            criteriaTable.Columns.Add("TestArea", typeof(string));
            criteriaTable.Columns.Add("Category", typeof(string));
            criteriaTable.Columns.Add("Subject", typeof(string));
            criteriaTable.Columns.Add("Weightage", typeof(int));
            criteriaTable.Columns.Add("Keyword", typeof(string));
            criteriaTable.Columns.Add("ExpectedQuestions", typeof(int));
            criteriaTable.Columns.Add("PickedQuestions", typeof(int));
            criteriaTable.Columns.Add("TotalRecordsinDB", typeof(int));
            criteriaTable.Columns.Add("QuestionsDifference", typeof(int));
            criteriaTable.Columns.Add("Remarks", typeof(string));

            return criteriaTable;
        }

        /// <summary>
        /// Method that saves the question set as a new test.
        /// </summary>
        /// <param name="questions">
        /// A list of <see cref="QuestionDetail"/> that holds the questions.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key as an output
        /// parameter.
        /// </param>
        /// <remarks>
        /// A <see cref="bool"/> that holds the status. True represents success
        /// and false represents failure.
        /// </remarks>
        private bool SaveTest(List<QuestionDetail> questions, out string testKey)
        {
            // Assign default value to test key.
            testKey = string.Empty;

            List<decimal> creditEarned = new List<decimal>();
            List<decimal> timeTaken = new List<decimal>();

            // Construct sum of credits earned and average time.
            foreach (QuestionDetail question in questions)
            {
                creditEarned.Add(question.CreditsEarned);
                timeTaken.Add(question.AverageTimeTaken);
            }

            // Construct test detail object.
            TestDetail testDetail = new TestDetail();

            string testName = Activities_pendingTests_testName.Value.Trim();
            testDetail.Name = testName;
            testDetail.Description = testName;
            testDetail.Questions = questions;
            testDetail.IsActive = true;
            testDetail.IsCertification = false;
            testDetail.CertificationId = 0;
            testDetail.CreatedBy = userID;
            testDetail.IsDeleted = false;
            testDetail.ModifiedBy = userID;
            testDetail.NoOfQuestions = questions.Count;
            testDetail.SystemRecommendedTime =
                Convert.ToInt32(timeTaken.Average()) * questions.Count;
            testDetail.RecommendedCompletionTime = testDetail.SystemRecommendedTime;

            testDetail.TestAuthorID = userID;
            testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
            testDetail.TestCreationDate = DateTime.Now;
            testDetail.TestStatus = TestStatus.Active;
            testDetail.TestType = TestType.Genenal;
            testDetail.TestMode = "SELF_ADMIN";
            testDetail.TestKey = "";

            // Get complexity.
            string complexity = new ControlUtility().GetComplexity
                (Constants.AttributeTypes.COMPLEXITY,
                questions).AttributeID.ToString();

            // Save the test and retrieve the test key.
            testKey = new TestBLManager().SaveTest(testDetail, userID, complexity);

            return true;
        }

        /// <summary>
        /// Method that creates the test session and schedules the candidate
        /// for the test.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID as an
        /// output parameter.
        /// </param>
        /// <remarks>
        /// A <see cref="bool"/> that holds the status. True represents success
        /// and false represents failure.
        /// </remarks>
        private bool ScheduleCandidate(string testKey, out string candidateSessionID)
        {
            // Assign default value to test key.
            candidateSessionID = string.Empty;

            // Construct test session detail.
            TestSessionDetail sessionDetail = GetTestSessionDetail(testKey);

            if (sessionDetail == null)
                return false;

            // Construct test schedule detail.
            TestScheduleDetail scheduleDetail = new TestScheduleDetail();
            scheduleDetail.CandidateID = base.userID.ToString();
            scheduleDetail.AttemptID = Constants.CandidateSelfAdminTestConstants.ATTEMPT_ID;
            scheduleDetail.EmailId = ((UserDetail)Session["USER_DETAIL"]).Email;
            scheduleDetail.ExpiryDate = DateTime.Now.AddDays(base.candidateExpiryDays);

            // Schedule the candidate.
            new TestBLManager().SaveTestSessionScheduleCandidate(sessionDetail, scheduleDetail,
                base.userID, out candidateSessionID);

            return true;
        }

        /// <summary>
        /// Method that constructs and returns the test session detail.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that holds the test session detail.
        /// </returns>
        private TestSessionDetail GetTestSessionDetail(string testKey)
        {
            // Get test detail.
            TestDetail testDetail = new TestBLManager().GetTestAndCertificateDetail(testKey);

            if (testDetail == null)
                return null;

            // Construct test session detail object.
            TestSessionDetail sessionDetail = new TestSessionDetail();

            sessionDetail.TestID = testDetail.TestKey;
            sessionDetail.TestName = testDetail.Name;

            // Set number of candidate session (session count)
            sessionDetail.NumberOfCandidateSessions = 1;

            // Set total credits limit
            sessionDetail.TotalCredit = testDetail.TestCost;

            // Set time limit
            sessionDetail.TimeLimit = Convert.ToInt32(ViewState["RECOMMENDED_TIME"]);

            sessionDetail.ClientRequestID = "0";

            // Set expiry date
            sessionDetail.ExpiryDate = DateTime.Now;

            // Set random question order status
            sessionDetail.IsRandomizeQuestionsOrdering = false;

            // Set display result status 
            sessionDetail.IsDisplayResultsToCandidate = true;

            // Set cyber proctoring status
            sessionDetail.IsCyberProctoringEnabled = false;

            // Set created by
            sessionDetail.CreatedBy = base.userID;

            // Set modified by
            sessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            sessionDetail.Instructions = Constants.CandidateSelfAdminTestConstants.TEST_INSTRUCTIONS;
            // Set session descriptions
            sessionDetail.TestSessionDesc = Constants.CandidateSelfAdminTestConstants.SESSION_DESCRIPTION;

            return sessionDetail;
        }

        /// <summary>
        /// Method that subscribes the paging control events.
        /// </summary>
        private void SubscribePagingEvents()
        {
            Activities_pendingTestsPagingNavigator.PageNumberClick += new
                RecommendedTestPageNavigator.PageNumberClickEventHandler
                   (Activities_pendingTestsPagingNavigator_PageNumberClick);

            Activities_completedTestsPagingNavigator.PageNumberClick += new
                RecommendedTestPageNavigator.PageNumberClickEventHandler
                   (Activities_completedTestsPagingNavigator_PageNumberClick);

            Activities_pendingInterviewsPagingNavigator.PageNumberClick += new
                RecommendedTestPageNavigator.PageNumberClickEventHandler
                   (Activities_pendingInterviewsPagingNavigator_PageNumberClick);

            Activities_completedInterviewsPagingNavigator.PageNumberClick += new
               RecommendedTestPageNavigator.PageNumberClickEventHandler
                  (Activities_completedInterviewsPagingNavigator_PageNumberClick);

            Activities_pendingOnlineInterviewsPagingNavigator.PageNumberClick += new
                RecommendedTestPageNavigator.PageNumberClickEventHandler
                   (Activities_pendingOnlineInterviewsPagingNavigator_PageNumberClick); 
        }

        /// <summary>
        /// Method that shows the list of pending tests for the given page 
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowPendingTests(int pageNumber)
        {
            // Keep page number in view state.
            ViewState["PAGE_NUMBER"] = pageNumber;

            int totalRecords = 0;
            List<CandidateTestSessionDetail> pendingTests = new CandidateBLManager().
                GetPendingTestActivities(base.userID, pageNumber, base.activitiesGridPageSize, out totalRecords);

            Activities_pendingTestsGridView.DataSource = pendingTests;
            Activities_pendingTestsGridView.DataBind();

            Activities_pendingTestsPagingNavigator.PageSize = base.activitiesGridPageSize;
            Activities_pendingTestsPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that shows the list of completed tests for the given page
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowCompletedTests(int pageNumber)
        {
            int totalRecords;

            List<CandidateTestDetail> completedTests = new CandidateBLManager().GetTests
                (CandidateTestStatus.Completed, base.userID,
                ViewState["COMPLETED_TESTS_SORT_FIELD"].ToString(),
                ((SortType)ViewState["COMPLETED_TESTS_SORT_ORDER"]),
                pageNumber, base.activitiesGridPageSize, out totalRecords);

            Activities_completedTestsGridView.DataSource = completedTests;
            Activities_completedTestsGridView.DataBind();

            Activities_completedTestsPagingNavigator.PageSize = base.activitiesGridPageSize;
            Activities_completedTestsPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that shows the list of pending interview for the given page 
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowPendingInterviews(int pageNumber)
        {
            int totalRecords;

            List<CandidateInterviewDetail> pendingInterviews = new CandidateBLManager().
                GetPendingInterviews(base.userID,
                ViewState["PENDING_INTERVIEWS_SORT_FIELD"].ToString(),
                ((SortType)ViewState["PENDING_INTERVIEWS_SORT_ORDER"]),
                pageNumber, base.activitiesGridPageSize, out totalRecords);

            Activities_pendingInterviewsGridView.DataSource = pendingInterviews;
            Activities_pendingInterviewsGridView.DataBind();

            Activities_pendingInterviewsPagingNavigator.PageSize = base.activitiesGridPageSize;
            Activities_pendingInterviewsPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that shows the list of completed interview for the given page 
        /// number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        private void ShowCompletedInterviews(int pageNumber)
        {
            int totalRecords;

            List<CandidateInterviewDetail> completedInterviews = new CandidateBLManager().
                GetCompletedInterviews(base.userID,
                ViewState["COMPLETED_INTERVIEWS_SORT_FIELD"].ToString(),
                ((SortType)ViewState["COMPLETED_INTERVIEWS_SORT_ORDER"]),
                pageNumber, base.activitiesGridPageSize, out totalRecords);

            Activities_completedInterviewsGridView.DataSource = completedInterviews;
            Activities_completedInterviewsGridView.DataBind();

            Activities_completedInterviewsPagingNavigator.PageSize = base.activitiesGridPageSize;
            Activities_completedInterviewsPagingNavigator.TotalRecords = totalRecords;
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            Activities_topErrorMessageLabel.Text = string.Empty;
            Activities_bottomErrorMessageLabel.Text = string.Empty;
            Activities_topSuccessMessageLabel.Text = string.Empty;
            Activities_bottomSuccessMessageLabel.Text = string.Empty;

            Activities_topErrorMessageLabel.Visible = false;
            Activities_bottomErrorMessageLabel.Visible = false;
            Activities_topSuccessMessageLabel.Visible = false;
            Activities_bottomSuccessMessageLabel.Visible = false;
        }

        /// <summary>
        /// Method that validate requests to retake a test. 
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session id.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="int"/> that holds the attempt id.
        /// </param>
        private bool IsValidRetake(string candidateSessionID, string testKey, int attemptID)
        {
            string retakeCount = string.Empty;
            Dictionary<string, string> dicRetakeDetails = new
                Dictionary<string, string>();
            string isCertificateTest = string.Empty;
            bool isValidRetake = true;
            string pendingAttempt = new CandidateBLManager().TestRetakeValidation(candidateSessionID, testKey,
                attemptID, out retakeCount, out isCertificateTest, out dicRetakeDetails);

            if (pendingAttempt != null && Convert.ToInt32(pendingAttempt) > 0)
            {
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, 
                    "Already an attempt is pending for this test session. Check with your pending tests");
                return isValidRetake = false;
            }
            if (Convert.ToInt32(isCertificateTest) > 0)
            {
                if (retakeCount != null && Convert.ToInt32(retakeCount) <= 0)
                {
                    base.ShowMessage(Activities_topErrorMessageLabel,
                        Activities_bottomErrorMessageLabel,
                        "You have utilized maximum number of attempts. You are not allowed to retake this test");
                    return isValidRetake = false;
                }
            }

            return isValidRetake;
        }

        #endregion Private Methods                                             

        #region Protected Methods                                              

        /// <summary>
        /// Method that will composed the facebook share URL.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="string"/> that holds the attempt ID.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds share URL.
        /// </returns>
        protected string GetFacebookShareURL(string testKey, string candidateSessionID, string attemptID)
        {
            UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

            string shareURL = ConfigurationManager.AppSettings["CANDIDATE_TEST_RESULT_URL"];
            shareURL += "?testkey=" + testKey;
            shareURL += "&parentpage=CAND_HOME";
            shareURL += "&candidatesessionid=" + candidateSessionID;
            shareURL += "&attemptid=" + attemptID;

            return shareURL;
        }

        /// <summary>
        /// Method that will composed message for completed test date.
        /// </summary>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the date.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed string.
        /// </returns>
        protected string GetCompletedTestDateFormat(DateTime date)
        {
            if (date == DateTime.MinValue)
                return string.Empty;

            return "You took this test on " + date.ToString("MMM dd yyyy");
        }

        /// <summary>
        /// Method that will composed message for completed interview date.
        /// </summary>
        /// <param name="date">
        /// A <see cref="DateTime"/> that holds the date.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed string.
        /// </returns>
        protected string GetCompletedInterviewDateFormat(DateTime date)
        {
            if (date == DateTime.MinValue)
                return string.Empty;

            return "You took this interview on " + date.ToString("MMM dd yyyy");
        }

        /// <summary>
        /// Method that retrieves the qualified status for certification. This
        /// helps to show or hide the view certification link icon in the 
        /// completed test grid section.
        /// </summary>
        /// <param name="certificationStatus">
        /// A <see cref="string"/> that holds the certification status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the certification available status.
        /// True represents available and false not available.
        /// </returns>
        /// <remarks>
        /// The applicable status for certification status are:
        /// 1. 'N/A' if not applicable.
        /// 2. 'Unavailable' if not qualified for certification.
        /// 3. 'Available' if qualified for certification.
        /// </remarks>
        protected bool IsQualified(string certificationStatus)
        {
            return (certificationStatus == "Available" ? true : false);
        }

        /// <summary>
        /// Method that retrieves the applicable status for retake. This
        /// helps to show or hide the retake request link icon in the 
        /// completed test grid section.
        /// </summary>
        /// <param name="initiateBy">
        /// A <see cref="string"/> that holds the initiate by name.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the retake applicable status.
        /// True represents applicable and false not applicable.
        /// </returns>
        /// <remarks>
        /// The initiate by will be SELF for self administered tests. For this
        /// tests retake is not applicable.
        /// </remarks>
        protected bool IsRetakeApplicable(string initiateBy)
        {
            return (initiateBy.Trim().ToUpper() == "SELF" ? false : true);
        }

        /// <summary>
        /// Method that retrieves the show results to candidate status. This
        /// helps to show or hide the show results link icon in the completed 
        /// test grid section.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the show results to candidate status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of show results
        /// to candidate status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for show results to candidates are:
        /// 1. True - Show.
        /// 2. False - Do not show.
        /// </remarks>
        protected bool IsShowResults(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        /// <summary>
        /// Method that retrieves the qualified status for certification. This
        /// helps to show or hide the view certification link icon in the 
        /// completed test grid section.
        /// </summary>
        /// <param name="cyberProctoringStatus">
        /// A <see cref="string"/> that holds the cyber proctoring status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the cyber proctoring status.
        /// </returns>
        protected bool IsCyberProctoring(string cyberProctoringStatus)
        {
            return cyberProctoringStatus.ToUpper() == "TRUE" ? true : false;
        }

        /// <summary>
        /// Method that retrieves the interview paused status. This helps to 
        /// show or hide the icon in the grid.
        /// </summary>
        /// <param name="status">
        /// A <see cref="string"/> that holds the interview paused status.
        /// </param>
        /// <returns>
        /// A <see cref="bool"/> that holds the boolean value of interview 
        /// paused status. True represents show and false do not show.
        /// </returns>
        /// <remarks>
        /// The applicable status for interview paused status are:
        /// 1. True - Paused.
        /// 2. False - Not Paused.
        /// </remarks>
        protected bool IsPaused(string status)
        {
            return (status.Trim().ToUpper() == "TRUE" ? true : false);
        }

        #endregion Protected Methods                                           

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Method that will load the pending, completed and expired tests into
        /// the grid.
        /// </summary>
        /// <remarks>
        /// This is a overridden method.
        /// </remarks>
        protected override void LoadValues()
        {
            int pageNumber = 1;

            // Get page number from session, if available.
            if (!Utility.IsNullOrEmpty(Session["ACTIVITIES_PAGE_NUMBER"]))
            {
                pageNumber = Convert.ToInt32(Session["ACTIVITIES_PAGE_NUMBER"]);
                Session["ACTIVITIES_PAGE_NUMBER"] = null;
            }

            // Check if display type query string is passed or not.
            if (Utility.IsNullOrEmpty(Request.QueryString["displaytype"]))
            {
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, "The display type parameter is missing");

                return;
            }

            // Retrieve display type.
            string displayType = Request.QueryString["displaytype"].ToUpper();

            if (displayType == "PT")
            {
                // Show pending tests for default page number 1.
                ShowPendingTests(pageNumber);

                // Move highlight focus to the page number.
                Activities_pendingTestsPagingNavigator.MoveToPage(pageNumber);
            }
            else if (displayType == "CT")
            {
                // Show completed tests for default page number 1.
                ShowCompletedTests(pageNumber);

                // Move highlight focus to the page number.
                Activities_completedTestsPagingNavigator.MoveToPage(pageNumber);
            }
            else if (displayType == "PI")
            {
                // Show pending interviews for default page number 1.
                ShowPendingInterviews(pageNumber); 
                
                // Move highlight focus to the page number.
                Activities_pendingInterviewsPagingNavigator.MoveToPage(pageNumber); 
            }
            else if (displayType == "OI")
            {
                SetSearchInterviewParam(pageNumber, "ONLINE_INTERVIEW_DATE", SortType.Descending); 
                GetOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria);
                Activities_pendingOnlineInterviewsPagingNavigator.MoveToPage(pageNumber); 
            }
            else if (displayType == "CI")
            {
                // Show completed interviews for default page number 1.
                ShowCompletedInterviews(pageNumber);

                // Move highlight focus to the page number.
                Activities_completedInterviewsPagingNavigator.MoveToPage(pageNumber);
            }
            else if (displayType == "ALL")
            {
                // Show pending tests for default page number 1.
                ShowPendingTests(pageNumber);

                // Show completed tests for default page number 1.
                ShowCompletedTests(pageNumber);

                // Show pending interviews for default page number 1.
                ShowPendingInterviews(pageNumber);

                // Show completed interviews for default page number 1.
                ShowCompletedInterviews(pageNumber);
            }
        }

        #endregion Protected Overridden Methods                                


        #region Online Interview Functions
        private void SetSearchInterviewParam(int pageNumber, string sortExpression, SortType sortType)
        {
            Session["INTERVIEW_SEARCH_CRITERIA"] = null;

            InterviewSearchCriteria interviewSearch = new InterviewSearchCriteria();

            interviewSearch.InterviewSessionKey = null;
            interviewSearch.InterviewSessionStatus = "SESS_SCHD";
            interviewSearch.Keyword = null;
            interviewSearch.SearchTenantID =0;
            interviewSearch.CurrentPage = pageNumber;
            interviewSearch.PageSize = base.activitiesGridPageSize;
            interviewSearch.SortExpression = sortExpression;
            interviewSearch.SortDirection = sortType;
            interviewSearch.CandidateId = base.userID;

            Session["INTERVIEW_SEARCH_CRITERIA"] = interviewSearch;
        }

        private void GetOnlineInterviews(InterviewSearchCriteria iSearchCreiteria)
        {    
            Activities_pendingOnlineInterviewsGridView.DataSource = null;
            Activities_pendingOnlineInterviewsGridView.DataBind();

            int totalRecords = 0; 

            List<OnlineCandidateSessionDetail> onlineInterviewCandidateList =
                new OnlineInterviewAssessorBLManager().GetOnlineInterviews(iSearchCreiteria, out totalRecords); 

            Activities_pendingOnlineInterviewsGridView.DataSource = onlineInterviewCandidateList;
            Activities_pendingOnlineInterviewsGridView.DataBind(); 

            Activities_pendingOnlineInterviewsPagingNavigator.PageSize = base.activitiesGridPageSize;
            Activities_pendingOnlineInterviewsPagingNavigator.TotalRecords = totalRecords;
        }
        #endregion 
         

        protected void Activities_pendingOnlineInterviewsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try 
            {
                if (e.CommandName == "StartInterview" || e.CommandName == "InterviewIntroduction")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string candidateSessionID = (Activities_pendingOnlineInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingOnlineInterviewsGridView_candidateSessionIDHiddenField") as HiddenField).Value;
                    int interviewId = Convert.ToInt32((Activities_pendingOnlineInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingOnlineInterviewsGridView_interviewIDHiddenField") as HiddenField).Value);

                    string chatRoomId = (Activities_pendingOnlineInterviewsGridView.Rows
                        [index].FindControl("Activities_pendingOnlineInterviewsGridView_chatRoomIDHiddenField") as HiddenField).Value;

                    if (Session["USER_DETAIL"] == null) return;

                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                    if (userDetail == null) return;

                    //Checking candidate have online interview for current date & time
                    OnlineCandidateSessionDetail onlineInterviewDetail =
                        new CandidateBLManager().GetOnlineInterviewDetail(userDetail.UserID, 0, chatRoomId);

                    if (onlineInterviewDetail != null)
                    {
                        if (new Utility().GetOnlineInterviewAvailable(onlineInterviewDetail.InterviewDate,
                            onlineInterviewDetail.TimeSlotFrom, onlineInterviewDetail.TimeSlotTo))
                        {
                            Response.Redirect(ConfigurationManager.AppSettings["ONLINE_INTERVIEW_URL"].ToString() + "?candidateid=" + onlineInterviewDetail.CandidateID.ToString().Trim()
                                + "&userrole=C" + "&userid=" + onlineInterviewDetail.CandidateID.ToString().Trim() + "&interviewkey=" + onlineInterviewDetail.CandidateSessionID
                                + "&chatroom=" + onlineInterviewDetail.ChatRoomName, false);
                            return;
                        }
                        else
                        {
                            if (onlineInterviewDetail.InterviewDate > DateTime.Today)
                            {
                                base.ShowMessage(Activities_topErrorMessageLabel,
                                    Activities_bottomErrorMessageLabel, "Interview start date on " + base.GetDateFormat(onlineInterviewDetail.InterviewDate));
                                return;
                            }

                            string currentTime = DateTime.Now.ToString("HH:mm:ss");

                            DateTime dbStartTime = Convert.ToDateTime(onlineInterviewDetail.TimeSlotFrom.ToString(new DateTimeFormatInfo()));
                            DateTime currentSysTime = Convert.ToDateTime(currentTime);

                            dbStartTime = dbStartTime.AddMinutes(-10);

                            TimeSpan startTimeDiff = currentSysTime.Subtract(dbStartTime);

                            //if start time differnece is less the 10 mins
                            if (startTimeDiff.Minutes > 10)
                            {
                                base.ShowMessage(Activities_topErrorMessageLabel,
                                   Activities_bottomErrorMessageLabel, "Interview start time on "
                                   + onlineInterviewDetail.TimeSlotFrom);
                                return;
                            }
                            else
                            {
                                base.ShowMessage(Activities_topErrorMessageLabel,
                                   Activities_bottomErrorMessageLabel, "Interview time has expired" );
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Activities_topErrorMessageLabel,
                    Activities_bottomErrorMessageLabel, exp.Message); 
            }
        }
     
    }
}