﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="EditProfile.aspx.cs" Inherits="Forte.HCM.Candidate.CandidateAdmin.EditProfile" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="EditProfile_headContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="EditProfile_mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function ShowHideDeleteConfirmation()
        {
            if (document.getElementById('EditProfile_deleteConfirmationDiv').style.display == "block")
            {
                document.getElementById('EditProfile_deleteConfirmationDiv').style.display = "none";
            }
            else
            {
                document.getElementById('EditProfile_deleteConfirmationDiv').style.display = "block";
            }

            return false;
        }

    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <asp:HiddenField ID="EditProfile_candidateResumeID" runat="server" />
                <div style="width: 960px; float: left">
                    <asp:Label ID="EditProfile_errorMessageLabel" runat="server" Text="" SkinID="sknErrorMessage"></asp:Label>
                </div>
                <div style="width: 960px; float: left">
                    <asp:Label ID="EditProfile_successMessageLabel" runat="server" Text="" SkinID="sknSuccessMessage"></asp:Label>
                </div>
                <div style="width: 960px; float: left">
                    <asp:Label runat="server" ID="EditProfile_myProfileLabel" CssClass="can_signup_ex_user"
                        Text="My Profile"></asp:Label>
                </div>
                <div style="display:none">
                    <asp:Button ID="EditProfile_defaultButton" runat="server" Text="Default Button" />
                </div>
                <div style="width: 960px; float: left">
                    <div class="can_tab_bg_left">
                        &nbsp;
                    </div>
                    <div class="can_tab_bg" style="width: 700px">
                        <asp:Label ID="EditProfile_changePasswordLabel" runat="server" Text="Change Password"
                            CssClass="edit_profile_section_title_label"></asp:Label>
                    </div>
                    <div class="can_tab_bg_right">
                        &nbsp;
                    </div>
                </div>
                <div style="width: 960px; float: left">
                    <div class="edit_profile_section_panel" style="padding-top: 8px">
                        <div class="edit_profile_control_row">
                            <div class="edit_profile_control_row_left">
                                <asp:Label runat="server" ID="EditProfile_oldPasswordLabel">Current Password<span>*</span></asp:Label>
                            </div>
                            <div class="edit_profile_control_row_right" style="padding-top: 4px">
                                <asp:TextBox ID="EditProfile_oldPasswordTextBox" runat="server" SkinID="sknCanTextBox"
                                    TextMode="Password" ToolTip="Enter old password" MaxLength="50"> </asp:TextBox>
                            </div>
                        </div>
                        <div class="edit_profile_control_row">
                            <div class="edit_profile_control_row_left">
                                <asp:Label ID="EditProfile_newPasswordLabel" runat="server">New Password<span>*</span></asp:Label>
                            </div>
                            <div class="edit_profile_control_row_right" style="padding-top: 4px">
                                <asp:TextBox ID="EditProfile_newPasswordTextBox" SkinID="sknCanTextBox" runat="server"
                                    TextMode="Password" ToolTip="Enter new password" MaxLength="50"> </asp:TextBox>
                            </div>
                        </div>
                        <div class="edit_profile_control_row">
                            <div class="edit_profile_control_row_left">
                                <asp:Label ID="EditProfile_retypeNewPasswordLabel" runat="server">Retype New Password <span>*</span></asp:Label>
                            </div>
                            <div class="edit_profile_control_row_right">
                                <div style="float: left; padding-top: 4px">
                                    <asp:TextBox ID="EditProfile_retypeNewPasswordTextBox" SkinID="sknCanTextBox" runat="server"
                                        TextMode="Password" ToolTip="Re-type new password" MaxLength="50"> </asp:TextBox>
                                </div>
                                <div style="float: left; padding-left: 10px; padding-top: 12px">
                                    <asp:Button ID="EditProfile_savePasswordButton" SkinID="sknButtonOrange" runat="server"
                                        Text="Change" OnClick="EditProfile_savePasswordButton_Click" ToolTip="Click here to save the password details" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 960px; float: left; height: 4px">
                </div>
                <div style="width: 960px; float: left">
                    <div class="can_tab_bg_left">
                        &nbsp;
                    </div>
                    <div class="can_tab_bg" style="width: 700px">
                        <asp:Label ID="EditProfile_registeredEmailsLabel" runat="server" Text="Registered Emails"
                            CssClass="edit_profile_section_title_label"></asp:Label>
                    </div>
                    <div class="can_tab_bg_right">
                        &nbsp;
                    </div>
                </div>
                <div style="width: 960px; float: left">
                    <div class="edit_profile_section_panel" style="padding-top: 8px">
                        <div class="edit_profile_control_row">
                            <div class="edit_profile_control_row_left">
                                <asp:Label ID="EditProfile_emailIDLabel" runat="server" Text="Email ID"></asp:Label>
                            </div>
                            <div class="edit_profile_control_row_right" style="padding-top: 8px">
                                <asp:Label ID="EditProfile_emailIDValueLabel" runat="server" Text="" CssClass="edit_profile_value_field_label"></asp:Label>
                            </div>
                        </div>
                        <div class="edit_profile_control_row">
                            <div class="edit_profile_control_row_left">
                                <asp:Label ID="EditProfile_alternateEmailIDLabel" runat="server" Text="">Alt Email ID</asp:Label>
                            </div>
                            <div class="edit_profile_control_row_right">
                                <div style="float: left; padding-top: 4px">
                                    <asp:TextBox ID="EditProfile_alternateEmailIDTextBox" SkinID="sknCanTextBox" runat="server"
                                        MaxLength="50" ToolTip="Enter alternate email ID"> </asp:TextBox>
                                </div>
                                <div style="float: left; padding-left: 10px; padding-top: 12px">
                                    <asp:Button ID="EditProfile_saveEmailDetailsButton" SkinID="sknButtonOrange" runat="server"
                                        Text="Save" OnClick="EditProfile_saveEmailDetailsButton_Click" ToolTip="Click here to save the email details" /></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 960px; float: left; height: 4px">
                </div>
                <div style="width: 960px; float: left">
                    <div class="can_tab_bg_left">
                        &nbsp;
                    </div>
                    <div class="can_tab_bg" style="width: 700px">
                        <asp:Label ID="EditProfile_skillsLabel" runat="server" Text="Skills" CssClass="edit_profile_section_title_label"></asp:Label>
                    </div>
                    <div class="can_tab_bg_right">
                        &nbsp;
                    </div>
                </div>
                <div style="width: 960px; float: left">
                    <div class="edit_profile_section_panel" style="padding-top: 8px; padding-bottom: 8px">
                        <div class="edit_profile_control_row">
                            <div style="width: 560px; height: 134px; overflow: auto; float: left;">
                                <asp:GridView ID="EditProfile_skillsGridView" runat="server" AutoGenerateColumns="False"
                                    Width="560px" SkinID="sknEditProfileSkillGridView">
                                    <Columns>
                                        <asp:BoundField DataField="SlNo" HeaderText="SNo" />
                                        <asp:BoundField DataField="Skill" HeaderText="Skill" />
                                        <asp:BoundField DataField="ExperienceYears" HeaderText="Exp (In Years)" />
                                        <asp:BoundField DataField="LastUsed" HeaderText="Last Used (Year)" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div style="width: 100px; float: right; padding-left: 4px">
                                <asp:Button ID="EditProfile_editSkillsButton" runat="server" Text="Edit Skills" SkinID="sknButtonOrange"
                                    OnClick="EditProfile_editSkillsButton_Click" ToolTip="Click here to edit your skills" />
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 960px; float: left; height: 4px">
                </div>
                <div style="width: 960px; float: left">
                    <div class="can_tab_bg_left">
                        &nbsp;
                    </div>
                    <div class="can_tab_bg" style="width: 700px">
                        <asp:Label ID="EditProfile_resumeLabel" runat="server" Text="Resume" CssClass="edit_profile_section_title_label"></asp:Label>
                    </div>
                    <div class="can_tab_bg_right">
                        &nbsp;
                    </div>
                </div>
                <div style="width: 960px; float: left">
                    <div class="edit_profile_section_panel" style="padding-top: 8px; padding-bottom: 8px">
                        <div class="edit_profile_control_row">
                            <div style="width: 520px; float: left">
                                <asp:LinkButton ID="EditProfile_resumeNameLink" runat="server" OnClick="EditProfile_downloadResume_Click"
                                    ToolTip="Click here to download and view your profile" CssClass="edit_profile_resume_name_link"
                                    Visible="false"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="edit_profile_control_row">
                            <div style="width: 680px; float: left">
                                <div style="width: 15px; float: left;padding-top:5px;">
                                    <asp:Image ID="EditProfile_resumeAlertImage" runat="server" SkinID="sknResumeAlertImage"
                                        ToolTip="Upload your resume" />
                                </div>
                                <div style="float: left; padding-top: 10px">
                                    <asp:Label ID="EditProfile_resumeStatusLabel" runat="server" Text="Upload your resume"
                                        CssClass="edit_profile_resume_status_alert_label"></asp:Label>
                                </div>
                                <div style="float: left; padding-top: 8px; padding-left: 4px;">
                                    <asp:Button ID="EditProfile_previewResumeButton" runat="server" Text="Review & Approve" Visible="false"
                                        SkinID="sknButtonBlue" OnClick="EditProfile_previewResumeButton_Click" ToolTip="Click here to review and approve your resume" />
                                </div>
                                <div style="float: left; padding-top: 8px; padding-left: 4px;">
                                    <asp:Button ID="EditProfile_uploadResumeButton" SkinID="sknButtonOrange" runat="server"
                                        Text="Update Your Resume" OnClick="EditProfile_uploadResumeButton_Click" ToolTip="Click here to update your resume" />
                                </div>
                                <div id="EditProfile_deleteLinkDiv" style="float: right; padding-top: 12px; padding-left: 4px; text-align: right">
                                    <asp:LinkButton ID="EditProfile_deleteResumeLink" Text="Delete Resume" runat="server"
                                        OnClientClick="javascript:return ShowHideDeleteConfirmation()" ToolTip="Click here to delete your profile"
                                        CssClass="self_label_text_link" Visible="false">
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="edit_profile_control_row">
                            <%--<div style="width: 680px; float: right; padding-top: 2px">--%>
                                <div id="EditProfile_deleteConfirmationDiv" style="width: 290px; float: left; padding-right: 4px; padding-left: 350px; display: none; padding-top: 4px">
                                <div class="edit_profile_delete_resume_warning_panel">
                                    <div style="float: left; padding-top: 3px">
                                        <asp:Label ID="EditProfile_deleteConfirmationLabel" Text="Are you sure that you want to delete this resume?"
                                            runat="server" CssClass="edit_profile_delete_resume_warning_label"></asp:Label>
                                    </div>
                                     <div style="float: left; padding-left: 4px">
                                        <asp:Button ID="EditProfile_deleteConfirmationYesButton" runat="server" Text="Yes"
                                            SkinID="sknButtonBlue" OnClick="EditProfile_deleteConfirmationYesButton_Click"/>
                                    </div>
                                     <div style="float: left; padding-left: 4px">
                                        <asp:Button ID="EditProfile_deleteConfirmationNoButton" runat="server" Text="No"
                                            SkinID="sknButtonBlue" OnClientClick="javascript:return ShowHideDeleteConfirmation()" />
                                    </div>
                                    </div>
                                </div>
                            <%--</div>--%>
                        </div>
                    </div>
                </div>
                <div style="width: 960px; float: left; height: 30px">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
