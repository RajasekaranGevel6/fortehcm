﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CandidateSearchTest.aspx.cs"
    Inherits="Forte.HCM.Candidate.CandidateAdmin.CandidateSearchTest" MasterPageFile="~/SiteMaster.Master" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<%@ Register Src="~/CommonControls/RecommendedTestPageNavigator.ascx" TagName="PageNavigator"
    TagPrefix="uc1" %>
<%@ Register Src="~/CommonControls/AutomatedTestSummaryControl.ascx" TagName="AutomatedTestControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<asp:Content ID="CandidateSearch_headContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="CandidateSearch_Content" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">

        function ShowHideDeleteConfirmation()
        {
            if (document.getElementById('CandidateSearchTest_deleteTestConfirmationDiv').style.display == "block")
            {
                document.getElementById('CandidateSearchTest_deleteTestConfirmationDiv').style.display = "none";
            }
            else
            {
                document.getElementById('CandidateSearchTest_deleteTestConfirmationDiv').style.display = "block";
            }

            return false;
        }

    </script>
    <asp:UpdatePanel ID="CandidateSearch_UpdatePanel" runat="server">
        <ContentTemplate>
         <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
            <div>
                <div>
                    <div align="center">
                        <div class="align_left">
                            <asp:Label ID="CandidateSearch_searchTestErrorLabel" SkinID="sknErrorMessage" runat="server"
                                Text=""></asp:Label>
                            <asp:Label ID="CandidateSearch_searchTestSuccessLabel" SkinID="sknSuccessMessage"
                                runat="server" Text=""></asp:Label>
                        </div>
                        <div style="text-align:left;width:940px;">
                            <asp:Label ID="CandidateSearch_searchandAdminLabel" runat="server" CssClass="search_text"
                                Text="Search for a test"></asp:Label>
                        </div>
                        <div>
                            &nbsp;</div>
                        <div>
                            <asp:TextBox ID="CandidateSearch_keywordsTextBox" runat="server" SkinID="sknSearchTextBox"
                                onFocus="javascript:this.select()"></asp:TextBox>
                            <asp:ImageButton ID="CandidateSearch_goImageButton" runat="server" SkinID="sknbtnSearchIconNew"
                                OnClick="CandidateSearch_goImageButton_Click" ToolTip="Click here to search" />
                        </div>
                        <div>
                            &nbsp;</div>
                        <div class="cand_search_mainouter_box" runat="server" id="CandidateSearch_searchContentDiv">
                            <div class="can_panel_body_bg">
                                <div>
                                    <div class="cand_search_outer_box_head">
                                        <asp:Label ID="CandidateSearch_recommendedTestNameTitleLabel" SkinID="sknCandLabelHeadText"
                                            runat="server"></asp:Label>
                                    </div>
                                    <div class="cand_search_inner_box">
                                        <asp:ListView ID="CandidateSearch_recommendedTestsListView" runat="server" OnItemDataBound="CandidateSearch_recommendedTestsListView_ItemDataBound">
                                            <LayoutTemplate>
                                                <asp:PlaceHolder ID="ItemPlaceHolder" runat="server" />
                                                <asp:DataPager ID="ItemDataPager" runat="server" PageSize="1" PagedControlID="CandidateSearch_recommendedTestsListView">
                                                </asp:DataPager>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <div class="cand_search_inner_box">
                                                    <div style="float: left; text-align: left;">
                                                        <div>
                                                            <div style="float: left; width: 70px;">
                                                                <asp:Label ID="CandidateSearch_recommendedTestsListView_recommendedTimeTitleLabel"
                                                                    runat="server" SkinID="sknCandLabelText" Text="Time Limit : "></asp:Label>
                                                            </div>
                                                            <div style="float: left; width: 100px;">
                                                                <asp:Label ID="CandidateSearch_recommendedTestsListView_recommendedTimeValueLabel"
                                                                    runat="server" CssClass="cand_search_test_time"></asp:Label>
                                                            </div>
                                                            <div style="height:25px">&nbsp;</div>
                                                        </div>
                                                        <div style="clear: both;">
                                                            <div style="float: left">
                                                                <asp:Label ID="CandidateSearch_recommendedTestsListView_descriptionValueLabel" runat="server"
                                                                    Text='<%# Eval("Description") %>' SkinID="sknCandLabelDiscriptionText"></asp:Label>
                                                                <asp:HiddenField ID="CandidateSearch_recommendedTestsListView_recommendedTestIDHiddenField"
                                                                    runat="server" Value='<%# Eval("TestRecommendedID") %>' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="clear: both; float: left; text-align: left;">
                                                        <div>
                                                            <asp:Chart ID="CandidateSearch_recommendedTestsChart" runat="server" Palette="BrightPastel"
                                                                BackColor="Transparent" AntiAliasing="Graphics" Font="Arial, 8.25pt, style=Bold"
                                                                Width="700" Height="250" EnableViewState="true">
                                                                <Legends>
                                                                    <asp:Legend Name="CandidateSearch_legend" BackColor="Transparent" Font="Arial, 8.25pt, style=Bold"
                                                                        IsTextAutoFit="False" Alignment="Center" ForeColor="114, 142, 192" Docking="Right"
                                                                        IsDockedInsideChartArea="False" BackImageWrapMode="TileFlipXY" ItemColumnSpacing="10"
                                                                        MaximumAutoSize="70" TextWrapThreshold="90">
                                                                        <CellColumns>
                                                                            <asp:LegendCellColumn ColumnType="SeriesSymbol" Name="CandidateSearch_legendSymbolColumn">
                                                                                <Margins Left="100" Right="0"></Margins>
                                                                            </asp:LegendCellColumn>
                                                                            <asp:LegendCellColumn Name="CandidateSearch_legendSeriesColumn" Alignment="MiddleLeft">
                                                                                <Margins Left="600" Right="0"></Margins>
                                                                            </asp:LegendCellColumn>
                                                                            <asp:LegendCellColumn Name="CandidateSearch_legendPercentColumn" Text="#PERCENT{P0}">
                                                                                <Margins Left="0" Right="0"></Margins>
                                                                            </asp:LegendCellColumn>
                                                                        </CellColumns>
                                                                    </asp:Legend>
                                                                </Legends>
                                                                <Series>
                                                                </Series>
                                                                <ChartAreas>
                                                                    <asp:ChartArea Name="CandidateSearch_recommendedTestsChart_subjectChartArea" BackSecondaryColor="Transparent"
                                                                        BackColor="Transparent" ShadowColor="Aqua">
                                                                        <AxisY>
                                                                            <MajorGrid Enabled="False" LineDashStyle="DashDotDot" />
                                                                            <LabelStyle ForeColor="180, 65, 140, 240" Angle="90" />
                                                                        </AxisY>
                                                                        <AxisX IsLabelAutoFit="False" Interval="1">
                                                                            <MajorGrid Enabled="False" LineDashStyle="DashDotDot" />
                                                                            <LabelStyle ForeColor="180, 65, 140, 240" Angle="0" TruncatedLabels="True" />
                                                                        </AxisX>
                                                                        <AxisX2 TitleForeColor="180, 65, 140, 240">
                                                                        </AxisX2>
                                                                        <Position Height="90" Width="45" Y="15" />
                                                                    </asp:ChartArea>
                                                                </ChartAreas>
                                                                <Titles>
                                                                    <asp:Title Name="CandidateSearch_recommendedTestsChart_testDescriptionTitle">
                                                                    </asp:Title>
                                                                </Titles>
                                                            </asp:Chart>
                                                        </div>
                                                        <div>
                                                            <%--  <asp:Label ID="CandidateSearch_recommendedTestTotalQuestionsTitleLabel" runat="server"
                                                            SkinID="sknCandLabelText" Text="Total : "></asp:Label>--%>
                                                            <asp:Label ID="CandidateSearch_recommendedTestsListView_totalQuestionsLabel" runat="server"
                                                                CssClass="cand_search_test_time" Text='<%# Eval("NoOfQuestions") + " Questions" %>'></asp:Label>
                                                            <%--<asp:Label ID="CandidateSearch_recommendedTestsListView_totalQuestionsTitleLabel"
                                                                runat="server" CssClass="cand_search_test_time" Text="Questions"></asp:Label>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </div>
                                    <div style="width: 740px; float: left;">
                                        <div style="width: 300px; float: left;padding-left:100px;">
                                            <div style="width: 30px; float: left">
                                                <asp:Image ID="CandidateSearch_testStatusAlertImage" runat="server" SkinID="sknAlertImage"
                                                    ToolTip="Test Status" />
                                            </div>
                                            <div style="width: 400px; padding-top: 10px">
                                                <asp:Label ID="CandidateSearch_testStatusAlertMessage" runat="server" CssClass="general_warning_orange_message"></asp:Label>
                                            </div>
                                        </div>
                                        <div style="width: 220px; float: left;">
                                            <div style="float: left; padding-top: 8px; padding-left: 4px">
                                                <asp:Button ID="CandidateSearch_takeTestButton" SkinID="sknButtonOrange" runat="server"
                                                    Text="Take Test Now" OnClick="CandidateSearch_takeTestButton_Click" /></div>
                                            <div style="float: left; padding-top: 8px; padding-left: 4px">
                                                <asp:Button ID="CandidateSearch_saveButton" runat="server" SkinID="sknButtonBlue"
                                                    Text="Save For Later" OnClick="CandidateSearch_saveButton_Click" />
                                            </div>
                                            <div style="float: left; padding-top: 10px; padding-left: 4px;">
                                                <asp:LinkButton ID="CandidateSearchTest_deleteTestLinkButton" Text="Delete" runat="server"
                                                    OnClientClick="javascript:return ShowHideDeleteConfirmation()"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="CandidateSearchTest_deleteTestConfirmationDiv" style="display: none; float: left;
                                        padding-left: 200px; padding-top: 8px">
                                        <div class="delete_self_admin_test_warning_panel">
                                            <div style="float: left; padding-top: 3px">
                                                <asp:Label ID="CandidateSearchTest_deleteTestConfirmationLabel" Text="Are you sure to delete the test ?"
                                                    runat="server" CssClass="delete_self_admin_test_warning_label"></asp:Label>
                                            </div>
                                            <div style="float: left; padding-left: 4px">
                                                <asp:Button ID="CandidateSearchTest_deleteTestConfirmationYesButton" runat="server"
                                                    Text="Yes" OnClick="CandidateSearchTest_deleteTestConfirmationYesButton_Click"
                                                    SkinID="sknButtonBlue" />
                                            </div>
                                            <div style="float: left; padding-left: 4px">
                                                <asp:Button ID="CandidateSearchTest_deleteTestConfirmationNoButton" runat="server"
                                                    Text="No" SkinID="sknButtonBlue" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div align="center">
                                <uc1:PageNavigator ID="CandidateSearch_recommendedTestsPageNavigator" runat="server" />
                            </div>
                        </div>
                        <div>
                            <%--  <uc2:automatedtestcontrol id="CreateAutomaticTest_byQuestion_automatedTestUserControl"
                                runat="server" />--%>
                        </div>
                    </div>
                </div>
                <div>
                    <asp:Panel ID="CandidateSearch_startTestPopupPanel" runat="server" Style="display: none;
                        height: 206px" CssClass="popupcontrol_confirm">
                        <div id="CandidateSearch_startTestHiddenDIV" style="display: none">
                            <asp:Button ID="CandidateSearch_startTestHiddenButton" runat="server" />
                        </div>
                        <uc3:ConfirmMsgControl ID="CandidateSearch_startTestConfirmMsgControl" runat="server"
                            OnOkClick="CandidateSearch_startTest_OkClick" OnCancelClick="CandidateSearch_startTest_CancelClick" />
                    </asp:Panel>
                    <ajaxToolKit:ModalPopupExtender ID="CandidateSearch_startTestPopupExtenderControl"
                        runat="server" PopupControlID="CandidateSearch_startTestPopupPanel" TargetControlID="CandidateSearch_startTestHiddenButton"
                        BackgroundCssClass="modalBackground">
                    </ajaxToolKit:ModalPopupExtender>
                </div>
            </div>
            </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
