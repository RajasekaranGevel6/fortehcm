﻿#region Header

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EditProfile.aspx.cs
// Class that represents the user interface layout and functionalities
// for the EditProfile page. This page helps in self editing of profile 
// details by candidate such as first name, last name, password change, 
// etc. This class inherits the Forte.HCM.UI.Common.PageBase class.

#endregion Header                                                              

#region Directives

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.Candidate.CandidateAdmin
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the EditProfile page. This page helps in self editing of profile 
    /// details by candidate such as first name, last name, password change, 
    /// etc. This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class EditProfile :  PageBase
    {
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set default button
                Page.Form.DefaultButton = EditProfile_defaultButton.UniqueID;

                // Set browser title.
                Master.SetPageCaption("Edit Profile");

                // Clear message and hide labels.
                ClearMessages();

                if (!IsPostBack)
                {
                    // Load user details.
                    LoadUserDetails();

                    // Load skill details.
                    LoadSkills();

                    // Load resume status.
                    LoadResumeStatus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditProfile_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the edit skills button is
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch edit skills page.
        /// </remarks>
        protected void EditProfile_editSkillsButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Signup.aspx?activetag=updateskills&parentpage=" + 
                    Constants.ParentPage.CANDIDATE_EDIT_PROFILE, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditProfile_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when preview resume button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch preview resume page.
        /// </remarks>
        protected void EditProfile_previewResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ResumeRepository/MyResume.aspx?parentpage=" +
                    Constants.ParentPage.CANDIDATE_EDIT_PROFILE, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditProfile_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the upload resume button 
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch upload resume page.
        /// </remarks>
        protected void EditProfile_uploadResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Signup.aspx?activetag=updateresume&parentpage=" + 
                    Constants.ParentPage.CANDIDATE_EDIT_PROFILE, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditProfile_errorMessageLabel, exp.Message);
            }
        }
        
        /// <summary>
        /// Handler method that will be called when the save email details 
        /// button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will save the user details.
        /// </remarks>
        protected void EditProfile_saveEmailDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate email address.
                if (!Utility.IsNullOrEmpty(EditProfile_alternateEmailIDTextBox.Text.Trim()))
                {
                    if (!IsValidEmailAddress(EditProfile_alternateEmailIDTextBox.Text.Trim()))
                    {
                        ShowMessage(EditProfile_errorMessageLabel, "Please enter valid Alt Email ID");
                        return;
                    }
                }

                // Construct user detail object.
                UserDetail userDetail = new UserDetail();
                userDetail.UserID = base.userID;
                userDetail.AltEmail = EditProfile_alternateEmailIDTextBox.Text.Trim();

                // Save the details.
                new CandidateBLManager().UpdateUserProfile(userDetail);

                // Update the user details into the session.
                UserDetail sessionUserDetail = Session["USER_DETAIL"] as UserDetail;
                sessionUserDetail.AltEmail = userDetail.AltEmail;
                Session["USER_DETAIL"] = sessionUserDetail;

                // Show a success message.
                ShowMessage(EditProfile_successMessageLabel, "Email details saved successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditProfile_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the save password button
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will save the user details.
        /// </remarks>
        protected void EditProfile_savePasswordButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if old password is entered or not
                if (EditProfile_oldPasswordTextBox.Text.Trim().Length == 0)
                {
                    ShowMessage(EditProfile_errorMessageLabel, "Current password cannot be empty");
                    return;
                }

                // Check if new password is entered or not
                if (EditProfile_newPasswordTextBox.Text.Trim().Length == 0)
                {
                    ShowMessage(EditProfile_errorMessageLabel, "New password cannot be empty");
                    return;
                }

                // Check if new password is entered or not
                if (EditProfile_retypeNewPasswordTextBox.Text.Trim().Length == 0)
                {
                    ShowMessage(EditProfile_errorMessageLabel, "New password cannot be empty");
                    return;
                }

                // Get the password.
                UserDetail userDetail = new UserRegistrationBLManager().GetPassword
                    (((UserDetail)Session["USER_DETAIL"]).UserID);

                // Check whether existing password is correct or not.
                if (new EncryptAndDecrypt().DecryptString(userDetail.Password) != 
                    EditProfile_oldPasswordTextBox.Text.Trim())
                {
                    ShowMessage(EditProfile_errorMessageLabel, "Your current password is wrong");
                    return;
                }

                // Check whether newly entered password matches.
                if (EditProfile_newPasswordTextBox.Text != EditProfile_retypeNewPasswordTextBox.Text)
                {
                    ShowMessage(EditProfile_errorMessageLabel, "Newly typed passwords did not match");
                    return;
                }

                // Update password.
                new UserRegistrationBLManager().UpdatePassword(new EncryptAndDecrypt().EncryptString(
                    EditProfile_newPasswordTextBox.Text.Trim()), ((UserDetail)Session["USER_DETAIL"]).UserID);

                // Show a success message.
                ShowMessage(EditProfile_successMessageLabel, "Password changed successfully");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditProfile_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the resume name link is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will download the resume from the repository.
        /// </remarks>
        protected void EditProfile_downloadResume_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(EditProfile_candidateResumeID.Value) || Convert.ToInt32(EditProfile_candidateResumeID.Value) == 0)
                {
                    base.ShowMessage(EditProfile_errorMessageLabel, "Resume not found to download");
                    return;
                }

                Response.Redirect("~/Common/Download.aspx?id=" + EditProfile_candidateResumeID.Value, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditProfile_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the 'Yes' button is clicked
        /// in the delete profile confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the candidate's profile from the repository.
        /// </remarks>
        protected void EditProfile_deleteConfirmationYesButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(EditProfile_candidateResumeID.Value) || Convert.ToInt32(EditProfile_candidateResumeID.Value) == 0)
                {
                    base.ShowMessage(EditProfile_errorMessageLabel, "Resume not found to delete");
                    return;
                }

                // Delete candidate resume.
                new CandidateBLManager().DeleteCandidateResume(Convert.ToInt32(EditProfile_candidateResumeID.Value));

                ShowMessage(EditProfile_successMessageLabel,
                    "Resume deleted successfully. Please update your resume");

                // Reload the resume status.
                LoadResumeStatus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EditProfile_errorMessageLabel, exp.Message);
            }
        }

        #region Private Methods

        /// <summary>
        /// Method that loads the user details.
        /// </summary>
        private void LoadUserDetails()
        {
            UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

            EditProfile_emailIDValueLabel.Text = userDetail.Email;
            EditProfile_alternateEmailIDTextBox.Text = userDetail.AltEmail;
        }

        /// <summary>
        /// Method that loads the skill details.
        /// </summary>
        private void LoadSkills()
        {
            List<SignupCandidate> skills = new CandidateBLManager().
                GetCandidateSkills(base.userID);

            EditProfile_skillsGridView.DataSource = skills;
            EditProfile_skillsGridView.DataBind();
        }

        /// <summary>
        /// Method the loads the resume status
        /// </summary>
        private void LoadResumeStatus()
        {
            // Assign default resume status.
            EditProfile_resumeStatusLabel.Text = "Upload your resume";
            EditProfile_resumeAlertImage.ToolTip = EditProfile_resumeStatusLabel.Text;
            EditProfile_resumeStatusLabel.Visible = true;
            EditProfile_resumeAlertImage.Visible = true;
            EditProfile_resumeNameLink.Visible = false;
            EditProfile_deleteResumeLink.Visible = false;
            EditProfile_previewResumeButton.Visible = false;
            
            // Get resume status.
            ResumeStatus resumeStatus = new CandidateBLManager().GetCandidateResumeStatus(base.userID);
           
            if (resumeStatus == null)
                return;
                
            // Compose and assign the resume status message.
            EditProfile_resumeStatusLabel.Text = base.GetResumeStatusMessage(resumeStatus);
            EditProfile_resumeAlertImage.ToolTip = EditProfile_resumeStatusLabel.Text;

            if (EditProfile_resumeStatusLabel.Text.Trim().Length == 0)
            {
                EditProfile_resumeStatusLabel.Visible = false;
                EditProfile_resumeAlertImage.Visible = false;
            }

            EditProfile_candidateResumeID.Value = resumeStatus.CandidateResumeID.ToString();

            EditProfile_resumeNameLink.Text = resumeStatus.ResumeName;

            // Hide/show buttons & change button text.
            if (resumeStatus.Uploaded)
            {
                EditProfile_resumeNameLink.Visible = true;
                EditProfile_deleteResumeLink.Visible = true;
                EditProfile_previewResumeButton.Visible = true;
            }

            if (resumeStatus.ShowApproveButton)
            {
                EditProfile_previewResumeButton.Text = "Review & Approve";
            }
            else
            {
                EditProfile_previewResumeButton.Text = "View Profile";
            }
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            EditProfile_errorMessageLabel.Text = string.Empty;
            EditProfile_successMessageLabel.Text = string.Empty;

            EditProfile_errorMessageLabel.Visible = false;
            EditProfile_successMessageLabel.Visible = false;
        }
        /// <summary>
        /// Method that will check for the valid email id
        /// </summary>
        /// <param name="userEmailID"></param>
        /// <returns></returns>
        private bool IsValidEmailAddress(string userEmailID)
        {
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$",
                RegexOptions.Compiled);
            return regex.IsMatch(userEmailID);
        }

        #endregion Private Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
 }
