﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CandidateSearch.aspx.cs"
    Inherits="Forte.HCM.Candidate.CandidateSearch" MasterPageFile="~/MasterPages/CandidateSignUpMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/CandidateSignUpMaster.Master" %>
<asp:Content ID="CandidateSearch_headContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="CandidateSearch_Content" runat="server" ContentPlaceHolderID="MainContent">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="Resource_Selection_cnt_outer">
                <div id="ass_left">
                    <div id="ass_tab_outer">
                        <div>
                            <asp:Label ID="CandidateSearch_searchandAdminLabel" runat="server" Text="Search and Administration"></asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="CandidateSearch_searchTestTextBox" runat="server"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="CandidateSearch_searchTestImageButton" runat="server" SkinID="sknbtnSearchTestIcon"
                                OnClick="CandidateSearch_searchTestImageButton_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="CandidateSearch_viewInstructionImageButton" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
