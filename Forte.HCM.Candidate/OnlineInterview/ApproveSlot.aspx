﻿<%@ Page Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="ApproveSlot.aspx.cs" Inherits="Forte.HCM.Candidate.OnlineInterview.ApproveSlot" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Approve_headContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Approve_mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .interview_label
        {
            font-size: 16px !important;
            color: #575f66 !important;
            white-space: nowrap !important;
            font-weight: bold;
            vertical-align: top;
            display: block;
        }
        
        .interview_label_text
        {
            font-size: 12px !important;
            color: #575f66 !important;
            padding-top: 0px;
        }
        
        .interview_outer_border
        {
            width: 800px;
            border: 1px solid #BEC7CF;
            padding: 10px;
            float: left;
            background-image: url(Images/act_text_area_bg.png);
            background-repeat: repeat-x;
            background-position: bottom;
            background-color: White;
        }
        .interview_can_tab_bg
        {
            background-image: url(CommonImages/can_tab_bg.png);
            background-repeat: repeat-x;
            background-position: bottom;
            background-color: #a9acba;
            padding: 0px 0px 0px 0px;
            text-align: center;
            height: 35px;
            float: left;
            width: 180px;
            color: #000;
            vertical-align: middle;
            margin: 0px 0px 0px 0px;
        }

       .interview_section_title_label
        {
            font-size: 16px;
            font-weight: bold;
            text-align: center;
            color: #fff !important;
            text-decoration: none !important;
            height: 35px;
            width: 300px;
            display: block;
            text-align: left;
            padding: 7px 0px 0px 0px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function GridRadioApproveCheck(rb) {
            var gv = document.getElementById("<%=ApproveSlot_slotGridView.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 960px; float: left">
                    <div class="can_tab_bg_left">
                        &nbsp;
                    </div>
                    <div class="can_tab_bg" style="width: 800px">
                        <asp:Label ID="EditProfile_changePasswordLabel" runat="server" Text="Approve Slot for your interview scheduled"
                            CssClass="interview_section_title_label"></asp:Label>
                    </div>
                    <div class="can_tab_bg_right">
                        &nbsp;
                    </div>
                </div>
                <div class="activities_main_div">
                    <div class="interview_outer_border" runat="server">
                        <div class="activities_empty_height">
                            &nbsp;
                        </div>
                        <div style="text-align: center;">
                            <asp:UpdatePanel ID="Approve_topSuccessErrorMsgUpdatePanel" UpdateMode="Conditional"
                                runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="ApproveSlot_successMessageLabel" Text="" SkinID="sknSuccessMessage" runat="server"></asp:Label>
                                    <asp:Label ID="ApproveSlot_errorMessageLabel" runat="server" Text="" SkinID="sknErrorMessage"></asp:Label>
                                    <asp:HiddenField ID="ApproveSlot_candidateInterviewIDHiddenField" runat="server" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ApproveSlot_slotGridView_ApproveButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <!-- Online interview instruction -->
                        <div>
                            <div style="float: left">
                                <div style="float: left;">
                                    <asp:Label ID="ApproveSlot_interviewNameLabelValue" CssClass="interview_label_text"
                                        runat="server"></asp:Label>
                                </div>
                                <div style="float: left; padding-left: 5px;">
                                    <asp:Label ID="ApproveSlot_interviewRequestedByLabelValue" CssClass="activity_value_label"
                                        runat="server"></asp:Label>
                                </div>
                            </div>
                            <div style="clear: both; float: left; height: 20px">
                                &nbsp;</div>
                            <div style="clear: both; float: left">
                                <div style="float: left">
                                    <asp:Label ID="ApproveSlot_interviewInstructionLabelValue" CssClass="interview_label_text"
                                        runat="server"></asp:Label>
                                </div>
                            </div>
                            <div style="clear: both; float: left; height: 20px">&nbsp;</div>
                        </div>
                        <!-- End online interview instruction -->
                        <div style="clear: both" class="form_container_grid">
                            <asp:UpdatePanel ID="ApproveSlot_slotGridViewUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <div class="form_container_grid" style="overflow: auto;" runat="server" id="ApproveSlot_slotGridViewDiv">
                                        <asp:GridView ID="ApproveSlot_slotGridView" runat="server" AutoGenerateColumns="False"
                                            Width="100%">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:RadioButton ID="ApproveSlot_slotGridView_ApproveRadioButton" onclick="GridRadioApproveCheck(this);"
                                                            runat="server" />
                                                        <asp:HiddenField ID="ApproveSlot_slotGridView_AcceptedIDHiddenField" Value='<%# Eval("RequestDateGenID") %>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="5%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ApproveSlot_slotGridView_ChoiceLabel" Text='<%# String.Format("Choice {0}",Container.DataItemIndex + 1)  %>'
                                                            runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ApproveSlot_slotGridView_DateLabel" runat="server" Text='<%# Eval("AvailabilityDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ApproveSlot_slotGridView_FromLabel" runat="server" SkinID="sknHomePageLabel"
                                                            Text='<%# String.Format("{0} To {1}",Eval("TimeSlotTextFrom"),Eval("TimeSlotTextTo")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30%" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ApproveSlot_slotGridView_ApproveButton" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="activities_empty_height">
                            &nbsp;
                        </div>
                        <div style="clear: both; float: left; height: 20px">
                                &nbsp;</div>
                        <div style="clear: both; float: left">
                            <asp:UpdatePanel ID="ApproveSlot_slotGridView_ApproveButtonUpdatePanel" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="ApproveSlot_slotGridView_ApproveButton" OnClick="ApproveSlot_slotGridView_ApproveButton_Click"
                                SkinID="sknButtonBlue" Text="Approve" runat="server" />        
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            
                        </div>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
