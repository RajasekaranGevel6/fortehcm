﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Forte.HCM.BL;
using Forte.HCM.UI.Common;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using System.Web.UI.WebControls;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

namespace Forte.HCM.Candidate.OnlineInterview
{
    public partial class ApproveSlot : PageBase
    {
        #region Private Variables                                              
        
        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="assessorDetail">
        /// A <see cref="AssessorDetail"/> that holds the assessor detail.
        /// </param>
        /// <param name="chatRoomKey">
        /// A <see cref="String"/> that holds the chat room key.
        /// </param>
        /// </param>
        /// <param name="candidateScheduleID">
        /// A <see cref="int"/> that holds the candidate interview id.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegateAssessor(List<AssessorDetail> assessorDetail,
            string chatRoomKey, int candidateScheduleID, EntityType entityType);

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate details.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegateCandidate(CandidateDetail candidateDetail, EntityType entityType);

        #endregion Private Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that is called when the page is loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message controls
                ClearMessages();

                // Set browser title.
                Master.SetPageCaption("Approve Slot");

                if (!IsPostBack)
                {
                    // Call and assign query string values.
                    if (RetrieveKey() == false)
                        return;

                    // Get slot detail
                    GetSlotDetail();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ApproveSlot_successMessageLabel,
                    ApproveSlot_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method that will be called when approve slots button is called
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void ApproveSlot_slotGridView_ApproveButton_Click(object sender, EventArgs e)
        {
            try
            {
                int acceptedID = 0;
                int candidateInterviewID = 0;

                foreach (GridViewRow row in ApproveSlot_slotGridView.Rows)
                {
                    RadioButton ApproveSlot_slotGridView_ApproveRadioButton =
                        ((RadioButton)row.FindControl("ApproveSlot_slotGridView_ApproveRadioButton"));

                    if (ApproveSlot_slotGridView_ApproveRadioButton.Checked)
                    {
                        HiddenField ApproveSlot_slotGridView_AcceptedIDHiddenField =
                            ((HiddenField)row.FindControl("ApproveSlot_slotGridView_AcceptedIDHiddenField"));

                        if (!Utility.IsNullOrEmpty(ApproveSlot_slotGridView_AcceptedIDHiddenField.Value))
                        {
                            acceptedID =
                                Convert.ToInt32(ApproveSlot_slotGridView_AcceptedIDHiddenField.Value);
                        }
                        break;
                    }
                }

                if (!Utility.IsNullOrEmpty(ApproveSlot_candidateInterviewIDHiddenField.Value))
                {
                    candidateInterviewID =
                        Convert.ToInt32(ApproveSlot_candidateInterviewIDHiddenField.Value);
                }

                if (acceptedID == 0 || candidateInterviewID == 0)
                {
                    base.ShowMessage(ApproveSlot_errorMessageLabel,
                        "Select slot to approve");
                    return;
                }

                string chatRoomkey = new OnlineInterviewBLManager().
                    ApproveSlotIDAndSchedule(candidateInterviewID, acceptedID, base.userID);

                // Get slot detail
                GetSlotDetail();
                ApproveSlot_successMessageLabel.Text = string.Empty;
                base.ShowMessage(ApproveSlot_successMessageLabel,
                    "Slot approved and scheduled successfully");

                List<AssessorDetail> assessors = new List<AssessorDetail>();

                assessors = new OnlineInterviewBLManager().
                    GetOnlineInterviewScheduledAssessorByCandidateID(candidateInterviewID);

                // Send mail to the requested assessor
                // Sent alert mail to the associated user asynchronously.
                AsyncTaskDelegateAssessor taskDelegate = new AsyncTaskDelegateAssessor(SendScheduledInfoToAssessor);
                IAsyncResult result = taskDelegate.BeginInvoke(assessors, chatRoomkey, candidateInterviewID,
                    EntityType.OnlineInterviewScheduledToAssessor,
                    new AsyncCallback(SendScheduledInfoToAssessorCallBack), taskDelegate);

                CandidateDetail candidateDetail = new CandidateDetail();
                candidateDetail.FirstName = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                candidateDetail.EMailID = ((UserDetail)Session["USER_DETAIL"]).Email;
                candidateDetail.ChatRoomKey = chatRoomkey;
                candidateDetail.CandidateInterviewID = candidateInterviewID;

                // Send mail to the scheduled candidate
                // Sent alert mail to the associated user asynchronously.
                AsyncTaskDelegateCandidate taskCandidateDelegate = new AsyncTaskDelegateCandidate(SendScheduledInfoToCandidate);
                IAsyncResult resultCandidate = taskCandidateDelegate.BeginInvoke(candidateDetail,
                    EntityType.OnlineInterviewScheduledToCandidate, new AsyncCallback(SendScheduledInfoToCandidateCallBack), taskCandidateDelegate);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(ApproveSlot_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieves the query string values and assign it to 
        /// hidden fields.
        /// </summary>
        private bool RetrieveKey()
        {
            // Check if correct key is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["s"]))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method to get the slot details
        /// </summary>
        private void GetSlotDetail()
        {
            //OnlineInterviewSessionDetail onlineInterviewDetail = new OnlineInterviewSessionDetail();

            AssessorTimeSlotDetail assessorSlotDetail = new AssessorTimeSlotDetail();

            string slotKey = Request.QueryString["s"].Trim();
            
            assessorSlotDetail = new AssessorBLManager().GetCandidateSlotDetail(slotKey);

            if (assessorSlotDetail!=null && 
                assessorSlotDetail.assessorTimeSlotDetails != null)
            {
                int acceptedID = 0;
                if (!Utility.IsNullOrEmpty(assessorSlotDetail.assessorTimeSlotDetails[0].AcceptedID))
                {
                    acceptedID = assessorSlotDetail.assessorTimeSlotDetails[0].AcceptedID;
                }

                if (acceptedID == 0)
                {
                    ApproveSlot_slotGridViewDiv.Style["display"] = "block";
                    ApproveSlot_slotGridView.DataSource = assessorSlotDetail.assessorTimeSlotDetails;
                    ApproveSlot_slotGridView.DataBind();
                }
                else
                {
                    ApproveSlot_successMessageLabel.Text = "No slot available to approve.";
                    ApproveSlot_slotGridViewDiv.Style["display"] = "none";
                    ApproveSlot_slotGridView_ApproveButton.Visible = false;
                }
            }

            if (assessorSlotDetail.onlineInterviewDetail != null)
            {
                ApproveSlot_candidateInterviewIDHiddenField.Value = 
                    assessorSlotDetail.onlineInterviewDetail.CandidateInterviewID.ToString();

                ApproveSlot_interviewNameLabelValue.Text = 
                    assessorSlotDetail.onlineInterviewDetail.InterviewName;
                ApproveSlot_interviewInstructionLabelValue.Text =
                    assessorSlotDetail.onlineInterviewDetail.InterviewInstruction;
                if (!Utility.IsNullOrEmpty(assessorSlotDetail.onlineInterviewDetail.RequestedBy))
                {
                    ApproveSlot_interviewRequestedByLabelValue.Text = 
                        string.Format("(Requested By {0})", assessorSlotDetail.onlineInterviewDetail.RequestedBy);
                }
            }
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            ApproveSlot_errorMessageLabel.Text = string.Empty;
            ApproveSlot_successMessageLabel.Text = string.Empty;

            ApproveSlot_errorMessageLabel.Visible = false;
            ApproveSlot_successMessageLabel.Visible = false;
        }

        /// <summary>
        /// Method that sends mail to the scheduled assessor
        /// </summary>
        /// <param name="assessorDetails">
        /// A <see cref="AssessorDetail"/> that holds the assessor
        /// detail.
        /// </param>
        /// <param name="chatRoomKey">
        /// A <see cref="String"/> that holds the chat room key
        /// </param>
        /// <param name="candidateInterviewID">
        /// A <see cref="int"/> that holds the candidate interview id
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendScheduledInfoToAssessor(List<AssessorDetail> assessorDetails,
            string chatRoomKey, int candidateScheduleID, EntityType entityType)
        {
            try
            {
                // Send email.
                foreach (AssessorDetail assessorDetail in assessorDetails)
                {
                    assessorDetail.CandidateInterviewID = candidateScheduleID;
                    assessorDetail.ChatRoomKey = chatRoomKey;
                    new EmailHandler().SendMail(entityType, assessorDetail);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that sends mail to the scheduled candidate
        /// </summary>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the candidate
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendScheduledInfoToCandidate(CandidateDetail candidateDetail,
            EntityType entityType)
        {
            try
            {
                // Send email.
                new EmailHandler().SendMail(entityType, candidateDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Private Methods               

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods

        #region Asynchronous Method Handlers                                   

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="resultAssessor">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendScheduledInfoToAssessorCallBack(IAsyncResult resultAssessor)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegateAssessor caller = (AsyncTaskDelegateAssessor)resultAssessor.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(resultAssessor);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="resultCandidate">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendScheduledInfoToCandidateCallBack(IAsyncResult resultCandidate)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegateCandidate caller = (AsyncTaskDelegateCandidate)resultCandidate.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(resultCandidate);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        #endregion Asynchronous Method Handlers
    }
}