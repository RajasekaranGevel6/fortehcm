﻿using System;
using System.Web.UI;
using System.Drawing;
using System.Drawing.Imaging;

using Forte.HCM.Support;

namespace Forte.HCM.UI.CandidateCenter
{
    public partial class CaptchaViewer : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["type"] == null)
                return;


            if (Request.QueryString["type"].ToUpper().Trim() == "OI")
            {
                if (Session["INTERVIEW_CAPTCHA_IMAGE"] != null)
                {
                    // Change the response headers to output a JPEG image.
                    this.Response.Clear();
                    this.Response.ContentType = "image/jpeg";

                    // Write the image to the response stream in JPEG format.
                    (Session["INTERVIEW_CAPTCHA_IMAGE"] as Image).Save
                        (this.Response.OutputStream, ImageFormat.Jpeg);
                }
            }

            if (Request.QueryString["type"].ToUpper().Trim() == "CP")
            {
                if (Session["TEST_CAPTCHA_IMAGE"] != null)
                {
                    // Change the response headers to output a JPEG image.
                    this.Response.Clear();
                    this.Response.ContentType = "image/jpeg";

                    // Write the image to the response stream in JPEG format.
                    (Session["TEST_CAPTCHA_IMAGE"] as Image).Save
                        (this.Response.OutputStream, ImageFormat.Jpeg);
                }
            }
        }
    }
}
