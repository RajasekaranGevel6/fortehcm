﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Forte.HCM.UI.Common
{
    /// <summary>
    /// Summary description for CaptchaImage
    /// </summary>
    public class CaptchaImageHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Session["CAPTCHA_IMAGE"] == null)
                return;
            context.Response.ContentType = "image/jpeg";
            Image img = context.Session["CAPTCHA_IMAGE"] as Image;
            MemoryStream ms = new MemoryStream();
            img.Save(ms, ImageFormat.Jpeg);
            context.Response.BinaryWrite(ms.ToArray());
            context.Response.End();
            context.Session["CAPTCHA_IMAGE"] = null;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}