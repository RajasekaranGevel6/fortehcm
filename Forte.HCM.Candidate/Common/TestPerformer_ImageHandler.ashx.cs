﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;

using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.Utilities;
using Forte.HCM.UI.Common;
using System.Web.SessionState;

namespace Forte.HCM.Candidate.Common
{
    /// <summary>
    /// Summary description for TestPerformer_ImageHandler
    /// </summary>
    public class TestPerformer_ImageHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {           
            HttpFileCollection files = context.Request.Files;
            if (context.Request.QueryString != null)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string canSessionKey = "";
                    int attemptID = 0;
                    string imageType = "";

                    int ModifiedBy =  ((UserDetail)context.Session["USER_DETAIL"]).UserID;
                    int CreatedBy =  ((UserDetail)context.Session["USER_DETAIL"]).UserID;
                    if (context.Request.QueryString["csk"] != null)
                    {
                        canSessionKey = context.Request.QueryString["csk"].ToString();
                    }
                    if (context.Request.QueryString["atmpt"] != null)
                    {
                        attemptID =Convert.ToInt32(context.Request.QueryString["atmpt"].ToString());
                    }

                    if (context.Request.QueryString["type"].ToString() == "Screen")
                        imageType = "SCREEN_IMG";
                      
                    else
                        imageType = "USER_IMG";
                       
                    byte[] imageBytes=null;
                    using (Stream fs = file.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                            {
                                 imageBytes = br.ReadBytes((Int32)fs.Length);
                            }
                    }
                    Int64 CP_IMAGE_ID=0;
                    new TestConductionBLManager().AddImageDetails(canSessionKey, attemptID, imageType, imageBytes, CreatedBy, ModifiedBy, out CP_IMAGE_ID);
                }
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write("Image saved successfully");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}