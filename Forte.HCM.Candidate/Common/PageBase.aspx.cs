﻿#region Headerr

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PageBase.aspx.cs
// File that represents the PageBase class that defines the base page for 
// the aspx pages used in the application. This base page contains common
// data and functionality used across the pages. 

#endregion Header

#region Directives

using System;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;



#endregion Directives

namespace Forte.HCM.UI.Common
{
    /// <summary>
    /// Class that defines the base page for the aspx pages used in the 
    /// application. This base page contains common data and functionality 
    /// used across the pages.
    /// </summary>
    public abstract partial class PageBase : Page
    {
        #region Protected Fields

        /// <summary>
        /// A <see cref="int"/> that holds the user ID.
        /// </summary>
        protected int userID = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the limited user status.
        /// </summary>
        protected bool isLimited = false;

        /// <summary>
        /// A <see cref="string"/> that holds the logged in user email ID.
        /// </summary>
        protected string userEmailID = string.Empty;

        /// <summary>
        /// A <see cref="double"/> that holds the candidate session expiry days.
        /// </summary>
        protected double candidateExpiryDays = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the activities grid page size.
        /// </summary>
        protected int activitiesGridPageSize = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the search test grid page size.
        /// </summary>
        protected int searchTestGridPageSize = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the max retakes for self admin 
        /// tests.
        /// </summary>
        protected int maxSelfAdminTestRetakes = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the max self admin tests can be
        /// saved/taken in a month.
        /// </summary>
        protected int maxSelfAdminTestPerMonth = 0;

        /// <summary>
        /// A <see cref="int"/> that holds the resume expiry days, after which
        /// the warning message is shown to the candidates.
        /// </summary>
        protected int resumeExpiryDays = 0;

        private string featureURL;

        private string url;
        private List<string> parameters;

        #endregion Protected Fields

        #region Protected Constants

        /// <summary>
        /// A <see cref="string"/> constant that holds the CSS class name
        /// for the mouse over style.
        /// </summary>
        protected const string MOUSE_OVER_STYLE = "className='grid_normal_row'";

        /// <summary>
        /// A <see cref="string"/> constant that holds the CSS class name
        /// for the mouse out style.
        /// </summary>
        protected const string MOUSE_OUT_STYLE = "className='grid_alternate_row'";

        #endregion Protected Constants

        #region Protected Abstract Methods

        /// <summary>
        /// Method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected abstract bool IsValidData();

        /// <summary>
        /// Method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        /// <remarks>
        /// This method needs to be overrided by the sub classes.
        /// </remarks>
        protected abstract void LoadValues();

        #endregion Protected Abstract Methods

        #region Protected Overridden Methods

        /// <summary>
        /// Represents the method that is called when the page is being initialized.
        /// </summary>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that contains the event data.
        /// </param>
        /// 
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                if (Session["USER_DETAIL"] != null)
                {
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                    this.userID = userDetail.UserID;
                    this.isLimited = userDetail.IsLimited;
                    this.userEmailID = userDetail.Email;
                }

                // If candidate admin settings is not present in session, get it
                // and keep it in session.
                if (Session["CANDIDATE_ADMIN_SETTINGS"] == null)
                {
                    // Keep the admin settings in session.
                    Session["CANDIDATE_ADMIN_SETTINGS"] = new AdminBLManager().
                        GetCandidateAdminSettings();
                }

                // Get candidate admin settings from session.
                CandidateAdminSettings candidateAdminSettings = Session
                    ["CANDIDATE_ADMIN_SETTINGS"] as CandidateAdminSettings;

                // Assign candidate settings to fields.
                this.candidateExpiryDays = candidateAdminSettings.CandidateSessionExpiryInDays;
                this.maxSelfAdminTestRetakes = candidateAdminSettings.MaxSelfAdminTestRetakes;
                this.maxSelfAdminTestPerMonth = candidateAdminSettings.MaxSelfAdminTestPerMonth;
                this.resumeExpiryDays = candidateAdminSettings.ResumeExpiryDays;
                this.activitiesGridPageSize = candidateAdminSettings.ActivitiesGridPageSize;
                this.searchTestGridPageSize = candidateAdminSettings.SearchTestGridPageSize;

                ConfigurePageDefaults();

                if (IsPostBack)
                    return;

                CheckAndUpdateReadOnlyTextBox(this.Page);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Protected Overridden Methods

        #region Private Methods

        /// <summary>
        /// This method checks and updates read only text boxes in UI
        /// </summary>
        /// <param name="Parent">
        /// A <see cref="Control"/> that contains the control to look for 
        /// textbox.
        /// </param>
        private void CheckAndUpdateReadOnlyTextBox(Control Parent)
        {
            foreach (Control Child in Parent.Controls)
            {
                if (Child.HasControls())
                    CheckAndUpdateReadOnlyTextBox(Child);
                else
                {
                    if (!(Child is TextBox))
                        continue;
                    if (!((TextBox)Child).ReadOnly)
                        continue;
                    ((TextBox)Child).ReadOnly = false;
                    ((TextBox)Child).Attributes.Add("readonly", "readonly");
                }
            }
        }

        private void ConfigurePageDefaults()
        {
            if (Utility.IsNullOrEmpty(featureURL))
            {
                featureURL = Request.AppRelativeCurrentExecutionFilePath.Replace('~', ' ').Trim().ToLower();
            }
            if (!IsIgnorePage())
            {
                AssignPermissions();
            }
        }

        private bool IsIgnorePage()
        {
            if (
                featureURL.Contains(@"/signup.aspx") ||
                featureURL.Contains(@"/forgotpassword.aspx") ||
                featureURL.Contains(@"/offerings.aspx") ||
                featureURL.Contains(@"/selfadmindemo.aspx") ||
                featureURL.Contains(@"/activityhome.aspx") ||
                featureURL.Contains(@"/candidatehelp.aspx") ||
                featureURL.Contains(@"/candidatetestresult.aspx") ||
                featureURL.Contains(@"/captchaviewer.aspx") ||
                featureURL.Contains(@"/captchaimagehandler.ashx") ||
                featureURL.Contains(@"/candidateaccessdenied.aspx") ||
                featureURL.Contains(@"/candidateunhandlederror.aspx") ||
                featureURL.Contains(@"/candidatepageunderconstruction.aspx") ||
                featureURL.Contains(@"/popupaccessdenied.aspx") ||
                featureURL.Contains(@"/popupunhandlederror.aspx") ||
                featureURL.Contains(@"/popupunderconstruction.aspx") ||
                featureURL.Contains(@"/aboutus.aspx") ||
                featureURL.Contains(@"/Agreement.aspx") ||
                featureURL.Contains(@"/feedback.aspx") ||
                featureURL.Contains(@"/rating.aspx") ||
                featureURL.Contains(@"/overview.aspx") ||
                featureURL.Contains(@"/signin.aspx") || 
                featureURL.Contains(@"/candidatetestdetails.aspx") ||
                featureURL.Contains(@"/approveslot.aspx"))
            {
                return true;
            }

            return false;
        }

        private void AssignPermissions()
        {
            UserDetail userDetails = new UserDetail();

            List<RoleRights> roleRightsList = new List<RoleRights>();

            if (Session["USER_DETAIL"] != null)
            {
                userDetails = ((UserDetail)Session["USER_DETAIL"]);

                if (userDetails.UserType == UserType.SiteAdmin)
                    return;

                roleRightsList = new AuthenticationBLManager().GetAuthorization(userDetails.UserID, featureURL.ToLower());
                if (!roleRightsList.Exists(item => item.RoleRightView == true))
                {
                    if (featureURL.Contains(@"/popup/"))
                        Response.Redirect(@"~/Popup/PopupAccessDenied.aspx", false);
                    else
                        Response.Redirect(@"~/Common/CandidateAccessDenied.aspx", false);
                }
            }
            else
            {
                Response.Redirect("~/SignIn.aspx", false);
            }
        }

        #endregion Private Methods

        #region Proctected Methods

        /// <summary>
        /// Method that compose and returns the resume status message based on
        /// the uploaded status, approved status and age of the resume.
        /// </summary>
        /// <param name="resumeStatus">
        /// A <see cref="ResumeStatus"/> that holds the resume status.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the composed message.
        /// </returns>
        /// <remarks>
        /// The <see cref="ResumeStatus"/> object will also set back some status
        /// to the caller method.
        /// </remarks>
        protected string GetResumeStatusMessage(ResumeStatus resumeStatus)
        {
            if (resumeStatus == null)
                return string.Empty;

            if (!resumeStatus.Uploaded)
                return "Upload your resume !";
            else
            {
                if (resumeStatus.DaysOld > resumeExpiryDays)
                {
                    int months = resumeStatus.DaysOld / 30;

                    if (resumeStatus.Approved)
                    {
                        // Resume approved.
                        return string.Format("Your resume is over {0} months old !", months);
                    }
                    else
                    {
                        // Resume not approved.
                        return string.Format("Your resume is over {0} months old !", months);
                    }
                }
                else
                {
                    if (resumeStatus.Approved)
                        return string.Empty;
                    else
                    {
                        resumeStatus.ShowApproveButton = true;
                        return "Your career profile is awaiting your approval !";
                    }
                }
            }
        }

        /// <summary>
        /// Method that will return the date in MM/dd/yyyy format. If date not exists, 
        /// then it displays empty value in column.
        /// </summary>
        /// <param name="date">This parameter is passed from aspx page value 
        /// </param>
        /// <returns></returns>
        protected string GetDateFormat(DateTime date)
        {
            string strDate = date.ToString("MM/dd/yyyy");
            if (strDate == "01/01/0001")
                strDate = "";
            return strDate;
        }

        /// <summary>
        /// Represents the method that sets the user interface elements 
        /// properties such as permission settings, styles, formatting, etc.
        /// </summary>
        /// <param name="controls">
        /// A <see cref="ControlCollection"/> that holds the list of controls.
        /// </param>
        protected void UpdateUI(ControlCollection controls)
        {
            foreach (Control parentControl in controls)
            {
                if (parentControl is Control)
                {
                    UpdateUI(parentControl.Controls);
                }
            }
        }

        /// <summary>
        /// Represents the method that shows the user message.
        /// </summary>
        /// <param name="topLabel">
        /// A <see cref="Label"/> that holds the top label control.
        /// </param>
        /// <param name="bottomLabel">
        /// A <see cref="Label"/> that holds the bottom label control.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        protected void ShowMessage(Label topLabel, Label bottomLabel,
            string message)
        {
            // Show the labels.
            topLabel.Visible = true;

            if (bottomLabel != null)
                bottomLabel.Visible = true;

            // Set the message to the label.
            if (topLabel.Text.Length == 0)
            {
                topLabel.Text = message;

                if (bottomLabel != null)
                    bottomLabel.Text = message;
            }
            else
            {
                topLabel.Text += "<br>" + message;

                if (bottomLabel != null)
                    bottomLabel.Text += "<br>" + message;
            }
        }

        /// <summary>
        /// Represents the method that shows the user message.
        /// </summary>
        /// <param name="label">
        /// A <see cref="Label"/> that holds the label control.
        /// </param>
        /// <param name="message">
        /// A <see cref="string"/> that holds the message.
        /// </param>
        protected void ShowMessage(Label topLabel, string message)
        {
            ShowMessage(topLabel, null, message);
        }

        /// <summary>
        /// Trim the Length content
        /// </summary>
        /// <param name="content"></param>
        /// <param name="lengthofContent"></param>
        /// <returns></returns>
        protected string TrimContent(string content, int lengthofContent)
        {
            if (Utility.IsNullOrEmpty(content))
                return "";
            content = content.Trim();
            if (content.Length < lengthofContent)
            {
                return content;

            }
            return content.Substring(0, lengthofContent - 3) + "...";
        }
        /// <summary>
        /// Reset the All controls
        /// </summary>
        /// <param name="parent"></param>
        protected void ResetFormControlValues(Control parent)
        {
            foreach (Control c in parent.Controls)
            {
                if (c.Controls.Count > 0)
                {
                    ResetFormControlValues(c);
                }
                else
                {
                    switch (c.GetType().ToString())
                    {
                        case "System.Web.UI.WebControls.TextBox":
                            ((TextBox)c).Text = "";
                            break;
                        case "System.Web.UI.WebControls.CheckBoxList":
                            ((CheckBoxList)c).ClearSelection();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Represents the method that acts as a handler for redirect to parent
        /// page action. This handler is integrated with cancel link buttons in
        /// pages.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender.
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// Based on the 'parentpage' query string parameter, the parent page 
        /// is determined.
        /// </remarks>
        protected void ParentPageRedirect(object sender, EventArgs e)
        {
            // Retrieve the parent page flag from the query string.
            string parentPage = Request.QueryString["parentpage"];

            // If parent page is empty then go to home page.
            if (Utility.IsNullOrEmpty(parentPage))
            {
                Response.Redirect("~/Dashboard.aspx", false);

                return;
            }

            // If parent page is 'MENU' then go to home page.
            if (parentPage.ToUpper() == "MENU")
            {
                Response.Redirect("~/Dashboard.aspx", false);

                return;
            }

            // Navigate to specific parent page.
            switch (parentPage.ToUpper().Trim())
            {
                // Sign In.
                case Constants.ParentPage.CANDIDATE_SIGN_IN:
                    Response.Redirect(@"~\SignIn.aspx", false);
                    break;

                // Self Signin Candidate Home.
                case Constants.ParentPage.CANDIDATE_DASHBOARD:
                    Response.Redirect(@"~\Dashboard.aspx", false);
                    break;

                // Candidate edit profile.
                case Constants.ParentPage.CANDIDATE_EDIT_PROFILE:
                    Response.Redirect(@"~\CandidateAdmin\EditProfile.aspx", false);
                    break;

                // Activity Home.
                case Constants.ParentPage.ACTIVITY_HOME:
                    Response.Redirect(@"~\ActivityHome.aspx", false);
                    break;

                // Candidate Search Test.
                case Constants.ParentPage.CANDIDATE_SEARCH_TEST:
                    Response.Redirect(@"~\TestCenter\CandidateSearchTest.aspx?" +
                        "parentpage=" + Constants.ParentPage.CANDIDATE_HOME, false);
                    break;

                // Candidate Self Admin Test.
                case Constants.ParentPage.CANDIDATE_SELF_ADMIN_TEST:
                    Response.Redirect(@"~\CandidateAdmin\CandidateSearchTest.aspx", false);
                    break;

                // Candidate Activities.
                case Constants.ParentPage.CANDIDATE_ACTIVITIES:
                    Response.Redirect(@"~\CandidateAdmin\Activities.aspx?displaytype=" + 
                        Request.QueryString["displaytype"] , false);
                    break;

                // Candidate Test Results.
                case Constants.ParentPage.CANDIDATE_TEST_RESULTS:
                    Response.Redirect(@"~\TestCenter\CandidateTestResult.aspx", false);
                    break;

                // My Tests.
                case Constants.ParentPage.MY_TESTS:
                    Response.Redirect(@"~\TestCenter\MyTests.aspx", false);
                    break;

                // My Interviews.
                case Constants.ParentPage.MY_INTERVIEWS:
                    Response.Redirect(@"~\Dashboard.aspx", false);
                    break;

                // Test Introduction.
                case Constants.ParentPage.TEST_INTRODUCTION:
                    {
                        string sourcePage = string.Empty;

                        if (Request.QueryString["sourcepage"] != null)
                            sourcePage = Request.QueryString["sourcepage"].Trim().ToUpper();

                        if (sourcePage == Constants.ParentPage.CANDIDATE_ACTIVITIES)
                        {
                            Response.Redirect(@"~\TestCenter\TestIntroduction.aspx" +
                                "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                                "&attemptid=" + Request.QueryString["attemptid"] +
                                "&displaytype=" + Request.QueryString["displaytype"] +
                                "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES, false);
                        }
                        else
                        {
                            Response.Redirect(@"~\TestCenter\TestIntroduction.aspx" +
                                "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                                "&attemptid=" + Request.QueryString["attemptid"] +
                                "&parentpage=" + Constants.ParentPage.CANDIDATE_HOME, false);
                        }
                    }
                    break;

                // Interview Introduction.
                case Constants.ParentPage.INTERVIEW_INTRODUCTION:
                    {
                        string sourcePage = string.Empty;

                        if (Request.QueryString["sourcepage"] != null)
                            sourcePage = Request.QueryString["sourcepage"].Trim().ToUpper();

                        if (sourcePage == Constants.ParentPage.CANDIDATE_ACTIVITIES)
                        {
                            Response.Redirect(@"~\InterviewCenter\InterviewIntroduction.aspx" +
                                "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                                "&attemptid=" + Request.QueryString["attemptid"] +
                                "&displaytype=" + Request.QueryString["displaytype"] +
                                "&parentpage=" + Constants.ParentPage.CANDIDATE_ACTIVITIES, false);
                        }
                        else
                        {
                            Response.Redirect(@"~\InterviewCenter\InterviewIntroduction.aspx" +
                                "?candidatesessionid=" + Request.QueryString["candidatesessionid"] +
                                "&attemptid=" + Request.QueryString["attemptid"] +
                                "&parentpage=" + Constants.ParentPage.CANDIDATE_HOME, false);
                        }
                    }
                    break;

                // Candidate Adaptive Summary.
                case Constants.ParentPage.CANDIDATE_ADAPTIVE_SUMMARY:
                    Response.Redirect(@"~\TestCenter\AdaptiveTestSummary.aspx" +
                        "?parentpage=" + Constants.ParentPage.CANDIDATE_ADAPTIVE_SUMMARY, false);
                    break;

                // Adaptive Test Recommendation.
                case Constants.ParentPage.CANDIDATE_ADAPTIVE_TEST_RECOMMENDATION:
                    Response.Redirect(@"~\TestCenter\AdaptiveTestRecommendation.aspx" +
                        "?id=" + Request.QueryString["id"] +
                        "&parentpage=" + Constants.ParentPage.CANDIDATE_ADAPTIVE_TEST_RECOMMENDATION, false);
                    break;

                default:
                    Response.Redirect(@"~\Dashboard.aspx", false);

                    break;
            }
        }

        /// <summary>
        /// Method that retrieves the sort column index for the given grid view
        /// and currently sorted column key.
        /// </summary>
        /// <param name="gridView">
        /// A <see cref="GridView"/> that holds the grid view object.
        /// </param>
        /// <param name="columnKey">
        /// A <see cref="string"/> that holds the currently sorted column key.
        /// </param>
        /// <returns></returns>
        protected int GetSortColumnIndex(GridView gridView, string columnKey)
        {
            foreach (DataControlField field in gridView.Columns)
            {
                if (field.SortExpression == columnKey)
                {
                    return gridView.Columns.IndexOf(field);
                }
            }
            return -1;
        }

        /// <summary>
        /// Method that adds sort image (ascending or descending) to the sorted
        /// row header.
        /// </summary>
        /// <param name="columnIndex">
        /// A <see cref="int"/> that holds the column index.
        /// </param>
        /// <param name="headerRow">
        /// A <see cref="GridViewRow"/> that hollds the grid view row object.
        /// </param>
        /// <param name="sortOrder">
        /// A <see cref="string"/> that holds the sort order. 'A' represents
        /// ascending and 'D' descending.
        /// </param>
        protected void AddSortImage(int columnIndex, GridViewRow headerRow,
            SortType sortOrder)
        {
            // Create and assign the sort image based on the sort direction.
            Image sortImage = new Image();

            if (sortOrder == SortType.Ascending)
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_asc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../App_Themes/DefaultTheme/images/sort_desc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            // Add the image to the appropriate header cell.
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// display the Yes and no while we get N and Y
        /// </summary>
        /// <param name="content"></param>
        /// <param name="lengthofContent"></param>
        /// <returns></returns>
        protected string GetExpansion(bool content)
        {
            if (content)
            {
                return "Yes";
            }
            else if (!content)
            {
                return "No";
            }
            else
            {
                return null;
            }
        }

        #endregion Proctected Methods

        #region Public Properties

        /// <summary>
        /// Property that retrieves the grid page size.
        /// </summary>
        public int GridPageSize
        {
            get
            {
                if (Application[Constants.SessionConstants.GRID_PAGE_SIZE] == null)
                {
                    //  Keep page size in session.
                    Application[Constants.SessionConstants.GRID_PAGE_SIZE] = new
                        AdminBLManager().GetAdminSettings().GridPageSize;
                }

                return Convert.ToInt32
                    (Application[Constants.SessionConstants.GRID_PAGE_SIZE]);
            }
        }

        /// <summary>
        /// Sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        public string URL
        {
            get { return this.url; }
            // write property
            set { this.url = value; }
        }

        /// <summary>
        /// Sets the tamper proof params.
        /// </summary>
        /// <value>The tamper proof params.</value>
        public List<string> Parameters
        {
            get { return this.parameters; }
            // write property
            set { this.parameters = value; }
        }

        #endregion Public Properties
    }
}