﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="CandidateUnhandledError.aspx.cs" Inherits="Forte.HCM.UI.Common.CandidateUnhandledError" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="UnhandledError_bodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 980px">
                    <div style="width: 468px; float: left">
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateUnhandledError_errorTitleLabel" runat="server" Text="Error"
                                CssClass="unhandled_error_title"></asp:Label>
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateUnhandledError_errorValueLabel" runat="server" Text="Unhandled error"
                                CssClass="unhandled_error_message"></asp:Label>
                        </div>
                        <div style="width: 458px; float: left; height: 20px">
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateUnhandledError_messageTitleLabel" runat="server" Text="Message"
                                CssClass="unhandled_error_title"></asp:Label>
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateUnhandledError_messageValueLabel" runat="server" Text=" An unhandled error has occured in the application. Please contact your administrator"
                                CssClass="unhandled_error_message"></asp:Label>
                        </div>
                        <div style="width: 458px; float: left; height: 20px">
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateUnhandledError_detailsTitleLabel" runat="server" Text="Details"
                                CssClass="unhandled_error_title"></asp:Label>
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="CandidateUnhandledError_detailsValueLabel" runat="server" Text="None"
                                CssClass="unhandled_error_message"></asp:Label>
                        </div>
                        <div style="width: 458px; float: left; height: 120px">
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left;" class="unhandled_error_message_link_message">
                            Click here to go to
                            <asp:LinkButton ID="CandidateUnhandledError_homeLinkButton" runat="server" Text="Home"
                                CssClass="unhandled_error_message_link_btn" PostBackUrl="~/Dashboard.aspx" ToolTip="Click here to go to home page">
                            </asp:LinkButton>
                            page
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left;" class="unhandled_error_message_link_message">
                            Click here to go to
                            <asp:LinkButton ID="CandidateUnhandledError_feedbackLinkButton" runat="server" Text="Feedback"
                                CssClass="unhandled_error_message_link_btn" PostBackUrl="~/General/Feedback.aspx"
                                ToolTip="Click here to go to feedback page">
                            </asp:LinkButton>
                            page
                        </div>
                    </div>
                    <div style="width: 508px; float: left" class="unhandled_error_background">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
