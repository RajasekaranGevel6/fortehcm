﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Download.cs
// File that represents the download functionality.
// to the users.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;

using iTextSharp.text;
using Forte.HCM.DataObjects;
using System.Collections.Generic;
using Forte.HCM.Utilities;

#endregion Directives                                                          

namespace Forte.HCM.UI.Common
{
    /// <summary>
    /// Class that represents the download functionality.
    /// </summary>
    public partial class Download : Page
    {
        #region Static Variables                                               

        static BaseColor itemColor = new BaseColor(20, 142, 192);
        static BaseColor headerColor = new BaseColor(40, 48, 51);

        #endregion Static Variables                                            

        #region Event Handlers                                                 

        /// <summary>
        /// Method that is called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e"> 
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void FileDownload_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                    return;

                 // Retrieve the document Id.
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    DownloadResume(Convert.ToInt32(Request.QueryString["id"]));
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "PDF")
                {
                    DownloadPDF();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "DOWNLOADRESUME")
                {
                    DownloadCandidateResume(Request.QueryString["fname"], Request.QueryString["mtype"]);
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "CERTIFICATE")
                {
                    DownloadCertificate();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "CERTIFICATE_TEMPLATE")
                {
                    DownloadCertificateTemplate();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "REPORT")
                {
                    DownloadReport();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "CYBER_PROCTORING_DOCUMENT")
                {
                    DownloadCyberProctoringDocument();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "CYBER_PROCTORING_EXECUTABLE")
                {
                    DownloadCyberProctoringZip();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "OFFLINE_INTERVIEW_RECORDER_DOCUMENT")
                {
                    DownloadOfflineInterviewRecorderDocument();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                    Request.QueryString["type"].ToUpper() == "OFFLINE_INTERVIEW_SETUP")
                {
                    DownloadOfflineInterviewZip();
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                throw ex;
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        private void DownloadReport()
        {
            byte[] candidateResults =
                (byte[])Session[Constants.SessionConstants.REPORT_RESULTS_PDF];
            if (candidateResults == null)
            {
                Logger.ExceptionLog("No PDF is found to download");
                return;
            }
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Request.QueryString["filename"]);
            Response.AddHeader("Content-Length", candidateResults.Length.ToString());
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(candidateResults);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        private void DownloadPDF()
        {
            byte[] candidateResults =
                (byte[])Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF];

            if (candidateResults == null)
            {
                Logger.ExceptionLog("No PDF is found to download");
                return;
            }
            string fileName = string.Empty;
            if (Session["CAND_SESSION_KEY"] != null
                && Session["ATTEMPT_ID"] != null)
                fileName = Session["CAND_SESSION_KEY"].ToString() + "_"
                    + Session["ATTEMPT_ID"].ToString();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + "pdf");
            Response.AddHeader("Content-Length", candidateResults.Length.ToString());
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(candidateResults);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that downloads the cyber proctoring trouble-shooting document.
        /// </summary>
        private void DownloadCyberProctoringDocument()
        {
            string fileName = ConfigurationManager.AppSettings["CYBER_PROCTORING_DOCUMENT"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that downloads the offline interview setup.
        /// </summary>
        private void DownloadCyberProctoringZip()
        {
            string fileName = ConfigurationManager.AppSettings["CYBER_PROCTORING_ZIP"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that downloads the offline interview recorder trouble-shooting 
        /// document.
        /// </summary>
        private void DownloadOfflineInterviewRecorderDocument()
        {
            string fileName = ConfigurationManager.AppSettings["OFFLINE_INTERVIEW_RECORDER_DOCUMENT"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that downloads the offline interview setup.
        /// </summary>
        private void DownloadOfflineInterviewZip()
        {
            string fileName = ConfigurationManager.AppSettings["OFFLINE_INTERVIEW_RECORDER_ZIP"];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            Response.TransmitFile(Server.MapPath("~/Downloads/" + fileName));
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID.
        /// </param>
        private void DownloadCandidateResume(string fileName, string mimeType)
        {
            byte[] resumeContent =
                    (byte[])Session[Constants.SessionConstants.DOWNLOAD_RESUME_CONTENT];
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + mimeType);
            Response.AddHeader("Content-Length", resumeContent.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(resumeContent);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that will download the certificate for the completed test.
        /// </summary>
        private void DownloadCertificate()
        {
            byte[] certificateImage = (byte[])Session[Constants.SessionConstants.CERTIFICATE_IMAGE];
            string certificateFileName = "Certificate";

            if (certificateImage == null)
            {
                Logger.ExceptionLog("No image is found to download");
                return;
            }

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + certificateFileName + ".jpg");
            Response.AddHeader("Content-Length", certificateImage.Length.ToString());
            Response.ContentType = "image/JPEG";
            Response.BinaryWrite(certificateImage);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that will download the certifcate template.
        /// </summary>
        private void DownloadCertificateTemplate()
        {
            byte[] certificateImage = (byte[])Session[Constants.SessionConstants.CERTIFICATE_TEMPLATE_IMAGE];
            string certificateFileName = "CertificateTemplate";

            if (certificateImage == null)
            {
                Logger.ExceptionLog("No image is found to download");
                return;
            }

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + certificateFileName + ".jpg");
            Response.AddHeader("Content-Length", certificateImage.Length.ToString());
            Response.ContentType = "image/JPEG";
            Response.BinaryWrite(certificateImage);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Method that retrieve the contents from the database and download to 
        /// the local system as a file.
        /// </summary>
        /// <param name="candidateResumeID">
        /// A <see cref="int"/> that holds the candidate resume ID.
        /// </param>
        private void DownloadResume(int candidateResumeID)
        {
            try
            {
                string mimeType;
                string fileName;
                byte[] resumeContents = new ResumeRepositoryBLManager().GetResumeContents
                    (candidateResumeID, out mimeType, out fileName);

                if (resumeContents == null)
                {
                    Logger.ExceptionLog("No resume contents found to in the database to download");
                    Response.End();
                }

                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + "." + mimeType);
                Response.AddHeader("Content-Length", resumeContents.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(resumeContents);
                Response.Flush();
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
            }
            finally
            {
                Response.End();

            }
        }

        #endregion Private Methods                                             
    }
}