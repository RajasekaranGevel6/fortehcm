﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestResultsPrintControl.cs
// File that represents the user interface Test Results Control
// This will helps to view the test result


#endregion

#region Directives
using System;
using System.Web.UI;


using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.PrntControls
{
    public partial class CandidateTestResultsPrintControl : System.Web.UI.UserControl
    {
        #region Event Handlers

        /// <summary>
        /// Hanlder method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                throw exception;
            }

        }

        #endregion Event Handlers

        #region Properties
        /// <summary>
        /// Represents the property TestStatisticsDataSource
        /// </summary> 
        public TestStatistics TestStatisticsDataSource
        {
            set;
            get;
        }

        /// <summary>
        /// Represents the property Candidate statistics detail
        /// </summary>
        public CandidateStatisticsDetail CandidateStatisticsDataSource
        {
            set;
            get;
        }

        /// <summary>
        /// Represents the property to display the error message in 
        /// candidate tab
        /// </summary>
        public string DisplayErrorMessage
        {
            set
            {
                //TestResult_errorMessageLabel.Text = value;
            }
        }

        #endregion Properties

        #region Public Methods
        /// <summary>
        /// Represents the method to load the test summary details 
        /// </summary>
        public void LoadTestSummaryDetails()
        {
            if (TestStatisticsDataSource != null)
            {
                //Assign the data source for the test test result control
                TestResults_testStatisticsControl.TestStatisticsDataSource = TestStatisticsDataSource;
            }

            //Load the details 
            TestResults_testStatisticsControl.LoadTestSummaryDetails();
        }

        /// <summary>
        /// Represents the method to load the test and candidate details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <param name="testName">
        /// A<see cref="string"/>that holds the test name
        /// </param>
        /// <param name="candidateName">
        /// A<see cref="string"/>that holds the candidate name
        /// </param>
        /// <param name="creditsEarned">
        /// A<see cref="string"/>that holds the credits earned
        /// </param>
        public void LoadCandidateTestDetails(string testId, string testName,
            string candidateName, DateTime creditsEarned)
        {
            TestResult_testResultHeaderControl.LoadCandidateTestDetails(testId,
                testName, candidateName, creditsEarned);

        }

        /// <summary>
        /// Used to assign the values for the candidate details
        /// </summary>
        public void LoadCandidateStatisticsDetails()
        {
            TestResult_testResulsCanditateControl.LoadCandidateStatisticsDetails(CandidateStatisticsDataSource);
        }



        public void LoadChartData(MultipleSeriesChartData chartCategoryDataSource, MultipleSeriesChartData chartSubjectDataSource,
            MultipleSeriesChartData chartTestAreaDataSource, MultipleSeriesChartData chartComplexityDataSource)
        {
            TestResult_testResulsCanditateControl.chartData(chartCategoryDataSource, chartSubjectDataSource, 
                chartTestAreaDataSource, chartComplexityDataSource);
        }

        /// <summary>
        /// Method that used to load the category chart control 
        /// </summary>
        /// <param name="chartDataSource">
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </param>
        public void LoadCategoryChartControl(MultipleSeriesChartData chartDataSource)
        {

            // TestResultControl_multipleSeriesChartControl.MultipleChartDataSource = chartDataSource;
        }

        /// <summary>
        /// Method that used to load the subject chart control 
        /// </summary>
        /// <param name="chartDataSource">
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </param>
        public void LoadSubjectChartControl(MultipleSeriesChartData chartDataSource)
        {
            //MultipleSeriesChartControl_subjectChart.MultipleChartDataSource = chartDataSource;
        }

        /// <summary>
        /// Method that used to load the test area chart control 
        /// </summary>
        /// <param name="chartDataSource">
        /// A<see cref="MultipleSeriesChartData"/>that holds the chart data
        /// </param>
        public void LoadTestAreaChartControl(MultipleSeriesChartData chartDataSource)
        {
            // MultipleSeriesChartControl_testAreaChart.MultipleChartDataSource = chartDataSource;
        }

        /// <summary>
        /// Represents the method to load the histogram chart control 
        /// </summary>
        /// <param name="chartDataSource">
        /// A<see cref="SingleChartData"/>that holds the single chart data
        /// </param>
        public void LoadHistogramChartControl(ReportHistogramData chartDataSource)
        {
            // TestResultsControl_histogramChartControl.LoadChartControl(chartDataSource);
            TestResult_testResulsCanditateControl.chartHistogramData(chartDataSource);
        }

        #endregion Public Methods
    }
}