﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Configuration;
using System.Web.Configuration; 
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.Common.DL; 
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using Resources;

using Forte.HCM.ResumeParser.Parser;

using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;

namespace Forte.HCM.Candidate
{
    public partial class Signup : PageBase
    {
        /// <summary>
        /// A <see cref="bool"/> that holds the status whether to parser the
        /// resume uploaded by the candidate immediately or later.
        /// </summary>
        protected bool parseImmediately = false;

        protected void Page_Load(object sender, EventArgs e)
        { 
            try
            {
                // Obtain parse immediately status from config file.
                parseImmediately = Convert.ToBoolean(ConfigurationManager.AppSettings["PARSE_IMMEDIATELY"]);

                // Set browser title.
                if (Request.QueryString["activetag"] != null)
                {
                    if (Request.QueryString["activetag"].ToString() == "updateresume")
                        Master.SetPageCaption("Resume Upload");
                    else if (Request.QueryString["activetag"].ToString() == "updateskills")
                        Master.SetPageCaption("Skills");
                    else
                        Master.SetPageCaption("Sign Up");
                }
                else
                    Master.SetPageCaption("Sign Up");

                //Set default button
                Page.Form.DefaultButton = Signup_defaultButton.UniqueID;

                // Clear message and hide labels.
                ClearMessages();

                string candidateSigninType = null;

                if (IsPostBack == false  && Request.QueryString["activetag"] == null)
                {
                    if (Session["USER_DETAIL"] != null)
                    {
                        GotoLandingPage(false);
                    }
                }

                if (!IsPostBack)
                { 
                    Candidate_SignupLinkButton.Attributes.Add("OnClick", "return false;");
                    Candidate_SkillsLinkButton.Attributes.Add("OnClick", "return false;");
                    Candidate_ResumeUploadLinkButton.Attributes.Add("OnClick", "return false;"); 
                 
                    Signin_CandidateNameLabel.Visible = false;

                    CandidateSkillControls(false);

                    UserDetail userdetail = null;

                    SignupCandidate_SkillText.Attributes.Add("onblur", "CheckValues('" +
                        SignupCandidate_SkillText.ClientID + "','" + 
                        SearchSubjectControl_SkillAutoCompleteExtender.ClientID + "')");

                   bool getOpenIDUser= GetOpenIDInformation();
                   Signin_CandidateNameLabel.Visible = true;
                    //For activate candidate account
                   if (Request.QueryString["SESID"] != null && Request.QueryString["P2"] != null)
                   {
                       Signin_CandidateNameLabel.Visible = false;

                       // Get encrypted confirmation code.
                       string encryptedConfirationCode = Request.QueryString
                       [ConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"]].Replace(' ', '+'); 

                       // Get decrypted user name.
                       string userID = new EncryptAndDecrypt().DecryptString(Request.QueryString["SESID"].ToString().Replace(' ','+'));  
                         
                       List<SignupCandidate> candidateInfo=null;
                       candidateInfo = new CandidateBLManager().GetUserInfomation(Convert.ToInt32(userID)); 
                       
                        // Check and update with the database.
                        /*string displayName = new UserRegistrationBLManager().CheckForActivationCodeWithLegalAcceptance
                            (userName, encryptedConfirationCode);  */
                       if (candidateInfo == null) return;

                       SetDirectSignupUserInfo(candidateInfo[0].EmailID, candidateInfo[0].Password, "NORMAL");
                       Set_Activetag(null, null, null);
                       candidateSigninType = "welcome";

                   }

                   if (Session["USER_DETAIL"]!=null) 
                       userdetail = Session["USER_DETAIL"] as UserDetail;

                    LoadYears(); 

                    if (userdetail != null)
                    { 
                        if (Request.QueryString["activetag"] != null)
                            candidateSigninType = Request.QueryString["activetag"].ToString();

                        ViewState["COMMONSKILLS"] = new CandidateBLManager().
                            GetCandidateSearchSkill(""); 

                        SignupCandidate_FirstNameText.Text = CheckObjectValue(userdetail.FirstName);
                        SignupCandidate_LastNameText.Text = CheckObjectValue(userdetail.LastName );
                        SignupCandidate_EmailIDText.Text = CheckObjectValue(userdetail.UserName  );
                        SignupCandidate_PasswordText.Text = (CheckObjectValue( userdetail.Password));
                        SignupCandidate_ReTypePasswordText.Text = SignupCandidate_PasswordText.Text;

                        if (Request.QueryString["SESID"] != null && Request.QueryString["P2"] != null)
                        {
                            Signin_CandidateNameLabel.Visible = false;
                            NewCandidate_WelcomeCandidateNameLabel.Text += SignupCandidate_FirstNameText.Text + " !";
                        }
                        else
                        {
                            Signin_CandidateNameLabel.Visible = true;
                            Signin_CandidateNameLabel.Text = "Hi " + SignupCandidate_FirstNameText.Text;
                        }
                        ViewState["SIGNIN_ACTIVETAG"] = candidateSigninType;
                        Session["SIGNUP_FIRSTNAME"] = SignupCandidate_FirstNameText.Text;
                        Session["SIGNUP_LASTNAME"] = SignupCandidate_LastNameText.Text;

                        if (getOpenIDUser == true)
                            Set_Activetag(null, null,null);

                       if(candidateSigninType !=null)
                                Handling_Tags(candidateSigninType);

                        //Getting candidate profile name
                        if (candidateSigninType=="updateresume")
                        {
                           ResumeStatus resumeStatus=new CandidateBLManager().
                               GetCandidateResumeStatus(base.userID);

                           SignupCandidate_profileNameTextBox.Text = resumeStatus.ResumeName;
                         }
                        //Getting candidate skill details
                        GetCandidateSkill(userdetail.UserID);

                        if (candidateSigninType=="updateskills")
                            ViewState["SKILLTEST"] = OrderSkills();
                    } 
                    else
                        Set_Activetag(NewCandidate_SignUpTag, NewCandidate_SignUpDiv,"signup");
                } 
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
                Handling_Tags("signup");
            }
        }

        private void CandidateSkillControls(bool flag)
        {
            NewSkill_titleLabel.Visible = flag;
            Signup_newSkillYesImageButton.Visible = flag;
            Signup_newSkillNolImageButton.Visible = flag; 
        }

        /// <summary>
        /// Current tag set
        /// </summary>
        /// <param name="activeTag"></param>
        /// <param name="activeDiv"></param>
        private void Set_Activetag(HtmlGenericControl activeTag, 
            HtmlGenericControl activeDiv,string activeVal)
        {
           
                NewCandidate_SignUpDiv.Visible = false;
                NewCandidate_ResumeDiv.Visible = false;
                NewCandidate_SkillsDiv.Visible = false;
                NewCandidate_WelcomeTag.Visible = false;
                Signup_intimationScreen.Visible = false;

                Candidate_SignupLinkButton.Visible = false;
                Candidate_ResumeUploadLinkButton.Visible = false;
                Candidate_SkillsLinkButton.Visible = false;
                    
                NewCandidate_SignUpTag.Attributes.Add("class", "can_tab_bg");
                NewCandidate_ResumeUploadTag.Attributes.Add("class", "can_tab_bg");
                NewCandidate_SkillsTag.Attributes.Add("class", "can_tab_bg");

                NewCandidate_ResumeUploadTag.Visible = false;
                NewCandidate_SkillsTag.Visible = false;
                NewCandidate_SignUpTag.Visible = true;

                Signup_leftCorner.Visible = true;
                Signup_rightCorner.Visible = true; 

                if (activeTag != null && activeDiv != null)
                {
                    activeDiv.Visible = true; 
                    activeTag.Attributes.Add("class", "can_tab_active_bg");
                }
                else if (activeTag == null && activeDiv != null)
                {
                    NewCandidate_ResumeUploadTag.Visible = false;
                    NewCandidate_SkillsTag.Visible = false;
                    NewCandidate_SignUpTag.Visible = false ;
                    Signup_leftCorner.Visible = false;
                    Signup_rightCorner.Visible = false;
                    Signup_intimationScreen.Visible = true; 
                }
                else
                {
                    NewCandidate_ResumeUploadTag.Visible = false;
                    NewCandidate_SkillsTag.Visible = false;
                    NewCandidate_SignUpTag.Visible = false;
                    NewCandidate_WelcomeTag.Visible = true;
                    Signup_leftCorner.Visible = false;
                    Signup_rightCorner.Visible = false;
                }

                if (activeVal == null) return;

                if (activeVal == "signup")
                {
                    Candidate_SignupLinkButton.Visible = true;
                }
                else if(activeVal !="signup")
                {
                    NewCandidate_SignUpTag.Visible = false;
                    Candidate_ResumeUploadLinkButton.Visible = true;
                    Candidate_SkillsLinkButton.Visible = true;
                }
        }

        /// <summary>
        /// Only active aag visible at update time
        /// </summary>
        /// <param name="visibleTag"></param>
        /// <param name="visibleDiv"></param>
        private void Set_Tagvisibility(HtmlGenericControl visibleTag,
            HtmlGenericControl visibleDiv)
        {
            NewCandidate_SignUpDiv.Visible = false;
            NewCandidate_ResumeDiv.Visible = false;
            NewCandidate_SkillsDiv.Visible = false;
            NewCandidate_ResumeUploadTag.Visible = false;
            NewCandidate_SkillsTag.Visible = false;
            NewCandidate_SignUpTag.Visible = false;
            NewCandidate_WelcomeTag.Visible = false;
            Signup_intimationScreen.Visible = false;

            visibleDiv.Visible = true;
            visibleTag.Visible = true;
            visibleTag.Attributes.Add("class", "can_tab_active_bg");
        }

        private void Handling_Tags(string tag)
        {
            SignupCandidate_ResumeUploadYesButton.Enabled = true;
            if (tag == "skills")            
                Set_Activetag(NewCandidate_SkillsTag,NewCandidate_SkillsDiv,"skills");
            else if (tag == "resume")             
                Set_Activetag(NewCandidate_ResumeUploadTag, NewCandidate_ResumeDiv,"resume");
            else if (tag == "updateskills")
            {    
                Set_Tagvisibility(NewCandidate_SkillsTag, NewCandidate_SkillsDiv);
            }
            else if (tag == "updateresume")
            {
                Set_Tagvisibility(NewCandidate_ResumeUploadTag, NewCandidate_ResumeDiv);
            }
            else if (tag=="welcome")
            {
                Set_Activetag(null, null, null);
            }
            else
                Set_Activetag(NewCandidate_SignUpTag, NewCandidate_SignUpDiv, "signup");
                
        }

        /// <summary>
        /// Loading Past 2o years for last yesr used
        /// </summary>
        private void LoadYears()
        {
            Candidate_Skill_LastUsedDropDownList.Items.Clear();
            for (int count = 0; count <20; count++)
            {
                Candidate_Skill_LastUsedDropDownList.
                    Items.Add(Convert.ToString((DateTime.Today.Year - count)));
            }
        }     
      
        private bool CheckForEmailAddressAvailability(string userEmail)
        {
            return new UserRegistrationBLManager().
                CheckUserEmailExists(userEmail, 1);
        }
        private bool IsValidEmailAddress(string userEmailID)
        { 
            Regex regex = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$",
                RegexOptions.Compiled);
            return regex.IsMatch(userEmailID);
        }
        #region Protected Override Methods
        protected override bool IsValidData()
        {
            bool value = true;

            if (SignupCandidate_FirstNameText.Text.Trim().Length == 0)
                value = false;
            
            if (SignupCandidate_LastNameText.Text.Trim().Length == 0)
                value = false;
            
            if (SignupCandidate_EmailIDText.Text.Trim().Length == 0)
                value = false;            

            if (SignupCandidate_PasswordText.Text.Trim().Length == 0)
               value = false;
             
            if (SignupCandidate_ReTypePasswordText.Text.Trim().Length == 0)
               value = false;
             
            if (!SignupCandidate_agreeCheckBox.Checked)
                value = false;

            return value;
        }
        protected override  void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Override Methods         
         
        /// <summary>
        /// Direct signup user information storing in session
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        private void SetDirectSignupUserInfo(string userName,string password,string loginType)
        { 
            UserDetail userDetail = null;
            if(loginType=="NORMAL")
                userDetail = new AuthenticationBLManager().GetAuthenticateCandidate
                          (userName, password, 0);
            else
                userDetail = new AuthenticationBLManager().GetAuthenticateCandidate
                          (userName, null, 0);

            Session["USER_DETAIL"] = userDetail;

            FormsAuthentication.SetAuthCookie(userName, true);

            try
            {
                HttpCookie cookie = Request.Cookies["FORTEHCM_CANDIDATE_DETAILS"];
                if (cookie == null)
                {
                    cookie = new HttpCookie("FORTEHCM_CANDIDATE_DETAILS");
                }

                cookie["FHCM_USER_NAME"] = userName;
                cookie["FHCM_PASSWORD"] = password;

                cookie.Expires = DateTime.Now.AddMonths
                    (Convert.ToInt32(ConfigurationManager.AppSettings["REMEMBER_ME_MONTHS"]));
                Response.AppendCookie(cookie);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                Handling_Tags("signup");
            }
        }

        protected void SignupCandidate_LoginButton_Click(object sender, EventArgs e)
        {
            if (!IsValidData())
            {
                base.ShowMessage(SignIn_errorMessageLabel,
                    HCMResource.NewCandidate_EnterRequiredFields);
                Set_Activetag(NewCandidate_SignUpTag, NewCandidate_SignUpDiv,"signup"); 
                return;
            }
            if (!IsValidEmailAddress(
                SignupCandidate_EmailIDText.Text.Trim()))
            {
                base.ShowMessage(SignIn_errorMessageLabel,
                    HCMResource.NewCandidate_EnterValidEmailID);
                Set_Activetag(NewCandidate_SignUpTag, NewCandidate_SignUpDiv,"signup"); 
                return;
            }

            if (SignupCandidate_PasswordText.Text.Trim().ToString() !=
                SignupCandidate_ReTypePasswordText.Text.Trim().ToString())
            {
                base.ShowMessage(SignIn_errorMessageLabel,"Password does not match the retyped password");
                Set_Activetag(NewCandidate_SignUpTag, NewCandidate_SignUpDiv,"signup");
                SignupCandidate_ReTypePasswordText.Text = "";
                SignupCandidate_PasswordText.Focus();
                return;
            }

            IDbTransaction transaction =  
                new TransactionManager().Transaction;

            try
            {
                if (!CheckForEmailAddressAvailability(SignupCandidate_EmailIDText.Text.Trim()))
                {
                    base.ShowMessage(SignIn_errorMessageLabel,
                      "Already exists this email");
                  Set_Activetag(NewCandidate_SignUpTag, NewCandidate_SignUpDiv,"signup"); 
                    return;
                }
                CandidateInformation candidateInformation 
                    = new CandidateInformation();

                Signin_CandidateNameLabel.Visible = false;
                if (SignupCandidate_FirstNameText.Text.Trim() != "")
                {
                    Signin_CandidateNameLabel.Visible = true;
                    Signin_CandidateNameLabel.Text =
                        "Hi " + SignupCandidate_FirstNameText.Text;

                    NewCandidate_WelcomeCandidateNameLabel.Text 
                        += SignupCandidate_FirstNameText.Text +" !";
                }

                candidateInformation.caiFirstName = 
                    SignupCandidate_FirstNameText.Text.Trim();
                
                candidateInformation.caiIsActive='N';
                
                candidateInformation.caiLastName =
                    SignupCandidate_LastNameText.Text.Trim();

                candidateInformation.caiEmail = 
                    SignupCandidate_EmailIDText.Text.Trim();

                candidateInformation.UserName =
                    SignupCandidate_EmailIDText.Text.Trim();
                
                candidateInformation.caiOpenEmailID = "N";
                candidateInformation.caiSelfSignIn = "Y";
                
                string password = SignupCandidate_PasswordText.Text.Trim(); 

                candidateInformation.Password =
                  new EncryptAndDecrypt().EncryptString(
                  SignupCandidate_PasswordText.Text.Trim()); 
                 
                candidateInformation.caiType = "CT_MANUAL";
                candidateInformation.caiEmployee = 'N';
                candidateInformation.caiLimitedAccess = false ;
                candidateInformation.caiCreatedBy = 1;

                int candidateID = 
                  new ResumeRepositoryBLManager().
                    InsertSignupCandidate(candidateInformation, 
                   1, 1,transaction); 

                if (candidateID > 0) 
                {
                    candidateInformation.caiID = candidateID;
                    SaveUserName(candidateInformation, transaction);
                }
                transaction.Commit();
                

                SendConfirmationCodeEmail(candidateInformation.UserName,
                     password, GetConfirmationCode(), candidateInformation.
                     caiFirstName,candidateInformation.caiLastName );
                 
                Set_Activetag(null, Signup_intimationScreen, null);
                /*SetDirectSignupUserInfo(candidateInformation.UserName, candidateInformation.Password,"NORMAL");
                Set_Activetag(null,null,null);*/
               // Set_Activetag(NewCandidate_SkillsTag, NewCandidate_SkillsDiv); 
             }
            catch (Exception ex)
            {
                transaction.Rollback(); 
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel,
                    ex.Message);
                Handling_Tags("signup");
            }
        } 
        /// <summary>
        /// Saves the name of the user.
        /// </summary>
        private void SaveUserName(CandidateInformation candidateInformation,
            IDbTransaction transaction)
        {   
            string confirmationCode = GetConfirmationCode(); 

            int result = new ResumeRepositoryBLManager().
                InsertSignupUsers(1,candidateInformation,
               1, confirmationCode, transaction); 
        }

        /// <summary>
        /// A Method that generates a random string and used 
        /// the encrypted string as confirmation code for a user
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that holds the encrypted string
        /// </returns>
        private string GetConfirmationCode()
        {
            return new EncryptAndDecrypt().EncryptString(RandomString(15));
        }

        /// <summary>
        /// Method that will generate a random string composed of captcha
        /// image characters.
        /// </summary>
        /// <param name="size">
        /// A <see cref="int"/> that holds the size.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that holds the generated characters.
        /// </returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            Random randomLower = new Random();
            Random randomNumeric = new Random();
            Random randomNum = new Random();
            int rndNum = 0;
            char ch;
            string invalidChars = "0oOiI1Q";

            for (int i = 0; i < size; )
            {
                rndNum = Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65));

                if (randomNumeric.Next(1, 4) == 1)
                {
                    rndNum = randomNum.Next(2, 9);
                    builder.Append(rndNum);
                    i++;
                }
                else
                {
                    ch = Convert.ToChar(rndNum);

                    if (!invalidChars.Contains(ch.ToString()))
                    {
                        if (randomLower.Next(1, 4) == 1)
                        {
                            ch = (char)(ch + 32);
                        }
                        builder.Append(ch);
                        i++;
                    }
                }
            }

            return builder.ToString().ToUpper();
        }

        protected void SignupCandidate_AddSkillButton_Click(object sender, EventArgs e)
        {    
            if (string.IsNullOrEmpty(SignupCandidate_SkillText.Text.Trim()))
            {
                base.ShowMessage(SignIn_errorMessageLabel,
                    HCMResource.NewCandidate_EnterRequiredFields);

                if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "updateskills") 
                    Set_Tagvisibility (NewCandidate_SkillsTag , NewCandidate_SkillsDiv);
                else
                    Set_Activetag(NewCandidate_SkillsTag, NewCandidate_SkillsDiv,"skills");
                     
                return;
            }

            if(CheckCandidateSkill(SignupCandidate_SkillText.Text.Trim()))
            {
                 base.ShowMessage(SignIn_errorMessageLabel,
                    "Selected skill already exists");
                return;
            }

            if(ViewState["COMMONSKILLS"]==null)
            {
                ViewState["COMMONSKILLS"] = new CandidateBLManager().
                            GetCandidateSearchSkill("");
            }

            string[] skills = ViewState["COMMONSKILLS"] as string[];

            if (skills.Length > 0)
            {
                bool checkAvailableSkill = false;
                foreach (string skillName in skills)
                {
                    if (skillName.ToString().Trim().ToUpper() == SignupCandidate_SkillText.Text.Trim().ToUpper())
                    {
                        checkAvailableSkill = true;
                        break;
                    }
                    else
                        checkAvailableSkill = false;
                }
                 
                if (checkAvailableSkill == false)
                {
                    CandidateSkillControls(true); 
                   return;       
                }
                else
                    CandidateSkillControls(false); 
            }  


            IDbTransaction transaction = new TransactionManager().Transaction;

            try 
            {
                SaveSkill("O", transaction);

                ViewState["SKILLTEST"] = OrderSkills();
            }
            catch (Exception ex) 
            {
                transaction.Rollback();
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel,
                    ex.Message);
                Handling_Tags("skills");
            }

            if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "updateskills")
                Set_Tagvisibility (NewCandidate_SkillsTag , NewCandidate_SkillsDiv);
            else 
                Set_Activetag(NewCandidate_SkillsTag , NewCandidate_SkillsDiv,"skills") ; 
        }

        private void SaveSkill(string skillType, IDbTransaction transaction)
        {
            SignupCandidate signupCandidate = new SignupCandidate();

            UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

            if (userDetail == null) 
                return;

            signupCandidate.CandidateID =
                Convert.ToInt32(userDetail.UserID);

            signupCandidate.CandidateSkill =
                SignupCandidate_SkillText.Text.Trim();

            signupCandidate.ExperienceYears = Candidate_ExperienceYearDropDownList.SelectedItem.Text;

            signupCandidate.LastUsed = Candidate_Skill_LastUsedDropDownList.SelectedItem.Text;

            signupCandidate.CandidateCreatedBy = 1;

            signupCandidate.SkillType = skillType;

            int SkillID = new CandidateBLManager().
                InsertSignupCandidateSkill(signupCandidate, transaction);

            transaction.Commit();

            GetCandidateSkill(userDetail.UserID);

            SignupCandidate_SkillText.Text = string.Empty;
            SignupCandidate_SkillText.Focus();
            CandidateSkillControls(false);  
        }

        private bool CheckCandidateSkill(string skill)
        {
            bool blnFlag = false;
            foreach (GridViewRow gv in SignupCandidate_GridViewCandidateSkills.Rows)
            {
                if (skill.ToUpper() == gv.Cells[1].Text.ToUpper().Trim())
                {
                    blnFlag=true;
                    break;
                }
            }
            return blnFlag;
        }

        private void GetCandidateSkill(int userID)
        { 
            SignupCandidate_GridViewCandidateSkills.DataSource = null;
            SignupCandidate_GridViewCandidateSkills.DataBind(); 

            SignupCandidate_GridViewCandidateSkills.DataSource =
                new CandidateBLManager().GetCandidateSkills
                (userID);

            SignupCandidate_GridViewCandidateSkills.DataBind();  
        }

        protected void SignupCandidate_GridViewCandidateSkills_RowDeleting(
           object sender, GridViewDeleteEventArgs e)
        {
            /*IDbTransaction transaction =
              new TransactionManager().Transaction;

            try
            { 
                HiddenField HFTempValue = (HiddenField)SignupCandidate_GridViewCandidateSkills.
                    Rows[e.RowIndex].FindControl("SignupCandidate_SkillIDHiddenField");

                if (string.IsNullOrEmpty(HFTempValue.Value)) return;  
           
                UserDetail userDetail =
                                  Session["USER_DETAIL"] as UserDetail;

                int SkillID = new SignupCandidateBLManager().DeleteCandidateSkill(
                    userDetail.UserID,
                    Convert.ToInt32(HFTempValue.Value), transaction);

                transaction.Commit();

                GetCandidateSkill(userDetail.UserID);
            }
            catch (Exception ex)
            { 
                transaction.Rollback(); 
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel,
             ex.Message);
            }*/

            if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "updateskills")
                Set_Tagvisibility(NewCandidate_SkillsTag, NewCandidate_SkillsDiv);
            else
                Set_Activetag(NewCandidate_SkillsTag , NewCandidate_SkillsDiv,"skills"); 
                
        }
        
        /// <summary>
        /// check the object is null or not
        /// </summary>
        /// <param name="candidateinfo"></param>
        /// <returns></returns>
        private string CheckObjectValue(object candidateinfo)
        {
            if (candidateinfo == null) return "";
            else
                return Convert.ToString(candidateinfo);
        }

        protected void SignupCandidate_toLandingPage_Click(object sender, EventArgs e)
        {
            try
            {
                // Retrieve user detail.
                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                if (userDetail == null)
                    return;

                // Set the reminder.
                ResumeReminderDetail reminderDetail = new ResumeReminderDetail();
                reminderDetail.EmailID = userDetail.Email;
                reminderDetail.ReminderDate = DateTime.Now.AddDays(Convert.ToInt32(ConfigurationManager.AppSettings["RESUME_REMINDER_DAYS"]));
                reminderDetail.UserID = base.userID;

                new CandidateBLManager().InsertResumeReminder(reminderDetail);

                GotoLandingPage(false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
            }
        }

        protected void Signupcandidate_tabChange_Click(object sender, EventArgs e)
        {
            if (!NewCandidate_ResumeUploadTag.Visible)
            {
                ScriptManager.RegisterStartupScript(this,
                    this.GetType(), "script", "location.replace('signup.aspx?activetag=updateresume');", true);  
            }
            Set_Activetag(NewCandidate_ResumeUploadTag , NewCandidate_ResumeDiv,"resume");          
        }

        /// <summary>
        /// Handler will be called when the browse button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> holds the event data.
        /// </param>
        /// <remarks>
        /// This will apply to upload the file to server.
        /// </remarks>
        protected void ResumeEditor_fileUpload_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                // Reset parsing completed status.
                Signup_parsingCompletedHiddenField.Value = "N";

                if (!ResumeUploader_fileUpload.HasFile)
                {
                    if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "resume")
                        Set_Activetag(NewCandidate_ResumeUploadTag, NewCandidate_ResumeDiv,"resume");   
                    else if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "updateresume")
                        Set_Tagvisibility(NewCandidate_ResumeUploadTag, NewCandidate_ResumeDiv);   

                    return;
                }
                // Validation for file extension
                if (((!Path.GetExtension(e.FileName).ToLower().Contains(".doc")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".docx")) &&
                    (!Path.GetExtension(e.FileName).ToLower().Contains(".pdf"))))
                {
                    ShowMessage(SignIn_errorMessageLabel, Resources.
                        HCMResource.ResumeParser_PleaseProvideAWordFile);
                    return;
                }

                UserDetail userDetail =
                                  Session["USER_DETAIL"] as UserDetail;

                if (userDetail == null) return;

                // To create a directory in the server
               Directory.CreateDirectory(Server.MapPath("~/") + "\\Resumes");
               // To save word file as 
                string saveAsPath = Server.MapPath("~/") + "Resumes\\"
                    + userDetail.CandidateInfoID + "_" + ResumeUploader_fileUpload.FileName;
                if (File.Exists(saveAsPath) != true)
                {   
                    ResumeUploader_fileUpload.SaveAs(saveAsPath);
                }
                ViewState["UploadResumeName"] = ResumeUploader_fileUpload.FileName;
                //For Temporary using by MKN
                ViewState["UploadResumePath"] = saveAsPath;

                if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "resume")
                    Set_Activetag(NewCandidate_ResumeUploadTag, NewCandidate_ResumeDiv,"resume");   
                else if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "updateresume")
                    Set_Tagvisibility(NewCandidate_ResumeUploadTag, NewCandidate_ResumeDiv); 
                 
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel, ex.Message);
                Handling_Tags("resume");
            }
        }
        
        private void GotoLandingPage(bool resumeUploaded)
        {
            if (ViewState["SKILLTEST"] != null)
            {
                string testCount = GetSkill_TestCount(ViewState["SKILLTEST"].ToString());
                if (testCount != "")
                {
                    Session["SEARCH_TEST"] = string.Empty;
                    Session["SEARCH_TEST"] = ViewState["SKILLTEST"].ToString();
                    Response.Redirect("~/CandidateAdmin/CandidateSearchTest.aspx", false);
                }
                else
                {
                    // Keep resume uploaded status in session.
                    Session["RESUME_UPLOADED"] = resumeUploaded;

                    Response.Redirect("~/Dashboard.aspx", false);
                }
            }
            else
            {
                // Keep resume uploaded status in session.
                Session["RESUME_UPLOADED"] = resumeUploaded;

                Response.Redirect("~/Dashboard.aspx", false);
            }
        }

        protected void SignupCandidate_uploadResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Reset the parsing completed status.
                Signup_parsingCompletedHiddenField.Value = "N";

                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
                 
                if (userDetail == null || ResumeUploader_fileUpload.HasFile == false)
                {

                    if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "resume") 
                        Set_Activetag(NewCandidate_ResumeUploadTag, NewCandidate_ResumeDiv,"resume");   
                    else if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "updateresume") 
                        Set_Tagvisibility(NewCandidate_ResumeUploadTag, NewCandidate_ResumeDiv);   

                    base.ShowMessage(SignIn_errorMessageLabel, "Select your resume");
                    return; 
                }
                else
                {
                    if (SignupCandidate_profileNameTextBox.Text.Trim().Length == 0)
                    {
                        base.ShowMessage(SignIn_errorMessageLabel, "Enter profile name");
                        SignupCandidate_profileNameTextBox.Focus();
                        return;
                    }

                    // Perform immediate or later parsing based on the config 
                    // setting.
                    if (parseImmediately == true)
                    {
                        // Parse resume.
                        ParseResume();
                    }
                    else
                    {
                        // Upload resume to parser folder.
                        UploadResume(userDetail, SignupCandidate_profileNameTextBox.Text.Trim());

                        // Goto landing page.
                        GotoLandingPage(true);
                    }
                }
            }
            catch (Exception exp)
            {
                Signup_parsingCompletedHiddenField.Value = "Y";
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
                Handling_Tags("resume");
            }
        }

        /// <summary>
        /// Method that parse the uploaded resume and navigates to the edit resume page.
        /// </summary>
        private void ParseResume()
        {
            try
            {
                string fileName;

                // Retrieve user detail from session.
                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                // Insert parser candidate information and retreive the ID.
                int parserCandidateID = new ResumeRepositoryBLManager().InsertParserCandidateInformation
                    (userDetail.CandidateInfoID, "RUT_CSU", ResumeUploader_fileUpload.FileName, base.userID);

                // Upload the resume.
                bool uploadStatus = UploadResume(out fileName, parserCandidateID);

                if (uploadStatus == false)
                {
                    Signup_resumeParserRotatingImageDiv.Visible = false;
                    Signup_resumeParserMessageTitleLabel.Text = "Error Occurred";
                    Signup_resumeParserMessageLabel.Text = "There was an error in uploading the resume. Please try later";
                    Signup_resumeParserCloseButton.Visible = true;
                    Signup_resumeParserMessageModalPopupExtender.Show();
                    Signup_parsingCompletedHiddenField.Value = "Y";
                    return;
                }

                ConverterSettings converterSettings = new ConverterSettings();
                converterSettings.ConvertedResumesPath = ConfigurationManager.AppSettings["CANDIDATE_CONVERSION_SUCCEEDED_FOLDER"];
                converterSettings.ConversionTemporaryPath = ConfigurationManager.AppSettings["CANDIDATE_CONVERSION_TEMP_FOLDER"];
                converterSettings.ConversionFailedPath = ConfigurationManager.AppSettings["CANDIDATE_CONVERSION_FAILED_FOLDER"];
                converterSettings.ReadyToParsePath = ConfigurationManager.AppSettings["CANDIDATE_READY_TO_PARSE_FOLDER"];
                converterSettings.UserID = base.userID;

                HCM.ResumeParser.Parser.ParserSettings parserSettings = new HCM.ResumeParser.Parser.ParserSettings();
                parserSettings.ProcessPath = ConfigurationManager.AppSettings["CANDIDATE_PARSING_ON_PROCESS_FOLDER"];
                parserSettings.SucceededPath = ConfigurationManager.AppSettings["CANDIDATE_PARSING_SUCCEEDED_FOLDER"];
                parserSettings.FailedPath = ConfigurationManager.AppSettings["CANDIDATE_PARSING_FAILED_FOLDER"];
                parserSettings.ProjectFailedPath = ConfigurationManager.AppSettings["CANDIDATE_PROJECT_FAILED_FOLDER"];
                parserSettings.HRXMLPath = ConfigurationManager.AppSettings["CANDIDATE_HRXML_FOLDER"];
                parserSettings.UserID = base.userID;

                // Get Parser instance.
                IParser parser = ParserFactory.GetInstance(converterSettings, parserSettings);

                // Convert resume
                bool convertResult = parser.ConvertDocument(fileName, parserCandidateID);

                if (convertResult == false)
                {
                    Signup_resumeParserRotatingImageDiv.Visible = false;
                    Signup_resumeParserMessageTitleLabel.Text = "Error Occurred";
                    Signup_resumeParserMessageLabel.Text = "There was an error in uploading the resume. Please try later";
                    Signup_resumeParserCloseButton.Visible = true;
                    Signup_resumeParserMessageModalPopupExtender.Show();
                    Signup_parsingCompletedHiddenField.Value = "Y";
                    return;
                }

                // Parse resume.
                string parseFile = ConfigurationManager.AppSettings["CANDIDATE_READY_TO_PARSE_FOLDER"] + "\\"
                    + parserCandidateID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                    Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".txt";

                bool parserResult = parser.Parse(parseFile, parserCandidateID);

                if (parserResult == false)
                {
                    Signup_resumeParserRotatingImageDiv.Visible = false;
                    Signup_resumeParserMessageTitleLabel.Text = "Error Occurred";
                    Signup_resumeParserMessageLabel.Text = "There was an error in parsing the resume. Please try later";
                    Signup_resumeParserCloseButton.Visible = true;
                    Signup_parsingCompletedHiddenField.Value = "Y";
                    Signup_resumeParserMessageModalPopupExtender.Show();
                    return;
                }

                Signup_resumeParserRotatingImageDiv.Visible = false;
                Signup_resumeParserMessageTitleLabel.Text = "Parsing Succeeded";
                Signup_resumeParserMessageLabel.Text = "Your resume has been parsed successfully. You can now review and approve it";
                Signup_resumeParserCloseButton.Visible = false;
                Signup_resumeParserApproveButton.Visible = true;
                Signup_resumeParserDashboardButton.Visible = true;
                Signup_parsingCompletedHiddenField.Value = "Y";
                Signup_resumeParserMessageModalPopupExtender.Show();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                Signup_parsingCompletedHiddenField.Value = "Y";
                Signup_resumeParserRotatingImageDiv.Visible = false;
                Signup_resumeParserMessageTitleLabel.Text = "Error Occurred";
                Signup_resumeParserMessageLabel.Text = "There was an error in parsing the resume. Please try later";
                Signup_resumeParserCloseButton.Visible = true;
                Signup_parsingCompletedHiddenField.Value = "Y";
                Signup_resumeParserMessageModalPopupExtender.Show();
            }
        }

        /// <summary>
        /// Method that uploads the resume.
        /// </summary>
        /// <param name="fileName">
        /// A <see cref="string"/> that holds the file name as an output parameter.
        /// </param>
        /// <param name="parserCandidateID">
        /// A <see cref="int"/> that holds the parser candidate ID. This ID will
        /// be prefixed with the file name.
        /// </param>
        private bool UploadResume(out string fileName, int parserCandidateID)
        {
            // Assign default values to output parameters.
            fileName = string.Empty;

            UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

            bool uploaded = false;

            if (!ResumeUploader_fileUpload.HasFile) 
                return false;

            // To check the file is present or not
            if (ResumeUploader_fileUpload.HasFile)
            {
                // Save file in the server and get the file name
                fileName = SaveUploadedFileAs(ConfigurationManager.AppSettings
                    ["CANDIDATE_UPLOADED_RESUMES_FOLDER"], parserCandidateID);

                //If file name is empty return
                if (fileName == string.Empty)
                    return false;

                uploaded = true;
            }

            if (uploaded)
            {
                // Insert the log entry.
                int activityLogID = new ResumeRepositoryBLManager().InsertUploadResumeLog(userDetail.CandidateInfoID,
                        Convert.ToString(Session["SIGNUP_FIRSTNAME"]), ResumeUploader_fileUpload.FileName,
                        base.userID, SignupCandidate_profileNameTextBox.Text.Trim());
            }

            return uploaded;
        }

        /// <summary>
        /// Method that uploads the resume.
        /// </summary>
        private void UploadResume(UserDetail userDetail,string profileName)
        { 
            bool uploaded = false;

           if (!ResumeUploader_fileUpload.HasFile) return;  
                 
            // To check the file is present or not
            if(ResumeUploader_fileUpload.HasFile)
            {
                //Save file in the server and get the file name
                string fileName = SaveUploadedFileAs();

                //If file name is empty return
                if (fileName == string.Empty)
                    return;
                uploaded = true;
            }
            else if (Session["PATH_NAME"].ToString() != "")
            {
                string strFilePath = ConfigurationManager.AppSettings
                    ["UPLOADED_RESUMES_PATH"].ToString(); 

                //To create a directory in the server 
                int userID = userDetail.CandidateInfoID;

                string saveAsPath = string.Empty;

                if (Path.GetExtension(Session["PATH_NAME"].ToString()).ToLower() == ".doc")
                {
                    //To save word file as 
                    saveAsPath = strFilePath + "\\" + userID + "_"
                        + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                        Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".doc";
                }

                if (Path.GetExtension(Session["PATH_NAME"].ToString()).ToLower() == ".pdf")
                {
                    //To save pdf file as 
                    saveAsPath = strFilePath + "\\"
                        + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".pdf";
                }
                if (Path.GetExtension(Session["PATH_NAME"].ToString()).ToLower() == ".docx")
                {
                    //To save pdf file as 
                    saveAsPath = strFilePath + "\\"
                        + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".docx";
                }
                if (Path.GetExtension(Session["PATH_NAME"].ToString()).ToLower() == ".rtf")
                {
                    //To save pdf file as 
                    saveAsPath = strFilePath + "\\"
                        + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".rtf";
                }

                File.Copy(Session["PATH_NAME"].ToString(), saveAsPath, true);

                ResumeUploader_fileUpload.Enabled = false;
                uploaded = true;
            }
            else if (ResumeUploader_fileUpload.Attributes["filename"] != "")
            {
                string strFilePath = ConfigurationManager.AppSettings
                    ["UPLOADED_RESUMES_PATH"].ToString();

                //To create a directory in the server
                //Directory.CreateDirectory(Server.MapPath("~/") + "\\" + strFilePath);
                int userID = userDetail.UserID;

                string saveAsPath = string.Empty;

                if (Path.GetExtension(ResumeUploader_fileUpload.Attributes["filename"]).ToLower() == ".doc")
                {
                    //To save word file as 
                    saveAsPath = strFilePath + "\\" + userID + "_" +
                        Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".doc";
                }

                if (Path.GetExtension(ResumeUploader_fileUpload.Attributes["filename"]).ToLower() == ".pdf")
                {
                    //To save pdf file as 
                    saveAsPath = strFilePath + "\\"
                        + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".pdf";
                }

                if (Path.GetExtension(ResumeUploader_fileUpload.Attributes["filename"]).ToLower() == ".docx")
                {
                    //To save pdf file as 
                    saveAsPath = strFilePath + "\\"
                        + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".docx";
                }

                if (Path.GetExtension(ResumeUploader_fileUpload.Attributes["filename"]).ToLower() == ".rtf")
                {
                    //To save pdf file as 
                    saveAsPath = strFilePath + "\\"
                        + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".rtf";
                }

                File.Copy(ResumeUploader_fileUpload.Attributes["filename"], saveAsPath, true);

                ResumeUploader_fileUpload.Enabled = false;

                uploaded = true;
            }
            else
            {
                ShowMessage(SignIn_errorMessageLabel,
                    Resources.HCMResource.ResumeUploader_SelectFileToUpload);
                return;
            }

            if (uploaded)
            {
                // Insert the log entry.
                int activityLogID = new ResumeRepositoryBLManager().InsertUploadResumeLog(userDetail.CandidateInfoID,
                        Convert.ToString(Session["SIGNUP_FIRSTNAME"]), Convert.ToString(Session["UPLOADED_FILE_NAME"]),
                        base.userID,profileName);
            } 
        }

        /// <summary>
        /// Method that saves the uploaded file into the specific path.
        /// </summary>
        /// <param name="path">
        /// A <see cref="string"/> that holds the path.
        /// </param>
        /// <param name="parserCandidateID">
        /// A <see cref="int"/> that holds the parser candidate ID. This ID will
        /// be prefixed with the file name.
        /// </param>
        private string SaveUploadedFileAs(string path, int parserCandidateID)
        {
            try
            {
                string fileName = ResumeUploader_fileUpload.FileName;

                UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                //Check if the uploaded file is word or pdf file
                if ((fileName == string.Empty) ||
                     ((ResumeUploader_fileUpload.PostedFile.ContentType
                     != "application/msword") && (ResumeUploader_fileUpload.PostedFile.ContentType != "application/ms-word") && (ResumeUploader_fileUpload.PostedFile.ContentType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && (ResumeUploader_fileUpload.PostedFile.ContentType
                     != "application/pdf")) ||
                      ((Path.GetExtension(fileName).ToLower() != ".doc") && (Path.GetExtension(fileName).ToLower() != ".pdf") && (Path.GetExtension(fileName).ToLower() != ".rtf") &&
                      (Path.GetExtension(fileName).ToLower() != ".docx")))
                {
                    ShowMessage(SignIn_errorMessageLabel,
                        Resources.HCMResource.ResumeUploader_PleaseProvideAWordFile);
                }
                else if (fileName != string.Empty)
                {
                    //To create a directory in the server
                    //Directory.CreateDirectory(Server.MapPath("~/") + "\\" + strFilePath);

                    string saveAsPath = string.Empty;

                    if(Path.GetExtension(ResumeUploader_fileUpload.FileName).ToLower() == ".doc")
                    {
                        //To save word file as 
                        saveAsPath = path + "\\"
                            + parserCandidateID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                             Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".doc";
                    }

                    if(Path.GetExtension(ResumeUploader_fileUpload.FileName).ToLower() == ".pdf")
                    {
                        //To save pdf file as 
                        saveAsPath = path + "\\"
                            + parserCandidateID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".pdf";
                    }
                    if(Path.GetExtension(ResumeUploader_fileUpload.FileName).ToLower() == ".rtf")
                    {
                        //To save pdf file as 
                        saveAsPath = path + "\\"
                            + parserCandidateID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".rtf";
                    }
                    if (Path.GetExtension(ResumeUploader_fileUpload.FileName).ToLower() == ".docx")
                    {
                        //To save pdf file as 
                        saveAsPath = path + "\\"
                            + parserCandidateID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                             Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".docx";
                    }

                    if (File.Exists(saveAsPath) == true)
                    {
                        File.Delete(saveAsPath);
                    }
                     
                    ResumeUploader_fileUpload.SaveAs(saveAsPath);
                    return saveAsPath;
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel, ex.Message);
            }

            return string.Empty;
        }

        /// <summary>
        /// This method helps to save the file into specified folder on server.
        /// </summary>
        private string SaveUploadedFileAs()
        {
            try
            {
                string fileName = ResumeUploader_fileUpload.FileName;
                string strFilePath = ConfigurationManager.AppSettings["UPLOADED_RESUMES_PATH"].ToString();

                UserDetail userDetail =
                                  Session["USER_DETAIL"] as UserDetail;

                //Check if the uploaded file is word or pdf file
                if ((fileName == string.Empty) ||
                     ((ResumeUploader_fileUpload.PostedFile.ContentType
                     != "application/msword") && (ResumeUploader_fileUpload.PostedFile.ContentType != "application/ms-word") && (ResumeUploader_fileUpload.PostedFile.ContentType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && (ResumeUploader_fileUpload.PostedFile.ContentType
                     != "application/pdf")) ||
                      ((Path.GetExtension(fileName).ToLower() != ".doc") && (Path.GetExtension(fileName).ToLower() != ".pdf") && (Path.GetExtension(fileName).ToLower() != ".rtf") &&
                      (Path.GetExtension(fileName).ToLower() != ".docx")))
                {
                    ShowMessage(SignIn_errorMessageLabel,
                        Resources.HCMResource.ResumeUploader_PleaseProvideAWordFile);
                }
                else if (fileName != string.Empty)
                {
                    //To create a directory in the server
                    //Directory.CreateDirectory(Server.MapPath("~/") + "\\" + strFilePath);
                    int userID = userDetail.CandidateInfoID;

                    string saveAsPath = string.Empty;

                    if (Path.GetExtension(ResumeUploader_fileUpload.FileName).ToLower() == ".doc")
                    {
                        //To save word file as 
                        saveAsPath = strFilePath + "\\"
                            + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                             Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".doc";
                    }

                    if (Path.GetExtension(ResumeUploader_fileUpload.FileName).ToLower() == ".pdf")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\"
                            + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".pdf";
                    }
                    if (Path.GetExtension(ResumeUploader_fileUpload.FileName).ToLower() == ".rtf")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\"
                            + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                            Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".rtf";
                    }
                    if (Path.GetExtension(ResumeUploader_fileUpload.FileName).ToLower() == ".docx")
                    {
                        //To save pdf file as 
                        saveAsPath = strFilePath + "\\"
                            + userID + "_" + Convert.ToString(Session["SIGNUP_FIRSTNAME"]) + "_" +
                             Convert.ToString(Session["SIGNUP_LASTNAME"]) + ".docx";
                    }

                    if (File.Exists(saveAsPath) == true)
                    {
                        File.Delete(saveAsPath);
                    }

                    ResumeUploader_fileUpload.SaveAs(saveAsPath);
                    return saveAsPath;
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel, ex.Message);
            }

            return string.Empty;
        }
         
        protected void OpenLogin_Click(object src, CommandEventArgs e)
        {
            try
            {
                string commandArg = e.CommandArgument.ToString();
                OpenIdRelyingParty openIDURL = new OpenIdRelyingParty();

                var uriBuilder = new UriBuilder(Request.Url) { Query = "" };
                var request = openIDURL.CreateRequest(commandArg, uriBuilder.Uri, uriBuilder.Uri);

                if (request.ClaimedIdentifier == null)
                {
                    Identifier id;
                    if (Identifier.TryParse(commandArg, out id))
                    {
                        try
                        {
                            var fetch = new FetchRequest();
                            fetch.Attributes.AddRequired(WellKnownAttributes.Name.FullName);
                            fetch.Attributes.AddRequired(WellKnownAttributes.Name.First);
                            fetch.Attributes.AddRequired(WellKnownAttributes.Name.Middle);
                            fetch.Attributes.AddRequired(WellKnownAttributes.Name.Last);
                            fetch.Attributes.AddRequired(WellKnownAttributes.Person.Gender);
                            fetch.Attributes.AddRequired(WellKnownAttributes.Contact.Email);
                            fetch.Attributes.AddRequired(WellKnownAttributes.Contact.Phone.Home);
                            fetch.Attributes.AddRequired(WellKnownAttributes.Contact.Phone.Mobile);
                            fetch.Attributes.AddRequired(WellKnownAttributes.Contact.Phone.Fax);

                            request.AddExtension(fetch);
                        }
                        catch { }
                    }
                }

               request.RedirectToProvider();
            }
            catch (Exception exp){
                Logger.ExceptionLog(exp);
                base.ShowMessage(SignIn_errorMessageLabel, exp.Message);
                Handling_Tags("signup");
            }
        }

        protected bool GetOpenIDInformation()
        {
            try
            {
                OpenIdRelyingParty openIDReplyParty = new OpenIdRelyingParty();
                var responseFromOpenID = openIDReplyParty.GetResponse();
                
                if (responseFromOpenID == null) return false;

                if (responseFromOpenID != null)
                {

                    IDbTransaction transaction =
                      new TransactionManager().Transaction;

                    switch (responseFromOpenID.Status)
                    {
                        case AuthenticationStatus.Authenticated:
                            var fetch = responseFromOpenID.GetExtension<FetchResponse>();

                            CandidateInformation candidateInfo = null;

                            string fullName = null;

                            if (fetch != null)
                            {
                                candidateInfo = new CandidateInformation();
                                candidateInfo.caiFirstName = fetch.GetAttributeValue(WellKnownAttributes.Name.First);
                                candidateInfo.caiLastName = fetch.GetAttributeValue(WellKnownAttributes.Name.Last);
                                candidateInfo.UserName = fetch.GetAttributeValue(WellKnownAttributes.Contact.Email);
                                candidateInfo.caiEmail = fetch.GetAttributeValue(WellKnownAttributes.Contact.Email); 
                                

                                if (candidateInfo.caiFirstName == null)
                                    fullName = fetch.GetAttributeValue(WellKnownAttributes.Name.FullName);

                                if (fetch.GetAttributeValue(WellKnownAttributes.Person.Gender) == "Male"
                                    || fetch.GetAttributeValue(WellKnownAttributes.Person.Gender) == "M")
                                    candidateInfo.caiGender = 1;
                                else if (fetch.GetAttributeValue(WellKnownAttributes.Person.Gender) == "Female"
                                    || fetch.GetAttributeValue(WellKnownAttributes.Person.Gender) == "F")
                                    candidateInfo.caiGender = 2;

                                candidateInfo.caiHomePhone = fetch.GetAttributeValue(WellKnownAttributes.Contact.Phone.Home);
                                candidateInfo.caiEmergencyContactNo = fetch.GetAttributeValue(WellKnownAttributes.Contact.Phone.Mobile);
                                candidateInfo.caiFax = fetch.GetAttributeValue(WellKnownAttributes.Contact.Phone.Fax);

                                if (fullName != null)
                                {
                                    string[] candidateNameSplit = fullName.Split(' ');
                                    candidateInfo.caiFirstName = candidateNameSplit[0].ToString();
                                    candidateInfo.caiLastName = candidateNameSplit[1].ToString();
                                }

                                candidateInfo.caiIsActive = 'Y';
                                candidateInfo.caiOpenEmailID = "Y";
                                candidateInfo.caiSelfSignIn = "Y";

                                candidateInfo.caiType = "CT_MANUAL";
                                candidateInfo.caiEmployee = 'N';
                                candidateInfo.caiLimitedAccess = false;
                                candidateInfo.caiCreatedBy = 1;
                            }

                            if (candidateInfo == null) return false;

                            if (!new CandidateBLManager().VerifyOpenEmailID(candidateInfo.caiEmail.Trim()))
                            {
                                int candidateID = new ResumeRepositoryBLManager().
                                    InsertSignupCandidate(candidateInfo, 1, 1, transaction); 

                                if (candidateID > 0)
                                {
                                    candidateInfo.caiID = candidateID;
                                    SaveUserName(candidateInfo, transaction);
                                }
                                transaction.Commit();

                                SetDirectSignupUserInfo(candidateInfo.UserName, candidateInfo.Password, "OPENIDUSER"); 

                                NewSignup_OpenIDCandidate_InformationBind(candidateInfo);                                 
                            }
                            else
                                base.ShowMessage(SignIn_errorMessageLabel,
                               "User Already exists");
                            break;
                        case AuthenticationStatus.Canceled:
                            base.ShowMessage(SignIn_errorMessageLabel,
                                 "Authentication is cancelled");
                            break;
                        case AuthenticationStatus.Failed:
                            base.ShowMessage(SignIn_errorMessageLabel,
                                 "Authentication is failed");
                            break;
                    } 
                }
                return true ;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel, ex.Message);
                return true;
            }
        }
        private void NewSignup_OpenIDCandidate_InformationBind(CandidateInformation candidateinfo)
        {
            SignupCandidate_FirstNameText.Text = CheckObjectValue(candidateinfo.caiFirstName);
            SignupCandidate_LastNameText.Text = CheckObjectValue(candidateinfo.caiLastName);
            SignupCandidate_EmailIDText.Text = CheckObjectValue(candidateinfo.caiEmail);
            SignupCandidate_PasswordText.Text = (CheckObjectValue(candidateinfo.Password));
            SignupCandidate_ReTypePasswordText.Text = SignupCandidate_PasswordText.Text;
            Session["SIGNUP_FIRSTNAME"] = SignupCandidate_FirstNameText.Text;
            Session["SIGNUP_LASTNAME"] = SignupCandidate_LastNameText.Text;
        }         

        protected void NewCandidate_WelcomeContinueButton_Click(object sender, EventArgs e)
        {
            Handling_Tags("skills");              
        }
        protected void NewCandidate_WelcomeLaterLinkButton_Click(object sender, EventArgs e)
        {
            GotoLandingPage(false);
        }

        protected void SignupCandidate_GridViewCandidateSkills_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    String skillName = e.Row.Cells[1].Text.ToString();
                    Button imageButton = (Button)e.Row.FindControl("SignupCandidate_searchTestButton");
                    imageButton.CommandName = "Select";
                    imageButton.CommandArgument = skillName;
                    imageButton.Command += new CommandEventHandler(SignupCandidate_GridViewCandidateSkills_Command);

                    //Getting skill test count
                    Label SingupCandidate_testCountLabel = (Label)e.Row.FindControl("SingupCandidate_testCountLabel");
                    SingupCandidate_testCountLabel.Text = GetSkill_TestCount(skillName.Trim());

                    //Assign delete key argument
                    ImageButton Delete_skillImageButton = (ImageButton)e.Row.FindControl("SignupCandidate_deleteSkillImageButton");
                    Delete_skillImageButton.CommandName = "Delete";
                    Delete_skillImageButton.CommandArgument = ((Forte.HCM.DataObjects.SignupCandidate)(e.Row.DataItem)).SkillID.ToString(); //e.Row.Cells[0].Text.ToString(); 
                    Delete_skillImageButton.Command += new CommandEventHandler(SignupCandidate_GridViewCandidateSkills_Command);
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel, ex.Message);
                Handling_Tags("skills");
            }

        }

        private string GetSkill_TestCount(string skillName)
        {
            List<TestDetail> recommendedTestDetails = null;

            int pagesize = 15;
            int totalPage = 0;
            int pageNumber = 1; 

            // Retrieve recomended test details.
            recommendedTestDetails = new TestBLManager().GetRecommendedTestDetails
                (skillName.Trim(), "TESTNAME",SortType.Ascending.ToString(), pagesize, pageNumber, out totalPage);

            int testCount = recommendedTestDetails.Count;             
            if (testCount > 1)
                return "We found "+ testCount + " test matches";
            else if (testCount == 1)
                return "We found "+ testCount + " test match";
            else
                return "";
        }

        protected void SignupCandidate_GridViewCandidateSkills_Command(object sender, CommandEventArgs e)
        {
            string commandArgument = null;
            Session["SEARCH_TEST"] = string.Empty;

            if (e.CommandName == "Select")
            {
                commandArgument= e.CommandArgument.ToString();
                if (!Utility.IsNullOrEmpty(commandArgument))
                {
                    Session["SEARCH_TEST"] = commandArgument;
                    Response.Redirect("~/CandidateAdmin/CandidateSearchTest.aspx", false);
                }
            }
            else if (e.CommandName == "Delete")
            {
                commandArgument = e.CommandArgument.ToString();
                if (commandArgument != null && commandArgument.Trim() != "")
                {
                    IDbTransaction transaction =new TransactionManager().Transaction;

                    try
                    {      
                        UserDetail userDetail =
                                          Session["USER_DETAIL"] as UserDetail;

                        int SkillID = new CandidateBLManager().DeleteCandidateSkill(
                            userDetail.UserID,
                            Convert.ToInt32(commandArgument), transaction);

                        transaction.Commit();

                        GetCandidateSkill(userDetail.UserID);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Logger.ExceptionLog(ex);
                        base.ShowMessage(SignIn_errorMessageLabel,
                     ex.Message);
                    }

                    if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "updateskills")
                        Set_Tagvisibility(NewCandidate_SkillsTag, NewCandidate_SkillsDiv);
                    else
                        Set_Activetag(NewCandidate_SkillsTag, NewCandidate_SkillsDiv, "skills"); 
                }
            }
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            SignIn_errorMessageLabel.Text = string.Empty;
            SignIn_errorMessageLabel.Visible = false;
        }

        protected void Signup_newSkillNolImageButton_Click(object sender, ImageClickEventArgs e)
        {
            CandidateSkillControls(false);
            SignupCandidate_SkillText.Text = "";
        }

        protected void Signup_newSkillYesImageButton_Click(object sender, ImageClickEventArgs e)
        {
            IDbTransaction transaction = new TransactionManager().Transaction;

            try
            {
                // Keep skill in local variable.
                string newSkill = SignupCandidate_SkillText.Text.Trim();

                // Save the skill.
                SaveSkill("N", transaction);

                try
                {
                    // Send mail to admin stating that new skill has been added
                    new EmailHandler().SendMail(ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                        ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                        "ForteHCM - New Skill Added", GetSkillMailBody(newSkill));
                }
                catch (Exception exp)
                {
                    // Log the exception only to the log file (if anything is raised during sending mail).
                    Logger.ExceptionLog(exp);
                }

                // Clear skill text box.
                SignupCandidate_SkillText.Text = string.Empty;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                Logger.ExceptionLog(ex);
                base.ShowMessage(SignIn_errorMessageLabel,
                    ex.Message);
            }

            if (Convert.ToString(ViewState["SIGNIN_ACTIVETAG"]).ToLower() == "updateskills")
                Set_Tagvisibility(NewCandidate_SkillsTag, NewCandidate_SkillsDiv);
            else
                Set_Activetag(NewCandidate_SkillsTag, NewCandidate_SkillsDiv, "skills"); 
        }

        /// <summary>
        /// Method that sends the activation email to the created user.
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="string"/> that holds the user email.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="string"/> that holds the encrypted confirmation code 
        /// to activate his/her account.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        private void SendConfirmationCodeEmail(string userEmail, string password,
            string confirmationCode, string firstName, string lastName)
        {
            int userID = new CandidateBLManager().GetUserID(userEmail);

            string encryptedUserID = new EncryptAndDecrypt().EncryptString(userID.ToString());
             
            new EmailHandler().SendMail(userEmail,
                ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"],
                "ForteHCM - Activate link for your account",
                GetMailBody(userEmail, confirmationCode, password, firstName, lastName, encryptedUserID));
        }

        /// <summary>
        /// Method that compose the body content of the email.
        /// </summary>
        /// <param name="userEmail">
        /// A <see cref="string"/> that holds the user email.
        /// </param>
        /// <param name="confirmationCode">
        /// A <see cref="string"/> that holds the encrypted confirmation code 
        /// to activate his/her account.
        /// </param>
        /// <param name="password">
        /// A <see cref="string"/> that holds the password.
        /// </param>
        /// <param name="firstName">
        /// A <see cref="string"/> that holds the first name.
        /// </param>
        /// <param name="lastName">
        /// A <see cref="string"/> that holds the last name.
        /// </param>
        /// <returns>
        /// A <see cref="string"/> that contains the activation mail body.
        /// </returns>
        private string GetMailBody(string userEmail, string confirmationCode,
            string password, string firstName, string lastName, string encryptedUserID)
        {
            StringBuilder sbBody = new StringBuilder();
             
            try
            {
                sbBody.Append("Dear ");
                sbBody.Append(firstName);
                sbBody.Append(" ");
                sbBody.Append(lastName);
                sbBody.Append(",");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Welcome to ForteHCM !");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                //sbBody.Append("This is to acknowledge that your request to create an account was processed successfully.");
                //sbBody.Append(Environment.NewLine);
                //sbBody.Append(Environment.NewLine);
                sbBody.Append("Your email ID is : " + userEmail);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Your password is : " + password);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("To activate your account, please copy and paste the ");
                sbBody.Append("following URL in the browser or click on the link");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string strUri =
                    string.Format(WebConfigurationManager.AppSettings["SIGNUP_ACTIVATIONURL"] + "?" +
                    WebConfigurationManager.AppSettings["SIGNUP_USERNAME"] + "={0}&" +
                    WebConfigurationManager.AppSettings["SIGNUP_CONFIRMATION"] + "={1}",
                    encryptedUserID, confirmationCode);
                sbBody.Append("<a href='" + strUri + "'");
                sbBody.Append(" >" + strUri + "</a>");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("We wish you a pleasant experience using ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("-The ForteHCM team");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }


        private string OrderSkills()
        { 
            string skills=null;
              
            for(int i=0;i<SignupCandidate_GridViewCandidateSkills.Rows.Count;i++)
            {
                skills += SignupCandidate_GridViewCandidateSkills.Rows[i].Cells[1].Text + ","; 
            }
            if (skills != null)
            {
                skills = skills.Remove(skills.Length-1, 1);
            }
            return skills;
        }

        /// <summary>
        /// Method to get the mail content
        /// </summary>
        /// <returns></returns>
        private string GetSkillMailBody(string skill)
        {
            StringBuilder sbBody = new StringBuilder();

            try
            {
                sbBody.Append("Dear Admin,");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                string manageSkillsURL = ConfigurationManager.AppSettings["MANAGE_SKILLS_URL"];
                sbBody.Append(string.Format("New skill '{0}' has been added by candidate. Click <a href='" + manageSkillsURL + "' alt=''> here </a> to approve/reject the skill", skill));
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("-The ForteHCM team");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

    }
}