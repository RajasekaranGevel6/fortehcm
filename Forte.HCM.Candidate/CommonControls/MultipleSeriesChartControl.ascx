﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultipleSeriesChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.MultipleSeriesChartControl" %>
<div id="MultipleSeriesChartControl_mainDiv">
    <div runat="server" id="MultipleSeriesChartControl_chartControl">
        <asp:Chart ID="MultipleSeriesChartControl_chart" runat="server" Height="250px" Width="454px"
            ToolTip="Click here to zoom graph" Palette="Pastel" BackColor="Transparent" AntiAliasing="Graphics"
            ForeColor="180, 65, 140, 240" Font="Arial, 8.25pt, style=Bold" EnableViewState="true">
            <Legends>
                <asp:Legend Name="MultipleSeriesChartControl_legend" Font="Arial, 8.25pt" ForeColor="114, 142, 192"
                    BackColor="Transparent" IsTextAutoFit="False">
                </asp:Legend>
            </Legends>
            <Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="MultipleSeriesChartControl_chartArea" BackSecondaryColor="Transparent"
                    BackColor="Transparent" ShadowColor="Transparent">
                    <AxisY>
                        <MajorGrid Enabled="False" LineDashStyle="DashDotDot" />
                        <LabelStyle ForeColor="180, 65, 140, 240" Angle="90" />
                    </AxisY>
                    <AxisX IsLabelAutoFit="False">
                        <MajorGrid Enabled="False" LineDashStyle="DashDotDot" />
                        <LabelStyle ForeColor="180, 65, 140, 240" Angle="0" TruncatedLabels="True" />
                    </AxisX>
                    <InnerPlotPosition X="13" Y="10" Height="68" Width="75" />
                    <Position Height="95" Width="90" Y="15" />
                </asp:ChartArea>
            </ChartAreas>
            <Titles>
                <asp:Title Name="MultipleSeriesChartControl_title">
                </asp:Title>
            </Titles>
        </asp:Chart>
    </div>
</div>
