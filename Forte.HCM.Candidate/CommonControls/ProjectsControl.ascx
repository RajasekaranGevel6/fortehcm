﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ProjectsControl" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<asp:UpdatePanel ID="ProjectsControl_updatePanel" runat="server">
    <ContentTemplate>
        <div id="MyResume_projectsMainDiv">
            <div style="clear: both; overflow: auto; height: 290px;" runat="server" id="ProjectsControl_controlsDiv"
                class="resume_Table_Bg">
                <asp:HiddenField ID="ProjectsControl_hiddenField" runat="server" />
                <asp:HiddenField ID="ProjectControl_deletedRowHiddenField" runat="server" />
               <div style="float:right;">
                <asp:Button ID="ProjectsControl_addDefaultButton" runat="server" 
                    SkinID="sknButtonId" ToolTip="Add Project" Text="Add" 
                    onclick="ProjectsControl_addDefaultButton_Click" />
                </div>
                <asp:ListView ID="ProjectsControl_listView" runat="server" OnItemDataBound="ProjectsControl_listView_ItemDataBound"
                    OnItemCommand="ProjectsControl_listView_ItemCommand">
                    <LayoutTemplate>
                        <div id="itemPlaceholderContainer" runat="server">
                            <div runat="server" id="itemPlaceholder">
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="resume_panel_list_view">
                            <asp:Panel GroupingText="Project" runat="server" ID="ProjectsControl_projectPanel"
                                CssClass="can_resume_panel_text">
                                <div style="clear: both; float: left;padding-top:8px;">
                                    <div>
                                        <div style="float: left;width:470px;">
                                            <div>
                                                <div>
                                                    <asp:Label runat="server" ID="ProjectsControl_nameLabel" Text="Name" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                                    <asp:TextBox ID="ProjectControl_deleteRowIndex" runat="server" Text='<%# Eval("ProjectId") %>'
                                                        Style="display: none" />
                                                </div>
                                                <div>
                                                    <asp:TextBox runat="server" ID="ProjectsControl_projectNameTextBox" Text='<%# Eval("ProjectName") %>'
                                                        Width="440px" ToolTip="Project Name" MaxLength="500" SkinID="sknCanResumeTextBox"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="float: right;width:420px;">
                                            <div>
                                                <div>
                                                    <asp:Label runat="server" ID="ProjectsControl_periodLabel" Text="Duration" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                                </div>
                                                <div>
                                                    <div>
                                                        <div style="float:left;padding-top:5px;">
                                                            <asp:Label runat="server" ID="ProjectsControl_fromLabel" Text="From" 
                                                                SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                                        </div>
                                                        <div style="float: left; padding-left: 20px;">
                                                            <ajaxToolKit:CalendarExtender ID="ProjectsControl_startDtCalendarExtender" runat="server"
                                                                TargetControlID="ProjectsControl_startDtTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy "
                                                                PopupPosition="BottomLeft" PopupButtonID="ProjectsControl_calendarStartDtImageButton" />
                                                            <ajaxToolKit:MaskedEditExtender ID="ProjectsControl_startDtMaskedEditExtender" runat="server"
                                                                TargetControlID="ProjectsControl_startDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                MaskType="Date" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                            <ajaxToolKit:MaskedEditValidator ID="ProjectsControl_startDtMaskedEditValidator"
                                                                runat="server" ControlExtender="ProjectsControl_startDtMaskedEditExtender" ControlToValidate="ProjectsControl_startDtTextBox"
                                                                EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" />
                                                            <asp:TextBox runat="server" ID="ProjectsControl_startDtTextBox" Text='<%# Eval("StartDate") %>'
                                                                ToolTip="Start Date" Width="72px" SkinID="sknCanResumeTextBox"></asp:TextBox>
                                                            <asp:ImageButton ID="ProjectsControl_calendarStartDtImageButton" SkinID="sknCalendarImageButton"
                                                                runat="server" ImageAlign="AbsMiddle" />
                                                        </div>
                                                        <div style="width: 30px; padding-top:5px;padding-left:5px;font-size: 12px;color: #000; float: left;">
                                                            <asp:Label runat="server" ID="ProjectsControl_toLabel" Text="To" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                                        </div>
                                                        <div style="float: left;">
                                                            <asp:TextBox runat="server" ID="ProjectsControl_endDtTextBox" Text='<%# Eval("EndDate") %>'
                                                                ToolTip="End Date" Width="72px" SkinID="sknCanResumeTextBox"></asp:TextBox>
                                                            <asp:ImageButton ID="ProjectsControl_calendarEndDtImageButton" SkinID="sknCalendarImageButton"
                                                                runat="server" ImageAlign="AbsMiddle" />
                                                            <ajaxToolKit:CalendarExtender ID="ProjectsControl_endDtCalendarExtender" runat="server"
                                                                TargetControlID="ProjectsControl_endDtTextBox" CssClass="MyCalendar" Format="MM/dd/yyyy "
                                                                PopupPosition="BottomLeft" PopupButtonID="ProjectsControl_calendarendDtImageButton" />
                                                            <ajaxToolKit:MaskedEditExtender ID="ProjectsControl_endDtMaskedEditExtender" runat="server"
                                                                TargetControlID="ProjectsControl_endDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                                                InputDirection="RightToLeft" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                MaskType="Date" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                                            <ajaxToolKit:MaskedEditValidator ID="ProjectsControl_endDtMaskedEditValidator" runat="server"
                                                                ControlExtender="ProjectsControl_endDtMaskedEditExtender" ControlToValidate="ProjectsControl_endDtTextBox"
                                                                EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                                                TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" />
                                                        </div>
                                                        <div id="MyResume_projectsDeleteImage" style="float:left;padding-left:25px;">
                                                            <asp:Button ID="ProjectsControl_addButton" runat="server" CommandName="addProject" SkinID="sknButtonId" ToolTip="Add Project" Text="Add" />
                                                            <asp:Button ID="ProjectsControl_deleteButton" runat="server" CommandName="deleteProject" SkinID="sknButtonId" ToolTip="Delete Project" Text="Remove"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both; float: left;width:890px;">
                                    <div>
                                        <!-- Left Panel -->
                                        <div style="float: left;width:470px;">
                                            <div>
                                                <div>
                                                    <asp:Label runat="server" ID="ProjectsControl_descLabel" Text="Description" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox runat="server" ID="ProjectsControl_projectDescTextBox" Text='<%# Eval("ProjectDescription") %>'
                                                        TextMode="MultiLine" Width="440px" Height="125px" ToolTip="Project Description"
                                                        MaxLength="5000" onkeyup="CommentsCount(5000,this)" SkinID="sknCanResumeTextBox"
                                                        onchange="CommentsCount(5000,this)"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Right Panel -->
                                        <div style="float: right;width:420px;">
                                            <div>
                                                <div>
                                                    <asp:Label runat="server" ID="ProjectsControl_environmentLabel" Text="Environment"
                                                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox runat="server" ID="ProjectsControl_environmentTextBox" Text='<%# Eval("Environment") %>'
                                                        Width="412px" Height="20px" ToolTip="Environment" SkinID="sknCanResumeTextBox"
                                                        TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                                </div>
                                                <div>
                                                    <asp:Label runat="server" ID="ProjectsControl_roleLabel" Text="Role" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox runat="server" ID="ProjectsControl_roleTextBox" Text='<%# Eval("Role") %>'
                                                        ToolTip="Role" MaxLength="500" Width="412px" Height="20px" TextMode="MultiLine"
                                                        SkinID="sknCanResumeTextBox"></asp:TextBox>
                                                </div>
                                                <div>
                                                    <asp:Label runat="server" ID="ProjectsControl_positionLabel" Text="Position" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox runat="server" ID="ProjectsControl_positionTextBox" Text='<%# Eval("PositionTitle") %>'
                                                        ToolTip="Position" Width="412px" Height="20px" SkinID="sknCanResumeTextBox" TextMode="MultiLine"
                                                        MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both;">
                                    <div style="float: left">
                                        <div style="width: 295px; float: left">
                                            <div>
                                                <asp:Label runat="server" ID="ProjectsControl_clientNameLabel" Text="Client Name"
                                                    SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox runat="server" ID="ProjectsControl_clientNameTextBox" Text='<%# Eval("ClientName") %>'
                                                    ToolTip="Client Name" MaxLength="500" SkinID="sknCanResumeTextBox" Width="280px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="width: 295px; float: left">
                                            <div>
                                                <asp:Label runat="server" ID="ProjectsControl_locationLabel" Text="Location" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox runat="server" ID="ProjectsControl_locationTextBox" Text='<%# SetLocation(Container.DataItem) %>'
                                                    ToolTip="Location" SkinID="sknCanResumeTextBox" Width="280px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="width: 295px; float: left">
                                            <div>
                                                <asp:Label runat="server" ID="ProjectsControl_clientIndustryLabel" Text="Industry"
                                                    SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox runat="server" ID="ProjectsControl_clientIndustryTextBox" Text='<%# Eval("ClientIndustry") %>'
                                                    ToolTip="Client Industry" MaxLength="500" SkinID="sknCanResumeTextBox" Width="292px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <asp:UpdatePanel ID="ProjectControl_confirmPopUpUpdatPanel" runat="server">
                    <ContentTemplate>
                        <div id="ProjectControl_hiddenDIV" runat="server" style="display: none">
                            <asp:Button ID="ProjectControl_hiddenPopupModalButton" runat="server" />
                        </div>
                        <ajaxToolKit:ModalPopupExtender ID="ProjectControl_deleteModalPopupExtender" runat="server"
                            PopupControlID="ProjectControl_deleteConfirmPopUpPanel" TargetControlID="ProjectControl_hiddenPopupModalButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:Panel ID="ProjectControl_deleteConfirmPopUpPanel" runat="server" Style="display: none;
                            width: 30%; height: 210px; background-position: bottom; background-color: #283033;">
                            <uc1:ConfirmMsgControl ID="ProjectControl_confirmPopupExtenderControl" runat="server"
                                Type="YesNo" Title="Delete Project" Message="Are you sure want to delete project details?"
                                OnOkClick="ProjectControl_okPopUpClick" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
