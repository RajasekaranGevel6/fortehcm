﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivityPageNavigator.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ActivityPageNavigator" %>
<table width="100%" cellpadding="0" cellspacing="0" id="navigate_text">
    <tr>
        <td valign="middle" height="30" align="left">
            <asp:Panel ID="ActivityPageNavigator_controlsPanel" runat="server" HorizontalAlign="Left">
                <asp:LinkButton ID="ActivityPageNavigator_moveFirstLinkButton" runat="server"
                    OnClick="ActivityPageNavigator_Move_Click" CommandName="First" ToolTip="Click here to show first set of pages"><asp:Image SkinID="sknPagingFirst" AlternateText="First" runat="server" ID="ActivityPageNavigator_moveFirstImage"/></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="ActivityPageNavigator_movePreviousLinkButton" runat="server"
                    OnClick="ActivityPageNavigator_Move_Click" CommandName="Previous" ToolTip="Click here to show previous set of pages"><asp:Image SkinID="sknPagingPrev" AlternateText="Prev" runat="server" ID="ActivityPageNavigator_movePrevImage"/></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="ActivityPageNavigator_oneLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="Solid">1</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_oneSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_twoLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">2</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_twoSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_threeLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">3</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_threeSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_fourLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">4</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_fourSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_fiveLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">5</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_fiveSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_sixLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">6</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_sixSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_sevenLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">7</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_sevenSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_eightLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">8</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_eightSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_nineLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">9</asp:LinkButton>
                <asp:Label ID="ActivityPageNavigator_nineSeparatorLabel" runat="server" Text="|"></asp:Label>
                <asp:LinkButton ID="ActivityPageNavigator_tenLinkButton" runat="server"
                    BorderWidth="3px" BorderColor="#FF5700" OnClick="ActivityPageNavigator_PageNumber_Click"
                    BorderStyle="None">10</asp:LinkButton>
                &nbsp;<asp:LinkButton ID="ActivityPageNavigator_moveNextLinkButton" runat="server"
                    OnClick="ActivityPageNavigator_Move_Click" CommandName="Next" ToolTip="Click here to show next set of pages"><asp:Image SkinID="sknPagingNext" AlternateText="Next" runat="server" ID="ActivityPageNavigator_moveNextImage"/></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="ActivityPageNavigator_moveLastLinkButton" runat="server" OnClick="ActivityPageNavigator_Move_Click"
                    Text="" CommandName="Last" ToolTip="Click here to show last set of pages"><asp:Image SkinID="sknPagingLast" AlternateText="Last" runat="server" ID="ActivityPageNavigator_moveLastImage"/></asp:LinkButton>&nbsp;
            </asp:Panel>
            <input id="ActivityPageNavigator_currentPageSetHidden" type="hidden" name="ActivityPageNavigator_currentPageSetHidden"
                value="1" runat="server" />
            <input id="ActivityPageNavigator_pageSizeHidden" type="hidden" name="ActivityPageNavigator_pageSizeHidden"
                value="1" runat="server" />
            <input id="ActivityPageNavigator_totalRecordsHidden" type="hidden" name="ActivityPageNavigator_totalRecordsHidden"
                value="0" runat="server" />
        </td>
    </tr>
</table>
