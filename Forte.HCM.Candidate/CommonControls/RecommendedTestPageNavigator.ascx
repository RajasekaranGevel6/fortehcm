﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecommendedTestPageNavigator.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.RecommendedTestPageNavigator" %>
<div id="navigate_text" align="center">
    <asp:Panel ID="RecommendedTestPageNavigator_controlsPanel" runat="server" HorizontalAlign="Right">
        <asp:LinkButton ID="RecommendedTestPageNavigator_moveFirstLinkButton" runat="server"
            OnClick="RecommendedTestPageNavigator_Move_Click" CommandName="First" ToolTip="Click here to show first set of pages"><img src="../App_Themes/DefaultTheme/Images/nav_btn_first.gif" style="border-style:none;" alt=""/></asp:LinkButton>
        &nbsp;<asp:LinkButton ID="RecommendedTestPageNavigator_movePreviousLinkButton" runat="server"
            OnClick="RecommendedTestPageNavigator_Move_Click" CommandName="Previous" ToolTip="Click here to show previous set of pages"><img src="../App_Themes/DefaultTheme/Images/nav_btn_prev.gif" style="border-style:none;" alt=""/></asp:LinkButton>
        &nbsp;<asp:LinkButton ID="RecommendedTestPageNavigator_oneLinkButton" runat="server"
            OnClick="RecommendedTestPageNavigator_PageNumber_Click" SkinID="sknNavSelectedLinkButton"
            Width="24" Height="24">1</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_oneSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_twoLinkButton" runat="server" OnClick="RecommendedTestPageNavigator_PageNumber_Click"
            SkinID="sknNavLinkButton" Width="24" Height="24">2</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_twoSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_threeLinkButton" runat="server"
            OnClick="RecommendedTestPageNavigator_PageNumber_Click" SkinID="sknNavLinkButton"
            Width="24" Height="24">3</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_threeSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_fourLinkButton" runat="server" OnClick="RecommendedTestPageNavigator_PageNumber_Click"
            SkinID="sknNavLinkButton" Width="24" Height="24">4</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_fourSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_fiveLinkButton" runat="server" OnClick="RecommendedTestPageNavigator_PageNumber_Click"
            SkinID="sknNavLinkButton" Width="24" Height="24">5</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_fiveSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_sixLinkButton" runat="server" OnClick="RecommendedTestPageNavigator_PageNumber_Click"
            SkinID="sknNavLinkButton" Width="24" Height="24">6</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_sixSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_sevenLinkButton" runat="server"
            OnClick="RecommendedTestPageNavigator_PageNumber_Click" SkinID="sknNavLinkButton"
            Width="24" Height="24">7</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_sevenSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_eightLinkButton" runat="server"
            OnClick="RecommendedTestPageNavigator_PageNumber_Click" SkinID="sknNavLinkButton"
            Width="24" Height="24">8</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_eightSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_nineLinkButton" runat="server" OnClick="RecommendedTestPageNavigator_PageNumber_Click"
            SkinID="sknNavLinkButton" Width="24" Height="24">9</asp:LinkButton>
        <asp:Label ID="RecommendedTestPageNavigator_nineSeparatorLabel" runat="server" Text=""></asp:Label>
        <asp:LinkButton ID="RecommendedTestPageNavigator_tenLinkButton" runat="server" OnClick="RecommendedTestPageNavigator_PageNumber_Click"
            SkinID="sknNavLinkButton" Width="24" Height="24">10</asp:LinkButton>
        &nbsp;<asp:LinkButton ID="RecommendedTestPageNavigator_moveNextLinkButton" runat="server"
            OnClick="RecommendedTestPageNavigator_Move_Click" CommandName="Next" ToolTip="Click here to show next set of pages"><img src="../App_Themes/DefaultTheme/Images/nav_btn_next.gif" style="border-style:none;" alt=""/></asp:LinkButton>
        &nbsp;<asp:LinkButton ID="RecommendedTestPageNavigator_moveLastLinkButton" runat="server"
            OnClick="RecommendedTestPageNavigator_Move_Click" Text="" CommandName="Last"
            ToolTip="Click here to show last set of pages"><img src="../App_Themes/DefaultTheme/Images/nav_btn_last.gif" style="border-style:none;" alt=""/> </asp:LinkButton>
    </asp:Panel>
    <input id="RecommendedTestPageNavigator_currentPageSetHidden" type="hidden" name="RecommendedTestPageNavigator_currentPageSetHidden"
        value="1" runat="server" />
    <input id="RecommendedTestPageNavigator_pageSizeHidden" type="hidden" name="RecommendedTestPageNavigator_pageSizeHidden"
        value="1" runat="server" />
    <input id="RecommendedTestPageNavigator_totalRecordsHidden" type="hidden" name="RecommendedTestPageNavigator_totalRecordsHidden"
        value="0" runat="server" />
</div>
