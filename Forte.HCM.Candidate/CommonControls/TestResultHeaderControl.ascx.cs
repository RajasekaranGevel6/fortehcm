﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forte.HCM.UI.CommonControls
{
    public partial class TestResultHeaderControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Represents the method to load the test and candidate details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <param name="testName">
        /// A<see cref="string"/>that holds the test name
        /// </param>
        /// <param name="candidateName">
        /// A<see cref="string"/>that holds the candidate name
        /// </param>
        /// <param name="testCompletedDate">
        /// A<see cref="string"/>that holds the testCompletedDate
        /// </param>
        public void LoadCandidateTestDetails(string testId, string testName,
            string candidateName, DateTime testCompletedDate)
        {
            TestResultHeaderControl_testNameValueTextBox.Text = string.Concat(testName," (",testId,")");
            TestResultHeaderControl_candidateNameValueTextBox.Text = candidateName;
            
            TestResultHeaderControl_testCompletedValueTextBox.Text = 
                "Completed on " + testCompletedDate.ToString("MMMM dd, yyyy");
        }
    }
}