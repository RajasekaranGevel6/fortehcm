﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TechnicalSkillsControl.cs
// File that represents the user interface for the technicalskills information details

#endregion Header

#region Directives                                                             

using System;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives 

namespace Forte.HCM.UI.CommonControls
{
    public partial class TechnicalSkillsControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

           
        }
        public TechnicalSkills DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                TechnicalSkillsControl_databaseTextBox.Text = value.Database;
                TechnicalSkillsControl_languageTextBox.Text = value.Language;
                TechnicalSkillsControl_uiToolsTextBox.Text = value.UITools;
                TechnicalSkillsControl_osTextBox.Text = value.OperatingSystem;
            }
        }
    }
}