﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuControl.ascx.cs"
    Inherits="Forte.HCM.Candidate.CommonControls.MenuControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="menustrip_normal">
            <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                    <td class="menustrip_bullet" >
                    </td>
                    <td id="tdCandidateHome" class="menutext_normal" onmouseover="document.body.style.cursor='hand'" onmouseout="document.body.style.cursor = 'default'">
                        <asp:LinkButton ID="MenuControl_candidateHomeLinkButton" runat="server" CssClass="menutext_menu_item_normal"
                            Text="Candidate Home" OnClick="MenuControl_candidateHomeLinkButton_Click"></asp:LinkButton>
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td id="tdTestRecommendations" class="menutext_normal" onmouseover="document.body.style.cursor='hand'" onmouseout="document.body.style.cursor = 'default'">
                        <asp:LinkButton ID="MenuControl_testRecommendationsLinkButton" runat="server" 
                            Text="Test Recommendations" CssClass="menutext_menu_item_normal"
                            OnClick="MenuControl_testRecommendationsLinkButton_Click"></asp:LinkButton>
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td id="tdMyTests" class="menutext_normal" onmouseover="document.body.style.cursor='hand'" onmouseout="document.body.style.cursor = 'default'">
                        <asp:LinkButton ID="MenuControl_myTestsLinkButton" runat="server" CssClass="menutext_menu_item_normal"
                            Text="My Tests" OnClick="MenuControl_myTestsLinkButton_Click"></asp:LinkButton>
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td id="tdMyInterviews" class="menutext_normal" onmouseover="document.body.style.cursor='hand'" onmouseout="document.body.style.cursor = 'default'">
                        <asp:LinkButton ID="MenuControl_myInterviewsLinkButton" runat="server" CssClass="menutext_menu_item_normal"
                            Text="My Interviews" OnClick="MenuControl_myInterviewsLinkButton_Click"></asp:LinkButton>
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td id="tdMyCredits" class="menutext_normal" onmouseover="document.body.style.cursor='hand'" onmouseout="document.body.style.cursor = 'default'">
                        <asp:LinkButton ID="MenuControl_myCreditsLinkButton" runat="server" CssClass="menutext_menu_item_normal"
                            Text="My Credits" OnClick="MenuControl_myCreditsLinkButton_Click"></asp:LinkButton>
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td id="tdSearchTest" class="menutext_normal" onmouseover="document.body.style.cursor='hand'" onmouseout="document.body.style.cursor = 'default'">
                        <asp:LinkButton ID="MenuControl_searchTestLinkButton" runat="server" CssClass="menutext_menu_item_normal"
                            Text="Search Test" OnClick="MenuControl_searchTestLinkButton_Click"></asp:LinkButton>
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td id="tdEditResume" class="menutext_normal" onmouseover="document.body.style.cursor='hand'" onmouseout="document.body.style.cursor = 'default'">
                        <asp:LinkButton ID="MenuControl_editResumeLinkButton" runat="server" CssClass="menutext_menu_item_normal"
                            Text="Edit Resume" OnClick="MenuControl_editResumeLinkButton_Click"></asp:LinkButton>
                    </td>
                    <td class="menustrip_bullet">
                    </td>
                    <td style="width: 5%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript" language="javascript">

    function ShowDefault()
    {
        var val = '<%= Request.QueryString["m"] %>';

        if (val != null && val != "")
        {
            if (val == "0")
            {
                document.getElementById("tdCandidateHome").className = "menustrip_active";
                document.getElementById("<%= MenuControl_candidateHomeLinkButton.ClientID %>").className = "menustrip_menu_item_active";
            }
            else if (val == "1")
            {
                document.getElementById("tdTestRecommendations").className = "menustrip_active";
                document.getElementById("<%= MenuControl_testRecommendationsLinkButton.ClientID %>").className = "menustrip_menu_item_active";
            }
            else if (val == "2")
            {
                document.getElementById("tdMyTests").className = "menustrip_active";
                document.getElementById("<%= MenuControl_myTestsLinkButton.ClientID %>").className = "menustrip_menu_item_active";
            }
            else if (val == "3")
            {
                document.getElementById("tdMyInterviews").className = "menustrip_active";
                document.getElementById("<%= MenuControl_myInterviewsLinkButton.ClientID %>").className = "menustrip_menu_item_active";
            }
            else if (val == "4")
            {
                document.getElementById("tdMyCredits").className = "menustrip_active";
                document.getElementById("<%= MenuControl_myCreditsLinkButton.ClientID %>").className = "menustrip_menu_item_active";
            }
            else if (val == "5")
            {
                document.getElementById("tdSearchTest").className = "menustrip_active";
                document.getElementById("<%= MenuControl_searchTestLinkButton.ClientID %>").className = "menustrip_menu_item_active";
            }
            else if (val == "6")
            {
                document.getElementById("tdEditResume").className = "menustrip_active";
                document.getElementById("<%= MenuControl_editResumeLinkButton.ClientID %>").className = "menustrip_menu_item_active";
            }
        }
        else
        {
            document.getElementById("tdCandidateHome").className = "menutext_normal";
            document.getElementById("tdTestRecommendations").className = "menutext_normal";
            document.getElementById("tdMyTests").className = "menutext_normal";
            document.getElementById("tdMyInterviews").className = "menutext_normal";
            document.getElementById("tdMyCredits").className = "menutext_normal";
            document.getElementById("tdSearchTest").className = "menutext_normal";
            document.getElementById("tdEditResume").className = "menutext_normal";

            document.getElementById("<%= MenuControl_candidateHomeLinkButton.ClientID %>").className = "menutext_menu_item_normal";
            document.getElementById("<%= MenuControl_testRecommendationsLinkButton.ClientID %>").className = "menutext_menu_item_normal";
            document.getElementById("<%= MenuControl_myTestsLinkButton.ClientID %>").className = "menutext_menu_item_normal";
            document.getElementById("<%= MenuControl_myInterviewsLinkButton.ClientID %>").className = "menutext_menu_item_normal";
            document.getElementById("<%= MenuControl_myCreditsLinkButton.ClientID %>").className = "menutext_menu_item_normal";
            document.getElementById("<%= MenuControl_searchTestLinkButton.ClientID %>").className = "menutext_menu_item_normal";
            document.getElementById("<%= MenuControl_editResumeLinkButton.ClientID %>").className = "menutext_menu_item_normal";
        }
    }

</script>

<script type="text/javascript" language="javascript">
    ShowDefault();
</script>
