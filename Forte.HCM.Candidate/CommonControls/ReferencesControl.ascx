﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReferencesControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ReferencesControl" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<asp:UpdatePanel ID="ReferencesControl_updatePanel" runat="server">
    <ContentTemplate>
        <div id="MyResume_referenceInfoMainDiv">
            <div style="clear: both; overflow: auto; height: 290px" runat="server" id="ReferencesControl_controlsDiv"
                class="resume_Table_Bg">
                <asp:HiddenField ID="ReferencesControl_hiddenField" runat="server" />
                <asp:HiddenField ID="ReferencesControl_deletedRowHiddenField" runat="server" />
                <div style="float:right;">
                <asp:Button ID="ReferencesControl_addDefaultButton" runat="server" 
                SkinID="sknButtonId" ToolTip="Add Reference" Text="Add" 
                    onclick="ReferencesControl_addDefaultButton_Click" />
                </div>
                <asp:ListView ID="ReferencesControl_listView" runat="server" OnItemDataBound="ReferencesControl_listView_ItemDataBound"
                    OnItemCommand="ReferencesControl_listView_ItemCommand">
                    <LayoutTemplate>
                        <div id="itemPlaceholderContainer" runat="server">
                            <div runat="server" id="itemPlaceholder">
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="resume_panel_list_view">
                            <asp:Panel GroupingText="References" runat="server" ID="ReferencesControl_referencePanel" CssClass="can_resume_panel_text">
                                <div id="MyResume_referencesDeleteImage" style="clear: both; float: right;">
                                    <asp:Button ID="ReferencesControl_addButton" runat="server" CommandName="addReference" SkinID="sknButtonId" ToolTip="Add Reference" Text="Add" />
                                    <asp:Button ID="ReferencesControl_deleteButton" runat="server" CommandName="deleteReference" SkinID="sknButtonId" ToolTip="Delete Reference" Text="Remove"/>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="ReferencesControl_nameLabel" Text="Name" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        <asp:TextBox ID="ReferencesControl_deleteRowIndex" runat="server" Text='<%# Eval("ReferenceId") %>'
                                            Style="display: none" />
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ReferencesControl_nameTextBox" Text='<%# Eval("Name") %>'
                                            SkinID="sknCanResumePanelTextBox" ToolTip="Name" MaxLength="500"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="ReferencesControl_organizationLabel" Text="Organization"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ReferencesControl_organizationTextBox" Text='<%# Eval("Organization") %>'
                                            SkinID="sknCanResumePanelTextBox" ToolTip="Organization" MaxLength="500"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="ReferencesControl_relationLabel" Text="Relation" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ReferencesControl_relationTextBox" Text='<%# Eval("Relation") %>'
                                            SkinID="sknCanResumePanelTextBox" ToolTip="Relation" MaxLength="500"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="ReferencesControl_contInfoLabel" Text="Contact Info"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="ReferencesControl_contInfoTextBox" Text='<%# SetContactInfo(((Forte.HCM.DataObjects.Reference)Container.DataItem).ContactInformation)%>'
                                            SkinID="sknCanResumePanelTextBox" ToolTip="Contact Info" MaxLength="500"></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <asp:UpdatePanel ID="ReferencesControl_confirmPopUpUpdatPanel" runat="server">
                    <ContentTemplate>
                        <div id="ReferencesControl_hiddenDIV" runat="server" style="display: none">
                            <asp:Button ID="ReferencesControl_hiddenPopupModalButton" runat="server" />
                        </div>
                        <ajaxToolKit:ModalPopupExtender ID="ReferencesControl_deleteModalPopupExtender" runat="server"
                            PopupControlID="ReferencesControl_deleteConfirmPopUpPanel" TargetControlID="ReferencesControl_hiddenPopupModalButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:Panel ID="ReferencesControl_deleteConfirmPopUpPanel" runat="server" Style="display: none;
                            width: 30%; height: 210px; background-position: bottom; background-color: #283033;">
                            <uc1:ConfirmMsgControl ID="ReferencesControl_confirmPopupExtenderControl" runat="server"
                                Type="YesNo" Title="Delete Reference" Message="Are you sure want to delete reference details?"
                                OnOkClick="ReferencesControl_okPopUpClick" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
