﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivityDetailControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ActivityDetailControl" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>

    <script type="text/javascript">

        function StartTest()
        {
            $find("<%= ActivityDetailControl_startTestPopupExtenderControl.ClientID  %>").show();
            return false;
        }

    </script>

<div id="ActivityDetailControl_activityDiv" runat="server" visible="false">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="cand_bg_dark" style="width: 100%">
                <table style="width: 100%">
                    <tr>
                        <td colspan="3" class="candidate_test_header">
                            <asp:Label ID="ActivityDetailControl_testNameLabel" runat="server" Text=""></asp:Label>
                            <asp:HiddenField ID="ActivityDetailControl_candidateSessionIDHiddenField" runat="server" />
                            <asp:HiddenField ID="ActivityDetailControl_attemptIDHiddenField" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40%" valign="top">
                            <table style="width: 100%">
                                <tr>
                                    <td valign="top" class="candidate_label_text" style="height: 85px">
                                        <asp:Label ID="ActivityDetailControl_testDescriptionLabel" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom">
                                        <table style="width: 100%; height:100%">
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="ActivityDetailControl_ScheduledByLabel" runat="server" Text="Scheduled By" SkinID="sknLabelFieldCaptionText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="ActivityDetailControl_ScheduledByValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="ActivityDetailControl_createdDateLabel" runat="server" Text="Created Date" SkinID="sknLabelFieldCaptionText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="ActivityDetailControl_createdDateValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="ActivityDetailControl_expiryDateLabel" runat="server" Text="Expiry Date" SkinID="sknLabelFieldCaptionText"></asp:Label>
                                                </td>
                                                <td style="width: 50%">
                                                    <asp:Label ID="ActivityDetailControl_expiryDateValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 20%" valign="middle">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 2%">
                                        <asp:ImageButton ID="ActivityDetailControl_showTestIntroductionImageButton" runat="server" ToolTip="Click here to show the test introduction"
                                            SkinID="sknTestIntroImageButton" OnClick="ActivityDetailControl_showTestIntroductionImageButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ActivityDetailControl_showTestIntroductionLinkButton" runat="server" ToolTip="Click here to show the test introduction"
                                            Text="Show Test Introduction" SkinID="sknActionLinkButton" OnClick="ActivityDetailControl_showTestIntroductionLinkButton_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 2%">
                                        <asp:ImageButton ID="ActivityDetailControl_setTestReminderImageButton" runat="server" ToolTip="Click here to set the test reminder"
                                            SkinID="sknReminderImageButton" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ActivityDetailControl_setTestReminderLinkButton" runat="server" ToolTip="Click here to set the test reminder"
                                            Text="Set Test Reminder" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 2%">
                                        <asp:ImageButton ID="ActivityDetailControl_emailSchedulerImageButton" runat="server" ToolTip="Click here to send an email to scheduler"
                                            SkinID="sknMailImage" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="ActivityDetailControl_emailSchedulerLinkButton" runat="server" ToolTip="Click here to send an email to scheduler"
                                            Text="Email Scheduler" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 30%">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="ActivityDetailControl_startTestImageButton" runat="server" ToolTip="Click here to start the test" SkinID="sknStartActivityTest" OnClientClick="javascript:return StartTest()"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                 <asp:Panel ID="ActivityDetailControl_startTestPopupPanel" runat="server" Style="display: none;
                    height: 206px" CssClass="popupcontrol_confirm">
                    <div id="ActivityDetailControl_hiddenDIV" style="display: none">
                        <asp:Button ID="ActivityDetailControl_hiddenButton" runat="server" />
                    </div>
                    <uc2:ConfirmMsgControl ID="ActivityDetailControl_confirmMsgControl" runat="server" OnOkClick="ActivityDetailControl_startTestButton_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="ActivityDetailControl_startTestPopupExtenderControl"
                    runat="server" PopupControlID="ActivityDetailControl_startTestPopupPanel" TargetControlID="ActivityDetailControl_hiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</div>
<div id="ActivityDetailControl_noActivityDiv" runat="server" visible="true">
    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="no_pending_test_found" style="width: 100%">
            </td>
        </tr>
    </table>
</div>
