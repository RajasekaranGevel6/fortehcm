﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestResultsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TestResultsControl" %>
<%@ Register Src="~/CommonControls/GeneralTestStatisticsControl.ascx" TagName="TestStatisticsControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/CommonControls/MultipleSeriesChartControl.ascx" TagName="MultipleSeriesChartControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/HistogramChartControl.ascx" TagName="HistogramChartControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/CommonControls/TestResultHeaderControl.ascx" TagName="TestResultHeaderControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/CommonControls/TestResulsCanditateControl.ascx" TagName="TestResulsCanditateControl"
    TagPrefix="uc6" %>
<div id="TestResult_mainDiv">
    <div style="margin-top:5px;">
        <div class="test_results_header_bg_div" style="padding-top: 5px;background-color:#FFFFFF">
            <uc5:TestResultHeaderControl ID="TestResult_testResultHeaderControl" runat="server" />
        </div>
    </div>
    <div style="margin-top:5px;">
        <ajaxToolKit:TabContainer ID="TestResult_testStaticsContainer" runat="server" Width="100%"
            ActiveTabIndex="0">
            <ajaxToolKit:TabPanel ID="TestResult_candidateStaticsTabPanel" runat="server">
                <HeaderTemplate>
                    My Report
                </HeaderTemplate>
                <ContentTemplate>
                    <div class="msg_align">
                        <asp:Label ID="TestResult_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestResult_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                    <div>
                        <uc6:TestResulsCanditateControl ID="TestResult_testResulsCanditateControl" runat="server" />
                    </div>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="TestResult_testStaticsTabPanel" HeaderText="Search By Test"
                runat="server">
                <HeaderTemplate>
                    Test Info
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <uc2:TestStatisticsControl ID="TestResults_testStatisticsControl" runat="server">
                        </uc2:TestStatisticsControl>
                    </div>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            
        </ajaxToolKit:TabContainer>
    </div>
</div>
