﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateHeaderControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CandidateHeaderControl" %>
<%@ Register Src="ChangePassword.ascx" TagName="ChangePassword" TagPrefix="uc1" %>
<%@ Register Src="ConfirmMsgControl.ascx" TagName="ConfirmMsgControl" TagPrefix="uc2" %>
<div style="width: 950px; float: left">
    <div style="width: 196px; float: left; height: 110px; padding-left: 4px">
        <p onclick="location.href='<%= homeUrl %>'">
            <asp:Image ID="CandidateHeaderControl_logoImage" runat="server" SkinID="sknHomePageLogoImage" />
        </p>
    </div>
    <div style="width: 750px; float: left; height: 110px">
        <div style="width: 520px; float: left; height: 110px">
            &nbsp;
        </div>
        <div style="width: 230px; float: left; padding-top: 10px" class="signin_text">
            <div style="width: 230px; float: left" class="signin_text">
                Signed in as <span class="username_text">
                    <asp:Label ID="CandidatHeaderControl_loginUserNameLabel" runat="server"></asp:Label></span>
            </div>
            <div style="width: 230px; float: left; padding-top: 10px" class="signin_text">
                Last Login: <span class="date_time_text">
                    <asp:Label ID="CandidateHeaderControl_lastLoginDateTimeLabel" runat="server"></asp:Label></span>
            </div>
            <div style="width: 230px; float: left; padding-top: 10px" class="signin_text">
                <div style="width: 170px; float: left">
                    <asp:Button ID="HeaderControl_logoutButton" runat="server" Text="Log Out" SkinID="sknButtonId"
                        OnClick="HeaderControl_logoutButton_Click" />
                </div>
                <div style="width: 20px; float: right" class="help_icon_hcm" title="Help" onclick="location.href='<%= helpUrl %>'">
                </div>
                <div style="width: 20px; float: right" class="home_icon_hcm" title="Home" onclick="location.href='<%= homeUrl %>'">
                </div>
            </div>
            <div style="width: 230px; float: left; padding-top: 10px" class="signin_text">
                <asp:LinkButton ID="CandidateHeaderControl_editProfileLinkButton" runat="server"
                    ToolTip="Click here to edit your profile" Text="Edit Profile" CssClass="link_btn"
                    Visible="true" PostBackUrl="~/CandidateAdmin/EditProfile.aspx">
                </asp:LinkButton>
            </div>
        </div>
    </div>
</div>
<div>
    <ajaxToolKit:ModalPopupExtender ID="CandidateHeaderControl_logoutLimitedUserModalPopUpExtender"
        runat="server" PopupControlID="CandidateHeaderControl_logoutLimitedUserPanel"
        TargetControlID="CandidateHeaderControl_logoutLimitedUserPopupModalButton" BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="CandidateHeaderControl_logoutLimitedUserPanel" runat="server" Style="display: none"
        Height="220px" CssClass="popupcontrol_confirm">
        <uc2:ConfirmMsgControl ID="CandidateHeaderControl_logoutLimitedUserConfirmMessageControl"
            runat="server" Message="You have only limited access. You will not be allowed to login again, unless you have any test or interview associated. If required you can take a copy of the results(if shown to you). Are you sure you want to logout?"
            Title="Warning" Type="LogoutYesNo" OnOkClick="CandidateHeaderControl_logoutLimitedUserOKPopupClick"
            OnCancelClick="CandidateHeaderControl_logoutLimitedUserCancelPopupClick" />
    </asp:Panel>
    <div id="CandidateHeaderControl_logoutLimitedHiddenDIV" runat="server" style="display: none">
        <asp:Button ID="CandidateHeaderControl_logoutLimitedUserPopupModalButton" runat="server" />
    </div>
</div>
