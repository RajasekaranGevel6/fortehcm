﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class CategorySubjectControl {
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryHeaderLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchCategorySubjectControl_categoryHeaderLiteral;
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryIDHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchCategorySubjectControl_categoryIDHiddenField;
        
        /// <summary>
        /// SearchCategorySubjectControl_QuestionKey control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchCategorySubjectControl_QuestionKey;
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SearchCategorySubjectControl_categoryTextBox;
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryAutoCompleteExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender SearchCategorySubjectControl_categoryAutoCompleteExtender;
        
        /// <summary>
        /// pnl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnl;
        
        /// <summary>
        /// SearchCategorySubjectControl_addLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchCategorySubjectControl_addLinkButton;
        
        /// <summary>
        /// SearchCategorySubjectControl_selectLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchCategorySubjectControl_selectLinkButton;
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchCategorySubjectControl_categoryHiddenField;
        
        /// <summary>
        /// SearchCategorySubjectControl_removeAllLinkButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton SearchCategorySubjectControl_removeAllLinkButton;
        
        /// <summary>
        /// SearchCategorySubjectControl_categoryDataList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataList SearchCategorySubjectControl_categoryDataList;
        
        /// <summary>
        /// SearchCategorySubjectControl_subjectHeaderLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SearchCategorySubjectControl_subjectHeaderLiteral;
        
        /// <summary>
        /// SearchCategorySubjectControl_SubjectIDToBeDeletedHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField SearchCategorySubjectControl_SubjectIDToBeDeletedHiddenField;
        
        /// <summary>
        /// SearchCategorySubjectControl_subjectGridview control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView SearchCategorySubjectControl_subjectGridview;
        
        /// <summary>
        /// CategorySubjectControl_categoryIdToBeDeleted control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField CategorySubjectControl_categoryIdToBeDeleted;
        
        /// <summary>
        /// CategorySubjectControl_subjectID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField CategorySubjectControl_subjectID;
        
        /// <summary>
        /// CategorySubjectControl_selectedSubjectIDs control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField CategorySubjectControl_selectedSubjectIDs;
    }
}
