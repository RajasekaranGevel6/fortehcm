﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EducationControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.EducationControl" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc1" %>
<asp:UpdatePanel ID="EducationControl_updatePanel" runat="server">
    <ContentTemplate>
        <div id="MyResume_educationMainDiv">
            <div style="clear: both; overflow: auto; height: 290px" runat="server" id="EducationControl_controlsDiv"
                class="resume_Table_Bg">
                <asp:HiddenField ID="EducationControl_hiddenField" runat="server" />
                <asp:HiddenField ID="EducationControl_deletedRowHiddenField" runat="server" />
                <div style="float:right;">
                    <asp:Button ID="EducationControl_addDefaultButton" runat="server" 
                        SkinID="sknButtonId" ToolTip="Add Education" Text="Add" 
                        onclick="EducationControl_addDefaultButton_Click" />
                </div>
                <asp:ListView ID="EducationControl_listView" runat="server" OnItemDataBound="EducationControl_listView_ItemDataBound"
                    OnItemCommand="EducationControl_listView_ItemCommand">
                    <LayoutTemplate>
                        <div id="itemPlaceholderContainer" runat="server">
                            <div runat="server" id="itemPlaceholder">
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="resume_panel_list_view">
                            <asp:Panel GroupingText="Institution" runat="server" ID="EducationControl_schoolPanel" CssClass="can_resume_panel_text">
                                <div id="MyResume_educationDeleteImage" style="clear: both; float: right;">
                                    <asp:Button ID="EducationControl_addButton" runat="server" CommandName="addEducation" SkinID="sknButtonId" ToolTip="Add Education" Text="Add" />
                                    <asp:Button ID="EducationControl_deleteButton" runat="server" CommandName="deleteEducation" SkinID="sknButtonId" ToolTip="Delete Education" Text="Remove"/>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="EducationControl_nameLabel" Text="Name" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                        <asp:TextBox ID="EducationControl_deleteRowIndex" runat="server" Text='<%# Eval("EducationId") %>'
                                            Style="display: none" />
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="EducationControl_nameTextBox" Text='<%# Eval("SchoolName") %>'
                                            ToolTip="School Name" MaxLength="500" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="EducationControl_locationLabel" Text="Location" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="EducationControl_locationTextBox" Text='<%# SetLocation(((Forte.HCM.DataObjects.Education)Container.DataItem).SchoolLocation) %>'
                                            ToolTip="School Location" MaxLength="500" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="EducationControl_degreeLabel" Text="Degree" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="EducationControl_degreeTextBox" Text='<%# Eval("DegreeName") %>'
                                            ToolTip="Degree Name" MaxLength="100" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="EducationControl_specializationLabel" Text="Specialization"
                                            SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="EducationControl_specializationTextBox" Text='<%# Eval("Specialization") %>'
                                            ToolTip="Specialization" MaxLength="500" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="can_resume_form_container_panel">
                                    <div class="can_resume_container_left">
                                        <asp:Label runat="server" ID="EducationControl_gpaLabel" Text="GPA" SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <asp:TextBox runat="server" ID="EducationControl_gpaTextBox" Text='<%# Eval("GPA") %>'
                                            ToolTip="GPA" MaxLength="5" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                    </div>
                                    <div class="can_resume_container_left" style="padding-left: 20px;">
                                        <asp:Label runat="server" ID="EducationControl_graduationDtLabel" Text="Graduation Date"
                                            SkinID="sknCanReumeLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="can_resume_container_right">
                                        <ajaxToolKit:MaskedEditExtender ID="EducationControl_MaskedEditExtender" runat="server"
                                            TargetControlID="EducationControl_graduationDtTextBox" Mask="99/99/9999" MessageValidatorTip="true"
                                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="Date"
                                            DisplayMoney="None" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                        <ajaxToolKit:MaskedEditValidator ID="EducationControl_MaskedEditValidator" runat="server"
                                            ControlExtender="EducationControl_MaskedEditExtender" ControlToValidate="EducationControl_graduationDtTextBox"
                                            EmptyValueMessage="Date is required" InvalidValueMessage="Date is invalid" Display="None"
                                            TooltipMessage="Input a date" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"
                                            ValidationGroup="MKE" />
                                        <ajaxToolKit:CalendarExtender ID="EducationControl_customCalendarExtender" runat="server"
                                            TargetControlID="EducationControl_graduationDtTextBox" CssClass="MyCalendar"
                                            Format="MM/dd/yyyy" PopupPosition="BottomLeft" PopupButtonID="EducationControl_calendarGraduationDtImageButton" />
                                        <asp:TextBox runat="server" ID="EducationControl_graduationDtTextBox" Text='<%# Eval("GraduationDate") %>'
                                            ToolTip="Graduation Date" Width="72px" SkinID="sknCanResumePanelTextBox"></asp:TextBox>
                                        <asp:ImageButton ID="EducationControl_calendarGraduationDtImageButton" SkinID="sknCalendarImageButton"
                                            runat="server" ImageAlign="AbsMiddle" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <asp:UpdatePanel ID="EducationControl_confirmPopUpUpdatPanel" runat="server">
                    <ContentTemplate>
                        <div id="EducationControl_hiddenDIV" runat="server" style="display: none">
                            <asp:Button ID="EducationControl_hiddenPopupModalButton" runat="server" />
                        </div>
                        <ajaxToolKit:ModalPopupExtender ID="EducationControl_deleteModalPopupExtender" runat="server"
                            PopupControlID="EducationControl_deleteConfirmPopUpPanel" TargetControlID="EducationControl_hiddenPopupModalButton"
                            BackgroundCssClass="modalBackground">
                        </ajaxToolKit:ModalPopupExtender>
                        <asp:Panel ID="EducationControl_deleteConfirmPopUpPanel" runat="server" Style="display: none;
                            width: 30%; height: 210px; background-position: bottom; background-color: #283033;">
                            <uc1:ConfirmMsgControl ID="EducationControl_confirmPopupExtenderControl" runat="server"
                                Type="YesNo" Title="Delete Education" Message="Are you sure want to delete education details?"
                                OnOkClick="EducationControl_okPopUpClick" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
