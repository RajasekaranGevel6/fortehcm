﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SingleSeriesChartControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.SingleSeriesChartControl" %>
<div id="SingleSeriesChartControl_mainDiv">
    <div runat="server" id="SingleSeriesChartControl_charControl">
        <asp:Chart ID="SingleSeriesChartControl_chart" runat="server" ToolTip="Click here to zoom graph"
            Palette="Pastel" BackColor="Transparent" AntiAliasing="Graphics" Height="144px"
            Width="250px" EnableViewState="true">
            <Legends>
                <asp:Legend Name="SingleSeriesChartControl_legend" BackColor="Transparent" Font="Arial, 8.25pt"
                    IsTextAutoFit="False" Alignment="Center" ForeColor="114, 142, 192" Docking="Right"
                    IsDockedInsideChartArea="False" BackImageWrapMode="TileFlipXY" ItemColumnSpacing="10"
                    MaximumAutoSize="70" TextWrapThreshold="90">
                    <CellColumns>
                        <asp:LegendCellColumn ColumnType="SeriesSymbol" Name="SingleSeriesChartControl_legendSymbolColumn">
                            <Margins Left="100" Right="0"></Margins>
                        </asp:LegendCellColumn>
                        <asp:LegendCellColumn Name="SingleSeriesChartControl_legendSeriesColumn" Alignment="MiddleLeft">
                            <Margins Left="0" Right="0"></Margins>
                        </asp:LegendCellColumn>
                        <asp:LegendCellColumn Name="SingleSeriesChartControl_legendPercentColumn" Text="#PERCENT{P0}"
                            Alignment="MiddleLeft">
                            <Margins Left="0" Right="0"></Margins>
                        </asp:LegendCellColumn>
                        <asp:LegendCellColumn Name="SingleSeriesChartControl_legendValueColumn" Text="(#VAL)">
                            <Margins Left="0" Right="0"></Margins>
                        </asp:LegendCellColumn>
                    </CellColumns>
                </asp:Legend>
            </Legends>
            <Series>
                <asp:Series Name="SingleSeriesChartControl_series" ChartArea="SingleSeriesChartControl_chartArea">
                </asp:Series>
            </Series>
            <Titles>
                <asp:Title Name="SingleSeriesChartControl_title" TextStyle="Default" ForeColor="180, 65, 140, 240"
                    Font="Arial, 8.25pt, style=Bold">
                </asp:Title>
            </Titles>
            <ChartAreas>
                <asp:ChartArea Name="SingleSeriesChartControl_chartArea" BackSecondaryColor="Transparent"
                    BackColor="Transparent" ShadowColor="Transparent" BackGradientStyle="TopBottom"
                    AlignmentOrientation="Horizontal">
                    <AxisY TitleForeColor="180, 65, 140, 240">
                        <MajorGrid Enabled="false" />
                    </AxisY>
                    <AxisX TitleForeColor="180, 65, 140, 240" IsLabelAutoFit="False">
                        <MajorGrid Enabled="false" />
                    </AxisX>
                    <Position Height="90" Width="35" Y="15" />
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
</div>
