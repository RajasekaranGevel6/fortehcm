﻿#region Header
// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// HistogramChartControl.cs
// File that represents the user interface for the histogram chart
//
#endregion

#region Directives
using System;
using System.IO;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Represents the method to load the histogram chart control
    /// </summary>
    public partial class HistogramChartControl : UserControl
    {
        #region Event Handlers
        /// <summary>
        /// Handler method that is called when the page is loaded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion Event Handlers

        #region Public Methods
        /// <summary>
        /// Represents the method to load the chart control
        /// </summary>
        /// <param name="value">
        /// A<see cref="HistogramData"/>that holds the histogram data
        /// </param>
        public void LoadChartControl(ReportHistogramData value)
        {
            //Set the height of the control 
            HistogramChartControl_histogramChart.Height = Unit.Pixel(value.ChartLength);

            //Set the width of the control
            HistogramChartControl_histogramChart.Width = Unit.Pixel(value.ChartWidth);

            //Defines whether the chart title has to display or not
            HistogramChartControl_histogramChart.Titles[0].Visible =
                value.IsDisplayChartTitle;

            //Assign the chart title to the chart
            if (value.IsDisplayChartTitle)
            {
                HistogramChartControl_histogramChart.
                    Titles["HistogramChartControl_title"].Text = value.ChartTitle;
            }

            //Defines whether the axis title has to display or not
            if (value.IsDisplayAxisTitle)
            {
                HistogramChartControl_histogramChart.ChartAreas
                    ["HistogramChartControl_chartArea"].AxisX.Title = value.XAxisTitle;

                HistogramChartControl_histogramChart.ChartAreas
                    ["HistogramChartControl_chartArea"].AxisY.Title = value.YAxisTitle;
            }

            //Assign the tool tip for the chart series point
            HistogramChartControl_histogramChart.Series
                ["HistogramChartControl_histogramSeries"].ToolTip = "#VAL";

            //Defines whether the value has to be shown as label in chart
            HistogramChartControl_histogramChart.Series
                ["HistogramChartControl_histogramSeries"].IsValueShownAsLabel = value.IsShowLabel;

            LoadHistogramChart(value.ChartData, value.SegmentInterval, value.CandidateScore
                , value.IsDisplayCandidateScore, value.IsDisplayCandidateName, value.CandidateName, value.CandidateScoreData);

            //Get the new session ID
            string sessionId = System.Guid.NewGuid().ToString();

            //Assign the session ID to the session 
            Session[sessionId] = value;

            //Assign the java script to show the zoomed chart for the td 
            HistogramChartControl_chartControl.Attributes.Add("onclick",
                "javascript:return ShowZoomedChart('" + sessionId + "','Histogram');");
            if (!Utility.IsNullOrEmpty(value.ChartImageName))
            {
                //if (!new FileInfo(Server.MapPath("../chart/") + value.ChartImageName + ".png").Exists)
                HistogramChartControl_histogramChart.SaveImage(Server.MapPath("../chart/") + value.ChartImageName + ".png", ChartImageFormat.Png);
            }
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Represents the method to load the hsitogram chart
        /// </summary>
        /// <param name="list">
        /// A<see cref="List<ChartData>"/>that holds the chart data
        /// </param>
        /// <param name="segmentInterval">
        /// A<see cref="int"/>that holds the segment interval
        /// </param>
        /// <param name="candidateScore">
        /// A<see cref="double"/>that holds the candidate score
        /// </param>
        /// <param name="isDisplayCandidateScore">
        /// A<see cref="bool"/>that holds the isDisplayCandidate Score
        /// </param>
        private void LoadHistogramChart(List<ChartData> list, int segmentInterval,
            double candidateScore, bool isDisplayCandidateScore, bool isDisplayCandidateName,
            string candidateName, List<HistogramData> candidateScoreData)
        {
            //Clear the point before assigning 
            HistogramChartControl_histogramChart.Series["HistogramChartControl_histogramSeries"].Points.Clear();

            /// <summary>
            /// Number of class intervals the data range is devided in.
            /// This property only has affect when "SegmentIntervalWidth" is 
            /// set to double.NaN.
            /// </summary>
            int SegmentIntervalNumber = segmentInterval;

            /// <summary>
            /// Histogram class interval width. Setting this value to "double.NaN"
            /// will result in automatic width calculation based on the data range
            /// and number of required interval specified in "SegmentIntervalNumber".
            /// </summary>
            double SegmentIntervalWidth = double.NaN;

            // Get data series minimum and maximum values
            double minValue = Convert.ToDouble(0);
            double maxValue = Convert.ToDouble(100);
            //int pointCount = 0;


            // Calculate interval width if it's not set
            if (double.IsNaN(SegmentIntervalWidth))
            {
                SegmentIntervalWidth = (maxValue - minValue) / SegmentIntervalNumber;
                SegmentIntervalWidth = RoundInterval(SegmentIntervalWidth);
            }

            //// Round minimum and maximum values
            //minValue = Math.Floor(minValue / SegmentIntervalWidth) * SegmentIntervalWidth;
            //maxValue = Math.Ceiling(maxValue / SegmentIntervalWidth) * SegmentIntervalWidth;

            // Create histogram series points
            double currentPosition = minValue;
            int maxYAxis = 0;
            for (currentPosition = minValue; currentPosition <= maxValue; currentPosition += SegmentIntervalWidth)
            {
                // Count all points from data series that are in current interval
                int count = 0;
                foreach (ChartData data in list)
                {
                    if (!Utility.IsNullOrEmpty(data))
                    {
                        double endPosition = currentPosition + SegmentIntervalWidth;
                        if (data.ChartYValue > currentPosition &&
                            data.ChartYValue <= endPosition)
                        {
                            ++count;
                        }

                        // Last segment includes point values on both segment boundaries
                        else if (currentPosition <= minValue)
                        {
                            if (data.ChartYValue >= currentPosition &&
                                data.ChartYValue <= endPosition)
                            {
                                ++count;
                            }
                        }
                    }
                }
                // Add data point into the histogram series
                HistogramChartControl_histogramChart.Series["HistogramChartControl_histogramSeries"]
                    .Points.AddXY(currentPosition + SegmentIntervalWidth / 2.0, count);

                if (maxYAxis < count)
                {
                    maxYAxis = count;
                }
            }

            Series histogramSeries = HistogramChartControl_histogramChart.
                                    Series["HistogramChartControl_histogramSeries"];

            histogramSeries["PointWidth"] = "1";

            ChartArea chartArea = HistogramChartControl_histogramChart.ChartAreas[0];
            chartArea.AxisX.Minimum = minValue;
            chartArea.AxisX.Maximum = maxValue;

            //chartArea.AxisY.Minimum = 0;
            chartArea.AxisY.Maximum = maxYAxis;
            //  chartArea.AxisY.MajorGrid.Interval = 1;


            // Set axis interval based on the histogram class interval
            // and do not allow more than 10 labels on the axis.
            double axisInterval = SegmentIntervalWidth;
            while ((maxValue - minValue) / axisInterval > 10.0)
            {
                axisInterval *= 2.0;
            }
            chartArea.AxisX.Interval = axisInterval;

            if (candidateScoreData == null)
                foreach (DataPoint point in histogramSeries.Points)
                {
                    //assign default  color for all points
                    point.Color = ColorTranslator.FromHtml("#BA55D3");

                    //assign different blue color for my score
                    if ((candidateScore == point.XValue - 5) && (candidateScore == 0))
                    {
                        point.Color = ColorTranslator.FromHtml("#7DC9E9");
                    }
                    else if (((candidateScore <= point.XValue + 5) &&
                        (candidateScore >= point.XValue + 5 - SegmentIntervalWidth + 1)))
                    {
                        point.Color = ColorTranslator.FromHtml("#7DC9E9");
                    }
                    //if ((candidateScore >= point.XValue - SegmentIntervalWidth && candidateScore <= point.XValue) ||
                    //    (candidateScore <= point.XValue + SegmentIntervalWidth && candidateScore >= point.XValue))
                    //{
                    //    point.Color = ColorTranslator.FromHtml("#BA55D3");
                    //}
                }

            //return if candidate score data is null 
            if (candidateScoreData == null)
                return;

            foreach (DataPoint point in histogramSeries.Points)
            {
                //assign default  color for all points
                point.Color = ColorTranslator.FromHtml("#BA55D3");
            }

            foreach (HistogramData data in candidateScoreData)
            {
                foreach (DataPoint point in histogramSeries.Points)
                {

                    if ((data.CandidateScore == point.XValue - 5) && (data.CandidateScore == 0))
                    {
                        point.Color = ColorTranslator.FromHtml("#7DC9E9");

                        if (data.CandidateName.Length > 5)
                        {
                            point.Label += "\n" + data.CandidateName.Substring(0, 5) + "\n";
                        }
                        else
                        {
                            point.Label += "\n" + data.CandidateName + "\n";
                        }

                        break;
                    }

                    else if (((data.CandidateScore <= point.XValue + 5) &&
                        (data.CandidateScore >= point.XValue + 5 - SegmentIntervalWidth + 1)))
                    {
                        point.Color = ColorTranslator.FromHtml("#7DC9E9");

                        if (data.CandidateName.Length > 5)
                        {
                            point.Label += "\n" + data.CandidateName.Substring(0, 5) + "\n";
                        }
                        else
                        {
                            point.Label += "\n" + data.CandidateName + "\n";
                        }
                        break;
                    }
                }
            }
            foreach (DataPoint point in histogramSeries.Points)
            {
                if (point.Color == ColorTranslator.FromHtml("#7DC9E9")
                    && point.YValues[0] != 0)
                {
                    point.Label += "\n" + "#VAL";
                }
            }

        }
        /// <summary>
        /// Helper method which rounds specified axsi interval.
        /// </summary>
        /// <param name="interval">Calculated axis interval.</param>
        /// <returns>Rounded axis interval.</returns>
        private double RoundInterval(double interval)
        {
            // If the interval is zero return error
            if (interval == 0.0)
            {
                throw (new ArgumentOutOfRangeException("interval", "Interval can not be zero."));
            }

            // If the real interval is > 1.0
            double step = -1;
            double tempValue = interval;
            while (tempValue > 1.0)
            {
                step++;
                tempValue = tempValue / 10.0;
                if (step > 1000)
                {
                    throw (new InvalidOperationException("Auto interval error due to invalid point values or axis minimum/maximum."));
                }
            }

            // If the real interval is < 1.0
            tempValue = interval;
            if (tempValue < 1.0)
            {
                step = 0;
            }

            while (tempValue < 1.0)
            {
                step--;
                tempValue = tempValue * 10.0;
                if (step < -1000)
                {
                    throw (new InvalidOperationException("Auto interval error due to invalid point values or axis minimum/maximum."));
                }
            }

            double tempDiff = interval / Math.Pow(10.0, step);
            if (tempDiff < 3.0)
            {
                tempDiff = 2.0;
            }
            else if (tempDiff < 7.0)
            {
                tempDiff = 5.0;
            }
            else
            {
                tempDiff = 10.0;
            }

            // Make a correction of the real interval
            return tempDiff * Math.Pow(10.0, step);
        }
        #endregion Private Methods
    }
}