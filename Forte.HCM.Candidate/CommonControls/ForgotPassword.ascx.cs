﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ForgotPassword.cs
// File that represents the user interface for the Forgot Password information 

#endregion Header

#region Directives                                                             
using System;
using System.Text;
using System.Web.UI;
using System.Reflection;

using Forte.HCM.BL;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ForgotPassword : System.Web.UI.UserControl
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ForgotPassword_topErrorMessageLabel.Text = "";
            ForgotPassword_topSuccessMessageLabel.Text = "";
        }

        /// <summary>
        /// Handles the Click event of the ForgotPassword_sendButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ForgotPassword_sendButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsValid())
                {
                    ShowErrorMessage(new Exception("Enter user ID"));
                }
                else
                {
                    UserDetail userDetail = new UserDetail();
                    userDetail = new UserRegistrationBLManager().GetPassword(ForgotPassword_userIDTextBox.Text.Trim());

                    if (Support.Utility.IsNullOrEmpty(userDetail))
                    {
                        ShowErrorMessage(new Exception("Enter the valid user ID"));
                    }
                    else
                    {
                        string emailId = "";
                        string DecryptedPassword = new HCM.Utilities.EncryptAndDecrypt().DecryptString(userDetail.Password);
                        MailDetail mailDetails = new MailDetail();
                        EmailHandler Email = new EmailHandler();
                        if (Support.Utility.IsNullOrEmpty(userDetail.Email))
                        {
                            emailId = userDetail.UserName;
                        }
                        else
                        {
                            emailId = userDetail.Email;
                        }
                        userDetail.Email = emailId;
                        userDetail.Password = DecryptedPassword;
                        Email.SendMail(ForgotPassword_userIDTextBox.Text.Trim(), "Password", GetMailBody(userDetail));
                        ForgotPassword_topSuccessMessageLabel.Text = "Password sent your email";
                    }
                }
            }
            catch (Exception ex)
            {
                ShowErrorMessage(new Exception(ex.Message));
            }
            finally {

                InvokePageBaseMethod("ForgotPasswordModalpPopupExtender", null);
            }
        }
        #endregion

        #region Private Methods                                                
        /// <summary>
        /// Gets the mail body.
        /// </summary>
        /// <param name="userDetail">The user detail.</param>
        /// <returns></returns>
        private string GetMailBody(UserDetail userDetail)
        {
            StringBuilder sbBody = new StringBuilder();
            try
            {
                sbBody.Append("Hi "+ userDetail.FirstName +" "+ userDetail.LastName);
                sbBody.Append(", ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This is your password: " + userDetail.Password);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("Thank you for your interest in ForteHCM.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append(Environment.NewLine);
                sbBody.Append("============================== Disclaimer ==============================");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("This message is for the designated recipient only and may contain privileged, proprietary, or ");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("otherwise private information. If you have received it in error, please notify the sender");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("immediately and delete the original. Any other use of the email by you is prohibited.");
                sbBody.Append(Environment.NewLine);
                sbBody.Append("=========================== End Of Disclaimer ============================");
                return sbBody.ToString();
            }
            finally
            {
                if (sbBody != null) sbBody = null;
                try
                {
                    GC.Collect(2, GCCollectionMode.Forced);
                }
                catch { try { GC.Collect(); } catch { } }
            }
        }

        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </returns>
        private bool IsValid()
        {
            bool isValid = true;
            if (Support.Utility.IsNullOrEmpty(ForgotPassword_userIDTextBox.Text))
            {
                isValid = false;
            }
            if (!isValid)
                InvokePageBaseMethod("ForgotPasswordModalpPopupExtender", null);
            return isValid;
        }

        /// <summary>
        /// Invokes the page base method.
        /// </summary>
        /// <param name="MethodName">Name of the method.</param>
        /// <param name="args">The args.</param>
        private void InvokePageBaseMethod(string MethodName, object[] args)
        {
            Page.GetType().InvokeMember(MethodName, BindingFlags.InvokeMethod, null, this.Page, args);
        }

        /// <summary>
        /// Shows the error message.
        /// </summary>
        /// <param name="exp">The exp.</param>
        private void ShowErrorMessage(Exception exp)
        {
            if (string.IsNullOrEmpty(ForgotPassword_topErrorMessageLabel.Text))
                ForgotPassword_topErrorMessageLabel.Text = exp.Message;
            else
                ForgotPassword_topErrorMessageLabel.Text += "<br />" + exp.Message;
        }
        #endregion
    }
}