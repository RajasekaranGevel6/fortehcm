﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestResultHeaderControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TestResultHeaderControl" %>
<div id="TestResultHeaderControl_testMainDiv">
    <div>
        <div style="float: left; width: 260px;">
                <asp:Label ID="TestResultHeaderControl_candidateNameValueTextBox" runat="server"
                    Text="" SkinID="sknLabelDivFieldText"></asp:Label>
            
        </div>
        <div style="float: left; width: 400px;">
            <asp:Label ID="TestResultHeaderControl_testNameValueTextBox" runat="server" Text=""
                SkinID="sknLabelDivFieldText"></asp:Label>
        </div>
        <div style="float: left; width: 230px;">
            <asp:Label ID="TestResultHeaderControl_testCompletedValueTextBox" runat="server"
                Text="" SkinID="sknLabelDivFieldText"></asp:Label>
        </div>
    </div>
</div>
