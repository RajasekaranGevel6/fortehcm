﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterviewDetailControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.InterviewDetailControl" %>
<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>

    <script type="text/javascript">

        function StartInterview()
        {
            $find("<%= InterviewDetailControl_startInterviewPopupExtenderControl.ClientID  %>").show();
            return false;
        }

        function ContinueInterview()
        {
            $find("<%= InterviewDetailControl_continueInterviewPopupExtenderControl.ClientID  %>").show();
            return false;
        }

    </script>

<div id="InterviewDetailControl_activityDiv" runat="server" visible="false">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="cand_bg_dark" style="width: 100%">
                <table style="width: 100%">
                    <tr>
                        <td colspan="3" class="candidate_test_header">
                            <asp:Label ID="InterviewDetailControl_interviewNameLabel" runat="server" Text=""></asp:Label>
                            <asp:HiddenField ID="InterviewDetailControl_candidateSessionIDHiddenField" runat="server" />
                            <asp:HiddenField ID="InterviewDetailControl_attemptIDHiddenField" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40%" valign="top">
                            <table style="width: 100%">
                                <tr>
                                    <td valign="top" class="candidate_label_text" style="height: 85px">
                                        <asp:Label ID="InterviewDetailControl_interviewDescriptionLabel" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom">
                                        <table style="width: 100%; height:100%">
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="InterviewDetailControl_scheduledByLabel" runat="server" Text="Scheduled By" SkinID="sknLabelFieldCaptionText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="InterviewDetailControl_scheduledByValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="InterviewDetailControl_createdDateLabel" runat="server" Text="Created Date" SkinID="sknLabelFieldCaptionText"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="InterviewDetailControl_createdDateValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="InterviewDetailControl_expiryDateLabel" runat="server" Text="Expiry Date" SkinID="sknLabelFieldCaptionText"></asp:Label>
                                                </td>
                                                <td style="width: 50%">
                                                    <asp:Label ID="InterviewDetailControl_expiryDateValueLabel" runat="server" Text=""
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 20%" valign="middle">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 2%">
                                        <asp:ImageButton ID="InterviewDetailControl_showInterviewIntroductionImageButton" runat="server" ToolTip="Click here to show the interview introduction"
                                            SkinID="sknTestIntroImageButton" OnClick="InterviewDetailControl_showInterviewIntroductionImageButton_Click" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="InterviewDetailControl_showInterviewIntroductionLinkButton" runat="server" ToolTip="Click here to show the interview introduction"
                                            Text="Show Interview Introduction" SkinID="sknActionLinkButton" OnClick="InterviewDetailControl_showInterviewIntroductionLinkButton_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 2%">
                                        <asp:ImageButton ID="InterviewDetailControl_setInterviewReminderImageButton" runat="server" ToolTip="Click here to set the interview reminder"
                                            SkinID="sknReminderImageButton" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="InterviewDetailControl_setInterviewReminderLinkButton" runat="server" ToolTip="Click here to set the interview reminder"
                                            Text="Set Interview Reminder" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 2%">
                                        <asp:ImageButton ID="InterviewDetailControl_emailSchedulerImageButton" runat="server" ToolTip="Click here to send an email to scheduler"
                                            SkinID="sknMailImage" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="InterviewDetailControl_emailSchedulerLinkButton" runat="server" ToolTip="Click here to send an email to scheduler"
                                            Text="Email Scheduler" SkinID="sknActionLinkButton"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 30%">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="InterviewDetailControl_startInterviewImageButton" runat="server" ToolTip="Click here to start the interview" SkinID="sknStartActivityTest" OnClientClick="javascript:return StartInterview()"/>
                                        <asp:ImageButton ID="InterviewDetailControl_continueInterviewImageButton" runat="server" ToolTip="Click here to continue the interview" SkinID="sknContinueActivityTest" OnClientClick="javascript:return ContinueInterview()" Visible="false"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                 <asp:Panel ID="InterviewDetailControl_startInterviewPopupPanel" runat="server" Style="display: none;
                    height: 206px" CssClass="popupcontrol_confirm">
                    <div id="InterviewDetailControl_startInterviewHiddenDIV" style="display: none">
                        <asp:Button ID="InterviewDetailControl_startInterviewHiddenButton" runat="server" />
                    </div>
                    <uc2:ConfirmMsgControl ID="InterviewDetailControl_confirmStartInterviewMsgControl" runat="server" OnOkClick="InterviewDetailControl_startInterviewButton_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="InterviewDetailControl_startInterviewPopupExtenderControl"
                    runat="server" PopupControlID="InterviewDetailControl_startInterviewPopupPanel" TargetControlID="InterviewDetailControl_startInterviewHiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td>
                 <asp:Panel ID="InterviewDetailControl_continueInterviewPopupPanel" runat="server" Style="display: none;
                    height: 206px" CssClass="popupcontrol_confirm">
                    <div id="InterviewDetailControl_continueInterviewHiddenDIV" style="display: none">
                        <asp:Button ID="InterviewDetailControl_continueInterviewHiddenButton" runat="server" />
                    </div>
                    <uc2:ConfirmMsgControl ID="InterviewDetailControl_confirmContinueInterviewMsgControl" runat="server" OnOkClick="InterviewDetailControl_continueInterviewButton_OkClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="InterviewDetailControl_continueInterviewPopupExtenderControl"
                    runat="server" PopupControlID="InterviewDetailControl_continueInterviewPopupPanel" TargetControlID="InterviewDetailControl_continueInterviewHiddenButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
            </td>
        </tr>
    </table>
</div>
<div id="InterviewDetailControl_noActivityDiv" runat="server" visible="true">
    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="no_pending_interview_found" style="width: 100%">
            </td>
        </tr>
    </table>
</div>
