﻿using System;
using System.Web.UI;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;

namespace Forte.HCM.UI.CommonControls
{
    public partial class TestResulsCanditateControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Used to assign the values for the candidate details
        /// </summary>
        public void LoadCandidateStatisticsDetails(CandidateStatisticsDetail CandidateStatisticsDataSource)
        {
            string noOfQuestionAttend = string.Empty;
            noOfQuestionAttend = string.Concat(CandidateStatisticsDataSource.TotalQuestionAttended.ToString(), "/",
                CandidateStatisticsDataSource.TotalQuestion.ToString());

            TestResulsCanditateControl_noOfQuestionLabelValue.Text = noOfQuestionAttend;

            TestResulsCanditateControl_percentileLabelValue.Text =
                string.Format("{0}%", CandidateStatisticsDataSource.Percentile.ToString());
            
            TestResulsCanditateControl_noOfQuestionTimeTakenLabelValue.Text
                = CandidateStatisticsDataSource.TimeTaken;

            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.TimeTaken))
            {
                int timeTaken = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(
                    CandidateStatisticsDataSource.TimeTaken));
                TestResulsCanditateControl_noOfQuestionTimeTakenLabelValue.Text =
                    Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(timeTaken);
            }

            TestResulsCanditateControl_percentageQuestionsAnsweredCorrectlyLabelValue.Text =
                 string.Format("{0}%", CandidateStatisticsDataSource.AnsweredCorrectly);

            TestResulsCanditateControl_myScoreLabelValue.Text = 
                 string.Format("{0}%", CandidateStatisticsDataSource.MyScore.ToString());
            
            decimal averageTimeTaken = 0;
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.AverageTimeTaken)
                && CandidateStatisticsDataSource.AverageTimeTaken.Length != 0)
            {
                averageTimeTaken = decimal.Parse(CandidateStatisticsDataSource.AverageTimeTaken);
            }
            TestResulsCanditateControl_avgTimeTakenLabelValue.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds
                (decimal.ToInt32(averageTimeTaken));

            if (!Utility.IsNullOrEmpty(TestResulsCanditateControl_avgTimeTakenLabelValue.Text))
            {
                int aveTimePerQuestion = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(
                    TestResulsCanditateControl_avgTimeTakenLabelValue.Text.ToString()));
                TestResulsCanditateControl_avgTimeTakenLabelValue.Text = 
                    Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(aveTimePerQuestion);
            }
        }

        public void chartData(MultipleSeriesChartData chartCategoryDataSource, MultipleSeriesChartData chartSubjectDataSource, 
            MultipleSeriesChartData chartTestAreaDataSource,MultipleSeriesChartData chartComplexityDataSource)
        {
            TestResultControl_multipleSeriesChartControl.MultipleChartDataSource = chartCategoryDataSource;
            MultipleSeriesChartControl_subjectChart.MultipleChartDataSource = chartSubjectDataSource;
            MultipleSeriesChartControl_testAreaChart.MultipleChartDataSource = chartTestAreaDataSource;
            MultipleSeriesChartControl_complexityChart.MultipleChartDataSource = chartComplexityDataSource;
        }
        public void chartHistogramData(ReportHistogramData chartComplexityDataSource)
        {
            //TestResultsControl_histogramChartControl.LoadChartControl(chartComplexityDataSource);
        }
    }
}