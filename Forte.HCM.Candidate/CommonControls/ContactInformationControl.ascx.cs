﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ContactInformationControl.cs
// File that represents the user interface for the contact information details

#endregion Header

#region Directives                                                             

using System;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ContactInformationControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
       
        public ContactInformation DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                ContactInformationControl_firstNameTextBox.Text = value.FirstName;
                ContactInformationControl_middleNameTextBox.Text = value.MiddleName;
                ContactInformationControl_lastNameTextBox.Text = value.LastName;
                ContactInformationControl_cityTextBox.Text = value.City;
                ContactInformationControl_stateTextBox.Text = value.State;
                ContactInformationControl_countryNameTextBox.Text = value.Country;
                ContactInformationControl_emailTextBox.Text = value.EmailAddress;
                ContactInformationControl_fixedLineTextBox.Text = value.Phone.Residence;
                ContactInformationControl_mobileTextBox.Text = value.Phone.Mobile;
                ContactInformationControl_postalCodeTextBox.Text = value.PostalCode;
                ContactInformationControl_streetTextBox.Text = value.StreetAddress;
                ContactInformationControl_websiteTextBox.Text = value.WebSiteAddress.Personal;
            }
        }
    }
}