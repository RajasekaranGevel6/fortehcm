﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutomatedTestSummaryControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.AutomatedTestSummaryControl" %>
<%@ Register Src="SingleSeriesChartControl.ascx" TagName="SingleSeriesChartControl"
    TagPrefix="uc1" %>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td class="grid_header_bg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="15%" class="header_text_bold">
                                    <asp:Literal ID="AutomatedTestSummaryControl_testSegmemtLiteral" runat="server" Text="Test Statistics">
                                    </asp:Literal>&nbsp;<%--<asp:Label ID="AutomatedTestSummaryControl_sortHelpLabel" runat="server"
                                        SkinID="sknLabelText" Text="<%$ Resources:HCMResource, GridHeaderSortHelp %>"></asp:Label>--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%" class="grid_body_bg">
                        <asp:UpdatePanel ID="AutomatedTestSummarControl" runat="server">
                            <ContentTemplate>
                                <div id="AutomatedTestSummaryControl_testSegmentDiv" runat="server">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:GridView ID="AutomatedTestSummaryControl_testSegmemtGridView" runat="server"
                                                    GridLines="Horizontal" BorderColor="white" BorderWidth="1px" AutoGenerateColumns="False"
                                                    Width="100%" OnRowDataBound="AutomatedTestSummaryControl_testSegmemtGridView_RowDataBound"
                                                    OnRowCreated="AutomatedTestSummaryControl_testSegmemtGridView_RowCreated">
                                                    <RowStyle CssClass="grid_alternate_row" />
                                                    <AlternatingRowStyle CssClass="grid_alternate_row" />
                                                    <HeaderStyle CssClass="grid_header_row" />
                                                    <Columns>
                                                        <asp:BoundField DataField="CategoryName" HeaderText="Category" />
                                                        <asp:BoundField DataField="SubjectName" HeaderText="Subject" />
                                                        <asp:BoundField DataField="TestAreaName" HeaderText="Test Area" />
                                                        <asp:BoundField DataField="Complexity" HeaderText="Complexity" />
                                                        <asp:BoundField DataField="NoofQuestionsInCategory" HeaderText="Question Count" ItemStyle-CssClass="td_padding_right_20"
                                                            ItemStyle-Width="18px" ItemStyle-HorizontalAlign="right" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
