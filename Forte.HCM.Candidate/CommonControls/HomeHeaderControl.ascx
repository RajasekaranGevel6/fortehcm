﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeHeaderControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.HomeHeaderControl" %>
<%@ Register Src="ForgotPassword.ascx" TagName="ForgotPassword" TagPrefix="uc1" %>
<%@ Register Src="ChangePassword.ascx" TagName="ChangePassword" TagPrefix="uc2" %>
<%@ Register Src="ConfirmMsgControl.ascx" TagName="ConfirmMsgControl" TagPrefix="uc3" %>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="left" style="width: 15%; vertical-align: middle; padding-top: 5px">
                        <p onclick="location.href='<%= homeUrl %>'">
                            <asp:Image ID="HomePage_logoImage" runat="server" SkinID="sknHomePageLogoImage" />
                        </p>
                    </td>
                    <td style="width: 85%" align="right" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td valign="top" align="right">
                                    <table cellpadding="3" cellspacing="2" border="0" class="login_bg" style="float: right;"
                                        id="HomeHeaderControl_loginTable" runat="server">
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" ID="HomePage_userNameLabel" Text="User Name" SkinID="sknHomePageLabel"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" SkinID="sknHomePageTextBox" ID="HomePage_userNametextbox"></asp:TextBox>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="HomePage_passwordLabel" runat="server" Text="Password" SkinID="sknHomePageLabel"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="HomePage_passwordtextbox" TextMode="Password" SkinID="sknHomePageTextBox"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="HomePage_goImageButton" runat="server" SkinID="sknHomePageGoImageButton"
                                                    OnClick="HomePage_goImageButton_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td colspan="3" align="left">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:CheckBox ID="HomePage_rememberMeCheckBox" runat="server" Text="Remember me"
                                                                CssClass="labletext" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td align="right">
                                                            <asp:LinkButton ID="HomePage_forgotPasswordLinkButton" runat="server" Text="Forgot Password?"
                                                                CssClass="link_btn"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="2" cellpadding="2" id="HomeHeaderControl_loggedInTable"
                                        runat="server">
                                        <tr>
                                            <td class="signin_text" align="left">
                                                Signed in as <span class="username_text">
                                                    <asp:Label ID="HomeHeaderControl_loginUserNameLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="signin_text" align="left">
                                                Last Login: <span class="date_time_text">
                                                    <asp:Label ID="HomeHeaderControl_lastLoginDateTimeLabel" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="HeaderControl_logoutButton" runat="server" Text="Log Out" SkinID="sknButtonId"
                                                                OnClick="HomeHeaderControl_logoutButton_Click" Width="60px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:LinkButton ID="HeaderControl_editProfileLinkButton" runat="server" Text="Edit Profile" ToolTip="Click here to edit your profile"
                                                    CssClass="link_btn" Visible="false" PostBackUrl="~/CandidateAdmin/EditProfile.aspx">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="HomeHeaderControl_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                                        EnableViewState="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:Panel ID="HomeHeaderControl_forgetPasswordPanel" runat="server" Style="display: none"
    CssClass="popupcontrol_forget_password">
    <uc1:ForgotPassword ID="HomeHeaderControl_forgotPassword" runat="server" />
</asp:Panel>
<ajaxToolKit:ModalPopupExtender ID="HomeHeaderControl_forgotPassword_ModalpPopupExtender"
    runat="server" TargetControlID="HomePage_forgotPasswordLinkButton" PopupControlID="HomeHeaderControl_forgetPasswordPanel"
    BackgroundCssClass="modalBackground">
</ajaxToolKit:ModalPopupExtender>

<ajaxToolKit:ModalPopupExtender ID="HomeHeaderControl_logoutLimitedUserModalPopUpExtender"
    runat="server" PopupControlID="HomeHeaderControl_logoutLimitedUserPanel"
    TargetControlID="HomeHeaderControl_logoutLimitedUserPopupModalButton" BackgroundCssClass="modalBackground">
</ajaxToolKit:ModalPopupExtender>
<asp:Panel ID="HomeHeaderControl_logoutLimitedUserPanel" runat="server" Style="display: none"
    Height="220px" CssClass="popupcontrol_confirm">
    <uc3:ConfirmMsgControl ID="HomeHeaderControl_logoutLimitedUserConfirmMessageControl"
        runat="server" Message="You have only limited access. You will not be allowed to login again, unless you have any test or interview associated. If required you can take a copy of the results(if shown to you). Are you sure you want to logout?" Title="Warning"
        Type="LogoutYesNo" OnOkClick="HomeHeaderControl_logoutLimitedUserOKPopupClick"
        OnCancelClick="HomeHeaderControl_logoutLimitedUserCancelPopupClick" />
</asp:Panel>
<div id="HomeHeaderControl_logoutLimitedHiddenDIV" runat="server" style="display: none">
    <asp:Button ID="HomeHeaderControl_logoutLimitedUserPopupModalButton" runat="server" />
</div>

