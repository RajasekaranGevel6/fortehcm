﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MenuControl.cs
// File that represents the class that defines the user interface layout and 
// functionalities for the menu control

#endregion Header

#region Directives

using System;
using System.Web.UI;

using Forte.HCM.Trace;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.Candidate.CommonControls
{
    /// <summary>
    /// A class that defines the user interface layout and functionalities
    /// for the menu control.
    /// </summary>
    public partial class MenuControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that is called when the 'candidate home' menu control is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will launch the 'candidate home' or 'activity home' page based
        /// on the limited user type flag.
        /// </remarks>
        protected void MenuControl_candidateHomeLinkButton_Click
            (object sender, EventArgs e)
        {
            if (Session["USER_DETAIL"] == null)
            {
                Response.Redirect("~/SignIn.aspx", false);
            }
            else
            {
                Response.Redirect("~/Dashboard.aspx", false);
            }
        }

        /// <summary>
        /// Method that is called when the 'adaptive test summary' menu control
        /// is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will launch the 'adaptive test summary' page.
        /// </remarks>
        protected void MenuControl_testRecommendationsLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/AdaptiveTestSummary.aspx?m=1", false);
        }

        /// <summary>
        /// Method that is called when the 'my tests' menu control is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will launch the 'my tests' page.
        /// </remarks>
        protected void MenuControl_myTestsLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/MyTests.aspx?m=2", false);
        }

        /// <summary>
        /// Method that is called when the 'my interviews' menu control is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will launch the 'my interviews' page.
        /// </remarks>
        protected void MenuControl_myInterviewsLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("~/InterviewCenter/MyInterviews.aspx?m=3", false);
        }

        /// <summary>
        /// Method that is called when the 'my credits' menu control is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will launch the 'my credits' page.
        /// </remarks>
        protected void MenuControl_myCreditsLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/MyCredits.aspx?m=4", false);
        }

        /// <summary>
        /// Method that is called when the 'search test' menu control is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will launch the 'search test' page.
        /// </remarks>
        protected void MenuControl_searchTestLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/CandidateSearchTest.aspx?m=5&parentpage=CAND_HOME", false);
        }

        /// <summary>
        /// Method that is called when the 'edit resume' menu control is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event args.
        /// </param>
        /// <remarks>
        /// This will launch the 'edit resume' page.
        /// </remarks>
        protected void MenuControl_editResumeLinkButton_Click
            (object sender, EventArgs e)
        {
            Response.Redirect("~/ResumeRepository/ResumeEditor.aspx?m=6&parentpage=CAND_HOME", false);
        }
    }
}