﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestConductorControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TestConductorControl" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="test_con_body_bg" valign="top">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td class="test_con_header_bg">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="test_con_title">
                                </td>
                                <td align="right">
                                    <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                        <tr>
                                            <td class="test_con_title_text" align="right">
                                                Time Remaining : 00:29:52
                                            </td>
                                            <td class="test_con_title_text" style="width: 5%" align="center">
                                                |
                                            </td>
                                            <td align="right" style="width: 11%">
                                                <asp:LinkButton ID="TestConductor_quitLinkButton" runat="server" Text="Quit Test"
                                                    CssClass="test_con_title_linkbutton"></asp:LinkButton>
                                            </td>
                                            <td class="test_con_title_text" style="width: 5%" align="center">
                                                |
                                            </td>
                                            <td class="test_con_help_button">
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="test_con_header_text">
                                    C# Framework
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle">
                                    <table width="750px" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="width: 20px">
                                            </td>
                                            <td style="width: 460px">
                                                <table width="90%" cellpadding="0" cellspacing="5" border="0" align="center">
                                                    <tr>
                                                        <td class="test_con_progress_text_bold">
                                                            Progress:
                                                        </td>
                                                        <td class="test_con_progress_bar">
                                                        </td>
                                                        <td class="test_con_progress_text">
                                                            Question 18 of 40
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="test_con_question_icon">
                                                                    </td>
                                                                    <td class="test_con_question_text">
                                                                        Which one of the following code samples translates the above Win32 API call to its
                                                                        C# equivalent?
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <table width="100%" cellpadding="3" cellspacing="3" border="0">
                                                                            <tr>
                                                                                <td class="test_con_label_text">
                                                                                    question 1
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="test_con_label_text">
                                                                                    question 2
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="test_con_label_text">
                                                                                    question 3
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="test_con_label_text">
                                                                                    question 4
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="test_con_label_text">
                                                                                    question 5
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="test_con_line">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td class="test_con_button">
                                                                                                Submit Answer
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:LinkButton ID="TestConductor_skipLinkButton" runat="server" Text="Skip" CssClass="link_button"></asp:LinkButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 270px" align="right">
                                                <div style="float: right;">
                                                    <table>
                                                        <tr>
                                                            <td class="test_con_infobg" valign="middle">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 30%">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="test_con_label_text">
                                                                            Time elapsed for question
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="test_con_textbox_green">
                                                                            00:01:15
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="test_con_label_text">
                                                                            Time Elapsed for Test
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="test_con_textbox_orange">
                                                                            00:31:18
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="test_con_label_text">
                                                                            Time Remaining
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td class="test_con_textbox_blue">
                                                                            00:29:42
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="width: 30%">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="test_con_label_text">
                                                                                        Questions answered
                                                                                    </td>
                                                                                    <td style="width: 2%">
                                                                                    </td>
                                                                                    <td class="test_con_questions_answered">
                                                                                        16
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="3" class="test_con_dotline">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="test_con_label_text">
                                                                                        Questions skipped
                                                                                    </td>
                                                                                    <td style="width: 2%">
                                                                                    </td>
                                                                                    <td class="test_con_questions_skipped">
                                                                                        02
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="3" class="test_con_dotline">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td class="test_con_label_text">
                                                                                        Questions remaining
                                                                                    </td>
                                                                                    <td style="width: 2%">
                                                                                    </td>
                                                                                    <td class="test_con_questions_remaining">
                                                                                        22
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="float: right; visibility: hidden;">
                                                    <table>
                                                        <tr>
                                                            <td class="test_con_infodoc">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 8px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="vertical-align: bottom">
                                        <table align="center">
                                            <tr>
                                                <td class="test_con_footer_text">
                                                    © 2010. Forte HCM. All Rights Reserved.
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
