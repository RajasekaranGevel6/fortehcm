﻿
#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ConfirmMsgControl.cs
// File that represents the confirmation dialog which performs a task 
// based on the button click.

#endregion Header

#region Directives

using System;
using System.Web.UI;

using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using System.Web.UI.WebControls;
using Forte.HCM.Support;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// The class that represents the layout for confirm message popup
    /// to across all the pages. Based on button click, the respective
    /// event handler will be performed. 
    /// </summary>
    public partial class ConfirmMsgControl : UserControl
    {
        // This event handler declaration for confirm message control
        public delegate void ConfirmMessageThrownDelegate(object sender, ConfirmMessageEventArgs e);

        #region Public Properties

        // This property allows for a way to add child controls to the user control on the page
        // Any controls added to it are added to the place holder in the OnInit method below
        public Control ContentTemplate { get; set; }

        /// <summary>
        /// This property helps to show the buttons in the popup
        /// based on the button type. 
        /// </summary>
        public MessageBoxType Type
        {
            set
            {
                if (value == MessageBoxType.YesNo)
                {
                    ConfirmMsgControl_messageDiv.Style.Add("height", "50px");
                    ConfirmMsgControl_okButton.Visible = false;
                    ConfirmMsgControl_yesButton.Visible = true;
                    ConfirmMsgControl_noButton.Visible = true;
                }
                else if (value == MessageBoxType.LogoutYesNo)
                {
                    ConfirmMsgControl_messageDiv.Style.Add("height", "75px");
                    ConfirmMsgControl_okButton.Visible = false;
                    ConfirmMsgControl_yesButton.Visible = true;
                    ConfirmMsgControl_noButton.Visible = true;
                }
                else if (value == MessageBoxType.Ok)
                {
                    ConfirmMsgControl_messageDiv.Style.Add("height", "100px");
                    ConfirmMsgControl_yesButton.Visible = false;
                    ConfirmMsgControl_noButton.Visible = false;
                    ConfirmMsgControl_okButton.Visible = true;
                }
                else if (value == MessageBoxType.OkSmall)
                {
                    ConfirmMsgControl_yesButton.Visible = false;
                    ConfirmMsgControl_noButton.Visible = false;
                    ConfirmMsgControl_okButton.Visible = true;
                }
                else if (value == MessageBoxType.YesNoSmall)
                {
                    ConfirmMsgControl_yesButton.Visible = false;
                    ConfirmMsgControl_noButton.Visible = false;
                    ConfirmMsgControl_okButton.Visible = true;
                }
                else if (value == MessageBoxType.EmailConfirmType)
                {
                    ConfirmMsgControl_yesButton.Visible = false;
                    ConfirmMsgControl_noButton.Visible = false;
                    ConfirmMsgControl_okButton.Visible = false;
                    ConfirmMsgControl_emailAttachmentButton.Visible = true;
                    ConfirmMsgControl_emailContentButton.Visible = true;
                }
                else if (value == MessageBoxType.ClientControl)
                {
                    ConfirmMsgControl_yesButton.Width = 100;
                    ConfirmMsgControl_okButton.Width = 120;
                    ConfirmMsgControl_noButton.Width = 80;
                    ConfirmMsgControl_yesButton.Text = "Add Department";
                    ConfirmMsgControl_okButton.Text = "Add Contact";
                    ConfirmMsgControl_noButton.Text = "Cancel";
                    ConfirmMsgControl_yesButton.Visible = true;
                    ConfirmMsgControl_noButton.Visible = true;
                    ConfirmMsgControl_okButton.Visible = true;
                    ConfirmMsgControl_emailAttachmentButton.Visible = false;
                    ConfirmMsgControl_emailContentButton.Visible = false;

                    ConfirmMsgControl_yesButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT_DEPARTMENT;
                    ConfirmMsgControl_okButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT_CONTACT;

                }
                else if (value == MessageBoxType.DepartmentControl)
                {
                    ConfirmMsgControl_yesButton.Visible = false;
                    ConfirmMsgControl_okButton.Width = 120;
                    ConfirmMsgControl_noButton.Width = 80;
                    ConfirmMsgControl_okButton.Text = "Add Contact";
                    ConfirmMsgControl_noButton.Text = "Cancel";
                    ConfirmMsgControl_noButton.Visible = true;
                    ConfirmMsgControl_okButton.Visible = true;
                    ConfirmMsgControl_emailAttachmentButton.Visible = false;
                    ConfirmMsgControl_emailContentButton.Visible = false;
                    ConfirmMsgControl_okButton.CommandName = Constants.ClientManagementConstants.ADD_CLIENT_CONTACT;
                }
                else if (value == MessageBoxType.DeleteContact)
                {
                    ConfirmMsgControl_yesButton.Visible = true;
                    ConfirmMsgControl_okButton.Width = 30;
                    ConfirmMsgControl_noButton.Width = 30;
                    ConfirmMsgControl_yesButton.Width = 30;
                    ConfirmMsgControl_okButton.Visible = false;
                    ConfirmMsgControl_noButton.Visible = true;
                    ConfirmMsgControl_emailAttachmentButton.Visible = false;
                    ConfirmMsgControl_emailContentButton.Visible = false;
                    ConfirmMsgControl_okButton.CommandName = Constants.ClientManagementConstants.DELETE_CONTACT;
                }
            }
        }

        /// <summary>
        /// Property that accepts a title string from the various pages.
        /// </summary>
        public string Title
        {
            set
            {
                ConfirmMsgControl_titleLiteral.Text = value;
            }
        }

        /// <summary>
        /// Property that accepts a message from various pages.
        /// </summary>
        public string Message
        {
            set
            {
                ConfirmMsgControl_messageLabel.Text = value;
                ConfirmMsgControl_messageLabel.DataBind();
            }
        }

        /// <summary>
        /// Gets the URL from a page to be redirected.
        /// </summary>
        public string RedirectURL
        {
            set
            {
                ConfirmMsgControl_hiddenField.Value = value;
            }
        }

        public int Client_ID
        {
            set
            {
                ConfirmMsgControl_okButton.CommandArgument = value.ToString();
                ConfirmMsgControl_yesButton.CommandArgument = value.ToString();
            }

        }
      
        #endregion Public Properties

        #region Public Events and Delegate

        // The public events must conform to this format
        public delegate void Button_Click(object sender, EventArgs e);
        public delegate void Button_CommandClick(object sender, CommandEventArgs e);

        // These two public events are to be customized as need on the page and will be called
        // by the private events of the buttons in the user control
        public event Button_Click OkClick;
        public event Button_Click CancelClick;

        public event Button_CommandClick CommandEventAdd_Click;
        

        #endregion Public Events and Delegate

        #region Event Handler

        /// <summary>
        /// Event handler will get fired when the YES button is clicked 
        /// in the confirmation popup.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void ConfirmMsgControl_yesButton_Click(object sender, EventArgs e)
        {
            
            if (OkClick != null)
                this.OkClick(sender, e);
        }

        /// <summary>
        /// Event handler will get triggered when the NO button is clicked in the popup.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ConfirmMsgControl_noButton_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        /// <summary>
        /// When the CANCEL button is clicked in the popup, this event 
        /// will get fired.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void ConfirmMsgControl_cancelButton_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
                this.CancelClick(sender, e);
        }

        /// <summary>
        /// This handler fires when the OK button is clicked.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> contains the event data.
        /// </param>
        protected void ConfirmMsgControl_okButton_Click(object sender, EventArgs e)
        {
            if (OkClick != null)
                this.OkClick(sender, e);
            //if (ConfirmMessageThrown != null)
            //{
            //    ConfirmMessageThrown(this, new ConfirmMessageEventArgs(ActionType.Ok));
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            // base.OnInit(e);

            //add the child controls from the public property to the place holder in the panel
            //  this.phMain.Controls.Add(this.ContentTemplate);
        }
        

        #endregion Event Handler

        protected void ConfirmMsgControl_button_Command(object sender, CommandEventArgs e)
        {
            if (CommandEventAdd_Click != null)
                this.CommandEventAdd_Click(sender, e);
        }
    }
}