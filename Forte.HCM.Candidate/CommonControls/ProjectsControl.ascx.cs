﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ProjectsControl.cs
// File that represents the user interface for the project information details

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ProjectsControl : System.Web.UI.UserControl
    {
        #region Custom Event Handler and Delegate                              

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;
        private int _rowCounter;
        #endregion Custom Event Handler and Delegate

        #region Event Handlere                                                 
        
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void ProjectsControl_addDefaultButton_Click(object sender, EventArgs e)
        {
            SetViewState();

            ProjectsControl_listView.DataSource = null;
            if (ViewState["dataSource"] != null)
            {
                this.dataSource = (List<Project>)ViewState["dataSource"];
            }
            else
                this.dataSource = new List<Project>();

            this.dataSource.Add(new Project());

            ViewState["dataSource"] = this.dataSource;
            _rowCounter = this.dataSource.Count;
            ProjectsControl_listView.DataSource = this.dataSource;
            ProjectsControl_listView.DataBind();
            SetDate();

        }

        protected void DeleteRowImagebutton_Click(object sender, ImageClickEventArgs e)
        {
            ListViewDataItem lv = (ListViewDataItem)((ImageButton)(sender)).Parent;
            this.dataSource = (List<Project>)ViewState["dataSource"];
            this.dataSource.RemoveAt(lv.DisplayIndex);
            ProjectsControl_listView.DataSource = dataSource;
            ProjectsControl_listView.DataBind();

            ViewState["dataSource"] = ProjectsControl_listView.DataSource;
            SetDate();
        }

        protected void ProjectsControl_listView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Button ProjectsControl_addButton = ((Button)e.Item.FindControl
                    ("ProjectsControl_addButton"));
            Button ProjectsControl_deleteButton = ((Button)e.Item.FindControl
                    ("ProjectsControl_deleteButton"));
            TextBox ProjectsControl_projectNameTextBox = ((TextBox)e.Item.FindControl
                    ("ProjectsControl_projectNameTextBox"));
            ((Panel)e.Item.FindControl("ProjectsControl_projectPanel")).GroupingText = 
                    "Project " + ((e.Item as ListViewDataItem).DisplayIndex + 1).ToString();

            ProjectsControl_addButton.Visible = false;
            ProjectsControl_deleteButton.Visible = false;

            if (_rowCounter == ((e.Item as ListViewDataItem).DisplayIndex + 1))
            {
                ProjectsControl_projectNameTextBox.Focus();
                ProjectsControl_addButton.Visible = true;
                if (_rowCounter != 1)
                    ProjectsControl_deleteButton.Visible = true;
            }
        }

        protected void ProjectsControl_listView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            SetViewState();
            SetDate();
            if (e.CommandName == "deleteProject")
            {
                Button deleteRowImagebutton = (Button)e.Item.FindControl("ProjectsControl_deleteButton");
                TextBox txtRowIndex = (TextBox)e.Item.FindControl("ProjectControl_deleteRowIndex");
                
                ProjectControl_deletedRowHiddenField.Value = txtRowIndex.Text;
                ProjectControl_okPopUpClick(deleteRowImagebutton, new EventArgs());
                
            }
            else if (e.CommandName == "addProject")
            {
                Button ProjectsControl_addButton = (Button)e.Item.FindControl("ProjectsControl_addButton");
                ProjectsControl_addDefaultButton_Click(ProjectsControl_addButton, new EventArgs());
            }
        }

        protected void ProjectControl_okPopUpClick(object sender, EventArgs e)
        {
            int intDeletedRowIndex = Convert.ToInt32(ProjectControl_deletedRowHiddenField.Value);
            if (intDeletedRowIndex != 0)
            {
                this.dataSource = (List<Project>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intDeletedRowIndex - 1);
            }
            else
            {
                int intLastRecord = ProjectsControl_listView.Items.Count - 1;
                this.dataSource = (List<Project>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intLastRecord);
            }
            _rowCounter = dataSource.Count;
            ProjectsControl_listView.DataSource = dataSource;
            ProjectsControl_listView.DataBind();
            SetViewState();
            SetDate();

            // Fire the message event
            if (ControlMessageThrown != null)
                ControlMessageThrown(this, new ControlMessageEventArgs("Project detail deleted successfully", MessageType.Success));
        }

        #endregion Event Handlers

        #region Public Properties                                              
        
        public string SetLocation(object project)
        {
            Project oProject = project as Project;
            Location oLocation = oProject.ProjectLocation;
            string location = "";
            if (oLocation != null)
            {
                location = (oLocation.City != null) ? oLocation.City : "";
                location += (oLocation.State != null) ? "," + oLocation.State : "";
                location += (oLocation.Country != null) ? "," + oLocation.Country : "";
                location = location.StartsWith(",") ? location.Substring(1) : location;
            }

            return location;
        }
       
        public string SetRole(object project)
        {
            Project oProject = project as Project;
            if (oProject.Role == null || oProject.Role == "")
                return "Role";
            else
                return oProject.Role;
        }

        public List<Project> DataSource
        {
            set
            {
                if (value == null)
                {
                    ProjectsControl_addDefaultButton.Visible = true;
                    return;
                }
                // Set values into controls.
                ProjectsControl_addDefaultButton.Visible = false;
                _rowCounter = value.Count;
                ProjectsControl_listView.DataSource = value;
                ProjectsControl_listView.DataBind();
                ViewState["dataSource"] = value;
            }
            get
            {
                if (ViewState["dataSource"] != null)
                    return (List<Project>)ViewState["dataSource"];
                else
                    return null;
            }
        }

        #endregion Event Handlers

        #region Private Properties                                             
        
        private List<Project> dataSource;
        
        #endregion Private Properties

        #region Protected Methods                                              

        protected void SetViewState()
        {

            List<Project> oProjectList = null;
            int intProjCnt = 0;
            foreach (ListViewDataItem item in ProjectsControl_listView.Items)
            {
                if (oProjectList == null)
                {
                    oProjectList = new List<Project>();
                }


                Project oProject = new Project();
                Location oLocation = new Location();

                TextBox txtProjName = (TextBox)item.FindControl("ProjectsControl_projectNameTextBox");
                TextBox txtProjDesc = (TextBox)item.FindControl("ProjectsControl_projectDescTextBox");
                TextBox txtProjRole = (TextBox)item.FindControl("ProjectsControl_roleTextBox");
                TextBox txtProjPosition = (TextBox)item.FindControl("ProjectsControl_positionTextBox");
                TextBox txtProjStartDate = (TextBox)item.FindControl("ProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ProjectsControl_endDtTextBox");
                TextBox txtProjLocation = (TextBox)item.FindControl("ProjectsControl_locationTextBox");
                TextBox txtProjEnvironment = (TextBox)item.FindControl("ProjectsControl_environmentTextBox");
                TextBox txtProjClientName = (TextBox)item.FindControl("ProjectsControl_clientNameTextBox");
                TextBox txtProjClientIndustry = (TextBox)item.FindControl("ProjectsControl_clientIndustryTextBox");
                intProjCnt = intProjCnt + 1;
                oProject.ProjectId = intProjCnt;
                oProject.ProjectName = txtProjName.Text.Trim();
                oProject.ProjectDescription = txtProjDesc.Text.Trim();
                oProject.Role = txtProjRole.Text.Trim();
                oProject.PositionTitle = txtProjPosition.Text.Trim();

                DateTime dtProjStartDate, dtProjEndDate;
                if ((DateTime.TryParse(txtProjStartDate.Text, out dtProjStartDate)))
                {
                    oProject.StartDate = dtProjStartDate;
                }
                if ((DateTime.TryParse(txtProjEndDate.Text, out dtProjEndDate)))
                {
                    oProject.EndDate = dtProjEndDate;
                }                              
                oLocation.City = txtProjLocation.Text.Trim();
                oProject.ProjectLocation = oLocation;
                oProject.Environment = txtProjEnvironment.Text.Trim();
                oProject.ClientName = txtProjClientName.Text.Trim();
                oProject.ClientIndustry = txtProjClientIndustry.Text.Trim();
                oProjectList.Add(oProject);
            }
            ProjectsControl_listView.DataSource = oProjectList;
            ProjectsControl_listView.DataBind();
            ViewState["dataSource"] = oProjectList;
            ProjectsControl_addDefaultButton.Visible = false;
        }

        protected void SetDate()
        {
            foreach (ListViewDataItem item in ProjectsControl_listView.Items)
            {
                TextBox txtProjStartDate = (TextBox)item.FindControl("ProjectsControl_startDtTextBox");
                TextBox txtProjEndDate = (TextBox)item.FindControl("ProjectsControl_endDtTextBox");
                if (txtProjStartDate.Text == "01/01/0001" || txtProjStartDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjStartDate.Text = "";
                }
                if (txtProjEndDate.Text == "01/01/0001" || txtProjEndDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtProjEndDate.Text = "";
                }
            }
        }

        #endregion Protected Methods
    }
}