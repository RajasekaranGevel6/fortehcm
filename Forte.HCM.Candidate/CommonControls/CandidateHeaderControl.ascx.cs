﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Collections.Generic;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using System.Configuration;

namespace Forte.HCM.UI.CommonControls
{
    public partial class CandidateHeaderControl : UserControl
    {
        public string homeUrl = string.Empty;
        public string helpUrl = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            // To get the host from the url.
            string[] uri = Request.Url.AbsoluteUri.Split('/');
            if (uri.Length > 3)
            {
                if (Session["USER_DETAIL"] as UserDetail != null)
                {
                    // Get user detail.
                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/Dashboard.aspx";   
                }
                else
                {
                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/signin.aspx";
                }
            }

            // Compose help url.
            helpUrl = uri[0] + "/" + uri[1] + "/" + uri[2] +  "/General/CandidateHelp.aspx";

            if (!IsPostBack)
            {
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    CandidatHeaderControl_loginUserNameLabel.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                    CandidateHeaderControl_lastLoginDateTimeLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];

                    UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;
                    if (userDetail.IsOpenID == true)
                        CandidateHeaderControl_editProfileLinkButton.Visible = false;
                    else
                        CandidateHeaderControl_editProfileLinkButton.Visible = true;
                }
                else if (!homeUrl.Contains("signin.aspx"))
                {
                   // Response.Redirect(ConfigurationManager.AppSettings["SITE_URL"], false);
                    string[] uri1 = Request.Url.AbsoluteUri.Split('/');
                    if (uri1.Length > 4)
                    {
                        // Compose home url.

                        Response.Redirect(uri1[0] + "/" + uri1[1] + System.Configuration.ConfigurationManager.AppSettings["SITE_URL"], false);
                    }
                }
                else if (homeUrl.Contains("signin.aspx"))
                {

                }
            }
            else
            {
                if (!Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                {
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    CandidatHeaderControl_loginUserNameLabel.Text = ((UserDetail)Session["USER_DETAIL"]).FirstName;
                    CandidateHeaderControl_lastLoginDateTimeLabel.Text = ((UserDetail)Session["USER_DETAIL"]).LastLogin.GetDateTimeFormats()[16];
                }
            }
        }

        protected void HeaderControl_logoutButton_Click(object sender, EventArgs e)
        {
            //bool isLimited = false;
            //if (Session["USER_DETAIL"] != null)
            //{
            //    isLimited = (Session["USER_DETAIL"] as UserDetail).IsLimited;
            //}

            //if (isLimited)
            //{
            //    CandidateHeaderControl_logoutLimitedUserModalPopUpExtender.Show();
            //}
            //else
            //{
                // Logout the user.
                Session["USER_DETAIL"] = null;

                // Clear cookies.
                try
                {
                    Response.Cookies["FORTEHCM_CANDIDATE_DETAILS"].Expires.AddDays(-1);
                    //Request.Cookies.Remove("FORTEHCM_CANDIDATE_DETAILS");
                    //Response.Cookies.Remove("FORTEHCM_CANDIDATE_DETAILS");
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                Session.Abandon();
                FormsAuthentication.SignOut();
                //FormsAuthentication.RedirectToLoginPage();
               // Response.Redirect(ConfigurationManager.AppSettings["SITE_URL"], false);
            string[] uri = Request.Url.AbsoluteUri.Split('/');
            if (uri.Length > 4)
            {
                // Compose home url.

                Response.Redirect(uri[0] + "/" + uri[1] + System.Configuration.ConfigurationManager.AppSettings["SITE_URL"], false);
            }
            //}
        }

        protected void CandidateHeaderControl_logoutLimitedUserOKPopupClick(object sender, EventArgs e)
        {
            try
            {
                // Logout the user.
                Session["USER_DETAIL"] = null;

                // Clear cookies.
                try
                {
                    Response.Cookies["FORTEHCM_CANDIDATE_DETAILS"].Expires.AddDays(-1);
                    //Request.Cookies.Remove("FORTEHCM_CANDIDATE_DETAILS");
                    //Response.Cookies.Remove("FORTEHCM_CANDIDATE_DETAILS");
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                Session.Abandon();
                FormsAuthentication.SignOut();
                Session["LOGOUT"] = true;
               // Response.Redirect(ConfigurationManager.AppSettings["SITE_URL"], false);
                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 4)
                {
                    // Compose home url.

                    Response.Redirect(uri[0] + "/" + uri[1] + ConfigurationManager.AppSettings["SITE_URL"], false);
                }
                //FormsAuthentication.RedirectToLoginPage();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void CandidateHeaderControl_logoutLimitedUserCancelPopupClick(object sender, EventArgs e)
        {
            try
            {
                // Do nothing.
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
    }
}