﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.

#endregion

#region Directives                                                             
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.DataObjects;
using Forte.HCM.Support;
using System.IO;
#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class that defines the datum and values for the SingleSeriesChartControl.
    /// </summary>
    public partial class SingleSeriesChartControl : UserControl
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that is called when the page is loaded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ////Defines whether has the legend has to display or not
                //SingleSeriesChartControl_chart.Legends
                //    ["SingleSeriesChartControl_legend"].Enabled = false;   
            }            
        }        
        #endregion

        #region Properties                                                     
        /// <summary>
        /// Property used to assign the 
        /// datasource of the chart control
        /// </summary>
        public SingleChartData DataSource
        {
            set
            {
                //Set the chart type of the control
                SingleSeriesChartControl_chart.
                    Series["SingleSeriesChartControl_series"].ChartType = value.ChartType;

                //Set the height of the control 
                SingleSeriesChartControl_chart.Height = Unit.Pixel(value.ChartLength);

                //Set the width of the control
                SingleSeriesChartControl_chart.Width = Unit.Pixel(value.ChartWidth);

                //Assign the datasource of the chart
                SingleSeriesChartControl_chart.DataSource = value.ChartData;

                //Defines whether the chart title has to display or not
                SingleSeriesChartControl_chart.Titles[0].Visible =
                    value.IsDisplayChartTitle;

                //Assign the chart title to the chart
                if (value.IsDisplayChartTitle)
                {
                    SingleSeriesChartControl_chart.
                        Titles["SingleSeriesChartControl_title"].Text = value.ChartTitle;
                }

                //Defines whether the axis title has to display or not
                if (value.IsDisplayAxisTitle)
                {
                    SingleSeriesChartControl_chart.ChartAreas
                        ["SingleSeriesChartControl_chartArea"].AxisX.Title = value.XAxisTitle;

                    SingleSeriesChartControl_chart.ChartAreas
                        ["SingleSeriesChartControl_chartArea"].AxisY.Title = value.YAxisTitle;
                }

                //Assign the datasource for the chart control
                SingleSeriesChartControl_chart.Series
                    ["SingleSeriesChartControl_series"].Points.DataBindXY
                    (value.ChartData, "ChartXValue", value.ChartData, "ChartYValue");

                //Defines whether has the legend has to display or not
                SingleSeriesChartControl_chart.Legends
                    ["SingleSeriesChartControl_legend"].Enabled = value.IsDisplayLegend;

                if (!value.IsDisplayLegend)
                {
                    //If the legend is not displayed then changed the chart area width to 100
                    SingleSeriesChartControl_chart.ChartAreas
                        ["SingleSeriesChartControl_chartArea"].Position.Width = 100;

                    //change the x axis label  angle to - 90 for vertical alignment of label
                    SingleSeriesChartControl_chart.ChartAreas
                       ["SingleSeriesChartControl_chartArea"].AxisX.LabelStyle.Angle = -90;
                }

                //Assign the tool tip for the chart series point
                SingleSeriesChartControl_chart.Series
                    ["SingleSeriesChartControl_series"].ToolTip = "#VAL";

                //Defines whether the value has to be shown as label in chart
                SingleSeriesChartControl_chart.Series
                    ["SingleSeriesChartControl_series"].IsValueShownAsLabel = value.IsShowLabel;

                //Assign the value in the point in the series
                if (value.IsShowLabel)
                {
                    SingleSeriesChartControl_chart.
                        Series["SingleSeriesChartControl_series"]["PieLabelStyle"] = "Inside";
                }
                if ((value.ChartType == SeriesChartType.Pie) && (!value.IsShowLabel))
                {
                    //Disabled the values in the chart 
                    SingleSeriesChartControl_chart.
                       Series["SingleSeriesChartControl_series"]["PieLabelStyle"] = "Disabled";
                }
                if (value.IsChangeSeriesColor)
                {
                    SingleSeriesChartControl_chart.Palette = value.PaletteName;
                }

                if (value.ChartData.Count == 0)
                {
                    SingleSeriesChartControl_chart.ToolTip = string.Empty;
                    return;
                }

                //Get the new session ID
                string sessionId = System.Guid.NewGuid().ToString();

                //Assign the session ID to the session 
                Session[sessionId] = value;

                //Assign the java script to show the zoomed chart for the div
                System.Web.UI.HtmlControls.HtmlGenericControl htmlCtrl = 
                    (System.Web.UI.HtmlControls.HtmlGenericControl)SingleSeriesChartControl_charControl;            
                htmlCtrl.Attributes.Add("onclick",
                    "javascript:return ShowZoomedChart('" + sessionId + "','SingleSeries');");
                
                 
                //SingleSeriesChartControl_charControl.Attributes.Add("onclick",
                //    "javascript:return ShowZoomedChart('" + sessionId + "','SingleSeries');");

                if (!Utility.IsNullOrEmpty(value.ChartImageName))
                {
                    //if (!new FileInfo(Server.MapPath("../chart/") + value.ChartImageName + ".png").Exists)
                        SingleSeriesChartControl_chart.SaveImage(Server.MapPath("../chart/") + value.ChartImageName + ".png", ChartImageFormat.Png);
                }
            }

        } 
        #endregion
    }
}