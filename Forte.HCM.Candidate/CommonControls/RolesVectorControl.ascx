﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RolesVectorControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.RolesVectorControl" Strict="false" %>
<%@ Register Src="ConfirmMsgControl.ascx" TagName="ConfirmMsgControl" TagPrefix="uc1" %>
<%@ Register Src="ConfirmMsgControl.ascx" TagName="ConfirmMsgControl" TagPrefix="uc2" %>
<script type="text/javascript" language="javascript">

    function FillCommandArgument(HiddenId, CntrlValue) {
        if (document.getElementById(HiddenId).value == '')
            document.getElementById(HiddenId).value = CntrlValue;
        return true;
    }
    function CommentsCount(characterLength, clientId) {
        var maxlength = new Number(characterLength);
        if (clientId.value.length > maxlength) {
            clientId.value = clientId.value.substring(0, maxlength);
        }
    }

    // Function that will call when any action is performed
    // on the resumeeditor page and maintain scroll position.
    var xCompetencyVectorPos, yCompetencyVectorPos;
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);
    function BeginRequestHandler(sender, args) {
        xCompetencyVectorPos = $get('<%=RolesVectorControl_competenceGridViewDiv.ClientID %>').scrollLeft;
        yCompetencyVectorPos = $get('<%=RolesVectorControl_competenceGridViewDiv.ClientID %>').scrollTop;
    }
    function EndRequestHandler(sender, args) {
        try {
            $get('<%=RolesVectorControl_competenceGridViewDiv.ClientID %>').scrollLeft = xCompetencyVectorPos;
            $get('<%=RolesVectorControl_competenceGridViewDiv.ClientID %>').scrollTop = yCompetencyVectorPos;
        }
        catch (err) {
            alert(err.description);
        }
    }

    // Function that invokes re-build confirmation modal pop up extender
    function ConfirmReBuild() {
        $find('RolesVectorControl_CompetencyVector_rebuildVectorModalPopUpExtender').show();
        return false;
    }
</script>
<div id="RolesVectorControl_mainDiv">
    <div style="display: inline; float: left" class="resume_Table_Bg">
        <asp:HiddenField ID="RolesVectorControl_candidateResumeIdHiddenField" runat="server"
            Value="-10" />
        <asp:HiddenField ID="RolesVectorControl_addVectorGroupIdHiddenField" runat="server" />
        <asp:HiddenField ID="RolesVectorControl_addEmptyRowHiddenField" runat="server" Value="-100" />
        <%--<asp:HiddenField ID="RolesVectorControl_deleteRowIndex" runat="server" />--%>
        <asp:UpdatePanel runat="server" ID="RolesVectorControl_updatePanel">
            <ContentTemplate>
                <div id="RolesVectorControl_messages">
                    <asp:Label ID="RolesVectorControl_successMessageLabel" EnableViewState="false" runat="server"></asp:Label>
                    <asp:Label ID="RolesVectorControl_errorMessageLabel" EnableViewState="false" runat="server"></asp:Label>
                </div>
                <div id="RolesVectorControl_addNewVectorDiv" style="height: 45px; padding-top: 20px;">
                    <div style="float: left">
                        <div style="float: left; padding-left: 10px;">
                            <asp:Label ID="RolesVectorControl_groupHeaderLabel" runat="server" Text="Group Name"></asp:Label>
                        </div>
                        <div style="float: left; padding-left: 10px;">
                            <asp:DropDownList ID="RolesVectorControl_vectorGroupDropDownList" runat="server"
                                Width="150px">
                            </asp:DropDownList>
                        </div>
                        <div style="float: left; padding-left: 10px;">
                            <asp:ImageButton ID="RolesVectorControl_addNewVectorImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_add_vector_group.gif"
                                ToolTip="Add Vector Group" OnClick="RolesVectorControl_addNewVectorImageButton_Click" />
                        </div>
                        <div style="float: left; padding-left: 400px;">
                            <asp:LinkButton ID="RolesVectorControl_reBuildVectorsLinkButton" runat="server" Text="Rebuild Vectors"
                                ToolTip="Rebuilds the vectors from the updated project deatils" OnClientClick="return ConfirmReBuild()"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div style="height: 225px; overflow: auto; width: 905px;" id="RolesVectorControl_competenceGridViewDiv"
                    runat="server">
                    <asp:GridView ID="RolesVectorControl_competencyGridView" runat="server" SkinID="sknCompetency"
                        AllowSorting="false" BorderStyle="None" BorderWidth="0" AlternatingRowStyle-BorderStyle="None"
                        RowStyle-BorderStyle="None" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0">
                        <HeaderStyle BackColor="White" BorderColor="White" BorderWidth="0" />
                        <RowStyle BackColor="#A3C0C8" />
                        <AlternatingRowStyle BackColor="#BCD2DC" />
                        <Columns>
                            <asp:TemplateField HeaderText="" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblVectorGroupId" runat="server" Text='<%# Eval("GROUP_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                                <ItemTemplate>
                                    <div runat="server" id="tblGroupName">
                                        <div style="float: left; width: 85px;">
                                            <asp:Label ID="lblVectorGroupName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                        </div>
                                        <div style="float: left; width: 15px;">
                                            <asp:ImageButton ID="AddVectorGroupNameImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_add_vector.gif"
                                                ToolTip="Add Vector" CommandName="edit" CommandArgument='<%# Eval("GROUP_ID") %>' />
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:GridView ID="RolesVectorControl_parametersGridView" runat="server" AutoGenerateColumns="false"
                                        Width="100%" AlternatingRowStyle-BorderStyle="None" RowStyle-BorderStyle="None"
                                        SkinID="sknCompetency">
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#DAE6EB" />
                                        <AlternatingRowStyle BackColor="#BDC9CE" />
                                        <Columns>
                                        </Columns>
                                    </asp:GridView>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="RolesVectorControl_controls">
        <asp:UpdatePanel ID="RolesVectorControl_CompetencyVector_confirmPopUpUpdatPanel"
            runat="server">
            <ContentTemplate>
                <div id="RolesVectorControl_CompetencyVector_hiddenDIV" runat="server" style="display: none">
                    <asp:Button ID="RolesVectorControl_CompetencyVector_hiddenPopupModalButton" runat="server" />
                    <asp:Button ID="RolesVectorControl_CompetencyVector_hiddenCancelPopupModalButton"
                        runat="server" />
                    <asp:Button ID="RolesVectorControl_CompetencyVector_hiddenReBuildVectorPopupModalButton"
                        runat="server" />
                </div>
                <ajaxToolKit:ModalPopupExtender ID="RolesVectorControl_CompetencyVector_saveTestModalPopupExtender"
                    runat="server" PopupControlID="RolesVectorControl_CompetencyVector_saveTestConfirmPopUpPanel"
                    TargetControlID="RolesVectorControl_CompetencyVector_hiddenPopupModalButton"
                    CancelControlID="RolesVectorControl_CompetencyVector_hiddenCancelPopupModalButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="RolesVectorControl_CompetencyVector_saveTestConfirmPopUpPanel" runat="server"
                    Style="display: none; width: 30%; height: 210px; background-position: bottom;
                    background-color: #283033;">
                    <uc1:ConfirmMsgControl ID="RolesVectorControl_CompetencyVector_confirmPopupExtenderControl"
                        runat="server" OnOkClick="RolesVectorControl_CompetencyVector_okPopUpClick" OnCancelClick="RolesVectorControl_CompetencyVector_cancelPopUpClick" />
                </asp:Panel>
                <ajaxToolKit:ModalPopupExtender ID="RolesVectorControl_CompetencyVector_rebuildVectorModalPopUpExtender"
                    BehaviorID="RolesVectorControl_CompetencyVector_rebuildVectorModalPopUpExtender"
                    runat="server" PopupControlID="RolesVectorControl_reBuildVectorConfirmPopUpPanel"
                    TargetControlID="RolesVectorControl_CompetencyVector_hiddenReBuildVectorPopupModalButton"
                    BackgroundCssClass="modalBackground">
                </ajaxToolKit:ModalPopupExtender>
                <asp:Panel ID="RolesVectorControl_reBuildVectorConfirmPopUpPanel" runat="server"
                    Style="display: none; width: 30%; height: 210px; background-position: bottom;
                    background-color: #283033;">
                    <uc2:ConfirmMsgControl ID="RolesVectorControl_reBuildVectorConfirmPopUpExtenderControl"
                        runat="server" OnOkClick="RolesVectorControl_reBuildVectorConfirmPopUpExtenderControl_OkPopUpClick"
                        OnCancelClick="RolesVectorControl_reBuildVectorConfirmPopUpExtenderControl_cancelPopUpClick"
                        Message="Are you sure to rebuild competency vector" Title="Confirmation" Type="YesNo" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
