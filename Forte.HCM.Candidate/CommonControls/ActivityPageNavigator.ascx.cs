﻿
#region Header
// Copyright (C) 2010-2011 Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PageNavigator.ascx.cs
// This class represents the PageNavigator user control.
// This controls helps to navigate through multiple pages of a 
// grid control. This control handle the paging changes within
// the control, and fire necessary events. Based on these events
// grids can produce the results.
//
#endregion

#region Directives

using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Drawing;
using System.Collections;
using System.Web.Security;
using System.Configuration;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;

using Forte.HCM.Support;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ActivityPageNavigator : System.Web.UI.UserControl
    {
        /// <summary>
        /// A <seealso cref="int"/> that holds the current page set.
        /// </summary>
        /// <remarks>
        /// For eg. page numbers 1 to 10 are page set 1, 11 to 20 are 
        /// page set 2, etc.
        /// </remarks>
        private int currentPageSet = 1;

        /// <summary>
        /// A <seealso cref="int"/> constants that holds the number of page 
        /// number links in the page.
        /// </summary>
        /// <remarks>
        /// This is by default 10 (for eg. 1 to 10).
        /// </remarks>
        private const int PAGE_LINKS = 10;

        /// <summary>
        /// A <seealso cref="int"/> constant the holds the default page size.
        /// </summary>
        /// <remarks>
        /// This is by default 10.
        /// </remarks>
        private const int DEFAULT_PAGE_SIZE = 10;

        /// <summary>
        /// A collection of <seealso cref="int"/> that holds the page number 
        /// caption list.
        /// </summary>
        private List<int> pageNumberCaption;

        /// <summary>
        /// A collection of <seealso cref="LinkButton"/> that holds the link 
        /// buttons list.
        /// </summary>
        private List<LinkButton> pageLinkButtons;

        /// <summary>
        /// A collection of <seealso cref="LinkButton"/> that holds the separator 
        /// labels list.
        /// </summary>
        private List<Label> separatorLabels;

        /// <summary>
        /// A <seealso cref="PageNavigator"/> that holds the reference to the
        /// page navigator control. 
        /// </summary>
        /// <remarks>
        /// When implementing in pages, usually 2 paging controls are used. So
        /// the state of one paging control must be reflected in another. This
        /// will associate with another control and copy the current state of
        /// control to another.
        /// </remarks>
        private ActivityPageNavigator associatedControl;

        /// <summary>
        /// A <seealso cref="delegate"/> that delegates the page number click event.
        /// </summary>
        /// <param name="sender">
        /// Sender of the event.
        /// </param>
        /// <param name="e">
        /// A <seealso cref="PageNumberEventArgs"/> that holds the event data.
        /// </param>
        public delegate void PageNumberClickEventHandler(object sender, PageNumberEventArgs e);
        
        /// <summary>
        /// A <seealso cref="event"/> that describes the page number click event.
        /// </summary>
        public event PageNumberClickEventHandler PageNumberClick;

        /// <summary>
        /// Represents the method that is called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// Sender of the event.
        /// </param>
        /// <param name="e">
        /// A <seealso cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ActivityPageNavigator_currentPageSetHidden.Value != null)
                currentPageSet = Convert.ToInt32(ActivityPageNavigator_currentPageSetHidden.Value);

            // Set paging properties.
            SetPagingProperties();

            // Set current state to the associated control. 
            if (associatedControl != null)
            {
                // Set paging properties.
                associatedControl.SetPagingProperties();
            }
        }
       
        /// <summary>
        /// Represents the method that adds the page link button and separator 
        /// labels to the list. This helps in setting the control's visibility
        /// during runtime.
        /// </summary>
        private void AddControls()
        {
            pageLinkButtons = new List<LinkButton>(PAGE_LINKS);

            pageLinkButtons.Add(ActivityPageNavigator_oneLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_twoLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_threeLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_fourLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_fiveLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_sixLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_sevenLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_eightLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_nineLinkButton);
            pageLinkButtons.Add(ActivityPageNavigator_tenLinkButton);

            separatorLabels = new List<Label>(PAGE_LINKS - 1);
            separatorLabels.Add(ActivityPageNavigator_oneSeparatorLabel);
            separatorLabels.Add(ActivityPageNavigator_twoSeparatorLabel);
            separatorLabels.Add(ActivityPageNavigator_threeSeparatorLabel);
            separatorLabels.Add(ActivityPageNavigator_fourSeparatorLabel);
            separatorLabels.Add(ActivityPageNavigator_fiveSeparatorLabel);
            separatorLabels.Add(ActivityPageNavigator_sixSeparatorLabel);
            separatorLabels.Add(ActivityPageNavigator_sevenSeparatorLabel);
            separatorLabels.Add(ActivityPageNavigator_eightSeparatorLabel);
            separatorLabels.Add(ActivityPageNavigator_nineSeparatorLabel);
        }

        /// <summary>
        /// Represents the method that is called when a page number is clicked.
        /// </summary>
        /// <param name="sender">
        /// Sender of the event.
        /// </param>
        /// <param name="e">
        /// A <seealso cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// All link button events are subscribed to this method only. Based on 
        /// the sender caption the button is identified.
        /// </remarks>
        protected void ActivityPageNavigator_PageNumber_Click(object sender, EventArgs e)
        {
            // Get the page number based on the caption.
            int pageNumber = Convert.ToInt32((sender as LinkButton).Text);

            // Create a new event args.
            PageNumberEventArgs eventArgs = new PageNumberEventArgs(pageNumber);

            // Fire the page number click event. This event is subscribed in 
            // pages and when fired, data is repopulated in the grid control.
            if (PageNumberClick != null)
                PageNumberClick(this, eventArgs);

            // Do not proceed if event is cancelled. 
            if (eventArgs.Cancel == true)
            {
                return;
            }

            // Reset the highlighted link button.
            ReSetHighlighed();

            // Reset the highlighted link button in the associated control.
            if (associatedControl != null)
                associatedControl.ReSetHighlighed();

            // Set the currently clicked button highlighted.
            (sender as LinkButton).BorderStyle = BorderStyle.Solid;

            // Set the currently clicked button highlighted on the associated control.
            if (associatedControl != null)
            {
                (associatedControl.FindControl((sender as LinkButton).ID) as LinkButton).
                    BorderStyle = BorderStyle.Solid;
            }
        }

        /// <summary>
        /// Represents the method that is called whenever move first, move previous, 
        /// move next or move last button is clicked.
        /// </summary>
        /// <param name="sender">
        /// Sender of the object.
        /// </param>
        /// <param name="e">
        /// A <seealso cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void ActivityPageNavigator_Move_Click(object sender, EventArgs e)
        {
            switch((sender as LinkButton).CommandName.Trim().ToUpper())
            {
                // Move first.
                case "FIRST":
                    currentPageSet = 1;
                    ActivityPageNavigator_currentPageSetHidden.Value = currentPageSet.ToString();
                    break;

                // Move previous.
                case "PREVIOUS":
                    currentPageSet--;
                    if (currentPageSet == 0)
                        currentPageSet = 1;

                    ActivityPageNavigator_currentPageSetHidden.Value = currentPageSet.ToString();
                    break;

                // Move next.
                case "NEXT":
                    currentPageSet++;
                    ActivityPageNavigator_currentPageSetHidden.Value = currentPageSet.ToString();
                    break;

                // Move last.
                case "LAST":
                    int pages = TotalRecords / (PAGE_LINKS * PageSize);

                    if (TotalRecords - (PAGE_LINKS * PageSize * pages) > 0)
                        pages++;
                    currentPageSet = pages;
                    ActivityPageNavigator_currentPageSetHidden.Value = currentPageSet.ToString();
                    break;
            }

            // Set current state to the associated control.
            if (associatedControl != null)
                associatedControl.ActivityPageNavigator_currentPageSetHidden.Value = currentPageSet.ToString();

            SetPageNumberCaption();
            SetMoveLinkVisibility();
            SetPageNumberLinkVisibility();

            // Reset the border to none.
            ReSetHighlighed();

            // Set the first button highlighted.
            ActivityPageNavigator_oneLinkButton.BorderStyle = BorderStyle.Solid;

            // Fire the page number click event. This event is subscribed in 
            // pages and when fired, data is repopulated in the grid control.
            int pageNumber = Convert.ToInt32(ActivityPageNavigator_oneLinkButton.Text);
            if (PageNumberClick != null)
                PageNumberClick(this, new PageNumberEventArgs(pageNumber));

            // Set current state to the associated control.
             if (associatedControl != null)
             {
                 associatedControl.SetPageNumberCaption();
                 associatedControl.SetMoveLinkVisibility();
                 associatedControl.SetPageNumberLinkVisibility();
                 associatedControl.ReSetHighlighed();
                 associatedControl.ActivityPageNavigator_oneLinkButton.BorderStyle = BorderStyle.Solid;
             }
        }

        /// <summary>
        /// Represents the method that reset the highlighted link button to
        /// default.
        /// </summary>
        /// <remarks>
        /// This method has to be called after every click on page number or
        /// navigation link. Including all page number links, the highlighted
        /// link is set to default. 
        /// </remarks>
        private void ReSetHighlighed()
        {
            foreach (LinkButton linkButton in pageLinkButtons)
                linkButton.BorderStyle = BorderStyle.None;
        }

        /// <summary>
        /// Represents the method that sets the caption for page number link
        /// buttons.
        /// </summary>
        /// <remarks>
        /// This needs to be set whenever move first, move previous, move next
        /// or move last button is clicked.
        /// </remarks>
        private void SetPageNumberCaption()
        {
            // Construct the caption list.
            pageNumberCaption = new List<int>(PAGE_LINKS);
            for (int pageNumber = 1; pageNumber <= PAGE_LINKS; pageNumber++)
                pageNumberCaption.Add(pageNumber);

            // Reconstruct the caption list.
            for (int pageNumber = 1; pageNumber <= PAGE_LINKS; pageNumber++)
                pageNumberCaption[pageNumber - 1] = ((currentPageSet - 1) * PAGE_LINKS) + pageNumber;

            // Set the caption for page number link buttons.
            for (int buttonIndex = 0; buttonIndex < PAGE_LINKS; buttonIndex++)
            {
                pageLinkButtons[buttonIndex].Text = pageNumberCaption[buttonIndex].ToString();

                // Set tool tip.
                pageLinkButtons[buttonIndex].ToolTip = string.Format
                    ("Click here to show activity # {0}", pageNumberCaption[buttonIndex]);
            }
        }

        /// <summary>
        /// Represents the method that sets the visibility status of the page
        /// number link buttons.
        /// </summary>
        /// <remarks>
        /// Page number link buttons are usaully 1 to 10.
        /// </remarks>
        private void SetPageNumberLinkVisibility()
        {
            // Show all link buttons.
            for (int i = 0; i < PAGE_LINKS; i++)
                pageLinkButtons[i].Visible = true;

            // Show all separator labels.
            for (int i = 0; i < PAGE_LINKS - 1; i++)
                separatorLabels[i].Visible = true;

            int recordsPerPageSet = PageSize * PAGE_LINKS;

            int rest = TotalRecords - ((currentPageSet - 1) * recordsPerPageSet);

            if (rest >= (PAGE_LINKS * PageSize) || rest < 0)
                return;

            int totalLinks = rest / PageSize;

            if (rest % PageSize > 0)
                totalLinks++;

            // Hide all the link buttons and separators that are not applicable.
            for (int i = totalLinks; i < PAGE_LINKS; i++)
                pageLinkButtons[i].Visible = false;

            if (totalLinks != 0)
                totalLinks--;

            for (int i = totalLinks; i < PAGE_LINKS - 1; i++)
                separatorLabels[i].Visible = false;
        }

        /// <summary>
        /// Represents the method that sets visibility status of move links.
        /// </summary>
        /// <remarks>
        /// Move links user here are: move first, move previous, move next and
        /// move last.
        /// </remarks>
        private void SetMoveLinkVisibility()
        {
            // Set the move first and move previous link buttons state.
            if (currentPageSet == 1)
            {
                ActivityPageNavigator_moveFirstLinkButton.Visible = false;
                ActivityPageNavigator_movePreviousLinkButton.Visible = false;
            }
            else
            {
                ActivityPageNavigator_moveFirstLinkButton.Visible = true;
                ActivityPageNavigator_movePreviousLinkButton.Visible = true;
            }

            // Set the move last and move next link buttons state.
            int recordsPerPageSet = PageSize * PAGE_LINKS;

            if (TotalRecords <= (currentPageSet * recordsPerPageSet))
            {
                ActivityPageNavigator_moveLastLinkButton.Visible = false;
                ActivityPageNavigator_moveNextLinkButton.Visible = false;
            }
            else
            {
                ActivityPageNavigator_moveLastLinkButton.Visible = true;
                ActivityPageNavigator_moveNextLinkButton.Visible = true;
            }
        }

        /// <summary>
        /// Represents the method that sets the paging properties such as
        /// setting page number link caption and visibility and move link 
        /// visibility.
        /// </summary>
        private void SetPagingProperties()
        {
            AddControls();
            SetPageNumberCaption();
            SetPageNumberLinkVisibility();
            SetMoveLinkVisibility();
        }

        /// <summary>
        /// Represents the property that sets or retrieves the total records.
        /// </summary>
        /// <remarks>
        /// This is maintained in the hidden controls, in order to retain the
        /// values between post backs.
        /// </remarks>
        [Browsable(true)]
        public int TotalRecords
        {
            get
            {
                if (ActivityPageNavigator_totalRecordsHidden.Value != null && ActivityPageNavigator_totalRecordsHidden.Value.Trim().Length != 0)
                    return Convert.ToInt32(ActivityPageNavigator_totalRecordsHidden.Value);
                else
                    return 0;
            }
            set
            {
                ActivityPageNavigator_totalRecordsHidden.Value = value.ToString();

                if (ActivityPageNavigator_currentPageSetHidden.Value != null)
                    currentPageSet = Convert.ToInt32(ActivityPageNavigator_currentPageSetHidden.Value);

                // Set paging properties.
                SetPagingProperties();

                // Set current state to the associated control.
                if (associatedControl != null)
                {
                    associatedControl.ActivityPageNavigator_totalRecordsHidden.Value = value.ToString();

                    if (ActivityPageNavigator_currentPageSetHidden.Value != null)
                        associatedControl.currentPageSet = Convert.ToInt32(ActivityPageNavigator_currentPageSetHidden.Value);

                    // Set paging properties.
                    associatedControl.SetPagingProperties();
                }
            }
        }

        /// <summary>
        /// Represents the property that sets or retrieves the page size.
        /// </summary>
        /// <remarks>
        /// This is maintained in the hidden controls, in order to retain the
        /// values between post backs.
        /// </remarks>
        [Browsable(true)]
        public int PageSize
        {
            get
            {
                if (ActivityPageNavigator_pageSizeHidden.Value != null && ActivityPageNavigator_pageSizeHidden.Value.Trim().Length != 0)
                    return Convert.ToInt32(ActivityPageNavigator_pageSizeHidden.Value);
                else
                    return DEFAULT_PAGE_SIZE;
            }
            set
            {
                ActivityPageNavigator_pageSizeHidden.Value = value.ToString();

                if (ActivityPageNavigator_currentPageSetHidden.Value != null)
                    currentPageSet = Convert.ToInt32(ActivityPageNavigator_currentPageSetHidden.Value);

                // Set paging properties.
                SetPagingProperties();

                // Set current state to the associated control.
                if (associatedControl != null)
                {
                    associatedControl.ActivityPageNavigator_pageSizeHidden.Value = value.ToString();

                    if (ActivityPageNavigator_currentPageSetHidden.Value != null)
                        associatedControl.currentPageSet = Convert.ToInt32(ActivityPageNavigator_currentPageSetHidden.Value);

                    // Set paging properties.
                    associatedControl.SetPagingProperties();
                 }
            }
        }

        /// <summary>
        /// Represents the method that resets the paging control. This has to 
        /// be called when the search button is clicked in the page.
        /// </summary>
        public void Reset()
        {
            ActivityPageNavigator_currentPageSetHidden.Value = "1";
            currentPageSet = 1;

            // Set paging properties.
            SetPagingProperties();

            // Set current state to the associated control. 
            if (associatedControl != null)
            {
                // Set paging properties.
                associatedControl.SetPagingProperties();
            }

            // Highlight first link.
            HighlightFirstLink();
        }

        /// <summary>
        /// Represents the method that will move the selected page to the given
        /// page number.
        /// </summary>
        /// <param name="pageNumber">
        /// A <see cref="int"/> that holds the page number.
        /// </param>
        public void MoveToPage(int pageNumber)
        {
            if (pageNumber == 1)
                currentPageSet = 1;
            else 
                currentPageSet = ((pageNumber - 1) / PAGE_LINKS) + 1;

            ActivityPageNavigator_currentPageSetHidden.Value = currentPageSet.ToString();

            // Set current state to the associated control.
            if (associatedControl != null)
                associatedControl.ActivityPageNavigator_currentPageSetHidden.Value = currentPageSet.ToString();

            SetPageNumberCaption();
            SetMoveLinkVisibility();
            SetPageNumberLinkVisibility();

            // Reset the border to none.
            ReSetHighlighed();

            // Highlight the page number
            pageLinkButtons[(pageNumber - 1) % PAGE_LINKS].BorderStyle = BorderStyle.Solid;

            // Set current state to the associated control.
            if (associatedControl != null)
            {
                associatedControl.SetPageNumberCaption();
                associatedControl.SetMoveLinkVisibility();
                associatedControl.SetPageNumberLinkVisibility();
                associatedControl.ReSetHighlighed();
                associatedControl.pageLinkButtons[(pageNumber - 1) % PAGE_LINKS].BorderStyle = BorderStyle.Solid;
            }
        }

        /// <summary>
        /// Represents the method that sets the first link button highlighted
        /// </summary>
        /// <remarks>
        /// This is applicable when the search button is clicked. During this
        /// time it will reset the existing highlighted button, and make the
        /// first link button highlighted.
        /// </remarks>
        private void HighlightFirstLink()
        {
            // Reset the highlighted link button.
            ReSetHighlighed();

            // Reset the highlighted link button in the associated control.
            if (associatedControl != null)
                associatedControl.ReSetHighlighed();

            // Set the first link button highlighted.
            (FindControl("ActivityPageNavigator_oneLinkButton") as LinkButton).
                BorderStyle = BorderStyle.Solid;

            // Set the first link button highlighted for the associated control.
            if (associatedControl != null)
            {
                (associatedControl.FindControl("ActivityPageNavigator_oneLinkButton") as LinkButton).
                    BorderStyle = BorderStyle.Solid;
            }
        }

        /// <summary>
        /// Represents the property that sets the associated control.
        /// </summary>
        /// <remarks>
        /// This is used to maintain the state of top and bottom paging
        /// controls with the grid implementation.
        /// </remarks>
        [Browsable(true)]
        public ActivityPageNavigator AssociatedControl
        {
            set
            {
                associatedControl = value;
            }
        }
    }
}