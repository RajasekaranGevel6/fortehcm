﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// RolesVectorControl.cs
// File that represents the user interface for Generating a 
// Competency vector for the selected candidate resume id.

#endregion Header

#region Directives                                                             

using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

#endregion

namespace Forte.HCM.UI.CommonControls
{
    public partial class RolesVectorControl : System.Web.UI.UserControl
    {

        #region Declarations                                                   

        // <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the vector table.
        /// </summary>
        private const string VECTORSTABLE_VIEWSTATE = "VECTORTABLE_VIEWSTATE";
        // <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the inner grid group id.
        /// </summary>
        private const string ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE="VECTOR_ID";
        // <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the inner grid edit index.
        /// </summary>
        private const string ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE="EDITINDEX";
        /// <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer delete grid view.
        /// </summary>
        private const string ROLESVECTORCONTROL_DELETEGRID_VIEWSTATE = "DELETEGRID";
        /// <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the inner grid pre render event.
        /// </summary>
        private int GridViewBoundCount = 2;
        /// <summary>
        /// A<see cref="string"/> constant that hold the value for the header
        /// color.
        /// </summary>
        private const string HEADER_COLOR = "#DAE6EB";
        /// <summary>
        /// A<see cref="string"/> constant that hold the value for
        /// header alternate color/
        /// </summary>
        private const string HEADER_ALTERNATE_COLOR = "#BDC9CE";
        /// <summary>
        /// A<see cref="Enum"/>that contains the dataset 
        /// table names which we will save in session
        /// </summary>
        private enum DataSetTableNames
        {
            MainTable = 0,
            CompetencyParameterTable = 1,
            VectorGroup = 2
        }
        /// <summary>
        /// A<see cref="Enum"/>that contains the status of the
        /// row
        /// </summary>
        private enum VectorStatus
        {
            Delete = 0,
            ExistingUpdated = 1,
            Existing = 2,
            NewRow = 3,
            NewRowVector = 4
        }
        // <summary>
        /// A<see cref="string"/> constans that holds the key for the viewstate
        /// which refer to the inner grid data table.
        /// </summary>
        private const string ROLESVECTOR_VECTORGRID_VIEWSTATE = "VECTORGRID_VIEWSTATE";

        #endregion Declarations

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loading.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE] == null)
                    ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE] = -10;
                if (ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE] == null)
                    ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE] = -2;
                if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) > 0)
                {
                    BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value),
                             Convert.ToInt32(ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE]),
                             Convert.ToInt32(ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE]), -10, false);
                }
                else if (RolesVectorControl_addVectorGroupIdHiddenField.Value != "")
                    BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value),
                        -10, -2, Convert.ToInt32(RolesVectorControl_addVectorGroupIdHiddenField.Value), false);
                else
                    BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value),
                        Convert.ToInt32(ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE]),
                        Convert.ToInt32(ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE]), -10, false);
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        /// <summary>
        /// Handler method that will be called when the 
        /// add new vector image button clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void RolesVectorControl_addNewVectorImageButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) > 0)
                {
                    CallBindMethodAsEmptyRowAdded();
                    return;
                }
                if (RolesVectorControl_vectorGroupDropDownList.SelectedIndex == 0)
                {
                    RolesVectorControl_errorMessageLabel.Text = Resources.HCMResource.
                        ResumeParser_CompetencyVector_InvalidVectorGroupSelect;
                    return;
                }
                BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value) ,- 10, -2, -10, true);
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        /// <summary>
        /// Handler method that will be called when the grid view edit button  
        /// has clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="GridViewEditEventArgs"/> that holds the 
        /// gridview edit event data.
        /// </param>
        protected void RolesVectorControl_InnerGrid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) > 0)
                {
                    CallBindMethodAsEmptyRowAdded();
                    return;
                }
                GridView EditGridView = (GridView)sender;
                ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE] = ((ImageButton)EditGridView.Rows[e.NewEditIndex].
                    FindControl("EditImageButton")).CommandArgument.Split('|')[0];
                ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE] = e.NewEditIndex;
                RolesVectorControl_addVectorGroupIdHiddenField.Value = "";
                RolesVectorControl_addEmptyRowHiddenField.Value = "-100";
                BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value),
                    Convert.ToInt32(ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE]),
                    Convert.ToInt32(ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE]), -10, false);
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        /// <summary>
        /// Handler method that will be called when the grid view update  
        /// button has clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewUpdateEventArgs"/> that holds 
        /// the gridview update event data.
        /// </param>
        protected void RolesVectorControl_InnerGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridView EditGridView = (GridView)sender;
                if (!UpdateRow(ref EditGridView, e.RowIndex,
                        Convert.ToInt32(((TextBox)EditGridView.Rows[e.RowIndex].FindControl("ROW_ID_TextBox")).Text) - 1,
                        ((RolesVectorControl_addVectorGroupIdHiddenField.Value == "") ? true : false),
                        ((DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE]).Tables[DataSetTableNames.MainTable.ToString()]))
                    return;
                RolesVectorControl_addEmptyRowHiddenField.Value = "-100";
                RolesVectorControl_addVectorGroupIdHiddenField.Value = "";
                RolesVectorControl_successMessageLabel.Text =
                    Resources.HCMResource.ResumeParser_CompetencyVector_RowValueUpdatedSuccessfully;
                ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE] = -10;
                ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE] = -2;
                BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value),
                 Convert.ToInt32(ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE]),
                 Convert.ToInt32(ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE]), -10, false);
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        /// <summary>
        /// Handler method that will be called when the grid view   
        /// edit cancel button has clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewCancelEditEventArgs"/> that 
        /// holds the gridview cancel edit event data.
        /// </param>
        protected void RolesVectorControl_InnerGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) > 0)
                {
                    DataTable dtGridMainTable = null;
                    DataTable dtVectorTable = null;
                    dtGridMainTable = ((DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE]).Tables[DataSetTableNames.MainTable.ToString()];
                    dtVectorTable = ((DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE]).Tables[DataSetTableNames.VectorGroup.ToString()];
                    RolesVectorControl_addEmptyRowHiddenField.Value = "-100";
                    dtGridMainTable.Rows.RemoveAt(dtGridMainTable.Rows.Count - 1);
                    CheckForGroup(ref dtVectorTable, dtGridMainTable, Convert.ToInt32(RolesVectorControl_addVectorGroupIdHiddenField.Value));
                }
                RolesVectorControl_addVectorGroupIdHiddenField.Value = "";
                RolesVectorControl_addEmptyRowHiddenField.Value = "-100";
                ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE] = -10;
                ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE] = -2;
                BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value), -10, -2, -10, false);
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        /// <summary>
        /// Handler method that will be called when the grid view   
        /// row delete button has clicked.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A<see cref="GridViewDeleteEventArgs"/> that holds
        /// the grid view delete events data.
        /// </param>
        protected void RolesVectorControl_InnerGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) > 0)
                {
                    CallBindMethodAsEmptyRowAdded();
                    return;
                }
                GridView DeleteGridView = (GridView)sender;
                ViewState[ROLESVECTORCONTROL_DELETEGRID_VIEWSTATE] =
                    ((Label)DeleteGridView.Rows[e.RowIndex].FindControl("ROW_ID_Label")).Text + "," +
                    ((Label)DeleteGridView.Rows[e.RowIndex].FindControl("GROUP_ID_LABEL")).Text;
                RolesVectorControl_CompetencyVector_confirmPopupExtenderControl.Title = "Warning";
                RolesVectorControl_CompetencyVector_confirmPopupExtenderControl.Message = "Are you sure want to delete details?";
                RolesVectorControl_CompetencyVector_confirmPopupExtenderControl.Type = MessageBoxType.YesNo;
                RolesVectorControl_CompetencyVector_saveTestModalPopupExtender.Show();
                DeleteGridView = null;
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        /// <summary>
        /// Event Handling for the common ConfirmMessage Popup Extenter Yes and 
        /// OK Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Ok/Yes the Event will be raised.
        /// </remarks>
        protected void RolesVectorControl_CompetencyVector_okPopUpClick(object sender, EventArgs e)
        {
            try
            {
                string DeleteRowGroup = ViewState[ROLESVECTORCONTROL_DELETEGRID_VIEWSTATE].ToString();
                if (DeleteRowGroup == "" || DeleteRowGroup.Split(',').Length == 0)
                    return;
                RolesVectorControl_addEmptyRowHiddenField.Value = "-100";
                DeleteRow(((DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE]).Tables[DataSetTableNames.MainTable.ToString()]
                    , Convert.ToInt32(DeleteRowGroup.Split(',')[0]) - 1,
                ((DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE]).Tables[DataSetTableNames.VectorGroup.ToString()],
                    Convert.ToInt32(DeleteRowGroup.Split(',')[1]));
                ViewState[ROLESVECTORCONTROL_DELETEGRID_VIEWSTATE] = null;
                ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE] = -10;
                ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE] = -2;
                RolesVectorControl_addVectorGroupIdHiddenField.Value = "";
                BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value),
                    -10, -2, -10, false);
                RolesVectorControl_successMessageLabel.Text =
                    Resources.HCMResource.ResumeParser_CompetencyVector_RowDeleteSuccessfully;
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        /// <summary>
        ///  Event Handling for the common ConfirmMessage Popup Extenter Cancel and 
        ///  Close Button event.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// If we click the Cancel/Close button Event will be raised.
        /// </remarks>
        protected void RolesVectorControl_CompetencyVector_cancelPopUpClick(object sender, EventArgs e)
        {
            try
            {
                ViewState[ROLESVECTORCONTROL_DELETEGRID_VIEWSTATE] = null;
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        protected void RolesVectorControl_reBuildVectorConfirmPopUpExtenderControl_OkPopUpClick
            (object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) > 0)
                {
                    CallBindMethodAsEmptyRowAdded();
                    return;
                }
                ReBuildCompetencyVector();
                BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value),
                        -10, -2, -10, false);
                
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        protected void RolesVectorControl_reBuildVectorConfirmPopUpExtenderControl_cancelPopUpClick
            (object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Handler method that will be called when the 
        /// grid view control is pre rendering to the client.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void RolesVectorControl_InnerGridView_PreRender(object sender, EventArgs e)
        {
            TextBox textBox = null;
            AjaxControlToolkit.FilteredTextBoxExtender filteredTextBoxExtender = null;
            try
            {
                string columnColor = "#77CDE9";
                string alternateColoumnColor = "#BEE562";
                GridView PreRenderGridView = (GridView)sender;
                for (int k = 0; k < PreRenderGridView.Rows.Count; k++)
                {
                    //
                    if (PreRenderGridView.Rows[k].RowState.CompareTo(DataControlRowState.Edit) >= 0)
                    {
                        for (int CellCount = 0; CellCount < PreRenderGridView.Rows[k].Cells.Count; CellCount++)
                        {
                            if (!PreRenderGridView.Rows[k].Cells[CellCount].HasControls())
                                continue;
                            if (PreRenderGridView.Rows[k].Cells[CellCount].Controls[0].GetType() != typeof(HtmlTable))
                                continue;
                            if (((TextBox)PreRenderGridView.Rows[k].Cells[CellCount].Controls[0].Controls[0].Controls[0].Controls[0]).ID.Contains("Years"))
                                continue;
                            textBox = (TextBox)PreRenderGridView.Rows[k].Cells[CellCount].Controls[0].Controls[0].Controls[0].Controls[0];
                            filteredTextBoxExtender =
                                      new AjaxControlToolkit.FilteredTextBoxExtender();
                            filteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Numbers;
                            filteredTextBoxExtender.ID = textBox.ID + "_FilteredTextBoxExtender";
                            filteredTextBoxExtender.TargetControlID = textBox.UniqueID;
                            PreRenderGridView.Rows[k].Cells[CellCount].Controls.Add(filteredTextBoxExtender);
                        }
                    }
                    //
                    PreRenderGridView.Rows[k].Cells[1].Style.Add("style", "width:150px;");
                    if (k % 2 == 0)
                    {
                        PreRenderGridView.Rows[k].BackColor = ColorTranslator.FromHtml(
                            (GridViewBoundCount % 2 == 0) ? "#DAE6EB" : "#BDC9CE");
                        for (int i = 2; i < PreRenderGridView.Rows[k].Cells.Count - 2; i++)
                        {
                            PreRenderGridView.Rows[k].Cells[i].Attributes.Add("style", "text-align:center;height:75px;width:90px;");
                            PreRenderGridView.Rows[k].Cells[i].BackColor = i % 2 == 0 ?
                                ColorTranslator.FromHtml((GridViewBoundCount % 2 == 0) ? columnColor : alternateColoumnColor) :
                                ColorTranslator.FromHtml((GridViewBoundCount % 2 == 0) ? alternateColoumnColor : columnColor);
                        }
                    }
                    else
                    {
                        PreRenderGridView.Rows[k].BackColor = ColorTranslator.FromHtml((GridViewBoundCount % 2 != 0) ? "#DAE6EB" : "#BDC9CE");
                        for (int i = 2; i < PreRenderGridView.Rows[k].Cells.Count - 2; i++)
                        {
                            PreRenderGridView.Rows[k].Cells[i].Attributes.Add("style", "text-align:center;height:75px;width:90px;");
                            PreRenderGridView.Rows[k].Cells[i].BackColor = i % 2 == 0 ?
                                ColorTranslator.FromHtml((GridViewBoundCount % 2 == 0) ? alternateColoumnColor : columnColor) :
                                ColorTranslator.FromHtml((GridViewBoundCount % 2 == 0) ? columnColor : alternateColoumnColor);
                        }
                    }
                }
                GridViewBoundCount++;
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(filteredTextBoxExtender)) filteredTextBoxExtender = null;
                if (!Utility.IsNullOrEmpty(textBox)) textBox = null;
            }
        }

        #endregion Event Handlers

        #region User Control Datasource                                        

        /// <summary>
        /// Send Candidate Resume id as string to load the competency vectors.
        /// </summary>
        public string DataSource
        {
            set
            {
                if (value == null)
                    return;
                // Set values into controls.
                RolesVectorControl_candidateResumeIdHiddenField.Value = value;
                if (RolesVectorControl_candidateResumeIdHiddenField.Value == "")
                    RolesVectorControl_candidateResumeIdHiddenField.Value = "-10";
                BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value), -10, -2, -10, false);
            }
        }

        /// <summary>
        /// This method saves the competency vectors
        /// to the database.
        /// </summary>
        /// <param name="Candidate_Resume_Id"></param>
        public void Save(int Candidate_Resume_Id)
        {
            try
            {
                if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) > 0)
                {
                    CallBindMethodAsEmptyRowAdded();
                    return;
                }
                if (Candidate_Resume_Id == 0)
                {
                    RolesVectorControl_errorMessageLabel.Text =
                        Resources.HCMResource.ResumeParser_CompetencyVector_CandidateIdEmpty;
                    return;
                }
                DataTable dtTemp = null;
                try
                {
                    dtTemp =
                        ((DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE]).Tables[DataSetTableNames.MainTable.ToString()];
                }
                catch (NullReferenceException)
                {
                    RolesVectorControl_errorMessageLabel.Text =
                        Resources.HCMResource.ResumeParser_CompetencyVector_EmptyVectorsSaves;
                    return;
                }
                if (dtTemp.Rows.Count == 0)
                {
                    RolesVectorControl_errorMessageLabel.Text =
                        Resources.HCMResource.ResumeParser_CompetencyVector_EmptyVectorsSaves;
                    return;
                }
                RolesVectorControl_candidateResumeIdHiddenField.Value = Candidate_Resume_Id.ToString();
                //SaveCandidateVectors(
                //    ((DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE]).Tables[DataSetTableNames.ParameterTable.ToString()],
                //    Candidate_Resume_Id);
                SaveCandidateVectors(dtTemp, Candidate_Resume_Id);
                RolesVectorControl_successMessageLabel.Text =
                    Resources.HCMResource.ResumeParser_CompetencyVector_SaveSuccessfully;
                ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE] = null;
                BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value), -10, -2, -10, false);
            }
            catch (Exception exp)
            {
                RolesVectorControl_errorMessageLabel.Text = exp.Message;
            }
        }

        #endregion User Control Datasource

        #region Private Methods                                                
        
        /// <summary>
        /// This method binds the vector group drop down list
        /// </summary>
        private void BindRolesVectorGroupDropDown()
        {
            if (RolesVectorControl_vectorGroupDropDownList.Items.Count != 0)
                return;
            RolesVectorControl_vectorGroupDropDownList.DataSource = GetCompetencyVectorGroupTable();
            RolesVectorControl_vectorGroupDropDownList.DataValueField = "Group_Id";
            RolesVectorControl_vectorGroupDropDownList.DataTextField = "Group_Name";
            RolesVectorControl_vectorGroupDropDownList.DataBind();
            RolesVectorControl_vectorGroupDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        /// <summary>
        /// This method returns the Competency Vector group table.
        /// </summary>
        /// <returns>Dataset contains the competency vector group table.</returns>
        private DataTable GetCompetencyVectorGroupTable()
        {
            return new CompetencyVectorBLManager().GetCompetencyVectorGroupTable().Tables[0];
        }

        /// <summary>
        /// This method add's an empty row to the session
        /// This method will be called when user cliks add button by selecting
        /// a vector group name from drop down list.
        /// This method checks whether the selected group exists 
        /// in the session or not. if exists then raises exception.
        /// </summary>
        /// <param name="dsCompetancy">Dataset contains in session</param>
        /// <param name="GroupEditId">The edit group id of the inner grid view
        /// Note:- This variable should be passed by ref keyword. This method
        /// sets a value to this so that next time the row will be edit state</param>
        ///  /// <param name="EditIndex">According to the group id which 
        /// row should display as edit index.
        /// Note:- This variable should be passed by ref keyword. This method
        /// sets a value to this so that next time the row will be edit state</param>
        private void AddEmptyRowToDataset(ref DataSet dsCompetancy,ref int GroupEditId,ref int EditIndex)
        {
            DataTable dtGridBindTable = null;
            DataTable dtParameter = null;
            DataTable dtVectorTable = null;
            try
            {
                dtGridBindTable = dsCompetancy.Tables[DataSetTableNames.MainTable.ToString()];
                if (dtGridBindTable.Select("GROUP_ID=" + RolesVectorControl_vectorGroupDropDownList.SelectedValue +
                        " AND ROWSTATUS_ID<>'" + VectorStatus.Delete + "'").Length > 0)
                {
                    RolesVectorControl_errorMessageLabel.Text =
                        Resources.HCMResource.ResumeParser_CompetencyVector_GroupExists;
                    return;
                }
                dtParameter = dsCompetancy.Tables[DataSetTableNames.CompetencyParameterTable.ToString()];
                dtVectorTable = dsCompetancy.Tables[DataSetTableNames.VectorGroup.ToString()];
                DataRow drNewRow = null;
                drNewRow = dtVectorTable.NewRow();
                drNewRow["Group_Id"] = RolesVectorControl_vectorGroupDropDownList.SelectedValue;
                drNewRow["Group_Name"] = RolesVectorControl_vectorGroupDropDownList.SelectedItem.Text;
                dtVectorTable.Rows.Add(drNewRow);
                drNewRow = null;
                StringBuilder sbParameterIds = null;
                StringBuilder sbParameterNames = null;
                sbParameterIds = new StringBuilder();
                sbParameterNames = new StringBuilder();
                drNewRow = dtGridBindTable.NewRow();
                drNewRow["ROW_ID"] = dtGridBindTable.Rows.Count + 1;
                drNewRow["GROUP_ID"] = RolesVectorControl_vectorGroupDropDownList.SelectedValue;
                drNewRow["RowStatus_ID"] = VectorStatus.NewRow;
                for (int i = 0; i < dtParameter.Rows.Count; i++)
                {
                    sbParameterIds.Append(dtParameter.Rows[i]["Parameter_Id"].ToString().Trim().Replace(' ', '_') + ",");
                    sbParameterNames.Append(dtParameter.Rows[i]["Parameter_Name"].ToString().Trim().Replace(' ', '_') + ",");
                }
                drNewRow["PARAMETER_ID"] = sbParameterIds.ToString().TrimEnd(',');
                drNewRow["PARAMETER_NAMES_ID"] = sbParameterNames.ToString().TrimEnd(',');
                dtGridBindTable.Rows.Add(drNewRow);
                GroupEditId = Convert.ToInt32(RolesVectorControl_vectorGroupDropDownList.SelectedValue);
                EditIndex = 0;
                RolesVectorControl_addEmptyRowHiddenField.Value = "10";
                ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE] = GroupEditId;
                ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE] = EditIndex;
                RolesVectorControl_addVectorGroupIdHiddenField.Value = GroupEditId.ToString();
                dsCompetancy = null;
                dsCompetancy = new DataSet();
                dsCompetancy.Tables.Add(dtVectorTable.Copy());
                dsCompetancy.Tables[0].TableName = Enum.GetName(typeof(DataSetTableNames), DataSetTableNames.VectorGroup);
                dsCompetancy.Tables.Add(dtParameter.Copy());
                dsCompetancy.Tables[1].TableName = Enum.GetName(typeof(DataSetTableNames), DataSetTableNames.CompetencyParameterTable);
                dsCompetancy.Tables.Add(dtGridBindTable.Copy());
                dsCompetancy.Tables[2].TableName = Enum.GetName(typeof(DataSetTableNames), DataSetTableNames.MainTable);
            }
            finally
            {
                if (dtGridBindTable != null) dtGridBindTable = null;
                if (dtParameter != null) dtParameter = null;
                if (dtVectorTable != null) dtVectorTable = null;
            }
        }

        /// <summary>
        /// This method binds the outer grid view and inner grid view.
        /// </summary>
        /// <param name="CandidateId">Candidate resume id for which to 
        /// add competency vector table.</param>
        /// <param name="GroupEditId">The edit group id of the inner grid view
        /// Note:- (If not needed then pass some negative value)</param>
        /// <param name="EditIndex">According to the group id which 
        /// row should display as edit index.
        /// Note:- If not needed then pass some negative value</param>
        /// <param name="AddGroupId">The group id to which
        /// new row should be added.
        /// <param name="AddEmptyRow">Wheter to add an emtpy row
        /// to the grid. If true it's add an empty row to the grid 
        /// with sent AddGroupId parameter, false means normal behaviour.</param>
        /// Note:- If not needed then pass some negative value</param>
        private void BindGrid(int CandidateId, int GroupEditId, int EditIndex, int AddGroupId,bool AddEmptyRow)
        {
            DataSet dsCompetancy = null;
            GridView InnerGridView = null;
            try
            {
                BindRolesVectorGroupDropDown();
                if (ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE] == null)
                    dsCompetancy = GetCompetancyVectorTable(CandidateId);
                else
                    dsCompetancy = (DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE];
                if (AddEmptyRow)
                {
                    AddEmptyRowToDataset(ref dsCompetancy, ref GroupEditId, ref EditIndex);
                    ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE] = dsCompetancy;
                }
                if ((!Utility.IsNullOrEmpty(dsCompetancy)) && (dsCompetancy.Tables.Count > 0) && (dsCompetancy.Tables.Count >= 2) && (dsCompetancy.Tables[2].Rows.Count == 0))
                {
                    RolesVectorControl_competencyGridView.DataSource = null;
                    RolesVectorControl_competencyGridView.DataBind();
                    return;
                }
                if (Utility.IsNullOrEmpty(dsCompetancy))
                    return;
                ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE] = dsCompetancy;
                RolesVectorControl_competencyGridView.BorderStyle = BorderStyle.None;
                RolesVectorControl_competencyGridView.AlternatingRowStyle.BorderStyle = BorderStyle.None;
                RolesVectorControl_competencyGridView.RowStyle.BorderStyle = BorderStyle.None;
                RolesVectorControl_competencyGridView.RowStyle.BorderWidth = new Unit(0);
                RolesVectorControl_competencyGridView.AlternatingRowStyle.BorderWidth = new Unit(0);
                RolesVectorControl_competencyGridView.ShowHeader = false;
                RolesVectorControl_competencyGridView.DataSource = dsCompetancy.Tables["VectorGroup"];
                RolesVectorControl_competencyGridView.DataBind();
                for (int i = 0; i < RolesVectorControl_competencyGridView.Rows.Count; i++)
                {
                    ((ImageButton)RolesVectorControl_competencyGridView.Rows[i].FindControl("AddVectorGroupNameImageButton"))
                        .Attributes.Add("onclick", "return FillCommandArgument('" +
                        RolesVectorControl_addVectorGroupIdHiddenField.ClientID + "','" + ((ImageButton)RolesVectorControl_competencyGridView.Rows[i].
                        FindControl("AddVectorGroupNameImageButton")).CommandArgument + "');");
                    InnerGridView = (GridView)RolesVectorControl_competencyGridView.Rows[i].FindControl("RolesVectorControl_parametersGridView");
                    BindParameterTableForVectorID(Convert.ToInt32(((Label)RolesVectorControl_competencyGridView.Rows[i].
                        FindControl("lblVectorGroupId")).Text), ref InnerGridView, ref dsCompetancy,
                        i == 0 ? true : false, GroupEditId, EditIndex, AddGroupId);
                }
            }
            finally
            {
                if (dsCompetancy != null) dsCompetancy = null;
            }
        }

        /// <summary>
        /// This method get the data from the database.
        /// </summary>
        /// <returns>returns the dataset of competency candidate vector
        /// table.</returns>
        private DataSet GetCompetancyVectorTable(int CandidateId)
        {
            DataSet CompetencyVectorDataSet = null;
            try
            {
                CompetencyVectorDataSet = new CompetencyVectorBLManager().GetCompetencyVectorTable(CandidateId);
                ViewState[VECTORSTABLE_VIEWSTATE] = CompetencyVectorDataSet.Tables[1];
                return CreateDataset(CompetencyVectorDataSet.Tables[0], CompetencyVectorDataSet.Tables[2].Copy());
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(CompetencyVectorDataSet)) CompetencyVectorDataSet = null;
            }
        }

        /// <summary>
        /// This method convert parameter rows in to columns names
        /// and bind appropriate values in to the data table.
        /// </summary>
        /// <param name="dtCompetency">competency candidate vector data 
        /// table object</param>
        /// <returns>Dataset contains various tables for to bind inner grid view.</returns>
        private DataSet CreateDataset(DataTable dtCompetency, DataTable dtParamTable)
        {
            DataSet dsReturn = null;
            DataTable dtTemp = null;
            DataTable dtParameterTable = null;
            StringBuilder sbParameterIds = null;
            StringBuilder sbParameterNames = null;
            try
            {
                dsReturn = new DataSet();
                dtTemp = new DataTable();
                dtTemp.Columns.Add("GROUP_ID", typeof(int));
                dtTemp.Columns.Add("GROUP_NAME", typeof(string));
                dtTemp = dtCompetency.DefaultView.ToTable(true, new string[2] { "GROUP_ID", "GROUP_NAME" });
                dsReturn.Tables.Add(dtTemp);
                dsReturn.Tables[0].TableName = "VectorGroup";
                dtTemp = null;
                dtTemp = new DataTable();
                dtTemp = dtParamTable.Copy();
                dsReturn.Tables.Add(dtParamTable);
                dsReturn.Tables[1].TableName = "CompetencyParameterTable";
                dtParameterTable = new DataTable();
                dtParameterTable.Columns.Add("ROW_ID", typeof(int)); 
                dtParameterTable.Columns.Add("GROUP_ID", typeof(int));
                dtParameterTable.Columns.Add("VECTOR_ID", typeof(int));
                //dtParameterTable.Columns.Add("GROUP_NAME", typeof(string));
                dtParameterTable.Columns.Add("VECTOR_NAME", typeof(string));
                dtParameterTable.Columns.Add("PARAMETER_ID", typeof(string));
                dtParameterTable.Columns.Add("PARAMETER_NAMES_ID", typeof(string));
                sbParameterIds = new StringBuilder();
                sbParameterNames = new StringBuilder();
                for (int i = 0; i < dtTemp.Rows.Count; i++)
                {
                    sbParameterIds.Append(dtTemp.Rows[i]["PARAMETER_ID"].ToString().Trim() + ",");
                    sbParameterNames.Append(dtTemp.Rows[i]["PARAMETER_NAME"].ToString().Trim().Replace(' ', '_') + ",");
                    dtParameterTable.Columns.Add(dtTemp.Rows[i]["PARAMETER_NAME"].ToString().Trim().Replace(' ', '_'), typeof(string));
                    dtParameterTable.Columns.Add(dtTemp.Rows[i]["PARAMETER_NAME"].ToString().Trim().Replace(' ', '_') + "_REMARKS", typeof(string));
                }
                dtParameterTable.Columns.Add("RowStatus_ID", typeof(string));
                dtParameterTable.Columns.Add("OLD_VECTOR_ID", typeof(int));
                dtTemp = dtCompetency.DefaultView.ToTable(true, new string[] { "GROUP_ID", "VECTOR_ID" });
                int VectorTableCount = dtTemp.Rows.Count;
                DataRow[] drVectorRows = null;
                DataRow drNewRow = null;
                for (int i = 0; i < VectorTableCount; i++)
                {
                    drVectorRows = null;
                    drVectorRows = dtCompetency.Select("GROUP_ID = " +
                        dtTemp.Rows[i]["GROUP_ID"] + " AND VECTOR_ID = " + dtTemp.Rows[i]["VECTOR_ID"]);
                    for (int k = 0; k < drVectorRows.Length; k++)
                    {
                        if (k == 0)
                        {
                            drNewRow = dtParameterTable.NewRow();
                            drNewRow["ROW_ID"] = i + 1;
                            //drNewRow["GROUP_NAME"] = drVectorRows[k]["GROUP_NAME"];
                            drNewRow["PARAMETER_ID"] = sbParameterIds.ToString().TrimEnd(',');
                            drNewRow["PARAMETER_NAMES_ID"] = sbParameterNames.ToString().TrimEnd(',');
                            drNewRow["GROUP_ID"] = drVectorRows[k]["GROUP_ID"];
                            drNewRow["VECTOR_ID"] = drVectorRows[k]["VECTOR_ID"];
                            drNewRow["VECTOR_NAME"] = drVectorRows[k]["VECTOR_NAME"];
                            drNewRow["ROWSTATUS_ID"] = VectorStatus.Existing;
                            drNewRow["OLD_VECTOR_ID"] = drVectorRows[k]["VECTOR_ID"];
                        }
                        drNewRow[drVectorRows[k]["PARAMETER_NAME"].ToString().Trim().Replace(' ', '_') + "_REMARKS"] = drVectorRows[k]["REMARKS"];
                        drNewRow[drVectorRows[k]["PARAMETER_NAME"].ToString().Trim().Replace(' ', '_')] = drVectorRows[k]["VALUE"];
                    }
                    dtParameterTable.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                dsReturn.Tables.Add(dtParameterTable);
                dsReturn.Tables[2].TableName = DataSetTableNames.MainTable.ToString();// "ParameterTable";
                //if (FromDataSource)
                //    ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE] = dsReturn;
                return dsReturn;
            }
            finally
            {
                if (dtTemp != null) dtTemp = null;
                if (dsReturn != null) dsReturn = null;
                if (dtParameterTable != null) dtParameterTable = null;
                if (sbParameterIds != null) sbParameterIds = null;
                if (sbParameterNames != null) sbParameterNames = null;
            }
        }

        /// <summary>
        /// This method binds the data to the outer grid view and
        /// inner grid view.
        /// This method will be called for all the events when a 
        /// new group is added and not updated.
        /// </summary>
        private void CallBindMethodAsEmptyRowAdded()
        {
            BindGrid(Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value),
                    Convert.ToInt32(ViewState[ROLESVECTORCONTROL_VECTOR_ID_VIEWSTATE]),
                    Convert.ToInt32(ViewState[ROLESVECTORCONTROL_EDITROWINDEX_VIEWSTATE]), -10, false);
            RolesVectorControl_errorMessageLabel.Text =
                Resources.HCMResource.ResumeParser_CompetencyVector_NewGroupAdded;
        }

        #region Inner Grid view Methods                                        

        /// <summary>
        /// This method creates template fields for the grid view
        /// </summary>
        /// <param name="GroupId">group id which the inner grid view is binding</param>
        /// <param name="InnerGridView">inner gridview as reference object.</param>
        /// <param name="dsCompetancy">The main table(s) dataset 
        /// for manipulations to get the child grid view appropriate rows.</param>
        /// <param name="ShowHeader">whether the inner gridview header
        /// should be shown or not.</param>
        /// <param name="GroupEditId">The edit group id of the inner grid view
        /// Note:- (If not needed then pass some negative value)</param>
        /// <param name="EditIndex">According to the edit group id which 
        /// row should display as edit index.
        /// Note:- If not needed then pass some negative value</param>
        /// <param name="AddGroupId">The group id to which
        /// new row should be added.
        /// Note:- If not needed then pass some negative value</param>
        private void BindParameterTableForVectorID(int GroupId,
            ref GridView InnerGridView, ref DataSet dsCompetancy, bool ShowHeader,
            int GroupEditId, int EditIndex, int AddGroupId)
        {
            DataTable dtCompetencyParameterTable = null;
            TemplateField templateField = null;
            DataTable dtParameterTable = null;
            DataView dvTemp = null;
            DataTable dtTemp = null;
            DataTable dtMainTableCopy = null;
            int HeadCount = 2;
            try
            {
                dtCompetencyParameterTable = dsCompetancy.Tables[DataSetTableNames.MainTable.ToString()];
                templateField = new TemplateField();
                templateField.HeaderText = "";
                templateField.ItemTemplate = new GridViewTemplate(ListItemType.Item,
                    "VECTOR_NAME", "", null, "Label");
                templateField.ItemStyle.Width = new Unit(80, UnitType.Pixel);
                templateField.ItemStyle.Height = new Unit(75, UnitType.Pixel);
                templateField.HeaderStyle.BackColor = Color.White;
                templateField.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                templateField.ItemStyle.VerticalAlign = VerticalAlign.Middle;
                dvTemp = ((DataTable)ViewState[VECTORSTABLE_VIEWSTATE]).DefaultView;
                dvTemp.RowFilter = "GROUP_ID=" + GroupId;
                dtTemp = dvTemp.ToTable(true, new string[2] { "VECTOR_ID", "VECTOR_NAME" });
                templateField.EditItemTemplate = new GridViewTemplate(ListItemType.EditItem, "VECTOR_NAME",
                    "", dtTemp, "TextBox");
                templateField.HeaderStyle.BorderStyle = BorderStyle.None;
                templateField.ItemStyle.BorderStyle = BorderStyle.None;
                InnerGridView.Columns.Add(templateField);
                dvTemp = null;
                dtTemp = null;
                dtParameterTable = new DataTable();
                for (int i = 0; i < dtCompetencyParameterTable.Columns.Count; i++)
                {
                    dtParameterTable.Columns.Add(dtCompetencyParameterTable.Columns[i].ColumnName.Trim(),
                        dtCompetencyParameterTable.Columns[i].DataType);
                    if ((dtCompetencyParameterTable.Columns[i].ColumnName.Trim() == "VECTOR_NAME") ||
                        (dtCompetencyParameterTable.Columns[i].ColumnName.Trim() == "GROUP_NAME") ||
                        (dtCompetencyParameterTable.Columns[i].ColumnName.Trim().Contains("REMARKS")))
                        continue;
                    templateField = null;
                    templateField = new TemplateField();
                    if (ShowHeader)
                    {
                        templateField.HeaderTemplate = new GridViewTemplate(ListItemType.Header,
                            dtCompetencyParameterTable.Columns[i].ColumnName.Trim().Replace('_', ' '), "..", null, "Label");
                        //templateField.HeaderText =
                        //  dtCompetencyParameterTable.Columns[i].ColumnName.Trim().Replace('_', ' ');
                    }
                    else
                        templateField.HeaderText = "";
                    templateField.ItemTemplate = new GridViewTemplate(ListItemType.Item,
                        dtCompetencyParameterTable.Columns[i].ColumnName,
                        dtCompetencyParameterTable.Columns[i].ColumnName + "_REMARKS", null, "Label");
                    templateField.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                    templateField.ItemStyle.VerticalAlign = VerticalAlign.Middle;
                    if (dtCompetencyParameterTable.Columns[i].ColumnName.Contains("_ID"))
                        templateField.Visible = false;
                    else
                        templateField.Visible = true;
                    // For Edit Item Template
                    templateField.EditItemTemplate = new GridViewTemplate(ListItemType.EditItem,
                        dtCompetencyParameterTable.Columns[i].ColumnName,
                          dtCompetencyParameterTable.Columns[i].ColumnName + "_REMARKS", null, "TextBox");
                    //
                    if (HeadCount++ % 2 != 0)
                        templateField.HeaderStyle.BackColor = ColorTranslator.FromHtml(HEADER_COLOR);
                    else
                        templateField.HeaderStyle.BackColor = ColorTranslator.FromHtml(HEADER_ALTERNATE_COLOR);
                    templateField.HeaderStyle.Font.Bold = false;
                    templateField.HeaderStyle.Font.Size = FontUnit.Small;
                    templateField.HeaderStyle.Height = new Unit(20, UnitType.Pixel);
                    templateField.HeaderStyle.ForeColor = Color.Black;
                    templateField.HeaderStyle.VerticalAlign = VerticalAlign.Middle;
                    templateField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    templateField.HeaderStyle.BorderStyle = BorderStyle.None;
                    //
                    templateField.ItemStyle.Width = new Unit(80, UnitType.Pixel);
                    templateField.ItemStyle.Height = new Unit(80, UnitType.Pixel);
                    templateField.ItemStyle.BorderStyle = BorderStyle.None;
                    InnerGridView.Columns.Add(templateField);
                    //
                }
                templateField = new TemplateField();
                templateField.HeaderStyle.BackColor = Color.White;
                templateField.ItemTemplate = new GridViewTemplate(ListItemType.Item, "..", "..", null,
                    "EditButton");
                templateField.EditItemTemplate = new GridViewTemplate(ListItemType.EditItem, "..", "..", null,
                    "UpdateButton");
                templateField.ItemStyle.BackColor = Color.White;
                templateField.ItemStyle.Width = new Unit(20, UnitType.Pixel);
                templateField.ItemStyle.Height = new Unit(10, UnitType.Pixel);
                templateField.ItemStyle.BorderStyle = BorderStyle.None;
                templateField.HeaderStyle.BackColor = Color.White;
                templateField.HeaderStyle.BorderStyle = BorderStyle.None;
                templateField.HeaderText = "";
                templateField.HeaderStyle.BorderWidth = new Unit(0, UnitType.Pixel);
                InnerGridView.Columns.Add(templateField);
                templateField = null;
                dtMainTableCopy = dsCompetancy.Tables[DataSetTableNames.MainTable.ToString()].Copy();
                dtTemp = dsCompetancy.Tables[DataSetTableNames.MainTable.ToString()].Copy();
                dvTemp = dtTemp.DefaultView;
                dvTemp.RowFilter = "GROUP_ID=" + GroupId + " AND RowStatus_ID <> '" + VectorStatus.Delete + "'";
                dtTemp = null;
                dtTemp = dvTemp.ToTable();
                DataRow drNewRow = null;
                for (int i = 0; i < dtTemp.Rows.Count; i++)
                {
                    drNewRow = dtParameterTable.NewRow();
                    for (int k = 0; k < dtTemp.Columns.Count; k++)
                        drNewRow[dtTemp.Columns[k].ColumnName.Trim()] = dtTemp.Rows[i][k];
                    dtParameterTable.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                if ((GroupEditId != 0) && (GroupEditId == GroupId))
                    InnerGridView.EditIndex = EditIndex;
                if (AddGroupId == GroupId)
                {
                    if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) < 0)
                    {
                        drNewRow = null;
                        drNewRow = dtParameterTable.NewRow();
                        drNewRow["ROW_ID"] = dtMainTableCopy.Rows.Count + 1;
                        drNewRow["PARAMETER_ID"] = dtParameterTable.Rows[dtParameterTable.Rows.Count - 1]["PARAMETER_ID"];
                        drNewRow["PARAMETER_NAMES_ID"] = dtParameterTable.Rows[dtParameterTable.Rows.Count - 1]["PARAMETER_NAMES_ID"];
                        dtParameterTable.Rows.Add(drNewRow);
                        InnerGridView.EditIndex = dtParameterTable.Rows.Count - 1;
                    }
                }
                dsCompetancy.Tables[DataSetTableNames.MainTable.ToString()].Clear();
                dsCompetancy.Tables[DataSetTableNames.MainTable.ToString()].Merge(dtMainTableCopy);
                InnerGridView.DataSource = dtParameterTable;
                InnerGridView.DataBind();
                InnerGridView.BorderStyle = BorderStyle.None;
                InnerGridView.BorderWidth = new Unit(0);
                InnerGridView.RowCancelingEdit += new
                    GridViewCancelEditEventHandler(RolesVectorControl_InnerGridView_RowCancelingEdit);
                InnerGridView.RowDeleting += new
                    GridViewDeleteEventHandler(RolesVectorControl_InnerGridView_RowDeleting);
                InnerGridView.RowEditing += new
                    GridViewEditEventHandler(RolesVectorControl_InnerGrid_RowEditing);
                InnerGridView.RowUpdating += new
                    GridViewUpdateEventHandler(RolesVectorControl_InnerGridView_RowUpdating);
                InnerGridView.PreRender += new
                    EventHandler(RolesVectorControl_InnerGridView_PreRender);
                if (!Utility.IsNullOrEmpty(InnerGridView.HeaderRow))
                    if (ShowHeader)
                    {
                        InnerGridView.HeaderRow.BackColor = Color.White;
                        InnerGridView.HeaderRow.BorderColor = Color.White;
                        InnerGridView.HeaderStyle.BorderWidth = new Unit(0, UnitType.Pixel);
                        InnerGridView.HeaderRow.Visible = true;
                        InnerGridView.HeaderRow.VerticalAlign = VerticalAlign.Middle;
                        InnerGridView.HeaderRow.HorizontalAlign = HorizontalAlign.Center;
                    }
                    else
                        InnerGridView.HeaderRow.Visible = false;
            }
            finally
            {
                if (dtCompetencyParameterTable != null) dtCompetencyParameterTable = null;
                if (dtParameterTable != null) dtParameterTable = null;
                if (dvTemp != null) dvTemp = null;
                if (templateField != null) templateField = null;
            }
        }

        #endregion Inner Grid view Methods

        #region Grid Session Methods                                           

        /// <summary>
        /// Updates or inserts the row to the database.
        /// </summary>
        /// <param name="EditGridView">Grid view object by reference</param>
        /// <param name="GridRowIndexRowIndex">The editing row index 
        /// of the grid view</param>
        /// <param name="RowIndex">Row index to update in data table.</param>
        /// <param name="Update">Whether to update or insert
        /// if true then in update mode else insert mode.</param>
        /// <param name="dtGridMainTable">The main data table that showing 
        /// to the user. This table will be in session object.</param>
        private bool UpdateRow(ref GridView EditGridView, int GridRowIndexRowIndex, int RowIndex, bool Update,
            DataTable dtGridMainTable)
        {
            string ParameterIds = ((TextBox)EditGridView.Rows[GridRowIndexRowIndex].FindControl("PARAMETER_ID_TEXTBOX")).Text;
            string[] ParameterNames = ((TextBox)EditGridView.Rows[GridRowIndexRowIndex].FindControl("PARAMETER_NAMES_ID_TEXTBOX")).Text.Split(',');
            bool isUpdated = true;
            int VectorId = Convert.ToInt32(((DropDownList)EditGridView.Rows[GridRowIndexRowIndex].
                        FindControl("Vector_Name_DropDownList")).SelectedValue);
            if (VectorId == 0)
            {
                RolesVectorControl_errorMessageLabel.Text =
                    Resources.HCMResource.ResumeParser_CompetencyVector_SelectVectorId;
                isUpdated = false;
                return isUpdated;
            }
            if (Update)
            {
                if (dtGridMainTable.Select("VECTOR_ID=" + VectorId + " and RowStatus_ID<>'" +
                             Enum.GetName(typeof(VectorStatus), VectorStatus.Delete) + "'").Length >
                             ((Convert.ToInt32(
                             ((dtGridMainTable.Rows[RowIndex]["VECTOR_ID"]) == DBNull.Value) ?
                             -10 : dtGridMainTable.Rows[RowIndex]["VECTOR_ID"]) == VectorId) ? 1 : 0))
                {
                    RolesVectorControl_errorMessageLabel.Text =
                        Resources.HCMResource.ResumeParser_CompetencyVector_VectorNameExists;
                    isUpdated = false;
                    return isUpdated;
                }
                dtGridMainTable.Rows[RowIndex]["VECTOR_ID"] = ((DropDownList)EditGridView.Rows[GridRowIndexRowIndex].FindControl("Vector_Name_DropDownList")).SelectedValue;
                dtGridMainTable.Rows[RowIndex]["VECTOR_NAME"] = ((DropDownList)EditGridView.Rows[GridRowIndexRowIndex].
                    FindControl("Vector_Name_DropDownList")).SelectedItem.Text;
                for (int i = 0; i < ParameterNames.Length; i++)
                {
                    dtGridMainTable.Rows[RowIndex][ParameterNames[i]] = ((TextBox)EditGridView.Rows[GridRowIndexRowIndex].
                        FindControl(ParameterNames[i] + "_TextBox")).Text;
                    dtGridMainTable.Rows[RowIndex][ParameterNames[i] + "_Remarks"] = ((TextBox)EditGridView.Rows[GridRowIndexRowIndex].
                        FindControl(ParameterNames[i] + "_Remarks_TextBox")).Text;
                }
                string RowStatus = dtGridMainTable.Rows[RowIndex]["RowStatus_ID"].ToString();
                if (RowStatus == Enum.GetName(typeof(VectorStatus), VectorStatus.Existing))
                    dtGridMainTable.Rows[RowIndex]["RowStatus_ID"] = VectorStatus.ExistingUpdated;
                else if (RowStatus == Enum.GetName(typeof(VectorStatus), VectorStatus.NewRow) ||
                    RowStatus == Enum.GetName(typeof(VectorStatus), VectorStatus.NewRowVector))
                    dtGridMainTable.Rows[RowIndex]["OLD_VECTOR_ID"] = dtGridMainTable.Rows[RowIndex]["VECTOR_ID"];
            }
            else if (!Update)
            {
                if (dtGridMainTable.Select("VECTOR_ID=" + VectorId + " and RowStatus_ID<>'" +
                            Enum.GetName(typeof(VectorStatus), VectorStatus.Delete) + "'").Length > 0)
                {
                    RolesVectorControl_errorMessageLabel.Text = "Vector Name already added";
                    isUpdated = false;
                    return isUpdated;
                }
                if (Convert.ToInt32(RolesVectorControl_addEmptyRowHiddenField.Value) < 0)
                    dtGridMainTable.Rows.Add(dtGridMainTable.NewRow());
                dtGridMainTable.Rows[RowIndex]["ROW_ID"] = RowIndex + 1;
                dtGridMainTable.Rows[RowIndex]["GROUP_ID"] = Convert.ToInt32(RolesVectorControl_addVectorGroupIdHiddenField.Value);
                dtGridMainTable.Rows[RowIndex]["VECTOR_ID"] = ((DropDownList)EditGridView.Rows[GridRowIndexRowIndex].
                    FindControl("Vector_Name_DropDownList")).SelectedValue;
                dtGridMainTable.Rows[RowIndex]["VECTOR_NAME"] = ((DropDownList)EditGridView.Rows[GridRowIndexRowIndex].
                    FindControl("Vector_Name_DropDownList")).SelectedItem.Text;
                StringBuilder sbNames = new StringBuilder();
                for (int i = 0; i < ParameterNames.Length; i++)
                {
                    dtGridMainTable.Rows[RowIndex][ParameterNames[i]] = ((TextBox)EditGridView.Rows[GridRowIndexRowIndex].FindControl(ParameterNames[i] + "_TextBox")).Text;
                    dtGridMainTable.Rows[RowIndex][ParameterNames[i] + "_Remarks"] = ((TextBox)EditGridView.Rows[GridRowIndexRowIndex].
                        FindControl(ParameterNames[i] + "_Remarks_TextBox")).Text;
                    sbNames.Append(ParameterNames[i] + ",");
                }
                dtGridMainTable.Rows[RowIndex]["PARAMETER_ID"] = ParameterIds.TrimEnd(',');
                dtGridMainTable.Rows[RowIndex]["PARAMETER_NAMES_ID"] = sbNames.ToString().TrimEnd(',');
                dtGridMainTable.Rows[RowIndex]["RowStatus_ID"] = VectorStatus.NewRow;
                dtGridMainTable.Rows[RowIndex]["OLD_VECTOR_ID"] = dtGridMainTable.Rows[RowIndex]["VECTOR_ID"];
            }
            return isUpdated;
        }

        /// <summary>
        /// This method deletes the row from the data table and it will
        /// automatically updates to session. This method
        /// will re arrange the row id's in the data table.
        /// </summary>
        /// <param name="dtGridMainTable">The main data table that showing 
        /// to the user. This table will be in session object.</param>
        /// <param name="RowIndex">Row index to delete row in data table.</param>
        /// <param name="dtVectorTable">The vector data table that showing 
        /// to the user. This table will be in session object.</param>
        /// <param name="GroupId">Group id of the row that is deleting.</param>
        private void DeleteRow(DataTable dtGridMainTable, int RowIndex, DataTable dtVectorTable, int GroupId)
        {
            if ((string)dtGridMainTable.Rows[RowIndex]["RowStatus_ID"] ==
                Enum.GetName(typeof(VectorStatus), VectorStatus.NewRow) ||
                (string)dtGridMainTable.Rows[RowIndex]["RowStatus_ID"] ==
                Enum.GetName(typeof(VectorStatus), VectorStatus.NewRowVector))
            {
                dtGridMainTable.Rows.RemoveAt(RowIndex);
                for (int i = RowIndex + 1; i <= dtGridMainTable.Rows.Count; i++)
                    dtGridMainTable.Rows[i - 1]["ROW_ID"] = i;
                CheckForGroup(ref dtVectorTable, dtGridMainTable, GroupId);
                return;
            }
            dtGridMainTable.Rows[RowIndex]["RowStatus_ID"] = VectorStatus.Delete;
            CheckForGroup(ref dtVectorTable, dtGridMainTable, GroupId);
        }

        /// <summary>
        /// This method checks whether any group data present in 
        /// grid view or not. if not it will remove group from the
        /// group data.
        /// </summary>
        /// <param name="dtVector">The vector data table that showing 
        /// to the user. This table will be in session object.</param>
        /// <param name="dtGridMainTable">The main data table that showing 
        /// to the user. This table will be in session object.</param>
        /// <param name="GroupId">Group id of the row that is deleting.</param>
        private void CheckForGroup(ref DataTable dtVector, DataTable dtGridMainTable, int GroupId)
        {
            if (dtGridMainTable.Select("GROUP_ID=" + GroupId + " and RowStatus_ID <> '" +
                VectorStatus.Delete + "'").Length > 0)
                return;
            DataRow[] drVectorCheck = dtVector.Select("GROUP_ID=" + GroupId);
            for (int i = 0; i < drVectorCheck.Length; i++)
            {
                dtVector.Rows.Remove(drVectorCheck[i]);
            }
        }

        #endregion Grid Session Methods

        #region Save Code                                                      

        private void SaveCandidateVectors(DataTable dtGridMainTable, int Candidate_Resume_Id)
        {
            string[] VectorStrings = Enum.GetNames(typeof(VectorStatus));
            List<CandidateCompetencyVector> VectorIdChange = null;
            List<CandidateCompetencyVector> InsertCandidate = null;
            List<CandidateCompetencyVector> DeleteCandidate = null;
            List<CandidateCompetencyVector> ExistingUpdated = null;
            DataTable dtTemp = null;
            DataView dvTemp = null;
            IDbTransaction transaction = null;
            try
            {
                int UserId = 0;
                if (Utility.IsNullOrEmpty(Session["USER_DETAIL"]))
                    Response.Redirect("../Login.aspx", false);
                else
                    UserId = ((UserDetail)Session["USER_DETAIL"]).UserID;
                for (int i = 0; i < VectorStrings.Length; i++)
                {
                    //if (VectorStrings[i] == "Existing")
                    //    continue;
                    switch (VectorStrings[i])
                    {
                        case "ExistingUpdated":
                            dvTemp = dtGridMainTable.DefaultView;
                            dvTemp.RowFilter = "RowStatus_Id='ExistingUpdated'";
                            dtTemp = dvTemp.ToTable();
                            if (dtTemp.Rows.Count == 0)
                                break;
                            GenerateCandidateVectorsForChangeVectorId(ref VectorIdChange,
                                dtTemp, Candidate_Resume_Id, UserId);
                            GenerateCandidateVectorList(ref ExistingUpdated, dtTemp, Candidate_Resume_Id, UserId);
                            break;
                        case "NewRow":
                        case "NewRowVector":
                            dvTemp = dtGridMainTable.DefaultView;
                            dvTemp.RowFilter =
                                VectorStrings[i] == "NewRow" ? "RowStatus_Id='NewRow'" :
                                VectorStrings[i] == "NewRowVector" ? "RowStatus_Id='" + VectorStatus.NewRowVector + "'" : "";
                            dtTemp = dvTemp.ToTable();
                            if (dtTemp.Rows.Count != 0)
                                GenerateCandidateVectorList(ref InsertCandidate, dtTemp, Candidate_Resume_Id, UserId);
                            break;
                        case "Delete":
                            dvTemp = dtGridMainTable.DefaultView;
                            dvTemp.RowFilter = "RowStatus_Id='Delete'";
                            dtTemp = dvTemp.ToTable();
                            if (dtTemp.Rows.Count != 0)
                                GenerateCandidateVectorForDeleteList(ref DeleteCandidate, dtTemp, Candidate_Resume_Id);
                            break;
                    }
                }
                new CompetencyVectorBLManager().DBOperationsForCandidateCompetencyVector(
                    DeleteCandidate, VectorIdChange, ExistingUpdated, InsertCandidate);
            }
            catch
            {
                if (!Utility.IsNullOrEmpty(transaction)) transaction.Rollback();
                throw;
            }
            finally
            {
                if (VectorIdChange != null) VectorIdChange = null;
                if (InsertCandidate != null) InsertCandidate = null;
                if (DeleteCandidate != null) DeleteCandidate = null;
                if (ExistingUpdated != null) ExistingUpdated = null;
                if (dtTemp != null) dtTemp = null;
                if (dvTemp != null) dvTemp = null;
            }
        }

        private void GenerateCandidateVectorList(ref List<CandidateCompetencyVector> candidateCompetencyVectors,
            DataTable dtGridTable, int Cand_Resume_Id, int UserId)
        {
            CandidateCompetencyVector candidateCompetencyVector = null;
            try
            {
                string ParameterIds = dtGridTable.Rows[0]["PARAMETER_ID"].ToString();
                string[] ParameterNames = dtGridTable.Rows[0]["PARAMETER_NAMES_ID"].ToString().Split(',');
                for (int i = 0; i < dtGridTable.Rows.Count; i++)
                {
                    if (candidateCompetencyVector == null)
                        candidateCompetencyVector = new CandidateCompetencyVector();
                    for (int j = 0; j < ParameterNames.Length; j++)
                    {
                        if (candidateCompetencyVector == null)
                            candidateCompetencyVector = new CandidateCompetencyVector();
                        candidateCompetencyVector.Vector_Id = Convert.ToInt32(dtGridTable.Rows[i]["VECTOR_ID"]); ;
                        candidateCompetencyVector.Old_Vector_Id = 0;
                        candidateCompetencyVector.Parameter_Id = Convert.ToInt32(ParameterIds.Split(',')[j]);
                        candidateCompetencyVector.Cand_Resume_Id = Cand_Resume_Id;
                        candidateCompetencyVector.CompetencyValue = dtGridTable.Rows[i][ParameterNames[j]].ToString();
                        candidateCompetencyVector.Remarks = dtGridTable.Rows[i][ParameterNames[j] + "_Remarks"].ToString();
                        candidateCompetencyVector.Created_By = UserId;
                        if (candidateCompetencyVectors == null)
                            candidateCompetencyVectors = new List<CandidateCompetencyVector>();
                        candidateCompetencyVectors.Add(candidateCompetencyVector);
                        candidateCompetencyVector = null;
                    }
                }
            }
            finally
            {
                if (candidateCompetencyVector != null) candidateCompetencyVector = null;
            }
        }

        private void GenerateCandidateVectorForDeleteList(ref List<CandidateCompetencyVector> candidateCompetencyVectors,
            DataTable dtGridTable, int Cand_Resume_Id)
        {
            CandidateCompetencyVector candidateCompetencyVector = null;
            try
            {
                for (int i = 0; i < dtGridTable.Rows.Count; i++)
                {
                    if (candidateCompetencyVector == null)
                        candidateCompetencyVector = new CandidateCompetencyVector();
                    candidateCompetencyVector.Vector_Id = Convert.ToInt32(dtGridTable.Rows[i]["OLD_VECTOR_ID"]);
                    candidateCompetencyVector.Cand_Resume_Id = Cand_Resume_Id;
                    if (candidateCompetencyVectors == null)
                        candidateCompetencyVectors = new List<CandidateCompetencyVector>();
                    candidateCompetencyVectors.Add(candidateCompetencyVector);
                    candidateCompetencyVector = null;
                }
            }
            finally
            {
                if (candidateCompetencyVector != null) candidateCompetencyVector = null;
            }
        }

        private void GenerateCandidateVectorsForChangeVectorId(ref List<CandidateCompetencyVector> candidateCompetencyVectors,
            DataTable dtGridTable, int Cand_Resume_Id, int UserId)
        {
            CandidateCompetencyVector candidateCompetencyVector = null;
            try
            {
                string ParameterIds = dtGridTable.Rows[0]["PARAMETER_ID"].ToString();
                string[] ParameterNames = dtGridTable.Rows[0]["PARAMETER_NAMES_ID"].ToString().Split(',');
                for (int i = 0; i < dtGridTable.Rows.Count; i++)
                {
                    if (dtGridTable.Rows[i]["VECTOR_ID"].Equals(dtGridTable.Rows[i]["OLD_VECTOR_ID"]))
                        continue;
                    if (candidateCompetencyVector == null)
                        candidateCompetencyVector = new CandidateCompetencyVector();
                    if (candidateCompetencyVector == null)
                        candidateCompetencyVector = new CandidateCompetencyVector();
                    candidateCompetencyVector.Old_Vector_Id = Convert.ToInt32(dtGridTable.Rows[i]["OLD_VECTOR_ID"]);
                    candidateCompetencyVector.Vector_Id = Convert.ToInt32(dtGridTable.Rows[i]["VECTOR_ID"]); ;
                    candidateCompetencyVector.Parameter_Id = 0;
                    candidateCompetencyVector.Cand_Resume_Id = Cand_Resume_Id;
                    candidateCompetencyVector.CompetencyValue = "";
                    candidateCompetencyVector.Remarks = "";
                    candidateCompetencyVector.Created_By = UserId;
                    if (candidateCompetencyVectors == null)
                        candidateCompetencyVectors = new List<CandidateCompetencyVector>();
                    candidateCompetencyVectors.Add(candidateCompetencyVector);
                    candidateCompetencyVector = null;
                }
            }
            finally
            {

                if (candidateCompetencyVector != null) candidateCompetencyVector = null;
            }
        }

        #endregion Save Code

        #region Re-Build vector from Project Details                           

        /// <summary>
        /// This method rebuilds the candidate competency vector from the project details.
        /// </summary>
        private void ReBuildCompetencyVector()
        {
            int Temp = 0;
            DataSet dsCompetencyVector = null;
            List<CandidateCompetencyVectorDetail> candidateCompetencyVectorDetail = null;
            try
            {
                candidateCompetencyVectorDetail =
                    new Forte.HCM.Utilities.CompetencyVectorManager().GetCompetencyVectors(
                    (List<Project>)Page.GetType().InvokeMember
                    ("GetProjectData", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null),
                    new CompetencyVectorBLManager().GetCompetencyVectorDetails
                    ("0", "", "", "AliasName", "A", 1, null, out Temp));
                if (candidateCompetencyVectorDetail == null)
                {
                    RolesVectorControl_errorMessageLabel.Text =
                            Resources.HCMResource.ResumeParser_CompetencyVector_ReBuildEmptyProjectDetails;
                    return;
                }
                if (ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE] == null)
                    dsCompetencyVector = GetCompetancyVectorTable(RolesVectorControl_candidateResumeIdHiddenField.Value == "" ?
                        -10 : Convert.ToInt32(RolesVectorControl_candidateResumeIdHiddenField.Value));
                else
                    dsCompetencyVector = (DataSet)ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE];
                DeletePreviousVectorRow(dsCompetencyVector.Tables[DataSetTableNames.MainTable.ToString()]);
                InsertNewVectorRows(candidateCompetencyVectorDetail,
                    dsCompetencyVector.Tables[DataSetTableNames.MainTable.ToString()]);
                CheckAndAddGroupTableRows(
                    dsCompetencyVector.Tables[DataSetTableNames.VectorGroup.ToString()],
                    dsCompetencyVector.Tables[DataSetTableNames.MainTable.ToString()]);
                if (ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE] == null)
                    ViewState[ROLESVECTOR_VECTORGRID_VIEWSTATE] = dsCompetencyVector;
                if (dsCompetencyVector.Tables[DataSetTableNames.MainTable.ToString()].
                        Select("ROWSTATUS_ID='" + VectorStatus.NewRowVector + "'").Length == 0)
                    RolesVectorControl_errorMessageLabel.Text =
                            Resources.HCMResource.ResumeParser_CompetencyVector_ReBuildNoVectorFound;
            }
            finally
            {
                if (candidateCompetencyVectorDetail != null) candidateCompetencyVectorDetail = null;
                if (dsCompetencyVector != null) dsCompetencyVector = null;
            }
        }

        /// <summary>
        /// This method checks and add new groups to the group datatable.
        /// </summary>
        /// <param name="dtGroupTable">
        /// A <see cref="DataTable"/> that contains the group table data.
        /// </param>
        /// <param name="dtMainTable">
        /// A <see cref="DataTable"/> that contains the candidate
        /// competency vectors.
        /// </param>
        private void CheckAndAddGroupTableRows(DataTable dtGroupTable, DataTable dtMainTable)
        {
            dtGroupTable.Rows.Clear();
            DataRow[] drGroupRows = null;
            DataRow drNewRow = null;
            try
            {
                drGroupRows = dtMainTable.Select("ROWSTATUS_ID='" + VectorStatus.NewRowVector + "'");
                for (int i = 0; i < drGroupRows.Length; i++)
                {
                    if (dtGroupTable.Select("GROUP_ID=" + drGroupRows[i]["GROUP_ID"]).Length > 0)
                        continue;
                    drNewRow = dtGroupTable.NewRow();
                    drNewRow["GROUP_ID"] = drGroupRows[i]["GROUP_ID"];
                    drNewRow["GROUP_NAME"] = drGroupRows[i]["GROUP_NAME"];
                    dtGroupTable.Rows.Add(drNewRow);
                    drNewRow = null;
                }
                dtMainTable.Columns.Remove("GROUP_NAME");
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(drGroupRows)) drGroupRows = null;
                if (!Utility.IsNullOrEmpty(drNewRow)) drNewRow = null;
            }
        }

        /// <summary>
        /// This method convert and insert the candidate competency vector list to the 
        /// system defined table format.
        /// </summary>
        /// <param name="candidateCompetencyVectorDetail">
        /// A <see cref="List"/> that contains the list of candidate competency
        /// vector based on project
        /// </param>
        /// <param name="dtMainTable">
        /// A <see cref="DataTable"/> that contains the candidate competency vector
        /// </param>
        private void InsertNewVectorRows(List<CandidateCompetencyVectorDetail> candidateCompetencyVectorDetail,
            DataTable dtMainTable)
        {
            int PreviousVectorId = 0;
            DataRow drNewRow = null;
            int RowCount = dtMainTable.Rows.Count + 1;
            string Parameter_Names = string.Empty;
            string Paramter_Ids = string.Empty;
            try
            {
                candidateCompetencyVectorDetail = candidateCompetencyVectorDetail.OrderBy(p => p.VectorID).ToList();
                if (dtMainTable.Rows.Count == 0)
                {
                    Parameter_Names = "No_Of_Years,Recency,Certification";
                    Paramter_Ids = "1,2,3";
                }
                else
                {
                    Parameter_Names = dtMainTable.Rows[dtMainTable.Rows.Count - 1]["PARAMETER_NAMES_ID"].ToString();
                    Paramter_Ids = dtMainTable.Rows[dtMainTable.Rows.Count - 1]["PARAMETER_ID"].ToString();
                }
                dtMainTable.Columns.Add("GROUP_NAME", typeof(string));
                for (int i = 0; i < candidateCompetencyVectorDetail.Count; i++)
                {
                    if (PreviousVectorId != candidateCompetencyVectorDetail[i].VectorID)
                    {
                        if (drNewRow != null)
                            dtMainTable.Rows.Add(drNewRow);
                        drNewRow = null;
                        PreviousVectorId = candidateCompetencyVectorDetail[i].VectorID;
                        drNewRow = dtMainTable.NewRow();
                        drNewRow["ROW_ID"] = RowCount++;
                        drNewRow["GROUP_ID"] = candidateCompetencyVectorDetail[i].VectorGroupID;
                        drNewRow["VECTOR_ID"] = candidateCompetencyVectorDetail[i].VectorID;
                        drNewRow["VECTOR_NAME"] = candidateCompetencyVectorDetail[i].VectorName;
                        drNewRow["ROWSTATUS_ID"] = VectorStatus.NewRowVector;
                        drNewRow["OLD_VECTOR_ID"] = drNewRow["VECTOR_ID"];
                        drNewRow["PARAMETER_ID"] = Paramter_Ids;
                        drNewRow["PARAMETER_NAMES_ID"] = Parameter_Names;
                        drNewRow["GROUP_NAME"] = candidateCompetencyVectorDetail[i].VectorGroup;
                    }
                    try
                    {
                        switch (candidateCompetencyVectorDetail[i].ParameterID)
                        {
                            case 1:
                                drNewRow["No_Of_Years"] = candidateCompetencyVectorDetail[i].Value;
                                break;
                            case 2:
                                drNewRow["Recency"] = candidateCompetencyVectorDetail[i].Value;
                                break;
                            case 3: drNewRow["Certification"] = candidateCompetencyVectorDetail[i].Value;
                                break;
                        }
                    }
                    catch (Exception) { }
                }
                if (drNewRow != null)
                    dtMainTable.Rows.Add(drNewRow);
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(drNewRow)) drNewRow = null;
            }
        }

        /// <summary>
        /// This method Change the status to delete of previous 
        /// competency vectors.
        /// </summary>
        /// <remarks>
        /// This method should call when the use clicks rebuild 
        /// competency vectors based on projects link clicked.
        /// </remarks>
        /// <param name="dtMainTable">
        /// A <see cref="DataTable"/> that contains the candidate competency
        /// vector.
        /// </param>
        private void DeletePreviousVectorRow(DataTable dtMainTable)
        {
            DataRow[] drCollection = null;
            try
            {
                drCollection = dtMainTable.Select("RowStatus_ID='" + VectorStatus.NewRowVector +
                    "' OR RowStatus_ID='" + VectorStatus.NewRow + "'");
                for (int i = 0; i < drCollection.Length; i++)
                    dtMainTable.Rows.Remove(drCollection[i]);
                drCollection = null;
                if (dtMainTable.Select("RowStatus_ID<>'" + VectorStatus.Delete + "'").Length == 0)
                    return;
                for (int i = 0; i < dtMainTable.Rows.Count; i++)
                    dtMainTable.Rows[i]["RowStatus_ID"] = VectorStatus.Delete;
            }
            finally
            {
                if (!Utility.IsNullOrEmpty(drCollection)) drCollection = null;
            }
        }

        #endregion Re-Build vector from Project Details

        #endregion Private Methods

    }       
}