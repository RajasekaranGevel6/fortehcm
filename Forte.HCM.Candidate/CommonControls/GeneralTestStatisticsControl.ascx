﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralTestStatisticsControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.GeneralTestStatisticsControl" %>
<%@ Register Src="PageNavigator.ascx" TagName="PageNavigator" TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/SingleSeriesChartControl.ascx" TagName="chart"
    TagPrefix="uc1" %>
<script type="text/javascript" language="javascript">
   
</script>
<div id="GeneralTestStatisticsControl_searchCriteriasDiv" runat="server" style="display: block;">
    <div style="height: 553px;">
        <div style="clear: both;">
            <div style="clear: both; float: left;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="margin-top: 5px;">
                        Test Summary
                    </div>
                </div>
                <div>
                    <div class="panel_body_bg_div">
                        <div>
                            <div class="test_statistics_container_left_1">
                               <asp:Label ID="GeneralTestStatisticsControl_noOfQuestionHeadLabel" runat="server"
                                    Text="Number Of Questions" SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_right_1">
                                <asp:Label ID="GeneralTestStatisticsControl_noOfQuestionLabel" runat="server" Text="25"
                                    SkinID="sknLabelDivFieldText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_left_2">
                                <asp:Label ID="GeneralTestStatisticsControl_highScoreHeadLabel" runat="server" Text="Highest Score"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_right_2">
                                <asp:Label ID="GeneralTestStatisticsControl_highScoreLabel" runat="server" Text="10"
                                    SkinID="sknLabelDivFieldText"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <div class="test_statistics_container_left_1">
                                <asp:Label ID="GeneralTestStatisticsControl_avgTimeHeadLabel" runat="server" Text="Average Time Taken By Candidates"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_right_1">
                                <asp:Label ID="GeneralTestStatisticsControl_avgTimeLabel" runat="server" Text="00:01:25"
                                    SkinID="sknLabelDivFieldText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_left_2">
                                <asp:Label ID="GeneralTestStatisticsControl_lowScoreHeadLabel" runat="server" Text="Lowest Score"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_right_2">
                                <asp:Label ID="GeneralTestStatisticsControl_lowScoreLabel" runat="server" Text="8"
                                    SkinID="sknLabelDivFieldText"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <div class="test_statistics_container_left_1">
                                <asp:Label ID="GeneralTestStatisticsControl_scoreRangeHeadLabel" runat="server" Text="Score Range"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_right_1">
                                <asp:Label ID="GeneralTestStatisticsControl_scoreRangeLabel" runat="server" Text="10"
                                    SkinID="sknLabelDivFieldText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_left_2">
                                <asp:Label ID="GeneralTestStatisticsControl_meanScoreHeadLabel" runat="server" Text="Mean Score"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_right_2">
                                <asp:Label ID="GeneralTestStatisticsControl_meanScoreLabel" runat="server" Text="10"
                                    SkinID="sknLabelDivFieldText"></asp:Label>
                            </div>
                        </div>
                        <div>
                            <div class="test_statistics_container_left_1">
                                
                            </div>
                            <div class="test_statistics_container_right_1">
                                
                            </div>
                            <div class="test_statistics_container_left_2">
                                <asp:Label ID="GeneralTestStatisticsControl_scoreSDHeadLabel" runat="server" Text="Standard Deviation"
                                    SkinID="sknLabelFieldHeaderText"></asp:Label>
                            </div>
                            <div class="test_statistics_container_right_2">
                                <asp:Label ID="GeneralTestStatisticsControl_scoreSDLabel" runat="server" Text="10"
                                    SkinID="sknLabelDivFieldText"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="float: right;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="margin-top: 5px;">
                        Categories / Subjects
                    </div>
                </div>
                <div class="panel_body_bg_div">
                    <div style="height: 130px; width: 461px; overflow: auto;">
                        <asp:GridView ID="GeneralTestStatisticsControl_categoryGridview" runat="server">
                            <Columns>
                                <asp:BoundField HeaderText="Category" DataField="CategoryName" />
                                <asp:BoundField HeaderText="Subject" DataField="SubjectName" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;">
            <div style="float: left; margin-top: 5px;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="margin-top: 5px;">
                        Assessment Content Distribution Amongst Categories
                    </div>
                </div>
                <div>
                    <div class="panel_body_bg_div">
                        <div style="height: 150px; overflow: auto;">
                            <div>
                                <uc1:chart ID="GeneralTestStatistics_categoryStatisticsChartControl" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="float: right; margin-top: 5px;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="margin-top: 5px;">
                        Assessment Content Distribution Amongst Subject
                    </div>
                </div>
                <div>
                    <div class="panel_body_bg_div">
                        <div style="height: 150px; overflow: auto;">
                            <div>
                                <uc1:chart ID="GeneralTestStatisticsControl_subjectChartStatisticsChartControl" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;">
            <div style="float: left; margin-top: 5px;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="margin-top: 5px;">
                        Assessment Content Distribution Amongst Test Area
                    </div>
                </div>
                <div>
                    <div class="panel_body_bg_div">
                        <div style="height: 150px; overflow: auto;">
                            <div>
                                <uc1:chart ID="GeneralTestStatisticsControl_testAreaStatistics" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="float: right; margin-top: 5px;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="margin-top: 5px;">
                        Assessment Content Distribution Amongst Complexity
                    </div>
                </div>
                <div class="panel_body_bg_div">
                    <div style="height: 150px; overflow: auto;">
                        <div>
                            <uc1:chart ID="GeneralTestStatisticsControl_complexityStatisticsChart" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
