﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategorySubjectControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.CategorySubjectControl" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%" style="table-layout: fixed; padding-right: 2px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="grid_header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="15%" class="header_text_bold">
                                                <asp:Literal ID="SearchCategorySubjectControl_categoryHeaderLiteral" runat="server"
                                                    Text="Category"></asp:Literal><span class="mandatory">&nbsp;*</span>
                                                <asp:HiddenField ID="SearchCategorySubjectControl_categoryIDHiddenField" runat="server" />
                                                <asp:HiddenField ID="SearchCategorySubjectControl_QuestionKey" runat="server" />
                                            </td>
                                            <td width="31%">
                                                <asp:TextBox ID="SearchCategorySubjectControl_categoryTextBox" runat="server" MaxLength="50"></asp:TextBox>
                                                <ajaxToolKit:AutoCompleteExtender ID="SearchCategorySubjectControl_categoryAutoCompleteExtender"
                                                    runat="server" ServicePath="~/AutoComplete.asmx" ServiceMethod="GetCategoryList"
                                                    TargetControlID="SearchCategorySubjectControl_categoryTextBox" MinimumPrefixLength="1"
                                                    CompletionListElementID="pnl" CompletionListCssClass="autocomplete_completionListElement"
                                                    EnableCaching="true" CompletionSetCount="12">
                                                </ajaxToolKit:AutoCompleteExtender>
                                                <asp:Panel ID="pnl" runat="server">
                                                </asp:Panel>
                                            </td>
                                            <td width="8%">
                                                <asp:LinkButton ID="SearchCategorySubjectControl_addLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                    Text="Add" ToolTip="Click here to add new category" OnClick="SearchCategorySubjectControl_addLinkButton_Click" />
                                                <span>&nbsp;&nbsp;|&nbsp;</span>
                                            </td>
                                            <td style="width: 34%">
                                                <asp:LinkButton ID="SearchCategorySubjectControl_selectLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Select" ToolTip="Click here to select a category"></asp:LinkButton>
                                                <asp:HiddenField ID="SearchCategorySubjectControl_categoryHiddenField" runat="server" />
                                            </td>
                                            <td width="12%">
                                                <asp:LinkButton ID="SearchCategorySubjectControl_removeAllLinkButton" runat="server"
                                                    SkinID="sknActionLinkButton" Text="Remove All" ToolTip="Click here to remove all the categories"
                                                    OnClick="SearchCategorySubjectControl_removeAllLinkButton_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="grid_body_bg" valign="top">
                                    <div style="width: 100%; height: 105px; float: left; overflow: auto">
                                        <asp:DataList ID="SearchCategorySubjectControl_categoryDataList" runat="server" RepeatColumns="2"
                                            RepeatDirection="Vertical" Width="100%" OnDeleteCommand="SearchCategorySubjectControl_categoryDataList_DeleteCommand">
                                            <ItemTemplate>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 20px;" class="td_padding_top_2">
                                                            <asp:ImageButton ID="SearchCategorySubjectControl_removeImageButton" runat="server"
                                                                SkinID="sknDeleteImageButton" ToolTip="Remove Category" CommandName="Delete"
                                                                CommandArgument='<%# Bind("CategoryID") %>' />
                                                        </td>
                                                        <td align="left">
                                                            <asp:Label ID="SearchCategorySubjectControl_categoryNameLabel" runat="server" SkinID="sknLabelText"
                                                                Text='<%# Bind("CategoryName") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%" style="table-layout: fixed; padding-left: 2px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="grid_header_bg">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="header_text_bold">
                                                <asp:Literal ID="SearchCategorySubjectControl_subjectHeaderLiteral" runat="server"
                                                    Text="Subject"></asp:Literal><span class='mandatory'>&nbsp;*</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="grid_body_bg">
                                    <asp:HiddenField ID="SearchCategorySubjectControl_SubjectIDToBeDeletedHiddenField"
                                        runat="server" />
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div style="height: 105px; width: 100%; float: left; overflow: auto">
                                                    <asp:GridView Width="100%" SkinID="CategorySubjectGridView" GridLines="None" ID="SearchCategorySubjectControl_subjectGridview"
                                                        runat="server" AutoGenerateColumns="false" OnRowDataBound="SearchCategorySubjectControl_subjectGridview_RowDataBound"
                                                        OnRowCommand="SearchCategorySubjectControl_subjectGridview_RowCommand" ShowHeader="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <table width="98%" cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td align="left" class="label_field_header_text" style="width: 45%">
                                                                                            <asp:Literal ID="SearchCategorySubjectControl_categoryNameLabel" runat="server" Text='<%# Bind("CategoryName") %>'></asp:Literal>
                                                                                            <asp:HiddenField ID="SearchCategorySubjectControl_categoryIdHiddenField" Value='<%# Bind("CategoryID") %>'
                                                                                                runat="server" />
                                                                                            <asp:HiddenField ID="SearchCategorySubjectControl_subjectIdHiddenField" runat="server"
                                                                                                Value='<%# Bind("SubjectID") %>' />
                                                                                            <asp:HiddenField ID="SearchCategorySubjectControl_selectedSubjectIdHiddenField" runat="server" />
                                                                                        </td>
                                                                                        <td align="right" style="width: 45%">
                                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:LinkButton ID="SearchCategorySubjectControl_selectAllLinkButton" runat="server"
                                                                                                            Text="Select&nbsp;All" CommandName="Select" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <span class="link_button">&nbsp;|&nbsp;</span>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:LinkButton ID="SearchCategorySubjectControl_clearAllLinkButton" runat="server"
                                                                                                            CommandName="ClearAll" Text="Clear&nbsp;All" SkinID="sknActionLinkButton"></asp:LinkButton>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="middle">
                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="SearchCategorySubjectControl_subjectComboTextBox" ReadOnly="true"
                                                                                                runat="server" Width="98%" Font-Size="X-Small"></asp:TextBox>
                                                                                            <asp:Panel ID="CategorySubjectControl_subjectPanel" runat="server" CssClass="popupControl">
                                                                                                <asp:CheckBoxList ID="SearchCategorySubjectControl_subjectCheckBoxList" RepeatColumns="1"
                                                                                                    runat="server" CellPadding="0" OnSelectedIndexChanged="SearchCategorySubjectControl_subjectCheckBoxList_SelectedIndexChanged"
                                                                                                    AutoPostBack="true">
                                                                                                </asp:CheckBoxList>
                                                                                            </asp:Panel>
                                                                                            <ajaxToolKit:PopupControlExtender ID="SearchCategorySubjectControl_subjectListPopupControlExtender"
                                                                                                runat="server" TargetControlID="SearchCategorySubjectControl_subjectComboTextBox"
                                                                                                PopupControlID="CategorySubjectControl_subjectPanel" OffsetX="2" OffsetY="2"
                                                                                                Position="Bottom">
                                                                                            </ajaxToolKit:PopupControlExtender>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <%--<asp:Panel ID="CategorySubjectControl_deleteCategoryConfirmPanel" runat="server"
                Style="display: none;" CssClass="popupcontrol_confirm_remove">
                <uc3:ConfirmMsgControl ID="CategorySubjectControl_confirmMessageControl" runat="server"
                    Message="Are you sure want to delete this category?" Title="Delete Category" />
            </asp:Panel>
            <div id="Div1" style="display: none;" runat="server">
                <asp:Button ID="CategorySubjectControl_hiddenButton" runat="server" />
            </div>
            <ajaxToolKit:ModalPopupExtender ID="SingleQuestionEntry_deleteCategoryPopupExtender"
                runat="server" PopupControlID="CategorySubjectControl_deleteCategoryConfirmPanel"
                TargetControlID="CategorySubjectControl_hiddenButton" BackgroundCssClass="modalBackground">
            </ajaxToolKit:ModalPopupExtender>--%>
            <asp:HiddenField ID="CategorySubjectControl_categoryIdToBeDeleted" runat="server" />
            <asp:HiddenField ID="CategorySubjectControl_subjectID" runat="server" />
            <asp:HiddenField ID="CategorySubjectControl_selectedSubjectIDs" runat="server" />
        </td>
    </tr>
</table>
