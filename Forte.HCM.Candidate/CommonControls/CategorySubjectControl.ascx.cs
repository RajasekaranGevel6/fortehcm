﻿
#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CategorySubjectControl.cs
// File that represents the user interface for adding categories and subjects.
// Also this will allow us to remove a category and along with their subjects.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives                                                          

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Class which defines the user interface for adding categories and subjects
    /// </summary>
    /// <remarks>
    /// This class is inherited from <see cref="UserControl"/> class.
    /// </remarks>
    public partial class CategorySubjectControl : UserControl
    {
        #region Custom Event Handler and Delegate                              

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;

        #endregion Custom Event Handler and Delegate                           

        #region Event Handler                                                  

        /// <summary>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Session["LAST_SUBJECT"] = null;
                    ViewState["SUBJECT_DATALIST"] = null;
                }
                SearchCategorySubjectControl_selectLinkButton.Attributes.Add
                    ("onclick", "javascript:return LoadSearchCategoryLookUp('" + 
                    SearchCategorySubjectControl_categoryHiddenField.ClientID + "');");

                if (SearchCategorySubjectControl_categoryHiddenField.Value.Length == 0)
                    return;

                BindCategories(SearchCategorySubjectControl_categoryHiddenField.Value);
                SearchCategorySubjectControl_categoryHiddenField.Value = string.Empty;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// This will perform to validate a category and then add to category list control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchCategorySubjectControl_addLinkButton_Click(object sender, EventArgs e)
        {
            string categoryTextValue = SearchCategorySubjectControl_categoryTextBox.Text.Trim();

            if (ControlMessageThrown != null)
                ControlMessageThrown(this, new ControlMessageEventArgs("", MessageType.Error));
            else
                return;

            try
            {
                // Check category text box is empty
                if (SearchCategorySubjectControl_categoryTextBox.Text.Length == 0)
                {
                    // If so, raise the event handler.
                    if (ControlMessageThrown != null)
                        ControlMessageThrown(this, new ControlMessageEventArgs
                            (Resources.HCMResource.SearchCategorySubjectControl_CategoryCannotBeEmpty, MessageType.Error));
                    return;
                }
                else
                {
                    // This part of the code will check if the category id contains non-numeric value
                    string[] categoryDetail = categoryTextValue.Split(new char[] { '|' });

                    if (categoryDetail.Length == 2)
                    {
                        int result = 0;
                        if (!int.TryParse(categoryDetail[1].ToString(), out result))
                        {
                            // If so, raise the event handler.
                            if (ControlMessageThrown != null)
                                ControlMessageThrown(this, new ControlMessageEventArgs
                                    (Resources.HCMResource.CategorySubjectControl_InvalidCategory, MessageType.Error));
                            return;
                        }
                    }
                }

                //Call this method to add a new category to the DataList
                BindCategories(SearchCategorySubjectControl_categoryTextBox.Text);

                //Clear the TextField once the category is added to the DataList
                SearchCategorySubjectControl_categoryTextBox.Text = string.Empty;
            }
            catch (Exception exp)
            {
                // Show the error log message to the user
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        /// <summary>
        /// It fires when select all or clear all linkbutton is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchCategorySubjectControl_subjectGridview_RowCommand
            (object sender, GridViewCommandEventArgs e)
        {
            // If the select all link button is clicked
            if (e.CommandName == "Select")
            {
                // Call the method to select all subjects
                CheckOrUncheckItems(true, Convert.ToInt32(e.CommandArgument));
            }

            // If the clear all link button is clicked
            if (e.CommandName == "ClearAll")
            {
                // Call the method to clear all the selection
                CheckOrUncheckItems(false, Convert.ToInt32(e.CommandArgument));
            }
        }

        /// <summary>
        /// It helps to bind the subjects to the respective controls (i.e. checkboxlist)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchCategorySubjectControl_subjectGridview_RowDataBound
            (object sender, GridViewRowEventArgs e)
        {
            try
            {
                string selectedSubjectName = string.Empty;

                List<Subject> lastSubjects = null;

                // Check if the LAST_SUBJECT is not equal to null
                // 
                if (Session["LAST_SUBJECT"] != null)
                {
                    lastSubjects = Session["LAST_SUBJECT"] as List<Subject>;
                }

                // Assign selected subjects list in viewstate
                // This is used when the page is EDIT MODE
                List<Subject> subjectsTobeSelected =
                    ViewState["SUBJECT_TO_BE_SELECTED"] as List<Subject>;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Find the subject checkboxlist control.
                    CheckBoxList SearchCategorySubjectControl_subjectCheckBoxList = (CheckBoxList)
                        e.Row.FindControl("SearchCategorySubjectControl_subjectCheckBoxList");

                    // Find category id hidden field and its value will be passed to
                    // the GetSubjects method in this event handler
                    HiddenField SearchCategorySubjectControl_categoryIdHiddenField = (HiddenField)
                        e.Row.FindControl("SearchCategorySubjectControl_categoryIdHiddenField");

                    int categoryID = Int32.Parse(SearchCategorySubjectControl_categoryIdHiddenField.Value);

                    // Find subject textbox
                    TextBox SearchCategorySubjectControl_subjectComboTextBox = (TextBox)
                        e.Row.FindControl("searchCategorySubjectControl_subjectComboTextBox");

                    // Find select all link button
                    LinkButton SearchCategorySubjectControl_selectAllLinkButton = (LinkButton)
                        e.Row.FindControl("SearchCategorySubjectControl_selectAllLinkButton");

                    // Assign current row index to the select all link button command argument
                    SearchCategorySubjectControl_selectAllLinkButton.CommandArgument = e.Row.RowIndex.ToString();

                    // Find clear all link button to assign the current row index to 
                    // its command argument property.
                    LinkButton SearchCategorySubjectControl_clearAllLinkButton = (LinkButton)
                        e.Row.FindControl("SearchCategorySubjectControl_clearAllLinkButton");

                    SearchCategorySubjectControl_clearAllLinkButton.CommandArgument = e.Row.RowIndex.ToString();

                    List<Subject> subjects = new List<Subject>();

                    // Retrieve the subjects against the category id
                    subjects = new CommonBLManager().GetSubjects
                        (Convert.ToInt32(SearchCategorySubjectControl_categoryIdHiddenField.Value));

                    //Bind subjects to the subject
                    SearchCategorySubjectControl_subjectCheckBoxList.DataSource = subjects;
                    SearchCategorySubjectControl_subjectCheckBoxList.DataTextField = "SubjectName";
                    SearchCategorySubjectControl_subjectCheckBoxList.DataValueField = "SubjectID";

                    SearchCategorySubjectControl_subjectCheckBoxList.DataBind();
                    SearchCategorySubjectControl_subjectComboTextBox.Text = "";

                    // This code segment is used in single question entry mode
                    if (lastSubjects != null)
                    {
                        // From the last selected items identity and set the selected subject.
                        foreach (ListItem item in SearchCategorySubjectControl_subjectCheckBoxList.Items)
                        {
                            int subjectID = Convert.ToInt32(item.Value);

                            foreach (Subject subject in lastSubjects)
                            {
                                if (subject.CategoryID == categoryID &&
                                    subject.SubjectID == subjectID)
                                {
                                    item.Selected = subject.IsSelected;
                                    if (item.Selected)
                                    {
                                        selectedSubjectName = selectedSubjectName + item.Text + ",";
                                    }
                                    break;
                                }
                            }

                        }
                    }

                    SearchCategorySubjectControl_subjectComboTextBox.Text =
                        selectedSubjectName.TrimEnd(',');

                    // This part of the code for Edit Mode
                    if (!IsPostBack)
                    {
                        selectedSubjectName = string.Empty;

                        // Loop through the subjects to get selected subjects list
                        foreach (Subject subject in subjectsTobeSelected)
                        {
                            // Check if the category id of current row is not equal to List.CategoryID
                            // Or Check if anyone subject selected status is false
                            if (categoryID != subject.CategoryID || !subject.IsSelected)
                                continue;

                            // Get currently selected item value
                            ListItem item =
                                SearchCategorySubjectControl_subjectCheckBoxList.Items.FindByValue
                                (subject.SubjectID.ToString());

                            // Make a selection to the subject
                            item.Selected = true;

                            // Add selected subjects with comma seperator.
                            selectedSubjectName = selectedSubjectName + item.Text + ",";
                        }

                        // Subjects will be shown when the single question entry page is loaded as edit mode.
                        SearchCategorySubjectControl_subjectComboTextBox.Text =
                            selectedSubjectName.ToString().TrimEnd(',');
                        
                    }
                }
            }
            catch (Exception exp)
            {
                // Show error message to the user
                Logger.ExceptionLog(exp);
                throw exp;
            }
        }

        protected void SearchCategorySubjectControl_subjectCheckBoxList_SelectedIndexChanged
            (object sender, EventArgs e)
        {
            string selectedSubjectName = "";
            CheckBoxList chkSampleStatus = sender as CheckBoxList;
            GridViewRow row = chkSampleStatus.NamingContainer as GridViewRow;
            TextBox SearchCategorySubjectControl_subjectComboTextBox = 
                row.FindControl("SearchCategorySubjectControl_subjectComboTextBox") as TextBox;

            HiddenField SearchCategorySubjectControl_subjectIdHiddenField = 
                row.FindControl("SearchCategorySubjectControl_subjectIdHiddenField") as HiddenField;

            foreach (ListItem item in chkSampleStatus.Items)
            {
                if (item.Selected)
                {
                    selectedSubjectName = selectedSubjectName + item.Text + ",";
                }
            }
            SearchCategorySubjectControl_subjectComboTextBox.Text = selectedSubjectName.TrimEnd(',');
        }

        /// <summary>
        /// This event handler is used to delete a selected category from the data list control
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void SearchCategorySubjectControl_categoryDataList_DeleteCommand
            (object source, DataListCommandEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ImageButton SearchCategorySubjectControl_removeImageButton = (ImageButton)
                    e.Item.FindControl("SearchCategorySubjectControl_removeImageButton");

                if (SearchCategorySubjectControl_removeImageButton.CommandName == "Delete")
                {
                    if (string.IsNullOrEmpty(SearchCategorySubjectControl_categoryIDHiddenField.Value))
                    {
                        SearchCategorySubjectControl_categoryIDHiddenField.Value =
                            SearchCategorySubjectControl_removeImageButton.CommandArgument;
                    }
                    else
                    {
                        SearchCategorySubjectControl_categoryIDHiddenField.Value =
                            SearchCategorySubjectControl_categoryIDHiddenField.Value + "," +
                            SearchCategorySubjectControl_removeImageButton.CommandArgument;
                    }
                    DeleteCategory(Convert.ToInt32(SearchCategorySubjectControl_removeImageButton.CommandArgument));
                }
            }
        }

        /// <summary>
        /// Remove all the data from the category and subject datalist controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchCategorySubjectControl_removeAllLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear category datalist
                SearchCategorySubjectControl_categoryDataList.DataSource = null;
                SearchCategorySubjectControl_categoryDataList.DataBind();

                // Clear the subject gridview
                SearchCategorySubjectControl_subjectGridview.DataSource = null;
                SearchCategorySubjectControl_subjectGridview.DataBind();

                // Free up the viewstate
                ViewState["CATEGORIES_DATASOURCE"] = null;
                Session["LAST_SUBJECT"] = null;

                //Remove the text from the alert label
                if (ControlMessageThrown != null)
                    ControlMessageThrown(this, new ControlMessageEventArgs("", MessageType.Warning));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                throw (exp);
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// This private method performs bind the categories according to the category id
        /// </summary>
        /// <param name="CategoryWithId"></param>
        private void BindCategories(string CategoryWithId)
        {
            //Category category = new Category();
            Subject subject = new Subject();

            string[] categoryDetail = CategoryWithId.Split(new char[] { '|' });

            // Validate whether both category name and id match with each other
            if (categoryDetail.Length == 2)
            {
                if (!new CommonBLManager().IsValidCategory
                    (Convert.ToInt32(categoryDetail[1]), categoryDetail[0].Trim()))
                {
                    // If fails
                    if (ControlMessageThrown != null)
                        ControlMessageThrown(this, new ControlMessageEventArgs("'" + categoryDetail[0].Trim() + "' and '"
                            + categoryDetail[1].Trim() + "' are mismatched", MessageType.Error));
                }
                else
                {
                    List<Subject> subjects = null;

                    // Retrieve details from the view state.
                    if (ViewState["CATEGORIES_DATASOURCE"] == null)
                        subjects = new List<Subject>();
                    else
                    {
                        subjects = ViewState["CATEGORIES_DATASOURCE"] as List<Subject>;
                    }

                    if (subjects != null)
                    {
                        // Check if the newly entered category is already exist.
                        Subject result = subjects.Find(
                            delegate(Subject cat)
                            {
                                return cat.CategoryID == Convert.ToInt32(categoryDetail[1]);
                            });

                        //If exists
                        if (result != null)
                        {
                            if (ControlMessageThrown != null)
                                ControlMessageThrown(this, new ControlMessageEventArgs
                                    ("Category '" + categoryDetail[0] + "' already exists", MessageType.Error));
                            return;
                        }
                    }

                    // Add the newly entered category to the list.
                    subjects.Add(new Subject(
                        int.Parse(categoryDetail[1].ToString()), categoryDetail[0].ToString()));

                    // Keep the category list in the view state.
                    ViewState["CATEGORIES_DATASOURCE"] = subjects;

                    // Assign the data source.
                    SearchCategorySubjectControl_categoryDataList.DataSource = subjects;
                    SearchCategorySubjectControl_categoryDataList.DataBind();

                    BindSubjects(subjects);
                }
            }
            else
            {
                // If invalid category is entered
                if (ControlMessageThrown != null)
                    ControlMessageThrown(this, new ControlMessageEventArgs(
                        Resources.HCMResource.CategorySubjectControl_InvalidCategory, MessageType.Error));
            }
        }

        /// <summary>
        /// This method helps to bind the subjects for the added category in the 
        /// category data list control
        /// </summary>
        /// <param name="subjects"></param>
        private void BindSubjects(List<Subject> subjects)
        {
            // This (this.SubjectDataSource) retrieves current data 
            // from the Subjects Gridview control
            Session["LAST_SUBJECT"] = this.SubjectDataSource;

            // Check if the passed argument (subjects) is null
            if (subjects.Count == 0 || subjects == null)
            {
                SearchCategorySubjectControl_subjectGridview.DataSource = null;
                SearchCategorySubjectControl_subjectGridview.DataBind();
            }
            else
            {
                // Bind subjects with subject id, name, category id
                SearchCategorySubjectControl_subjectGridview.DataSource = subjects;
                SearchCategorySubjectControl_subjectGridview.DataBind();
            }
        }

        /// <summary>
        /// This method helps to call a method which deletes a category
        /// against the category id
        /// </summary>
        /// <param name="categoryID"></param>
        private void DeleteCategory(int categoryID)
        {
            // Call confirm message control popup
            CategorySubjectControl_categoryIdToBeDeleted.Value = categoryID.ToString();

            ConfirmDeleteCategory(Convert.ToInt32(CategorySubjectControl_categoryIdToBeDeleted.Value));
        }

        /// <summary>
        /// Deletes the selected category.
        /// </summary>
        /// <param name="categoryID"></param>
        private void ConfirmDeleteCategory(int categoryID)
        {
            List<Subject> subjects = null;
            if (ViewState["CATEGORIES_DATASOURCE"] == null)
                return;

            // Retrieve subject details from the viewstate.
            subjects = ViewState["CATEGORIES_DATASOURCE"] as List<Subject>;

            // Find a category (to be deleted) index from the data source 
            int indexToDelete = -1;
            for (int index = 0; index < subjects.Count; index++)
            {
                if (categoryID == subjects[index].CategoryID)
                {
                    indexToDelete = index;
                    break;
                }
            }

            // Delete the category from the list.
            if (indexToDelete != -1)
                subjects.RemoveAt(indexToDelete);

            // Keep the category list in the view state.
            ViewState["CATEGORIES_DATASOURCE"] = subjects;

            List<Subject> subjestList = new List<Subject>();
            if (!Utility.IsNullOrEmpty(Session["LAST_SUBJECT"]))
                subjestList = Session["LAST_SUBJECT"] as List<Subject>;

            if (subjects.Count == 0 || subjects == null)
            {
                // Assign the data source.
                SearchCategorySubjectControl_categoryDataList.DataSource = null;
                SearchCategorySubjectControl_categoryDataList.DataBind();
            }
            else
            {
                // Assign the data source.
                SearchCategorySubjectControl_categoryDataList.DataSource = subjects;
                SearchCategorySubjectControl_categoryDataList.DataBind();
            }
            // Assign the data source to subject list.
            BindSubjects(subjects);
        }

        /// <summary>
        /// It helps us to check or uncheck checkbox items
        /// </summary>
        /// <param name="isCheck"></param>
        /// <param name="rowIndex"></param>
        private void CheckOrUncheckItems(bool isCheck, Int32 rowIndex)
        {
            GridViewRow row = SearchCategorySubjectControl_subjectGridview.Rows[rowIndex];
            
            CheckBoxList SearchCategorySubjectControl_subjectCheckBoxList = (CheckBoxList)
                row.FindControl("SearchCategorySubjectControl_subjectCheckBoxList");
            
            TextBox SearchCategorySubjectControl_subjectComboTextBox = (TextBox)
                                 row.FindControl("searchCategorySubjectControl_subjectComboTextBox");
            
            HiddenField SearchCategorySubjectControl_subjectIdHiddenField = 
                row.FindControl("SearchCategorySubjectControl_subjectIdHiddenField") as HiddenField;
            string selsectedSubjectName = "";

            for (int i = 0; i < SearchCategorySubjectControl_subjectCheckBoxList.Items.Count; i++)
            {
                SearchCategorySubjectControl_subjectCheckBoxList.Items[i].Selected = isCheck;

                if (isCheck)
                    selsectedSubjectName = selsectedSubjectName 
                        + SearchCategorySubjectControl_subjectCheckBoxList.Items[i].Text + ",";
            }
            SearchCategorySubjectControl_subjectComboTextBox.Text = selsectedSubjectName.TrimEnd(',');
        }

        #endregion Private Methods

        #region Public Properties                                              

        /// <summary>
        /// It can contain list of category ids which all to be deleted 
        /// from the question relation table 
        /// </summary>
        public string DeletedCategories
        {
            get
            {
                return (SearchCategorySubjectControl_categoryIDHiddenField.Value.ToString().Trim());
            }
        }

        /// <summary>
        /// Receives a question key from single/edit question entry page
        /// </summary>
        public string QuestionKey
        {
            set
            {
                SearchCategorySubjectControl_QuestionKey.Value = value;
            }
        }

        /// <summary>
        /// Datasource for category which is used by 'LoadQuestionDetails(string questionKey)'
        /// in the single question entry page to load categories in the category data list control.
        /// </summary>
        public List<Subject> CategoryDataSource
        {
            set
            {
                // Keep the category list in the view state.
                ViewState["CATEGORIES_DATASOURCE"] = value;

                // Assign the data source.
                SearchCategorySubjectControl_categoryDataList.DataSource = value;
                SearchCategorySubjectControl_categoryDataList.DataBind();
            }
            get
            {
                if (ViewState["CATEGORIES_DATASOURCE"] == null)
                    return null;

                return ViewState["CATEGORIES_DATASOURCE"] as List<Subject>;
            }
        }

        /// <summary>
        /// This Collection holds the subject list when the single question 
        /// entry page is opened as edit mode. Then 'SubjectsToBeSelected' collection
        /// iterated to get all the selected (i.e. IsSelected=true) subject list 
        /// in the RowDataBound event handler
        /// </summary>
        public List<Subject> SubjectsToBeSelected
        {
            set
            {
                ViewState["SUBJECT_TO_BE_SELECTED"] = value;
            }
        }

        /// <summary>
        /// This datasource is used to load subjects based on the categories
        /// Also it returns category information (id, name) and subject details(
        /// id, name, isSelected=true) to the caller
        /// </summary>
        public List<Subject> SubjectDataSource
        {
            set
            {
                // Keep the category list in session state.
                Session["LAST_SUBJECT"] = value;

                // Assign the data source.
                SearchCategorySubjectControl_subjectGridview.DataSource = value;
                SearchCategorySubjectControl_subjectGridview.DataBind();
            }
            get
            {
                List<Subject> subjects = null;
                string selectedSubjectIDs = string.Empty;

                // Loop through the subject and construct the list.
                foreach (GridViewRow row in SearchCategorySubjectControl_subjectGridview.Rows)
                {
                    CheckBoxList SearchCategorySubjectControl_subjectCheckBoxList = (CheckBoxList)
                        row.FindControl("SearchCategorySubjectControl_subjectCheckBoxList");

                    // Find category id hidden field
                    HiddenField SearchCategorySubjectControl_categoryIdHiddenField = (HiddenField)
                        row.FindControl("SearchCategorySubjectControl_categoryIdHiddenField");

                    // Find category name label
                    Literal SearchCategorySubjectControl_categoryNameLabel = (Literal)
                        row.FindControl("SearchCategorySubjectControl_categoryNameLabel");

                    if (subjects == null)
                        subjects = new List<Subject>();

                    // This loop helps to segregate the subjects which are selected and unselected by the user
                    for (int checkboxIndex = 0; checkboxIndex <
                        SearchCategorySubjectControl_subjectCheckBoxList.Items.Count; checkboxIndex++)
                    {
                        // Create and construct a subject object.
                        Subject subject = new Subject();
                        
                        subject.CategoryID = 
                            Convert.ToInt32(SearchCategorySubjectControl_categoryIdHiddenField.Value);
                        
                        subject.CategoryName = 
                            SearchCategorySubjectControl_categoryNameLabel.Text;

                        // If the subject is selected then add subject to the list with IsSelected=True
                        if (SearchCategorySubjectControl_subjectCheckBoxList.Items[checkboxIndex].Selected == true)
                        {
                            subject.SubjectID =
                                Convert.ToInt32(SearchCategorySubjectControl_subjectCheckBoxList.Items[checkboxIndex].Value);
                            subject.SubjectName = 
                                SearchCategorySubjectControl_subjectCheckBoxList.Items[checkboxIndex].Text;
                            subject.IsSelected = true;

                            selectedSubjectIDs = selectedSubjectIDs + subject.SubjectID + ",";
                        }
                        else
                        {
                            subject.SubjectID =
                                Convert.ToInt32(SearchCategorySubjectControl_subjectCheckBoxList.Items[checkboxIndex].Value);
                            subject.SubjectName = SearchCategorySubjectControl_subjectCheckBoxList.Items[checkboxIndex].Text;
                            subject.IsSelected = false;
                        }

                        // Add subject object to the list.
                        subjects.Add(subject);
                    }
                }
                
                // Assign selected subject ids to the hidden field.
                // This will be used for inserting multiple (subjects) rows 
                // to the question relation table
                CategorySubjectControl_selectedSubjectIDs.Value =
                    selectedSubjectIDs.ToString().TrimEnd(',');

                return subjects;
            }
        }

        /// <summary>
        /// Returns selected subject ids list. The hidden fields will get the
        /// subject ids from SubjectDataSource property in GET method.
        /// This property (SelectedSubjectIDList) will be used in 
        /// SingleQuestionEntry:SaveQuestionDetails
        /// method to assign QuestionDetail object.
        /// </summary>
        public string SelectedSubjectIDList
        {
            get
            {
                return CategorySubjectControl_selectedSubjectIDs.Value.ToString().Trim();
            }
        }
        #endregion  Public Properties
    }
}