﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Forte.HCM.UI.CommonControls {
    
    
    public partial class TestResultHeaderControl {
        
        /// <summary>
        /// TestResultHeaderControl_candidateNameValueTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_candidateNameValueTextBox;
        
        /// <summary>
        /// TestResultHeaderControl_testNameValueTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_testNameValueTextBox;
        
        /// <summary>
        /// TestResultHeaderControl_testCompletedValueTextBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TestResultHeaderControl_testCompletedValueTextBox;
    }
}
