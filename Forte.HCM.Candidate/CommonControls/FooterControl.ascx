﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.FooterControl" %>
    <div style="width:950px">
        <div class ="line_thick">
            
        </div>
        <div>
           <div style="text-align:center" class="footer_content">© 2012. Forte HCM. All Rights Reserved.</div> 
           <div style="float:right;text-align:center" class="footer_menu_text">                           
                    <asp:LinkButton ID="FooterControl_aboutUsLinkButton" runat="server" Text="About Us"
                CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/AboutUs.aspx"></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="FooterControl_feedbackLinkButton" runat="server" Text="Feedback"
                CssClass="footer_menu_text_feedbackLinkButton" PostBackUrl="~/General/Feedback.aspx"></asp:LinkButton>
           </div>
        </div>
    </div> 
