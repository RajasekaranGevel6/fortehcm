﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestResulsCanditateControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.TestResulsCanditateControl" %>
<%@ Register Src="~/CommonControls/MultipleSeriesChartControl.ascx" TagName="MultipleSeriesChartControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/CommonControls/HistogramChartControl.ascx" TagName="HistogramChartControl"
    TagPrefix="uc4" %>
<div id="TestResulsCanditateControl_mainDiv">
    <div style="height: 428px;">
        <div style="clear: both;">
            <div style="float: left;">
                <div>
                    <div class="my_report_bg_div">
                        <div class="candidate_statistics_container_left">
                            <asp:Label ID="TestResulsCanditateControl_myScoreLabel" runat="server" Text="Score" CssClass="label_field_header_text_score">
                            </asp:Label>
                        </div>
                        <div class="candidate_statistics_container_right">
                            <asp:Label ID="TestResulsCanditateControl_myScoreLabelValue" runat="server"
                                Font-Bold="True" CssClass="label_field_text_score_div"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_left">
                            <asp:Label ID="TestResulsCanditateControl_percentileLabel" runat="server" Text="Percentile"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_right">
                            <asp:Label ID="TestResulsCanditateControl_percentileLabelValue" runat="server" SkinID="sknLabelDivFieldText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_left">
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionHeadLabel" runat="server" Text="Number Of Questions Attended"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_right">
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionLabelValue" runat="server"
                                SkinID="sknLabelDivFieldText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_left">
                            <asp:Label ID="TestResulsCanditateControl_percentageQuestionsAnsweredCorrectlyLabel"
                                runat="server" Text="Percentage Of Questions Answered Right" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_right">
                            <asp:Label ID="TestResulsCanditateControl_percentageQuestionsAnsweredCorrectlyLabelValue"
                                runat="server" SkinID="sknLabelDivFieldText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_left">
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionTimeTakenLabel" runat="server"
                                Text="Time Taken To Complete Test" SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_right">
                            <asp:Label ID="TestResulsCanditateControl_noOfQuestionTimeTakenLabelValue" runat="server"
                                SkinID="sknLabelDivFieldText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_left">
                            <asp:Label ID="TestResulsCanditateControl_avgTimeTakenLabel" runat="server" Text="Average Time Taken Per Question"
                                SkinID="sknLabelFieldHeaderText"></asp:Label>
                        </div>
                        <div class="candidate_statistics_container_right">
                            <asp:Label ID="TestResulsCanditateControl_avgTimeTakenLabelValue" runat="server"
                                SkinID="sknLabelDivFieldText"></asp:Label>
                        </div>
                        <%-- <div style="height: 185px; overflow: auto;">
                            <uc4:HistogramChartControl ID="TestResultsControl_histogramChartControl" runat="server" />
                        </div>--%>
                    </div>
                </div>
            </div>
            <div style="float: right;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="padding-top: 5px;">
                        Score Distribution Amongst Categories</div>
                </div>
                <div>
                    <div class="panel_body_bg_div" style="height: 175px;">
                        <div style="height: 185px; overflow: auto;">
                            <uc3:MultipleSeriesChartControl ID="TestResultControl_multipleSeriesChartControl"
                                runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both; padding-top: 10px;">
            <div style="float: left;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="padding-top: 5px;">
                        Score Distribution Amongst Subjects</div>
                </div>
                <div>
                    <div class="panel_body_bg_div" style="height: 180px;">
                        <div style="height: 185px; overflow: auto;">
                            <uc3:MultipleSeriesChartControl ID="MultipleSeriesChartControl_subjectChart" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div style="float: right;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="padding-top: 5px;">
                        Score Distribution Amongst Test Areas
                    </div>
                </div>
                <div>
                    <div class="panel_body_bg_div" style="height: 180px;">
                        <div style="height: 185px; overflow: auto;">
                            <uc3:MultipleSeriesChartControl ID="MultipleSeriesChartControl_testAreaChart" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div style="clear: both; padding-top: 10px;display:none;">
            <div style="float: left;">
                <div class="grid_header_bg_div">
                    <div class="header_text_bold" style="padding-top: 5px;">
                        Score Distribution Amongst Complexity</div>
                </div>
                <div>
                    <div class="panel_body_bg_div" style="height: 180px;">
                        <div style="height: 185px; overflow: auto;">
                            <uc3:MultipleSeriesChartControl ID="MultipleSeriesChartControl_complexityChart" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
