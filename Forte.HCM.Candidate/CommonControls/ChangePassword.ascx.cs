﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ChangePassword.cs
// File that represents the user interface for the Change Password .

#endregion Header

#region Directives                                                             
using System;
using System.Web.UI;

using Resources;

using Forte.HCM.BL;
using Forte.HCM.DataObjects;
using System.Web.Security;
using System.Configuration;

#endregion Directives
namespace Forte.HCM.UI.CommonControls
{
    public partial class ChangePassword : UserControl
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               // Page.Form.DefaultButton = ChangePassword_sendButton.UniqueID;
                //Page.Form.DefaultFocus = ChangePassword_oldPasswordTextBox.UniqueID;
                if (IsPostBack)
                {
                    ChangePassword_panel.Visible = true;
                    ChangePassword_sendButton.Visible = true;
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the ChangePassword_sendButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ChangePassword_sendButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid())
                {
                    UserDetail userDetail = new UserDetail();
                    userDetail = new UserRegistrationBLManager().GetPassword(((UserDetail)Session["USER_DETAIL"]).UserID);
                    if (new HCM.Utilities.EncryptAndDecrypt().DecryptString(userDetail.Password) != ChangePassword_oldPasswordTextBox.Text.Trim())
                    {
                        ShowErrorMessage(HCMResource.ChangePassword_CurrentCorrectPassword);
                        return;
                    }
                    new UserRegistrationBLManager().UpdatePassword(new HCM.Utilities.EncryptAndDecrypt().EncryptString(
                        ChangePassword_newPasswordTextBox.Text.Trim()), ((UserDetail)Session["USER_DETAIL"]).UserID);
                    ChangePassword_topSuccessMessageLabel.Text= HCMResource.ChangePassword_Successfully;

                    ChangePassword_panel.Visible = false;
                    ChangePassword_sendButton.Visible = false;

                  
                }
            }
            catch (Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
            finally
            {
                InvokePageBaseMethod();
            }
        }
        #endregion

        #region Private Methods                                                
                                              
        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </returns>
        private bool IsValid()
        {
            bool validFlag = true;
            if (Support.Utility.IsNullOrEmpty(ChangePassword_oldPasswordTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.ChangePassword_CurrentPasswordEmpty);
                validFlag = false;
            }
            if (Support.Utility.IsNullOrEmpty(ChangePassword_newPasswordTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.ChangePassword_NewPasswordEmpty);
                validFlag = false;
            }
            else if (ChangePassword_newPasswordTextBox.Text.Trim().Length < 6)
            {
                ShowErrorMessage(HCMResource.ChangePassword_MinimumPasswordFailed);
                validFlag = false;
            }
            if (Support.Utility.IsNullOrEmpty(ChangePassword_confirmPasswordTextBox.Text.Trim()))
            {
                ShowErrorMessage(HCMResource.ChangePassword_ConfirmPasswordEmpty);
                validFlag = false;
            }
            if ((ChangePassword_confirmPasswordTextBox.Text.Trim()) != ChangePassword_newPasswordTextBox.Text.Trim())
            {
                ShowErrorMessage(HCMResource.ChangePassword_PasswordMisMatch);
                validFlag = false;
            }
            return validFlag;
        }

        /// <summary>
        /// Shows the error message.
        /// </summary>
        /// <param name="Message">The message.</param>
        private void ShowErrorMessage(string Message)
        {
            if (string.IsNullOrEmpty(ChangePassword_topErrorMessageLabel.Text))
                ChangePassword_topErrorMessageLabel.Text = Message;
            else
                ChangePassword_topErrorMessageLabel.Text += "<br />" + Message;
        }

        /// <summary>
        /// A Method that shows the error message to the user
        /// </summary>
        /// <param name="exp">
        /// A <see cref="System.Exception"/> that holds the exception object.
        /// </param>
        private void ShowErrorMessage(Exception exp)
        {
            if (string.IsNullOrEmpty(ChangePassword_topErrorMessageLabel.Text))
                ChangePassword_topErrorMessageLabel.Text = exp.Message;
            else
                ChangePassword_topErrorMessageLabel.Text += "<br />" + exp.Message;
            InvokePageBaseMethod();
        }

        /// <summary>
        /// Invokes the page base method.
        /// </summary>
        private void InvokePageBaseMethod()
        {
            try
            {
                if (ChangePassword_topSuccessMessageLabel.Text == HCMResource.ChangePassword_Successfully)
                {
                    Session["USER_DETAIL"] = null;
                    Session.Abandon();
                    //  HomeHeaderControl_loginUserNameLabel.Text = "Guest";
                    FormsAuthentication.SignOut();
                    //FormsAuthentication.RedirectToLoginPage();
                   // Response.Redirect(ConfigurationManager.AppSettings["SITE_URL"], false);
                    string[] uri = Request.Url.AbsoluteUri.Split('/');
                    if (uri.Length > 4)
                    {
                        // Compose home url.

                        Response.Redirect(uri[0] + "/" + uri[1] + ConfigurationManager.AppSettings["SITE_URL"], false);
                    }
                }
            }
            catch(Exception exp)
            {
                ShowErrorMessage(exp.Message);
            }
        }
        #endregion 
    }
}