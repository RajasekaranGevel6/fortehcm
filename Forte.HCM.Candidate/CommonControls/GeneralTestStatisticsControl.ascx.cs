﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// QuestionSettings.cs
// File that represents the user interface for question settings page.
// This will helps to view and set the question settings such as categories, 
// subjects and test areas.

#endregion Header

#region  Directives
using System;
using System.Collections.Generic;


using System.Web.UI;
using Forte.HCM.BL;
using Forte.HCM.DataObjects;
using System.Web.UI.DataVisualization.Charting;
using Forte.HCM.Trace;
using Forte.HCM.Support;
#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    /// <summary>
    /// Represents the class that holds the User Interface and 
    /// fucntionalities of the GeneralTestStatisticsControl control.
    /// This page helps to view the  various statistics and count of the 
    /// questions in the test
    /// </summary>
    public partial class GeneralTestStatisticsControl : UserControl
    {
        #region Properties
        /// <summary>
        /// Represents the property TestStatisticsDataSource
        /// </summary>
        public TestStatistics TestStatisticsDataSource
        {
            set;
            get;
        }
        #endregion Properties

        #region Handler Events
        /// <summary>
        /// Hanlder method that will be called when the control is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion Handler Events

        #region Public Methods
        /// <summary>
        /// Method to bind the summary details
        /// </summary>
        public void LoadTestSummaryDetails()
        {
            try
            {
                GeneralTestStatisticsControl_highScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.HighestScore.ToString().Trim());
                
                GeneralTestStatisticsControl_noOfQuestionLabel.Text =
                    TestStatisticsDataSource.NoOfQuestions.ToString().Trim();

                GeneralTestStatisticsControl_lowScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.LowestScore.ToString().Trim());
                
                GeneralTestStatisticsControl_meanScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.MeanScore.ToString().Trim());
                
                GeneralTestStatisticsControl_avgTimeLabel.Text =
                    Utility.ConvertSecondsToHoursMinutesSeconds(
                     Convert.ToInt32(TestStatisticsDataSource.
                    AverageTimeTakenByCandidates));

                if (!Utility.IsNullOrEmpty(GeneralTestStatisticsControl_avgTimeLabel.Text))
                {
                    int avgTimeTaken = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(
                        GeneralTestStatisticsControl_avgTimeLabel.Text));
                    GeneralTestStatisticsControl_avgTimeLabel.Text =
                        Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(avgTimeTaken);
                }
                GeneralTestStatisticsControl_scoreSDLabel.Text =
                TestStatisticsDataSource.StandardDeviation.ToString();

                GeneralTestStatisticsControl_scoreRangeLabel.Text = "TBD";
                //TestStatisticsDataSource.ScoreRange.ToString();


                //Load the datagrid for category and subject control

                GeneralTestStatisticsControl_categoryGridview.DataSource =
                   TestStatisticsDataSource.SubjectStatistics;

                GeneralTestStatisticsControl_categoryGridview.DataBind();

                //Load the category statistics chart

                TestStatisticsDataSource.CategoryStatisticsChartData.ChartType
                                                        = SeriesChartType.Pie;

                TestStatisticsDataSource.CategoryStatisticsChartData.ChartLength
                                                            = 140;

                TestStatisticsDataSource.CategoryStatisticsChartData.ChartWidth
                                                            = 400;

                TestStatisticsDataSource.CategoryStatisticsChartData.ChartTitle
                                                        = "Assessment Content Distribution Amongst Categories";

                TestStatisticsDataSource.CategoryStatisticsChartData.ChartImageName =
                    Constants.ChartConstants.CHART_CATEGORY + "-" + TestStatisticsDataSource.TestID;

                TestStatisticsDataSource.CategoryStatisticsChartData.IsShowLabel = true;
                GeneralTestStatistics_categoryStatisticsChartControl.DataSource
                          = TestStatisticsDataSource.CategoryStatisticsChartData;



                //Load the subject statistics chart

                TestStatisticsDataSource.SubjectStatisticsChartData.
                    ChartType = SeriesChartType.Pie;

                TestStatisticsDataSource.SubjectStatisticsChartData.
                    ChartLength = 140;

                TestStatisticsDataSource.SubjectStatisticsChartData.
                    ChartWidth = 400;

                TestStatisticsDataSource.SubjectStatisticsChartData.
                    ChartTitle = "Assessment Content Distribution Amongst Subject";

                TestStatisticsDataSource.SubjectStatisticsChartData.ChartImageName =
                    Constants.ChartConstants.CHART_SUBJECT + "-" + TestStatisticsDataSource.TestID;

                TestStatisticsDataSource.SubjectStatisticsChartData.IsShowLabel = true;

                GeneralTestStatisticsControl_subjectChartStatisticsChartControl.
                    DataSource = TestStatisticsDataSource.SubjectStatisticsChartData;

                //Load the test area statistics chart

                TestStatisticsDataSource.TestAreaStatisticsChartData.
                    ChartType = SeriesChartType.Pie;

                TestStatisticsDataSource.TestAreaStatisticsChartData.
                    ChartLength = 140;

                TestStatisticsDataSource.TestAreaStatisticsChartData.
                    ChartWidth = 400;

                TestStatisticsDataSource.TestAreaStatisticsChartData.
                    ChartTitle = "Assessment Content Distribution Amongst Test Area";

                TestStatisticsDataSource.TestAreaStatisticsChartData.ChartImageName =
                    Constants.ChartConstants.CHART_TESTAREA + "-" + TestStatisticsDataSource.TestID;

                TestStatisticsDataSource.TestAreaStatisticsChartData.IsShowLabel = true;

                GeneralTestStatisticsControl_testAreaStatistics.DataSource =
                TestStatisticsDataSource.TestAreaStatisticsChartData;

                //Load the complexity statistics chart

                TestStatisticsDataSource.ComplexityStatisticsChartData.
                    ChartType = SeriesChartType.Pie;

                TestStatisticsDataSource.ComplexityStatisticsChartData.
                    ChartLength = 140;

                TestStatisticsDataSource.ComplexityStatisticsChartData.
                    ChartWidth = 400;

                TestStatisticsDataSource.ComplexityStatisticsChartData.
                    ChartTitle = "Assessment Content Distribution Amongst Complexity";

                TestStatisticsDataSource.ComplexityStatisticsChartData.ChartImageName =
                   Constants.ChartConstants.CHART_COMPLEXITY + "-" + TestStatisticsDataSource.TestID;

                TestStatisticsDataSource.ComplexityStatisticsChartData.IsShowLabel = true;

                GeneralTestStatisticsControl_complexityStatisticsChart.
                    DataSource =
                TestStatisticsDataSource.ComplexityStatisticsChartData;

            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                throw exception;
            }
        }
        #endregion Public Methods

    }
}