﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExecutiveSummaryControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ExecutiveSummaryControl" %>
<div id="MyResume_ExecutiveSummary_MainDiv">
    <div id="MyResume_ExecutiveSummary_Controls" >
        <div id="ExecutiveSummaryControl_controlsDiv" style="display: block;" runat="server" >
            <asp:HiddenField ID="ExecutiveSummaryControl_hiddenField" runat="server" />
            <asp:TextBox ID="ExecutiveSummaryControl_execSummaryTextBox" runat="server" Height="290"
                Width="950px" TextMode="MultiLine" MaxLength="5000" onkeyup="CommentsCount(5000,this)"
                onchange="CommentsCount(5000,this)" SkinID="sknCanResumeTextBox"></asp:TextBox>
        </div>
    </div>
</div>
