﻿#region Directives                                                              

using System;
using System.Web.UI;

using Forte.HCM.Trace;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class InterviewDetailControl : UserControl
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InterviewDetailControl_confirmStartInterviewMsgControl.Message = "Are you ready to start the interview ?";
                    InterviewDetailControl_confirmStartInterviewMsgControl.Title = "Start Interview";
                    InterviewDetailControl_confirmStartInterviewMsgControl.Type = MessageBoxType.YesNo;

                    InterviewDetailControl_confirmContinueInterviewMsgControl.Message = "Are you ready to continue the interview ?";
                    InterviewDetailControl_confirmContinueInterviewMsgControl.Title = "Continue Interview";
                    InterviewDetailControl_confirmContinueInterviewMsgControl.Type = MessageBoxType.YesNo;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        public CandidateInterviewSessionDetail DataSource
        {
            set
            {
                if (value == null)
                {
                    InterviewDetailControl_activityDiv.Visible = false;
                    InterviewDetailControl_noActivityDiv.Visible = true;
                    ClearValues();
                    return;
                }

                InterviewDetailControl_activityDiv.Visible = true;
                InterviewDetailControl_noActivityDiv.Visible = false;

                // Candidate session ID.
                InterviewDetailControl_candidateSessionIDHiddenField.Value = value.CandidateTestSessionID;
                InterviewDetailControl_attemptIDHiddenField.Value = value.AttemptID.ToString();

                // Test name.
                InterviewDetailControl_interviewNameLabel.Text = value.InterviewTestName;

                // Test description.
                InterviewDetailControl_interviewDescriptionLabel.Text = value.InterviewTestDescription;

                // Scheduled By.
                InterviewDetailControl_scheduledByValueLabel.Text = value.SchedulerName;

                // Created date.
                InterviewDetailControl_createdDateValueLabel.Text = string.Format("{0}", value.CreatedDateFormatted);

                // Expiry date.
                InterviewDetailControl_expiryDateValueLabel.Text = string.Format("{0}", value.ExpiryDateFormatted);

                // Add handler for 'set test reminder' image and link button.
                InterviewDetailControl_setInterviewReminderImageButton.Attributes.Add("OnClick",
                    "javascript:return OpenTestReminderFromHome('INT','" + InterviewDetailControl_candidateSessionIDHiddenField.Value + "','" + InterviewDetailControl_attemptIDHiddenField.Value + "')");
                InterviewDetailControl_setInterviewReminderLinkButton.Attributes.Add("OnClick",
                    "javascript:return OpenTestReminderFromHome('INT','" + InterviewDetailControl_candidateSessionIDHiddenField.Value + "','" + InterviewDetailControl_attemptIDHiddenField.Value + "')");

                // Add hander for 'email scheduler' image and link button.
                InterviewDetailControl_emailSchedulerImageButton.Attributes.Add("OnClick",
                    "javascript:return EmailSchedulerFromHome('" + InterviewDetailControl_candidateSessionIDHiddenField.Value + "','" + InterviewDetailControl_attemptIDHiddenField.Value + "','INT')");
                InterviewDetailControl_emailSchedulerLinkButton.Attributes.Add("OnClick",
                    "javascript:return EmailSchedulerFromHome('" + InterviewDetailControl_candidateSessionIDHiddenField.Value + "','" + InterviewDetailControl_attemptIDHiddenField.Value + "','INT')");

                if (value.IsPaused)
                {
                    InterviewDetailControl_startInterviewImageButton.Visible = false;
                    InterviewDetailControl_continueInterviewImageButton.Visible = true;
                }
                else
                {
                    InterviewDetailControl_startInterviewImageButton.Visible = true;
                    InterviewDetailControl_continueInterviewImageButton.Visible = false;
                }
            }
        }

        #endregion Event Handlers

        private void ClearValues()
        {
            InterviewDetailControl_candidateSessionIDHiddenField.Value = string.Empty;
            InterviewDetailControl_attemptIDHiddenField.Value = string.Empty;
            InterviewDetailControl_interviewNameLabel.Text = string.Empty;
            InterviewDetailControl_interviewDescriptionLabel.Text = string.Empty;

            InterviewDetailControl_scheduledByValueLabel.Text = string.Empty;
            InterviewDetailControl_createdDateValueLabel.Text = string.Empty;
            InterviewDetailControl_expiryDateValueLabel.Text = string.Empty;
        }

        protected void InterviewDetailControl_showInterviewIntroductionImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            // Redirect to test introduction page.
            Response.Redirect("~/InterviewCenter/InterviewIntroduction.aspx" +
               "?candidatesessionid=" + InterviewDetailControl_candidateSessionIDHiddenField.Value +
               "&attemptid=" + InterviewDetailControl_attemptIDHiddenField.Value +
               "&parentpage=ACT_HOME", false);
        }

        protected void InterviewDetailControl_showInterviewIntroductionLinkButton_Click
            (object sender, EventArgs e)
        {
            // Redirect to test introduction page.
            Response.Redirect("~/InterviewCenter/InterviewIntroduction.aspx" +
               "?candidatesessionid=" + InterviewDetailControl_candidateSessionIDHiddenField.Value +
               "&attemptid=" + InterviewDetailControl_attemptIDHiddenField.Value +
               "&parentpage=ACT_HOME", false);
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start interview confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void InterviewDetailControl_startInterviewButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/InterviewCenter/InterviewInstructions.aspx" +
                "?candidatesessionid=" + InterviewDetailControl_candidateSessionIDHiddenField.Value +
                "&attemptid=" + InterviewDetailControl_attemptIDHiddenField.Value +
                "&parentpage=CAND_HOME", false);
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the continue interview confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void InterviewDetailControl_continueInterviewButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/InterviewCenter/ContinueInterviewInstructions.aspx" +
                "?candidatesessionid=" + InterviewDetailControl_candidateSessionIDHiddenField.Value +
                "&attemptid=" + InterviewDetailControl_attemptIDHiddenField.Value +
                "&parentpage=CAND_HOME", false);
        }
    }
}