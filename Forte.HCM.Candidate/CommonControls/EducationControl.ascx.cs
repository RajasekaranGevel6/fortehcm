﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EducationControl.cs
// File that represents the user interface for the educational information details

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class EducationControl : System.Web.UI.UserControl
    {
        
        #region Custom Event Handler and Delegate                              

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;
        private int _rowCounter;
        #endregion Custom Event Handler and Delegate

        #region Event Handlers                                                 
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void EducationControl_addDefaultButton_Click(object sender, EventArgs e)
        {
            SetViewState();
            EducationControl_listView.DataSource = null;
            if (ViewState["dataSource"] != null)
            {
                this.dataSource = (List<Education>)ViewState["dataSource"];
            }
            else
            {
                this.dataSource = new List<Education>();
            }
            this.dataSource.Add(new Education());
            ViewState["dataSource"] = this.dataSource;
            _rowCounter = this.dataSource.Count;
            EducationControl_listView.DataSource = this.dataSource;
            EducationControl_listView.DataBind();
            SetDate();

        }

        protected void EducationControl_deleteButton_Click(object sender, EventArgs e)
        {
            ListViewDataItem lv = (ListViewDataItem)((Button)(sender)).Parent;
            this.dataSource = (List<Education>)ViewState["dataSource"];
            this.dataSource.RemoveAt(lv.DisplayIndex);
            EducationControl_listView.DataSource = dataSource;
            EducationControl_listView.DataBind();
            ViewState["dataSource"] = dataSource;
            SetDate();
        }

        protected void DeleteRowImagebutton_Click(object sender, EventArgs e)
        {
            ListViewDataItem lv = (ListViewDataItem)((ImageButton)(sender)).Parent;
            this.dataSource = (List<Education>)ViewState["dataSource"];
            this.dataSource.RemoveAt(lv.DisplayIndex);
            EducationControl_listView.DataSource = dataSource;
            EducationControl_listView.DataBind();
            ViewState["dataSource"] = dataSource;
            SetDate();
        }

        protected void EducationControl_okPopUpClick(object sender, EventArgs e)
        {
            int intDeletedRowIndex = Convert.ToInt32(EducationControl_deletedRowHiddenField.Value);
            if (intDeletedRowIndex != 0)
            {               
                this.dataSource = (List<Education>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intDeletedRowIndex-1);               
            }
            else
            {
                int intLastRecord = EducationControl_listView.Items.Count-1;
                this.dataSource = (List<Education>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intLastRecord);               
            }
            _rowCounter = dataSource.Count;
            EducationControl_listView.DataSource = dataSource;
            EducationControl_listView.DataBind();
            SetViewState();
            SetDate();       
            
            // Fire the message event
            if (ControlMessageThrown != null)
                ControlMessageThrown(this, new ControlMessageEventArgs("Education detail deleted successfully", MessageType.Success));
        }
          
        protected void EducationControl_listView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Button EducationControl_addButton = ((Button)e.Item.FindControl
            ("EducationControl_addButton"));
            Button EducationControl_deleteButton = ((Button)e.Item.FindControl
                 ("EducationControl_deleteButton"));
            TextBox EducationControl_nameTextBox = ((TextBox)e.Item.FindControl
                 ("EducationControl_nameTextBox"));
            ((Panel)e.Item.FindControl("EducationControl_schoolPanel")).GroupingText =
                "Institution " + ((e.Item as ListViewDataItem).DisplayIndex + 1).ToString();

            EducationControl_addButton.Visible = false;
            EducationControl_deleteButton.Visible = false;

            if (_rowCounter == ((e.Item as ListViewDataItem).DisplayIndex + 1))
            {
                EducationControl_nameTextBox.Focus();
                EducationControl_addButton.Visible = true;
                if (_rowCounter != 1)
                    EducationControl_deleteButton.Visible = true;
            }
        }
               
        protected void EducationControl_listView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            SetViewState();
            SetDate();
            if (e.CommandName == "deleteEducation")
            {
                ImageButton deleteRowImagebutton = (ImageButton)e.Item.FindControl("EducationControl_deleteImageButton");
                TextBox txtRowIndex = (TextBox)e.Item.FindControl("EducationControl_deleteRowIndex");
                EducationControl_deletedRowHiddenField.Value = txtRowIndex.Text;
                EducationControl_okPopUpClick(deleteRowImagebutton, new EventArgs());
            }
            else if (e.CommandName == "addEducation")
            {
                Button EducationControl_addButton = (Button)e.Item.FindControl("EducationControl_addButton");
                EducationControl_addDefaultButton_Click(EducationControl_addButton, new EventArgs());
            }
        }

        #endregion Event Handlers
            
        #region Private Variables                                              
                                            
        private List<Education> dataSource;

        #endregion

        #region Public Methods                                                 
                                               
        public string SetLocation(Location pLocation)
        {
            string location = "";
            if (pLocation != null)
            {
                location = (pLocation.City != null) ? pLocation.City : "";
                location += (pLocation.State != null) ? "," + pLocation.State : "";
                location += (pLocation.Country != null) ? "," + pLocation.Country : "";
                location = location.StartsWith(",") ? location.Substring(1) : location;
            }
            return location;
        }

        public List<Education> DataSource
        {
            set
            {
                if (value == null)
                {
                    EducationControl_addDefaultButton.Visible = true;
                    return;
                }
                // Set values into controls.
                _rowCounter = value.Count;
                
                if(_rowCounter!=0)
                    EducationControl_addDefaultButton.Visible = false;

                EducationControl_listView.DataSource = value;
                EducationControl_listView.DataBind();
                ViewState["dataSource"] =  value;
            }
            get
            {
                if (ViewState["dataSource"] != null)
                    return (List<Education>)ViewState["dataSource"];
                else
                    return null;
            }
        }

        #endregion Public Methods

        #region Protected Methods                                              

        protected void SetViewState()
        {

            List<Education> oEduList = null;
            int intEduCnt = 0;
            foreach (ListViewDataItem item in EducationControl_listView.Items)
            {
                if (oEduList == null)
                {
                    oEduList = new List<Education>();
                }
                          

                Education oEducation = new Education();
                Location oLocation=new Location();
                TextBox txtEduName = (TextBox)item.FindControl("EducationControl_nameTextBox");
                TextBox txtEduLocation = (TextBox)item.FindControl("EducationControl_locationTextBox");
                TextBox txtEduDegree = (TextBox)item.FindControl("EducationControl_degreeTextBox");
                TextBox txtEduSpec = (TextBox)item.FindControl("EducationControl_specializationTextBox");
                TextBox txtEduGPA = (TextBox)item.FindControl("EducationControl_gpaTextBox");
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                TextBox txtRowIndex = (TextBox)item.FindControl("EducationControl_deleteRowIndex");
                intEduCnt = intEduCnt + 1;              
                oEducation.EducationId = intEduCnt;
                oEducation.DegreeName = txtEduDegree.Text.Trim();
                oLocation.City=txtEduLocation.Text.Trim();
                oEducation.SchoolLocation = oLocation;
                oEducation.Specialization = txtEduSpec.Text.Trim();
                oEducation.SchoolName = txtEduName.Text.Trim();
                DateTime DtEduGradDate;
                if ((DateTime.TryParse(txtEduGradDate.Text, out DtEduGradDate)))
                {
                    oEducation.GraduationDate = DtEduGradDate;
                }               
                oEducation.GPA=txtEduGPA.Text.Trim();
                oEduList.Add(oEducation);
            }
            EducationControl_listView.DataSource = oEduList;
            EducationControl_listView.DataBind();
            ViewState["dataSource"] = oEduList;
            EducationControl_addDefaultButton.Visible = false;
        }
        protected void SetDate()
        {
            foreach (ListViewDataItem item in EducationControl_listView.Items)
            {
                TextBox txtEduGradDate = (TextBox)item.FindControl("EducationControl_graduationDtTextBox");
                if (txtEduGradDate.Text == "01/01/0001" || txtEduGradDate.Text == "1/1/0001 12:00:00 AM")
                {
                    txtEduGradDate.Text = "";
                }
            }
        }

        #endregion Protected Methods

    }
}