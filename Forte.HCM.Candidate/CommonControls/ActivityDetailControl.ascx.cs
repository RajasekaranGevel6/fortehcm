﻿
#region Directives                                                              

using System;
using System.Web.UI;

using Forte.HCM.Trace;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ActivityDetailControl : UserControl
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ActivityDetailControl_confirmMsgControl.Message = "Are you ready to start the test ?";
                    ActivityDetailControl_confirmMsgControl.Title = "Start Test";
                    ActivityDetailControl_confirmMsgControl.Type = MessageBoxType.YesNo;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        public CandidateTestSessionDetail DataSource
        {
            set
            {
                if (value == null)
                {
                    ActivityDetailControl_activityDiv.Visible = false;
                    ActivityDetailControl_noActivityDiv.Visible = true;
                    ClearValues();
                    return;
                }

                ActivityDetailControl_activityDiv.Visible = true;
                ActivityDetailControl_noActivityDiv.Visible = false;

                // Candidate session ID.
                ActivityDetailControl_candidateSessionIDHiddenField.Value = value.CandidateTestSessionID;
                ActivityDetailControl_attemptIDHiddenField.Value = value.AttemptID.ToString();

                // Test name.
                ActivityDetailControl_testNameLabel.Text = value.TestName;

                // Test description.
                ActivityDetailControl_testDescriptionLabel.Text = value.TestDescription;

                // Scheduled By.
                ActivityDetailControl_ScheduledByValueLabel.Text = value.SchedulerName;

                // Created date.
                ActivityDetailControl_createdDateValueLabel.Text = string.Format("{0}", value.CreatedDateFormatted);

                // Expiry date.
                ActivityDetailControl_expiryDateValueLabel.Text = string.Format("{0}", value.ExpiryDateFormatted);

                // Add handler for 'set test reminder' image and link button.
                ActivityDetailControl_setTestReminderImageButton.Attributes.Add("OnClick",
                    "javascript:return OpenTestReminderFromHome('TST','" + ActivityDetailControl_candidateSessionIDHiddenField.Value + "','" + ActivityDetailControl_attemptIDHiddenField.Value + "')");
                ActivityDetailControl_setTestReminderLinkButton.Attributes.Add("OnClick",
                    "javascript:return OpenTestReminderFromHome('TST','" + ActivityDetailControl_candidateSessionIDHiddenField.Value + "','" + ActivityDetailControl_attemptIDHiddenField.Value + "')");

                // Add hander for 'email scheduler' image and link button.
                ActivityDetailControl_emailSchedulerImageButton.Attributes.Add("OnClick",
                    "javascript:return EmailSchedulerFromHome('" + ActivityDetailControl_candidateSessionIDHiddenField.Value + "','" + ActivityDetailControl_attemptIDHiddenField.Value + "','TST')");
                ActivityDetailControl_emailSchedulerLinkButton.Attributes.Add("OnClick",
                    "javascript:return EmailSchedulerFromHome('" + ActivityDetailControl_candidateSessionIDHiddenField.Value + "','" + ActivityDetailControl_attemptIDHiddenField.Value + "','TST')");
            }
        }

        #endregion Event Handlers

        private void ClearValues()
        {
            ActivityDetailControl_candidateSessionIDHiddenField.Value = string.Empty;
            ActivityDetailControl_attemptIDHiddenField.Value = string.Empty;
            ActivityDetailControl_testNameLabel.Text = string.Empty;
            ActivityDetailControl_testDescriptionLabel.Text = string.Empty;

            ActivityDetailControl_ScheduledByValueLabel.Text = string.Empty;
            ActivityDetailControl_createdDateValueLabel.Text = string.Empty;
            ActivityDetailControl_expiryDateValueLabel.Text = string.Empty;
        }

        protected void ActivityDetailControl_showTestIntroductionImageButton_Click
            (object sender, ImageClickEventArgs e)
        {
            // Redirect to test introduction page.
            Response.Redirect("~/TestCenter/TestIntroduction.aspx" +
               "?candidatesessionid=" + ActivityDetailControl_candidateSessionIDHiddenField.Value +
               "&attemptid=" + ActivityDetailControl_attemptIDHiddenField.Value +
               "&parentpage=ACT_HOME", false);
        }

        protected void ActivityDetailControl_showTestIntroductionLinkButton_Click
            (object sender, EventArgs e)
        {
            // Redirect to test introduction page.
            Response.Redirect("~/TestCenter/TestIntroduction.aspx" +
               "?candidatesessionid=" + ActivityDetailControl_candidateSessionIDHiddenField.Value +
               "&attemptid=" + ActivityDetailControl_attemptIDHiddenField.Value +
               "&parentpage=ACT_HOME", false);
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void ActivityDetailControl_startTestButton_OkClick(object sender, EventArgs e)
        {
            Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                "?candidatesessionid=" + ActivityDetailControl_candidateSessionIDHiddenField.Value +
                "&attemptid=" + ActivityDetailControl_attemptIDHiddenField.Value +
                "&parentpage=CAND_HOME", false);
        }
    }
}