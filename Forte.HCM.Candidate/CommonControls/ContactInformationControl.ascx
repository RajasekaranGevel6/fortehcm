﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInformationControl.ascx.cs"
    Inherits="Forte.HCM.UI.CommonControls.ContactInformationControl" %>
    
<div id="MyResume_contactInfoMainDiv">
    <!-- Contact controls -->
    <div id="MyResume_contactInfoControls" style="clear: both; float: left; height: 290px" class="resume_Table_Bg">
        <div id="ContactInformationControl_controlsDiv" style="display: block;" runat="server">
            <!-- Moved from name control to contact information control -->
            <div>
                <div style="width:95px; padding-top: 10px; padding-left:10px; font-size: 14px; color: #000; float: left;">
                    <asp:Label ID="ContactInformationControl_firstNameLabel" runat="server" Text="First Name"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div style="width: 210px; float: left;">
                    <asp:TextBox ID="ContactInformationControl_firstNameTextBox" 
                    runat="server" MaxLength="100" SkinID="sknCanResumeTextBoxName"  ></asp:TextBox>
                </div>
                <div style="width: 95px; float: left; padding-top: 10px;padding-left:10px;">
                    <asp:Label ID="ContactInformationControl_middleNameLabel" runat="server" Text="Middle Name"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label></div>
                <div style="width: 220px; float: left;">
                    <asp:TextBox ID="ContactInformationControl_middleNameTextBox"  
                        runat="server" MaxLength="100" SkinID="sknCanResumeTextBoxName"></asp:TextBox>
                </div>
                <div style="width: 85px; float: left; padding-top: 10px;">
                    <asp:Label ID="ContactInformationControl_lastNameLabel" runat="server" Text="Last Name"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div style="width: 90px; float: left;">
                    <asp:TextBox ID="ContactInformationControl_lastNameTextBox"  
                        runat="server" MaxLength="100" SkinID="sknCanResumeTextBoxName"></asp:TextBox>
                </div>
            </div>
            <!-- End first,middle and last name information control-->
            <div class="can_resume_form_container">
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_streetLabel" runat="server" Text="Street"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_left">
                    <asp:TextBox ID="ContactInformationControl_streetTextBox" runat="server" Width="795px"
                        TextMode="MultiLine" Height="30" MaxLength="500" onkeyup="CommentsCount(500,this)"
                        onchange="CommentsCount(500,this)" SkinID="sknCanResumeTextBox"></asp:TextBox>
                </div>
            </div>
            <div class="can_resume_form_container" >
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_cityLabel" runat="server" Text="City" 
                    SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_right">
                    <asp:TextBox ID="ContactInformationControl_cityTextBox" runat="server" SkinID="sknCanResumeTextBox" MaxLength="100"></asp:TextBox>
                </div>
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_stateLabel" runat="server" Text="State"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_right">
                    <asp:TextBox ID="ContactInformationControl_stateTextBox" runat="server" SkinID="sknCanResumeTextBox" MaxLength="100"></asp:TextBox>
                </div>
            </div>
            <div class="can_resume_form_container">
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_countryNameLabel" runat="server" Text="Country"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_right">
                    <asp:TextBox ID="ContactInformationControl_countryNameTextBox" runat="server" 
                    SkinID="sknCanResumeTextBox" MaxLength="100"></asp:TextBox>
                </div>
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_postalCodeLabel" runat="server" Text="Postal Code"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_right">
                    <asp:TextBox ID="ContactInformationControl_postalCodeTextBox" runat="server" 
                        SkinID="sknCanResumeTextBox" MaxLength="10"></asp:TextBox>
                </div>
            </div>
            <div class="can_resume_form_container">
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_fixedLineLabel" runat="server" Text="Fixed Line"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_right">
                    <asp:TextBox ID="ContactInformationControl_fixedLineTextBox" 
                    runat="server" MaxLength="100" SkinID="sknCanResumeTextBox"></asp:TextBox>
                </div>
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_mobileLabel" runat="server" Text="Mobile"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_right">
                    <asp:TextBox ID="ContactInformationControl_mobileTextBox" runat="server" SkinID="sknCanResumeTextBox"
                         MaxLength="100"></asp:TextBox>
                </div>
            </div>
            <div class="can_resume_form_container">
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_emailLabel" runat="server" Text="Email"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_right">
                    <asp:TextBox ID="ContactInformationControl_emailTextBox" runat="server" 
                    SkinID="sknCanResumeTextBox" MaxLength="100"></asp:TextBox>
                </div>
                <div class="can_resume_contact_container_left">
                    <asp:Label ID="ContactInformationControl_websiteLabel" runat="server" Text="Website"
                        SkinID="sknCanReumeLabelFieldText"></asp:Label>
                </div>
                <div class="can_resume_contact_container_right">
                    <asp:TextBox ID="ContactInformationControl_websiteTextBox" runat="server" SkinID="sknCanResumeTextBox"
                        MaxLength="100"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <!-- End Contact controls-->
</div>
