﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// ReferencesControl.cs
// File that represents the user interface for the reference information details

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.EventSupport;
using System.Data;

#endregion Directives

namespace Forte.HCM.UI.CommonControls
{
    public partial class ReferencesControl : System.Web.UI.UserControl
    {
        #region Custom Event Handler and Delegate                              

        public delegate void ControlMessageThrownDelegate(object sender, ControlMessageEventArgs c);
        public event ControlMessageThrownDelegate ControlMessageThrown;
        private int _rowCounter;
        #endregion Custom Event Handler and Delegate

        #region Event Handlers                                                 
       
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        
        protected void ReferencesControl_listView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void ReferencesControl_addDefaultButton_Click(object sender, EventArgs e)
        {
            SetViewState();

            ReferencesControl_listView.DataSource = null;
            if (ViewState["dataSource"] != null)
            {
                this.dataSource = (List<Reference>)ViewState["dataSource"];
            }
            else
                this.dataSource = new List<Reference>();
            this.dataSource.Add(new Reference());
            ViewState["dataSource"] = this.dataSource;
            _rowCounter = this.dataSource.Count;
            ReferencesControl_listView.DataSource = this.dataSource;
            ReferencesControl_listView.DataBind();
        }        
            
        protected void ReferencesControl_listView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Button ReferencesControl_addButton = ((Button)e.Item.FindControl
                ("ReferencesControl_addButton"));
            Button ReferencesControl_deleteButton = ((Button)e.Item.FindControl
                ("ReferencesControl_deleteButton"));
            TextBox ReferencesControl_nameTextBox = ((TextBox)e.Item.FindControl
                ("ReferencesControl_nameTextBox"));
            ((Panel)e.Item.FindControl("ReferencesControl_referencePanel")).GroupingText =
               "References " +((e.Item as ListViewDataItem).DisplayIndex + 1).ToString();

            ReferencesControl_addButton.Visible = false;
            ReferencesControl_deleteButton.Visible = false;

            if (_rowCounter == ((e.Item as ListViewDataItem).DisplayIndex + 1))
            {
                ReferencesControl_nameTextBox.Focus();
                ReferencesControl_addButton.Visible = true;
                if(_rowCounter!=1)
                    ReferencesControl_deleteButton.Visible = true;
            }
        }

        protected void ReferencesControl_listView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            SetViewState();
            if (e.CommandName == "deleteReference")
            {
                Button ReferencesControl_deleteButton = (Button)e.Item.FindControl("ReferencesControl_deleteButton");
                TextBox txtRowIndex = (TextBox)e.Item.FindControl("ReferencesControl_deleteRowIndex");
                ReferencesControl_deletedRowHiddenField.Value = txtRowIndex.Text;
                ReferencesControl_okPopUpClick(ReferencesControl_deleteButton, new EventArgs());
            }
            else if (e.CommandName == "addReference")
            {
                Button ReferencesControl_addButton = (Button)e.Item.FindControl("ReferencesControl_addButton");
                ReferencesControl_addDefaultButton_Click(ReferencesControl_addButton, new EventArgs());
            }
        }

        protected void ReferencesControl_okPopUpClick(object sender, EventArgs e)
        {            
            int intDeletedRowIndex = Convert.ToInt32(ReferencesControl_deletedRowHiddenField.Value);
            if (intDeletedRowIndex != 0)
            {
                this.dataSource = (List<Reference>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intDeletedRowIndex - 1);                 
            }
            else
            {
                int intLastRecord = ReferencesControl_listView.Items.Count - 1;
                this.dataSource = (List<Reference>)ViewState["dataSource"];
                this.dataSource.RemoveAt(intLastRecord);                  
            }
            _rowCounter = dataSource.Count;
            ReferencesControl_listView.DataSource = dataSource;
            ReferencesControl_listView.DataBind();
            SetViewState();   
            // Fire the message event
            if (ControlMessageThrown != null)
                ControlMessageThrown(this, new ControlMessageEventArgs("Reference detail deleted successfully", MessageType.Success));
                   
        }

        #endregion Event Handlers

        #region Public Properties                                              
        
        public List<Reference> DataSource
        {
            set
            {
                if (value == null)
                {
                    ReferencesControl_addDefaultButton.Visible = true;
                    return;
                }
                // Set values into controls.
                _rowCounter = value.Count;
                
                if(_rowCounter!=0)
                    ReferencesControl_addDefaultButton.Visible = false;

                ReferencesControl_listView.DataSource = value;
                ReferencesControl_listView.DataBind();
                ViewState["dataSource"] = value;
            }
            get
            {
                if (ViewState["dataSource"] != null)
                    return (List<Reference>)ViewState["dataSource"];
                else
                    return null;
            }
        }

        public string SetContactInfo(ContactInformation pContInfo)
        {
            string contInfo = "";
            if (pContInfo != null)
            {
                contInfo += (pContInfo.Phone != null) ? pContInfo.Phone.Mobile : "";
                //contInfo = (pContInfo.City != null) ? "," + pContInfo.City : "";
                //contInfo += (pContInfo.Country != null) ? "," + pContInfo.Country : "";
                //contInfo += (pContInfo.EmailAddress != null) ? "," + pContInfo.EmailAddress : "";
                
                contInfo = contInfo.StartsWith(",") ? contInfo.Substring(1) : contInfo;
            }
            return contInfo;
        }
        
        #endregion Public Properties

        #region Private Properties                                             

        private List<Reference> dataSource;
       
        #endregion Private Properties

        #region Protected Methods                                              
        
        protected void SetViewState()
        {

            List<Reference> oReferenceList = null;
            int intRefCnt = 0;
            foreach (ListViewDataItem item in ReferencesControl_listView.Items)
            {
                if (oReferenceList == null)
                {
                    oReferenceList = new List<Reference>();
                }

                Reference oReference = new Reference();
                ContactInformation oContactInfo = new ContactInformation();
                PhoneNumber oPhone = new PhoneNumber();
                TextBox txtRefName = (TextBox)item.FindControl("ReferencesControl_nameTextBox");
                TextBox txtRefOrg = (TextBox)item.FindControl("ReferencesControl_organizationTextBox");
                TextBox txtRefRelation = (TextBox)item.FindControl("ReferencesControl_relationTextBox");
                TextBox txtRefContactInfo = (TextBox)item.FindControl("ReferencesControl_contInfoTextBox");
                intRefCnt = intRefCnt + 1;
                oReference.ReferenceId = intRefCnt;
                oReference.Name = txtRefName.Text.Trim();
                oReference.Organization = txtRefOrg.Text.Trim();
                oReference.Relation = txtRefRelation.Text.Trim();
                oPhone.Mobile = txtRefContactInfo.Text.Trim();
                oContactInfo.Phone = oPhone;
                oReference.ContactInformation = oContactInfo;               
                oReferenceList.Add(oReference);
            }
            ReferencesControl_listView.DataSource = oReferenceList;
            ReferencesControl_listView.DataBind();
            ViewState["dataSource"] = oReferenceList;
            ReferencesControl_addDefaultButton.Visible = false;
        }

        #endregion Protected Methods

    }
}