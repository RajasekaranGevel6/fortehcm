﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MultiSeriesZoomedChart.cs
// File that represents the user interface for the multiple series zoomed chart
//
#endregion Header

#region Directives                                                             

using System;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that defines the user interface layout and functionalities for 
    /// MultipleSeriesZoomedChartControl. 
    /// </summary>
    public partial class MultiSeriesZoomedChart : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that is called when the page is loaded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Zoomed Chart");
                // Clear message and hide labels.
                ClearMessages();

                // Load the chart values 
                LoadValues();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                // Show error messages to the user
                base.ShowMessage(MultiSeriesZoomedChart_topErrorMessageLabel,
                    exception.Message);
            }
        }

        #endregion Event Handlers

        #region Overridden Methods                                             

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Represents the method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            //String that holds the session name
            string sessionId = Request.QueryString["sessionName"];

            if (Session[sessionId] == null)
                return;

            //Get the chart data source from session
            MultipleSeriesChartData chartDetails = Session[sessionId] as MultipleSeriesChartData;

            //Set the height and width for chart
            //SetChartHeightAndWidth(chartDetails);

            //Set the header literal text
            MultiSeriesZoomedChart_headerLiteral.Text = chartDetails.ChartTitle;

            //Data bind the table 
            MultiSeriesZoomedChart_chart.DataBindTable(chartDetails.
                MultipleSeriesChartDataSource, "Name");

            //Assign the tooltip for each series
            foreach (Series item in MultiSeriesZoomedChart_chart.Series)
            {
                item.ToolTip = "#VAL%";
                item.ChartType = chartDetails.ChartType;
                item.Label = "#VAL%";
            }
            //Assign the x axis title 
            //Defines whether the axis title has to display or not
            if (chartDetails.IsDisplayAxisTitle)
            {
                MultiSeriesZoomedChart_chart.ChartAreas
                    ["MultiSeriesZoomedChart_chartArea"].AxisX.Title = chartDetails.XAxisTitle;

                MultiSeriesZoomedChart_chart.ChartAreas
                    ["MultiSeriesZoomedChart_chartArea"].AxisY.Title = chartDetails.YAxisTitle;
            }

        }

        #endregion Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            MultiSeriesZoomedChart_topErrorMessageLabel.Text = string.Empty;
            MultiSeriesZoomedChart_topSuccessMessageLabel.Text = string.Empty;

            MultiSeriesZoomedChart_topErrorMessageLabel.Visible = false;
            MultiSeriesZoomedChart_topSuccessMessageLabel.Visible = false;
        }

        /// <summary>
        /// Represents the method to set the chart height and width 
        /// </summary>
        /// <param name="chartDetails">
        /// A<see cref="SingleChartData"/>that holds the chart data
        /// </param>
        private void SetChartHeightAndWidth(MultipleSeriesChartData chartDetails)
        {
            //Initialize the default width
            int defaultLength = 33;

            int chartWidth = 0;

            //Initialize the default height
            //int defaultHeightCount = 23;

            //int chartHeight = 0;

            int maxLength = 0;

            //Find the maximum length of the x axis variable
            foreach (MultipleChartData item in chartDetails.MultipleSeriesChartDataSource)
            {
                maxLength += item.Name.Length;
            }

            //if the length of string is greater than 33
            if (maxLength > defaultLength)
            {
                //Increase the chart Width by 15 px for each character
                chartWidth = (maxLength - defaultLength) * 15;

                int defaultChartWidth = int.Parse(MultiSeriesZoomedChart_chart.Width.Value.ToString());

                //set the chart width to new value
                MultiSeriesZoomedChart_chart.Width = Unit.Pixel(chartWidth + defaultChartWidth);
            }

            //check the maximum count of the number of chart data
            //if (chartDetails.ChartData.Count > defaultHeightCount)
            //{
            //    defaultHeightCount = chartDetails.ChartData.Count;

            //    //Increase the chart Width by 20 px for each line
            //    chartHeight = (defaultHeightCount - 23) * 20;

            //    int defaultChartHeight = int.Parse(ZoomedChart_chart.Height.Value.ToString());

            //    //set the chart height to new value
            //    ZoomedChart_chart.Height = Unit.Pixel(chartHeight + defaultChartHeight);
            //}
        }

        #endregion Private Methods
    }
}
