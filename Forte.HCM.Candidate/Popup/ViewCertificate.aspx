﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCertificate.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.ViewCertificate"  MasterPageFile="~/SiteMaster.Master"
    Title="View Certificate" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc3" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>

<asp:Content ID="ViewCertificate_content" ContentPlaceHolderID="MainContent"
    runat="server">
    <script type="text/javascript" language="javascript">

        // Function that will show the email confirm message extender.
        function ShowEmailConfirm()
        {
            //displays email confirmation pop up extender
            $find("<%= ViewCertificate_emailConfirmation_ModalPopupExtender.ID %>").show();
            return false;
        }

        // Function that will open the download window.
        function OpenPopUp()
        {
            window.open('../Common/Download.aspx?type=<%=EmailAttachment_downloadTypeHiddenField.Value %>', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }

    </script>

    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div class="popup_right_panel">
                    <div class="popup_body_bg_div">
                        <div class="popup_header_text_bold_div" style="width: 510px">
                            <asp:Literal ID="ViewCertificate_headerLiteral" runat="server" Text="View Certificate"></asp:Literal>
                        </div>
                        <div class="popup_controls_header_bg_right">
                            <div class="popup_controls_header_bg_left">
                                <asp:Button ID="ViewCertificate_topDownloadButton" runat="server" Text="Download" SkinID="sknButtonId" />
                                <asp:HiddenField ID="EmailAttachment_downloadTypeHiddenField" runat="server" />
                            </div>
                            <div class="popup_controls_header_bg_left">
                                <asp:Button ID="ViewCertificate_topEmailButton" runat="server" Text="Email" SkinID="sknButtonId"
                                    OnClientClick="javascript:return ShowEmailConfirm();" />
                            </div>
                            <div class="popup_controls_header_bg_left">
                                <asp:LinkButton ID="ViewCertificate_topCancelLinkButton" runat="server" SkinID="sknPopupLinkButton" Text="Cancel"
                                    OnClick="ParentPageRedirect" />
                            </div>
                        </div>
                    </div>
                    <div class="empty_height">
                        &nbsp;
                    </div>
                    <div class="popup_tab_body_bg_div">
                        <div class="msg_align">
                            <asp:Label ID="ViewCertificate_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ViewCertificate_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </div>
                        <div class="popup_cont_div">
                            <div class="popup_cont_div_left">
                                <asp:Label ID="ViewCertificate_expiryDateHeadLabel" runat="server" Text="Expiry Date"
                                    SkinID="sknLabelFieldText"></asp:Label>
                            </div>
                            <div class="popup_cont_div_right">
                                <asp:Label ID="ViewCertificate_expiryDateLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                <asp:HiddenField ID="ViewCertificate_htmlTextHiddenField" runat="server" />
                            </div>
                        </div>
                        <div class="popup_cont_div">
                            <div id="ViewCertificate_previewDiv" runat="server" style="height: 430px; width: 630px;
                                overflow: auto;">
                                <asp:Image ID="ViewCertificate_certificatePreviewImage" runat="server" />
                            </div>
                        </div>
                        <div class="msg_align">
                            <asp:Label ID="ViewCertificate_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                            <asp:Label ID="ViewCertificate_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                        </div>
                    </div>
                    <div class="popup_body_bg_div">
                        <div class="popup_header_text_bold_div" style="width: 510px">
                            &nbsp;
                        </div>
                        <div class="popup_controls_header_bg_right">
                            <div class="popup_controls_header_bg_left">
                                <asp:Button ID="ViewCertificate_bottomDownloadButton" runat="server" Text="Download" SkinID="sknButtonId" />
                            </div>
                            <div class="popup_controls_header_bg_left">
                                <asp:Button ID="ViewCertificate_bottomEmailButton" runat="server" Text="Email" SkinID="sknButtonId"
                                    OnClientClick="javascript:return ShowEmailConfirm();" />
                            </div>
                            <div class="popup_controls_header_bg_left">
                                <asp:LinkButton ID="ViewCertificate_bottomCancelLinkButton" runat="server" SkinID="sknPopupLinkButton" Text="Cancel"
                                    OnClick="ParentPageRedirect" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <asp:Panel ID="ViewCertificate_emailConfirmationPanel" runat="server" Style="display: none;
            height: 205px; width: 250px;" CssClass="popupcontrol_confirm">
            <div id="ViewCertificate_emailConfirmationDiv" style="display: none">
                <asp:Button ID="ViewCertificate_emailConfirmation_hiddenButton" runat="server" />
            </div>
            <uc3:ConfirmMsgControl ID="ViewCertificate_emailConfirmation_ConfirmMsgControl" runat="server"
                Title="Email Confirmation" Type="EmailConfirmType" Message="How do you want to email your certificate?" />
        </asp:Panel>
        <ajaxToolKit:ModalPopupExtender ID="ViewCertificate_emailConfirmation_ModalPopupExtender"
            BehaviorID="ViewCertificate_emailConfirmation_ModalPopupExtender" runat="server"
            PopupControlID="ViewCertificate_emailConfirmationPanel" TargetControlID="ViewCertificate_emailConfirmation_hiddenButton"
            BackgroundCssClass="modalBackground">
        </ajaxToolKit:ModalPopupExtender>
    </div>
</asp:Content>
