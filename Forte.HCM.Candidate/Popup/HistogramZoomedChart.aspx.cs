﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
// HistogramZoomedChart.cs contains the coding for the graphical 
// representation of the histogram chart

#endregion Header

#region Directives                                                             

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Represents the class that holds the User Interface and 
    /// fucntionalities of the HistogramZoomedChart page.
    /// </summary>
    public partial class HistogramZoomedChart : PageBase
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                // Load the chart values 
                LoadValues();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);

                HistogramZoomedChart_topErrorMessageLabel.Text =
                    exception.Message;
            }
        }

        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            HistogramZoomedChart_topErrorMessageLabel.Text = string.Empty;
            HistogramZoomedChart_topSuccessMessageLabel.Text = string.Empty;

            HistogramZoomedChart_topErrorMessageLabel.Visible = false;
            HistogramZoomedChart_topSuccessMessageLabel.Visible = false;
        }

        /// <summary>
        /// Helper method which rounds specified axsi interval.
        /// </summary>
        /// <param name="interval">Calculated axis interval.</param>
        /// <returns>Rounded axis interval.</returns>
        private double RoundInterval(double interval)
        {
            // If the interval is zero return error
            if (interval == 0.0)
            {
                throw (new ArgumentOutOfRangeException("interval", "Interval can not be zero."));
            }

            // If the real interval is > 1.0
            double step = -1;
            double tempValue = interval;
            while (tempValue > 1.0)
            {
                step++;
                tempValue = tempValue / 10.0;
                if (step > 1000)
                {
                    throw (new InvalidOperationException("Auto interval error due to invalid point values or axis minimum/maximum."));
                }
            }

            // If the real interval is < 1.0
            tempValue = interval;
            if (tempValue < 1.0)
            {
                step = 0;
            }

            while (tempValue < 1.0)
            {
                step--;
                tempValue = tempValue * 10.0;
                if (step < -1000)
                {
                    throw (new InvalidOperationException("Auto interval error due to invalid point values or axis minimum/maximum."));
                }
            }

            double tempDiff = interval / Math.Pow(10.0, step);
            if (tempDiff < 3.0)
            {
                tempDiff = 2.0;
            }
            else if (tempDiff < 7.0)
            {
                tempDiff = 5.0;
            }
            else
            {
                tempDiff = 10.0;
            }

            // Make a correction of the real interval
            return tempDiff * Math.Pow(10.0, step);
        }
        #endregion

        #region Override Methods                                               
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Override Method used to load the values
        /// </summary>
        protected override void LoadValues()
        {
            //String that holds the session name
            string sessionId = Request.QueryString["sessionName"];

            if (Session[sessionId] != null)
            {
                //Defines the chart data
                ReportHistogramData chartdatas = new ReportHistogramData();

                //Get the chart data source from session
                chartdatas = Session[sessionId] as ReportHistogramData;

                //Defines whether the axis title has to display or not
                if (chartdatas.IsDisplayAxisTitle)
                {
                    HistogramZoomedChart_chart.ChartAreas
                        ["HistogramZoomedChart_chartArea"].AxisX.Title = chartdatas.XAxisTitle;

                    HistogramZoomedChart_chart.ChartAreas
                        ["HistogramZoomedChart_chartArea"].AxisY.Title = chartdatas.YAxisTitle;
                }

                HistogramZoomedChart_headerLiteral.Text = chartdatas.ChartTitle;

                /// <summary>
                /// Number of class intervals the data range is devided in.
                /// This property only has affect when "SegmentIntervalWidth" is 
                /// set to double.NaN.
                /// </summary>
                int SegmentIntervalNumber = chartdatas.SegmentInterval;

                /// <summary>
                /// Histogram class interval width. Setting this value to "double.NaN"
                /// will result in automatic width calculation based on the data range
                /// and number of required interval specified in "SegmentIntervalNumber".
                /// </summary>
                double SegmentIntervalWidth = double.NaN;

                // Get data series minimum and maximum values
                double minValue = Convert.ToDouble(0);
                double maxValue = Convert.ToDouble(100);


                // Calculate interval width if it's not set
                if (double.IsNaN(SegmentIntervalWidth))
                {
                    SegmentIntervalWidth = (maxValue - minValue) / SegmentIntervalNumber;
                    SegmentIntervalWidth = RoundInterval(SegmentIntervalWidth);
                }



                // Create histogram series points
                double currentPosition = minValue;
                int maxYAxis = 0;
                for (currentPosition = minValue; currentPosition <= maxValue; currentPosition += SegmentIntervalWidth)
                {
                    // Count all points from data series that are in current interval
                    int count = 0;
                    foreach (ChartData data in chartdatas.ChartData)
                    {
                        if (!Utility.IsNullOrEmpty(data))
                        {
                            double endPosition = currentPosition + SegmentIntervalWidth;
                            if (data.ChartYValue > currentPosition &&
                                data.ChartYValue <= endPosition)
                            {
                                ++count;
                            }

                            // Last segment includes point values on both segment boundaries
                            else if (currentPosition <= minValue)
                            {
                                if (data.ChartYValue >= currentPosition &&
                                    data.ChartYValue <= endPosition)
                                {
                                    ++count;
                                }
                            }
                        }
                    }
                    // Add data point into the histogram series
                    HistogramZoomedChart_chart.Series["HistogramZoomedChart_chartPrimarySeries"]
                        .Points.AddXY(currentPosition + SegmentIntervalWidth / 2.0, count);

                    //Assign the tool tip for the chart series point
                    HistogramZoomedChart_chart.Series["HistogramZoomedChart_chartPrimarySeries"].ToolTip = "#VAL";

                    //Assign the y axis maximum value
                    maxYAxis = maxYAxis < count ? count : maxYAxis;

                }

                Series histogramSeries = HistogramZoomedChart_chart.
                                        Series["HistogramZoomedChart_chartPrimarySeries"];
                //Assign the point width for the series 
                histogramSeries["PointWidth"] = "1";

                ChartArea chartArea = HistogramZoomedChart_chart.ChartAreas[0];
                //Assign the X axis minimum value
                chartArea.AxisX.Minimum = minValue;

                //Assign the Y axis minimum value
                chartArea.AxisX.Maximum = maxValue;

                //Set the maximum value for the y axis
                //chartArea.AxisY.Minimum = 0;
                chartArea.AxisY.Maximum = maxYAxis;

                // Set axis interval based on the histogram class interval
                // and do not allow more than 10 labels on the axis.
                double axisInterval = SegmentIntervalWidth;
                while ((maxValue - minValue) / axisInterval > 10.0)
                {
                    axisInterval *= 2.0;
                }
                chartArea.AxisX.Interval = axisInterval;

                if (chartdatas.CandidateScoreData == null)
                    //If the candidate score lies in between the 
                    //points change the color of the series
                    foreach (DataPoint point in histogramSeries.Points)
                    {
                        //assign default  color for all points
                        point.Color = ColorTranslator.FromHtml("#BA55D3");

                        //assign different blue color for my score
                        if ((chartdatas.CandidateScore == point.XValue - 5) && (chartdatas.CandidateScore == 0))
                        {
                            point.Color = ColorTranslator.FromHtml("#7DC9E9");
                        }
                        else if (((chartdatas.CandidateScore <= point.XValue + 5) &&
                            (chartdatas.CandidateScore >= point.XValue + 5 - SegmentIntervalWidth + 1)))
                        {
                            point.Color = ColorTranslator.FromHtml("#7DC9E9");
                        }
                        //if ((chartdatas.CandidateScore >= point.XValue - SegmentIntervalWidth
                        //    && chartdatas.CandidateScore <= point.XValue) ||
                        //    (chartdatas.CandidateScore < point.XValue + SegmentIntervalWidth
                        //    && chartdatas.CandidateScore >= point.XValue))
                        //{
                        //    point.Color = ColorTranslator.FromHtml("#BA55D3");
                        //}
                    }


                if (chartdatas.CandidateScoreData == null)
                    if (chartdatas.IsDisplayCandidateName)
                    {
                        foreach (DataPoint point in histogramSeries.Points)
                        {
                            if (point.Color == ColorTranslator.FromHtml("#7DC9E9") && point.YValues[0] != 0)
                            {
                                point.Label = chartdatas.CandidateName + "\n" + "#VAL";
                            }
                        }
                    }


                if (chartdatas.CandidateScoreData == null)
                    return;

                foreach (DataPoint point in histogramSeries.Points)
                {
                    //assign default  color for all points
                    point.Color = ColorTranslator.FromHtml("#BA55D3");
                }


                foreach (HistogramData data in chartdatas.CandidateScoreData)
                {
                    foreach (DataPoint point in histogramSeries.Points)
                    {
                        //assign different blue color for my score

                        if ((data.CandidateScore == point.XValue - 5) && (data.CandidateScore == 0))
                        {
                            point.Color = ColorTranslator.FromHtml("#7DC9E9");

                            point.Label += "\n" + data.CandidateName + "\n";

                            break;
                        }

                        else if (((data.CandidateScore <= point.XValue + 5) &&
                            (data.CandidateScore >= point.XValue + 5 - SegmentIntervalWidth + 1)))
                        {
                            point.Color = ColorTranslator.FromHtml("#7DC9E9");

                            point.Label += "\n" + data.CandidateName + "\n";
                            break;
                        }
                    }
                }

                foreach (DataPoint point in histogramSeries.Points)
                {
                    if (point.Color == ColorTranslator.FromHtml("#7DC9E9")
                        && point.YValues[0] != 0)
                    {
                        point.Label += "\n" + "#VAL";
                    }
                }
            }
        }
        #endregion Override Methods
    }
}
