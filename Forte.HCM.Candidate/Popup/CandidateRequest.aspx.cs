﻿#region Header

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateRequest.cs
// File that represents the user interface layout and functionalities for
// the CandidateRequest page. This helps in sending request to the 
// scheduler to retake or reactivate a test.

#endregion Header

#region Directives

using System;
using System.Configuration;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the CandidateRequest page. This helps in sending request to the 
    /// scheduler to retake or reactivate a test. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class CandidateRequest : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                // Set default button.
                Page.Form.DefaultButton = CandidateRequest_topSubmitButton.UniqueID;

                // Set page title based on the request type
                if (Request.QueryString["requesttype"] == "retake")
                {
                    CandidateRequest_headerLiteral.Text = "Test Retake Request";

                    // Set browser title.
                    Master.SetPageCaption("Test Retake Request");
                }
                else
                {
                    CandidateRequest_headerLiteral.Text = "Test Activation Request";

                    // Set browser title.
                    Master.SetPageCaption("Test Activation Request");
                }

                if (!IsPostBack)
                {
                    Page.Form.DefaultFocus = CandidateRequest_reasonTextBox.UniqueID;
                    CandidateRequest_reasonTextBox.Focus();

                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CandidateRequest_topErrorMessageLabel, 
                    CandidateRequest_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the submit button is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void CandidateRequest_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool isMailSent = false;

                // Validate the input fields
                if (!IsValidData())
                    return;

                // Update retake request/reactivate test as 'Y' in the session candidate table
                // for the given candidate session key.
                if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]) &&
                    (Request.QueryString["requesttype"] == "retake"))

                    new CandidateBLManager().UpdateTestRequest
                            (Request.QueryString["candidatesessionid"].ToString().Trim(), "RETAKE");
                else
                    new CandidateBLManager().UpdateTestRequest
                        (Request.QueryString["candidatesessionid"].ToString().Trim(), "REACTIVATE");

                base.ShowMessage(CandidateRequest_topSuccessMessageLabel,
                    CandidateRequest_bottomSuccessMessageLabel,
                    Resources.HCMResource.CandidateRequest_AlertSentSuccessfully);

                try
                {
                    // Add the logged in user in the CC list when 'send me a copy ' is on
                    if (CandidateRequest_sendMeACopyCheckBox.Checked)
                    {
                        if (CandidateRequest_ccTextBox.Text.Trim().Length == 0)
                            CandidateRequest_ccTextBox.Text = base.userEmailID;
                        else
                            CandidateRequest_ccTextBox.Text += "," + base.userEmailID;
                    }

                    // Send email.
                    new EmailHandler().SendMail(CandidateRequest_fromHiddenField.Value.ToString().Trim(),
                        CandidateRequest_toHiddenField.Value.ToString().Trim(), CandidateRequest_ccTextBox.Text.Trim(),
                        CandidateRequest_subjectValueLabel.Text.Trim(), CandidateRequest_reasonTextBox.Text.Trim());

                    // Mail sent successfully.
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }

                if (isMailSent == false)
                {
                    base.ShowMessage(CandidateRequest_topErrorMessageLabel,
                        CandidateRequest_bottomErrorMessageLabel,
                        Resources.HCMResource.CandidateRequest_MailCannotBeSent);
                }
            }
            catch (Exception exp)
            {
                base.ShowMessage(CandidateRequest_topErrorMessageLabel,
                    CandidateRequest_bottomErrorMessageLabel, exp.Message);
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;

            if (CandidateRequest_ccTextBox.Text.Trim().Length > 0)
            {
                string[] ccMailIDs = CandidateRequest_ccTextBox.Text.Split(new char[] { ',', ';' });

                for (int idx = 0; idx < ccMailIDs.Length; idx++)
                {
                    if (!Regex.IsMatch(ccMailIDs[idx].Trim(), @"^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$"))
                    {
                        base.ShowMessage(CandidateRequest_topErrorMessageLabel,
                            CandidateRequest_bottomErrorMessageLabel,
                            Resources.HCMResource.CandidateRequest_InvalidCCEmailAddress);
                        isValidData = false;
                        break;
                    }
                }
            }

            // Validate reason feild
            if (CandidateRequest_reasonTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CandidateRequest_topErrorMessageLabel, 
                    CandidateRequest_bottomErrorMessageLabel, 
                    Resources.HCMResource.CandidateRequest_ReasonCannotBeEmpty);
                isValidData =  false;
            }

            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
            {
                TestScheduleDetail scheduleDetail = new CandidateBLManager().GetSchedulerDetails
                    (Request.QueryString["candidatesessionid"].ToString().Trim(),
                    Convert.ToInt32(Request.QueryString["attemptid"]));

                if (scheduleDetail == null)
                    return;

                if (scheduleDetail.CandidateName != null && scheduleDetail.EmailId != null)
                {
                    //CandidateRequest_fromValueLabel.Text = scheduleDetail.CandidateName +
                    //    " &lt;" + scheduleDetail.EmailId + "&gt;";
                    //CandidateRequest_fromHiddenField.Value = scheduleDetail.EmailId.ToString();

                    CandidateRequest_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                    CandidateRequest_fromHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                }

                if (scheduleDetail.SchedulerEmail != null && scheduleDetail.SchedulerFirstName != null)
                {
                    CandidateRequest_toValueLabel.Text = scheduleDetail.SchedulerFirstName +
                        " &lt;" + scheduleDetail.SchedulerEmail + "&gt;";

                    // Get the FROM and TO email address
                    CandidateRequest_toHiddenField.Value = scheduleDetail.SchedulerEmail.ToString();
                }

                // Change the subject according to the request type
                if (Request.QueryString["requesttype"] == "retake")
                {
                    CandidateRequest_subjectValueLabel.Text =
                        string.Format("Request to retake test {0} :{1}",
                        scheduleDetail.TestID,
                        scheduleDetail.TestName);
            }
                else
                {
                    CandidateRequest_subjectValueLabel.Text = 
                        string.Format("Request to activate test {0} :{1}",
                        scheduleDetail.TestID,
                        scheduleDetail.TestName);
                }
            }
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            CandidateRequest_topErrorMessageLabel.Text = string.Empty;
            CandidateRequest_topSuccessMessageLabel.Text = string.Empty;
            CandidateRequest_bottomErrorMessageLabel.Text = string.Empty;
            CandidateRequest_bottomSuccessMessageLabel.Text = string.Empty;

            CandidateRequest_topErrorMessageLabel.Visible = false;
            CandidateRequest_topSuccessMessageLabel.Visible = false;
            CandidateRequest_bottomErrorMessageLabel.Visible = false;
            CandidateRequest_bottomSuccessMessageLabel.Visible = false;
        }

        #endregion Private Methods
    }
}