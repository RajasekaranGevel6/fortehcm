﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="PopupUnderConstruction.aspx.cs" Title="Popup UnderConstruction" Inherits="Forte.HCM.UI.Popup.PopupUnderConstruction" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="PopupUnderConstruction_content" runat="server" ContentPlaceHolderID="MainContent">
 <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 980px">
                    <div style="width: 468px; float: left">
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="PopupUnderConstruction_errorTitleLabel" runat="server" Text="Error"
                                CssClass="unhandled_error_title"></asp:Label>
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="PopupUnderConstruction_errorValueLabel" runat="server" Text="Page under construction"
                                CssClass="unhandled_error_message"></asp:Label>
                        </div>
                        <div style="width: 458px; float: left; height: 20px">
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="PopupUnderConstruction_messageTitleLabel" runat="server" Text="Message"
                                CssClass="unhandled_error_title"></asp:Label>
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="PopupUnderConstruction_messageValueLabel" runat="server" Text="The requested page cannot be accessed at this time . The page might under be construction . Please contact your administrator"
                                CssClass="unhandled_error_message"></asp:Label>
                        </div>
                        <div style="width: 458px; float: left; height: 120px">
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left;" class="unhandled_error_message_link_message">
                            Click here to go to
                            <asp:LinkButton ID="PopupUnderConstruction_homeLinkButton" runat="server"
                                Text="Home" CssClass="unhandled_error_message_link_btn" PostBackUrl="~/Dashboard.aspx"
                                ToolTip="Click here to go to home page">
                            </asp:LinkButton>
                            page
                        </div>
                        <div style="width: 458px; padding-left: 10px; padding-top: 10px; float: left;" class="unhandled_error_message_link_message">
                            Click here to go to
                            <asp:LinkButton ID="PopupUnderConstruction_feedbackLinkButton" runat="server"
                                Text="Feedback" CssClass="unhandled_error_message_link_btn" PostBackUrl="~/General/Feedback.aspx"
                                ToolTip="Click here to go to feedback page">
                            </asp:LinkButton>
                            page
                        </div>
                    </div>
                    <div style="width: 508px; float: left" class="under_construction_background">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>