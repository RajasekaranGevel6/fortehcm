﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// MultiSeriesReportZoomedChart.cs
// File that represents the user interface and coding 
// for the multiple series report zoomed chart
//
#endregion Header

#region Directives                                                             
using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    public partial class MultiSeriesReportZoomedChart : PageBase
    {
        #region Event Handlers                                                 
        /// <summary>
        /// Handler method that is called when the page is loaded
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/>that holds the sender of the event
        /// </param>
        /// <param name="e">
        /// A<see cref="EventArgs"/>that holds the EventArgs
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Load the chart values 
                LoadValues();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                // Show error messages to the user
                base.ShowMessage(MultiSeriesReportZoomedChart_topErrorMessageLabel,
                    exception.Message);
            }
        }

        #endregion Event Handlers

        #region Overridden Methods                                             

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Represents the method to load the values 
        /// </summary>
        protected override void LoadValues()
        {
            //String that holds the session name
            string sessionId = Request.QueryString["sessionName"];

            if (Session[sessionId] != null)
            {
                //Get the chart data source from session
                MultipleSeriesChartData chartDetails = Session[sessionId] as MultipleSeriesChartData;

              

                //Set the header literal text
                MultiSeriesReportZoomedChart_headerLiteral.Text = chartDetails.ChartTitle;

                //Assign the tooltip for each series
                foreach (Series item in MultiSeriesReportZoomedChart_chart.Series)
                {
                    item.ToolTip = "#VAL";
                    item.ChartType = chartDetails.ChartType;
                }

                //Assign the x axis title 
                //Defines whether the axis title has to display or not
                if (chartDetails.IsDisplayAxisTitle)
                {
                    MultiSeriesReportZoomedChart_chart.ChartAreas
                        ["MultiSeriesReportZoomedChart_chartArea"].AxisX.Title = chartDetails.XAxisTitle;

                    MultiSeriesReportZoomedChart_chart.ChartAreas
                        ["MultiSeriesReportZoomedChart_chartArea"].AxisY.Title = chartDetails.YAxisTitle;
                }

                MultiSeriesReportZoomedChart_chart.Series.Clear();


                if (!chartDetails.IsComparisonReport)
                {
                    //Data bind the table 
                    MultiSeriesReportZoomedChart_chart.DataBindTable
                        (chartDetails.MultipleSeriesChartDataSource, "Name");
                }
                else
                {
                    DataView dataView = chartDetails.ComparisonReportDataTable.DefaultView;

                    //Databind the values to the chart
                    MultiSeriesReportZoomedChart_chart.DataBindTable(dataView,
                        "Name");
                }

                //Set the y axis interval to the whole number 
                if (chartDetails.IsFullNumberYAxis)
                    MultiSeriesReportZoomedChart_chart.ChartAreas
                        ["MultiSeriesReportZoomedChart_chartArea"].AxisY.Interval = 1;

                //Assign the tooltip for each chart series 
                foreach (Series item in MultiSeriesReportZoomedChart_chart.Series)
                {
                    item.ToolTip = "#VAL";
                    item.ChartType = chartDetails.ChartType;
                    item.IsValueShownAsLabel = true;
                }
                if (chartDetails.IsHideComparativeScores)
                {
                    if (!chartDetails.IsComparisonReport)
                    {
                        MultiSeriesReportZoomedChart_chart.Series["MinScore"].Enabled = false;
                        MultiSeriesReportZoomedChart_chart.Series["MaxScore"].Enabled = false;
                        MultiSeriesReportZoomedChart_chart.Series["AvgScore"].Enabled = false;
                    }
                    else
                    {
                        MultiSeriesReportZoomedChart_chart.Series["Min Score"].Enabled = false;
                        MultiSeriesReportZoomedChart_chart.Series["Max Score"].Enabled = false;
                        MultiSeriesReportZoomedChart_chart.Series["Avg Score"].Enabled = false; 
                    }
                }

                //Set the height and width for chart
                SetChartHeightAndWidth(chartDetails);
            }
        }

        #endregion Overridden Methods

        #region Private Methods                                                
        /// <summary>
        /// Represents the method to set the chart height and width 
        /// </summary>
        /// <param name="chartDetails">
        /// A<see cref="SingleChartData"/>that holds the chart data
        /// </param>
        private void SetChartHeightAndWidth(MultipleSeriesChartData chartDetails)
        {
            //Initialize the default width
            int defaultLength = 100;

            int chartWidth = 0;

            //Initialize the default height
            //int defaultHeightCount = 23;

            //int chartHeight = 0;

            int maxLength = 0;

            if (chartDetails.MultipleSeriesChartDataSource == null && 
                chartDetails.ComparisonReportDataTable == null)
                return;

            if (chartDetails.MultipleSeriesChartDataSource != null)
            {
                //Find the maximum length of the x axis variable
                foreach (MultipleChartData item in chartDetails.MultipleSeriesChartDataSource)
                {
                    maxLength += item.Name.Trim().Length;
                }
            }
            if (chartDetails.ComparisonReportDataTable != null)
            {
                foreach (DataRow row in chartDetails.ComparisonReportDataTable.Rows)
                {
                     maxLength += row["Name"].ToString().Trim().Length;
                }
            }

            //if the length of string is greater than 33
            if (maxLength > defaultLength)
            {
                //Increase the chart Width by 15 px for each character
                chartWidth = (maxLength - defaultLength) * 15;

                int defaultChartWidth = int.Parse(MultiSeriesReportZoomedChart_chart.Width.Value.ToString());

                //set the chart width to new value
                MultiSeriesReportZoomedChart_chart.Width = Unit.Pixel(chartWidth + defaultChartWidth);
            }

            //check the maximum count of the number of chart data
            //if (chartDetails.ChartData.Count > defaultHeightCount)
            //{
            //    defaultHeightCount = chartDetails.ChartData.Count;

            //    //Increase the chart Width by 20 px for each line
            //    chartHeight = (defaultHeightCount - 23) * 20;

            //    int defaultChartHeight = int.Parse(ZoomedChart_chart.Height.Value.ToString());

            //    //set the chart height to new value
            //    ZoomedChart_chart.Height = Unit.Pixel(chartHeight + defaultChartHeight);
            //}

        }

        #endregion Private Methods
    }
}
