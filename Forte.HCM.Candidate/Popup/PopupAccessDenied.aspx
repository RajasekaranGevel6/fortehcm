﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/SiteMaster.Master"
    CodeBehind="PopupAccessDenied.aspx.cs" Inherits="Forte.HCM.UI.Popup.PopupAccessDenied" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="PopupAccessDenied_content" runat="server" ContentPlaceHolderID="MainContent">
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div style="width: 980px">
                    <div style="width: 488px; float: left">
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="PopupAccessDenied_errorTitleLabel" runat="server" Text="Error" CssClass="unhandled_error_title"></asp:Label>
                        </div>
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="PopupAccessDenied_errorValueLabel" runat="server" Text="Access denied"
                                CssClass="unhandled_error_message"></asp:Label>
                        </div>
                        <div style="width: 468px; float: left; height: 20px">
                        </div>
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="PopupAccessDenied_messageTitleLabel" runat="server" Text="Message"
                                CssClass="unhandled_error_title"></asp:Label>
                        </div>
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left">
                            <asp:Label ID="PopupAccessDenied_messageValueLabel" runat="server" Text="You do not have sufficient rights to access to the specific page"
                                CssClass="unhandled_error_message"></asp:Label>
                        </div>
                        <div style="width: 468px; float: left; height: 120px">
                        </div>
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left;" class="unhandled_error_message_link_message">
                            Click here to go to
                            <asp:LinkButton ID="PopupAccessDenied_homeLinkButton" runat="server" Text="Home"
                                CssClass="unhandled_error_message_link_btn" PostBackUrl="~/Dashboard.aspx" ToolTip="Click here to go to home page">
                            </asp:LinkButton>
                            page
                        </div>
                        <div style="width: 468px; padding-left: 10px; padding-top: 10px; float: left;" class="unhandled_error_message_link_message">
                            Click here to go to
                            <asp:LinkButton ID="PopupAccessDenied_feedbackLinkButton" runat="server" Text="Feedback"
                                CssClass="unhandled_error_message_link_btn" PostBackUrl="~/General/Feedback.aspx"
                                ToolTip="Click here to go to feedback page">
                            </asp:LinkButton>
                            page
                        </div>
                    </div>
                    <div style="width: 488px; float: left" class="access_denied_background">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
