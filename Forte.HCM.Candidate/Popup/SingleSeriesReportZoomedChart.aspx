﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SingleSeriesReportZoomedChart.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.SingleSeriesReportZoomedChart" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ZoomedChart_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 90%" class="popup_header_text_grey">
                            <asp:Literal ID="SingleSeriesReportZoomedChart_headerLiteral" runat="server" Text=""></asp:Literal>
                        </td>
                        <td style="width: 10%" align="right">
                            <asp:ImageButton ID="ZommedChart_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="SingleSeriesReportZoomedChart_topSuccessMessageLabel" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="SingleSeriesReportZoomedChart_topErrorMessageLabel" runat="server"
                    SkinID="sknErrorMessage"></asp:Label><asp:HiddenField ID="SearchQuestion_stateHiddenField"
                        runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="popupcontrol_question_inner_bg">
                            <div style="height: 460px; overflow: auto; width: 900px">
                                <asp:Chart ID="SingleSeriesReportZoomedChart_chart" runat="server" Palette="Pastel"
                                    BackColor="Transparent" AntiAliasing="Graphics" Width="900px" Height="450px">
                                    <Series>
                                        <asp:Series Name="SingleSeriesReportZoomedChart_chartPrimarySeries" ChartArea="SingleSeriesReportZoomedChart_chartArea">
                                        </asp:Series>
                                    </Series>
                                    <Titles>
                                        <asp:Title Name="SingleSeriesReportZoomedChart_chartTitle" TextStyle="Default" ForeColor="180, 65, 140, 240"
                                            Font="Arial, 8.25pt, style=Bold">
                                        </asp:Title>
                                    </Titles>
                                    <ChartAreas>
                                        <asp:ChartArea Name="SingleSeriesReportZoomedChart_chartArea" BackSecondaryColor="Transparent"
                                            BackColor="Transparent" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                            <AxisY>
                                                <MajorGrid Enabled="false" />
                                                <LabelStyle ForeColor="180, 65, 140, 240" />
                                            </AxisY>
                                            <AxisX>
                                                <MajorGrid Enabled="false" />
                                                <LabelStyle ForeColor="180, 65, 140, 240" />
                                            </AxisX>
                                            <Position Height="91" Width="100" Y="5" />
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="popup_td_padding_10">
                <asp:LinkButton ID="SingleSeriesReportZoomedChart_cancelLinkButton" SkinID="sknPopupLinkButton"
                    runat="server" Text="Cancel" OnClientClick="javascript:CloseMe()" />
            </td>
        </tr>
    </table>
</asp:Content>
