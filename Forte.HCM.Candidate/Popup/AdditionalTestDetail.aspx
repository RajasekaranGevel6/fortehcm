<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionalTestDetail.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.AdditionalTestDetail" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<%@ Register Src="~/CommonControls/SingleSeriesChartControl.ascx" TagName="SingleChartControl"
    TagPrefix="uc1" %>
<asp:Content ID="AdditionalTestDetail_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">

    <script src="../JS/ChartScript.js" type="text/javascript"></script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="popup_td_padding_10">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="popup_header_text_grey">
                            <asp:Literal ID="AdditionalTestDetail_headerLiteral" runat="server" Text="Test Statistics"></asp:Literal>
                        </td>
                        <td style="width: 50%" align="right">
                            <asp:ImageButton ID="AdditionalTestDetail_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                OnClientClick="javascript:CloseMe()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_10" valign="top" align="left">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="popupcontrol_question_inner_bg">
                    <tr>
                        <td class="popup_td_padding_10">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="msg_align">
                                        <asp:Label ID="AdditionalTestDetail_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                        <asp:Label ID="AdditionalTestDetail_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 10%">
                                                    <asp:Label ID="AdditionalTestDetail_testIDLabel" runat="server" Text="Test ID" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 30%">
                                                    <asp:Label ID="AdditionalTestDetail_testIDValueLabel" runat="server" Text="" SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                                <td style="width: 10%">
                                                    <asp:Label ID="AdditionalTestDetail_testNameLabel" runat="server" Text="Test Name"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 50%">
                                                    <asp:Label ID="AdditionalTestDetail_testNameValueLabel" runat="server" Text="" ReadOnly="true"
                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="header_bg">
                                        <asp:Literal ID="AdditionalTestDetail_detailsHeaderLabel" runat="server" Text="Test Details"
                                            SkinID="sknLabelText"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left">
                                                    <uc1:SingleChartControl ID="AdditionalTestDetail_categoryChartControl" runat="server" />
                                                </td>
                                                <td align="left">
                                                    <uc1:SingleChartControl ID="AdditionalTestDetail_subjectChartControl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <uc1:SingleChartControl ID="AdditionalTestDetail_testAreaChartControl" runat="server" />
                                                </td>
                                                <td align="left">
                                                    <uc1:SingleChartControl ID="AdditionalTestDetail_complexityChartControl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header_bg">
                                        <asp:Literal ID="AdditionalTestDetail_additionalDetailsLabel" runat="server" Text="Summary"
                                            SkinID="sknLabelText"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid_body_bg">
                                        <table width="100%" cellspacing="3" cellpadding="0">
                                            <tr>
                                                <td style="width: 15%">
                                                    <asp:Label ID="AdditionalTestDetail_noOfAdministeredLabel" runat="server" Text="Number Of Questions"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="AdditionalTestDetail_noOfAdministeredValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                        Text=""></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AdditionalTestDetail_avgTimeTakenLabel" runat="server" Text="Average Time Taken"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="AdditionalTestDetail_avgTimeTakenValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                        Text=""></asp:Label>
                                                </td>
                                                <td style="width: 30%">
                                                    <asp:Label ID="AdditionalTestDetail_minScoreLabel" runat="server" Text="Minimum Score Obtained"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 19%">
                                                    <asp:Label ID="AdditionalTestDetail_minScoreValueLabel" runat="server" ReadOnly="True"
                                                        SkinID="sknLabelFieldText" Text="" Width="60%"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 25%">
                                                    <asp:Label ID="AdditionalTestDetail_maxScoreLabel" runat="server" Text="Maximum Score Obtained"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="AdditionalTestDetail_maxScoreValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                        Text=""></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="AdditionalTestDetail_avgScoreLabel" runat="server" Text="Average Score"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 8%">
                                                    <asp:Label ID="AdditionalTestDetail_avgScoreValueLabel" runat="server" ReadOnly="True"
                                                        SkinID="sknLabelFieldText" Text=""></asp:Label>
                                                </td>
                                                <td style="width: 30%">
                                                    <asp:Label ID="AdditionalTestDetail_costLabel" runat="server" Text="Cost (in $)"
                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                </td>
                                                <td style="width: 19%">
                                                    <asp:Label ID="AdditionalTestDetail_costValueLabel" runat="server" SkinID="sknLabelFieldText"
                                                        Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="AdditionalTestDetail_viewCertificationTR" runat="server" visible="false">
                                                <td colspan="2">
                                                    <asp:LinkButton ID="AdditionalTestDetail_viewCertificateLinkButton" runat="server"
                                                        SkinID="sknActionLinkButton" Text="View Certificate"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="popup_td_padding_left_8" align="left">
                <asp:LinkButton ID="AdditionalTestDetail_bottomCancelButton" runat="server" Text="Cancel"
                    SkinID="sknPopupLinkButton" OnClientClick="javascript:window.close();"></asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>
