﻿#region Header                                                                 

// Copyright (C) ForteHCM. All rights reserved.
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestReminder.cs
// File that represents the user interface layout and functionalities for
// the TestReminder page. This helps in setting multiple reminders for 
// pending tests and interviews about to take. 

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the TestReminder page. This helps in setting multiple reminders for 
    /// pending tests and interviews about to take. This class inherits 
    /// Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class TestReminder : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <see cref="string"/> that holds the activity type. This indicates
        /// either test (TST) or interview (INT).
        /// </summary>
        private string activityType = null;

        #endregion Private Variables

        #region Event Handlers                                                 

        //// <summary>
        /// Method that is called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                Page.Form.DefaultButton = TestReminder_setButton.UniqueID;

                if (Request.QueryString["activitytype"] != null)
                    activityType = Request.QueryString["activitytype"].Trim().ToUpper();

                // Set caption.
                SetCaption();

                //TestReminder_bottomCancelLinkButton.Attributes.Add("onclick", "Javascript:return CloseMe();");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (!IsPostBack)
                {
                    Page.Form.DefaultFocus = TestReminder_expectedDateTextBox.UniqueID;
                    TestReminder_expectedDateTextBox.Focus();
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReminder_topErrorMessageLabel,
                    TestReminder_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event handler that will fire when the submit button is clicked. 
        /// During that time, it'll insert/update reminder settings 
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        /// <remarks>
        /// This will reset the popup to the defaut stage.
        /// </remarks>
        protected void TestReminder_setButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate the input fields
                if (!IsValidData())
                    return;

                if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
                    return;

                if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
                    return;

                SetReminder(Request["candidatesessionid"].ToString().Trim(),
                    Convert.ToInt32(Request.QueryString["attemptid"]));
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(TestReminder_topErrorMessageLabel, 
                    TestReminder_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handler                                                                   

        #region Private Methods                                                

        /// <summary>
        /// Load reminder attributes.
        /// </summary>      
        private void LoadReminderAttributes()
        {
            List<AttributeDetail> attributeDetail = new AttributeBLManager().GetReminderAttributes();
            if (attributeDetail != null && attributeDetail.Count > 0)
            {
                TestReminder_timeFrameCheckBoxList.DataSource = attributeDetail;
                TestReminder_timeFrameCheckBoxList.DataTextField = "AttributeName";
                TestReminder_timeFrameCheckBoxList.DataValueField = "AttributeID";
                TestReminder_timeFrameCheckBoxList.DataBind();
            }
        }

        /// <summary>
        /// Method that Insert reminder details
        /// </summary>
        /// <param name="candidateSessionID">
        /// A <see cref="candidatesessionid"/> that contains the candidate session ID.
        /// </param>
        /// <param name="attemptID">
        /// A <see cref="attemptid"/> that contains the attempt ID.
        /// </param>       
        private void SetReminder(string candidateSessionID, int attemptID)
        {
            if (activityType == "TST")
            {
                TestReminderDetail reminderDetails = new TestReminderDetail();
                reminderDetails.Intervals = GetSelectedTimeFrameID();
                reminderDetails.CandidateSessionID = candidateSessionID;
                reminderDetails.AttemptID = attemptID;
                if (IsSelected(TestReminder_timeFrameCheckBoxList))
                {
                    DateTime scheduledDate =
                        DateTime.Parse(TestReminder_expectedDateTextBox.Text
                        + " " + TestReminder_expectedTimeTextBox.Text);

                    reminderDetails.ReminderDate = scheduledDate;
                    reminderDetails.UserID = base.userID;
                    new TestSchedulerBLManager().SaveTestReminder(reminderDetails);
                    base.ShowMessage(TestReminder_topSuccessMessageLabel,
                        TestReminder_bottomSuccessMessageLabel,
                          Resources.HCMResource.TestReminder_TestReminderSetSuccessfully);
                }
                else
                {
                    new TestSchedulerBLManager().DeleteReminderAll
                        (reminderDetails.CandidateSessionID, reminderDetails.AttemptID);
                    base.ShowMessage(TestReminder_topSuccessMessageLabel,
                        TestReminder_bottomSuccessMessageLabel,
                    Resources.HCMResource.TestReminder_TestReminderReset);
                }
            }
            else if (activityType == "INT")
            {
                InterviewReminderDetail reminderDetail = new InterviewReminderDetail();
                reminderDetail.Intervals = GetSelectedTimeFrameID();
                reminderDetail.CandidateSessionID = candidateSessionID;
                reminderDetail.AttemptID = attemptID;
                if (IsSelected(TestReminder_timeFrameCheckBoxList))
                {
                    DateTime scheduledDate =
                        DateTime.Parse(TestReminder_expectedDateTextBox.Text
                        + " " + TestReminder_expectedTimeTextBox.Text);

                    reminderDetail.ReminderDate = scheduledDate;
                    reminderDetail.UserID = base.userID;
                    new CandidateBLManager().SaveInterviewReminder(reminderDetail);

                    base.ShowMessage(TestReminder_topSuccessMessageLabel,
                        TestReminder_bottomSuccessMessageLabel,
                          Resources.HCMResource.TestReminder_InterviewReminderSetSuccessfully);
                }
                else
                {
                    new CandidateBLManager().DeleteAllInterviewReminders
                        (reminderDetail.CandidateSessionID, reminderDetail.AttemptID);
                    base.ShowMessage(TestReminder_topSuccessMessageLabel,
                        TestReminder_bottomSuccessMessageLabel,
                    Resources.HCMResource.TestReminder_InterviewReminderReset);
                }
            }

            // Reload the values.
            LoadValues();
        }

        /// <summary>
        /// This method checks selected or not in a checkbox list
        /// </summary>
        /// <param name="chcboxlst"></param>
        /// <returns name="bool"></returns>
        public bool IsSelected(CheckBoxList chcboxlst)
        {
            bool IsPreset = false;
            for (int i = 0; i < chcboxlst.Items.Count; i++)
            {
                if (chcboxlst.Items[i].Selected)
                {
                    return IsPreset = true;
                }
            }
            return IsPreset;
        }

        private List<ReminderIntervalDetail> GetSelectedTimeFrameID()
        {
            List<ReminderIntervalDetail> reminderIntervalDetails = null;
            ReminderIntervalDetail reminderIntervalDetail = null;
            try
            {
                List<string> Existing = (List<string>)ViewState["SELECTED_ITEMS"];
                for (int i = 0; i < TestReminder_timeFrameCheckBoxList.Items.Count; i++)
                {
                    if (reminderIntervalDetails == null)
                        reminderIntervalDetails = new List<ReminderIntervalDetail>();
                    if (TestReminder_timeFrameCheckBoxList.Items[i].Selected)
                    {
                        if (reminderIntervalDetail == null)
                            reminderIntervalDetail = new ReminderIntervalDetail();
                        reminderIntervalDetail.IntervalID = TestReminder_timeFrameCheckBoxList.Items[i].Value;
                        if (Existing == null)
                            reminderIntervalDetail.RecordStatus = RecordStatus.New;
                        else if (Existing.IndexOf(TestReminder_timeFrameCheckBoxList.Items[i].Value) >= 0)
                            reminderIntervalDetail.RecordStatus = RecordStatus.Modified;
                        else
                            reminderIntervalDetail.RecordStatus = RecordStatus.New;

                        reminderIntervalDetails.Add(reminderIntervalDetail);
                        reminderIntervalDetail = null;
                    }
                    else
                    {
                        if (Existing != null)
                        {
                            if (Existing.IndexOf(TestReminder_timeFrameCheckBoxList.Items[i].Value) >= 0)
                            {
                                if (reminderIntervalDetail == null)
                                    reminderIntervalDetail = new ReminderIntervalDetail();
                                reminderIntervalDetail.IntervalID = TestReminder_timeFrameCheckBoxList.Items[i].Value;
                                reminderIntervalDetail.RecordStatus = RecordStatus.Deleted;
                                reminderIntervalDetails.Add(reminderIntervalDetail);
                                reminderIntervalDetail = null;
                            }
                        }
                    }
                }
                return reminderIntervalDetails;
            }
            finally
            {
                if (reminderIntervalDetail != null) reminderIntervalDetail = null;
                if (reminderIntervalDetails != null) reminderIntervalDetails = null;
            }
        }

        /// <summary>
        /// Method that sets the caption based on activity type.
        /// </summary>
        private void SetCaption()
        {
            if (activityType == "TST")
            {
                TestReminder_headerLiteral.Text = Resources.HCMResource.TestReminder_TestReminderTitle;
                Master.SetPageCaption(Resources.HCMResource.TestReminder_TestReminderTitle);
                TestReminder_testNameLabel.Text = "Test Name";
                TestReminder_testDescriptionLabel.Text = "Test Description";
                TestReminder_scheduleDateImageButton.ToolTip = "Click here to select the expected date & time (in 24 hrs format) to take the test";
            }
            else if (activityType == "INT")
            {
                TestReminder_headerLiteral.Text = Resources.HCMResource.TestReminder_InterviewReminderTitle;
                Master.SetPageCaption(Resources.HCMResource.TestReminder_InterviewReminderTitle);
                TestReminder_testNameLabel.Text = "Interview Name";
                TestReminder_testDescriptionLabel.Text = "Interview Description";
                TestReminder_scheduleDateImageButton.ToolTip = "Click here to select the expected date & time (in 24 hrs format) to take the interview";
            }
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            TestReminder_topErrorMessageLabel.Text = string.Empty;
            TestReminder_topSuccessMessageLabel.Text = string.Empty;
            TestReminder_topErrorMessageLabel.Visible = false;
            TestReminder_topSuccessMessageLabel.Visible = false;

            TestReminder_bottomErrorMessageLabel.Text = string.Empty;
            TestReminder_bottomSuccessMessageLabel.Text = string.Empty;
            TestReminder_bottomErrorMessageLabel.Visible = false;
            TestReminder_bottomSuccessMessageLabel.Visible = false;
        }

        #endregion Private Methods                                             

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            // Validate reason feild
            bool isValidData = true;
            DateTime timeLimit;
            TestReminder_expectedDtMaskedEditValidator.Validate();

            if (IsSelected(TestReminder_timeFrameCheckBoxList))
            {
                if (TestReminder_expectedDateTextBox.Text.Trim().Length == 0)
                {
                    isValidData = false;
                    base.ShowMessage(TestReminder_topErrorMessageLabel,
                        TestReminder_bottomErrorMessageLabel,
                        TestReminder_expectedDtMaskedEditValidator.EmptyValueMessage.ToString());
                }
                // This will check if the date is valid date. i.e. 99/99/9999
                else if (!TestReminder_expectedDtMaskedEditValidator.IsValid)
                {
                    isValidData = false;
                    base.ShowMessage(TestReminder_topErrorMessageLabel,
                        TestReminder_bottomErrorMessageLabel,
                        TestReminder_expectedDtMaskedEditValidator.InvalidValueMessage.ToString());
                }

                else if (DateTime.Parse(TestReminder_expectedDateTextBox.Text) >
                DateTime.Parse(TestReminder_expiryDateValueLabel.Text) ||
                DateTime.Parse(TestReminder_expectedDateTextBox.Text) < DateTime.Today)
                {
                    isValidData = false;
                    base.ShowMessage(TestReminder_topErrorMessageLabel,
                        TestReminder_bottomErrorMessageLabel,
                        Resources.HCMResource.TestReminder_ScheduledDateValidation);
                }
                else if (TestReminder_expectedTimeTextBox.Text.Trim().Length == 0)
                {
                    isValidData = false;
                    base.ShowMessage(TestReminder_topErrorMessageLabel,
                        TestReminder_bottomErrorMessageLabel,
                        TestReminder_expectedTimetMaskedEditValidator.EmptyValueMessage.ToString());
                }
                else
                {
                    if (DateTime.TryParse(TestReminder_expectedTimeTextBox.Text, out timeLimit) == false)
                    {
                        isValidData = false;
                        base.ShowMessage(TestReminder_topErrorMessageLabel,
                            TestReminder_bottomErrorMessageLabel,
                            TestReminder_expectedTimetMaskedEditValidator.InvalidValueMessage);
                    }
                    else
                    {
                        DateTime differenceDateTime = DateTime.Now;
                        DateTime enteredDateTime = DateTime.Parse(TestReminder_expectedDateTextBox.Text
                                          + " " + TestReminder_expectedTimeTextBox.Text);

                        string errorMessage = string.Empty;
                        foreach (ListItem item in TestReminder_timeFrameCheckBoxList.Items)
                        {
                            if (!item.Selected)
                                continue;

                            switch (item.Value)
                            {
                                case "ONE_HR":
                                    differenceDateTime = enteredDateTime.AddHours(-1);
                                    break;

                                case "TWO_HR":
                                    differenceDateTime = enteredDateTime.AddHours(-2);
                                    break;

                                case "SIX_HR":
                                    differenceDateTime = enteredDateTime.AddHours(-6);
                                    break;

                                case "ONE_DAY":
                                    differenceDateTime = enteredDateTime.AddHours(-24);
                                    break;

                                case "TWO_DAY":
                                    differenceDateTime = enteredDateTime.AddHours(-48);
                                    break;
                            }

                            if (differenceDateTime < DateTime.Now)
                            {
                                base.ShowMessage(TestReminder_topErrorMessageLabel, TestReminder_bottomErrorMessageLabel,
                                    "Reminder cannot be set for '"
                                    + item.Text + "'. The reminder time is elapsed");
                                isValidData = false;
                            }
                        }
                    }
                }
            }

            return isValidData;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Load reminder attributes.
            LoadReminderAttributes();

            if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
                return;

            if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
                return;

            List<string> selectedItems  = null;

            if (activityType == "TST")
            {
                TestReminderDetail reminderDetail = new TestSchedulerBLManager().GetTestReminder
                    (Request["candidatesessionid"].ToString().Trim(),
                    Convert.ToInt32(Request.QueryString["attemptid"].ToString().Trim()));

                TestReminder_testNameValueLabel.Text = reminderDetail.TestName;
                TestReminder_testDescriptionValueLabel.Text = 
                    reminderDetail.TestDescription == null ? reminderDetail.TestDescription : 
                        reminderDetail.TestDescription.ToString().Replace(Environment.NewLine, "<br />");
                TestReminder_expiryDateValueLabel.Text = reminderDetail.ExpiryDate.ToString("MM/dd/yyyy");
                TestReminder_scheduledByValueLabel.Text = reminderDetail.SchedulerName;

                if (Utility.IsNullOrEmpty(reminderDetail.ReminderDate) ||
                    GetDateFormat(reminderDetail.ReminderDate) == string.Empty)
                {
                    TestReminder_expectedDateTextBox.Text = DateTime.Now.ToShortDateString();
                    TestReminder_expectedTimeTextBox.Text = "00:00:00";
                }
                else
                {
                    TestReminder_expectedDateTextBox.Text = reminderDetail.ReminderDate.Date.ToString("MM/dd/yyyy");
                    TestReminder_expectedTimeTextBox.Text = reminderDetail.ReminderDate.ToString("HH:mm:ss");
                }

                // To set selected Items in checkboxlist
                if (Utility.IsNullOrEmpty(reminderDetail.AttributeList))
                    return;

                selectedItems = reminderDetail.AttributeList;
                if (selectedItems.Count > 0)
                {
                    (from i in TestReminder_timeFrameCheckBoxList.Items.Cast<ListItem>()
                     where selectedItems.Contains(Convert.ToString(i.Value))
                     select i).ToList().ForEach(i => i.Selected = true);
                }
            }
            else if (activityType == "INT")
            {
                InterviewReminderDetail reminderDetail = new CandidateBLManager().GetInterviewReminder
                    (Request["candidatesessionid"].ToString().Trim(),
                    Convert.ToInt32(Request.QueryString["attemptid"].ToString().Trim()));

                TestReminder_testNameValueLabel.Text = reminderDetail.InterviewName;
                TestReminder_testDescriptionValueLabel.Text = 
                    reminderDetail.InterviewDescription == null ? reminderDetail.InterviewDescription : 
                        reminderDetail.InterviewDescription.ToString().Replace(Environment.NewLine, "<br />");
                TestReminder_expiryDateValueLabel.Text = reminderDetail.ExpiryDate.ToString("MM/dd/yyyy");
                TestReminder_scheduledByValueLabel.Text = reminderDetail.SchedulerName;

                if (Utility.IsNullOrEmpty(reminderDetail.ReminderDate) ||
                    GetDateFormat(reminderDetail.ReminderDate) == string.Empty)
                {
                    TestReminder_expectedDateTextBox.Text = DateTime.Now.ToShortDateString();
                    TestReminder_expectedTimeTextBox.Text = "00:00:00";
                }
                else
                {
                    TestReminder_expectedDateTextBox.Text = reminderDetail.ReminderDate.Date.ToString("MM/dd/yyyy");
                    TestReminder_expectedTimeTextBox.Text = reminderDetail.ReminderDate.ToString("HH:mm:ss");
                }

                // To set selected Items in checkboxlist
                if (Utility.IsNullOrEmpty(reminderDetail.AttributeList))
                    return;

                selectedItems = reminderDetail.AttributeList;
                if (selectedItems.Count > 0)
                {
                    (from i in TestReminder_timeFrameCheckBoxList.Items.Cast<ListItem>()
                     where selectedItems.Contains(Convert.ToString(i.Value))
                     select i).ToList().ForEach(i => i.Selected = true);
                }
            }

            // Viewstate that holds the selected intervals.
            ViewState["SELECTED_ITEMS"] = selectedItems;
        }

        #endregion Protected Overridden Methods                                
    }
}