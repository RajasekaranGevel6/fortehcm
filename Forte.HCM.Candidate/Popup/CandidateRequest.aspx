﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CandidateRequest.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.CandidateRequest" MasterPageFile="~/SiteMaster.Master" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="CandidateRequest_content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" language="javascript">
        
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div class="popup_right_panel">
                    <asp:UpdatePanel ID="CandidateRequest_updatePanel" runat="server">
                        <ContentTemplate>
                            <div class="popup_body_bg_div" style="float: left">
                                <div class="popup_header_text_bold_div" style="width: 578px">
                                    <asp:Literal ID="CandidateRequest_headerLiteral" runat="server"></asp:Literal>
                                </div>
                                <div class="popup_controls_header_bg_right">
                                    <div class="popup_controls_header_bg_left">
                                        <asp:Button ID="CandidateRequest_topSubmitButton" runat="server" Text="Submit" OnClick="CandidateRequest_submitButton_Click"
                                            SkinID="sknButtonId" TabIndex="5" />
                                    </div>
                                    <div class="popup_controls_header_bg_left">
                                        <asp:LinkButton ID="CandidateRequest_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknPopupLinkButton" TabIndex="6" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                             <div class="empty_height">
                                &nbsp;
                            </div>
                            <div class="popup_tab_body_bg_div">
                                <div class="msg_align">
                                    <asp:Label ID="CandidateRequest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="CandidateRequest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </div>
                                <div>
                                    <div class="popup_cont_div">
                                        <div class="popup_cont_div_left">
                                            <asp:Label ID="CandidateRequest_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </div>
                                        <div class="popup_cont_div_right">
                                            <div style="width: 450px; float: left">
                                                <asp:Label ID="CandidateRequest_fromValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                <asp:HiddenField ID="CandidateRequest_fromHiddenField" runat="server" />
                                            </div>
                                            <div style="width: 100px; float: left">
                                                <asp:CheckBox ID="CandidateRequest_sendMeACopyCheckBox" runat="server" Checked="true"
                                                    TextAlign="Right" Text="Send me a copy" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="popup_cont_div">
                                        <div class="popup_cont_div_left">
                                            <asp:Label ID="CandidateRequest_toLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </div>
                                        <div class="popup_cont_div_right">
                                            <asp:Label ID="CandidateRequest_toValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                            <asp:HiddenField ID="CandidateRequest_toHiddenField" runat="server" />
                                        </div>
                                    </div>
                                    <div class="popup_cont_div">
                                        <div class="popup_cont_div_left">
                                            <asp:Label ID="CandidateRequest_ccLabel" runat="server" Text="CC" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </div>
                                        <div class="popup_cont_div_right">
                                            <asp:TextBox ID="CandidateRequest_ccTextBox" runat="server" Width="99%" TextMode="MultiLine"
                                                SkinID="sknMultiLineTextBox" Height="23" MaxLength="100" onkeyup="CommentsCount(100,this)"
                                                onchange="CommentsCount(100,this)" TabIndex="1"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="popup_cont_div">
                                        <div class="popup_cont_div_left">
                                            <asp:Label ID="CandidateRequest_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </div>
                                        <div class="popup_cont_div_right">
                                            <asp:Label ID="CandidateRequest_subjectValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="popup_cont_div">
                                        <div class="popup_cont_div_left">
                                            <asp:Label ID="CandidateRequest_reasonLabel" runat="server" Text="Reason" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                            <span class="mandatory">*</span>
                                        </div>
                                        <div class="popup_cont_div_right">
                                            <asp:TextBox ID="CandidateRequest_reasonTextBox" TextMode="MultiLine" Height="200"
                                                runat="server" Width="99%" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                onchange="CommentsCount(500,this)" TabIndex="2">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="msg_align">
                                    <asp:Label ID="CandidateRequest_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="CandidateRequest_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </div>
                            </div>
                            <div class="popup_body_bg_div">
                                <div class="popup_header_text_bold_div" style="width: 578px">
                                    &nbsp;
                                </div>
                                <div class="popup_controls_header_bg_right">
                                    <div class="popup_controls_header_bg_left">
                                        <asp:Button ID="CandidateRequest_bottomSubmitButton" runat="server" Text="Submit" OnClick="CandidateRequest_submitButton_Click"
                                            SkinID="sknButtonId" TabIndex="5" />
                                    </div>
                                    <div class="popup_controls_header_bg_left">
                                        <asp:LinkButton ID="CandidateRequest_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknPopupLinkButton" TabIndex="6" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
