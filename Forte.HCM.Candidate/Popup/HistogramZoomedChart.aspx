﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HistogramZoomedChart.aspx.cs"
    MasterPageFile="~/MasterPages/PopupMaster.Master" Inherits="Forte.HCM.UI.Popup.HistogramZoomedChart"
    Title="Zoomed Chart" %>

<asp:Content ID="HistogramZoomedChart_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <div id="HistogramZoomedChart_mainDiv">
        <div style="padding-top: 20px;">
            <div class="popup_header_text_grey" style="float: left;">
                <asp:Literal ID="HistogramZoomedChart_headerLiteral" runat="server" Text=""></asp:Literal>
            </div>
            <div style="float: right;">
                <asp:ImageButton ID="HistogramZoomedChart_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                    OnClientClick="javascript:CloseMe()" />
            </div>
        </div>
        <div>
            <div class="msg_align">
                <asp:Label ID="HistogramZoomedChart_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="HistogramZoomedChart_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                    ID="SearchQuestion_stateHiddenField" runat="server" Value="0" />
            </div>
        </div>
        <div style="clear: both; padding-top: 10px;">
            <div class="popupcontrol_question_inner_bg">
                <div style="height: 460px; overflow: auto; width: 900px">
                    <asp:Chart ID="HistogramZoomedChart_chart" runat="server" BackColor="Transparent"
                        Palette="Pastel" Width="900px" Height="450px" AntiAliasing="Graphics">
                        <Titles>
                            <asp:Title Name="HistogramZoomedChart_chartTitle">
                            </asp:Title>
                        </Titles>
                        <Series>
                            <asp:Series Name="HistogramZoomedChart_chartPrimarySeries" BorderColor="64, 64, 64, 64"
                                IsValueShownAsLabel="true" Font="Helvetica, 11px">
                                <SmartLabelStyle AllowOutsidePlotArea="Yes" IsMarkerOverlappingAllowed="True" MaxMovingDistance="200" />
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="HistogramZoomedChart_chartArea" BorderColor="64, 64, 64, 64"
                                BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent"
                                BackGradientStyle="TopBottom">
                                <AxisX TitleFont="Trebuchet MS, 8pt" LineColor="64, 64, 64, 64" IsLabelAutoFit="False"
                                    IsStartedFromZero="true">
                                    <LabelStyle ForeColor="180, 65, 140, 240" Angle="0" />
                                    <MajorGrid Enabled="false" />
                                </AxisX>
                                <AxisY Interval="1">
                                    <MajorGrid Enabled="false" />
                                    <LabelStyle ForeColor="180, 65, 140, 240" />
                                </AxisY>
                                <Position Height="91" Width="100" Y="5" />
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
            </div>
        </div>
        <div class="empty_height">
            &nbsp;</div>
        <div style="float: left;">
            <asp:LinkButton ID="HistogramZoomedChart_cancelLinkButton" SkinID="sknPopupLinkButton"
                runat="server" Text="Cancel" OnClientClick="javascript:CloseMe()" />
        </div>
    </div>
</asp:Content>
