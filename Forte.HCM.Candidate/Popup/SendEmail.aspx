﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SendEmail.aspx.cs" Inherits="Forte.HCM.UI.Popup.SendEmail"
    MasterPageFile="~/SiteMaster.Master" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="SendEmail_content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div class="popup_right_panel">
                    <asp:UpdatePanel ID="SendEmail_updatePanel" runat="server">
                        <ContentTemplate>
                            <div class="popup_body_bg_div">
                                <div class="popup_header_text_bold_div" style="width: 590px">
                                    <asp:Literal ID="SendEmail_headerLiteral" runat="server" Text="Email"></asp:Literal>
                                </div>
                                <div class="popup_controls_header_bg_right">
                                    <div class="popup_controls_header_bg_left">
                                        <asp:Button ID="SendEmail_topSendButton" runat="server" Text="Send" OnClick="SendEmail_sendButton_Click"
                                            SkinID="sknButtonId" TabIndex="5" />
                                    </div>
                                    <div class="popup_controls_header_bg_left">
                                        <asp:LinkButton ID="SendEmail_topCancelLinkButton" runat="server" Text="Cancel" SkinID="sknPopupLinkButton"
                                            TabIndex="6" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="empty_height">
                                &nbsp;
                            </div>
                            <div class="popup_tab_body_bg_div">
                                <div class="msg_align">
                                    <asp:Label ID="SendEmail_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="SendEmail_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                </div>
                                <div>
                                    <div class="popup_cont_div">
                                        <div class="popup_cont_div_left">
                                            <asp:Label ID="SendEmail_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldText"></asp:Label>
                                        </div>
                                        <div class="popup_cont_div_right">
                                            <div style="width: 450px; float: left">
                                                <asp:Label ID="SendEmail_fromValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                <asp:HiddenField ID="SendEmail_fromEmailIDHiddenField" runat="server" />
                                            </div>
                                            <div style="width: 100px; float: left">
                                                <asp:CheckBox ID="SendEmail_sendMeACopyCheckBox" runat="server"
                                                    Checked="true" TextAlign="Right" Text="Send me a copy" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="SendEmail_toLabel" runat="server" Text="To" SkinID="sknLabelFieldText"></asp:Label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <div class="popup_cont_div_right">
                                        <asp:Label ID="SendEmail_toValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                        <asp:HiddenField ID="SendEmail_toEmailIDHiddenField" runat="server" />
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="SendEmail_ccLabel" runat="server" Text="CC" SkinID="sknLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="popup_cont_div_right">
                                        <asp:TextBox ID="SendEmail_ccTextBox" runat="server" Width="99%" TextMode="MultiLine"
                                            SkinID="sknMultiLineTextBox" Height="23" MaxLength="100" onkeyup="CommentsCount(100,this)"
                                            onchange="CommentsCount(100,this)" TabIndex="2"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="SendEmail_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldText"></asp:Label>
                                    </div>
                                    <div class="popup_cont_div_right">
                                        <asp:TextBox ID="SendEmail_subjectTextBox" TabIndex="3" runat="server" MaxLength="100"
                                            onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)" SkinID="sknMultiLineTextBox"
                                            Height="23" Width="99%" TextMode="MultiLine">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="SendEmail_messageLabel" runat="server" Text="Message" SkinID="sknLabelFieldText"></asp:Label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <div class="popup_cont_div_right">
                                        <asp:TextBox ID="SendEmail_messageTextBox" TabIndex="4" TextMode="MultiLine" Height="200"
                                            runat="server" Width="99%" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                            onchange="CommentsCount(500,this)" Style="border-bottom: 0px;">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="msg_align">
                                <asp:Label ID="SendEmail_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                <asp:Label ID="SendEmail_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                            </div>
                            </div>
                            
                            <div class="popup_body_bg_div">
                                <div class="popup_header_text_bold_div" style="width: 590px">
                                    &nbsp;
                                </div>
                                <div class="popup_controls_header_bg_right">
                                    <div class="popup_controls_header_bg_left">
                                        <asp:Button ID="SendEmail_bottomSendButton" runat="server" Text="Send" OnClick="SendEmail_sendButton_Click"
                                            SkinID="sknButtonId" TabIndex="5" />
                                    </div>
                                    <div class="popup_controls_header_bg_left">
                                        <asp:LinkButton ID="SendEmail_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknPopupLinkButton" TabIndex="6" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
