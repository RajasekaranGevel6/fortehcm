﻿#region Header                                                                 

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// PopupUnderConstruction.aspx.cs
// File that represents the user interface layout and functionalities
// for the PopupUnderConstruction page. This page helps in displaying
// page under construction error message to the user. This page
// will be shown when trying to access a wrong URL or upcoming feature.

#endregion Header

#region Directives                                                             

using System;
using System.Web.UI;

using Forte.HCM.Trace;
using Forte.HCM.UI.Common;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the PopupUnderConstruction page. This page helps in displaying 
    /// page under construction error message to the user. This page
    /// will be shown when trying to access a wrong URL or upcoming feature. 
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class PopupUnderConstruction : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Page Under Construction");
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Event Handlers

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        #endregion Protected Overridden Methods
    }
}