<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ZoomedChart.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.ZoomedChart"  %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="ZoomedChart_content" runat="server" ContentPlaceHolderID="PopupMaster_contentPlaceHolder">
    <div id="ZoomedChart_mainDiv">
        <div style="padding-top:20px;">
            <div class="popup_header_text_grey" style="float: left;">
                <asp:Literal ID="ZoomedChart_headerLiteral" runat="server" Text=""></asp:Literal></div>
            <div style="float: right;">
                <asp:ImageButton ID="ZommedChart_topCloseImageButton" runat="server" SkinID="sknCloseImageButton"
                    OnClientClick="javascript:CloseMe()" />
            </div>
        </div>
        <div>
            <div class="msg_align">
                <asp:Label ID="ZoomedChart_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="ZoomedChart_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label><asp:HiddenField
                    ID="SearchQuestion_stateHiddenField" runat="server" Value="0" />
            </div>
        </div>
        <div style="clear: both; padding-top: 10px;">
            <div class="popupcontrol_question_inner_bg">
                <div style="height: 460px; overflow: auto; width: 900px">
                    <asp:Chart ID="ZoomedChart_chart" runat="server" BackColor="Transparent" Palette="Pastel"
                        Width="900px" Height="450px" AntiAliasing="Graphics">
                        <Legends>
                            <asp:Legend Name="ZoomedChart_legend" BackColor="Transparent" IsEquallySpacedItems="True"
                                Font="Verdana, 9.25pt" IsTextAutoFit="False" Alignment="Center" ForeColor="114, 142, 192"
                                Docking="Right" IsDockedInsideChartArea="False" ItemColumnSpacing="80" MaximumAutoSize="100">
                            </asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Name="ZoomedChart_chartTitle">
                            </asp:Title>
                        </Titles>
                        <Series>
                            <asp:Series Name="ZoomedChart_chartPrimarySeries" BorderColor="64, 64, 64, 64" IsValueShownAsLabel="false"
                                Font="Helvetica, 11px">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ZoomedChart_chartArea" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent"
                                BackColor="Transparent" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                <Position Height="91" Width="50" />
                                <AxisX Interval="1">
                                    <MajorGrid Enabled="false" />
                                </AxisX>
                                <AxisY>
                                    <MajorGrid Enabled="false" />
                                </AxisY>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
            </div>
        </div>
        <div style="float: left;">
            <asp:LinkButton ID="ZoomedChart_cancelLinkButton" SkinID="sknPopupLinkButton" runat="server"
                Text="Cancel" OnClientClick="javascript:CloseMe()" />
        </div>
    </div>
</asp:Content>
