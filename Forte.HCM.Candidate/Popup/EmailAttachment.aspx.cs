﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// EmailAttachment.cs
// File that represents the user interface to send a mail with attachment
// to the users.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;

#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface to send test results to
    /// the respective candidates through email. This class is inherited
    /// from PageBase.    
    /// </summary>
    public partial class EmailAttachment : PageBase
    {
        #region Private Members                                                

        static BaseColor itemColor = new BaseColor(40, 48, 51);
        static BaseColor headerColor = new BaseColor(40, 48, 51);
        static BaseColor headerTitle1 = new BaseColor(71, 71, 70);
        static BaseColor headerTitle2 = new BaseColor(109, 109, 108);

        private string chartImagePath = string.Empty;
        private string baseURL = string.Empty;
        string bgImagePage1 = string.Empty;
        string bgImagePage2 = string.Empty;
        #endregion Private Members                                             

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that helps to implement the default settings.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ClearMessages();
                
                //Set browser title
                Master.SetPageCaption("EMail Attachment");

                string baseURL = ConfigurationManager.AppSettings["ATTACHMENT_URL"];

                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                //Load Style Sheets and Skin
                if (!Utility.IsNullOrEmpty(baseURL))
                {
                    chartImagePath = baseURL + "chart/";
                    bgImagePage1 = baseURL +
                        "App_Themes/DefaultTheme/Images/candidate_certificate_1.jpg";
                    bgImagePage2 = baseURL +
                        "App_Themes/DefaultTheme/Images/candidate_certificate_2.jpg";

                }
                
                // Set default button field
                Page.Form.DefaultButton = EmailAttachment_submitButton.UniqueID;

                // Validate email address
                EmailAttachment_submitButton.Attributes.Add("onclick", "javascript:return IsValidEmail('"
                    + EmailAttachment_ccTextBox.ClientID + "','"
                    + EmailAttachment_errorMessageLabel.ClientID + "','"
                    + EmailAttachment_successMessageLabel.ClientID + "');");
                
                EmailAttachment_attachmentImageButton.Attributes.Add("onclick", "javascript:return OpenPopUp('"
                    + Request.QueryString["candidatesessionid"] + "','" 
                    + Request.QueryString["attemptid"]+ "');");

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = EmailAttachment_toTextBox.UniqueID;
                    EmailAttachment_toTextBox.Focus();

                    UserDetail userDetail = new CommonBLManager().GetUserDetail(base.userID);

                    if (userDetail != null)
                    {
                        //EmailAttachment_fromValueLabel.Text = userDetail.FirstName
                        //    + " &lt;" + userDetail.Email + "&gt;";
                        //EmailAttachment_fromHiddenField.Value = userDetail.Email.Trim();
                        EmailAttachment_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                        EmailAttachment_fromHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                    }

                    // Call PDF construct method
                    if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "Y")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "PDF";
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_certificateDIV.Visible = false;
                        ConstructPDF();
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CERTIFICATE")
                    {
                        // Certificate for completed test
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_certificateDIV.Visible = false;
                        EmailAttachment_contentIframe.Style.Add("display", "none");

                        EmailAttachment_attachmentImageButton.ImageUrl
                            = "~/App_Themes/DefaultTheme/Images/icon_view_certificate.gif";
                        EmailAttachment_downloadTypeHiddenField.Value = "CERTIFICATE";
                        ConstructCertificate();
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CERTIFICATE_TEMPLATE")
                    {
                        // Certificate template for certificate test
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_contentIframe.Style.Add("display", "none");
                        EmailAttachment_certificateDIV.Visible = false;

                        EmailAttachment_attachmentImageButton.ImageUrl
                            = "~/App_Themes/DefaultTheme/Images/icon_view_certificate.gif";
                        EmailAttachment_downloadTypeHiddenField.Value = "CERTIFICATE_TEMPLATE";
                        ConstructCertificateTemplate();
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CONTENT")
                    {
                        // Send email as content
                        string pageName = string.Empty;

                        EmailAttachment_contentIframe.Style.Add("display", "none");
                        EmailAttachment_messageTextBox.Style.Add("height", "100px");
                        EmailAttachment_certificateDIV.Visible = true;

                        string certificateTemplateURL =
                            Session[Constants.SessionConstants.CERTIFICATE_IMAGE_URL].ToString().Replace("~/", "");
                        baseURL += certificateTemplateURL.Trim();

                        EmailAttachment_certificateImage.Attributes.Add("src", baseURL.Trim());
                        ViewState["CERTIFICATE_TEMPLATE_CONTENT_URL"] = baseURL.Trim();
                    }
                    else if (!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                   Request.QueryString["type"].ToUpper() == "PP")
                    {
                        EmailAttachment_downloadTypeHiddenField.Value = "PP";
                        EmailAttachment_attachmentTR.Style.Add("display", "block");
                        EmailAttachment_certificateDIV.Visible = false;
                       
                    }
                    else
                    {
                        EmailAttachment_certificateDIV.Visible = false;
                        string pageName = "CandidateTestResultTranscript.aspx";
                        EmailAttachment_contentIframe.Style.Add("display", "block");
                        EmailAttachment_messageTextBox.Style.Add("height", "100px");

                        if (!Utility.IsNullOrEmpty(baseURL))
                        {
                            EmailAttachment_contentIframe.Attributes["src"] = baseURL +
                                     "PrintPages/" + pageName + "?candidatesessionid=" +
                                     Request.QueryString["candidatesessionid"] + "&attemptid=" +
                                     Request.QueryString["attemptid"] + "&testkey=" +
                                     Request.QueryString["testkey"];
                        }
                    }

                    if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]) &&
                        !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]) &&
                        !Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
                    {
                        Session["CAND_SESSION_KEY"] = Request.QueryString["candidatesessionid"].ToString();
                        Session["ATTEMPT_ID"] = Request.QueryString["attemptid"].ToString();

                        // Set the tool tip for the attachment icon.
                        EmailAttachment_attachmentImageButton.ToolTip =
                            Request.QueryString["candidatesessionid"].ToString() + "_"
                            + Request.QueryString["attemptid"].ToString() + ".pdf";
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAttachment_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// This event handler helps to send an alert email
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void EmailAttachment_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear all the error and success labels
                EmailAttachment_errorMessageLabel.Text = string.Empty;
                EmailAttachment_successMessageLabel.Text = string.Empty;
                string fileName = string.Empty;
                bool isCertificateSent = false;
                bool isPositionProfileSent = false;

                // Validate the fields
                if (!IsValidData())
                    return;

                // Check if the querystrings are not empty.
                if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]) &&
                    !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
                {
                    fileName = Request.QueryString["candidatesessionid"].ToString() + "_"
                        + Request.QueryString["attemptid"].ToString() + ".pdf";
                }

                // Build Mail Details
                MailDetail mailDetail = new MailDetail();
                MailAttachment mailAttachment = new MailAttachment();
                mailDetail.Attachments = new List<MailAttachment>();
                mailDetail.To = new List<string>();
                mailDetail.CC = new List<string>();

                // Add the logged in user in the CC list when 'send me a copy ' is on
                if (EmailAttachement_sendMeACopyCheckBox.Checked)
                {
                    mailDetail.CC.Add(base.userEmailID);
                }

                if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "Y")
                {
                    byte[] attachementContent = (byte[])
                        Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF];
                    mailAttachment.FileContent = attachementContent;
                    mailAttachment.FileName = fileName;
                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CERTIFICATE")
                {
                    byte[] certificateAttachment = Session[Constants.SessionConstants.CERTIFICATE_IMAGE] as byte[];
                    mailAttachment.FileContent = certificateAttachment;
                    mailAttachment.FileName = "MyCertificate.JPG";
                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    isCertificateSent = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CERTIFICATE_TEMPLATE")
                {
                    byte[] certificateAttachment = Session[Constants.SessionConstants.CERTIFICATE_TEMPLATE_IMAGE] as byte[];
                    mailAttachment.FileContent = certificateAttachment;
                    mailAttachment.FileName = "CertificateTemplate.JPG";
                    mailDetail.Attachments.Add(mailAttachment);
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                    isCertificateSent = true;
                }
                else if (!Utility.IsNullOrEmpty(Request.QueryString["emailtype"]) &&
                        Request.QueryString["emailtype"].ToUpper() == "N" &&
                        !Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "CONTENT")
                {
                    string certificateTemplatePath =
                        "<img src='" + ViewState["CERTIFICATE_TEMPLATE_CONTENT_URL"].ToString().Trim()
                        + "' alt='Certificate'><br/>";
                    mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim() + certificateTemplatePath;
                    mailDetail.isBodyHTML = true;
                    isCertificateSent = true;
                }
                else if(!Utility.IsNullOrEmpty(Request.QueryString["type"]) &&
                        Request.QueryString["type"].ToUpper() == "PP")
                {
                    if (!Utility.IsNullOrEmpty(Session[Constants.SessionConstants.PP_PDF_CONTENT]) || !Utility.IsNullOrEmpty(Session[Constants.SessionConstants.PP_PDF_HEADER]))
                    {
                        CandidateReportDetail candidateReportDetail = new CandidateReportDetail();
                        candidateReportDetail = (CandidateReportDetail)Session[Constants.SessionConstants.PP_PDF_HEADER];
                        List<Dictionary<object, object>> dictinaryList = (List<Dictionary<object, object>>)Session[Constants.SessionConstants.PP_PDF_CONTENT];
                     
                        byte[] attachementContent = new WidgetReport().GetPDFPPByteArray("POSITION PROFILE", dictinaryList, candidateReportDetail);
                        mailAttachment.FileContent = attachementContent;
                        mailAttachment.FileName = candidateReportDetail.TestName.Replace(" ", "_")+".pdf";
                        mailDetail.Attachments.Add(mailAttachment);
                        mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                        isPositionProfileSent = true;
                    }
                }
                else
                {
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]) &&
                            !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]) &&
                            !Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
                    {
                        string pageName =  "CandidateTestResultTranscript.aspx";

                        Server.Execute("../PrintPages/" + pageName + "?candidatesessionid="
                            + Request.QueryString["candidatesessionid"] + "&attemptid="
                            + Request.QueryString["attemptid"] + "&testkey="
                            + Request.QueryString["testkey"], htw);
                        mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim() + sw.ToString().Trim();
                        mailDetail.isBodyHTML = true;
                    }
                }

                // Add 'To' mail addresses
                string[] toMailIDs = EmailAttachment_toTextBox.Text.Split(new char[] { ',' });
                for (int idx = 0; idx < toMailIDs.Length; idx++)
                    mailDetail.To.Add(toMailIDs[idx].Trim());

                if (!Utility.IsNullOrEmpty(EmailAttachment_ccTextBox.Text.Trim()))
                    mailDetail.CC.Add(EmailAttachment_ccTextBox.Text.Trim());

                mailDetail.From = EmailAttachment_fromHiddenField.Value.Trim();
                mailDetail.Subject = EmailAttachment_subjectTextBox.Text.Trim();

                // mailDetail.Message = EmailAttachment_messageTextBox.Text.Trim();
                bool isMailSent = false;

                try
                {
                    // Send mail
                    new EmailHandler().SendMail(mailDetail);
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }

                if (isMailSent && isCertificateSent)
                    base.ShowMessage(EmailAttachment_successMessageLabel,
                        Resources.HCMResource.EmailAttachment_CertificateSentSuccessfully);
                else if (isMailSent && isPositionProfileSent)
                    base.ShowMessage(EmailAttachment_successMessageLabel, "Position profile sent successfully");
                else if (isMailSent)
                    base.ShowMessage(EmailAttachment_successMessageLabel,
                        Resources.HCMResource.EmailAttachment_TestResultsSentSuccessfully);
                else
                    base.ShowMessage(EmailAttachment_errorMessageLabel,
                        Resources.HCMResource.EmailAttachment_MailCouldNotSend);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(EmailAttachment_errorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Protected Methods                                              

        /// <summary>
        /// This override method helps to validate input fields
        /// </summary>
        /// <returns>Returns FALSE if the condition meets TRUE</returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;
            if (EmailAttachment_toTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(EmailAttachment_errorMessageLabel,
                    Resources.HCMResource.EmailAttachment_ToAddressCannotBeEmpty);
                isValidData = false;
            }
            else
            {
                string[] toMailIDs = EmailAttachment_toTextBox.Text.Split(new char[] { ',' });

                for (int idx = 0; idx < toMailIDs.Length; idx++)
                {
                    if (!Regex.IsMatch(
                        toMailIDs[idx].Trim(), @"^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$"))
                    {
                        base.ShowMessage(EmailAttachment_errorMessageLabel,
                            Resources.HCMResource.EmailAttachment_InvalidEmailAddress);
                        isValidData = false;
                        break;
                    }
                }
            }
            // Validate reason feild
            if (EmailAttachment_messageTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(EmailAttachment_errorMessageLabel,
                    Resources.HCMResource.EmailAttachment_MessageCannotBeEmpty);
                isValidData = false;
            }
            return isValidData;
        }

        /// <summary>
        /// This protected method is used to load the default values when the page is loaded.
        /// </summary>
        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }

        #endregion Protected Methods                                           

        #region Private Methods                                                

        /// <summary>
        /// Method that will construct the PDF document.
        /// </summary>
        private void ConstructPDF()
        {
            string candSessionID = string.Empty;
            string attemptID = string.Empty;
            string testKey = string.Empty;
            string fileName = string.Empty;
            string histogramSessionKey = string.Empty;
            string appPath = HttpContext.Current.Request.ApplicationPath;
            string physicalPath = HttpContext.Current.Request.MapPath(appPath);
            DateTime testCompletedDate = DateTime.Now;

            if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]) &&
              !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]) &&
              !Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
            {
                candSessionID = Request.QueryString["candidatesessionid"].ToString();
                testKey = Request.QueryString["testkey"].ToString();
                attemptID = Request.QueryString["attemptid"].ToString();
            }

            string testName = string.Empty;
            testName = new ReportBLManager().GetTestName(testKey);

            //Get testCompletedDate
            testCompletedDate = new ReportBLManager().GetTestCompletedDateByCandidate(candSessionID, Convert.ToInt32(attemptID));


            string candidateName = string.Empty;
            candidateName = new CandidateBLManager().GetCandidateName(candSessionID);

            fileName = candSessionID + "_" + attemptID + ".pdf";

            //Response.ContentType = "application/pdf";
            //if (fileName != string.Empty)
            //    Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            //else
            //    Response.AddHeader("content-disposition", "attachment;filename=TestResult.pdf");

            //Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // Set Styles

            // Set Styles
            iTextSharp.text.Font fontCaptionStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, headerColor);
            iTextSharp.text.Font fontHeaderStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, headerColor);
            iTextSharp.text.Font fontItemStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, itemColor);

            //Style for Candidate and Test name
            iTextSharp.text.Font fontItemCandidateNameStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 14, iTextSharp.text.Font.NORMAL, itemColor);

            CandidateStatisticsDetail candidateStatisticsDetail =
            Session["CANDIDATE_STATISTICS"] as CandidateStatisticsDetail;


            //Set Document Size as A4.
            Document doc = new Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 10f);

            //Set document path 
            //PdfWriter.GetInstance(doc, new FileStream(Server.MapPath(@"../Chart/Test.pdf"), FileMode.Create));

            #region Set BackgroundImage
            iTextSharp.text.Image setBackGroundImage = iTextSharp.text.Image.GetInstance(bgImagePage1);
            setBackGroundImage.ScaleToFit(3000, 820);
            setBackGroundImage.Alignment = iTextSharp.text.Image.UNDERLYING;
            //setBackGroundImage.SetAbsolutePosition(80f, 60f);
            //setBackGroundImage.SpacingBefore = 60f;

            #endregion Set BackgroundImage

            #region Candidate Information
            //Construct Table Structures
            PdfPTable tableCandidateInfo = new PdfPTable(1);

            tableCandidateInfo.TotalWidth = 210f;
            //table.LockedWidth = true;            
            tableCandidateInfo.SpacingBefore = 200f;
            tableCandidateInfo.SpacingAfter = 20f;

            //Print Candidate Name
            PdfPCell cellCandidateName = new PdfPCell(new Phrase(candidateName,
                iTextSharp.text.FontFactory.GetFont("Arial", 24, iTextSharp.text.Font.NORMAL, headerTitle1)));
            cellCandidateName.BorderWidth = 0;
            //cellCandidateName.PaddingLeft=150;
            cellCandidateName.PaddingTop = 120;
            cellCandidateName.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfo.AddCell(cellCandidateName);


            //Print Test Name
            PdfPCell cellTestName = new PdfPCell(new Phrase(testName,
                iTextSharp.text.FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.NORMAL, headerTitle1)));
            cellTestName.BorderWidth = 0;
            cellTestName.PaddingTop = 57;
            cellTestName.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfo.AddCell(cellTestName);


            //Print Completed Date
            string formatCompletedDate = string.Empty;
            formatCompletedDate = string.Concat("on ", testCompletedDate.ToString("MMMM dd, yyyy"), " with a");
            PdfPCell cellCompletedDate = new PdfPCell(new Phrase(formatCompletedDate,
                iTextSharp.text.FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.ITALIC, headerTitle2)));
            cellCompletedDate.BorderWidth = 0;
            cellCompletedDate.PaddingTop = 23;
            cellCompletedDate.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfo.AddCell(cellCompletedDate);

            //Print Score of
            string scoreArea = string.Empty;
            if (!Utility.IsNullOrEmpty(candidateStatisticsDetail.MyScore.ToString()))
            {
                int testScore = 0;
                string scoreInWords = string.Empty;
                testScore = (int)Math.Ceiling(Convert.ToDouble(candidateStatisticsDetail.MyScore.ToString()));
                scoreInWords = Utility.GetNumberInLetters(testScore);

                scoreArea = string.Concat("score of ", testScore, "%", " [", scoreInWords, " percent", "]");
            }


            PdfPCell cellScoreOfValue = new PdfPCell(new Phrase(scoreArea,
               iTextSharp.text.FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL, headerTitle1)));
            cellScoreOfValue.PaddingTop = 15;
            cellScoreOfValue.HorizontalAlignment = Element.ALIGN_CENTER;
            cellScoreOfValue.BorderWidth = 0;
            tableCandidateInfo.AddCell(cellScoreOfValue);

          
            #endregion Candidate Information


            #region Set Query String Values
            string testID = string.Empty;
            if (!Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
            {
                testID = Request.QueryString["testkey"];
            }

            string sessionKey = string.Empty;
            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesessionid"].ToString();
            
            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();
            #endregion Set Query String Values

            #region Test Information

            #region Category and Subject
            PdfPTable tableCategoryStatistics = new PdfPTable(2);
            tableCategoryStatistics.SpacingAfter = 8f;
            tableCategoryStatistics.WidthPercentage = 100;

            //Category Statistics
            System.Drawing.Image orgChartCategory = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_CATEGORY + "-" + testID + ".png");
            iTextSharp.text.Image imageCategory = iTextSharp.text.Image.GetInstance(GetResizedImage(orgChartCategory, new System.Drawing.Size(1024, 968)),
                 System.Drawing.Imaging.ImageFormat.Png);


            imageCategory.ScaleToFit(250f, 650f);
            imageCategory.Border = Rectangle.BOX;
            imageCategory.Alignment = iTextSharp.text.Image.ALIGN_CENTER;


            PdfPCell cellCategoryImage = new PdfPCell(imageCategory);
            cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellCategoryImage.PaddingTop = 90;
            cellCategoryImage.BorderWidth = 0;
            cellCategoryImage.PaddingLeft = 50;
            tableCategoryStatistics.AddCell(cellCategoryImage);


            //Subject Statistics
            //iTextSharp.text.Image imageSubject = iTextSharp.text.Image.GetInstance(
            //    chartImagePath + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png");

            System.Drawing.Image orgChartSubject = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png");
            iTextSharp.text.Image imageSubject = iTextSharp.text.Image.GetInstance(GetResizedImage(orgChartSubject, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);


            imageSubject.ScaleToFit(250f, 650f);
            imageSubject.Border = Rectangle.BOX;
            imageSubject.Alignment = iTextSharp.text.Image.ALIGN_CENTER;


            PdfPCell cellSubjectImage = new PdfPCell(imageSubject);
            cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellSubjectImage.PaddingTop = 90;
            cellSubjectImage.BorderWidth = 0;
            cellSubjectImage.PaddingLeft = -40;
            tableCategoryStatistics.AddCell(cellSubjectImage);

            #endregion category and subject

            #region Test Area and Complexity Statistics

            PdfPTable tableTestAreaComplexStatistics = new PdfPTable(2);
            tableTestAreaComplexStatistics.SpacingAfter = 8f;
            tableTestAreaComplexStatistics.WidthPercentage = 100;

            //iTextSharp.text.Image imageArea = iTextSharp.text.Image.GetInstance(
            //    chartImagePath + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png");

            System.Drawing.Image orgChartTestArea = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png");
            iTextSharp.text.Image imageArea = iTextSharp.text.Image.GetInstance(GetResizedImage(orgChartTestArea, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageArea.ScaleToFit(250f, 650);
            imageArea.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_CENTER;

            PdfPCell cellAreaImage = new PdfPCell(imageArea);
            cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellAreaImage.PaddingTop = 110;
            cellAreaImage.BorderWidth = 0;
            cellAreaImage.PaddingLeft = 50;
            tableTestAreaComplexStatistics.AddCell(cellAreaImage);

            //Complexity Statistics
            //iTextSharp.text.Image imageComplexity = iTextSharp.text.Image.GetInstance(
            //    chartImagePath + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png");

            System.Drawing.Image orgChartComplexity = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png");
            iTextSharp.text.Image imageComplexity = iTextSharp.text.Image.GetInstance(GetResizedImage(orgChartComplexity, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageComplexity.ScaleToFit(250f, 650f);
            imageComplexity.Border = Rectangle.BOX;
            imageComplexity.Alignment = iTextSharp.text.Image.ALIGN_CENTER;

            PdfPCell cellComplexityImage = new PdfPCell(imageComplexity);
            cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellComplexityImage.PaddingTop = 110;
            cellComplexityImage.BorderWidth = 0;
            cellComplexityImage.PaddingLeft = -40;
            tableTestAreaComplexStatistics.AddCell(cellComplexityImage);

            #endregion Test Area and Complexity Statistics


            #endregion Test Information

            iTextSharp.text.Image imgPage2 = iTextSharp.text.Image.GetInstance(bgImagePage2);
            imgPage2.ScaleToFit(3000, 820);
            imgPage2.Alignment = iTextSharp.text.Image.UNDERLYING;
            //bg3.SetAbsolutePosition(25, 69);


            #region Score Analysis Charts

            PdfPTable tableScoreAnalysisCategorySubject = new PdfPTable(2);
            tableScoreAnalysisCategorySubject.SpacingAfter = 8f;
            tableScoreAnalysisCategorySubject.WidthPercentage = 100;
            tableScoreAnalysisCategorySubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right

            #region Category and Subject

            //iTextSharp.text.Image imageScoreCategory = iTextSharp.text.Image.GetInstance(chartImagePath +
            //    Constants.ChartConstants.CHART_CATEGORY +
            //    "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png");

            System.Drawing.Image orgScoreCategory = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_CATEGORY + "-"
                + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            iTextSharp.text.Image imageScoreCategory = iTextSharp.text.Image.GetInstance(GetResizedImage(orgScoreCategory, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageScoreCategory.ScaleToFit(250f, 600f);
            imageScoreCategory.Border = Rectangle.BOX;
            imageScoreCategory.BorderWidth = 0;
            imageScoreCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellScoreCategoryImage = new PdfPCell(imageScoreCategory);
            cellScoreCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            cellScoreCategoryImage.PaddingTop = 60;
            cellScoreCategoryImage.BorderWidth = 0;
            cellScoreCategoryImage.PaddingLeft = 50;
            tableScoreAnalysisCategorySubject.AddCell(cellScoreCategoryImage);


            //Subject Score
            //iTextSharp.text.Image imageScoreSubject = iTextSharp.text.Image.GetInstance(chartImagePath +
            //    Constants.ChartConstants.CHART_SUBJECT + "-" +
            //    testKey + "-" + sessionKey + "-" + attemptID + ".png");

            System.Drawing.Image orgScoreSubject = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_SUBJECT + "-"
                + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            iTextSharp.text.Image imageScoreSubject = iTextSharp.text.Image.GetInstance(GetResizedImage(orgScoreSubject, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);


            imageScoreSubject.ScaleToFit(250f, 600f);
            imageScoreSubject.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellScoreSubjectImage = new PdfPCell(imageScoreSubject);
            cellScoreSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellScoreSubjectImage.PaddingTop = 60;
            cellScoreSubjectImage.BorderWidth = 0;
            cellScoreSubjectImage.PaddingLeft = -40;
            tableScoreAnalysisCategorySubject.AddCell(cellScoreSubjectImage);

            #endregion Category and Subject

            #region Test Area and Complexity
            PdfPTable tableScoreAnalysisTestAreaComplex = new PdfPTable(2);
            tableScoreAnalysisTestAreaComplex.SpacingAfter = 8f;
            tableScoreAnalysisTestAreaComplex.WidthPercentage = 100;
            tableScoreAnalysisTestAreaComplex.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right

            //Test Areas Score
            //iTextSharp.text.Image imageScoreArea = iTextSharp.text.Image.GetInstance(chartImagePath +
            //    Constants.ChartConstants.CHART_TESTAREA + "-" +
            //    testKey + "-" + sessionKey + "-" + attemptID + ".png");

            System.Drawing.Image orgScoreArea = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_TESTAREA + "-"
                + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            iTextSharp.text.Image imageScoreArea = iTextSharp.text.Image.GetInstance(GetResizedImage(orgScoreArea, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageScoreArea.ScaleToFit(250f, 600f);
            imageScoreArea.Border = Rectangle.BOX;
            imageScoreArea.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellScoreAreaImage = new PdfPCell(imageScoreArea);
            cellScoreAreaImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellScoreAreaImage.PaddingTop = 95;
            cellScoreAreaImage.PaddingLeft = 50;
            cellScoreAreaImage.BorderWidth = 0;
            tableScoreAnalysisTestAreaComplex.AddCell(cellScoreAreaImage);

            System.Drawing.Image orgScoreComplexity = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_COMPLEXITY + "-"
                + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            iTextSharp.text.Image imageScoreComplexity = iTextSharp.text.Image.GetInstance(GetResizedImage(orgScoreComplexity, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageScoreComplexity.ScaleToFit(250f, 600f);
            imageScoreComplexity.Border = Rectangle.BOX;
            imageScoreComplexity.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellScoreComplexityImage = new PdfPCell(imageScoreComplexity);
            cellScoreComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellScoreComplexityImage.PaddingTop = 95;
            cellScoreComplexityImage.PaddingLeft = -40;
            cellScoreComplexityImage.BorderWidth = 0;
            tableScoreAnalysisTestAreaComplex.AddCell(cellScoreComplexityImage);


            #endregion Test Area and Complexity

            #endregion Score Information


            #region Canddidate information for page 2

            //Construct Table Structures
            PdfPTable tableCandidateInfoPage2 = new PdfPTable(1);

            tableCandidateInfoPage2.TotalWidth = 210f;
            tableCandidateInfoPage2.SpacingBefore = 200f;
            tableCandidateInfoPage2.SpacingAfter = 20f;

            //Print Candidate Name
            PdfPCell cellCandidateNamePage2 = new PdfPCell(new Phrase(candidateName,
                iTextSharp.text.FontFactory.GetFont("Arial", 24, iTextSharp.text.Font.NORMAL, headerTitle1)));

            cellCandidateNamePage2.BorderWidth = 0;
            cellCandidateNamePage2.PaddingTop = 74;
            cellCandidateNamePage2.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfoPage2.AddCell(cellCandidateNamePage2);


            //Print Test Name
            PdfPCell cellTestNamePage2 = new PdfPCell(new Phrase(testName,
                iTextSharp.text.FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.NORMAL, headerTitle1)));
            cellTestNamePage2.BorderWidth = 0;
            cellTestNamePage2.PaddingTop = 22;
            cellTestNamePage2.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfoPage2.AddCell(cellTestNamePage2);


            #endregion
            


            //Response Output
            //PdfWriter.GetInstance(doc, Response.OutputStream);

            //Response Output
            MemoryStream memoryStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);


            //Response Output
            doc.Open();
            doc.NewPage();
            doc.Add(setBackGroundImage);
            doc.Add(tableCandidateInfo);
            doc.Add(tableCategoryStatistics);
            doc.Add(tableTestAreaComplexStatistics);

            doc.NewPage();
            doc.Add(imgPage2);
            doc.Add(tableCandidateInfoPage2);
            doc.Add(tableScoreAnalysisCategorySubject);
            doc.Add(tableScoreAnalysisTestAreaComplex);

            writer.CloseStream = false;
            doc.Close();
            memoryStream.Position = 0;

            // Convert stream to byte array.
            BinaryReader reader = new BinaryReader(memoryStream);
            byte[] dataAsByteArray = new byte[memoryStream.Length];
            dataAsByteArray = reader.ReadBytes(Convert.ToInt32(memoryStream.Length));

            // Store the byte array in Session
            Session[Constants.SessionConstants.CANDIDATE_RESULTS_PDF] = dataAsByteArray;
        }

        /// <summary>
        /// Method that will retrieve the certificate image as bytes from the DB
        /// against the candidate session id and attempt id.
        /// </summary>
        private void ConstructCertificate()
        {
            if (!Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]) &&
                !Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
                Session[Constants.SessionConstants.CERTIFICATE_IMAGE] = new AdminBLManager().GetCertificateImage
                    (Request.QueryString["candidatesessionid"].Trim(),
                    Convert.ToInt32(Request.QueryString["attemptid"]));
        }

        /// <summary>
        /// Method that will check the tempate session is null or not.
        /// </summary>
        private void ConstructCertificateTemplate()
        {
            if (Session[Constants.SessionConstants.CERTIFICATE_TEMPLATE_IMAGE] == null)
                return;
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            EmailAttachment_successMessageLabel.Text = string.Empty;
            EmailAttachment_errorMessageLabel.Text = string.Empty;

            EmailAttachment_successMessageLabel.Visible = false;
            EmailAttachment_errorMessageLabel.Visible = false;
        }
        #endregion Private Methods                                             

        #region Public Methods
        public System.Drawing.Image GetResizedImage(System.Drawing.Image image, System.Drawing.Size size)
        {
            int newWidth;
            int newHeight;

            int originalWidth = image.Width;
            int originalHeight = image.Height;
            float percentWidth = (float)size.Width / (float)originalWidth;
            float percentHeight = (float)size.Height / (float)originalHeight;
            float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
            newWidth = (int)(originalWidth * percent);
            newHeight = (int)(originalHeight * percent);

            System.Drawing.Image newImage = new System.Drawing.Bitmap(newWidth, newHeight);
            using (System.Drawing.Graphics graphicsHandle = System.Drawing.Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }
        #endregion Public Methods
    }
}