﻿#region Header                                                                 

// Copyright (C) ForteHCM. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// SendEmail.cs
// File that represents the user interface layout and functionalities for
// the SendEmail page. This helps in sending emails to the scheduler,
// recruiter, etc.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the SendEmail page. This helps in sending emails to the scheduler,
    /// recruiter, etc. This class inherits Forte.HCM.UI.Common.PageBase.
    /// </summary>
    public partial class SendEmail : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                Response.Cache.SetCacheability(HttpCacheability.NoCache);

                // Set title and page caption.
                if (!Utility.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    string type = Request.QueryString["type"].ToString().Trim().ToUpper();
                    if (type == "SCH")
                    {
                        Master.SetPageCaption("EMail Scheduler");
                        SendEmail_headerLiteral.Text = "EMail Scheduler";
                    }
                    else if (type == "REC")
                    {
                        Master.SetPageCaption("EMail Recruiter");
                        SendEmail_headerLiteral.Text = "EMail Recruiter";
                    }
                }

                // Set default button.
                Page.Form.DefaultButton = SendEmail_topSendButton.UniqueID;

                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = SendEmail_messageTextBox.UniqueID;
                    SendEmail_messageTextBox.Focus();

                    // Load values.
                    LoadValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SendEmail_topErrorMessageLabel, 
                    SendEmail_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the send button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void SendEmail_sendButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate the fields
                if (!IsValidData())
                    return;

                // Compose mail detail.
                MailDetail mailDetail = new MailDetail();
                
                // Construct from address
                mailDetail.From = SendEmail_fromEmailIDHiddenField.Value.Trim();

                // Construct 'to' addresses.
                mailDetail.To = new List<string>();
                mailDetail.To.Add(SendEmail_toEmailIDHiddenField.Value);

                // Construct 'cc' addresses.
                mailDetail.CC = new List<string>();

                // Add the logged in user in the CC list when 'send me a copy ' is on
                if (SendEmail_sendMeACopyCheckBox.Checked)
                {
                    mailDetail.CC.Add(base.userEmailID);
                }
               
                if (!Utility.IsNullOrEmpty(SendEmail_ccTextBox.Text.Trim()))
                {
                    string[] ccMailIDs = SendEmail_ccTextBox.Text.Split(new char[] { ',', ';' });

                    for (int idx = 0; idx < ccMailIDs.Length; idx++)
                        mailDetail.CC.Add(ccMailIDs[idx].Trim());
                }

                // Construct subject.
                mailDetail.Subject = SendEmail_subjectTextBox.Text.Trim();

                // Construct message.
                mailDetail.Message = SendEmail_messageTextBox.Text.Trim();

                bool isMailSent = false;

                try
                {
                    // Send mail
                    new EmailHandler().SendMail(mailDetail);
                    isMailSent = true;
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    isMailSent = false;
                }

                if (isMailSent)
                    base.ShowMessage(SendEmail_topSuccessMessageLabel, SendEmail_bottomSuccessMessageLabel,
                        Resources.HCMResource.SendEmail_MailSentSuccessfully);
                else
                    base.ShowMessage(SendEmail_topErrorMessageLabel, SendEmail_bottomErrorMessageLabel, 
                        Resources.HCMResource.SendEmail_MailCouldNotSend);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SendEmail_topErrorMessageLabel, 
                    SendEmail_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                              

        #region Protected Overridden Methods                                   

        /// <summary>
        /// This override method helps to validate input fields
        /// </summary>
        /// <returns>Returns FALSE if the condition meets TRUE</returns>
        protected override bool IsValidData()
        {
            bool isValidData = true;

            if (SendEmail_ccTextBox.Text.Trim().Length > 0)
            {
                string[] ccMailIDs = SendEmail_ccTextBox.Text.Split(new char[] { ',', ';' });

                for (int idx = 0; idx < ccMailIDs.Length; idx++)
                {
                    if (!Regex.IsMatch(ccMailIDs[idx].Trim(), @"^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$"))
                    {
                        base.ShowMessage(SendEmail_topErrorMessageLabel,
                            SendEmail_bottomErrorMessageLabel,
                            Resources.HCMResource.SendEmail_InvalidCCEmailAddress);
                        isValidData = false;
                        break;
                    }
                }
            }

            // Validate message field.
            if (SendEmail_messageTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(SendEmail_topErrorMessageLabel, SendEmail_bottomErrorMessageLabel,
                    Resources.HCMResource.SendEmail_MessageCannotBeEmpty);
                isValidData = false;
            }
            return isValidData;
        }

        /// <summary>
        /// This protected method is used to load the default values when the page is loaded.
        /// </summary>
        protected override void LoadValues()
        {
            if (Utility.IsNullOrEmpty(Request.QueryString["type"]))
            {
                base.ShowMessage(SendEmail_topErrorMessageLabel, 
                    SendEmail_bottomErrorMessageLabel, "Necessary information is not provided");
                return;
            }

            string type = Request.QueryString["type"].ToString().Trim().ToUpper();

            if (type == "SCH")
            {
                // Get candidate session id.
                string candidateSessionID = null;
                if (Utility.IsNullOrEmpty(Request.QueryString["candidatesessionid"]))
                {
                    base.ShowMessage(SendEmail_topErrorMessageLabel, 
                        SendEmail_bottomErrorMessageLabel, "Candidate session ID is not provided");
                    return;
                }
                candidateSessionID = Request.QueryString["candidatesessionid"].ToString().Trim();

                // Get attempt id.
                if (Utility.IsNullOrEmpty(Request.QueryString["attemptid"]))
                {
                    base.ShowMessage(SendEmail_topErrorMessageLabel,
                        SendEmail_bottomErrorMessageLabel, "Attempt ID is not provided");
                    return;
                }

                // Parse attempt ID.
                int attemptID = 0;
                if (int.TryParse(Request.QueryString["attemptid"].ToString().Trim(), out attemptID) == false)
                {
                    base.ShowMessage(SendEmail_topErrorMessageLabel,
                        SendEmail_bottomErrorMessageLabel, "Attempt ID is not valid");
                    return;
                }

                // Get activity type.
                string activityType = null;
                if (Utility.IsNullOrEmpty(Request.QueryString["activityType"]))
                {
                    base.ShowMessage(SendEmail_topErrorMessageLabel,
                        SendEmail_bottomErrorMessageLabel, "Activity type is not provided");
                    return;
                }
                activityType = Request.QueryString["activityType"].ToString().Trim().ToUpper();

                // Get candidate session detail.
                TestScheduleDetail schedulerDetail = null;

                if (activityType == "TST")
                {
                    schedulerDetail = new CandidateBLManager().
                        GetTestSchedulerEmailDetail(candidateSessionID, attemptID);
                }
                else if (activityType == "INT")
                {
                    schedulerDetail = new CandidateBLManager().
                        GetInterviewSchedulerEmailDetail(candidateSessionID, attemptID);
                }

                if (schedulerDetail == null)
                    return;

                // Assign details.
                //SendEmail_fromValueLabel.Text = schedulerDetail.CandidateName;
                //SendEmail_fromEmailIDHiddenField.Value = schedulerDetail.CandidateEmail;

                SendEmail_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                SendEmail_fromEmailIDHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];

                SendEmail_toValueLabel.Text = schedulerDetail.SchedulerName;
                SendEmail_toEmailIDHiddenField.Value = schedulerDetail.SchedulerEmail;
            }
            else if (type == "REC")
            {
                // Get attempt id.
                if (Utility.IsNullOrEmpty(Request.QueryString["candidateloginid"]))
                {
                    base.ShowMessage(SendEmail_topErrorMessageLabel, 
                        SendEmail_bottomErrorMessageLabel, "Candidate login ID is not provided");
                    return;
                }

                // Parse candidate login ID.
                int candidateLoginID = 0;
                if (int.TryParse(Request.QueryString["candidateloginid"].ToString().Trim(), out candidateLoginID) == false)
                {
                    base.ShowMessage(SendEmail_topErrorMessageLabel, 
                        SendEmail_bottomErrorMessageLabel, "Candidate login ID is not valid");
                    return;
                }

                // Get candidate session detail.
                RecruiterDetail recruiterDetail = new CandidateBLManager().
                    GetRecruiterEmailDetail(candidateLoginID);

                if (recruiterDetail == null)
                    return;

                // Assign details.
                //SendEmail_fromValueLabel.Text = recruiterDetail.CandidateName;
                //SendEmail_fromEmailIDHiddenField.Value = recruiterDetail.CandidateEmail;

                SendEmail_fromValueLabel.Text = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];
                SendEmail_fromEmailIDHiddenField.Value = ConfigurationManager.AppSettings["EMAIL_FROM_ADDRESS"];

                SendEmail_toValueLabel.Text = recruiterDetail.RecruiterName;
                SendEmail_toEmailIDHiddenField.Value = recruiterDetail.RecruiterEmail;
            }
        }

        #endregion Protected Overridden Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            SendEmail_topErrorMessageLabel.Text = string.Empty;
            SendEmail_topSuccessMessageLabel.Text = string.Empty;
            SendEmail_bottomErrorMessageLabel.Text = string.Empty;
            SendEmail_bottomSuccessMessageLabel.Text = string.Empty;

            SendEmail_topErrorMessageLabel.Visible = false;
            SendEmail_topSuccessMessageLabel.Visible = false;
            SendEmail_bottomErrorMessageLabel.Visible = false;
            SendEmail_bottomSuccessMessageLabel.Visible = false;
        }

        #endregion Private Methods
    }
}