<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreditRequest.aspx.cs" MasterPageFile="~/MasterPages/PopupMaster.Master"
    Inherits="Forte.HCM.UI.Popup.CreditRequest" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/MasterPages/PopupMaster.Master" %>
<asp:Content ID="CreditRequest_content" ContentPlaceHolderID="PopupMaster_contentPlaceHolder"
    runat="server">
    <script type="text/javascript" language="javascript">

        // This function helps to close the window
        function CloseMe()
        {
            self.close();
        }
        
    </script>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:UpdatePanel ID="CreditRequest_updatePanel" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="popup_td_padding_10">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="width: 50%" class="popup_header_text_grey" align="left">
                                                <asp:Literal ID="CreditRequest_headerLiteral" runat="server" Text="Request For Credits"></asp:Literal>
                                            </td>
                                            <td style="width: 50%" align="right">
                                                <asp:ImageButton ID="CreditRequest_topCancelImageButton" runat="server" SkinID="sknCloseImageButton"
                                                    OnClientClick="javascript:CloseMe()" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="popup_td_padding_10" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="popupcontrol_question_inner_bg">
                                        <tr>
                                            <td class="msg_align">
                                                <asp:Label ID="CreditRequest_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                <asp:Label ID="CreditRequest_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                <asp:HiddenField ID="CreditRequest_toHiddenField" runat="server" />
                                                <asp:HiddenField ID="CreditRequest_fromHiddenField" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="CreditRequest_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 55%">
                                                                        <asp:Label ID="CreditRequest_fromValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                                    </td>
                                                                     <td style="width: 30%" align="right">
                                                                        <asp:CheckBox ID="CreditRequest_sendMeACopyCheckBox" runat="server" Checked="true"
                                                                            TextAlign="Right" Text="Send me a copy" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="CreditRequest_toLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="CreditRequest_toValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="CreditRequest_ccLabel" runat="server" Text="CC" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td style="padding: 3px;">
                                                            <asp:TextBox ID="CreditRequest_ccTextBox" runat="server" Width="99%" TextMode="MultiLine"
                                                                SkinID="sknMultiLineTextBox" Height="23" MaxLength="100" onkeyup="CommentsCount(100,this)"
                                                                onchange="CommentsCount(100,this)" TabIndex="1"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="CreditRequest_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="CreditRequest_subjectValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_height_5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td valign="top" align="left">
                                                                        <asp:Label ID="CreditRequest_messageLabel" runat="server" Text="Message" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        <span class="mandatory">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="CreditRequest_messageTextBox" TextMode="MultiLine" Height="200"
                                                                            runat="server" Width="99%" SkinID="sknMultiLineTextBox" MaxLength="500" onkeyup="CommentsCount(500,this)"
                                                                            onchange="CommentsCount(500,this)" TabIndex="2">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_height_8">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="popup_td_padding_left_8">
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:Button ID="CreditRequest_bottomSubmitButton" runat="server" Text="Send" OnClick="CreditRequest_submitButton_Click"
                                                    SkinID="sknButtonId" TabIndex="3" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="CreditRequest_bottomCancelButton" runat="server" Text="Cancel"
                                                    SkinID="sknPopupLinkButton" TabIndex="4"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
