<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestReminder.aspx.cs" Inherits="Forte.HCM.UI.Popup.TestReminder"
    Title="Test Reminder" ValidateRequest="false" MasterPageFile="~/SiteMaster.Master" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="TestReminder_content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" language="javascript">
        
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div>
                    <div class="msg_align">
                        <asp:Label ID="TestReminder_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestReminder_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div id="InterviewIntroduction_mainDiv" class="test_right_panel">
                    <div class="test_body_bg_div">
                        <div class="test_header_text_bold_div">
                            <asp:Literal ID="TestReminder_headerLiteral" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="test_body_bg_div" style="height: 20px">
                    </div>
                    <div class="test_body_bg_div">
                        <div id="TestReminder_leftPanel" style="width: 590px; float: left;">
                            <div style="float: left; width: 580px" class="test_subheader_text_bold_div">
                                <asp:Label ID="TestReminder_testNameLabel" runat="server" Text="Test Name"></asp:Label>
                            </div>
                            <div style="float: left; width: 580px" class="test_text_div">
                                <asp:Label ID="TestReminder_testNameValueLabel" Text="" runat="server"></asp:Label>
                            </div>
                            <div style="float: left; width: 580px" class="test_subheader_text_bold_div">
                                <asp:Label ID="TestReminder_testDescriptionLabel" runat="server" Text="Test Description"></asp:Label>
                            </div>
                            <div style="float: left; width: 580px;word-wrap: break-word;white-space:normal;" class="test_text_div">
                                <asp:Label ID="TestReminder_testDescriptionValueLabel" Text="" runat="server"></asp:Label>
                            </div>
                            <div style="float: left; width: 580px" class="test_subheader_text_bold_div">
                                <asp:Label ID="TestReminder_expiryDateLabel" runat="server" Text="Expiry&nbsp;Date"></asp:Label>
                            </div>
                            <div style="float: left; width: 580px" class="test_text_div">
                                <asp:Label ID="TestReminder_expiryDateValueLabel" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: left; width: 580px" class="test_subheader_text_bold_div">
                                <asp:Label ID="TestReminder_scheduledByLabel" runat="server" Text="Scheduled By"></asp:Label>
                            </div>
                            <div style="float: left; width: 580px" class="test_text_div">
                                <asp:Label ID="TestReminder_scheduledByValueLabel" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div id="TestReminder_rightPanel" style="width: 260px; float: left;">
                             <div style="float: left; width: 250px" class="test_subheader_text_bold_div">
                                    <asp:Label ID="TestReminder_expectedDateLabel" runat="server" Text="Scheduled Date & Time"></asp:Label>
                             </div>
                             <div style="float: left; width: 250px" class="test_text_div">
                                <div style="width: 250px; float: left">
                                    <div style="width: 71px; float: left">
                                        <asp:TextBox ID="TestReminder_expectedDateTextBox" runat="server" MaxLength="10"
                                            AutoCompleteType="None" Text="" Width="66px" SkinID="sknDefaultTextBox"></asp:TextBox>
                                    </div>
                                    <div style="width: 71px; float: left">
                                        <asp:TextBox ID="TestReminder_expectedTimeTextBox" runat="server" MaxLength="10"
                                            AutoCompleteType="None" Text="" Width="66px" SkinID="sknDefaultTextBox"></asp:TextBox>
                                    </div>
                                    <div style="width: 18px; float: left; padding-top: 1px">
                                        <asp:ImageButton ID="TestReminder_calendarImageButton" SkinID="sknCalendarImageButton"
                                            runat="server" ImageAlign="Middle" />
                                    </div>
                                    <div style="width: 18px; float: left; padding-top: 1px">
                                        <asp:ImageButton ID="TestReminder_scheduleDateImageButton" SkinID="sknHelpImageButton"
                                            runat="server" ImageAlign="Middle" OnClientClick="javascript:return false;" />
                                    </div>
                                    <div>
                                        <ajaxToolKit:CalendarExtender ID="TestReminder_expectedDtCalendarExtender" runat="server"
                                            TargetControlID="TestReminder_expectedDateTextBox" CssClass="MyCalendar" PopupPosition="BottomLeft"
                                            PopupButtonID="TestReminder_calendarImageButton" />
                                        <ajaxToolKit:MaskedEditExtender ID="TestReminder_expectedDateMaskedEditExtender"
                                            runat="server" TargetControlID="TestReminder_expectedDateTextBox" Mask="99/99/9999"
                                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                            MaskType="Date" DisplayMoney="Left" AcceptNegative="Left" ErrorTooltipEnabled="True" />
                                        <ajaxToolKit:MaskedEditValidator ID="TestReminder_expectedDtMaskedEditValidator"
                                            runat="server" ControlExtender="TestReminder_expectedDateMaskedEditExtender"
                                            ControlToValidate="TestReminder_expectedDateTextBox" EmptyValueMessage="Date is required"
                                            InvalidValueMessage="Date is invalid" Display="None" TooltipMessage="Input a date"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                        <ajaxToolKit:MaskedEditExtender ID="TestReminder_expectedTimeMaskedEditExtender"
                                            runat="server" TargetControlID="TestReminder_expectedTimeTextBox" Mask="99:99:99"
                                            MessageValidatorTip="true" MaskType="Time" AcceptAMPM="false" AcceptNegative="None"
                                            InputDirection="LeftToRight" ErrorTooltipEnabled="True" />
                                        <ajaxToolKit:MaskedEditValidator ID="TestReminder_expectedTimetMaskedEditValidator"
                                            runat="server" ControlExtender="TestReminder_expectedTimeMaskedEditExtender"
                                            ControlToValidate="TestReminder_expectedTimeTextBox" EmptyValueMessage="Time is required"
                                            InvalidValueMessage="Time is invalid" Display="None" TooltipMessage="Input a date"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="MKE" />
                                    </div>
                                </div>
                            </div>

                            <div style="float: left; width: 180px" class="test_subheader_text_bold_div">
                                <asp:Label ID="TestReminder_reminderLabel" runat="server" Text="Reminder"></asp:Label>
                            </div>
                            <div class="checkbox_list_bg" style="width: 166px; float: left">
                                <asp:CheckBoxList runat="server" ID="TestReminder_timeFrameCheckBoxList" RepeatDirection="Vertical"
                                    Width="120px">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                        <div class="test_body_bg_div">
                            <div style="width: 410px; height: 40px; float: left">
                            </div>
                            <div style="width: 400px; text-align: center; float: left;">
                                <div style="float: left; width: 70px; text-align: left">
                                    <asp:Button ID="TestReminder_setButton" runat="server" Text="Save" OnClick="TestReminder_setButton_Click"
                                        Width="60px" SkinID="sknButtonId" />
                                </div>
                                <div style="float: left; width: 80px; text-align: left">
                                    <asp:LinkButton ID="TestReminder_cancelLinkButton" SkinID="sknActionLinkButton"
                                        runat="server" Text="Cancel" OnClick="ParentPageRedirect"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
                <div>
                    <div class="msg_align">
                        <asp:Label ID="TestReminder_bottomSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="TestReminder_bottomErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
