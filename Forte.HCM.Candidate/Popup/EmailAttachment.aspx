﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailAttachment.aspx.cs"
    Inherits="Forte.HCM.UI.Popup.EmailAttachment" MasterPageFile="~/SiteMaster.Master"
    Title="EMail Attachment" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="EmailAttachment_content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        // This function helps to Open save/open dialog
        function OpenPopUp(candidateSessionID, attemptID) {
            window.open('../Common/Download.aspx?candidatesessionid='
            + candidateSessionID + '&attemptid=' + attemptID
            + '&type=<%=EmailAttachment_downloadTypeHiddenField.Value %>', '', 'toolbar=0,resizable=0,width=1,height=1', '');
        }
    </script>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div class="popup_right_panel">
                    <asp:HiddenField ID="EmailAttachment_creditsEarnedHiddenField" runat="server" />
                    <asp:HiddenField ID="EmailAttachment_downloadTypeHiddenField" runat="server" />
                    <asp:UpdatePanel ID="EmailAttachment_updatePanel" runat="server">
                        <ContentTemplate>
                            <div class="popup_body_bg_div">
                                <div class="popup_header_text_bold_div" style="width: 590px">
                                    <asp:Literal ID="EmailAttachment_headerLiteral" runat="server" Text="EMail Attachment"></asp:Literal>
                                </div>
                                <div class="popup_controls_header_bg_right">
                                    <div class="popup_controls_header_bg_left">
                                        <asp:Button ID="EmailAttachment_topSendButton" runat="server" Text="Send" OnClick="EmailAttachment_submitButton_Click"
                                            SkinID="sknButtonId" TabIndex="5" />
                                    </div>
                                    <div class="popup_controls_header_bg_left">
                                        <asp:LinkButton ID="EmailAttachment_topCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknPopupLinkButton" TabIndex="7" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="empty_height">
                                &nbsp;
                            </div>
                            <div class="email_attachment_popup_tab_body_bg_div">
                                <div class="msg_align">
                                    <asp:Label ID="EmailAttachment_successMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                    <asp:Label ID="EmailAttachment_errorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                                    <asp:HiddenField ID="EmailAttachment_fromHiddenField" runat="server" />
                                </div>
                                <div>
                                    <div class="popup_cont_div">
                                        <div class="popup_cont_div_left">
                                            <asp:Label ID="EmailAttachment_fromLabel" runat="server" Text="From" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        </div>
                                        <div class="popup_cont_div_right">
                                            <div style="width: 450px; float: left">
                                                <asp:Label ID="EmailAttachment_fromValueLabel" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                            </div>
                                            <div style="width: 100px; float: left">
                                                <asp:CheckBox ID="EmailAttachement_sendMeACopyCheckBox" runat="server" Checked="true"
                                                    TextAlign="Right" Text="Send me a copy" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="EmailAttachment_toLabel" runat="server" Text="To" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <div class="popup_cont_div_right">
                                        <asp:TextBox ID="EmailAttachment_toTextBox" TabIndex="1" runat="server" MaxLength="100"
                                            onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)" SkinID="sknMultiLineTextBox"
                                            Width="543px">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="EmailAttachment_ccLabel" runat="server" Text="CC" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="popup_cont_div_right">
                                        <asp:TextBox ID="EmailAttachment_ccTextBox" runat="server" Width="543px" TextMode="MultiLine"
                                            SkinID="sknMultiLineTextBox" Height="23" MaxLength="100" onkeyup="CommentsCount(100,this)"
                                            onchange="CommentsCount(100,this)" TabIndex="2"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="EmailAttachment_subjectLabel" runat="server" Text="Subject" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="popup_cont_div_right">
                                        <asp:TextBox ID="EmailAttachment_subjectTextBox" TabIndex="3" runat="server" MaxLength="100"
                                            onkeyup="CommentsCount(100,this)" onchange="CommentsCount(100,this)" SkinID="sknMultiLineTextBox"
                                            Height="23" Width="543px" TextMode="MultiLine">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="popup_cont_div" id="EmailAttachment_attachmentTR" runat="server" style="display: none;">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="EmailAttachment_attachmentLabel" runat="server" Text="Attachment"
                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                    </div>
                                    <div class="popup_cont_div_right">
                                        <asp:ImageButton ID="EmailAttachment_attachmentImageButton" runat="server" ImageUrl="~/App_Themes/DefaultTheme/Images/icon_pdf.gif"
                                            AlternateText="Attachment" ToolTip="Attachment" />
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div class="popup_cont_div_left">
                                        <asp:Label ID="EmailAttachment_messageLabel" runat="server" Text="Message" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                        <span class="mandatory">*</span>
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div style="width: 625px; float: left; padding-bottom: 4px; padding-top: 4px;">
                                        <asp:TextBox ID="EmailAttachment_messageTextBox" TabIndex="4" TextMode="MultiLine"
                                            Height="200" runat="server" Width="620px" SkinID="sknMultiLineTextBox" MaxLength="500"
                                            onkeyup="CommentsCount(500,this)" onchange="CommentsCount(500,this)" Style="border-bottom: 0px;">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="popup_cont_div">
                                    <div style="width: 625px; float: left; padding-bottom: 4px; padding-top: 4px;">
                                        <iframe id="EmailAttachment_contentIframe" runat="server" width="620px" scrolling="auto"
                                            style="height: 140px; overflow: auto; display: none; color: #33424B; border: 1px solid #D8D8D8;
                                            background-color: #FFFFFF; padding-left: 3px; border-top: 0px;"></iframe>
                                        <div id="EmailAttachment_certificateDIV" runat="server" style="height: 180px; width: 716px;
                                            overflow: auto;">
                                            <img id="EmailAttachment_certificateImage" runat="server" alt="Certificate" />
                                        </div>
                                    </div>
                                </div>
                                <div class="empty_height">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="popup_body_bg_div">
                                <div class="popup_header_text_bold_div" style="width: 590px">
                                    &nbsp;
                                </div>
                                <div class="popup_controls_header_bg_right">
                                    <div class="popup_controls_header_bg_left">
                                        <asp:Button ID="EmailAttachment_submitButton" runat="server" Text="Send" OnClick="EmailAttachment_submitButton_Click"
                                            SkinID="sknButtonId" TabIndex="5" />
                                    </div>
                                    <div class="popup_controls_header_bg_left">
                                        <asp:LinkButton ID="EmailAttachment_bottomCancelLinkButton" runat="server" Text="Cancel"
                                            SkinID="sknPopupLinkButton" TabIndex="6" OnClick="ParentPageRedirect"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
