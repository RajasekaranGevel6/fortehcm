﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CreditRequest.aspx.cs
// File that represents the CreditRequest class that defines the user interface
// layout and functionalities for the CreditRequest page. This page helps in 
// sending credit requests. This class inherits Forte.HCM.UI.Common.PageBase
// class.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;


#endregion Directives                                                          

namespace Forte.HCM.UI.Popup
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities
    /// for the CreditRequest page. This page helps in sending credit requests.
    /// This class inherits the Forte.HCM.UI.Common.PageBase class.
    /// </summary>
    public partial class CreditRequest : PageBase
    {
        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Set browser title
                Master.SetPageCaption("Credit Request");
                // Set default button field
                Page.Form.DefaultButton = CreditRequest_bottomSubmitButton.UniqueID;
                CreditRequest_bottomCancelButton.Attributes.Add("onclick", "Javascript:return CloseMe();");
                // Call validate email javascript function
                CreditRequest_bottomSubmitButton.Attributes.Add("onclick", "javascript:return IsValidEmail('"
                    + CreditRequest_ccTextBox.ClientID + "','"
                    + CreditRequest_topErrorMessageLabel.ClientID + "','"
                    + CreditRequest_topSuccessMessageLabel.ClientID + "');");
                if (!IsPostBack)
                {
                    // Set default focus field
                    Page.Form.DefaultFocus = CreditRequest_messageTextBox.UniqueID;
                    CreditRequest_messageTextBox.Focus();

                    UserDetail userDetail = new CommonBLManager().GetUserDetail(base.userID);

                    if (userDetail != null)
                    {
                        CreditRequest_fromValueLabel.Text = userDetail.FirstName
                            + " &lt;" + userDetail.Email + "&gt;";

                        CreditRequest_toValueLabel.Text = "Admin &lt;" +
                            ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"].ToString() + "&gt;";

                        CreditRequest_toHiddenField.Value = ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"].ToString();
                        CreditRequest_fromHiddenField.Value = userDetail.Email.Trim();
                    }

                    // Set subject.
                    CreditRequest_subjectValueLabel.Text = "Request for credits";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreditRequest_topErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the send button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will send the credit request.
        /// </remarks>
        protected void CreditRequest_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear all the error and success labels
                CreditRequest_topErrorMessageLabel.Text = string.Empty;
                CreditRequest_topSuccessMessageLabel.Text = string.Empty;

                // Validate the fields
                if (!IsValidData())
                    return;

                // Update credit request.
                new CandidateBLManager().InsertCreditRequest(base.userID);

                // Show successful message
                base.ShowMessage(CreditRequest_topSuccessMessageLabel,
                    "Request sent successfully");

                bool mailSent = true;

                try
                {
                    // Add the logged in user in the CC list when 'send me a copy ' is on
                    if (CreditRequest_sendMeACopyCheckBox.Checked)
                    {
                        if (CreditRequest_ccTextBox.Text.Trim().Length == 0)
                            CreditRequest_ccTextBox.Text = base.userEmailID;
                        else
                            CreditRequest_ccTextBox.Text += "," + base.userEmailID;
                    }

                    // Send email.
                    new EmailHandler().SendMail(EntityType.CreditRequest, base.userID.ToString(), 
                        CreditRequest_ccTextBox.Text.Trim(), CreditRequest_messageTextBox.Text.Trim());
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                    mailSent = false;
                }

                if (mailSent == false)
                {
                    base.ShowMessage(CreditRequest_topErrorMessageLabel,
                        "<br>Mail cannot be sent to your recruiter/caretaker");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(CreditRequest_topErrorMessageLabel, exp.Message);
            }
        }

        #endregion Event Handlers                                               

        #region Protected Methods                                              

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            // Validate reason feild
            if (CreditRequest_messageTextBox.Text.Trim().Length == 0)
            {
                base.ShowMessage(CreditRequest_topErrorMessageLabel,
                    "Message cannot be empty");
                return false;
            }
            
            // Check if credit request already exist.
            if (new CandidateBLManager().IsCreditRequestExist(base.userID))
            {
                base.ShowMessage(CreditRequest_topErrorMessageLabel,
                    "Already a credit request was pending. Cannot make another request");
                
                return false;
            }
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            
        }

        #endregion Protected Methods                                           
    }
}