﻿using System;
using System.Web.UI;

namespace Forte.HCM.UI.MasterPages
{
    public partial class HCMCandidateMaster : MasterPage
    {
        protected string pageCaption;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void SetPageCaption(string pageTitle)
        {
            if (pageTitle != null)
            {

                pageCaption = pageTitle.Trim();
            }
        }

        public bool ShowMenu
        {
            set
            {
                HCMCandidateMaster_menuControl.Visible = value;
            }
        }
    }
}
