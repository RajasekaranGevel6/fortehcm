﻿using System;
using System.Web.UI;
using System.Collections.Generic;

namespace Forte.HCM.UI.MasterPages
{
    public partial class HomeMaster : MasterPage
    {
        List<string> breadCrumbText;
        protected string pageCaption;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HomeMaster_headerControl.BreadCrumbText = breadCrumbText;
            }
        }
        public void ForgotPasswordModalpPopupExtender()
        {
         
        }

        public void SetPageCaption(string pageTitle)
        {
            if (pageTitle != null)
            {
                pageCaption = pageTitle.Trim();
            }
        }

        public List<string> BreadCrumbText
        {
            set
            {
                breadCrumbText = value;
            }
        }
    }
}