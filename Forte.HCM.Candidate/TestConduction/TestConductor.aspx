﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestConductor.aspx.cs"
    Inherits="Forte.HCM.UI.TestConduction.TestConductor" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Assembly="EO.Web" Namespace="EO.Web" TagPrefix="eo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Taking Test</title>
    <base target="_self" />
    <script src="../JS/TestConductorJS.js" type="text/javascript"></script>
</head>
<body style="background-color: #000000;">
    <form id="TestConductor_form" runat="server">
    <ajaxToolKit:ToolkitScriptManager ID="TestConductor_scriptManager" runat="server">
    </ajaxToolKit:ToolkitScriptManager>
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <table border="0" width="980px" align="center" cellpadding="0" cellspacing="0" style="height: 100%">
                    <tr>
                        <td>
                            <table width="400px" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td class="test_con_body_bg">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="test_con_header_bg">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="test_con_title">
                                                            </td>
                                                            <td align="right">
                                                                <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="TestConductor_timeRemainsHeadLabel" runat="server" Text="Time Remaining : "
                                                                                SkinID="sknLabelTestConHead"></asp:Label><asp:Label ID="TestConductor_timeRemainsLabel"
                                                                                    runat="server" Text="00:29:52" SkinID="sknLabelTestConHead"></asp:Label>
                                                                        </td>
                                                                        <td class="test_con_title_text" style="width: 5%" align="center">
                                                                            |
                                                                        </td>
                                                                        <td align="right" style="width: 11%">
                                                                            <asp:LinkButton ID="TestConductor_quitTestLinkButton" runat="server" Text="Quit Test"
                                                                                SkinID="sknQuitTestLinkButton" OnClick="TestConductor_quitTestLinkButton_Click"></asp:LinkButton>
                                                                        </td>
                                                                        <td class="test_con_title_text" style="width: 5%" align="center">
                                                                            |
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="TestConductor_helpImageButton" runat="server" SkinID="sknTestConHelpImageButton" />
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="msg_align">
                                                    <asp:UpdatePanel runat="server" ID="TestConductor_topSuccessUpdatePanel">
                                                        <ContentTemplate>
                                                            <asp:Label ID="TestConductor_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                                                                Height="17px"></asp:Label>
                                                            <asp:Label ID="TestConductor_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                                                                Height="17px"></asp:Label>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="TestConductor_mainHeadLabel" runat="server" SkinID="sknLabelTestConMainHead"
                                                                    Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="middle">
                                                                <table width="750px" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="width: 20px">
                                                                        </td>
                                                                        <td style="width: 460px">
                                                                            <table width="90%" cellpadding="0" cellspacing="5" border="0" align="center">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="TestConductor_progressHeadLabel" runat="server" SkinID="sknLabelProgressHead"
                                                                                            Text="Progress:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <eo:progressbar id="TestConductor_progressBar" runat="server" width="150px" value="50"
                                                                                            showpercentage="false" indicatorimage="~/App_Themes/DefaultTheme/Images/test_con_progressbar.gif"
                                                                                            backgroundimageright="~/App_Themes/DefaultTheme/Images/test_con_progressbar_right.gif"
                                                                                            controlskinid="None" forecolor="Red" backgroundimage="~/App_Themes/DefaultTheme/Images/test_con_progressbar_bg.gif"
                                                                                            indicatorincrement="7" backgroundimageleft="~/App_Themes/DefaultTheme/Images/test_con_progressbar_left.gif">
                                                                                        </eo:progressbar>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="TestConductor_progressQuestLabel" runat="server" SkinID="sknLabelProgressText"
                                                                                            Text="Question "></asp:Label>
                                                                                        <asp:Label ID="TestConductor_currentQuestCountLabel" runat="server" SkinID="sknLabelProgressText"
                                                                                            Text=""></asp:Label><asp:Label ID="TestConductor_ofLabel" runat="server" SkinID="sknLabelProgressText"
                                                                                                Text=" of "></asp:Label><asp:Label ID="TestConductor_totalQuestCountLabel" runat="server"
                                                                                                    SkinID="sknLabelProgressText" Text=""></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3">
                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td class="test_con_question_icon">
                                                                                                </td>
                                                                                                <td>
                                                                                                    <div style="overflow: auto;" id="TestConductor_questionDiv" runat="server">
                                                                                                        <asp:Label ID="TestConductor_questionLabel" runat="server" SkinID="sknLabelTestCondQuest"
                                                                                                            Text=""></asp:Label>
                                                                                                        <br />
                                                                                                        <asp:Image runat="server" ID="TestConductor_questionImage" />
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <table width="100%" cellpadding="3" cellspacing="3" border="0">
                                                                                                        <tr>
                                                                                                            <td class="test_con_label_text">
                                                                                                                <asp:RadioButtonList ID="TestConductor_answerRadioButtonList" runat="server" CellSpacing="5"
                                                                                                                    CssClass="check_box_text" RepeatColumns="1" RepeatDirection="Horizontal" TextAlign="Right"
                                                                                                                    Width="100%">
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="test_con_line">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:Button ID="TestConductor_submitButton" runat="server" Text="Submit Answer" SkinID="sknButtonTestCond"
                                                                                                                                OnClick="TestConductor_submitButton_Click" />
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <asp:LinkButton ID="TestConductor_skipLinkButton" runat="server" Text="Skip" SkinID="sknActionLinkButton"
                                                                                                                                OnClick="TestConductor_skipLinkButton_Click"></asp:LinkButton>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td style="width: 180px" align="right" runat="server" id="TestConductor_rightSidePanel">
                                                                            <div style="float: right;" runat="server" id="TestConductor_dockShowDiv">
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td class="test_con_infobg" valign="middle">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td style="width: 32%">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="test_con_label_text">
                                                                                                        Time elapsed for question
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="test_con_textbox_green">
                                                                                                        <span id="TestConductor_qstTimeEllapsedSpan" runat="server"></span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="test_con_label_text">
                                                                                                        Time elapsed for test
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="test_con_textbox_orange">
                                                                                                        <span id="TestConductor_testTimeEllapsedSpan" runat="server"></span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="test_con_label_text">
                                                                                                        Time remaining
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                    <td class="test_con_textbox_blue">
                                                                                                        <span id="TestConductor_testTimeRemainSpan" runat="server"></span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td style="width: 30%">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="test_con_label_text">
                                                                                                                    Questions answered
                                                                                                                </td>
                                                                                                                <td style="width: 2%">
                                                                                                                </td>
                                                                                                                <td class="test_con_questions_answered">
                                                                                                                    <asp:Label runat="server" ID="TestConductor_answeredQtsLabel" Text="0"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td colspan="3" class="test_con_dotline">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 30%">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="test_con_label_text">
                                                                                                                    Questions skipped
                                                                                                                </td>
                                                                                                                <td style="width: 2%">
                                                                                                                </td>
                                                                                                                <td class="test_con_questions_skipped">
                                                                                                                    <asp:Label runat="server" ID="TestConductor_skippedQtsLabel" Text="0"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td colspan="3" class="test_con_dotline">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 30%">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="test_con_label_text">
                                                                                                                    Questions remaining
                                                                                                                </td>
                                                                                                                <td style="width: 2%">
                                                                                                                </td>
                                                                                                                <td class="test_con_questions_remaining">
                                                                                                                    <asp:Label runat="server" ID="TestConductor_remainingQtsLabel" Text="0"></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                            <div class="test_con_infodoc" style="display: none" runat="server" id="TestConductor_dockHideDiv">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="vertical-align: bottom">
                                                                    <table align="center">
                                                                        <tr>
                                                                            <td class="test_con_footer_text">
                                                                                © 2010. Forte HCM. All Rights Reserved.
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="TestConductor_recommendedTimeHiddenField" Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_timeEllapsedHiddenField" Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_questionTimeEllapsedHiddenField"
        Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_questionKeyHiddenField" Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_maintainDockHiddenField" Value="undock" />
    <asp:HiddenField runat="server" ID="TestConductor_timeElapsedStatus" Value="0" />
    <asp:Panel ID="TestConductor_timeElapsedPanel" runat="server" Style="display: none"
        CssClass="popupcontrol_confirm_remove">
        <div style="display: none;">
            <asp:Button ID="TestConductor_hiddenTimeElapsedButton" runat="server" Text="Hidden" /></div>
        <uc2:ConfirmMsgControl ID="TestConductor_timeElapsedConfirmMsgControl" runat="server"
            Title="Time elapsed" Type="OkSmall" Message="Your time was elapsed!" OnOkClick="TestConductor_OkClick" />
    </asp:Panel>
    <ajaxToolKit:ModalPopupExtender ID="TestConductor_timeElapsedModalPopupExtender"
        runat="server" PopupControlID="TestConductor_timeElapsedPanel" TargetControlID="TestConductor_hiddenTimeElapsedButton"
        BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    </form>
</body>
</html>
