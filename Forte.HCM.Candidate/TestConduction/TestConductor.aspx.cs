﻿#region Header                                                                 

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// TestConductor.aspx.cs
// This page allows the candidate to take up the test by displaying the questions 
// one by one. All informations like total time elapsed for the test, total time remaining, 
// time ellpased for the question and the question count are displayed.

#endregion Header                                                              

#region Directives                                                             

using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.ExternalService;

#endregion Directives                                                          

namespace Forte.HCM.UI.TestConduction
{
    /// <summary>
    /// This page allows the candidate to take up the test by displaying the questions 
    /// one by one. All informations like total time elapsed for the test, total time remaining, 
    /// time elapsed for the question and the question count are displayed.
    /// </summary>
    /// <remarks>
    /// This class subclasses the <see cref="PageBase"/> object.
    /// </remarks>
    public partial class TestConductor : PageBase
    {
        #region Private Variables                                              

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        private delegate void AsyncTaskDelegate();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionDetail"></param>
        private delegate void AsyncSubmitQuestionDelegate(AsyncEventArgs e);

        /// <summary>
        /// A <seealso cref="AsyncTaskDelegate"/> that holds the reference to 
        /// asynchronously retrieve the next question
        /// </summary>
        private AsyncTaskDelegate delegateAsyncTask;

        /// <summary>
        /// A <seealso cref="AsyncTaskDelegate"/> that holds the reference to
        /// asynchronously submit the question
        /// </summary>
        private AsyncSubmitQuestionDelegate delegateAsyncSubmitQuestion;

        #endregion Private Variables                                           

        #region Event Handlers                                                 

        /// <summary>
        /// Handler that will call when the page gets loaded.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Clear message and hide labels.
                ClearMessages();

                if (!IsPostBack)
                {
                    LoadValues();
                }

                ShowHideDockPanel();

                // If CandidateSessionKey and AttemptId are passed in QueryString, then start the timer 
                if (Session["CANDIDATE_SESSIONKEY"].ToString() != ""
                    && Convert.ToInt32(Session["ATTEMPT_ID"]) != 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "start",
                         "<script type='text/javascript'> window.onload = WindowLoad; </script>");
                }
                TestConductor_topErrorMessageLabel.Text = "";
            }
            catch (Exception exception)
            {
                base.ShowMessage(TestConductor_topErrorMessageLabel,
                    exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// This handler triggers on clicking the submit button. It passes all the 
        /// values related to submitted question to BLManager.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestConductor_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CURRENT_QUESTION_INDEX"] == null || ViewState["TOTAL_QUESTIONS"] == null)
                    return;

                // Validate the options radio buttons. If answer is not selected before clicking submit button, 
                // then display the error message.
                if (TestConductor_answerRadioButtonList.SelectedIndex != -1)
                {
                    TestConductor_answeredQtsLabel.Text = ((Convert.ToInt32(TestConductor_answeredQtsLabel.Text) + 1).ToString());

                    // Call the async method to submit the current question and to retrieve the next question.
                    CallAsyncMethods('N');
                }
                else
                    base.ShowMessage(TestConductor_topErrorMessageLabel,
                     Resources.HCMResource.TestConductor_SelectAnswer);
            }
            catch (Exception exception)
            {
                base.ShowMessage(TestConductor_topErrorMessageLabel,
                    exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// This handler triggers on clicking the skip button. It passes all the values
        /// related to skipped question to BL manager.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestConductor_skipLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                TestConductor_skippedQtsLabel.Text =
                    (Convert.ToInt32(TestConductor_skippedQtsLabel.Text) + 1).ToString();
                // Call the async method to submit the current question and to retrieve the next question.
                CallAsyncMethods('Y');
            }
            catch (Exception exception)
            {
                base.ShowMessage(TestConductor_topErrorMessageLabel,
                    exception.Message);
                Logger.ExceptionLog(exception);
            }
        }
        /// <summary>
        /// This method gets called on clicking the Quit link button or when the time
        /// is elapsed. Time elapsed will pass the event argument as 'SESS_ELLP'
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestConductor_quitTestLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                string status = Request.Params.Get("__EVENTARGUMENT");
                if (status == "")
                {
                    status = Constants.CandidateSessionStatus.QUIT;
                    SubmitTestDetails(status, null, 'Y');
                }
                else
                {
                    TestConductor_timeElapsedStatus.Value = "Elapsed";
                    TestConductor_timeElapsedModalPopupExtender.Show();
                }

            }
            catch (Exception exception)
            {
                base.ShowMessage(TestConductor_topErrorMessageLabel,
                    exception.Message);
                Logger.ExceptionLog(exception);
            }

        }

        /// <summary>
        /// Handler that will call when the test time is elapsed.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void TestConductor_OkClick(object sender, EventArgs e)
        {
            try
            {
                SubmitTestDetails(Constants.CandidateSessionStatus.ELAPSED, null, 'Y');
            }
            catch (Exception exception)
            {
                base.ShowMessage(TestConductor_topErrorMessageLabel,
                    exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        #endregion Event Handlers                                              

        #region Private Methods                                                

        /// <summary>
        /// This method show or hide the right side timing panel.
        /// </summary>
        private void ShowHideDockPanel()
        {
            // Dock/Undock the timing panel
            if (TestConductor_maintainDockHiddenField.Value == "undock")
            {
                TestConductor_dockShowDiv.Attributes.Add("Style", "display:''");
                TestConductor_dockHideDiv.Attributes.Add("Style", "display:none");
            }
            else
            {
                TestConductor_dockShowDiv.Attributes.Add("Style", "display:none");
                TestConductor_dockHideDiv.Attributes.Add("Style", "display:''");
            }
        }

        /// <summary>
        /// This method calls the asynchronous functions to submit the 
        /// existing question and to retrieve the next question details.
        /// </summary>
        /// <param name="isSkipped">
        /// This parameter specifies the skipped status 'Y'/'N'
        /// </param>
        private void CallAsyncMethods(char isSkipped)
        {
            // Assign the values to Asynchronous event argument.
            AsyncEventArgs asyncArg = new AsyncEventArgs();
            asyncArg.QuestionKey = TestConductor_questionKeyHiddenField.Value.ToString();
            if (TestConductor_answerRadioButtonList.SelectedIndex == -1)
                asyncArg.SubmittedAnswer = "";
            else
                asyncArg.SubmittedAnswer = TestConductor_answerRadioButtonList.SelectedItem.Text;

            // Calculate the End time of the question by adding elapsed value with Start time
            asyncArg.QuestionEndTime = Convert.ToDateTime(Session["QUESTION_START_TIME"]).AddSeconds(Convert.ToDouble(TestConductor_questionTimeEllapsedHiddenField.Value));
            // Initialize the question time elapsed value
            TestConductor_questionTimeEllapsedHiddenField.Value = "0";
            asyncArg.QuestionStartTime = Convert.ToDateTime(Session["QUESTION_START_TIME"]);
            asyncArg.isSkipped = isSkipped;

            // Check whether all the questions are displayed by comparing currentIndex with total questions count.
            if (Convert.ToInt32(ViewState["CURRENT_QUESTION_INDEX"].ToString()) <= Convert.ToInt32(ViewState["TOTAL_QUESTIONS"].ToString()))
            {
                // Register the asynchronous task to submit the question details.
                Page.RegisterAsyncTask(new PageAsyncTask(
                           new BeginEventHandler(this.BeginSubmitQuestionDetail),
                           new EndEventHandler(this.EndSubmitQuestion),
                           new EndEventHandler(this.TimeoutHandler), asyncArg));

                Page.RegisterAsyncTask(new PageAsyncTask(
                    new BeginEventHandler(this.BeginRetrieveNextQuestion),
                    new EndEventHandler(this.EndRetrieveNextQuestion),
                    new EndEventHandler(this.TimeoutHandler), true));

                DisplayQuestion();
            }
            else
                SubmitTestDetails(Constants.CandidateSessionStatus.COMPLETED, asyncArg, isSkipped);
        }

        /// <summary>
        /// This method stores the test result values to DB.
        /// </summary>
        /// <param name="testStatus">
        /// testStatus parameter can be one of the three values "quit","completed","timeout"
        /// </param>
        private void SubmitTestDetails(string testStatus, AsyncEventArgs asyncArgs, char isSkipped)
        {
            try
            {
                StoreQuestionDetails(asyncArgs);
                CandidateTestResult candidateTestResult = new CandidateTestResult();
                candidateTestResult.CandidateSessionKey = Session["CANDIDATE_SESSIONKEY"].ToString();
                TestSessionDetail testSession = null;
                if (Session["TEST_SESSION"] != null)
                {
                    testSession = Session["TEST_SESSION"] as TestSessionDetail;
                    candidateTestResult.TestKey = testSession.TestID;
                }
                candidateTestResult.AttemptID = Convert.ToInt32(Session["ATTEMPT_ID"].ToString());
                candidateTestResult.TotalQuestionViewed = Convert.ToInt32(TestConductor_currentQuestCountLabel.Text);
                candidateTestResult.Status = testStatus;
                // Calculate the totalQuestionsNotViewed by subtracting the currently viewed question index from total questions
                candidateTestResult.TotalQuestionNotViewed = Convert.ToInt32(ViewState["TOTAL_QUESTIONS"].ToString()) - Convert.ToInt32(TestConductor_currentQuestCountLabel.Text);
                candidateTestResult.StartTime = Convert.ToDateTime(Session["START_TIME"].ToString());

                if (asyncArgs != null)
                    // Instead of calculating the end time for test, use the last question end time.
                    candidateTestResult.EndTime = asyncArgs.QuestionEndTime;
                else
                    candidateTestResult.EndTime =
                        Convert.ToDateTime(Session["START_TIME"]).AddSeconds
                        (Convert.ToDouble(TestConductor_timeEllapsedHiddenField.Value));

                // TODO : Later release
                candidateTestResult.RelativeScore = 0;
                candidateTestResult.ModifiedBy = base.userID;
                candidateTestResult.CreatedBy = base.userID;
                new TestConductionBLManager().InsertCandidateTestResult(candidateTestResult);

                bool isMailSent = true;

                // Pass the completed status to DB.
                new TestConductionBLManager().UpdateSessionStatus(Session["CANDIDATE_SESSIONKEY"].ToString(), 
                    Convert.ToInt32(Session["ATTEMPT_ID"]), Constants.CandidateAttemptStatus.COMPLETED, 
                    Constants.CandidateSessionStatus.COMPLETED, base.userID, out isMailSent);

                try
                {
                    // Send mail to scheduler and position profile owner, indicating 
                    // that the candidate has completed the test. This email will 
                    // consist of a link that will take the user directly to the 
                    // candidate’s test report screen
                    new EmailHandler().SendMail(EntityType.TestCompleted,
                        Session["CANDIDATE_SESSIONKEY"].ToString(), 
                        Session["ATTEMPT_ID"].ToString());
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                try
                {
                    // Send mail to candidate, if it is a self admin test. This 
                    // email will consist of a link that will take the user directly
                    // to the test resuls page.
                    CandidateTestSessionDetail candidateTestSessionDetail = new TestSchedulerBLManager().
                        GetCandidateTestSessionEmailDetail(Session["CANDIDATE_SESSIONKEY"].ToString()
                        , Convert.ToInt32(Session["ATTEMPT_ID"].ToString()));

                    if (candidateTestSessionDetail != null && candidateTestSessionDetail.SchedulerID ==
                        Convert.ToInt32(candidateTestSessionDetail.CandidateID))
                    {
                        new EmailHandler().SendMail(EntityType.SelfAdminTestCompleted,
                           Session["CANDIDATE_SESSIONKEY"].ToString(),
                           Session["ATTEMPT_ID"].ToString());
                    }
                }
                catch (Exception exp)
                {
                    Logger.ExceptionLog(exp);
                }

                // Get certificate test status
                TestDetail testDetail = new TestBLManager().GetTestDetail(testSession.TestID);
                bool isCertificate = Convert.ToBoolean(testDetail.IsCertification);

                // Check if the test is certificate test.
                if (isCertificate)
                {
                    bool isCertificateGenerated = GenerateCertificate(testSession.TestID, candidateTestResult.CandidateSessionKey,
                        candidateTestResult.AttemptID, candidateTestResult.StartTime,
                        candidateTestResult.EndTime, testDetail.Name.Trim());
                }

                if (testSession != null && testSession.IsDisplayResultsToCandidate)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "completed",
                        "<script type='text/javascript'> var OnReload = 'OpenTestCompletedPage(\"" + "../TestCenter/CandidateTestResult.aspx?testkey=" +
                        testSession.TestID + "&parentpage=MY_TST&mode=SHARE&candidatesessionid=" + Session["CANDIDATE_SESSIONKEY"].ToString()
                        + "&attemptid=" + Session["ATTEMPT_ID"].ToString() + "\")';eval(OnReload);</script>");
                else
                    // Append the query string and navigate to Completed page according the status.
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "completed",
                        "<script type='text/javascript'> var OnReload = 'OpenTestCompletedPage(\"" + "../TestCenter/TestConductionCompleted.aspx"
                        + "?candidatesessionid=" + Session["CANDIDATE_SESSIONKEY"].ToString()
                        + "&attemptid=" + Session["ATTEMPT_ID"].ToString() 
                        + "&status=" + testStatus + "\")';eval(OnReload);</script>");
            }
            catch (Exception exception)
            {
                base.ShowMessage(TestConductor_topErrorMessageLabel,
                     exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Method that will generate the certificate for completed test.
        /// </summary>
        private bool GenerateCertificate(string testID, string candidateSessionKey,
            int attemptID, DateTime startTime, DateTime endTime, string testName)
        {
            try
            {
                // Insert certificate image in CANDIDATE_TEST_RESULT_CERTIFICATE table.
                string candidateName = new CommonBLManager().GetUserDetail(base.userID).FirstName.Trim();
                string htmlTemplate = string.Empty;
                byte[] imageData = null;

                // Get HTML text against the test key
                CertificationDetail certificationDetail =
                    new TestBLManager().GetTestCertificateDetail(testID);

                // Get candidate score obtained after the test is completed.
                CandidateTestDetail candidateTestDetail =
                    new TestBLManager().GetCandidateScoreDetail
                    (candidateSessionKey, attemptID);

                // Check if the candidate obtained score is eligible for certificate.
                bool isValidScore = ((candidateTestDetail.MyScore * 100)
                    >= certificationDetail.MinimumTotalScoreRequired) ? true : false;

                TimeSpan diffTime = endTime - startTime;

                // Validate with minimum required score and maximum time permissible.
                if (isValidScore &&
                    (certificationDetail.MaximumTimePermissible >=
                    Utility.ConvertHoursMinutesSecondsToSeconds(diffTime.ToString())))
                {
                    // Replace #Name, #TestName, #CompletedDate in the html data.
                    htmlTemplate = certificationDetail.HtmlText.Trim();
                    htmlTemplate = htmlTemplate.Replace("#Name", candidateName);
                    htmlTemplate = htmlTemplate.Replace("#TestName", testName);
                    htmlTemplate = htmlTemplate.Replace("#CompletedDate",
                        GetDateFormat(Convert.ToDateTime(endTime.ToString())));

                    // Make an HTML file and converted into an image.
                    string filePath = Server.MapPath("../CertificateFormats/") 
                        + candidateSessionKey + "_" + attemptID + ".htm";

                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        writer.WriteLine(htmlTemplate.Trim());
                        writer.Close();
                    }

                    string address = filePath;
                    int width = 620;
                    int height = 420;

                    Bitmap bmp = WebPageCaptureManager.GetWebPageCaptureImage
                        (address, 620, 420, width, height);

                    imageData = ImageToByteArray(bmp);

                    new TestConductionBLManager().InsertCertificateImage
                        (candidateSessionKey, attemptID, imageData, base.userID);
                    return true;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method that will convert an image to byte array.
        /// </summary>
        /// <param name="img">
        /// An <see cref="Image"/> that contains the image instance.
        /// </param>
        /// <returns>
        /// A <see cref="byte[]"/> that contains the byte array.
        /// </returns>
        private byte[] ImageToByteArray(Image img)
        {
            ImageConverter convertor = new ImageConverter();
            return ((byte[])convertor.ConvertTo(img, typeof(byte[])));
        }

        /// <summary>
        /// Get all the question ids by passing Candidate_Session_Key. 
        /// </summary>
        private void GetQuestionIds()
        {
            Dictionary<string, string> questionIds = null;
            // Get the question keys and store it in session
            questionIds = new TestConductionBLManager().GetQuestionIDs(Session["CANDIDATE_SESSIONKEY"].ToString());
            Session["QUESTION_IDS"] = questionIds;

            // Initialize all the viewstate values
            ViewState["CURRENT_QUESTION_INDEX"] = 0;
            ViewState["TOTAL_QUESTIONS"] = questionIds.Count;
        }

        /// <summary>
        /// This method is used to display the question in page.
        /// </summary>
        private void DisplayQuestion()
        {
            if (Session["QUESTION_DETAIL"] != null)
            {
                // Get the next question from Session and display the details. 
                QuestionDetail questionDetail = Session["QUESTION_DETAIL"] as QuestionDetail;
                TestConductor_questionLabel.Text = questionDetail.Question;
                TestConductor_questionKeyHiddenField.Value = questionDetail.QuestionKey;
                // Assign the answer choices to radio buttons
                TestConductor_answerRadioButtonList.DataSource = questionDetail.AnswerChoices;
                TestConductor_answerRadioButtonList.DataTextField = "Choice";
                TestConductor_answerRadioButtonList.DataValueField = "ChoiceID";
                TestConductor_answerRadioButtonList.DataBind();

                if (questionDetail.HasImage)
                {
                    Session["POSTED_QUESTION_IMAGE"] = questionDetail.QuestionImage;
                    TestConductor_questionImage.ImageUrl = @"~/Common/ImageHandler.ashx?source=QUESTION_IMAGE&question_key=" + questionDetail.QuestionKey;
                    TestConductor_questionImage.Visible = true;
                    TestConductor_questionDiv.Style.Add("height", "140px");
                }
                else
                {
                    TestConductor_questionImage.Visible = false;
                    TestConductor_questionDiv.Style.Add("height", "100%");
                }

                int currentIndex = Convert.ToInt32(ViewState["CURRENT_QUESTION_INDEX"].ToString());
                int totalQuestions = Convert.ToInt32(ViewState["TOTAL_QUESTIONS"].ToString());

                // Calculate the progress bar value by using current display question index and total questions count.
                TestConductor_progressBar.Value = (100 * (currentIndex - 1)) / totalQuestions;

                TestConductor_currentQuestCountLabel.Text = ViewState["CURRENT_QUESTION_INDEX"].ToString();
                TestConductor_totalQuestCountLabel.Text = ((Dictionary<string, string>)Session["QUESTION_IDS"]).Count.ToString();

                int answered = Convert.ToInt32(TestConductor_answeredQtsLabel.Text);
                int skipped = Convert.ToInt32(TestConductor_skippedQtsLabel.Text);
                int total = Convert.ToInt32(ViewState["TOTAL_QUESTIONS"].ToString());

                // Calculate the remaining questions pending to attend
                TestConductor_remainingQtsLabel.Text = (total - (answered + skipped)).ToString();
            }

            Session["QUESTION_START_TIME"] = DateTime.Now;
        }

        /// <summary>
        /// This method is used to get the next question key from session and
        /// to get the question from DB. Retrieved question is stored in session for next display.
        /// </summary>
        private void GetNextQuestionInSession()
        {
            string questionKey = GetNextQuestionKey();
            Session["QUESTION_DETAIL"] = GetQuestionByQuestionID(questionKey);
        }

        private string GetNextQuestionKey()
        {
            string questionKey = string.Empty;
            if (Session["QUESTION_IDS"] == null)
                throw new Exception("Error in displaying data. Contact Admin");
            if (ViewState["CURRENT_QUESTION_INDEX"] == null || ViewState["CURRENT_QUESTION_INDEX"].ToString() == string.Empty)
                return questionKey;
            int currentIndex = Convert.ToInt32(ViewState["CURRENT_QUESTION_INDEX"].ToString());

            // Get the question keys from session.
            Dictionary<string, string> questionIDs = (Dictionary<string, string>)Session["QUESTION_IDS"];
            List<string> questionKeys = new List<string>(questionIDs.Keys);

            // Check next question is available. If so retrieve the question key and return.
            if (questionIDs.Count > currentIndex)
            {
                questionKey = questionKeys[currentIndex];
            }
            ViewState["CURRENT_QUESTION_INDEX"] = ++currentIndex;

            return questionKey;
        }

        /// <summary>
        /// This method is used to retrieve the Question details by passing the question key.
        /// </summary>
        /// <param name="questionKey"></param>
        /// <returns></returns>
        private QuestionDetail GetQuestionByQuestionID(string questionKey)
        {
            QuestionDetail questionDetail = null;
            questionDetail = new TestConductionBLManager().GetQuestionByQuestionKey(questionKey);
            return questionDetail;
        }


        /// <summary>
        /// This method stores all the question details to db.
        /// </summary>
        /// <param name="isSkipped">
        /// This parameter is used to identify whether this question is
        /// skipped or answered. It receives the values 'Y' or 'N'
        /// </param>
        private void StoreQuestionDetails(AsyncEventArgs e)
        {
            try
            {
                // While quiting the test, don't submit the last question which is in display.
                if (e == null)
                    return;
                // Create the instance for CandidateTestResultFact and assign all the values.
                CandidateTestResultFact candidateTestResultFact = new CandidateTestResultFact();
                candidateTestResultFact.CandidateSessionKey = Session["CANDIDATE_SESSIONKEY"].ToString();
                candidateTestResultFact.AttemptID = Convert.ToInt32(Session["ATTEMPT_ID"]);

                Dictionary<string, string> questionIDs = (Dictionary<string, string>)Session["QUESTION_IDS"];

                candidateTestResultFact.TestQuestionID = Convert.ToInt32(questionIDs[e.QuestionKey]);
                candidateTestResultFact.QuestionKey = e.QuestionKey;
                candidateTestResultFact.Status = "COMPLETED";

                // TODO: Need to check whether these session time values gets modified in DisplayQuestion() in async.
                candidateTestResultFact.StartTime = e.QuestionStartTime;
                candidateTestResultFact.EndTime = e.QuestionEndTime;
                candidateTestResultFact.Skipped = e.isSkipped;
                candidateTestResultFact.ModifiedBy = base.userID;

                // Insert the Candidate TestResult details to DB
                new TestConductionBLManager().UpdateCandidateTestResultFact(candidateTestResultFact, e.SubmittedAnswer);
            }
            catch (Exception exception)
            {
                base.ShowMessage(TestConductor_topErrorMessageLabel,
                     exception.Message);
                Logger.ExceptionLog(exception);
            }
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            TestConductor_topErrorMessageLabel.Text = string.Empty;
            TestConductor_topSuccessMessageLabel.Text = string.Empty;

            TestConductor_topErrorMessageLabel.Visible = false;
            TestConductor_topSuccessMessageLabel.Visible = false;
        }

        #endregion Private Methods                                             

        #region IAsync Methods - Retrieve Next Question                        

        /// <summary>
        /// Asynchronous method to retrieve the next question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        IAsyncResult BeginRetrieveNextQuestion
            (Object sender, EventArgs e, AsyncCallback callback, object state)
        {
            try
            {
                delegateAsyncTask = new AsyncTaskDelegate(RetrieveNextQuestionFromDB);
                IAsyncResult result = delegateAsyncTask.BeginInvoke(callback, state);

                return result;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                return null;
            }
        }
        private void RetrieveNextQuestionFromDB()
        {
            GetNextQuestionInSession();
        }
        private void EndRetrieveNextQuestion(IAsyncResult asyncResult)
        {
            delegateAsyncTask.EndInvoke(asyncResult);
        }
        /// <summary>
        /// Represents the method that acts as the timeout event handler for 
        /// retrieving and submitting the question.
        /// </summary>
        /// <param name="asyncResult">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        private void TimeoutHandler(IAsyncResult asyncResult)
        {
            Logger.TraceLog("Async timed out");
        }

        #endregion IAsync Methods - Retrieve Next Question                     

        #region IAsync Methods - Submit Question                               

        /// <summary>
        /// Asynchronous method to submit the question details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        IAsyncResult BeginSubmitQuestionDetail
            (Object sender, EventArgs e, AsyncCallback callback, object state)
        {
            try
            {
                delegateAsyncSubmitQuestion = new AsyncSubmitQuestionDelegate(SubmitQuestionDetails);

                IAsyncResult result = delegateAsyncSubmitQuestion.BeginInvoke(state as AsyncEventArgs, callback, state);

                return result;
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);

                return null;
            }
        }
        private void SubmitQuestionDetails(AsyncEventArgs e)
        {
            StoreQuestionDetails(e);
        }

        private void EndSubmitQuestion(IAsyncResult asyncResult)
        {
            delegateAsyncSubmitQuestion.EndInvoke(asyncResult);
        }

        #endregion IAsync Methods - Submit Question                            

        #region Override Methods                                               

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            return true;
        }

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
            // Clear the session values 
            Session["START_TIME"] = DateTime.Now;
            Session["ATTEMPT_ID"] = "";
            Session["TEST_SESSION"] = "";
            Session["CANDIDATE_SESSIONKEY"] = "";

            // If CandidateSessionKey and AttemptID are not passed in query string,
            // then assign default values.
            if (!Utility.IsNullOrEmpty(Request.QueryString["csk"]))
                Session["CANDIDATE_SESSIONKEY"] = Request.QueryString["csk"].ToString();
            else
                Session["CANDIDATE_SESSIONKEY"] = "";
            if (!Utility.IsNullOrEmpty(Request.QueryString["atmpt"]))
                Session["ATTEMPT_ID"] = Request.QueryString["atmpt"].ToString();
            else
                Session["ATTEMPT_ID"] = 0;

            if (Session["CANDIDATE_SESSIONKEY"].ToString() != "" && Convert.ToInt32(Session["ATTEMPT_ID"]) != 0)
            {
                // Start the test conduction by changing the status as InProgress.
                TestSessionDetail testSession = new TestConductionBLManager().StartTestConduction(Session["CANDIDATE_SESSIONKEY"].ToString(),
                    Convert.ToInt32(Session["ATTEMPT_ID"]), Constants.CandidateAttemptStatus.IN_PROGRESS,
                    Constants.CandidateSessionStatus.IN_PROGRESS, base.userID);
                Session["TEST_SESSION"] = testSession;

                // Get all the question keys and store it in session
                GetQuestionIds();

                // Get first question in session and then display
                GetNextQuestionInSession();
                DisplayQuestion();

                TestConductor_dockShowDiv.Attributes.Add("onclick", "DockRightPanel('" + TestConductor_dockShowDiv.ClientID + "','" + TestConductor_dockHideDiv.ClientID + "')");
                TestConductor_dockHideDiv.Attributes.Add("onclick", "DockRightPanel('" + TestConductor_dockShowDiv.ClientID + "','" + TestConductor_dockHideDiv.ClientID + "')");
                TestConductor_submitButton.Attributes.Add("onclick", " this.disabled = true;document.getElementById('" + TestConductor_skipLinkButton.ClientID + "').disabled=true;" +
                    ClientScript.GetPostBackEventReference(TestConductor_submitButton, null) + ";");
                TestConductor_skipLinkButton.Attributes.Add("onclick", " this.disabled = true;document.getElementById('" + TestConductor_submitButton.ClientID + "').disabled=true;" +
    ClientScript.GetPostBackEventReference(TestConductor_skipLinkButton, null) + ";");

                // Assign the timeLimit value
                TestConductor_recommendedTimeHiddenField.Value = testSession.TimeLimit.ToString();

                TestConductor_mainHeadLabel.Text = testSession.TestName;

                // Register the asynchronous task to retrieve the next question.
                Page.RegisterAsyncTask(new PageAsyncTask(
                    new BeginEventHandler(this.BeginRetrieveNextQuestion),
                    new EndEventHandler(this.EndRetrieveNextQuestion),
                    new EndEventHandler(this.TimeoutHandler), true));
            }
        }

        #endregion Override Methods                                            
    }

    /// <summary>
    /// This class is used as eventArgs to pass as parameter in asynchronous methods
    /// </summary>
    internal class AsyncEventArgs
    {
        public string QuestionKey;
        public string SubmittedAnswer;
        public DateTime QuestionEndTime;
        public DateTime QuestionStartTime;
        public char isSkipped;
    }
}