﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="TestPerformer.aspx.cs"
    Inherits="Forte.HCM.Candidate.TestConduction.TestPerformer" %>

<%@ Register Src="~/CommonControls/ConfirmMsgControl.ascx" TagName="ConfirmMsgControl"
    TagPrefix="uc2" %>
<%@ Register Assembly="EO.Web" Namespace="EO.Web" TagPrefix="eo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Taking Test</title>
    <base target="_self" />
    <script src="../JS/TestConductorJS.js" type="text/javascript"></script>
    <script src="../JS/jquery-1.11.0.min.js"></script>
    <script src="../JS/jquery-3.3.1.slim.min.js" type="text/javascript"></script>
    <style type="text/css">
        .grid_header_row_no_wrap th
        {
            display: none !important;
        }
    </style>
    <script src="../JS/circlos.js"></script>
    <%-- <link rel="stylesheet" href="../App_Themes/DefaultTheme/Bootsrap.css"><script src="../JS/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../App_Themes/DefaultTheme/ProgressBar.css">--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#TestConductor_answerTextBox').keyup(checkCount);
            $('#TestConductor_answerTextBox').keydown(checkCount);
            $('#TestConductor_answerTextBox').keypress(checkCount);
            //$('#TestConductor_answerTextBox').keypress(function(e){ 
            function checkCount() {
                var chars = $('#TestConductor_answerTextBox').val();
                var withoutSpace = chars.replace(/\s/g, "");
                var charsLength = withoutSpace.length;
                var TotalChars = $('#TestConductor_answerMaxLengthHidden').val();
                var RemainingChars = TotalChars - charsLength;
                $('#TestConductor_answerRemainingCharsValueLabel').text(RemainingChars);

                //                if (charsLength == TotalChars) {
                //                    //e.preventDefault();//prevent key enter 
                //                } else 
                if (charsLength > TotalChars) {
                    // Maximum exceeded
                    $('#TestConductor_answerTextBox').val($('#TestConductor_answerTextBox').val().substring(0, (TotalChars - 1)));
                    checkCount();
                }
            }
            $('#TestConductor_questionGridview_questionDiv').click(function () {
                $('#TestConductor_answerTextBox').click();
            });
            $(".cdev").circlos();
        });

        // js function time remaining warning window.
        function hideTimeRemainingDiv() {
            document.getElementById("TestConductor_timeRemainingVisibilityHidden").value = "0";
            document.getElementById("TestConductor_timeRemainingWarning").style.display = "none";
        }

    </script>
</head>
<body style="background-color: #000000;">
    <form id="TestConductor_form" runat="server">
    <ajaxToolKit:ToolkitScriptManager ID="TestConductor_scriptManager" runat="server">
    </ajaxToolKit:ToolkitScriptManager>
    <div class="test_performer_innerpage_outer">
        <div class="test_performer_innerpage_outer">
            <div class="test_performer_cnt_outer">
                <div class="test_perform_con_inner_left">
                    <div class="test_performer_forte_logo">
                        &nbsp;
                    </div>
                    <div class="test_performer_line">
                        &nbsp;
                    </div>
                    <div class="test_performer_title_padding">
                        <asp:Label ID="TestConductor_mainHeadLabel" runat="server" SkinID="sknLabelTestPerformConMainHead"
                            Text=""></asp:Label>
                    </div>
                </div>
                <div class="test_perform_con_inner_middle" id="TestConductor_timeRemainingWarning"
                    style="display: none">
                    <div class="test_perform_con_timeRemaining_div">
                        <div class="test_perform_con_timeRemainingAlert">
                            <input type="hidden" id="TestConductor_timeRemainingVisibilityHidden" value="1" />
                            <asp:HiddenField ID="TestConductor_timeRemainingWarningSecsHidden" Value="120" runat="server" />
                            <img alt="" height="35px" width="35px" id="TestConductor_timeRemainingAlertImg" src="../App_Themes/DefaultTheme/Images/alert.png" />
                            &nbsp;&nbsp;
                            <asp:Label runat="server" ID="TestConductor_timeRemainAlertLabel" Text="Only 4 minutes & 30 secs more.."
                                Width="180px"></asp:Label>
                        </div>
                        <div class="test_perform_con_timeRemainingAlert_close">
                            <img alt="" height="15px" width="15px" id="TestConductor_timeRemainingAlertCloseImg"
                                src="../App_Themes/DefaultTheme/CommonImages/delete.png" onclick="hideTimeRemainingDiv();" />
                        </div>
                    </div>
                </div>
                <div class="test_perform_con_right">
                    <div class="test_perform_con_inner_right">
                        <asp:LinkButton ID="TestConductor_quitTestLinkButton" runat="server" Text="Quit Test"
                            SkinID="sknQuitTestPerformLinkButton" OnClick="TestConductor_quitTestLinkButton_Click"></asp:LinkButton>
                        <asp:ImageButton ID="TestConductor_helpImageButton" runat="server" SkinID="sknTestPerformConHelpImageButton" />
                    </div>
                </div>
            </div>
            <div class="test_perform_outer">
                <div runat="server" id="TestConductor_leftSidePanel" class="test_perform_outer_left">
                    <div runat="server" id="TestConductor_questionPanelDiv" class="test_perform_con_infobg_left">
                        <span class="test_performer_question_palette" style="display: none">Question Palette</span>
                        <div class="test_performer_doc_padding_left" id="test_performer_scrollbar">
                            <asp:GridView ID="TestConductor_questiontGridview" runat="server" SkinID="sknTestPerformerGridView"
                                OnRowDataBound="TestConductor_questionGridview_onRowDataBound" OnRowCommand="TestConductor_questionGridview_onRowCommand">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div id="TestConductor_questionGridview_questionDiv" runat="server">
                                                <asp:LinkButton ID="TestConductor_questionGridview_linkbutton" CommandArgument='<%# Eval("QuestionKey") %>'
                                                    CommandName="loadQuestion" runat="server" CssClass="Test_Performer_questionList"
                                                    Width="150px">
                                                    <%# "Question   " + (Container.DataItemIndex + 1) %>
                                                    <asp:Image ID="TestConductor_questionGridview_questionStatusImage" runat="server"
                                                        ImageUrl="~/App_Themes/DefaultTheme/Images/not_answered.png" Style="border-width: 0px;"
                                                        Height="20px" Width="20px" />
                                                </asp:LinkButton>
                                                <asp:HiddenField ID="TestConductor_questionGridview_questionKeyHiddden" runat="server"
                                                    Value='<%#Eval("QuestionKey")%>' />
                                                <br />
                                                <%--<asp:Label runat="server" ID="TestConductor_questionGridview_questionLabel" Text="Question"
                                                                    CssClass="test_performer_ques_label"></asp:Label>
                                                                <asp:Label runat="server" ID="TestConductor_questionGridview_questionNoLabel" Text='<%# Container.DataItemIndex + 1 %>'
                                                                    CssClass="test_performer_ques_label"></asp:Label>
                                                                <asp:ImageButton runat="server" ID="TestConductor_questionGridview_questionAnsStatusImage"
                                                                    ImageUrl="~/App_Themes/DefaultTheme/Images/not_answered.png" Height="15px" Width="15px" />
                                                                    <asp:Button runat="server" id="TestConductor_questionGridviewHiddenBtn" style="display:none" OnClick="TestConductor_questionGridviewHiddenBtn_OnClick" />--%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="TestConductor_questionGridview_hidddenValue" runat="server"
                                                Value='<%#Eval("QuestionKey") %>' />
                                            <asp:Panel ID="TestConductor_questionGridview_hoverPanel" CssClass="Test_performer_table_outline_bg"
                                                runat="server" Width="550px">
                                                <table width="100%" border="0" cellspacing="2" cellpadding="3">
                                                    <tr>
                                                        <th class="popup_question_icon">
                                                            <div style="word-wrap: break-word; white-space: normal; font-size: large;">
                                                                <asp:Label ID="TestConductor_questionGridview_questionDetailPreviewControlQuestionLabel"
                                                                    Text='<%# Eval("Question")==null ? Eval("Question") : Eval("Question").ToString().Replace("\n", "<br />") %>'
                                                                    runat="server" SkinID="sknLabelFieldMultiText"></asp:Label>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <ajaxToolKit:HoverMenuExtender ID="TestConductor_questionGridview_hoverMenuExtender"
                                                runat="server" PopupControlID="TestConductor_questionGridview_hoverPanel" PopupPosition="Bottom"
                                                HoverCssClass="popupHover" TargetControlID="TestConductor_questionGridview_linkbutton"
                                                PopDelay="100" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="test_perform_question_legend">
                            <table>
                                <tr>
                                    <td>
                                        <img id="test_perform_question_legend_notAnsweredImg" src="../App_Themes/DefaultTheme/Images/not_answered.png"
                                            alt="" />
                                    </td>
                                    <td>
                                        <span>Not Answered</span>
                                    </td>
                                    <td><asp:Label ID="TestConductor_legend_notAnsweredLabel" runat="server" ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <img id="test_perform_question_legend_AnsweredImg" src="../App_Themes/DefaultTheme/Images/answered.png"
                                            alt="" />
                                    </td>
                                    <td>
                                        <span>Answered</span>
                                    </td>
                                    <td><asp:Label ID="TestConductor_legend_answeredLabel" runat="server" ></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <img id="test_perform_question_legend_SaveforlaterImg" src="../App_Themes/DefaultTheme/Images/save_for_later.png"
                                            alt="" />
                                    </td>
                                    <td>
                                        <span>Saved for Later</span>
                                    </td>
                                    <td><asp:Label ID="TestConductor_legend_savedForLaterLabel" runat="server" ></asp:Label></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="test_perform_outer_middle">
                    <div>
                        <div class="test_perform_con_msg_align" style="display: none">
                            <asp:UpdatePanel runat="server" ID="TestConductor_topSuccessUpdatePanel">
                                <ContentTemplate>
                                    <asp:Label ID="TestConductor_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"
                                        Height="17px"></asp:Label>
                                    <asp:Label ID="TestConductor_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"
                                        Height="17px"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div>
                            <div class="test_performer_empty_padding" style="float: left; padding-top: 30px;">
                                <asp:Label ID="TestConductor_progressHeadLabel" runat="server" SkinID="sknLabelTestProgressHead"
                                    Text="Progress:"></asp:Label>
                            </div>
                            <div class="test_performer_empty_padding" style="float: left;">
                                <%-- <eo:ProgressBar ID="TestConductor_progressBar" runat="server" Width="150px" Value="50"
                                    ShowPercentage="false" IndicatorImage="~/App_Themes/DefaultTheme/Images/test_con_progressbar.gif"
                                    BackgroundImageRight="~/App_Themes/DefaultTheme/Images/test_con_progressbar_right.gif"
                                    ControlSkinID="None" ForeColor="Red" BackgroundImage="~/App_Themes/DefaultTheme/Images/test_con_progressbar_bg.gif"
                                    IndicatorIncrement="7" BackgroundImageLeft="~/App_Themes/DefaultTheme/Images/test_con_progressbar_left.gif">
                                </eo:ProgressBar>--%>
                                <%-- <div class="col-md-1 mb20">
                                    
                                </div>--%>
                                <%--<div class="progress-bar" data-percent="0" data-duration="1000" data-color=",#27ae60"
                                    runat="server" id="TestConductor_progressDiv">
                                </div>--%>
                                <div class="cdev" data-percent="0" data-duration="10" data-color="#C0C0C0,#27ae60" runat="server"
                                    id="TestConductor_progressDiv">
                                </div>
                            </div>
                            <div class="test_perform_timer">
                                <asp:Label ID="TestConductor_timeRemainsHeadLabel" runat="server" Text="Time Remaining : "
                                    SkinID="sknLabelTestPerformConHead"></asp:Label><asp:Label ID="TestConductor_timeRemainsLabel"
                                        runat="server" Text="00:00:00" SkinID="sknLabelTestPerformConHead"></asp:Label></div>
                            <div style="clear: both">
                            </div>
                            <div>
                                <asp:Label ID="TestConductor_progressQuestLabel" runat="server" SkinID="sknLabelTestPerformProgressText"
                                    Text="Question "></asp:Label>
                                <asp:Label ID="TestConductor_currentQuestCountLabel" runat="server" SkinID="sknLabelTestPerformProgressText"
                                    Text=""></asp:Label>
                                <asp:Label ID="TestConductor_ofLabel" runat="server" SkinID="sknLabelTestPerformProgressText"
                                    Text=" of "></asp:Label>
                                <asp:Label ID="TestConductor_totalQuestCountLabel" runat="server" SkinID="sknLabelTestPerformProgressText"
                                    Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="test_performer_empty_padding">
                        </div>
                        <div style="overflow: auto; word-wrap: break-word; white-space: normal;" id="TestConductor_questionDiv"
                            runat="server">
                            <asp:Label ID="TestConductor_questionLabel" runat="server" SkinID="sknLabelTestProgressHead"
                                Text=""></asp:Label>
                            <br />
                            <asp:Image runat="server" ID="TestConductor_questionImage" />
                        </div>
                        <div style="word-wrap: break-word; white-space: normal;">
                            <asp:RadioButtonList ID="TestConductor_answerRadioButtonList" Visible="false" CssClass="test_perform_con_label_text"
                                runat="server" CellSpacing="5" RepeatColumns="1" RepeatDirection="Horizontal"
                                TextAlign="Right" Width="100%" Font-Names="arial" ForeColor="Red">
                            </asp:RadioButtonList>
                        </div>
                        <div style="word-wrap: break-word; white-space: normal;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="TestConductor_answerTextBox" Visible="false" runat="server" TextMode="MultiLine"
                                            AutoPostBack="false" Height="150px" Width="600px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="TestConductor_answerMarkLabel" runat="server" Visible="false" Text="Marks: "
                                            SkinID="sknLabelTestProgressHead"></asp:Label><asp:Label ID="TestConductor_answerMarkValueLabel"
                                                runat="server" Visible="false" SkinID="sknLabelTestProgressHead"></asp:Label>
                                        <asp:HiddenField ID="TestConductor_answerMaxLengthHidden" runat="server" />
                                    </td>
                                    <td style="float: right">
                                        <asp:Label ID="TestConductor_answerRemainingCharsLabel" runat="server" Visible="false"
                                            Text="Remaining characters: " SkinID="sknLabelTestProgressHead"></asp:Label>
                                        <asp:Label ID="TestConductor_answerRemainingCharsValueLabel" runat="server" Visible="false"
                                            SkinID="sknLabelTestProgressHead"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </div>
                        <div style="clear: both; vertical-align: top">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="float: left">
                                        <asp:Button ID="TestConductor_submitButton" runat="server" Text="Save & Submit" SkinID="sknButtonTestCond"
                                            OnClick="TestConductor_submitButton_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="TestConductor_saveForLaterButton" runat="server" Text="Save for Later"
                                            SkinID="sknButtonTestCond" OnClick="TestConductor_saveForLaterButton_Click">
                                        </asp:Button>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="TestConductor_skipLinkButton" runat="server" Text="Skip" SkinID="sknActionTestPerformLinkButton"
                                            OnClick="TestConductor_skipLinkButton_Click"></asp:LinkButton>
                                    </td>
                                    <td style="float: right">
                                        <asp:Button ID="TestConductor_submitTestButton" runat="server" Text="Submit Test"
                                            SkinID="sknButtonTestCond" OnClick="TestConductor_submitTestButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div runat="server" id="TestConductor_rightSidePanel" class="test_perform_outer_right">
                    <div runat="server" id="TestConductor_dockShowDiv" class="test_perform_con_infobg">
                        <div class="test_performer_doc_padding">
                            <div>
                                <div class="test_perform_con_label_text">
                                    Time elapsed for question</div>
                            </div>
                            <div>
                                <div class="test_perform_con_textbox_green">
                                    <span id="TestConductor_qstTimeEllapsedSpan" runat="server"></span>
                                </div>
                            </div>
                            <div>
                                <div class="test_perform_con_label_text">
                                    Time elapsed for test
                                </div>
                            </div>
                            <div>
                                <div class="test_perform_con_textbox_orange">
                                    <span id="TestConductor_testTimeEllapsedSpan" runat="server"></span>
                                </div>
                            </div>
                            <div>
                                <div class="test_perform_con_label_text">
                                    Time remaining
                                </div>
                            </div>
                            <div>
                                <div class="test_perform_con_textbox_blue">
                                    <span id="TestConductor_testTimeRemainSpan" runat="server"></span>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <div class="test_perform_con_label_text">
                                        Questions answered
                                    </div>
                                    <div class="test_perform_con_questions_answered">
                                        <asp:Label runat="server" ID="TestConductor_answeredQtsLabel" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <div class="test_perform_con_dotline">
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <div class="test_perform_con_label_text">
                                        Questions skipped
                                    </div>
                                    <div class="test_perform_con_questions_skipped">
                                        <asp:Label runat="server" ID="TestConductor_skippedQtsLabel" Text="0"></asp:Label>
                                    </div>
                                </div>
                                <div>
                                    <div class="test_perform_con_dotline">
                                    </div>
                                </div>
                                <div>
                                    <div class="test_perform_con_label_text">
                                        Questions remaining
                                    </div>
                                    <div class="test_perform_con_questions_remaining">
                                        <asp:Label runat="server" ID="TestConductor_remainingQtsLabel" Text="0"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="test_perform_con_infodoc" style="display: none" runat="server" id="TestConductor_dockHideDiv">
                    </div>
                </div>
            </div>
            <div class="test_perform_copyright_text">
                © 2012. Forte HCM. All Rights Reserved.</div>
            <div class="empty_height">
                &nbsp;
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="TestConductor_recommendedTimeHiddenField" Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_timeEllapsedHiddenField" Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_timeRemainingBlinkHiddenField"
        Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_questionTimeEllapsedHiddenField"
        Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_questionKeyHiddenField" Value="0" />
    <asp:HiddenField runat="server" ID="TestConductor_maintainDockHiddenField" Value="undock" />
    <asp:HiddenField runat="server" ID="TestConductor_timeElapsedStatus" Value="0" />
    <asp:Panel ID="TestConductor_timeElapsedPanel" runat="server" Style="display: none"
        CssClass="popupcontrol_confirm_remove">
        <div style="display: none;">
            <asp:Button ID="TestConductor_hiddenTimeElapsedButton" runat="server" Text="Hidden" />
        </div>
        <uc2:ConfirmMsgControl ID="TestConductor_timeElapsedConfirmMsgControl" runat="server"
            Title="Time elapsed" Type="OkSmall" Message="Your time was elapsed!" OnOkClick="TestConductor_OkClick"
            OnCancelClick="TestConductor_CancelClick" />
    </asp:Panel>
    <ajaxToolKit:ModalPopupExtender ID="TestConductor_timeElapsedModalPopupExtender"
        runat="server" PopupControlID="TestConductor_timeElapsedPanel" TargetControlID="TestConductor_hiddenTimeElapsedButton"
        BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    <asp:Panel ID="TestConductor_confirmTestSubmitPanel" runat="server" Style="display: none"
        CssClass="popupcontrol_confirm_remove">
        <asp:Button ID="TestConductor_confirmTestSubmitPanelButton" runat="server" />
        <uc2:ConfirmMsgControl ID="TestConductor_testSubmitConfirmMsgControl" runat="server"
            Title="Submit Test" Message="Would you like to submit the test? Please make sure you answered all the questions.press OK to submit and exit the test. Cancel to continue the test"
            OnOkClick="TestConductor_testSubmitOkClick" OnCancelClick="TestConductor_testSubmitCancelClick" />
    </asp:Panel>
    <ajaxToolKit:ModalPopupExtender ID="TestConductor_testSubmitConfirmModalPopupExtender"
        runat="server" PopupControlID="TestConductor_confirmTestSubmitPanel" TargetControlID="TestConductor_confirmTestSubmitPanelButton"
        BackgroundCssClass="modalBackground">
    </ajaxToolKit:ModalPopupExtender>
    
    </form>
</body>
</html>
<style>
    .check_box_text
    {
        font-size: 14px;
        font-weight: normal;
        color: #000000;
        background-color: Transparent;
    }
</style>
