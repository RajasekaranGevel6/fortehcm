﻿using System;
using System.Web.UI;

namespace Forte.HCM.Candidate
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected string pageCaption;
        protected void Page_Load(object sender, EventArgs e)
        { 
            if (Session["USER_DETAIL"] == null)
            {
                HCMCandidateMaster_headerControl.Visible = false;
                FORTEHCMLogo.Visible = true;
            }
            else if (Request.Url.AbsoluteUri.ToLower().Contains("signin.aspx") && Session["USER_DETAIL"]!=null)
            {
                FORTEHCMLogo.Visible = false;
                HCMCandidateMaster_headerControl.Visible = false;
                Session["USER_DETAIL"] = null;
            }
            else
            {
                FORTEHCMLogo.Visible = false;
                HCMCandidateMaster_headerControl.Visible = true;

                //ScriptManager.RegisterStartupScript(this, this.GetType(),
                //    "script", "window.history.forward(-1);", true);           

   
                /*ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "script", "window.history.forward(); window.onunload = " +
                " function () { window.location.href('./DashBoard.aspx') };", true);*/

                /**/
            }
        }

        public void SetPageCaption(string pageTitle)
        {
            if (pageTitle != null)
            {
                pageCaption = pageTitle.Trim();
            }
        }
    }
}