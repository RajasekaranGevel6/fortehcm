﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.UI.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.UI.CommonControls;

namespace Forte.HCM.Candidate
{
    public partial class DashBoard : PageBase
    {
        int candidateID = 0;
        /// <summary>
        /// A <see cref="string"/> that holds the result Url for publishing
        /// into Facebook,Twitter,Yahoo & LinkedIn.
        /// </summary>
        protected string resultUrl;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Set browser title.
                Master.SetPageCaption("Dashboard");

                // Clear message and hide labels.
                ClearMessages();

                //Set default button
                Page.Form.DefaultButton = DashBoard_searchTestImageButton.UniqueID;
                if (!IsPostBack)
                {
                    ClearControls();

                    this.DashBoard_searchTestTextBox.Focus();

                    // Show resume uploaded success message.
                    if (!Utility.IsNullOrEmpty(Session["RESUME_UPLOADED"]) && Convert.ToBoolean(Session["RESUME_UPLOADED"]) == true)
                    {
                        // Show message.
                        ShowMessage(DashBoard_successMessageLabel, "Your resume has been uploaded to the parsing server. It will take few minutes for parsing and you will be indicated through email shortly. After that you can review & approve your profile");

                        // Reset resume uploaded status from session.
                        Session["RESUME_UPLOADED"] = false;
                    }

                    HtmlControlDivInvisible();
                    if (Session["USER_DETAIL"] != null)
                    {
                        UserDetail userDetail =
                                          Session["USER_DETAIL"] as UserDetail;
                        candidateID = userDetail.UserID;
                        ViewState["RECOMMENDED_TIME"] = "";

                        // Load candidate activities.
                        LoadCandidateActivities(candidateID);

                        //Load candidate online activities
                        LoadCandidateOnlineInterview(candidateID);

                        // Assign LinkedIn share URL. 
                        AssignLinkedInShareURL();

                        //get candidate resume status
                        DashBoard_ResumeStatuLabel.Text = "";
                        DashBoard_PreviewImage.ToolTip = "";

                        DashBoard_ResumeStatuLabel.Text = GetResumeStatusMessage(GetCandidateResumeStatus(candidateID));
                        DashBoard_PreviewImage.ToolTip = DashBoard_ResumeStatuLabel.Text;

                        CandidateHome_confirmMsgControl.Message = "Are you ready to start the test?";
                        CandidateHome_confirmMsgControl.Title = "Start Test";
                        CandidateHome_confirmMsgControl.Type = MessageBoxType.YesNo;

                        InterviewCandidateHome_confirmMsgControl.Message = "Are you ready to start the interview?";
                        InterviewCandidateHome_confirmMsgControl.Title = "Start Interview";
                        InterviewCandidateHome_confirmMsgControl.Type = MessageBoxType.YesNo;
                        ViewState["SORT_ORDER"] = SortType.Ascending;
                        ViewState["SORT_FIELD"] = "TESTNAME";
                        GetCandidateSkill(candidateID);
                        LoadSkills();
                        // Load resume status.
                        LoadResumeStatus();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the 'Yes' button is clicked
        /// in the delete profile confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will delete the candidate's profile from the repository.
        /// </remarks>
        protected void DashBoard_deleteConfirmationYesButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(DashBoard_candidateResumeID.Value) || Convert.ToInt32(DashBoard_candidateResumeID.Value) == 0)
                {
                    base.ShowMessage(DashBoard_errorMessageLabel, "Resume not found to delete");
                    return;
                }

                // Delete candidate resume.
                new CandidateBLManager().DeleteCandidateResume(Convert.ToInt32(DashBoard_candidateResumeID.Value));

                ShowMessage(DashBoard_successMessageLabel,
                    "Resume deleted successfully. Please update your resume");

                // Reload the resume status.
                LoadResumeStatus();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// return candidate resume status
        /// </summary>
        /// <param name="candidateID"></param>
        /// <returns></returns>
        private ResumeStatus GetCandidateResumeStatus(int candidateID)
        {
            return new CandidateBLManager().GetCandidateResumeStatus(candidateID);
        }
        /// <summary>
        /// Get candidate skills
        /// </summary>
        /// <param name="candidateID"></param>
        private void GetCandidateSkill(int candidateID)
        {
            DashBoard_SkillsDataList.DataSource = null;
            DashBoard_SkillsDataList.DataBind();

            if (candidateID == 0) return;

            DashBoard_SkillsDataList.DataSource =
                new CandidateBLManager().GetCandidateSkills
                (candidateID);

            DashBoard_SkillsDataList.DataBind();

        }

        /// <summary>
        /// Method that loads the skill details.
        /// </summary>
        private void LoadSkills()
        {
            List<SignupCandidate> skills = new CandidateBLManager().
                GetCandidateSkills(base.userID);

            DashBoard_skillsGridView.DataSource = skills;
            DashBoard_skillsGridView.DataBind();
        }

        protected void DashBoard_AddSkillButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("signup.aspx?activetag=updateskills", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        protected void DashBoard_UploadResumeButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("signup.aspx?activetag=updateresume&parentpage=" +
                    Constants.ParentPage.CANDIDATE_EDIT_PROFILE, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Clear the controls before data load
        /// </summary>
        private void ClearControls()
        {
            DashBoard_errorMessageLabel.Text = "";
            DashBoard_PendingTest_NameLabel.Text = "";
            DashBoard_PendingTest_DescLabel.Text = "";
            DashBoard_PendingTest_CreatedDateLabel.Text = "";
            DashBoard_PendingTest_ExpiryDateLabel.Text = "";

            DashBoard_PendingInterview_NameLabel.Text = "";
            DashBoard_PendingInterview_DescLabel.Text = "";
            DashBoard_PendingInterview_CreatedDateLabel.Text = "";
            DashBoard_PendingInterview_ExpiryDateLabel.Text = "";

            DashBoard_CompletedTest_NameLabel.Text = "";
            DashBoard_CompletedTest_DescLabel.Text = "";
            DashBoard_CompletedTest_CreateDateLabel.Text = "";

            DashBoard_CompletedInterview_NameLabel.Text = "";
            DashBoard_CompletedInterview_DescLabel.Text = "";

            DashBoard_CompletedInterview_ExpiryDateLabel.Text = "";
        }

        private void HtmlControlDivInvisible()
        {
            PendingTest_Div.Style["display"] = "none";
            CompletedTest_Div.Style["display"] = "none";
            CompletedInterview_Div.Style["display"] = "none";
            PendingInterview_Div.Style["display"] = "none";

            PendingTest_emptyDiv.Style["display"] = "block";
            CompletedTest_emptyDiv.Style["display"] = "block";
            CompletedInterview_emptyDiv.Style["display"] = "block";
            PendingInterview_emptyDiv.Style["display"] = "block";
        }

        private string DateFormat(DateTime dt)
        {
            return String.Format("{0:MM/dd/yyyy}", dt);
        }

        /// <summary>
        /// Method that loads the candidate activities.
        /// </summary>
        /// <param name="candidateID">
        /// A <see cref="int"/> that holds the candidate ID.
        /// </param>
        private void LoadCandidateActivities(int candidateID)
        {
            CandidateSummary candidateSummary =
                new CandidateBLManager().GetCandidateActivity(candidateID);

            if (candidateSummary == null)
            {
                PendingTest_IntroductionImageButton.Visible = false;
                DashBoard_PendingTest_ReminderImageButton.Visible = false;
                HtmlControlDivInvisible();
                return;
            }

            #region "Get Last Completed Test"
            List<CandidateTestSessionDetail> completedTest = candidateSummary.CompletedTest;

            if (completedTest != null)
            {
                CompletedTest_Div.Style["display"] = "block";
                CompletedTest_emptyDiv.Style["display"] = "none";

                DashBoard_CompletedTest_ViewResultsImageButton.Visible =
                    completedTest[0].ShowResults;

                if (!Utility.IsNullOrEmpty(completedTest[0].TestName))
                {
                    DashBoard_CompletedTest_NameLabel.Visible = true;
                    DashBoard_CompletedTest_NameLabel.Text =
                        completedTest[0].TestName;
                }
                else
                    DashBoard_CompletedTest_NameLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(completedTest[0].TestDescription))
                {
                    DashBoard_CompletedTest_DescLabel.Visible = true;
                    DashBoard_CompletedTest_DescLabel.Text = completedTest[0].TestDescription.ToString().Length > 130 ?
                        completedTest[0].TestDescription.ToString().Substring(0, 130) + "..." : completedTest[0].TestDescription.ToString();
                }
                else
                    DashBoard_CompletedTest_DescLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(completedTest[0].DateCompleted))
                {
                    DashBoard_CompletedTest_CreatedLabel.Text = "Completed On  : ";
                    DashBoard_CompletedTest_CreateDateLabel.Visible = true;
                    DashBoard_CompletedTest_CreateDateLabel.Text =
                       DateFormat(completedTest[0].DateCompleted);
                }
                else
                    DashBoard_CompletedTest_CreateDateLabel.Visible = false;

                DashBoard_CompletedTest_AttemptIDHiddenField.Value = completedTest[0].AttemptID.ToString();
                DashBoard_CompletedTest_SessionIDHiddenField.Value = completedTest[0].CandidateTestSessionID;
                DashBoard_CompletedTest_TestKeyHiddenField.Value = completedTest[0].TestID;
                DashBoard_resultsShareDiv.Visible = false;
                CandidateTestResult_shareInLinkedInHyperLink.Visible = false;

                if (completedTest[0].ShowResults)
                {
                    DashBoard_resultsShareDiv.Visible = true;
                    CandidateTestResult_shareInLinkedInHyperLink.Visible = true;
                }
            }
            else
            {
                CompletedTest_Div.Style["display"] = "none";
                CompletedTest_emptyDiv.Style["display"] = "block";
            }
            #endregion

            #region "Getting Last Pending Test"

            List<CandidateTestSessionDetail> pendingTest =
            candidateSummary.PendingTest;

            if (pendingTest != null && pendingTest[0].IsExpired == false)
            {
                PendingTest_Div.Style["display"] = "block";
                PendingTest_emptyDiv.Style["display"] = "none";

                PendingTest_IntroductionImageButton.Visible = false;
                DashBoard_PendingTest_ReminderImageButton.Visible = false;

                DashBoard_pendingTests_testRecommendID.Value = pendingTest[0].TestRecommandID;

                if (!Utility.IsNullOrEmpty(pendingTest[0].TestName))
                {
                    DashBoard_PendingTest_NameLabel.Visible = true;
                    DashBoard_PendingTest_NameLabel.Text = pendingTest[0].TestName;
                }
                else
                    DashBoard_PendingTest_NameLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(pendingTest[0].TestDescription))
                {
                    DashBoard_PendingTest_DescLabel.Visible = true;
                    DashBoard_PendingTest_DescLabel.Text = pendingTest[0].TestDescription.ToString().Length > 130 ?
                        pendingTest[0].TestDescription.ToString().Substring(0, 130) + "..." : pendingTest[0].TestDescription.ToString();
                }
                else
                    DashBoard_PendingTest_DescLabel.Visible = false;


                if (!pendingTest[0].IsSelfAdmin)
                {
                    if (!Utility.IsNullOrEmpty(pendingTest[0].ExpiryDate) &&
                        pendingTest[0].ExpiryDate != Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                    {
                        DashBoard_PendingTest_ExpiryLabel.Visible = true;
                        DashBoard_PendingTest_ExpiryDateLabel.Visible = true;
                        DashBoard_PendingTest_ExpiryDateLabel.Text =
                            DateFormat(pendingTest[0].ExpiryDate);
                    }
                    else
                    {
                        DashBoard_PendingTest_ExpiryLabel.Visible = false;
                        DashBoard_PendingTest_ExpiryDateLabel.Visible = false;
                    }


                    if (!Utility.IsNullOrEmpty(pendingTest[0].CreatedDate))
                    {
                        DashBoard_PendingTest_CreatedLabel.Visible = true;
                        DashBoard_PendingTest_CreatedDateLabel.Visible = true;
                        DashBoard_PendingTest_CreatedDateLabel.Text =
                        DateFormat(pendingTest[0].CreatedDate);
                    }
                    else
                    {
                        DashBoard_PendingTest_CreatedDateLabel.Visible = false;
                        DashBoard_PendingTest_CreatedLabel.Visible = false;
                    }

                    if (!pendingTest[0].IsExpired)
                    {
                        PendingTest_IntroductionImageButton.Visible = true;
                        DashBoard_PendingTest_ReminderImageButton.Visible = true;
                    }
                }
                else
                {
                    DashBoard_PendingTest_CreatedLabel.Visible = false;
                    PendingTest_IntroductionImageButton.Visible = false;
                    DashBoard_PendingTest_ReminderImageButton.Visible = false;
                    DashBoard_PendingTest_ExpiryLabel.Visible = false;
                    DashBoard_PendingTest_ExpiryDateLabel.Visible = false;
                }

                DashBoard_pendingScheduledTestStartButton.Visible = false;
                DashBoard_pendingSelfTestStartButton.Visible = false;

                DashBoard_PendingTest_RecommandIDHiddenField.Value = pendingTest[0].TestRecommandID;

                if (!Utility.IsNullOrEmpty(pendingTest[0].TestRecommandTime))
                    DashBoard_PendingTest_RecommandTimeHiddenField.Value = Convert.ToString(pendingTest[0].TestRecommandTime);
                DashBoard_PendingTest_totalQuestionsHiddenField.Value = pendingTest[0].TotalQuestions.ToString();
                DashBoard_PendingTest_TestIDHiddenField.Value = pendingTest[0].TestID;
                DashBoard_PendingTest_CandidateSessionKeyHiddenField.Value = pendingTest[0].CandidateTestSessionID;
                DashBoard_PendingTest_TestSessionIDHiddenField.Value = pendingTest[0].TestSessionID;

                DashBoard_PendingTest_StatusHiddenField.Value = pendingTest[0].TestSchduled;
                DashBoard_PendingTest_AttemptIDHiddenField.Value = pendingTest[0].AttemptID.ToString();

                if (pendingTest[0].ExpiryDate != Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    bool isExpired = false;
                    if (!Utility.IsNullOrEmpty(pendingTest[0].ExpiryDate))
                    {
                        if (pendingTest[0].ExpiryDate < DateTime.Now)
                            isExpired = true;
                    }

                    if (pendingTest[0].IsScheduled && !isExpired)
                        DashBoard_pendingScheduledTestStartButton.Visible = true;
                    else if (!pendingTest[0].IsScheduled && !isExpired)
                        DashBoard_pendingSelfTestStartButton.Visible = true;
                }
                else
                {
                    if (pendingTest[0].IsScheduled)
                        DashBoard_pendingScheduledTestStartButton.Visible = true;
                    else
                        DashBoard_pendingSelfTestStartButton.Visible = true;
                }
            }
            else
            {
                PendingTest_Div.Style["display"] = "none";
                PendingTest_emptyDiv.Style["display"] = "block";
            }
            #endregion

            #region Completed Interviews
            List<CandidateInterviewSessionDetail> completedInterview =
                candidateSummary.CompletedInterview;

            if (completedInterview != null)
            {
                CompletedInterview_Div.Style["display"] = "block";
                CompletedInterview_emptyDiv.Style["display"] = "none";

                if (!Utility.IsNullOrEmpty(completedInterview[0].InterviewTestName))
                {
                    DashBoard_CompletedInterview_NameLabel.Visible = true;
                    DashBoard_CompletedInterview_NameLabel.Text = completedInterview[0].InterviewTestName;
                }
                else
                    DashBoard_CompletedInterview_NameLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(completedInterview[0].InterviewTestDescription))
                {
                    DashBoard_CompletedInterview_DescLabel.Visible = true;
                    DashBoard_CompletedInterview_DescLabel.Text = completedInterview[0].InterviewTestDescription.ToString().Length > 130 ?
                        completedInterview[0].InterviewTestDescription.ToString().Substring(0, 130) + "..." :
                        completedInterview[0].InterviewTestDescription.ToString();
                }
                else
                    DashBoard_CompletedInterview_DescLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(completedInterview[0].DateCompleted))
                {
                    DashBoard_CompletedInterview_ExpiryLabel.Text = "Completed On  : ";
                    DashBoard_CompletedInterview_ExpiryDateLabel.Visible = true;
                    DashBoard_CompletedInterview_ExpiryDateLabel.Text = DateFormat(completedInterview[0].DateCompleted);
                }
                else
                    DashBoard_CompletedInterview_ExpiryDateLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(completedInterview[0].CandidateInterviewSessionID))
                    DashBoard_CompletedInterview_CandidateSessionIDHiddenField.Value = completedInterview[0].CandidateInterviewSessionID;

                if (!Utility.IsNullOrEmpty(completedInterview[0].AttemptID))
                    DashBoard_CompletedInterview_AttemptIDHiddenField.Value = completedInterview[0].AttemptID.ToString();
            }
            else
            {
                CompletedInterview_Div.Style["display"] = "none";
                CompletedInterview_emptyDiv.Style["display"] = "block";
            }
            #endregion

            #region "Getting Last Pending Interview"

            List<CandidateInterviewSessionDetail> pendingInterview
                = candidateSummary.PendingInterview;

            if (pendingInterview != null)
            {
                PendingInterview_Div.Style["display"] = "block";
                PendingInterview_emptyDiv.Style["display"] = "none";

                if (!Utility.IsNullOrEmpty(pendingInterview[0].InterviewTestName))
                {
                    DashBoard_PendingInterview_NameLabel.Visible = true;
                    DashBoard_PendingInterview_NameLabel.Text =
                        pendingInterview[0].InterviewTestName;
                }
                else
                    DashBoard_PendingInterview_NameLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(pendingInterview[0].InterviewTestDescription))
                {
                    DashBoard_PendingInterview_DescLabel.Visible = true;
                    DashBoard_PendingInterview_DescLabel.Text = pendingInterview[0].InterviewTestDescription.ToString().Length > 130 ?
                        pendingInterview[0].InterviewTestDescription.ToString().Substring(0, 130) + "..." :
                        pendingInterview[0].InterviewTestDescription.ToString();
                }
                else
                    DashBoard_PendingInterview_DescLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(pendingInterview[0].CreatedDate))
                {
                    DashBoard_PendingInterview_CreatedDateLabel.Visible = true;
                    DashBoard_PendingInterview_CreatedDateLabel.Text =
                        DateFormat(pendingInterview[0].CreatedDate);
                }
                else
                    DashBoard_PendingInterview_CreatedDateLabel.Visible = false;

                if (!Utility.IsNullOrEmpty(pendingInterview[0].ExpiryDate))
                {
                    DashBoard_PendingInterview_ExpiryDateLabel.Visible = true;
                    DashBoard_PendingInterview_ExpiryDateLabel.Text =
                    DateFormat(pendingInterview[0].ExpiryDate);
                }
                else
                    DashBoard_PendingInterview_ExpiryDateLabel.Visible = false;

                DashBoard_PendingInterview_StartButton.Visible = true;
                PendingInterview_CandidateSessionIDHiddenField.Value = pendingInterview[0].CandidateInterviewSessionID;
                PendingInterview_AttemptIDHiddenField.Value = pendingInterview[0].AttemptID.ToString();

                bool isExpired = false;
                if (!Utility.IsNullOrEmpty(pendingInterview[0].ExpiryDate))
                {
                    if (pendingInterview[0].ExpiryDate < DateTime.Now)
                        isExpired = true;
                }

                // Show/hide visibility.
                DashBoard_PendingInterview_IntroImageButton.Visible = !isExpired;
                DashBoard_PendingInterview_RemiderImageButton.Visible = !isExpired;
                DashBoard_PendingInterview_StartButton.Visible = !isExpired;
            }
            else
            {
                PendingInterview_Div.Style["display"] = "none";
                PendingInterview_emptyDiv.Style["display"] = "block";
            }
            #endregion
        }

        private void LoadCandidateOnlineInterview(int candidateId)
        {
            int totalRecords = 0;

            SetSearchInterviewParam(candidateId);

            List<OnlineCandidateSessionDetail> onlineInterviewCandidateList =
                new OnlineInterviewAssessorBLManager().
                GetOnlineInterviews(Session["INTERVIEW_SEARCH_CRITERIA"] as InterviewSearchCriteria, out totalRecords);
            
            SetOnlineInterviewControlVisibility(false);

            if (totalRecords == 0) return;

            SetOnlineInterviewControlVisibility(true);
            DashBoard_pendingOnlineInterview_chatRoomIdHiddenField.Value = onlineInterviewCandidateList[0].ChatRoomName;
            DashBoard_pendingOnlineInterview_NameLabel.Text = onlineInterviewCandidateList[0].InterviewName;
            DashBoard_pendingOnlineInterview_DescLabel.Text = onlineInterviewCandidateList[0].InterviewDesc;
            DashBoard_pendingOnlineInterview_ScheduledValueLabel.Text = base.GetDateFormat(onlineInterviewCandidateList[0].InterviewDate);
            DashBoard_pendingOnlineInterview_ExpiredValueLabel.Text = base.GetDateFormat(onlineInterviewCandidateList[0].InterviewDate);
            
            DashBoard_pendingOnlineInterview_StartButton.Visible = false;
            DashBoard_pendingOnlineInterview_IndroImageButton.Visible = false;

            if (onlineInterviewCandidateList[0].InterviewDate  == DateTime.Today) 
            {
                DashBoard_pendingOnlineInterview_StartButton.Visible = true ;
                DashBoard_pendingOnlineInterview_IndroImageButton.Visible = true ;
            }
        }

        private void SetOnlineInterviewControlVisibility(bool flag)
        {
            DashBoard_pendingOnlineInterview_NameLabel.Visible = flag;
            DashBoard_pendingOnlineInterview_DescLabel.Visible = flag;
            DashBoard_pendingOnlineInterview_IndroImageButton.Visible = flag;
            DashBoard_pendingOnlineInterview_SchduledDateTitleLabel.Visible = flag;
            DashBoard_pendingOnlineInterview_ScheduledValueLabel.Visible = flag;
            DashBoard_pendingOnlineInterview_ExpiredTitleLabel.Visible = flag;
            DashBoard_pendingOnlineInterview_ExpiredValueLabel.Visible = flag;
            DashBoard_pendingOnlineInterview_MoreLinkButton.Visible = flag;
            DashBoard_pendingOnlineInterview_StartButton.Visible = flag;
        }

        private void SetSearchInterviewParam(int candidateId)
        {
            Session["INTERVIEW_SEARCH_CRITERIA"] = null;

            InterviewSearchCriteria interviewSearch = new InterviewSearchCriteria();

            interviewSearch.InterviewSessionKey = null;
            interviewSearch.InterviewSessionStatus = "SESS_SCHD";
            interviewSearch.Keyword = null;
            interviewSearch.SearchTenantID = 0;
            interviewSearch.CurrentPage = 1;
            interviewSearch.PageSize = 1;
            interviewSearch.SortExpression = "ONLINE_INTERVIEW_DATE";
            interviewSearch.SortDirection = SortType.Descending;
            interviewSearch.CandidateId = candidateId;

            Session["INTERVIEW_SEARCH_CRITERIA"] = interviewSearch;
        }
        protected void ReminderImageButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string commandArg = e.CommandArgument.ToString().Trim();
                switch (commandArg)
                {
                    case "PendingTest_TestReminder":
                        Response.Redirect("~/Popup/TestReminder.aspx" +
                        "?activitytype=TST&candidatesessionid= " +
                       DashBoard_PendingTest_CandidateSessionKeyHiddenField.Value.ToString() +
                        "&attemptid=" + DashBoard_PendingTest_AttemptIDHiddenField.Value.ToString());
                        break;
                    case "PendingInterview_IntReminder":
                        Response.Redirect("~/Popup/TestReminder.aspx" +
                        "?activitytype=INT&candidatesessionid=" +
                        PendingInterview_CandidateSessionIDHiddenField.Value.ToString() +
                        "&attemptid=" + PendingInterview_AttemptIDHiddenField.Value.ToString());
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        protected void Introduction_Command(object src, CommandEventArgs e)
        {
            try
            {
                string commandArg = e.CommandArgument.ToString();
                string candidateSessionID = null;
                string attemptID = null;
                string testKey = null;

                switch (commandArg)
                {
                    case "PendingInterview_Introduction":
                        candidateSessionID = PendingInterview_CandidateSessionIDHiddenField.Value;
                        attemptID = PendingInterview_AttemptIDHiddenField.Value;
                        IntroductionPagePageRedirect(candidateSessionID, attemptID, "CANDIDATE_INTERVIEW");
                        break;
                    case "MyTest_Introduction":
                        /*candidateSessionID = MyTest_CandidateSession_TestKeyHiddenField.Value;
                        attemptID=MyTest_CandidateSession_AttemptIDHiddenField.Value;
                        IntroductionPagePageRedirect(candidateSessionID, attemptID, "CANDIDATE_TEST");*/
                        break;
                    case "CompletedTest_ViewResults":
                        candidateSessionID = DashBoard_CompletedTest_SessionIDHiddenField.Value;
                        attemptID = DashBoard_CompletedTest_AttemptIDHiddenField.Value;
                        testKey = DashBoard_CompletedTest_TestKeyHiddenField.Value;
                        ResultPagePageRedirect(candidateSessionID, attemptID, testKey, "CANDIDATE_TEST");
                        break;
                    case "PendingTest_Introduction":
                        candidateSessionID = DashBoard_PendingTest_CandidateSessionKeyHiddenField.Value;
                        attemptID = DashBoard_PendingTest_AttemptIDHiddenField.Value;
                        IntroductionPagePageRedirect(candidateSessionID, attemptID, "CANDIDATE_TEST");
                        break;
                    case "PendingOnlineInterview_Introduction":
                        OnlineInterviewStarting();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }
        private void ResultPagePageRedirect(string candidateSessionID, string attemptID, string testKey, string type)
        {
            if (candidateSessionID == null || candidateSessionID == "")
                return;
            if (attemptID == null || attemptID == "")
                return;

            if (type == "CANDIDATE_TEST")
            {
                // Redirect to test introduction page.
                Response.Redirect("~/TestCenter/CandidateTestResult.aspx" +
                    "?testkey=" + testKey +
                    "&candidatesessionid=" + candidateSessionID +
                    "&attemptid=" + attemptID +
                    "&mode=SHARE" +
                    "&parentpage=CAND_HOME", false);
            }
        }

        private void IntroductionPagePageRedirect(string candidateSessionID, string attemptID, string type)
        {
            if (candidateSessionID == null || candidateSessionID == "") return;
            if (attemptID == null || attemptID == "") return;

            if (type == "CANDIDATE_INTERVIEW")
            {
                // Redirect to test introduction page.
                Response.Redirect("~/InterviewCenter/InterviewIntroduction.aspx" +
                   "?candidatesessionid=" + candidateSessionID +
                   "&attemptid=" + attemptID +
                   "&parentpage=ACT_HOME", false);
            }
            else
                // Redirect to test introduction page.
                Response.Redirect("~/TestCenter/TestIntroduction.aspx" +
                   "?candidatesessionid=" + candidateSessionID +
                   "&attemptid=" + attemptID +
                   "&parentpage=CAND_HOME", false);
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start test confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the test instructions page.
        /// </remarks>
        protected void CandidateHome_startTestButton_OkClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/TestCenter/TestInstructions.aspx" +
                    "?candidatesessionid=" + DashBoard_PendingTest_CandidateSessionKeyHiddenField.Value +
                    "&attemptid=" + DashBoard_PendingTest_AttemptIDHiddenField.Value +
                    "&parentpage=CAND_HOME", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called when the ok button is clicked
        /// in the start interview confirmation window.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will launch the interview instructions page.
        /// </remarks>
        protected void PendingInterviews_startInterviewButton_OkClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/InterviewCenter/InterviewInstructions.aspx" +
                    "?candidatesessionid=" + PendingInterview_CandidateSessionIDHiddenField.Value +
                    "&attemptid=" + PendingInterview_AttemptIDHiddenField.Value +
                    "&parentpage=MY_INT", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        private void LoadExpectedQuestions(ref DataTable dtSearchCriteria, int NoofQuestions, out bool ZeroExpectedQuestion)
        {
            ZeroExpectedQuestion = false;
            int SearchSegements = dtSearchCriteria.Rows.Count;
            int QuestionsAdded = 0;
            int ExpectedQuestions = 0;
            int LoopI = 0;
            for (LoopI = 0; LoopI < SearchSegements; LoopI++)
            {
                QuestionsAdded = Convert.ToInt32((Convert.ToDecimal(dtSearchCriteria.Rows[LoopI]["Weightage"])
                    / 100) * NoofQuestions);
                dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] = QuestionsAdded;
                ExpectedQuestions += QuestionsAdded;
            }
            if (ExpectedQuestions > NoofQuestions)
            {
                int QuestionsToAdd = ExpectedQuestions - NoofQuestions;
                for (LoopI = SearchSegements - 1; LoopI >= 0; LoopI--)
                {
                    if (Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd <= 0)
                        continue;
                    dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["ExpectedQuestions"]) - QuestionsToAdd;
                    break;
                }
            }
            else if (ExpectedQuestions < NoofQuestions)
                dtSearchCriteria.Rows[LoopI - 1]["ExpectedQuestions"] = QuestionsAdded +
                    (NoofQuestions - ExpectedQuestions);
            if (dtSearchCriteria.Select("ExpectedQuestions <= 0").Length > 0)
            {
                ZeroExpectedQuestion = true;
            }
        }

        /// <summary>
        /// This method allocates weightages to the segements
        /// and ensures that the total weightage is 100% or not.
        /// </summary>
        /// <param name="dtSearchCriteria">test segment grid datatable</param>
        /// <param name="NoofQuestions">Total number of questions for the test</param>
        /// <param name="TotalWeightage">Total weightage allocated by this method 
        /// (Note:- it should be out parametarized)</param>
        private void LoadWeightages(ref DataTable dtSearchCriteria, int NoofQuestions, out int TotalWeightage)
        {
            DataRow[] drWeightage = null;
            int GivenWeightage = 0;
            int SearchSegements = 0;
            int WeightagePercentage = 0;
            int LoopI = 0;
            int ToAddExtraWeightage = 0;
            try
            {
                SearchSegements = dtSearchCriteria.Rows.Count;
                drWeightage = dtSearchCriteria.Select("Weightage > 0");
                for (LoopI = 1; LoopI <= drWeightage.Length; LoopI++)
                    GivenWeightage += Convert.ToInt32(drWeightage[LoopI - 1]["Weightage"]);

                if (GivenWeightage == 0)
                    WeightagePercentage = 100 / SearchSegements;
                else if (GivenWeightage < 100)
                {
                    if (SearchSegements == drWeightage.Length)
                        dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"] = 100 -
                            (GivenWeightage - Convert.ToInt32(dtSearchCriteria.Rows[SearchSegements - 1]["Weightage"]));
                    else
                        WeightagePercentage = (100 - GivenWeightage) / (SearchSegements - drWeightage.Length);
                }
                ToAddExtraWeightage = 100 - ((WeightagePercentage * (SearchSegements - (LoopI - 1))) + GivenWeightage);
                for (LoopI = 0; LoopI < SearchSegements; LoopI++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[LoopI]["Weightage"]))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] = WeightagePercentage;
                    if ((ToAddExtraWeightage > 0) && (WeightagePercentage != 0) && (LoopI == SearchSegements - 1))
                        dtSearchCriteria.Rows[LoopI]["Weightage"] =
                            Convert.ToInt32(dtSearchCriteria.Rows[LoopI]["Weightage"]) + ToAddExtraWeightage;
                    dtSearchCriteria.Rows[LoopI]["PickedQuestions"] = 0;
                    dtSearchCriteria.Rows[LoopI]["TotalRecordsinDB"] = 0;
                    dtSearchCriteria.Rows[LoopI]["QuestionsDifference"] = 0;
                }
                TotalWeightage = 100;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(drWeightage)) drWeightage = null;
            }
        }

        /// <summary>
        /// Method that loads the questions for the given criteria.
        /// </summary>
        /// <param name="noOfQuestions">
        /// A <see cref="int"/> that holds the number of questions.
        /// </param>
        /// <param name="criteriaTable">
        /// A <see cref="DataTable"/> that holds the search criteria.
        /// </param>
        private List<QuestionDetail> LoadQuestions(int noOfQuestions, ref DataTable criteriaTable)
        {
            int TotalWeightage = 0;
            bool ZeroExpectedQuestion = false;
            LoadWeightages(ref criteriaTable, noOfQuestions, out TotalWeightage);
            if (TotalWeightage > 100)
                return null;

            LoadExpectedQuestions(ref criteriaTable, noOfQuestions, out ZeroExpectedQuestion);
            if (ZeroExpectedQuestion)
                return null;

            List<QuestionDetail> questionDetails =
                new QuestionBLManager().
                GetRecommendedAutomatedQuestions(QuestionType.MultipleChoice,
                GetQuestionSearchCriteria(ref criteriaTable),
                noOfQuestions, ref criteriaTable, 1, base.userID);

            ViewState["RECOMMENDED_QUESTIONS"] = questionDetails;

            if (questionDetails == null)
                return null;

            return questionDetails;

            /*TestStatistics testStatistics = GetTestStatistics(questionDetails); */
        }
        private List<TestSearchCriteria> GetQuestionSearchCriteria(ref DataTable dtSearchCriteria)
        {
            List<TestSearchCriteria> testSearchCriterias = null;
            TestSearchCriteria testSearchCriteria = null;
            Subject subject = null;
            try
            {
                for (int i = 0; i < dtSearchCriteria.Rows.Count; i++)
                {
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria))
                        testSearchCriteria = new TestSearchCriteria();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Cat_sub_id"]))
                    {
                        if (Forte.HCM.Support.Utility.IsNullOrEmpty(subject))
                            subject = new Subject();
                        subject.SubjectID = Convert.ToInt32(dtSearchCriteria.Rows[i]["Cat_sub_id"]);
                        testSearchCriteria.Subjects = new List<Subject>();
                        testSearchCriteria.Subjects.Add(subject);
                    }
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["TestArea"]))
                        testSearchCriteria.TestAreasID = dtSearchCriteria.Rows[i]["TestArea"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Complexity"]))
                        testSearchCriteria.Complexity = dtSearchCriteria.Rows[i]["Complexity"].ToString();
                    if (!Forte.HCM.Support.Utility.IsNullOrEmpty(dtSearchCriteria.Rows[i]["Keyword"]))
                        testSearchCriteria.Keyword = dtSearchCriteria.Rows[i]["Keyword"].ToString();
                    testSearchCriteria.NoOfQuestions = Convert.ToInt16(ViewState["NO_OF_QUESTIONS"]);
                    if (Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias))
                        testSearchCriterias = new List<TestSearchCriteria>();
                    testSearchCriterias.Add(testSearchCriteria);
                    testSearchCriteria = null;
                    subject = null;
                }
                return testSearchCriterias;
            }
            finally
            {
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriteria)) testSearchCriteria = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(testSearchCriterias)) testSearchCriterias = null;
                if (!Forte.HCM.Support.Utility.IsNullOrEmpty(subject)) subject = null;

            }
        }
        /// <summary>
        /// Method that construct and returns the test statistics.
        /// </summary>
        /// <param name="questions">
        /// A list of <see cref="QuestionDetail"/> that holds the question 
        /// details.
        /// </param>
        /// <returns>
        /// A <see cref="TestStatistics"/> that holds the test statistics.
        /// </returns>
        private TestStatistics GetTestStatistics(List<QuestionDetail> questions)
        {
            StringBuilder categoryNames = new StringBuilder();
            AutomatedTestSummaryGrid automatedTestSummaryGrid = null;
            TestStatistics testStatistics = null;
            List<AutomatedTestSummaryGrid> automatedTestSummaryGridOrderdByQuestion = null;
            int totalCategory = 0;

            foreach (QuestionDetail question in questions)
            {
                if (categoryNames.ToString().IndexOf("CAT: " + question.CategoryName.Trim()) >= 0)
                    continue;

                totalCategory = questions.FindAll(
                    p => " CAT: " + p.CategoryName.Trim() == " CAT: " + question.CategoryName.Trim()).Count;

                categoryNames.Append(" CAT: " + question.CategoryName.Trim());

                if (Forte.HCM.Support.Utility.IsNullOrEmpty(automatedTestSummaryGrid))
                    automatedTestSummaryGrid = new AutomatedTestSummaryGrid();

                automatedTestSummaryGrid.CategoryName = question.CategoryName;
                automatedTestSummaryGrid.TestAreaName = question.TestAreaName;
                automatedTestSummaryGrid.Complexity = question.Complexity;
                automatedTestSummaryGrid.NoofQuestionsInCategory = totalCategory;
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics))
                    testStatistics = new TestStatistics();
                if (Forte.HCM.Support.Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
                    testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                testStatistics.AutomatedTestSummaryGrid.Add(automatedTestSummaryGrid);
                automatedTestSummaryGrid = null;
                totalCategory = 0;
            }

            if (!Utility.IsNullOrEmpty(testStatistics.AutomatedTestSummaryGrid))
            {
                automatedTestSummaryGridOrderdByQuestion = testStatistics.AutomatedTestSummaryGrid.OrderByDescending(p => p.NoofQuestionsInCategory).ToList();
                testStatistics.AutomatedTestSummaryGrid = null;
                testStatistics.AutomatedTestSummaryGrid = new List<AutomatedTestSummaryGrid>();
                testStatistics.AutomatedTestSummaryGrid = automatedTestSummaryGridOrderdByQuestion;

                ViewState["QuestionDetails"] = automatedTestSummaryGridOrderdByQuestion;
            }

            testStatistics.NoOfQuestions = questions.Count;

            return testStatistics;
        }

        /// <summary>
        /// Method that construct and returns the test segment criteria table.
        /// </summary>
        /// <param name="criteriaTable">
        /// A <see cref="DataTable"> that holds the criteria table.
        /// </param>
        private DataTable GetTestSegmentCriteriaTable()
        {
            DataTable criteriaTable = new DataTable();
            criteriaTable.Columns.Add("SNO", typeof(int));
            criteriaTable.Columns.Add("Cat_sub_id", typeof(string));
            criteriaTable.Columns.Add("Complexity", typeof(string));
            criteriaTable.Columns.Add("TestArea", typeof(string));
            criteriaTable.Columns.Add("Category", typeof(string));
            criteriaTable.Columns.Add("Subject", typeof(string));
            criteriaTable.Columns.Add("Weightage", typeof(int));
            criteriaTable.Columns.Add("Keyword", typeof(string));
            criteriaTable.Columns.Add("ExpectedQuestions", typeof(int));
            criteriaTable.Columns.Add("PickedQuestions", typeof(int));
            criteriaTable.Columns.Add("TotalRecordsinDB", typeof(int));
            criteriaTable.Columns.Add("QuestionsDifference", typeof(int));
            criteriaTable.Columns.Add("Remarks", typeof(string));


            return criteriaTable;
        }

        /// <summary>
        /// Method that constructs and returns the criteria table for the given
        /// recommended test ID.
        /// </summary>
        /// <param name="recommendedTestID">
        /// A <see cref="int"/> that holds the recommended test ID.
        /// </param>
        /// <returns>
        /// A <see cref="DataTable"/> that holds the criteria table.
        /// </returns>
        private DataTable GetCriteriaTable(int recommendedTestID)
        {
            // Get search criteria for the given recommended test ID.
            List<TestSearchCriteria> testSearchCriterias = new TestBLManager().
                GetSearchCriteria(recommendedTestID);

            // Check if criteria present.
            if (testSearchCriterias == null || testSearchCriterias.Count == 0)
                return null;

            // Construct test segment criteria table.
            DataTable criteriaTable = GetTestSegmentCriteriaTable();

            DataRow drNewRow = null;

            int SNo = 1;

            // Fill the segment criteria rows.
            foreach (TestSearchCriteria criteria in testSearchCriterias)
            {
                drNewRow = criteriaTable.NewRow();
                drNewRow["SNO"] = SNo++.ToString();
                drNewRow["Cat_sub_id"] = criteria.CategoriesID;
                drNewRow["Weightage"] = criteria.Weightage; ;
                drNewRow["Keyword"] = criteria.Keyword;
                drNewRow["TestArea"] = criteria.TestAreasID;
                drNewRow["Complexity"] = criteria.Complexity;
                criteriaTable.Rows.Add(drNewRow);
                drNewRow = null;
            }
            return criteriaTable;
        }
        protected void DashBoard_PendingTestStart_Click(object sender, EventArgs e)
        {
            string testKey = string.Empty;

            //testKey = MyTest_CandidateSession_TestKeyHiddenField.Value;
            // Clear fields and data.
            ViewState["RECOMMENDED_QUESTIONS"] = null;
            ViewState["NO_OF_QUESTIONS"] = null;
            ViewState["TEST_RECOMMENDED_ID"] = null;
            ViewState["CANDIDATE_SESSION_ID"] = null;

            if (DashBoard_PendingTest_RecommandIDHiddenField.Value == null
                || DashBoard_PendingTest_RecommandIDHiddenField.Value == "0")
                return;

            List<QuestionDetail> questions = null;

            if (testKey == null || testKey == "")
            {
                // Check if questions present to save.
                if (Utility.IsNullOrEmpty(ViewState["RECOMMENDED_QUESTIONS"]))
                {
                    DataTable criteriaTable = null;
                    if (DashBoard_PendingTest_RecommandIDHiddenField.Value == null) return;
                    criteriaTable = GetCriteriaTable(Convert.ToInt32(DashBoard_PendingTest_RecommandIDHiddenField.Value));

                    int totalQuestions = Convert.ToInt32(DashBoard_PendingTest_totalQuestionsHiddenField.Value);
                    // Load questions.
                    questions = LoadQuestions(totalQuestions, ref criteriaTable);

                }

                // Check if questions present to save.
                if (Utility.IsNullOrEmpty(ViewState["RECOMMENDED_QUESTIONS"]))
                    return;

                // Check if questions found.
                if (questions == null || questions.Count == 0)
                {
                    base.ShowMessage(DashBoard_errorMessageLabel, "No test/questions generated to proceed.");
                    return;
                }
                // Save test for the list of questions. 
                bool saved = SaveTest(questions, out testKey);
            }

            GenerateSchedule(testKey);
        }

        /// <summary>
        /// Method that saves the question set as a new test.
        /// </summary>
        /// <param name="questions">
        /// A list of <see cref="QuestionDetail"/> that holds the questions.
        /// </param>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key as an output
        /// parameter.
        /// </param>
        /// <remarks>
        /// A <see cref="bool"/> that holds the status. True represents success
        /// and false represents failure.
        /// </remarks>
        private bool SaveTest(List<QuestionDetail> questions, out string testKey)
        {
            // Assign default value to test key.
            testKey = string.Empty;

            List<decimal> creditEarned = new List<decimal>();
            List<decimal> timeTaken = new List<decimal>();

            // Construct sum of credits earned and average time.
            foreach (QuestionDetail question in questions)
            {
                creditEarned.Add(question.CreditsEarned);
                timeTaken.Add(question.AverageTimeTaken);
            }

            // Construct test detail object.
            TestDetail testDetail = new TestDetail();

            string testName = DashBoard_PendingTest_NameLabel.Text.Trim();
            testDetail.Name = testName;
            testDetail.Description = testName;
            testDetail.Questions = questions;
            testDetail.IsActive = true;
            testDetail.IsCertification = false;
            testDetail.CertificationId = 0;
            testDetail.CreatedBy = userID;
            testDetail.IsDeleted = false;
            testDetail.ModifiedBy = userID;
            testDetail.NoOfQuestions = questions.Count;
            testDetail.SystemRecommendedTime =
                Convert.ToInt32(timeTaken.Average()) * questions.Count;
            testDetail.RecommendedCompletionTime = testDetail.SystemRecommendedTime;

            testDetail.TestAuthorID = userID;
            testDetail.TestCost = Convert.ToDecimal(creditEarned.Average());
            testDetail.TestCreationDate = DateTime.Now;
            testDetail.TestStatus = TestStatus.Active;
            testDetail.TestType = TestType.Genenal;
            testDetail.TestMode = "AUTO";
            testDetail.TestKey = "";

            // Get complexity.
            string complexity = new ControlUtility().GetComplexity
                (Constants.AttributeTypes.COMPLEXITY,
                questions).AttributeID.ToString();

            // Save the test and retrieve the test key.
            testKey = new TestBLManager().SaveTest(testDetail, userID, complexity);

            return true;
        }

        private void GenerateSchedule(string testKey)
        {
            // Create test session, schedule candidate and proceed 
            // to test instructions page.
            string candidateSessionID = string.Empty;
            bool scheduled = ScheduleCandidate(testKey, out candidateSessionID);

            if (!Utility.IsNullOrEmpty(DashBoard_pendingTests_testRecommendID.Value))
            {
                int testRecommendationID = 0;

                int.TryParse(DashBoard_pendingTests_testRecommendID.Value, out testRecommendationID);

                if (testRecommendationID != 0)
                {
                    // Update the test key & scheduled status.
                    new CandidateBLManager().UpdateCandidateTestRecommendationStatus
                        (base.userID, testRecommendationID, testKey,
                        Constants.CandidateSelfAdminTestStatus.SCHEDULED);
                }
            }

            if (scheduled == false)
                return;

            // Check if start test confirm message needs to be shown. If
            // configureds as true then show the confirmation message, else
            // navigates to test instructions page.
            if (ConfigurationManager.AppSettings["CONFIRM_SELF_ADMIN_START_TEST"] != null
                && Convert.ToBoolean(ConfigurationManager.AppSettings["CONFIRM_SELF_ADMIN_START_TEST"]) == false)
            {
                // Construct the url for the test instructions page.
                string url = "~/TestCenter/TestInstructions.aspx" +
                    "?candidatesessionid=" + candidateSessionID +
                    "&attemptid=" + Constants.CandidateSelfAdminTestConstants.ATTEMPT_ID +
                    "&parentpage=" + Constants.ParentPage.CANDIDATE_SELF_ADMIN_TEST;

                // Navigates to the test instructions page to proceed to take
                // the generated test.
                Response.Redirect(url, false);
            }
            else
            {
                // Keep the candidate session ID in view state.
                ViewState["CANDIDATE_SESSION_ID"] = candidateSessionID;

                // Show the start test confirmation window.
                //CandidateSearch_startTestPopupExtenderControl.Show();
            }
        }

        /// <summary>
        /// Method that creates the test session and schedules the candidate
        /// for the test.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <param name="candidateSessionID">
        /// A <see cref="string"/> that holds the candidate session ID as an
        /// output parameter.
        /// </param>
        /// <remarks>
        /// A <see cref="bool"/> that holds the status. True represents success
        /// and false represents failure.
        /// </remarks>
        private bool ScheduleCandidate(string testKey, out string candidateSessionID)
        {
            // Assign default value to test key.
            candidateSessionID = string.Empty;

            // Construct test session detail.
            TestSessionDetail sessionDetail = GetTestSessionDetail(testKey);

            if (sessionDetail == null)
                return false;

            // Construct test schedule detail.
            TestScheduleDetail scheduleDetail = new TestScheduleDetail();
            scheduleDetail.CandidateID = base.userID.ToString();
            scheduleDetail.AttemptID = Constants.CandidateSelfAdminTestConstants.ATTEMPT_ID;
            scheduleDetail.EmailId = ((UserDetail)Session["USER_DETAIL"]).Email;
            scheduleDetail.ExpiryDate = DateTime.Now.AddDays(base.candidateExpiryDays);

            // Schedule the candidate.
            new TestBLManager().SaveTestSessionScheduleCandidate(sessionDetail, scheduleDetail,
                base.userID, out candidateSessionID);

            return true;
        }

        /// <summary>
        /// Method that constructs and returns the test session detail.
        /// </summary>
        /// <param name="testKey">
        /// A <see cref="string"/> that holds the test key.
        /// </param>
        /// <returns>
        /// A <see cref="TestSessionDetail"/> that holds the test session detail.
        /// </returns>
        private TestSessionDetail GetTestSessionDetail(string testKey)
        {
            // Get test detail.
            TestDetail testDetail = new TestBLManager().GetTestAndCertificateDetail(testKey);

            if (testDetail == null)
                return null;

            // Construct test session detail object.
            TestSessionDetail sessionDetail = new TestSessionDetail();

            sessionDetail.TestID = testDetail.TestKey;
            sessionDetail.TestName = testDetail.Name;

            // Set number of candidate session (session count)
            sessionDetail.NumberOfCandidateSessions = 1;

            // Set total credits limit
            sessionDetail.TotalCredit = testDetail.TestCost;

            // Set time limit
            sessionDetail.TimeLimit = Convert.ToInt32(DashBoard_PendingTest_RecommandTimeHiddenField.Value);  //Convert.ToInt32(ViewState["RECOMMENDED_TIME"]);

            sessionDetail.ClientRequestID = "0";

            // Set expiry date
            sessionDetail.ExpiryDate = DateTime.Now;

            // Set random question order status
            sessionDetail.IsRandomizeQuestionsOrdering = false;

            // Set display result status 
            sessionDetail.IsDisplayResultsToCandidate = true;

            // Set cyber proctoring status
            sessionDetail.IsCyberProctoringEnabled = false;

            // Set created by
            sessionDetail.CreatedBy = base.userID;

            // Set modified by
            sessionDetail.ModifiedBy = base.userID;

            // Set test instructions
            sessionDetail.Instructions = Constants.CandidateSelfAdminTestConstants.TEST_INSTRUCTIONS;
            // Set session descriptions
            sessionDetail.TestSessionDesc = Constants.CandidateSelfAdminTestConstants.SESSION_DESCRIPTION;

            return sessionDetail;
        }

        #region Protected Override Methods

        protected override bool IsValidData()
        {
            bool value = true;
            return value;
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion Protected Override Methods
        protected void DashBoard_searchTestImageButton_Click(object sender, EventArgs e)
        {
            try
            {
                Session["SEARCH_TEST"] = string.Empty;
                if (Utility.IsNullOrEmpty(DashBoard_searchTestTextBox.Text.Trim()))
                {
                    base.ShowMessage(DashBoard_errorMessageLabel, "Enter Skill");
                    DashBoard_searchTestTextBox.Focus();
                    return;
                }
                Session["SEARCH_TEST"] = DashBoard_searchTestTextBox.Text.Trim();
                Response.Redirect("~/CandidateAdmin/CandidateSearchTest.aspx", false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Event to redirect to resume preview page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DashBoard_PreviewButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ResumeRepository/MyResume.aspx?parentpage=" +
                    Constants.ParentPage.CANDIDATE_EDIT_PROFILE, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        protected void DashBoard_MoreLinkButton_Command(object sender, CommandEventArgs e)
        {
            string commandArgs = e.CommandArgument.ToString().Trim();

            if (commandArgs == null || commandArgs == "") return;

            switch (commandArgs)
            {
                case "CompletedTest_More":
                    Response.Redirect("~/CandidateAdmin/Activities.aspx?displaytype=CT", false);
                    break;
                case "PendingTest_More":
                    Response.Redirect("~/CandidateAdmin/Activities.aspx?displaytype=PT", false);
                    break;
                case "CompletedInterview_More":
                    Response.Redirect("~/CandidateAdmin/Activities.aspx?displaytype=CI", false);
                    break;
                case "PendingInterview_More":
                    Response.Redirect("~/CandidateAdmin/Activities.aspx?displaytype=PI", false);
                    break;
                case "PendingOnlineInterview_More":
                    Response.Redirect("~/CandidateAdmin/Activities.aspx?displaytype=OI", false);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Method that assigns the LinkedIn share URL.
        /// </summary>
        private void AssignLinkedInShareURL()
        {
            UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

            resultUrl = ConfigurationManager.AppSettings["CANDIDATE_TEST_RESULT_URL"];
            resultUrl += "?testkey=" + DashBoard_CompletedTest_TestKeyHiddenField.Value.ToString();
            resultUrl += "&parentpage=CAND_HOME";
            resultUrl += "&candidatesessionid=" + DashBoard_CompletedTest_SessionIDHiddenField.Value.ToString();
            resultUrl += "&attemptid=" + DashBoard_CompletedTest_AttemptIDHiddenField.Value.ToString();

            string url = string.Format("http://www.linkedin.com/shareArticle?mini=true&url={0}&title={1}&summary={2}&source=www.fortehcm.com",
                resultUrl, "Test results of " + userDetail.FullName, DashBoard_CompletedTest_NameLabel.Text);
            CandidateTestResult_shareInLinkedInHyperLink.NavigateUrl = url;
        }

        /// <summary>
        /// Method that will clear the message labels and hide it.
        /// </summary>
        private void ClearMessages()
        {
            DashBoard_errorMessageLabel.Text = string.Empty;
            DashBoard_errorMessageLabel.Visible = false;
            DashBoard_successMessageLabel.Text = string.Empty;
            DashBoard_successMessageLabel.Visible = false;
        }


        /// <summary>
        /// Handler method that will be called when the resume name link is 
        /// clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will download the resume from the repository.
        /// </remarks>
        protected void DashBoard_resumeNameLink_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.IsNullOrEmpty(DashBoard_candidateResumeID.Value) || Convert.ToInt32(DashBoard_candidateResumeID.Value) == 0)
                {
                    base.ShowMessage(DashBoard_errorMessageLabel, "Resume not found to download");
                    return;
                }

                Response.Redirect("~/Common/Download.aspx?id=" + DashBoard_candidateResumeID.Value, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Method the loads the resume status
        /// </summary>
        private void LoadResumeStatus()
        {
            // Assign default resume status.
            DashBoard_resumeStatusLabel.Text = "Upload your resume";
            DashBoard_resumeAlertImage.ToolTip = DashBoard_resumeStatusLabel.Text;

            DashBoard_resumeStatusLabel.Visible = true;
            DashBoard_resumeAlertImage.Visible = true;
            DashBoard_resumeNameLink.Visible = false;
            DashBoard_deleteResumeLink.Visible = false;
            DashBoard_previewResumeButton.Visible = false;

            // Get resume status.
            ResumeStatus resumeStatus = new CandidateBLManager().GetCandidateResumeStatus(base.userID);

            if (resumeStatus == null)
                return;

            // Compose and assign the resume status message.
            DashBoard_resumeStatusLabel.Text = base.GetResumeStatusMessage(resumeStatus);
            DashBoard_resumeAlertImage.ToolTip = DashBoard_resumeStatusLabel.Text;

            if (DashBoard_resumeStatusLabel.Text.Trim().Length == 0)
            {
                DashBoard_resumeStatusLabel.Visible = false;
                DashBoard_resumeAlertImage.Visible = false;
            }

            DashBoard_candidateResumeID.Value = resumeStatus.CandidateResumeID.ToString();

            DashBoard_resumeNameLink.Text = resumeStatus.ResumeName;

            // Hide/show buttons & change button text.
            if (resumeStatus.Uploaded)
            {
                DashBoard_resumeNameLink.Visible = true;
                DashBoard_deleteResumeLink.Visible = true;
                DashBoard_previewResumeButton.Visible = true;
            }

            if (resumeStatus.ShowApproveButton)
            {
                DashBoard_previewResumeButton.Text = "Review & Approve";
            }
            else
            {
                DashBoard_previewResumeButton.Text = "View Profile";
            }
        }

        private void OnlineInterviewStarting()
        {
            if (Session["USER_DETAIL"] == null) return;

            UserDetail userDetail = Session["USER_DETAIL"] as UserDetail;

            if (userDetail == null) return;

            //Checking candidate have online interview for current date & time
            OnlineCandidateSessionDetail onlineInterviewDetail =
                new CandidateBLManager().GetOnlineInterviewDetail(userDetail.UserID, 0, DashBoard_pendingOnlineInterview_chatRoomIdHiddenField.Value);

            if (onlineInterviewDetail != null)
            {
                if (new Utility().GetOnlineInterviewAvailable(onlineInterviewDetail.InterviewDate,
                    onlineInterviewDetail.TimeSlotFrom, onlineInterviewDetail.TimeSlotTo))
                {
                    Response.Redirect(ConfigurationManager.AppSettings["ONLINE_INTERVIEW_URL"].ToString() + "?candidateid=" + onlineInterviewDetail.CandidateID.ToString().Trim()
                        + "&userrole=C" + "&userid=" + onlineInterviewDetail.CandidateID.ToString().Trim() + "&interviewkey=" + onlineInterviewDetail.CandidateSessionID
                        + "&chatroom=" + onlineInterviewDetail.ChatRoomName, false);
                    return;
                }
                else
                {
                    if (onlineInterviewDetail.InterviewDate > DateTime.Today)
                    {
                        base.ShowMessage(DashBoard_errorMessageLabel, "Interview start date on " + base.GetDateFormat(onlineInterviewDetail.InterviewDate));
                        return;
                    }
                }
            }
        }

        protected void DashBoard_pendingOnlineInterview_StartButton_Click(object sender, EventArgs e)
        {
            try 
            {
                OnlineInterviewStarting();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(DashBoard_errorMessageLabel, exp.Message);  
            }
        }
    }
}
