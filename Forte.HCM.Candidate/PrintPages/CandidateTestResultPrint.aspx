﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CandidateTestResultPrint.aspx.cs"
    Inherits="Forte.HCM.UI.PrintPages.CandidateTestResultPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Stylesheet" id="lnkStyleSheet" runat="server" />
    <link rel="Stylesheet" id="lnkSkin" runat="server" />
</head>
<body>
    <form id="TestResultPrint_frm" runat="server">
    <div id="TestResultPrint_div" runat="server">
        <table border="0" cellspacing="0" cellpadding="0" width="100%" class="table_bg" style="padding: 10px">
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td class="td_height_5" align="center">
                    <asp:Image ID="CandidateTestResult_forteHCMLogo" runat="server" ImageUrl="~/Images/logo.png"
                        Width="123" Height="69" alt="ForteHCM" title="ForteHCM" Style="padding-top: 20px" />
                </td>
            </tr>
            <tr>
                <td class="tab_body_bg">
                    <table width="100%" cellpadding="0" cellspacing="3" border="0">
                        <tr>
                            <td>
                                <asp:Label ID="CandidateTestResultPrint_candidateNameValueTextBox" runat="server"
                                    Text="" Width="60%" SkinID="sknLabelDivFieldText"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="CandidateTestResultPrint_testNameValueTextBox" runat="server" Text=""
                                    SkinID="sknLabelDivFieldText"></asp:Label>
                            </td>
                            <td><asp:Label ID="CandidateTestResultPrint_testCompletedDateValueTextBox" runat="server" Text="" 
                                    SkinID="sknLabelDivFieldText"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="td_height_5">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <div id="CandidateTestResultPrint_searchCriteriasDiv" runat="server" style="display: block;">
                                                            <table cellpadding="0" cellspacing="3" width="100%">
                                                                <tr>
                                                                    <td style="width: 50%" valign="top">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td class="grid_header_bg">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="header_text_bold">
                                                                                                Test Summary
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="panel_body_bg" style="width: 100%">
                                                                                    <div style="height: 130px; overflow: auto;">
                                                                                        <table width="100%" cellpadding="0" cellspacing="5" border="0">
                                                                                            <tr>
                                                                                                <td style="width: 40%">
                                                                                                    <asp:Label ID="CandidateTestResultPrint_noOfQuestionHeadLabel" runat="server" Text="Number Of Questions"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 20%">
                                                                                                    <asp:Label ID="CandidateTestResultPrint_noOfQuestionLabel" runat="server" Text="25"
                                                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 30%">
                                                                                                    <asp:Label ID="CandidateTestResultPrint_highScoreHeadLabel" runat="server" Text="Highest Score"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_highScoreLabel" runat="server" Text="10"
                                                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_avgTimeHeadLabel" runat="server" Text="Average Time Taken By Candidates"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_avgTimeLabel" runat="server" Text="00:01:25"
                                                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_lowScoreHeadLabel" runat="server" Text="Lowest Score"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_lowScoreLabel" runat="server" Text="8" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_scoreRangeHeadLabel" runat="server" Text="Score Range"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_scoreRangeLabel" runat="server" Text="10"
                                                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_meanScoreHeadLabel" runat="server" Text="Mean Score"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_meanScoreLabel" runat="server" Text="10"
                                                                                                        SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_scoreSDHeadLabel" runat="server" Text="Standard Deviation"
                                                                                                        SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Label ID="CandidateTestResultPrint_scoreSDLabel" runat="server" Text="10" SkinID="sknLabelFieldText"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 50%" valign="top">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td class="grid_header_bg">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="header_text_bold">
                                                                                                Categories / Subjects
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="panel_body_bg">
                                                                                    <div style="height: 130px; width: 470px; overflow: auto;">
                                                                                        <asp:GridView ID="CandidateTestResultPrint_categoryGridview" Style="vertical-align: top"
                                                                                            runat="server" AutoGenerateColumns="false">
                                                                                            <Columns>
                                                                                                <asp:BoundField HeaderText="Category" DataField="CategoryName" />
                                                                                                <asp:BoundField HeaderText="Subject" DataField="SubjectName" />
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2" colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 50%" valign="top">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="grid_header_bg">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="header_text_bold">
                                                                                                Assessment Content Distribution Amongst Categories
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="panel_body_bg">
                                                                                    <div style="height: 150px; overflow: auto;">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td align="left" valign="middle">
                                                                                                    <asp:Image ID="CandidateTestResultPrint_categoryStatisticsImage" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 50%">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="grid_header_bg">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="header_text_bold">
                                                                                                Assessment Content Distribution Amongst Subject
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="panel_body_bg">
                                                                                    <div style="height: 150px; overflow: auto;">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:Image ID="CandidateTestResultPrint_subjectStatisticsImage" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td_height_2" colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="grid_header_bg">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="header_text_bold">
                                                                                                Assessment Content Distribution Amongst Test Area
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="panel_body_bg">
                                                                                    <div style="height: 150px; overflow: auto;">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:Image ID="CandidateTestResultPrint_testAreaStatisticsImage" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 50%">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td class="grid_header_bg">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="header_text_bold">
                                                                                                Assessment Content Distribution Amongst Complexity
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="panel_body_bg">
                                                                                    <div style="height: 150px; overflow: auto;">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:Image ID="CandidateTestResultPrint_complexityStatisticsImage" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_height_5">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="4" border="0">
                                    <tr>
                                        <td colspan="2">
                                            <table width="100%" cellpadding="0" cellspacing="4" border="0">
                                                <tr>
                                                    <td style="width: 25%">
                                                        <asp:Label ID="CandidateTestResultPrint_myScoreLabel" runat="server" Text="My Score"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_myScoreLabelValue" runat="server" SkinID="sknLabelFieldText"
                                                            Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td style="width: 13%">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_noOfQuestionCandidateStatisticsHeadLabel"
                                                            runat="server" Text="Number Of Questions Attended" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_noOfQuestionLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_absoluteScoreLabel" runat="server" Text="Absolute Score"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_absoluteScoreLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 13%">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_percentageQuestionsAnsweredCorrectlyLabel"
                                                            runat="server" Text="Percentage Of Questions Answered Right" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_percentageQuestionsAnsweredCorrectlyLabelValue"
                                                            runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_relativeScoreLabel" runat="server" Text="Relative Score"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_relativeScoreLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 13%">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_noOfQuestionTimeTakenLabel" runat="server"
                                                            Text="Time Taken To Complete Test" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_noOfQuestionTimeTakenLabelValue" runat="server"
                                                            SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_percentileLabel" runat="server" Text="Percentile"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_percentileLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                    <td style="width: 13%">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_avgTimeTakenLabel" runat="server" Text="Average Time Taken Per Question"
                                                            SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CandidateTestResultPrint_avgTimeTakenLabelValue" runat="server" SkinID="sknLabelFieldText"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="grid_header_bg">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    Test Scores
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_body_bg">
                                                        <div style="height: 180px; overflow: auto;">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Image ID="CandidateTestResultPrint_testScoreImage" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 50%">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="grid_header_bg">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    Score Distribution Amongst Categories
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_body_bg">
                                                        <div style="height: 180px; overflow: auto;">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Image ID="CandidateTestResultPrint_testScoreCategoryImage" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="grid_header_bg">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    Score Distribution Amongst Subjects
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_body_bg">
                                                        <div style="height: 180px; overflow: auto;">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Image ID="CandidateTestResultPrint_testScoreSubjectImage" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="grid_header_bg">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="header_text_bold">
                                                                    Score Distribution Amongst Test Areas
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="panel_body_bg">
                                                        <div style="height: 180px; overflow: auto;">
                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Image ID="CandidateTestResultPrint_testAreaScoreImage" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="td_height_5">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
