﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestResultPrint.cs
// File that Represents the layout to print Candidate Test 
// result page. This page helps to view the  various statistics and count
// of the questions in the test

#endregion Header

#region Directives

using System;
using System.Web;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;

#endregion Directives

namespace Forte.HCM.UI.PrintPages
{
    /// <summary>
    /// Represents the class that holds the layout to print Candidate Test 
    /// result page. This page helps to view the  various statistics and count
    /// of the questions in the test
    /// </summary>
    public partial class CandidateTestResultPrint : System.Web.UI.Page
    {
        #region Variables

        static BaseColor itemColor = new BaseColor(20, 142, 192);
        static BaseColor headerColor = new BaseColor(40, 48, 51);
        string chartImagePath = string.Empty;
        string forteHCMLogoPath = string.Empty;

        #endregion Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                string baseURL = ConfigurationManager.AppSettings["ATTACHMENT_URL"];

                //Load Style Sheet and Skin
                if (!Utility.IsNullOrEmpty(baseURL))
                {
                    chartImagePath = baseURL + "chart/";
                    lnkStyleSheet.Href = baseURL +
                        "App_Themes/DefaultTheme/DefaultStyle.css";
                    lnkSkin.Href = baseURL +
                        "App_Themes/DefaultTheme/DefaultSkin.skin";
                    forteHCMLogoPath = baseURL +
                        "App_Themes/DefaultTheme/Images/logo.png";
                }

                if (!IsPostBack)
                {
                    Response.Clear();
                    //Method to load the values in test statistics
                    LoadValues();

                    //Method to load the values in candidate statistics 
                    LoadCandidateStatistics();

                    if (!Utility.IsNullOrEmpty(Request.QueryString["downloadoremail"]))
                    {
                        // Download PDF
                        if (Request.QueryString["downloadoremail"].ToUpper() == "D")
                            PrintPdfUsingiTextSharpTables();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method to load the candidate statistics details
        /// </summary>
        private void LoadCandidateStatistics()
        {
            string sessionKey = string.Empty;
            string testKey = string.Empty;
            string histogramSessionKey = string.Empty;
            int attemptID = 0;

            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesessionid"].ToString();

            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());

            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            CandidateStatisticsDetail CandidateStatisticsDataSource =
                new ReportBLManager().GetCandidateTestResultStatisticsDetails(sessionKey, testKey, attemptID);

            LoadCandidateStatisticsDetails(CandidateStatisticsDataSource);

            CandidateTestResultPrint_testScoreImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_HISTOGRAM
                + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            CandidateTestResultPrint_testScoreCategoryImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_CATEGORY
                + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            CandidateTestResultPrint_testScoreSubjectImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_SUBJECT
                + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            CandidateTestResultPrint_testAreaScoreImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_TESTAREA
                + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";
        }

        /// <summary>
        /// Method used to load the values
        /// </summary>
        private void LoadValues()
        {
            //Get the test id
            string testID = Request.QueryString["testkey"];
            //Get the test name   
            string testName;
            testName = new ReportBLManager().GetTestName(Request.QueryString["testkey"]);
            string candidateSessionId = null;
            string candidateName = null;
            DateTime testCompletedDate = DateTime.Now;
            if (Request.QueryString["candidatesessionid"] != null &&
                Request.QueryString["candidatesessionid"].ToString().Length != 0)
            {
                candidateSessionId = Request.QueryString["candidatesessionid"].ToString().Trim();

                //Get testCompletedDate
                int attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
                testCompletedDate = new ReportBLManager().GetTestCompletedDateByCandidate(candidateSessionId, attemptID);

                //Get the CandidateName
                candidateName = new CandidateBLManager().GetCandidateName(candidateSessionId);
                LoadCandidateTestDetails
                    (testID, testName, candidateName, testCompletedDate);
            }

            CandidateTestResultPrint_categoryStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_CATEGORY + "-" + testID + ".png";
            CandidateTestResultPrint_subjectStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png";
            CandidateTestResultPrint_testAreaStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png";
            CandidateTestResultPrint_complexityStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png";
            LoadTestSummaryDetails(testID);
        }

        /// <summary>
        /// Represents the method to load candidate details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <param name="testName">
        /// A<see cref="string"/>that holds the test name
        /// </param>
        /// <param name="candidateName">
        /// A<see cref="string"/>that holds the candidate name
        /// </param>
        /// <param name="creditsEarned">
        /// A<see cref="string"/>that holds the credits earned
        /// </param>
        private void LoadCandidateTestDetails(string testId, string testName,
            string candidateName, DateTime testCompletedDate)
        {
            CandidateTestResultPrint_testNameValueTextBox.Text = string.Concat(testName," (",testId,")");
            CandidateTestResultPrint_candidateNameValueTextBox.Text = candidateName;

            CandidateTestResultPrint_testCompletedDateValueTextBox.Text =
                "Completed on " + testCompletedDate.ToString("MMMM dd, yyyy");
        }

        /// <summary>
        /// Represents the method to load the candidate test summary details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        private void LoadTestSummaryDetails(string testID)
        {
            try
            {
                //Get the test details from database
                TestStatistics TestStatisticsDataSource =
                    new ReportBLManager().GetTestSummaryDetails(testID);
                //Get the general test statistics and load the 
                //test summary details
                
                CandidateTestResultPrint_highScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.HighestScore.ToString().Trim());
                CandidateTestResultPrint_noOfQuestionLabel.Text =
                    TestStatisticsDataSource.NoOfQuestions.ToString().Trim();
                CandidateTestResultPrint_lowScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.LowestScore.ToString().Trim());
                    TestStatisticsDataSource.LowestScore.ToString().Trim();
                CandidateTestResultPrint_meanScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.MeanScore.ToString().Trim());
                    
                CandidateTestResultPrint_avgTimeLabel.Text =
                    Utility.ConvertSecondsToHoursMinutesSeconds(
                   Convert.ToInt32(TestStatisticsDataSource.
                    AverageTimeTakenByCandidates));

                if (!Utility.IsNullOrEmpty(CandidateTestResultPrint_avgTimeLabel.Text))
                {
                    int avgTimeLabel = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(CandidateTestResultPrint_avgTimeLabel.Text));
                    CandidateTestResultPrint_avgTimeLabel.Text =
                        Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(avgTimeLabel);
                }
                CandidateTestResultPrint_scoreSDLabel.Text = TestStatisticsDataSource.StandardDeviation.ToString();
                CandidateTestResultPrint_scoreRangeLabel.Text = TestStatisticsDataSource.ScoreRange.ToString();
                CandidateTestResultPrint_categoryGridview.DataSource =
                   TestStatisticsDataSource.SubjectStatistics;
                CandidateTestResultPrint_categoryGridview.DataBind();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                throw exception;
            }
        }

        /// <summary>
        /// Represents the method to load the candidate statistics details
        /// </summary>
        /// <param name="CandidateStatisticsDataSource">
        /// A<see cref="CandidateStatisticsDetail"/>that holds the candidate statistics details 
        /// </param>
        private void LoadCandidateStatisticsDetails(CandidateStatisticsDetail CandidateStatisticsDataSource)
        {
            string noOfQuestionAttend = string.Empty;

            noOfQuestionAttend = string.Concat(CandidateStatisticsDataSource.TotalQuestionAttended.ToString(), "/",
                CandidateStatisticsDataSource.TotalQuestion.ToString());
            CandidateTestResultPrint_noOfQuestionLabelValue.Text = noOfQuestionAttend;
            CandidateTestResultPrint_absoluteScoreLabelValue.Text =
                CandidateStatisticsDataSource.AbsoluteScore.ToString();
            CandidateTestResultPrint_relativeScoreLabelValue.Text = CandidateStatisticsDataSource.RelativeScore.ToString();
            CandidateTestResultPrint_percentileLabelValue.Text
                = string.Format("{0}%", CandidateStatisticsDataSource.Percentile.ToString());
            CandidateTestResultPrint_noOfQuestionTimeTakenLabelValue.Text
                = CandidateStatisticsDataSource.TimeTaken;
            if (!Utility.IsNullOrEmpty(CandidateTestResultPrint_noOfQuestionTimeTakenLabelValue.Text))
            {
                int queTimeTaken = Convert.ToInt32(Forte.HCM.Support.Utility.
                    ConvertHoursMinutesSecondsToSeconds(CandidateTestResultPrint_noOfQuestionTimeTakenLabelValue.Text));
                CandidateTestResultPrint_noOfQuestionTimeTakenLabelValue.Text =
                    Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(queTimeTaken);
            }
            CandidateTestResultPrint_percentageQuestionsAnsweredCorrectlyLabelValue.Text
                = string.Format("{0}%", CandidateStatisticsDataSource.AnsweredCorrectly.ToString());
            CandidateTestResultPrint_myScoreLabelValue.Text =
                string.Format("{0}%", CandidateStatisticsDataSource.MyScore.ToString());
            decimal averageTimeTaken = 0;
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.AverageTimeTaken)
                && CandidateStatisticsDataSource.AverageTimeTaken.Length != 0)
            {
                averageTimeTaken = decimal.Parse(CandidateStatisticsDataSource.AverageTimeTaken);
            }
            
            CandidateTestResultPrint_avgTimeTakenLabelValue.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds
                (decimal.ToInt32(averageTimeTaken));

            if (!Utility.IsNullOrEmpty(CandidateTestResultPrint_avgTimeTakenLabelValue.Text))
            {
                int avgTimeTaken = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(CandidateTestResultPrint_avgTimeTakenLabelValue.Text));
                CandidateTestResultPrint_avgTimeTakenLabelValue.Text =
                    Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(avgTimeTaken);
            }
        }

        /// <summary>
        /// Represents the method to download the page layout as pdf using Itext libraries
        /// </summary>       
        private void PrintPdfUsingiTextSharpTables()
        {
            //Response.Clear();

            string candSessionID = string.Empty;
            string candAttemptID = string.Empty;
            string histogramSessionKey = string.Empty;

            string fileName = string.Empty;
            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                candSessionID = Request.QueryString["candidatesessionid"].ToString();
            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                candAttemptID = Request.QueryString["attemptid"].ToString();

            fileName = candSessionID + "_" + candAttemptID + ".pdf";

            Response.ContentType = "application/pdf";
            if (fileName != string.Empty)
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            else
                Response.AddHeader("content-disposition", "attachment;filename=TestResult.pdf");

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // Set Styles
            iTextSharp.text.Font fontCaptionStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, headerColor);
            iTextSharp.text.Font fontHeaderStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, headerColor);
            iTextSharp.text.Font fontItemStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, itemColor);

            //Style for Candidate and Test name
            iTextSharp.text.Font fontItemCandidateNameStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 14, iTextSharp.text.Font.NORMAL, itemColor);

            //Set Document Size as A4.
            Document doc = new Document(iTextSharp.text.PageSize.A4);

            //Set document path
            //PdfWriter.GetInstance(doc, new FileStream(Server.MapPath(@"../Chart/Test.pdf"), FileMode.Create));

            //Show ForteHCM Logo
            PdfPTable tableForteHCMLogo = new PdfPTable(1);
            tableForteHCMLogo.SpacingBefore = 10f;
            tableForteHCMLogo.SpacingAfter = 20f;

            iTextSharp.text.Image forteHCMLogo = iTextSharp.text.Image.GetInstance(forteHCMLogoPath);
            forteHCMLogo.ScaleToFit(123f, 69f);
            forteHCMLogo.Border = Rectangle.BOX;
            forteHCMLogo.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellforteHCMLogo = new PdfPCell(forteHCMLogo);
            cellforteHCMLogo.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            tableForteHCMLogo.AddCell(cellforteHCMLogo);
            
            //Construct Table Structures
            PdfPTable tableCandidateInfo = new PdfPTable(2);

            tableCandidateInfo.TotalWidth = 210f;
            //table.LockedWidth = true;            
            tableCandidateInfo.SpacingBefore = 10f;
            tableCandidateInfo.SpacingAfter = 20f;

            //Candidate Info table Structure
            PdfPCell cellCandidateName = new PdfPCell(
                new Phrase(CandidateTestResultPrint_candidateNameValueTextBox.Text, fontItemCandidateNameStyle));
                tableCandidateInfo.AddCell(cellCandidateName);


            //new Phrase(string.Concat(CandidateTestResultPrint_testNameValueTextBox.Text," ",CandidateTestResultPrint_testCompletedDateValueTextBox.Text),

            PdfPCell cellTestName = new PdfPCell(
                new Phrase(CandidateTestResultPrint_testNameValueTextBox.Text,fontItemCandidateNameStyle));
                tableCandidateInfo.AddCell(cellTestName);

            PdfPTable tableTestStatisticsHeader = new PdfPTable(1);
            tableTestStatisticsHeader.SpacingAfter = 8f;
            PdfPCell tableTestStatisticsHeaderCell = new PdfPCell(
                new Phrase("Test Statistics", fontCaptionStyle));
            tableTestStatisticsHeaderCell.BackgroundColor = new BaseColor(235, 239, 241);
            PdfPCell tableTestStatisticsItems = new PdfPCell(
                new Phrase("Items", fontItemStyle));
            //tableTestStatisticsHeader.AddCell(tableTestStatisticsHeaderCell);

            //Summary table structure
            PdfPTable tableTestStatistics = new PdfPTable(8);
            tableTestStatistics.SpacingAfter = 8f;
            tableTestStatistics.WidthPercentage = 100;
            PdfPCell cellTestSummaryHeader = new PdfPCell(
                new Phrase("Test Summary", fontCaptionStyle));
            cellTestSummaryHeader.Colspan = 4;
            cellTestSummaryHeader.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableTestStatistics.AddCell(cellTestSummaryHeader);
            PdfPCell cellTestCategoryHeader = new PdfPCell(
                new Phrase("Categories / Subjects", fontCaptionStyle));
            cellTestCategoryHeader.Colspan = 4;
            cellTestCategoryHeader.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableTestStatistics.AddCell(cellTestCategoryHeader);

            PdfPCell cellNOofQuestionsHeader = new PdfPCell(
                new Phrase("Number Of Questions", fontHeaderStyle));
            tableTestStatistics.AddCell(cellNOofQuestionsHeader);

            PdfPCell cellNOofQuestions = new PdfPCell(
                new Phrase(CandidateTestResultPrint_noOfQuestionLabel.Text, fontItemStyle));

            tableTestStatistics.AddCell(cellNOofQuestions);

            PdfPCell cellTestScoreHeader = new PdfPCell(
                new Phrase("Highest Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellTestScoreHeader);
            PdfPCell cellTestScore = new PdfPCell(
                new Phrase(CandidateTestResultPrint_highScoreLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellTestScore);

            string testID = string.Empty;
            if (!Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
            {
                testID = Request.QueryString["testkey"];
            }

            PdfPCell cellCategoryOrSubjects = new PdfPCell(new Phrase("Category", fontHeaderStyle));
            cellCategoryOrSubjects.Colspan = 4;
            cellCategoryOrSubjects.Rowspan = 6;
            cellCategoryOrSubjects.Table = new PdfPTable(2);

            //cellCategoryOrSubjects.Table.SpacingAfter = 20f;
            cellCategoryOrSubjects.Table.AddCell(new Phrase("Category", fontItemStyle));
            cellCategoryOrSubjects.Table.AddCell(new Phrase("Subject", fontItemStyle));

            for (int i = 0; i < CandidateTestResultPrint_categoryGridview.Rows.Count; i++)
            {
                PdfPCell cellInnerCategory = new PdfPCell(new Phrase(
                    CandidateTestResultPrint_categoryGridview.Rows[i].Cells[0].Text, fontHeaderStyle));
                PdfPCell cellInnerSubject = new PdfPCell(new Phrase(
                    CandidateTestResultPrint_categoryGridview.Rows[i].Cells[1].Text, fontHeaderStyle));
                cellCategoryOrSubjects.Table.AddCell(cellInnerCategory);
                cellCategoryOrSubjects.Table.AddCell(cellInnerSubject);
            }

            tableTestStatistics.AddCell(cellCategoryOrSubjects);

            PdfPCell cellAverageTimeHeader = new PdfPCell(
                new Phrase("Average Time Taken By Candidates", fontHeaderStyle));
            tableTestStatistics.AddCell(cellAverageTimeHeader);
            PdfPCell cellAverageTime = new PdfPCell(
                new Phrase(CandidateTestResultPrint_avgTimeLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellAverageTime);

            PdfPCell cellLowestScoreHeader = new PdfPCell(
                new Phrase("Lowest Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellLowestScoreHeader);
            PdfPCell cellLowestScore = new PdfPCell(
                new Phrase(CandidateTestResultPrint_lowScoreLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellLowestScore);

            PdfPCell cellScoreRangeHeader = new PdfPCell(
                new Phrase("Score Range", fontHeaderStyle));
            tableTestStatistics.AddCell(cellScoreRangeHeader);
            PdfPCell cellScoreRange = new PdfPCell(
                new Phrase(CandidateTestResultPrint_scoreRangeLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellScoreRange);

            PdfPCell cellTestMeanScoreHeader = new PdfPCell(
                new Phrase("Mean Score", fontHeaderStyle));
            tableTestStatistics.AddCell(cellTestMeanScoreHeader);
            PdfPCell cellTestMeanScore = new PdfPCell(
                new Phrase(CandidateTestResultPrint_meanScoreLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellTestMeanScore);

            PdfPCell cellAverageTimeHeaderTest = new PdfPCell(
                new Phrase("", fontHeaderStyle));
            tableTestStatistics.AddCell(cellAverageTimeHeaderTest);
            PdfPCell cellAverageTimeTest = new PdfPCell(
                new Phrase("", fontItemStyle));
            tableTestStatistics.AddCell(cellAverageTimeTest);

            PdfPCell cellStandardDeviationHeader = new PdfPCell(
                new Phrase("Standard Deviation", fontHeaderStyle));
            tableTestStatistics.AddCell(cellStandardDeviationHeader);
            PdfPCell cellStandardDeviation = new PdfPCell(
                new Phrase(CandidateTestResultPrint_scoreSDLabel.Text, fontItemStyle));
            tableTestStatistics.AddCell(cellStandardDeviation);


            PdfPCell cellEmptyHeader = new PdfPCell(new Phrase("", fontHeaderStyle));
            tableTestStatistics.AddCell(cellEmptyHeader);
            PdfPCell cellEmptyItem = new PdfPCell(new Phrase("", fontItemStyle));
            tableTestStatistics.AddCell(cellEmptyItem);

            int rowCount = 0;
            if (CandidateTestResultPrint_categoryGridview.Rows.Count > 6)
                rowCount = CandidateTestResultPrint_categoryGridview.Rows.Count - 6;
            for (int j = 0; j < rowCount; j++)
            {
                PdfPCell cellEmptyItemforRowSpan = new PdfPCell(new Phrase("", fontItemStyle));
                cellEmptyItemforRowSpan.Colspan = 4;
                tableTestStatistics.AddCell(cellEmptyItemforRowSpan);
            }

            tableTestStatisticsItems.AddElement(tableTestStatistics);

            //Category and Subject Statistics table structure
            PdfPTable tableCategoryStatistics = new PdfPTable(2);
            tableCategoryStatistics.SpacingAfter = 8f;
            tableCategoryStatistics.WidthPercentage = 100;
            PdfPCell cellCategoryStatistics = new PdfPCell(new Phrase("Category Statistics", fontCaptionStyle));
            PdfPCell cellSubjectStatistics = new PdfPCell(new Phrase("Subject Statistics", fontCaptionStyle));
            cellCategoryStatistics.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            cellSubjectStatistics.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tableCategoryStatistics.AddCell(cellCategoryStatistics);
            tableCategoryStatistics.AddCell(cellSubjectStatistics);
            iTextSharp.text.Image imageCategory = iTextSharp.text.Image.GetInstance(
                chartImagePath + Constants.ChartConstants.CHART_CATEGORY + "-" + testID + ".png");
            imageCategory.ScaleToFit(205f, 300f);
            imageCategory.Border = Rectangle.BOX;
            imageCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellCategoryImage = new PdfPCell(imageCategory);
            cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableCategoryStatistics.AddCell(cellCategoryImage);
            iTextSharp.text.Image imageSubject = iTextSharp.text.Image.GetInstance(
                chartImagePath + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png");
            imageSubject.ScaleToFit(205f, 300f);
            imageSubject.Border = Rectangle.BOX;
            imageSubject.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellSubjectImage = new PdfPCell(imageSubject);
            cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableCategoryStatistics.AddCell(cellSubjectImage);

            tableTestStatisticsItems.AddElement(tableCategoryStatistics);

            //Test Area and complexity table structure
            PdfPTable tableTestAreaStatistics = new PdfPTable(2);
            //tableTestAreaStatistics.SpacingAfter = 3f;
            tableTestAreaStatistics.WidthPercentage = 100;
            PdfPCell cellAreStatisticsHeader = new PdfPCell(
                new Phrase("Test Area Statistics", fontCaptionStyle));
            PdfPCell cellComplexityStatisticsHeader = new PdfPCell(
                new Phrase("Complexity Statistics", fontCaptionStyle));
            tableTestAreaStatistics.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableTestAreaStatistics.AddCell(cellAreStatisticsHeader);
            tableTestAreaStatistics.AddCell(cellComplexityStatisticsHeader);
            iTextSharp.text.Image imageArea = iTextSharp.text.Image.GetInstance(
                chartImagePath + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png");
            imageArea.ScaleToFit(205f, 300f);
            imageArea.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellAreaImage = new PdfPCell(imageArea);
            cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestAreaStatistics.AddCell(cellAreaImage);
            iTextSharp.text.Image imageComplexity = iTextSharp.text.Image.GetInstance(
                chartImagePath + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png");
            imageComplexity.ScaleToFit(205f, 300f);
            imageComplexity.Border = Rectangle.BOX;
            imageComplexity.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellComplexityImage = new PdfPCell(imageComplexity);
            cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestAreaStatistics.AddCell(cellComplexityImage);

            tableTestStatisticsItems.AddElement(tableTestAreaStatistics);
            tableTestStatisticsHeader.AddCell(tableTestStatisticsItems);

            PdfPTable tableCandidateStatisticsHeader = new PdfPTable(1);
            tableCandidateStatisticsHeader.SpacingAfter = 8f;
            PdfPCell tableCandidateStatisticsHeaderCell = new PdfPCell(
                new Phrase("Candidate Statistics", fontCaptionStyle));
            tableCandidateStatisticsHeaderCell.BackgroundColor = new BaseColor(235, 239, 241);
            PdfPCell tableCandidateStatisticsItemCell = new PdfPCell(new Phrase("", fontItemStyle));
            //tableCandidateStatisticsHeader.AddCell(tableCandidateStatisticsHeaderCell);

            //Candidate Statistics
            PdfPTable tableCandidateStatistics = new PdfPTable(4);
            tableCandidateStatistics.SpacingAfter = 8f;
            tableCandidateStatistics.WidthPercentage = 100;
            
            
            PdfPCell cellCandidateMyScoreHeader = new PdfPCell(
                new Phrase("My Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateMyScoreHeader);
            PdfPCell cellCandidateMyScore = new PdfPCell(
                new Phrase(CandidateTestResultPrint_myScoreLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateMyScore);

            PdfPCell cellCandidateNoOfQuestionsHeader = new PdfPCell(
                new Phrase("Number Of Questions Attended", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateNoOfQuestionsHeader);
            PdfPCell cellCandidateNoOfQuestion = new PdfPCell(
                new Phrase(CandidateTestResultPrint_noOfQuestionLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateNoOfQuestion);


            PdfPCell cellCCandidateAbsoluteScoreHeader = new PdfPCell(
                new Phrase("Absolute Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCCandidateAbsoluteScoreHeader);
            PdfPCell cellCandidateAbsolute = new PdfPCell(
                new Phrase(CandidateTestResultPrint_absoluteScoreLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAbsolute);


            PdfPCell cellCandidateAnsweredCorrectlyPercentageHeader = new PdfPCell(
               new Phrase("Percentage Of Questions Answered Right", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyPercentageHeader);

            PdfPCell cellCandidateAnsweredCorrectlyPercentage = new PdfPCell(
                new Phrase(CandidateTestResultPrint_percentageQuestionsAnsweredCorrectlyLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAnsweredCorrectlyPercentage);


            
            PdfPCell cellCandidateRelativeScoreHeader = new PdfPCell(
            new Phrase("Relative Score", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateRelativeScoreHeader);
            PdfPCell cellCandidateRelativeScore = new PdfPCell(
                new Phrase(CandidateTestResultPrint_relativeScoreLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateRelativeScore);


            PdfPCell cellCandidateTotalTimeTakenHeader = new PdfPCell(
               new Phrase("Time Taken To Complete Test", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalTimeTakenHeader);
            PdfPCell cellCandidateTotalTimeTaken = new PdfPCell(
                new Phrase(CandidateTestResultPrint_noOfQuestionTimeTakenLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateTotalTimeTaken);


            PdfPCell cellCandidatePercentileHeader = new PdfPCell(
            new Phrase("Percentile", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidatePercentileHeader);
            PdfPCell cellCandidateercentile = new PdfPCell(
                new Phrase(CandidateTestResultPrint_percentileLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateercentile);


            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestionHeader = new PdfPCell(
              new Phrase("Average Time Taken Per Question", fontHeaderStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestionHeader);

            PdfPCell cellCandidateAverageTimeTakenToAnswerEachQuestion = new PdfPCell(
                new Phrase(CandidateTestResultPrint_avgTimeTakenLabelValue.Text, fontItemStyle));
            tableCandidateStatistics.AddCell(cellCandidateAverageTimeTakenToAnswerEachQuestion);

            tableCandidateStatisticsItemCell.AddElement(tableCandidateStatistics);

            string sessionKey = string.Empty;
            string testKey = string.Empty;
            int attemptID = 0;
            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesessionid"].ToString();
            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            //Test Score and category table structure
            PdfPTable tableTestScore = new PdfPTable(2);
            tableTestScore.SpacingAfter = 8f;
            tableTestScore.WidthPercentage = 100;
            PdfPCell cellScoreHeader = new PdfPCell(new Phrase("Test Scores", fontCaptionStyle));
            PdfPCell cellScoreCategoryHeader = new PdfPCell(
                new Phrase("Score Distribution Amongst Categories", fontCaptionStyle));
            tableTestScore.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableTestScore.AddCell(cellScoreHeader);
            tableTestScore.AddCell(cellScoreCategoryHeader);

            iTextSharp.text.Image imageTestScore = iTextSharp.text.Image.GetInstance(chartImagePath +
                Constants.ChartConstants.CHART_HISTOGRAM + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png");

            imageTestScore.ScaleToFit(205f, 300f);
            imageTestScore.Border = Rectangle.BOX;
            imageTestScore.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellTestScoreImage = new PdfPCell(imageTestScore);
            cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestScore.AddCell(cellTestScoreImage);
            iTextSharp.text.Image imageScoreCategory = iTextSharp.text.Image.GetInstance(chartImagePath +
                Constants.ChartConstants.CHART_CATEGORY +
                "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            imageScoreCategory.ScaleToFit(205f, 300f);
            imageScoreCategory.Border = Rectangle.BOX;
            imageScoreCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellScoreCategoryImage = new PdfPCell(imageScoreCategory);
            cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            tableTestScore.AddCell(cellScoreCategoryImage);

            tableCandidateStatisticsItemCell.AddElement(tableTestScore);

            //Score subject and score distribution area table structure
            PdfPTable tableScoreSubject = new PdfPTable(2);
            //tableScoreSubject.SpacingAfter = 3f;
            tableScoreSubject.WidthPercentage = 100;
            PdfPCell cellScoreSubjectHeader = new PdfPCell(
                new Phrase("Score Distribution Amongst Subjects", fontCaptionStyle));
            PdfPCell cellScoreTestAreaHeader = new PdfPCell(
                new Phrase("Score Distribution Amongst Test Areas", fontCaptionStyle));
            tableScoreSubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableScoreSubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            tableScoreSubject.AddCell(cellScoreSubjectHeader);
            tableScoreSubject.AddCell(cellScoreTestAreaHeader);
            iTextSharp.text.Image imageScoreSubject = iTextSharp.text.Image.GetInstance(chartImagePath +
                Constants.ChartConstants.CHART_SUBJECT + "-" +
                testKey + "-" + sessionKey + "-" + attemptID + ".png");
            imageScoreSubject.ScaleToFit(205f, 300f);
            imageScoreSubject.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellScoreSubjectImage = new PdfPCell(imageScoreSubject);
            cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            tableScoreSubject.AddCell(cellScoreSubjectImage);
            iTextSharp.text.Image imageScoreArea = iTextSharp.text.Image.GetInstance(chartImagePath +
                Constants.ChartConstants.CHART_TESTAREA + "-" +
                testKey + "-" + sessionKey + "-" + attemptID + ".png");
            imageScoreArea.ScaleToFit(205f, 300f);
            imageScoreArea.Border = Rectangle.BOX;
            imageScoreArea.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellScoreAreaImage = new PdfPCell(imageScoreArea);
            cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            tableScoreSubject.AddCell(cellScoreAreaImage);

            tableCandidateStatisticsItemCell.AddElement(tableScoreSubject);
            tableCandidateStatisticsHeader.AddCell(tableCandidateStatisticsItemCell);

            //Response Output
            PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();
            doc.Add(tableForteHCMLogo);
            doc.Add(tableCandidateInfo);
            doc.Add(tableCandidateStatisticsHeader);
            doc.Add(tableTestStatisticsHeader);
            doc.Close();
        }

        #endregion Private Methods
    }
}