#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// CandidateTestResultPrint.cs
// File that Represents the layout to print Candidate Test 
// result page. This page helps to view the  various statistics and count
// of the questions in the test

#endregion Header

#region Directives

using System;
using System.Web;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;
using System.Drawing.Drawing2D;
#endregion Directives

namespace Forte.HCM.UI.PrintPages
{
    /// <summary>
    /// Represents the class that holds the layout to print Candidate Test 
    /// result page. This page helps to view the  various statistics and count
    /// of the questions in the test
    /// </summary>
    public partial class CandidateTestResultTranscript : System.Web.UI.Page
    {
        #region Variables

        static BaseColor itemColor = new BaseColor(System.Drawing.Color.Black);
        static BaseColor headerColor = new BaseColor(40, 48, 51);
        static BaseColor headerTitle1 = new BaseColor(71, 71, 70);
        static BaseColor headerTitle2 = new BaseColor(109,109,108);
        string chartImagePath = string.Empty;
        string forteHCMLogoPath = string.Empty;
        string bgImagePage1 = string.Empty;
        string bgImagePage2 = string.Empty;
        #endregion Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A<see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                string baseURL = ConfigurationManager.AppSettings["ATTACHMENT_URL"];

                //Load Style Sheet and Skin
                if (!Utility.IsNullOrEmpty(baseURL))
                {
                    chartImagePath = baseURL + "chart/";
                    lnkStyleSheet.Href = baseURL +
                        "App_Themes/DefaultTheme/DefaultStyle.css";
                    lnkSkin.Href = baseURL +
                        "App_Themes/DefaultTheme/DefaultSkin.skin";
                    forteHCMLogoPath = baseURL +
                        "App_Themes/DefaultTheme/Images/logo.png";
                    bgImagePage1 = baseURL +
                        "App_Themes/DefaultTheme/Images/candidate_certificate_1.jpg";
                    bgImagePage2 = baseURL +
                        "App_Themes/DefaultTheme/Images/candidate_certificate_2.jpg";

                    
                }

                if (!IsPostBack)
                {
                    Response.Clear();
                    //Method to load the values in test statistics
                    LoadValues();

                    //Method to load the values in candidate statistics 
                    LoadCandidateStatistics();

                    if (!Utility.IsNullOrEmpty(Request.QueryString["downloadoremail"]))
                    {
                        // Download PDF
                        if (Request.QueryString["downloadoremail"].ToUpper() == "D")
                            PrintPdfUsingiTextSharpTables();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        /// <summary>
        /// Represents the method to load the candidate statistics details
        /// </summary>
        private void LoadCandidateStatistics()
        {
            string sessionKey = string.Empty;
            string testKey = string.Empty;
            string histogramSessionKey = string.Empty;
            int attemptID = 0;

            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesessionid"].ToString();

            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());

            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();

            CandidateStatisticsDetail CandidateStatisticsDataSource =
                new ReportBLManager().GetCandidateTestResultStatisticsDetails(sessionKey, testKey, attemptID);

            LoadCandidateStatisticsDetails(CandidateStatisticsDataSource);


            CandidateTestResultTranscript_testScoreCategoryImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_CATEGORY
                + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            CandidateTestResultTranscript_testScoreSubjectImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_SUBJECT
                + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";

            CandidateTestResultTranscript_testAreaScoreImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_TESTAREA
                + "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png";
        }

        /// <summary>
        /// Method used to load the values
        /// </summary>
        private void LoadValues()
        {
            //Get the test id
            string testID = Request.QueryString["testkey"];
            //Get the test name   
            string testName;
            testName = new ReportBLManager().GetTestName(Request.QueryString["testkey"]);
            string candidateSessionId = null;
            string candidateName = null;
            DateTime testCompletedDate = DateTime.Now;
            if (Request.QueryString["candidatesessionid"] != null &&
                Request.QueryString["candidatesessionid"].ToString().Length != 0)
            {
                candidateSessionId = Request.QueryString["candidatesessionid"].ToString().Trim();

                //Get testCompletedDate
                int attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
                testCompletedDate = new ReportBLManager().GetTestCompletedDateByCandidate(candidateSessionId, attemptID);

                //Get the CandidateName
                candidateName = new CandidateBLManager().GetCandidateName(candidateSessionId);
                LoadCandidateTestDetails
                    (testID, testName, candidateName, testCompletedDate);
            }

            CandidateTestResultTranscript_categoryStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_CATEGORY + "-" + testID + ".png";
            CandidateTestResultTranscript_subjectStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png";
            CandidateTestResultTranscript_testAreaStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png";
            CandidateTestResultTranscript_complexityStatisticsImage.ImageUrl =
                chartImagePath + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png";
            LoadTestSummaryDetails(testID);
        }

        /// <summary>
        /// Represents the method to load candidate details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        /// <param name="testName">
        /// A<see cref="string"/>that holds the test name
        /// </param>
        /// <param name="candidateName">
        /// A<see cref="string"/>that holds the candidate name
        /// </param>
        /// <param name="creditsEarned">
        /// A<see cref="string"/>that holds the credits earned
        /// </param>
        private void LoadCandidateTestDetails(string testId, string testName,
            string candidateName, DateTime testCompletedDate)
        {
            CandidateTestResultTranscript_testNameValueTextBox.Text = testName;
            CandidateTestResultTranscript_candidateNameValueTextBox.Text = candidateName;

            if(!Utility.IsNullOrEmpty(testCompletedDate))
                CandidateTestResultTranscript_testCompletedDateValueTextBox.Text = testCompletedDate.ToString("MMMM dd, yyyy");
        }

        /// <summary>
        /// Represents the method to load the candidate test summary details
        /// </summary>
        /// <param name="testId">
        /// A<see cref="string"/>that holds the test id 
        /// </param>
        private void LoadTestSummaryDetails(string testID)
        {
            try
            {
                //Get the test details from database
                TestStatistics TestStatisticsDataSource =
                    new ReportBLManager().GetTestSummaryDetails(testID);
                //Get the general test statistics and load the 
                //test summary details
                
                CandidateTestResultTranscript_highScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.HighestScore.ToString().Trim());
                CandidateTestResultTranscript_noOfQuestionLabel.Text =
                    TestStatisticsDataSource.NoOfQuestions.ToString().Trim();
                CandidateTestResultTranscript_lowScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.LowestScore.ToString().Trim());
                    TestStatisticsDataSource.LowestScore.ToString().Trim();
                CandidateTestResultTranscript_meanScoreLabel.Text =
                    string.Format("{0}%", TestStatisticsDataSource.MeanScore.ToString().Trim());
                    
                CandidateTestResultTranscript_avgTimeLabel.Text =
                    Utility.ConvertSecondsToHoursMinutesSeconds(
                    Convert.ToInt32(TestStatisticsDataSource.
                    AverageTimeTakenByCandidates.ToString()));

                if (!Utility.IsNullOrEmpty(CandidateTestResultTranscript_avgTimeLabel.Text))
                {
                    int avgTimeLabel = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(CandidateTestResultTranscript_avgTimeLabel.Text));
                    CandidateTestResultTranscript_avgTimeLabel.Text =
                        Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(avgTimeLabel);
                }
                CandidateTestResultTranscript_scoreSDLabel.Text = TestStatisticsDataSource.StandardDeviation.ToString();
                CandidateTestResultTranscript_scoreRangeLabel.Text = TestStatisticsDataSource.ScoreRange.ToString();
                CandidateTestResultTranscript_categoryGridview.DataSource =
                   TestStatisticsDataSource.SubjectStatistics;
                CandidateTestResultTranscript_categoryGridview.DataBind();
            }
            catch (Exception exception)
            {
                Logger.ExceptionLog(exception);
                throw exception;
            }
        }

        /// <summary>
        /// Represents the method to load the candidate statistics details
        /// </summary>
        /// <param name="CandidateStatisticsDataSource">
        /// A<see cref="CandidateStatisticsDetail"/>that holds the candidate statistics details 
        /// </param>
        private void LoadCandidateStatisticsDetails(CandidateStatisticsDetail CandidateStatisticsDataSource)
        {
            string noOfQuestionAttend = string.Empty;

            noOfQuestionAttend = string.Concat(CandidateStatisticsDataSource.TotalQuestionAttended.ToString(), "/",
                CandidateStatisticsDataSource.TotalQuestion.ToString());
            CandidateTestResultTranscript_noOfQuestionLabelValue.Text = noOfQuestionAttend;

            CandidateTestResultTranscript_noOfQuestionTimeTakenLabelValue.Text
                = CandidateStatisticsDataSource.TimeTaken;
            if (!Utility.IsNullOrEmpty(CandidateTestResultTranscript_noOfQuestionTimeTakenLabelValue.Text))
            {
                int queTimeTaken = Convert.ToInt32(Forte.HCM.Support.Utility.
                    ConvertHoursMinutesSecondsToSeconds(CandidateTestResultTranscript_noOfQuestionTimeTakenLabelValue.Text));
                CandidateTestResultTranscript_noOfQuestionTimeTakenLabelValue.Text =
                    Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(queTimeTaken);
            }
            CandidateTestResultTranscript_percentageQuestionsAnsweredCorrectlyLabelValue.Text
                = string.Format("{0}%", CandidateStatisticsDataSource.AnsweredCorrectly.ToString());
            CandidateTestResultTranscript_myScoreLabelValue.Text =
                string.Format("{0}%", CandidateStatisticsDataSource.MyScore.ToString());
            CandidateTestResultTranscript_myScoreHiddenField.Value = CandidateStatisticsDataSource.MyScore.ToString();
            decimal averageTimeTaken = 0;
            if (!Utility.IsNullOrEmpty(CandidateStatisticsDataSource.AverageTimeTaken)
                && CandidateStatisticsDataSource.AverageTimeTaken.Length != 0)
            {
                averageTimeTaken = decimal.Parse(CandidateStatisticsDataSource.AverageTimeTaken);
            }
            
            CandidateTestResultTranscript_avgTimeTakenLabelValue.Text =
                Utility.ConvertSecondsToHoursMinutesSeconds
                (decimal.ToInt32(averageTimeTaken));

            if (!Utility.IsNullOrEmpty(CandidateTestResultTranscript_avgTimeTakenLabelValue.Text))
            {
                int avgTimeTaken = Convert.ToInt32(Forte.HCM.Support.Utility.ConvertHoursMinutesSecondsToSeconds(CandidateTestResultTranscript_avgTimeTakenLabelValue.Text));
                CandidateTestResultTranscript_avgTimeTakenLabelValue.Text =
                    Forte.HCM.Support.Utility.ConvertSecondsToHoursMinutesSecondsWithTextualPhrase(avgTimeTaken);
            }
        }

        /// <summary>
        /// Represents the method to download the page layout as pdf using Itext libraries
        /// </summary>       
        private void PrintPdfUsingiTextSharpTables()
        {
            //Response.Clear();

            string candSessionID = string.Empty;
            string candAttemptID = string.Empty;
            string histogramSessionKey = string.Empty;
            string appPath = HttpContext.Current.Request.ApplicationPath;
            string physicalPath = HttpContext.Current.Request.MapPath(appPath);
            
            string fileName = string.Empty;
            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                candSessionID = Request.QueryString["candidatesessionid"].ToString();
            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                candAttemptID = Request.QueryString["attemptid"].ToString();

            fileName = candSessionID + "_" + candAttemptID + ".pdf";

            Response.ContentType = "application/pdf";
            if (fileName != string.Empty)
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            else
                Response.AddHeader("content-disposition", "attachment;filename=TestResult.pdf");

            Response.Cache.SetCacheability(HttpCacheability.NoCache);


            // Set Styles
            iTextSharp.text.Font fontCaptionStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 8, iTextSharp.text.Font.NORMAL, headerColor);
            iTextSharp.text.Font fontHeaderStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, headerColor);
            iTextSharp.text.Font fontItemStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 7, iTextSharp.text.Font.NORMAL, itemColor);

            //Style for Candidate and Test name
            iTextSharp.text.Font fontItemCandidateNameStyle = iTextSharp.text.FontFactory.GetFont(
                "Arial,sans-serif", 14, iTextSharp.text.Font.NORMAL, itemColor);

            //Set Document Size as A4.
            Document doc = new Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 10f);
            
            //Set document path 
            //PdfWriter.GetInstance(doc, new FileStream(Server.MapPath(@"../Chart/Test.pdf"), FileMode.Create));

            #region Set BackgroundImage
            iTextSharp.text.Image setBackGroundImage = iTextSharp.text.Image.GetInstance(bgImagePage1);
            setBackGroundImage.ScaleToFit(3000, 820);
            setBackGroundImage.Alignment = iTextSharp.text.Image.UNDERLYING;
            //setBackGroundImage.SetAbsolutePosition(80f, 60f);
            //setBackGroundImage.SpacingBefore = 60f;
            
            #endregion Set BackgroundImage

            #region Candidate Information
            //Construct Table Structures
            PdfPTable tableCandidateInfo = new PdfPTable(1);

            tableCandidateInfo.TotalWidth = 210f;
            //table.LockedWidth = true;            
            tableCandidateInfo.SpacingBefore = 200f;
            tableCandidateInfo.SpacingAfter = 20f;

            //Print Candidate Name
            PdfPCell cellCandidateName = new PdfPCell(new Phrase(CandidateTestResultTranscript_candidateNameValueTextBox.Text,
                iTextSharp.text.FontFactory.GetFont("Arial", 24, iTextSharp.text.Font.NORMAL, headerTitle1)));
            cellCandidateName.BorderWidth = 0;
            //cellCandidateName.PaddingLeft=150;
            cellCandidateName.PaddingTop = 120;
            cellCandidateName.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfo.AddCell(cellCandidateName);


            //Print Test Name
            PdfPCell cellTestName = new PdfPCell(new Phrase(CandidateTestResultTranscript_testNameValueTextBox.Text,
                iTextSharp.text.FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.NORMAL, headerTitle1)));
            cellTestName.BorderWidth = 0;
            cellTestName.PaddingTop = 57;
            cellTestName.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfo.AddCell(cellTestName);

           
            //Print Completed Date
            string formatCompletedDate = string.Empty;
            formatCompletedDate = string.Concat("on ", CandidateTestResultTranscript_testCompletedDateValueTextBox.Text, " with a");
            PdfPCell cellCompletedDate = new PdfPCell(new Phrase(formatCompletedDate,
                iTextSharp.text.FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.ITALIC, headerTitle2)));
            cellCompletedDate.BorderWidth = 0;
            cellCompletedDate.PaddingTop = 23;
            cellCompletedDate.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfo.AddCell(cellCompletedDate);

            //Print Score of
            string scoreArea = string.Empty;
            if (!Utility.IsNullOrEmpty(CandidateTestResultTranscript_myScoreHiddenField.Value))
            {
                int testScore = 0;
                string scoreInWords = string.Empty;
                testScore = (int)Math.Ceiling(Convert.ToDouble(CandidateTestResultTranscript_myScoreHiddenField.Value));
                scoreInWords = Utility.GetNumberInLetters(testScore);

                scoreArea = string.Concat("score of ",testScore, "%", " [", scoreInWords, " percent", "]");
            }


            PdfPCell cellScoreOfValue = new PdfPCell(new Phrase(scoreArea,
               iTextSharp.text.FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL, headerTitle1)));
            cellScoreOfValue.PaddingTop = 15;
            cellScoreOfValue.HorizontalAlignment = Element.ALIGN_CENTER;
            cellScoreOfValue.BorderWidth = 0;
            tableCandidateInfo.AddCell(cellScoreOfValue);

            ////Print Score of in words
            //PdfPCell cellScoreOfValueInWords = new PdfPCell(new Phrase(scoreInWords,
            //  iTextSharp.text.FontFactory.GetFont("futura", 15, iTextSharp.text.Font.NORMAL, itemColor)));
            //cellScoreOfValueInWords.PaddingTop = -5;
            //cellScoreOfValueInWords.PaddingLeft = 25;
            //cellScoreOfValueInWords.HorizontalAlignment = Element.ALIGN_RIGHT;
            //cellScoreOfValueInWords.BorderWidth = 0;
            //tableCandidateInfo.AddCell(cellScoreOfValueInWords);

            #endregion Candidate Information

            
            #region Set Query String Values
            string testID = string.Empty;
            if (!Utility.IsNullOrEmpty(Request.QueryString["testkey"]))
            {
                testID = Request.QueryString["testkey"];
            }

            string sessionKey = string.Empty;
            string testKey = string.Empty;
            int attemptID = 0;
            if ((Request.QueryString["candidatesessionid"] != null) &&
                (Request.QueryString["candidatesessionid"].ToString().Length != 0))
                sessionKey = Request.QueryString["candidatesessionid"].ToString();
            if ((Request.QueryString["attemptid"] != null) &&
                (Request.QueryString["attemptid"].ToString().Length != 0))
                attemptID = int.Parse(Request.QueryString["attemptid"].ToString());
            if ((Request.QueryString["testkey"] != null) &&
               (Request.QueryString["testkey"].ToString().Length != 0))
                testKey = Request.QueryString["testkey"].ToString();
            #endregion Set Query String Values

            #region Test Information
           
            #region Category and Subject
            PdfPTable tableCategoryStatistics = new PdfPTable(2);
            tableCategoryStatistics.SpacingAfter = 8f;
            tableCategoryStatistics.WidthPercentage = 100;
            
            //Category Statistics
            System.Drawing.Image orgChartCategory = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_CATEGORY + "-" + testID + ".png");
            iTextSharp.text.Image imageCategory = iTextSharp.text.Image.GetInstance(GetResizedImage(orgChartCategory, new System.Drawing.Size(1024, 968)),
                 System.Drawing.Imaging.ImageFormat.Png);


            imageCategory.ScaleToFit(250f, 650f);
            imageCategory.Border = Rectangle.BOX;
            imageCategory.Alignment = iTextSharp.text.Image.ALIGN_CENTER;


            PdfPCell cellCategoryImage = new PdfPCell(imageCategory);
            cellCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellCategoryImage.PaddingTop = 90;
            cellCategoryImage.BorderWidth = 0;
            cellCategoryImage.PaddingLeft = 50;
            tableCategoryStatistics.AddCell(cellCategoryImage);


            //Subject Statistics
            //iTextSharp.text.Image imageSubject = iTextSharp.text.Image.GetInstance(
            //    chartImagePath + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png");

            System.Drawing.Image orgChartSubject = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_SUBJECT + "-" + testID + ".png");
            iTextSharp.text.Image imageSubject = iTextSharp.text.Image.GetInstance(GetResizedImage(orgChartSubject, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);


            imageSubject.ScaleToFit(250f, 650f);
            imageSubject.Border = Rectangle.BOX;
            imageSubject.Alignment = iTextSharp.text.Image.ALIGN_CENTER;


            PdfPCell cellSubjectImage = new PdfPCell(imageSubject);
            cellSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellSubjectImage.PaddingTop = 90;
            cellSubjectImage.BorderWidth = 0;
            cellSubjectImage.PaddingLeft = -40;
            tableCategoryStatistics.AddCell(cellSubjectImage);

            #endregion category and subject

            #region Test Area and Complexity Statistics
            
            PdfPTable tableTestAreaComplexStatistics = new PdfPTable(2);
            tableTestAreaComplexStatistics.SpacingAfter = 8f;
            tableTestAreaComplexStatistics.WidthPercentage = 100;

            //iTextSharp.text.Image imageArea = iTextSharp.text.Image.GetInstance(
            //    chartImagePath + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png");

            System.Drawing.Image orgChartTestArea = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_TESTAREA + "-" + testID + ".png");
            iTextSharp.text.Image imageArea = iTextSharp.text.Image.GetInstance(GetResizedImage(orgChartTestArea, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageArea.ScaleToFit(250f,650);
            imageArea.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_CENTER;

            PdfPCell cellAreaImage = new PdfPCell(imageArea);
            cellAreaImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellAreaImage.PaddingTop = 110;
            cellAreaImage.BorderWidth = 0;
            cellAreaImage.PaddingLeft = 50;
            tableTestAreaComplexStatistics.AddCell(cellAreaImage);

            //Complexity Statistics
            //iTextSharp.text.Image imageComplexity = iTextSharp.text.Image.GetInstance(
            //    chartImagePath + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png");

            System.Drawing.Image orgChartComplexity =  System.Drawing.Image.FromFile(physicalPath+"\\chart\\" + Constants.ChartConstants.CHART_COMPLEXITY + "-" + testID + ".png");
            iTextSharp.text.Image imageComplexity = iTextSharp.text.Image.GetInstance(GetResizedImage(orgChartComplexity, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageComplexity.ScaleToFit(250f, 650f);
            imageComplexity.Border = Rectangle.BOX;
            imageComplexity.Alignment = iTextSharp.text.Image.ALIGN_CENTER;

            PdfPCell cellComplexityImage = new PdfPCell(imageComplexity);
            cellComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellComplexityImage.PaddingTop = 110;
            cellComplexityImage.BorderWidth = 0;
            cellComplexityImage.PaddingLeft = -40;
            tableTestAreaComplexStatistics.AddCell(cellComplexityImage);

            #endregion Test Area and Complexity Statistics

           
            #endregion Test Information

            iTextSharp.text.Image imgPage2 = iTextSharp.text.Image.GetInstance(bgImagePage2);
            imgPage2.ScaleToFit(3000, 820);
            imgPage2.Alignment = iTextSharp.text.Image.UNDERLYING;
            //bg3.SetAbsolutePosition(25, 69);


            #region Score Analysis Charts
            
            PdfPTable tableScoreAnalysisCategorySubject = new PdfPTable(2);
            tableScoreAnalysisCategorySubject.SpacingAfter = 8f;
            tableScoreAnalysisCategorySubject.WidthPercentage = 100;
            tableScoreAnalysisCategorySubject.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right

            #region Category and Subject

            //iTextSharp.text.Image imageScoreCategory = iTextSharp.text.Image.GetInstance(chartImagePath +
            //    Constants.ChartConstants.CHART_CATEGORY +
            //    "-" + testKey + "-" + sessionKey + "-" + attemptID + ".png");

            System.Drawing.Image orgScoreCategory = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_CATEGORY + "-"
                + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            iTextSharp.text.Image imageScoreCategory = iTextSharp.text.Image.GetInstance(GetResizedImage(orgScoreCategory, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageScoreCategory.ScaleToFit(250f, 600f);
            imageScoreCategory.Border = Rectangle.BOX;
            imageScoreCategory.BorderWidth = 0;
            imageScoreCategory.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            PdfPCell cellScoreCategoryImage = new PdfPCell(imageScoreCategory);
            cellScoreCategoryImage.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            cellScoreCategoryImage.PaddingTop = 60;
            cellScoreCategoryImage.BorderWidth = 0;
            cellScoreCategoryImage.PaddingLeft = 50;
            tableScoreAnalysisCategorySubject.AddCell(cellScoreCategoryImage);


            //Subject Score
            //iTextSharp.text.Image imageScoreSubject = iTextSharp.text.Image.GetInstance(chartImagePath +
            //    Constants.ChartConstants.CHART_SUBJECT + "-" +
            //    testKey + "-" + sessionKey + "-" + attemptID + ".png");

            System.Drawing.Image orgScoreSubject = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_SUBJECT + "-"
                + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            iTextSharp.text.Image imageScoreSubject = iTextSharp.text.Image.GetInstance(GetResizedImage(orgScoreSubject, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);


            imageScoreSubject.ScaleToFit(250f, 600f);
            imageScoreSubject.Border = Rectangle.BOX;
            imageArea.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellScoreSubjectImage = new PdfPCell(imageScoreSubject);
            cellScoreSubjectImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellScoreSubjectImage.PaddingTop = 60;
            cellScoreSubjectImage.BorderWidth = 0;
            cellScoreSubjectImage.PaddingLeft = -40;
            tableScoreAnalysisCategorySubject.AddCell(cellScoreSubjectImage);

            #endregion Category and Subject

            #region Test Area and Complexity
            PdfPTable tableScoreAnalysisTestAreaComplex = new PdfPTable(2);
            tableScoreAnalysisTestAreaComplex.SpacingAfter = 8f;
            tableScoreAnalysisTestAreaComplex.WidthPercentage = 100;
            tableScoreAnalysisTestAreaComplex.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right

            //Test Areas Score
            //iTextSharp.text.Image imageScoreArea = iTextSharp.text.Image.GetInstance(chartImagePath +
            //    Constants.ChartConstants.CHART_TESTAREA + "-" +
            //    testKey + "-" + sessionKey + "-" + attemptID + ".png");

            System.Drawing.Image orgScoreArea = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_TESTAREA + "-"
                + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            iTextSharp.text.Image imageScoreArea = iTextSharp.text.Image.GetInstance(GetResizedImage(orgScoreArea, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageScoreArea.ScaleToFit(250f, 600f);
            imageScoreArea.Border = Rectangle.BOX;
            imageScoreArea.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellScoreAreaImage = new PdfPCell(imageScoreArea);
            cellScoreAreaImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellScoreAreaImage.PaddingTop = 95;
            cellScoreAreaImage.PaddingLeft = 50;
            cellScoreAreaImage.BorderWidth = 0;
            tableScoreAnalysisTestAreaComplex.AddCell(cellScoreAreaImage);

            System.Drawing.Image orgScoreComplexity = System.Drawing.Image.FromFile(physicalPath + "\\chart\\" + Constants.ChartConstants.CHART_COMPLEXITY + "-"
                + testKey + "-" + sessionKey + "-" + attemptID + ".png");
            iTextSharp.text.Image imageScoreComplexity = iTextSharp.text.Image.GetInstance(GetResizedImage(orgScoreComplexity, new System.Drawing.Size(1024, 768)),
                 System.Drawing.Imaging.ImageFormat.Png);

            imageScoreComplexity.ScaleToFit(250f, 600f);
            imageScoreComplexity.Border = Rectangle.BOX;
            imageScoreComplexity.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            PdfPCell cellScoreComplexityImage = new PdfPCell(imageScoreComplexity);
            cellScoreComplexityImage.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cellScoreComplexityImage.PaddingTop = 95;
            cellScoreComplexityImage.PaddingLeft = -40;
            cellScoreComplexityImage.BorderWidth = 0;
            tableScoreAnalysisTestAreaComplex.AddCell(cellScoreComplexityImage);


            #endregion Test Area and Complexity

            #endregion Score Information


            #region Canddidate information for page 2

            //Construct Table Structures
            PdfPTable tableCandidateInfoPage2 = new PdfPTable(1);

            tableCandidateInfoPage2.TotalWidth = 210f;
            tableCandidateInfoPage2.SpacingBefore = 200f;
            tableCandidateInfoPage2.SpacingAfter = 20f;

            //Print Candidate Name
            PdfPCell cellCandidateNamePage2 = new PdfPCell(new Phrase(CandidateTestResultTranscript_candidateNameValueTextBox.Text,
                iTextSharp.text.FontFactory.GetFont("Arial", 24, iTextSharp.text.Font.NORMAL, headerTitle1)));

            cellCandidateNamePage2.BorderWidth = 0;
            cellCandidateNamePage2.PaddingTop = 74;
            cellCandidateNamePage2.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfoPage2.AddCell(cellCandidateNamePage2);


            //Print Test Name
            PdfPCell cellTestNamePage2 = new PdfPCell(new Phrase(CandidateTestResultTranscript_testNameValueTextBox.Text,
                iTextSharp.text.FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.NORMAL, headerTitle1)));
            cellTestNamePage2.BorderWidth = 0;
            cellTestNamePage2.PaddingTop = 22;
            cellTestNamePage2.HorizontalAlignment = Element.ALIGN_CENTER;
            tableCandidateInfoPage2.AddCell(cellTestNamePage2);


            #endregion


            //Response Output
            PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();
            doc.NewPage();
            doc.Add(setBackGroundImage);
            doc.Add(tableCandidateInfo);
            doc.Add(tableCategoryStatistics);
            doc.Add(tableTestAreaComplexStatistics);

            doc.NewPage();
            doc.Add(imgPage2);
            doc.Add(tableCandidateInfoPage2);
            doc.Add(tableScoreAnalysisCategorySubject);
            doc.Add(tableScoreAnalysisTestAreaComplex);
            //doc.Add(tableCandidateInfo);
            doc.Close();

        }

        #endregion Private Methods

        #region Public Methods
        public System.Drawing.Image GetResizedImage(System.Drawing.Image image, System.Drawing.Size size)
        {
            int newWidth;
            int newHeight;

            int originalWidth = image.Width;
            int originalHeight = image.Height;
            float percentWidth = (float)size.Width / (float)originalWidth;
            float percentHeight = (float)size.Height / (float)originalHeight;
            float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
            newWidth = (int)(originalWidth * percent);
            newHeight = (int)(originalHeight * percent);

            System.Drawing.Image newImage = new System.Drawing.Bitmap(newWidth, newHeight);
            using (System.Drawing.Graphics graphicsHandle = System.Drawing.Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }
        #endregion Public Methods
    }
}