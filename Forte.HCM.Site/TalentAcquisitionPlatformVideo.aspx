﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="TalentAcquisitionPlatformVideo.aspx.cs" Inherits="Forte.HCM.Site.TalentAcquisitionPlatformVideo" %>

     <%@ MasterType VirtualPath="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Watch Our Talent Acquisition
                Platform Video
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div>
                        <!--script type="text/javascript">
            var req = swfobject.hasFlashPlayerVersion("8");
            var bookmark = args.movie ? args.movie : 0;
            if (req) {
                swfobject.embedSWF("Video/001_controller.swf", "flashcontent", "720", "595", "8", null, { csConfigFile: "Video/001_config.xml", csColor: "FFFFFF", csFilesetBookmark: bookmark }, { bgcolor: "FFFFFF", quality: "best", allowscriptaccess: "always" });
            }
        </script-->
                        <!--embed width=720 height=595 fullscreen=yes src="Video/001.swf"-->
                        <!-- intelliSPOT google Video starts here -->
                        <object width="560" height="315" standby="Loading">
                            <param name="movie" value="http://www.youtube.com/v/k0l3_kyQAxY?version=3&amp;hl=en_US">
                            </param>
                            <param name="controller" value="true" />
                            <param name="autoplay" value="false" />
                            <param name="allowFullScreen" value="true"></param>
                            <param name="allowscriptaccess" value="always"></param>
                            <embed src="http://www.youtube.com/v/k0l3_kyQAxY?version=3&amp;hl=en_US" type="application/x-shockwave-flash"
                                width="560" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>
                        <%--<iframe width="420" height="315" src="http://www.youtube.com/embed/z9ZX2nRRz1o" frameborder="0"
                            allowfullscreen></iframe>--%>
                        <!-- intelliSPOT google Video ends here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
