﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="AssessmentintelliVIEW.aspx.cs" Inherits="ForteHCMWeb.AssessmentintelliVIEW" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="keywords" content="crowdsourcing, collaborative interview, assessor, assessment, interview">
    <meta name="description" content="intelliVIEW schedules and administers collaborative candidate interviews. It can function as a stand-alone web-based interview tool or work in unison with Forte HCM family of tools. ">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> <a href="Assessment.aspx">Assessment</a>&raquo;
                intelliTEST</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div id="tab_outer">
                        <div id="tab_inner_intelliVIEW">
                            <div id="tab_intelliTEST_btn">
                                <a href="AssessmentintelliTEST.aspx">intelliTEST</a></div>
                            <div id="tab_intelliVIEW_btn">
                                intelli<span>VIEW</span></div>
                        </div>
                    </div>
                    <div>
                        <p>
                            intelliVIEW schedules and administers collaborative candidate interviews. It can
                            function as a stand-alone web-based interview tool or work in unison with ForteHCM
                            family of tools. With the ability to identify one or more assessors whose qualifications
                            are relevant to interview areas, create and save pre-defined ‘question sets,’ upload
                            content to the question repository &amp; design a report, it is the final stop in
                            the talent shortlist process. Its unique credits system helps you build your own
                            crowdsourcing!</p>
                    </div>
                </div>
                <div id="Resource_Selection_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="AssessmentintelliVIEWFeature.aspx">Features</a></li>
                        <li><a href="Downloads.aspx">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
