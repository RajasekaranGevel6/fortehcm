﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Downloads.aspx.cs" Inherits="ForteHCMWeb.Downloads" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Downloads</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div>
                        <div id="download_brochure_ctn">
                            Downloads brochures for individual products and learn more about the services and
                            products offered by Forte HCM.</div>
                        <div id="download_brochure_outer">
                            <ul>
                                <li><a href="Brochure/forteHCM_brochure.pdf" target="_blank">Talent Acquisition Infrastructure
                                </a></li>
                                <li><a href="Brochure/Forte_HCM_Inc_overview.pdf" target="_blank">Forte HCM Inc Overview
                                </a></li>
                                <li><a href="Brochure/FHCM_ProductSuite.pdf" target="_blank">FHCM Product Suite</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
