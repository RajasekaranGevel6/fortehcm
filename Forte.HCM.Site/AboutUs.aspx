﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="ForteHCMWeb.AboutUs" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
    <div id="innerpage_content_inside_outer">
      <div id="breadcrumb"><span><a href="default.aspx">Home </a>&raquo;</span> About Us</div>
      <div id="Resource_Selection_cnt_outer">
        <div id="Resource_Selection_left">
         
          <div>
            <p>Innovation is central to ForteHCM!</p>
            <p>Backed by a dynamic team with exceptional startup experience, 75+ years domain experience, premier B-school footing and sound business fundamentals, our patent pending, integrated recruitment and assessment tool provides a seamless experience.</p>
            <p>We are devoted to helping our clients make informed choices when it comes to hiring. We view ourselves as <a href="Partnership.aspx">partners</a> with our clients in Talent Acquisition and Management.</p>
            <p>But we're just  getting started, and we’ll have lots more to show you in the weeks and months  ahead. Our most amazing inventions will only happen if you collaborate with us  &amp; help us create them.</p>
          </div>
         <div><img src="Images/comment-separator.png"></div>
        </div>
        <div id="Resource_Selection_middle"><img src="Images/resourceselection_right_bg.gif"></div>
        <div id="Resource_Selection_right">
          <ul>
            <li><a href="AboutTeam.aspx">Team</a></li>
            
          </ul>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
