﻿using System;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.Trace;
using Forte.HCM.Site.SiteService;

namespace ForteHCMWeb
{
    public partial class GetInTouch : Page
    {
        /// <summary>
        /// A <see cref="FHCMSignupService"/> that refers to the web service.
        /// </summary>
        private FHCMSignupService service = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Instantiate the web service object.
                service = new FHCMSignupService();
                service.Url = ConfigurationManager.AppSettings["SITE_SERVICE_URL"];

                if (!IsPostBack)
                {
                    // Set default button and focus.
                    Page.Form.DefaultButton = GetInTouch_submitButton.UniqueID;
                    GetInTouch_firstNameTextBox.Focus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void GetInTouch_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                GetInTouch_errorLabel.Text = string.Empty;
                GetInTouch_successLabel.Text = string.Empty;

                // Check if mandatory fields are entered.
                if (!IsValidData())
                {
                    GetInTouch_errorLabel.Text = "Required fields cannot be empty";
                    GetInTouch_firstNameTextBox.Focus();
                    return;
                }

                bool sent = service.SendCorporateAccountRequest
                    (GetInTouch_firstNameTextBox.Text.Trim(), GetInTouch_lastNameTextBox.Text.Trim(), GetInTouch_emailTextBox.Text.Trim(), GetInTouch_phoneNumberTextBox.Text.Trim(), GetInTouch_companyTextBox.Text.Trim(), GetInTouch_titleTextBox.Text.Trim(), GetInTouch_additionalQuestionsTextBox.Text.Trim());

                if (sent == true)
                    GetInTouch_successLabel.Text = "Your request has been submitted and you will be contacted by the support team shortly";
                else
                    GetInTouch_errorLabel.Text = "Unable to submit your request now. Please try again later";
            }
            catch (Exception exp)
            {
                GetInTouch_errorLabel.Text = "Unable to submit your request now. Please try again later";
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that checks if the input is valid or not.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True if valid
        /// and false otherwise.
        /// </returns>
        private bool IsValidData()
        {
            if (GetInTouch_firstNameTextBox.Text.Trim().Length == 0 ||
                GetInTouch_emailTextBox.Text.Trim().Length == 0 ||
                GetInTouch_phoneNumberTextBox.Text.Trim().Length == 0 ||
                GetInTouch_titleTextBox.Text.Trim().Length == 0 ||
                GetInTouch_companyTextBox.Text.Trim().Length == 0)
            {
                return false;
            }

            return true;
        }
    }
}