﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Staffing.aspx.cs" Inherits="ForteHCMWeb.Staffing" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="keywords" content="UI, staff augmentation, opportunity. ">
    <meta name="description" content="Staff Augmentation Service, by nature, requires speed, precision, and consistency as the timeframe available for delivery is normally very limited and the candidates are required to be strong in select skill areas.">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Staffing</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="staffing_outer">
                    <div>
                        <p>
                            Staff Augmentation Service, by nature, requires speed, precision, and consistency
                            as the timeframe available for delivery is normally very limited and the candidates
                            are required to be strong in select skill areas. The critical technology needs in
                            Staff Augmentation processes are in the requirement definition, resource match,
                            resource assessment, and resource selection steps. The ForteHCM platform covers
                            all the above areas, and much more, in a user friendly UI and is built for speed
                            and precision.</p>
                        <p>
                            ForteHCM helps service providers build up qualified databases of candidates with
                            meaningful information accretion. Besides enabling fast, thorough, and precise selections,
                            ForteHCM brings longevity in to the process, building new recurring value into the
                            normal staffing process.</p>
                        <p>
                            Try us for free! Increase your opportunities & decrease your cycle time.</p>
                    </div>
                     <div class="video_icon_link">
                        <span><a href="TalentAcquisitionPlatformVideo.aspx">Watch Our Talent Acquisition Platform Video </a></span>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
