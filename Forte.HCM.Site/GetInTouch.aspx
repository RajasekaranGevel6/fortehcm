﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true"
    CodeBehind="GetInTouch.aspx.cs" Inherits="ForteHCMWeb.GetInTouch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" language="javascript">

        // Handler for onchange and onkeyup to check for max characters.
        function CommentsCount(characterLength, clientId) {
            var maxlength = new Number(characterLength);
            if (clientId.value.length > maxlength) {
                clientId.value = clientId.value.substring(0, maxlength);
            }
        }

    </script>
    <div id="popup_outer">
        <div id="popup_title_outer">
            <div id="popup_title_left">
                <img src="Images/icon_call.png" width="33" height="25"></div>
            <div id="popup_title_right">
                Let us call you or please call us at 877-377-9681</div>
            <asp:Label ID="GetInTouch_successLabel" runat="server" Text="" CssClass="success_msg_site"></asp:Label>
            <asp:Label ID="GetInTouch_errorLabel" runat="server" Text="" CssClass="error_msg_site"></asp:Label>
        </div>
        <div id="contact_form_inner">
            <div>
                <asp:Label runat="server" ID="GetInTouch_firstNameLabel" CssClass="popup_inner_txt">First Name:<span>*</span></asp:Label>
            </div>
            <div id="popup_textbox_outer">
                <asp:TextBox runat="server" ID="GetInTouch_firstNameTextBox" CssClass="text_box"
                    MaxLength="50"></asp:TextBox>
            </div>
            <div>
                <asp:TextBox runat="server" ID="GetInTouch_lastNameTextBox" CssClass="text_box" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div id="contact_form_inner">
            <div>
                <asp:Label runat="server" ID="GetInTouch_emailLabel" CssClass="popup_inner_txt">Email:<span>*</span></asp:Label></div>
            <div id="popup_textbox_outer">
                <asp:TextBox runat="server" ID="GetInTouch_emailTextBox" CssClass="text_box_width"
                    MaxLength="50"></asp:TextBox>
               <%-- <asp:RegularExpressionValidator ID="GetInTouch_emailValidator" runat="server" ControlToValidate="GetInTouch_emailTextBox"
                    ErrorMessage="You must enter a valid email address" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                </asp:RegularExpressionValidator>--%>
            </div>
        </div>
        <div id="contact_form_inner">
            <div>
                <asp:Label runat="server" ID="GetInTouch_phoneNumberLabel" CssClass="popup_inner_txt">Phone Number:<span>*</span></asp:Label>
            </div>
            <div>
                <asp:TextBox runat="server" ID="GetInTouch_phoneNumberTextBox" CssClass="text_box_width"
                    MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div id="contact_form_inner">
            <div>
                <asp:Label runat="server" ID="GetInTouch_titleLabel" CssClass="popup_inner_txt">Title:<span>*</span></asp:Label></div>
            <div>
                <asp:TextBox runat="server" ID="GetInTouch_titleTextBox" CssClass="text_box_width"
                    MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div id="contact_form_inner">
            <div>
                <asp:Label runat="server" ID="GetInTouch_companyLabel" CssClass="popup_inner_txt"> Company:<span>*</span></asp:Label></div>
            <div>
                <asp:TextBox runat="server" ID="GetInTouch_companyTextBox" CssClass="text_box_width"
                    MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div id="contact_form_inner">
            <div>
                <asp:Label runat="server" ID="GetInTouch_additionalQuestionsLabel" CssClass="popup_inner_txt">Additional Questions:</asp:Label></div>
            <div>
                <asp:TextBox runat="server" ID="GetInTouch_additionalQuestionsTextBox" Width="320px"
                    Height="80px" TextMode="MultiLine" MaxLength="200" CssClass="popup_text_area"
                    onkeyup="CommentsCount(200,this)" onchange="CommentsCount(200,this)"></asp:TextBox>
            </div>
        </div>
        <div>
            <div id="get_in_touch_Requiredfiled">
                <span>*</span> Required field</div>
            <div id="popup_cnt">
                We will use the information provided only to correspond
                <br>
                about demo and quote.</div>
        </div>
        <div id="btn_outer">
            <asp:Button ID="GetInTouch_submitButton" CssClass="submit_btn" BackColor="Transparent" BorderStyle="None" runat="server" OnClick="GetInTouch_submitButton_Click" />
            <%-- <a href="#">
                <img src="Images/btn_submit.png" alt="submit" width="112" height="32" border="0"></a>--%></div>
    </div>
</asp:Content>
