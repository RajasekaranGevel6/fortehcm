﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Whitepaper.aspx.cs" Inherits="Forte.HCM.Site.Whitepaper" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Whitepaper
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="contact_outer">
                    <div id="whitepaper_right">
                        <div id="contact_right_inner">
                            <div>
                                <asp:Label ID="Whitepaper_successLabel" runat="server" Text="" CssClass="success_msg_site"></asp:Label>
                                <asp:Label ID="Whitepaper_errorLabel" runat="server" Text="" CssClass="error_msg_site"></asp:Label>
                            </div>
                            <div id="contact_form_table">
                                <div>
                                    <asp:Label runat="server" ID="Whitepaper_firstNameLabel" CssClass="contact_right_inner_text">First Name:<span>*</span></asp:Label></div>
                                <div id="textbox_outer">
                                    <asp:TextBox ID="Whitepaper_firstNameTextBox" runat="server" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div id="contact_form_table">
                                <div>
                                    <asp:Label runat="server" ID="Whitepaper_lastNameLabel" CssClass="contact_right_inner_text">Last Name:<span>*</span></asp:Label></div>
                                <div>
                                    <asp:TextBox ID="Whitepaper_lastNameTextBox" runat="server" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div id="contact_form_table">
                                <div class="fl">
                                    <asp:Label runat="server" ID="Whitepaper_emailLabel" CssClass="contact_right_inner_text">Email:<span>*</span></asp:Label>
                                </div>
                                <div id="textbox_outer_error_msg">
                                    <asp:TextBox runat="server" ID="Whitepaper_emailTextBox" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                                <div class="fl">
                                    <asp:RegularExpressionValidator ID="Whitepaper_emailTextBoxValidator" CssClass="error_msg_validation" runat="server"
                                        ControlToValidate="Whitepaper_emailTextBox" ErrorMessage="Invalid email address"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div id="contact_form_table">
                                <div  class="fl">
                                    <asp:Label runat="server" ID="Whitepaper_phoneLabel" CssClass="contact_right_inner_text">Phone:<span>*</span></asp:Label>
                                </div>
                                <div  class="fl"> 
                                    <asp:TextBox runat="server" ID="Whitepaper_phoneTextBox" CssClass="contact_text_box"
                                        MaxLength="10"></asp:TextBox>
                                </div>
                                <div class="fl">
                                    <asp:RegularExpressionValidator ID="Whitepaper_phoneTextBoxValidator" CssClass="error_msg_validation" runat="server"
                                        ControlToValidate="Whitepaper_phoneTextBox" ErrorMessage="Invalid phone number. Phone number must be a 10 digit number without any additional character"
                                        ValidationExpression="\d{10}"> </asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div id="contact_form_table">
                                <div>
                                    <asp:Label runat="server" ID="Whitepaper_organizationLabel" CssClass="contact_right_inner_text"> Organization:<span>*</span></asp:Label>
                                </div>
                                <div id="textbox_outer">
                                    <asp:TextBox runat="server" ID="Whitepaper_organizationTextBox" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div id="contact_form_table">
                                <div>
                                    <asp:Label runat="server" ID="Whitepaper_titleLabel" CssClass="contact_right_inner_text">Title:</asp:Label></div>
                                <div>
                                    <asp:TextBox runat="server" ID="Whitepaper_titleTextBox" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div id="contact_btn_outer">
                                <asp:Button ID="Whitepaper_submitButton" runat="server" BackColor="Transparent" BorderStyle="None"
                                    CssClass="contact_btn_submit" OnClick="Whitepaper_submitButton_Click" />
                            </div>
                            <div id="Requiredfiled">
                                <span>*</span> Required field</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
