﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Workflow.aspx.cs" Inherits="ForteHCMWeb.Workflow" %>

     <%@ MasterType VirtualPath="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Workflow
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Rpo_left">
                    <div>
                        <br>
                        <img src="Images/workflow.png" width="715" height="514" border="0" usemap="#Map"
                            title="Workflow">
                        <map name="Map">
                            <area shape="rect" coords="527,44,602,150" href="ResourceSelection.aspx">
                            <area shape="rect" coords="518,208,621,299" href="AssessmentintelliTEST.aspx">
                            <area shape="rect" coords="382,209,485,300" href="AssessmentintelliVIEW.aspx">
                            <area shape="rect" coords="264,380,367,471" href="Reporting.aspx">
                        </map>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
