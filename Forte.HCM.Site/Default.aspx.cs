﻿using System;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.Trace;
using Forte.HCM.Utilities;
using Forte.HCM.Site.SiteService;


namespace ForteHCMWeb
{
    public partial class _Default : System.Web.UI.Page
    {
        FHCMSignupService service = null;

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="ipAddress">
        /// A <see cref="string"/> that holds the ip address.
        /// </param>
        private delegate void AsyncTaskDelegate(string userName, string ipAddress);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Instantiate the bridge handler service object.
                service = new FHCMSignupService();
                service.Url = ConfigurationManager.AppSettings["SITE_SERVICE_URL"];
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void Index_loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear message.
                Index_loginErrorLabel.Text = string.Empty;

                // Check if user name and password is entered.
                if (Index_userNameTextBox.Text.Trim().Length == 0 ||
                    Index_passwordTextBox.Text.Trim().Length == 0)
                {
                    Index_loginErrorLabel.Text = "User name or password cannot be empty";
                    return;
                }

                string result = service.GetUserDetail(Index_userNameTextBox.Text,
                    new EncryptAndDecrypt().EncryptString(Index_passwordTextBox.Text));
                
                
                // insert user log asynchronously.
                AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(InsertUserLog);
                IAsyncResult logResult = taskDelegate.BeginInvoke(Index_userNameTextBox.Text, GetIP4Address(),
                    new AsyncCallback(InsertUserLogCallBack), taskDelegate);
                
                if (result == null || result.Trim().Length == 0)
                {
                    Index_loginErrorLabel.Text = "Invalid username or password or you dont have privileges to access the application";
                    return;
                }

                // Extract user type and session key.
                string userType = result.Substring(0, 1);

                // Extract session key.
                string sessionKey = result.Substring(2);

                string url = string.Empty;

                if (userType == "C")
                {
                    // Compose candidate URL.
                    url = ConfigurationManager.AppSettings["CANDIDATE_URL"];
                }
                else
                {
                    // Compose user URL.
                    url = ConfigurationManager.AppSettings["USER_URL"];
                }

                // Add session key.
                url += "?sitesessionkey=" + sessionKey;

                // Redirect.
                Response.Redirect(url, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Handler method that will be called when the send button is clicked.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> that holds the event data.
        /// </param>
        /// <remarks>
        /// This will valid the user name and send to the password to the 
        /// email.
        /// </remarks>
        protected void ForgotPassword_sendPasswordButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (service.GetUserPassword(Index_userNameTextBox.Text.Trim()))
                {
                    // Show a message to the user.
                    Index_loginErrorLabel.Text = "Your password has been sent to your email";
                }
                else
                {
                    Index_loginErrorLabel.Text = "No such user name exist !";
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Handler method that acts as the callback method for insert log.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void InsertUserLogCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #region Private Methods

        /// <summary>
        /// Method to insert user log details
        /// </summary>
        /// <param name="userName">
        /// A <see cref="string"/> that holds the user name.
        /// </param>
        /// <param name="ipAddress">
        /// A <see cref="string"/> that holds the ip address.
        /// </param>
        private void InsertUserLog(string userName, string ipAddress)
        {
            try
            {
                service.InsertUserLog(userName, ipAddress);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method to get the logged user ip address
        /// </summary>
        /// <returns>
        /// A <see cref="string"/> that contains the ip address.
        /// </returns>
        private string GetIP4Address()
        {
            string IP4Address = string.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            if (IP4Address != string.Empty)
            {
                return IP4Address;
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            return IP4Address;
        }

        

        #endregion Private Methods
    }
}
