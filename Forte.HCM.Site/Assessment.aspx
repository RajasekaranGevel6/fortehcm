﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Assessment.aspx.cs" Inherits="ForteHCMWeb.Assessment" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="keywords" content="Talent intelligence, talent acquisition, quick turnaround, modular, adaptive questions, cyber proctoring, granular scoring, analytics, IT analytics, pre-employment tests, IT skills assessment, employee evaluation, job recruitment, dishonest practice, qualified database, talent infrastructure, talent funnel ">
    <meta name="description" content="Our assessment engine consists of two products: intelliTEST (online test) & intelliVIEW (collaborative interview) which are designed to work either in silos or in unison with our resource selection tool, to provide objectivity in talent selection.">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Assessment</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="ass_left">
                    <div id="ass_tab_outer">
                        <div id="tab_inner">
                            <div id="tab_intelliTEST_btn">
                                <a href="AssessmentintelliTEST.aspx">intelliTEST</a></div>
                            <div id="tab_intelliVIEW_btn">
                                <a href="AssessmentintelliVIEW.aspx">intelliVIEW</a></div>
                        </div>
                    </div>
                    <div>
                        <p>
                            Our assessment engine consists of two products: intelliTEST (online test) & intelliVIEW
                            (collaborative interview) which are designed to work either in silos or in unison
                            with our resource selection tool, to provide objectivity in talent selection. The
                            modular & scalable products help users create, schedule and administer assessments
                            to candidates. The test engine gives users 100% control over the content of the
                            test, the ability to design tests from scratch, batch or manual entry, adaptive
                            question recommendation, as well as cyber proctoring. The engine(s) capture and
                            record candidate performances at a granular level, thereby offering users the ability
                            to execute precision-searches for talent.<br>
                            <br>
                            Our customizable content is tweaked on a daily basis. While we do that, we welcome
                            you to test with us or assess your talent.</p>
                        <p>
                            Give us a try &amp; hire objectively!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
