﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="ResourceSelection.aspx.cs" Inherits="ForteHCMWeb.ResourceSelection" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="keywords" content="Online recruiting, career openings, e recruiting software, candidate management, talent management, resume database, Talent intelligence, employee screening, candidate screening, hcm, hrms, epm, target technology, weight based search, efficiency, talent sense, talent spot, talent infrastructure, Intuitive, context search, relevancy search, proximity, job search, profile search, position search, competency vectors, talent funnel. ">
    <meta name="description" content="An engaging talent mining tool that allows users to perform searches across multiple sources and obtain standardized, ranked and informative results, in a simple & rich user interface.">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Resource Selection</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div>
                        <p>
                            An engaging talent mining tool that allows users to perform searches across multiple
                            sources and obtain standardized, ranked and informative results, in a simple & rich
                            user interface. All this is done via the use of an intuitive search engine powered
                            by proprietary algorithms.
                            <br>
                            <br>
                            Designed for use by professionals such as delivery managers, hiring managers & recruiters
                            who play a critical role in hiring talent, the tool searches out user-specified
                            sources, including assessment data to assess normalized performance scores & ranks
                            them on a unified platform.</p>
                        <p>
                            Unlike traditional keyword search, IntelliSPOT’s search engine takes an intuitive
                            human-like approach & uses Contextual Search where keyword occurrences are valued
                            based on the context of their use within a resume.</p>
                        <p>
                            Give us a try & experience the proactive build of talent funnel!</p>
                    </div>
                    <div>
                        <img src="Images/comment-separator.png"></div>
                </div>
                <div id="Resource_Selection_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="ResourceSelectionFeature.aspx">Features</a></li>
                        <li><a href="ResourceSelectionAddons.aspx">Add-ons</a></li>
                        <li><a href="ResourceSelectionIntelliSPOTVideo.aspx">Product Peek </a></li>
                        <li><a href="Downloads.aspx">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
