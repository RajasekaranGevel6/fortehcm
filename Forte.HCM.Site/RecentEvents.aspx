﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="RecentEvents.aspx.cs" Inherits="ForteHCMWeb.RecentEvents" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Recent Events
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div>
                        <div>
                            <ul>
                                <li>
                                    <p>
                                       FHCM gets listed in SIIA publication  <a href="http://www.siia.net/index.php?option=com_content&view=article&id=710&Itemid=781" target="_blank">"Visions from the top"</a>. The publication is released annually at the All About the Cloud conference in San Francisco.<a name="news8"></a>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        ForteHCM wins the 2012 prestigious CODiE™ award for the <a href="http://www.siia.net/blog/index.php/2012/05/siia-announces-codie-award-winners-for-business-software-industry/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+siia%2FinkV+%28SIIA+Digital+Discourse%29
Codie award update
Friday, May 11, 2012
11:34 AM
FHCM" target="_blank">best Human Capital Management Solution.</a> The CODiE™ award recognizes excellence in the business
                                        software, digital content, and education technology industries. Through a thorough
                                        review of products and services by people who know the industry, the CODiE™ Awards
                                        allow companies to validate their products against the competition. This year 119
                                        finalists were nominated and the SIIA members then reviewed these finalists and
                                        voted to select 28 CODiE Awards Winners.<a name="news8"></a>
                                    </p>
                                </li>
                                <p>
                                    <a href="http://www.siia.net/blog/index.php/2012/05/siia-announces-codie-award-winners-for-business-software-industry/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+siia%2FinkV+%28SIIA+Digital+Discourse%29
Codie award update
Friday, May 11, 2012
11:34 AM
FHCM" target="_blank">
                                        <img src="Images/CODiE_logo_winner.png" title="CODiE" alt="CODiE" width="99" height="37"></a>
                                </p>
                                <li>
                                    <p>
                                        ForteHCM made a new release that includes enhancements to candidate dashboard, resume
                                        uploader and email templates.<a name="news7"></a>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <a href="http://www.mynewsdesk.com/us/view/pressrelease/fortehcm-named-a-2012-siia-codie-award-finalist-for-best-human-capital-management-solution-743754"
                                            target="_blank">ForteHCM Named a 2012 SIIA CODiE Award Finalist for Best Human Capital
                                            Management Solution </a><a name="news6"></a>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        The SIIA CODiE™ Awards recognize excellence in business software, digital content,
                                        and education technology industries. ForteHCM is delighted to be named finalist
                                        for " Best Human Capital Management Solution". <a name="news5"></a>
                                    </p>
                                </li>
                                <p>
                                    <a href="http://www.siia.net/codies/2012/finalist_detail.asp?id=53" target="_blank">
                                        <img src="Images/CODiE_logo.png" title="CODiE" alt="CODiE" width="99" height="37"></a>
                                </p>
                                <li>
                                    <p>
                                        ForteHCM releases its offline interview module which lets clients to schedule and
                                        administer interviews using intuitive and user-friendly tools.<a name="news4"></a>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        ForteHCM's new video on Talent Acquisition platform briefs on building a qualified
                                        talent pool to be competitive in today's marketplace. <a name="news3"></a>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        ForteHCM to present at LEHRN (Leading Edge Human Resource Network) on its talent
                                        mining tool and integration with assessment data. The event will take place at Thirvent
                                        Financials HQ in Minneapolis.<a name="news2"></a></p>
                                </li>
                                <li>
                                    <p>
                                        ForteHCM will debut at HR Tech conference (Oct 3rd & 4th). Come visit us at booth
                                        # 1253.<a name="news1"></a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
