﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="AssessmentintelliTESTFeature.aspx.cs" Inherits="ForteHCMWeb.AssessmentintelliTESTFeature" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_ass_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> <a href="Assessment.aspx">Assessment</a>
                &raquo; <a href="AssessmentintelliTEST.aspx">intelliTEST</a> &raquo; Features
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div class="outer_screenshort">
                        <div id="Resource_Selection_left_img">
                            <div id="Automated_Test">
                                <div id="Automated_Test_title">
                                    Automated Test Generation</div>
                                <div id="Automated_Test_cnt">
                                    Too bored to create your own test? Let our engine suggest & create one for you.</div>
                            </div>
                        </div>
                        <div id="Resource_Selection_left2">
                            <div id="Flexibility_in_Content_Uploads">
                                <div id="Flexibility_in_Content_Uploads_title">
                                    Flexibility in Content Uploads</div>
                                <div id="Flexibility_in_Content_Uploads_cnt">
                                    You could either upload your tests one by one or
                                    <br>
                                    upload it in batch.</div>
                            </div>
                        </div>
                    </div>
                    <div class="outer_screenshort">
                        <div id="Resource_Selection_left_img">
                            <div id="Question_summary">
                                <div id="Collaborative_title">
                                    Collaborative Contribution</div>
                                <div id="Collaborative_cnt">
                                    See who is contributing, award them and build your own crowd!</div>
                            </div>
                        </div>
                        <div id="Resource_Selection_left2">
                            <div id="Test_segments">
                                <div id="Skills_Dissection_title">
                                    Skills Dissection</div>
                                <div id="Skills_Dissection_cnt">
                                    Questions based on category, test areas and segments<br>
                                    that lets you pick on specifics.</div>
                            </div>
                        </div>
                    </div>
                    <div id="Adaptive_Test_Authoring">
                        <div id="Adaptive_Test_Authoring_title">
                            Adaptive Test Authoring</div>
                        <div id="Adaptive_Test_Authoring_cnt">
                            You pick a few; the system listen to you and recommends the rest</div>
                    </div>
                    <div id="Summary_of_Objectives">
                        <div id="Summary_of_Objectives_title">
                            Summary of Objectives</div>
                        <div id="Summary_of_Objectives_cnt">
                            Pop up extenders , test summary, complexity & much more-decisive information in
                            a pleasing UI</div>
                    </div>
                    <div class="outer_screenshort">
                        <div id="Resource_Selection_left_img">
                            <div id="test_session">
                                <div id="test_session_title">
                                    Candidate Zone</div>
                            </div>
                        </div>
                        <div id="Resource_Selection_left2">
                            <div id="certification">
                                <div id="certification_title">
                                    Push out tests with specific instructions</div>
                                <div id="certification_cnt">
                                    In addition to skills testing by organizations, certification agencies benefit greatly
                                    by our rich framework.</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <img src="Images/comment-separator.png" width="751" height="6"></div>
                </div>
                <div id="Resource_Selection_ass_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="AssessmentintelliVIEW.aspx">intelliVIEW</a></li>
                        <li><a href="AssessmentintelliTESTAddons.aspx">Add-ons</a></li>
	 <li><a href="AssessmentintelliTESTVideo.aspx">Product Peek </a></li>
                        <li><a href="Downloads.aspx">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
