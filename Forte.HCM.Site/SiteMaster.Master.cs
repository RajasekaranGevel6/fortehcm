﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ForteHCMWeb
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void HighlightItem(string key)
        {
            switch (key)
            {
                case "RESOURCESELECTION":
                    SiteMaster_topResourceSelectionHyperLink.CssClass = "active";
                    //SiteMaster_bottomResourceSelectionHyperLink.CssClass = "active";
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_.png";
                    
                    break;

                case "ASSESSMENT":
                    SiteMaster_topAssessmentHyperLink.CssClass = "active";
                    //SiteMaster_bottomAssessmentHyperLink.CssClass = "active";
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_assessment.png";
                    break;

                case "ENTERPRISETALENT":
                    SiteMaster_topEnterpriseTalentHyperLink.CssClass = "active";
                    //SiteMaster_bottomEnterpriseTalentHyperLink.CssClass = "active";
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_enterprise.png";
                    break;

                case "RPO":
                    SiteMaster_topRPOHyperLink.CssClass = "active";
                    //SiteMaster_bottomRPOHyperLink.CssClass = "active";
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_RPO.png";

                    break;

                case "STAFFING":
                    SiteMaster_topStaffingHyperLink.CssClass = "active";
                    //SiteMaster_bottomStaffingyperLink.CssClass = "active";
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_Staffing.png";

                    break;

                case "CANDIDATES":
                    //SiteMaster_topStaffingHyperLink.CssClass = "active";
                    //SiteMaster_bottomStaffingyperLink.CssClass = "active";
                    SiteMaster_topCandidateHyperLink.CssClass = "active";
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_candidates.png";

                    break;


                case "ABOUTUS":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_about.png";
                    break;


                case "TEAM":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_team.png";
                    break;


                case "ASSESSMENTINTELLITEST":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_intellites.png";
                    break;

                case "ASSESSMENTINTELLIVIEW":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_intelliVIEW.png";
                    break;


                case "FEATURES":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_features.png";
                    break;

                case "ADDONS":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_Add-ons.png";
                    break;


                case "CONTACTUS":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_contact_Us.png";
                    break;

                case "DOWNLOADS":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_download.png";
                    break;


                case "FEATUREANDPRICING":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_pricing.png";
                    break;


                case "SIGNUP":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_sign.png";
                    break;

                case "PARTNERSHIP":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_Partnershi.png";
                    break;


                case "RECENTEVENTS":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_recent_eve.png";
                    break;


                case "REPORTING":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_reporting.png";
                    break;             

                case "WORKFLOW":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_workflow.png";
                    break;


                case "INTELLISPOTVIDEO":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_product_pe.png";
                    break;

                case "INTELLITESTVIDEO":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_product_pe.png";
                    break;

                case "WHITEPAPER":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_Whitepaper.png";
                    break;

                case "TALENT":
                    SiteMaster_headerImage.ImageUrl = "Images/title_inner_page_talent.png";
                    break;

            }
        }
    }
}
