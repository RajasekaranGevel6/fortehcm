﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="ContactUs.aspx.cs" Inherits="ForteHCMWeb.ContactUs" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Contact Us
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="contact_outer">
                    <div id="contact_left">
                        <div id="contact_outer_cnt">
                            <b>Forte HCM Inc</b><br>
                            2500 W. Higgins Rd, #870,
                            <br>
                            Hoffman Estates, IL- 60169<br>
                            <strong>Phone: </strong>877-377-9681<br>
                            <strong>Fax:</strong> 847-841-3796<br>
                            <strong>Email:</strong> <a href="mailto:hello@fortehcm.com">hello@fortehcm.com</a>
                        </div>
                    </div>
                    <div id="contact_right">
                        <div id="contact_right_inner">
                            <div>
                                <asp:Label ID="ContactUs_successLabel" runat="server" Text="" CssClass="success_msg_site"></asp:Label>
                                <asp:Label ID="ContactUs_errorLabel" runat="server" Text="" CssClass="error_msg_site"></asp:Label>
                            </div>
                            <div id="contact_form_table">
                                <div>
                                    <asp:Label runat="server" ID="ContactUs_firstNameLabel" CssClass="contact_right_inner_text">First Name:<span>*</span></asp:Label></div>
                                <div id="textbox_outer">
                                    <asp:TextBox ID="ContactUs_firstNameTextBox" runat="server" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div id="contact_form_table">
                                <div>
                                    <asp:Label runat="server" ID="ContactUs_lastNameLabel" CssClass="contact_right_inner_text">Last Name:<span>*</span></asp:Label></div>
                                <div>
                                    <asp:TextBox ID="ContactUs_lastNameTextBox" runat="server" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                                <div>
                                    <asp:Label runat="server" ID="ContactUs_organizationLabel" CssClass="contact_right_inner_text"> Organization:<span>*</span></asp:Label>
                                </div>
                                <div id="textbox_outer">
                                    <asp:TextBox runat="server" ID="ContactUs_organizationTextBox" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                            <div id="contact_form_table">
                                <div>
                                    <asp:Label runat="server" ID="ContactUs_reasonLabel" CssClass="contact_right_inner_text">Reason:<span>*</span></asp:Label></div>
                                <div>
                                    <asp:DropDownList ID="ContactUs_reasonDropDownList" CssClass="dropdown_list" runat="server">
                                        <asp:ListItem Text="Partnerships" Value="Partnerships" Selected="True" />
                                        <asp:ListItem Text="Feedback" Value="Feedback" />
                                        <asp:ListItem Text="Additional info on products" Value="AdditonalInfo" />
                                        <asp:ListItem Text="Custom quote" Value="CustomQuote" />
                                    </asp:DropDownList>
                                </div>
                                <div>
                                    <asp:Label runat="server" ID="ContactUs_emailLabel" CssClass="contact_right_inner_text">Email:<span>*</span></asp:Label>
                                </div>
                                <div id="textbox_outer_error_msg">
                                    <asp:TextBox runat="server" ID="ContactUs_emailTextBox" CssClass="contact_text_box"
                                        MaxLength="100"></asp:TextBox>
                                </div>
                                <div>
                                    <asp:RegularExpressionValidator ID="ContactUs_emailTextBoxValidator" CssClass="error_msg_validation" runat="server"
                                        ControlToValidate="ContactUs_emailTextBox" ErrorMessage="Invalid email address"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div id="contact_form_table">
                                <div class="fl">
                                    <asp:Label runat="server" ID="ContactUs_phoneLabel" CssClass="contact_right_inner_text">Phone:<span>*</span></asp:Label></div>
                                <div class="fl">
                                    <asp:TextBox runat="server" ID="ContactUs_phoneTextBox" CssClass="contact_text_box"
                                        MaxLength="10"></asp:TextBox>
                                </div>
                                <div class="fl">
                                    <asp:RegularExpressionValidator ID="ContactUs_phoneTextBoxValidator" CssClass="error_msg_validation" runat="server"
                                        ControlToValidate="ContactUs_phoneTextBox" ErrorMessage="Invalid phone number. Phone number must be a 10 digit number without any additional character"
                                        ValidationExpression="\d{10}"> </asp:RegularExpressionValidator>
                                </div>
                        </div>
                        <div id="contact_btn_outer">
                            <asp:Button ID="ContactUs_submitButton" runat="server" BackColor="Transparent" BorderStyle="None"
                                CssClass="contact_btn_submit" OnClick="ContactUs_submitButton_Click" />
                            <asp:Button ID="ContactUs_clearButton" runat="server" BackColor="Transparent" BorderStyle="None"
                                CssClass="btn_clear" OnClientClick="javascript: location.href = location.href;" />
                            <%--<a href="#">
                                    <img src="Images/contact_btn_submit.png" width="78" height="24" title="submit" border="0"></a><a
                                        href="#"><img src="Images/btn_clear.png" border="0"></a>--%></div>
                        <div id="Requiredfiled">
                            <span>*</span> Required field</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
