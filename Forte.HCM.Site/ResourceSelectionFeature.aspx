﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="ResourceSelectionFeature.aspx.cs" Inherits="ForteHCMWeb.ResourceSelectionFeature" %>

     <%@ MasterType VirtualPath="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_fea_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> <a href="ResourceSelection.aspx">
                    Resource Selection</a> &raquo; Features
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div class="outer_screenshort">
                        <div id="Resource_Selection_left_img">
                            <div id="Choice_of_Views">
                                <div id="Choice_of_Views_title">
                                    Choice of Views</div>
                                <div id="Choice_of_Views_cnt">
                                    You know all about the traditional Grid View which gives tabular list down. Have
                                    you tried our Pan View yet? It allows viewing of similar talent profiles clustered
                                    together. Give us a try!</div>
                            </div>
                        </div>
                        <div id="Resource_Selection_left2">
                            <div id="Innovative_UI">
                                <div id="Innovative_UI_title">
                                    Innovative UI</div>
                                <div id="Innovative_UI_cnt">
                                    Resource-spheres based on matching scores are
                                    <br>
                                    positioned on a gradient display with the highest<br>
                                    scorer(s) in the center. The distance from the
                                    <br>
                                    center is a direct visual indicator of the level
                                    <br>
                                    of match.</div>
                            </div>
                        </div>
                    </div>
                    <div id="Weight_based_search">
                        <div id="Weight_based_search_title">
                            Weight based search</div>
                        <div id="Weight_based_search_cnt">
                            Precision search based on assigned weights.</div>
                        <div id="Weight_based_search_cnt_bottom">
                            Automatic balance allocation of weights</div>
                    </div>
                    <div id="Talent_search">
                        <div id="Talent_search_title">
                            Integrated Search</div>
                        <div id="Integrated_Search_cnt">
                            Talent search integrated with assessment data qualifies & evaluates talent objectively.</div>
                    </div>
                    <div id="skils_screenshort">
                        <div id="Resource_Selection_left_img">
                            <div id="Skills_and_Recency">
                                <div id="Skills_and_Recency_title">
                                    Skills and Recency</div>
                                <div id="Skills_and_Recency_cnt">
                                </div>
                                <div id="Skills_and_Recency_cnt">
                                    Wouldn't it be nice to know how
                                    <br>
                                    recent is the skill and how it
                                    <br>
                                    breaks down?</div>
                            </div>
                        </div>
                        <div id="Resource_Selection_right_img">
                            <div id="Resource_Cloning">
                                <div id="Resource_Cloning_title">
                                    Resource Cloning</div>
                                <div id="Resource_Cloning_cnt">
                                    Identify resources similar to target candidate profile which takes into consideration
                                    variety of factors including competencies, domain background, roles played ..etc.
                                    Isn't it so similar to cloning someone? We decided to call it just that!</div>
                            </div>
                        </div>
                    </div>
                    <div id="Detailed_Summary">
                        <div id="Detailed_Summary_title">
                            Candidate Summary</div>
                        <div id="Detailed_Summary_cnt">
                            A summary that gives ratio along with skills breakdown histogram.</div>
                    </div>
                    <div>
                        <img src="Images/comment-separator.png" width="751" height="6"></div>
                </div>
                <div id="Resource_Selection_fea_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="ResourceSelectionAddons.aspx">Add-ons</a></li>
                         <li><a href="ResourceSelectionIntelliSPOTVideo.aspx">Product Peek </a></li>
                        <li><a href="Downloads.aspx">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
