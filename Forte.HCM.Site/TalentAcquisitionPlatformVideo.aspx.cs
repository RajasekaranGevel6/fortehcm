﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Forte.HCM.Site
{
    public partial class TalentAcquisitionPlatformVideo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.HighlightItem("TALENT");
        }
    }
}