﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="ReportingFeature.aspx.cs" Inherits="ForteHCMWeb.ReportingFeature" %>

    <%@ MasterType VirtualPath="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer_RPO">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> <a href="Reporting.aspx">Reporting</a>
                &raquo; Features
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div id="reporting_features">
                        <div id="reporting_features_title">
                            Design a Report</div>
                        <div id="reporting_features_cnt">
                            Everybody talks about customization these days. We talk about it too but we go a
                            step further by designing a custom-build report(s) with only those elements that
                            add value in your decision making process. Features like Candidate Compare & Group
                            Analysis let us you compare performances of a select number of candidates in a specific
                            test or combined performance of a group of candidates.</div>
                    </div>
                    <div>
                        <img src="Images/comment-separator.png"></div>
                </div>
                <div id="Resource_Selection_middle_rpo">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="Downloads.aspx">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
