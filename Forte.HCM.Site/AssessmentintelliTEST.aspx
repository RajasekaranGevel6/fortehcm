﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="AssessmentintelliTEST.aspx.cs" Inherits="ForteHCMWeb.AssessmentintelliTEST" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="description" content="intelliTEST plays an important role in the Forte HCM suite of products by providing a platform for users to create, schedule and administer online assessments to candidates through the use of an intuitive and user-friendly interface. ">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> <a href="Assessment.aspx">Assessment</a>
                &raquo; intelliTEST
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div id="tab_outer">
                        <div id="tab_inner_intelliTEST">
                            <div id="tab_intelliTEST_btn">
                                intelli<span>TEST</span></div>
                            <div id="tab_intelliVIEW_btn">
                                <a href="AssessmentintelliVIEW.aspx">intelliVIEW</a></div>
                        </div>
                    </div>
                    <div>
                        <p>
                            intelliTEST plays an important role in the ForteHCM suite of products by providing
                            a platform for users to create, schedule and administer online assessments to candidates
                            through the use of an intuitive and user-friendly interface. Integrated with resource
                            selection (intelliSPOT) &amp; the report center, it displays candidate performances
                            at a granular level &amp; executes precision-searches.</p>
                    </div>
                </div>
                <div id="Resource_Selection_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="AssessmentintelliTESTFeature.aspx">Features</a></li>
                        <li><a href="AssessmentintelliTESTAddons.aspx">Add-ons</a></li>
                        <li><a href="AssessmentintelliTESTVideo.aspx">Product Peek </a></li>
                        <li><a href="Downloads.ASPX">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
