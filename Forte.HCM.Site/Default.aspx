﻿<%@ Page Title="Forte HCM" Language="C#" MasterPageFile="~/IndexMaster.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ForteHCMWeb._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="keywords" content="Talent acquisition platform, recruitment process outsourcing, IT staffing, body shopping, online test, collaborative interview, job interviews, job testing, IT skills assessment, talent search, precision search, keyword search, staffing solutions, rpo solutions, adaptive testing, pre-employment assessment, post-employment assessment, skills testing, talent pool. ">
    <meta name="description" content="A talent mining tool that integrates with assessment data & engages users to search based on position profile or weights; compare candidate profiles, view standardized & ranked results.">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div id="main_container">
            <div id="header_outer">
                <div id="header_inner">
                    <div id="header_left_logo">
                        <a href="default.aspx">
                            <img src="Images/logo.png" alt="Forte HCM" title="FORTE HCM" width="123" height="69"
                                border="0" /></a></div>
                    <div id="header_right">
                        <!-- customized login script starts here -->
                        <div class="ddpanel">
                            <div id="mypanelcontent" class="ddpanelcontent">
                                <div id="loginpadding_top">
                                </div>
                                <div id="login_title">
                                    Login</div>
                                <div id="login_form_outer">
                                    <div id="login_form_username">
                                        User Name:</div>
                                    <div id="login_form_texbox">
                                        <asp:TextBox ID="Index_userNameTextBox" runat="server" MaxLength="50" CssClass="login_form_texbox_inner"></asp:TextBox>
                                    </div>
                                    <div id="login_form_password">
                                        Password:</div>
                                    <div id="login_form_texbox">
                                        <asp:TextBox ID="Index_passwordTextBox" runat="server" MaxLength="50" TextMode="Password"
                                            CssClass="login_form_texbox_inner"></asp:TextBox>
                                    </div>
                                    <div id="login_form_btn">
                                        <asp:Button ID="Index_loginButton" runat="server" Text="Login" OnClick="Index_loginButton_Click"
                                            CssClass="login_btn_bg" BackColor="Transparent" BorderStyle="None" />
                                    </div>
                                    <div id="error_msg">
                                        <asp:Label ID="Index_loginErrorLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="login_btn_outer">
                                <div id="mypaneltab" class="ddpaneltab">
                                    <a href="#"><span></span></a>
                                </div>
                                <div id="mypaneltab" class="ddpaneltab_free_trail">
                                    <a href="Signup.aspx"><span></span></a>
                                </div>
                                <%--  <div id="Div1" class="ddpaneltab_candidate">
                                    <a href="#"><span></span></a>
                                </div>--%>
                            </div>
                        </div>
                        <!-- customized login script ends here -->
                        <div id="index_main_menu">
                            <ul>
                                <li><a href="#" class="active">Home</a></li>
                                <li><a href="ResourceSelection.aspx">Resource Selection</a></li>
                                <li><a href="Assessment.aspx">Assessment </a></li>
                                <li><a href="EnterpriseTalent.aspx">Enterprise Talent</a></li>
                                <li><a href="RPO.aspx">RPO</a></li>
                                <li><a href="Staffing.aspx">Staffing</a></li>
                                <li><a href="Candidates.aspx">Candidates</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="header_banner_outer">
                    <!-- header banner slider starts here -->
                    <div id="slider">
                        <div id="bg_color">
                            <div id="container">
                                <div id="example">
                                    <!--img src="Images/BannerImages/new-ribbon.png" width="112" height="112" alt="New Ribbon" id="ribbon"-->
                                    <div id="slides">
                                        <div class="slides_container">
                                            <div class="slide">
                                                <a href="http://www.siia.net/blog/index.php/2012/05/siia-announces-codie-award-winners-for-business-software-industry/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+siia%2FinkV+%28SIIA+Digital+Discourse%29
Codie award update
Friday, May 11, 2012
11:34 AM
FHCM" target="_blank" title="Best Human Capital Management Solution">
                                                    <img src="Images/BannerImages/slide-0.png" width="900" height="340" title="Objective Hiring! Made Simple"
                                                        alt="Objective Hiring! Made Simple" border="0"></a>
                                            </div>
                                            <div class="slide">
                                                <img src="Images/BannerImages/slide-1.png" width="900" height="340" title="Objective Hiring! Made Simple"
                                                    alt="Objective Hiring! Made Simple" border="0">
                                            </div>
                                            <div class="slide">
                                                <a href="ResourceSelection.aspx" title="Talent Search">
                                                    <img src="Images/BannerImages/slide-2.png" width="900" height="340" alt="Talent Search"
                                                        border="0" title="Talent Search"></a>
                                            </div>
                                            <div class="slide">
                                                <a href="Assessment.aspx" title="Assessment">
                                                    <img src="Images/BannerImages/slide-3.png" width="900" height="340" title="Assessment"
                                                        alt="Assessment"></a>
                                            </div>
                                            <div class="slide">
                                                <a href="Reporting.aspx" title="Reporting">
                                                    <img src="Images/BannerImages/slide-4.png" width="900" height="340" title="Reporting"
                                                        alt="Reporting"></a>
                                            </div>
                                            <div class="slide">
                                                <a href="RPO.aspx" title="RPO">
                                                    <img src="Images/BannerImages/slide-5.png" title="Talent Infrastructure" width="900"
                                                        height="340" alt="Talent Infrastructure">
                                                </a>
                                            </div>
                                        </div>
                                        <a href="#" class="prev">
                                            <img src="Images/BannerImages/arrow-prev.png" width="24" height="43" alt="Arrow Prev"
                                                border="0"></a> <a href="#" class="next">
                                                    <img src="Images/BannerImages/arrow-next.png" width="24" height="43" alt="Arrow Next"
                                                        border="0"></a>
                                    </div>
                                    <!--img src="Images/BannerImages/example-frame.png" width="739" height="341" alt="Example Frame" id="frame"-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- header banner slider ends here -->
                </div>
            </div>
            <div id="middle_bg">
                <div id="middle_btn_outer">
                    <div id="middle_btn_inner">
                        <a href="Workflow.aspx">
                            <img src="Images/btn_view.png" width="236" title="Learn More" alt="Learn More" height="57"
                                border="0" /></a></div>
                    <div id="middle_btn_inner_right">
                        <a href="Signup.aspx">
                            <img src="Images/btn_sign.png" width="236" height="57" title="Sign Up Today!" alt="Sign Up Today!"
                                border="0" /></a></div>
                    <!--
	<div class="fl pR10">
      <div class="middle_btn_left"><a href="#"><img src="Images/btn_left_curve.png" width="27" height="59" border="0" /></a></div>
      <div class="middle_btn_bg">View The Demo</div>
      <div class="middle_btn_right"><a href="#"><img src="Images/btn_right_curve.png" width="27" height="59" border="0" /></a></div>
	  </div>
	  <div class="fl">
      <div class="middle_btn_left"><a href="#"><img src="Images/btn_left_curve.png" width="27" height="59" border="0" /></a></div>
      <div class="middle_btn_bg">Sign Up Today! </div>
      <div class="middle_btn_right"><a href="#"><img src="Images/btn_right_curve.png" width="27" height="59" border="0" /></a></div>
	  </div>
-->
                </div>
            </div>
            <div id="content_area">
                <!--content resource area start here-->
                <div id="cnt_outer">
                    <div id="cnt_left_outer">
                        <div id="cnt_left_outer_title">
                            <a href="ResourceSelection.aspx">Resource Selection</a></div>
                        <div id="resource_cnt">
                            <a href="ResourceSelection.aspx">
                                <img src="Images/icon_resource.png" name="resource_left_icon" border="0" align="left"
                                    id="resource_left_icon" title="Resource Selection" alt="Resource Selection" /></a>A
                            talent mining tool that integrates with assessment data & engages users to search
                            based on position profile or weights; compare candidate profiles, view standardized
                            & ranked results.
                        </div>
                        <div align="right">
                            <a href="ResourceSelection.aspx">
                                <img src="Images/btn_readmore.png" title="Read More" width="78" height="24" border="0" /></a></div>
                    </div>
                    <div id="cnt_left_outer_border">
                        &nbsp;</div>
                    <div id="cnt_left_outer">
                        <div id="ass_title">
                            <a href="Assessment.aspx">Assessment</a></div>
                        <div id="resource_cnt">
                            <a href="Assessment.aspx">
                                <img src="Images/icon_Assessment.png" name="resource_left_icon" width="66" height="66"
                                    border="0" align="left" id="resource_left_icon" title="Assessment" alt="Assessment" /></a>An
                            assessment platform with a multitude of features like adaptive test authoring, customized
                            tests, cyber- proctoring, collaborative interview, granular scoring &amp; performance
                            reporting across tests.
                        </div>
                        <div align="right">
                            <a href="Assessment.aspx">
                                <img src="Images/btn_readmore.png" title="Read More" width="78" height="24" border="0" /></a></div>
                    </div>
                    <div id="cnt_left_outer_border">
                        &nbsp;</div>
                    <div id="cnt_left_outer">
                        <div id="rpo_title">
                            <a href="Reporting.aspx">Reporting</a></div>
                        <div id="resource_cnt">
                            <a href="Reporting.aspx">
                                <img src="Images/icon_Reporting.png" name="resource_left_icon" width="66" height="66"
                                    border="0" align="left" id="resource_left_icon" title="Reporting" alt="Reporting" /></a>Custom
                            reports that add value in the decision making process with features like candidate
                            compare & group analysis: used to compare candidate performances in a specific test
                            or generate reports that are representative of the combined performances of the
                            select group.
                        </div>
                        <div align="right" id="w280">
                            <a href="Reporting.aspx">
                                <img src="Images/btn_readmore.png" title="Read More" width="78" height="24" border="0" /></a></div>
                    </div>
                </div>
                <!--content resource area End here-->
                <div id="cnt_outer">
                    <div>
                        <div id="Something_About_cnt">
                            <div id="about_title">
                                <a href="AboutUs.aspx">Something About Us</a></div>
                            <div id="Something_About_inner">
                                <a href="AboutUs.aspx">
                                    <img src="Images/icon_Something.png" width="67" height="66" border="0" align="left"
                                        id="Something_About_left" title="Something About Us" alt="Something About Us" /></a>We
                                are a Talent acquisition platform company. In addition to helping our clients build
                                a qualified talent funnel, our patent pending platform with features like contextual
                                search, proactive build, customizable assessments &amp; cyber proctoring puts objectivity
                                back in hiring!&nbsp;
                            </div>
                            <div align="right">
                                <a href="AboutUs.aspx">
                                    <img src="Images/btn_readmore.png" width="78" title="Read More" height="24" border="0" /></a></div>
                        </div>
                        <div id="cnt_left_outer_border">
                        </div>
                        <div id="Download_outer">
                            <div id="download_title">
                                <a href="Downloads.aspx">Download</a>s</div>
                            <div id="Download_inner">
                                <a href="Downloads.aspx">
                                    <img src="Images/icon_Download.png" name="Download_inner_left" width="67" height="66"
                                        border="0" align="left" id="Download_inner_left" title="Downloads" alt="Downloads" /></a>
                                Learn more about our products and benefits.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="offer_strip_outer">
            <div id="offer_strip_inner">
                <div id="offer_strip_inner_left">
                    Qualified talent pool for Enterprises, RPO's, System Integrators, Staffing firms
                    & YOU!</div>
            </div>
            <div id="arrow">
            </div>
        </div>
        <div id="footer_main_outer">
            <div id="footer_strip">
                <div id="footer_inner">
                    <div id="footer_inner_top">
                        <div id="footer_left">
                            <div id="about_team_title">
                                <a href="RecentEvents.aspx">Recent Events</a></div>
                            <div id="footer_left_inner">
                                <div id="footer_right_arrow">
                                    <script type="text/javascript">

                                        /***********************************************
                                        * Cross browser Marquee II- © Dynamic Drive (www.dynamicdrive.com)
                                        * This notice MUST stay intact for legal use
                                        * Visit http://www.dynamicdrive.com/ for this script and 100s more.
                                        ***********************************************/

                                        var delayb4scroll = 2000 //Specify initial delay before marquee starts to scroll on page (2000=2 seconds)
                                        var marqueespeed = 1 //Specify marquee scroll speed (larger is faster 1-10)
                                        var pauseit = 1 //Pause marquee onMousever (0=no. 1=yes)?

                                        ////NO NEED TO EDIT BELOW THIS LINE////////////

                                        var copyspeed = marqueespeed
                                        var pausespeed = (pauseit == 0) ? copyspeed : 0
                                        var actualheight = ''

                                        function scrollmarquee() {
                                            if (parseInt(cross_marquee.style.top) > (actualheight * (-1) + 8)) //if scroller hasn't reached the end of its height
                                                cross_marquee.style.top = parseInt(cross_marquee.style.top) - copyspeed + "px" //move scroller upwards
                                            else //else, reset to original position
                                                cross_marquee.style.top = parseInt(marqueeheight) + 8 + "px"
                                        }

                                        function initializemarquee() {
                                            cross_marquee = document.getElementById("vmarquee")
                                            cross_marquee.style.top = 0
                                            marqueeheight = document.getElementById("marqueecontainer").offsetHeight
                                            actualheight = cross_marquee.offsetHeight //height of marquee content (much of which is hidden from view)
                                            if (window.opera || navigator.userAgent.indexOf("Netscape/7") != -1) { //if Opera or Netscape 7x, add scrollbars to scroll and exit
                                                cross_marquee.style.height = marqueeheight + "px"
                                                cross_marquee.style.overflow = "scroll"
                                                return
                                            }
                                            setTimeout('lefttime=setInterval("scrollmarquee()",30)', delayb4scroll)
                                        }

                                        if (window.addEventListener)
                                            window.addEventListener("load", initializemarquee, false)
                                        else if (window.attachEvent)
                                            window.attachEvent("onload", initializemarquee)
                                        else if (document.getElementById)
                                            window.onload = initializemarquee


                                    </script>
                                    <div id="marqueecontainer" onmouseover="copyspeed=pausespeed" onmouseout="copyspeed=marqueespeed">
                                        <div id="vmarquee" style="position: absolute">
                                            <!--YOUR SCROLL CONTENT HERE-->
                                            » FHCM gets listed in SIIA publication” Visions from the top”. The publication is
                                            released annually at the All About the Cloud conference in San Francisco.
                                            <br />
                                            <br />
                                            » ForteHCM wins the 2012 prestigious CODiE™ award for the best Human Capital Management
                                            Solution. The CODiE™ award recognizes excellence in the business software, digital
                                            content, and education technology industries. Through a thorough review of products
                                            and services by people who know the industry, the CODiE™ Awards allow companies
                                            to validate their products against the competition. This year 119 finalists were
                                            nominated and the SIIA members then reviewed these finalists and voted to select
                                            28 CODiE Awards Winners.
                                            <br />
                                            <br />
                                            » ForteHCM made a new release that includes enhancements to candidate dashboard,
                                            resume uploader and email templates.
                                            <br />
                                            <br />
                                            » ForteHCM Named a 2012 SIIA CODiE Award Finalist for Best Human Capital Management
                                            Solution
                                            <br />
                                            <br />
                                            » The SIIA CODiE™ Awards recognize excellence in business software, digital content,
                                            and education technology industries. ForteHCM is delighted to be named finalist
                                            for " Best Human Capital Management Solution".
                                            <br />
                                            <br />
                                            <!--YOUR SCROLL CONTENT HERE-->
                                            » ForteHCM releases its offline interview module which lets clients to schedule
                                            and administer interviews using intuitive and user-friendly tools.
                                            <br />
                                            <br />
                                            <!--YOUR SCROLL CONTENT HERE-->
                                            » ForteHCM's new video on Talent Acquisition platform briefs on building a qualified
                                            talent pool to be competitive in today's marketplace.
                                            <br />
                                            <br />
                                            <!--YOUR SCROLL CONTENT HERE-->
                                            » ForteHCM to present at LEHRN (Leading Edge Human Resource Network) on its talent
                                            mining tool and integration with assessment data. The event will take place at Thirvent
                                            Financials HQ in Minneapolis.
                                            <br />
                                            <br />
                                            <!--YOUR SCROLL CONTENT HERE-->
                                            » ForteHCM will debut at HR Tech conference (Oct 3rd & 4th). Come visit us at booth
                                            # 1253.
                                            <br />
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <a href="RecentEvents.aspx">Read more</a></div>
                        </div>
                        <div id="about_team">
                            <div id="about_team_title">
                                <a href="AboutUs.aspx">About Us</a></div>
                            <div id="about_team_inner_cnt">
                                We are devoted to helping our clients make informed choices when it comes to hiring...</div>
                            <div>
                                <a href="AboutUs.aspx">Read more</a></div>
                        </div>
                        <div id="Pricing_team">
                            <div id="about_team_title">
                                <a href="FeaturesAndPricing.aspx">Features and Pricing</a></div>
                            <div id="Pricing_team_inner_cnt">
                                Learn about our account types,<br>
                                features and pricing…</div>
                            <div>
                                <a href="FeaturesAndPricing.aspx">Read more</a></div>
                        </div>
                    </div>
                    <div id="footer_inner_bottom">
                        <div id="Partnership_team">
                            <div id="about_team_title">
                                <a href="Partnership.aspx">Partnership</a></div>
                            <div id="Partnership_team_inner_cnt">
                                Partnering with ForteHCM can help you<br />
                                to increase revenue, build your brand...</div>
                            <div>
                                <a href="Partnership.aspx">Read more</a></div>
                        </div>
                        <div id="contact_team">
                            <div id="contact_team_title">
                                <a href="ContactUs.aspx">Contact Us</a></div>
                            <div id="contact_team_inner_cnt">
                                <b>Forte HCM Inc</b><br>
                                2500 W. Higgins Rd, #870,
                                <br>
                                Hoffman Estates, IL- 60169<br>
                                <img src="Images/icon_telephone.png">: 877-377-9681<br>
                                <img src="Images/icon_fax.png">: 847-841-3796<br>
                                <img src="Images/icon_mail.png">: <a href="mailto:hello@fortehcm.com">hello@fortehcm.com</a>
                            </div>
                        </div>
                        <div id="contact_team">
                            <div id="contact_team_title">
                                <a href="Whitepaper.aspx">Whitepaper</a></div>
                            <div id="contact_team_inner_cnt">
                                Towards a New State in Talent
                                <br />
                                Acquisition
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%-- <div id="twitter_bottom_outer">
            <div id="twitter_bottom_inner">
            </div>
        </div>--%>
        <div id="footer_bottom">
            <div id="footer_bottom_inner">
                <div id="footer_bottom_inner_SPLIT">
                    <%-- <a href="http://www.siia.net/codies/2012/finalist_detail.asp?id=53" target="_blank">
                        <img src="Images/CODiE_logo.png" title="CODiE" alt="CODiE" width="99" height="37"></a>--%>
                </div>
                <div id="footer_bottom_inner_SPLIT_right">
                    <div id="twitter_bottom_inner_left">
                        Follow us on :</div>
                    <div id="twitter_bottom_inner_right">
                        <a href="http://twitter.com/#!/ForteHCM" target="_blank">
                            <img src="Images/icon_twitter.png" title="Twitter" alt="Twitter" width="27" height="21"></a>
                        <a href="RSSFile.xml" target="_blank">
                            <img src="Images/icon_reader.png" title="Rss Reader" alt="Rss Reader" width="28"
                                height="22"></a> <a href="http://www.linkedin.com/company/forte-hcm-inc" target="_blank">
                                    <img src="Images/linkedin.png" alt="Linkedin" width="22" height="22" title="Linkedin"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
