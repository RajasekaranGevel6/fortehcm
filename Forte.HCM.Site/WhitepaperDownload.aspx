﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="WhitepaperDownload.aspx.cs" Inherits="Forte.HCM.Site.WhitepaperDownload" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Whitepaper Downloads</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div>
                        <div id="download_brochure_outer">
                            <ul>
                                <li><a href="Whitepaper/TalentAcquisition.pdf" target="_blank">Towards a New State in
                                    Talent Acquisition </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
