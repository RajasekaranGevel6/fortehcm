﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Candidates.aspx.cs" Inherits="ForteHCMWeb.Candidate" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Candidates</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div>
                        <p>
                            Companies haven't been able to fill open jobs despite the huge number of candidates available in the market.
                            What it does point to is that there is a shortage of skills and not people. Resume
                            overload furthers the hiring problem. Now is the time to showcase your skills and
                            gain an edge over your peers. Our testing tool captures and records your test performance(s)
                            at a granular level, thereby offering employers quick access to pre-screened talent.
                            <br>
                            <br>
                            Your test scores will be available online for a period of 4 months. Using the link
                            we provide, you could either download the results or choose to publish the link.
                            Any potential employer can click on the link and gain access to your detailed scores.</p>
                        <div id="highlight_txt">
                            <a href="http://candidate.fortehcm.com/ForteHCMCandidate/signin.aspx">Are you ready to show case your skills?</a></div>
                    </div>
                    <div>
                        <img src="Images/comment-separator.png"></div>
                </div>
                <div id="Resource_Selection_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="http://candidate.fortehcm.com/ForteHCMCandidate/signin.aspx" target="_blank">
                            Login</a></li>
                        <li><a href="http://candidate.fortehcm.com/ForteHCMCandidate/signup.aspx" target="_blank">
                            Sign up</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
