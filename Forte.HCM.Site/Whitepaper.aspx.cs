﻿using System;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.Trace;
using Forte.HCM.Site.SiteService;

namespace Forte.HCM.Site
{
    public partial class Whitepaper : Page
    {
        /// <summary>
        /// A <see cref="FHCMSignupService"/> that refers to the web service.
        /// </summary>
        private FHCMSignupService service = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.HighlightItem("WHITEPAPER");

                // Instantiate the web service object.
                service = new FHCMSignupService();
                service.Url = ConfigurationManager.AppSettings["SITE_SERVICE_URL"];

                if (!IsPostBack)
                {
                    // Set default button and focus.
                    Page.Form.DefaultButton = Whitepaper_submitButton.UniqueID;
                    Whitepaper_firstNameTextBox.Focus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void Whitepaper_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                Whitepaper_errorLabel.Text = string.Empty;
                Whitepaper_successLabel.Text = string.Empty;

                // Check if mandatory fields are entered.
                if (!IsValidData())
                {
                    Whitepaper_errorLabel.Text = "Required fields cannot be empty";
                    Whitepaper_firstNameTextBox.Focus();
                    return;
                }

                bool sent = true;
                    //service.SendWhitepaperDownloadRequest(
                    //Whitepaper_firstNameTextBox.Text.Trim(),
                    //Whitepaper_lastNameTextBox.Text.Trim(),
                    //Whitepaper_emailTextBox.Text.Trim(),
                    //Whitepaper_phoneTextBox.Text.Trim(),
                    //Whitepaper_organizationTextBox.Text.Trim(),
                    //Whitepaper_titleTextBox.Text.Trim());

                if (sent == true)
                {
                    // Redirect to white paper download page.
                    Response.Redirect("~/WhitepaperDownload.aspx", false);
                }
                else
                    Whitepaper_errorLabel.Text = "Unable to submit your request now. Please try again later";
            }
            catch (Exception exp)
            {
                Whitepaper_errorLabel.Text = "Unable to submit your request now. Please try again later";
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that checks if the input is valid or not.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True if valid
        /// and false otherwise.
        /// </returns>
        private bool IsValidData()
        {
            if (Whitepaper_firstNameTextBox.Text.Trim().Length == 0 ||
                Whitepaper_lastNameTextBox.Text.Trim().Length == 0 ||
                Whitepaper_emailTextBox.Text.Trim().Length == 0 ||
                Whitepaper_phoneTextBox.Text.Trim().Length == 0 ||
                Whitepaper_organizationTextBox.Text.Trim().Length == 0)
            {
                return false;
            }

            return true;
        }
    }
}