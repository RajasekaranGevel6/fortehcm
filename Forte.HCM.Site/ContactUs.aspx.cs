﻿using System;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.Trace;
using Forte.HCM.Site.SiteService;

namespace ForteHCMWeb
{
    public partial class ContactUs : Page
    {
        /// <summary>
        /// A <see cref="FHCMSignupService"/> that refers to the web service.
        /// </summary>
        private FHCMSignupService service = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Master.HighlightItem("CONTACTUS");

                // Instantiate the web service object.
                service = new FHCMSignupService();
                service.Url = ConfigurationManager.AppSettings["SITE_SERVICE_URL"];

                if (!IsPostBack)
                {
                    // Set default button and focus.
                    Page.Form.DefaultButton = ContactUs_submitButton.UniqueID;
                    ContactUs_firstNameTextBox.Focus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void ContactUs_submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                ContactUs_errorLabel.Text = string.Empty;
                ContactUs_successLabel.Text = string.Empty;

                // Check if mandatory fields are entered.
                if (!IsValidData())
                {
                    ContactUs_errorLabel.Text = "Required fields cannot be empty";
                    ContactUs_firstNameTextBox.Focus();
                    return;
                }

                bool sent = service.SendContactRequest(
                    ContactUs_firstNameTextBox.Text.Trim(), 
                    ContactUs_lastNameTextBox.Text.Trim(), 
                    ContactUs_organizationTextBox.Text.Trim(), 
                    ContactUs_reasonDropDownList.SelectedItem.Text.Trim(),
                    ContactUs_emailTextBox.Text.Trim(), 
                    ContactUs_phoneTextBox.Text.Trim());

                if (sent == true)
                    ContactUs_successLabel.Text = "Your request has been submitted and you will be contacted by the support team shortly";
                else
                    ContactUs_errorLabel.Text = "Unable to submit your request now. Please try again later";
            }
            catch (Exception exp)
            {
                ContactUs_errorLabel.Text = "Unable to submit your request now. Please try again later";
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that checks if the input is valid or not.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True if valid
        /// and false otherwise.
        /// </returns>
        private bool IsValidData()
        {
            if (ContactUs_firstNameTextBox.Text.Trim().Length == 0 ||
                ContactUs_lastNameTextBox.Text.Trim().Length == 0 ||
                ContactUs_organizationTextBox.Text.Trim().Length == 0 ||
                ContactUs_reasonDropDownList.SelectedItem == null ||
                ContactUs_reasonDropDownList.SelectedItem.Text.Trim().Length == 0 ||
                ContactUs_emailTextBox.Text.Trim().Length == 0 ||
                ContactUs_phoneTextBox.Text.Trim().Length == 0)
            {
                return false;
            }

            return true;
        }
    }
}