﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;

using Forte.HCM.Trace;
using Forte.HCM.Utilities;
using Forte.HCM.Site.SiteService;

namespace Forte.HCM.Site.UI
{
    public partial class Login : System.Web.UI.Page
    {
        FHCMSignupService service = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Instantiate the bridge handler service object.
            service = new FHCMSignupService();
            service.Url = ConfigurationManager.AppSettings["SITE_SERVICE_URL"];
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string result = service.GetUserDetail(txtUser.Text, 
                    new EncryptAndDecrypt().EncryptString(txtPassword.Text));

                if (result == null || result.Trim().Length == 0)
                {
                    lblFailure.Text = "Invalid username or password or you dont have privileges to access the application";
                    return;
                }

                // Extract user type and session key.
                string userType = result.Substring(0, 1);

                // Extract session key.
                string sessionKey = result.Substring(2);

                string url = string.Empty;

                if (userType == "C")
                {
                    // Compose candidate URL.
                    url = ConfigurationManager.AppSettings["CANDIDATE_URL"];
                }
                else
                {
                    // Compose user URL.
                    url = ConfigurationManager.AppSettings["USER_URL"];
                }

                // Add session key.
                url += "?sessionKey=" + sessionKey;

                // Redirect.
                Response.Redirect(url, false);
            }
            catch (Exception exp)
            {
                lblFailure.Text = exp.Message;
                Logger.ExceptionLog(exp);
            }
        }

        protected void btnSendCorporateRequest_Click(object sender, EventArgs e)
        {
            try
            {
                bool sent = service.SendCorporateAccountRequest
                    ("Harshadha", "Mithrah", "harshadha.mithrah@gmail.com", 
                    "9840612210", "Mithrah Terrain", "Mithrah Terrain", "N/A");

                if (sent == true)
                    lblSuccess.Text = "A corporate account request was made successfully. You will be contacted by the ForteHCM admin shortly";
                else
                    lblFailure.Text = "Unable to send the request now. Please try again later";
            }
            catch (Exception exp)
            {
                lblFailure.Text = exp.Message;
                Logger.ExceptionLog(exp);
            }
        }
    }
}