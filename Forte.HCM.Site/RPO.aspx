﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="RPO.aspx.cs" Inherits="ForteHCMWeb.RPO" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="keywords" content="Talent Management, Metrics, Bottom Line, Resource clone, ATS, RPO, Objectivity. ">
    <meta name="description" content="Forte HCM's breakthrough technology in Talent Management significantly improves the internal operational metrics, quality of deliverables, and top & bottom-line performance for RPO service providers. ">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> RPO</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Rpo_left">
                    <div>
                        <p>
                            ForteHCM's breakthrough technology in Talent Management significantly improves the
                            internal operational metrics, quality of deliverables, and top & bottom-line performance
                            for RPO service providers.
                        </p>
                        <p>
                            Its desktop utility & proactive build of the resume repository saves time for recruiters
                            seeking new candidates. It also ranks and categorizes the profiles, thereby qualifying
                            them objectively.</p>
                        <p>
                            Features like “Resource Clone” enables recruiters to identify resources similar
                            to a target profile, through a series of techniques that take into consideration
                            factors like resource competencies across skills, domain background, the nature
                            of roles played by the resource across past projects & resource performance in specialized
                            assessment(s) that may have been administered. The integrated assessment engine
                            helps recruiters eliminate domain bandwidth limitations.</p>
                        <p>
                            As a modular, hosted and scalable system the ForteHCM platform can serve both small
                            and large RPO service providers and can be integrated with standard Applicant Tracking
                            Systems or provide limited ATS features required for the complete recruitment process.
                        </p>
                        <p>
                            Try us for free and call us for partnership!</p>
                    </div>
                    <div class="video_icon_link">
                        <span><a href="TalentAcquisitionPlatformVideo.aspx">Watch Our Talent Acquisition Platform Video </a></span>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
