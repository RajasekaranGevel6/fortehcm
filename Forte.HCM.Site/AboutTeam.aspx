﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="AboutTeam.aspx.cs" Inherits="ForteHCMWeb.AboutTeam" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
    <div id="innerpage_content_inside_outer">
      <div id="breadcrumb"><span><a href="default.aspx">Home </a>&raquo; <a href="AboutUs.aspx">About Us  </a></span>&raquo; Team </div>
      <div id="Resource_Selection_cnt_outer">
        <div id="Resource_Selection_left">
         
				  
				  <div id="cnt_left_outer_team">
         <img src="Images/team_Herald.png" title="Herald" alt="Herald" width="137" height="183"  id="resource_left_icon_team" ><span id="cnt_left_outer_title_part">Herald Manjooran</span>      
		<p>Herald is the visionary behind Forte HCM Inc. With nearly three decades of IT industry experience,his drive for real solution need in Talent Acquisition led to the research and development effort that resulted in Forte HCM. Herald founded DivIHN Integration Inc., a software services organization, in 2002. Prior to that he served as Business Unit heads for IT services  organizations in the US, Saudi Arabia, and India. Herald believes in strong organizational values and treats relationships, collaboration, and innovation as key attributes for health and growth. He has an MBA (High Honors) from University of Chicago, and another MBA (first rank) and BS (college first) from, India.</p>
        </div>  
		
		 <div id="cnt_left_outer_team"> <img src="Images/team_DanPepper.png" title="Dan Pepper" alt="Dan Pepper"  id="resource_left_icon_team" ><span id="cnt_left_outer_title_part">Dan Pepper</span>     
		
            <p>Dan is a startup guru and brings more than three decades of enterprise software sales and marketing experience. It was when he left Oracle to join the startup nCUBE in 1994 that he began toiling in the startup arena, and never looked back. All 11 startups with which he has been involved in senior leadership are either still going concerns or enjoyed successful exits, including three IPO’s. Dan has held key management positions with technology leaders including Oracle, Dun & Bradstreet, and GE, as well as enterprise software start-ups, including Versant, PureEdge, BenefitAmerican, AppStream and Responsys and takes on the role of marketing head at Forte HCM Inc.</p>
        </div>  
		
		
		<div id="cnt_left_outer_team"><img src="Images/team_KarthicAthreya.png" title="Karthic Athreya" alt="Karthic Athreya" width="132" height="183" id="resource_left_icon_team" ><span id="cnt_left_outer_title_part">Karthic Athreya</span>      
		<p>As head of business operations, Karthic brings two decades of IT services, in both the US and India, BPO, Telecom & Product management experience. He is a team player with expertise in managing culturally diverse teams, lead software services & consulting engagements & has played a role in many startups and in his most recent, revenue and employee strength grew multi fold. He holds an MBA from Kellogg, Masters in Applied Electronics & undergraduate degree in Electronics from India.  </p>        
		</div>  
		
		<div id="cnt_left_outer_team">
         <img src="Images/team_PaaulSelvakummar.png" title="Paaul Selvakummar" alt="Paaul Selvakummar" width="132" height="183" id="resource_left_icon_team" ><span id="cnt_left_outer_title_part">Paaul Selvakummar</span>  
		
            <p>Paaul is focused on the technical aspects of the business including design and delivery. He has over 10 years in product management and research & development. Over the last several years he has played variety of techno-commercial roles in the HCM space. His past work includes conceptualization and development of a novel state-space descriptive approach to structured knowledge representation leading to an intelligent text understanding system named TUNSTAR, and implementation of a neural network framework that derives the strengths of multiple networks, for applications that require forecasting and decision-making. Paaul has a Masters in Electrical & Computer Engineering from the University of Illinois at Chicago, and is the recipient of the J. Watumull meritorious award. He holds an undergraduate degree in Electronics Engineering from Anna University (India) and graduated with a first class with distinction.</p>
        </div> 
		
		<div id="cnt_left_outer_team">
         <img src="Images/team_MarkDickelman.png" alt="Mark Dickelman " width="132" height="183" title="Mark Dickelman"  id="resource_left_icon_team" ><span id="cnt_left_outer_title_part">Mark Dickelman </span>      
		<p>To say Mark has a creative, inventive mind is an understatement. He has received or applied for no less than a dozen patents during his career & advises ForteHCM team on innovation. Most recently, as head of Innovation R & D for U.S. Bank, he led the launch of Social Media both externally and internally for the company’s 61,000 employees. Previously he headed U.S. Bank’s business technology delivery group.  Prior to this he was CTO for Anexsys LLC, a payments processing subsidiary of JP Morgan Chase. Mark has also served as Vice-President of Mobile Commerce with Bank of Montreal, having previously led a number of e-commerce and internet ventures for Bank of Montreal and its subsidiary Harris Bank.
He earned his BS Computer Science for the U of I College of Engineering.</p>
        </div>  
		  
		  
		
          <DIV><img src="Images/comment-separator.png"></DIV>
        </div>
        <div id="Resource_Selection_middle_team"><img src="Images/resourceselection_right_bg.gif"></div>
        <div id="Resource_Selection_right">
          <ul>
            <li><a href="AboutUs.aspx">About Us</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
