﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Reporting.aspx.cs" Inherits="ForteHCMWeb.Reporting" %>

     <%@ MasterType VirtualPath="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Reporting</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div>
                        <p>
                            The report center is the go to destination for all assessment reporting. The “Design
                            a Report” feature leverages the granular score storage mechanism &amp; offers flexibility
                            to design and customize reports. And it offers categorized performance metrics.
                            In addition, features like “Candidate Compare” &amp; “Group Analysis” facilitates
                            the user to compare candidate performance in a select test or across groups.</p>
                    </div>
                    <div>
                        <img src="Images/comment-separator.png"></div>
                </div>
                <div id="Resource_Selection_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="ReportingFeature.aspx">Features</a></li>
                        <li><a href="Downloads.aspx">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
