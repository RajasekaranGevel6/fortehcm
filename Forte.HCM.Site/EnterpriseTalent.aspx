﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="EnterpriseTalent.aspx.cs" Inherits="ForteHCMWeb.EnterpriseTalent" %>
<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="innerpage_content_outer">
    <div id="innerpage_content_inside_outer">
      <div id="breadcrumb"><span><a href="default.aspx">Home </a>&raquo;</span> Enterprise Talent</div>
      <div id="Resource_Selection_cnt_outer">
        <div id="staffing_outer">
         
          <div>
            <p>ForteHCM provides enterprises with a revolutionary platform for Talent Discovery, Assurance and Calibration. The innovative ForteHCM platform enables discovery of well-matched and qualified talent, provides reliable and objective predictors of future success, and enables calibration of capabilities for ongoing development and deployment of the resources. </p>
            <p>The granularity and width of performance information captured by ForteHCM products lead to meaningful comparisons in selection and useful pointers in development and resource management. The ForteHCM platform thus adds unique value throughout the employment or contract life of the knowledge professional with the enterprise, spanning the phases of resource identification and selection, resource inventory, resource development planning, ongoing assessment for development, and resource deployment planning.  </p>
            <p>In addition to the discovery, assurance, and calibration, the ForteHCM platform opens up collaborative learning possibilities and furthers the resource development objectives of the enterprise.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</asp:Content>
