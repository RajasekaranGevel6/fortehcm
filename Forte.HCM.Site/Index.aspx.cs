﻿using System;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.Trace;
using Forte.HCM.Utilities;
using Forte.HCM.Site.SiteService;
using System.Web;
using System.Web.Security;


namespace ForteHCMWeb
{
    public partial class Index : Page
    {
        FHCMSignupService service = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Instantiate the bridge handler service object.
                service = new FHCMSignupService();
                service.Url = ConfigurationManager.AppSettings["SITE_SERVICE_URL"];

                //If logged out from application
                if(!string.IsNullOrEmpty(Request.QueryString["a"]))
                    if(Convert.ToInt32(Request.QueryString["a"])==1)
                        Response.Cookies["FORTEHCM_SITE_DETAILS"].Expires.AddDays(-1);

                RetrieveCookies();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Event that handles/calls the login process method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Index_loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                TryLogin();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method handles login process
        /// </summary>
        private void TryLogin()
        {
            try
            {
                // Clear message.
                Index_loginErrorLabel.Text = string.Empty;

                // Check if user name and password is entered.
                if (Index_userNameTextBox.Text.Trim().Length == 0 ||
                    Index_passwordTextBox.Text.Trim().Length == 0)
                {
                    Index_loginErrorLabel.Text = "User name or password cannot be empty";
                    return;
                }

                string result = service.GetUserDetail(Index_userNameTextBox.Text,
                    new EncryptAndDecrypt().EncryptString(Index_passwordTextBox.Text));

                if (result == null || result.Trim().Length == 0)
                {
                    Index_loginErrorLabel.Text = "Invalid username or password or you dont have privileges to access the application";
                    return;
                }

                // Extract user type and session key.
                string userType = result.Substring(0, 1);

                // Extract session key.
                string sessionKey = result.Substring(2);

                string url = string.Empty;

                if (userType == "C")
                {
                    // Compose candidate URL.
                    url = ConfigurationManager.AppSettings["CANDIDATE_URL"];
                }
                else
                {
                    // Compose user URL.
                    url = ConfigurationManager.AppSettings["USER_URL"];
                }

                // Add session key.
                url += "?sitesessionkey=" + sessionKey;

                // Try to add the cookie information.
                if (Index_rememberMeCheckBox.Checked)
                {
                    try
                    {
                        HttpCookie cookie = Request.Cookies["FORTEHCM_SITE_DETAILS"];
                        if (cookie == null)
                        {
                            cookie = new HttpCookie("FORTEHCM_SITE_DETAILS");
                        }

                        cookie["FHCM_SITE_USER_NAME"] = Index_userNameTextBox.Text;
                        cookie["FHCM_SITE_PASSWORD"] = Index_passwordTextBox.Text;

                        cookie.Expires = DateTime.Now.AddMonths
                            (Convert.ToInt32(ConfigurationManager.AppSettings["REMEMBER_ME_MONTHS"]));
                        Response.AppendCookie(cookie);
                    }
                    catch (Exception exp)
                    {
                        Logger.ExceptionLog(exp);
                    }
                }

                // Redirect.
                FormsAuthentication.SetAuthCookie(Index_userNameTextBox.Text.Trim(), false);
                Response.Redirect(url, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }


        /// <summary>
        /// Method that retreives the user name and password form cookie and 
        /// tries to login using that.
        /// </summary>
        private void RetrieveCookies()
        {
            try
            {
                //// Check if not post back and user already logged in.
                if (IsPostBack)
                    return;

                // Try to get cookie information.
                HttpCookie cookie = Request.Cookies["FORTEHCM_SITE_DETAILS"];

                if (cookie != null)
                {
                    string userName = cookie["FHCM_SITE_USER_NAME"];
                    string password = cookie["FHCM_SITE_PASSWORD"];

                    if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                    {
                        // Assign user name and password.
                        Index_userNameTextBox.Text = userName;
                        Index_passwordTextBox.Text = password;

                        // Try to login.
                        TryLogin();
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }
    }
}