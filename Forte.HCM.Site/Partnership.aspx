﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Partnership.aspx.cs" Inherits="ForteHCMWeb.Partnership" %>

     <%@ MasterType VirtualPath="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> Partnership</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left_partners">
                    <div id="cnt_left_outer_part">
                        <div>
                            <p>
                                Forte HCM Inc is a Talent Acquisition Infrastructure Company. Its goal is to help clients
                                build a qualified talent funnel across industries and skills. That's why we seek
                                to partner with complementary businesses, certification agencies, vendor management
                                systems and consulting partners selected for their expertise in the human capital
                                management industry. The combination of partner expertise and ForteHCM’s innovative
                                technology provides a unique opportunity for the partner to enhance the overall
                                value proposition in assisting clients with talent management needs.</p>
                        </div>
                        <div id="resource_cnt_part">
                            <p>
                                <img src="Images/icon_strategic_consulting.png" alt="Strategic Consulting" title="Strategic Consulting"
                                    width="104" height="109" align="left" id="resource_left_icon_part" /><span id="cnt_left_outer_title_part">
                                        Strategic Consulting </span>
                            </p>
                            HR consulting firms play a vital role in building a global HR strategy & talent
                            funnel for their clients. This in collaboration with the ForteHCM suite of products
                            will enhance process efficiencies, cost-effectiveness, cycle time to hire and retention
                            of top talent throughout the organization.
                        </div>
                        <div id="cnt_left_outer_border">
                            &nbsp;</div>
                        <div id="resource_cnt_part">
                            <p>
                                <img src="Images/icon_certifying_agencies.png" alt="Certifying agencies" width="108"
                                    height="87" align="left" id="resource_left_icon_part" title="Certifying agencies" /><span
                                        id="cnt_left_outer_title_part"> Certifying agencies </span>
                            </p>
                            Certifying agencies greatly benefit by using ForteHCM’s assessment platform. With rich
                            features like Customized Tests, Randomized Questions, Cyber Proctoring, Absolute/Relative
                            Scoring, Rich Reporting & Hosted Model, certifying agencies can leave the test administration
                            part to us and worry about other issues. By using our framework, overall cost is
                            reduced & user experience is greatly enhanced.
                        </div>
                        <div id="resource_cnt_fullwidth">
                            <p>
                                <img src="Images/Vendor_Management.png" alt="Strategic Consulting" name="resource_left_icon_part"
                                    align="left" id="resource_left_icon_part" title="Strategic Consulting" /></p>
                            <span id="cnt_left_outer_title_part">Vendor Management Systems </span>
                            <p>
                                VMS firms deliver value by being their client’s single touch point. With increasingly
                                more vendors competing against each other, it becomes increasing difficult for the
                                VMS firms to dissect information based on performance data of their vendors & recommend
                                the best ones to their clients.
                            </p>
                            <span id="cnt_left_outer_title_part">ForteHCM can help! </span>
                            <p>
                                Using its proprietary algorithms, ForteHCM can obtain standardized, ranked and informative
                                results, in a simple & rich user interface for the candidate profiles submitted
                                by vendors.
                            </p>
                        </div>
                        <div id="cnt_left_outer_title_part_01">
                            Call us or <u><a href="mailto:partner@fortehcm.com">email</a></u> to become a ForteHCM partner
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
