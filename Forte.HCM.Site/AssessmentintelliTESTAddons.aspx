﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="AssessmentintelliTESTAddons.aspx.cs" Inherits="ForteHCMWeb.AssessmentintelliTESTAddons" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_add-ons_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> <a href="Assessment.aspx">Assessment</a>
                &raquo; <a href="AssessmentintelliTEST.aspx">intelliTEST</a> &raquo; Add-ons
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div class="outer_screenshort">
                        <div id="Resource_Selection_left_img">
                            <div id="Client_Management">
                                <div id="Client_Management_title">
                                    Client Management</div>
                                <div id="Client_Management_cnt">
                                    Features to add client deptartments, contacts and attributes to manage/track job
                                    requests.</div>
                            </div>
                        </div>
                        <div id="Resource_Selection_left2">
                            <div id="Nomenclature_Customization">
                                <div id="Nomenclature_Customization_title">
                                    Nomenclature Customization</div>
                                <div id="Nomenclature_Customization_cnt">
                                    Nomenclature customization based on client, client
                                    <br>
                                    departments and client contacts.</div>
                            </div>
                        </div>
                    </div>
                    <div class="outer_screenshort">
                        <div id="Resource_Selection_left_img">
                            <div id="Position_profile">
                                <div id="Position_profile_title">
                                    Position profile</div>
                                <div id="Position_profile_cnt">
                                    Search based on uploaded job order/position profile which can be created from scratch.
                                    If you would much rather search using key words, assign weights to skills to enable
                                    precision search</div>
                            </div>
                        </div>
                        <div id="Resource_Selection_left2">
                            <div id="Activity_Log">
                                <div id="Activity_Log_title">
                                    Activity Log</div>
                                <div id="Activity_Log_cnt">
                                    Progress based on position profile informs both<br>
                                    client and vendor on activities & allows them to make<br>
                                    informed decisions.</div>
                            </div>
                        </div>
                    </div>
                    <div id="Resume_Uploader">
                        <div id="Resume_Uploader_title">
                            Resume Uploader</div>
                        <div id="Resume_Uploader_cnt">
                            Desktop utility that constantly listens for new resumes being added.</div>
                    </div>
                    <div>
                        <img src="Images/comment-separator.png" width="751" height="6"></div>
                </div>
                <div id="Resource_Selection_middle_add-ons">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="AssessmentintelliVIEW.aspx">intelliVIEW</a></li>
                        <li><a href="AssessmentintelliTESTFeature.aspx">Features</a></li>
                        <li><a href="AssessmentintelliTESTVideo.aspx">Product Peek </a></li>
                        <li><a href="Downloads.aspx">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
