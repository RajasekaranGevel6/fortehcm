﻿using System;
using System.Web.UI;
using System.Configuration;

using Forte.HCM.Trace;
using Forte.HCM.Site.SiteService;

namespace ForteHCMWeb
{
    public partial class Signup : Page
    {
        /// <summary>
        /// A <see cref="FHCMSignupService"/> that refers to the web service.
        /// </summary>
        private FHCMSignupService service = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Instantiate the web service object.
                service = new FHCMSignupService();
                service.Url = ConfigurationManager.AppSettings["SITE_SERVICE_URL"];
                Master.HighlightItem("SIGNUP");
                if (!IsPostBack)
                {
                    // Set default button and focus.
                    Page.Form.DefaultButton = Signup_continueButton.UniqueID;
                    Signup_firstNameTextBox.Focus();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        protected void Signup_continueButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear messages.
                Signup_successLabel.Text = string.Empty;
                Signup_errorLabel.Text = string.Empty;

                // Check if mandatory fields are entered.
                if (!IsValidData())
                {
                    Signup_errorLabel.Text = "Required fields cannot be empty";
                    Signup_firstNameTextBox.Focus();
                    return;
                }

                // Check if passwords match.
                if (Signup_passwordTextBox.Text != Signup_confirmPasswordTextBox.Text)
                {
                    Signup_errorLabel.Text = "Passwords do not match";
                    Signup_passwordTextBox.Focus();
                    return;
                }

                // Check if user agree to terms and conditions.
                if (!Signup_agreeCheckBox.Checked)
                {
                    Signup_errorLabel.Text = "You must agree to the terms of services to proceed";
                    Signup_agreeCheckBox.Focus();
                    return;
                }

                // Get subscription ID.
                int subscriptionID = 1;
                if (Signup_businessRadioButton.Checked)
                    subscriptionID = 2;

                // Create the user details.
                string result = service.CreateAccount(Signup_firstNameTextBox.Text.Trim(),
                    Signup_lastNameTextBox.Text.Trim(), Signup_emailTextBox.Text.Trim(), Signup_passwordTextBox.Text.Trim(), Signup_yourPhoneTextBox.Text.Trim(), subscriptionID);

                switch (result)
                {
                    case "1":
                        Signup_successLabel.Text = "Your account has been created and activation mail sent your email";
                        break;

                    case "2":
                        Signup_errorLabel.Text = "Account already exist for this email";
                        break;

                    case "3":
                        Signup_successLabel.Text = "Your account has been created. For activation please enail to " +
                            ConfigurationManager.AppSettings["ADMIN_EMAIL_ADDRESS"];
                        break;

                    case "4":
                        Signup_errorLabel.Text = "Unable to submit your request now. Please try again later";
                        break;
                }
            }
            catch (Exception exp)
            {
                Signup_errorLabel.Text = "Unable to submit your request now. Please try again later";
                Logger.ExceptionLog(exp);
            }
        }

        /// <summary>
        /// Method that checks if the input is valid or not.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True if valid
        /// and false otherwise.
        /// </returns>
        private bool IsValidData()
        {
            if (Signup_firstNameTextBox.Text.Trim().Length == 0 ||
                Signup_lastNameTextBox.Text.Trim().Length == 0 ||
                Signup_emailTextBox.Text.Trim().Length == 0 ||
                Signup_passwordTextBox.Text.Trim().Length == 0 ||
                Signup_confirmPasswordTextBox.Text.Trim().Length == 0)
            {
                return false;
            }

            return true;
        }

     }

}