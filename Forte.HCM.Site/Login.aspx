﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Forte.HCM.Site.UI.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmLogin" runat="server">
    <div>
    <table style="width: 100%">
    <tr>
        <td colspan="2"> 
            <asp:Label ID="lblSuccess" runat="server" Text="" ForeColor="DarkGreen"></asp:Label>
            <asp:Label ID="lblFailure" runat="server" Text="" ForeColor="Red"></asp:Label>
        </td>
    </tr>
        <tr>
            <td>
                <asp:Label ID="lblUser" runat="server" Text="User"></asp:Label>
            </td>
             <td>
                <asp:TextBox ID="txtUser" runat="server" ></asp:TextBox>                
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
            </td>
             <td>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>                
            </td>
        </tr>
        <tr>    
            <td>
                <asp:Button ID="btnLogin" runat="server" Text="Login" 
                    onclick="btnLogin_Click" />
            </td>
            <td>
            <asp:Button ID="btnSendCorporateRequest" runat="server" 
                    Text="Send Corporate Request" onclick="btnSendCorporateRequest_Click" 
                   />
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
