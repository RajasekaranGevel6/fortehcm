﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PopupMaster.Master" AutoEventWireup="true"
    CodeBehind="PricingFreeTrial.aspx.cs" Inherits="ForteHCMWeb.PricingFreeTrial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Pricing_cnt">
        <p><strong>What happens after the free trial? </strong></p>
        <p>
            You get 15 days to try all the features of ForteHCM Business. At the end of 15
            days, you will need to move to a paid plan to continue using all the features. If
            you decide not to continue, you can still use ForteHCM Professional with limited
            functionalities and will still be able to access everything you created.</p>
    </div>
</asp:Content>
