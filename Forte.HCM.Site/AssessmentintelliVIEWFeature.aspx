﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="AssessmentintelliVIEWFeature.aspx.cs" Inherits="ForteHCMWeb.AssessmentintelliVIEWFeature" %>

    <%@ MasterType VirtualPath="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_ass_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> <a href="Assessment.aspx">Assessment</a>
                &raquo; <a href="AssessmentintelliVIEW.aspx">intelliVIEW</a> &raquo; Features
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div class="outer_screenshort">
                        <img src="Images/intelliVIEW_screen_1.png" width="751" height="321" title="intelliVIEW_Features"><img
                            src="Images/intelliVIEW_screen_3.png" title="intelliVIEW_Features"></div>
                    <div>
                        <img src="Images/comment-separator.png" width="751" height="6"></div>
                </div>
                <div id="Resource_Selection_intelliVIEW_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="AssessmentintelliTEST.aspx">intelliTEST</a></li>
                        <li><a href="Downloads.aspx">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
