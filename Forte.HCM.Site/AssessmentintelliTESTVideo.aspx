﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="AssessmentintelliTESTVideo.aspx.cs" Inherits="ForteHCMWeb.AssessmentintelliTESTVideo" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo;</span> <a href="Assessment.aspx">Assessment</a>
                &raquo; <a href="AssessmentintelliTEST.aspx">intelliTEST</a> &raquo; Product Peek
            </div>
            <div id="Resource_Selection_cnt_outer">
                <div id="Resource_Selection_left">
                    <div>
                        <!--script type="text/javascript">
            var req = swfobject.hasFlashPlayerVersion("8");
            var bookmark = args.movie ? args.movie : 0;
            if (req) {
                swfobject.embedSWF("Video/001_controller.swf", "flashcontent", "720", "595", "8", null, { csConfigFile: "Video/001_config.xml", csColor: "FFFFFF", csFilesetBookmark: bookmark }, { bgcolor: "FFFFFF", quality: "best", allowscriptaccess: "always" });
            }
        </script-->
                        <object width="420" height="315">
                            <param name="movie" value="http://www.youtube.com/v/Yb63qduvxWU?version=3&amp;hl=en_US">
                            </param>
                            <param name="allowFullScreen" value="true"></param>
                            <param name="allowscriptaccess" value="always"></param>
                            <embed src="http://www.youtube.com/v/Yb63qduvxWU?version=3&amp;hl=en_US" type="application/x-shockwave-flash"
                                width="420" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>
                        <%--<iframe width="420" height="315" src="http://www.youtube.com/embed/Yb63qduvxWU" frameborder="0" allowfullscreen></iframe>--%>
                    </div>
                </div>
                <div id="Resource_Selection_middle">
                    <img src="Images/resourceselection_right_bg.gif"></div>
                <div id="Resource_Selection_right">
                    <ul>
                        <li><a href="AssessmentintelliTESTFeature.aspx">Features</a></li>
                        <li><a href="AssessmentintelliTESTAddons.aspx">Add-ons</a></li>
                        <li><a href="Downloads.ASPX">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
