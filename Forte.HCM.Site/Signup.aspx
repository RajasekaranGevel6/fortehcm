﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="Signup.aspx.cs" Inherits="ForteHCMWeb.Signup" %>

<%@ MasterType VirtualPath="~/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="innerpage_content_outer">
        <div id="innerpage_content_inside_outer">
            <div id="breadcrumb">
                <span><a href="default.aspx">Home </a>&raquo; </span>Sign Up Today</div>
            <div id="Resource_Selection_cnt_outer">
                <div id="sign_outer">
                    <div id="sign_up_band_bg">
                        <div id="title_sign_up">
                            Talent Acquisition Infrastructure</div>
                    </div>
                    <div id="sign_inner">
                        <div id="select_plan">
                            <ul>
                                <li>
                                    <img src="Images/icon_sign_up.png"></li>
                                <li>Select a ForteHCM plan:</li>
                            </ul>
                        </div>
                        <div id="sing_left">
                            <div id="sing_left_title">
                                <asp:RadioButton ID="Signup_professionalRadioButton" runat="server" GroupName="SubscriptionType"
                                    Checked="false" />
                                Business</div>
                            <div id="sing_left_user_title">
                                Free (15 day trial)</div>
                            <div id="sing_left_user_01">
                                1 User
                            </div>
                            <div id="sing_left_user_02">
                                intelliSPOT
                            </div>
                            <div id="sing_left_user_03">
                                Assessment (Limited to 3)
                            </div>
                            <div id="sing_left_user_04">
                                Client Management (Limited to 5)
                            </div>
                            <div id="sing_left_user_05">
                                Cyber Proctoring</div>
                            <div id="sing_left_user_06">
                                Reports
                            </div>
                            <div id="sing_left_btn">
                                <a href="FeaturesAndPricing.aspx">See all features</a></div>
                            <div id="what_free_trail">
                                <a href="#" onclick="Popup=window.open('PricingFreeTrial.aspx','Popup','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no, width=420,height=200,left=300,top=203'); return false;">
                                    <img src="Images/what_free_trail.png" title="What happens after the free trial? "></a>
                            </div>
                        </div>
                        <div id="sign_middle">
                            <div id="sing_middle_title">
                                <asp:RadioButton ID="Signup_businessRadioButton" runat="server" GroupName="SubscriptionType"
                                    Checked="true" />
                                Enterprise</div>
                            <div id="sing_left_user_title">
                                Get in touch</div>
                            <div id="sing_left_user_07">
                                Unlimited position profiles</div>
                            <div id="sing_left_user_08">
                                Link to external job boards</div>
                            <div id="sing_left_user_09">
                                Assessment (Limited to 30)</div>
                            <div id="sing_left_user_10">
                                Collaborative interviews (Limited to 15)</div>
                            <div id="sing_left_user_11">
                                Branding</div>
                            <div id="sing_left_user_12">
                                Customization</div>
                            <div id="sign_middle_btn">
                                <a href="FeaturesAndPricing.aspx">See all features</a></div>
                        </div>
                        <div id="sign_form_outer">
                            <div id="sing_form_title">
                                Set up your account
                            </div>
                            <div id="form_outer">
                                <div>
                                    <asp:Label ID="Signup_successLabel" runat="server" Text="" ForeColor="DarkGreen"></asp:Label>
                                    <asp:Label ID="Signup_errorLabel" runat="server" Text="" ForeColor="Red"></asp:Label></div>
                                <div id="form_container">
                                    <div id="form_container_left">
                                        <asp:Label runat="server" ID="Signup_firstNameLabel">First Name:<span>*</span></asp:Label>
                                    </div>
                                    <div id="form_container_right">
                                        <asp:TextBox runat="server" ID="Signup_firstNameTextBox" CssClass="text_box"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="form_container">
                                    <div id="form_container_left">
                                        <asp:Label runat="server" ID="Signup_lastNameLabel">Last Name:<span>*</span></asp:Label>
                                    </div>
                                    <div id="form_container_right">
                                        <asp:TextBox runat="server" ID="Signup_lastNameTextBox" CssClass="text_box"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="form_outer">
                                    <div id="form_container">
                                        <div id="form_container_left">
                                            <asp:Label runat="server" ID="Signup_emailLabel">Email:<span>*</span></asp:Label>
                                        </div>
                                        <div id="form_container_right">
                                            <asp:TextBox runat="server" ID="Signup_emailTextBox" CssClass="text_box"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div id="form_outer">
                                    <div id="form_container">
                                        <div id="form_container_left">
                                            <asp:Label runat="server" ID="Signup_passwordLabel">Password:<span>*</span></asp:Label>
                                        </div>
                                        <div id="form_container_right">
                                            <asp:TextBox runat="server" ID="Signup_passwordTextBox" CssClass="text_box" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div id="form_outer">
                                    <div id="form_container">
                                        <div id="form_container_left">
                                            <asp:Label runat="server" ID="Signup_confirmPasswordLabel">Confirm password:<span>*</span></asp:Label>
                                        </div>
                                        <div id="form_container_right">
                                            <asp:TextBox runat="server" ID="Signup_confirmPasswordTextBox" CssClass="text_box"
                                                TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div id="form_outer">
                                    <div id="form_container">
                                        <div id="form_container_left">
                                            <asp:Label runat="server" ID="Signup_yourPhoneLabel">Your Phone:</asp:Label>
                                        </div>
                                        <div id="form_container_right">
                                            <asp:TextBox runat="server" ID="Signup_yourPhoneTextBox" CssClass="text_box"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div id="form_txt">
                                    <asp:CheckBox runat="server" ID="Signup_agreeCheckBox" />
                                    I agree to ForteHCM's <a href="#" onclick="Popup=window.open('Agreement.aspx','Popup','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no, width=950,height=700,left=300,top=203'); return false;">
                                        <span>Terms of Services</span></a>
                                </div>
                                <div id="form_outer">
                                    <div id="form_container_top">
                                        <div id="sign_Requiredfiled">
                                            Required field <span>*</span>
                                        </div>
                                        <div id="form_container_right">
                                            <asp:Button ID="Signup_continueButton" runat="server" CssClass="continue_btn" OnClick="Signup_continueButton_Click"
                                                BackColor="Transparent" BorderStyle="None" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="help_cnt">
                                Need Help deciding? Call 877-377-9681</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
