﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Forte.HCM.OnlineInterviewConduction.Common;
using Forte.HCM.DataObjects;
using Forte.HCM.BL;
using Forte.HCM.Support;
using Forte.HCM.Trace;
using Forte.HCM.ExternalService;
using System.Configuration;

namespace Forte.HCM.OnlineInterviewConduction
{
    public partial class SubmitAssessorRating : PageBase
    {
        #region Private Variables

        /// <summary>
        /// A <seealso cref="delegate"/> for the asynchronous page processing.
        /// </summary>
        /// <param name="candidateInterviewDetail">
        /// A <see cref="CandidateInterviewSessionDetail"/> that holds the candidate interview detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type.
        /// </param>
        private delegate void AsyncTaskDelegate(CandidateInterviewSessionDetail candidateInterviewDetail, 
            EntityType entityType);

        /// <summary>
        /// A <see cref="int"/> that hold the candidate interview id.
        /// </summary>
        private int candidateInterviewID = 0;

        /// A <see cref="int"/> that hold the assessor id.
        /// </summary>
        private int assessorID = 0;

        #endregion Private Variables

        #region Event Handlers

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set browser title.
            Master.SetPageCaption("Submit Assessor Rating");

            // Check if candidate interview ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["candidateinterviewid"]))
            {
                base.ShowMessage(SubmitAssessorRating_topErrorMessageLabel,
                    "Candidate Interview ID parameter missing");
                return;
            }

            // Check if assessor ID is passed.
            if (Utility.IsNullOrEmpty(Request.QueryString["assessorid"]))
            {
                base.ShowMessage(SubmitAssessorRating_topErrorMessageLabel,
                    "Assessor ID parameter missing");
                return;
            }

            // Get candidate interview ID.
            int.TryParse(Request.QueryString["candidateinterviewid"], out candidateInterviewID);

            if (candidateInterviewID <= 0)
            {
                base.ShowMessage(SubmitAssessorRating_topErrorMessageLabel,
                    "Candidate interview id is invalid");
                return;
            }

            // Get assessor ID.
            int.TryParse(Request.QueryString["assessorid"], out assessorID);

            if (assessorID <= 0)
            {
                base.ShowMessage(SubmitAssessorRating_topErrorMessageLabel,
                    "Candidate interview id is invalid");
                return;
            }

            if (!IsPostBack)
            {
                LoadInterviewSkillScore();

                if (!Utility.IsNullOrEmpty(Request.QueryString["self"]) &&
                    Request.QueryString["self"].ToString().Trim() == "Y")
                {
                    SubmitAssessorRating_topCancelLinkButton.Visible = true;
                    SubmitAssessorRating_logoutButton.Visible = false;
                    SubmitAssessorRating_submitExitButton.Visible = false;
                    SubmitAssessorRating_submitButton.Visible = true;
                }
                else
                {
                    SubmitAssessorRating_topCancelLinkButton.Visible = false;
                    SubmitAssessorRating_logoutButton.Visible = true;
                    SubmitAssessorRating_submitExitButton.Visible = true;
                    SubmitAssessorRating_submitButton.Visible = false;
                }
            }
        }

        /// <summary>
        /// Handler method that will be called 
        /// when submit rating button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirect to submit assessor rating page
        /// </remarks>
        protected void SubmitAssessorRating_submitExitButton_Click(object sender,
           EventArgs e)
        {
            try
            {
                SubmitRedirection();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SubmitAssessorRating_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called 
        /// when submit rating button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirect to submit assessor rating page
        /// </remarks>
        protected void SubmitAssessorRating_submitButton_Click(object sender,
           EventArgs e)
        {
            try
            {
                SubmitRedirection();
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SubmitAssessorRating_topErrorMessageLabel,
                    exp.Message);
            }
        }

        protected void SubmitAssessorRating_logoutButton_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                string returnUrl = ConfigurationManager.AppSettings["ONLINE_ASSESSOR_URL"].ToString();
                Response.Redirect(returnUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(SubmitAssessorRating_topErrorMessageLabel,
                    exp.Message);
            }
        }

        #endregion Event Handlers

        #region Private Methods

        private void SubmitRedirection()
        {
            List<SkillDetail> skillRating = new List<SkillDetail>();

            skillRating = ConstructSkillScoreDetail();

            if (skillRating == null || skillRating.Count == 0)
                return;

            new OnlineInterviewBLManager().SaveOnlineInterviewSkillRating(skillRating, candidateInterviewID,
                assessorID, SubmitAssessorRating_overAllCommentsTextBox.Text.Trim());

            if (!Utility.IsNullOrEmpty(Request.QueryString["self"]) &&
                Request.QueryString["self"].ToString().Trim() == "Y")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "CloseSubmitRating", "<script language='javascript'>CloseSubmitRating();</script>", false);
            }
            else
            {
               CandidateInterviewSessionDetail candidateInterviewDetail =
               new CandidateInterviewSessionDetail();

                candidateInterviewDetail = new OnlineInterviewBLManager().
                    GetCandidateOnlineInterviewDetail(candidateInterviewID);

                if (candidateInterviewDetail != null)
                {
                    //Set candidate interview id
                    candidateInterviewDetail.CandidateInterviewID = candidateInterviewID;

                    //Send mail to scheduler/candidate owner/position profile owner
                    // Sent alert mail to the associated user asynchronously.
                    AsyncTaskDelegate taskDelegate = new AsyncTaskDelegate(SendInterviewCompletedMail);
                    IAsyncResult result = taskDelegate.BeginInvoke(candidateInterviewDetail,
                        EntityType.OnlineInterviewCompleted, new AsyncCallback(SendInterviewCompletedMailCallBack),
                        taskDelegate);
                }

                base.ShowMessage(SubmitAssessorRating_topSuccessMessageLabel, "Score submitted successfully");

                Session.Abandon();
                string returnUrl = ConfigurationManager.AppSettings["ONLINE_ASSESSOR_URL"].ToString();
                Response.Redirect(returnUrl, false);
            }
        }


        /// <summary>
        /// Method thats interview skills
        /// </summary>
        public void LoadInterviewSkillScore()
        {
            List<SkillDetail> skillDetail = new List<SkillDetail>();

            skillDetail = new OnlineInterviewBLManager().
                GetOnlineInterviewAssessorSkillReport(candidateInterviewID, assessorID);

            SubmitAssessorRating_skillsGridView.DataSource = null;
            SubmitAssessorRating_skillsGridView.DataBind();

            if (skillDetail != null && skillDetail.Count > 0)
            {
                //Bind the parent data list
                SubmitAssessorRating_skillsGridView.DataSource = skillDetail;
                SubmitAssessorRating_skillsGridView.DataBind();
            }

            //Display overall score
            string overAllComments =  new OnlineInterviewBLManager().
                GetAssessorOverAllComments(candidateInterviewID, assessorID);

            if (!Utility.IsNullOrEmpty(overAllComments))
            {
                SubmitAssessorRating_overAllCommentsTextBox.Text = overAllComments;
            }
        }

        /// <summary>
        /// Method that construct the current grid view details
        /// </summary>
        /// <returns></returns>
        private List<SkillDetail> ConstructSkillScoreDetail()
        {
            List<SkillDetail> skillRating = null;

            foreach (GridViewRow row in SubmitAssessorRating_skillsGridView.Rows)
            {
                // Instantiate skill list.
                if (skillRating == null)
                    skillRating = new List<SkillDetail>();

                // Create a skill object.
                SkillDetail skill = new SkillDetail();

                TextBox SubmitAssessorRating_skillsGridView_scoreTextBox = ((TextBox)row.FindControl
                       ("SubmitAssessorRating_skillsGridView_scoreTextBox"));

                TextBox SubmitAssessorRating_skillsGridView_commentsTextBox = ((TextBox)row.FindControl
                       ("SubmitAssessorRating_skillsGridView_commentsTextBox"));

                HiddenField SubmitAssessorRating_skillsGridView_skillIDHiddenField = ((HiddenField)row.FindControl
                    ("SubmitAssessorRating_skillsGridView_skillIDHiddenField"));

                if (!Utility.IsNullOrEmpty(SubmitAssessorRating_skillsGridView_skillIDHiddenField.Value))
                {
                    skill.SkillID = Convert.ToInt32(SubmitAssessorRating_skillsGridView_skillIDHiddenField.Value);
                }

                if (!Utility.IsNullOrEmpty(SubmitAssessorRating_skillsGridView_scoreTextBox.Text))
                {
                    skill.SkillRating = Convert.ToInt32(SubmitAssessorRating_skillsGridView_scoreTextBox.Text);
                }
                else
                {
                    skill.SkillRating = -1;
                }

                if (!Utility.IsNullOrEmpty(SubmitAssessorRating_skillsGridView_commentsTextBox.Text))
                {
                    skill.SkillComments = SubmitAssessorRating_skillsGridView_commentsTextBox.Text;
                }

                skillRating.Add(skill);
            }
            return skillRating;
        }

        /// <summary>
        /// Method that send email to the requested 
        /// candidates with list of choice slots.
        /// </summary>
        /// <param name="candidateDetail">
        /// A <see cref="CandidateDetail"/> that holds the requested candidate
        /// detail.
        /// </param>
        /// <param name="entityType">
        /// A <see cref="EntityType"/> that holds the entity type to send request mail
        /// </param>
        /// <remarks>
        /// This method is called asynchronously.
        /// </remarks>
        private void SendInterviewCompletedMail(CandidateInterviewSessionDetail candidateInterviewDetail,
            EntityType entityType)
        {
            try
            {
                // Send the email.
                new EmailHandler().SendMail(EntityType.OnlineInterviewCompleted,
                    candidateInterviewDetail);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Private Methods

        #region Public Methods

        public string GetSkillScore(int computedScore, int SkillScore)
        {
            string retVal = "";

            if (computedScore == -1 && SkillScore == 0)
                retVal = "";
            if (SkillScore == 0 && computedScore>=0)
                retVal = computedScore.ToString();
            else if (SkillScore != 0)
                retVal = SkillScore.ToString();
           
            return retVal;
        }

        #endregion Pubic Metods

        #region Protected Overridden Methods

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            return isValid;
        }

        #endregion Protected Overridden Methods

        #region Asynchronous Method Handlers

        /// <summary>
        /// Handler method that acts as the callback method for email alert.
        /// </summary>
        /// <param name="result">
        /// A <see cref="IAsyncResult"/> that holds the result.
        /// </param>
        protected void SendInterviewCompletedMailCallBack(IAsyncResult result)
        {
            try
            {
                // Retrieve the delegate.
                AsyncTaskDelegate caller = (AsyncTaskDelegate)result.AsyncState;

                // Call EndInvoke to retrieve the results.
                caller.EndInvoke(result);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
            }
        }

        #endregion Asynchronous Method Handlers
    }
}