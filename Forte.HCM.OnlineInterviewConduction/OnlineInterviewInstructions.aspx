﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OverviewMaster.Master"
    AutoEventWireup="true" CodeFile="OnlineInterviewInstructions.aspx.cs" Inherits="Forte.HCM.OnlineInterviewConduction.OnlineInterviewInstructions" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="OverviewMaster_body" runat="server">
     
    <div class="innerpage_content_outer">
        <div class="innerpage_content_inside_outer">
            <div class="Resource_Selection_cnt_outer">
                <div>
                    <div class="msg_align">
                        <asp:Label ID="OnlineInterviewInstructions_topSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewInstructions_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
                <div class="cnt_outer_content">
                    <div class="activities_outer_border">
                        <div class="activities_inner_cont_left" style="float: left">
                            <input type="image" name="ctl00$MainContent$Activities_pendingInterviewsImageButton"
                                id="ctl00_MainContent_Activities_pendingInterviewsImageButton" src="App_Themes/DefaultTheme/Images/pending_interview_icon.png"
                                style="border-width: 0px;" />
                        </div>
                        <div style="float: left; padding-top: 30px">
                            <asp:Label ID="OnlineInterviewInstructions_intervieNameLabel" runat="server" CssClass="activity_name_label"></asp:Label>
                        </div>
                        <div class="activities_border_top">
                        </div>
                        <div class="test_body_bg_div">
                            <div class="interview_test_content">
                                <asp:Label ID="OnlineInterviewInstructions_interviewDescLabel" runat="server" SkinID="sknActivityValueLabel"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="test_body_bg_div">
                        <div class="interview_test_stop_icon">
                        </div>
                        <div id="TestInstructions_title" class="interview_test_header">
                            <asp:Literal ID="OnlineInterviewInstructions_headerLiteral" runat="server" Text="Please read this section completely before starting your interview"></asp:Literal>
                        </div>
                    </div>
                    <div class="test_body_bg_div">
                        <div class="interview_test_content">
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instructionSlNo1Literal" runat="server"></asp:Literal>
                                </div>
                                <div style="width: 890px; float: right; padding-left: 3px; text-align: left;">
                                    <asp:Literal ID="OnlineInterviewInstructions_instruction1Literal" runat="server"></asp:Literal>
                                 </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instructionSlNo2Literal" runat="server"></asp:Literal>
                                </div>
                                <div style="width: 890px; float: right; padding-left: 3px; text-align: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instruction2Literal" runat="server"></asp:Literal> 
                                </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instructionSlNo3Literal" runat="server"></asp:Literal></div>
                                <div style="width: 890px; float: right; padding-left: 3px; text-align: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instruction3Literal" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float: left">
                                     <asp:Literal ID="OnlineInterviewInstructions_instructionSlNo4Literal" runat="server"></asp:Literal>
                                </div>
                                <div style="width: 890px; float: right; padding-left: 3px; text-align: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instruction4Literal" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="interview_test_div_padding">
                                <div class="activity_number_label" style="float: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instructionSlNo5Literal" runat="server"></asp:Literal>
                                 </div>
                                <div style="width: 890px; float: right; padding-left: 3px; text-align: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instruction5Literal" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="interview_test_div_padding"  id="OnlineInterviewInstructions_instructionSlNo6Div" runat="server">
                                <div class="activity_number_label" style="float: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instructionSlNo6Literal" runat="server"></asp:Literal>
                                </div>
                                <div style="width: 890px; float: right; padding-left: 3px; text-align: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instruction6Literal" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="interview_test_div_padding"  id="OnlineInterviewInstructions_instructionSlNo7Div" runat="server">
                                <div class="activity_number_label" style="float: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instructionSlNo7Literal" runat="server"></asp:Literal>
                                 </div>
                                <div style="width: 890px; float: right; padding-left: 3px; text-align: left">
                                    <asp:Literal ID="OnlineInterviewInstructions_instruction7Literal" runat="server"
                                        Text=""></asp:Literal>
                                </div>
                            </div>
                            <div class="interview_test_div_padding_left" style="padding-left: 40px;">
                                <div class="interview_test_instruction_04">
                                </div>
                            </div>
                            <div class="interview_test_wish" style="text-align: left; padding-left: 40px;">
                                <asp:Label ID="OnlineInterviewInstruction_wishLabel" runat="server" Text="Wish you all the best!"></asp:Label>
                            </div>
                            <div class="interview_btn" style="text-align: left; padding-left: 40px;">
                                <asp:Button ID="OnlineInterviewInstructions_startButton" Height="24px" runat="server"
                                    Text="Start Interview" SkinID="sknButtonId" OnClick="OnlineInterviewInstructions_startButton_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="test_body_bg_div" style="display: none">
                        <div class="test_body_bg_div">
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="OnlineInterviewInstructions_minimumHardwareLiteral" runat="server"
                                    Text="Minimum Hardware & OS Requirements"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="OnlineInterviewInstructions_minimumHardwareValueLiteral" runat="server"
                                    Text=""></asp:Literal>
                            </div>
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="OnlineInterviewInstructions_interviewInstructionsLiteral" runat="server"
                                    Text="Interview Instructions"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="OnlineInterviewInstructions_interviewInstructionsValueLiteral" runat="server"></asp:Literal>
                            </div>
                            <div class="test_subheader_text_bold_div">
                                <asp:Literal ID="OnlineInterviewInstructions_interviewDescriptionLiteral" runat="server"
                                    Text="Interview Description"></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="OnlineInterviewInstructions_interviewDescriptionValueLiteral" runat="server"
                                    Text=""></asp:Literal>
                            </div>
                            <div class="test_text_div">
                                <asp:Literal ID="OnlineInterviewInstructions_warningLiteral" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="empty_height">
                    &nbsp;
                </div>
                <div>
                    <div class="msg_align">
                        <asp:Label ID="OnlineInterviewInstructions_bottomSuccessMessageLabel" runat="server"
                            SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="OnlineInterviewInstructions_bottomErrorMessageLabel" runat="server"
                            SkinID="sknErrorMessage"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
