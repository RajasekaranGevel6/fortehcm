﻿using System.Web;
using System.IO;
using System.Configuration;

namespace Forte.HCM.OnlineInterviewConduction
{
    /// <summary>
    /// Summary description for Datahandler
    /// </summary>
    public class Datahandler : IHttpHandler
    {
        string requestType = null;
        string folderPath = null;
        string sessionId = null;
        string chatRoomId = null;
        string chatText = null;

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request["call"] != null)
            {
                requestType = context.Request["call"].ToString();
                sessionId = context.Request["sessionId"].ToString();
                chatRoomId = context.Request["chatRoomId"].ToString();
                chatText = context.Request["chatText"].ToString();

                SelectingTextChatType(requestType, sessionId, chatRoomId, chatText);
            }
        }

        private void SelectingTextChatType(string paramType, string sessionid, string chatRoomId, string chatText)
        {
            switch (paramType.ToLower())
            {
                case "chattext":
                    WriteTextChat(sessionId, chatRoomId, chatText);
                    break;
                default:
                    break;
            }
        }

        private void WriteTextChat(string sessionId, string chatRoomId, string chatText)
        {
            FileStream fs = null;
            StreamWriter m_streamWriter = null;
            try
            {
                folderPath = ConfigurationManager.AppSettings["ONLINEINTERVIEWS"].ToString();
                folderPath = folderPath + "\\" + sessionId + "\\" + chatRoomId + "\\";

                if (!File.Exists(folderPath + "chat.txt"))
                    File.Create(folderPath + "chat.txt");

                fs = new FileStream(folderPath + "chat.txt", FileMode.OpenOrCreate, FileAccess.Write);
                m_streamWriter = new StreamWriter(fs); 
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(chatText); 
                m_streamWriter.Flush();  
            }
            catch
            {
            }
            finally
            {
                m_streamWriter.Close();
                fs.Close();
                m_streamWriter = null;
                fs=null;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}