﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OverviewMaster.Master"
    AutoEventWireup="true" CodeFile="OnlineInterviewCandidateConduction.aspx.cs"
    Inherits="Forte.HCM.OnlineInterviewConduction.OnlineInterviewCandidateConduction" %>
    <%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="OverviewMaster_body" runat="server">    
    <table cellpadding="0" cellspacing="0" border="0"  style="height:100%;width:100%">
         <tr>
            <td style="height:35px;padding-right:25px" align="right">&nbsp;<asp:Button 
                    ID="OnlineInterviewCandidateConduction_logOutButton" runat="server" 
                    SkinID="sknButtonId" Text="Logout" 
                    onclick="OnlineInterviewCandidateConduction_logOutButton_Click" /></td>
        </tr>
        <tr>
            <!-- Chat Room-->
            <td valign="top" style="height:100%;width:100%;background-color:#fff;" >
                <iframe  id="OnlineInterviewCandidateConduction_iFrame" runat="server"  
                frameborder="0"  style="border-style: none;height:364px;background-color:#ffffff;" scrolling="no"  width="100%"></iframe> 
            </td>
        </tr>
        <tr>
            <td style="height:100px">&nbsp;</td>
        </tr>
    </table>
</asp:Content>
