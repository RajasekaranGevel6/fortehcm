﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Forte.HCM.OnlineInterviewConduction.Default"
    MasterPageFile="~/MasterPages/OverviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="Default_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <script type="text/javascript" src="Scripts/jquery.min.js"></script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 30%" class="header_text_bold">
                            <asp:Literal ID="Default_headerLiteral" runat="server" Text="Online Interview Conduction"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align" style="text-align: center">
                <asp:Label ID="Default_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="Default_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                <asp:HiddenField ID="Default_skillRatingSelectedHiddenField" runat="server">
                </asp:HiddenField>
            </td>
        </tr>
        <tr style="display: none">
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%">
                            <table border="0" cellpadding="0" cellspacing="0" align="right">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="Default_bottomRefreshButton" runat="server" SkinID="sknButtonId"
                                            Text="Refresh" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg_assessor">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                       <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
