﻿using System;
using System.Globalization;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.OnlineInterviewConduction.Common;
using System.Collections.Generic;


namespace Forte.HCM.OnlineInterviewConduction
{
    public partial class OnlineInterviewInstructions : PageBase
    {
        #region Event Handlers
        /// <summary>
        /// Handler that will call when the page gets loaded. This handler
        /// loads the default values and set javascript functions to the 
        /// buttons.
        /// </summary>
        /// <param name="sender">
        /// An <see cref="object"/> that contains the sender of the event.
        /// </param>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Title = "Online Interview Instructions";
                Master.SetPageCaption("Online Interview Instructions");

                //Clear controls
                ClearControls();

                if (Request.QueryString["userrole"] != null && Request.QueryString["userrole"].ToString() == "C")
                    OnlineInterviewInstruction_wishLabel.Visible =true ;
                 else
                    OnlineInterviewInstruction_wishLabel.Visible = false;

                if (!IsPostBack)
                {
                    LoadIntroductionValues();
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewInstructions_topErrorMessageLabel,
                    OnlineInterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        /// <summary>
        /// Verifying Chatroom,date and time
        /// verification returns false function will exit else
        ///  to load the online interview screen 
        /// </summary>
        /// <param name="sender">
        /// <see cref="Object"/>that contains sender of the event
        /// </param>
        /// <param name="e">
        /// <see cref="EventArgs"/> that contain event data
        /// </param>
        protected void OnlineInterviewInstructions_startButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["interviewkey"] == null) return;

                OnlineCandidateSessionDetail onlineInterviewDetail = null;
                //Checking candidate have online interview for current date & time 

                if (Request.QueryString["userrole"] != null && Request.QueryString["userrole"].ToString() == "C")
                    onlineInterviewDetail = new CandidateBLManager().GetOnlineInterviewDetail(Convert.ToInt32(Request.QueryString["candidateid"]), 0, Request.QueryString["chatroom"].ToString());
                else if (Request.QueryString["userrole"] != null && Request.QueryString["userrole"].ToString() == "A")
                    onlineInterviewDetail = new CandidateBLManager().GetOnlineInterviewDetail(0, Convert.ToInt32(Request.QueryString["userid"]), Request.QueryString["chatroom"].ToString());

                if (onlineInterviewDetail != null)
                {
                    if (new Utility().GetOnlineInterviewAvailable(onlineInterviewDetail.InterviewDate,
                        onlineInterviewDetail.TimeSlotFrom, onlineInterviewDetail.TimeSlotTo))
                    {
                        if (Request.QueryString["userrole"].ToString() == "C")
                            Response.Redirect("~/OnlineInterviewCandidateConduction.aspx?candidateid=" +
                                                Request.QueryString["candidateid"].ToString() + "&userrole=C" + "&userid=" + Request.QueryString["candidateid"].ToString() + "&interviewkey=" + Request.QueryString["interviewkey"].ToString()
                                                + "&chatroom=" + Request.QueryString["chatroom"].ToString(), false);
                        else
                            Response.Redirect("~/OnlineInterviewAssessorConduction.aspx?candidateid=" + 
                                Request.QueryString["candidateid"].ToString() + "&userrole=A" + "&userid=" + Request.QueryString["userid"].ToString().Trim() + "&interviewkey=" + Request.QueryString["interviewkey"].ToString()
                                + "&chatroom=" + Request.QueryString["chatroom"].ToString(), false);

                        return;
                    }
                    else
                    {
                        if (onlineInterviewDetail.InterviewDate > DateTime.Today)
                        {
                            base.ShowMessage(OnlineInterviewInstructions_topErrorMessageLabel, 
                                OnlineInterviewInstructions_bottomErrorMessageLabel, "Interview start date on " 
                                + base.GetDateFormat(onlineInterviewDetail.InterviewDate));
                            return;
                        }

                        string currentTime = DateTime.Now.ToString("HH:mm:ss");

                        DateTime dbStartTime = Convert.ToDateTime(onlineInterviewDetail.TimeSlotFrom.ToString(new DateTimeFormatInfo()));
                        DateTime currentSysTime = Convert.ToDateTime(currentTime);

                        dbStartTime = dbStartTime.AddMinutes(-10);

                        TimeSpan startTimeDiff = currentSysTime.Subtract(dbStartTime); 

                        //if start time differnece is less the 10 mins
                        if (startTimeDiff.Minutes > 10)
                        {
                            base.ShowMessage(OnlineInterviewInstructions_topErrorMessageLabel,
                               OnlineInterviewInstructions_bottomErrorMessageLabel, "Interview start time on "
                               +  onlineInterviewDetail.TimeSlotFrom);
                            return;
                        } 
                    }
                } 

                /*if (Request.QueryString["userrole"].ToString() == "C")
                      Response.Redirect("~/OnlineInterviewCandidateConduction.aspx?candidateid=" +
                      Request.QueryString["candidateid"].ToString() + "&userrole=C" + "&userid=" + Request.QueryString["candidateid"].ToString() + "&interviewkey=" + Request.QueryString["interviewkey"].ToString()
                      + "&chatroom=" + Request.QueryString["chatroom"].ToString(), false);
                  else
                      Response.Redirect("~/OnlineInterviewAssessorConduction.aspx?candidateid=" +
                         Request.QueryString["candidateid"].ToString() + "&userrole=A" + "&userid=" + Request.QueryString["userid"].ToString().Trim() + "&interviewkey=" + Request.QueryString["interviewkey"].ToString()
                          + "&chatroom=" + Request.QueryString["chatroom"].ToString(), false);*/

            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewInstructions_topErrorMessageLabel,
                    OnlineInterviewInstructions_bottomErrorMessageLabel, exp.Message);
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// This function initiate the visual controls when
        /// page load and user calling functions inside
        /// </summary>
        private void ClearControls()
        {
            OnlineInterviewInstructions_bottomErrorMessageLabel.Text = string.Empty;
            OnlineInterviewInstructions_bottomSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewInstructions_topErrorMessageLabel.Text = string.Empty;
            OnlineInterviewInstructions_topSuccessMessageLabel.Text = string.Empty;
        }


        /// <summary>
        /// Getting the interview details aganist the interview key query string.
        /// </summary>
        private void LoadIntroductionValues()
        {
            if (Request.QueryString["userrole"] != null)
            {
                string role = Request.QueryString["userrole"].Trim();

                switch (role)
                { 
                    case "C":
                        OnlineInterviewInstructions_instruction1Literal.Text = "This is a video interview in which you will be talking to one or more interviewers.";
                        OnlineInterviewInstructions_instruction2Literal.Text = "Please ensure that you have a working webcam, microphone and speakers connected to your computer, and that you are seated in a well-lit room.";
                        OnlineInterviewInstructions_instruction3Literal.Text = "You will need to be using a Windows Operating System that is Windows XP or newer.";
                        OnlineInterviewInstructions_instruction4Literal.Text = "You will be presented with an Adobe Flash prompt that looks like this"+ " <a href='App_Themes/DefaultTheme/Images/OnlineInterview.png' target='_blank'>sample screenshot</a>" +". after you click on the Start button. Please click on ‘Allow’ on this window to enable your webcam and microphone.";
                        OnlineInterviewInstructions_instruction5Literal.Text = "You may now start your interview by clicking on the START button below.";
                        OnlineInterviewInstructions_instruction6Literal.Text = string.Empty;
                        OnlineInterviewInstructions_instruction7Literal.Text = string.Empty;
                        OnlineInterviewInstructions_instructionSlNo1Literal.Text = "1";
                        OnlineInterviewInstructions_instructionSlNo2Literal.Text = "2";
                        OnlineInterviewInstructions_instructionSlNo3Literal.Text = "3";
                        OnlineInterviewInstructions_instructionSlNo4Literal.Text = "4";
                        OnlineInterviewInstructions_instructionSlNo5Literal.Text = "5";
                        OnlineInterviewInstructions_instructionSlNo6Literal.Text = string.Empty;
                        OnlineInterviewInstructions_instructionSlNo7Literal.Text = string.Empty;
                        OnlineInterviewInstructions_instructionSlNo7Div.Visible = false;
                        OnlineInterviewInstructions_instructionSlNo6Div.Visible = false; 
                        break;
                    case "A":
                        OnlineInterviewInstructions_instructionSlNo7Div.Visible = true;
                        OnlineInterviewInstructions_instructionSlNo6Div.Visible = true; 
                        OnlineInterviewInstructions_instructionSlNo1Literal.Text = "1";
                        OnlineInterviewInstructions_instructionSlNo2Literal.Text = "2";
                        OnlineInterviewInstructions_instructionSlNo3Literal.Text = "3";
                        OnlineInterviewInstructions_instructionSlNo4Literal.Text = "4";
                        OnlineInterviewInstructions_instructionSlNo5Literal.Text = "5";
                        OnlineInterviewInstructions_instructionSlNo6Literal.Text = "6";
                        OnlineInterviewInstructions_instructionSlNo7Literal.Text = "7"; 
                        OnlineInterviewInstructions_instruction1Literal.Text = "This is a video interview in which you will be talking to the candidate, with possibly other interviewers participating.";
                        OnlineInterviewInstructions_instruction2Literal.Text = "You will need to be using a Windows Operating Systems that is Windows XP or newer.";
                        OnlineInterviewInstructions_instruction3Literal.Text = "Should you plan on sharing your webcam, please click on ‘Allow’ on the Adobe Flash prompt that will appear after you click on the start button below.";
                        OnlineInterviewInstructions_instruction4Literal.Text = "A sample screenshot of the interview screen can be found here." + " <a href='App_Themes/DefaultTheme/Images/OnlineInterview.png' target='_blank'>sample screenshot</a>";
                        OnlineInterviewInstructions_instruction5Literal.Text = "In order to rate a candidate’s response for a specific question, please click on the question, and then provide your score and comments in the corresponding fields.";
                        OnlineInterviewInstructions_instruction6Literal.Text = "At the end of the interview, please click on the Submit Score button to record your final scores. If scores had been recorded for individual questions earlier, the system will compute Area scores automatically. You will have the option of overriding these scores and recording new scores. If individual question scores had not been recorded earlier, please enter the Area scores on the displayed screen.";
                        OnlineInterviewInstructions_instruction7Literal.Text = "You may now start the interview by clicking on the START button below.";
                        break;
                }
                 
            }
                //OnlineInterviewInstructions_instruction1Literal
            if (Request.QueryString["interviewkey"] == null) return;

            OnlineInterviewSessionDetail interviewDetail = new OnlineInterviewBLManager().GetOnlineInterviewDetailByKey
                (Request.QueryString["interviewkey"]);

            if (interviewDetail == null)
                return;

            OnlineInterviewInstructions_intervieNameLabel.Text = interviewDetail.InterviewName;
            OnlineInterviewInstructions_interviewDescLabel.Text = interviewDetail.InterviewInstruction; 

            if (interviewDetail.skillDetail == null) return;

            string skillArea = string.Empty;
            string skillName = string.Empty;

            foreach (SkillDetail skilldetail in interviewDetail.skillDetail)
            {
                if (Utility.IsNullOrEmpty(skillArea))
                    skillArea = skilldetail.CategoryName.Trim() + ",";
                else
                    skillArea = skillArea + skilldetail.CategoryName.Trim() + ",";

                if (Utility.IsNullOrEmpty(skillName))
                    skillName = skilldetail.SkillName.Trim() + ",";
                else
                    skillName = skillName + skilldetail.SkillName.Trim() + ",";
            }

            if (!Utility.IsNullOrEmpty(skillArea))
                skillArea = skillArea.Remove(skillArea.Length - 1, 1);

            if (!Utility.IsNullOrEmpty(skillName))
                skillName = skillName.Remove(skillName.Length - 1, 1); 
        }

        private bool OnlineInterviewVerification(string interviewkey)
        {
            OnlineCandidateSessionDetail onlineDetail =
                new OnlineInterviewBLManager().GetOnlineInterviewDateTime(interviewkey);

            if (onlineDetail == null) return false;


            if (onlineDetail.InterviewDate != DateTime.Today)
            {
                base.ShowMessage(OnlineInterviewInstructions_topErrorMessageLabel,
                  OnlineInterviewInstructions_bottomErrorMessageLabel, "Your date on " +
                  onlineDetail.InterviewDate.ToShortDateString());
                return false;
            }

            DateTime startTime = Convert.ToDateTime(onlineDetail.TimeSlotFrom);

            DateTime endTime = Convert.ToDateTime(onlineDetail.TimeSlotTo);

            if (DateTime.Now.TimeOfDay > startTime.TimeOfDay)
            {
                base.ShowMessage(OnlineInterviewInstructions_topErrorMessageLabel,
                  OnlineInterviewInstructions_bottomErrorMessageLabel, "Your time start on " +
                  startTime.TimeOfDay.ToString());
                return false;
            }

            if (endTime.TimeOfDay > DateTime.Now.TimeOfDay)
            {
                base.ShowMessage(OnlineInterviewInstructions_topErrorMessageLabel,
                  OnlineInterviewInstructions_bottomErrorMessageLabel, "Your time expired");
                return false;
            }

            if (Request.QueryString["chatroomname"].ToString() != onlineDetail.CandidateName)
            {
                base.ShowMessage(OnlineInterviewInstructions_topErrorMessageLabel,
                  OnlineInterviewInstructions_bottomErrorMessageLabel, "Chat Room mismatch");
                return false;
            }

            return true;
        }
        #endregion

        #region Protected Override Methods
        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        #endregion


    }
}