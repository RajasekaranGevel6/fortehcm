﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlineInterviewAssessorConduction.aspx.cs"
    Inherits="Forte.HCM.OnlineInterviewConduction.OnlineInterviewAssessorConduction"
    MasterPageFile="~/MasterPages/OverviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="OnlineInterviewAssessorConduction_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <script type="text/javascript" src="Scripts/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            $("#<%=OnlineInterviewAssessorConduction_questionRatingSaveButton.ClientID %>").hide();

            $("tr.categoryPanelTab").live('click', function () {
                var imageID = $(this).find("img.ToggleImageStyle").attr("id");

                if ($(this).closest("table").parent().parent().next().find("tr").css("display") == "none") {
                    $(this).closest("table").parent().parent().next().find("tr").css("display", "block");
                    $(this).find("img.ToggleImageStyle").attr("src", "Images/minus_icon.gif");
                    $(this).closest("table").parent().parent().next().find("td#tdQuestionList").addClass("question_bg");
                    $(this).find("img.ToggleImageStyle").attr("title", "Hide Questions")
                }
                else {
                    $(this).closest("table").parent().parent().next().find("tr").css("display", "none");
                    $(this).find("img.ToggleImageStyle").attr("src", "Images/plus_icon.gif");
                    $(this).closest("table").parent().parent().next().find("td#tdQuestionList").removeClass("question_bg");
                    $(this).find("img.ToggleImageStyle").attr("title", "Show Questions")
                }
            });


            $("td.QuestionLink").live('click', function () {
                //Clear the error control values
                $("#<%=OnlineInterviewAssessorConduction_topSuccessMessageLabel.ClientID %>").html('');
                $("#<%=OnlineInterviewAssessorConduction_topErrorMessageLabel.ClientID %>").html('');

                //Show the question rating button
                $("#<%=OnlineInterviewAssessorConduction_questionRatingSaveButton.ClientID %>").show();

                $("#<%=OnlineInterviewAssessorConduction_questionRatingSuccessMessageLabel.ClientID %>").html('');
                $("#<%=OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.ClientID %>").html('');

                var questionid = $(this).find('.QuestionID').attr('id');

                var interviewKey = '<%=interviewKey%>';
                var candidateInterviewID = '<%=candidateinterviewID%>';
                var assessorID = '<%=assessorID%>';


                var dataObj = new Object();

                dataObj.interviewKey = interviewKey;
                dataObj.candidateInterviewId = candidateInterviewID;
                dataObj.assessorId = assessorID;
                dataObj.questionId = questionid;

                var dataString = JSON.stringify(dataObj)
                $.ajax({
                    type: "POST",
                    url: "OnlineInterviewAssessorConduction.aspx/GetQuestion",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccessDisplaySelectedQuestion,
                    failure: function (response) {
                        $("#<%=OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.ClientID %>").html(response.d);
                    },
                    error: function (response) {
                        $("#<%=OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.ClientID %>").html(response.d);
                    }
                });
            });

            //Save question rating
            $('#<%= OnlineInterviewAssessorConduction_questionRatingSaveButton.ClientID %>').live('click', function () {

                $("#<%=OnlineInterviewAssessorConduction_questionRatingSuccessMessageLabel.ClientID %>").html('');
                $("#<%=OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.ClientID %>").html('');

                //Set params
                var quID = $("#<%=OnlineInterviewAssessorConduction_questionIDHiddenField.ClientID %>").val();
                var ratingValue = $find("OnlineInterviewAssessorConduction_ratingBehaviorID").get_Rating();

                
                var ratingObj = new Object();
                ratingObj.candidateInterviewId = '<%=candidateinterviewID%>';
                ratingObj.assessorId = '<%=assessorID%>';
                ratingObj.questionId = quID;
                ratingObj.rating = ratingValue;
                ratingObj.comments = $("#<%=OnlineInterviewAssessorConduction_userCommentsTextBox.ClientID %>").val();

                var ratingString = JSON.stringify(ratingObj);

                $.ajax({
                    type: "POST",
                    url: "OnlineInterviewAssessorConduction.aspx/SaveQuestionRatingAndComments",
                    data: ratingString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccessSaveQuestionRating,
                    failure: function (response) {
                        $("#<%=OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.ClientID %>").html(response.d);
                    },
                    error: function (response) {
                        $("#<%=OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.ClientID %>").html(response.d);
                    }
                });
            });
        });

        function OnSuccessDisplaySelectedQuestion(response) {
            var selectedQuestion = response.d;

            //Clear the existing selected question details
            $("#<%=OnlineInterviewAssessorConduction_questionCategoryLabelValue.ClientID %>").html('');
            $("#<%=OnlineInterviewAssessorConduction_questionLabelValue.ClientID %>").html('');
            $("#<%=OnlineInterviewAssessorConduction_answerLabelValue.ClientID %>").html('');
            $("#<%=OnlineInterviewAssessorConduction_questionIDHiddenField.ClientID %>").val('');
            $("#<%=OnlineInterviewAssessorConduction_userCommentsTextBox.ClientID %>").val('');
            $find("OnlineInterviewAssessorConduction_ratingBehaviorID").set_Rating(Number("0"));

            var selectedQuestionID;

            $.each(selectedQuestion, function (index, quest) {
                $("#<%=OnlineInterviewAssessorConduction_questionCategoryLabelValue.ClientID %>").text(quest.SkillName);
                $("#<%=OnlineInterviewAssessorConduction_questionLabelValue.ClientID %>").text(quest.Question);
                $("#<%=OnlineInterviewAssessorConduction_answerLabelValue.ClientID %>").html(quest.Choice_Desc);
                $("#<%=OnlineInterviewAssessorConduction_questionIDHiddenField.ClientID %>").val(quest.QuestionRelationId);
                $("#<%=OnlineInterviewAssessorConduction_userCommentsTextBox.ClientID %>").val(quest.AssessorComments);
                selectedQuestionID = quest.QuestionRelationId;
                if (quest.AssessorRating != "0") {
                    $find("OnlineInterviewAssessorConduction_ratingBehaviorID").set_Rating(Number(quest.AssessorRating));
                }
            });

            //Display other assessor rating
            DisplayOtherAssessorRating(selectedQuestionID);
        }

        function DisplayOtherAssessorRating(quetionid) {
            var otherRatingDataObj = new Object();
            otherRatingDataObj.questionId = quetionid;
            otherRatingDataObj.candidateInterviewId = '<%=candidateinterviewID%>';
            otherRatingDataObj.assessorId = '<%=assessorID%>';
            var otherRatingDataString = JSON.stringify(otherRatingDataObj);
            $.ajax({
                type: "POST",
                url: "OnlineInterviewAssessorConduction.aspx/GetOtherAssessorRating",
                data: otherRatingDataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccessBindOtherAssessorRatingComments,
                failure: function (response) {
                    $("#<%=OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.ClientID %>").html(response.d);
                },
                error: function (response) {
                    $("#<%=OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.ClientID %>").html(response.d);
                }
            });
        }


        function OnSuccessSaveQuestionRating(response) {
            $("#<%=OnlineInterviewAssessorConduction_questionRatingSuccessMessageLabel.ClientID %>").html("Comments and rating saved successfully");
        }

        function OnSuccessBindOtherAssessorRatingComments(response) {
            if (response.d != null) {
                var OtherAssessorRatingComments = response.d;

                //Clear the existing table
                $('div.ShowOtherAssessorComment').html('');

                var table = "<table cellpadding='2' cellspacing='2' width='100%' border='0'>";

                $.each(OtherAssessorRatingComments, function (indexOtherRating, otherRating) {
                    //Construct table struction
                    var row = "<tr>";
                    row += "<td class='other_assessor_rating_bottom_bg' valign='top'>";
                    row += "<table cellpadding='0' cellspacing='0' border='0' width='100%'>";
                    row += "<tr>";
                    row += "<td style='text-align: left' class='other_assessor_name_field_header_text'>";
                    row += otherRating.FirstName;
                    row += "</td>";
                    row += "<td class='other_assessor_rating_field_text' style='text-align: right'>&nbsp;&nbsp;&nbsp;Rating(" + otherRating.QuestionRating + " of 5)</td>";
                    row += "</tr>";
                    row += "<tr>";
                    row += "<td class='td_height_5'>";
                    row += "</td>";
                    row += "</tr>";
                    row += "<tr>";
                    row += "<td class='td_height_5'>";
                    row += "</td>";
                    row += "</tr>";
                    row += "<tr>";
                    row += "<td colspan='2' style='text-align: left' class='label_field_text'>";
                    row += otherRating.Comments;
                    row += "</td>";
                    row += "</tr>";
                    row += "</table>";
                    row += "</td>";
                    row += "</tr>";

                    table += row;

                });

                $('div.ShowOtherAssessorComment').html(table);
            }
            else {
                var noRecordTable = "<table cellpadding='0' cellspacing='0' width='100%'>";
                noRecordTable += "<tr>";
                noRecordTable += "<td class='error_message_text' align='center'>No rating avilable.</td>";
                noRecordTable += "</tr>";
                noRecordTable += "</table>";

                $('div.ShowOtherAssessorComment').html(noRecordTable);
            }
        }

        // Handler for onchange and onkeyup to check for max characters.
        function CommentsCount(characterLength, clientId) {
            var maxlength = new Number(characterLength);
            if (clientId.value.length > maxlength) {
                clientId.value = clientId.value.substring(0, maxlength);
            }
        }

        function ShowSumbitRating() {

            var candidateInterviewID = '<%=candidateinterviewID%>';
            var assessorID = '<%=assessorID%>';

            var height = 590;
            var width = 950;
            var top = (screen.availHeight - parseInt(height)) / 2;
            var left = (screen.availWidth - parseInt(width)) / 2;
            var sModalFeature = "dialogHeight:" + height + "px;dialogWidth:" + width + "px;dialogTop:" + top + ";dialogLeft:" + left + ";scrolling:yes";
            var queryStringValue = "SubmitAssessorRating.aspx" +
                "?self=Y&candidateinterviewid=" + candidateInterviewID + "&assessorid=" + assessorID;

            window.open(queryStringValue, window.self, sModalFeature);

            return false;
        }
        function openWindow() {
            var candidateInterviewID = '<%=candidateinterviewID%>';
            var assessorID = '<%=assessorID%>';
            var queryStringValue = "SubmitAssessorRating.aspx" +
                "?self=Y&candidateinterviewid=" + candidateInterviewID + "&assessorid=" + assessorID;
            window.open(queryStringValue);  
        }
        
    </script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="header_bg">
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td style="width: 50%" class="header_text_bold">
                            <asp:Literal ID="OnlineInterviewAssessorConduction_headerLiteral" runat="server"
                                Text="Online Interview Conduction"></asp:Literal>
                        </td>
                         <td align="right">
                             <asp:Button ID="OnlineInterviewAssessorConduction_logoutButton" runat="server" SkinID="sknButtonId"
                                Text="Logout" onclick="OnlineInterviewAssessorConduction_logoutButton_Click" />
                         </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="OnlineInterviewAssessorConduction_topSuccessMessageLabel" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="OnlineInterviewAssessorConduction_topErrorMessageLabel" runat="server"
                    SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tab_body_bg" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
                    <tr>
                        <td class="td_height_5" align="right" >
                           
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="OnlineInterviewAssessorConduction_searchCriteriasDiv" runat="server">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="2" width="100%" border="0">
                                                            <tr>
                                                                <td style="width: 300px; padding-top: 10px" valign="top" class="border_right">
                                                                    <table cellpadding="4" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <asp:LinkButton ID="OnlineInterviewAssessorConduction_candidateNameLinkButton" runat="server"
                                                                                    SkinID="sknAssessorActionLinkButton" ToolTip="Click here to view candidate profile"
                                                                                    OnClientClick="return false;"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_interviewNameLabel" runat="server"
                                                                                    SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_interviewScheduledDateLabel" runat="server"
                                                                                    SkinID="sknLabelAssessorFieldText"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <div runat="server" id="OnlineInterviewAssessorConduction_displayOverallScoreDiv"
                                                                                    style="display: none">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 130px">
                                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_overallWeightageScoreLabel" runat="server"
                                                                                                    Text="Total Weighted Score" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_overallWeightageScoreLabelValue"
                                                                                                    SkinID="sknLabelRatingScoreFieldText" runat="server"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="width: 10px;">
                                                                </td>
                                                                <td valign="top" align="left" style="width: 350px; padding-top: 10px">
                                                                    <table cellpadding="4" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:LinkButton ID="OnlineInterviewAssessorConduction_positionProfileLinkButton"
                                                                                    runat="server" SkinID="sknAssessorActionLinkButton" ToolTip="Click here to view position profile details"
                                                                                    OnClientClick="return false;"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:LinkButton ID="OnlineInterviewAssessorConduction_clientNameLinkButton" OnClientClick="return false;"
                                                                                    runat="server" Text="Client Name" SkinID="sknLabelFieldTextLinkButton" ToolTip="Click here to view client details"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_showClientDepartmentsLabel" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_showClientContactsLabel" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="OnlineInterviewAssessorConduction_exitSubmitRatingButton" runat="server"
                                                                                    CssClass="submit_rating_button" Text="Submit Score and Exit" OnClick="OnlineInterviewAssessorConduction_exitSubmitRatingButton_Click" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="OnlineInterviewAssessorConduction_submitRatingButton" runat="server"
                                                                                    CssClass="submit_rating_button" Text="Submit Score" Visible="false" OnClientClick="javascript:return ShowSumbitRating();"  />

                                                                                 <asp:HyperLink ID="OnlineInterviewAssessorConduction_viewInterviewSummaryHyperLink" runat="server"
                                                                                    ToolTip="Submit Rating" CssClass="submit_rating_button submit_rating_hyperlink" onclick="openWindow();" NavigateUrl="#" Text="">Submit Score
                                                                                </asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_height_5">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <!-- Chat Room-->
                        <td style="width: 700px;" valign="top">
                            <table cellpadding="0" cellspacing="0" width="778px">
                                <tr>
                                    <td valign="top">
                                        <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="height: 350px" align="left" valign="top">
                                                    <iframe id="OnlineInterviewAssessorConduction_conductionVideoIframe" 
                                                    runat="server" style="border-style: none;height:364px" scrolling="no" width="100%"></iframe>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_height_5">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <!-- Part 1 -->
                                                <td style="width: 350px; height: 100px;" valign="top">
                                                    <table cellpadding="0" border="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left" valign="top" class="rating_panel_body_bg_small" style="height: 300px;">
                                                                <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                    <tr>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="OnlineInterviewAssessorConduction_questionCategoryLabel" CssClass="category_title"
                                                                                runat="server" Text="Area :"></asp:Label>
                                                                            <asp:Label ID="OnlineInterviewAssessorConduction_questionCategoryLabelValue" CssClass="category_title"
                                                                                runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="OnlineInterviewAssessorConduction_questionLabel" SkinID="sknLabelFieldHeaderText"
                                                                                runat="server" Text="Question :"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" align="left">
                                                                            <div style="float: left; width: 15px; padding-top: 2px;">
                                                                                <asp:Image ID="OnlineInterviewAssessorConduction_questionImage" runat="server" SkinID="sknArrowImage" />
                                                                            </div>
                                                                            <div style="float: left; width: 350px; overflow: auto; word-wrap: break-word; white-space: normal;">
                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_questionLabelValue" SkinID="sknLabelFieldText"
                                                                                    runat="server"></asp:Label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="OnlineInterviewAssessorConduction_answerLabel" SkinID="sknLabelFieldHeaderText"
                                                                                runat="server" Text="Answer :"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" align="left">
                                                                            <div style="float: left; width: 15px; padding-top: 2px;">
                                                                                <asp:Image ID="OnlineInterviewAssessorConduction_answerImage" runat="server" SkinID="sknArrowImage" />
                                                                            </div>
                                                                            <div style="float: left; width: 350px; overflow: auto; word-wrap: break-word; white-space: normal;">
                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_answerLabelValue" SkinID="sknLabelFieldText"
                                                                                    runat="server"></asp:Label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td style="width: 50px">
                                                                                        <asp:Label ID="OnlineInterviewAssessorConduction_questionRatingLabel" SkinID="sknLabelFieldHeaderText"
                                                                                            runat="server" Text="Rating :"></asp:Label>
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <ajaxToolKit:Rating ID="OnlineInterviewAssessorConduction_rating" BehaviorID="OnlineInterviewAssessorConduction_ratingBehaviorID"
                                                                                            runat="server" MaxRating="5" CurrentRating="0" CssClass="rating_star" StarCssClass="rating_item"
                                                                                            WaitingStarCssClass="rating_saved" FilledStarCssClass="rating_filled" EmptyStarCssClass="rating_empty" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="OnlineInterviewAssessorConduction_userCommentsLabel" runat="server"
                                                                                Text="Comments :" SkinID="sknLabelFieldHeaderText"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="OnlineInterviewAssessorConduction_userCommentsTextBox" runat="server"
                                                                                MaxLength="500" Width="350px" Height="60px" TextMode="MultiLine" onkeyup="CommentsCount(500,this)"
                                                                                onchange="CommentsCount(500,this)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_height_10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="OnlineInterviewAssessorConduction_questionRatingSuccessMessageLabel"
                                                                                            runat="server" SkinID="sknSuccessMessage"></asp:Label>
                                                                                        <asp:Label ID="OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel"
                                                                                            runat="server" SkinID="sknErrorMessage"></asp:Label>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:HiddenField ID="OnlineInterviewAssessorConduction_questionIDHiddenField" runat="server" />
                                                                                        <asp:Button ID="OnlineInterviewAssessorConduction_questionRatingSaveButton" runat="server"
                                                                                            Text="Save" SkinID="sknButtonId" OnClientClick="return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width: 5px" valign="top">
                                                </td>
                                                <!-- Part 2 -->
                                                <td style="width: 350px" valign="top">
                                                    <table style="width: 100%;" cellpadding="0" border="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="category_header_bg">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:Label ID="OnlineInterviewAssessorConduction_otherAssessorRatingComments" runat="server"
                                                                                            Text="Other Assessor Question Rating & Comments"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="other_assessor_rating_bg_panel">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td align="left" valign="top">
                                                                                        <div class="ShowOtherAssessorComment">
                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td class="error_message_text" align="center">
                                                                                                        No question selected.
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_height_10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Recomment Area & Question -->
                        <td style="width: 200px" valign="top">
                            <table style="width: 100%;" cellpadding="0" border="0" cellspacing="0">
                                <tr>
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="OnlineInterviewAssessorConduction_recommendArea" runat="server" Text="Recommend Area"></asp:Label>
                                                    |
                                                    <asp:Label ID="OnlineInterviewAssessorConduction_recommendQuestion" runat="server"
                                                        Text="Questions"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <asp:UpdatePanel ID="OnlineInterviewAssessorConduction_categoryDataListUpdatePanel"
                                                UpdateMode="Conditional" runat="server">
                                                <ContentTemplate>
                                                    <asp:DataList ID="OnlineInterviewAssessorConduction_categoryDataList" runat="server"
                                                        DataKeyField="SkillID">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td class="category_header_bg">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr class="categoryPanelTab">
                                                                            <td align="left">
                                                                                <asp:Label ID="OnlineInterviewAssessorConduction_categoryLabel" runat="server" Text='<%# Eval("SkillName") %>'></asp:Label>
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:Image ID="OnlineInterviewAssessorConduction_questionExpandImage" CssClass="ToggleImageStyle"
                                                                                    ToolTip="Hide Questions" ImageUrl="~/App_Themes/DefaultTheme/Images/minus_icon.gif"
                                                                                    runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr style="display: block">
                                                                <td class="question_bg" id="tdQuestionList">
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td align="left" valign="top">
                                                                                <div style="width: 100%; overflow: auto; height: 150px;" runat="server" id="contentInfo">
                                                                                    <asp:DataList ID="OnlineInterviewAssessorConduction_categoryQuestionDataList" runat="server">
                                                                                        <ItemTemplate>
                                                                                            <table cellpadding="0" cellspacing="0" border="0" align="left">
                                                                                                <tr>
                                                                                                    <td valign="top" align="left" style="width: 15px; padding-top: 3px;">
                                                                                                        <asp:Image ID="OnlineInterviewAssessorConduction_arrowImage" SkinID="sknArrowImage"
                                                                                                            runat="server" />
                                                                                                    </td>
                                                                                                    <td style="text-align: left" class="QuestionLink">
                                                                                                        <asp:UpdatePanel ID="OnlineInterviewAssessorConduction_questionUpdatePanel" runat="server"
                                                                                                            UpdateMode="Conditional">
                                                                                                            <ContentTemplate>
                                                                                                                <asp:LinkButton ID="OnlineInterviewAssessorConduction_questionLinkButton" runat="server"
                                                                                                                    OnClientClick="return false;" SkinID="sknQustionLinkButton" ToolTip='<%# Eval("Question") %>'
                                                                                                                    Text='<%# Eval("Question") %>' />
                                                                                                                <div id='<%# Eval("QuestionRelationId") %>' class="QuestionID">
                                                                                                                </div>
                                                                                                            </ContentTemplate>
                                                                                                        </asp:UpdatePanel>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_height_5">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="msg_align">
                <asp:Label ID="OnlineInterviewAssessorConduction_bottomSuccessMessageLabel" runat="server"
                    SkinID="sknSuccessMessage"></asp:Label>
                <asp:Label ID="OnlineInterviewAssessorConduction_bottomErrorMessageLabel" runat="server"
                    SkinID="sknErrorMessage"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="header_bg">
                <table border="0" cellpadding="0" cellspacing="0" align="right">
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
