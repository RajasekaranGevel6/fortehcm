﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubmitAssessorRating.aspx.cs"
    Inherits="Forte.HCM.OnlineInterviewConduction.SubmitAssessorRating" MasterPageFile="~/MasterPages/OverviewMaster.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/OverviewMaster.Master" %>
<asp:Content ID="SubmitAssessorRating_bodyContent" runat="server" ContentPlaceHolderID="OverviewMaster_body">
    <script language="javascript" type="text/javascript">
    function CloseSubmitRating() {
        // Handler method that will be called 
        // when the 'submit rating' button is clicked

        if (window.dialogArguments) {
            window.opener = window.dialogArguments;
        }

        self.close();
    }
    // Handler for onchange and onkeyup to check for max characters.
    function CommentsCount(characterLength, clientId) {
        var maxlength = new Number(characterLength);
        if (clientId.value.length > maxlength) {
            clientId.value = clientId.value.substring(0, maxlength);
        }
    }
</script>
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
            <td class="msg_align" style="text-align: center">
                <asp:UpdatePanel ID="SubmitAssessorRating_messageUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="SubmitAssessorRating_topSuccessMessageLabel" runat="server" SkinID="sknSuccessMessage"></asp:Label>
                        <asp:Label ID="SubmitAssessorRating_topErrorMessageLabel" runat="server" SkinID="sknErrorMessage"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="SubmitAssessorRating_submitButton" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="header_bg">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:Literal ID="OnlineInterviewConduction_overallCommentsLiteral" runat="server"
                                                        Text="Submit Score"></asp:Literal>
                                                </td>
                                                <td align="right">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="SubmitAssessorRating_logoutButton" runat="server" SkinID="sknButtonId"
                                                                    Text="Logout" OnClick="SubmitAssessorRating_logoutButton_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="SubmitAssessorRating_topCancelLinkButton" runat="server" SkinID="sknActionLinkButton"
                                                                    Text="Cancel" OnClientClick="window.close();return false;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="assessor_overall_comments_panel_body_bg" align="left" valign="top">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" width="80%" align="center">
                                                        <tr>
                                                            <td class="td_height_5">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 150px">
                                                                <asp:Label ID="SubmitAssessorRating_overAllCommentsLabel" SkinID="sknLabelFieldHeaderText" Text="Overall Comments" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="SubmitAssessorRating_overAllCommentsTextBox" runat="server" TextMode="MultiLine"
                                                                    Columns="100" Width="500px" Height="70px" Rows="50" onkeyup="CommentsCount(1000,this)"
                                                                    onchange="CommentsCount(1000,this)"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="interview_skill_body_bg">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="td_height_20">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="SubmitAssessorRating_skillsUpdatePanel" runat="server">
                                                                    <ContentTemplate>
                                                                        <div style="height: 300px; overflow: auto;">
                                                                            <asp:GridView ID="SubmitAssessorRating_skillsGridView" runat="server" AutoGenerateColumns="False"
                                                                                AllowSorting="False" SkinID="sknOnlieInterviewGrid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Skill" HeaderStyle-Width="20%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SubmitAssessorRating_skillsGridView_skillNameLabel" runat="server"
                                                                                                Text='<%# Eval("SkillName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Computed Score(%)" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Right">
                                                                                        <ItemStyle/>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="SubmitAssessorRating_skillsGridView_computedScoreLabel" Text='<%# Eval("Weightage").ToString()=="-1" ? "" : Eval("Weightage").ToString() %>'  runat="server"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Score(%)" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Right">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SubmitAssessorRating_skillsGridView_scoreTextBox" runat="server"
                                                                                                MaxLength="3" Width="25%" Text='<%# GetSkillScore(Convert.ToInt32(Eval("Weightage")),Convert.ToInt32(Eval("SkillRating"))) %>'></asp:TextBox>
                                                                                            <ajaxToolKit:FilteredTextBoxExtender ID="SubmitAssessorRating_scoreTextBoxExtender"
                                                                                                runat="server" FilterType="Custom,Numbers" TargetControlID="SubmitAssessorRating_skillsGridView_scoreTextBox"
                                                                                                ValidChars=".">
                                                                                            </ajaxToolKit:FilteredTextBoxExtender>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Comments" HeaderStyle-Width="50%">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SubmitAssessorRating_skillsGridView_commentsTextBox" 
                                                                                            runat="server" TextMode="MultiLine" Columns="100" Width="300px" Height="40px"  Rows="20" onkeyup="CommentsCount(500,this)" 
                                                                                            onchange="CommentsCount(500,this)" Text='<%# Eval("SkillComments") %>'></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="SubmitAssessorRating_skillsGridView_skillIDHiddenField" runat="server"
                                                                                                Value='<%# DataBinder.Eval(Container.DataItem, "SkillID")%>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <asp:HiddenField ID="SubmitAssessorRating_categoryIDHiddenField" runat="server" />
                                                                        <asp:HiddenField ID="SubmitAssessorRating_skillIDHiddenField" runat="server" />
                                                                        <asp:HiddenField ID="SubmitAssessorRating_skillHiddenField" runat="server" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_10">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:UpdatePanel ID="SubmitAssessorRating_submitUpdatePanel" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Button ID="SubmitAssessorRating_submitExitButton" runat="server" SkinID="sknButtonId"
                                                                Text="Submit Score and Exit Interview" OnClick="SubmitAssessorRating_submitExitButton_Click" />
                                                            &nbsp;
                                                            <asp:Button ID="SubmitAssessorRating_submitButton" runat="server" SkinID="sknButtonId"
                                                                Text="Submit Score and Return to Interview" OnClick="SubmitAssessorRating_submitButton_Click" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_height_5">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
