﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// Default.aspx.cs
// File that represents the user interface layout and functionalities for
// the Default page. This will helps to display the instruction

#endregion Header

#region Directives

using System;
using System.IO; 
using System.Web;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.Services;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;


using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.DataObjects;
using Forte.HCM.BL;
using Forte.HCM.Utilities;
using Forte.HCM.OnlineInterviewConduction.Common;

#endregion Directives

namespace Forte.HCM.OnlineInterviewConduction
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the Default page. This will helps to display the instruction. This class 
    /// inherits Forte.HCM.UI.Common.PageBase
    /// </summary>
    public partial class Default : PageBase
    {

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Clear controls
                ClearControls();

                if (!IsPostBack)
                {
                    // Set browser title.
                    Master.SetPageCaption("Online Interview Conduction");
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(Default_topErrorMessageLabel,
                    exp.Message);
            }
        }

       
        #endregion Event Handlers

        #region Private Methods                                                

        /// <summary>
        /// Method that clears the message controls
        /// </summary>
        private void ClearControls()
        {
            Default_topSuccessMessageLabel.Text = string.Empty;
            Default_topErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            return isValid;
        }

        #endregion Protected Overridden Methods

    }
}