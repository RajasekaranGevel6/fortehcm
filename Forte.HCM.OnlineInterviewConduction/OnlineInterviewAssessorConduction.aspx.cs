﻿#region Header

// Copyright (C) Forte. All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Forte.
//
// OnlineInterviewAssessorConduction.aspx.cs
// File that represents the user interface layout and functionalities for
// the OnlineInterviewAssessorConduction page. This will helps to assessor
// to assess the candidate.

#endregion Header

#region Directives

using System;

using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Support;
using Forte.HCM.Utilities;
using Forte.HCM.DataObjects;
using Forte.HCM.OnlineInterviewConduction.Common;
using System.Configuration;

#endregion Directives

namespace Forte.HCM.OnlineInterviewConduction
{
    /// <summary>
    /// Class that represents the user interface layout and functionalities for
    /// the OnlineInterviewAssessorConduction page. This will helps to assessor
    /// to assess the candidate. This class 
    /// inherits Forte.HCM.UI.Common.PageBase
    /// </summary>
    public partial class OnlineInterviewAssessorConduction : PageBase
    {
        #region Private Variables                                              

        public string interviewKey = string.Empty;

        public int candidateinterviewID = 0;

        public int assessorID = 0;

        #endregion Private Variables

        #region Event Handlers                                                 

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Clear controls
                ClearControls();

                // Check if interview key is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["interviewkey"]))
                {
                    // Get interview key.
                    interviewKey = Request.QueryString["interviewkey"].ToString();
                }

                // Check if assessor id is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["userid"]))
                {
                    // Get the assessor id
                    assessorID = Convert.ToInt32(Request.QueryString["userid"].ToString());
                }

                // Check if chatroom key is passed.
                if (!Utility.IsNullOrEmpty(Request.QueryString["chatroom"]))
                {
                    CandidateInterviewSessionDetail candidateInterviewDetail = new CandidateInterviewSessionDetail();

                    candidateInterviewDetail = new OnlineInterviewAssessorBLManager().GetOnlineInterviewDetail(interviewKey,
                        Request.QueryString["chatroom"].ToString().Trim());

                    // Get the candidate interview id
                    if (candidateInterviewDetail != null)
                    {
                        candidateinterviewID = candidateInterviewDetail.GenID;
                    }

                    if (candidateinterviewID == 0)
                        return;
                }

              
                // Set browser title.
                Master.SetPageCaption("Online Interview Conduction");

                if (!IsPostBack)
                {
                    string baseURL = ConfigurationManager.AppSettings["DEFAULT_URL"];

                    string iFrameSrc = string.Empty;

                    iFrameSrc = baseURL + "InterviewConductionPage.aspx?";

                    if (Request.QueryString["interviewkey"] != null)
                        iFrameSrc += "interviewkey=" + Request.QueryString["interviewkey"].ToString();

                    if (Request.QueryString["userid"] != null)
                        iFrameSrc += "&userid=" + Request.QueryString["userid"].ToString();

                    if (Request.QueryString["candidateid"] != null)
                        iFrameSrc += "&candidateid=" + Request.QueryString["candidateid"].ToString();

                    if (Request.QueryString["userrole"] != null)
                        iFrameSrc += "&userrole=" + Request.QueryString["userrole"].ToString();

                    if (Request.QueryString["chatroom"] != null)
                        iFrameSrc += "&chatroom=" + Request.QueryString["chatroom"].ToString();


                    OnlineInterviewAssessorConduction_conductionVideoIframe.Attributes["src"] = iFrameSrc;
                    
                    //LoadCategory();
                    LoadSkillQuestion(candidateinterviewID, assessorID);

                    // Load candidate and position profile details
                    LoadCandidateDetailsPositionProfileDetails(candidateinterviewID);
                }
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewAssessorConduction_topErrorMessageLabel,
                    exp.Message);
            }
        }

        /// <summary>
        /// Handler method that will be called 
        /// when exit and submit rating button is clicked
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        /// <remarks>
        /// This will redirect to submit rating page
        /// </remarks>
        protected void OnlineInterviewAssessorConduction_exitSubmitRatingButton_Click(object sender,
           EventArgs e)
        {
            try
            {
                Response.Redirect("SubmitAssessorRating.aspx?candidateinterviewid=" + candidateinterviewID 
                    + "&assessorid=" + assessorID, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewAssessorConduction_topErrorMessageLabel,
                    exp.Message);
            }
        }

        #endregion Event Handlers

        #region Web Methods                                                    

        /// <summary>
        /// Web method to get the selected question details
        /// </summary>
        /// <param name="questionId">
        /// A <see cref="string"/> that holds question id.
        /// </param>
        /// <returns></returns>
        [WebMethod]
        public static List<QuestionDetail> GetQuestion(string interviewKey, string candidateInterviewId,
            string assessorId, string questionId)
        {
            int quesionRelationID = 0;

            if (!Utility.IsNullOrEmpty(questionId))
            {
                quesionRelationID = Convert.ToInt32(questionId);
            }

            List<QuestionDetail> skillQuestion = new List<QuestionDetail>();

            skillQuestion = new OnlineInterviewBLManager().
                GetOnlineInterviewConductionSummary(interviewKey, 
                Convert.ToInt32(candidateInterviewId.ToString()), Convert.ToInt32(assessorId), 0);

            var query = from q in skillQuestion
                        where q.QuestionRelationId == quesionRelationID
                        select q;
            if (query != null)
            {
                return query.ToList();
            }
            else
            {
                return null;
            }
        }

        
        /// <summary>
        /// Method to get the other assessor rating
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<AssessorDetail> GetOtherAssessorRating(string candidateInterviewId,
            string assessorId, string questionId)
        {
            int quesionRelationID = 0;

            if (!Utility.IsNullOrEmpty(questionId))
            {
                quesionRelationID = Convert.ToInt32(questionId);
            }

            List<AssessorDetail> assessorDetail = new List<AssessorDetail>();

            assessorDetail = new OnlineInterviewBLManager().
                GetOnlineInterviewOtherAssessorRating( Convert.ToInt32(candidateInterviewId.ToString()),
                quesionRelationID, Convert.ToInt32(assessorId.ToString()));

            if (assessorDetail != null)
            {
                return assessorDetail;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Method to save the question rating and comments
        /// </summary>
        [WebMethod]
        public static bool SaveQuestionRatingAndComments(string candidateInterviewId, string assessorId,
            string questionId, string rating, string comments)
        {

            bool isRatingDone = false;
            int candidateInterviewID = 0;
            int assessorID = 0;

            QuestionDetail questionDetails = new QuestionDetail();

            if (!Utility.IsNullOrEmpty(questionId))
            {
                questionDetails.QuestionID = Convert.ToInt32(questionId);
            }
            if (!Utility.IsNullOrEmpty(rating))
            {
                questionDetails.Rating = Convert.ToInt32(rating);
            }

            if (!Utility.IsNullOrEmpty(candidateInterviewId))
            {
                candidateInterviewID = Convert.ToInt32(candidateInterviewId);
            }

            if (!Utility.IsNullOrEmpty(assessorId))
            {
                assessorID = Convert.ToInt32(assessorId);
            }

            questionDetails.Comments = comments;

            if (questionDetails.QuestionID == 0)
            {
                return isRatingDone = false;
            }
            try
            {
                new OnlineInterviewBLManager().SaveOnlineInterviewQuestionRatingComments(questionDetails,
                    candidateInterviewID, assessorID, assessorID);
                isRatingDone = true;
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return isRatingDone;
        }

        #endregion Web Methods

        #region Private Methods                                                

        /// <summary>
        /// Method that loads the candidate and position profile detail.
        /// </summary>
        /// <param name="assessmentSummary">
        /// A <see cref="AssessmentSummary"/> that holds the assessment summary.
        /// </param>
        private void LoadCandidateDetailsPositionProfileDetails(int candidateInterviewID)
        {
            AssessmentSummary candidateAssessmentSummaryDetail = new AssessmentSummary();

            candidateAssessmentSummaryDetail = new OnlineInterviewBLManager().
              GetOnlineInterviewAssessmentCandidate(candidateInterviewID);

            if (candidateAssessmentSummaryDetail != null)
            {
                OnlineInterviewAssessorConduction_interviewNameLabel.Text =
                    candidateAssessmentSummaryDetail.InterviewName;

                if (!Utility.IsNullOrEmpty(candidateAssessmentSummaryDetail.ScheduledDate))
                {
                    OnlineInterviewAssessorConduction_interviewScheduledDateLabel.Text =
                      "Interview Scheduled on " + candidateAssessmentSummaryDetail.ScheduledDate.ToString("MMMM dd, yyyy");
                }

                OnlineInterviewAssessorConduction_candidateNameLinkButton.Text =
                    candidateAssessmentSummaryDetail.CanidateName;

                // Add handler for candidate name link button.
                if (candidateAssessmentSummaryDetail.CandidateID != 0)
                {
                    OnlineInterviewAssessorConduction_candidateNameLinkButton.Attributes.Add("onclick",
                        "javascript:return OpenViewCandidateProfilePopup('" + candidateAssessmentSummaryDetail.CandidateID + "');");
                }

                OnlineInterviewAssessorConduction_clientNameLinkButton.Text = candidateAssessmentSummaryDetail.ClientName;

                // Add handler for client name link button.
                if (candidateAssessmentSummaryDetail.ClientID != 0)
                {
                    OnlineInterviewAssessorConduction_clientNameLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowViewClient('" + candidateAssessmentSummaryDetail.ClientID + "');");
                }

                // Get client details
                if (candidateAssessmentSummaryDetail.PositionProfileID != 0)
                {
                    ViewState["POSITION_PROFILE_ID"] = candidateAssessmentSummaryDetail.PositionProfileID;
                    ClientInformation clientInfo =
                        new ClientBLManager().GetClientInfoPositionProfileID(candidateAssessmentSummaryDetail.PositionProfileID);

                    if (!Utility.IsNullOrEmpty(clientInfo))
                    {
                        // Get client departments
                        new ClientInfoDataManager().GetClientDepartmentContactInfo(OnlineInterviewAssessorConduction_showClientDepartmentsLabel,
                            clientInfo.ClientDepartments, "CD", false);
                        // Get client contacts
                        new ClientInfoDataManager().GetClientDepartmentContactInfo(OnlineInterviewAssessorConduction_showClientContactsLabel,
                            clientInfo.ClientContacts, "CC", false);
                    }
                }

                OnlineInterviewAssessorConduction_positionProfileLinkButton.Text =
                    candidateAssessmentSummaryDetail.PositionProfileName;

                // Add handler for position profile link button.
                if (candidateAssessmentSummaryDetail.PositionProfileID != 0)
                {
                    OnlineInterviewAssessorConduction_positionProfileLinkButton.Attributes.Add("onclick",
                        "javascript:return ShowViewPositionProfile('" + candidateAssessmentSummaryDetail.PositionProfileID + "');");
                }
            }
        }

        /// <summary>
        /// Method to load the skills and question against the interview
        /// </summary>
        public void LoadSkillQuestion(int candidateInterviewID,
            int assessorID)
        {
            List<SkillDetail> skillDetail = new List<SkillDetail>();

            skillDetail = new OnlineInterviewBLManager().
                GetOnlineInterviewScheduledAssessorSkills(candidateInterviewID, assessorID);

            if (skillDetail != null && skillDetail.Count > 0)
            {
                //Bind the parent data list
                OnlineInterviewAssessorConduction_categoryDataList.DataSource = skillDetail;
                OnlineInterviewAssessorConduction_categoryDataList.DataBind();

                //Bind the child list

                List<QuestionDetail> skillQuestion = new List<QuestionDetail>();
                skillQuestion = new OnlineInterviewBLManager().GetOnlineInterviewConductionSummary(interviewKey,
                    candidateInterviewID, assessorID, 0);

                Session["SKILL_QUESTIONS"] = skillQuestion;

                // foreach loop over each item of DataList control
                foreach (DataListItem Item in OnlineInterviewAssessorConduction_categoryDataList.Items)
                {
                    BindNestedDataList(Item.ItemIndex);
                }
            }
        }


        /// <summary>
        /// Method to bind the nested datalist
        /// </summary>
        /// <param name="ItemIndex">ItemIndex</param>
        private void BindNestedDataList(int ItemIndex)
        {
            int skillID = Convert.ToInt32(OnlineInterviewAssessorConduction_categoryDataList.DataKeys[ItemIndex]);

            List<QuestionDetail> skillQuestions = new List<QuestionDetail>();

            if (Session["SKILL_QUESTIONS"] == null)
                skillQuestions = new List<QuestionDetail>();
            else
                skillQuestions = Session["SKILL_QUESTIONS"] as List<QuestionDetail>;

            //filter by skill id
            var questionFilteredList = Enumerable.Where(skillQuestions, s => s.SkillID == skillID);

            // findControl function to get the nested datalist control
            DataList nestedDataList = (DataList)OnlineInterviewAssessorConduction_categoryDataList.Items[ItemIndex].
                FindControl("OnlineInterviewAssessorConduction_categoryQuestionDataList");

            if (questionFilteredList != null)
            {
                nestedDataList.DataSource = questionFilteredList;
                nestedDataList.DataBind();
            }
        }

        /// <summary>
        /// Method that clears the message controls
        /// </summary>
        private void ClearControls()
        {
            OnlineInterviewAssessorConduction_topSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewAssessorConduction_topErrorMessageLabel.Text = string.Empty;
            OnlineInterviewAssessorConduction_questionRatingSuccessMessageLabel.Text = string.Empty;
            OnlineInterviewAssessorConduction_questionRatingErrorMessageLabel.Text = string.Empty;
        }

        #endregion Private Methods

        #region Protected Overridden Methods                                   

        /// <summary>
        /// Overridden method that loads values into user input controls such 
        /// as dropdown lists, list boxes, radio button lists, etc.
        /// </summary>
        protected override void LoadValues()
        {
        }

        /// <summary>
        /// Overridden method that validates the data entered by the user.
        /// </summary>
        /// <returns>
        /// A <see cref="bool"/> that holds the validity status. True indicates
        /// valid and false invalid.
        /// </returns>
        protected override bool IsValidData()
        {
            bool isValid = true;

            return isValid;
        }

        #endregion Protected Overridden Methods
       
        protected void OnlineInterviewAssessorConduction_logoutButton_Click(object sender, EventArgs e)
        {
            try 
            {
                Session.Abandon();
                string returnUrl = ConfigurationManager.AppSettings["ONLINE_ASSESSOR_URL"].ToString();
                Response.Redirect(returnUrl, false);
            }
            catch (Exception exp)
            {
                Logger.ExceptionLog(exp);
                base.ShowMessage(OnlineInterviewAssessorConduction_topErrorMessageLabel,
                    exp.Message);
            }
        }
}
}