﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InterviewConductionPage.aspx.cs"
    Inherits="Forte.HCM.OnlineInterviewConduction.InterviewConductionPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="OnlineVideoChat/history/history.css" />
    <script type="text/javascript" src="OnlineVideoChat/AC_OETags.js" language="javascript"></script>
    <script type="text/javascript" src="OnlineVideoChat/history/history.js" language="javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var requiredMajorVersion = 9;
        var requiredMinorVersion = 0;
        var requiredRevision = 28;                
    </script>
</head>
<body style="border:0px">
    <form id="form1" runat="server">
    <div style="width:800px;overflow:auto">
        <script language="JavaScript" type="text/javascript">
            var hasProductInstall = DetectFlashVer(6, 0, 65);

            var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

            if (hasProductInstall && !hasRequestedVersion) {
                var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
                var MMredirectURL = window.location;
                document.title = document.title.slice(0, 47) + " - Flash Player Installation";
                var MMdoctitle = document.title;

                AC_FL_RunContent(
		                        "src", "playerProductInstall",
		                        "FlashVars", "rtmp=<%= rtmp %>&URL=<%= URL %>&userRole=<%= userRole %>&userID=<%= userID %>&candidateID=<%= candidateID %>&chatRoomId=<%= chatRoomId %>&sessionId=<%= sessionId %>&securityURL=<%= securityURL %>&MMredirectURL=" + MMredirectURL + '&MMplayerType=' + MMPlayerType + '&MMdoctitle=' + MMdoctitle + "",
		                        "width", "776",
		                        "height", "364",
		                        "align", "left",
		                        "id", "Onlinechat",
		                        "quality", "high",
		                        "bgcolor", "#ffffff",
		                        "name", "Onlinechat",
		                        "allowScriptAccess", "sameDomain",
		                        "type", "application/x-shockwave-flash",
		                        "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                        );
            } else if (hasRequestedVersion) {
                AC_FL_RunContent(
			                            "src", "OnlineVideoChat/Onlinechat",
			                            "FlashVars", "rtmp=<%= rtmp %>&URL=<%= URL %>&userRole=<%= userRole %>&userID=<%= userID %>&candidateID=<%= candidateID %>&chatRoomId=<%= chatRoomId %>&sessionId=<%= sessionId %>&securityURL=<%= securityURL %>",
			                            "width", "776",
			                            "height", "364",
			                            "align", "left",
			                            "id", "Onlinechat",
			                            "quality", "high",
			                            "bgcolor", "#ffffff",
			                            "name", "Onlinechat",
			                            "allowScriptAccess", "sameDomain",
			                            "type", "application/x-shockwave-flash",
			                            "pluginspage", "http://www.adobe.com/go/getflashplayer"
	                            );
            } else {
                var alternateContent = 'Alternate HTML content should be placed here. '
                                                                + 'This content requires the Adobe Flash Player. '
                                                                + '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
                document.write(alternateContent);
            }
 
        </script>
        <noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="Onlinechat" width="776px"
                height="364px" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
                <param name="movie" value="Onlinechat.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="sameDomain" />
                <embed src="OnlineVideoChat/Onlinechat.swf" quality="high" bgcolor="#ffffff" width="776px"
                    height="364px" name="Onlinechat" align="left" play="true" loop="false" quality="high"
                    allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer">
			                                                </embed>
            </object>
        </noscript>
    </div>
    </form>
</body>
</html>
