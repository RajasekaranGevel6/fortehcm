﻿
using System;
using System.IO;
using System.Web.UI;
using System.Configuration;


using Forte.HCM.OnlineInterviewConduction.Common;
 
namespace Forte.HCM.OnlineInterviewConduction
{
    public partial class OnlineInterviewCandidateConduction : PageBase
    {
        #region Public Variables
        public string rtmp = string.Empty;
        public string userRole = string.Empty;
        public string userID = string.Empty;
        public string candidateID = string.Empty;
        public string URL = string.Empty;
        public string chatRoomName = string.Empty;
        public string chatRoomId = string.Empty;
        public string sessionId = string.Empty;
        public string securityURL = string.Empty;
        #endregion

        /// <summary>
        /// Handler method that will be called when the page is being loaded.
        /// </summary>
        /// <param name="sender">
        /// A <see cref="object"/> that holds the sender of the event.
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/>that holds the event data.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.SetPageCaption("Online Interview - Candidate Conduction");

            if (!IsPostBack)
            {
                string iFrameSrc = string.Empty;
                iFrameSrc = "InterviewConductionPage.aspx?";

                if (Request.QueryString["interviewkey"] != null)
                    iFrameSrc += "interviewkey=" + Request.QueryString["interviewkey"].ToString();

                if (Request.QueryString["userid"] != null)
                    iFrameSrc += "&userid="+Request.QueryString["userid"].ToString();

                if (Request.QueryString["candidateid"] != null)
                    iFrameSrc += "&candidateid="+Request.QueryString["candidateid"].ToString();

                if (Request.QueryString["userrole"] != null)
                    iFrameSrc += "&userrole="+Request.QueryString["userrole"].ToString(); 
                 
                if (Request.QueryString["chatroom"] != null)
                    iFrameSrc += "&chatroom=" + Request.QueryString["chatroom"].ToString();
                 


                //OnlineInterviewAssessorConduction_conductionVideoIframe.Attributes["src"] = iFrameSrc;
                
                // Set browser title. 
                //Master.SetPageCaption("Online Interview Candidate Conduction"); 
                OnlineInterviewCandidateConduction_iFrame.Attributes["src"] = iFrameSrc;
                //OnlineInterviewCandidateConduction_iFrame.Style.Add("background-color", "white");
               /* URL = ConfigurationManager.AppSettings["APP_DATA_URL"].ToString();

                securityURL = ConfigurationManager.AppSettings["CROSS_DOMAIN_URL"].ToString();

                if (Request.QueryString["userid"] != null)
                    userID = Request.QueryString["userid"].ToString();

                if (Request.QueryString["candidateid"] != null)
                    candidateID = Request.QueryString["candidateid"].ToString();

                if (Request.QueryString["userrole"] != null)
                    userRole = Request.QueryString["userrole"].ToString();

                if (Request.QueryString["interviewkey"] != null)
                    sessionId = Request.QueryString["interviewkey"].ToString();
                else
                    sessionId = userID;

                if (Request.QueryString["chatroom"] != null)
                    chatRoomId = Request.QueryString["chatroom"].ToString();
                else
                    chatRoomId = "R1";

                chatRoomName = CreatingFolder(sessionId, chatRoomId);

                rtmp = ConfigurationManager.AppSettings["RTMP"].ToString() + "/" + sessionId + "/" + chatRoomId;*/
            }
        }

        /// <summary>
        /// Method that creates online interview folder
        /// </summary>
        /// <param name="folderName">The folder name</param>
        /// <param name="ChatRoomName">The chat room name</param>
        /// <returns></returns>
        private string CreatingFolder(string folderName, string ChatRoomName)
        {
            string folderPath = ConfigurationManager.AppSettings["ONLINEINTERVIEWS"].ToString();
            folderPath += folderName;

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            string mainFolderPath = ConfigurationManager.AppSettings["ONLINEINTERVIEWS"].ToString();

            CreateChatRoom(folderPath, "\\" + ChatRoomName);

            CopyFiles(mainFolderPath, folderPath + "\\" + ChatRoomName + "\\");

            return folderPath + "\\" + ChatRoomName;
        }

        /// <summary>
        /// Method that creates chat room
        /// </summary>
        /// <param name="folderPath">The folder path</param>
        /// <param name="chatRoomName">The chat room name</param>
        private void CreateChatRoom(string folderPath, string chatRoomName)
        {
            if (!Directory.Exists(folderPath + chatRoomName))
                Directory.CreateDirectory(folderPath + chatRoomName);
        }

        /// <summary>
        /// Method that copy the files
        /// </summary>
        /// <param name="mainFolderPath">The mail folder path</param>
        /// <param name="folderPath">The folder path</param>
        private void CopyFiles(string mainFolderPath, string folderPath)
        {
            File.Copy(mainFolderPath + "main.asc", folderPath + "main.asc", true);
        }

        protected override bool IsValidData()
        {
            throw new NotImplementedException();
        }

        protected override void LoadValues()
        {
            throw new NotImplementedException();
        }
        protected void OnlineInterviewCandidateConduction_logOutButton_Click(object sender, EventArgs e)
        {
            try 
            {
                Session.Abandon();
                Request.QueryString["chatroom"].ToString();
                string returnUrl = ConfigurationManager.AppSettings["ONLINE_CANDIDATE_URL"].ToString();
                returnUrl = string.Format(returnUrl, Request.QueryString["chatroom"].ToString());

                Response.Redirect(returnUrl, false);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
}
}