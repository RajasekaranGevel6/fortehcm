﻿using System; 
using System.IO;
using System.Configuration;

using Forte.HCM.BL;
using Forte.HCM.Trace;
using Forte.HCM.Utilities;


namespace Forte.HCM.OnlineInterviewConduction
{
    public partial class InterviewConductionPage : System.Web.UI.Page
    {
        #region Public Variables
        public string rtmp = string.Empty;
        public string userRole = string.Empty;
        public string userID = string.Empty;
        public string candidateID = string.Empty;
        public string URL = string.Empty;
        public string chatRoomName = string.Empty;
        public string chatRoomId = string.Empty;
        public string sessionId = string.Empty;
        public string securityURL = string.Empty;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set browser title. 
                //Master.SetPageCaption("Online Interview Candidate Conduction"); 

                URL = ConfigurationManager.AppSettings["APP_DATA_URL"].ToString();

                securityURL = ConfigurationManager.AppSettings["CROSS_DOMAIN_URL"].ToString();

                if (Request.QueryString["userid"] != null)
                    userID =  new OnlineInterviewAssessorBLManager().GetUserEmailID(Convert.ToInt32( Request.QueryString["userid"]));
                    //userID =  new EncryptAndDecrypt().DecryptString(Request.QueryString["userid"].ToString());

                if (Request.QueryString["candidateid"] != null)
                    candidateID = new OnlineInterviewAssessorBLManager().GetUserEmailID(Convert.ToInt32(Request.QueryString["candidateid"])); 
                    //candidateID = new EncryptAndDecrypt().DecryptString(Request.QueryString["candidateid"].ToString());

                if (Request.QueryString["userrole"] != null)
                    userRole = Request.QueryString["userrole"].ToString();

                if (Request.QueryString["interviewkey"] != null)
                    sessionId = Request.QueryString["interviewkey"].ToString();
                else
                    sessionId = userID; 

                if (Request.QueryString["chatroom"] != null)
                    chatRoomId = Request.QueryString["chatroom"].ToString();
                else
                    chatRoomId = "R1";

                chatRoomName = CreatingFolder(sessionId, chatRoomId);

                rtmp = ConfigurationManager.AppSettings["RTMP"].ToString() + "/" + sessionId + "/" + chatRoomId; 
            }
        }


        /// <summary>
        /// Method that creates online interview folder
        /// </summary>
        /// <param name="folderName">The folder name</param>
        /// <param name="ChatRoomName">The chat room name</param>
        /// <returns></returns>
        private string CreatingFolder(string folderName, string ChatRoomName)
        {
            string folderPath = ConfigurationManager.AppSettings["ONLINEINTERVIEWS"].ToString();
            folderPath += folderName;

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            string mainFolderPath = ConfigurationManager.AppSettings["ONLINEINTERVIEWS"].ToString();

            CreateChatRoom(folderPath, "\\" + ChatRoomName);

            CopyFiles(mainFolderPath, folderPath + "\\" + ChatRoomName + "\\");

            return folderPath + "\\" + ChatRoomName;
        }

        /// <summary>
        /// Method that creates chat room
        /// </summary>
        /// <param name="folderPath">The folder path</param>
        /// <param name="chatRoomName">The chat room name</param>
        private void CreateChatRoom(string folderPath, string chatRoomName)
        {
            if (!Directory.Exists(folderPath + chatRoomName))
                Directory.CreateDirectory(folderPath + chatRoomName);
        }

        /// <summary>
        /// Method that copy the files
        /// </summary>
        /// <param name="mainFolderPath">The mail folder path</param>
        /// <param name="folderPath">The folder path</param>
        private void CopyFiles(string mainFolderPath, string folderPath)
        {
            File.Copy(mainFolderPath + "main.asc", folderPath + "main.asc", true);
        }

    }
}