﻿#region Namespace                                                              

using System;
using System.Web.UI;


#endregion Namespace

namespace Forte.HCM.OnlineInterviewConduction.CommonControls
{
    public partial class OverviewHeaderControl : UserControl
    {
        #region Variables                                                       

        /// <summary>
        /// A <see cref="string"/> that holds the home page URL.
        /// </summary>
        protected string homeUrl = string.Empty;

        #endregion Variables

        #region Events                                                         

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string[] uri = Request.Url.AbsoluteUri.Split('/');
                if (uri.Length > 4)
                {
                    // Compose home url.
                    homeUrl = uri[0] + "/" + uri[1] + "/" + uri[2] + "/" + uri[3] + "/Default.aspx";
                }

                if (IsPostBack)
                    return;
            }
            catch (Exception exp)
            {
                //Logger.ExceptionLog(exp);
            }
        }

        #endregion Events
    }
}